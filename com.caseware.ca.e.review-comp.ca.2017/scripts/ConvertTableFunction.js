// Windows Script Host program to go through each form's formName-tables.js
// file and change any stringified functions to actual functions.
//
// Parameters within tableCalculations() now use paramsObj
//
// Original:
// "tableCalculations": "function (summaryRow, bucket, num, rowIndex, colIndex, tableCalcs) {\r\n              if (tableCalcs.getNumVal(tableCalcs.getCellValue(summaryRow, 1)) > 100) {\r\n                tableCalcs.setCellValue(summaryRow, 1, 100);\r\n              }\r\n\r\n              //if (\r\n              //  tableCalcs.getNumVal(tableCalcs.getCellValue(summaryRow, 1)) > 50) {\r\n              //  console.log(tableCalcs.getNumVal(tableCalcs.getCellValue(summaryRow, 1)));\r\n              //  tableCalcs.setCellValue(summaryRow, 1, 50);\r\n              //}\r\n            }"
//
// Converted:
// tableCalculations: function(paramsObj) {
//    var summaryRow = paramsObj.summaryRow;
//    var tableCalcs = paramsObj.tableCalcUtils;
//    var numChanged = paramsObj.numChanged;
//    var bucket = paramsObj.bucket;
//    var rowIndex = paramsObj.rowIndex;
//    var colIndex = paramsObj.colIndex;

//    if (tableCalcs.getNumVal(tableCalcs.getCellValue(summaryRow, 1)) > 100) {
//        tableCalcs.setCellValue(summaryRow, 1, 100);
//    }
//}
//
// Usage: ConvertTableFunctions.js

debugger;

var objArgs = WScript.Arguments;

if (!String.prototype.trim) {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "");
    };
}

if (!String.prototype.rtrim) {
    String.prototype.rtrim = function () {
        return this.replace(/\s+$/, "");
    };
}

if (!String.prototype.trimAllSpaces) {
    String.prototype.trimAllSpaces = function () {
        return this.replace(/\s/g, "");
    };
}

if (!String.prototype.camelCase) {
    String.prototype.camelCase = function () {
        var pieces = this.split("-");
        var sret = pieces[0];
        for (var i = 1; i < pieces.length; i++) {
            if (pieces[i].length)
                sret = sret + pieces[i].substr(0, 1).toUpperCase() + pieces[i].substr(1);
        }
        return sret;
    };
}

try {
    var WshShell = WScript.CreateObject("WScript.Shell");
    var fso = new ActiveXObject("Scripting.FileSystemObject");
    var sPath = WScript.Arguments.length > 0 ? WScript.Arguments(0) : WshShell.CurrentDirectory;

    // make sure path given is valid
    var folder = fso.GetFolder(sPath);

    // recursively find all form folders by searching for form.json
    var folderList = new oFolderList(folder.Path, "form.json");

    for (var i = 0; i < folderList.folders.length; i++) {
        var sTableFile = folderList.folders[i].name;
        if (!sTableFile)
            continue;

        var sTableFile = folderList.folders[i].path + "\\" + sTableFile;
        if (!fso.FileExists(sTableFile))
            throw sTableFile + " does not exist";

        var FOR_READING = 1;
        var lines = (new oFile(sTableFile, FOR_READING)).readFile();

        var converted = new oTableConverter(lines);

        if (converted.bModified) {
            var FOR_WRITING = 2;
            (new oFile(sTableFile, FOR_WRITING)).writeFile(converted.text);
        }
    }
}

catch (e) {
    if (e)
        WScript.Echo("Error: " + e);

    WScript.Quit(1);
}

WScript.Echo("formName-tables.js files converted");

//////////////////////////////

function oTableConverter(text) {
    try {
        // Properties
        this.bModified = false;
        this.text = new Array();

        // Methods

        // Constructor
        for (var i = 0; i < text.length; i++) {
            var line = text[i].trimAllSpaces().toLowerCase();
            var sScan = "\"tablecalculations\":";
            if (line.substr(0, sScan.length) != sScan) {
                this.text.push(text[i]);
                continue;
            }

            this.bModified = true;

            this.text.push("      tableCalculations: function(paramsObj) {");
            this.text.push("        var summaryRow = paramsObj.summaryRow;");
            this.text.push("        var tableCalcs = paramsObj.tableCalcUtils;");
            this.text.push("        var numChanged = paramsObj.numChanged;");
            this.text.push("        var bucket = paramsObj.bucket;");
            this.text.push("        var rowIndex = paramsObj.rowIndex;");
            this.text.push("        var colIndex = paramsObj.colIndex;");

            var nBraces = 0;
            line = "";
            for (var j = 0; j < text[i].length; j++) {
                var myChar = text[i].charAt(j);
                if (myChar == "{") {
                    nBraces++;
                    if (nBraces == 1)
                        continue;
                }
                else if (myChar == "\\" && (text[i].charAt(j + 1) == "r" || text[i].charAt(j + 1) == "n")) {
                    j++;
                    if (text[i].charAt(j) == "r")
                        continue;

                    this.text.push(line);
                    line = "";
                    continue;
                }

                if (nBraces > 0)
                    line = line + myChar;

                if (myChar == "}") {
                    nBraces--;
                    if (nBraces < 0)
                        throw "Mismatched braces";
                }
            }

            if (nBraces)
                throw "Mismatched braces";

            line = line.rtrim();
            if (!line.length || line.charAt(line.length - 1) != "}")
                throw "Missing closing brace";

            var lastChar = text[i].charAt(text[i].length - 1);
            if (lastChar != "\"") {
                line = line + lastChar;
                lastChar = text[i].charAt(text[i].length - 2);
            }

            if (lastChar != "\"")
                throw "Missing end quote of stringified tableCalculations";

            this.text.push(line);
        }
    }

    catch (e) {
        if (e)
            throw new Error(103, "oTableConverter: " + e.message);
        throw e;
    }
}

//////////////////////////////

function oFile(path, mode) {
    try {
        // Properties
        this.sPath = path;
        this.TextStream = null;

        // Methods
        this.openFile = function (openMode) {
            if (!path)
                throw "Must supply name of form";

            var FOR_READING = 1;
            var FOR_WRITING = 2;
            var TRISTATE_USE_DEFAULT = -2;

            var fso = new ActiveXObject("Scripting.FileSystemObject");
            if (openMode && openMode == FOR_WRITING) {
                if (fso.FileExists(path))
                    fso.DeleteFile(path);

                this.TextStream = fso.CreateTextFile(path, true);
            }
            else {
                var fTextFile = fso.GetFile(path);
                this.TextStream = fTextFile.OpenAsTextStream(openMode ? openMode : FOR_READING, TRISTATE_USE_DEFAULT);
            }
        };

        this.readLine = function () {
            return this.TextStream.AtEndOfStream ? null : this.TextStream.ReadLine();
        };

        this.readFile = function () {
            this.openFile();
            var lines = new Array();
            var text;
            while ((text = this.readLine()) != null)
                lines.push(text.rtrim());

            this.closeFile();
            return lines;
        };

        this.writeFile = function (lines) {
            var FOR_WRITING = 2;
            this.openFile(FOR_WRITING);
            for (var i = 0; i < lines.length; i++)
                this.TextStream.writeLine(lines[i]);

            this.closeFile();
            return lines;
        };

        this.closeFile = function () {
            if (!this.TextStream) return;

            this.TextStream.Close();
            this.TextStream = null;
        };

        // Constructor
        var FOR_WRITING = 2;
        if (!mode || mode != FOR_WRITING) {
            this.openFile();
            this.closeFile();
        }
    }

    catch (e) {
        throw new Error(100, "oFile " + path + ": " + e.message);
    }
}

//////////////////////////////

function oFolderList(path, filespec) {
    try {
        // Properties
        this.folders = new Array();
        this.filespec = filespec;

        // Methods
        this.traverse = function (path) {
            var fso = new ActiveXObject("Scripting.FileSystemObject");
            var folder = fso.GetFolder(path);

            var sFileSpec = folder.Path + "\\" + this.filespec;
            if (fso.FileExists(sFileSpec) && folder.ParentFolder) {
                var folderFiles = new Enumerator(folder.files);
                for (; !folderFiles.atEnd() ; folderFiles.moveNext()) {
                    var fName = folderFiles.item().Name;
                    if (fName.toLowerCase().indexOf("-tables.js") != -1)
                        this.folders.push({ "path": folder.Path, "name": fName });

                }
            }

            // traverse any folders within this one
            var subFolders = new Enumerator(folder.subFolders);
            for (; !subFolders.atEnd() ; subFolders.moveNext())
                this.traverse(subFolders.item().Path);
        };

        // Constructor
        this.traverse(path);
    }

    catch (e) {
        if (e)
            throw new Error(103, "oFolderList: " + e.message);
        throw e;
    }
}

//////////////////////////////
