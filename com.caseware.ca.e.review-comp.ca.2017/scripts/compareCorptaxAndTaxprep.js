/**Usage:
 *from terminal install the following:
 *npm install fast-csv
 *npm install json2csv --save
 *export taxprep current year input to csv file: taxprepDataEnterThisYear.csv
 *export the whole taxprep return value input to another csv file: taxprepCompletedReturn.csv
 *import taxprepDataEnterThisYear.csv to corptax using the import button
 *after global calcs finish running, click "download global" to get corptax completed global value
 *you will get a file called globalCorpTax.json after click download
 *put taxprepCompletedReturn.csv and globalCorpTax.json in the same folder as this script
 *from terminal run: node compareCorptaxAndTaxprep.js
 *the output json will be named outputComparisonValue.json for developer
 *the output csv will be named outputComparisonValue.csv for end user
 */

/************************************************************************
 * ***********************HELPER FUNCTIONS*******************************
 ************************************************************************/

function isEmptyOrNull(value) {
  return value === undefined || value === null || value.toString().trim() === '';
}

//add 0 for all corptaxNum which have less than 3 digits
function padZeros(num) {
  if (isNaN(num) || num.toString().length >= 3)
    return num;
  return padZeros('0' + num.toString());
}

//Parse global and map out all repeat forms ordered on their sequences
function mapRepeatForms(global) {
  var result = {};
  Object.keys(global).forEach(function(formId) {
    var index = formId.indexOf('-');
    if (!(~index))
      return;

    var mainFormId = formId.substring(0, index);
    var sequence = global[formId].sequence ? global[formId].sequence.value : 1;

    //initialize attribute
    if (!result.hasOwnProperty(mainFormId))
      result[mainFormId] = [];

    result[mainFormId][sequence - 1] = formId;
  });
  return result;
}

function parseTpField(fullField) {
  var fields = fullField.split('.');
  if (fields.length < 2)
    return null;

  var result = {};

  //re-construct fullField + extract [X] indexes
  var indexes = [];
  result.fullField = '';
  fields.forEach(function(field, i) {
    var bracketIndex = field.indexOf('[');
    result.fullField += (bracketIndex === -1) ? field : field.substring(0, bracketIndex);

    if (i < fields.length - 1)
      result.fullField += '.';

    if (bracketIndex !== -1)
      indexes.push(field.substring(bracketIndex + 1, field.length - 1));
  });

  //process elements in field split starting from the last element
  var position = fields.length - 1;

  //process 'fieldId / column' part of taxPrep field
  result.num = fields[position];
  position--;

  //process 'table' part of taxPrep field if it exists
  var mapObj = mapping.hasOwnProperty(result.fullField) ? mapping[result.fullField] : {};
  var isTable = (Array.isArray(mapObj) && mapObj.hasOwnProperty('0')) ? mapObj[0].isTable : mapObj.isTable;
  if (isTable && !(fields.length <= 2)) { //length <= 2 means table but not repeated table
    if (indexes.length)
      result.rowIndex = indexes.pop() - 1;

    var field = fields[position];
    result.table = result.hasOwnProperty('rowIndex') ?
        field.substring(0, field.length - (2 + String.valueOf(result.rowIndex).length)) : field;
    position--;
  }

  //position should be >= 0 or mapping failed
  if (position < 0) {
    console.error('Mapping field mismatch: expected table for %s', fullField);
    return null;
  }

  //Process 'form' part of taxPrep field
  if (indexes.length)
    result.repeatIndex = indexes.pop() - 1;

  //If an index remains, it must be the link sequence
  if (indexes.length)
    result.linkIndex = indexes.pop();

  return result;
};

//helper function to strip out [x] sequences in taxprep field identifiers
function stripTaxprepBrackets(field) {
  var index = field ? field.indexOf('[') : -1;
  if (index === -1)
    return field;

  while (index !== -1) {
    field = field.slice(0, index) + field.slice(index + 3, field.length);
    index = field.indexOf('[');
  }
  return field;
}

/************************************************************************
 * ****************************MAIN SCRIPT*******************************
 ************************************************************************/

var fs = require("fs");
var csv = require('fast-csv');
var json2csv = require('json2csv');

//Define file paths and retrieve
var corpTaxPath = "globalCorpTax.json";
var taxprepPath = "taxprepCompletedReturn.csv";

//Path to taxprepMapping json file
var mapping = JSON.parse(fs.readFileSync("mapping.json"));

var corptaxObj = JSON.parse(fs.readFileSync(corpTaxPath));
var taxprepData = [];

fs.createReadStream(taxprepPath)
    .pipe(csv())
    .on('data', function (data) {
      taxprepData.push({
        taxprepId: data[0],
        taxprepCurrentYear: data[1],
        taxprepPriorYear: data[2],
        taxprepDescription: data[3],
      })
    })
    .on('end', executeAfterRead);

//function executed after we read in taxprepData
function executeAfterRead() {
  /*********************************
   * Construct the comparison output
   *********************************/
  var output = {};
  var repeatMap = mapRepeatForms(corptaxObj);
  var linkRepeatMap = {nextSeq: 0};

  taxprepData.forEach(function(line) {
    var taxprepId = stripTaxprepBrackets(line.taxprepId);
    if (!mapping.hasOwnProperty(taxprepId)) {
      // console.log('Mapping does not exist for taxprepId: %s', taxprepId);
      return;
    }
    //Skip empty values since they won't be imported anyway
    if (isEmptyOrNull(line.taxprepCurrentYear))
      return;

    //Get mapping + field info
    var mapObjects = mapping[taxprepId];
    if (!Array.isArray(mapObjects))
      mapObjects = [mapObjects];

    mapObjects.forEach(function(mapObj) {
      var importObj = parseTpField(line.taxprepId);

      var repeatNum;
      //get sequence unique to each linkIndex+repeatIndex pair
      if (importObj.linkIndex) {
        linkRepeatMap[importObj.linkIndex] = linkRepeatMap[importObj.linkIndex] || {};
        if (!isEmptyOrNull(linkRepeatMap[importObj.linkIndex][importObj.repeatIndex])) {
          repeatNum = linkRepeatMap[importObj.linkIndex][importObj.repeatIndex];
        }
        else {
          repeatNum = linkRepeatMap.nextSeq;
          linkRepeatMap[importObj.linkIndex][importObj.repeatIndex] = linkRepeatMap.nextSeq;
          linkRepeatMap.nextSeq++;
        }
        if(mapObj.corpTaxFormId === 'T2S8ADD'){
          console.log("T2S8Add has linkIndex repeatNum: " + repeatNum);
        }
      }
      else {
        repeatNum = importObj.repeatIndex ? importObj.repeatIndex : 0;
        if(mapObj.corpTaxFormId === 'T2S8ADD'){
          console.log("T2S8Add assigned 2nd block repeatNum: " + repeatNum);
        }
      }

      if (!importObj)
        return;
      if(mapObj.corpTaxFormId === 'T2S8ADD'){
        console.log("repeatNum: " + repeatNum);
      }
      //Get formId, retrieve repeat form id if necessary
      var formId = repeatMap.hasOwnProperty(mapObj.corpTaxFormId) ?
          repeatMap[mapObj.corpTaxFormId][repeatNum] : mapObj.corpTaxFormId;
      var num = padZeros(mapObj.corpTaxNum);

      if (!formId) {
        console.log('Corptax global did not have repeat form %s sequence %s',
            mapObj.corpTaxFormId, importObj.repeatIndex);
        return;
      }

      //Retrieve Corptax value from global
      var corpTaxValue;
      try {
        corpTaxValue = mapObj.isTable ?
            corptaxObj[formId][num].value.cells[importObj.rowIndex][mapObj.colIndex].value :
            corptaxObj[formId][num].value;
      } catch (e) {
        corpTaxValue = "Import Error";
      }

      //Initialize output.formId if not initialized
      if (!output.hasOwnProperty(formId))
        output[formId] = [];

      var compareObj = {
        corpTaxNum : mapObj.corpTaxNum,
        taxprepId : importObj.fullField,
        isTable : mapObj.isTable,
        rowIndex : importObj.rowIndex,
        colIndex : mapObj.colIndex,
        taxprepValue : line.taxprepCurrentYear,
        corpTaxValue : corpTaxValue,
        diffValue : !(line.taxprepCurrentYear == corpTaxValue)
      };
      output[formId].push(compareObj);
    });
  });

  /******************************
   * EXPORT OUTPUT AS JSON + CSV
   ******************************/
  //export output to a json file
  fs.writeFile('outputComparisonValue.json', JSON.stringify(output, null, 2), function () {
    console.log("The json file was saved!");
  });

  //export output as a csv file
  var fields = ['corpTaxFormId', 'corpTaxNum', 'taxprepId', 'isTable', 'rowIndex', 'colIndex', 'taxprepValue', 'corpTaxValue', 'diffValue'];
  var csvData = [];

  Object.keys(output).forEach(function(formId) {
    output[formId].forEach(function(compareObj) {
      compareObj.corpTaxFormId = formId;
      csvData.push(compareObj);
    });
  });

  json2csv({data: csvData, fields: fields}, function (err, csv) {
    if (err) console.log(err);
    fs.writeFile('outputComparisonValue.csv', csv, function (err) {
      if (err) throw err;
      console.log("The csv file was saved!");
    });
  });
}