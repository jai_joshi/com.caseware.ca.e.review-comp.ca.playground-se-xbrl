var fs = require('fs');
var jf = require('jsonfile');

var corpTaxTns = jf.readFileSync('corpTaxTns.json');
var efileTns = jf.readFileSync('efileTns.json');
var result = {};
var counter = {};

// borrowed from comparingTns.js
var notImplementedYet = [
  "t2s12", "t2s17", "t2s18", "t2s20", "t2s30", "t2s32", "t2s60", "t2s34", "t2s35", "t2s38", "t2s39", "t2s42", "t2s43", "t2s45",
  "t2s46", "t2s48", "t2s61", "t2s62", "t2s91", "t2s301", "t2s302", "t2s303", "t2s304", "t2s305", "t2s306", "t2s308",
  "t2s309", "t2s321", "t2s360", "t2s365", "t2s367", "t2s380", "t2s381", "t2s384", "t2s385", "t2s387", "t2s388",
  "t2s389", "t2s390", "t2s391", "t2s392", "t2s393", "t2s394", "t2s402", "t2s403", "t2s410", "t2s422", "t2s423",
  "t2s442", "t2s460", "t2s490", "t2s506", "t2s508", "t2s513", "t2s554", "t2s556", "t2s558", "t2s560", "t2s566", "t2s343"
];

// not checking gifi codes currently because of the restriction of getCorpTaxTnList.js
var limitation = ['t2s100', 't2s101', 't2s140', 't2s125'];

for (var form in efileTns) {
  var formcontra = form;
  if (form == 't2s200') {
    formcontra = 't2j';
  }
  if (corpTaxTns[formcontra] && limitation.indexOf(form) == -1) {
    // efileTns included the first line for schedule number as line code
    var sliced = efileTns[form].slice(1);
    for (var index = 0; index < sliced.length; index++) {
      //efileTns involved 4 digit line number because efile specs included line codes
      var num = sliced[index];
      // console.log(form, formcontra, num);
      // for the l996 form branched from t2s200
      if (form == 't2s200' && num == '996') {
        formcontra = 'l996';
      }
      else if (form == 't2s200') {
        formcontra = 't2j';
      }
      if (num.length > 3) {
        num = num.slice(1);
      }
      if (corpTaxTns[formcontra]['list'].indexOf(num) == -1) {
        if (!result[form]) {
          result[form] = [];
        }
        result[form].push(num);
      }
      else {
        if (!counter[form]) {
          counter[form] = [];
        }
        counter[form].push(num);
      }
    }
  }
  else if (notImplementedYet.indexOf(form) == -1 && limitation.indexOf(form) == -1) {
    result[form] = efileTns[form];
  }
}

var remarks = "//Remarks:\n//tn 055, 095, 096, 898 of t2s200 is agency box\n//t2s200 does not have tn 200\n//t2s200 is the t2 jacket\n";

// console.log('result', result);
fs.writeFileSync('checkResult.json', remarks + JSON.stringify(result));
fs.writeFileSync('counterCheckResult.json', JSON.stringify(counter));


