(function() {
  'use strict';
  if (!wpw.tax.script)
    wpw.tax.script = {};

  wpw.tax.script.getCorpTaxTnList = function() {
    function updateFormTn(formTns, formTnsList, tn, row, extraData) {
      if (tn) {
        formTns[tn] = angular.merge({type: row.type ? row.type : 'default'}, extraData);
        formTnsList.push(tn);
      }
    }

    var addressFields = ['add1', 'add2', 'city', 'prov', 'postal', 'country'];

    function iterateRows(rows, formTns, formTnsList) {
      rows.forEach(function(row) {
        updateFormTn(formTns, formTnsList, row.tn, row, {});

        switch (row.type) {
          case 'splitTable':
            iterateRows(row.side1, formTns, formTnsList);
            iterateRows(row.side2, formTns, formTnsList);
            break;
          case 'splitInputs':
            updateFormTn(formTns, formTnsList, row.other && row.other.tn, row, {tnType: 'otherTn'});
            break;
          case 'infoField':
            updateFormTn(formTns, formTnsList, row.tn1, row, {tnType: 'tn1'});
            updateFormTn(formTns, formTnsList, row.tn2, row, {tnType: 'tn2'});

            if (row.inputType == 'address') {
              addressFields.forEach(function(addField) {
                updateFormTn(formTns, formTnsList, row[addField + 'Tn'], row,
                    {tnType: addField + 'Tn', type: 'address'});
              });
            }
            break;
          case 'table':
            var tableObj = formTableObj[row.num];
            if (tableObj) {
              tableObj.columns.forEach(function(column, colIndex) {
                updateFormTn(formTns, formTnsList, column.tn, row,
                    {tnType: 'columnTn', colIndex: colIndex, tableNum: row.num, looping: !row.fixedRows});

                updateFormTn(formTns, formTnsList, column.totalTn, row,
                    {tnType: 'columnTotalTn', colIndex: colIndex, tableNum: row.num, looping: !row.fixedRows});

                if (column.type == 'tableAddress') {
                  addressFields.forEach(function(addField) {
                    updateFormTn(formTns, formTnsList, row[addField + 'Tn'], row,
                        {tnType: addField + 'Tn', colIndex: colIndex, tableNum: row.num, looping: !row.fixedRows});
                  });
                }
              });

              if (tableObj.fixedRows) {
                if (tableObj.cells) {
                  tableObj.cells.forEach(function(cellRow, rIndex) {
                    for (var colIndex in cellRow) {
                      var cell = cellRow[colIndex];
                      if (cell) {
                        updateFormTn(formTns, formTnsList, cell.tn, row,
                            {
                              tnType: 'cellTn',
                              rowIndex: rIndex,
                              colIndex: colIndex,
                              tableNum: row.num,
                              looping: !row.fixedRows
                            });
                      }

                    }
                  });
                }
              }
            }
            break;
          default:
            updateFormTn(formTns, formTnsList, row.extraInfoTn, row, {tnType: 'extraInfoTn'});
            updateFormTn(formTns, formTnsList, row.tn2, row, {tnType: 'tn2'});
            break;
        }
      });
    }

    var tns = {};
    for (var form in wpw.tax.global.formData) {
      var formData = wpw.tax.global.formData[form];
      tns[form] = {numbers: {}, list: []};
      var formTns = tns[form].numbers;
      var formTnsList = tns[form].list;

      var formTableObj = wpw.tax.create.retrieve('tables', form);

      formData.sections.forEach(function(section) {
        iterateRows(section.rows, formTns, formTnsList);
      })
    }

    console.log(JSON.stringify(tns));
  };

}());

