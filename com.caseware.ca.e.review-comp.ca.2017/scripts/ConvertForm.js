// Windows Script Host program to convert a CorpTax form folder, breaking out the
// formData JSON object in formName-controller.js into its own file: formName-formdata.js
//
// form.json is also converted
//
// Usage: ConvertForm.js <formFolderPath> <bOverWrite>
//
// formFolderPath - folder where CorpTax form files reside - if not supplied, the default directory is used
// bOverWrite - if supplied, files are overwritten. Otherwise, .new extension is appended to converted file names

debugger;

var objArgs = WScript.Arguments;
if (WScript.Arguments.length < 0)
{
    WScript.Echo("Usage: ConvertForm.js formFolderPath");
    WScript.Quit(1);
}

if (!String.prototype.trim) {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "");
    };
}

if (!String.prototype.trimAllSpaces) {
    String.prototype.trimAllSpaces = function () {
        return this.replace(/\s/g, "");
    };
}

if (!String.prototype.camelCase) {
    String.prototype.camelCase = function () {
        var pieces = this.split("-");
        var sret = pieces[0];
        for (var i = 1; i < pieces.length; i++) {
            if (pieces[i].length)
                sret = sret + pieces[i].substr(0, 1).toUpperCase() + pieces[i].substr(1);
        }
        return sret;
    };
}

try	{
    var WshShell = WScript.CreateObject("WScript.Shell");
    var sPath = WScript.Arguments.length > 0 ? WScript.Arguments(0) : WshShell.CurrentDirectory;
    var bOverWrite = WScript.Arguments.length >= 2 ? WScript.Arguments(1) : false;

    // make sure path given is valid
    var fso = new ActiveXObject("Scripting.FileSystemObject");
    var folder = fso.GetFolder(sPath);

    // recursively find all form folders by searching for form.json
    var folderList = new oFolderList(folder.Path, "form.json", ["t2s1", "roll-forward", "diagnostics-display"]);

    for (var i = 0; i < folderList.folders.length; i++) {
        // form name is the same as the parent folder
        var formName = folderList.folders[i].name;
        if (!formName)
            continue;

        var bFoundController = false;
        var fController = folderList.folders[i].path + "\\" + formName + "-controller.js";
        if (!fso.FileExists(fController)) {
            // parent folder can have different name - search for a controller
            var folder = fso.GetFolder(folderList.folders[i].path);
            var files = new Enumerator(folder.files);
            for (; !files.atEnd() ; files.moveNext()) {
                var controllerPos = files.item().Name.search(/-controller\.js/i);
                if (controllerPos > 0) {
                    formName = files.item().Name.substr(0, controllerPos);
                    fController = folderList.folders[i].path + "\\" + formName + "-controller.js";
                    bFoundController = fso.FileExists(fController);
                    break;
                }
            }
        }
        else
            bFoundController = true;

        if (bFoundController) {
            var myConverter = new oConverter({ "form": formName, "path": folderList.folders[i].path });
            myConverter.convert(bOverWrite);
        }
        else
            WScript.Echo("Warning: " + fController + " not found");
    }
}

catch (e)
{
    if (e)
        WScript.Echo("Error: " + e.message);

	WScript.Quit(1);
}

WScript.Echo(sPath + " converted successfully");


//////////////////////////////

function oFolderList(path, filespec, exclusions) {
    try {
        // Properties
        this.folders = new Array();
        this.filespec = filespec;
        this.excl = new ActiveXObject("Scripting.Dictionary");

        // Methods
        this.traverse = function (path) {
            var fso = new ActiveXObject("Scripting.FileSystemObject");
            var folder = fso.GetFolder(path);
            if (this.excl.Exists(folder.Name.toLowerCase()))
                return;

            if (fso.FileExists(folder.Path + "\\" + this.filespec) && folder.ParentFolder)
                this.folders.push({ "path": folder.Path, "name": folder.ParentFolder.Name });

            // traverse any folders within this one
            var subFolders = new Enumerator(folder.subFolders);
            for (; !subFolders.atEnd() ; subFolders.moveNext())
                this.traverse(subFolders.item().Path);
        };

        // Constructor
        if (exclusions && exclusions.constructor == Array) {
            for (var i = 0; i < exclusions.length; i++)
                this.excl.add(exclusions[i].toLowerCase(), null);
        }
        this.traverse(path);
    }

    catch (e) {
        if (e)
            throw new Error(103, "oFolderList: " + e.message);
        throw e;
    }
}

//////////////////////////////

function oConverter(params) {
    try {
        if (!params.form || params.form.constructor != String)
            throw "Must supply name of form";

        if (!params.path || params.path.constructor != String)
            throw "Must supply form folder to convert";

        // Properties
        this.formName = null;
        this.formPath = null;
        this.form = null;
        this.calcs = null;
        this.formData = null;
        this.formJSON = null;

        // Methods
        this.convert = function (bOverWrite) {

            this.formData = new Array();
            this.formData.push("(function() {");
            this.formData.push("  'use strict';");

            // find first line to start converting after line beginning with "function($scope"
            for (var begLine = 0; begLine < this.form.controller.length; begLine++) {
                var sLine = this.form.controller[begLine].trimAllSpaces().toLowerCase();
                if (sLine.substr(0, 15) == "function($scope") {
                    if (sLine != "function($scope){" &&
                        sLine != "function($scope,$routeparams){")
                        WScript.Echo("Warning: " + this.formName + "-controller.js has declaration \"" + this.form.controller[begLine].trim() + "\"");

                    begLine++;
                    break;
                }
            }

            if (begLine >= this.form.controller.length)
                throw "Unable to find starting line \"function($scope) {\"";

            var bFormData = false;
            var bWithinDiag = false;
            for (var i = begLine; i < this.form.controller.length; i++) {
                var sLine = this.form.controller[i].trimAllSpaces().toLowerCase();
                if (sLine.substr(0, 15) == "$scope.formdata") {
                    bFormData = true;
                    this.formData.push("  wpw.tax.formData." + this.formName + " = {");
                }
                else {
                    // must check for diagnostic block which ends with "};"
                    if (bFormData && sLine.substr(0, 7) == "return{") {
                        bWithinDiag = true;
                    }
                    if (sLine != "'usestrict';" &&
                        sLine != "$scope.id=$routeparams.id;") {
                        this.formData.push(this.form.controller[i]);
                        if (bFormData && !bWithinDiag && sLine == "};") {
                            i++;
                            break;
                        }
                    }
                    if (bWithinDiag && sLine.length >= 2 && sLine.substr(sLine.length - 2) == "};")
                        bWithinDiag = false;
                }
            }

            this.formData.push("})();");

            // remainder of the file is calculations only
            this.calcs = new Array();
            this.calcs.push("(function() {");
            var bFoundHeader = false;
            for (; i < this.form.controller.length; i++) {
                var sLine = this.form.controller[i].trimAllSpaces().toLowerCase();
                if (sLine == "])" || sLine == "]);") {
                    this.calcs.pop();
                    break;
                }
                else if (!bFoundHeader && sLine.substr(0, 19) == "$scope.calculations") {
                    bFoundHeader = true;
                    this.calcs.push("  wpw.tax.formCalculations." + this.formName + " = function(paramsObj) {");
                    this.calcs.push("");
                    this.calcs.push("    var bucket = paramsObj.bucket;");
                    this.calcs.push("    var num = paramsObj.numChanged;");
                    this.calcs.push("    var calcs = paramsObj.calcUtils;");
                }
                else
                    this.calcs.push(this.form.controller[i]);
            }
            this.calcs.push("})();");

            // form.json
            this.formJSON = new Array();
            for (var i = 0; i < this.form.JSON.length; i++) {
                var bPush = true;
                var sLine = this.form.JSON[i].trim().toLowerCase();
                if (sLine.substr(0, 9) == "\"default\"") {
                    this.formJSON.push("  \"default\": \"common/tax-abstract.html\",");
                    bPush = false;
                }
                else if (sLine.substr(0, 4) == "\"id\"") {
                    // isloate form id which is last string in quotes
                    var begQuote;
                    var endQuote = this.form.JSON[i].length - 1;
                    while (this.form.JSON[i].charAt(endQuote) != "\"" && endQuote >= 0)
                        endQuote--;

                    if (endQuote > 0) {
                        begQuote = endQuote - 1;
                        while (begQuote >= 0 && this.form.JSON[i].charAt(begQuote) != "\"")
                            begQuote--;
                        if (begQuote >= 0)
                        {
                            var posName = endQuote - 1;
                            while (posName > begQuote && this.form.JSON[i].charAt(posName) != ".")
                                posName--;

                            if (posName > begQuote) {
                                sLine = this.form.JSON[i].substr(0, posName + 1) + this.formName +
                                    this.form.JSON[i].substr(endQuote, this.form.JSON[i].length - endQuote + 1);
                                this.formJSON.push(sLine);
                                bPush = false;
                            }
                        }
                    }
                }
                else if (sLine.substr(0, 4) == "\"js\"") {
                    this.formJSON.push("  \"js\": [\"" + this.formName + "-formdata.js\", \"" + this.formName + "-calculations.js\"]");
                    bPush = false;
                }

                if (bPush)
                    this.formJSON.push(this.form.JSON[i]);
            }

            // write form.json, form-calculations.js, and form-formdata.js
            this.form.write({
                "name": this.formName, "JSON": this.formJSON, "calcs": this.calcs,
                "formData": this.formData, "overWrite": bOverWrite
            });
        };

        this.rename = function (oldname, newname) {
            // rename folder and its matching documents
            var fso = new ActiveXObject("Scripting.FileSystemObject");
            var folder = fso.GetFolder(this.formPath);
            var folderName = folder.Name;
            fc = new Enumerator(folder.files);
            for (; !fc.atEnd() ; fc.moveNext()) {
                if (oldname.toLowerCase() == fc.item().Name.substr(0, oldname.length).toLowerCase())
                {
                    var fold = fc.item().Path;
                    var fnew = fc.item().ParentFolder.Path + "\\" + newname + fc.item().Name.substr(oldname.length);
                    fso.moveFile(fold, fnew);
                }
            }

            var fold = folder.ParentFolder.Path;
            var fnew = folder.ParentFolder.ParentFolder.Path + "\\" + newname;
            fso.MoveFolder(fold, fnew);

            this.formName = newname;
            this.formPath = fnew + "\\" + folderName;
		};

        // Constructor

        // if form name has a dash(es), remove and change to camel case notation
        this.formName = params.form.camelCase();
		if (params.form != this.formName) {
		    this.formPath = params.path;
            this.rename(params.form, this.formName);
            params.path = this.formPath;
		}

        // make sure path given is valid
		var fso = new ActiveXObject("Scripting.FileSystemObject");
		var folder = fso.GetFolder(params.path);
		this.formPath = folder.Path;

        // load form-controller and form.json into internal arrays for later manipulation
		this.form = new oForm(this.formName, this.formPath, true);
    }

    catch(e) {
		if (e)
			throw new Error(102, "oConvert: " + e.message);
		throw e;
	}
}

//////////////////////////////

function oForm(formName, path) {
    try {
        if (!formName)
            throw "Must supply name of form";

        var sController = path + "\\" + formName + "-controller.js";
        var sJSON = path + "\\form.json";
        var FOR_WRITING = 2;

        // Properties
        this.form = formName;
        this.fController = new oFile(sController);
        this.fJSON = new oFile(sJSON);

        // Methods
        this.getController = function () {
            return this.controller;
        };

        this.getJSON = function () {
            return this.JSON;
        };

        this.write = function (params) {
            if (!params.name)
                throw "Must supply name of form";

            if (params.JSON)
                this.writeFile(path + "\\" + (params.overWrite ? "" : "new-") + "form.json", params.JSON);

            if (params.calcs)
                this.writeFile(path + "\\" + (params.overWrite ? "" : "new-") + this.form + "-calculations.js", params.calcs);

            if (params.formData)
                this.writeFile(path + "\\" + (params.overWrite ? "" : "new-") + this.form + "-formdata.js", params.formData);
        };

        this.writeFile = function (fName, body) {
            var FOR_WRITING = 2;
            var myFile = new oFile(fName, FOR_WRITING);
            myFile.writeFile(body);
        }

        // Constructor
        this.controller = this.fController.readFile();
        this.JSON = this.fJSON.readFile();
    }

    catch (e) {
        if (e)
            throw new Error(101, "oForm " + formName + ": " + e.message);
        throw e;
    }
}

//////////////////////////////

function oFile(path, mode) {
    try {
		// Properties
		this.sPath = path;
		this.TextStream = null;

		// Methods
		this.openFile = function(openMode) {
			if(!path)
				throw "Must supply name of form";

			var FOR_READING = 1;
			var FOR_WRITING = 2;
			var TRISTATE_USE_DEFAULT = -2;

			var fso = new ActiveXObject("Scripting.FileSystemObject");
			if (openMode && openMode == FOR_WRITING) {
			    if (fso.FileExists(path))
			        fso.DeleteFile(path);

			    this.TextStream = fso.CreateTextFile(path, true);
			}
            else {
                var fTextFile = fso.GetFile(path);
                this.TextStream = fTextFile.OpenAsTextStream(openMode ? openMode : FOR_READING, TRISTATE_USE_DEFAULT);
            }
        };

		this.readLine = function() {
			return this.TextStream.AtEndOfStream ? null: this.TextStream.ReadLine();
		};

		this.readFile = function() {
			this.openFile();
			var lines = new Array();
			var text;
			while ((text = this.readLine()) != null)
				lines.push(text);

			this.closeFile();
			return lines;
		};

		this.writeFile = function(lines) {
			var FOR_WRITING = 2;
			this.openFile(FOR_WRITING);
			for (var i = 0; i < lines.length; i++)
				this.TextStream.writeLine(lines[i]);

			this.closeFile();
			return lines;
		};

		this.closeFile = function() {
			if (!this.TextStream) return;

			this.TextStream.Close();
			this.TextStream = null;
		};

		// Constructor
		var FOR_WRITING = 2;
		if (!mode || mode != FOR_WRITING) {
		    this.openFile();
		    this.closeFile();
		}
    }

    catch(e) {
		throw new Error(100, "oFile " + path + ": " + e.message);
	}
}

//////////////////////////////
