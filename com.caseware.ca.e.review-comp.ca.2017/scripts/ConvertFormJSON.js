// Windows Script Host program to add formName-tables.js to each tax form's
// form.json file
//
// Usage: ConvertFormJSON.js

debugger;

var objArgs = WScript.Arguments;

if (!String.prototype.trim) {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "");
    };
}

if (!String.prototype.trimAllSpaces) {
    String.prototype.trimAllSpaces = function () {
        return this.replace(/\s/g, "");
    };
}

if (!String.prototype.camelCase) {
    String.prototype.camelCase = function () {
        var pieces = this.split("-");
        var sret = pieces[0];
        for (var i = 1; i < pieces.length; i++) {
            if (pieces[i].length)
                sret = sret + pieces[i].substr(0, 1).toUpperCase() + pieces[i].substr(1);
        }
        return sret;
    };
}

try {
    var WshShell = WScript.CreateObject("WScript.Shell");
    var fso = new ActiveXObject("Scripting.FileSystemObject");
    var sPath = WScript.Arguments.length > 0 ? WScript.Arguments(0) : WshShell.CurrentDirectory;

    // make sure path given is valid
    var folder = fso.GetFolder(sPath);

    // recursively find all form folders by searching for form.json
    var folderList = new oFolderList(folder.Path, "form.json");

    for (var i = 0; i < folderList.folders.length; i++) {
        var sTableFile = folderList.folders[i].name;
        if (!sTableFile)
            continue;

        var sJSONfile = folderList.folders[i].path + "\\form.json";
        if (fso.FileExists(sJSONfile)) {
            // add formName-tables.js to form.json -> "js": []
            var bModified = false;
            var FOR_READING = 1;
            var lines = (new oFile(sJSONfile, FOR_READING)).readFile();
            for (var j = 0; j < lines.length; j++) {
                // find "js":
                var myLine = lines[j].trimAllSpaces().toLowerCase();
                var sTableStr = "\"" + sTableFile + "\"";
                if (myLine.substr(0, 6) == "\"js\":[") {
                    if (myLine.indexOf(sTableStr.toLowerCase()) >= 0) {
                        bModified = true;
                        break;
                    }

                    var endPos = lines[j].indexOf("]");
                    if (endPos >= 0) {
                        lines[j] = lines[j].substr(0, endPos) + ", " + sTableStr + "]";

                        var FOR_WRITING = 2;
                        (new oFile(sJSONfile, FOR_WRITING)).writeFile(lines);
                        bModified = true;
                        break;
                    }
                    else
                        throw sJSONfile + " missing closing ']' in line: " + lines[j];
                }
            }
            if (!bModified)
                throw sJSONfile + " missing line \"js\": []";
        }
        else
            throw sJSONfile + " does not exist";

    }
}

catch (e) {
    if (e)
        WScript.Echo("Error: " + e);

    WScript.Quit(1);
}

WScript.Echo("form.json files converted to include formName-tables.js");

//////////////////////////////

function oFile(path, mode) {
    try {
        // Properties
        this.sPath = path;
        this.TextStream = null;

        // Methods
        this.openFile = function (openMode) {
            if (!path)
                throw "Must supply name of form";

            var FOR_READING = 1;
            var FOR_WRITING = 2;
            var TRISTATE_USE_DEFAULT = -2;

            var fso = new ActiveXObject("Scripting.FileSystemObject");
            if (openMode && openMode == FOR_WRITING) {
                if (fso.FileExists(path))
                    fso.DeleteFile(path);

                this.TextStream = fso.CreateTextFile(path, true);
            }
            else {
                var fTextFile = fso.GetFile(path);
                this.TextStream = fTextFile.OpenAsTextStream(openMode ? openMode : FOR_READING, TRISTATE_USE_DEFAULT);
            }
        };

        this.readLine = function () {
            return this.TextStream.AtEndOfStream ? null : this.TextStream.ReadLine();
        };

        this.readFile = function () {
            this.openFile();
            var lines = new Array();
            var text;
            while ((text = this.readLine()) != null)
                lines.push(text);

            this.closeFile();
            return lines;
        };

        this.writeFile = function (lines) {
            var FOR_WRITING = 2;
            this.openFile(FOR_WRITING);
            for (var i = 0; i < lines.length; i++)
                this.TextStream.writeLine(lines[i]);

            this.closeFile();
            return lines;
        };

        this.closeFile = function () {
            if (!this.TextStream) return;

            this.TextStream.Close();
            this.TextStream = null;
        };

        // Constructor
        var FOR_WRITING = 2;
        if (!mode || mode != FOR_WRITING) {
            this.openFile();
            this.closeFile();
        }
    }

    catch (e) {
        throw new Error(100, "oFile " + path + ": " + e.message);
    }
}

//////////////////////////////

function oFolderList(path, filespec) {
    try {
        // Properties
        this.folders = new Array();
        this.filespec = filespec;

        // Methods
        this.traverse = function (path) {
            var fso = new ActiveXObject("Scripting.FileSystemObject");
            var folder = fso.GetFolder(path);

            var sFileSpec = folder.Path + "\\" + this.filespec;
            if (fso.FileExists(sFileSpec) && folder.ParentFolder) {
                var folderFiles = new Enumerator(folder.files);
                for (; !folderFiles.atEnd() ; folderFiles.moveNext()) {
                    var fName = folderFiles.item().Name;
                    if (fName.toLowerCase().indexOf("-tables.js") != -1)
                        this.folders.push({ "path": folder.Path, "name": fName });

                }
            }

            // traverse any folders within this one
            var subFolders = new Enumerator(folder.subFolders);
            for (; !subFolders.atEnd() ; subFolders.moveNext())
                this.traverse(subFolders.item().Path);
        };

        // Constructor
        this.traverse(path);
    }

    catch (e) {
        if (e)
            throw new Error(103, "oFolderList: " + e.message);
        throw e;
    }
}

//////////////////////////////
