// Windows Script Host program to separate table objects from a text file and create
// corresponding form-tables.js files
//
// Usage: ConvertTable.js textfile

debugger;

var objArgs = WScript.Arguments;
if (WScript.Arguments.length < 1) {
  WScript.Echo("Usage: ConvertTable.js textfile");
  WScript.Quit(1);
}

if (!String.prototype.trim) {
  String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "");
  };
}

if (!String.prototype.trimAllSpaces) {
  String.prototype.trimAllSpaces = function() {
    return this.replace(/\s/g, "");
  };
}

if (!String.prototype.camelCase) {
  String.prototype.camelCase = function() {
    var pieces = this.split("-");
    var sret = pieces[0];
    for (var i = 1; i < pieces.length; i++) {
      if (pieces[i].length)
        sret = sret + pieces[i].substr(0, 1).toUpperCase() + pieces[i].substr(1);
    }
    return sret;
  };
}

try {
  var WshShell = WScript.CreateObject("WScript.Shell");
  var fso = new ActiveXObject("Scripting.FileSystemObject");

  // oTables is created from a text file supplied with form name and corresponding properties
  //
  // i.e.:
  //
  //"t2s7": {
  //    "301": {
  //        "repeats": ["302"],
  //	    "executeAtEnd": true
  //    },
  //    "302": {
  //        "totals": {
  //            "1": "350",
  //		    "4": "385",
  //		    "5": "360"
  //        },
  //	    "startTable": "301"
  //    }
  //}

  var tableObj = new oTables(WScript.Arguments(0));

  for (var i = 0; i < tableObj.tables.length; i++) {
    // form name is the same as the parent folder
    var formName = tableObj.tables[i].formName;
    if (!formName)
      throw "Table object array with nameless form";

    var fPath = formName + "\\1";
    if (!fso.FolderExists(fPath))
      throw "Folder: " + fPath + " does not exist";

    var FOR_WRITING = 2;
    var fName = fPath + "\\" + formName + "-tables.js";
    var myFile = new oFile(fName, FOR_WRITING);
    myFile.writeFile(tableObj.tables[i].lines);
  }
}

catch (e) {
  if (e)
    WScript.Echo("Error: " + e.message);

  WScript.Quit(1);
}

WScript.Echo("formName-tables.js created successfully from " + WScript.Arguments(0));

//////////////////////////////

function oTables(sTextFile) {
  try {
    // Properties
    this.tables = new Array();

    // Methods

    // Constructor
    var FOR_READING = 1;
    var fText = new oFile(sTextFile, FOR_READING);
    var text = fText.readFile();

    // first and last lines of the text file have
    var nBraces = 0;
    for (var i = 1; i < text.length - 1; i++) {
      var line = text[i].trimAllSpaces();
      if (!line.length || line.charAt(0) != "\"" || line.charAt(line.length - 1) != "{")
        throw "Expecting form name";

      var endQuote = line.indexOf("\"", 1);
      if (endQuote < 0)
        throw "Expecting quoted form name";

      var formName = line.substr(1, endQuote - 1);

      // get all table lines for this form
      var lines = new Array();
      lines.push("(function(){");
      lines.push("wpw.tax.global.tableCalculations." + formName + " = {");
      var nBraces = 1;
      for (var j = i + 1; j < text.length; j++) {
        var line = text[j].trimAllSpaces();
        for (var k = 0; k < line.length; k++) {
          var lineChar = line.charAt(k);
          if (lineChar == "{")
            nBraces++;
          else if (lineChar == "}")
            nBraces--
        }
        if (nBraces == 0)
          break;

        lines.push(text[j]);
      }
      lines.push("}");
      lines.push("})();");

      var table = new Object();
      table.formName = formName;
      table.lines = lines;
      this.tables.push(table);

      i = j;
    }
  }

  catch (e) {
    if (e)
      throw new Error(103, "oTables: " + e.message);
    throw e;
  }
}

//////////////////////////////

function oFile(path, mode) {
  try {
    // Properties
    this.sPath = path;
    this.TextStream = null;

    // Methods
    this.openFile = function(openMode) {
      if (!path)
        throw "Must supply name of form";

      var FOR_READING = 1;
      var FOR_WRITING = 2;
      var TRISTATE_USE_DEFAULT = -2;

      var fso = new ActiveXObject("Scripting.FileSystemObject");
      if (openMode && openMode == FOR_WRITING) {
        if (fso.FileExists(path))
          fso.DeleteFile(path);

        this.TextStream = fso.CreateTextFile(path, true);
      }
      else {
        var fTextFile = fso.GetFile(path);
        this.TextStream = fTextFile.OpenAsTextStream(openMode ? openMode : FOR_READING, TRISTATE_USE_DEFAULT);
      }
    };

    this.readLine = function() {
      return this.TextStream.AtEndOfStream ? null : this.TextStream.ReadLine();
    };

    this.readFile = function() {
      this.openFile();
      var lines = new Array();
      var text;
      while ((text = this.readLine()) != null)
        lines.push(text);

      this.closeFile();
      return lines;
    };

    this.writeFile = function(lines) {
      var FOR_WRITING = 2;
      this.openFile(FOR_WRITING);
      for (var i = 0; i < lines.length; i++)
        this.TextStream.writeLine(lines[i]);

      this.closeFile();
      return lines;
    };

    this.closeFile = function() {
      if (!this.TextStream) return;

      this.TextStream.Close();
      this.TextStream = null;
    };

    // Constructor
    var FOR_WRITING = 2;
    if (!mode || mode != FOR_WRITING) {
      this.openFile();
      this.closeFile();
    }
  }

  catch (e) {
    throw new Error(100, "oFile " + path + ": " + e.message);
  }
}

//////////////////////////////
