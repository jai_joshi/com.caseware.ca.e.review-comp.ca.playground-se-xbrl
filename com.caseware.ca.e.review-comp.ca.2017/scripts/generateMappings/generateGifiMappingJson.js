/**********************************************************
 * This script generates the json object as outputGifi.json
 * from gifiMappingCsv.csv (taken from data conversion spreadsheet)
 * should be inserted into taxprepGifiMapping.js
 *
 * to run in console: node generateGifiMappingJson.js
 * ********************************************************/

var fs = require('fs');
var csv = require('fast-csv');

//define the csv path
var csvPath= "gifiMappingCsv.csv";

//take value from csv
var csvData = [];
fs.createReadStream(csvPath)
    .pipe(csv())
    .on('data', function(data) {
      csvData.push({
        gifiCode: data[0],
        taxprepCurrentYearID: data[1],
        taxprepCurrentYearDes: data[2],
        taxprepPriorYearID: data[3],
        taxprepPriorYearDes: data[4],
        taxprepAccountID: data[5],
        taxprepAccountDes: data[6],
        isS101: data[7],
        isCredit: data[8]
      })
    })
    .on('end', afterReadCsv);

//helper function to strip out [x] sequences in taxprep field identifiers
function stripTaxprepBrackets(field) {
  var index = field ? field.indexOf('[') : -1;
  if (index === -1)
    return field;

  while (index !== -1) {
    field = field.slice(0, index) + field.slice(index + 3, field.length);
    index = field.indexOf('[');
  }
  return field;
}

function isEmptyOrNullOrZero(value) {
  return value === undefined || value === null || (parseInt(value) == 0) || value.toString().trim() === '';
}

function afterReadCsv() {
  var output = {};

  csvData.forEach(function(lineObj) {
    lineObj.taxprepCurrentYearID = stripTaxprepBrackets(lineObj.taxprepCurrentYearID);
    lineObj.taxprepPriorYearID = stripTaxprepBrackets(lineObj.taxprepPriorYearID);

    var mapObjProto = {
      gifiCode: lineObj.gifiCode,
      isCredit: isEmptyOrNullOrZero(lineObj.isCredit) ? 0 : 1
    };
    if (!isEmptyOrNullOrZero(lineObj.isS101))
      mapObjProto.arrIndex = 1;

    //Add current year
    if (!isEmptyOrNullOrZero(lineObj.taxprepCurrentYearID) && !output.hasOwnProperty(lineObj.taxprepCurrentYearID)) {
      var mapObj1 = JSON.parse(JSON.stringify(mapObjProto));
      mapObj1.colIndex = 0;
      output[lineObj.taxprepCurrentYearID] = mapObj1;
    }

    //Add prior year
    if (!isEmptyOrNullOrZero(lineObj.taxprepPriorYearID) && !output.hasOwnProperty(lineObj.taxprepPriorYearID)) {
      var mapObj2 = JSON.parse(JSON.stringify(mapObjProto));
      mapObj2.colIndex = 1;
      output[lineObj.taxprepPriorYearID] = mapObj2;
    }
  });

  //export the json to file
  fs.writeFile('gifiMapping.json', JSON.stringify(output, null, 2), function () {
    console.log("The file was saved!");
  });

  var mappingMock = '(function() {\n'+
      '"use strict";\n'+
      'if (!wpw.tax.testdata)\n'+
      'wpw.tax.testdata = {};\n'+
      'wpw.tax.testdata.gifiMappingMock =\n' +
      JSON.stringify(output, null, 2)+';\n\n' +
      '}());';

  //export the json to mock mapping
  fs.writeFile('gifi-mapping-mock.js', mappingMock, function () {
    console.log("The mock mapping file was saved!");
  });
}






