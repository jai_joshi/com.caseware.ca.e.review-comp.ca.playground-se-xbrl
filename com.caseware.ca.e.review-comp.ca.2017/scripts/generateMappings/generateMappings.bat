echo & echo.Get the CSVs from the downloads folder if there exists mapping CSVs and move them into the folder where this script is
move /y %USERPROFILE%\Downloads\"Data conversion data - dataconversion.csv" %~dp0\"Data conversion data - dataconversion.csv"
move /y %USERPROFILE%\Downloads\"Data conversion data - GIFI mapping.csv" %~dp0\"Data conversion data - GIFI mapping.csv"

echo & echo.Rename downloaded CSVs to mappingCsv.csv and gifiMappingCsv.csv
move /y "Data conversion data - dataconversion.csv" mappingCsv.csv
move /y "Data conversion data - GIFI mapping.csv" gifiMappingCsv.csv

echo & echo.run the javascript files that generate the mappings from these csvs
node generateMappingJson.js
node generateGifiMappingJson.js

echo Copy mapping.json to sanitize folder
copy /y mapping.json ..\SanitizeCSV\mapping.json
copy /y gifiMapping.json ..\SanitizeCSV\gifiMapping.json
