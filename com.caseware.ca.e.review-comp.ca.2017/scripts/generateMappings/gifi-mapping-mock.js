(function() {
"use strict";
if (!wpw.tax.testdata)
wpw.tax.testdata = {};
wpw.tax.testdata.gifiMappingMock =
{
  "taxprepCurrentYearID": {
    "gifiCode": "gifiCode",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "taxprepPriorYearID": {
    "gifiCode": "gifiCode",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 1
  },
  "GFBGII.GFGII.Ttwgii1": {
    "gifiCode": "0001",
    "isCredit": 0,
    "colIndex": 0
  },
  "N/A": {
    "gifiCode": "0001",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGII.Ttwgii2": {
    "gifiCode": "0002",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba62": {
    "gifiCode": "1000",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba132": {
    "gifiCode": "1000",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba63": {
    "gifiCode": "1001",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba133": {
    "gifiCode": "1001",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba64": {
    "gifiCode": "1002",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba134": {
    "gifiCode": "1002",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba65": {
    "gifiCode": "1003",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba135": {
    "gifiCode": "1003",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba66": {
    "gifiCode": "1004",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba136": {
    "gifiCode": "1004",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba67": {
    "gifiCode": "1005",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba137": {
    "gifiCode": "1005",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba68": {
    "gifiCode": "1006",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba138": {
    "gifiCode": "1006",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba69": {
    "gifiCode": "1007",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba139": {
    "gifiCode": "1007",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba71": {
    "gifiCode": "1060",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba141": {
    "gifiCode": "1060",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba72": {
    "gifiCode": "1062",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba142": {
    "gifiCode": "1062",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba73": {
    "gifiCode": "1064",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba143": {
    "gifiCode": "1064",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba74": {
    "gifiCode": "1066",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba144": {
    "gifiCode": "1066",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba75": {
    "gifiCode": "1067",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba145": {
    "gifiCode": "1067",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba76": {
    "gifiCode": "1068",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba146": {
    "gifiCode": "1068",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba77": {
    "gifiCode": "1069",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba147": {
    "gifiCode": "1069",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba78": {
    "gifiCode": "1071",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba148": {
    "gifiCode": "1071",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba203": {
    "gifiCode": "1073",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba204": {
    "gifiCode": "1073",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba80": {
    "gifiCode": "1061",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBA.Ttwgba150": {
    "gifiCode": "1061",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBA.Ttwgba81": {
    "gifiCode": "1063",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBA.Ttwgba151": {
    "gifiCode": "1063",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBA.Ttwgba82": {
    "gifiCode": "1065",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBA.Ttwgba152": {
    "gifiCode": "1065",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBA.Ttwgba83": {
    "gifiCode": "1070",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBA.Ttwgba153": {
    "gifiCode": "1070",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBA.Ttwgba84": {
    "gifiCode": "1072",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBA.Ttwgba154": {
    "gifiCode": "1072",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBA.Ttwgba86": {
    "gifiCode": "1120",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba156": {
    "gifiCode": "1120",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba87": {
    "gifiCode": "1121",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba157": {
    "gifiCode": "1121",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba88": {
    "gifiCode": "1122",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba158": {
    "gifiCode": "1122",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba89": {
    "gifiCode": "1123",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba159": {
    "gifiCode": "1123",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba90": {
    "gifiCode": "1124",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba160": {
    "gifiCode": "1124",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba91": {
    "gifiCode": "1125",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba161": {
    "gifiCode": "1125",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba92": {
    "gifiCode": "1126",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba162": {
    "gifiCode": "1126",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba93": {
    "gifiCode": "1127",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba163": {
    "gifiCode": "1127",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba95": {
    "gifiCode": "1180",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba165": {
    "gifiCode": "1180",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba96": {
    "gifiCode": "1181",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba166": {
    "gifiCode": "1181",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba97": {
    "gifiCode": "1182",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba167": {
    "gifiCode": "1182",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba98": {
    "gifiCode": "1183",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba168": {
    "gifiCode": "1183",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba99": {
    "gifiCode": "1184",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba169": {
    "gifiCode": "1184",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba100": {
    "gifiCode": "1185",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba170": {
    "gifiCode": "1185",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba101": {
    "gifiCode": "1186",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba171": {
    "gifiCode": "1186",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba102": {
    "gifiCode": "1187",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba172": {
    "gifiCode": "1187",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba104": {
    "gifiCode": "1240",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba174": {
    "gifiCode": "1240",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba105": {
    "gifiCode": "1241",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba175": {
    "gifiCode": "1241",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba106": {
    "gifiCode": "1242",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba176": {
    "gifiCode": "1242",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba107": {
    "gifiCode": "1243",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba177": {
    "gifiCode": "1243",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba108": {
    "gifiCode": "1244",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba178": {
    "gifiCode": "1244",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba110": {
    "gifiCode": "1300",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba180": {
    "gifiCode": "1300",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba111": {
    "gifiCode": "1301",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba181": {
    "gifiCode": "1301",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba112": {
    "gifiCode": "1302",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba182": {
    "gifiCode": "1302",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba113": {
    "gifiCode": "1303",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba183": {
    "gifiCode": "1303",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba115": {
    "gifiCode": "1360",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba185": {
    "gifiCode": "1360",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba116": {
    "gifiCode": "1380",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba186": {
    "gifiCode": "1380",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba117": {
    "gifiCode": "1400",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba187": {
    "gifiCode": "1400",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba118": {
    "gifiCode": "1401",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba188": {
    "gifiCode": "1401",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba119": {
    "gifiCode": "1402",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba189": {
    "gifiCode": "1402",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba120": {
    "gifiCode": "1403",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba190": {
    "gifiCode": "1403",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba122": {
    "gifiCode": "1460",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba192": {
    "gifiCode": "1460",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba123": {
    "gifiCode": "1480",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba193": {
    "gifiCode": "1480",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba124": {
    "gifiCode": "1481",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba194": {
    "gifiCode": "1481",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba125": {
    "gifiCode": "1482",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba195": {
    "gifiCode": "1482",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba126": {
    "gifiCode": "1483",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba196": {
    "gifiCode": "1483",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba127": {
    "gifiCode": "1484",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba197": {
    "gifiCode": "1484",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba128": {
    "gifiCode": "1485",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba198": {
    "gifiCode": "1485",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba129": {
    "gifiCode": "1486",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba199": {
    "gifiCode": "1486",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBA.Ttwgba131": {
    "gifiCode": "1599",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBA.Ttwgba201": {
    "gifiCode": "1599",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb97": {
    "gifiCode": "1600",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb201": {
    "gifiCode": "1600",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb98": {
    "gifiCode": "1601",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb202": {
    "gifiCode": "1601",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb151": {
    "gifiCode": "1602",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb203": {
    "gifiCode": "1602",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb99": {
    "gifiCode": "1620",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb204": {
    "gifiCode": "1620",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb152": {
    "gifiCode": "1621",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb205": {
    "gifiCode": "1621",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb100": {
    "gifiCode": "1622",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb206": {
    "gifiCode": "1622",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb153": {
    "gifiCode": "1623",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb207": {
    "gifiCode": "1623",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb101": {
    "gifiCode": "1624",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb208": {
    "gifiCode": "1624",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb154": {
    "gifiCode": "1625",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb209": {
    "gifiCode": "1625",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb102": {
    "gifiCode": "1626",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb210": {
    "gifiCode": "1626",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb155": {
    "gifiCode": "1627",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb211": {
    "gifiCode": "1627",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb103": {
    "gifiCode": "1628",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb212": {
    "gifiCode": "1628",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb156": {
    "gifiCode": "1629",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb213": {
    "gifiCode": "1629",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb104": {
    "gifiCode": "1630",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb214": {
    "gifiCode": "1630",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb157": {
    "gifiCode": "1631",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb215": {
    "gifiCode": "1631",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb105": {
    "gifiCode": "1632",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb216": {
    "gifiCode": "1632",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb158": {
    "gifiCode": "1633",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb217": {
    "gifiCode": "1633",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb107": {
    "gifiCode": "1680",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb218": {
    "gifiCode": "1680",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb160": {
    "gifiCode": "1681",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb219": {
    "gifiCode": "1681",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb108": {
    "gifiCode": "1682",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb220": {
    "gifiCode": "1682",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb161": {
    "gifiCode": "1683",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb221": {
    "gifiCode": "1683",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb109": {
    "gifiCode": "1684",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb222": {
    "gifiCode": "1684",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb111": {
    "gifiCode": "1740",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb223": {
    "gifiCode": "1740",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb163": {
    "gifiCode": "1741",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb224": {
    "gifiCode": "1741",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb112": {
    "gifiCode": "1742",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb225": {
    "gifiCode": "1742",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb164": {
    "gifiCode": "1743",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb226": {
    "gifiCode": "1743",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb113": {
    "gifiCode": "1744",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb227": {
    "gifiCode": "1744",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb165": {
    "gifiCode": "1745",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb228": {
    "gifiCode": "1745",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb114": {
    "gifiCode": "1746",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb229": {
    "gifiCode": "1746",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb166": {
    "gifiCode": "1747",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb230": {
    "gifiCode": "1747",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb115": {
    "gifiCode": "1748",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb231": {
    "gifiCode": "1748",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb167": {
    "gifiCode": "1749",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb232": {
    "gifiCode": "1749",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb116": {
    "gifiCode": "1750",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb233": {
    "gifiCode": "1750",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb168": {
    "gifiCode": "1751",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb234": {
    "gifiCode": "1751",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb117": {
    "gifiCode": "1752",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb235": {
    "gifiCode": "1752",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb169": {
    "gifiCode": "1753",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb236": {
    "gifiCode": "1753",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb118": {
    "gifiCode": "1754",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb237": {
    "gifiCode": "1754",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb170": {
    "gifiCode": "1755",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb238": {
    "gifiCode": "1755",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb119": {
    "gifiCode": "1756",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb239": {
    "gifiCode": "1756",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb171": {
    "gifiCode": "1757",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb240": {
    "gifiCode": "1757",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb120": {
    "gifiCode": "1758",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb241": {
    "gifiCode": "1758",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb172": {
    "gifiCode": "1759",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb242": {
    "gifiCode": "1759",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb121": {
    "gifiCode": "1760",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb243": {
    "gifiCode": "1760",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb173": {
    "gifiCode": "1761",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb244": {
    "gifiCode": "1761",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb122": {
    "gifiCode": "1762",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb245": {
    "gifiCode": "1762",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb174": {
    "gifiCode": "1763",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb246": {
    "gifiCode": "1763",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb123": {
    "gifiCode": "1764",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb247": {
    "gifiCode": "1764",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb175": {
    "gifiCode": "1765",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb248": {
    "gifiCode": "1765",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb124": {
    "gifiCode": "1766",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb249": {
    "gifiCode": "1766",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb176": {
    "gifiCode": "1767",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb250": {
    "gifiCode": "1767",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb125": {
    "gifiCode": "1768",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb251": {
    "gifiCode": "1768",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb177": {
    "gifiCode": "1769",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb252": {
    "gifiCode": "1769",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb126": {
    "gifiCode": "1770",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb253": {
    "gifiCode": "1770",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb178": {
    "gifiCode": "1771",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb254": {
    "gifiCode": "1771",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb127": {
    "gifiCode": "1772",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb255": {
    "gifiCode": "1772",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb179": {
    "gifiCode": "1773",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb256": {
    "gifiCode": "1773",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb128": {
    "gifiCode": "1774",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb257": {
    "gifiCode": "1774",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb180": {
    "gifiCode": "1775",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb258": {
    "gifiCode": "1775",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb129": {
    "gifiCode": "1776",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb259": {
    "gifiCode": "1776",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb181": {
    "gifiCode": "1777",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb260": {
    "gifiCode": "1777",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb130": {
    "gifiCode": "1778",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb261": {
    "gifiCode": "1778",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb182": {
    "gifiCode": "1779",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb262": {
    "gifiCode": "1779",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb131": {
    "gifiCode": "1780",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb263": {
    "gifiCode": "1780",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb183": {
    "gifiCode": "1781",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb264": {
    "gifiCode": "1781",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb132": {
    "gifiCode": "1782",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb265": {
    "gifiCode": "1782",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb133": {
    "gifiCode": "1783",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb266": {
    "gifiCode": "1783",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb184": {
    "gifiCode": "1784",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb267": {
    "gifiCode": "1784",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb134": {
    "gifiCode": "1785",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb268": {
    "gifiCode": "1785",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb185": {
    "gifiCode": "1786",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb269": {
    "gifiCode": "1786",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb135": {
    "gifiCode": "1787",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb270": {
    "gifiCode": "1787",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb186": {
    "gifiCode": "1788",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb271": {
    "gifiCode": "1788",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb137": {
    "gifiCode": "1900",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb272": {
    "gifiCode": "1900",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb188": {
    "gifiCode": "1901",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb273": {
    "gifiCode": "1901",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb138": {
    "gifiCode": "1902",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb274": {
    "gifiCode": "1902",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb189": {
    "gifiCode": "1903",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb275": {
    "gifiCode": "1903",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb139": {
    "gifiCode": "1904",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb276": {
    "gifiCode": "1904",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb190": {
    "gifiCode": "1905",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb277": {
    "gifiCode": "1905",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb140": {
    "gifiCode": "1906",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb278": {
    "gifiCode": "1906",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb191": {
    "gifiCode": "1907",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb279": {
    "gifiCode": "1907",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb141": {
    "gifiCode": "1908",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb280": {
    "gifiCode": "1908",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb192": {
    "gifiCode": "1909",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb281": {
    "gifiCode": "1909",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb142": {
    "gifiCode": "1910",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb282": {
    "gifiCode": "1910",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb193": {
    "gifiCode": "1911",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb283": {
    "gifiCode": "1911",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb143": {
    "gifiCode": "1912",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb284": {
    "gifiCode": "1912",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb194": {
    "gifiCode": "1913",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb285": {
    "gifiCode": "1913",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb144": {
    "gifiCode": "1914",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb286": {
    "gifiCode": "1914",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb195": {
    "gifiCode": "1915",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb287": {
    "gifiCode": "1915",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb145": {
    "gifiCode": "1916",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb288": {
    "gifiCode": "1916",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb196": {
    "gifiCode": "1917",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb289": {
    "gifiCode": "1917",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb146": {
    "gifiCode": "1918",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb290": {
    "gifiCode": "1918",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb197": {
    "gifiCode": "1919",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb291": {
    "gifiCode": "1919",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb147": {
    "gifiCode": "1920",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb292": {
    "gifiCode": "1920",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb148": {
    "gifiCode": "1921",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb293": {
    "gifiCode": "1921",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb198": {
    "gifiCode": "1922",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb294": {
    "gifiCode": "1922",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb150": {
    "gifiCode": "2008",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb295": {
    "gifiCode": "2008",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBB.Ttwgbb200": {
    "gifiCode": "2009",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBB.Ttwgbb296": {
    "gifiCode": "2009",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd29": {
    "gifiCode": "2010",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd61": {
    "gifiCode": "2010",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd45": {
    "gifiCode": "2011",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd62": {
    "gifiCode": "2011",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd30": {
    "gifiCode": "2012",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd63": {
    "gifiCode": "2012",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd46": {
    "gifiCode": "2013",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd64": {
    "gifiCode": "2013",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd31": {
    "gifiCode": "2014",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd65": {
    "gifiCode": "2014",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd47": {
    "gifiCode": "2015",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd66": {
    "gifiCode": "2015",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd32": {
    "gifiCode": "2016",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd67": {
    "gifiCode": "2016",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd48": {
    "gifiCode": "2017",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd68": {
    "gifiCode": "2017",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd33": {
    "gifiCode": "2018",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd69": {
    "gifiCode": "2018",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd49": {
    "gifiCode": "2019",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd70": {
    "gifiCode": "2019",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd34": {
    "gifiCode": "2020",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd71": {
    "gifiCode": "2020",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd50": {
    "gifiCode": "2021",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd72": {
    "gifiCode": "2021",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd35": {
    "gifiCode": "2022",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd73": {
    "gifiCode": "2022",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd51": {
    "gifiCode": "2023",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd74": {
    "gifiCode": "2023",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd36": {
    "gifiCode": "2024",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd75": {
    "gifiCode": "2024",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd52": {
    "gifiCode": "2025",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd76": {
    "gifiCode": "2025",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd37": {
    "gifiCode": "2026",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd77": {
    "gifiCode": "2026",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd53": {
    "gifiCode": "2027",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd78": {
    "gifiCode": "2027",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd39": {
    "gifiCode": "2070",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd79": {
    "gifiCode": "2070",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd55": {
    "gifiCode": "2071",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd80": {
    "gifiCode": "2071",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd40": {
    "gifiCode": "2072",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd81": {
    "gifiCode": "2072",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd56": {
    "gifiCode": "2073",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd82": {
    "gifiCode": "2073",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd41": {
    "gifiCode": "2074",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd83": {
    "gifiCode": "2074",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd57": {
    "gifiCode": "2075",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd84": {
    "gifiCode": "2075",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd42": {
    "gifiCode": "2076",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd85": {
    "gifiCode": "2076",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd58": {
    "gifiCode": "2077",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd86": {
    "gifiCode": "2077",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd44": {
    "gifiCode": "2178",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd87": {
    "gifiCode": "2178",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBD.Ttwgbd60": {
    "gifiCode": "2179",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBD.Ttwgbd88": {
    "gifiCode": "2179",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf46": {
    "gifiCode": "2180",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf96": {
    "gifiCode": "2180",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf47": {
    "gifiCode": "2181",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf97": {
    "gifiCode": "2181",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf48": {
    "gifiCode": "2182",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf98": {
    "gifiCode": "2182",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf49": {
    "gifiCode": "2183",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf99": {
    "gifiCode": "2183",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf51": {
    "gifiCode": "2190",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf101": {
    "gifiCode": "2190",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf52": {
    "gifiCode": "2200",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf102": {
    "gifiCode": "2200",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf53": {
    "gifiCode": "2220",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf103": {
    "gifiCode": "2220",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf54": {
    "gifiCode": "2240",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf104": {
    "gifiCode": "2240",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf55": {
    "gifiCode": "2241",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf105": {
    "gifiCode": "2241",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf56": {
    "gifiCode": "2242",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf106": {
    "gifiCode": "2242",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf57": {
    "gifiCode": "2243",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf107": {
    "gifiCode": "2243",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf58": {
    "gifiCode": "2244",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf108": {
    "gifiCode": "2244",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf59": {
    "gifiCode": "2245",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf109": {
    "gifiCode": "2245",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf60": {
    "gifiCode": "2246",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf110": {
    "gifiCode": "2246",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf61": {
    "gifiCode": "2247",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf111": {
    "gifiCode": "2247",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf62": {
    "gifiCode": "2248",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf112": {
    "gifiCode": "2248",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf63": {
    "gifiCode": "2249",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf113": {
    "gifiCode": "2249",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf64": {
    "gifiCode": "2250",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf114": {
    "gifiCode": "2250",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf66": {
    "gifiCode": "2280",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf116": {
    "gifiCode": "2280",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf67": {
    "gifiCode": "2300",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf117": {
    "gifiCode": "2300",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf68": {
    "gifiCode": "2301",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf118": {
    "gifiCode": "2301",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf69": {
    "gifiCode": "2302",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf119": {
    "gifiCode": "2302",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf70": {
    "gifiCode": "2303",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf120": {
    "gifiCode": "2303",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf71": {
    "gifiCode": "2304",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf121": {
    "gifiCode": "2304",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf72": {
    "gifiCode": "2305",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf122": {
    "gifiCode": "2305",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf73": {
    "gifiCode": "2306",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf123": {
    "gifiCode": "2306",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf74": {
    "gifiCode": "2307",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf124": {
    "gifiCode": "2307",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf75": {
    "gifiCode": "2308",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf125": {
    "gifiCode": "2308",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf76": {
    "gifiCode": "2309",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf126": {
    "gifiCode": "2309",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf77": {
    "gifiCode": "2310",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf127": {
    "gifiCode": "2310",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf78": {
    "gifiCode": "2311",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf128": {
    "gifiCode": "2311",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf80": {
    "gifiCode": "2360",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf130": {
    "gifiCode": "2360",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf81": {
    "gifiCode": "2361",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf131": {
    "gifiCode": "2361",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf82": {
    "gifiCode": "2362",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf132": {
    "gifiCode": "2362",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf83": {
    "gifiCode": "2363",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf133": {
    "gifiCode": "2363",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf84": {
    "gifiCode": "2364",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf134": {
    "gifiCode": "2364",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf86": {
    "gifiCode": "2420",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf136": {
    "gifiCode": "2420",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf87": {
    "gifiCode": "2421",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf137": {
    "gifiCode": "2421",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf88": {
    "gifiCode": "2422",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf138": {
    "gifiCode": "2422",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf89": {
    "gifiCode": "2423",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf139": {
    "gifiCode": "2423",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf90": {
    "gifiCode": "2424",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf140": {
    "gifiCode": "2424",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf91": {
    "gifiCode": "2426",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf141": {
    "gifiCode": "2426",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf92": {
    "gifiCode": "2427",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf142": {
    "gifiCode": "2427",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf94": {
    "gifiCode": "2425",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf144": {
    "gifiCode": "2425",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBF.Ttwgbf95": {
    "gifiCode": "2589",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBF.Ttwgbf145": {
    "gifiCode": "2589",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGIB.Ttwgib30": {
    "gifiCode": "2590",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGIB.Ttwgib46": {
    "gifiCode": "2590",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg40": {
    "gifiCode": "2600",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg84": {
    "gifiCode": "2600",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg41": {
    "gifiCode": "2620",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg85": {
    "gifiCode": "2620",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg42": {
    "gifiCode": "2621",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg86": {
    "gifiCode": "2621",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg43": {
    "gifiCode": "2622",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg87": {
    "gifiCode": "2622",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg44": {
    "gifiCode": "2623",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg88": {
    "gifiCode": "2623",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg45": {
    "gifiCode": "2624",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg89": {
    "gifiCode": "2624",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg46": {
    "gifiCode": "2625",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg90": {
    "gifiCode": "2625",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg47": {
    "gifiCode": "2626",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg91": {
    "gifiCode": "2626",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg48": {
    "gifiCode": "2627",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg92": {
    "gifiCode": "2627",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg49": {
    "gifiCode": "2628",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg93": {
    "gifiCode": "2628",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg50": {
    "gifiCode": "2629",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg94": {
    "gifiCode": "2629",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg129": {
    "gifiCode": "2630",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg130": {
    "gifiCode": "2630",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg52": {
    "gifiCode": "2680",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg96": {
    "gifiCode": "2680",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg53": {
    "gifiCode": "2700",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg97": {
    "gifiCode": "2700",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg54": {
    "gifiCode": "2701",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg98": {
    "gifiCode": "2701",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg55": {
    "gifiCode": "2702",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg99": {
    "gifiCode": "2702",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg56": {
    "gifiCode": "2703",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg100": {
    "gifiCode": "2703",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg57": {
    "gifiCode": "2704",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg101": {
    "gifiCode": "2704",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg58": {
    "gifiCode": "2705",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg102": {
    "gifiCode": "2705",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg59": {
    "gifiCode": "2706",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg103": {
    "gifiCode": "2706",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg132": {
    "gifiCode": "2707",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg133": {
    "gifiCode": "2707",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg61": {
    "gifiCode": "2770",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg105": {
    "gifiCode": "2770",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg62": {
    "gifiCode": "2780",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg106": {
    "gifiCode": "2780",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg63": {
    "gifiCode": "2781",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg107": {
    "gifiCode": "2781",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg64": {
    "gifiCode": "2782",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg108": {
    "gifiCode": "2782",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg65": {
    "gifiCode": "2783",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg109": {
    "gifiCode": "2783",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg67": {
    "gifiCode": "2840",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg111": {
    "gifiCode": "2840",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg68": {
    "gifiCode": "2860",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg112": {
    "gifiCode": "2860",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg69": {
    "gifiCode": "2861",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg113": {
    "gifiCode": "2861",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg70": {
    "gifiCode": "2862",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg114": {
    "gifiCode": "2862",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg71": {
    "gifiCode": "2863",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg115": {
    "gifiCode": "2863",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg73": {
    "gifiCode": "2920",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg117": {
    "gifiCode": "2920",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg74": {
    "gifiCode": "2940",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg118": {
    "gifiCode": "2940",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg75": {
    "gifiCode": "2960",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg119": {
    "gifiCode": "2960",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg76": {
    "gifiCode": "2961",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg120": {
    "gifiCode": "2961",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg77": {
    "gifiCode": "2962",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg121": {
    "gifiCode": "2962",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg78": {
    "gifiCode": "2963",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg122": {
    "gifiCode": "2963",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg79": {
    "gifiCode": "2964",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg123": {
    "gifiCode": "2964",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg80": {
    "gifiCode": "2965",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg124": {
    "gifiCode": "2965",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg81": {
    "gifiCode": "2966",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg125": {
    "gifiCode": "2966",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBG.Ttwgbg83": {
    "gifiCode": "3139",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBG.Ttwgbg127": {
    "gifiCode": "3139",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh36": {
    "gifiCode": "3140",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh75": {
    "gifiCode": "3140",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh37": {
    "gifiCode": "3141",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh76": {
    "gifiCode": "3141",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh38": {
    "gifiCode": "3142",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh77": {
    "gifiCode": "3142",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh39": {
    "gifiCode": "3143",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh78": {
    "gifiCode": "3143",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh40": {
    "gifiCode": "3144",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh79": {
    "gifiCode": "3144",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh41": {
    "gifiCode": "3145",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh80": {
    "gifiCode": "3145",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh42": {
    "gifiCode": "3146",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh81": {
    "gifiCode": "3146",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh43": {
    "gifiCode": "3147",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh82": {
    "gifiCode": "3147",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh44": {
    "gifiCode": "3148",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh83": {
    "gifiCode": "3148",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh45": {
    "gifiCode": "3149",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh84": {
    "gifiCode": "3149",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh46": {
    "gifiCode": "3150",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh85": {
    "gifiCode": "3150",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh47": {
    "gifiCode": "3151",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh86": {
    "gifiCode": "3151",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh48": {
    "gifiCode": "3152",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh87": {
    "gifiCode": "3152",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh50": {
    "gifiCode": "3200",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh89": {
    "gifiCode": "3200",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh51": {
    "gifiCode": "3210",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh90": {
    "gifiCode": "3210",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh52": {
    "gifiCode": "3220",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh91": {
    "gifiCode": "3220",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh53": {
    "gifiCode": "3240",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh92": {
    "gifiCode": "3240",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh54": {
    "gifiCode": "3260",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh93": {
    "gifiCode": "3260",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh55": {
    "gifiCode": "3261",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh94": {
    "gifiCode": "3261",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh56": {
    "gifiCode": "3262",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh95": {
    "gifiCode": "3262",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh57": {
    "gifiCode": "3263",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh96": {
    "gifiCode": "3263",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh59": {
    "gifiCode": "3270",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh98": {
    "gifiCode": "3270",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh60": {
    "gifiCode": "3280",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh99": {
    "gifiCode": "3280",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh61": {
    "gifiCode": "3300",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh100": {
    "gifiCode": "3300",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh62": {
    "gifiCode": "3301",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh101": {
    "gifiCode": "3301",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh63": {
    "gifiCode": "3302",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh102": {
    "gifiCode": "3302",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh65": {
    "gifiCode": "3320",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh104": {
    "gifiCode": "3320",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh66": {
    "gifiCode": "3321",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh105": {
    "gifiCode": "3321",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh67": {
    "gifiCode": "3322",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh106": {
    "gifiCode": "3322",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh68": {
    "gifiCode": "3323",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh107": {
    "gifiCode": "3323",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh69": {
    "gifiCode": "3324",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh108": {
    "gifiCode": "3324",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh70": {
    "gifiCode": "3325",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh109": {
    "gifiCode": "3325",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh71": {
    "gifiCode": "3326",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh110": {
    "gifiCode": "3326",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh72": {
    "gifiCode": "3327",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh111": {
    "gifiCode": "3327",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh115": {
    "gifiCode": "3328",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh116": {
    "gifiCode": "3328",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBH.Ttwgbh74": {
    "gifiCode": "3450",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBH.Ttwgbh113": {
    "gifiCode": "3450",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGIB.Ttwgib34": {
    "gifiCode": "3460",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGIB.Ttwgib50": {
    "gifiCode": "3460",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGIB.Ttwgib35": {
    "gifiCode": "3470",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGIB.Ttwgib51": {
    "gifiCode": "3470",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBI.Ttwgbi10": {
    "gifiCode": "3500",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBI.Ttwgbi20": {
    "gifiCode": "3500",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBI.Ttwgbi11": {
    "gifiCode": "3520",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBI.Ttwgbi21": {
    "gifiCode": "3520",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBI.Ttwgbi12": {
    "gifiCode": "3540",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBI.Ttwgbi22": {
    "gifiCode": "3540",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBI.Ttwgbi13": {
    "gifiCode": "3541",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBI.Ttwgbi23": {
    "gifiCode": "3541",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBI.Ttwgbi14": {
    "gifiCode": "3542",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBI.Ttwgbi24": {
    "gifiCode": "3542",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBI.Ttwgbi15": {
    "gifiCode": "3543",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBI.Ttwgbi25": {
    "gifiCode": "3543",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBI.Ttwgbi17": {
    "gifiCode": "3570",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBI.Ttwgbi27": {
    "gifiCode": "3570",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBI.Ttwgbi32": {
    "gifiCode": "3580",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBI.Ttwgbi33": {
    "gifiCode": "3580",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBI.Ttwgbi18": {
    "gifiCode": "3600",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBI.Ttwgbi28": {
    "gifiCode": "3600",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBI.Ttwgbi19": {
    "gifiCode": "3620",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBI.Ttwgbi29": {
    "gifiCode": "3620",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBJ.Ttwgbj13": {
    "gifiCode": "3660",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBJ.Ttwgbj27": {
    "gifiCode": "3660",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBJ.Ttwgbj14": {
    "gifiCode": "3680",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBJ.Ttwgbj28": {
    "gifiCode": "3680",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBJ.Ttwgbj15": {
    "gifiCode": "3700",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBJ.Ttwgbj29": {
    "gifiCode": "3700",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBJ.Ttwgbj16": {
    "gifiCode": "3701",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBJ.Ttwgbj30": {
    "gifiCode": "3701",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBJ.Ttwgbj17": {
    "gifiCode": "3702",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFGBJ.Ttwgbj31": {
    "gifiCode": "3702",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFGBJ.Ttwgbj19": {
    "gifiCode": "3720",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBJ.Ttwgbj33": {
    "gifiCode": "3720",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBJ.Ttwgbj20": {
    "gifiCode": "3740",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBJ.Ttwgbj34": {
    "gifiCode": "3740",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBJ.Ttwgbj21": {
    "gifiCode": "3741",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBJ.Ttwgbj35": {
    "gifiCode": "3741",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBJ.Ttwgbj22": {
    "gifiCode": "3742",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBJ.Ttwgbj36": {
    "gifiCode": "3742",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBJ.Ttwgbj23": {
    "gifiCode": "3743",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBJ.Ttwgbj37": {
    "gifiCode": "3743",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBJ.Ttwgbj24": {
    "gifiCode": "3744",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBJ.Ttwgbj38": {
    "gifiCode": "3744",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBJ.Ttwgbj42": {
    "gifiCode": "3745",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBJ.Ttwgbj43": {
    "gifiCode": "3745",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGBJ.Ttwgbj26": {
    "gifiCode": "3849",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGBJ.Ttwgbj40": {
    "gifiCode": "3849",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij68": {
    "gifiCode": "8000",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij143": {
    "gifiCode": "8000",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij69": {
    "gifiCode": "8020",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij144": {
    "gifiCode": "8020",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij219": {
    "gifiCode": "8030",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij220": {
    "gifiCode": "8030",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij70": {
    "gifiCode": "8040",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij145": {
    "gifiCode": "8040",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij71": {
    "gifiCode": "8041",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij146": {
    "gifiCode": "8041",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij72": {
    "gifiCode": "8042",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij147": {
    "gifiCode": "8042",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij73": {
    "gifiCode": "8043",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij148": {
    "gifiCode": "8043",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij74": {
    "gifiCode": "8044",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij149": {
    "gifiCode": "8044",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij75": {
    "gifiCode": "8045",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij150": {
    "gifiCode": "8045",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij76": {
    "gifiCode": "8046",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij151": {
    "gifiCode": "8046",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij77": {
    "gifiCode": "8047",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij152": {
    "gifiCode": "8047",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij78": {
    "gifiCode": "8048",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij153": {
    "gifiCode": "8048",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij79": {
    "gifiCode": "8049",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij154": {
    "gifiCode": "8049",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij80": {
    "gifiCode": "8050",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij155": {
    "gifiCode": "8050",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij81": {
    "gifiCode": "8051",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij156": {
    "gifiCode": "8051",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij82": {
    "gifiCode": "8052",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij157": {
    "gifiCode": "8052",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij83": {
    "gifiCode": "8053",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij158": {
    "gifiCode": "8053",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij85": {
    "gifiCode": "8089",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij160": {
    "gifiCode": "8089",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij86": {
    "gifiCode": "8090",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij161": {
    "gifiCode": "8090",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij87": {
    "gifiCode": "8091",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij162": {
    "gifiCode": "8091",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij88": {
    "gifiCode": "8092",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij163": {
    "gifiCode": "8092",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij89": {
    "gifiCode": "8093",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij164": {
    "gifiCode": "8093",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij90": {
    "gifiCode": "8094",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij165": {
    "gifiCode": "8094",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij91": {
    "gifiCode": "8095",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij166": {
    "gifiCode": "8095",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij92": {
    "gifiCode": "8096",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij167": {
    "gifiCode": "8096",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij93": {
    "gifiCode": "8097",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij168": {
    "gifiCode": "8097",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij95": {
    "gifiCode": "8100",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij170": {
    "gifiCode": "8100",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij96": {
    "gifiCode": "8101",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij171": {
    "gifiCode": "8101",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij97": {
    "gifiCode": "8102",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij172": {
    "gifiCode": "8102",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij98": {
    "gifiCode": "8103",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij173": {
    "gifiCode": "8103",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij100": {
    "gifiCode": "8120",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij175": {
    "gifiCode": "8120",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij101": {
    "gifiCode": "8121",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij176": {
    "gifiCode": "8121",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij103": {
    "gifiCode": "8140",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij178": {
    "gifiCode": "8140",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij104": {
    "gifiCode": "8141",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij179": {
    "gifiCode": "8141",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij105": {
    "gifiCode": "8142",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij180": {
    "gifiCode": "8142",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij107": {
    "gifiCode": "8150",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij182": {
    "gifiCode": "8150",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij108": {
    "gifiCode": "8160",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij183": {
    "gifiCode": "8160",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij109": {
    "gifiCode": "8161",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij184": {
    "gifiCode": "8161",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij110": {
    "gifiCode": "8162",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij185": {
    "gifiCode": "8162",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij111": {
    "gifiCode": "8163",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij186": {
    "gifiCode": "8163",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij112": {
    "gifiCode": "8164",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij187": {
    "gifiCode": "8164",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij113": {
    "gifiCode": "8165",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij188": {
    "gifiCode": "8165",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij114": {
    "gifiCode": "8166",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij189": {
    "gifiCode": "8166",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij116": {
    "gifiCode": "8210",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij191": {
    "gifiCode": "8210",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij117": {
    "gifiCode": "8211",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij192": {
    "gifiCode": "8211",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij118": {
    "gifiCode": "8212",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij193": {
    "gifiCode": "8212",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij222": {
    "gifiCode": "8220",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij223": {
    "gifiCode": "8220",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij225": {
    "gifiCode": "8221",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij226": {
    "gifiCode": "8221",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij228": {
    "gifiCode": "8222",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij229": {
    "gifiCode": "8222",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij231": {
    "gifiCode": "8223",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij232": {
    "gifiCode": "8223",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij234": {
    "gifiCode": "8224",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij235": {
    "gifiCode": "8224",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij120": {
    "gifiCode": "8230",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij195": {
    "gifiCode": "8230",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij121": {
    "gifiCode": "8231",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij196": {
    "gifiCode": "8231",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij122": {
    "gifiCode": "8232",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij197": {
    "gifiCode": "8232",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij123": {
    "gifiCode": "8233",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij198": {
    "gifiCode": "8233",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij124": {
    "gifiCode": "8234",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij199": {
    "gifiCode": "8234",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij125": {
    "gifiCode": "8235",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij200": {
    "gifiCode": "8235",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij126": {
    "gifiCode": "8236",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij201": {
    "gifiCode": "8236",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij127": {
    "gifiCode": "8237",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij202": {
    "gifiCode": "8237",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij128": {
    "gifiCode": "8238",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij203": {
    "gifiCode": "8238",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij129": {
    "gifiCode": "8239",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij204": {
    "gifiCode": "8239",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij130": {
    "gifiCode": "8240",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij205": {
    "gifiCode": "8240",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij131": {
    "gifiCode": "8241",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij206": {
    "gifiCode": "8241",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij132": {
    "gifiCode": "8242",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij207": {
    "gifiCode": "8242",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij133": {
    "gifiCode": "8243",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij208": {
    "gifiCode": "8243",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij134": {
    "gifiCode": "8244",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij209": {
    "gifiCode": "8244",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij135": {
    "gifiCode": "8245",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij210": {
    "gifiCode": "8245",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij136": {
    "gifiCode": "8246",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij211": {
    "gifiCode": "8246",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij137": {
    "gifiCode": "8247",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij212": {
    "gifiCode": "8247",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij138": {
    "gifiCode": "8248",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij213": {
    "gifiCode": "8248",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij139": {
    "gifiCode": "8249",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij214": {
    "gifiCode": "8249",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij140": {
    "gifiCode": "8250",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij215": {
    "gifiCode": "8250",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIJ.Ttwgij142": {
    "gifiCode": "8299",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIJ.Ttwgij217": {
    "gifiCode": "8299",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik47": {
    "gifiCode": "8300",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik98": {
    "gifiCode": "8300",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik48": {
    "gifiCode": "8301",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik99": {
    "gifiCode": "8301",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik49": {
    "gifiCode": "8302",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik100": {
    "gifiCode": "8302",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik50": {
    "gifiCode": "8303",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik101": {
    "gifiCode": "8303",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik52": {
    "gifiCode": "8320",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik103": {
    "gifiCode": "8320",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik53": {
    "gifiCode": "8340",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik104": {
    "gifiCode": "8340",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik54": {
    "gifiCode": "8350",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik105": {
    "gifiCode": "8350",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik55": {
    "gifiCode": "8360",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik106": {
    "gifiCode": "8360",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik56": {
    "gifiCode": "8370",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik107": {
    "gifiCode": "8370",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik57": {
    "gifiCode": "8400",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik108": {
    "gifiCode": "8400",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik58": {
    "gifiCode": "8401",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik109": {
    "gifiCode": "8401",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik59": {
    "gifiCode": "8402",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik110": {
    "gifiCode": "8402",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik60": {
    "gifiCode": "8403",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik111": {
    "gifiCode": "8403",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik61": {
    "gifiCode": "8404",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik112": {
    "gifiCode": "8404",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik62": {
    "gifiCode": "8405",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik113": {
    "gifiCode": "8405",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik63": {
    "gifiCode": "8406",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik114": {
    "gifiCode": "8406",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik64": {
    "gifiCode": "8407",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik115": {
    "gifiCode": "8407",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik65": {
    "gifiCode": "8408",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik116": {
    "gifiCode": "8408",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik66": {
    "gifiCode": "8409",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik117": {
    "gifiCode": "8409",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik67": {
    "gifiCode": "8410",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik118": {
    "gifiCode": "8410",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik68": {
    "gifiCode": "8411",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik119": {
    "gifiCode": "8411",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik69": {
    "gifiCode": "8412",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik120": {
    "gifiCode": "8412",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik71": {
    "gifiCode": "8435",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik122": {
    "gifiCode": "8435",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik72": {
    "gifiCode": "8436",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik123": {
    "gifiCode": "8436",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik73": {
    "gifiCode": "8437",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik124": {
    "gifiCode": "8437",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik74": {
    "gifiCode": "8438",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik125": {
    "gifiCode": "8438",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik75": {
    "gifiCode": "8439",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik126": {
    "gifiCode": "8439",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik76": {
    "gifiCode": "8440",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik127": {
    "gifiCode": "8440",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik77": {
    "gifiCode": "8441",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik128": {
    "gifiCode": "8441",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik79": {
    "gifiCode": "8450",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik130": {
    "gifiCode": "8450",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik80": {
    "gifiCode": "8451",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik131": {
    "gifiCode": "8451",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik81": {
    "gifiCode": "8452",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik132": {
    "gifiCode": "8452",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik82": {
    "gifiCode": "8453",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik133": {
    "gifiCode": "8453",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik83": {
    "gifiCode": "8454",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik134": {
    "gifiCode": "8454",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik84": {
    "gifiCode": "8455",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik135": {
    "gifiCode": "8455",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik85": {
    "gifiCode": "8456",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik136": {
    "gifiCode": "8456",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik86": {
    "gifiCode": "8457",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik137": {
    "gifiCode": "8457",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik87": {
    "gifiCode": "8458",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik138": {
    "gifiCode": "8458",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik88": {
    "gifiCode": "8459",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik139": {
    "gifiCode": "8459",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik89": {
    "gifiCode": "8460",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik140": {
    "gifiCode": "8460",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik90": {
    "gifiCode": "8461",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik141": {
    "gifiCode": "8461",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik92": {
    "gifiCode": "8500",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik143": {
    "gifiCode": "8500",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik93": {
    "gifiCode": "8501",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik144": {
    "gifiCode": "8501",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik94": {
    "gifiCode": "8502",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik145": {
    "gifiCode": "8502",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik95": {
    "gifiCode": "8503",
    "isCredit": 1,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik146": {
    "gifiCode": "8503",
    "isCredit": 1,
    "colIndex": 1
  },
  "GFBGII.GFGIK.Ttwgik97": {
    "gifiCode": "8518",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIK.Ttwgik148": {
    "gifiCode": "8518",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil120": {
    "gifiCode": "8520",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil256": {
    "gifiCode": "8520",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil121": {
    "gifiCode": "8521",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil257": {
    "gifiCode": "8521",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil122": {
    "gifiCode": "8522",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil258": {
    "gifiCode": "8522",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil123": {
    "gifiCode": "8523",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil259": {
    "gifiCode": "8523",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil124": {
    "gifiCode": "8524",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil260": {
    "gifiCode": "8524",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil126": {
    "gifiCode": "8570",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil262": {
    "gifiCode": "8570",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil408": {
    "gifiCode": "8571",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil409": {
    "gifiCode": "8571",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil127": {
    "gifiCode": "8590",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil263": {
    "gifiCode": "8590",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil128": {
    "gifiCode": "8610",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil264": {
    "gifiCode": "8610",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil129": {
    "gifiCode": "8611",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil265": {
    "gifiCode": "8611",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil131": {
    "gifiCode": "8620",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil267": {
    "gifiCode": "8620",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil132": {
    "gifiCode": "8621",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil268": {
    "gifiCode": "8621",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil133": {
    "gifiCode": "8622",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil269": {
    "gifiCode": "8622",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil134": {
    "gifiCode": "8623",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil270": {
    "gifiCode": "8623",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil136": {
    "gifiCode": "8650",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil272": {
    "gifiCode": "8650",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil137": {
    "gifiCode": "8670",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil273": {
    "gifiCode": "8670",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil138": {
    "gifiCode": "8690",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil274": {
    "gifiCode": "8690",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil139": {
    "gifiCode": "8691",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil275": {
    "gifiCode": "8691",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil141": {
    "gifiCode": "8710",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil277": {
    "gifiCode": "8710",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil142": {
    "gifiCode": "8711",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil278": {
    "gifiCode": "8711",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil143": {
    "gifiCode": "8712",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil279": {
    "gifiCode": "8712",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil144": {
    "gifiCode": "8713",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil280": {
    "gifiCode": "8713",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil145": {
    "gifiCode": "8714",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil281": {
    "gifiCode": "8714",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil146": {
    "gifiCode": "8715",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil282": {
    "gifiCode": "8715",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil147": {
    "gifiCode": "8716",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil283": {
    "gifiCode": "8716",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil148": {
    "gifiCode": "8717",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil284": {
    "gifiCode": "8717",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil150": {
    "gifiCode": "8740",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil286": {
    "gifiCode": "8740",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil151": {
    "gifiCode": "8741",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil287": {
    "gifiCode": "8741",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil152": {
    "gifiCode": "8742",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil288": {
    "gifiCode": "8742",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil154": {
    "gifiCode": "8760",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil290": {
    "gifiCode": "8760",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil155": {
    "gifiCode": "8761",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil291": {
    "gifiCode": "8761",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil156": {
    "gifiCode": "8762",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil292": {
    "gifiCode": "8762",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil157": {
    "gifiCode": "8763",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil293": {
    "gifiCode": "8763",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil158": {
    "gifiCode": "8764",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil294": {
    "gifiCode": "8764",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil161": {
    "gifiCode": "8790",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil297": {
    "gifiCode": "8790",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil162": {
    "gifiCode": "8810",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil298": {
    "gifiCode": "8810",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil163": {
    "gifiCode": "8811",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil299": {
    "gifiCode": "8811",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil164": {
    "gifiCode": "8812",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil300": {
    "gifiCode": "8812",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil165": {
    "gifiCode": "8813",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil301": {
    "gifiCode": "8813",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil167": {
    "gifiCode": "8860",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil303": {
    "gifiCode": "8860",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil168": {
    "gifiCode": "8861",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil304": {
    "gifiCode": "8861",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil169": {
    "gifiCode": "8862",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil305": {
    "gifiCode": "8862",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil170": {
    "gifiCode": "8863",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil306": {
    "gifiCode": "8863",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil171": {
    "gifiCode": "8864",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil307": {
    "gifiCode": "8864",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil172": {
    "gifiCode": "8865",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil308": {
    "gifiCode": "8865",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil173": {
    "gifiCode": "8866",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil309": {
    "gifiCode": "8866",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil174": {
    "gifiCode": "8867",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil310": {
    "gifiCode": "8867",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil175": {
    "gifiCode": "8868",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil311": {
    "gifiCode": "8868",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil176": {
    "gifiCode": "8869",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil312": {
    "gifiCode": "8869",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil177": {
    "gifiCode": "8870",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil313": {
    "gifiCode": "8870",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil178": {
    "gifiCode": "8871",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil314": {
    "gifiCode": "8871",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil179": {
    "gifiCode": "8872",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil315": {
    "gifiCode": "8872",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil180": {
    "gifiCode": "8873",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil316": {
    "gifiCode": "8873",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil181": {
    "gifiCode": "8874",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil317": {
    "gifiCode": "8874",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil182": {
    "gifiCode": "8875",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil318": {
    "gifiCode": "8875",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil183": {
    "gifiCode": "8876",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil319": {
    "gifiCode": "8876",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil184": {
    "gifiCode": "8877",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil320": {
    "gifiCode": "8877",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil186": {
    "gifiCode": "8910",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil322": {
    "gifiCode": "8910",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil187": {
    "gifiCode": "8911",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil323": {
    "gifiCode": "8911",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil188": {
    "gifiCode": "8912",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil324": {
    "gifiCode": "8912",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil189": {
    "gifiCode": "8913",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil325": {
    "gifiCode": "8913",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil190": {
    "gifiCode": "8914",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil326": {
    "gifiCode": "8914",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil191": {
    "gifiCode": "8915",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil327": {
    "gifiCode": "8915",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil192": {
    "gifiCode": "8916",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil328": {
    "gifiCode": "8916",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil193": {
    "gifiCode": "8917",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil329": {
    "gifiCode": "8917",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil194": {
    "gifiCode": "8918",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil330": {
    "gifiCode": "8918",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil196": {
    "gifiCode": "8960",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil332": {
    "gifiCode": "8960",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil197": {
    "gifiCode": "8961",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil333": {
    "gifiCode": "8961",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil198": {
    "gifiCode": "8962",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil334": {
    "gifiCode": "8962",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil199": {
    "gifiCode": "8963",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil335": {
    "gifiCode": "8963",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil200": {
    "gifiCode": "8964",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil336": {
    "gifiCode": "8964",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil202": {
    "gifiCode": "9010",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil338": {
    "gifiCode": "9010",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil203": {
    "gifiCode": "9011",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil339": {
    "gifiCode": "9011",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil204": {
    "gifiCode": "9012",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil340": {
    "gifiCode": "9012",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil205": {
    "gifiCode": "9013",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil341": {
    "gifiCode": "9013",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil206": {
    "gifiCode": "9014",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil342": {
    "gifiCode": "9014",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil208": {
    "gifiCode": "9060",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil344": {
    "gifiCode": "9060",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil209": {
    "gifiCode": "9061",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil345": {
    "gifiCode": "9061",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil210": {
    "gifiCode": "9062",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil346": {
    "gifiCode": "9062",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil211": {
    "gifiCode": "9063",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil347": {
    "gifiCode": "9063",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil212": {
    "gifiCode": "9064",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil348": {
    "gifiCode": "9064",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil213": {
    "gifiCode": "9065",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil349": {
    "gifiCode": "9065",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil214": {
    "gifiCode": "9066",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil350": {
    "gifiCode": "9066",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil216": {
    "gifiCode": "9110",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil352": {
    "gifiCode": "9110",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil217": {
    "gifiCode": "9130",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil353": {
    "gifiCode": "9130",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil218": {
    "gifiCode": "9131",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil354": {
    "gifiCode": "9131",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil219": {
    "gifiCode": "9132",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil355": {
    "gifiCode": "9132",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil220": {
    "gifiCode": "9133",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil356": {
    "gifiCode": "9133",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil221": {
    "gifiCode": "9134",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil357": {
    "gifiCode": "9134",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil222": {
    "gifiCode": "9135",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil358": {
    "gifiCode": "9135",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil223": {
    "gifiCode": "9136",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil359": {
    "gifiCode": "9136",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil224": {
    "gifiCode": "9137",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil360": {
    "gifiCode": "9137",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil225": {
    "gifiCode": "9138",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil361": {
    "gifiCode": "9138",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil226": {
    "gifiCode": "9139",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil362": {
    "gifiCode": "9139",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil393": {
    "gifiCode": "9150",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil394": {
    "gifiCode": "9150",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil396": {
    "gifiCode": "9151",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil397": {
    "gifiCode": "9151",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil399": {
    "gifiCode": "9152",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil400": {
    "gifiCode": "9152",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil228": {
    "gifiCode": "9180",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil364": {
    "gifiCode": "9180",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil229": {
    "gifiCode": "9200",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil365": {
    "gifiCode": "9200",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil230": {
    "gifiCode": "9201",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil366": {
    "gifiCode": "9201",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil232": {
    "gifiCode": "9220",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil368": {
    "gifiCode": "9220",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil233": {
    "gifiCode": "9221",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil369": {
    "gifiCode": "9221",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil234": {
    "gifiCode": "9222",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil370": {
    "gifiCode": "9222",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil235": {
    "gifiCode": "9223",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil371": {
    "gifiCode": "9223",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil236": {
    "gifiCode": "9224",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil372": {
    "gifiCode": "9224",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil237": {
    "gifiCode": "9225",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil373": {
    "gifiCode": "9225",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil239": {
    "gifiCode": "9270",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil375": {
    "gifiCode": "9270",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil240": {
    "gifiCode": "9271",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil376": {
    "gifiCode": "9271",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil241": {
    "gifiCode": "9272",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil377": {
    "gifiCode": "9272",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil242": {
    "gifiCode": "9273",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil378": {
    "gifiCode": "9273",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil243": {
    "gifiCode": "9274",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil379": {
    "gifiCode": "9274",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil244": {
    "gifiCode": "9275",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil380": {
    "gifiCode": "9275",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil245": {
    "gifiCode": "9276",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil381": {
    "gifiCode": "9276",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil246": {
    "gifiCode": "9277",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil382": {
    "gifiCode": "9277",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil247": {
    "gifiCode": "9278",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil383": {
    "gifiCode": "9278",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil248": {
    "gifiCode": "9279",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil384": {
    "gifiCode": "9279",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil249": {
    "gifiCode": "9280",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil385": {
    "gifiCode": "9280",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil250": {
    "gifiCode": "9281",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil386": {
    "gifiCode": "9281",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil251": {
    "gifiCode": "9282",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil387": {
    "gifiCode": "9282",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil252": {
    "gifiCode": "9283",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil388": {
    "gifiCode": "9283",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil253": {
    "gifiCode": "9284",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil389": {
    "gifiCode": "9284",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil402": {
    "gifiCode": "9285",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil403": {
    "gifiCode": "9285",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil405": {
    "gifiCode": "9286",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil406": {
    "gifiCode": "9286",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIL.Ttwgil255": {
    "gifiCode": "9367",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIL.Ttwgil391": {
    "gifiCode": "9367",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim53": {
    "gifiCode": "9370",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim112": {
    "gifiCode": "9370",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim54": {
    "gifiCode": "9371",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim113": {
    "gifiCode": "9371",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim55": {
    "gifiCode": "9372",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim114": {
    "gifiCode": "9372",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim56": {
    "gifiCode": "9373",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim115": {
    "gifiCode": "9373",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim57": {
    "gifiCode": "9374",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim116": {
    "gifiCode": "9374",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim58": {
    "gifiCode": "9375",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim117": {
    "gifiCode": "9375",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim59": {
    "gifiCode": "9376",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim118": {
    "gifiCode": "9376",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim60": {
    "gifiCode": "9377",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim119": {
    "gifiCode": "9377",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim61": {
    "gifiCode": "9378",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim120": {
    "gifiCode": "9378",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim62": {
    "gifiCode": "9379",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim121": {
    "gifiCode": "9379",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim64": {
    "gifiCode": "9420",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim123": {
    "gifiCode": "9420",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim65": {
    "gifiCode": "9421",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim124": {
    "gifiCode": "9421",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim66": {
    "gifiCode": "9422",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim125": {
    "gifiCode": "9422",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim67": {
    "gifiCode": "9423",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim126": {
    "gifiCode": "9423",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim68": {
    "gifiCode": "9424",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim127": {
    "gifiCode": "9424",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim69": {
    "gifiCode": "9425",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim128": {
    "gifiCode": "9425",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim70": {
    "gifiCode": "9426",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim129": {
    "gifiCode": "9426",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim72": {
    "gifiCode": "9470",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim131": {
    "gifiCode": "9470",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim73": {
    "gifiCode": "9471",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim132": {
    "gifiCode": "9471",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim74": {
    "gifiCode": "9472",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim133": {
    "gifiCode": "9472",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim75": {
    "gifiCode": "9473",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim134": {
    "gifiCode": "9473",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim76": {
    "gifiCode": "9474",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim135": {
    "gifiCode": "9474",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim77": {
    "gifiCode": "9475",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim136": {
    "gifiCode": "9475",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim78": {
    "gifiCode": "9476",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim137": {
    "gifiCode": "9476",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim79": {
    "gifiCode": "9477",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim138": {
    "gifiCode": "9477",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim172": {
    "gifiCode": "9478",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim173": {
    "gifiCode": "9478",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim175": {
    "gifiCode": "9479",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim176": {
    "gifiCode": "9479",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim178": {
    "gifiCode": "9480",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim179": {
    "gifiCode": "9480",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim81": {
    "gifiCode": "9520",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim140": {
    "gifiCode": "9520",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim82": {
    "gifiCode": "9521",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim141": {
    "gifiCode": "9521",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim181": {
    "gifiCode": "9522",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim182": {
    "gifiCode": "9522",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim184": {
    "gifiCode": "9523",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim185": {
    "gifiCode": "9523",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim187": {
    "gifiCode": "9524",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim188": {
    "gifiCode": "9524",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim84": {
    "gifiCode": "9540",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim143": {
    "gifiCode": "9540",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim85": {
    "gifiCode": "9541",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim144": {
    "gifiCode": "9541",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim86": {
    "gifiCode": "9542",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim145": {
    "gifiCode": "9542",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim190": {
    "gifiCode": "9544",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim191": {
    "gifiCode": "9544",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim209": {
    "gifiCode": "9545",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim210": {
    "gifiCode": "9545",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim212": {
    "gifiCode": "9546",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim213": {
    "gifiCode": "9546",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim89": {
    "gifiCode": "9570",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim148": {
    "gifiCode": "9570",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim90": {
    "gifiCode": "9571",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim149": {
    "gifiCode": "9571",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim91": {
    "gifiCode": "9572",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim150": {
    "gifiCode": "9572",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim92": {
    "gifiCode": "9573",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim151": {
    "gifiCode": "9573",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim96": {
    "gifiCode": "9600",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim155": {
    "gifiCode": "9600",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim97": {
    "gifiCode": "9601",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim156": {
    "gifiCode": "9601",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim98": {
    "gifiCode": "9602",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim157": {
    "gifiCode": "9602",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim99": {
    "gifiCode": "9603",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim158": {
    "gifiCode": "9603",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim100": {
    "gifiCode": "9604",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim159": {
    "gifiCode": "9604",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim101": {
    "gifiCode": "9605",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim160": {
    "gifiCode": "9605",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim102": {
    "gifiCode": "9606",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim161": {
    "gifiCode": "9606",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim103": {
    "gifiCode": "9607",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim162": {
    "gifiCode": "9607",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim104": {
    "gifiCode": "9608",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim163": {
    "gifiCode": "9608",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim105": {
    "gifiCode": "9609",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim164": {
    "gifiCode": "9609",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim106": {
    "gifiCode": "9610",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim165": {
    "gifiCode": "9610",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim107": {
    "gifiCode": "9611",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim166": {
    "gifiCode": "9611",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim108": {
    "gifiCode": "9612",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim167": {
    "gifiCode": "9612",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim109": {
    "gifiCode": "9613",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim168": {
    "gifiCode": "9613",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim193": {
    "gifiCode": "9614",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim194": {
    "gifiCode": "9614",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim196": {
    "gifiCode": "9615",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim197": {
    "gifiCode": "9615",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim199": {
    "gifiCode": "9616",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim200": {
    "gifiCode": "9616",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim206": {
    "gifiCode": "9617",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim207": {
    "gifiCode": "9617",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim202": {
    "gifiCode": "9650",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim203": {
    "gifiCode": "9650",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIM.Ttwgim111": {
    "gifiCode": "9659",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIM.Ttwgim170": {
    "gifiCode": "9659",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin59": {
    "gifiCode": "9660",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin121": {
    "gifiCode": "9660",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin60": {
    "gifiCode": "9661",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin122": {
    "gifiCode": "9661",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin61": {
    "gifiCode": "9662",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin123": {
    "gifiCode": "9662",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin62": {
    "gifiCode": "9663",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin124": {
    "gifiCode": "9663",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin63": {
    "gifiCode": "9664",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin125": {
    "gifiCode": "9664",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin66": {
    "gifiCode": "9710",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin128": {
    "gifiCode": "9710",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin67": {
    "gifiCode": "9711",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin129": {
    "gifiCode": "9711",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin68": {
    "gifiCode": "9712",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin130": {
    "gifiCode": "9712",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin69": {
    "gifiCode": "9713",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin131": {
    "gifiCode": "9713",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin70": {
    "gifiCode": "9714",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin132": {
    "gifiCode": "9714",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin72": {
    "gifiCode": "9760",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin134": {
    "gifiCode": "9760",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin73": {
    "gifiCode": "9761",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin135": {
    "gifiCode": "9761",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin74": {
    "gifiCode": "9762",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin136": {
    "gifiCode": "9762",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin75": {
    "gifiCode": "9763",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin137": {
    "gifiCode": "9763",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin76": {
    "gifiCode": "9764",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin138": {
    "gifiCode": "9764",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin77": {
    "gifiCode": "9765",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin139": {
    "gifiCode": "9765",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin79": {
    "gifiCode": "9790",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin141": {
    "gifiCode": "9790",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin80": {
    "gifiCode": "9791",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin142": {
    "gifiCode": "9791",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin81": {
    "gifiCode": "9792",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin143": {
    "gifiCode": "9792",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin82": {
    "gifiCode": "9793",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin144": {
    "gifiCode": "9793",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin83": {
    "gifiCode": "9794",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin145": {
    "gifiCode": "9794",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin84": {
    "gifiCode": "9795",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin146": {
    "gifiCode": "9795",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin85": {
    "gifiCode": "9796",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin147": {
    "gifiCode": "9796",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin86": {
    "gifiCode": "9797",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin148": {
    "gifiCode": "9797",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin87": {
    "gifiCode": "9798",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin149": {
    "gifiCode": "9798",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin88": {
    "gifiCode": "9799",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin150": {
    "gifiCode": "9799",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin89": {
    "gifiCode": "9800",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin151": {
    "gifiCode": "9800",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin90": {
    "gifiCode": "9801",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin152": {
    "gifiCode": "9801",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin91": {
    "gifiCode": "9802",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin153": {
    "gifiCode": "9802",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin92": {
    "gifiCode": "9803",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin154": {
    "gifiCode": "9803",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin93": {
    "gifiCode": "9804",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin155": {
    "gifiCode": "9804",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin94": {
    "gifiCode": "9805",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin156": {
    "gifiCode": "9805",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin95": {
    "gifiCode": "9806",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin157": {
    "gifiCode": "9806",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin96": {
    "gifiCode": "9807",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin158": {
    "gifiCode": "9807",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin97": {
    "gifiCode": "9808",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin159": {
    "gifiCode": "9808",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin98": {
    "gifiCode": "9809",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin160": {
    "gifiCode": "9809",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin99": {
    "gifiCode": "9810",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin161": {
    "gifiCode": "9810",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin100": {
    "gifiCode": "9811",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin162": {
    "gifiCode": "9811",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin101": {
    "gifiCode": "9812",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin163": {
    "gifiCode": "9812",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin102": {
    "gifiCode": "9813",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin164": {
    "gifiCode": "9813",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin103": {
    "gifiCode": "9814",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin165": {
    "gifiCode": "9814",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin104": {
    "gifiCode": "9815",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin166": {
    "gifiCode": "9815",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin105": {
    "gifiCode": "9816",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin167": {
    "gifiCode": "9816",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin106": {
    "gifiCode": "9817",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin168": {
    "gifiCode": "9817",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin107": {
    "gifiCode": "9818",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin169": {
    "gifiCode": "9818",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin108": {
    "gifiCode": "9819",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin170": {
    "gifiCode": "9819",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin109": {
    "gifiCode": "9820",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin171": {
    "gifiCode": "9820",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin110": {
    "gifiCode": "9821",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin172": {
    "gifiCode": "9821",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin111": {
    "gifiCode": "9822",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin173": {
    "gifiCode": "9822",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin112": {
    "gifiCode": "9823",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin174": {
    "gifiCode": "9823",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin113": {
    "gifiCode": "9824",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin175": {
    "gifiCode": "9824",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin114": {
    "gifiCode": "9825",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin176": {
    "gifiCode": "9825",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin115": {
    "gifiCode": "9826",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin177": {
    "gifiCode": "9826",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin116": {
    "gifiCode": "9827",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin178": {
    "gifiCode": "9827",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin117": {
    "gifiCode": "9828",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin179": {
    "gifiCode": "9828",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin184": {
    "gifiCode": "9829",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin185": {
    "gifiCode": "9829",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin187": {
    "gifiCode": "9830",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin188": {
    "gifiCode": "9830",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin190": {
    "gifiCode": "9831",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin191": {
    "gifiCode": "9831",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin193": {
    "gifiCode": "9832",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin194": {
    "gifiCode": "9832",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin196": {
    "gifiCode": "9833",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin197": {
    "gifiCode": "9833",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin199": {
    "gifiCode": "9834",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin200": {
    "gifiCode": "9834",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin202": {
    "gifiCode": "9835",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin203": {
    "gifiCode": "9835",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin209": {
    "gifiCode": "9836",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin210": {
    "gifiCode": "9836",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin205": {
    "gifiCode": "9850",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin206": {
    "gifiCode": "9850",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin119": {
    "gifiCode": "9870",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin181": {
    "gifiCode": "9870",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIN.Ttwgin120": {
    "gifiCode": "9898",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIN.Ttwgin182": {
    "gifiCode": "9898",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGOA.Ttwgoa62": {
    "gifiCode": "1000",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa63": {
    "gifiCode": "1001",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa64": {
    "gifiCode": "1002",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa65": {
    "gifiCode": "1003",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa66": {
    "gifiCode": "1004",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa67": {
    "gifiCode": "1005",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa68": {
    "gifiCode": "1006",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa69": {
    "gifiCode": "1007",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa71": {
    "gifiCode": "1060",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa72": {
    "gifiCode": "1062",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa73": {
    "gifiCode": "1064",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa74": {
    "gifiCode": "1066",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa75": {
    "gifiCode": "1067",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa76": {
    "gifiCode": "1068",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa77": {
    "gifiCode": "1069",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa78": {
    "gifiCode": "1071",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa133": {
    "gifiCode": "1073",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa80": {
    "gifiCode": "1061",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa81": {
    "gifiCode": "1063",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa82": {
    "gifiCode": "1065",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa83": {
    "gifiCode": "1070",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa84": {
    "gifiCode": "1072",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa86": {
    "gifiCode": "1120",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa87": {
    "gifiCode": "1121",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa88": {
    "gifiCode": "1122",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa89": {
    "gifiCode": "1123",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa90": {
    "gifiCode": "1124",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa91": {
    "gifiCode": "1125",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa92": {
    "gifiCode": "1126",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa93": {
    "gifiCode": "1127",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa95": {
    "gifiCode": "1180",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa96": {
    "gifiCode": "1181",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa97": {
    "gifiCode": "1182",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa98": {
    "gifiCode": "1183",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa99": {
    "gifiCode": "1184",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa100": {
    "gifiCode": "1185",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa101": {
    "gifiCode": "1186",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa102": {
    "gifiCode": "1187",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa104": {
    "gifiCode": "1240",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa105": {
    "gifiCode": "1241",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa106": {
    "gifiCode": "1242",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa107": {
    "gifiCode": "1243",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa108": {
    "gifiCode": "1244",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa110": {
    "gifiCode": "1300",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa111": {
    "gifiCode": "1301",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa112": {
    "gifiCode": "1302",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa113": {
    "gifiCode": "1303",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa115": {
    "gifiCode": "1360",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa116": {
    "gifiCode": "1380",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa117": {
    "gifiCode": "1400",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa118": {
    "gifiCode": "1401",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa119": {
    "gifiCode": "1402",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa120": {
    "gifiCode": "1403",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa122": {
    "gifiCode": "1460",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa123": {
    "gifiCode": "1480",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa124": {
    "gifiCode": "1481",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa125": {
    "gifiCode": "1482",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa126": {
    "gifiCode": "1483",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa127": {
    "gifiCode": "1484",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa128": {
    "gifiCode": "1485",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa129": {
    "gifiCode": "1486",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOA.Ttwgoa131": {
    "gifiCode": "1599",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob51": {
    "gifiCode": "1600",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob52": {
    "gifiCode": "1601",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob53": {
    "gifiCode": "1620",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob54": {
    "gifiCode": "1622",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob55": {
    "gifiCode": "1624",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob56": {
    "gifiCode": "1626",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob57": {
    "gifiCode": "1628",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob58": {
    "gifiCode": "1630",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob59": {
    "gifiCode": "1632",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob61": {
    "gifiCode": "1680",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob62": {
    "gifiCode": "1682",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob63": {
    "gifiCode": "1684",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob65": {
    "gifiCode": "1740",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob66": {
    "gifiCode": "1742",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob67": {
    "gifiCode": "1744",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob68": {
    "gifiCode": "1746",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob69": {
    "gifiCode": "1748",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob70": {
    "gifiCode": "1750",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob71": {
    "gifiCode": "1752",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob72": {
    "gifiCode": "1754",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob73": {
    "gifiCode": "1756",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob74": {
    "gifiCode": "1758",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob75": {
    "gifiCode": "1760",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob76": {
    "gifiCode": "1762",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob77": {
    "gifiCode": "1764",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob78": {
    "gifiCode": "1766",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob79": {
    "gifiCode": "1768",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob80": {
    "gifiCode": "1770",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob81": {
    "gifiCode": "1772",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob82": {
    "gifiCode": "1774",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob83": {
    "gifiCode": "1776",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob84": {
    "gifiCode": "1778",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob85": {
    "gifiCode": "1780",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob86": {
    "gifiCode": "1782",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob87": {
    "gifiCode": "1783",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob88": {
    "gifiCode": "1785",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob89": {
    "gifiCode": "1787",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob91": {
    "gifiCode": "1900",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob92": {
    "gifiCode": "1902",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob93": {
    "gifiCode": "1904",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob94": {
    "gifiCode": "1906",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob95": {
    "gifiCode": "1908",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob96": {
    "gifiCode": "1910",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob97": {
    "gifiCode": "1912",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob98": {
    "gifiCode": "1914",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob99": {
    "gifiCode": "1916",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob100": {
    "gifiCode": "1918",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob101": {
    "gifiCode": "1920",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob102": {
    "gifiCode": "1921",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOB.Ttwgob104": {
    "gifiCode": "2008",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc47": {
    "gifiCode": "1602",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc48": {
    "gifiCode": "1621",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc49": {
    "gifiCode": "1623",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc50": {
    "gifiCode": "1625",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc51": {
    "gifiCode": "1627",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc52": {
    "gifiCode": "1629",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc53": {
    "gifiCode": "1631",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc54": {
    "gifiCode": "1633",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc56": {
    "gifiCode": "1681",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc57": {
    "gifiCode": "1683",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc59": {
    "gifiCode": "1741",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc60": {
    "gifiCode": "1743",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc61": {
    "gifiCode": "1745",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc62": {
    "gifiCode": "1747",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc63": {
    "gifiCode": "1749",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc64": {
    "gifiCode": "1751",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc65": {
    "gifiCode": "1753",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc66": {
    "gifiCode": "1755",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc67": {
    "gifiCode": "1757",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc68": {
    "gifiCode": "1759",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc69": {
    "gifiCode": "1761",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc70": {
    "gifiCode": "1763",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc71": {
    "gifiCode": "1765",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc72": {
    "gifiCode": "1767",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc73": {
    "gifiCode": "1769",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc74": {
    "gifiCode": "1771",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc75": {
    "gifiCode": "1773",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc76": {
    "gifiCode": "1775",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc77": {
    "gifiCode": "1777",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc78": {
    "gifiCode": "1779",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc79": {
    "gifiCode": "1781",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc80": {
    "gifiCode": "1784",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc81": {
    "gifiCode": "1786",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc82": {
    "gifiCode": "1788",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc84": {
    "gifiCode": "1901",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc85": {
    "gifiCode": "1903",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc86": {
    "gifiCode": "1905",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc87": {
    "gifiCode": "1907",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc88": {
    "gifiCode": "1909",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc89": {
    "gifiCode": "1911",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc90": {
    "gifiCode": "1913",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc91": {
    "gifiCode": "1915",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc92": {
    "gifiCode": "1917",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc93": {
    "gifiCode": "1919",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc94": {
    "gifiCode": "1922",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOC.Ttwgoc96": {
    "gifiCode": "2009",
    "isCredit": 1,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOD.Ttwgod15": {
    "gifiCode": "2010",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOD.Ttwgod16": {
    "gifiCode": "2012",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOD.Ttwgod17": {
    "gifiCode": "2014",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOD.Ttwgod18": {
    "gifiCode": "2016",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOD.Ttwgod19": {
    "gifiCode": "2018",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOD.Ttwgod20": {
    "gifiCode": "2020",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOD.Ttwgod21": {
    "gifiCode": "2022",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOD.Ttwgod22": {
    "gifiCode": "2024",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOD.Ttwgod23": {
    "gifiCode": "2026",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOD.Ttwgod25": {
    "gifiCode": "2070",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOD.Ttwgod26": {
    "gifiCode": "2072",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOD.Ttwgod27": {
    "gifiCode": "2074",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOD.Ttwgod28": {
    "gifiCode": "2076",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOD.Ttwgod30": {
    "gifiCode": "2178",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOE.Ttwgoe15": {
    "gifiCode": "2011",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOE.Ttwgoe16": {
    "gifiCode": "2013",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOE.Ttwgoe17": {
    "gifiCode": "2015",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOE.Ttwgoe18": {
    "gifiCode": "2017",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOE.Ttwgoe19": {
    "gifiCode": "2019",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOE.Ttwgoe20": {
    "gifiCode": "2021",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOE.Ttwgoe21": {
    "gifiCode": "2023",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOE.Ttwgoe22": {
    "gifiCode": "2025",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOE.Ttwgoe23": {
    "gifiCode": "2027",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOE.Ttwgoe25": {
    "gifiCode": "2071",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOE.Ttwgoe26": {
    "gifiCode": "2073",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOE.Ttwgoe27": {
    "gifiCode": "2075",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOE.Ttwgoe28": {
    "gifiCode": "2077",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOE.Ttwgoe30": {
    "gifiCode": "2179",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof46": {
    "gifiCode": "2180",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof47": {
    "gifiCode": "2181",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof48": {
    "gifiCode": "2182",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof49": {
    "gifiCode": "2183",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof51": {
    "gifiCode": "2190",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof52": {
    "gifiCode": "2200",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof53": {
    "gifiCode": "2220",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof54": {
    "gifiCode": "2240",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof55": {
    "gifiCode": "2241",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof56": {
    "gifiCode": "2242",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof57": {
    "gifiCode": "2243",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof58": {
    "gifiCode": "2244",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof59": {
    "gifiCode": "2245",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof60": {
    "gifiCode": "2246",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof61": {
    "gifiCode": "2247",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof62": {
    "gifiCode": "2248",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof63": {
    "gifiCode": "2249",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof64": {
    "gifiCode": "2250",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof66": {
    "gifiCode": "2280",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof67": {
    "gifiCode": "2300",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof68": {
    "gifiCode": "2301",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof69": {
    "gifiCode": "2302",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof70": {
    "gifiCode": "2303",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof71": {
    "gifiCode": "2304",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof72": {
    "gifiCode": "2305",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof73": {
    "gifiCode": "2306",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof74": {
    "gifiCode": "2307",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof75": {
    "gifiCode": "2308",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof76": {
    "gifiCode": "2309",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof77": {
    "gifiCode": "2310",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof78": {
    "gifiCode": "2311",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof80": {
    "gifiCode": "2360",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof81": {
    "gifiCode": "2361",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof82": {
    "gifiCode": "2362",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof83": {
    "gifiCode": "2363",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof84": {
    "gifiCode": "2364",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof86": {
    "gifiCode": "2420",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof87": {
    "gifiCode": "2421",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof88": {
    "gifiCode": "2422",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof89": {
    "gifiCode": "2423",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof90": {
    "gifiCode": "2424",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof91": {
    "gifiCode": "2426",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof92": {
    "gifiCode": "2427",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof94": {
    "gifiCode": "2425",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOF.Ttwgof95": {
    "gifiCode": "2589",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog40": {
    "gifiCode": "2600",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog41": {
    "gifiCode": "2620",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog42": {
    "gifiCode": "2621",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog43": {
    "gifiCode": "2622",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog44": {
    "gifiCode": "2623",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog45": {
    "gifiCode": "2624",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog46": {
    "gifiCode": "2625",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog47": {
    "gifiCode": "2626",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog48": {
    "gifiCode": "2627",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog49": {
    "gifiCode": "2628",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog50": {
    "gifiCode": "2629",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog85": {
    "gifiCode": "2630",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog52": {
    "gifiCode": "2680",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog53": {
    "gifiCode": "2700",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog54": {
    "gifiCode": "2701",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog55": {
    "gifiCode": "2702",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog56": {
    "gifiCode": "2703",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog57": {
    "gifiCode": "2704",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog58": {
    "gifiCode": "2705",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog59": {
    "gifiCode": "2706",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog132": {
    "gifiCode": "2707",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog61": {
    "gifiCode": "2770",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog62": {
    "gifiCode": "2780",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog63": {
    "gifiCode": "2781",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog64": {
    "gifiCode": "2782",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog65": {
    "gifiCode": "2783",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog67": {
    "gifiCode": "2840",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog68": {
    "gifiCode": "2860",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog69": {
    "gifiCode": "2861",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog70": {
    "gifiCode": "2862",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog71": {
    "gifiCode": "2863",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog73": {
    "gifiCode": "2920",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog74": {
    "gifiCode": "2940",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog75": {
    "gifiCode": "2960",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog76": {
    "gifiCode": "2961",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog77": {
    "gifiCode": "2962",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog78": {
    "gifiCode": "2963",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog79": {
    "gifiCode": "2964",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog80": {
    "gifiCode": "2965",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog81": {
    "gifiCode": "2966",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOG.Ttwgog83": {
    "gifiCode": "3139",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh36": {
    "gifiCode": "3140",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh37": {
    "gifiCode": "3141",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh38": {
    "gifiCode": "3142",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh39": {
    "gifiCode": "3143",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh40": {
    "gifiCode": "3144",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh41": {
    "gifiCode": "3145",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh42": {
    "gifiCode": "3146",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh43": {
    "gifiCode": "3147",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh44": {
    "gifiCode": "3148",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh45": {
    "gifiCode": "3149",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh46": {
    "gifiCode": "3150",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh47": {
    "gifiCode": "3151",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh48": {
    "gifiCode": "3152",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh50": {
    "gifiCode": "3200",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh51": {
    "gifiCode": "3210",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh52": {
    "gifiCode": "3220",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh53": {
    "gifiCode": "3240",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh54": {
    "gifiCode": "3260",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh55": {
    "gifiCode": "3261",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh56": {
    "gifiCode": "3262",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh57": {
    "gifiCode": "3263",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh59": {
    "gifiCode": "3270",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh60": {
    "gifiCode": "3280",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh61": {
    "gifiCode": "3300",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh62": {
    "gifiCode": "3301",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh63": {
    "gifiCode": "3302",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh65": {
    "gifiCode": "3320",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh66": {
    "gifiCode": "3321",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh67": {
    "gifiCode": "3322",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh68": {
    "gifiCode": "3323",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh69": {
    "gifiCode": "3324",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh70": {
    "gifiCode": "3325",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh71": {
    "gifiCode": "3326",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh72": {
    "gifiCode": "3327",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh76": {
    "gifiCode": "3328",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOH.Ttwgoh74": {
    "gifiCode": "3450",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOI.Ttwgoi10": {
    "gifiCode": "3500",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOI.Ttwgoi11": {
    "gifiCode": "3520",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOI.Ttwgoi12": {
    "gifiCode": "3540",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOI.Ttwgoi13": {
    "gifiCode": "3541",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOI.Ttwgoi14": {
    "gifiCode": "3542",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOI.Ttwgoi15": {
    "gifiCode": "3543",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOI.Ttwgoi17": {
    "gifiCode": "3570",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOI.Ttwgoi22": {
    "gifiCode": "3580",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOI.Ttwgoi18": {
    "gifiCode": "3600",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOI.Ttwgoi19": {
    "gifiCode": "3620",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOJ.Ttwgoj13": {
    "gifiCode": "3660",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOJ.Ttwgoj14": {
    "gifiCode": "3680",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOJ.Ttwgoj15": {
    "gifiCode": "3700",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOJ.Ttwgoj16": {
    "gifiCode": "3701",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOJ.Ttwgoj17": {
    "gifiCode": "3702",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOJ.Ttwgoj19": {
    "gifiCode": "3720",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOJ.Ttwgoj20": {
    "gifiCode": "3740",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOJ.Ttwgoj21": {
    "gifiCode": "3741",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOJ.Ttwgoj22": {
    "gifiCode": "3742",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOJ.Ttwgoj23": {
    "gifiCode": "3743",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOJ.Ttwgoj24": {
    "gifiCode": "3744",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOJ.Ttwgoj28": {
    "gifiCode": "3745",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGOJ.Ttwgoj26": {
    "gifiCode": "3849",
    "isCredit": 0,
    "arrIndex": 1,
    "colIndex": 0
  },
  "GFGFA.Ttwgfa9": {
    "gifiCode": "9970",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGFA.Ttwgfa17": {
    "gifiCode": "9970",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGFA.Ttwgfa10": {
    "gifiCode": "9975",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGFA.Ttwgfa18": {
    "gifiCode": "9975",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGFA.Ttwgfa11": {
    "gifiCode": "9976",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGFA.Ttwgfa19": {
    "gifiCode": "9976",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGFA.Ttwgfa12": {
    "gifiCode": "9980",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGFA.Ttwgfa20": {
    "gifiCode": "9980",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGFA.Ttwgfa13": {
    "gifiCode": "9985",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGFA.Ttwgfa21": {
    "gifiCode": "9985",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGFA.Ttwgfa14": {
    "gifiCode": "9990",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGFA.Ttwgfa22": {
    "gifiCode": "9990",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGFA.Ttwgfa15": {
    "gifiCode": "9995",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGFA.Ttwgfa23": {
    "gifiCode": "9995",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGFA.Ttwgfa28": {
    "gifiCode": "9998",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGFA.Ttwgfa29": {
    "gifiCode": "9998",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFGFA.Ttwgfa16": {
    "gifiCode": "9999",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFGFA.Ttwgfa24": {
    "gifiCode": "9999",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIP.Ttwgip9": {
    "gifiCode": "7000",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIP.Ttwgip17": {
    "gifiCode": "7000",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIP.Ttwgip10": {
    "gifiCode": "7002",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GGIP.Ttwgip18": {
    "gifiCode": "7002",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIP.Ttwgip11": {
    "gifiCode": "7004",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIP.Ttwgip19": {
    "gifiCode": "7004",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIP.Ttwgip12": {
    "gifiCode": "7006",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIP.Ttwgip20": {
    "gifiCode": "7006",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIP.Ttwgip13": {
    "gifiCode": "7008",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIP.Ttwgip21": {
    "gifiCode": "7008",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIP.Ttwgip14": {
    "gifiCode": "7010",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIP.Ttwgip22": {
    "gifiCode": "7010",
    "isCredit": 0,
    "colIndex": 1
  },
  "GFBGII.GFGIP.Ttwgip15": {
    "gifiCode": "7020",
    "isCredit": 0,
    "colIndex": 0
  },
  "GFBGII.GFGIP.Ttwgip23": {
    "gifiCode": "7020",
    "isCredit": 0,
    "colIndex": 1
  }
};

}());