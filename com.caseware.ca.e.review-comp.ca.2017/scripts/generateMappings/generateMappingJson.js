/**********************************************************
 * This script generates the json object as output.json
 * from mappingCsv.csv
 * should be inserted into taxprepMapping.js
 *
 * to run in console: node generateMappingJson.js
 * ********************************************************/

var fs = require("fs");
var csv = require('fast-csv');

//define the csv path
var csvPath= "mappingCsv.csv";

//take value from csv
var csvData = [];
var cols = null;
fs.createReadStream(csvPath)
    .pipe(csv())
    .on('data', function(data) {

      if (!cols) {
        cols = [];
        for (var i = 0; i < data.length; i++) {
          cols[i] = data[i];
        }
      }

      var dataObj = {};
      cols.forEach(function(col, index) {
        dataObj[col] = data[index];
      });
      csvData.push(dataObj);

    })
    .on('end', afterReadCsv);

//helper function to strip out [x] sequences in taxprep field identifiers
function stripTaxprepBrackets(field) {
  var index = field ? field.indexOf('[') : -1;
  if (index === -1)
    return field;

  while (index !== -1) {
    field = field.slice(0, index) + field.slice(index + 3, field.length);
    index = field.indexOf('[');
  }
  return field;
}

//Convert csv data into desired mapping
function convertToMap(origMap) {
  var result = {};
  if (!origMap)
    return result;

  origMap.forEach(function(row) {
    //Remove all invalid corpTaxNums here
    if (row.taxprepId === 'N/A' || row.corpTaxFormId === 'T2S426')
      return;

    //Add desired attributes from csv
    var newMapping = {};
    newMapping.corpTaxFormId = row.corpTaxFormId.toUpperCase();
    newMapping.corpTaxNum = row.corpTaxNum;
    newMapping.isTable = parseInt(row.isTable) || 0;
    newMapping.colIndex = parseInt(row.colIndex) || 0;
    newMapping.rowIndex = parseInt(row.rowIndex) || 0;
    newMapping.isSanitized = parseInt(row.isSanitized) || 0;
    newMapping.type = row.type;

    //Check for multiple mappings of the same taxprepId
    var strippedId = stripTaxprepBrackets(row.taxprepId);
    if (result.hasOwnProperty(strippedId)) {
      if (!Array.isArray(result[strippedId]))
        result[strippedId] = [result[strippedId]]; //wrap in array
      result[strippedId].push(newMapping)
    } else {
      result[strippedId] = newMapping;
    }
  });
  return result;
}

function afterReadCsv() {
  var output = convertToMap(csvData);

  //export the json to file
  fs.writeFile('mapping.json', JSON.stringify(output, null, 2), function () {
    console.log("The file was saved!");
  });

  var mappingMock = '(function() {\n'+
  '"use strict";\n'+
      'if (!wpw.tax.testdata)\n'+
      'wpw.tax.testdata = {};\n'+
  'wpw.tax.testdata.mappingMock =\n' +
      JSON.stringify(output, null, 2)+';\n\n' +
      '}());';

  //export the json to mock mapping
  fs.writeFile('mapping-mock.js', mappingMock, function () {
    console.log("The mock mapping file was saved!");
  });
}
