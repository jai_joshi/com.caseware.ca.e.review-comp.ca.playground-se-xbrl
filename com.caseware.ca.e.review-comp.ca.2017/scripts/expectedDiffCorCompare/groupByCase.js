/**Usage:
 * Download csv from COR Diff from Google spreadsheet named it as 'input.csv'
 * run this by node groupByCase.js
 * It will output 'result' which you can copy and paste to scenario-testing-service.js inside
 * function exceptionsByScenario(testName, compareKey, obj2)
 *
 * Note: in the google  doc:
 *  if there is only formID --> ignore wholeform--> push to ignoredForms array
 *  if there is formID, fieldID --> push to ignoredFields array
 */


var fs = require("fs");
var csv = require('fast-csv');
var json2csv = require('json2csv');

var inputPath = "input.csv";
var inputData = [];
var cols = null;
fs.createReadStream(inputPath)
    .pipe(csv())
    .on('data', function(data) {

      if (!cols) {
        cols = [];
        for (var i = 0; i < data.length; i++) {
          cols[i] = data[i];
        }
      }

      var dataObj = {};
      cols.forEach(function(col, index) {
        dataObj[col] = data[index];
      });
      inputData.push(dataObj);

    })
    .on('end', afterReadCsv);

//COLUMN NAMES
var case_name = 'Case name';
var form_id = 'Form ID';
var field_id = 'Field ID';
var row_index = 'rIndex';

//STRING BUILDING VARS
var tab = '  ';
var nl = '\n';
var result = '';
var brk = 'break;';

function buildEntry(form, field, row) {
  var sequence = '1';
  var result = form;
  if (field && field !== '') {
    result = result + '-' + sequence + '-' + field;
  }
  if (row && row !== '') {
    result = result + '-' + row;
  }
  return result;
}

function generateFormsLine(formsArr) {
  if (!formsArr.length) {
    return '';
  }

  return tab + 'ignoredForms = ' + JSON.stringify(formsArr) + ';' + nl;
}

function generateFieldsLine(fieldsArr) {
  if (!fieldsArr.length) {
    return '';
  }

  return tab + 'ignoredFields = ' + JSON.stringify(fieldsArr) + ';' + nl;
}

function generateSwitchCases(inputJson) {

  Object.keys(inputJson).forEach(function(key) {
    result += 'case ' + '\'' + key + '\'' + ':' + nl +
        generateFormsLine(inputJson[key].forms) +
        generateFieldsLine(inputJson[key].fields) +
        tab + brk + nl;
  });

  return result;
}

function afterReadCsv() {
  var resultJson = {};
  var resultFile;

  inputData.forEach(function(dataObj) {
    resultJson[dataObj[case_name]] = resultJson[dataObj[case_name]] || {forms: [], fields: []};
    var entry = buildEntry(dataObj[form_id], dataObj[field_id], dataObj[row_index]);
    if (entry) {
      if (!dataObj[field_id] || dataObj[field_id] === '')
        resultJson[dataObj[case_name]].forms.push(entry);
      else
        resultJson[dataObj[case_name]].fields.push(entry);
    }
  });

  resultFile = generateSwitchCases(resultJson);

  //export to file
  fs.writeFile('result', resultFile, function () {
    console.log("The file was saved!");
  });
}