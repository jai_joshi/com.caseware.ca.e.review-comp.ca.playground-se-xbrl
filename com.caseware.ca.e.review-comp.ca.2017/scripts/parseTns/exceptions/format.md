The format for the exceptions json object should be:

```
{
  FORMID: {
    TN: REASON
  }
}
```

The `REASON` is optional and can just be replaced with `true` or any other truthy value.
