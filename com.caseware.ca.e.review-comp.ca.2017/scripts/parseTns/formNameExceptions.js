var fs = require('fs');
var path = require('path');

var formFolder = '../../../canada/t2/tax-cdn'
var formExceptions = {};

function getDirectories(srcpath) {
  return fs.readdirSync(srcpath).filter(function(file) {
    return fs.statSync(path.join(srcpath, file)).isDirectory();
  });
}

/**
 * Converts names that contains a dash to camelcase.
 * e.g. 'form-name' -> 'formName'
 **/
function normalizeFormName(name) {
  var result = '';
  var prevIndex = 0, strange = false;
  for (var i = 0; i < name.length; i++) {
    if (name[i] == '-') {
      if (/[A-Za-z]/.test(name[i+1])) {
        result += name.substring(prevIndex, i);
        result += name[i+1].toUpperCase();
        prevIndex = i + 1;
      } else {
        strange = true;
      }
    }
  }
  if (strange) {
    console.warn('Strange form name found:', name);
  }
  result += name.substr(prevIndex);
  return result;
}

getDirectories(formFolder).forEach(function(dir) {
  if (/[A-Z-]/.test(dir)) {
    dir = normalizeFormName(dir);
    formExceptions[dir.toUpperCase()] = dir;
  }
});

module.exports = formExceptions;
