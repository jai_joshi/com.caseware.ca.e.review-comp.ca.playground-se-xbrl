var formNameExceptions = require('./formNameExceptions.js');

function isArray(x) {
  return x.constructor === Array
}
function isObject(x) {
  return typeof x === 'object';
}
function isDefined(x) {
  return typeof x !== 'undefined';
}
function isUndefined(x) {
  return typeof x === 'undefined';
}
function isFunction(value) {
  return typeof value === 'function';
}
function isDate(value) {
  return toString.call(value) === '[object Date]';
}
function isRegExp(value) {
  return toString.call(value) === '[object RegExp]';
}

function forEach(obj, iterator, context) {
  for (var key in obj) {
    if (obj.hasOwnProperty) {
      if (obj.hasOwnProperty(key)) {
        iterator.call(context, obj[key], key, obj);
      }
    } else {
      iterator.call(context, obj[key], key, obj);
    }
  }

}

function setHashKey(obj, h) {
  if (h) {
    obj.$$hashKey = h;
  } else {
    delete obj.$$hashKey;
  }
}

function baseExtend(dst, objs, deep) {
  var h = dst.$$hashKey;

  for (var i = 0, ii = objs.length; i < ii; ++i) {
    var obj = objs[i];
    if (!isObject(obj) && !isFunction(obj)) continue;
    var keys = Object.keys(obj);
    for (var j = 0, jj = keys.length; j < jj; j++) {
      var key = keys[j];
      var src = obj[key];

      if (deep && isObject(src)) {
        if (isDate(src)) {
          dst[key] = new Date(src.valueOf());
        } else if (isRegExp(src)) {
          dst[key] = new RegExp(src);
        } else {
          if (!isObject(dst[key])) dst[key] = isArray(src) ? [] : {};
          baseExtend(dst[key], [src], true);
        }
      } else {
        dst[key] = src;
      }
    }
  }

  setHashKey(dst, h);
  return dst;
}

function extend(dst) {
  return baseExtend(dst, [].slice.call(arguments, 1), false);
}
function merge(dst) {
  return baseExtend(dst, [].slice.call(arguments, 1), true);
}
function getFullFieldId(fieldId, rowIndex, colIndex) {
  if (isDefined(rowIndex) && isDefined(colIndex))
    return fieldId + '[' + rowIndex + ',' + colIndex + ']';
  return fieldId;
}
function getCalcFormId(formId) {
  var calcFormId = formId.toLowerCase();

  var dashPos = calcFormId.indexOf('-');
  if (dashPos != -1)
    calcFormId = calcFormId.substring(0, dashPos);

  if (formNameExceptions[calcFormId.toUpperCase()]) {
    calcFormId = formNameExceptions[calcFormId.toUpperCase()];
  }

  return calcFormId;
}

function codeToDropdownOptions(codes, language) {
  var options = [];
  for (var code in codes) {
    options.push({
      option: codes[code][language],
      value: code
    });
  }
  return options
}

function initializeGifiCells(tableCalcObj) {
  var initialRows = [].concat(tableCalcObj.fixedGifiRows ? tableCalcObj.fixedGifiRows : [],
      tableCalcObj.fixedRows ? [] : [{}],
      tableCalcObj.endFixedGifiRows ? tableCalcObj.endFixedGifiRows : []);

  return initialRows.map(function(gifiRow, index) {
    if (gifiRow && gifiRow.code) {
      var cellRowObj = {
        0: {label: gifiRow.code, labelClass: 'bold center', inputType: 'none'},
        1: {label: gifiRow.description, labelClass: 'bold', inputType: 'none'}
      };

      if (tableCalcObj.fixedGifiRows && index < tableCalcObj.fixedGifiRows.length) {
        if (index == tableCalcObj.fixedGifiRows.length - 1)
          cellRowObj.cannotDelete = true;
        else
          cellRowObj.hideButtons = true;
      }
      else if (tableCalcObj.endFixedGifiRows && index >= initialRows.length - tableCalcObj.endFixedGifiRows.length) {
        cellRowObj.hideButtons = true;
      }

      if (gifiRow.isCredit)
        cellRowObj.isCredit = true;

      return cellRowObj;
    }
    return {};
  });
}

module.exports = {
  isArray: isArray,
  isObject: isObject,
  isDefined: isDefined,
  isUndefined: isUndefined,
  forEach: forEach,
  extend: extend,
  merge: merge,
  getFullFieldId: getFullFieldId,
  codeToDropdownOptions: codeToDropdownOptions,
  initializeGifiCells: initializeGifiCells
}
