@echo off

:: Make sure the output folders exists
mkdir missing
mkdir tns
mkdir exceptions

:: Parse the TNs from corptax
node parseTns.js 

:: Parse the TNs from efile workchart
node parseEfileWorkchart.js 

:: Check for EFILE TNs that aren't in corptax
node compareTns.js -e tns\corpTaxTns.json tns\efileTns.json > missing\missingCorpTax.json

:: Check for TNs in corptax that aren't in EFILE workchart
node compareTns.js -x exceptions\workchartExceptions.json tns\workchartTns.json tns\corpTaxTns.json > missing\missingWorkchart.json

pause
