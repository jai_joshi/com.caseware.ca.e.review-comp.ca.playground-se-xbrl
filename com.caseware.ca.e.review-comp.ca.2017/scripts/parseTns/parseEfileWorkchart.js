var fs = require('fs');

var efileWorkChartPath = '../../../abstract/utilities/efile.js';

var efileWorkchart = fs.readFileSync(efileWorkChartPath, 'utf8');
eval(efileWorkchart.match(/var taxForms((?!};)[\s\S])+};/)[0]);

var tnList = {};

function addTns(formId, data) {
  formId = formId.toLowerCase();
  if (!tnList[formId]) {
    tnList[formId] = {};
  }
  for (var i = 0; i < data.length; i++) {
    var field = data[i];
    if (!field.type) {
      continue;
    }
    if (field.type == 'table') {
      addTns(formId, field.cells);
    } else {
      tnList[formId][field.lineCode] = true;
    }
  }
}
Object.keys(taxForms).forEach(function(formId) {
  addTns(formId, taxForms[formId].data);
})

for (var k in tnList) {
  tnList[k] = Object.keys(tnList[k]);
}

fs.writeFileSync('tns/workchartTns.json', JSON.stringify(tnList, null, 2));
