var fs = require('fs');
var path = require('path');
var q = require('q');
var mockFns = require('./mockFns.js');

var outputFolder = 'tns';
var outputFileName = 'corpTaxTns.json';

var formParserPath = '../../services/formdata/formParser.js';
var formsPath = '../../../canada/t2/tax-cdn';

var fileContents = fs.readFileSync(formParserPath, 'utf8');

function getInitializer(file) {
  var start = file.indexOf('function getFormParser');
  var end = file.indexOf('angular.module');
  var match = file.substring(start, end);
  return match;
}

// Initialize formParser
eval(getInitializer(fileContents));
var formParser = getFormParser(mockFns);

// Mock wpw and required functions
var wpw = {tax: {
  codes: {}
}};
wpw.tax.actions = {
  codeToDropdownOptions: mockFns.codeToDropdownOptions
};
wpw.tax.global = {
  formData: {},
  tableCalculations: {}
};

var angular = {
  extend: mockFns.extend,
  merge: mockFns.merge,
  forEach: mockFns.forEach,
  isArray: mockFns.isArray,
  isObject: mockFns.isObject
};

/**
 * @param {string} type - can be 'isDirectory' or 'isFile'. See fs.Stats
 **/
function getFiles(srcpath, type) {
  return fs.readdirSync(srcpath).filter(function(file) {
    return fs.statSync(path.join(srcpath, file))[type]();
  }).map(function(file) {
    return path.join(srcpath, file);
  });
}

function evalFile(file) {
  eval(fs.readFileSync(file, 'utf8'));
}

// Load files containing data we need
evalFile('../../codes/currency-codes.js');
evalFile('../../codes/titles-list.js');
evalFile('../../codes/country-address-codes.js');
evalFile('../../codes/gifi-codes.js');


/* * * Code to generate TN list * * */
// Stuff before this was just to set up mock environment to load
// all the form and table data.

function loadForm(formDir) {
  var deferred = q.defer();
  // console.log('loading form', formDir);
  var formDataFile, tablesFile;
  formDir = path.join(formDir, '1');

  fs.stat(formDir, function(err, stat) {
    if (!err) {
      getFiles(formDir, 'isFile').forEach(function(file) {
        if (file.endsWith('-formdata.js')) {
          formDataFile = file;
        } else if (file.endsWith('-tables.js')) {
          tablesFile = file;
        }
      });

      if (tablesFile) {
        evalFile(tablesFile);
      }
      if (formDataFile) {
        evalFile(formDataFile);
      }
      deferred.resolve(true);
    } else {
      console.warn('skipping:', formDir, 'does not exist');
      deferred.resolve(false);
    }
  });
  return deferred.promise;
}

function loadAllForms() {
  var waitList = [];
  getFiles(formsPath, 'isDirectory').forEach(function(dir) {
    waitList.push(loadForm(dir));
  });
  return q.all(waitList);
}



var tnList = {};

function addTn(tn, data, argsObj) {
  var formId = argsObj.formId;
  if (!tnList[formId]) {
    tnList[formId] = {};
  }
  tnList[formId][tn] = true;
}

formParser.executeOn('afterParse', function(data, output, argsObj) {
  if (data.tn) {
    addTn(data.tn, data, argsObj);
  }
  if (data.tn2) {
    addTn(data.tn2, data, argsObj);
  }
  if (argsObj.formId == 't2s345') {
    //console.log(data)
  }
  if (data.type == 'table') {
    table = formParser.getTableData(argsObj.formId, data.num);
    if (table && table.columns) {
      // parse the columns for tn
      for (var i = 0; i < table.columns.length; i++) {
        var col = table.columns[i];
        if (col.tn) {
          addTn(col.tn, col, argsObj);
        }
        if (col.totalTn) {
          addTn(col.totalTn, col, argsObj);
        }
      }
    }
  }
});

loadAllForms().then(function() {
  for (var formId in wpw.tax.global.formData) {
    try {
      formParser.parseFormData(wpw.tax.global.formData[formId], formId);
    } catch(e) {
      console.error('parseError', e);
      return;
    }
  }

  for (var formId in tnList) {
    tnList[formId] = Object.keys(tnList[formId]);
  }

  var output = JSON.stringify(tnList, null, 2);
  // console.log(output);
  fs.writeFileSync(path.join(outputFolder, outputFileName), output);
});

