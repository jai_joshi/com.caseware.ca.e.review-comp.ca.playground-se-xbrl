#!/bin/node
var fs = require('fs');
var mockFns = require('./mockFns.js');

var fileToCompare;
var corpTaxFile;
var isEfile = false;
var showAll = false;
var exceptions = {};
//var outputFileName = "tnDiffs/diffs.json";

function help() {
  console.log('Usage: node compareTns.js [OPTION]... FILE1 FILE2');
  console.log('Prints the tns that are in FILE2 but not in FILE1.');
  console.log('');
  console.log('');
  console.log('Options:');
  console.log('\t-x FILE\tDo not list anything listed inside the FILE');
  console.log('\t-e\tSet this option if FILE2 represents E-FILE tns');
  console.log('\t-a\tList all differences including the forms that ');
  console.log('\t\texist in FILE2 but not FILE1');
}

// Parse the command line arguments
function init() {
  var files = [];
  for (var i = 2; i < process.argv.length; i++) {
    var arg = process.argv[i];
    var nextArg = process.argv[i + 1];
    switch (arg) {
      case '-x':
        exceptions = JSON.parse(fs.readFileSync(nextArg, 'utf8'));
        i++;
        break;
      case '-e':
        isEfile = true;
        break;
      case '-a':
        showAll = true;
        break;
      default:
        files.push(arg);
    }
  }
  if (files.length < 2) {
    help();
    return;
  }
  corpTaxFile = files[0];
  fileToCompare = files[1];
  main();
}

/* Mismatching forms */
var efileCWMapping = {
  "t2s200": "t2j",
  "t2s30": "t2s1263"
};

/* Forms not supported by CW */
var notImplementedYet = [
  "t2s12", "t2s17", "t2s18", "t2s20", "t2s30", "t2s32","t2s60","t2s34", "t2s35", "t2s38", "t2s39", "t2s42", "t2s43", "t2s45",
  "t2s46", "t2s48", "t2s61", "t2s62", "t2s91", "t2s301", "t2s302", "t2s303", "t2s304", "t2s305", "t2s306", "t2s308",
  "t2s309", "t2s321", "t2s360", "t2s365", "t2s367", "t2s380", "t2s381", "t2s384", "t2s385", "t2s387", "t2s388",
  "t2s389", "t2s390", "t2s391", "t2s392", "t2s393", "t2s394", "t2s402", "t2s403", "t2s410", "t2s422", "t2s423",
  "t2s442", "t2s460", "t2s490", "t2s506", "t2s508", "t2s513", "t2s554", "t2s556", "t2s558", "t2s560", "t2s566", "t2s343"
];

/* CW unique forms */
var corpTaxOnlyForms = [
  "t2s10add", "advertisingWorkchart", "ratesMb", "ratesSk","automobileLeasing", "acbTracker", "t2s8w", "sec_20_1_e",
  "automobileInterest","rentalsWcSummaryTotal", "ratesBc","advertisingWorkchartS","ratesOn","wacOnline", "mealsEntS",
  "rentalsWcSummary", "scenarioWorkchart", "flagFormsWorkchart", "automobileSummary", "cp", "tpp", "t2s13w",
  "efile", "optimizationWorkchart", "t2s8rec", "t2rentalworkchart","mealsEnt","instalmentsWorkchart","t2s2w",
  "engagementProfile", "t2s2s", "t2s141s", "t2s10w", "ratesNs", "t2s2aw", "integrationWorkchart",
  "t2s2as", "t2s341", "rpp", "dataTransferWorkchart", "t2s8add", "ratesFed", "t2s21w", "t1134s","t106s","l996"
];

/* Forms in progrss */
var workingOn =["t2s37"];

/* Forms classified as other forms by CRA */
var otherForms = ["t1044", "rc59", "t1134","sec_20_1_e","t1135","rc366","t2s101","t106","t2054","t183","t2s89"];

/* Forms existing but picked up by script */
var existingForms = ["t2s427","t2s346","t2s72","t2s500","t2s341","t2s71"];

function main() {
  var efileJson = fs.readFileSync(fileToCompare, 'utf8');
  var corpTaxJson = fs.readFileSync(corpTaxFile, 'utf8');

  var efileTns = JSON.parse(efileJson);
  var corpTaxTns = JSON.parse(corpTaxJson);

  var diffs = {};
  for (var formId in efileTns) {
    var corptaxForm = formId;
    var ignore = {};
    if (isEfile) {
      if (efileCWMapping[formId]) {
        corptaxForm = efileCWMapping[formId];
      }
      if (formId.startsWith('t2s')) {
        // If the form is t2s3, we want to ignore field '003'
        ignore[padStr(formId.substr(3), 3)] = true;
      }
    }
    var result = compare(corpTaxTns[formId], efileTns[formId], mockFns.extend(ignore, exceptions[formId]));
    if (result.length) {
      diffs[corptaxForm] = result;
    }
  }

  console.log(JSON.stringify(diffs, null, 2));

  return;
  /* Getting efile list of formIds not in corpTax */
  var efileListNotCorpTax = [];
  Object.keys(efileTns).forEach(function(formId){
    var corpTaxFormId = formId;
    if (efileCWMapping[formId])
      corpTaxFormId = efileCWMapping[formId];

    if (!corpTaxTns[corpTaxFormId])
      efileListNotCorpTax.push(formId)
  });


  /* Getting corpTax list of formIds not in efile specs */
  var corpTaxListNotEfile = [];
  Object.keys(corpTaxTns).forEach(function(formId){
    if (!efileTns[formId])
      corpTaxListNotEfile.push(formId)
  });


  /* Getting list of all missing tns of non-mismatching forms */
  var missingCorpTaxTns = {};

  Object.keys(efileTns).forEach(function(formId){
    var corpTaxFormId = formId;
    if (efileCWMapping[formId])
      corpTaxFormId = efileCWMapping[formId];

    if (!corpTaxTns[corpTaxFormId])
      return;

    var formTns = efileTns[formId];

    var corpTaxFormTns = corpTaxTns[corpTaxFormId].list;

    missingCorpTaxTns[corpTaxFormId] = [];

    if (formTns.length){
      formTns.forEach(function(tn){
        if(corpTaxFormTns.indexOf(tn) == -1){
          missingCorpTaxTns[corpTaxFormId].push(tn);
        }
      });
    }
  });


  /* Remove all empty arrays and Gifi forms from list of missing tns */
  var realList = {};
  for (var formId in missingCorpTaxTns){
    if (['t2s100', 't2s125', 't2s140'].indexOf(formId) != -1)
      continue;

    var formNum = null;
    if (formId.indexOf('t2s') == 0) {
      formNum = formId.substring(3);
      if (formNum.length == 1){
        formNum = '00' + formNum;
      }
      else if (formNum.length == 2) {
        formNum = '0' + formNum;
      }
    }

    if (formNum && missingCorpTaxTns[formId][0] == formNum) {
      missingCorpTaxTns[formId].splice(0, 1);
    }

    if (!missingCorpTaxTns[formId].length)
      continue;

    realList[formId] = missingCorpTaxTns[formId];
  }


  /* Getting list of efile tns not in CorpTax */

  var corpTaxEfileMapping = reverseObject(efileCWMapping);

  var missingEfileTns = {};

  Object.keys(corpTaxTns).forEach(function(formId){
    var efileFormId = formId
    if (corpTaxEfileMapping [formId])
      efileFormId = corpTaxEfileMapping [formId];

    if (!efileTns[efileFormId])
      return;

    var formTns = corpTaxTns[formId].list;
    var efileFormTns = efileTns[efileFormId];

    missingEfileTns[efileFormId] = [];

    if (formTns.length){
      formTns.forEach(function(tn){
        if(efileFormTns .indexOf(tn) == -1){
          missingEfileTns[efileFormId].push(tn);
        }
      });
    }
  });

  var realListMissingEfileTns = {};
  for (var formId in missingEfileTns){
    if (['t2s100', 't2s125', 't2s140'].indexOf(formId) != -1)
      continue;

    if (!missingEfileTns[formId].length)
      continue;

    realListMissingEfileTns[formId] = missingEfileTns[formId];
  }

  //fs.writeFileSync(outputFileName, JSON.stringify(realListMissingEfileTns, null, 2));
  console.log(JSON.stringify(realList, null, 2));
  //console.log(JSON.stringify(realListMissingEfileTns, null, 2));
}

/**
 * Pads the str to the desired length by prepending 0.
 **/
function padStr(str, length) {
  var diff = length - str.length

  // if no padding is required
  if (diff <= 0) {
    return str;
  }
  // else return padded string
  return new Array(diff + 1).join('0') + str;
}

function compare(form1, form2, ignore) {
  if (!form1) {
    // if form1 doesn't contain anything then it is missing everything in form2, but we still need to exclude the
    // nums listed as exceptions.
    return showAll ? form2.filter(num => !ignore[num]) : [];
  }

  var ret = {};
  for (var i = 0; i < form2.length; i++) {
    if (ignore[form2[i]]) {
      continue;
    }
    // check if form1 doesn't contain this number from form2
    if (form1.indexOf(form2[i]) == -1) {
      ret[form2[i]] = true;
    }
  }
  return Object.keys(ret);
}

function reverseObject(array) {
  var store = {};
  for (var num in array) {
    store[array[num]] = num;
  }
  return store;
}

init();
