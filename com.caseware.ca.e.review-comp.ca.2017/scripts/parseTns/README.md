### parseTns.js ###
parseTns retrieves all the tns from CorpTax. it does this by setuping up an
environment that imitates the CorpTax runtime environment by mocking functions
and creating the actual formdata objects.

It makes use of formParser to parse through each formdata and obtain the list
of tn numbers that can be used to compare with CRA specs.
```
Usage: node parseTns.js
used to retrieve all the tns from corptax and output to tns/corpTaxTns.json
```

### parseEfileWorkchart.js ###
```
Usage: node parseEfileWorkchart.js
used to retrieve all the tns from efile workchart and output to tns/workchartTns.json
```


### compareTns.js ###
```
Usage: node compareTns.js [OPTION]... FILE1 FILE2
Prints the tns that are in FILE2 but not in FILE1.

Options:
        -x FILE Do not list anything listed inside the FILE
        -e      Set this option if FILE2 represents E-FILE tns
        -a      List all differences including the forms that
                exist in FILE2 but not FILE1
```
