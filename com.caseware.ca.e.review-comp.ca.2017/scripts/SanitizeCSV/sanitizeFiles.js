
var fs = require('fs');
var csv = require('fast-csv');
var path = require('path');
var mapping = JSON.parse(fs.readFileSync('mapping.json'));
var gifiMapping = JSON.parse(fs.readFileSync('gifiMapping.json'));

//helper function to strip out [x] sequences in taxprep field identifiers
function stripTaxprepBrackets(field) {
  var index = field ? field.indexOf('[') : -1;
  if (index === -1) {
    return field;
  }

  while (index !== -1) {
    field = field.slice(0, index) + field.slice(index + 3, field.length);
    index = field.indexOf('[');
  }
  return field;
}

var transformRow = function(row) {
  var strippedTaxprepId = stripTaxprepBrackets(row[0]);
  var mappingObj = mapping[strippedTaxprepId];
  var gifiMappingObj = gifiMapping[strippedTaxprepId];

  if (gifiMappingObj) {
    return row; //Don't sanitize if taxprepId is part of GIFI
  }

  if (Array.isArray(mappingObj)) {
    //Get first item of array if mappingObj has multiple mappings
    //For sanitize purposes they should all be equivalent
    mappingObj = mappingObj[0];
  }

  if ((mappingObj && (mappingObj.isSanitized || isEmptyOrNull(mappingObj.corpTaxNum))) || !mappingObj) {
    row[1] = '';
    row[2] = '';
  }
  return row;
};

var isNullUndefined = function(value) {
  return value === undefined || value === null;
};

var isEmptyOrNull = function(value) {
  return isNullUndefined(value) || value.toString().trim() === '';
};

var inputDirectoryArg = __dirname + '/input';
var outputDirectoryArg = __dirname + '/output';
var anonOutputDirectoryArg = __dirname + '/output_anonymized';
var fileList = fs.readdirSync(inputDirectoryArg);
// var fileList = ['test.csv'];
fileList.forEach(function(fileName, index) {
  if (!fileName.endsWith('.csv')) {
    return;
  }
  var filePath = path.join(inputDirectoryArg, fileName);

  //Initialize Input stream
  var fileInputStream = fs.createReadStream(filePath);

  //Init output streams
  var csvOutputStream = csv.createWriteStream({headers: true});
  var sanitizedFileName = fileName.substr(0, fileName.length - 4) + '-sanitized-' + index + '.csv';
  var anonymizedFileName = 'anonymized-' + index + '.csv';
  var fileOutputStream = fs.createWriteStream(path.join(outputDirectoryArg, sanitizedFileName));
  var fileOutputStreamAnon = fs.createWriteStream(path.join(anonOutputDirectoryArg, anonymizedFileName));
  csvOutputStream.pipe(fileOutputStream);
  csvOutputStream.pipe(fileOutputStreamAnon);
  fileOutputStream.on("finish", function(){
    console.log("Finished generating %s", sanitizedFileName);
  });

  //Stream in csv data
  csv.fromStream(fileInputStream).transform(function(data) {
    return transformRow(data);
  })
  .on('data', function(data) {
    if (!isEmptyOrNull(data[1])) {
      csvOutputStream.write(data);
    }
  })
  .on('end', function() {
    csvOutputStream.end();
  });
});