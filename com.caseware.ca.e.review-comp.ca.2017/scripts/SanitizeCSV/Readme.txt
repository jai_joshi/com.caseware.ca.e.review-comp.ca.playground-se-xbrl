This script is for the express purpose of sanitizing sensitive data in your Taxprep .CSV export files



TO USE:

1) Ensure that Node.js is installed on your computer

2) Place all the Taxprep .CSV files you want to sanitize inside the SanitizeCSV/input folder

3) Open the run.bat file. Press Enter to confirm

4) The script runs, producing:

- Sanitized .CSV files in the /output folder as {name}-sanitized-{index}.csv

- Duplicates of the sanitized .CSV files in the /output_anonymized folder as anonymized-{index}.csv