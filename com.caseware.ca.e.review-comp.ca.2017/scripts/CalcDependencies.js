// Windows Script Host program to scan a CorpTax form folder, parsing all
// calculation files to determine external form cell references
//
// Usage: CalcDependencies.js <formFolderPath>
//
// formFolderPath - folder where CorpTax form files reside - if not supplied, the default directory is used

debugger;

var objArgs = WScript.Arguments;
if (WScript.Arguments.length < 0) {
  WScript.Echo("Usage: ConvertForm.js formFolderPath");
  WScript.Quit(1);
}

if (!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "");
  };
}

if (!String.prototype.trimAllSpaces) {
  String.prototype.trimAllSpaces = function () {
    return this.replace(/\s/g, "");
  };
}

if (!String.prototype.stripComments) {
  String.prototype.stripComments = function () {
    var iComment = this.indexOf("//");
    return iComment >= 0 ? this.substr(0, iComment) : this;
  };
}

if (!String.prototype.camelCase) {
  String.prototype.camelCase = function () {
    var pieces = this.split("-");
    var sret = pieces[0];
    for (var i = 1; i < pieces.length; i++) {
      if (pieces[i].length)
        sret = sret + pieces[i].substr(0, 1).toUpperCase() + pieces[i].substr(1);
    }
    return sret;
  };
}

if (!String.prototype.cmpAt) {
  String.prototype.cmpAt = function (pos, str) {
    return this.substr(pos, str.length) == str ? 0 : -1;
  };
}

try {
  var htmlfile = WSH.CreateObject('htmlfile'), JSON;
  htmlfile.write('<meta http-equiv="x-ua-compatible" content="IE=9" />');
  htmlfile.close(JSON = htmlfile.parentWindow.JSON);

  var WshShell = WScript.CreateObject("WScript.Shell");
  var sPath = WScript.Arguments.length > 0 ? WScript.Arguments(0) : WshShell.CurrentDirectory;
  var bOverWrite = WScript.Arguments.length >= 2 ? WScript.Arguments(1) : false;

  // make sure path given is valid
  var fso = new ActiveXObject("Scripting.FileSystemObject");
  var folder = fso.GetFolder(sPath);

  // recursively find all form folders by searching for form.json
  var folderList = new oFolderList(folder.Path, "form.json", []);
  var fileExclusions = ["optimizationworkchart-tables.js"];

  var calcFiles = [/-calculations\.js/i, /-formdata\.js/i, /-tables\.js/i];

  var globalDependencies = new oDependencies(JSON);

  for (var i = 0; i < folderList.folders.length; i++) {
    // parent folder can have different name - search for a controller
    var folder = fso.GetFolder(folderList.folders[i].path);
    var files = new Enumerator(folder.files);
    for (; !files.atEnd() ; files.moveNext()) {
      for (var j = 0; j < calcFiles.length; j++) {
        if (files.item().Name.search(calcFiles[j]) != -1) {
          var bExclude = false;
          for (var k = 0; k < fileExclusions.length; k++) {
            if (fileExclusions[k] == files.item().Name.toLowerCase()) {
              bExclude = true;
              break;
            }
          }
          if (bExclude)
            continue;

          var iDelimiter = files.item().Name.lastIndexOf("-");
          if (iDelimiter > 0) {
            var sFormID = files.item().Name.substr(0, iDelimiter).toLowerCase();
            var fileDependencies = new oFileDependencies({ "path": files.item().Path, "id": sFormID });
            globalDependencies.addRefs(sFormID, fileDependencies.externalRefs);
          }
        }
      }
    }
  }
  var sJSON = globalDependencies.toJSONString();

  var sOutputFile = "Dependencies.json";
  var FOR_WRITING = 2;
  var fJSON = new oFile(sOutputFile, FOR_WRITING);
  fJSON.writeFile([sJSON]);
}

catch (e) {
  if (e)
    WScript.Echo("Error: " + e.message);

  WScript.Quit(1);
}

WScript.Echo(sPath + " scanned with output \"" + sOutputFile + "\"");


//////////////////////////////

function oDependencies(JSON) {
  try {
    // Properties
    this.JSON = JSON;
    this.dependencies = new ActiveXObject("Scripting.Dictionary");
    this.cells = new ActiveXObject("Scripting.Dictionary");

    // Methods
    this.addRefs = function (formID, refs) {
      if (!refs || !refs.Count)
        return;

      var sID = formID;
      var formDict = this.dependencies(sID);
      if (!formDict)
        formDict = new ActiveXObject("Scripting.Dictionary");

      var keys = (new VBArray(refs.Keys())).toArray();
      for (var i in keys) {
        if (keys[i] != sID) {
          formDict(keys[i]) = null;

          var cells = refs(keys[i]);
          if (cells && cells.Count > 0) {
            var myCells = this.cells(keys[i]);
            if (typeof (myCells) == "undefined")
              myCells = new ActiveXObject("Scripting.Dictionary");

            var cellKeys = (new VBArray(cells.Keys())).toArray();
            for (var j in cellKeys)
              myCells(cellKeys[j]) = null;

            this.cells(keys[i]) = myCells;
          }

        }
      }

      if (formDict.Count > 0)
        this.dependencies(sID) = formDict;
    }

    this.toJSONString = function () {
      var dependents = new Object();
      dependents.forms = [];
      var forms = (new VBArray(this.dependencies.Keys())).toArray();
      for (var i in forms) {
        var form = new Object();
        form.name = forms[i];
        form.extRefs = [];
        var oRefs = this.dependencies(forms[i]);
        if (!oRefs)
          continue;

        var refs = (new VBArray(oRefs.Keys())).toArray();
        for (var j in refs)
          form.extRefs[form.extRefs.length] = refs[j];

        dependents.forms[dependents.forms.length] = form;
      }

      dependents.cells = [];
      var cells = (new VBArray(this.cells.Keys())).toArray();
      for (var i in cells) {
        var form = new Object();
        form.name = cells[i];
        form.cells = [];
        var oRefs = this.cells(cells[i]);
        if (!oRefs)
          continue;

        var refs = (new VBArray(oRefs.Keys())).toArray();
        for (var j in refs)
          form.cells[form.cells.length] = refs[j];

        dependents.cells[dependents.cells.length] = form;
      }

      return this.JSON.stringify(dependents, "", "\t");
    }

    // Constructor
  }

  catch (e) {
    if (e)
      throw new Error(102, "oDependencies: " + e);
    throw e;
  }
}

//////////////////////////////

function oFileDependencies(params) {
  try {
    if (!params.path || params.path.constructor != String)
      throw "Must supply calculation file";

    // Properties
    this.path = params.path;
    this.formID = params.id.toLowerCase();
    this.externalRefs = new ActiveXObject("Scripting.Dictionary");
    this.line = null;

    // Methods
    this.scanFile = function () {
      var fCalc = new oFile(this.path);
      var lines = fCalc.readFile({ "join": "true", "stripComments": "true" });
      for (var i = 0; i < lines.length; i++) {
        this.parseLine(lines[i]);
      }
    };

    this.parseLine = function (line) {
      this.line = new oLine(line);

      // references to wpw.tax.actions. can be assumed to be using values from "CP"
      var sFunctActions = "wpw.tax.actions.";
      for (this.line.indexOf(sFunctActions) ; this.line.iPos >= 0; this.line.indexOf(sFunctActions)) {
        this.line.advance(sFunctActions.length);
        var sToken = this.line.getToken();
        if (sToken == "ismultiplejurisdiction" || sToken == "getprovincesofjurisdiction")
          this.addRef("cp", "prov_residence");
        else if (sToken == "iswithinfiscalyear" || sToken == "calculatedaysdifference") {
          this.addRef("cp", "tax_start");
          this.addRef("cp", "tax_end");
        }
      }

      // form ID is the 2nd parameter of getGlobalValue() or setRepeatSummaryValue()
      var functs = [".getglobalvalue(", ".setrepeatsummaryvalue("];
      for (var ii = 0; ii < functs.length; ii++) {
        var sFunct = functs[ii];
        for (this.line.indexOf(sFunct) ; this.line.iPos >= 0; this.line.indexOf(sFunct)) {
          var bNextArg = true;
          if (ii == 0) {
            var iActions = this.line.iPos - sFunctActions.length + 1;
            if (iActions >= 0 && this.line.sLine.cmpAt(iActions, sFunctActions) == 0)
              bNextArg = false;
          }

          this.line.advance(sFunct.length);
          if (bNextArg) {
            if (this.line.nextArg() == -1) {
              this.errMess(sFunct + " missing param #2 in: " + this.formID + " - \"" + line + "\"");
              continue;
            }
          }

          var sRef = null;
          if (!this.line.quoteChar()) {
            var param2 = this.line.getToken();
            var formVars = ["repeatformids", "ccaworkchartlinkedformid", "sourcefullformid", "t2s13wlinkedformid", "rentalformid",
              "s125formids", "s125repeatforms", "s53formids", "s21wformids"];
            var bFound = false;
            for (var i = 0; i < formVars.length; i++) {
              if (param2.substr(0, formVars[i].length) == formVars[i]) {
                bFound = true;
                if (this.formID == "rentalswcsummary")
                  sRef = "t2rentalworkchart";
                else if (this.formID == "rentalswcsummarytotal")
                  sRef = "rentalswcsummary";
                else if (this.formID == "t2rentalworkchart")
                  sRef = "t2s8w";
                else if (this.formID == "t2s5")
                  sRef = "t2s21w";
                else if (this.formID == "t2s1" || this.formID == "t2s6" || this.formID == "t2s7" || this.formID == "t2s8rec" || this.formID == "t2s140")
                  sRef = "t2s125";
                else if (this.formID == "t2s8add")
                  sRef = "t2s8w";
                else if (this.formID == "t2s8w") {
                  if (formVars[i] == "rentalformid")
                    sRef = "t2rentalworkchart";
                  else if (formVars[i] == "repeatformids")
                    sRef = "t2s8add";
                  else if (formVars[i] == "ccaworkchartlinkedformid")
                    sRef = "t2s8w";
                }
                else if (this.formID == "t2s10" || this.formID == "t2s10add")
                  sRef = "t2s10w";
                else if (this.formID == "t2s10w")
                  sRef = "t2s10add";
                else if (this.formID == "t2s13" || this.formID == "t2s13w")
                  sRef = "t2s13w";
                else if (this.formID == "t2s55")
                  sRef = "t2s53";

                break;
              }
            }

            if (!bFound) {
              this.errMess(sFunct + " param #2 not quoted in: " + this.formID + " - \"" + line + "\"");
              continue;
            }

            if (!sRef) {
              this.errMess(sFunct + " param #2 '" + formVars[i] + "' not found in: " + this.formID + " - \"" + line + "\"");
              continue;
            }

            // 3rd argument is the form cell number
            var sCell = this.getNextArg(3);
            this.addRef(sRef, sCell);
            continue;
          }

          sRef = this.line.getToken();
          if (sRef == "") {
            this.errMess(sFunct + " param #2 missing in: " + this.formID + " - \"" + line + "\"");
            continue;
          }

          // 3rd argument is the form cell number
          var sCell = this.getNextArg(3);
          this.addRef(sRef, sCell);
        }
      }

      // linkedRepeatForm: has the external form reference right after it, i.e. linkedRepeatForm: 'advertisingWorkchart'
      // or the external form reference is part of an object with id: as the form name, i.e.
      // linkedRepeatForm: { id: 'automobileInterest', filter: {'340': {compareValue: '0', operation: 'greaterThanOrEquals'}}

      var sFunct = "linkedrepeatform";
      for (this.line.indexOf(sFunct) ; this.line.iPos >= 0; this.line.indexOf(sFunct)) {
        if (this.line.iPos > 0 && this.line.quoteChar(this.line.iPos - 1))
          this.line.advance(-1);

        this.line.getToken();
        if (this.line.charAt() != ":")
          continue;

        this.line.advance(1);
        var sRef = this.line.getToken("id");
        if (sRef == "") {
          this.errMess("linkedRepeatForm missing external form reference in: " + this.formID + " - \"" + line + "\"");
          continue;
        }

        this.addRef(sRef);
      }

      // linkedExternalTable: has the external form reference as part of an object with formId: as the form name,
      // and fieldId: as the cell. Takes an array, consistent with linkedTable property.
      //  i.e. linkedExternalTable: [{formId: 't2s430', fieldId: '1000'}]

      var sFunct = "linkedexternaltable";
      for (this.line.indexOf(sFunct) ; this.line.iPos >= 0; this.line.indexOf(sFunct)) {
        if (this.line.iPos > 0 && this.line.quoteChar(this.line.iPos - 1))
          this.line.advance(-1);

        this.line.getToken();
        if (this.line.charAt() != ":")
          continue;

        this.line.advance(1);
        var sRef = this.line.getToken("formid");
        if (sRef == "") {
          this.errMess("linkedExternalTable missing external form reference in: " + this.formID + " - \"" + line + "\"");
          continue;
        }

        // 3rd argument is the form cell number
        var sCell = this.line.getToken("fieldid");
        this.addRef(sRef, sCell);
      }

      // calcUtils.form(formId).field(cellNo)

      var sFunct = "calcutils.form(";
      for (this.line.indexOf(sFunct) ; this.line.iPos >= 0; this.line.indexOf(sFunct)) {
        this.line.advance(sFunct.length);
        var sRef = this.line.getToken();
        if (this.line.charAt() != ")") {
          this.errMess("calcutils.form() right bracket expected in: " + this.formID + " - \"" + line + "\"");
          continue;
        }

        var sCell = null;
        this.line.advance(1);
        var sCmp = ".field(";
        if (this.line.cmpAt(sCmp) == 0) {
          this.line.advance(sCmp.length);
          sCell = this.line.getToken();
          if (this.line.charAt() != ")") {
            this.errMess("calcutils.form().field() right bracket expected in: " + this.formID + " - \"" + line + "\"");
            continue;
          }
        }

        this.addRef(sRef, sCell);
      }

      var sFunct = "field(";
      for (this.line.indexOf(sFunct) ; this.line.iPos >= 0; this.line.indexOf(sFunct)) {
        this.line.advance(sFunct.length);
        var sRef, sCell = this.line.getToken();
        var dotPos = sCell.indexOf('.');
        if (dotPos != -1) {
          sRef = sCell.substr(0, dotPos);
          sCell = sCell.substr(dotPos + 1);

          this.addRef(sRef, sCell);
        }
      }
    };

    this.getNextArg = function (argNum) {
      if (this.line.nextArg() != -1) {
        var sArg = this.line.getToken("fieldid");
        if (sArg != "")
          return sArg;
      }

      this.errMess(" param" + (typeof (argNum) == "undefined" ? "" : argNum.toString()) + " missing in: " + this.formID + " - \"" + this.line.text + "\"");
      return "";
    }

    this.addRef = function (formID, cellNo) {
      if (formID.length && formID != this.formID) {
        var cells = this.externalRefs(formID);
        if (typeof (cells) == "undefined")
          cells = new ActiveXObject("Scripting.Dictionary");

        if (cellNo && cellNo.length)
          cells(cellNo) = null;

        this.externalRefs(formID) = cells;
      }
    }

    this.errMess = function (sErr) {
      WScript.Echo(sErr);
    }

    // Constructor
    this.scanFile(); // scan each line of the file for dependencies
  }

  catch (e) {
    if (e)
      throw new Error(102, "oFileDependencies: " + e);
    throw e;
  }
}

//////////////////////////////

function oFolderList(path, filespec, exclusions) {
  try {
    // Properties
    this.folders = new Array();
    this.filespec = filespec;
    this.excl = new ActiveXObject("Scripting.Dictionary");

    // Methods
    this.traverse = function (path) {
      var fso = new ActiveXObject("Scripting.FileSystemObject");
      var folder = fso.GetFolder(path);
      if (this.excl.Exists(folder.Name.toLowerCase()))
        return;

      if (fso.FileExists(folder.Path + "\\" + this.filespec) && folder.ParentFolder)
        this.folders.push({ "path": folder.Path });

      // traverse any folders within this one
      var subFolders = new Enumerator(folder.subFolders);
      for (; !subFolders.atEnd() ; subFolders.moveNext())
        this.traverse(subFolders.item().Path);
    };

    // Constructor
    if (exclusions && exclusions.constructor == Array) {
      for (var i = 0; i < exclusions.length; i++)
        this.excl.add(exclusions[i].toLowerCase(), null);
    }
    this.traverse(path);
  }

  catch (e) {
    if (e)
      throw new Error(103, "oFolderList: " + e);
    throw e;
  }
}

//////////////////////////////

function oFile(path, mode) {
  try {
    // Properties
    this.sPath = path;
    this.TextStream = null;

    // Methods
    this.openFile = function (openMode) {
      if (!path)
        throw "Must supply name of form";

      var FOR_READING = 1;
      var FOR_WRITING = 2;
      var TRISTATE_USE_DEFAULT = -2;

      var fso = new ActiveXObject("Scripting.FileSystemObject");
      if (openMode && openMode == FOR_WRITING) {
        if (fso.FileExists(path))
          fso.DeleteFile(path);

        this.TextStream = fso.CreateTextFile(path, true);
      }
      else {
        var fTextFile = fso.GetFile(path);
        this.TextStream = fTextFile.OpenAsTextStream(openMode ? openMode : FOR_READING, TRISTATE_USE_DEFAULT);
      }
    };

    this.readLine = function () {
      return this.TextStream.AtEndOfStream ? null : this.TextStream.ReadLine();
    };

    this.readFile = function (args) {
      var bStripComments = args && args.stripComments ? true : false;
      var bJoin = args && args.join ? true : false;

      this.openFile();
      var lines = new Array();
      var text;
      while ((text = this.readLine()) != null) {
        if (bStripComments)
          text = text.stripComments();

        if (bJoin) {
          var text = text.trim();
          var line = text;
          while (!line.length || line.charAt(line.length - 1) != ";") {
            if ((line = this.readLine()) == null)
              break;

            if (bStripComments)
              line = line.stripComments();

            line = line.trim();
            text += line;
          }
        }
        lines.push(text);
      }

      this.closeFile();
      return lines;
    };

    this.writeFile = function (lines) {
      var FOR_WRITING = 2;
      this.openFile(FOR_WRITING);
      for (var i = 0; i < lines.length; i++)
        this.TextStream.writeLine(lines[i]);

      this.closeFile();
      return lines;
    };

    this.closeFile = function () {
      if (!this.TextStream) return;

      this.TextStream.Close();
      this.TextStream = null;
    };

    // Constructor
    var FOR_WRITING = 2;
    if (!mode || mode != FOR_WRITING) {
      this.openFile();
      this.closeFile();
    }
  }

  catch (e) {
    throw new Error(100, "oFile " + path + ": " + e);
  }
}

//////////////////////////////

function oLine(line) {
  try {
    // Properties
    this.text = line;
    this.sLine = this.text.trimAllSpaces().toLowerCase();
    this.iPos = 0;

    // Methods
    this.getToken = function (sProp) {
      if (this.iPos >= this.sLine.length)
        return "";

      if (typeof (sProp) != "undefined" && (this.charAt() == "{" || this.charAt() == ","))
        return this.getProp(sProp) != -1 ? this.getToken() : "";

      var quote = this.quoteChar();
      if (quote)
        this.iPos++;

      var iToken = this.iPos;
      while (this.iPos < this.sLine.length) {
        var myChar = this.charAt();
        if (quote) {
          if (this.charAt() == quote)
            break;
        }
        else if (!((myChar >= "a" && myChar <= "z") || (myChar >= "0" && myChar <= "9") || myChar == "-" || myChar == "_"))
          break;

        this.iPos++;

      }
      var iPos = this.iPos;
      if (quote && this.iPos < this.sLine.length)
        this.iPos++;

      return this.sLine.substr(iToken, iPos - iToken);
    }

    this.getProp = function (sID) {
      if (this.charAt() == "{" || this.charAt() == ",")
        this.advance(1);

      var sToken = this.getToken(sID);
      if (sToken != sID || this.charAt() != ":")
        return -1;

      return this.advance(1);
    }

    this.nextArg = function () {
      if (this.charAt() == "{") {
        this.iPos = this.sLine.indexOf("}", this.iPos + 1);
        if (this.iPos == -1)
          return -1;
      }

      var nBrackets = 0;
      while (this.iPos < this.sLine.length) {
        var myChar = this.charAt();
        if (myChar == ")") {
          if (nBrackets > 0)
            nBrackets--;
          else
            break;
        }
        else if (myChar == "(")
          nBrackets++;
        else if (myChar == ",")
          return this.advance(1);

        this.iPos++;
      }
      return -1;
    }

    this.quoteChar = function (iPos) {
      var myChar = this.charAt(iPos);
      return myChar == "'" || myChar == "\"" ? myChar : null;
    }

    this.charAt = function (iPos) {
      return this.sLine.charAt(typeof(iPos) == "undefined" ? this.iPos : iPos);
    }

    this.cmpAt = function (sCmp, iPos) {
      return this.sLine.cmpAt(typeof (iPos) == "undefined" ? this.iPos : iPos, sCmp);
    }

    this.indexOf = function (str) {
      this.iPos = this.sLine.indexOf(str, this.iPos);
      return this.iPos;
    }

    this.advance = function (nChars){
      this.iPos += nChars;
      return this.iPos;
    }

    // Constructor
  }

  catch (e) {
    throw new Error(100, "oLine " + this.sLine + ": " + e);
  }
}

//////////////////////////////