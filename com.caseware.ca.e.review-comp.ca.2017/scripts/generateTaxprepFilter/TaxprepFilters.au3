;Make sure the Xpress filter window is at the bottom left corner of the Tax Prep window
;Make sure the Xpress filter is closed in Tax Prep before running



#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>
#include <WinAPI.au3>
AutoItSetOption ("SendKeyDelay",300)
Opt("WinTitleMatchMode",2)
Global $counter = 1

openTaxPrep()
For $i = 1 To 35 Step +1
   addForm($i)
Next
saveFilter()
openXpress()
For $i = 1 To 35 Step +1
   addForm($i)
Next
saveFilter()
openXpress()
For $i = 1 To 41 Step +1
   addForm($i)
Next
saveFilter()
Sleep(2000)
Send("{ESC}")
Send("{ESC}")
For $i = 1 To 115 Step +1
   Send("{DOWN}")
Next
Send("{ENTER}")
openXpress()
For $i = 1 To 35 Step +1
   addForm($i)
Next
saveFilter()
openXpress()
For $i = 1 To 35 Step +1
   addForm($i)
Next
saveFilter()
openXpress()
For $i = 1 To 35 Step +1
   addForm($i)
Next
saveFilter()
openXpress()
For $i = 1 To 36 Step +1
   addForm($i)
Next
saveFilter()
MsgBox($MB_SYSTEMMODAL, "Error", "MADE IT TO THE END")
Exit

;Select all cells and add to grid
Func addForm($timeCheck)
   Sleep(2000)
   Send("+^{F7}")
   MouseClick($MOUSE_CLICK_LEFT, 90, 1111, 1)
   If ($timeCheck < 32) Then
     Sleep(3000)
   Else
     Sleep(15000)
   EndIf
   Send("{ESC}")
   Send("{ESC}")
   Send("{ESC}")
   Sleep(1000)
   Send("{DOWN}")
   Sleep(1000)
   Send("{ENTER}")
   checkExceptions()
EndFunc

;Opens taxprep and prepares the xpress filter
Func openTaxPrep()
   Send("{LWIN}")
   Send("taxprep")
   Send("{ENTER}")
   Sleep(5000)
   WinActivate("Corporate Taxprep")
   If(StringInStr(WinGetTitle(""), "Corporate Taxprep") = 0) Then
     Exit
   EndIf
   Send("^{n}")
   Send("^{F5}")
   MouseClick($MOUSE_CLICK_RIGHT, 874, 1030, 1)
   Send("{E}")
   Send("{N}")
EndFunc

;Saves the xpress filter
Func saveFilter()
   Send("^{F5}")
   Send("{SPACE}")
   Send("comprehensive"&$counter)
   $counter+=1
   Send("{TAB}")
   Send("{SPACE}")
   Sleep(20000)
EndFunc

Func openXpress()
   Send("^{F5}")
   Sleep(1000)
   MouseClick($MOUSE_CLICK_RIGHT, 874, 1030, 1)
   Sleep(1000)
   Send("{E}")
   Send("{N}")
EndFunc

;Catches expected pop ups and other errors
Func checkExceptions()
   If(WinGetTitle("[ACTIVE]") = "Create Link Wizard") Then
     Send("{TAB}")
     Send("{SPACE 2}")
   EndIf
   If(WinGetTitle("[ACTIVE]") = "Notice") Then
     Send("{SPACE}")
     Send("{ESC}")
     Send("{DOWN}")
     Send("{ENTER}")
   EndIf
   If(WinGetTitle("[ACTIVE]") = "Wolters Kluwer CCH - Error Message") Then
     ;MsgBox($MB_SYSTEMMODAL, "Error", "Script failed due to Taxprep Error.")
     ;Exit
	 Send("{TAB 2}")
	 Send("{Space}")
	 Sleep(1000)
     Send("{TAB 2}")
	 Send("{Space}")
	 Sleep(1000)
     Send("{TAB 2}")
	 Send("{Space}")
	 Sleep(1000)
	 Send("{ENTER}")
  EndIf
  If(_WinAPI_GetClassName(WinGetHandle("[ACTIVE]")) == "TCCHMessageBox") Then
	 Send("{SPACE}")
	 ;ControlSend("TCCHMessageBox", "", "[CLASS:TCCHWinButton; INSTANCE:3]", "{SPACE}")
	 Sleep(1000)
	 Send("comprehensive-atDiag-"&$counter)
	 Send("{TAB}")
	 Send("{SPACE}")
	 Sleep(20000)
	 openXpress()
   EndIf
EndFunc