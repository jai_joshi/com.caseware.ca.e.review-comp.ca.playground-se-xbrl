/**Usage:
 * need to install 'fast-csv' package in npm using: npm install fast-csv
 * put 2 cor file in the folder 'CorFiles'
 * change name1, name2 to the corresponding Software name of 2 cor file (defaul will be 2 corfile name)
 * double click RunCompareCor.bat to execute this script
 * output will be inside diffs.json which contains all different value from 2 cor file under the following format:
 * IF rowindex == undefined ---> normal inputField and not a table.
 * "formId-formSequence-fieldId-rowIndex": {
        "name1": fieldValue1,
        "name2": fieldValue1
      }
 * if either fieldValue1 or fieldValue2 is undefined, it will only show name1 or name2 like below:
 * if fieldValue2 is undefined
 "formId-formSequence-fieldId-rowIndex": {
              "name1": fieldValue1
           }
 * if fieldValue1 is undefined
 "formId-formSequence-fieldId-rowIndex": {
              "name2": fieldValue1
           }
 */
var fs = require("fs");
var csv = require('fast-csv');

var dirPath = 'CorFiles/';

//those fields are always different between 2 programs: program code version.
var exceptionTn = ['200-1-099-undefined', '200-1-101-undefined'];

fs.readdir(dirPath, function(err, testFolders) {
  testFolders.forEach(function(testFolder) {
    var curFolder = dirPath + testFolder + '/';
    fs.readdir(curFolder, function(err, items) {
      var corItems = [];
      var paths = [];
      for (var i = 0; i < items.length && paths.length < 2; i++) {
        if (/\.cor$/i.test(items[i])) {
          corItems.push(items[i])
          paths.push(curFolder + items[i]);
        }
      }
      if (paths.length < 2) {
        console.log('Not enough cor files! Skipping', curFolder);
        return;
      }
      var path1 = paths[0];
      var path2 = paths[1];
      //define the name for 2 cor file: for example: taxprep, Cra, TaxCycle, corpTax, etc...but default will be the corfile name
      var name1 = corItems[0].substr(0, corItems[0].indexOf('.'));
      var name2 = corItems[1].substr(0, corItems[1].indexOf('.'));
      var separatorStringReg = '\u001d';

      var cor1Data = [];
      var cor2Data = [];

      var output1 = {};
      var output2 = {};
      var differentValue;

      var formData;

      fs.createReadStream(path1)
        .pipe(csv())
        .on('data', function(data) {
          // cor1Data.push(data)
          formData = reformatCorFile(data, output1, formData);
        })
        .on('end', afterReadCor1);
      //This function is executed after we finished reading first cor file
      function afterReadCor1() {
        // reformatCorFile(cor1Data, output1);
        //read corTax.cor file
        fs.createReadStream(path2)
          .pipe(csv())
          .on('data', function(data) {
            // cor2Data.push(data)
            formData = reformatCorFile(data, output2, formData);
          })
          .on('end', afterReadCor2);
      }

      //This function is executed after we finished reading corTax.cor
      function afterReadCor2() {
        // reformatCorFile(cor2Data, output2);
        //return diff
        differentValue = returnDiff(output1, output2, name1, name2);
        fs.writeFile(curFolder + 'diffs.json', JSON.stringify(differentValue, null, 2), function() {
          console.log("The json file was saved!");
        });
      }

      function reformatCorFile(data, output, formData) {
        data = String(data);
        formData = formData || {};

        var matchLine = data.match(separatorStringReg);
        var formId = formData.formId;
        var formSequence = formData.sequence;
        var fieldId = undefined;
        var fieldValue = undefined;
        var rowIndex = undefined;

        //for formHeader or repeated table
        if (matchLine !== null) {
          var index = matchLine.index;
          var input = matchLine.input;
          //this is header
          if (index == 3) {
            formId = input.substring(0, 3);
            formSequence = input.substring(4, input.length);
          }
          //this is repeated table
          else {
            fieldId = input.substring(0, 3);
            fieldValue = input.substring(3, index);
            rowIndex = input.substring(index + 1, input.length);
          }
        }
        else {
          //for GIFI forms
          if (['100', '125', '140'].indexOf(formId) !== -1) {
            fieldId = data.substring(0, 4);
            fieldValue = data.substring(4, data.length);
          }
          //for other forms
          else {
            fieldId = data.substring(0, 3);
            fieldValue = data.substring(3, data.length);
          }
        }
        var param = formId + "-" + formSequence + "-" + fieldId + "-" + rowIndex;
        output[param] = fieldValue;

        return {
          formId: formId,
          sequence: formSequence
        };
      }

      //this function compare 2 outputJson then return diff value
      function returnDiff(obj1, obj2, name1, name2) {
        var diffs = {}, key;

        for (key in obj1) {
          if(~exceptionTn.indexOf(key)) {
            continue;
          }
          typeof obj1[key] == 'string' ? obj1[key] = obj1[key].toUpperCase() : null;
          typeof obj2[key] == 'string' ? obj2[key] = obj2[key].toUpperCase() : null;
          //check if key is also in obj2
          //check for value
          if (obj1[key] !== obj2[key]) {
            diffs[key] = {};
            diffs[key][name1] = obj1[key];
            diffs[key][name2] = obj2[key];
          }
        }

        for (key in obj2) {
          if(~exceptionTn.indexOf(key)) {
            continue;
          }
          if (!(key in obj1)) {
            diffs[key] = {};
            diffs[key][name1] = obj1[key];
            diffs[key][name2] = obj2[key];
          }
        }
        return diffs;
      }

    });
  })

});
