/**Usage:
 * need to install 'fast-csv' package in npm using: npm install fast-csv
 * put 2 cor file in the folder 'CorFiles' 
 * change name1, name2 to the corresponding Software name of 2 cor file (defaul will be 2 corfile name)
 * double click RunCompareCor.bat to execute this script
 * output will be inside diffs.json which contains all different value from 2 cor file under the following format:
     * IF rowindex == undefined ---> normal inputField and not a table.
     * "formId-formSequence-fieldId-rowIndex": {
        "name1": fieldValue1,
        "name2": fieldValue1
      }
 * if either fieldValue1 or fieldValue2 is undefined, it will only show name1 or name2 like below:
     * if fieldValue2 is undefined
           "formId-formSequence-fieldId-rowIndex": {
              "name1": fieldValue1
           }
     * if fieldValue1 is undefined
           "formId-formSequence-fieldId-rowIndex": {
              "name2": fieldValue1
           }
 */