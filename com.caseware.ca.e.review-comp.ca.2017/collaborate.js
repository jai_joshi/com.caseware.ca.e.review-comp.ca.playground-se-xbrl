var CAImpl = function () {
  this.id = "25145a50-a04f-444a-ac92-ea6b1ec3f194";

  function notContact() {
    return !CWI.ProfileManager.get().isContact();
  }

  this.canOpen = notContact();
  this.canCreate = notContact();
  this.canDelete = notContact();

  this.openInNewTab = true;
  this.supportsQueries = true;

  this.firmSettingsPages = [{
      "heading": "Manage Template 2017",
      "anchorId": "1",
      "url": "../e/template.jsp?t=" + this.id + "&s=com.caseware.ca.e.review-comp.ca.2017"
  }];

  this.gridIconSVG = "../e/prod/com.caseware.ca.e.review-comp.ca.2017/icon.svg";
  this.doormatSVG = "../e/prod/com.caseware.ca.e.review-comp.ca.2017/icon.svg";

  return this;
};

// CAImpl @extends CollaborateImpl
CAImpl.prototype = new CollaborateImpl();
collaborate.addModel(new CAImpl());
