(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

    var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
    var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
    var MINUS = wpw.analysis.AnalysisFormula.FORMULAS.minus;
    var MULTIPLY = wpw.analysis.AnalysisFormula.FORMULAS.multiply;
    var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
    var CONSTANT = wpw.analysis.AnalysisFormula.FORMULAS.constant;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
 //None

  /********************* Formulas ends **********************/
  var investmentDef = new wpw.analysis.AnalysisDefinition();
  investmentDef.id = 'J2G7R956QzyKi8Mw58XZXw';
  investmentDef.name = 'Investments';

   // Accounts
  var component = new ComponentDefinition('rJZ3_iAxzg', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.breakdownGroupNumbers = ['113','131']; // Short term investments, Long term investments
  component.allowEdit = false;
  investmentDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(investmentDef);
})(wpw);
