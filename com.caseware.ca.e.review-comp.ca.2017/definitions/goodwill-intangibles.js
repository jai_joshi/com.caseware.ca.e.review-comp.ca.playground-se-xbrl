(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
  var OTHER_INTANGIBLE_ASSETS_COST_FORMULA  = TAG('171');
  var OTHER_INTANGIBLE_ASSETS_AMORTIZATION_IMPAIRMENT_FORMULA  = TAG('172');
  var IMPAIRMENT_OF_GOODWILL_FORMULA = TAG('514');
  var AMORTIZATION_OF_OTHER_INTANGIBLE_ASSETS_FORMULA = TAG('513.100');
  var IMPAIRMENT_OF_OTHER_INTANGIBLE_ASSETS_FORMULA = TAG('513.200');

  var AMORTIZATION_EXPENSE_TO_GROSS_INTANGIBLES_FORMULA = DIVIDE(AMORTIZATION_OF_OTHER_INTANGIBLE_ASSETS_FORMULA, 
    OTHER_INTANGIBLE_ASSETS_COST_FORMULA);
  var ACCUMULATED_AMORTIZATION_TO_INTANGIBLES_FORMULA = DIVIDE(OTHER_INTANGIBLE_ASSETS_AMORTIZATION_IMPAIRMENT_FORMULA, 
    OTHER_INTANGIBLE_ASSETS_COST_FORMULA);

  /********************* Formulas end ***********************/

  var goodwillDef = new wpw.analysis.AnalysisDefinition();
  goodwillDef.id = 'g7gArgPLSpSwoXcYeHqBHQ';
  goodwillDef.name = 'Goodwill and intangibles';

  // Overview
  var OTHER_INTANGIBLE_ASSETS_COST = new KPIModel('COST', 'Other intangible assets - Cost', DATA_TYPE.MONETARY, 
        true, null, OTHER_INTANGIBLE_ASSETS_COST_FORMULA);
  var OTHER_INTANGIBLE_ASSETS_AMORTIZATION_IMPAIRMENT = new KPIModel('AAR', 'Other intangible assets - Accumulated amortization and impairment ',
        DATA_TYPE.MONETARY, true, null, OTHER_INTANGIBLE_ASSETS_AMORTIZATION_IMPAIRMENT_FORMULA);
  var IMPAIRMENT_OF_GOODWILL = new KPIModel('IMPAIRMENT_OF_GOODWILL', 'Impairment of goodwill', DATA_TYPE.MONETARY, true, null,
  IMPAIRMENT_OF_GOODWILL_FORMULA);
  var AMORTIZATION_OF_OTHER_INTANGIBLE_ASSETS = new KPIModel('AMORTIZATION_OF_OTHER_INTANGIBLE_ASSETS',
        'Amortization of other intangible assets', DATA_TYPE.MONETARY, true, null,
        AMORTIZATION_OF_OTHER_INTANGIBLE_ASSETS_FORMULA);
  var IMPAIRMENT_OF_OTHER_INTANGIBLE_ASSETS = new KPIModel('IMPAIRMENT_OF_OTHER_INTANGIBLE_ASSETS', 'Impairment of other intangible assets', 
        DATA_TYPE.MONETARY, true, null, IMPAIRMENT_OF_OTHER_INTANGIBLE_ASSETS_FORMULA);

  var componentModels = [OTHER_INTANGIBLE_ASSETS_COST, OTHER_INTANGIBLE_ASSETS_AMORTIZATION_IMPAIRMENT, IMPAIRMENT_OF_GOODWILL, 
    AMORTIZATION_OF_OTHER_INTANGIBLE_ASSETS, IMPAIRMENT_OF_OTHER_INTANGIBLE_ASSETS];
  var component = new ComponentDefinition('S1mV9FHha', 'Overview', COMPONENT_TYPES.CHART,
      CHART_TYPES.COLUMN, wpw.analysis.modelsToKeys(componentModels));
  goodwillDef.kpiModels = goodwillDef.kpiModels.concat(componentModels);
  goodwillDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('SJXnCozfMl', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.allowEdit = false;
  component.breakdownGroupNumbers = ['170', '173'];
  goodwillDef.componentDefinitions.push(component);

  // Ratios
  var AMORTIZATION_EXPENSE_TO_GROSS_INTANGIBLES = new KPIModel('AMORTIZATION_EXPENSE_TO_GROSS_INTANGIBLES', 'Amortization expense to gross intangibles',
      DATA_TYPE.PERCENTAGE, false, null, AMORTIZATION_EXPENSE_TO_GROSS_INTANGIBLES_FORMULA, 'Amortization of other intangible assets / Intangibles - Cost');
  var ACCUMULATED_AMORTIZATION_TO_INTANGIBLES = new KPIModel('ACCUMULATED_AMORTIZATION_TO_INTANGIBLES', 'Accumulated amortization to intangibles',
      DATA_TYPE.PERCENTAGE, false, null, ACCUMULATED_AMORTIZATION_TO_INTANGIBLES_FORMULA, 'Accumulated amortization of intangible assets / Intangibles - Cost');
 
  componentModels = [AMORTIZATION_EXPENSE_TO_GROSS_INTANGIBLES, ACCUMULATED_AMORTIZATION_TO_INTANGIBLES];
  component = new ComponentDefinition('rkEVcYS3T', 'Ratios', COMPONENT_TYPES.TABLE, null, 
        wpw.analysis.modelsToKeys(componentModels));
  goodwillDef.kpiModels = goodwillDef.kpiModels.concat(componentModels);
  goodwillDef.componentDefinitions.push(component);


  // Detailed - Intangible Assets Costs
  goodwillDef.rootTagsToTraverse[OTHER_INTANGIBLE_ASSETS_COST_FORMULA.tagNumber] = OTHER_INTANGIBLE_ASSETS_COST.key;
  component = new ComponentDefinition('rJ8V9FB3a', 'Intangible Assets Costs',
     COMPONENT_TYPES.CHART,  CHART_TYPES.STACK, [], OTHER_INTANGIBLE_ASSETS_COST.key, false, true);
  component.extraGroupsToLoad.push({key: OTHER_INTANGIBLE_ASSETS_COST.key});
  goodwillDef.componentDefinitions.push(component);

  // Detailed - Intangible Assets - Accumulated Depreciation and Impairment
  goodwillDef.rootTagsToTraverse[OTHER_INTANGIBLE_ASSETS_AMORTIZATION_IMPAIRMENT_FORMULA.tagNumber] = OTHER_INTANGIBLE_ASSETS_AMORTIZATION_IMPAIRMENT.key;
  component = new ComponentDefinition('H1HE5tB2T',
      'Intangible Assets - Accumulated Depreciation and Impairment', COMPONENT_TYPES.CHART,
      CHART_TYPES.STACK, [], OTHER_INTANGIBLE_ASSETS_AMORTIZATION_IMPAIRMENT.key, false, true);
  component.extraGroupsToLoad.push({key: OTHER_INTANGIBLE_ASSETS_AMORTIZATION_IMPAIRMENT.key});
  goodwillDef.componentDefinitions.push(component);

  goodwillDef.matchGroupSettings.push(new wpw.analysis.MatchGroupSetting([OTHER_INTANGIBLE_ASSETS_COST_FORMULA.tagNumber,
    OTHER_INTANGIBLE_ASSETS_AMORTIZATION_IMPAIRMENT_FORMULA.tagNumber]));

  wpw.injectedAnalysisDefinitions.push(goodwillDef);
})(wpw);
