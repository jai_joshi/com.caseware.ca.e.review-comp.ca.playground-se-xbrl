(function() {
  'use strict';
  if (!wpw.tax.codes)
    wpw.tax.codes = {};

  wpw.tax.codes.scienceTechCodes = {
    'Mathematics': {
      '1.01.01': 'Pure mathematics',
      '1.01.02': 'Applied mathematics',
      '1.01.03': 'Statistics and probability'
    },
    'Computer and Information Sciences': {
      '1.02.01': 'Computer sciences',
      '1.02.02': 'Information technology and bioinformatics (Software engineering and technology under 2.02.09)' +
      ' (hardware development under 2.02.08)'
    },
    'Physical Sciences': {
      '1.03.01': 'Atomic, molecular and chemical physics',
      '1.03.02': 'Interaction with radiation',
      '1.03.03': 'Magnetic resonances',
      '1.03.04': 'Condensed matter physics',
      '1.03.05': 'Solid state physics & superconductivity',
      '1.03.06': 'Particles and fields physics',
      '1.03.07': 'Nuclear physics',
      '1.03.08': 'Fluids and plasma physics (including surface physics)',
      '1.03.09': 'Optics (including laser optics and quantum optics)',
      '1.03.10': 'Acoustics',
      '1.03.11': 'Astronomy (including astrophysics, space science)'
    },
    'Chemical Sciences': {
      '1.04.01': 'Organic chemistry',
      '1.04.02': 'Inorganic and nuclear chemistry',
      '1.04.03': 'Physical chemistry, polymer science & plastics',
      '1.04.04': 'Electrochemistry (dry cells, batteries, fuel cells, metal corrosion, electrolysis)',
      '1.04.05': 'Colloid chemistry',
      '1.04.06': 'Analytical chemistry'
    },
    'Earth and related Environmental sciences': {
      '1.05.01': 'Geosciences, multidisciplinary',
      '1.05.02': 'Mineralogy & paleontology',
      '1.05.03': 'Geochemistry & geophysics',
      '1.05.04': 'Physical geography',
      '1.05.05': 'Geology & volcanology',
      '1.05.06': 'Environmental sciences',
      '1.05.07': 'Meteorology, atmospheric sciences & climatic research',
      '1.05.08': 'Oceanography, hydrology & water resources'
    },
    'Biological sciences': {
      '1.06.01': 'Cell biology, microbiology & virology',
      '1.06.02': 'Biochemistry, molecular biology & Biochemical research',
      '1.06.03': 'Mycology',
      '1.06.04': 'Biophysics',
      '1.06.05': 'Genetics and heredity (medical genetics under code 3)',
      '1.06.06': 'Reproductive biology (medical aspects under code 3)',
      '1.06.07': 'Developmental biology',
      '1.06.08': 'Plant sciences & botany',
      '1.06.09': 'Zoology, ornithology, entomology & Behavioural sciences biology',
      '1.06.10': 'Marine biology, freshwater biology & limnology',
      '1.06.11': 'Ecology & biodiversity conservation',
      '1.06.12': 'Biology (theoretical, thermal, cryobiology, chronobiology)',
      '1.06.13': 'Evolutionary biology'
    },
    'Other natural sciences': {
      '1.07.01': 'Other natural sciences'
    },
    'Civil engineering': {
      '2.01.01': 'Civil engineering',
      '2.01.02': 'Architecture engineering',
      '2.01.03': 'Municipal and structural engineering',
      '2.01.04': 'Transport engineering'
    },
    'Electrical engineering, Electronic engineering & Information technology': {
      '2.02.01': 'Electrical and electronic engineering',
      '2.02.02': 'Robotics and automatic control',
      '2.02.03': 'Micro-electronics',
      '2.02.04': 'Semiconductors',
      '2.02.05': 'Automation and control systems',
      '2.02.06': 'Communication engineering and systems',
      '2.02.07': 'Telecommunications',
      '2.02.08': 'Computer hardware and architecture',
      '2.02.09': 'Software engineering and technology'
    },
    'Mechanical engineering': {
      '2.03.01': 'Mechanical engineering',
      '2.03.02': 'Applied mechanics',
      '2.03.03': 'Thermodynamics',
      '2.03.04': 'Aerospace engineering',
      '2.03.05': 'Nuclear related engineering (nuclear physics under 1.03.07)',
      '2.03.06': 'Acoustical engineering',
      '2.03.07': 'Reliability analysis and non-destructive testing',
      '2.03.08': 'Automotive and transportation engineering and manufacturing',
      '2.03.09': 'Tooling, machinery and equipment engineering and manufacturing',
      '2.03.10': 'Heating, ventilation and Air conditioning engineering and manufacturing'
    },
    'Chemical engineering': {
      '2.04.01': 'Chemical engineering (plants, products)',
      '2.04.02': 'Chemical process engineering'
    },
    'Materials engineering': {
      '2.05.01': 'Materials engineering & metallurgy',
      '2.05.02': 'Ceramics',
      '2.05.03': 'Coating and films (including packaging and printing)',
      '2.05.04': 'Plastics, Rubber and Composites (including laminates and reinforced plastics)',
      '2.05.05': 'Paper and wood & textiles',
      '2.05.06': 'Construction materials (organic and inorganic)'
    },
    'Medical engineering': {
      '2.06.01': 'Medical and biomedical engineering',
      '2.06.02': 'Medical laboratory technology (Biomaterials under 2.09.05)'
    },
    'Environmental engineering': {
      '2.07.01': 'Environmental and geological engineering',
      '2.07.02': 'Petroleum engineering (fuel, oils)',
      '2.07.03': 'Energy and fuels',
      '2.07.04': 'Remote sensing',
      '2.07.05': 'Mining and mineral processing',
      '2.07.06': 'Marine engineering, sea vessels & ocean engineering'
    },
    'Environmental biotechnology': {
      '2.08.01': 'Environmental biotechnology',
      '2.08.02': 'Bioremediation',
      '2.08.03': 'Diagnostic biotechnologies in environmental management (DNA chips & biosensing devices)'
    },
    'Industrial biotechnology': {
      '2.09.01': 'Industrial biotechnology',
      '2.09.02': 'Bioprocessing technologies',
      '2.09.03': 'Biocatalysis & fermentation',
      '2.09.04': 'Bioproducts (products that are manufactured using biological material as feedstock)',
      '2.09.05': 'Biomaterials (bioplastics, biofuels, bioderived bulk and fine chemicals, bio-derived materials)'
    },
    'Nano-technology': {
      '2.10.01': 'Nano-materials (production and properties)',
      '2.10.02': 'Nano-processes (applications on nano-scale)'
    },
    'Other engineering and technologies': {
      '2.11.01': 'Food and beverages',
      '2.11.02': 'Oenology',
      '2.11.03': 'Other engineering and technologies'
    },
    'Basic medecine': {
      '3.01.01': 'Anatomy & morphology (plant science under 1.06.08)',
      '3.01.02': 'Human genetics',
      '3.01.03': 'Immunology',
      '3.01.04': 'Neurosciences',
      '3.01.05': 'Phamacology and pharmacy & medicinal chemistry',
      '3.01.06': 'Toxicology',
      '3.01.07': 'Physiology & cytology',
      '3.01.08': 'Pathology'
    },
    'Clinical medecine': {
      '3.02.01': 'Andrology',
      '3.02.02': 'Obstetrics and gynaecology',
      '3.02.03': 'Paediatrics',
      '3.02.04': 'Cardiac and cardiovascular systems',
      '3.02.05': 'Hematology',
      '3.02.06': 'Anaesthesiology',
      '3.02.07': 'Orthopaedics',
      '3.02.08': 'Radiology & nuclear medicine',
      '3.02.09': 'Dentistry, oral surgery and medicine',
      '3.02.10': 'Dermatology, venereal diseases & allergy',
      '3.02.11': 'Rheumatology',
      '3.02.12': 'Endocrinology and metabolism & gastroenterology',
      '3.02.13': 'Urology and nephrology',
      '3.02.14': 'Oncology'
    },
    'Health sciences': {
      '3.03.01': 'Health care sciences & nursing',
      '3.03.02': 'Nutrition & dietetics',
      '3.03.03': 'Parasitology',
      '3.03.04': 'Infectious diseases & epidemiology',
      '3.03.05': 'Occupational health'
    },
    'Medical biotechnology': {
      '3.04.01': 'Health-related biotechnology',
      '3.04.02': 'Technologies involving the manipulation of cells, tissues, organs or the whole organism',
      '3.04.03': 'Technologies involving identifying the functioning of DNA, proteins and enzymes',
      '3.04.04': 'Pharmacogenomics, gene-based therapeutics',
      '3.04.05': 'Biomaterials (related to medical implants, devices, sensors)'
    },
    'Other medical sciences': {
      '3.05.01': 'Forensic science',
      '3.05.02': 'Other medical sciences'
    },
    'Agriculture, Forestry and Fisheries': {
      '4.01.01': 'Agriculture',
      '4.01.02': 'Forestry',
      '4.01.03': 'Fisheries and Aquaculture',
      '4.01.04': 'Soil science',
      '4.01.05': 'Horticulture',
      '4.01.06': 'Viticulture',
      '4.01.07': 'Agronomy',
      '4.01.08': 'Plant breeding and plant protection (Agricultural biotechnology under 4.04.01)'
    },
    'Animal and Dairy science': {
      '4.02.01': 'Animal and dairy science (Animal biotechnology under 4.04.01)',
      '4.02.02': 'Animal husbandry'
    },
    'Veterinary science': {
      '4.03.01': 'Veterinary science (all)'
    },
    'Agricultural biotechnology': {
      '4.04.01': 'Agricultural biotechnology & food biotechnology',
      '4.04.02': 'Genetically Modified (GM) organism technology & livestock cloning',
      '4.04.03': 'Diagnostics (DNA chips and biosensing devices)',
      '4.04.04': 'Biomass feedstock production technologies',
      '4.04.05': 'Biopharming'
    },
    'Other agricultural sciences': {
      '4.05.01': 'Other agricultural sciences'
    }
  };
}());
