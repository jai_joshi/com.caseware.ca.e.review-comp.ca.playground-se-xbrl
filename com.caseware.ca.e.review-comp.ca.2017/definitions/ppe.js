(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var MINUS = wpw.analysis.AnalysisFormula.FORMULAS.minus;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /************ Formulas *********/
  //Accounts
  var PPE_FORMULA = TAG('145')
  //Overview
  var PPE_COST_FORMULA = TAG('150');
  var ACCUMULATED_AMORT_PPE_FORMULA = TAG('160');
  var RM_FORMULA = TAG('535');
  var GAIN_LOSS_DISPOSITION_FORMULA = TAG('605');
  //Ratios
  var RM_TO_GROSS_DEP_PPE_FORMULA = DIVIDE(RM_FORMULA, PPE_COST_FORMULA);
  var DEP_EXP_TO_GROSS_DEP_PPE_FORMULA = DIVIDE(TAG('521'), PPE_COST_FORMULA);
  var ACCUMULATED_DEP_PPE_FORMULA = DIVIDE(ACCUMULATED_AMORT_PPE_FORMULA, PPE_COST_FORMULA);
  var GROSS_PPE_TO_TOTAL_ASSETS_FORMULA = DIVIDE(PPE_COST_FORMULA,TAG('100'));
  var FIXED_ASSETS_TURNOVER_FORMULA = DIVIDE(TAG('300'),PPE_FORMULA);
  var BOOK_VALUE_RATIO_FORMULA = DIVIDE(PPE_FORMULA, PPE_COST_FORMULA);
  
  /********* Formulas end ********/
  var ppeDefinition = new wpw.analysis.AnalysisDefinition();
  ppeDefinition.id = 'U4dYPIziSpG37Zp1sY5JHw';
  ppeDefinition.name = 'Property, plant and equipment';

  //wpw.analysis.modelsToKeys(componentModels)

  // Overview
 
  var PPE_COST = new KPIModel('PPE_COST', 'Property, plant and equipment - cost', DATA_TYPE.MONETARY, true, null, PPE_COST_FORMULA);
  var ACCUMULATED_AMORT_PPE = new KPIModel('ACCUMULATED_AMORT_PPE', 'Accumulated amortization of property plant and equipment', DATA_TYPE.MONETARY, true,
      null, ACCUMULATED_AMORT_PPE_FORMULA);
  var RM = new KPIModel('RM', 'R&M expense', DATA_TYPE.MONETARY, true, null, RM_FORMULA);
  var GAIN_LOSS_DISPOSITION = new KPIModel('GAIN_LOSS_DISPOSITION', 'Realized gain on disposal of assets', DATA_TYPE.MONETARY,
      true, null, GAIN_LOSS_DISPOSITION_FORMULA); 
  var componentModels = [PPE_COST,ACCUMULATED_AMORT_PPE,RM,GAIN_LOSS_DISPOSITION];
  var component = new ComponentDefinition('rytN8YB2p', 'Overview', COMPONENT_TYPES.CHART,
      CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
  ppeDefinition.kpiModels = ppeDefinition.kpiModels.concat(componentModels);
  ppeDefinition.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('HkQUJglbGe', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.allowEdit = false;
  component.breakdownGroupNumbers = ['145'];
  ppeDefinition.componentDefinitions.push(component);

  // Ratio
  var RM_TO_GROSS_DEP_PPE = new KPIModel('RM_TO_GROSS_DEP_PPE', 'Repairs & maintenance / Gross depreciable PPE',
    DATA_TYPE.PERCENTAGE, false, null, RM_TO_GROSS_DEP_PPE_FORMULA, 'Repairs & maintenance / Gross depreciable PPE',
      'Expresses the relationship of repairs and maintenance to gross property and equipment. Percentages outside of ' +
      'normal ranges could indicate classification errors in amounts capitalised. High percentages can indicate ' +
      'older property and equipment with needs replacement.');
  var DEP_EXP_TO_GROSS_DEP_PPE = new KPIModel('DEP_EXP_TO_GROSS_DEP_PPE',
    'Depreciation expense / Gross depreciable PPE', DATA_TYPE.PERCENTAGE, false, null,
    DEP_EXP_TO_GROSS_DEP_PPE_FORMULA, 'Depreciation expense / Gross depreciable PPE', 'Measures the rate at which ' +
      'productive asset costs are allocated to operations.');
  var ACCUMULATED_DEP_PPE = new KPIModel('ACCUMULATED_DEP_PPE', 'Accumulated depreciation / Gross depreciable PPE',
    DATA_TYPE.PERCENTAGE, false, null, ACCUMULATED_DEP_PPE_FORMULA, 'Accumulated depreciation / Gross depreciable PPE',
      'Shows the cumulative percentage of productive asset costs allocated to operations. A high percentage usually ' +
      'indicates older property and equipment. Significant capital investment may be required in future periods.');
  var GROSS_PPE_TO_TOTAL_ASSETS = new KPIModel('GROSS_PPE_TO_TOTAL_ASSETS', 'Gross PPE / Total Assets',
    DATA_TYPE.PERCENTAGE, false, null, GROSS_PPE_TO_TOTAL_ASSETS_FORMULA, 'Gross PPE / Total Assets');
  var FIXED_ASSETS_TURNOVER = new KPIModel('FIXED_ASSETS_TURNOVER', 'Fixed assets turnover', DATA_TYPE.NUMBER,
      false, null, FIXED_ASSETS_TURNOVER_FORMULA, 'Sales/Period-end PPE (this is Net PPE ie. Cost - A/Dep at year end)');
  FIXED_ASSETS_TURNOVER.numberOfDecimals = 2;
  var BOOK_VALUE_RATIO = new KPIModel('BOOK_VALUE_RATIO', 'Book value ratio', DATA_TYPE.PERCENTAGE, false, null,
    BOOK_VALUE_RATIO_FORMULA, 'Net PPE / Cost PPE');
  componentModels = [RM_TO_GROSS_DEP_PPE, DEP_EXP_TO_GROSS_DEP_PPE, ACCUMULATED_DEP_PPE, GROSS_PPE_TO_TOTAL_ASSETS,
    FIXED_ASSETS_TURNOVER, BOOK_VALUE_RATIO];
  ppeDefinition.kpiModels = ppeDefinition.kpiModels.concat(componentModels);
  component = new ComponentDefinition('B1jTJjSn6', 'Ratios', COMPONENT_TYPES.TABLE, null,
    wpw.analysis.modelsToKeys(componentModels));
  ppeDefinition.componentDefinitions.push(component);

  //PPE cost - top five
  var LAND = new KPIModel('LAND', 'Land', DATA_TYPE.MONETARY, true, null, TAG('151'));
  var DEPLETABLE_ASSETS = new KPIModel('DEPLETABLE_ASSETS', 'Depletable assets', DATA_TYPE.MONETARY, true, null, TAG('153'));
  var BUILDINGS = new KPIModel('BUILDINGS', 'Buildings', DATA_TYPE.MONETARY, true, null, TAG('155'));
  var MACHINERY_EQUIP_FF = new KPIModel('MACHINERY_EQUIP_FF', 'Machinery, equipment, furniture and fixtures', DATA_TYPE.MONETARY, true, null, TAG('157'));
  var OTHER_TANGIBLE_CAP_ASSETS = new KPIModel('OTHER_TANGIBLE_CAP_ASSETS', 'Other tangible capital assets', DATA_TYPE.MONETARY, true, null, TAG('159'));
  
  componentModels = [LAND, DEPLETABLE_ASSETS,BUILDINGS,MACHINERY_EQUIP_FF,OTHER_TANGIBLE_CAP_ASSETS];
  component = new ComponentDefinition('H1C6kiH2a', 'PPE Costs', COMPONENT_TYPES.STACK,
      CHART_TYPES.COLUMN, wpw.analysis.modelsToKeys(componentModels));
  component.groupKeysInStack('PPE_COST', [LAND, DEPLETABLE_ASSETS,BUILDINGS,MACHINERY_EQUIP_FF,OTHER_TANGIBLE_CAP_ASSETS], false, 1);
  ppeDefinition.addUniqueKpiModels(componentModels);
  ppeDefinition.componentDefinitions.push(component);

  //PPE A/Dep - top five
  var A_LAND_IMPROVEMENTS = new KPIModel('A_LAND_IMPROVEMENTS', 'Accumulated amortization of land improvements', DATA_TYPE.MONETARY, true, null, TAG('161'));
  var A_DEPLETABLE_ASSETS = new KPIModel('A_DEPLETABLE_ASSETS', 'Accumulated amortization of depletable assets', DATA_TYPE.MONETARY, true, null, TAG('163'));
  var A_BUILDINGS = new KPIModel('A_BUILDINGS', 'Accumulated amortization of buildings', DATA_TYPE.MONETARY, true, null, TAG('165'));
  var A_MACHINERY_EQUIP_FF = new KPIModel('A_MACHINERY_EQUIP_FF', 'Accumulated amortization of machinery, equipment, furniture and fixtures', DATA_TYPE.MONETARY, true, null, TAG('167'));
  var A_OTHER_TANGIBLE_CAP_ASSETS = new KPIModel('A_OTHER_TANGIBLE_CAP_ASSETS', 'Accumulated amortization of other tangible capital assets', DATA_TYPE.MONETARY, true, null, TAG('169'));
  
  componentModels = [A_LAND_IMPROVEMENTS,A_DEPLETABLE_ASSETS,A_BUILDINGS,A_MACHINERY_EQUIP_FF,A_OTHER_TANGIBLE_CAP_ASSETS];
  component = new ComponentDefinition('p1CQ8JK2a', 'PPE - Accumulated Depreciation and Impairment', COMPONENT_TYPES.STACK,
      CHART_TYPES.COLUMN, wpw.analysis.modelsToKeys(componentModels));
  component.groupKeysInStack('PPE_ADEP', [A_LAND_IMPROVEMENTS,A_DEPLETABLE_ASSETS,A_BUILDINGS,A_MACHINERY_EQUIP_FF,A_OTHER_TANGIBLE_CAP_ASSETS], false, 1);
  ppeDefinition.addUniqueKpiModels(componentModels);
  ppeDefinition.componentDefinitions.push(component);



  wpw.injectedAnalysisDefinitions.push(ppeDefinition);
})(wpw);
