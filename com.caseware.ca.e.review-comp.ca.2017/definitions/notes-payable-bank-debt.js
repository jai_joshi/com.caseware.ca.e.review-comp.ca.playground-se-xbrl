(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  var analysisDef = new wpw.analysis.AnalysisDefinition();
  analysisDef.id = 'hj8q0fvZT02Dx7qkVfOOQw';
  analysisDef.name = 'Notes payable and bank debt';


  var SHORT_TERM_DEBT = new KPIModel('SHORT_TERM_DEBT', 'Short term debt', DATA_TYPE.MONETARY, true, null,
  TAG('213'));
  var component = new ComponentDefinition('HJTyiS2T', 'Overview', COMPONENT_TYPES.CHART,
      CHART_TYPES.COLUMN, [SHORT_TERM_DEBT.key]);
  analysisDef.addUniqueKpiModels([SHORT_TERM_DEBT]);
  analysisDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('H12Aofffl', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.allowEdit = false;
  component.breakdownGroupNumbers = ['213'];
  analysisDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);
