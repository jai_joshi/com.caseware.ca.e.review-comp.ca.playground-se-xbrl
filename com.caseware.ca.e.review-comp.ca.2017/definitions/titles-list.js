/**
 * Created by bucky.nguyen on 10/8/2015.
 */
(function () {
  'use strict';
  if (!wpw.tax.codes)
    wpw.tax.codes = {};

  wpw.tax.codes.titleList =
  {
    '1': {value: 'Mr'},
    '2': {value: 'Mrs'}
  }
}());
