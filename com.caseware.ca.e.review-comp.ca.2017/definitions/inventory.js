(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

    var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
    var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
    var MINUS = wpw.analysis.AnalysisFormula.FORMULAS.minus;
    var MULTIPLY = wpw.analysis.AnalysisFormula.FORMULAS.multiply;
    var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
    var CONSTANT = wpw.analysis.AnalysisFormula.FORMULAS.constant;

    var DATA_TYPE = wpw.analysis.DataType;
    var COMPONENT_TYPES = wpw.analysis.ComponentType;
    var CHART_TYPES = wpw.analysis.ChartType;
    var KPIModel = wpw.analysis.KPIModel;
    var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
    //Accounts
    var INVENTORY_FORMULA = TAG('125');
  
    //Overview
    var SALES_FORMULA = TAG('300');
    var COS_FORMULA = TAG('400');
    var GROSS_MARGIN_FORMULA = MINUS(SALES_FORMULA, COS_FORMULA);
    var ACCOUNTS_PAYABLE_FORMULA = TAG('215');
  
  //Ratios
    var INVENTORY_TURNOVER_FORMULA = DIVIDE(COS_FORMULA, INVENTORY_FORMULA);
    var DAYS_COS_INVENTORY_FORMULA = DIVIDE(MULTIPLY(INVENTORY_FORMULA, CONSTANT(365)), COS_FORMULA);
    var OPERATING_CYCLE_DAYS_FORMULA = ADD(
        DIVIDE(MULTIPLY(CONSTANT(365), TAG('115')), SALES_FORMULA), // days receivables
        DAYS_COS_INVENTORY_FORMULA);
    var INVENTORY_SALES_RATIO_FORMULA = DIVIDE(INVENTORY_FORMULA, SALES_FORMULA);
    var INVENTORY_GROWTH_FORMULA = DIVIDE(MINUS(TAG(INVENTORY_FORMULA), TAG(INVENTORY_FORMULA, -1)), TAG(INVENTORY_FORMULA, -1));
    var INVENTORY_TO_WC_FORMULA = DIVIDE(TAG(INVENTORY_FORMULA), MINUS(TAG('105'),TAG('210')));
    var ACCOUNTS_PAYABLE_TO_INV = DIVIDE(TAG('215'),TAG(INVENTORY_FORMULA));


  /********************* Formulas ends **********************/

    var inventoryDef = new wpw.analysis.AnalysisDefinition();
    inventoryDef.id = 'yrFqmG9MTkKF6cOOenTebw';
    inventoryDef.name = 'Inventories';

  // Overview
    var SALES = new KPIModel('SALES', 'Sales', DATA_TYPE.MONETARY, true, null, SALES_FORMULA);
    var COS = new KPIModel('COS', 'Cost of sales', DATA_TYPE.MONETARY, true, null, COS_FORMULA);
    var GROSS_MARGIN = new KPIModel('GROSS_MARGIN', 'Gross margin', DATA_TYPE.NUMBER, false, null,
        GROSS_MARGIN_FORMULA, 'Revenue - Cost of sales');
    var ACCOUNTS_PAYABLE = new KPIModel('ACCOUNTS_PAYABLE', 'Accounts payable', DATA_TYPE.MONETARY, true, null,
        ACCOUNTS_PAYABLE_FORMULA);
    var componentModels = [SALES, COS, GROSS_MARGIN, ACCOUNTS_PAYABLE];
    var component = new ComponentDefinition('rytN9YB2T', 'Overview', COMPONENT_TYPES.CHART,
        CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
    inventoryDef.kpiModels = inventoryDef.kpiModels.concat(componentModels);
    inventoryDef.componentDefinitions.push(component);

  // Accounts
    component = new ComponentDefinition('HybLyge-ze', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
    component.breakdownGroupNumbers = [INVENTORY_FORMULA.tagNumber];
    component.allowEdit = false;
    inventoryDef.componentDefinitions.push(component);

  // Ratios
  var INVENTORY_TURNOVER = new KPIModel('INVENTORY_TURNOVER', 'Inventory turnover', DATA_TYPE.NUMBER, false, null,
    INVENTORY_TURNOVER_FORMULA, 'Cost of sales / Inventory', 'Measures the number of times inventory turns over ' +
      'during the year. High turnover can indicate better liquidity or superior merchandising. Conversely, it can ' +
      'indicate a shortage of needed inventory for the level of sales. Low turnover can indicate poor liquidity, ' +
      'overstocking, or obsolescence.');
  INVENTORY_TURNOVER.numberOfDecimals = 2;
  var DAYS_COS_INVENTORY = new KPIModel('DAYS_COS_INVENTORY', 'Days cost of sales in inventory', DATA_TYPE.NUMBER,
    false, null, DAYS_COS_INVENTORY_FORMULA, '(Inventory X 365) / Cost of Sales', 'Measures the average length of ' +
      'time units are in inventory. Helps determine if inadequate or excess inventory levels exist.');
      DAYS_COS_INVENTORY.numberOfDecimals = 2;
  var OPERATING_CYCLE_DAYS = new KPIModel('OPERATING_CYCLE_DAYS', 'Operating cycle days', DATA_TYPE.NUMBER, false,
      null, OPERATING_CYCLE_DAYS_FORMULA, 'Days Sales in Inventory / Days cost of Sales in Inventory', 'Measures the ' +
      'time it takes to convert products and services into cash. An unfavorable trend may be a leading indicator of ' +
      'future cash flow problems.');
      OPERATING_CYCLE_DAYS.numberOfDecimals = 2;
  var GROSS_MARGIN = new KPIModel('GROSS_MARGIN', 'Gross margin', DATA_TYPE.PERCENTAGE, false, null,
      GROSS_MARGIN_FORMULA, '(Revenue - Cost of sales) / Revenue');
  var INVENTORY_SALES_RATIO = new KPIModel('INVENTORY_SALES_RATIO', 'Inventory / Sales', DATA_TYPE.PERCENTAGE, false,
      null, INVENTORY_SALES_RATIO_FORMULA, 'Inventory / Sales');
  var INVENTORY_GROWTH = new KPIModel('INVENTORY_GROWTH', 'Inventory growth', DATA_TYPE.PERCENTAGE, false, null,
      INVENTORY_GROWTH_FORMULA, '');
  var INVENTORY_TO_WC = new KPIModel('INVENTORY_TO_WC', 'Inventory to working capital', DATA_TYPE.PERCENTAGE, false, null,
      INVENTORY_TO_WC_FORMULA, '');
    componentModels = [INVENTORY_TURNOVER, DAYS_COS_INVENTORY, OPERATING_CYCLE_DAYS, GROSS_MARGIN, INVENTORY_SALES_RATIO, INVENTORY_GROWTH, INVENTORY_TO_WC];
    component = new ComponentDefinition('SkqN5FrhT', 'Ratios', COMPONENT_TYPES.TABLE, null,
    wpw.analysis.modelsToKeys(componentModels));
    inventoryDef.addUniqueKpiModels(componentModels);
    inventoryDef.componentDefinitions.push(component);


    wpw.injectedAnalysisDefinitions.push(inventoryDef);
})(wpw);
