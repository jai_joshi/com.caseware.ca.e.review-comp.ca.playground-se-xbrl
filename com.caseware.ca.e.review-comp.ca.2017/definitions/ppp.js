(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var MULTIPLY = wpw.analysis.AnalysisFormula.FORMULAS.multiply;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  var CONSTANT = wpw.analysis.AnalysisFormula.FORMULAS.constant;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
  var AP_FORMULA = TAG('215');
  var SALES_FORMULA = TAG('300');  
  var COS_FORMULA = TAG('400');
  var OPERATING_EXPENSE_FORMULA = TAG('500')
  var DAYS_COS_IN_PAYABLE_FORMULA = DIVIDE(MULTIPLY(AP_FORMULA, CONSTANT(365)), COS_FORMULA);
  var AP_TO_SALES_FORMULA = DIVIDE(AP_FORMULA, SALES_FORMULA);
  var AP_TURNOVER_FORMULA = DIVIDE(COS_FORMULA, AP_FORMULA);

  /********************* Formulas ends **********************/

  var pppDef = new wpw.analysis.AnalysisDefinition();
  pppDef.id = 'WKzCjUd5QQeOPgAAE7Xajw';
  pppDef.name = 'Accounts payable and accrued liabiliies';


  //Overview
  var SALES = new KPIModel('SALES', 'Sales', DATA_TYPE.MONETARY, true, null, SALES_FORMULA);
  var COS = new KPIModel('COS', 'Cost of sales', DATA_TYPE.MONETARY, true, null, COS_FORMULA);
  var OPERATING_EXPENSE = new KPIModel('OPERATING_EXPENSE', 'Operating expenses',
      DATA_TYPE.MONETARY, true, null, OPERATING_EXPENSE_FORMULA);
  var componentModels = [SALES, COS, OPERATING_EXPENSE];
  var component = new ComponentDefinition('aVke2ls8p', 'Overview', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN,
    wpw.analysis.modelsToKeys(componentModels));
  pppDef.kpiModels = pppDef.kpiModels.concat(componentModels);
  pppDef.componentDefinitions.push(component);

  //Ratios
  var DAYS_COS_IN_PAYABLE = new KPIModel('DAYS_COS_IN_PAYABLE', 'Days cost of sales in payables', DATA_TYPE.NUMBER, false, null,
    DAYS_COS_IN_PAYABLE_FORMULA,'(Accounts payable X 365) / Cost of sales');
    DAYS_COS_IN_PAYABLE.numberOfDecimals = 2;
  var AP_TO_SALES = new KPIModel('AP_TO_SALES', 'Accounts payable to sales', DATA_TYPE.NUMBER,
    false, null, AP_TO_SALES_FORMULA,'Accounts payable / Sales');
    AP_TO_SALES.numberOfDecimals = 2;
  var AP_TURNOVER = new KPIModel('AP_TURNOVER', 'Accounts payable turnover', DATA_TYPE.NUMBER, false,
    null, AP_TURNOVER_FORMULA, 'Cost of sales / Accounts payable');
    AP_TURNOVER.numberOfDecimals = 2;
  var componentModels = [DAYS_COS_IN_PAYABLE, AP_TO_SALES, AP_TURNOVER];
  component = new ComponentDefinition('Kdlaog3kl', 'Ratios',COMPONENT_TYPES.CHART,CHART_TYPES.LINE,wpw.analysis.modelsToKeys(componentModels));
  pppDef.addUniqueKpiModels(componentModels);
  pppDef.componentDefinitions.push(component);


  // Accounts
  component = new ComponentDefinition('HJgh0ozMGg',  'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.allowEdit = false;
  component.breakdownGroupNumbers = ['215'];
  pppDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(pppDef);
})(wpw);
