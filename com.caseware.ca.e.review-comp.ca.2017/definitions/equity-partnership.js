(function() {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var MINUS = wpw.analysis.AnalysisFormula.FORMULAS.minus;
  var MULTIPLY = wpw.analysis.AnalysisFormula.FORMULAS.multiply;
  var CONSTANT = wpw.analysis.AnalysisFormula.FORMULAS.constant;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  var NET_INCOME_FORMULA = TAG('285.200');
  var SHAREHOLDERS_EQUITY_FORMULA = TAG('270');
  var RETURN_ON_INVESTMENT_FORMULA = DIVIDE(NET_INCOME_FORMULA, SHAREHOLDERS_EQUITY_FORMULA);
  var DIVIDENDS_FORMULA = TAG('285.300');
  const CONSTANT_FORMULA = 1;
  var RETENTION_FORMULA = DIVIDE(DIVIDENDS_FORMULA, NET_INCOME_FORMULA);
  var RETENTION_RATIO_FORMULA = MINUS(CONSTANT(1), RETENTION_FORMULA);
  var POTENTIAL_GROWTH_RATIO_FORMULA = MULTIPLY(RETURN_ON_INVESTMENT_FORMULA, RETENTION_RATIO_FORMULA);
  var TOTAL_CAPITAL_FORMULA = ADD(TAG('297'), TAG('298'), TAG('299'));
  var TOTAL_ASSETS_FORMULA = TAG('100');
  var SHAREHOLDERS_EQUITY_RATIO_FORMULA = DIVIDE(TOTAL_CAPITAL_FORMULA, TOTAL_ASSETS_FORMULA);

  /************ Formulas end *****/
  var equityDef = new wpw.analysis.AnalysisDefinition();
  equityDef.id = 'vgtTahp3RYq9ZaIhG5VEig';
  equityDef.name = 'Equity - Partnership';
  
  function createDetailChart(id, name, rootKey) {
    var compDef = new ComponentDefinition(id, name, COMPONENT_TYPES.CHART, CHART_TYPES.STACK, [], rootKey, true, false);
    compDef.visibilityTagNumber = rootKey
    compDef.shouldDisplay = false;
    return compDef;
  }

   // Accounts
   var componentDef = new ComponentDefinition('BJfe8EMzx', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
   componentDef.allowEdit = false;
   componentDef.breakdownGroupNumbers = ['297', '298', '299'];
   equityDef.componentDefinitions.push(componentDef);
   wpw.injectedAnalysisDefinitions.push(equityDef);

  // Ratios
  var RETURN_ON_INVESTMENT_RATIO = new KPIModel('RETURN_ON_INVESTMENT_RATIO', 'Return on Investment', DATA_TYPE.PERCENTAGE, false, null,
      RETURN_ON_INVESTMENT_FORMULA, 'Net income / Shareholders\' equity');
  var RETENTION_RATIO = new KPIModel('RETENTION_RATIO', 'Retention Ratio', DATA_TYPE.PERCENTAGE, false, null,
      RETENTION_RATIO_FORMULA, '1 - (Dividends / Net income)');
  var POTENTIAL_GROWTH_RATIO = new KPIModel('POTENTIAL_GROWTH_RATIO', 'Potential Growth Ratio', DATA_TYPE.PERCENTAGE, false, null,
      POTENTIAL_GROWTH_RATIO_FORMULA, 'Return on Investment X Retention Ratio');
  var SHAREHOLDERS_EQUITY_RATIO = new KPIModel('SHAREHOLDERS_EQUITY_RATIO', 'Shareholders Equity Ratio', DATA_TYPE.PERCENTAGE, false, null,
      SHAREHOLDERS_EQUITY_RATIO_FORMULA, 'Total Capital / Total Assets');
  
  var componentModels = [RETURN_ON_INVESTMENT_RATIO, RETENTION_RATIO, POTENTIAL_GROWTH_RATIO, SHAREHOLDERS_EQUITY_RATIO];
  var component = new ComponentDefinition('Sy_VcKr2p', 'Ratios', COMPONENT_TYPES.TABLE, null, [RETURN_ON_INVESTMENT_RATIO.key, RETENTION_RATIO.key,
    POTENTIAL_GROWTH_RATIO.key, SHAREHOLDERS_EQUITY_RATIO.key]);
  
  equityDef.addUniqueKpiModels(componentModels);
  equityDef.componentDefinitions.push(component);
  


})(wpw);
