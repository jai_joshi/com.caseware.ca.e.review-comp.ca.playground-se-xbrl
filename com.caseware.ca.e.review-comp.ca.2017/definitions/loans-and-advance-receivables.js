(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
  var LOANS_AND_ADVR_FORMULA  = TAG('118.100');
  var ALLOWANCE_DOUBTFUL_ACCOUNTS_FORMULA = TAG('118.200');
  
  /********************* Formulas ends **********************/

  var loansAndAdvancesDef = new wpw.analysis.AnalysisDefinition();
  loansAndAdvancesDef.id = 'zIePYfZFTGGNOE4H3NhxPQ';
  loansAndAdvancesDef.name = 'Loans and advances receivable';

  // Overview
  var LOANS_AND_ADVR = new KPIModel('LOANS_AND_ADVR', 'Loans and advances receivable', DATA_TYPE.MONETARY,
      true, null, LOANS_AND_ADVR_FORMULA);
  var ALLOWANCE_DOUBTFUL_ACCOUNTS = new KPIModel('ALLOWANCE_DOUBTFUL_ACCOUNTS', 'Allowances for doubtful loans and advances receivables',
      DATA_TYPE.MONETARY, true, null, ALLOWANCE_DOUBTFUL_ACCOUNTS_FORMULA);
      var componentModels = [LOANS_AND_ADVR, ALLOWANCE_DOUBTFUL_ACCOUNTS];
  var component = new ComponentDefinition('BkHWiPfl0', 'Overview', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, wpw.analysis.modelsToKeys(componentModels));
  loansAndAdvancesDef.addUniqueKpiModels(componentModels);
  loansAndAdvancesDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('S1g8yeeWGx', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.breakdownGroupNumbers = ['118', '121','135', '123', '137', '127', '136','133'];
  component.allowEdit = false;
  loansAndAdvancesDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(loansAndAdvancesDef);
})(wpw);
