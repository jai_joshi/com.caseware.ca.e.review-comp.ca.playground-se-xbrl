(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var MINUS = wpw.analysis.AnalysisFormula.FORMULAS.minus;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /************ Formulas *********/
  var NET_INCOME_FORMULA = TAG('285.200');
  var INCOME_TAX_EXPENSE_FORMULA = TAG('860')
  var EFFECTIVE_TAX_RATE_FORMULA = DIVIDE(TAG('860'), ADD(MINUS(MINUS(TAG('300'),TAG('400')), TAG('500')), TAG('600')));
  /*********** Formulas End ******/

  var incomeTaxDef = new wpw.analysis.AnalysisDefinition();
  incomeTaxDef.id = 'iQRAKC9aQQqz2OE4gQJyDQ';
  incomeTaxDef.name = 'Income taxes';

  // Overview
  var NET_INCOME = new KPIModel('NET_INCOME', 'Net income (loss)', DATA_TYPE.MONETARY, true, null, NET_INCOME_FORMULA);
  var componentModels = [NET_INCOME];
  var component = new ComponentDefinition('rJwE5FH3a', 'Overview', COMPONENT_TYPES.CHART,
      CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
  incomeTaxDef.addUniqueKpiModels(componentModels);
  incomeTaxDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('SkIJelbzg', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.breakdownGroupNumbers = ['217', '860', '116', '129', '174', '227', '250', '128.300'];
  component.allowEdit = false;
  incomeTaxDef.componentDefinitions.push(component);

  // Ratios
  var EFFECTIVE_TAX_RATE = new KPIModel('EFFECTIVE_TAX_RATE', 'Effective tax rate', DATA_TYPE.PERCENTAGE, false, null,
      EFFECTIVE_TAX_RATE_FORMULA, 'Income Tax Expense / Pretax Income', '');
  incomeTaxDef.kpiModels.push(EFFECTIVE_TAX_RATE);
  component = new ComponentDefinition('Sy_VcKr2p', 'Ratios', COMPONENT_TYPES.TABLE, null, [EFFECTIVE_TAX_RATE.key]);
  incomeTaxDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(incomeTaxDef);
})(wpw);
