(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var MINUS = wpw.analysis.AnalysisFormula.FORMULAS.minus;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var MULTIPLY = wpw.analysis.AnalysisFormula.FORMULAS.multiply;
  var CONSTANT = wpw.analysis.AnalysisFormula.FORMULAS.constant;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;

  /************ Formulas *********/
  var AR_TAG_NUMBER = '115';
  var AR_FORMULA = TAG(AR_TAG_NUMBER);
  var GROSS_AR_FORMULA = TAG('115.100');
  var AR_ALLOWANCE_FORMULA = TAG('115.200');
  var REVENUE_FORMULA = TAG('300');
  var DEFERRED_REVENUE_FORMULA = TAG('218');
  
  // Ratios
  var RECEIVABLE_PER_FORMULA = DIVIDE(AR_FORMULA, REVENUE_FORMULA);
  var AR_TURNOVER_FORMULA = DIVIDE(REVENUE_FORMULA, AR_FORMULA);
  var DAYS_SALES_FORMULA = DIVIDE(MULTIPLY(AR_FORMULA, CONSTANT(365)), REVENUE_FORMULA);
  var ALLOWANCE_OF_RECEIVABLES_FORMULA = DIVIDE(AR_ALLOWANCE_FORMULA, AR_FORMULA);
  var AR_TO_WORKING_CAP_FORMULA = DIVIDE(AR_FORMULA, MINUS(TAG('105'),TAG('210')));
  var REVENUE_RATIO_FORMULA = CONSTANT(1);
  var GROSS_MARGIN_FORMULA = MINUS(REVENUE_FORMULA, TAG('400'));
  
  /********* Formulas end ********/

  var rrrDefinition = new wpw.analysis.AnalysisDefinition();
  rrrDefinition.id = 'GJB_OJJXT5KqgPOdFE0O8Q';
  rrrDefinition.name = 'Accounts receivable';

  // Overview
  var GROSS_AR = new KPIModel('AR', 'Accounts receivable (AR gross)', DATA_TYPE.MONETARY, false, null, GROSS_AR_FORMULA);
  var REVENUE = new KPIModel('REVENUE', 'Revenue', DATA_TYPE.MONETARY, false, null, REVENUE_FORMULA);
  var DEFER_REVENUE = new KPIModel('DEFER_REVENUE', 'Deferred revenue', DATA_TYPE.MONETARY, false, null, DEFERRED_REVENUE_FORMULA);
  var AR_ALLOWANCE = new KPIModel('AR_ALLOWANCE', 'Accounts receivable allowance', DATA_TYPE.MONETARY, true, null,
      AR_ALLOWANCE_FORMULA);
 
      var componentModels = [GROSS_AR, AR_ALLOWANCE, REVENUE, DEFER_REVENUE];
  var component = new wpw.analysis.ComponentDefinition('Hk49KBhT', 'Overview', COMPONENT_TYPES.CHART,
      CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
 // component.groupKeysInStack('CUSTOMER_ADV_DEP', [CUSTOMER_ADV_DEP_CUR, CUSTOMER_ADV_DEP_NONCUR], false, 3);
  rrrDefinition.addUniqueKpiModels(componentModels);
  rrrDefinition.componentDefinitions.push(component);

  /// Accounts
  component = new wpw.analysis.ComponentDefinition('HyGnus0xfx', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.breakdownGroupNumbers = [AR_TAG_NUMBER];
  component.allowEdit = false;
  rrrDefinition.componentDefinitions.push(component);

   // RRR - Ratio Table
  var RECEIVABLE_PER = new KPIModel('RECEIVABLE_PERCENT', 'Receivables as % of revenue', DATA_TYPE.PERCENTAGE,
    false, '#e4d354', RECEIVABLE_PER_FORMULA, 'Receivable / Revenue');
  var AR_TURNOVER = new KPIModel('AR_TRUNOVER', 'AR turnover', DATA_TYPE.MONETARY, false, '#51E0E0',
    AR_TURNOVER_FORMULA, 'Credit sales / Accounts receivable', 'Measures the number of times trade receivables ' +
      'turnover during the year. The higher the turnover, the shorter the time between sale and cash collection.');
  AR_TURNOVER.numberOfDecimals = 2;
  
  var DAYS_SALES = new KPIModel('DAY_SALES', 'Days sales in receivables', DATA_TYPE.MONETARY, false,
    '#2b908f', DAYS_SALES_FORMULA, '(Accounts Receivable X 365) / Total sales', 'Measures the average length of time ' +
      'receivables are outstanding. Helps determine if potential collection problems exist.');
    DAYS_SALES.numberOfDecimals = 2;
  
    var ALLOWANCE_OF_RECEIVABLES = new KPIModel('ALLOWANCE_OF_RECEIVABLES', 'Allowance as a % of receivables', DATA_TYPE.PERCENTAGE, false,
    null, ALLOWANCE_OF_RECEIVABLES_FORMULA, 'Allowance / Receivables');
    ALLOWANCE_OF_RECEIVABLES.numberOfDecimals = 2;

    var AR_TO_WORKING_CAP = new KPIModel('AR_TO_WORKING_CAP', 'Accounts receivable to working capital', DATA_TYPE.NUMBER, false,
    null, AR_TO_WORKING_CAP_FORMULA, 'Accounts receivables / Working capital');
    AR_TO_WORKING_CAP.numberOfDecimals = 2;


  rrrDefinition.componentDefinitions.push(new wpw.analysis.ComponentDefinition('ryxN9Yr2T', 'Ratios',
      COMPONENT_TYPES.TABLE, null, [RECEIVABLE_PER.key, AR_TURNOVER.key, DAYS_SALES.key, ALLOWANCE_OF_RECEIVABLES.key,AR_TO_WORKING_CAP.key]));
  rrrDefinition.kpiModels.push(RECEIVABLE_PER, AR_TURNOVER, DAYS_SALES, ALLOWANCE_OF_RECEIVABLES, AR_TO_WORKING_CAP);


  // RRR - Revenue
  var REVENUE = new KPIModel('REVENUE', 'Revenue', DATA_TYPE.MONETARY, true,
      wpw.analysis.KPIModel.BRIGHT_COLORS[0], REVENUE_FORMULA, 'Revenue');
  var GROSS_MARGIN = new KPIModel('GROSS_MARGIN', 'Gross margin', DATA_TYPE.MONETARY, false, null,
      GROSS_MARGIN_FORMULA, 'Revenue - Cost of sales');
  componentModels = [REVENUE, GROSS_MARGIN];
  component = new wpw.analysis.ComponentDefinition('HkzN9trhT', 'Revenue', COMPONENT_TYPES.CHART,
      CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
  component.preferRight = true;
  rrrDefinition.componentDefinitions.push(component);
  rrrDefinition.addUniqueKpiModels(componentModels);

// RRR - Allowance
 
var componentModels = [AR_ALLOWANCE];
var component = new wpw.analysis.ComponentDefinition('BkWV9Kr3p', 'Allowance', COMPONENT_TYPES.CHART,
    CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
rrrDefinition.addUniqueKpiModels(componentModels);
rrrDefinition.componentDefinitions.push(component);


  wpw.injectedAnalysisDefinitions.push(rrrDefinition);
})(wpw);