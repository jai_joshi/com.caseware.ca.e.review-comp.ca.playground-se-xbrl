(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var MINUS = wpw.analysis.AnalysisFormula.FORMULAS.minus;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /************ Formulas *********/
  
  var LT_NONCURRENT_FORMULA = TAG('231');
  var TOTAL_LIABILITIES_FORMULA = TAG('205');
  var INTEREST_EXPENSE_FORMULA = TAG('525');
  var TOTAL_EQUITY_FORMULA = TAG('270');
  var DEBT_EQUITY_RATIO_FORMULA = DIVIDE(TOTAL_LIABILITIES_FORMULA, TOTAL_EQUITY_FORMULA);
  var LT_DEBT_EQUITY_RATIO_FORMULA = DIVIDE(LT_NONCURRENT_FORMULA, TOTAL_EQUITY_FORMULA);
  var DEBT_ASSETS_RATIO_FORMULA = DIVIDE(TOTAL_LIABILITIES_FORMULA, TAG('100'));
  var LT_DEBT_ASSETS_RATIO_FORMULA = DIVIDE(LT_NONCURRENT_FORMULA, TAG('100'));
  var LTLWCR_FORMULA = DIVIDE(TAG('230'),MINUS(TAG('105'),TAG('210')));
  var INIBT_FORMULA = DIVIDE(INTEREST_EXPENSE_FORMULA,ADD(TAG('285.200'), INTEREST_EXPENSE_FORMULA));
  var TIMES_INTEREST_EARNED_FORMULA = DIVIDE(ADD(TAG('285.200'),INTEREST_EXPENSE_FORMULA,TAG('860')),
  INTEREST_EXPENSE_FORMULA);
   
  /************ Formulas end *********/

  var ltdDefinition = new wpw.analysis.AnalysisDefinition();
  ltdDefinition.id = 'sVRtuEodT3Kb91QfY63DyQ';
  ltdDefinition.name = 'Loans, advances and long-term debt';

  //Overview
  var TOTAL_LIABILITIES = new KPIModel('TOTAL_LIABILITIES', 'Total liabilities', DATA_TYPE.MONETARY, true, null,
    TOTAL_LIABILITIES_FORMULA);
  var INTEREST_EXPENSE = new KPIModel('INTEREST_EXPENSE', 'Interest expense', DATA_TYPE.MONETARY, true, null,
    INTEREST_EXPENSE_FORMULA);
  var TOTAL_EQUITY = new KPIModel('TOTAL_EQUITY', 'Equity', DATA_TYPE.MONETARY, true, null,
    TOTAL_EQUITY_FORMULA);
  
    ltdDefinition.kpiModels.push(TOTAL_LIABILITIES, INTEREST_EXPENSE, TOTAL_EQUITY);
  var ltiChart = new ComponentDefinition('S1ZeE5KShT', 'Overview', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN,
    [TOTAL_LIABILITIES.key, INTEREST_EXPENSE.key,TOTAL_EQUITY.key]);
  ltiChart.groupKeysInStack('LT_DEBT', [TOTAL_LIABILITIES, INTEREST_EXPENSE, TOTAL_EQUITY], false, 1);
  ltdDefinition.componentDefinitions.push(ltiChart);

  // Accounts
  var component = new ComponentDefinition('SkREAryXl', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.breakdownGroupNumbers = ['225','231','224','234'];
  component.allowEdit = false;
  ltdDefinition.componentDefinitions.push(component);


  //Ratios
  var DEBT_EQUITY_RATIO = new KPIModel('DEBT_EQUITY_RATIO', 'Debt / Equity ratio', DATA_TYPE.PERCENTAGE, false, null,
      DEBT_EQUITY_RATIO_FORMULA, 'Total Liabilities / Equity', 'Measures the relative use of debt compared to ' +
      'resources invested by owners/shareholders.');
  var LT_DEBT_EQUITY_RATIO = new KPIModel('LT_DEBT_EQUITY_RATIO', 'Long-term debt / Equity ratio',
      DATA_TYPE.PERCENTAGE, false, null, LT_DEBT_EQUITY_RATIO_FORMULA, 'Long-term Debt / Equity', 'Measures the ' +
      'relative use of long-term debt compared to resources invested by owners/shareholders.');
  var DEBT_ASSETS_RATIO = new KPIModel('DEBT_ASSETS_RATIO', 'Debt / Assets ratio', DATA_TYPE.PERCENTAGE, false, null,
      DEBT_ASSETS_RATIO_FORMULA, 'Total Liabilities / Total Assets', 'Measures the portion of total assets provided by' +
        ' the company\'s creditors. In conjunction with other ratios, this ratio indicates the degree to which ' +
        'operating losses may be cushioned from adverse actions by creditors.');
  var LT_DEBT_ASSETS_RATIO = new KPIModel('LT_DEBT_ASSETS_RATIO', 'Long-term debt / Assets ratio',
        DATA_TYPE.PERCENTAGE, false, null, LT_DEBT_ASSETS_RATIO_FORMULA, 'Long-term Debt / Total Assets', 'Measures ' +
        'the portion of total assets finaniced with loans or other finanical obligations that extend more than one ' +
        'year. It provides a general measure of the entties ability to meet the finanical requirements for outstanding ' +
        'loans.');
  var LTDWCR = new KPIModel('LT_DEBT_TO_WORKING_CAPITAL_RATIO', 'Long-term Liabilities / Working capital ratio',
    DATA_TYPE.PERCENTAGE, false, null, LTLWCR_FORMULA, 'Long-term debt / Working capital', 'In conjunction with ' +
      'other ratios, the Long Term Liabilities to Working Capital Ratio measures the degree to which long-term ' +
      'borrowings have been used to replenish working capital versus fixed asset acquisition. A high ratio may ' +
      'indicate that the company has reached its borrowing limit and should look toward equity financing. A low ' +
      'ratio may indicate an opportunity to convert short-term into long-term borrowing during periods of favourable ' +
      'interest rates.');
  var INIBT = new KPIModel('INTEREST_NET_INCOME_BEFORE_INTEREST',
    'Interest / Net income before interest', DATA_TYPE.PERCENTAGE, false, null, INIBT_FORMULA,
    'Interest Expense / (Net Income + Interest Expense)', 'Measures the company’s ability to pay its contractual ' +
      'interest payments. Also indicates the extent to which financial leverage had a positive or negative effect ' +
      'on net income.');
  var TIMES_INTEREST_EARNED = new KPIModel('TIMES_INTEREST_EARNED', 'Times interest earned / Interest coverage',
      DATA_TYPE.NUMBER, false, null, TIMES_INTEREST_EARNED_FORMULA, '(Net income + Interest + Income Tax) / Interest',
      'Measures the company’s ability to pay its contractual interest payments. Also indicates the extent to which ' +
      'financial leverage had a positive or negative effect on net income.');
      TIMES_INTEREST_EARNED.numberOfDecimals = 2;
  ltdDefinition.kpiModels.push(DEBT_EQUITY_RATIO, LT_DEBT_EQUITY_RATIO, DEBT_ASSETS_RATIO, LT_DEBT_ASSETS_RATIO, LTDWCR,
      INIBT, TIMES_INTEREST_EARNED);
  ltdDefinition.componentDefinitions.push(new ComponentDefinition('HkfgNcKS2T', 'Ratios', COMPONENT_TYPES.TABLE, null,
      [DEBT_EQUITY_RATIO.key, LT_DEBT_EQUITY_RATIO.key, DEBT_ASSETS_RATIO.key, LT_DEBT_ASSETS_RATIO.key, LTDWCR.key,
        INIBT.key, TIMES_INTEREST_EARNED.key]));

  wpw.injectedAnalysisDefinitions.push(ltdDefinition);
})(wpw);
