(function() {
  'use strict';

  /**
   * @name wpw.tax.diagnostics.diagnosticDetails
   * @type {Object}
   * @property {string} [category='N/A'] - the type of error (e.g. 'TAX', 'EFILE', 'OTHER')
   * @property {string} [severity='warning'] - severity of the error (e.g. 'warning', 'critical')
   * @property {string} label - description of the error
   * @property {boolean} ignorable - true to allow the user to ignore/remove diagnostic
   * @property {string} [type='field'] - the type of diagnostic (e.g. 'field, 'form', 'bundle')
   * @property {boolean} isUnique
   */

  /**
   * @type {Object.<string, wpw.tax.diagnostics.diagnosticDetails>}
   */
  wpw.tax.codes.diagnostics = {
    'required': {label: 'Field is required', severity: 'critical', category: 'VALIDATOR'},
    'format': {label: 'Incorrect format', severity: 'critical', category: 'VALIDATOR'},
    'email': {label: 'Email Address has incorrect format', severity: 'critical', category: 'VALIDATOR'},
    'range': {label: 'The field value must be between %MIN% and %MAX%', severity: 'critical', category: 'VALIDATOR'},
    'min': {label: 'The field value must be greater than %MIN%', severity: 'critical', category: 'VALIDATOR'},
    'max': {label: 'The field value must be less than %MAX%', severity: 'critical', category: 'VALIDATOR'},
    'postalCodeFormat': {label: 'Invalid postal code', severity: 'critical', category: 'VALIDATOR'},
    'cityFormat': {label: 'Invalid city', severity: 'critical', category: 'VALIDATOR'},
    'addressFormat': {label: 'Invalid address', severity: 'critical', category: 'VALIDATOR'},
    'BN': {label: 'Business number does not pass the mod10 check.', severity: 'critical', category: 'VALIDATOR'},
    'phone': {label: 'Telephone number has incorrect format.', severity: 'critical', category: 'VALIDATOR'},
    'percent': {label: 'Percent has incorrect format.', severity: 'critical', category: 'VALIDATOR'},

    'override': {label: 'Field value has been overridden', severity: 'warning', category: 'OVERRIDE'},

    // TODO: delete these error codes (examples only)
    '102': {label: 'Incorrect format: %DESC%'},

    //INTERNET RULES and CORPORATION INTERNET ERROR MESSAGES
    '301': {
      label: 'Due to CRA’s daily systems maintenance, the Corporation Internet filing service is currently not available.',
      category: 'WEBSERVICE'
    },
    '302': {
      label: 'Due to CRA’s daily systems maintenance, the Corporation Internet filing service is currently not available. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '303': {
      label: 'CRA is unable to establish a session for you. To better identify the source of this technical problem, contact your software vendor.',
      category: 'WEBSERVICE'
    },
    '304': {
      label: 'CRA is experiencing technical difficulties connecting to the Corporation Internet Filing services. For reference purposes the error is specific to: xxxxxxxx',
      category: 'WEBSERVICE'
    },
    '307': {
      label: 'The tax year end date cannot be before the year 2002; submit your return on paper.',
      category: 'WEBSERVICE'
    },
    '308': {
      label: 'Enter either the Web access code or your EFILE number and password.',
      category: 'WEBSERVICE'
    },
    '309': {
      label: 'You have entered an invalid Web access code. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '310': {
      label: 'You have entered an invalid Web access code. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '311': {
      label: 'Enter the file name and location.',
      category: 'WEBSERVICE'
    },
    '312': {
      label: 'You have entered an invalid EFILE number. Contact CRA’s T1 EFILE Helpdesk Support for further information.',
      category: 'WEBSERVICE'
    },
    '313': {
      label: 'The file type is not valid. To better identify the source of this technical problem, contact your software vendor.',
      category: 'WEBSERVICE'
    },
    '314': {
      label: 'The file cannot exceed 4,000,000 bytes. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '315': {
      label: 'The file link cannot be found or the file is empty.',
      category: 'WEBSERVICE'
    },
    '316': {
      label: 'CRA is experiencing technical difficulties connecting to the Corporation Internet Filing service. Contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803 for further information.',
      category: 'WEBSERVICE'
    },
    '319': {
      label: 'You have entered an invalid password. Contact CRA’s T1 EFILE Helpdesk Support for further information.',
      category: 'WEBSERVICE'
    },
    '320': {
      label: 'The information submitted is either incorrect or incomplete.',
      category: 'WEBSERVICE'
    },
    '321': {
      label: 'The Business number and/or the tax year end are not registered in our system. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '322': {
      label: 'The Web access code that you have provided does not match the Business number and/or the tax year end in CRA’s records. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1 800-959-2803.',
      category: 'WEBSERVICE'
    },
    '323': {
      label: 'According to CRA’s records, the Web access code entered has been cancelled. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '324': {
      label: 'The Web access code entered does not match CRA’s electronic signature on file. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '325': {
      label: 'Access to CRA’s Corporation Internet Filing system has been denied due to three incorrect attempts to enter the Web access code. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1 800-959-2803.',
      category: 'WEBSERVICE'
    },
    // '326' : {
    //   label : 'According to CRA’s records, your Business number is not active. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1 800-959-2803.',
    //   category : 'WEBSERVICE'
    // },
    '327': {
      label: 'Access to CRA’s Corporation Internet Filing system has been denied as you have exceeded the maximum number of transmission attempts. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '340': {
      label: 'The information submitted is either incorrect or incomplete. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '341': {
      label: 'The information submitted is either incorrect or incomplete. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '342': {
      label: 'The information submitted is either incorrect or incomplete. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '343': {
      label: 'The information submitted is either incorrect or incomplete. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '344': {
      label: 'The information submitted is either incorrect or incomplete. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '345': {
      label: 'The information submitted is either incorrect or incomplete. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '346': {
      label: 'The information submitted is either incorrect or incomplete. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '347': {
      label: 'Access to CRA’s Corporation Internet filing system has been denied as you have exceeded the maximum number of attempts. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '355': {
      label: 'The tax year start date is missing or invalid. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '357': {
      label: 'The tax year end date is missing or invalid. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '363': {
      label: 'The name of the corporation is missing. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '364': {
      label: 'The name of the corporation provided does not match CRA’s records. A corporation name change cannot be processed while Internet filing. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    // '365' : {
    //   label : 'A corporation name change cannot be processed while Internet filing. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
    //   category : 'WEBSERVICE'
    // },
    '366': {
      label: 'A head office address change cannot be processed while Internet filing. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '367': {
      label: 'A mailing address change cannot be processed while Internet filing. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '368': {
      label: 'A change to the location of the books and records is indicated but the address is missing. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '369': {
      label: 'A change to the location of the books and records is indicated but the address provided is incomplete. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '370': {
      label: 'CRA cannot accept the return transmitted at this time because the tax year end date is after the transmission date. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '371': {
      label: 'The tax year end date is earlier than the tax year start date. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1 800-959-2803.',
      category: 'WEBSERVICE'
    },
    '372': {
      label: 'CRA cannot accept the return transmitted because the tax year is more than 378 days. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '375': {
      label: 'CRA is unable to accept the return since it was prepared using a software package that does not support the electronic filing of SR&ED claims. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '378': {
      label: 'The type of corporation at the end of the tax year is missing. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '379': {
      label: 'The type of corporation at the end of the tax year is not valid. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '381': {
      label: 'CRA is unable to process the return since it was prepared using a software package that was not approved by the CRA. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '382': {
      label: 'CRA is unable to accept the return since it was prepared using a software package that does not support the filing period. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '385': {
      label: 'CRA cannot accept the return transmitted because it contains entries in the direct deposit area. For assistance, contact CRA’s Corporation Internet filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '387': {
      label: 'CRA cannot process the return transmitted for this tax year since they have already received this return. If you are submitting an amended return you must indicate this in your software. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '388': {
      label: 'CRA cannot process the return transmitted since the tax period for this return overlaps an existing tax period for a return already assessed. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '395': {
      label: 'Account already registered for Manage online mail service. Any updates to email address should be made directly in the MyBA/RAC portal.',
      category: 'WEBSERVICE'
    },
    '396': {
      label: 'We cannot accept the email address because the program account is not eligible for MyBA.',
      category: 'WEBSERVICE'
    },
    '397': {
      label: 'We cannot accept the email address because the email address is not in a valid format.',
      category: 'WEBSERVICE'
    },
    '398': {
      label: 'We cannot accept email addresses on an amended T2 return.',
      category: 'WEBSERVICE'
    },
    '399': {
      label: 'We cannot accept the email address because the user selected “no” at line 88. Either remove the email address, or select “yes” and keep the address.',
      category: 'WEBSERVICE'
    },
    '451': {
      label: 'According to CRA’s records, the EFILE password provided is not valid. Contact CRA’s T1 EFILE Helpdesk Support for further information.',
      category: 'WEBSERVICE'
    },
    '452': {
      label: 'According to CRA’s records, your EFILE On-Line account is suspended. Contact CRA’s T1 EFILE Helpdesk Support at for further information.',
      category: 'WEBSERVICE'
    },
    '453': {
      label: 'According to CRA’s records, the EFILE number provided is not valid. Contact CRA’s T1 EFILE Helpdesk Support for further information.',
      category: 'WEBSERVICE'
    },
    '454': {
      label: 'CRA is unable to process the return at this time due to system maintenance. Contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803 for further information.',
      category: 'WEBSERVICE'
    },
    '455': {
      label: 'According to CRA’s records, the Business number submitted is not valid. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '456': {
      label: 'According to CRA’s records, your EFILE password has expired. Yearly renewal is mandatory. Contact CRA’s T1 EFILE Helpdesk Support for further information.',
      category: 'WEBSERVICE'
    },
    '457': {
      label: 'According to CRA’s records, you are not currently permitted to file the return using your EFILE password as suitability screening has not been completed. Contact CRA’s T1 EFILE Helpdesk Support for further information.',
      category: 'WEBSERVICE'
    },
    '501': {
      label: 'CRA is unable to process your return at this time as the file is empty. For reference purposes the data surrounding the error contained: xxxxxxxx',
      category: 'WEBSERVICE'
    },
    '502': {
      label: 'CRA is unable to process your return at this time as an error was found with the format of the attached file. The format is invalid due to missing or incorrect trailer information. For reference purposes the data surrounding the error contained: xxxxxxxx',
      category: 'WEBSERVICE'
    },
    '503': {
      label: 'CRA is unable to process your return at this time as an error was found with the format of the attached file. The format is invalid due to missing or incorrect data delimiters. For reference purposes the data surrounding the error contained: xxxxxxxx',
      category: 'WEBSERVICE'
    },
    '504': {
      label: 'CRA is unable to process your return at this time as an error was found with the format of the attached file. The format is invalid due to data overflow in the line number below: Schedule: xxx Page: xxx Line: xxx Row: xxx',
      category: 'WEBSERVICE'
    },
    '505': {
      label: 'CRA is unable to process your return at this time as an error was found with the format of the attached file. The format is invalid as a non-numeric schedule number was detected. For reference purposes the data surrounding the error contained: xxxxxxxx',
      category: 'WEBSERVICE'
    },
    '506': {
      label: 'CRA is unable to process your return at this time as an error was found with the format of the attached file. The format is invalid due to an invalid page number on schedule: Schedule: xxx. For reference purposes the data surrounding the error contained: xxxxxxxx',
      category: 'WEBSERVICE'
    },
    '507': {
      label: 'CRA is unable to process your return at this time as an error was found with the format of the attached file. The format is invalid due to an invalid row number: Schedule: xxx Page: xxx Line: xxx. For reference purposes the data surrounding the error contained: xxxxxxxx',
      category: 'WEBSERVICE'
    },
    '508': {
      label: 'CRA is unable to process your return at this time as an error was found with the format of the attached file. The format is invalid as an unknown schedule number was detected: Schedule: xxx',
      category: 'WEBSERVICE'
    },
    '509': {
      label: 'CRA is unable to process your return at this time as an error was found with the format of the attached file. The format is invalid as a duplicate schedule was detected: Schedule: xxx ',
      category: 'WEBSERVICE'
    },
    '510': {
      label: 'CRA is unable to process your return at this time as an error was found with the format of the attached file. The format is invalid as a duplicate line number was detected: Schedule: xxx Page: xxx Line: xxx Row: xxx',
      category: 'WEBSERVICE'
    },
    '512': {
      label: 'CRA is unable to process your return at this time as an error was found with the format of the attached file. The format is invalid as there is no data at line number: Schedule: xxx Page: xxx Line: xxx',
      category: 'WEBSERVICE'
    },
    '513': {
      label: 'CRA is unable to process your return at this time as an error was found with the format of the attached file. The format is invalid as a line number is not numeric: Schedule: xxx Page: xxx. For reference purposes the data surrounding the error contained: xxxxxxxx',
      category: 'WEBSERVICE'
    },
    '521': {
      label: 'According to CRA’s records, this tax year has not been assessed therefore an amendment cannot be processed. For assistance, contact CRA’s corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '522': {
      label: 'CRA is unable to process this amendment as a (re)assessment is currently in progress or you have an Appeal under review for this tax year. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '523': {
      label: 'CRA is unable to process this amendment as a (re)assessment is currently in progress or you have an Appeal under review for this tax year. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '530': {
      label: 'CRA is unable to accept the T2 E-Attachment(s) since the software version is not approved by the CRA. To better identify the source of this problem, contact your software vendor.',
      category: 'WEBSERVICE'
    },
    '531': {
      label: 'CRA is unable to accept the T2 E-Attachment(s) since the confirmation number is not valid. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '532': {
      label: 'The file name of the T2 E-Attachment is not valid. Please ensure that the file name does not contain special characters: \\ / : * ? “ < > |',
      category: 'WEBSERVICE'
    },
    '533': {
      label: 'The file type of the T2 E-Attachment is not valid. CRA accepts the following file types for T2 attachments: pdf, doc, docx, xls, xlsx, rtf, txt, jpeg, jpg, tiff, tif, xps.',
      category: 'WEBSERVICE'
    },
    '534': {
      label: 'The total size of all T2 E-Attachment(s) cannot exceed 150 Mb. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '535': {
      label: 'The number of T2 E-Attachments does not match the number of file names provided. To better identify the source of this problem, contact your software vendor.',
      category: 'WEBSERVICE'
    },
    '540': {
      label: 'CRA is unable to accept the T2 E-Attachment(s) since the T2 return was filed over 24 hours ago. Please mail the documents to your tax centre.',
      category: 'WEBSERVICE'
    },
    '541': {
      label: 'CRA is unable to accept the T2 E-Attachment(s) since the T2 return is not for an insurance corporation. CRA only accepts E-Attachment(s) for insurance corporations at this time. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '542': {
      label: 'CRA is unable to accept the T2 E-Attachment(s) since the business number, tax year-end or confirmation number does not match our records. For assistance, contact CRA’s Corporation Internet Filing Helpdesk at 1-800-959-2803.',
      category: 'WEBSERVICE'
    },
    '543': {
      label: 'CRA is unable to accept the T2 E-Attachment(s) since the software package is not approved to use this service. For assistance, contact your software vendor.',
      category: 'WEBSERVICE'
    },
    '544': {
      label: 'CRA is unable to accept the T2 E-Attachment(s) since the document identifier is not valid.',
      category: 'WEBSERVICE'
    },
    //Software approval code
    '2000500': {
      label: 'The software approval code was not included at line 200099 of the return.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000501': {
      label: 'The Software approval code is not valid for the tax year start.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000502': {
      label: 'The Software approval code was not valid for the tax year end.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000505': {
      label: 'The Software approval code is not valid.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000506': {
      label: 'The Software approval code is only valid for filing computer prepared paper returns and not for electronic returns.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000601': {
      label: 'The Software approval code is not valid for Schedule 1.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000602': {
      label: 'The Software approval code is not valid for Schedule 2.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000603': {
      label: 'The Software approval code is not valid for Schedule 3.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000604': {
      label: 'The Software approval code is not valid for Schedule 4.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000605': {
      label: 'The Software approval code is not valid for Schedule 5.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000606': {
      label: 'The Software approval code is not valid for Schedule 6.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000607': {
      label: 'The Software approval code is not valid for Schedule 7.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000608': {
      label: 'The Software approval code is not valid for Schedule 8.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000609': {
      label: 'The Software approval code is not valid for Schedule 9.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000610': {
      label: 'The Software approval code is not valid for Schedule 10.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000611': {
      label: 'The Software approval code is not valid for Schedule 11.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000612': {
      label: 'The Software approval code is not valid for Schedule 12.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000613': {
      label: 'The Software approval code is not valid for Schedule 13.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000614': {
      label: 'The Software approval code is not valid for Schedule 14.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000615': {
      label: 'The Software approval code is not valid for Schedule 15.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000616': {
      label: 'The Software approval code is not valid for Schedule 16.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000617': {
      label: 'The Software approval code is not valid for Schedule 17.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000618': {
      label: 'The Software approval code is not valid for Schedule 18.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000619': {
      label: 'The Software approval code is not valid for Schedule 19.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000620': {
      label: 'The Software approval code is not valid for Schedule 20.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000621': {
      label: 'The Software approval code is not valid for Schedule 21.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000622': {
      label: 'The Software approval code is not valid for Schedule 22.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000623': {
      label: 'The Software approval code is not valid for Schedule 23.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000624': {
      label: 'The Software approval code is not valid for Schedule 24.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000625': {
      label: 'The Software approval code is not valid for Schedule 25.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000627': {
      label: 'The Software approval code is not valid for Schedule 27.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000628': {
      label: 'The Software approval code is not valid for Schedule 28.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000629': {
      label: 'The Software approval code is not valid for Schedule 29.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000631': {
      label: 'The Software approval code is not valid for Schedule 31.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000632': {
      label: 'The Software approval code is not valid for Schedule 32.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000633': {
      label: 'The Software approval code is not valid for Schedule 33.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000634': {
      label: 'The Software approval code is not valid for Schedule 34.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000635': {
      label: 'The Software approval code is not valid for Schedule 35.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000637': {
      label: 'The Software approval code is not valid for Schedule 37.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000638': {
      label: 'The Software approval code is not valid for Schedule 38.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000639': {
      label: 'The Software approval code is not valid for Schedule 39.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000642': {
      label: 'The Software approval code is not valid for Schedule 42.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000643': {
      label: 'The Software approval code is not valid for Schedule 43.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000644': {
      label: 'The Software approval code is not valid for Schedule 44.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000645': {
      label: 'The Software approval code is not valid for Schedule 45.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000646': {
      label: 'The Software approval code is not valid for Schedule 46.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000647': {
      label: 'The Software approval code is not valid for Schedule 47 (Form T1131).',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000649': {
      label: 'The Software approval code is not valid for Schedule 49.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000650': {
      label: 'The Software approval code is not valid for Schedule 50.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000651': {
      label: 'The Software approval code is not valid for Schedule 30.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000653': {
      label: 'The Software approval code is not valid for Schedule 53.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000654': {
      label: 'The Software approval code is not valid for Schedule 54.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000655': {
      label: 'The Software approval code is not valid for Schedule 55.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000673': {
      label: 'The Software approval code is not valid for Schedule 73.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000688': {
      label: 'The Software approval code is not valid for Schedule 88.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000679': {
      label: 'The Software approval code is not valid for Schedule 97.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000700': {
      label: 'The Software approval code is not valid for Schedules 100, 101, 125, 140, and 141.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000701': {
      label: 'The Software approval code is not valid for Schedule 384.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000702': {
      label: 'The Software approval code is not valid for Schedule 385.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000708': {
      label: 'The Software approval code is not valid for Schedule 308.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000709': {
      label: 'The Software approval code is not valid for Schedule 303.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000710': {
      label: 'The Software approval code is not valid for Schedule 301.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000711': {
      label: 'The Software approval code is not valid for Schedule 321.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000712': {
      label: 'The Software approval code is not valid for Schedule 340.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000713': {
      label: 'The Software approval code is not valid for Schedule 342.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000714': {
      label: 'The Software approval code is not valid for Schedule 343.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000715': {
      label: 'The Software approval code is not valid for Schedule 344.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000716': {
      label: 'The Software approval code is not valid for Schedule 345.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000719': {
      label: 'The Software approval code is not valid for Schedule 365.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000720': {
      label: 'The Software approval code is not valid for Schedule 381.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000722': {
      label: 'The Software approval code is not valid for Schedule 400.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000723': {
      label: 'The Software approval code is not valid for Schedule 402.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000725': {
      label: 'The Software approval code is not valid for Schedule 360.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000726': {
      label: 'The Software approval code is not valid for Schedule 380.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000728': {
      label: 'The Software approval code is not valid for Schedule 403.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000729': {
      label: 'The Software approval code is not valid for Schedule 410.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000730': {
      label: 'The Software approval code is not valid for Schedule 460.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000731': {
      label: 'The Software approval code is not valid for Schedule 048 (Form T1177).',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000732': {
      label: 'The Software approval code is not valid for Schedule 302.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000733': {
      label: 'The Software approval code is not valid for Schedule 421.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000734': {
      label: 'The Software approval code is not valid for Schedule 422.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000735': {
      label: 'The Software approval code is not valid for Schedule 423.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000736': {
      label: 'The Software approval code is not valid for Schedule 425.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000738': {
      label: 'The Software approval code is not valid for Schedule 480.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000739': {
      label: 'The Software approval code is not valid for Schedule 426.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000746': {
      label: 'The Software approval code is not valid for Schedule 091.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000747': {
      label: 'The Software approval code is not valid for Schedule 442.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000822': {
      label: 'The Software approval code is not valid for Schedule 428.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000823': {
      label: 'The Software approval code is not valid for Schedule 304.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000824': {
      label: 'The Software approval code is not valid for Schedule 347.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000825': {
      label: 'The Software approval code is not valid for Schedule 388.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000826': {
      label: 'The Software approval code is not valid for Schedule 060.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000827': {
      label: 'The Software approval code is not valid for Schedule 387.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000828': {
      label: 'The Software approval code is not valid for Schedule 389.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000829': {
      label: 'The Software approval code is not valid for Schedule 062.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000830': {
      label: 'The Software approval code is not valid for Schedule 061.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000831': {
      label: 'The Software approval code is not valid for Schedule 305.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000832': {
      label: 'The Software approval code is not valid for Schedule 306.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000833': {
      label: 'The Software approval code is not valid for Schedule 490.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000834': {
      label: 'The Software approval code is not valid for Schedule 429.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000835': {
      label: 'The Software approval code is not valid for Schedule 430.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000837': {
      label: 'The Software approval code is not valid for Schedule 367.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000852': {
      label: 'The Software approval code is not valid for Schedule 502.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000854': {
      label: 'The Software approval code is not valid for Schedule 504.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000855': {
      label: 'The Software approval code is not valid for Schedule 525.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000856': {
      label: 'The Software approval code is not valid for Schedule 506.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000858': {
      label: 'The Software approval code is not valid for Schedule 508.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000860': {
      label: 'The Software approval code is not valid for Schedule 510.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000861': {
      label: 'The Software approval code is not valid for Schedule 511.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000862': {
      label: 'The Software approval code is not valid for Schedule 512.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000863': {
      label: 'The Software approval code is not valid for Schedule 513.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000867': {
      label: 'The Software approval code is not valid for Schedule 547.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000868': {
      label: 'The Software approval code is not valid for Schedule 548.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000869': {
      label: 'The Software approval code is not valid for Schedule 524.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000870': {
      label: 'The Software approval code is not valid for Schedule 550.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000871': {
      label: 'The Software approval code is not valid for Schedule 546.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000872': {
      label: 'The Software approval code is not valid for Schedule 552.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000874': {
      label: 'The Software approval code is not valid for Schedule 554.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000876': {
      label: 'The Software approval code is not valid for Schedule 556.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000878': {
      label: 'The Software approval code is not valid for Schedule 558.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000880': {
      label: 'The Software approval code is not valid for Schedule 560.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000882': {
      label: 'The Software approval code is not valid for Schedule 562.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000884': {
      label: 'The Software approval code is not valid for Schedule 564.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000886': {
      label: 'The Software approval code is not valid for Schedule 566.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000888': {
      label: 'The Software approval code is not valid for Schedule 568.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000889': {
      label: 'The Software approval code is not valid for Schedule 569.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000890': {
      label: 'The Software approval code is not valid for Schedule 390.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000891': {
      label: 'The Software approval code is not valid for Schedule 391.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000892': {
      label: 'The Software approval code is not valid for Schedule 392.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000893': {
      label: 'The Software approval code is not valid for Schedule 393.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000894': {
      label: 'The Software approval code is not valid for Schedule 394.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000810': {
      label: 'The Software approval code is not valid for a return filed under Income Tax Regulation 403.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000811': {
      label: 'The Software approval code is not valid for a return filed under Income Tax Regulation 404.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000812': {
      label: 'The Software approval code is not valid for a return filed under Income Tax Regulation 405.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000813': {
      label: 'The Software approval code is not valid for a return filed under Income Tax Regulation 406(1).',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000814': {
      label: 'The Software approval code is not valid for a return filed under Income Tax Regulation 406(2).',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000815': {
      label: 'The Software approval code is not valid for a return filed under Income Tax Regulation 407.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000816': {
      label: 'The Software approval code is not valid for a return filed under Income Tax Regulation 408.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000817': {
      label: 'The Software approval code is not valid for a return filed under Income Tax Regulation 409.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000818': {
      label: 'The Software approval code is not valid for a return filed under Income Tax Regulation 410.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000819': {
      label: 'The Software approval code is not valid for a return filed under Income Tax Regulation 411.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000820': {
      label: 'The Software approval code is not valid for a return filed under Income Tax Regulation 412.',
      category: 'SOFTWAREAPPROVALCODE'
    },
    '2000821': {
      label: 'The Software approval code is not valid for a return filed under Income Tax Regulation 413.',
      category: 'SOFTWAREAPPROVALCODE'
    },

    //T2S100/S101
    '1000001': {
      label: 'Balance sheet information is missing.',
      category: 'EFILE'
    },
    '1000002': {
      label: 'Total assets does not equal total liabilities plus total shareholder equity.',
      category: 'EFILE'
    },
    '1010002': {
      label: 'Total assets does not equal total liabilities plus total shareholder equity.',
      category: 'EFILE'
    },

    //T2S125
    '1250001': {
      label: 'The corporation did not file Schedule 125.',
      category: 'EFILE'
    },
    '1250002': {
      label: 'There is no entry at GIFI line code 9999.',
      category: 'EFILE'
    },
    '1250003': {
      label: 'The corporation has filed more than one Schedule 125, but there are entries at GIFI line codes 9975 to 9999.',
      category: 'EFILE'
    },
    '1250004': {
      label: 'The corporation did not provide a sequence number at GIFI line code 0003.',
      category: 'EFILE'
    },

    //T2S140
    '1400001': {
      label: 'There is no entry at GIFI line code 9970.',
      category: 'EFILE'
    },
    '1400002': {
      label: 'There is no entry at GIFI line code 9999.',
      category: 'EFILE'
    },
    '1401015': {
      label: 'There is only one Schedule 125 completed, but Schedule 140 is completed.',
      category: 'EFILE'
    },

    //T2S141
    '1410002': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 141101.',
      category: 'EFILE'
    },
    '1410003': {
      label: 'The corporation has answered Yes (1) at line 141101, but has not answered Yes (1) or No (2) at lines 141104 to 141107.',
      category: 'EFILE'
    },
    '1410004': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 141108.',
      category: 'EFILE'
    },
    '1410006': {
      label: 'The corporation has answered Yes (1) at line 141095 and No (2) at line 141097, but has not answered line 141198.',
      category: 'EFILE'
    },
    '1410007': {
      label: 'The corporation has answered (1) or (2) at line 141198, but has not answered Yes (1) or No (2) at line 141099.',
      category: 'EFILE'
    },

    //T2J
    '2000001': {
      label: 'The name of the corporation has not been provided at line 200002.',
      category: 'EFILE'
    },
    '2000002': {
      label: 'The Business number of the corporation assigned by CRA has not been provided at line 200001.',
      category: 'EFILE'
    },
    '2000006': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 200010, whether there has been a change of head office address since the last time the Department was notified.',
      category: 'EFILE'
    },
    '2000007': {
      label: 'The corporation has answered Yes (1) at line 200010, but has not provided the head office address information required (where applicable) at lines 200011, 200012, 200015, 200016, 200017, and 200018.',
      category: 'EFILE'
    },
    '2000008': {
      label: 'The corporation has answered Yes (1) at line 200010, but has not provided complete head office address information required (where applicable) at lines 200011, 200012, 200015, 200016, 200017, and 200018.',
      category: 'EFILE'
    },
    '2000050': {
      label: 'The corporation indicated that its country code is CA, but has a province or territory code at lines 200016, 200026, or 200036 other than AB, BC, MB, NB, NL, NU, NS, NT, ON, PE, QC, SK or YT. ',
      category: 'EFILE'
    },
    '2000009': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 200020, whether the mailing address has changed since the last time the Department was notified.',
      category: 'EFILE'
    },
    '2000010': {
      label: 'The corporation has answered Yes (1) at line 200020, but has not provided the mailing address information required (where applicable) at lines 200021, 200022, 200023, 200025, 200026, 200027, and 200028.',
      category: 'EFILE'
    },
    '2000011': {
      label: 'The corporation has answered Yes (1) at line 200020, but has not provided complete mailing address information required (where applicable) at lines 200021, 200022, 200023, 200025, 200026, 200027, and 200028.',
      category: 'EFILE'
    },
    '2000012': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 200030, whether the location of books and records has changed since the last time the Department was notified.',
      category: 'EFILE'
    },
    '2000013': {
      label: 'The corporation has answered Yes (1) at line 200030, but has not provided the location of books and records information required (where applicable) at lines 200031, 200032, 200035, 200036, 200037, and 200038.',
      category: 'EFILE'
    },
    '2000014': {
      label: 'The corporation has answered Yes (1) at line 200030, but has not provided complete location of books and records information required (where applicable) at lines 200031, 200032, 200035, 200036, 200037, and 200038.',
      category: 'EFILE'
    },
    '2000016': {
      label: 'The tax year start and tax year-end have not been provided at lines 200060 and 200061.',
      category: 'EFILE'
    },
    '2000017': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 200070, whether this is the first year of filing after incorporation.',
      category: 'EFILE'
    },
    '2000018': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 200071, whether this is the first year of filing after amalgamation.',
      category: 'EFILE'
    },
    '2000024': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 200078, whether this is the final return up to dissolution.',
      category: 'EFILE'
    },
    '2000051': {
      label: 'The corporation has answered Yes (1) at line 200070 or 200071, but has not answered Yes (1) at line 200030 or there is no entry at one or more of lines 200031, 200035, 200036, 200037, or 200038.',
      category: 'EFILE'
    },
    '2000025': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 200063, whether control of the corporation has changed from the previous tax year.',
      category: 'EFILE'
    },
    '2000026': {
      label: 'The corporation has answered Yes (1) at line 200063, but has not provided the date the control was changed at line 200065.',
      category: 'EFILE'
    },
    '2000027': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 200067, whether it is a professional corporation that is a member of a partnership.',
      category: 'EFILE'
    },
    '2000028': {
      label: 'The corporation has answered No (2) at line 200957, but has not provided the contact name and telephone number at lines 200958 and 200959.',
      category: 'EFILE'
    },
    '2000029': {
      label: 'The corporation has not indicated its language of correspondence at line 200990.',
      category: 'EFILE'
    },
    '2000030': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 200280.',
      category: 'EFILE'
    },
    '2000031': {
      label: 'The corporation has not provided complete information for Direct Deposit at lines 200910, 200914, and 200918.',
      category: 'EFILE'
    },
    '2000032': {
      label: 'The corporation has not provided complete certification information at lines 200950, 200951, 200954, 200955, 200956, 200957.',
      category: 'EFILE'
    },
    '2000033': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 200080, whether it is a resident of Canada.',
      category: 'EFILE'
    },
    '2000035': {
      label: 'The corporation has indicated more than one exception from tax under section 149 of the Income Tax Act at line 200085.',
      category: 'EFILE'
    },
    '2000036': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 200072, whether there has been a wind-up of a wholly owned subsidiary during the current tax year.',
      category: 'EFILE'
    },
    '2000038': {
      label: 'The corporation has not provided the tax year start and end at lines 200060 and 200061, in a valid date format.',
      category: 'EFILE'
    },
    '2000039': {
      label: 'The tax year end entered at line 200061 is prior to 2014-01-01.',
      category: 'EFILE'
    },
    '2000040': {
      label: 'A return with the same tax year end has already been filed.',
      category: 'EFILE'
    },
    '2000041': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 200076, whether this is the final tax year before amalgamation.',
      category: 'EFILE'
    },
    '2000045': {
      label: 'The corporation has answered No (2) at line 200080 regarding Canadian residency, but has not provided its country of residence at line 200081.',
      category: 'EFILE'
    },
    '2000046': {
      label: 'The corporation has made a request for direct deposit, but has not provided all the required information at lines 200910, 20914, and 200918.',
      category: 'EFILE'
    },
    '2000047': {
      label: 'Return not acceptable, early filed.',
      category: 'EFILE'
    },
    '2000088': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 200088.',
      category: 'EFILE'
    },
    '2000100': {
      label: 'The type of corporation at the end of the tax year has not been provided at line 200040.',
      category: 'EFILE'
    },
    '2000150': {
      label: 'The corporation has not provided the net income or loss for income tax purposes at line 200300.',
      category: 'EFILE'
    },
    '2000270': {
      label: 'The corporation has not answered Yes (1) or No (2) at line 200270.',
      category: 'EFILE'
    },
    '2000299': {
      label: 'The NAICS code has not been indicated at line 200299.',
      category: 'EFILE'
    },
    '2000300': {
      label: 'The name of the province or territory where the corporation earned its income has not been indicated at line 200750.',
      category: 'EFILE'
    },
    '2000801': {
      label: 'The corporation has claimed tax withheld at source at line 200800, but did not provide the total payments on which tax has been withheld.',
      category: 'EFILE'
    },
    '2000920': {
      label: 'The corporation has reported an amount in one or more of columns 200490, 200500, or 200505, but there are not a corresponding and equal number of entries in columns 200490, 200500, and 200505.',
      category: 'EFILE'
    },
    '2000997': {
      label: 'The corporation has indicated that the return is an amended return at line 200997, but did not provide the description of changes at line 200996.',
      category: 'EFILE'
    },
    '2001000': {
      // label: 'The corporation has answered No (2) at line 200063 and the tax year provided at lines 200060 and 200061 is greater than 371 days.',
      //  label: 'The corporation has indicated an acquisition of control and the tax year is greater than 378 days.',
      category: 'EFILE'
    },
    '2001001': {
      label: 'The tax year start date provided at line 200060 is not immediately after the tax year-end date of the immediately preceding tax year.',
      category: 'EFILE'
    },
    '2001002': {
      label: 'The tax year-end date provided at line 200061 is not within 7 days of the day and the month of the preceding tax year end date, nor does it agree with the date of dissolution, amalgamation, bankruptcy or acquisition of control in our records.',
      category: 'EFILE'
    },
    '2001003': {
      label: 'The tax year start date provided at line 200060 precedes the date of incorporation or amalgamation in our records.',
      category: 'EFILE'
    },
    '2001007': {
      label: 'The corporation has answered Yes (1) at line 200067 that it is a professional corporation that is a member of a partnership, but the tax year-end is not December 31.',
      category: 'EFILE'
    },
    '2001010': {
      label: 'The corporation has answered Yes (1) at line 200070 or 200071 that this is the first year of filing after incorporation or amalgamation, but a return has been filed/assessed for a prior tax year.',
      category: 'EFILE',
      severity: 'none'
    },
    '2001011': {
      label: 'The corporation has answered No (2) at line 200070 or 200071 that this is not the first year of filing after incorporation or amalgamation, but there are no returns filed/assessed for prior tax years.',
      category: 'EFILE',
      severity: 'warning'
    },
    '2001015': {
      label: 'The corporation has answered Yes (1) at line 200076 that this is the final tax year before amalgamation, but the tax year end date provided at line 200061 does not agree with the effective date of amalgamation in our records.',
      category: 'EFILE'
    },
    '2001018': {
      label: 'The Business number provided at line 200001 is not valid.',
      category: 'EFILE'
    },
    '2001019': {
      label: 'The tax year provided at lines 200060 and 200061 spans three calendar years.',
      category: 'EFILE'
    },
    '2001020': {
      label: 'The corporation has answered Yes (1) at line 200078 that this is the final tax year before dissolution, but we have no record of the dissolution.',
      category: 'EFILE',
      severity: 'none'
    },
    '2001102': {
      label: 'Our records indicate that the corporation type provided at line 200040 is different from that of the previous tax year.  Therefore, the effective date of the change must be provided at line 200043.',
      category: 'EFILE'
    },
    '2001104': {
      label: 'The effective date of the change of corporation type provided at line 200043 is not within the tax year provided at lines 200060 and 200061.',
      category: 'EFILE'
    },
    '2001268': {
      label: 'The corporation has answered Yes (1) at line 200266, but the corporation is not a CCPC, line 200040 is equal to 2, 3, 4, or 5.',
      category: 'EFILE'
    },
    // efile specs 2016-2
    // '2005050': {
    //   label : 'According to our records, the corporation is a life insurance corporation.  Life insurance corporations cannot submit returns electronically.',
    //   category : 'EFILE'
    // },
    // '2005051': {
    //   label : 'According to our records, the corporation is a deposit insurance corporation.  Deposit insurance corporations cannot submit returns electronically.',
    //   category : 'EFILE'
    // },
    // '2005052': {
    //   label : 'According to our records, the corporation is a general insurance corporation.  General insurance corporations cannot submit returns electronically.',
    //   category : 'EFILE'
    // },
    // '2005053': {
    //   label : 'The corporation has indicated at line 024100 that it is a life insurance corporation. Life insurance corporations cannot submit returns electronically.',
    //   category : 'EFILE'
    // },
    // '2005054': {
    //   label : 'The corporation has indicated at line 024100 that it is a deposit insurance corporation.  Deposit insurance corporations cannot submit returns electronically.',
    //   category : 'EFILE'
    // },
    // '2005055': {
    //   label : 'The corporation has indicated at line 024100 that it is a general insurance corporation.  General insurance corporations cannot submit returns electronically.',
    //   category : 'EFILE'
    // },
    //T2S1
    '0010001': {
      label: 'The corporation has answered YES at line 200201, but Schedule 1 is not completed.',
      category: 'EFILE'
    },
    '0010002': {
      label: 'The net income (loss) on the financial statements at line 1259999 does not equal the entry at line 200300 and Schedule 1 has not been completed, and the type of corporation at line 200040 is other than 2.',
      category: 'EFILE'
    },

    //T2S2
    '0020001': {
      label: 'The corporation has answered YES at line 200202, but Schedule 2 is not completed.',
      category: 'EFILE'
    },
    '0020002': {
      label: 'The corporation is claiming charitable donations at line 200311, but there is no entry at lines 002210, 002240, and 002250.',
      category: 'EFILE'
    },
    // '0020003': {
    //   label: 'The corporation is claiming gifts to Canada or a province at line 200312, but there is no entry at lines 002310, 002340, and 002350.',
    //   category: 'EFILE'
    // },
    '0020004': {
      label: 'The corporation is claiming cultural gifts at line 200313, but there is no entry at lines 002410, 002440, and 002450.',
      category: 'EFILE'
    },
    '0020005': {
      label: 'The corporation is claiming ecological gifts at line 200314, but there is no entry at lines 002510, 002520, 002540, and 002550.',
      category: 'EFILE'
    },
    '0020006': {
      label: 'The corporation is claiming an additional deduction for gifts of medicine at line 200315, but there is no entry at lines 002610, 002640, and 002650.',
      category: 'EFILE'
    },

    //T2S3
    '0030001': {
      label: 'The corporation is claiming -non-taxable dividend under section 83 at line 001402; or - taxable dividends deductible under section 112 or 113; or subsection 138(6) at line 200320; or - a dividend refund at line 200784; but Schedule 3 is not completed.',
      category: 'EFILE'
    },
    '0030002': {
      label: 'The corporation is reporting a non-taxable dividend under section 83 at line 001402, but there is no entry in column 003230.',
      category: 'EFILE'
    },
    '0030003': {
      label: 'The corporation is reporting taxable dividends deductible under section 112 or 113, or subsection 138(6) at line 200320, but there is no entry in column 003240.',
      category: 'EFILE'
    },
    '0030004': {
      label: 'The corporation is reporting Part IV tax payable at line 200712, but there is no entry in column 003240.',
      category: 'EFILE'
    },
    '0030005': {
      label: 'The corporation is claiming a dividend refund at line 200784, but there is no entry in column 003430 and at line 003450.',
      category: 'EFILE'
    },
    '0030006': {
      label: 'The corporation is reporting a non-taxable dividend under section 83 in column 003230, but for the same row there is no corresponding entry in columns 003200 and/or 003210.',
      category: 'EFILE'
    },
    '0030007': {
      label: 'The corporation is reporting taxable dividends deductible from taxable income in column 003240 and the answer in column 003205 is Yes (1), but for the same row there is no corresponding entry at one or more of columns 003200, 003210, 003220, or 003250.',
      category: 'EFILE'
    },
    '0030008': {
      label: 'The corporation is reporting taxable dividends deductible from taxable income in column 003240 and the answer in column 003205 is No (blank), but for the same row there is no corresponding entry in column 003200.',
      category: 'EFILE'
    },
    '0030009': {
      label: 'The corporation is reporting taxable dividends paid to connected corporation in column 003430, but for the same row there is no corresponding entry in one or more of columns 003400, 003410, or 003420.',
      category: 'EFILE'
    },
    '0030010': {
      label: 'The corporation is reporting taxable dividends paid in the taxation year at line 003460, but there is no entry in column 003430 and at line 003450.',
      category: 'EFILE'
    },

    //T2S4
    '0040001': {
      label: 'The corporation has claimed an amount at one or more of lines 200331, 200332, 200333, 200334, 200335, 003335, 003345, or 006655, but Schedule 4 is not completed.',
      category: 'EFILE'
    },
    '0040002': {
      label: 'The corporation has answered YES at line 200204, but Schedule 4 is not completed.',
      category: 'EFILE'
    },
    '0040003': {
      label: 'The corporation has reported a limited partnership loss in column 004604, but for the same row there is no corresponding entry in columns 004600 and/or 004602.',
      category: 'EFILE'
    },
    '0040004': {
      label: 'There is a limited partnership loss reported in column 004675, but there are no corresponding entries in columns 004632, 004636, or 004638 for the same partnership as indicated in columns 004630 and 004660.',
      category: 'EFILE'
    },
    '0040010': {
      category: 'EFILE'
    },

    //'0040010': {
    //  label: 'There is an entry in one of columns 004634, 004636, 004638 or 04650, but for the same row there is no entry in column 004630.',
    //  category: 'EFILE'
    //},
    //'0040010': {
    //  label: 'There is an entry in one of columns 004662, 004664, 004670, 004675 or 04680, but for the same row there is no entry in column 004660.',
    //  category: 'EFILE'
    //},

    //T2S5
    '0050010': {
      label: 'The corporation has indicated that the jurisdiction is MJ at line 200750, but Schedule 5 is not completed.',
      category: 'EFILE'
    },
    '0050020': {
      label: 'The corporation has indicated it has multiple jurisdictions at line 200750, but the Regulation number is blank (or invalid) at line 005100.',
      category: 'EFILE'
    },
    '0050021': {
      label: 'The corporation has indicated it is a chartered bank, but either the Regulation provided at line 005100 is incorrect or entries are missing for either salaries or wages or loans and deposits on Part 1 of Schedule 5.',
      category: 'EFILE'
    },
    '0050022': {
      label: 'The corporation has indicated it is an airline corporation, but either the Regulation provided at line 005100 is incorrect or entries are missing for the capital cost of fixed assets or revenue plane miles on Part 1 of Schedule 5.',
      category: 'EFILE'
    },
    '0050025': {
      label: 'Total salaries and wages paid in a jurisdiction, or gross revenue attributable to a jurisdiction have been reported but a permanent establishment has not been indicated for the jurisdiction.',
      category: 'EFILE'
    },
    '0050030': {
      label: 'The corporation is claiming Provincial and territorial refundable tax credits at line 200812, but there is no entry at all of the provincial and territorial refundable tax credit lines on Schedule 5.',
      category: 'EFILE'
    },
    '0050150': {
      label: 'The corporation is claiming a provincial political contribution tax credit on Schedule 5, but the amount of the contribution is not reported.',
      category: 'EFILE'
    },
    '0050500': {
      label: 'The corporation is claiming the Nova Scotia corporate tax reduction for new small businesses at line 005556, but the certificate number is not provided at line 005834.',
      category: 'EFILE'
    },
    '0050503': {
      label: 'As the corporation is claiming the Nova Scotia film industry tax credit at line 005565, the Nova Scotia authorization certificate must be filed.',
      category: 'EFILE'
    },
    '0050523': {
      label: 'As the corporation is claiming the Newfoundland and Labrador film and video industry tax credit at line 005521, the film and video industry tax credit certificate must be filed.',
      category: 'EFILE'
    },
    '0050525': {
      label: 'The corporation is claiming the Newfoundland and Labrador film and video industry tax credit at line 005521, but the certificate number is not provided at line 005821 or in column 302100.',
      category: 'EFILE'
    },
    '0050530': {
      label: 'The corporation is claiming the New Brunswick film tax credit at line 005595, but the certificate number is not provided at line 005850 or in column 365100.',
      category: 'EFILE'
    },
    '0050532': {
      label: 'As the corporation is claiming the New Brunswick film tax credit at line 005595, the New Brunswick authorization certificate must be filed.',
      category: 'EFILE'
    },
    '0050580': {
      label: 'The corporation is claiming the British Columbia small business venture capital tax credit at line 005656, but there is no entry at lines 005880 and 005881.',
      category: 'EFILE'
    },
    '0050590': {
      label: 'The corporation has reported a British Columbia small business venture capital tax credit at line 005881, but the certificate number is not provided at line 005882.',
      category: 'EFILE'
    },
    '0050600': {
      label: 'The corporation is claiming the Manitoba film and video production tax credit at line 005620, but there is no entry at line 388900.',
      category: 'EFILE'
    },
    '0050665': {
      label: 'The corporation is claiming the British Columbia book publishing tax credit at line 005665, but there is no entry at line 005886.',
      category: 'EFILE'
    },
    '0050841': {
      label: 'The corporation is claiming the Nova Scotia capital investment tax credit at line 005568, but the certificate number is not provided at line 005841.',
      category: 'EFILE'
    },

    //T2S6
    '0060001': {
      label: 'The corporation has answered YES at line 200206, but Schedule 6 is not completed.',
      category: 'EFILE'
    },
    '0060002': {
      label: 'The corporation has reported taxable capital gains at line 001113, but there is no entry in all of lines 006120, 006220, 006320, 006420, 006520, 006620, 006875, 006880, and 006899, and (006897 and/or 006898).',
      category: 'EFILE'
    },
    '0060003': {
      label: 'The corporation has claimed allowable business investment losses at line 001406, but there is no entry in column 006930 and 006940.',
      category: 'EFILE'
    },
    '0060004': {
      label: 'The corporation has claimed unapplied listed personal property losses from prior years in column 006655, but there is no entry in column 006620.',
      category: 'EFILE'
    },
    '0060005': {
      label: 'The corporation has claimed current year capital losses at line 004210, but there is no entry in all of lines 006130, 006140, 006230, 006240, 006330, 006340, 006430, 006440, 006885, and 006901.',
      category: 'EFILE'
    },
    '0060006': {
      label: 'The corporation has claimed current year listed personal property losses at line 004510, but there is no entry in column 006630 and 006640.',
      category: 'EFILE'
    },

    //T2S7
    '0070001': {
      label: 'The corporation has answered YES at line 200207 and is claiming the Small business deduction at line 200430, but there are no entries at all of lines 007002, 007032, 007082, 007400, 007450, 007500, 007520, 007530, 007540, 007615, and 007625.',
      category: 'EFILE'
    },
    '0070002': {
      label: 'The corporation has claimed the Refundable portion of Part I Tax at line 200450 and has reported Foreign investment income at line 200445, but there is no entry at line 007001 and 007019.',
      category: 'EFILE'
    },
    '0070003': {
      label: 'The corporation has claimed the Refundable portion of Part I Tax at line 200450, but there is no entry at line 007002 and 007032.',
      category: 'EFILE'
    },
    '0070004': {
      label: 'The corporation is claiming the Small business deduction at line 200430 and has reported amounts at lines 007380, 007385, 007400, or 007450, but there are not a corresponding and equal number of entries in columns: -007200, 007300, 007310, 007320, 007325, 007330, and 007340; or -007200, 007311, 007320 and 007340',
      category: 'EFILE'
    },
    '0070007': {
      label: 'Schedule 053 has been completed for General Rate Income Pool (GRIP) calculation, but Schedule 007 is not completed.',
      category: 'EFILE'
    },
    '0070008': {
      label: 'The corporation has reported an amount in columns 007335 and/or 007420, but there are not a corresponding and equal number of entries in columns 007405, 007406, (007410, 007411, or 007412), 007415, 007416, and 007420, or there is no entry in column 007335.',
      category: 'EFILE'
    },
    '0070009': {
      label: 'The corporation has reported an amount at columns 007336 and/or 007440, but there are not a corresponding and equal number of entries in columns 007425, 007426, 007430, 007435, 007436, and 007440, or there is no entry in column 007336.',
      category: 'EFILE'
    },
    '0070010': {
      label: 'There is an entry in column 007600, but for the same row there is no corresponding entry in columns 007610 and 007620.',
      category: 'EFILE'
    },
    '0071023': {
      label: 'The amount of corporation’s share at line 007310 is greater than or equal to the total income(loss) of partnership from an active business at line 007300.',
      category: 'EFILE'
    },

    //T2S8
    '0080001': {
      label: 'The corporation has answered YES at line 200208, but Schedule 8 is not completed.',
      category: 'EFILE'
    },
    '0080002': {
      label: 'The corporation has claimed capital cost allowance at line 001403, but there is no entry in column 008217.',
      category: 'EFILE'
    },
    '0080003': {
      label: 'The corporation has claimed terminal loss at line 001404, but there is no entry in column 008215.',
      category: 'EFILE'
    },
    '0080004': {
      label: 'The corporation has reported recapture of capital cost allowance at line 001107, but there is no entry in column 008213.',
      category: 'EFILE'
    },
    '0080005': {
      label: 'For each entry in columns 008201, 008203, 008205, 008207, 008211, 008212, 008213, 008215, 008217, or 008220 a corresponding entry is required in column 008200.',
      category: 'EFILE'
    },
    '0080006': {
      label: 'For each capital cost allowance claim in column 008217, a corresponding entry is required in column 008201, 008203, or 008205.',
      category: 'EFILE'
    },
    '0080007': {
      label: 'For each terminal loss claim in column 008215, a corresponding entry is required in column 008201, 008203, or 008205.',
      category: 'EFILE'
    },
    '0080008': {
      label: 'For each recapture of capital cost allowance claim in column 008213, a corresponding entry is required in column 008207.',
      category: 'EFILE'
    },
    '0080009': {
      label: 'For each 50% rule entry in column 008211, a corresponding entry is required in column 008203 and/or 008205.',
      category: 'EFILE'
    },
    '0080200': {
      label: 'The corporation has reported the class number 43.1 and/or 43.2 in column 008200 and for the same row there is an entry greater than zero in column 008203, but there is no entry in one or more of columns 008300, 008301, 008302, or 008303.',
      category: 'EFILE'
    },
    '0080300': {
      label: 'There is an entry in column 008300, but for the same row, there is no corresponding entry in one or more of columns 008301, 008302, or 008303.',
      category: 'EFILE'
    },
    '0080303': {
      label: 'There is an entry in column 008303, but the total of the percentage allocated to all assets of class number 43.1 for the same row number in column 008300 is not equal to 100 and/or the total of the percentage allocated to all assets of class number 43.2 for the same row number in column 008300 is not equal to 100.',
      category: 'EFILE'
    },

    //T2S9
    '0090001': {
      label: 'The corporation has answered YES at line 200150, but Schedule 9 is not completed.',
      category: 'EFILE'
    },
    '0090002': {
      category: 'EFILE'
    },
    //'0090002': {
    //  label: 'For each entry in column 009100, there must be corresponding entries in columns 009200 or 009300, and 009400.',
    //  category: 'EFILE'
    //},
    //'0090002': {
    //  label: 'For each entry in column 009500 or 009550, there must be a corresponding entry in the other column.',
    //  category: 'EFILE'
    //},
    //'0090002': {
    //  label: 'For each entry in column 009600 or 009650, there must be a corresponding entry in the other column.',
    //  category: 'EFILE'
    //},
    //'0090002': {
    //  label: 'For each entry in column 009500, 009550, 009600 or 009650, there must be a corresponding entry in column 009700.',
    //  category: 'EFILE'
    //},
    //'0090002': {
    //  label: 'For each entry in column 009500, 009550, 009600, 009650 or 009700, there must be corresponding entry in column 009100.',
    //  category: 'EFILE'
    //},

    //T2S10
    '0100001': {
      label: 'The corporation has answered YES at line 200210, but Schedule 10 is not completed.',
      category: 'EFILE'
    },
    '0100002': {
      label: 'The corporation has claimed cumulative eligible capital deduction at line 001405, but there is no entry in all of lines 010200, 010222, 010224, 010226, and 010228.',
      category: 'EFILE'
    },
    '0100003': {
      label: 'The corporation has reported a gain on sale of eligible capital property at line 001108, but there is no entry in all of lines 010242, 010244, and 010246.',
      category: 'EFILE'
    },

    //T2S11
    '0110001': {
      label: 'The corporation has answered YES at line 200162, but Schedule 11 is not completed.',
      category: 'EFILE'
    },
    '0110002': {
      category: 'EFILE'
    },

    //T2S12
    '0120001': {
      label: 'The corporation has claimed depletion at line 001344, but there is no entry at line 012115, 012140, or 012170.',
      category: 'EFILE'
    },
    '0120002': {
      label: 'The corporation has claimed Canadian exploration expenses at line 001341, but there is no entry at line 012245 or 012295.',
      category: 'EFILE'
    },
    '0120003': {
      label: 'The corporation has claimed Canadian development expenses at line 001340, but there is no entry at line 012345 or 012395.',
      category: 'EFILE'
    },
    '0120004': {
      label: 'The corporation has claimed foreign exploration and development and/or resource expenses at line 001345, but there are no entries at lines 012520, 012570, 012620, 012670, 012720, and 012770.',
      category: 'EFILE'
    },
    '0120005': {
      label: 'The corporation has claimed Canadian oil and gas property expenses at line 001342, but there is no entry at line 012445 or 012495.',
      category: 'EFILE'
    },
    '0120006': {
      label: 'The corporation has claimed Earned depletion allowance at line 012115, but there is no entry at line 012101 or 012105.',
      category: 'EFILE'
    },
    '0120007': {
      label: 'The corporation has claimed Earned depletion allowance at line 012140, but there is no entry at line 012126, 012130, or 012132.',
      category: 'EFILE'
    },
    '0120010': {
      label: 'The corporation has claimed Mining exploration depletion at line 012170, but there is no entry at line 012150, 012155, or 012160.',
      category: 'EFILE'
    },
    '0120012': {
      label: 'The corporation has claimed Canadian exploration expenses at line 012245, but there is no entry at any of lines 012200, 012205, 012206, 012210, 012215, 012217, or 012220.',
      category: 'EFILE'
    },
    '0120013': {
      label: 'The corporation has claimed Canadian exploration expenses at line 012295, but there is no entry at any of lines 012250, 012255, 012260, or 012265.',
      category: 'EFILE'
    },
    '0120016': {
      label: 'The corporation has claimed Canadian development expenses at line 012345, but there is no entry at any of lines 012300, 012303, 012304, 012305, or 012310.',
      category: 'EFILE'
    },
    '0120017': {
      label: 'The corporation has claimed Canadian development expenses at line 012395, but there is no entry at any of lines 012350, 012355, or 012357.',
      category: 'EFILE'
    },
    '0120020': {
      label: 'The corporation has claimed Canadian oil and gas property expenses at line 012445, but there is no entry at any of lines 012400, 012405, 012410, or 012415.',
      category: 'EFILE'
    },
    '0120021': {
      label: 'The corporation has claimed Canadian oil and gas property expenses at line 012495, but there is no entry at any of lines 012450, 012455, or 012460.',
      category: 'EFILE'
    },
    '0120024': {
      label: 'The corporation has claimed Foreign exploration and development expenses at line 012520, but there is no entry at line 012500 or 012510.',
      category: 'EFILE'
    },
    '0120025': {
      label: 'The corporation has claimed Foreign exploration and development expenses at line 012570, but there is no entry at line 012550, 012555, or 012560.',
      category: 'EFILE'
    },
    '0120026': {
      label: 'The corporation has answered YES at line 200212, but Schedule 12 is not completed.',
      category: 'EFILE'
    },
    '0120027': {
      label: 'The corporation has claimed specified foreign exploration and development regular expenses in column 012620, but there is no entry at any of columns 012600, 012610, or 012611',
      category: 'EFILE'
    },
    '0120028': {
      label: 'The corporation has claimed specified foreign exploration and development successor expenses in column 012670, but there is no entry at any of columns 012650, 012655, or 012660',
      category: 'EFILE'
    },
    '0120029': {
      label: 'The corporation has claimed foreign resource regular expenses in column 012720, but there is no entry at any of columns 012700, 0127505, 012710, or 012711 ',
      category: 'EFILE'
    },
    '0120030': {
      label: 'The corporation has claimed foreign resource successor expenses in column 012770, but there is no entry at any of columns 012750, 012755, or 012760.',
      category: 'EFILE'
    },

    //T2S13
    '0130001': {
      label: 'The corporation has answered YES at line 200213, but Schedule 13 is not completed.',
      category: 'EFILE'
    },
    '0130005': {
      label: 'The corporation has reported capital gains reserve opening balance at line 006880, but there is no entry at lines 013001 and/or (013002 and 013003).',
      category: 'EFILE'
    },
    '0130010': {
      label: 'The corporation has reported capital gains reserve closing balance at line 006885, but there is no entry at lines 013001 and/or 013004.',
      category: 'EFILE'
    },
    '0130015': {
      label: 'The corporation has reported other reserves at line 001125, but there is no entry at lines 013110, 013115, 013130, 013135, 013150, 013155, 013190, 013195, 013210, 013215, 013230, and 013235.',
      category: 'EFILE'
    },
    '0130020': {
      label: 'The corporation has claimed other reserves at line 001413, but there is no entry at lines 013120, 013140, 013160, 013200, 013220, and 013240.',
      category: 'EFILE'
    },
    '0130025': {
      category: 'EFILE'
    },
    //'0130025': {
    //  label: 'For each entry in columns 013002, 013003, or 013004, a corresponding entry is required in column 013001.',
    //  category: 'EFILE'
    //},
    //'0130025': {
    //  label: 'For each entry in column 013001, a corresponding entry is required in columns 013002, 013003, or 013004.',
    //  category: 'EFILE'
    //},

    //T2S14
    '0140001': {
      label: 'The corporation has answered YES at line 200164, but Schedule 14 is not completed.',
      category: 'EFILE'
    },
    '0140002': {
      category: 'EFILE'
    },
    //'0140002': {
    //  label: 'For each and every occurrence of the name of the recipient in column 014100, there must be a corresponding address in column 014200, and at least one entry at either column 014300, 014400, 014500, 014600, or 014700.',
    //  category: 'EFILE'
    //},
    //'0140002': {
    //  label: 'For each and every occurrence at either column 014300, 014400, 014500, 014600, or 014700, there must be a name and address of recipient in columns 014100 and 014200.',
    //  category: 'EFILE'
    //},

    //T2S15
    '0150001': {
      label: 'The corporation has answered YES at line 200165, but Schedule 15 is not completed.',
      category: 'EFILE'
    },
    //'0150002': {
    //  label: 'For each type of plan identified in column 015100, an entry is required in column 015200 and for every entry in column 015200; a type of plan is required in column 015100.',
    //  category: 'EFILE'
    //},
    //'0150002': {
    //  label: 'There is an entry of a 1 (RPP), 2 (RSUBP), 3 (DPSP), or 5 (PRPP) in column 015100, but for the same row there is no corresponding entry in columns 015200 and/or 015300.',
    //  category: 'EFILE'
    //},
    //'0150002': {
    //  label: 'There is an entry of a 4 (EPSP) in column 015100, but for the same row there is no corresponding entry in one or more of columns 015200, 015400, 015500, or 015600.',
    //  category: 'EFILE'
    //},

    //T216
    '0160001': {
      label: 'The corporation is claiming a Patronage dividend deduction at line 001416, but Schedule 16 is not completed.',
      category: 'EFILE'
    },
    '0160005': {
      label: 'The corporation has reported amounts at one or more of lines 001416, 016109, 016110, 016111, or 016113, but there is no entry at any of lines 016101, 016102, 016104, or 016105.',
      category: 'EFILE'
    },
    '0160010': {
      label: 'The corporation has answered No (2) at line 016200 and has reported a Patronage dividend deduction for the current year payments at line 016113, but there is no entry at lines 016110 and 016111.',
      category: 'EFILE'
    },
    '0160015': {
      label: 'The corporation has indicated that the active business income before the patronage dividend deduction is different from net income before patronage dividend deduction, but there is no entry at lines 016118, 016119, and 016120.',
      category: 'EFILE'
    },
    '0160020': {
      label: 'The corporation has reported amounts at one or more of lines 016101, 016102, 016104, or 016105, but there is no entry at line 016200.',
      category: 'EFILE'
    },
    '0160021': {
      label: 'The corporation has answered Yes (1) at line 016150, but there is no entry at line 016209.',
      category: 'EFILE'
    },
    '0160025': {
      label: 'There is an entry greater than zero at line 016119, but there is no entry at lines 016110 or 016111 in order to determine Percentage C.',
      category: 'EFILE'
    },

    //T2S17
    '0170001': {
      label: 'The corporation has answered YES at line 200217, but Schedule 17 is not completed.',
      category: 'EFILE'
    },
    '0170002': {
      label: 'The corporation is claiming a deduction for payments made pursuant to allocations in proportion to borrowing and bonus interest payment at line 001315, but there is no entry in one or more of columns 017100, 017200, or 017300, and in one or more of columns 017110, 017210, or 017310.',
      category: 'EFILE'
    },
    '0170003': {
      label: 'The corporation is reporting an allocation in proportion to borrowing in column 017300, but for the same row there is no corresponding entry in columns 017100 and/or 017200.',
      category: 'EFILE'
    },
    '0170004': {
      label: 'The corporation is reporting bonus interest payments in column 017310, but for the same row there is no corresponding entry in columns 017110 and/or 017210.',
      category: 'EFILE'
    },
    '0170005': {
      label: 'The corporation is claiming an additional deduction for credit unions at line 200628, but there is no entry at line 017600.',
      category: 'EFILE'
    },
    '0170006': {
      label: 'The corporation is claiming an additional deduction for credit unions at line 200628 and the corporation is not a first time filer, but there is no entry at line 017700.',
      category: 'EFILE'
    },
    '0170007': {
      label: 'The corporation is claiming an additional deduction for credit unions at line 200628 and has answered Yes (1) at line 200071, but there is no entry at line 017750.',
      category: 'EFILE'
    },

    //T2S18
    '2000788': {
      label: 'The corporation is claiming a federal capital gains refund at line 200788, but Schedule 18 is not completed.',
      category: 'EFILE'
    },
    '2000808': {
      label: 'The corporation is claiming a provincial and territorial capital gains refund at line 200808, but there is no entry at line 018200, 018260, and 018262.',
      category: 'EFILE'
    },
    '0180810': {
      label: 'The corporation is claiming an Ontario capital gains refund, but there is no entry at lines: - 018162 and 018164; and - 018169; and - 018180.',
      category: 'EFILE'
    },

    //T2S19
    '0190001': {
      label: 'The corporation has answered YES at line 200151, but Schedule 19 is not completed.',
      category: 'EFILE'
    },
    '0190002': {
      label: 'For each class of shares identified in column 019100 a percentage owned by non-residents is required in column 019200, and for each percentage owned by non-residents reported in column 019200 a class of shares is required in column 019100.',
      category: 'EFILE'
    },
    '0190010': {
      label: 'The corporation has not provided an overall percentage of voting shares owned by non-residents at line 019300.',
      category: 'EFILE'
    },

    //T2S20
    //'0200001': {
    //  label: 'The corporation is subject to Part XIV, but Schedule 20 is not completed.',
    //  category: 'EFILE'
    //},
    //'0200001': {
    //  label: 'The corporation is subject to Part XIV, but Schedule 20 is not completed.',
    //  category: 'EFILE'
    //},
    //'0200001': {
    //  label: 'The corporation is subject to Part XIV, but Schedule 20 is not completed.',
    //  category: 'EFILE'
    //},

    //T2S21
    '0210001': {
      label: 'The corporation has reported amounts at one or more of lines 001407, 005408, 200355, 200632, 200636, 200640, or 510550, but Schedule 21 is not completed.',
      category: 'EFILE'
    },
    '0210005': {
      label: 'The corporation is claiming a Federal foreign non-business income tax credit at line 200632, but there is no entry in column 021120.',
      category: 'EFILE'
    },
    '0210010': {
      label: 'The corporation is claiming a Foreign non-business tax deduction at line 001407, but there is no entry in column 021130.',
      category: 'EFILE'
    },
    '0210015': {
      label: 'The corporation is claiming a Federal foreign business income tax credit at line 200636, but there is no entry in column 021220 and 021230.',
      category: 'EFILE'
    },
    '0210020': {
      label: 'The corporation is claiming a Federal logging tax credit at line 200640, but there is no entry at lines 021500 and/or 021510, and there is no entry 021520 and/or 021530.',
      category: 'EFILE'
    },
    '0210030': {
      label: 'The corporation has reported Foreign non-business income tax paid in the year in column 021120, but there is no corresponding entry in columns 021100 and/or 021110.',
      category: 'EFILE'
    },
    '0210035': {
      label: 'The corporation has reported Foreign non-business income tax deduction in column 021130, but there is no corresponding entry in one or more of columns 021100, 021110, or 021120.',
      category: 'EFILE'
    },
    '0210040': {
      label: 'The corporation has reported Foreign business income tax paid in column 021220 or Unused foreign business income tax credit in column 021230, but there is no corresponding entry in columns 021200 and/or 021210.',
      category: 'EFILE'
    },
    '0210045': {
      label: 'The corporation is claiming a Foreign business income tax credit carry-back in one or more of columns 021901, 021902 or 021903, but the corresponding country code in column 021900 is either missing or does not match any country code in column 021200.',
      category: 'EFILE'
    },
    '0210050': {
      label: 'The country code is missing in column 021345.',
      category: 'EFILE'
    },

    //T2S22
    '0220001': {
      label: 'The corporation has answered YES at line 200168, but Schedule 22 is not completed.',
      category: 'EFILE'
    },
    '0220002': {
      label: 'For each name of a non-resident discretionary trust listed in column 022100, corresponding entry is required in columns 022200 and 022300 for a mailing address and name of trustee.',
      category: 'EFILE'
    },

    //T2S23
    '0230002': {
      label: 'The corporation has indicated that it is an associated Canadian-controlled private throughout the tax year that is claiming the one-month extension of the date that the balance of tax is due or claiming the small business deduction, but Schedule 23 (master agreement) is not completed or the filing corporation is not listed on the Schedule 23.',
      category: 'EFILE'
    },
    '0230150': {
      label: 'There are not a corresponding and equal number of entries in columns 023100, 023200, and 023300, or column 023400 is blank.',
      category: 'EFILE'
    },
    '0230010': {
      label: 'The calendar year to which the agreement applies at line 023050 is not provided or does not match calendar year portion (YYYY) at line 200061.',
      category: 'EFILE'
    },
    '0231012': {
      label: 'The same Business number (BN) is entered more than once in column 023200.',
      category: 'EFILE'
    },

    //T2S24
    '0240001': {
      label: 'The corporation has answered Yes (1) at line 200070, but did not indicate the type of operation at line 024100.',
      category: 'EFILE'
    },
    '0240002': {
      label: 'The corporation has answered Yes (1) at line 200070, but Schedule 101 is not completed.',
      category: 'EFILE'
    },
    '0240003': {
      label: 'The corporation has answered Yes (1) at line 200071, but Schedule 24 is not completed.',
      category: 'EFILE'
    },
    '0240005': {
      label: 'The corporation has answered Yes (1) at line 200071, but there is no entry in columns 024200 and/or 024300, or there are not a corresponding and equal number of entries in columns 024200 and 024300.',
      category: 'EFILE'
    },
    '0240006': {
      label: 'The corporation has answered Yes (1) at line 200072, but Schedule 24 is not completed.',
      category: 'EFILE'
    },
    '0240008': {
      label: 'The corporation has answered Yes (1) at line 200072, but the information provided on Schedule 24 is incomplete.',
      category: 'EFILE'
    },
    '0241009': {
      label: 'The corporation has answered Yes (1) at line 200071, but the information provided on Schedule 24 is incomplete and/or is not correct.',
      category: 'EFILE'
    },

    //T2S25
    '0250001': {
      label: 'The corporation has answered YES at line 200169, but Schedule 25 is not completed.',
      category: 'EFILE'
    },
    '0250002': {
      label: 'The corporation has not provided a complete Schedule 25, including all of the names of foreign affiliate, equity percentage held and if the foreign affiliate is controlled or other.',
      category: 'EFILE'
    },

    //T2S27
    '0270001': {
      label: 'The corporation does not qualify as a small manufacturer. Therefore, to be eligible to claim the Manufacturing and processing profits deduction at line 200616, there must be an entry at line 027120; and - at lines (027140 and 027150); or - at lines (027160 and 027170); or - at line 027210.',
      category: 'EFILE'
    },
    '0270003': {
      label: 'The corporation has claimed the Manufacturing and processing profits deduction at line 200616, therefore there must be an entry: - at line 027100 or if the corporation is associated at lines 027100 and 027105 (if the amount is zero, change the value to any number and back to zero); or - at lines 027120 and at lines (027140 and 027150) or (027160 and 027170) or 027210.',
      category: 'EFILE'
    },
    '0270004': {
      label: 'An entry is required at line 027105 to substantiate the claim to be treated as a small manufacturer since the corporation has indicated it is associated.',
      category: 'EFILE'
    },
    '0270005': {
      label: 'The corporation has claimed the Manufacturing and processing profits from generating electrical energy for sale or producing steam for sale at line 027210, but there is no entry at lines 027205 and 027206.',
      category: 'EFILE'
    },
    '0270006': {
      label: 'Canadian Manufacturing and Processing Deduction is not claimed at line 200616, but Schedule 27 is filed with the Schedule 502.',
      category: 'EFILE'
    },

    //T2S28
    '0280002': {
      label: 'The corporation indicated it is an associated Canadian-controlled private corporation that is a “third corporation” and has elected under subsection 256(2) not to be associated for purposes of the small business deduction, but Schedule 28 (master agreement) is not completed or the filing corporation is not listed on the Schedule 28.',
      category: 'EFILE'
    },
    '0280150': {
      label: 'There are not a corresponding and equal number of entries in columns 028100 and 028200.',
      category: 'EFILE'
    },
    '0280010': {
      label: 'The name, Business number, or tax year-end at lines 028030, 028040, or 028050 are not provided or the calendar year to which the agreement applies at line 023050 does not match calendar year portion (YYYY) at line 200061.',
      category: 'EFILE'
    },
    '0281010': {
      label: 'The same Business number (BN) is entered more than once in column 028200.',
      category: 'EFILE'
    },

    //T2S29
    '0290001': {
      label: 'The corporation has answered YES at line 200170, but Schedule 29 is not completed.',
      category: 'EFILE'
    },
    '0290002': {
      label: 'The corporation has not provided a complete Schedule 29, including all of the names, address, payment codes and amounts of each non-resident.',
      category: 'EFILE'
    },

    //T2S30
    '0300001': {
      label: 'Schedule 30 has been completed, but there is no entry at line 032370.',
      category: 'EFILE'
    },

    //T2S31
    '0310001': {
      label: 'The corporation has answered YES at line 200231 or has claimed an investment tax credit at line 200652 or an investment tax credit refund at line 200780, but Schedule 31 is not completed.',
      category: 'EFILE'
    },
    '0310002': {
      label: 'The corporation has reported current year investment tax credit at line 031240 and/or 031242, but either there is no entry in all of columns 031105, 031110, 031115, 031120, and 031125 or there are not a corresponding and equal number of entries in columns 031105, 031110, 031115, 031120, and 031125.',
      category: 'EFILE'
    },
    '0310003': {
      label: 'The corporation has reported a current year investment tax credit for SR&ED at line 031540, but there is no entry at all of lines 031350, 031360, and 031370.',
      category: 'EFILE'
    },
    '0310005': {
      label: 'The corporation is claiming an investment tax credit refund at line 200780, but there is no entry at all of lines 031235, 031240, 031242, 031250, 031540, and 031550.',
      category: 'EFILE'
    },
    '0310006': {
      label: 'The corporation is requesting a carry-back of investment tax credit on qualified property, but there is no entry at all of lines 031235, 031240, 031242, and 031250.',
      category: 'EFILE'
    },
    '0310007': {
      label: 'The corporation is requesting a carry-back of investment tax credit on SR&ED, but there is no entry at lines 031540 and 031550.',
      category: 'EFILE'
    },
    '0310008': {
      label: 'The corporation is claiming an investment tax credit refund at lines 031310, 031610, or 200780, but there is no entry at line 031101.',
      category: 'EFILE'
    },
    '0310010': {
      label: 'The corporation has reported a recapture of investment tax credit at line 200602, but there are no entries in Part 16 and in Part 28 of Schedule 31. There must be an entry: - at one or more of lines 031700, 031710, 031720, 031730, 031740, 031750, or 031760; or - at one or more of lines 031792, 031795, 031797, or 031799.',
      category: 'EFILE'
    },
    '0310012': {
      label: 'There is a claim for pre-production mining expenditures, but exploration information in Part 18 of Schedule 031 is missing or incomplete. There must be an entry at line 031800 and an entry in at least one of lines 031805, 031806, or 031807.',
      category: 'EFILE'
    },
    '0310013': {
      label: 'The description is missing at line 031825 for one or more of the other pre-production mining expenditures entered at line 031826.',
      category: 'EFILE'
    },
    '0310014': {
      label: 'There is a claim for current-year investment tax credit from pre-production mining expenditures on Schedule 031, but there is no entry at all of lines 031810, 031811, 031812, 031813, 031820, 031821, 031826, and 031835.',
      category: 'EFILE'
    },
    '0310015': {
      label: 'The corporation has reported eligible salaries and wages for apprenticeship job creation at line 031603, but the contract number at line 031601 and/or the name of eligible trade at line 031602 are missing.',
      category: 'EFILE'
    },
    '0310016': {
      label: 'The corporation has reported an Apprenticeship job creation tax credit at line 031640, but there are no salaries and wages reported at line 031603.',
      category: 'EFILE'
    },
    '0310017': {
      label: 'The corporation is claiming a carry-back of Apprenticeship job creation tax credit, but there is no entry at all of lines 031635, 031640, and 031655.',
      category: 'EFILE'
    },
    '0310018': {
      label: 'The corporation has reported an amount of investment at line 031695, but there is not a corresponding entry at all of lines 031665, 031675, and 031685.',
      category: 'EFILE'
    },

    //T2S32
    '0320001': {
      label: 'There is an entry at one or more of lines 001231, 001411, 031350, 031360, 031370, 031420, 031430, 031440, 031450, 031460, 031480, or 031490, but Form T661 is not completed.',
      category: 'EFILE'
    },
    '0320003': {
      label: 'The corporation has made a claim for SR&ED expenditure pool deduction at line 001411, but there is no entry at all of lines 032300, 032305, 032310, 032320, 032325, 032340, 032345, 032350, 032355, 032360,032370, 032390, 032445, 032450, 032452, and 032453.',
      category: 'EFILE'
    },
    '0320005': {
      label: 'The corporation has reported current expenditures on Schedule 31, but there is no entry at all of lines 032300, 032305, 032307, 032309, 032310, 032320, 032325, 032340, 032345, 032350, 032355, 032360, 032370, 032500, 032502, and 032508.',
      category: 'EFILE'
    },
    '0320006': {
      label: 'The corporation has reported capital expenditures on Schedule 31, but there is no entry at all of lines 032390, 032504, and 032510.',
      category: 'EFILE'
    },
    '0320007': {
      label: 'The corporation has reported repayments made in the year on Schedule 31, but there is no entry at line 032560.',
      category: 'EFILE'
    },
    '0320023': {
      label: 'There is an entry for salaries and wages of a specified employee in column 032860, but there are not, for each row, corresponding entries in columns 032850, 032852, 032854, 032856, or 032858.',
      category: 'EFILE'
    },
    '0320027': {
      label: 'The corporation indicates at line 032151 that the Form T5013 was not filed, but the name of partners, the percentage, the Business number or the social insurance number at lines 032153, 032156, or 032157 are not provided.',
      category: 'EFILE'
    },
    '0320028': {
      label: 'Schedule 60 is completed, but there is no entry at lines 032160 and 032162.',
      category: 'EFILE'
    },
    '0320029': {
      label: 'There is an entry at line 032429, but there is no entry at lines 032513 and/or 032514.',
      category: 'EFILE'
    },
    '0320030': {
      label: 'There is an entry at lines 032153, 032156, and/or 032157, but there is no entry at line 032151.',
      category: 'EFILE'
    },
    '0320032': {
      label: 'Form T661 is completed, but there is no entry at one or more of lines 032100, 032105, 032115, 032120, or 032165.',
      category: 'EFILE'
    },
    '0320033': {
      label: 'There is an entry in at least one of lines 032300, 032305, 032307, 032309, 032320, 032325, 032340, 032345, 032350, 032360, or 032390, but Schedule 60 is not completed.',
      category: 'EFILE'
    },
    '0320034': {
      label: 'There is an entry of Recapture of SR&ED expenditure pool at line 001231, but there is no entry at all of lines 032429, 032431, 032432, 032435, and 032440.',
      category: 'EFILE'
    },
    '0320040': {
      label: 'There is an entry at line 032750, but there is not, for each row, a corresponding entry at lines 032752, 032754, and 032756.',
      category: 'EFILE'
    },
    //The diagnostic handles which label to display for 0320040
    /*    '0320040': {
     label: 'There is an entry at one or more of lines 032752, 032754, or 032756, but there is not, for each row, a corresponding entry at line 032750.',
     category: 'EFILE'
     },*/
    '0320045': {
      label: 'Form T661 is completed, but there is no entry at line 032935.',
      category: 'EFILE'
    },
    '0320046': {
      label: 'The corporation has answered Yes (1) at line 032935, but there is no entry at one or more of columns 032940, 032945, 032950, or 032965.',
      category: 'EFILE'
    },
    '0320047': {
      label: 'For the same row, there is no corresponding entry in one or more of columns 032940, 032945, 032950, or 032965.',
      category: 'EFILE'
    },
    '0320049': {
      label: 'The entry in column 032950 is 1, 2, 3, or 4, but for the same row there is no entry in column 032955.',
      category: 'EFILE'
    },
    '0320100': {
      label: 'The entry in column 032950 is 5, but for the same row there is no entry in column 032960.',
      category: 'EFILE'
    },
    '0320103': {
      label: 'Form T661 is completed, but there is no entry at line 032970.',
      category: 'EFILE'
    },

    //T2S60
    '0600001': {
      label: 'There must be an entry in each of lines 060200, 060202, 060204, 060218, 060260, 060265, 060266, and 060267.',
      category: 'EFILE'
    },
    '0600003': {
      label: 'There must be an entry at either line 060208 or 060210.',
      category: 'EFILE'
    },
    '0600004': {
      label: 'The corporation has indicated that the project was done in collaboration with other businesses at line 060218, but there is no entry at line 060220.',
      category: 'EFILE'
    },
    '0600009': {
      label: 'There must be an entry in each of lines 060242, 060244, and 060246.',
      category: 'EFILE'
    },
    '0600010': {
      label: 'There must be an entry in at least one of lines 060253, 060255, or 060257.',
      category: 'EFILE'
    },
    '0600011': {
      label: 'There is an entry at line 060253, but there is no entry at line 060254.',
      category: 'EFILE'
    },
    '0600012': {
      label: 'There is an entry at line 060255, but there is no entry at line 060256.',
      category: 'EFILE'
    },
    '0600013': {
      label: 'There is an entry at line 060257, but there is no entry at line 060258.',
      category: 'EFILE'
    },
    '0600014': {
      label: 'The entry at line 060267 is Yes (1), but there are not a corresponding and equal number of entries at lines 060268 and 060269.',
      category: 'EFILE'
    },
    '0600015': {
      label: 'There must be an entry in at least one of lines 060270, 060271, 060272, 060273, 060274, 060275, 060276, 060277, 060278, 060279, 060280, 060281, or 060282.',
      category: 'EFILE'
    },
    '0600016': {
      label: 'There is an entry at line 060281, but there is no entry at line 060282.',
      category: 'EFILE'
    },
    '0600018': {
      label: 'There must be an entry at line 060206.',
      category: 'EFILE'
    },

    //T2S33
    '0330015': {
      label: 'The TYE is after 2013-12-31 and the corporation has answered Yes (1) at line 200233, but none of schedules 33, 34, or 35 are completed.',
      category: 'EFILE'
    },
    '0330020': {
      label: 'Schedule 33 has been completed, but there is no entry at: all of lines 033101 to 033112; or lines 033610 and 033701.',
      category: 'EFILE'
    },
    '0331000': {
      label: 'More than one Part I.3 Large Corporation schedule has been completed.',
      category: 'EFILE'
    },

    //T2S34
    '0340020': {
      label: 'Schedule 34 has been completed, but there is no entry at all of lines 034201 to 034206, 034301, 034302, 034511, and 034512.',
      category: 'EFILE'
    },

    //T2S35
    '0350020': {
      label: 'Schedule 35 has been completed, but there is no entry at all of lines 035102 to 035106; 035201 to 035206, 035301 to 035331, 035511, 035512, and 035521.',
      category: 'EFILE'
    },
    '0350004': {
      label: 'The corporation has claimed a portion of capital over reserve liabilities at line 035530 but there is no entry at:  •',
      category: 'EFILE'
    },
    '0350005': {
      label: 'The corporation has completed the calculation for a taxable capital amount for the year at line 035650, but there is no entry at:  • all of lines 035201 to 035206 inclusive; and • lines 035611 or 035612.',
      category: 'EFILE'
    },

    //T2S37
    '0370006': {
      label: 'The corporation is claiming a prior year surtax credit against gross part VI tax at line 038885, but there is no entry at line 037120 or 037220.',
      category: 'EFILE'
    },

    //T2S38
    '0380001': {
      label: 'The corporation has answered YES at line 200238, but there is no entry at: -',
      category: 'EFILE'
    },
    '0380003': {
      label: 'The corporation has reported Part VI tax payable at line 200720, but there is no entry at: - all of lines 038101 to 038106 inclusive, or line 038650 or 038655; and - lines 038201 and 038202, or line 038650 or 038655; and - line 038591; and - all of lines 038301 to 038304 inclusive.',
      category: 'EFILE'
    },
    '0380006': {
      label: 'The corporation has reported gross Part VI tax at line 038831, but there is no entry at: - all of the lines 038101 to 038106 inclusive, or 038650, or 038655; and - lines 038201 and 038202, or line 038650, or 038655; and - line 038591; and - all of lines 038301 to 038304 inclusive.',
      category: 'EFILE'
    },

    //T2S39
    '0390001': {
      label: 'The financial institution indicated it is a member of a related group, has gross Part VI tax and has claimed a capital deduction, but Schedule 39 (master agreement) is not completed or the filing corporation is not listed on the Schedule 39.',
      category: 'EFILE'
    },
    '0390002': {
      label: 'The financial institution has indicated it is a member of a related group, has reported Part VI tax payable and has claimed a capital deduction, but Schedule 39 (master agreement) is not completed or the filing corporation is not listed on the Schedule 39.',
      category: 'EFILE'
    },
    '0390150': {
      label: 'There are not a corresponding and equal number of entries in columns 039200, and 039300, or column 039450 is blank.',
      category: 'EFILE'
    },
    '0390008': {
      label: 'The calendar year to which the agreement applies at line 039030 is not provided or does not match calendar year portion (YYYY) at line 200061.',
      category: 'EFILE'
    },

    //T2S42
    '0420002': {
      label: 'The corporation is claiming a Part I tax credit against Part VI tax at line 038884, but there is no entry at line 042120 and 042220.',
      category: 'EFILE'
    },

    //T2S43
    //'0430050': {
    //  label: 'The corporation has reported an amount of Part VI.1 tax transferred to a related corporation at line 043260, but there is no entry at line 045105 and/or there are not a corresponding and equal number of entries in columns 045225, 045230, 045233, and 045235.',
    //  category: 'EFILE'
    //},
    '0430055': {
      label: 'The calendar year to which the agreement applies at line 043118 is not provided or does not match calendar year portion (YYYY) at line 200061.',
      category: 'EFILE'
    },

    //T2S44
    '0440002': {
      label: 'The corporation has answered YES at line 200163, but Schedule 44 is not completed.',
      category: 'EFILE'
    },
    '0440004': {
      label: 'There are not a corresponding and equal number of entries in columns 044100, 044200, and 044300.',
      category: 'EFILE'
    },

    //T2S45
    '0430045': {
      label: 'The corporation has reported an amount of Part VI.1 tax transferred from a related corporation at line 043250, but there is no entry in one or more of lines 045105, 045110, 045115, or 045120.',
      category: 'EFILE'
    },
    '0430050': {
      label: 'The corporation has reported an amount of Part VI.1 tax transferred to a related corporation at line 043260, but there is no entry at line 045105 and/or there are not a corresponding and equal number of entries in columns 045225, 045230, 045233, and 045235.',
      category: 'EFILE'
    },

    //T2S46
    '0460001': {
      label: 'The corporation has answered YES at line 200249, but Schedule 46 is not completed.',
      category: 'EFILE'
    },
    '0460002': {
      label: 'The corporation has reported Part II tax at line 200708, but there is no entry in one or more of lines 046100, 046105, or 046110.',
      category: 'EFILE'
    },

    //T2S47
    '0470001': {
      label: 'The corporation is claiming a Canadian film or video production tax credit refund at line 200796, but Form T1131 is not completed.',
      category: 'EFILE'
    },
    '0470002': {
      label: 'The corporation has completed Form T1131, but there is no entry in one or more of lines 047151, 047153, 047301, 047311, or 047312.',
      category: 'EFILE'
    },
    '0470003': {
      label: 'The corporation has completed Form T1131, but there is no entry in one or more of lines or group of lines 047302 and/or [047304 and (047305 and/or 047306)].',
      category: 'EFILE'
    },
    '0470007': {
      label: 'The corporation is claiming a Canadian film and video production tax credit at line 047620, but there is no entry at line 047421 and/or there is no entry at all of lines 047601, 047603, 047605, 047606, 047607, 047609, and 047611.',
      category: 'EFILE'
    },
    '0470010': {
      label: 'As the corporation is claiming a Canadian film or video production tax credit refund at line 200796 and/or 047620, the certificate "A" or the certificate of completion "B" must be filed.',
      category: 'EFILE'
    },
    '0470015': {
      label: 'The corporation is claiming a Canadian film and video production tax credit at line 047620, but there is no entry at one or more of lines 047330, 047335, 047340, 047345, or 047350.',
      category: 'EFILE'
    },

    //T2S48
    '0480001': {
      label: 'The corporation is claiming a Film or video production services tax credit refund at line 200797, but Form T1177 is not completed.',
      category: 'EFILE'
    },
    '0480002': {
      label: 'The corporation has completed Form T1177, but there is no entry at lines 048151 and/or 048153.',
      category: 'EFILE'
    },
    '0480003': {
      label: 'The corporation has completed Form T1177, but there is no entry at one or more of lines 048301 and/or 048302.',
      category: 'EFILE'
    },
    '0480005': {
      label: 'The corporation has completed Form T1177, but there is no entry at line 048304, and there is no entry at lines 048305 and/or 048306.',
      category: 'EFILE'
    },
    '0480006': {
      label: 'The corporation is claiming a Film or video production services tax credit at line 048620, but there is no entry at all of lines 048601, 048603, 048605, 048606, 048607, 048609, and 048611.',
      category: 'EFILE'
    },
    '0480010': {
      label: 'As the corporation is claiming a Film or video production services tax credit refund at lines 200797 and/or 048620, the accredited film or video production certificate must be filed.',
      category: 'EFILE'
    },
    '0480014': {
      label: 'The corporation is claiming a Film or video production services tax credit at line 048620, but there is no entry at one or more of lines 048330, 048335, 048340, or 048345.',
      category: 'EFILE'
    },

    //T2S49
    '0490002': {
      label: 'The corporation has indicated that it is an associated Canadian-controlled private corporation and is claiming an investment tax credits on qualifying Scientific Research & Experimental Development expenditures at the 35% rate, but Schedule 49 (master agreement) is not completed or the filing corporation is not listed on the Schedule 49.',
      category: 'EFILE'
    },
    '0490150': {
      label: 'There are not a corresponding and equal number of entries in columns 049100, 049200, and 049300, or column 049400 is blank.',
      category: 'EFILE'
    },
    '0490010': {
      label: 'The calendar year to which the agreement applies at line 049050 is not provided or does not match calendar year portion (YYYY) at line 200061.',
      category: 'EFILE'
    },

    //T2S50
    '0500001': {
      label: 'The corporation has answered YES at line 200173, but Schedule 50 is not completed.',
      category: 'EFILE'
    },
    //'0500002': {
    //  label: 'For each shareholder listed at line 050100, a percentage of common shares (050400) and/or percentage of preferred shares (050500) are required.',
    //  category: 'EFILE'
    //},
    //'0500002': {
    //  label: 'For each Business number provided in column 050200, a name and percentage of common shares and/or preferred shares are required.',
    //  category: 'EFILE'
    //},
    //'0500002': {
    //  label: 'For each social insurance number provided in column 050300, a name and percentage of common shares and/or preferred shares are required.',
    //  category: 'EFILE'
    //},
    //'0500002': {
    //  label: 'For each trust number provided in column 050350, a name and percentage of common shares and/or preferred shares are required.',
    //  category: 'EFILE'
    //},
    //'0500002': {
    //  label: 'For each percentage of common shares provided in column 050400, a name of a shareholder is required in column 050100.',
    //  category: 'EFILE'
    //},
    //'0500002': {
    //  label: 'For each percentage of preferred shares provided in column 050500, a name of a shareholder is required in column 050100.',
    //  category: 'EFILE'
    //},
    '0501001': {
      label: 'The percentage of common shares owned exceeds 100%.',
      category: 'EFILE'
    },
    '0501002': {
      label: 'The total percentage of common shares owned exceeds 100%.',
      category: 'EFILE'
    },
    '0501003': {
      label: 'The percentage of preferred shares owned exceeds 100%.',
      category: 'EFILE'
    },
    '0501004': {
      label: 'The total percentage of preferred shares owned exceeds 100%.',
      category: 'EFILE'
    },

    //T2S53
    '0530030': {
      label: 'A GRIP balance before adjustment for specified future tax consequences is reported at line 053490, but there is no entry at lines 053100, 053110, 053120, 053130, 053140, 053200, 053210, 053220, 053230, 053240, 053300, and 053310.',
      category: 'EFILE'
    },
    '0530040': {
      label: 'A GRIP addition is claimed at line 053290, but there is no entry at lines 053220, 053230, and 053240.',
      category: 'EFILE'
    },

    //T2S54
    '0540010': {
      label: 'The corporation has reported entries in any of lines 054210, 054220, 054240, 054250, 054270, or 054280, but there is no corresponding entry at line 054200.',
      category: 'EFILE'
    },

    //T2S55
    '0550010': {
      label: 'There is an amount of Part III.1 tax payable on excessive eligible dividend designations at line 055190, but there is no entry at line 055150.',
      category: 'EFILE'
    },
    '0550020': {
      label: 'There is an amount of Part III.1 tax payable on excessive eligible dividend designations at line 055290, but Schedule 54 is not completed.',
      category: 'EFILE'
    },
    '0550030': {
      label: 'The corporation is reporting GRIP at the end of the tax year at line 055160, but Schedule 53 is not completed.',
      category: 'EFILE'
    },
    '0550070': {
      label: 'There is an amount of Part III.1 tax payable at line 200710, but Schedule 55 is not completed. In addition, Schedule 53 or Schedule 54 must be completed to support the calculation of Part III.1 tax on Schedule 55.',
      category: 'EFILE'
    },

    //T2S61
    '0610001': {
      label: 'There is an entry at line 061010, but there is no entry at lines 061015 and 061020.',
      category: 'EFILE'
    },
    '0610003': {
      label: 'There is an entry at either line 061015 or 061020, but there is no entry at line 061010.',
      category: 'EFILE'
    },
    '0610005': {
      label: 'Schedule 61 (Form T1145) has been completed, but there is no entry at lines 061010, and 061015 or 061020.',
      category: 'EFILE'
    },
    '0610009': {
      label: 'The corporation has answered Yes (1) at line 061030, but there is no entry at line 061035.',
      category: 'EFILE'
    },
    '0610011': {
      label: 'Schedule 61 (Form T1145) has been completed, but there is no entry at one or more of lines 061040, 061045, 061050, 061055, 061060, 061070, 061075, 061080, 061085, or 061090.',
      category: 'EFILE'
    },

    //T2S62
    '0620001': {
      label: 'There is an entry at line 062010, but there is no entry at lines 062015 and 062020.',
      category: 'EFILE'
    },
    '0620003': {
      label: 'There is an entry at either lines 062015 or 062020, but there is no entry at line 062010.',
      category: 'EFILE'
    },
    '0620005': {
      label: 'Schedule 62 (Form T1146) has been completed, but there is no entry at line 062010 and/or at lines 062015 and 062020.',
      category: 'EFILE'
    },
    '0620009': {
      label: 'The corporation has answered Yes (1) at line 062030, but there is no entry at line 062035.',
      category: 'EFILE'
    },
    '0620011': {
      label: 'Schedule 62 (Form T1146) has been completed, but there is no entry in one or more of lines 062040, 062045, 062050, 062055, 062060, 062070, 062075, 062080, 062085, 062090, 062100, 062102, 062106, 062110, or 062114.',
      category: 'EFILE'
    },

    //T2S73
    '0730010': {
      label: 'The corporation has reported an entry in at least one of lines 001130 and/or 001131, but Schedule 73 is not completed.',
      category: 'EFILE'
    },
    '0730020': {
      label: 'The corporation has reported an entry in at least one of columns 073100 to 073310, but for the same row there is no corresponding entry in column 073100.',
      category: 'EFILE'
    },

    //T2S88
    '0880050': {
      label: 'The corporation has answered YES at line 200180, but Schedule 88 is not completed.',
      category: 'EFILE'
    },

    //T2S91
    '0910001': {
      label: 'The corporation has answered Yes (1) at line 200082, but Schedule 91 is not completed.',
      category: 'EFILE'
    },
    '0910002': {
      label: 'There is an entry greater than zero at line 091125, but the information concerning physical facilities in Canada at line 091135 is not provided.',
      category: 'EFILE'
    },
    '0910004': {
      label: 'Schedule 91 has been completed, but information concerning the province where services were rendered at line 091111 and/or information concerning the business activity carried on in Canada at line 091112 is not provided.',
      category: 'EFILE'
    },

    //T2S97
    '0971000': {
      label: 'There is an indication that the corporation is a non-resident, but Schedule 97 is not completed.',
      category: 'EFILE'
    },
    '0971001': {
      label: 'Schedule 97 has been completed, but the Canadian income of a non-resident corporation type at line 097300 is not provided.',
      category: 'EFILE'
    },

    //T2S301
    '3010001': {
      label: 'The corporation is claiming the Newfoundland and Labrador research and development tax credit at line 005520, but there is no entry at lines 301103, 301130, et 301140.',
      category: 'EFILE'
    },

    //T2S303
    '3030004': {
      label: 'The corporation is claiming the Newfoundland and Labrador direct equity tax credit at line 005505, but Schedule 303 is not completed.',
      category: 'EFILE'
    },
    '3030005': {
      label: 'The corporation is reporting a current year Newfoundland and Labrador direct equity tax credit at line 303120, but there is no investment in eligible shares reported in column 303103.',
      category: 'EFILE'
    },
    '3030006': {
      label: 'The corporation is requesting a carry-back of Newfoundland and Labrador direct equity tax credit at line 303901, 303902, or 303903, but there is no current year credit earned at line 303120.',
      category: 'EFILE'
    },
    '3030007': {
      label: 'There is a claim for the Newfoundland and Labrador direct equity tax credit in column 303103, but there is no entry in column 303100 for the same row.',
      category: 'EFILE'
    },
    '3030008': {
      label: 'There is a tax credit receipt number in column 303100, but there is no entry in column 303103 for the same row.',
      category: 'EFILE'
    },

    //T2S304
    '3040001': {
      label: 'The corporation is claiming the Newfoundland and Labrador resort property investment tax credit at line 005507, but Schedule 304 is not completed.',
      category: 'EFILE'
    },
    '3040002': {
      label: 'The corporation is reporting a current-year Newfoundland and Labrador resort property investment tax credit at line 304120, but there is no resort property investment tax credit amount reported in column 304103.',
      category: 'EFILE'
    },
    '3040003': {
      label: 'The corporation is requesting a carry-back of Newfoundland and Labrador resort property investment tax credit at lines 304901, 304902 or 304903, but there is no current-year credit earned at line 304120.',
      category: 'EFILE'
    },
    '3040004': {
      label: 'There is a claim for the Newfoundland and Labrador resort property investment tax credit in column 304103, but there is no entry in column 304100 for the same row.',
      category: 'EFILE'
    },
    '3040005': {
      label: 'There is a tax credit receipt number in column 304100, but there is no entry in column 304103 for the same row.',
      category: 'EFILE'
    },
    '3040006': {
      label: 'The Newfoundland and Labrador resort property investment tax credit cannot be carried back to a tax year ending before January 1, 2006.',
      category: 'EFILE'
    },

    //T2S308
    '3080001': {
      label: 'The corporation is claiming the Newfoundland and Labrador venture capital tax credit at line 005504, but Schedule 308 is not completed.',
      category: 'EFILE'
    },

    //T2S302
    '3020540': {
      label: 'There are not a corresponding and equal number of entries in column 302100 and 302200.',
      category: 'EFILE'
    },

    //T2S305
    '3050001': {
      label: 'Schedule 305 has been completed or Newfoundland and Labrador capital tax on financial institutions is reported at line 005518, but the tax year start is before November 1, 2008.',
      category: 'EFILE'
    },
    '3050002': {
      label: 'Schedule 305 has been completed and Newfoundland and Labrador capital tax on financial institutions is reported at line 005518, but the corporation has no permanent establishment in Newfoundland and Labrador.',
      category: 'EFILE'
    },
    '3050003': {
      label: 'The corporation is a financial institution with a permanent establishment in Newfoundland and Labrador, but Schedule 305 is not completed.',
      category: 'EFILE'
    },
    '3050004': {
      label: 'The corporation is reporting Newfoundland and Labrador capital tax on financial institutions at line 005518, but Schedule 305 is not completed.',
      category: 'EFILE'
    },
    '3050005': {
      label: 'The corporation is related and a capital deduction is claimed at line 305120, but Schedule 306 is not completed.',
      category: 'EFILE'
    },

    //T2S306
    '3060002': {
      label: 'The filing corporation is not listed on the Schedule 306.',
      category: 'EFILE'
    },
    '3060003': {
      label: 'There is an entry in column 306400, but for the same row there is no corresponding entry in columns 306200 and/or 306300.',
      category: 'EFILE'
    },

    //T2S309
    '3090001': {
      label: 'A certificate number is required to support the claim for a Newfoundland and Labrador interactive digital media tax credit.',
      category: 'EFILE'
    },
    '3090005': {
      label: 'There are entries in both of line 005840 and column 309100. Schedule 309 should only be completed if the taxpayer is reporting information from more than one certificate.',
      category: 'EFILE'
    },
    '3090010': {
      label: 'There is an entry in column 309100, but for the same row there is no corresponding entry in column 309200.',
      category: 'EFILE'
    },
    '3090015': {
      label: 'There is an entry in column 309200, but for the same row there is no corresponding entry in column 309100.',
      category: 'EFILE'
    },

    //T2S340
    '3400002': {
      label: 'The corporation is claiming the Nova Scotia research and development tax credit at line 005566, but there is no entry at lines 340103, 340130, and 340140.',
      category: 'EFILE'
    },

    //T2S345
    '0050501': {
      label: 'The corporation is claiming the Nova Scotia film industry tax credit at line 005565, but the certificate number is not provided at line 005836 or in column 345100.',
      category: 'EFILE'
    },
    '0050502': {
      label: 'There are not a corresponding and equal number of entries in columns 345100 and 345200.',
      category: 'EFILE'
    },

    //T2S21
    '3210001': {
      label: 'The corporation is claiming the Prince Edward Island corporate investment tax credit at line 005530, but there is no entry in columns 321101, 321102, or 321103, or at all of lines 321105, 321110, 321130, and 321140.',
      category: 'EFILE'
    },
    '3210002': {
      label: 'For each entry in column 321101, 321102, or 321103, a corresponding and equal number of entries are required in the other columns.',
      category: 'EFILE'
    },
    //T2S347
    '3470001': {
      label: 'A certificate number is required to support the claim for a Nova Scotia digital media tax credit.',
      category: 'EFILE'
    },
    '3470005': {
      label: 'There are entries in both of line 005838 and column 347100. Schedule 347 should only be completed if the taxpayer is reporting information from more than one certificate. ',
      category: 'EFILE'
    },
    '3470010': {
      label: 'There is an entry in column 347100, but for the same row there is no corresponding entry in column 347200.',
      category: 'EFILE'
    },
    '3470015': {
      label: 'There is an entry in column 347200, but for the same row there is no corresponding entry in column 347100.',
      category: 'EFILE'
    },

    //T2S348
    '3480001': {
      label: 'A certificate number is required to support the claim for a Nova Scotia digital animation tax credit.',
      category: 'EFILE'
    },
    '3480005': {
      label: 'There are entries in both of line 005839 and column 348100. Schedule 348 should only be completed if the taxpayer is reporting information from more than one certificate.',
      category: 'EFILE'
    },
    '3480010': {
      label: 'There is an entry in column 348100, but for the same row there is no corresponding entry in column 348200.',
      category: 'EFILE'
    },
    '3480015': {
      label: 'There is an entry in column 348200, but for the same row there is no corresponding entry in column 348100.',
      category: 'EFILE'
    },

    //T2S360
    '3600002': {
      label: 'The corporation is claiming the New Brunswick research and development tax credit at line 005597, but there is no entry at all of lines 360106, 360131, and 360141.',
      category: 'EFILE'
    },
    '3600003': {
      label: 'The corporation is claiming a recapture of New Brunswick research and development tax credit at line 005573, but there is no entry in all of columns 360700, 360710, 360720, 360730, 360740, 360750, and 360760.',
      category: 'EFILE'
    },
    //T2S367
    '3670001': {
      label: 'The corporation is claiming the New Brunswick small business investor tax credit at line 005578, but Schedule 367 is not completed.',
      category: 'EFILE'
    },
    //T2S380
    '3800001': {
      label: 'Members may bring guests to a club upon payment of $15 (including applicable tax). Guests must be signed in and meet with a GoodLife associate at the Front desk before entering the club.',
      category: 'EFILE'
    },
    '3800003': {
      label: 'The corporation is requesting a carry-back of credit at lines 380901, 380902, or 380903, but there is no entry at lines 380121, 380123, 380124, 380126, 380130, and 380140.',
      category: 'EFILE'
    },

    //T2S384
    '3840001': {
      label: 'The corporation is claiming the Manitoba paid work experience tax credit at line 005603 or a Manitoba refundable co-op education and apprenticeship tax credit at line 005622, but Schedule 384 is not completed.',
      category: 'EFILE'
    },
    '3840004': {
      label: 'A serial number is entered in column 384100, but for the same row there is no corresponding entry in column 384203.',
      category: 'EFILE'
    },
    '3840005': {
      label: 'A refundable credit is entered in column 384203, but for the same row there is no corresponding entry in column 384100.',
      category: 'EFILE'
    },
    '3840007': {
      label: 'The corporation is reporting a current year Manitoba refundable paid work experience tax credit at line 384125, but there is no entry in column 384203.',
      category: 'EFILE'
    },
    '3840009': {
      label: 'The corporation is reporting salary and wages in column 384260, but for the same row there is no corresponding entry in column 384250.',
      category: 'EFILE'
    },
    '3840010': {
      label: 'The corporation is reporting salary and wages in column 384360, but for the same row there is no corresponding entry in column 384350.',
      category: 'EFILE'
    },
    '3840011': {
      label: 'The corporation is reporting salary and wages in column 384460, but for the same row there is no corresponding entry in column 384450.',
      category: 'EFILE'
    },
    '3840012': {
      label: 'The corporation is reporting salary and wages in column 384560, but for the same row there is no corresponding entry in column 384550.',
      category: 'EFILE'
    },
    '3840013': {
      label: 'The corporation is reporting salary and wages in column 384660, but for the same row there is no corresponding entry in column 384650.',
      category: 'EFILE'
    },
    '3840014': {
      label: 'The corporation is reporting salary and wages in column 384155, but for the same row there is no corresponding entry in column 384150.',
      category: 'EFILE'
    },
    '3840015': {
      label: 'The corporation is reporting salary and wages in column 384760, but for the same row there is no corresponding entry in column 384750.',
      category: 'EFILE'
    },

    //T2S381
    '3810001': {
      label: 'The corporation is claiming the Manitoba manufacturing investment tax credit at line 005605, but there is no entry in one or more of columns 381101, 381102, or 381103, and there is no entry at lines 381105, 381110, 381130, and 381140.',
      category: 'EFILE'
    },
    '3810002': {
      label: 'For each entry in columns 381101, 381102, or 381103, there must be a corresponding entry in the other columns.',
      category: 'EFILE'
    },
    '3810003': {
      label: 'The corporation is claiming the Manitoba refundable manufacturing investment tax credit at line 005621, but there is no entry in one or more of columns 381101, 381102, or 381103 and there is no entry at lines 381130, and 381140.',
      category: 'EFILE'
    },

    //T2S385
    '3850001': {
      label: 'The corporation is claiming the Manitoba odour-control tax credit at line 005607, but Schedule 385 is not completed.',
      category: 'EFILE'
    },
    '3850002': {
      label: 'The corporation is reporting a current-year Manitoba odour-control tax credit at line 385120, but there is no entry at lines 385100, 385101, and 385102.',
      category: 'EFILE'
    },
    '3850003': {
      label: 'The corporation is claiming a carryback of a current-year Manitoba odour-control tax credit at line 385901, 385902, or 385903, but there is no entry at line 385120.',
      category: 'EFILE'
    },
    '3850007': {
      label: 'The corporation is claiming the Manitoba refundable odour-control tax credit for agricultural corporations at line 005623, but Schedule 385 is not completed.',
      category: 'EFILE'
    },

    //T2S387
    '3870005': {
      label: 'The corporation is claiming the Manitoba small business venture capital tax credit at line 005608, but Schedule 387 is not completed.',
      category: 'EFILE'
    },
    '3870010': {
      label: 'The corporation is reporting a Manitoba small business venture tax credit earned in the current tax year at line 387120, but there is no entry at line 387102.',
      category: 'EFILE'
    },

    //T2S389
    '3890001': {
      label: 'The corporation is claiming the Manitoba book publishing tax credit at line 005615, but Schedule 389 is not completed.',
      category: 'EFILE'
    },
    '3890002': {
      label: 'The corporation is claiming the Manitoba book publishing tax credit, but has not answered all of the eligible publisher questions at lines 389110, 389115, 389120, 389130, and 389140.',
      category: 'EFILE'
    },
    '3890003': {
      label: 'The corporation is claiming the Manitoba book publishing tax credit, but has not answered all of the eligible publisher questions at lines 389110, 389115, 389120, 389130, and 389140.',
      category: 'EFILE'
    },
    '3890004': {
      label: 'There are not a corresponding and equal number of entries in columns 389200, 389210, 389220, 389230, 389240, 389250, 389260, and 389270.',
      category: 'EFILE'
    },
    '3890005': {
      label: 'The corporation is claiming the Manitoba book publishing tax credit at line 005615, but there is no entry in all of lines 389400, 389415, 389420, 389450, and 389455.',
      category: 'EFILE'
    },

    //T2S390
    '3900001': {
      label: 'The corporation is claiming a Manitoba cooperative development tax credit at line 005609 or a Manitoba refundable cooperative development tax credit at line 005612, but Schedule 390 is not completed.',
      category: 'EFILE'
    },
    '3900003': {
      label: '390103	If there is an entry at this line, there must be an entry at lines 390100, 390101, and 390102.	3900003	The corporation is reporting a non-refundable Manitoba cooperative development tax credit at line 390103, but there is no entry at one or more of lines 390100, 390101, or 390102.',
      category: 'EFILE'
    },
    '3900005': {
      label: '390104	If there is an entry at this line, there must be an entry at lines 390100, 390101, and 390102.	3900005	The corporation is reporting a refundable Manitoba cooperative development tax credit at line 390104, but there is no entry at one or more of lines 390100, 390101, or 390102.',
      category: 'EFILE'
    },

    //T2S391
    '3910001': {
      label: 'If there is an entry at this line, Schedule 391 must be filed.	3910001	The corporation is claiming a Manitoba neighbourhoods Alive! tax credit at line 005610, but Schedule 391 is not completed.',
      category: 'EFILE'
    },

    //T2S392
    '3920001': {
      label: 'The corporation is claiming Manitoba data processing investment tax credits at line 005324, but Schedule 392 is not completed.',
      category: 'EFILE'
    },
    '3920002': {
      label: 'There are entries in both columns 392103 and 392104 for the same row.',
      category: 'EFILE'
    },
    '3920003': {
      label: 'The corporation is claiming Manitoba data processing investment tax credits at line 005324 and there is an entry in column 392103 or 392104, but for the same row there is no corresponding entry in columns 392101 and/or 392102.',
      category: 'EFILE'
    },
    '3920004': {
      label: 'There are entries in both columns 392203 and 392204 for the same row.',
      category: 'EFILE'
    },
    '3920005': {
      label: 'The corporation is claiming Manitoba data processing investment tax credits at line 005324 and there is an entry in column 392203 or 392204, but for the same row there is no corresponding entry in columns 392201 and/or 392202.',
      category: 'EFILE'
    },
    '3920006': {
      label: 'There are entries in both columns 392403 and 392404 for the same row.',
      category: 'EFILE'
    },
    '3920007': {
      label: 'The corporation is claiming Manitoba data processing investment tax credits at line 005324 and there is an entry in column 392403 or 392404, but for the same row there is no corresponding entry in columns 392401 and/or 392402.',
      category: 'EFILE'
    },
    '3920008': {
      label: 'The corporation is claiming Manitoba data processing investment tax credits at line 005324 and there is an entry in column 392603, but for the same row there is no corresponding entry in columns 392601 and/or 392602.',
      category: 'EFILE'
    },
    '3920009': {
      label: 'The corporation is claiming Manitoba data processing investment tax credits at line 005324 and there is an entry in column 392606, but for the same row there is no corresponding entry in columns 392604 and/or 392605.',
      category: 'EFILE'
    },
    //T2S393
    '3930001': {
      label: 'The corporation is claiming a Manitoba nutrient management tax credit at line 005325, but Schedule 393 is not completed.',
      category: 'EFILE'
    },
    '3930003': {
      label: 'The corporation is claiming a Manitoba nutrient management tax credit at line 005325 and has reported a capital cost in column 393103, but for the same row there is no corresponding entry in columns 390101 and/or 393102.',
      category: 'EFILE'
    },

    //T2S394
    '3940001': {
      label: 'The corporation is claiming a Manitoba rental housing construction tax credit at line 005602 and/or a Manitoba refundable rental housing construction tax credit at line 005326, but Schedule 394 is not completed.',
      category: 'EFILE'
    },
    '3940002': {
      label: 'There is an entry in column 394150, but for the same row there is no corresponding entry in columns 394100, 394110, or 394120.',
      category: 'EFILE'
    },
    '3940003': {
      label: 'There is an entry in column 394150, but for the same row there is no corresponding entry in columns 394100, 394110, or 394120.',
      category: 'EFILE'
    },
    '3941006': {
      label: 'The corporation has entered a number of less than 5 for the number of residential units in eligible rental housing project at line 394120, but this number must be greater than 4.',
      category: 'EFILE'
    },
    '3941020': {
      label: 'The corporation has entered a number of less than 5 for the number of residential units in eligible rental housing project at line 394220, but this number must be greater than 4.',
      category: 'EFILE'
    },

    //T2S402
    '4020001': {
      label: 'The corporation is claiming the Saskatchewan manufacturing and processing investment tax credit at line 005630, but there is no entry in columns 402101, 402102, or 402103, and there is no entry at lines 402105 and 402110.',
      category: 'EFILE'
    },
    '4020002': {
      label: 'For each entry in columns 402101, 402102, or 402103, a corresponding and equal number of entries are required in the other columns.',
      category: 'EFILE'
    },
    '4020003': {
      label: 'The corporation is claiming the Saskatchewan manufacturing and processing refundable investment tax credit at line 005644, but there is no entry in columns 402101, 402102, or 402103, and there is no entry at lines 402230 and 402240.',
      category: 'EFILE'
    },

    //T2S403
    '4030001': {
      label: 'The corporation is claiming a Saskatchewan research and development tax credit at line 005631, but there is no entry at lines 403101, 403102, 403106, 403107, 403105, 403110, 403130, 403140, 403211, 403214, 403216, and 403219.',
      category: 'EFILE'
    },
    '4030002': {
      label: 'The corporation is claiming a Saskatchewan refundable research and development tax credit at line 005645, but there is no entry at lines 403211 and 403216.',
      category: 'EFILE'
    },
    '4030003': {
      label: 'The corporation has reported Saskatchewan research and development eligible expenditures for purposes of refundable tax credit, but Schedule 31 and/or Schedule 49 are not completed.',
      category: 'EFILE'
    },
    '4030004': {
      label: 'The corporation has reported Saskatchewan research and development eligible expenditures for purposes of refundable tax credit, but Schedule 31 and/or Schedule 49 are not completed.',
      category: 'EFILE'
    },

    //T2S410
    '4100002': {
      label: 'There are not a corresponding and equal number of entries in columns 410100 and 410200.',
      category: 'EFILE'
    },

    //T2S421
    '4210001': {
      label: 'The corporation is claiming the British Columbia mining exploration tax credit at line 005673, but Schedule 421 is not completed.',
      category: 'EFILE'
    },
    '4210002': {
      label: 'Schedule 421 has been completed, but exploration information (either the mineral names, project information or both) in Part 1 is missing.',
      category: 'EFILE'
    },
    '4210003': {
      label: 'Schedule 421 has been completed, but information in Part 2 – Qualified mining exploration expenses and/or at line 421220 is not completed.',
      category: 'EFILE'
    },
    '4210004': {
      label: 'Schedule 421 has been completed, but information in the table in Part 2 (Qualified mining exploration expenses) is incomplete.',
      category: 'EFILE'
    },

    //T2S422
    '4220001': {
      label: 'The corporation is claiming a British Columbia film and television tax credit at line 005671, but Schedule 422 (Form T1196) is not completed.',
      category: 'EFILE'
    },
    '4220002': {
      label: 'Schedule 422 (Form T1196) is completed, but there is no entry at lines 422151 and/or 422153.',
      category: 'EFILE'
    },
    '4220003': {
      label: 'Schedule 422 (Form T1196) is completed, but there is no entry in one or more of lines 422220, 422222, 422230, 422235, 422240, 422245, or 422250.',
      category: 'EFILE'
    },
    '4220004': {
      label: 'Schedule 422 (Form T1196) is completed, but there is no entry in one or more of lines 422301, 422302, 422303, 422310, 422311, 422312, or 422313.',
      category: 'EFILE'
    },
    '4220005': {
      label: 'The corporation is claiming a British Columbia film and television tax credit at line 422800, but there is no entry at line 422405 and there is no entry at all of lines 422505, 422515, 422516, 422520, 422521, 422525, and 422530.',
      category: 'EFILE'
    },
    '4220010': {
      label: 'As the corporation is claiming a British Columbia film and television tax credit, the eligibility certificate must be filed.',
      category: 'EFILE'
    },

    //T2S423
    '4230001': {
      label: 'The corporation is claiming a British Columbia productions services tax credit at line 005672, but Schedule 423 (Form T1197) is not completed.',
      category: 'EFILE'
    },
    '4230002': {
      label: 'Schedule 423 (Form T1197) is completed, but there is no entry at lines 423151 and/or 423153.',
      category: 'EFILE'
    },
    '4230003': {
      label: 'Schedule 423 (Form T1197) is completed, but there is no entry in one or more of lines 423220, 423222, 423230, 423235, 423240, 423245, or 423250.',
      category: 'EFILE'
    },
    '4230004': {
      label: 'Schedule 423 (Form T1197) is completed, but there is no entry in one or more of lines 423301, 423302 or 423303.',
      category: 'EFILE'
    },
    '4230005': {
      label: 'The corporation is claiming a British Columbia production services tax credit at line 423800, but there is no entry at line 423505, or there is no entry at all of lines 423405, 423420, 423425, 423430, and 423435.',
      category: 'EFILE'
    },
    '4230010': {
      label: 'As the corporation is claiming a British Columbia production services tax credit, the accreditation certificate must be filed.',
      category: 'EFILE'
    },

    //T2S425
    '4250001': {
      label: 'The corporation is claiming a British Columbia SR&ED non-refundable tax credit at line 005659, but Schedule 425 is not completed.',
      category: 'EFILE'
    },
    '4250002': {
      label: 'The corporation is claiming a British Columbia SR&ED refundable tax credit at line 005674, but Schedule 425 is not completed.',
      category: 'EFILE'
    },
    '4250004': {
      label: 'The corporation is claiming a recapture of British Columbia SR&ED tax credit at line 005241, but there is no entry in columns 425700 and 425710.',
      category: 'EFILE'
    },

    //T2S426
    '4260001': {
      label: 'The corporation is claiming a British Columbia manufacturing and processing tax credit at line 005660, but there is no entry in columns:  - 426101, 426102, and 426103; and/or - at lines 426100, 426105, 426110, or 426130.',
      category: 'EFILE'
    },
    '4260002': {
      label: 'The information provided on Schedule 426 is incomplete.  For each and every entry in column 426101, 426102, or 426103, there must be a corresponding and equal number of entries in the other columns.',
      category: 'EFILE'
    },

    //T2S428
    '4280001': {
      label: 'The corporation is claiming a British Columbia Training Tax Credit at line 005679, but Schedule 428 is not completed.',
      category: 'EFILE'
    },
    '4280002': {
      label: 'The corporation has reported salaries and wages for the basic tax credit in column 428110, but for the same row the registration number (SIN or name of employee) in column 428100 and/or the name of program in column 428105 are missing.',
      category: 'EFILE'
    },
    '4280003': {
      label: 'The corporation has reported salaries and wages for the completion tax credit for an employee that completed level three of an eligible apprenticeship program in column 428210, but for the same row the registration number (SIN or name of employee) in column 428200 and/or the name of program in column 428205 are missing.',
      category: 'EFILE'
    },
    '4280004': {
      label: 'The corporation has reported salaries and wages for the completion tax credit for an employee that completed level four or higher of an eligible apprenticeship program in column 428310, but for the same row the registration number (SIN or name of employee) in column 428300 and/or the name of program in column 428305 are missing.',
      category: 'EFILE'
    },
    '4280005': {
      label: 'The corporation has reported salaries and wages for the enhanced tax credit for an employee’s first 24 months of a Red Seal apprenticeship program in column 428411, but for the same row the registration number (SIN or name of employee) in column 428400 and/or the name of program in column 428405 are missing.',
      category: 'EFILE'
    },
    '4280006': {
      label: 'The corporation has reported salaries and wages for the enhanced tax credit for an employee’s first 24 months of a non-Red Seal apprenticeship program in column 428510, but for the same row the registration number (SIN or name of employee) in column 428500 and/or the name of program in column 428505 are missing.',
      category: 'EFILE'
    },
    '4280007': {
      label: 'The corporation has reported salaries and wages for the enhanced tax credit for an employee that has completed level 3 of an eligible apprenticeship program in column 428610, but for the same row the registration number (SIN or name of employee) in column 428600 and/or the name of program in column 428605 are missing.',
      category: 'EFILE'
    },
    '4280008': {
      label: 'The corporation has reported salaries and wages for the enhanced tax credit for an employee that has completed level 4 or higher of an eligible apprenticeship program in column 428710, but for the same row the registration number (SIN or name of employee) in column 428700 and/or the name of program in column 428705 are missing.',
      category: 'EFILE'
    },

    //T2S429
    '4290001': {
      label: 'The corporation is claiming a British Columbia interactive digital media tax credit at line 005680, but Schedule 429 is not completed.',
      category: 'EFILE'
    },
    '4290002': {
      label: 'The corporation is claiming a British Columbia interactive digital media tax credit at lines 005680, or there is an entry at line 429400, or the entry at line 429210 is Yes (1), but there is no entry at line 429215.',
      category: 'EFILE'
    },
    '4290003': {
      label: 'The corporation is claiming a British Columbia interactive digital media tax credit at line 005680, but there is no entry at one or more of lines 429310, 429315, 429320, 429325, 429330, 429335, 429340, 429345, 429350, or 429355.',
      category: 'EFILE'
    },

    //T2S430
    '4300001': {
      label: 'The corporation is claiming a British Columbia shipbuilding and ship repair industry tax credit at line 005681, but Schedule 430 is not completed.',
      category: 'EFILE'
    },
    '4300002': {
      label: 'The corporation has reported salary and wages payable after September 30, 2012 in column 430110, but for the same row the ITA identification number, SIN, or name of employee in column 430100 and/or the name of program in column 430105 are missing.',
      category: 'EFILE'
    },
    '4300003': {
      label: 'The corporation has reported salary and wages in column 430210, but for the same row the ITA identification number, SIN, or name of employee in column 430200 and/or the name of program in column 430205 are missing.',
      category: 'EFILE'
    },
    '4300004': {
      label: 'The corporation has reported salary and wages in column 430310, but for the same row the ITA identification number, SIN, or name of employee in column 430300 and/or the name of program in column 430305 are missing.',
      category: 'EFILE'
    },
    '4300005': {
      label: 'The corporation has reported salary and wages payable after September 30, 2012 in column 430410, but for the same row the ITA identification number, SIN, or name of employee in column 430400 and/or the name of program in column 430405 are missing.',
      category: 'EFILE'
    },
    '4300006': {
      label: 'The corporation has reported salary and wages in column 430510, but for the same row the ITA identification number, SIN, or name of employee in column 430500 and/or the name of program in column 430505 are missing.',
      category: 'EFILE'
    },
    '4300007': {
      label: 'The corporation has reported salary and wages in column 430610, but for the same row the ITA identification number, SIN, or name of employee in column 430600 and/or the name of program in column 430605 are missing.',
      category: 'EFILE'
    },
    '4300008': {
      label: 'The corporation has reported an amount in columns 430120 and/or 430420, but Schedule 428 is not completed.',
      category: 'EFILE'
    },

    //T2S442
    '4420001': {
      label: 'There is a claim for the Yukon research and development tax credit at line 005698, but there is no entry in all of lines 442103, 442130, 442140, and 442145.',
      category: 'EFILE'
    },

    //T2S460
    '4600001': {
      label: 'The corporation is claiming the Norwest Territories investment tax credit at line 005705, but Schedule 460 is not completed.',
      category: 'EFILE'
    },
    '4600010': {
      label: 'The corporation is claiming a credit for an investment in labour-sponsored venture capital corporations at line 460050, but no certificate number has been entered at line 460001.',
      category: 'EFILE'
    },
    '4600011': {
      label: 'The corporation is claiming a credit for an investment in community-endorsed venture capital corporations at line 460051, but no certificate number has been entered at line 460002.',
      category: 'EFILE'
    },
    '4600012': {
      label: 'The corporation is claiming a credit for a direct investment in territorial business corporations at line 460052, but no certificate number has been entered at line 460003.',
      category: 'EFILE'
    },

    //T2S490
    '4900005': {
      label: 'The corporation is claiming the Nunavut business training tax credit at line 005740, but Schedule 490 is not completed.',
      category: 'EFILE'
    },
    '4900015': {
      label: 'The corporation has completed Schedule 490 for the Nunavut business training tax credit, but the amount of business training expenses is not provided at line 490130.',
      category: 'EFILE'
    },

    //T2S502
    '5020005': {
      label: 'The corporation is claiming an Ontario tax credit for manufacturing and processing at line 005406, but Schedule 502 is not completed.',
      category: 'EFILE'
    },
    '5020010': {
      label: 'The corporation has reported Canadian farming, fishing or logging income, but there are entries missing at lines 502100, 502110, 502120, 502130, or 502140.',
      category: 'EFILE'
    },
    '5020015': {
      label: 'The corporation has reported Canadian industries mining operations incomes, but there are entries missing at lines 502200, 502210, 502220, 502230, or 502240.',
      category: 'EFILE'
    },

    //T2S504
    '5040001': {
      label: 'The corporation has reported an Ontario additional tax re Crown royalties at line 005274, but Schedule 504 is not completed.',
      category: 'EFILE'
    },
    '5040002': {
      label: 'The corporation has reported an Ontario additional tax re Crown royalties at line 005274, but there is no entry at line 504100.',
      category: 'EFILE'
    },
    '5040003': {
      label: 'The corporation is claiming an Ontario resource tax credit at line 005404, but Schedule 504 is not completed.',
      category: 'EFILE'
    },
    '5040004': {
      label: 'The corporation is claiming an Ontario resource tax credit at line 005404, but there is no entry at one or more of lines 504105, 504115, 504120, and 504130.',
      category: 'EFILE'
    },
    '5040005': {
      label: 'The corporation is claiming an Ontario current year resource tax credit at line 504130, but there is no entry at line 504105.',
      category: 'EFILE'
    },

    //T2S506
    '5060001': {
      label: 'There is an entry at line 005276 or at line 005414, but Schedule 506 is not completed.',
      category: 'EFILE'
    },
    //'5060002': {
    //  label: 'There is an entry at line 506170, but no election has been made at line 506400.',
    //  category: 'EFILE'
    //},
    //'5060002': {
    //  label: 'An election has been made at line 506400, but there is no entry at line 506170.',
    //  category: 'EFILE'
    //},
    '5060003': {
      label: 'There is an entry at line 506460, but there is no entry at one or more of lines 506638, 506644, or 506670, or there is no entry at line 506660.',
      category: 'EFILE'
    },
    '5060004': {
      label: 'There is an entry at line 506470, but there is no entry at lines 506735 and 506740.',
      category: 'EFILE'
    },
    '5060005': {
      label: 'There is an entry at line 506735, but there is no entry at line 506660.',
      category: 'EFILE'
    },
    '5060006': {
      label: 'There is an entry at line 506435, but there is no entry at line 506430.',
      category: 'EFILE'
    },

    //T2S508
    '5080005': {
      label: 'The corporation is claiming Ontario research and development tax credit at line 005416, but Schedule 508 is not completed.',
      category: 'EFILE'
    },
    '5080010': {
      label: 'The corporation is claiming a recapture of Ontario research and development tax credit at line 005277, but there is no entry in all of lines 508700, 508710, 508720, 508730, 508740, 508750, and 508760.',
      category: 'EFILE'
    },
    '5080015': {
      label: 'The corporation has reported an amount at line 508200, 508201, or 508202, but there is no entry at lines 508100 and 508110.',
      category: 'EFILE'
    },
    '5080020': {
      label: 'The corporation has reported an amount at line 508215, but there is no entry at line 508210.',
      category: 'EFILE'
    },
    '5080021': {
      label: 'The corporation has reported an amount at line 508216, but there is no entry at line 508211.',
      category: 'EFILE'
    },
    '5080022': {
      label: 'The corporation has reported an amount at line 508217, but there is no entry at line 508212.',
      category: 'EFILE'
    },
    '5080025': {
      label: 'The corporation has reported an amount at line 508225, but there is no entry at line 508220.',
      category: 'EFILE'
    },
    '5080030': {
      label: 'The corporation has indicated at line 508315 that the corporation was waiving all or part of the ORDTC, but there is no entry at line 508320.',
      category: 'EFILE'
    },
    '5080035': {
      label: 'The corporation has reported an amount at lines 508901, 508902, or 508903, but has not reported current portion of Ontario research and development tax credit.',
      category: 'EFILE'
    },

    //T2S510
    '5100001': {
      label: 'The corporation is reporting an Ontario corporate minimum tax at line 005278 or is claiming an Ontario corporate minimum tax credit at line 005418, but Schedule 510 is not completed.',
      category: 'EFILE'
    },
    '5100002': {
      label: 'Ontario corporate minimum tax is reported at line 005278, but there is no entry in Part 2 of Schedule 510 and both of Schedule 1 and Schedule 125 are not completed.',
      category: 'EFILE'
    },
    '5100003': {
      label: 'Corporation is subject to CMT, but Schedule 510 is not completed.',
      category: 'EFILE'
    },
    '5100004': {
      label: 'Schedule 510 has been completed, but there is no entry in: • all of lines 510112, 510114, and 510116; or • all of lines 510142, 510144, and 510146.',
      category: 'EFILE'
    },
    '5100005': {
      label: 'Corporation is subject to CMT, but Schedule 510 is not completed.',
      category: 'EFILE'
    },

    //T2S511
    '5110001': {
      label: 'The corporation has reported total assets of associated corporations at line 510116 and/or total revenue of associated corporations at line 510146, but Schedule 511 is not completed.',
      category: 'EFILE'
    },
    '5110002': {
      label: 'There are not a corresponding and equal number of entries in columns 511200, 511300, and (511400 and/or 511500).',
      category: 'EFILE'
    },
    '5110003': {
      label: 'Schedule 510 has been completed and the corporation is associated, but Schedule 511 is not completed.',
      category: 'EFILE'
    },

    //T2S512
    '5120001': {
      label: 'The corporation has reported a special additional tax at line 005280, but Schedule 512 is not completed.',
      category: 'EFILE'
    },
    '5120002': {
      label: 'The corporation is a life insurance corporation with a permanent establishment in Ontario, but Schedule 512 is not completed.',
      category: 'EFILE'
    },
    '5120003': {
      label: 'The corporation is a resident life assurance corporation, but Part 1 and/or Part 2 of Schedule 512 is not completed.',
      category: 'EFILE'
    },
    '5120004': {
      label: 'The corporation is a non-resident life assurance corporation, but Part 3 of Schedule 512 is not completed.',
      category: 'EFILE'
    },
    '5120005': {
      label: 'Schedule 512 has been completed and the capital allowance is claimed at line 512330, but there is no entry at line 512310.',
      category: 'EFILE'
    },

    //T2S513
    '5130001': {
      label: 'The life insurance corporation indicated that it is member of a related group and is claiming a capital allowance on Schedule 512, but Schedule 513 is not completed.',
      category: 'EFILE'
    },
    '5130002': {
      label: 'The filer corporation is not listed on the Schedule 513.',
      category: 'EFILE'
    },
    '5130003': {
      label: 'The name of the life insurance corporation in column 513200 or the Business number in column 513300 is missing on the Schedule 513.',
      category: 'EFILE'
    },

    //T2S525
    '5250005': {
      label: 'The corporation is claiming the Ontario political contribution tax credit at line 005415, but Schedule 525 is not completed.',
      category: 'EFILE'
    },

    //T2S546
    '5460001': {
      label: 'More than one Corporations Information Act Annual Return have been completed.',
      category: 'EFILE'
    },
    '5460002': {
      label: 'The corporation has completed Schedule 546, but has not provided the name, the date of incorporation or amalgamation and/or the Ontario corporation number.',
      category: 'EFILE'
    },
    '5460003': {
      label: 'The corporation has completed Schedule 546, but has not provided the head or registered address information required.',
      category: 'EFILE'
    },
    '5460004': {
      label: 'The corporation has indicated 2 (changes) at line 546300, but has not provided the changes information required (where applicable).',
      category: 'EFILE'
    },
    '5460005': {
      label: 'The corporation has indicated 1 or 2 at line 546500, but has provided mailing address information.',
      category: 'EFILE'
    },
    '5460006': {
      label: 'The corporation has indicated 3 at line 546500, but has not provided the mailing address information required.',
      category: 'EFILE'
    },
    '5460007': {
      label: 'The corporation has not provided complete certification information at lines 546300, 546450, 546451 and 546460.',
      category: 'EFILE'
    },
    '5460008': {
      label: 'The corporation has indicated 1 (no changes) at line 546300, but has provided changes information in these parts: - mailing address (lines 546500 to 546590); and/or - language of preference (line 546600); and/or - director/officer Information (Schedule 547).',
      category: 'EFILE'
    },
    'part2.addressCheck':{
      label: 'Address field is updated from TOC-Tax Optimiser Checklist. Please verify if data is correct.',
      category: 'CUSTOM',
      severity: 'warning',
      ignorable: true
    },

    //T2S547
    '5470001': {
      label: 'The corporation has reported director/officer information on Schedule 547, but Schedule 546 is not completed or the entry at line 546300 is 1.',
      category: 'EFILE'
    },
    '5470002': {
      label: 'The Schedule 547 has been completed, but the corporation has not provided the changes information required (where applicable).',
      category: 'EFILE'
    },
    '5470003': {
      label: 'The corporation has indicated a date ceased for the director at line 547797, but has not provided the date elected/appointed at line 547796.',
      category: 'EFILE'
    },
    '5470004': {
      label: 'The corporation has indicated a date elected/appointed and/or ceased at lines 547796 and/or 547797, but there is no answer at line 547795.',
      category: 'EFILE'
    },
    '5470005': {
      label: 'The corporation has indicated 1 or 2 at line 547795, but has not provided the changes information required at lines 547796 and/or 547797.',
      category: 'EFILE'
    },
    '5470006': {
      label: 'The corporation has indicated a ceased date for an officer but has not provided the corresponding date elected/appointed.',
      category: 'EFILE'
    },

    //T2S548
    '5480001': {
      label: 'More than one Corporations information Act Annual Return have been completed.',
      category: 'EFILE'
    },
    '5480002': {
      label: 'The corporation has completed Schedule 548, but has not provided the name, the jurisdiction, the date of incorporation or amalgamation and/or the Ontario corporation number.',
      category: 'EFILE'
    },
    '5480003': {
      label: 'The corporation has completed Schedule 548, but has not provided the head or registered address information required.',
      category: 'EFILE'
    },
    '5480004': {
      label: 'The corporation has indicated 2 (changes) at line 548300, but has not provided the changes information required (where applicable).',
      category: 'EFILE'
    },
    '5480005': {
      label: 'The corporation has indicated 1 at line 548500, but has provided address of principal office in Ontario information.',
      category: 'EFILE'
    },
    '5480006': {
      label: 'The corporation has indicated 2 at line 548500, but has not provided the changes information required.',
      category: 'EFILE'
    },
    '5480007': {
      label: 'The corporation has indicated a date ceased for the activities but has not provided the date activities commenced.',
      category: 'EFILE'
    },
    '5480008': {
      label: 'The corporation has indicated a name for the chief officer or manager but has not provided the address and date appointed.',
      category: 'EFILE'
    },
    '5480009': {
      label: 'The corporation has indicated an address for the chief officer or manager but has not provided the name and date appointed.',
      category: 'EFILE'
    },
    '5480010': {
      label: 'The corporation has indicated a date appointed for the chief officer or manager but has not provided the name and address.',
      category: 'EFILE'
    },
    '5480011': {
      label: 'The corporation has indicated a date ceased for the chief officer or manager but has not provided the date appointed.',
      category: 'EFILE'
    },
    '5480012': {
      label: 'Both parts for individual and corporation information are completed.',
      category: 'EFILE'
    },
    '5480013': {
      label: 'Both parts for individual and corporation information are completed.',
      category: 'EFILE'
    },
    '5480014': {
      label: 'The address of the individual is not completed.',
      category: 'EFILE'
    },
    '5480015': {
      label: 'The address of the corporation is not completed.',
      category: 'EFILE'
    },
    '5480016': {
      label: 'The corporation has not provided complete certification information at lines 548300, 548450, 548451, and 548460.',
      category: 'EFILE'
    },
    '5480017': {
      label: 'Part 9 – Agent for Service in Ontario is incomplete.',
      category: 'EFILE'
    },
    '5480018': {
      label: 'The corporation has indicated 1 (no changes) at line 548300, but has provided changes information in these parts: - address of principal office in Ontario (lines 548500 to 548570); and/or - language of preference (line 548600); and/or - date activity commenced or ceased in Ontario (lines 548700 to 548710); and/or - chief officer or manager in Ontario (lines 548800 to 5488950); and/or - agent for Service in Ontario (lines 548900 to 548980).',
      category: 'EFILE'
    },
    '5480019': {
      label: 'The corporation has completed Schedule 548 – Corporations Information Act Annual Return for Foreign Business Corporations, but the jurisdiction at line 548110 is not outside Canada.',
      category: 'EFILE'
    },

    //T2S550
    '5500010': {
      label: 'The corporation is claiming the Ontario co-operative education tax credit at line 005452, but Schedule 550 is not completed.',
      category: 'EFILE'
    },
    '5500020': {
      label: 'The corporation has reported eligible expenditures for the Ontario co-operative education tax credit in columns 550450 and/or 550452, but corporation’s salaries and wages paid in the previous tax year are not provided at line 550300.',
      category: 'EFILE'
    },
    '5500030': {
      label: 'The corporation has reported eligible expenditures for the Ontario co-operative education tax credit in columns 550450 and/or 550452, but there are no corresponding entries in columns 550400, 550405, 550410, 550430, and 550435.',
      category: 'EFILE'
    },
    '5500040': {
      label: 'The corporation has reported a repayment of government assistance for an Ontario co-operative education tax credit in column 550480 without corresponding entries in columns 550400, 550405, 550410, 550430, and 550435.',
      category: 'EFILE'
    },
    '5500045': {
      label: 'The corporation has provided entries to indicate a claim for CETC on eligible expenditures or on repayment of government assistance, but the eligible expenditures or CETC on repayment of government assistance is not provided in column 550450, 550452, or 550480.',
      category: 'EFILE'
    },
    '5500050': {
      label: 'The corporation indicates the Ontario co-operative education tax credit was earned through a partnership, but the name of the partnership is not provided at line 550160.',
      category: 'EFILE'
    },
    '5500060': {
      label: 'The corporation indicates that the claim for the Ontario co-operative education tax credit was earned through a partnership, but the percentage of the partnership’s CETC allocated to the corporation is not provide at line 550170.',
      category: 'EFILE'
    },
    '5500065': {
      label: 'The corporation indicates at line 550150 that the claim for the Ontario co-operative education tax credit was not earned through a partnership, but the name of a partnership is provided at line 550160 and/or a percentage of a partnership’s CETC allocated to the corporation is provided at line 550170.',
      category: 'EFILE'
    },
    '5500070': {
      label: 'The corporation is claiming the Ontario co-operative education tax credit at line 550500, but has not answered the eligibility questions at lines 550200 and 550210.',
      category: 'EFILE'
    },

    //T2S552
    '5520010': {
      label: 'The corporation is claiming the Ontario apprenticeship training tax credit at line 005454, but Schedule 552 is not completed.',
      category: 'EFILE'
    },
    '5520020': {
      label: 'The corporation has reported eligible expenditures for the Ontario apprenticeship training tax credit in columns 552452 and/or 552453, but the corporation\'s salaries and wages paid in previous tax year are not provided at line 552300.',
      category: 'EFILE'
    },
    '5520030': {
      label: 'The corporation has reported eligible expenditures for the Ontario apprenticeship training tax credit in columns 552452 and/or 552453, but there are no corresponding entries in columns 552400, 552405, 552410, 552420, 552425, 552430, and/or 552435.',
      category: 'EFILE'
    },
    '5520040': {
      label: 'The corporation has reported an Ontario apprenticeship training tax credit on repayment of government assistance in column 552480, but there are no corresponding entries in columns 552400, 552405, 552410, 552420, 552425, 552430, and 552435.',
      category: 'EFILE'
    },
    '5520045': {
      label: 'The corporation has provided entries to indicate a claim for ATTC on eligible expenditures or on a repayment of government assistance, but the eligible expenditures or repayment of government assistance is not provided in columns 552452, 552453, or 552480.',
      category: 'EFILE'
    },
    '5520050': {
      label: 'The corporation indicates at line 552150 that the Ontario apprenticeship training tax credit was earned through a partnership, but has not provided the name of the partnership at line 552160.',
      category: 'EFILE'
    },
    '5520060': {
      label: 'The corporation indicates at line 552150 that the Ontario apprenticeship training tax credit was earned through a partnership, but has not provided the percentage of the partnership’s ATTC allocated to the corporation at line 552170.',
      category: 'EFILE'
    },
    '5520065': {
      label: 'The corporation indicates at line 552150 that the claim for the Ontario apprenticeship training tax credit was not earned through a partnership, but the name of a partnership is provided at line 552160 and/or a percentage of a partnership’s CETC allocated to the corporation is provide at line 552170.',
      category: 'EFILE'
    },
    '5520070': {
      label: 'The corporation is claiming an Ontario apprenticeship training tax credit at line 552500, but has not answered the eligibility questions at lines 552200 and 552210.',
      category: 'EFILE'
    },
    '5520184': {
      label: 'For the same row, the number of days reported in column 552442 exceeds the number of days in the tax year that span the start date in column 552430 and the date in column 552435.',
      category: 'EFILE'
    },
    '5520185': {
      label: 'For the same row, the number of days reported in column 552443 exceeds the number of days in the tax year that span the start date in column 552430 and the date in column 552435.',
      category: 'EFILE'
    },

    //T2S554
    '5540005': {
      label: 'The corporation is claiming an Ontario computer animation and special effects tax credit at line 005456, but Schedule 554 is not completed.',
      category: 'EFILE'
    },
    '5540010': {
      label: 'As the corporation is claiming an Ontario computer animation and special effects tax credit at lines 005456, 554610 and 554611, the certificate of eligibility must be filed.',
      category: 'EFILE'
    },
    '5540015': {
      label: 'There is an entry at lines 554610 and/or 554611, but there is no entry at one or more of lines 554200, 554210, or 554220.',
      category: 'EFILE'
    },
    '5540020': {
      label: 'There is an entry at lines 554610 and/or 554611, but there is no entry at one or more of lines 554300, 554310, 554320, or 554330.',
      category: 'EFILE'
    },

    //T2S556
    '5560005': {
      label: 'The corporation is claiming an Ontario film and television tax credit at line 005458, but Schedule 556 is not completed.',
      category: 'EFILE'
    },
    '5560010': {
      label: 'As the corporation is claiming an Ontario film and television tax credit at any of lines 005458, 556755, 556815, or 556915, the certificate of eligibility must be filed.',
      category: 'EFILE'
    },
    '5560015': {
      label: 'There is an entry at line 556755, 556815, or 556915, but there is no entry at one or more of lines 556200, 556205, 556210, 556215, 556220, 556225, 556230, 556235, 556240, 556245, or 556250.',
      category: 'EFILE'
    },
    '5560020': {
      label: 'There is an entry at lines 556755, 556815 or 556915, but there is no entry at one or more of lines 556300, 556310, 556320, 556330, 556340, or 556350.',
      category: 'EFILE'
    },
    '5560025': {
      label: 'There is an entry at lines 556755, 556815, or 556915, but there is no entry at line 556400.',
      category: 'EFILE'
    },

    //T2S558
    '5580005': {
      label: 'The corporation is claiming an Ontario production services tax credit at line 005460, but Schedule 558 is not completed.',
      category: 'EFILE'
    },
    '5580010': {
      label: 'As the corporation is claiming an Ontario production services tax credit at line 005460 and/or 558720, the certificate of eligibility must be filed.',
      category: 'EFILE'
    },
    '5580015': {
      label: 'There is an entry at line 558720, but there is no entry at one or more of lines 558200, 558210, 558220, 558230, or 558240.',
      category: 'EFILE'
    },
    '5580020': {
      label: 'There is an entry at line 558720, but there is no entry at one or more of lines 558300, 558310, 558320, or 558330.',
      category: 'EFILE'
    },
    '5580025': {
      label: 'There is an entry at line 558720, but there is no entry at line 558400.',
      category: 'EFILE'
    },

    //T2S560
    '5600005': {
      label: 'The corporation is claiming an Ontario interactive digital media tax credit at line 005462, but Schedule 560 is not completed.',
      category: 'EFILE'
    },
    '5600010': {
      label: 'As the corporation is claiming an Ontario interactive digital media tax credit at lines 005462 and/or 560620, the certificate of eligibility must be filed.',
      category: 'EFILE'
    },
    '5600015': {
      label: 'There is an entry at line 560620, but there is no entry at one or more of lines 560200, 560210, or 560220.',
      category: 'EFILE'
    },
    '5600020': {
      label: 'There is an entry at line 560620, but there is no entry at one or more of lines 560300, 560310, 560320, or 560330.',
      category: 'EFILE'
    },
    '5600022': {
      label: 'There is an entry at line 560470, 560471, 560472, 560473, 560575, 560576, 560577, or 560578, but there is no entry at lines 560400 and/or 560403.',
      category: 'EFILE'
    },

    //T2S562
    '5620010': {
      label: 'As the corporation is claiming an Ontario sound recording tax credit at line 005464 and/or 562800, the certificate of eligibility must be filed.',
      category: 'EFILE'
    },
    '5620020': {
      label: 'As the corporation is claiming an Ontario sound recording tax credit at line 005464 and/or 562800, the certificate of eligibility or a copy of the certificate must be filed.',
      category: 'EFILE',
      severity: 'none'
    },
    '5620030': {
      label: 'The corporation is claiming an Ontario sound recording tax credit at line 562800, but there is no entry at one or more of lines 562200, 562210, 562220, 562230, 562240, or 562250.',
      category: 'EFILE'
    },
    '5620040': {
      label: 'The corporation has reported qualifying expenditures incurred primarily in Ontario at line 562700, but there is no entry at all of lines 562400, 562410, 562420, 562430, 562440, 562450, 562460, 562470, 562480, 562490, 562500, 562590, 562600, 562610, 562620, 562630, 562640, 562650, 562660, 562670, 562680, and 562690.',
      category: 'EFILE'
    },
    '5620050': {
      label: 'The corporation has reported expenditures incurred outside Ontario at line 562705, but there is no entry at all of lines 562510, 562595, 562605, 562615, 562625, 562635, 562645, 562655, 562665, 562675, 562685, and 562695.',
      category: 'EFILE'
    },
    '5620060': {
      label: 'The corporation indicates that the Ontario sound recording tax credits was earned through a partnership, but has not provided the name of the partnership at line 562160.',
      category: 'EFILE'
    },
    '5620070': {
      label: 'The corporation indicates that the Ontario sound recording tax credits was earned through a partnership, but has not provided the percentage of the partnership\'s credit allocated to the corporation at line 562170.',
      category: 'EFILE'
    },
    '5620080': {
      label: 'The corporation is claiming an Ontario sound recording tax credit at line 562800, but has not answered all the eligibility questions at lines 562300, 562310, 562320, or 562330.',
      category: 'EFILE'
    },

    //T2S564
    '5640010': {
      label: 'As the corporation is claiming an Ontario book publishing tax credit at lines 005466 and/or 564550, the certificate of eligibility must be filed.',
      category: 'EFILE'
    },
    '5640020': {
      label: 'As the corporation is claiming an Ontario book publishing tax credit at lines 005466 and/or 564550, the certificate of eligibility or a copy of the certificate must be filed.',
      category: 'EFILE',
      severity: 'none'
    },
    '5640030': {
      label: 'The corporation is claiming an Ontario book publishing tax credit at line 564550, but there is no entry at one or more of lines 564200, 564210, 564220, 564230, 564240, or 564250.',
      category: 'EFILE'
    },
    '5640040': {
      label: 'The corporation has reported qualifying expenditures at line 564500, but there is no entry at all of lines 564400, 564410, 564420, 564430, 564435, 564440, 564450, 564460, 564465, 564470, 564480, and 564490.',
      category: 'EFILE'
    },
    '5640050': {
      label: 'The corporation indicates that the Ontario book publishing tax credit was earned through a partnership, but has not provided the name of the partnership at line 564160.',
      category: 'EFILE'
    },
    '5640060': {
      label: 'The corporation indicates that the Ontario book publishing tax credit was earned through a partnership, but has not provided the percentage of the partnership’s credit allocated to the corporation at line 564170.',
      category: 'EFILE'
    },
    '5640070': {
      label: 'The corporation is claiming an Ontario book publishing tax credit at line 564550, but has not answered all the eligibility questions at lines 564300, 564310, 564320, and 564330.',
      category: 'EFILE'
    },

    //T2S566
    '5660005': {
      label: 'The corporation is claiming an Ontario innovation tax credit at line 005468, but Schedule 566 is not completed.',
      category: 'EFILE'
    },
    '5660010': {
      label: 'The corporation is claiming an Ontario innovation tax credit at line 566710, 566711, or 566712, but has not answered all the eligibility questions at lines 566100, 566105, 566110, 566115, and 566120.',
      category: 'EFILE'
    },
    '5660015': {
      label: 'The corporation is claiming an Ontario innovation tax credit at line 566710, 566711, or 566712, but has not reported qualified expenditures or eligible repayments at any of lines 566215, 566300, 566305, 566310, and 566315.',
      category: 'EFILE'
    },
    '5660025': {
      label: 'The corporation has calculated designated repayments made in the year of government or non-government assistance or contract payments relating to Ontario qualified expenditures for first term or second term shared-use equipment × 25% at line 566315, but there is no entry at line 566310.',
      category: 'EFILE'
    },
    '5660030': {
      label: 'The corporation is claiming an Ontario innovation tax credit at line 566710, 566711, or 566712, but has not reported the total of all taxable incomes of the corporation and of its associated corporations for their last tax year ending in the previous calendar year at line 566420.',
      category: 'EFILE'
    },
    '5660040': {
      label: 'The corporation has reported a specified capital amount for the corporation and each of its associated corporations at line 566425 or 566505, but there are not a corresponding and equal number of entries in columns 566510, 566515, and 566520 or there is no entry in columns 566510, 566515, and 566520 for the filing corporation.',
      category: 'EFILE'
    },
    '5660050': {
      label: 'If there is an entry at any one of these lines, and the corporation is associated, and there is no entry at line 031385 or the entry at line 031385 is 1, there must be a corresponding and equal number of entries in columns 566600, 566605, and 566610 and the Business number of the filing corporation (line 200001) is required in column 566605.	5660050	The corporation is claiming an Ontario innovation tax credit at line 566710, 566711, or 566712 and is associated, but there are not a corresponding and equal number of entries in columns 566600, 566605, and 566610, or there is no entry in columns 566600, 566605, and 566610, for the filing corporation.',
      category: 'EFILE'
    },
    '5660055': {
      label: 'The corporation has indicated that the corporation was waiving all or part of the Ontario innovation tax credit at line 566715, but there is no entry at line 566720.',
      category: 'EFILE'
    },

    //T2S568
    '5680005': {
      label: 'The corporation is claiming an Ontario business-research institute tax credit at line 005470, but Schedule 568 is not completed.',
      category: 'EFILE'
    },
    '5680010': {
      label: 'The corporation has reported qualified expenditures for the OBRITC for the tax year at line 568410, but there is no entry at line 568100.',
      category: 'EFILE'
    },
    '5680012': {
      label: 'The corporation has reported qualified expenditures for the OBRITC for the tax year at line 568410, but there is no entry at line 568105.',
      category: 'EFILE'
    },
    '5680015': {
      label: 'The corporation is claiming an Ontario business-research institute tax credit at line 005470, but there is no entry at line 568200.',
      category: 'EFILE'
    },
    '5680025': {
      label: 'The corporation is associated and is claiming the Ontario business-research institute tax credit at line 005470, but Part 3 of Schedule 568 is not completed.',
      category: 'EFILE'
    },
    '5680030': {
      label: 'The corporation is associated and is claiming the Ontario business-research institute tax credit at line 005470, but there are not a corresponding and equal number of entries in columns 568300, 568305, and 568310.',
      category: 'EFILE'
    },

    //T2S569
    '5690005': {
      label: 'The corporation has reported total qualified expenditures at line 005405, but Schedule 569 is not completed.',
      category: 'EFILE'
    },
    '5690010': {
      label: 'There is an entry at lines 569300 and 569310, but there is no entry at one or more of lines 569110, 569115, 569120, and 569135.',
      category: 'EFILE'
    },
    '5690015': {
      label: 'The contract date is before August 10, 2007, but there is no entry in one or more of lines 569125 and 569130.',
      category: 'EFILE'
    },
    '5690020': {
      label: 'The corporation is claiming an Ontario business-research institute tax credit earned through a partnership, but there is no entry at one or more of lines 569140, 569145, and 569150.',
      category: 'EFILE'
    },
    '5690025': {
      label: 'There is an entry at lines 569300 and 569310, but there is no entry at one or more of lines 569200, 569205, 569210, 569215, 569220, 569225, and 569230.',
      category: 'EFILE'
    },
    '5690030': {
      label: 'There is an entry at line 569310, but there is no entry at line 569300.',
      category: 'EFILE'
    },

    // T2S4
    //'0040002': {
    //  label: 'The corporation has answered YES at line 200204, but Schedule 4 is not completed.',
    //  isUnique: true
    //},
    //
    //'5130002': {
    //  label: 'The filer corporation is not listed on the Schedule 513.',
    //  category: 'EFILE',
    //  severity: 'warning',
    //  isUnique: true
    //},

    // T2S9
    //'0090001': {
    //  label: 'The corporation has answered YES at line 200150, but Schedule 9 is not completed',
    //  severity: 'critical',
    //  type: 'form'
    //}

    //Custom error codes
    'default': {
      label: 'Currently using default value.',
      category: 'DEFAULT',
      severity: 'warning',
      ignorable: true
    },
    'sec20.beforeDate': {
      label: 'You have indicated that the expense has been incurred in the previous tax year but Column 5 "Amount deducted in previous taxation year" has no entry.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'sec20.afterDate': {
      label: 'The expenses that occurred in the current tax year or in the previous tax year can be claimed under paragraph 20(1)(2). Please verify the date of expense.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'sec20.withinDate': {
      label: 'The expense has been incurred during the fiscal period and Column 5 "Amount deducted in previous taxation year" has been entered. Please verify.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '001.113': {
      label: 'If there is an amount at this line and the corporation has taxable income, Schedule 7 is required.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '001.114': {
      label: 'Line 114 must be greater than or equal to the total of lines 525120 and 005890 to 005899 inclusive.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '001.216': {
      label: 'A deduction under S20(1)(e) ITA has been claimed on line 395, but no amount has been added back on line 216 or line 235.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '003.455': {
      label: 'The eligible dividends paid in the year is greater than the balance of the general rate income pool (GRIP) at the end of the taxation year reported in Schedule 5. Therefore, the corporation must pay a Part III. 1 tax. Make sure that the dividend amount in column R1 and/or on line 450a are correct. Also verify if the corporation makes the election under subsection 185.1(2), to treat the eligible dividends as ordinary dividends.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '004.atRisk': {
      label: 'Verify if the corporation\'s at-risk amount has to be entered in line 606.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '005.507': {
      label: 'Cannot be more than $50,000.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '005.522': {
      label: 'The corporation is claiming the Newfoundland and Labrador interactive digital media tax credit on line 522 of Schedule 5, the interactive digital media tax credit certificate (or a copy) must be filed.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '005.840': {
      label: 'If there is an entry at line 522, an entry is required at this line or at line 300 of the Schedule 309.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '005.556': {
      label: 'This amount cannot be more than the gross Nova Scotia tax minus all other Nova Scotia credits.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '005.839': {
      label: 'If there is an entry at line 569, an entry is required at this line or at line 300 of the Schedule 348.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '027.329': {
      label: 'The amount at line A12 cannot be greater than the amount at line I6.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '027.344': {
      label: 'The amount at line A12 should include the amount entered at line A7.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '031.trainingTaxCredit': {
      label: 'Ontario apprenticeship training tax credits have been claimed in Schedule 552. Verify if these credits are also eligible for ITC from apprenticeship job creation expenditures.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '031.102': {
      label: 'For TYE 2010 and subsequent, if Yes (1), an entry is required at line 103.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '031.350': {
      label: 'Requires an entry in at least one of lines 031103, 032380, 032500, 032502, or 032508.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '031.360': {
      label: 'Requires an entry in at least one of lines 032390, 032504, or 032510. Capital expenditures incurred after December 31, 2013 are not qualified SR&ED expenditures.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '031.370': {
      label: 'Requires an entry at line 032560.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '031.420': {
      label: 'Eligible for the 35% rate Requires an entry in at least one of lines 032300, 032305, 032310, 032320, 032340, 032345, 032350, 032355, 032360, 032370, 032500, 032502, or 032508. For corporation that are not CCPCs, enter "0".',
      category: 'TAX RULE',
      severity: 'required'
    },
    '031.430': {
      label: 'Eligible for the 20% rate for tax years that end after 2013, the general SR&ED rate is reduced from 20% to 15%, except that, for 2014 tax years that start before 2014, the reduction is pro-rated based on the number of days in the tax year that are after 2013. Requires an entry in at least one of lines 032300, 032305, 032310, 032320, 032340, 032345, 032350, 032355, 032360, 032370, 032500, 032502, or 032508. If negative,enter "0".',
      category: 'TAX RULE',
      severity: 'required'
    },
    '031.440': {
      label: 'Eligible for the 35% rate requires an entry in at least one of lines 032390, 032504, or 032510. For corporation that are not CCPCs, enter "0".',
      category: 'TAX RULE',
      severity: 'required'
    },
    '031.450': {
      label: 'Eligible for the 20% rate requires an entry in at least one of lines 032390, 032504, or 032510. If negative, enter "0".',
      category: 'TAX RULE',
      severity: 'required'
    },
    '031.460': {
      label: 'Requires an entry at line 032560.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '031.480': {
      label: 'Requires an entry at line 032560.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '031.490': {
      label: 'Requires an entry at line 032560.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '031.611': {
      label: 'The apprenticeship job creation tax credit cannot be claimed because the corporation has entered "No" at line 611 of part 21.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '031.650': {
      label: 'If the answer at line 101 is Yes (1), there must be an entry at this line.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '031.830': {
      label: 'If there is an entry at this line there must be an entry at one or more of lines 810, 811, 812, 813, 820, 821, or in column 826.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '047.304': {
      label: 'There must be an entry at this line if there is no entry at lines 305 and 306. Format = ANNNNNN (that starts by A or B); or Format = NNNNNNNNN.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '047.305': {
      label: 'There must be an entry at this line if there is no entry at line 304. Format = ANNNNNN (that starts by A or B).',
      category: 'TAX RULE',
      severity: 'required'
    },
    '047.306': {
      label: 'There must be an entry at this line if there is an entry at line 305. Format = ANNNNNN (that starts by A or B).',
      category: 'TAX RULE',
      severity: 'required'
    },
    '050.050': {
      label: 'Provide shareholder information, if a shareholder holds 10% or more shares.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '050.200': {
      label: 'Only one of line 200, 300 or 350 can be entered per row.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '053.checkBoxes': {
      label: 'Schedule 53 should only be completed if the corporation is a Canadian-controlled private corporation (CCPC). As indicated on line 040 of the Corporation Profile, this corporation is not a CCPC. Complete Schedule 54 instead.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '053.200': {
      label: 'This line cannot be greater than line 200320.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '053.1100': {
      label: 'Specify an applicable type of adjustment for further calculation.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'EFILE.type': {
      label: 'The corporation has indicated that the return was prepared by a tax preparer for a fee but also wants to transmit it\'s own corporate tax return. Please verify the credentials in form "EI - Efile Information.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    'CP.071': {
      label: 'The corporation has entered "Yes" at line 070 or 071. Verify that the tax year-end date is correct at line 061. Click the REMOVE at the top right of this popup to remove this diagnostic.',
      category: 'TAX RULE',
      severity: 'warning',
      ignorable: true
    },
    'CP.105': {
      label: 'An entry is required for Incorporation date.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'CP.107': {
      label: 'The tax year start date does not agree with the date of amalgamation provided.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'CP.111': {
      label: 'It is indicated that this is the final return up to dissolution on line 078, but no date of dissolution has been entered.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'CP.146': {
      label: 'The jurisdiction does not include Ontario but a value has been entered for Ontario Corporation account number. Please verify. (If this field is hidden, switch the jurisdiction to Ontario to edit the entry.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'CP.147': {
      label: 'The corporation has indicated that Ontario is one of the jurisdictions but hasn\'t provided an entry for the type of corporation in the current year.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'CP.167': {
      label: 'Partnership data has been entered in Schedule 4, 7 and/or 73. Verify if line 167 should be checked.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'CP.270': {
      label: 'You have indicated that the corporation used IFRS to prepare its financial statements. Verify if any amounts need to be entered at GIFI line 9998.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'CP.totalCheck': {
      label: 'The total percentage for this column must be greater than 0% and less than or equal to 100%',
      category: 'TAX RULE',
      severity: 'critical'
    },
    'CP.011': {
      label: 'The head office address information is required.',
      category: 'MISSING',
      severity: 'critical'
    },
    'CP.088': {
      label: 'The corporation cannot register for the Online mail service upon reassessment submission. The registration can be done through the My Business Account instead.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'CP.termsOfUse': {
      label: 'The corporation has selected to receive online mail but has not accepted the Terms of Use in form EMAIL - Online Mail Terms of Use (Link provided below).',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'CP.2662': {
      label: 'The line 200266 or 200267 has been answered as "Yes". This requires T2202 submission which is not ' +
      'available under the current version. The form is available on CaseWare Review and Compilation - Canada Help. File a complete T2202 with your return.',
      category: 'TAX RULE',
      severity: 'none',
      ignorable: true
    },
    'CP.prov_residence': {
      label: 'Note - Province of Residence in Corporation profile and data entered in Part 1 of Schedule 5 ' +
      'do not match. Please check.',
      category: 'INFO',
      severity: 'warning'
    },
    'CP.shortYear': {
      label: 'Short taxation year entered.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'CP.albertaCorpNumber': {
      label: 'Alberta was entered as a jurisdiction but there is no entry for Alberta Corporation account number.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'CP.ontarioCorpNumber': {
      label: 'Ontario was entered as a jurisdiction but there is no entry for Ontario Corporation account number.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'CP.validNAICS': {
      label: 'An invalid Canadian NAICS code has been entered.',
      category: 'EFILE',
      severity: 'critical'
    },
    'CP.directDepositChange': {
      label: 'The corporation cannot use Corporation Internet Filing service to change the corporation\'s direct' +
      ' deposit information (including new requests). It must be done on a letter, online through CRA or on the phone by contacting the Business Enquiries line.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    'CP.supportedDateCheck': {
      label: 'The tax year end date of the corporation is not within the supported fiscal period that this version supports.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    'CP.IncomeSprinkling':{
      label: 'Income Sprinkling applies to this return. Make sure that the latest CRA requirements are complied with',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '008W.155': {
      label: 'The Cost before GST and PST, or HST must be greater than $30,000 for Class 10.1' +
      ' otherwise Class 10 must be used.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '008W.217AB': {
      label: 'The federal CCA claim does not equal the Alberta CCA claim. Verify if either value needs to be overridden.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '008W.class10.1-1': {
      label: 'A passenger vehicle that was acquired and disposed of in the same taxation year can not be added to class 10.1. No capital cost allowance is allowed with regards to this passenger vehicle for the year.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '008W.class10.1-2': {
      label: 'A current year acquisition should not have an adjustment or opening balance. A new class 10.1 should be created for current year asset.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '008W.class12': {
      label: 'Please select class 12.1 for die, jig, pattern, motion pictures, video tape, etc. or select class 12.2 for all other items not included in 12.1.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '008W.class13Date': {
      label: 'An entry is required for termination date if Class 13 is selected.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '008W.class1WithManufacturing': {
      label: 'Corporation has Manufacturing and Processing and an addition to Class 1. Consider changing to Class 1.2 (10% CCA Rate) or Class 1.3 (6% CCA Rate).',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '421.180': {
      label: 'If there is an entry at line 180 there must be an entry at one or more of lines 100, 110, 120, 130, 140.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '568.315': {
      label: 'Cannot exceed $20 million.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '569.135': {
      label: 'If line 135 is Yes (1), an entry is required at lines 140 and 145.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '569.305': {
      label: 'The number of days reported on line 305 cannot be greater than the number of days in the tax year.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '097.300': {
      label: 'There must be only one type of Canadian income of non-resident corporation but more than one is selected.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '100.foreignCheck': {
      label: 'An amount in one of the following GIFI items are entered: GIFI 1004, 1005, 1187, 2301, 2302, 8091, 8097. Please verify if T1135 is applicable.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '100.duplicate': {
      label: 'This GIFI Code has already been used. CRA disallows re-using the same GIFI code. If you still use this GIFI code, the system will automatically aggregate all amounts allocated to this code during EFILE transmission.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '100.2018': {
      label: 'The value entered for gifi code 2018 must be less than 1.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '100.3660': {
      label: 'The retained earning beginning balance must equal to the ending balance of previous year.',
      category: 'TAX RULE',
      severity: 'warning',
      ignorable: true
    },
    'gifi.noCode': {
      label: 'This row contains an entry but no GIFI code has been entered.',
      category: 'TAX RULE',
      severity: 'required'
    },
    'gifi.requiredPair': {
      label: 'An entry for this GIFI code also requires an entry for the corresponding GIFI code (this code minus 1). This line must be negative and less than or equal to the corresponding line.',
      category: 'TAX RULE',
      severity: 'required'
    },
    'ACB.30Day': {
      label: 'It is indicated that this asset was repurchased within 30 calendar days after the disposition. Verify if this transaction is a superficial loss.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'INS.quarterlyPayment': {
      label: 'The corporation already meets one or more conditions to be eligible for quarterly instalments. Verify the instalments payment method.',
      category: 'TAX RULE',
      severity: 'warning',
      ignorable: true
    },
    'INS.amalgamation': {
      label: 'A new corporation formed on an amalgamation is treated as a continuation of the predecessor corporations. The instalment base may need to be adjusted. Refer to subsection 5301(4) of the Income Tax Regulations.',
      category: 'TAX RULE',
      severity: 'warning',
      ignorable: true
    },
    'INS.NoPay': {
      label: 'The corporation is not required to pay instalments but data has been entered on the Instalments Workchart. Verify that the corporation wishes to proceed with the instalment payments.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'autoFill.diffCheck': {
      label: 'The value imported from T2 AutoFill is different from the current value in CompTax. Select the checkbox in this row to import this value from T2 AutoFill.',
      category: 'autoFill',
      highlightLevel: 'jump',
      severity: 'warning'
    },
    'T2J.refundCheck': {
      label: 'The refund code on line 894 has not been entered.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    'T2J.addressChange': {
      label: 'The corporation cannot use Corporation Internet Filing service to change the corporation\'s ' +
      'head office or mailing address. It must be done on a letter, online through CRA or on the phone by contacting the' +
      ' Business Enquiries line.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    'T2J.894': {
      label: 'You have a balance owing, but the refund code on line 894 is filled. Please verify.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    'T2J.727': {
      label: 'The corporation is not required to pay instalments as taxes payable is less than $3,000. The Instalments workchart has been adjusted accordingly. Please check in case you would like to change this',
      category: 'TAX RULE',
      severity: 'none'
    },
    'T2J.259': {
      label: 'The corporation must complete T1135 for foreign property where the total cost amount of all such property, at any time in the year was more than $100,000.',
      category: 'TAX RULE',
      severity: 'none'
    },
    'T2J.271': {
      label: 'CompTax does not currently support T1134-Information Return Relating to Controlled and Not-Controlled Foreign Affiliate.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'T2J.760': {
      label: 'The corporation is not required to pay instalment on provincial or territorial taxes as the amount is less than $3,000. The Instalment workchart has been adjusted accordingly.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '002W.DonType': {
      label: "This transaction date does not meet the qualification to be included in the deduction calculation. Verify the dates again before proceeding.",
      category: 'TAX RULE',
      severity: 'warning'
    },
    '125.9898Or9368': {
      label: "There must be an entry at either line 8299 or 9368.",
      category: 'TAX RULE',
      severity: 'required'
    },
    '125.8299': {
      label: "There must be an entry at this line unless only farming revenue is reported (i.e. there is an entry at line 1259659).If there is no entry at line 1259659, then there must be an entry at this line.",
      category: 'TAX RULE',
      severity: 'required'
    },
    '125.farmingCheck': {
      label: "There are farming values being reported on this form that are currently hidden. Change the type to \"Farming and Non-farming\" to see all values.",
      category: 'TAX RULE',
      severity: 'warning'
    },
    '125.9368': {
      label: "There must be an entry at this line unless only farming expenses are reported (i.e. there is an entry at GIFI line 1259898).If there is no entry at line 1259898, then there must be an entry at this line.",
      category: 'TAX RULE',
      severity: 'required'
    },
    '125.requiredNegative': {
      label: "This line must contain a negative value.",
      category: 'TAX RULE',
      severity: 'required'
    },
    '125.overMillion': {
      label: 'All corporations with gross revenue in excess of $1 million and filing return with tax year ending in 2010 and subsequent will be required to Internet file their return using the CIF service. See Chapter 2.2.2 for exceptions.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '088.120': {
      label: "The percentage cannot exceed 100%.",
      category: 'TAX RULE',
      severity: 'warning'
    },
    '088.100': {
      label: "The number of websites/webpages reported on line 100 does not correspond to the URL addresses provided",
      category: 'TAX RULE',
      severity: 'warning'
    },
    'rentalWC.partnershipCalc': {
      label: 'Properties from partnerships require the net income amount from the T5013 slip to calculate your share of the rental income (line e). If applicable, enter the amounts for any GST/HST rebates or other partner expenses incurred.',
      category: 'NOTICE',
      severity: 'none'
    },
    'rentalWC.nonCorpPercentage': {
      label: 'The non-corporation portion is automatically calculated and pro-rated if a percentage is entered. Change the non-corporation percentage to zero to override calculated values.',
      category: 'NOTICE',
      severity: 'none'
    },
    '054.date': {
      label: 'The date in Part 2 (Line 200) does not correspond with adjustments dates.',
      category: 'NOTICE',
      severity: 'required'
    },
    '054.540': {
      label: 'An entry for "amount of dividends paid by the corporation during the year" was reported in Schedule 3 but columns 240, 270 and line 540 are empty.',
      category: 'TAX RULE',
      severity: 'required'
    },
    '055.200': {
      label: 'The corporation has indicated that dividends have been paid by the corporation during the year in Schedule 54, but line 200 is blank.',
      category: 'NOTICE',
      severity: 'critical'
    },
    'WAC.400': {
      label: 'Because the transmitter is the corporation, as indicated in the Corporation Profile, you can request a Web Access Code (WAC) online using the WAC form, "WAC - Web Access Code Online Request", or enter the code in the "EI - Efile Information" form.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'TYH.priorIncorporation': {
      label: 'A date prior to the incorporation date has been entered. Please verify.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'TYH.afterCurrentDate': {
      label: 'The year end cannot be after the current date.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    'TYH.numberDaysInYear': {
      label: 'The difference between the current tax year start date and the previous tax year start date does not correspond to an entire year. If this difference is intended due to a specific situation please ignore this diagnostic.',
      category: 'TAX RULE',
      severity: 'warning',
      ignorable: true
    },
    'MNE.type8': {
      label: 'The maximum number of Office party type expenditures allowed per form is 6.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '661.part8Check': {
      label: 'The checklist in part 8 has not been completed.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '661.355': {
      label: 'Enter 50% of lease costs if the corporation uses the proxy method or enter "0" if the corporation uses the traditional method.',
      category: 'NOTICE',
      severity: 'none'
    },
    '661.370': {
      label: 'This line should be the total of all lines 030704. For third-party payments made after 2013, no amounts for purchasing or leasing capital can be included.',
      category: 'NOTICE',
      severity: 'none'
    },
    '008.duplicateClasses': {
      label: 'There are multiple entries for the same class number. Please review the CCA Workchart.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '1135.requiredToFile': {
      label: "One of the these checkboxes must be selected to file T1135.",
      category: 'T1135',
      severity: 'required'
    },
    '1135.100': {
      label: "Individual Code 1 or 2 is required",
      category: 'T1135',
      severity: 'required'
    },
    '1135.headOfficeAddressCheck': {
      label: "Head office information is required.",
      category: 'T1135',
      severity: 'required'
    },
    '1135.addressCheck': {
      label: "A period (.) is not allowed at the start of the address and the following characters are not allowed at the start or end of the address: hyphen (-), apostrophe ('), slash (/), ampersand (&), brackets (), and pound symbol (#).",
      category: 'T1135',
      severity: 'required'
    },
    '1135.partAorB': {
      label: "Only one checkbox is allowed to be selected for this section.",
      category: 'T1135',
      severity: 'required'
    },
    '1135.propertyType': {
      label: "Part A – at least one type of property must be selected or fill out a row in the tables in Part B.",
      category: 'T1135',
      severity: 'required'
    },
    '1135.countryCode': {
      label: "Part A: please select at least one country code.",
      category: 'T1135',
      severity: 'required'
    },
    '1135.foreignIncome': {
      label: "Part A: please enter an amount in the Income from all specified foreign property field. If nil enter 0.",
      category: 'T1135',
      severity: 'required'
    },
    '1135.gainLoss': {
      label: "Part A: please enter an amount in the Gain/Loss from the disposition from all specified foreign property field. If nil enter 0.",
      category: 'T1135',
      severity: 'required'
    },
    '1135.partBTableCheck': {
      label: "One of the rows you have entered is incomplete. There must be a value in each column. If nil, please enter 0.",
      category: 'T1135',
      severity: 'required'
    },
    '1135.rowFilledCheck': {
      label: "Specified foreign property has been divided into 7 categories. At least one row must be fully completed in one of the 7 tables provided.",
      category: 'T1135',
      severity: 'required'
    },
    '1135.nameCheck': {
      label: "Certification section - Name is required.",
      category: 'T1135',
      severity: 'required'
    },
    '1135.clearFields': {
      label: "Would you like to clear all fields?",
      category: 'NOTICE',
      severity: 'none'
    },
    '1135.maxRows': {
      label: "A maximum of 100 occurrences is allowed for each table in Part B. If any category exceeds 100 occurrences, form must be printed and mailed.",
      category: 'T1135',
      severity: 'required'
    },
    '1135.startDate': {
      label: "The taxation start date and end date are mandatory.",
      category: 'T1135',
      severity: 'required'
    },
    '1135.addressFilled': {
      label: "Street Address is required.",
      category: 'T1135',
      severity: 'required'
    },
    '1135.eligibility': {
      label: 'The answer does not match as T1135 has been filled.',
      category: 'T1135',
      severity: 'warning'
    },
    '106.requiredToFile': {
      label: "An entry at this field is required to file T106.",
      category: 'T106',
      severity: 'required'
    },
    '106.1001Check': {
      label: "Summary Information Section 1, Corporation Name required.",
      category: 'T106',
      severity: 'required'
    },
    '106.1002Check': {
      label: "Summary Information Section 1, Business Number required.",
      category: 'T106',
      severity: 'required'
    },
    '106.102Check': {
      label: "Summary Information Section 1, Summary Action Code required.",
      category: 'T106',
      severity: 'required'
    },
    '106.104Check': {
      label: "Street Address is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.106Check': {
      label: "Summary - Province or territory is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.107Check': {
      label: "Summary - City is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.108Check': {
      label: "Summary - Country is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.109Check': {
      label: "Summary - Postal or Zip code is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.110Check': {
      label: "Summary – Section 1: Taxation Year “From” Date is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.111Check': {
      label: "Summary – Section 1: Taxation Year “To” Date is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.112Check': {
      label: "Summary – Section 2: Yes or No is required for “Is this the first time you have filed a T106 form?",
      category: 'T106',
      severity: 'required'
    },
    '106.113Check': {
      label: "Summary - Section 2: Number of slips is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.114Check': {
      label: "Summary – Section 2: Total amount from all box “I” amounts is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.115Check': {
      label: "Summary – Section 2: Gross revenue of the reporting person/partnership is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.116Check': {
      label: "Summary – Section 2, Question 7: Yes or No is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.117Check': {
      label: "Summary – Section 2, Question 8: Yes or No is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.118Check': {
      label: "Summary – Section 2, Question 9: Yes or No is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.119Check': {
      label: "Summary – Section 2, Question 10: Yes or No is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.120Check': {
      label: "Summary – Section 3, Question 1: Yes or No is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.121Check': {
      label: "Summary – Section 3, Question 2: Yes or No is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.1131Check': {
      label: "Summary – Section 2: At least 1 NAICS code is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.1171Check': {
      label: "Certification Section – Name is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.1181Check': {
      label: "Certification Section – Date is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.1183Check': {
      label: "Certification Section – Title of Certifier is required.",
      category: 'T106',
      severity: 'required'
    },
    '106.lastReturnFilingDate': {
      label: "Summary – Section 2: Yes or No is required for “Is this the first time you have filed a T106 form?",
      category: 'T106',
      severity: 'required'
    },
    '106.primaryAccountNumber': {
      label: "Summary – Section 2: If yes is entered for Question 10, at least one primary account number is required (up to 2).",
      category: 'T106',
      severity: 'required'
    },
    '106.eligibility': {
      label: 'The answer does not match as T106 has been filled.',
      category: 'T106',
      severity: 'warning'
    },
    '106s.requiredToFile': {
      label: "An entry is required at this line if T106 Summary has been completed or partially completed.",
      category: 'T106s',
      severity: 'required'
    },
    '106s.deleteReasonCheck': {
      label: "This T106 Slip is set to be deleted but there is no reason provided below.",
      category: 'T106s',
      severity: 'required'
    },
    '106s.100Check': {
      label: "Slip Information, Summary Action Code required.",
      category: 'T106s',
      severity: 'required'
    },
    '106s.1000Check': {
      label: "Part II: Name of non-resident required.",
      category: 'T106s',
      severity: 'required'
    },
    '106s.1009Check': {
      label: "Part II: Address of non-resident required.",
      category: 'T106s',
      severity: 'required'
    },
    '106s.1010Check': {
      label: "Part II - Country Code is required.",
      category: 'T106s',
      severity: 'required'
    },
    '106s.1150-1Check': {
      label: "Part II: Type of Relationship is required.",
      category: 'T106s',
      severity: 'required'
    },
    '106s.1131Check': {
      label: "Part II: At least 1 valid NAICS code is required.",
      category: 'T106s',
      severity: 'required'
    },
    '106s.1135Check': {
      label: "Part II, Question 5: At least 1 Country Code is required.",
      category: 'T106s',
      severity: 'required'
    },
    '106s.1062-6Check': {
      label: "Part II, Question 6: Please select Yes or No.",
      category: 'T106s',
      severity: 'required'
    },
    '106s.1062-7Check': {
      label: "Part II, Question 7: Please select Yes or No.",
      category: 'T106s',
      severity: 'required'
    },
    '106s.taxTreatyCheck': {
      label: "Part II: Is the non-resident in a country with which Canada does not have a tax treaty, Yes or No is required.",
      category: 'T106s',
      severity: 'required'
    },
    '106s.tpmCheck': {
      label: "Part III: Transfer pricing methodology (TPM) is required.",
      category: 'T106s',
      severity: 'required'
    },
    '106s.descriptionCheck': {
      label: "If a numerical entry is made there must be an entry in the corresponding description field.",
      category: 'T106s',
      severity: 'required'
    },
    '1134.requiredToFile': {
      label: "An entry at this field is required to file T1134.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.corpName': {
      label: "Summary - Section 1, Corporation Name required.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.businessNum': {
      label: "Summary - Section 1, Business Number is required.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.corpOnly': {
      label: "Only submissions for corporations are accepted.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.addressCheck': {
      label: "A period (.) is not allowed at the start of the address and the following characters are not allowed at the start or end of the address: hyphen (-), apostrophe ('), slash (/), ampersand (&), brackets (), and pound symbol (#).",
      category: 'T1134',
      severity: 'required'
    },
    '1134.addressFilled': {
      label: "Summary – Section 1: Street Address is required.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.cityCheck': {
      label: "Summary – Section 1: City is required.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.provCheck': {
      label: "Summary - Section 1: Province or territory is required.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.postalCheck': {
      label: "Summary – Section 1: Postal or Zip code is required.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.countryCheck': {
      label: "Summary – Section 1: Country is required.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.taxYearStartCheck': {
      label: "Summary – Section 1: Taxation Year “From” Date is required.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.taxYearEndCheck': {
      label: "Summary – Section 1: Taxation Year “To” Date is required.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.shortYearCheck': {
      label: "Summary – Section 1: Yes or No is required for 2 or more short taxation years.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.suppFilledCheck': {
      label: "Summary – Section 1: Number of supplements is required.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.suppTotalCountCheck': {
      label: "Maximum number of Supplements allowed per transmission cannot exceed 1500. If you have more than 1500 supplements, you must paper file the return to the Canada Revenue Agency.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.suppCountCheck': {
      label: "Summary – Section 1: Number of supps does not match the number of supplements being transmitted, please review and update as required.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.certifierNameCheck': {
      label: "Summary – Section 2: Certifier Name is required.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.certifierPositionCheck': {
      label: "Certification Section – Title of Certifier is required.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.orgAccountNum': {
      label: "Summary – Section 3: Account Number is required.",
      category: 'T1134',
      severity: 'required'
    },
    '1134.percentFormatCheck': {
      label: "This field allows up to 3 digits before the decimal and up to 2 digits after the decimal. Cannot be less than zero (negative).",
      category: 'T1134',
      severity: 'required'
    },
    '1134.eligibility': {
      label: 'The answer does not match as T1134 has been filled.',
      category: 'T1134',
      severity: 'warning'
    },
    '1134s.financialIndicatorCheck': {
      label: "An entry is required at this line.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.requiredToFile': {
      label: "An entry is required at this line if T1134 Summary has been completed or partially completed.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.nameCheck': {
      label: "Supplement – Section 2, Part A: Name of Foreign Affiliate is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.headOfficeAddressCheck': {
      label: "Supplement – Section 2, Part A: Address of Head Office is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.section1TaxYearStartCheck': {
      label: "Supplement – Section 2, Part A: Year Corporation became a foreign affiliate is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.ceaseAffiliateCheck': {
      label: "Supplement - Section 2, Part A: Did the corporation cease to be a foreign affiliate of the reporting entity in the year is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.part2NAICSCheck': {
      label: "Supplement - Section 2, Part A: At least 1 valid NAICS code is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.countryCodeCheck': {
      label: "Supplement - Section 2, Part A: At least one country code is required for where the foreign affiliate either carries on a business or has other income earning activity.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.foreignCountryCodeCheck': {
      label: "Supplement -Section 2, Part A: The country code of residence for the foreign affiliate is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.firstTimeFilingCheck': {
      label: "Supplement - Section 2, Part A: First time the reporting entity has filed form T1134 for this foreign affiliate is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.controlledAffiliate': {
      label: "Supplement - Section 2, Part A: Is it a controlled foreign affiliate as defined in subsection 95(1) is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.currencyCodeCheck': {
      label: "Supplement – Section 2, Part B: Currency Code required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.percentFilledCheck1': {
      label: "Supplement – Section 2, Part C, (i): Percentage is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.percentFilledCheck2': {
      label: "Supplement – Section 2, Part C, (ii): Percentage is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.qualifyingInterestTaxYearStartCheck': {
      label: "Supplement - Section 2, Part C, (iii) (a): Yes or No is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.qualifyingInterestTaxYearEndCheck': {
      label: "Supplement - Section 2, Part C, (iii) (b): Yes or No is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.section3YearStartCheck': {
      label: "Supplement – Section 3: Taxation Year From Date is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.section3YearEndCheck': {
      label: "Supplement – Section 3: Taxation Year To Date is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.totalAssetCurrencyCodeCheck': {
      label: "Supplement - Section 3: Currency code required for Total Assets amount.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.netIncomeCurrencyCodeCheck': {
      label: "Supplement - Section 3: Currency code required for Accounting Net Income amount.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.incomeProfitCurrencyCodeCheck': {
      label: "Supplement - Section 3: Currency code required for Income or Profits tax paid or payable on income amount.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.incomeProfitCountryCodeCheck': {
      label: "Supplement -Section 3: The country code to which income or profits tax was paid or payable to is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.capitalStockForeignCheck': {
      label: "Supplement – Section 4, Q1, Did the reporting entity receive a dividend on a share of the capital stock of the foreign affiliate any time within the taxation year, please select Yes or No.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.surplusCheck': {
      label: "Supplement – Section 4, Q1: An entry is required on at least one surplus amount field.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.subsection93IndicatorCheck': {
      label: "Supplement – Section 4, Q2: Was a subsection 93(1) election made or will such an election be made for the disposition of shares of the foreign affiliate in the year, please select Yes or No.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.subsection93AmountCheck': {
      label: "Supplement – Section 4, Q2: If a subsection 93(1) election was made, please enter the amount elected on.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.subsection93CurrencyCodeCheck': {
      label: "Supplement – Section 4, Q2: Currency code required for actual or estimated amount elected on.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.involvedIndicatorCheck': {
      label: "Supplement - Section 4, Part B, Q1: Yes or No is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.involvedDescriptionCheck': {
      label: "Supplement - Section 4, Part B, Q1: Description is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.acquireDisposeIndicatorCheck': {
      label: "Supplement - Section 4, Part B, Q2: Yes or No is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.acquireDisposeDescriptionCheck': {
      label: "Supplement - Section 4, Part B, Q2: Description is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.part3NAICSCheck': {
      label: "Supplement – Part III, Section 1: At least 1 valid NAICS code is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.employeeCountIndicatorCheck': {
      label: "Part III – Section 1: Number of Employees is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.interestCurrencyCodeCheck': {
      label: "Supplement – Part 3, Section 2: Interest Currency Code is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.interestOtherCurrencyCodeCheck': {
      label: "Supplement – Part 3, Section 2: Interest - Other Currency Code is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.dividendCurrencyCodeCheck': {
      label: "Supplement – Part 3, Section 2: Dividends Currency Code is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.dividendOtherCurrencyCodeCheck': {
      label: "Supplement – Part 3, Section 2: Dividends - Other Currency Code is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.royaltyCurrencyCodeCheck': {
      label: "Supplement – Part 3, Section 2: Royalties Currency Code is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.rentalCurrencyCodeCheck': {
      label: "Supplement – Part 3, Section 2: Rental & Leasing Currency Code is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.loanLendingCurrencyCodeCheck': {
      label: "Supplement – Part 3, Section 2: Loans or Lending Currency Code is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.insuranceRiskCurrencyCodeCheck': {
      label: "Supplement – Part 3, Section 2: Insurance or reinsurance Currency Code is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.accountsReceivableCurrencyCode': {
      label: "Supplement – Part 3, Section 2: Factoring of trade accounts receivable Currency Code is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.investmentPropertyCurrencyCode': {
      label: "Supplement – Part 3, Section 2: Disposition of Investment Property Currency Code is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.accrualPropertyIncomeCheck': {
      label: "Supplement - Section 3 (i): Yes or No is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.foreignAccrualPercentFilledCheck': {
      label: "Supplement – Part III, Section 3 (ii): Percentage is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.excludedPropertyCheck': {
      label: "Supplement – Part III, Section 4 (i): Yes or No is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.notExcludedPropertyCheck': {
      label: "Supplement – Part III, Section 4 (ii): Yes or No is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.act95Check': {
      label: "Supplement – Part III, Section 5: Yes or No is required.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.summaryDescriptionCheck': {
      label: "Part IV – Disclosure: Please specify information not available.",
      category: 'T1134s',
      severity: 'required'
    },
    '1134s.percentFormatCheck': {
      label: "This field allows up to 3 digits before the decimal and up to 2 digits after the decimal. Cannot be less than zero (negative).",
      category: 'T1134',
      severity: 'required'
    },
    '008.terminalLoss': {
      label: 'Terminal Loss has been flagged for a CCA Class.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '008.terminalLossDuplicate': {
      label: 'Terminal Loss has been flagged where you have multiple CCA Classes for the same Class.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '008.recapturedDepreciation': {
      label: 'Recaptured Depreciation has been flagged for a CCA Class.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '008.recapturedDepreciationDuplicate': {
      label: 'Recaptured Depreciation has been flagged where you have multiple CCA Classes for the same Class.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '390.104': {
      label: 'Cannot exceed $750.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '367.100-1': {
      label: 'Cannot exceed $75000.',
      category: 'TAX RULE',
      severity: 'critical'
    },

    '380.118': {
      label: 'Amount A cannot exceed the total of line 106 plus 107',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '367.100-2': {
      label: 'The relevant NB-SBITC-1 certificates must be filed; a copy of the certificates is accepted.',
      category: 'NOTICE',
      severity: 'none'
    },
    '389.bookCredit': {
      label: 'The corporation is not eligible to claim the MB book publishing tax credit.',
      category: 'NOTICE',
      severity: 'none'
    },
    '384.155Limit': {
      label: 'Maximum amount is $20,000 per qualified youth.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '384.260Limit': {
      label: 'Maximum amount is $10,000 per co-op student.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '384.360Limit': {
      label: 'Maximum amount is $50,000 per graduate.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '384.460Limit': {
      label: 'Maximum amount is $33,333 per apprentice.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '384.560Limit': {
      label: 'Maximum amount is $25,000 per apprentice.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '384.760Limit': {
      label: 'Maximum amount is $20,000 per apprentice.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '384.660Limit': {
      label: 'Maximum amount is $33,333 per journeyperson.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '392.410': {
      label: 'Must be at least $10 million.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    'RC59.oneCheckboxOnlyPart3': {
      label: 'Only one type of authorization level should be chosen. Please verify.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    'RC59.oneCheckboxOnlyPart4': {
      label: 'Details of program accounts and fiscal years must be filled when a specific program account is selected.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    'RC59.321': {
      label: 'Details of program accounts and fiscal years must be filled when a specific program account is selected.',
      category: 'TAX RULE',
      severity: 'warning',
      ignorable: true
    },
    'T2J.997': {
      label: 'If the entry at line T2J.997 is Yes (1), there must be an entry on form "Line 996 Amended Tax Return Description of Changes" for line 996.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '302.300': {
      label: 'Maximum: $3,000,000.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '306.410': {
      label: 'Cannot exceed $5,000,000.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '303.160': {
      label: 'Cannot exceed $50,000.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '304.123': {
      label: 'Cannot exceed $50,000.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '308.145': {
      label: 'Cannot exceed $75,000.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '308.200': {
      label: 'No deduction can be made for a taxation year preceding the 2014 tax year.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '308.1000': {
      label: 'An opening balance relating to the Newfoundland and Labrador Venture Capital Tax Credit has been carried forward to Schedule 308 for a taxation year ending before January 1, 2014. Only credits earned after December 31, 2013 can be carried forward.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    'percentTotal': {
      label: 'Total must equal 100%.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '661.percentCheck': {
      label: 'Total must be between 0.001% and 75%.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '007.310': {
      label: 'The income (loss) at line 310 cannot be greater or equal than the income (loss) at line 300.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '008W.14-1': {
      label: 'Class 14.1 should only be used when corporation\'s fiscal year end is after January 1, 2017. Complete Schedule 10 instead.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '423.490': {
      label: 'Requires an entry in at least one of lines 405, 420, 425, 430, 432 or 435.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '009.datesFilledCheck': {
      label: 'Enter the taxation year for the related or associated corporation.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '009.priorYearTaxableCapital': {
      label: 'An entry for the amount of taxable capital employed in Canada from the prior tax year is required. If "0" then clear the field and re-enter "0".',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '009.taxYearEndCheck': {
      label: 'The related or associated corporation\'s tax year end date cannot end in a different calendar year than the filing corporation\'s tax year end date.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '010.100': {
      label: 'Complete this table if CEC deductions were claimed in previous years.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '010.14-1': {
      label: 'As of January 1, 2017, The ECP rules have been repealed and replaced by new CCA Class 14.1. Update asset class 14.1 on Schedule 8 instead.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '015.202': {
      label: 'Verify if amount included in line A isn\'t already deducted on the Income Statement.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '566.415': {
      label: 'Cannot be more than $3,000,000.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '010W.1000': {
      label: 'Incorporation expenses incurred after 2016 can be deducted up to $3,000 per corporation. Verify if it is applicable and include the excess amount in class 14.1.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '423.491': {
      label: 'Requires an entry in at least one of lines 406, 421, 426, 431, 433 or 436.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '423.800': {
      label: 'Requires an entry at line 610.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '566.440': {
      label: 'Cannot be more than $3,000,000.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '061.010': {
      label: 'This line must be equal to line 015 plus line 020.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '062.010': {
      label: 'This line must be equal to line 106.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '062.106': {
      label: 'The amount transferred cannot be greater than the amount at line 104.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '048.304': {
      label: 'There must be an entry at this line if there is no entry at lines 305 and 306.'
    },
    '048.305': {
      label: 'There must be an entry at this line if there is no entry at line 304.'
    },
    '048.306': {
      label: 'There must be an entry at this line if there is an entry at line 305.'
    },
    '091.113': {
      label: 'If the entry at line 112 is (11), an entry is required to this line.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '554.440': {
      label: 'If there is an entry at this line, complete lines 450 and 460.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '554.640': {
      label: 'If there is an entry at this line, complete lines 650 and 660.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '558.645': {
      label: 'If there is an entry at this line, complete lines 646 and 647.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '558.745': {
      label: 'If there is an entry at this line, complete lines 746 and 747.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '560.510': {
      label: 'Cannot exceed $100,000.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '422.535': {
      label: 'Entry required if there is an entry at 530.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '422.590': {
      label: 'Entry required if there is any entry at 480, 505, 515, 516, 520, or 525.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '422.610': {
      label: 'Entry required if the entry at either line 590 or 620 is greater than "0."',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '524.100': {
      label: 'Identify the specialty types of the corporation.',
      category: 'TAX RULE',
      severity: 'warning'
    },
    '556.450': {
      label: 'If there is an entry at this line, complete lines 470 and 480.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '556.800': {
      label: 'Cannot be more than $50,000.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    'T2J.010': {
      label: 'The corporation cannot use Corporation Internet Filing service to change the corporation\'s ' +
      'head office or mailing address. It must be done on a letter, online through CRA or on the phone by contacting the' +
      ' Business Enquiries line.',
      category: 'TAX RULE',
      severity: 'critical'
    },
    '562.400': {
      label: 'The sound recording tax credit has been claimed. Note that the expenditures incurred after April 23, 2015, only qualify for the tax credit if the following conditions are met: the Canadian sound recording commenced before April 23, 2015, the expenditures have been incurred before May 1, 2016, and the corporation did not receive any amount from the Ontario Music Fund (OMF) in respect of these expenditures. Make sure that you enter only the expenditures that are eligible.',
      category: 'TAX RULE',
      severity: 'warning',
      ignorable: true
    }
  };
})();

