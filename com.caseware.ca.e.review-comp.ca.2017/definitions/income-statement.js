(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var MINUS = wpw.analysis.AnalysisFormula.FORMULAS.minus;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  var incomeStatementDef = new wpw.analysis.AnalysisDefinition();
  incomeStatementDef.id = '8lG0qQx5TrqFOQAA1Ujg0Q';
  incomeStatementDef.name = 'Income statement';

/********************* Formulas ***************************/
 var GROSS_PROFIT_MARGIN_FORMULA = DIVIDE(MINUS(TAG('300'),TAG('400')),TAG('300'));
 var NET_PROFIT_MARGIN_FORMULA = DIVIDE(TAG('285.200'),TAG('300'));
 var RETURN_ON_ASSETS_FORMULA = DIVIDE(TAG('285.200'),TAG('100'));
 var PRETAX_RETURN_EQ_FORMULA = DIVIDE(ADD(TAG('285.200'),TAG('860')),TAG('270'));
 var AFTERTAX_RETURN_EQ_FORMULA = DIVIDE(TAG('285.200'),TAG('270'));

  /********************* Formulas ends **********************/



  // Accounts
  var componentDef = new wpw.analysis.ComponentDefinition('HkgMlINffe', 'Accounts', wpw.analysis.ComponentType.ACCOUNTS_TABLE);
  componentDef.allowEdit = false;
  componentDef.breakdownGroupNumbers = ['300', '400','500','860','285.200'];
  incomeStatementDef.componentDefinitions.push(componentDef);

//Ratios
  var GROSS_PROFIT_MARGIN = new KPIModel('GROSS_PROFIT_MARGIN', 'Gross profit margin', DATA_TYPE.PERCENTAGE, false, null,
      GROSS_PROFIT_MARGIN_FORMULA, 'Gross profit / Total sales');
  var NET_PROFIT_MARGIN = new KPIModel('NET_PROFIT_MARGIN', 'Net profit margin', DATA_TYPE.PERCENTAGE, false, null,
      NET_PROFIT_MARGIN_FORMULA, 'Net income / Total sales');
  var PRETAX_RETURN_EQ = new KPIModel('PRETAX_RETURN_EQ', 'Pre-tax return on equity', DATA_TYPE.PERCENTAGE, false, null,
      PRETAX_RETURN_EQ_FORMULA, '(Net Income + Tax) / Equity');
  var AFTERTAX_RETURN_EQ = new KPIModel('AFTERTAX_RETURN_EQ', 'After-tax return on equity', DATA_TYPE.PERCENTAGE, false, null,
      AFTERTAX_RETURN_EQ_FORMULA, 'Net Income / Equity');
  var componentModels = [GROSS_PROFIT_MARGIN,NET_PROFIT_MARGIN,PRETAX_RETURN_EQ,AFTERTAX_RETURN_EQ];
  var component = new ComponentDefinition('S9sqN5FrhT', 'Ratios', COMPONENT_TYPES.TABLE, null,
  wpw.analysis.modelsToKeys(componentModels));
  incomeStatementDef.addUniqueKpiModels(componentModels);
  incomeStatementDef.componentDefinitions.push(component);




  wpw.injectedAnalysisDefinitions.push(incomeStatementDef);
})(wpw);

