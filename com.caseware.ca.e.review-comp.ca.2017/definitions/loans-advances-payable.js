(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  var loansPayableDef = new wpw.analysis.AnalysisDefinition();
  loansPayableDef.id = 'zq6d_lEdQ2qbOZxwkRodNw';
  loansPayableDef.name = 'Loans and advances payable';


  var DEFERRED_REV = new KPIModel('DEFERRED_REV', 'Deferred revenue', DATA_TYPE.MONETARY, true, null,
  TAG('218'));
  var component = new ComponentDefinition('LDfdl89e', 'Overview', COMPONENT_TYPES.CHART,
      CHART_TYPES.COLUMN, [DEFERRED_REV.key]);
  loansPayableDef.addUniqueKpiModels([DEFERRED_REV]);
  loansPayableDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('H12Aozxfl', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.allowEdit = false;
  component.breakdownGroupNumbers = ['221','235','222','236','223','237'];
  loansPayableDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(loansPayableDef);
})(wpw);
