(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var MINUS = wpw.analysis.AnalysisFormula.FORMULAS.minus;
  var MULTIPLY = wpw.analysis.AnalysisFormula.FORMULAS.multiply;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  var CONSTANT = wpw.analysis.AnalysisFormula.FORMULAS.constant;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
  var CASH_FORMULA = TAG('111');
  var RESTRICTED_CASH_FORMULA = TAG('184');
  var CURRENT_RATIO_FORMULA = DIVIDE(TAG('105'),TAG('210'));
  var QUICK_RATIO_FORMULA = DIVIDE(ADD(TAG('111'),TAG('113'),TAG('115')),TAG('210'));
  var CASH_RATIO_FORMULA = DIVIDE(TAG('111'),TAG('210'));

  /********************* Formulas ends **********************/

  var analysisDef = new wpw.analysis.AnalysisDefinition();
  analysisDef.id = 'zLU3BwioQlaZct847TrSPw';
  analysisDef.name = 'Cash';




 // Overall
 var CASH = new KPIModel('CASH', 'Cash', DATA_TYPE.MONETARY, true, null, CASH_FORMULA);
 var componentModels = [CASH];
 var component = new ComponentDefinition('ryt0V9YB2T', 'Overview', COMPONENT_TYPES.CHART,
     CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
  analysisDef.kpiModels = analysisDef.kpiModels.concat(componentModels);
  analysisDef.componentDefinitions.push(component);


 // Accounts
component = new ComponentDefinition('BJ2uo0gfe', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
component.breakdownGroupNumbers = ['111','184'];
component.allowEdit = false;
analysisDef.componentDefinitions.push(component);



  //Ratios
  var CURRENT_RATIO = new KPIModel('CURRENT_RATIO', 'Current ratio', DATA_TYPE.NUMBER, false, null,
  CURRENT_RATIO_FORMULA, 'Current assets / Current liabilities');
  CURRENT_RATIO.numberOfDecimals = 2;
  var QUICK_RATIO =  new KPIModel('QUICK_RATIO', 'Quick ratio', DATA_TYPE.NUMBER, false, null,
  QUICK_RATIO_FORMULA, 'Quick assets / Current liabilities');
  QUICK_RATIO.numberOfDecimals = 2;
  var CASH_RATIO=  new KPIModel('CASH_RATIO', 'Cash ratio', DATA_TYPE.NUMBER, false, null,
  CASH_RATIO_FORMULA, 'Cash and cash equivalents / Current liabilities');
  CASH_RATIO.numberOfDecimals = 2;
  var component = new ComponentDefinition('B1ln_iCgzg', 'Ratios', COMPONENT_TYPES.TABLE, null, [CURRENT_RATIO.key, QUICK_RATIO.key, CASH_RATIO.key]);
analysisDef.addUniqueKpiModels([CASH_RATIO, CURRENT_RATIO, QUICK_RATIO]);
  analysisDef.componentDefinitions.push(component);

 
  
  wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);