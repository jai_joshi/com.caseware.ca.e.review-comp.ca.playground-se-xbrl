(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
  var PREPAID_EXP_CUR_FORMULA = TAG('128.400');
  var OTHER_ASSETS_CUR_FORMULA = TAG('128.800');
  
  /********************* Formulas ends **********************/
  var prepaidOtherAssetsDef = new wpw.analysis.AnalysisDefinition();
  prepaidOtherAssetsDef.id = 'NgOcqeuZTPunH1Q2jKgYzg';
  prepaidOtherAssetsDef.name = 'Prepaid expenses and other assets';

  var PREPAID_EXP_CUR = new KPIModel('PREPAID_EXP_CUR', 'Prepaid expenses, current', DATA_TYPE.MONETARY, true, null,
      PREPAID_EXP_CUR_FORMULA);

  var OTHER_ASSETS_CUR = new KPIModel('OTHER_ASSETS_CUR', 'Other assets current', DATA_TYPE.MONETARY, true, null,
      OTHER_ASSETS_CUR_FORMULA);
  var componentModels = [PREPAID_EXP_CUR, OTHER_ASSETS_CUR];
  var component = new ComponentDefinition('H1C6kiH2a', 'Overview', COMPONENT_TYPES.STACK,
      CHART_TYPES.COLUMN, wpw.analysis.modelsToKeys(componentModels));
  component.groupKeysInStack('PREPAID_EXP', [PREPAID_EXP_CUR, OTHER_ASSETS_CUR], false, 1);
  prepaidOtherAssetsDef.addUniqueKpiModels(componentModels);
  prepaidOtherAssetsDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('HJGUkxxWzl', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.breakdownGroupNumbers = ['128', '181', '183', '185'];
  component.allowEdit = false;
  prepaidOtherAssetsDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(prepaidOtherAssetsDef);
})(wpw);
