(function() {
  'use strict';
  if (!wpw.tax.codes)
    wpw.tax.codes = {};

  wpw.tax.codes.assetCode = {
    '1': {value: '01 Cogeneration systems'},
    '2': {value: '02 Waste-fuelled electrical generation equipment'},
    '3': {value: '03 Thermal waste electrical generation equipment'},
    '4': {value: '04 Wind energy conversion systems'},
    '5': {value: '05 Small-scale hydro-electric installations'},
    '6': {value: '06 Fuel cell equipment'},
    '7': {value: '07 Photovoltaic equipment'},
    '8': {value: '08 Wave/tidal energy equipment'},
    '9': {value: '09 Geothermal electrical generation equipment'},
    '10': {value: '10 Active solar heating equipment'},
    '11': {value: '11 Ground source heat pump systems'},
    '12': {value: '12 District energy equipment'},
    '13': {value: '13 Waste-fuelled thermal energy equipment'},
    '14': {value: '14 Heat recovery equipment'},
    '15': {value: '15 Landfill gas/digester gas collection equipment'},
    '16': {value: '16 Bio-oil production systems'},
    '17': {value: '17 Biogas production systems'},
    '18': {value: '18 Enhanced combined cycle systems'},
    '19': {value: '19 Expansion engine system'},
    '20': {value: '20 Gasification equipment'},
    '21': {value: '21 Electric vehicle charging stations'},
    '22': {value: '22 Electric energy storage'},
    '23': {value: '23 Geothermal energy equipment – acquired after March 21, 2017'},
    '24': {value: '24 Thermal energy source - acquired after March 21, 2017'}
  }
}());
