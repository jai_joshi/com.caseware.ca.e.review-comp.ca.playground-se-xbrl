(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  var analysisDef = new wpw.analysis.AnalysisDefinition();
  analysisDef.id = 'DXqBjo_iTnqRa87x5QJehw';
  analysisDef.name = 'Bank indebtedness';


  var CASH_AND_EQUIV_FORMULA = new KPIModel('CASH_AND_EQUIV_FORMULA', 'Cash and cash equivalents', DATA_TYPE.MONETARY, true, null,
  TAG('111'));
  var component = new ComponentDefinition('HJTyiS2T', 'Overview', COMPONENT_TYPES.CHART,
      CHART_TYPES.COLUMN, [CASH_AND_EQUIV_FORMULA.key]);
  analysisDef.addUniqueKpiModels([CASH_AND_EQUIV_FORMULA]);
  analysisDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('Lad0lkao8s', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.allowEdit = false;
  component.breakdownGroupNumbers = ['211'];
  analysisDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);
