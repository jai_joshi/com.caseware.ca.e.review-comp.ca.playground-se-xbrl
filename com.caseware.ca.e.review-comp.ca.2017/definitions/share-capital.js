(function(wpw) {
  'use strict';

  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var MINUS = wpw.analysis.AnalysisFormula.FORMULAS.minus;
  var MULTIPLY = wpw.analysis.AnalysisFormula.FORMULAS.multiply;
  var CONSTANT = wpw.analysis.AnalysisFormula.FORMULAS.constant;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /************ Formulas *********/
  var OTHER_COMPREHENSIVE_INCOME_FORMULA = TAG('292');
  var NET_INCOME_FORMULA = TAG('285.200');
  var DIVIDENDS_FORMULA = TAG('285.300');

  var SHAREHOLDERS_EQUITY_FORMULA = TAG('270');
  var RETURN_ON_INVESTMENT_FORMULA = DIVIDE(NET_INCOME_FORMULA, SHAREHOLDERS_EQUITY_FORMULA);
  var RETENTION_FORMULA = DIVIDE(DIVIDENDS_FORMULA, NET_INCOME_FORMULA);
  var RETENTION_RATIO_FORMULA = MINUS(CONSTANT(1), RETENTION_FORMULA);
  var POTENTIAL_GROWTH_RATIO_FORMULA = MULTIPLY(RETURN_ON_INVESTMENT_FORMULA, RETENTION_RATIO_FORMULA);
  var TOTAL_CAPITAL_FORMULA = ADD(TAG('297'), TAG('298'), TAG('299'));
  var TOTAL_ASSETS_FORMULA = TAG('100');
  var SHAREHOLDERS_EQUITY_RATIO_FORMULA = DIVIDE(TOTAL_CAPITAL_FORMULA, TOTAL_ASSETS_FORMULA);

  /************ Formulas end *****/

  var equityDef = new wpw.analysis.AnalysisDefinition();
  equityDef.id = 'ni8qLnG4TyeKYqMTsOeuBw';
  equityDef.name = 'Share capital';

  // Overview

  var OTHER_COMPREHENSIVE_INCOME = new KPIModel('OTHER_COMPREHENSIVE_INCOME', 'Other comprehensive income', DATA_TYPE.MONETARY, 
        true, null, OTHER_COMPREHENSIVE_INCOME_FORMULA);

  var NET_INCOME = new KPIModel('NET_INCOME', 'Net income (loss)', DATA_TYPE.MONETARY, true, null, NET_INCOME_FORMULA);
  var DIVIDENDS = new KPIModel('DIVIDENDS', 'Dividends (ordinary dividends + preference dividends)', DATA_TYPE.MONETARY, 
        true, null, DIVIDENDS_FORMULA);
  var component = new ComponentDefinition('rJzfiu2AC', 'Overview', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN,
      [OTHER_COMPREHENSIVE_INCOME.key, NET_INCOME.key, DIVIDENDS.key]);

  equityDef.kpiModels.push(OTHER_COMPREHENSIVE_INCOME, NET_INCOME, DIVIDENDS);
  equityDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('rkMnAszMMe', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.allowEdit = false;
  component.breakdownGroupNumbers = ['281', '282', '283', '284', '285', '295'];
  equityDef.componentDefinitions.push(component);

    // Ratios
    var RETURN_ON_INVESTMENT_RATIO = new KPIModel('RETURN_ON_INVESTMENT_RATIO', 'Return on Investment', DATA_TYPE.PERCENTAGE, false, null,
    RETURN_ON_INVESTMENT_FORMULA, 'Net income / Shareholders\' equity');
    var RETENTION_RATIO = new KPIModel('RETENTION_RATIO', 'Retention Ratio', DATA_TYPE.PERCENTAGE, false, null,
    RETENTION_RATIO_FORMULA, '1 - (Dividends / Net income)');
    var POTENTIAL_GROWTH_RATIO = new KPIModel('POTENTIAL_GROWTH_RATIO', 'Potential Growth Ratio', DATA_TYPE.PERCENTAGE, false, null,
    POTENTIAL_GROWTH_RATIO_FORMULA, 'Return on Investment X Retention Ratio');
    var SHAREHOLDERS_EQUITY_RATIO = new KPIModel('SHAREHOLDERS_EQUITY_RATIO', 'Shareholders Equity Ratio', DATA_TYPE.PERCENTAGE, false, null,
    SHAREHOLDERS_EQUITY_RATIO_FORMULA, 'Total Capital / Total Assets');

    var componentModels = [RETURN_ON_INVESTMENT_RATIO, RETENTION_RATIO, POTENTIAL_GROWTH_RATIO, SHAREHOLDERS_EQUITY_RATIO];
    component = new ComponentDefinition('Sy_VcKr2p', 'Ratios', COMPONENT_TYPES.TABLE, null, [RETURN_ON_INVESTMENT_RATIO.key, RETENTION_RATIO.key,
    POTENTIAL_GROWTH_RATIO.key, SHAREHOLDERS_EQUITY_RATIO.key]);

    equityDef.addUniqueKpiModels(componentModels);
    equityDef.componentDefinitions.push(component);

    wpw.injectedAnalysisDefinitions.push(equityDef);

})(wpw);
