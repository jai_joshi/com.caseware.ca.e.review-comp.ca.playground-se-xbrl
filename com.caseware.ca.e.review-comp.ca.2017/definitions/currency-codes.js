(function () {
  'use strict';
  if (!wpw.tax.codes)
    wpw.tax.codes = {};

  wpw.tax.codes.currencyCodes =
  {
    'USD': {value: 'USD U.S. dollar'},
    'GBP': {value: 'GBP British pound'},
    'AUD': {value: 'AUD Australian dollar'},
    'EUR': {value: 'EUR Euro'},
  }
}());
