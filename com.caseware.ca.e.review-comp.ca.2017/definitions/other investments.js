(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var MINUS = wpw.analysis.AnalysisFormula.FORMULAS.minus;
  var MULTIPLY = wpw.analysis.AnalysisFormula.FORMULAS.multiply;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  var CONSTANT = wpw.analysis.AnalysisFormula.FORMULAS.constant;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
  var INVESTMENT_REV_FORMULA = TAG('321');
  var INCOME_SUBS_ASSOC_JV_FORMULA = TAG('330');
  var GAIN_ON_INV_FORMULA = TAG('605.100');
  var INVESTMENTS_TOTAL_ASSETS_FORMULA = DIVIDE(TAG('131'), TAG('100'));
  var INVESTMENTS_SUBS_ASSO_JV_FORMULA = DIVIDE(TAG('134'),TAG('100'));
  var INCOME_DIV_BY_INVESTMENT_FORMULA = DIVIDE(TAG('330'),TAG('134'));


  /********************* Formulas ends **********************/

  var otherInvestmentDef = new wpw.analysis.AnalysisDefinition();
  otherInvestmentDef.id = 'K-fCQDHgTY-UEHc84Z1GFw';
  otherInvestmentDef.name = 'Other investments';

  // Overview
  var INVESTMENT_REV = new KPIModel('INVESTMENT_REV', 'Investment revenue', DATA_TYPE.MONETARY, true, null, INVESTMENT_REV_FORMULA);
  var INCOME_SUBS_ASSOC_JV = new KPIModel('INCOME_SUBS_ASSOC_JV', 'Income from subsidiaries, associates, and joint ventures', DATA_TYPE.MONETARY, true, null, INCOME_SUBS_ASSOC_JV_FORMULA);
  var GAIN_ON_INV = new KPIModel('GAIN_ON_INV', 'Gain (loss) on investments in subsidiaries, asscoiates, and joint ventures', DATA_TYPE.NUMBER, false, null,
     GAIN_ON_INV_FORMULA);
  var componentModels = [INVESTMENT_REV, INCOME_SUBS_ASSOC_JV, GAIN_ON_INV];
  var component = new ComponentDefinition('asdfi8lpSS', 'Overview', COMPONENT_TYPES.CHART,
      CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
  otherInvestmentDef.kpiModels = otherInvestmentDef.kpiModels.concat(componentModels);
  otherInvestmentDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('HybLyge-ze', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.breakdownGroupNumbers = ['126','134','138','140'];
  component.allowEdit = false;
  otherInvestmentDef.componentDefinitions.push(component);

  // Ratios
  var INVESTMENTS_TOTAL_ASSETS = new KPIModel('INVESTMENTS_TOTAL_ASSETS', 'Investments to total assets', DATA_TYPE.PERCENTAGE, false, null,
    INVESTMENTS_TOTAL_ASSETS_FORMULA, 'Long-term investments / Total assets');
    INVESTMENTS_TOTAL_ASSETS.numberOfDecimals = 2;
  var INVESTMENTS_SUBS_ASSO_JV = new KPIModel('INVESTMENTS_SUBS_ASSO_JV', 'Investments in joint ventures and partnerships to total assets', DATA_TYPE.PERCENTAGE,
    false, null, INVESTMENTS_SUBS_ASSO_JV_FORMULA,'Investments in subsidiaries, associates, and joint ventures / Total assets');
    INVESTMENTS_SUBS_ASSO_JV.numberOfDecimals = 2;
  var INCOME_DIV_BY_INVESTMENT = new KPIModel('INCOME_DIV_BY_INVESTMENT', 'Income from related entities to total assets', DATA_TYPE.PERCENTAGE, false,
      null, INCOME_DIV_BY_INVESTMENT_FORMULA,'Income from subsidiaries, associates, and joint ventures / Investments in subsidiaries, associates, and joint ventures');
      INCOME_DIV_BY_INVESTMENT.numberOfDecimals = 2;
  componentModels = [INVESTMENTS_TOTAL_ASSETS, INVESTMENTS_SUBS_ASSO_JV, INCOME_DIV_BY_INVESTMENT];
  component = new ComponentDefinition('SpkqN5G9rhT', 'Ratios', COMPONENT_TYPES.CHART, CHART_TYPES.LINE_COLUMN,
  wpw.analysis.modelsToKeys(componentModels),null, true);
  otherInvestmentDef.addUniqueKpiModels(componentModels);
  otherInvestmentDef.componentDefinitions.push(component);


  wpw.injectedAnalysisDefinitions.push(otherInvestmentDef);
})(wpw);
