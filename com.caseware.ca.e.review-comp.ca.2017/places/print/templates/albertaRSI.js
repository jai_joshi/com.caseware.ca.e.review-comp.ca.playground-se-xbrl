(function(wpw) {

  //According to the RSI requirements we need to replace single spaces with double spaces
  var /** @const @type {String} */ DOUBLE_SPACE = "&nbsp;&nbsp;";
  //As well as replace minus sign of negative numbers with "**"
  var /** @const @type {String} */ MINUS_SIGN_REPLACER = "**";

  /**
   * Doubles all spaces in given string
   * @param {String} str
   * @return {String}
   */
  function doubleAllSpaces(str){
    return str.replace(/(\S)\s{1}(?=\S)/g, "$1" + DOUBLE_SPACE);
  }

  /**
   * Replaces minus sign of negative numbers with MINUS_SIGN_REPLACER
   * @param str
   * @return {*}
   */
  function replaceMinusSign(str){
    return str.replace(/(^\-)(\d*$)/g, MINUS_SIGN_REPLACER + "$2");
  }

  /**
   * Replaces all two or more consequent "#" characters with just one
   * @param {String} str
   * @return {String}
   */
  function repMultipleNumberSigns(str){
    return str.replace(/\#{2,}/g, "#");
  }

  /**
   * Removes one or more "#" characters if they are in the beginning of the string
   * @param {String} str
   * @return {String}
   */
  function removeNumberSignFromTheBeginningOfAString(str){
    return str.replace(/^\#*/g, "");
  }

  function preprocessData(data){
    Object.keys(data).forEach(function(formId){
      var form = data[formId];
      Object.keys(form).forEach(function(fieldId){
        var value = form[fieldId];
        if(typeof value === 'string') {
          value = replaceMinusSign(value);
          value = repMultipleNumberSigns(value);
          value = removeNumberSignFromTheBeginningOfAString(value);
          form[fieldId] = doubleAllSpaces(value);
        }
      });
    });
    return data;
  }

  wpw.tax.albertaRSI = function() {
    var defer = wpw.tax.global.q.defer();
    wpw.tax.exports.at1.getData(function(data) {
      defer.resolve(preprocessData(data));
    }, function(err) {
      console.log(err);
      defer.reject(err);
    });
    return defer.promise;
  }

})(wpw);