(function (wpw) {
  'use strict';

  wpw.main.requires.push('wpw.efile');

  wpw.main.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/efile/:instruction?/:instructionDetails?', {
      templateUrl: wpw.utilities.templateUrl('prod/com.caseware.ca.e.review-comp.ca.2017/places/efile/efile.html'),
      controller: 'EFILEController',
      title: 'EFILE',
      reloadOnSearch: false
    });
  }]);

  wpw.efile = angular.module('wpw.efile', ['wpw.extensibility'])
    .config(['extensionServiceProvider',
      function (/** wpw.extensibility.ExtensionServiceProvider*/ extensionServiceProvider) {
        extensionServiceProvider.registerNavbarPlace(new wpw.extensibility.NavbarPlace('efile', 'EFILE',
          'prod/com.caseware.ca.e.review-comp.ca.2017/images/efile_white.svg',
          'prod/com.caseware.ca.e.review-comp.ca.2017/images/efile_white.svg', undefined, false));
      }
    ]);

  /**
   * @param {Object} $scope
   * @param {wpw.DocumentsModule.Service} documentsService
   * @constructor
   */
  wpw.efile.Controller = function ($scope, $location, $window, documentsService, saveService) {
    /**
     * Expose wpw's global variables to the html code.
     */
    $scope.global = wpw.global;
    $scope.forms = [];
    $scope.formInfo = {};

    var forms = ['com.caseware.ca.tax.ca.corp.efile',
      'com.caseware.ca.tax.ca.corp.wacOnline',
      'com.caseware.ca.tax.ca.corp.t1134',
      'com.caseware.ca.tax.ca.corp.t1135',
      'com.caseware.ca.tax.ca.corp.t106'
    ];

    var parts = $location.absUrl().split('/');

    wpw.tax.global.values = saveService.values;

    $scope.openCollaborate = function() {

      var webapps =wpw.utilities.getWebappsUrl("").split('/')[2];

      $window.open($location.protocol() + '://' + parts[2] + '/' + parts[3] +
        '/' + webapps + '/#Files?filter=Tax+Engagement', "_blank");
    };



    /**
     * Controller Initialization
     */
    function initialize() {
      for (var i = 0; i < forms.length; i++) {
        var result = documentsService.findAllFormDocuments(forms[i]);
        if (result && result.length > 0) {
          $scope.forms.push({
            id: result[0].id,
            name: result[0].number + result[0].name,
            icon: wpw.global.staticRoot + 'img/' + result[0].icon
          });
        }
      }
    }

    initialize();

  };

  wpw.efile.controller('EFILEController',
    ['$scope', '$location', '$window', 'documentsService', 'saveService', wpw.efile.Controller]);
})(wpw);
