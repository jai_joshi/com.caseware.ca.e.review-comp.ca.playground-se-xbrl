(function() {

  wpw.tax.getTableTaxInc = function getTableTaxInc(labelsObj) {
  labelsObj.num = labelsObj.num || [];
  return {
    fixedRows: true,
    infoTable: true,
    columns: [
      {
        colClass: 'std-input-width',
        type: 'none',
        cellClass: 'alignCenter'
      },
      {type: 'none', colClass: 'std-padding-width'},
      {
        colClass: 'std-input-width'
      },
      {
        colClass: 'std-padding-width',
        type: 'none',
        cellClass: 'alignCenter'
      },
      {

        type: 'none',
        cellClass: 'alignCenter'
      },
      {
        colClass: 'std-padding-width',
        type: 'none',
        cellClass: 'alignCenter'
      },
      {
        colClass: 'std-input-width'
      },
      {
        colClass: 'std-padding-width',
        type: 'none',
        cellClass: 'alignCenter'
      },
      {
        colClass: 'std-input-width'
      },
      {
        type: 'none',
        colClass: 'std-padding-width'
      }
    ],
    cells: [
      {
        '0': {label: labelsObj.label[0]},
        '2': {num: labelsObj.num[0]},
        '3': {label: 'x'},
        '4': {
          label: labelsObj.label[1],
          labelClass: 'center',
          cellClass: 'singleUnderline'
        },
        '6': {
          num: labelsObj.num[1],
          cellClass: 'singleUnderline'
        },
        '7': {label: '='},
        '8': {num: labelsObj.num[2]},
        '9': {label: labelsObj.indicator || ''}
      },
      {
        '2': {type: 'none'},
        '4': {
          label: labelsObj.label[2],
          labelClass: 'center fullLength'
        },
        '6': {num: labelsObj.num[3]},
        '8': {type: 'none'}
      }
    ]
  }
};

  wpw.tax.getTableRate = function getTableRate(labelsObj) {
  labelsObj.num = labelsObj.num || [];
  return {
    fixedRows: true,
    infoTable: true,
    columns: [
      {
        colClass: 'std-input-width',
        type: 'none',
        cellClass: 'alignCenter'
      },
      {
        colClass: 'std-input-width'
      },
      {
        colClass: 'std-padding-width',
        type: 'none',
        cellClass: 'alignCenter'
      },
      {
        type: 'none',
        cellClass: 'alignCenter'
      },
      {
        colClass: 'std-spacing-width',
        type: 'none',
        cellClass: 'alignCenter'
      },
      {
        colClass: 'std-input-width'
      },
      {
        colClass: 'std-padding-width',
        type: 'none',
        cellClass: 'alignCenter'
      },
      {
        colClass: 'std-input-width'
      },
      {
        type: 'none',
        colClass: 'std-padding-width'
      },
      {
        type: 'none',
        colClass: 'std-input-width'
      },
      {
        colClass: 'std-padding-width',
        type: 'none',
        cellClass: 'alignCenter'
      }
    ],
    cells: [
      {
        '0': {
          label: labelsObj.label[0]
        },
        1: {
          num: labelsObj.num[0]
        },
        '2': {
          label: 'x'
        },
        '3': {
          label: labelsObj.label[1],
          cellClass: 'center singleUnderline'
        },
        5: {
          num: labelsObj.num[1],
          cellClass: 'singleUnderline'
        },
        '6': {
          label: '='
        },
        7: {
          num: labelsObj.num[2]
        },
        '8': {
          label: labelsObj.indicator
        }
      },
      {
        '1': {
          type: 'none'
        },
        '3': {
          label: labelsObj.label[2]
        },
        '5': {
          num: labelsObj.num[3]
        },
        '7': {
          type: 'none'
        }
      }
    ]
  }
};

  wpw.tax.getSimpleTableRate = function getSimpleTableRate(labelsObj) {
  labelsObj.num = labelsObj.num || [];
  return {
    fixedRows: true,
    infoTable: true,
    columns: [
      {
        type: 'none',
        cellClass: 'alignLeft'
      },
      {
        type: 'none',
        colClass: 'std-padding-width'
      },
      {
        colClass: 'small-input-width'
      },
      {
        colClass: 'std-padding-width',
        type: 'none',
        cellClass: 'alignCenter'
      },
      {
        colClass: 'std-input-width'
      },
      {
        type: 'none',
        colClass: 'std-padding-width'
      }
    ],
    cells: [
      {
        '0': {
          label: labelsObj.label[0]
        },
        '1': {
          label: 'x',
          labelClass: 'center'
        },
        2: {
          num: labelsObj.num[0]
        },
        '3': {
          label: '%='
        },
        4: {
          num: labelsObj.num[1]
        },
        '5': {
          label: labelsObj.indicator
        }
      }
    ]
  }
};

  wpw.tax.getTableProRate = function getTableProRate(labelsObj) {
    labelsObj.num = labelsObj.num || [];

    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '1': {num: labelsObj.num[0]},
          '2': {label: 'x', labelClass: 'center'},
          '3': {
            label: labelsObj.label[1],
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          '5': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '6': {label: 'x'},
          '7': {num: labelsObj.num[2], decimals: labelsObj.decimals},
          '8': {label: '%='},
          '9': {num: labelsObj.num[3]},
          '10': {label: labelsObj.indicator}
        },
        {
          '1': {type: 'none'},
          '3': {
            label: labelsObj.label[2],
            labelClass: 'center'
          },
          '5': {num: labelsObj.num[4]},
          '7': {type: 'none'},
          '9': {type: 'none'}
        }
      ]
    }
  };

})();