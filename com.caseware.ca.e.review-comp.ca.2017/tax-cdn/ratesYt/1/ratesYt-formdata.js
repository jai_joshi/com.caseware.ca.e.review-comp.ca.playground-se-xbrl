(function() {

  wpw.tax.create.formData('ratesYt', {
    formInfo: {
      abbreviation: 'ratesYt',
      title: 'Yukon Table of Rates and Values',
      showCorpInfo: true,
      description: [
        {type: 'heading', text: 'Yukon'}
      ],
      category: 'Rates Tables'
    },
    sections: [
      {
        'header': 'Schedule 440 - Yukon Manufacturing and processing profits tax credit',
        'rows': [
          {
            'label': 'The lesser of amounts A and C',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '101',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'The lesser of amounts E and H',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '102',
                  'decimals': 1
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 441 - Yukon mineral exploration tax credit',
        'rows': [
          {
            'label': 'part 4 value 1',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '131'
                }
              }
            ]
          },
          {
            'label': 'part 4 value 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '132'
                }
              }
            ]
          },
          {
            'label': 'tn325',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '133'
                }
              }
            ]
          },
          {
            'label': 'tn460',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '134'
                }
              }
            ]
          },
          {
            'label': 'Maximum Yukon mineral exploration tax credit on expenses incurred after March 31, 2006 and before April 1, 2007',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '135'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 442 - Yukon Research and development tax credit',
        'rows': [
          {
            'label': 'tn120',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '120'
                }
              }
            ]
          },
          {
            'label': 'tn121',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '121'
                }
              }
            ]
          },
          {
            'label': 'Applicable rate - not paid to the Yukon College',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '122'
                }
              }
            ]
          },
          {
            'label': 'Applicable rate - paid to the Yukon College',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '123'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 443 - Yukon Corporation Tax Calculation',
        'rows': [
          {
            'label': 'Lower rate before July 1, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '111'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Lower rate after June 30, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '112'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Higher rate before July 1, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '113'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Higher rate after June 30, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '114'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      }
    ]
  });
})();
