(function() {

  wpw.tax.create.calcBlocks('ratesYt', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      field('101').assign(1.5);
      field('102').assign(12.5);
      field('111').assign(3);
      field('112').assign(2);
      field('113').assign(15);
      field('114').assign(12);
      field('120').assign(15);
      field('121').assign(5);
      field('122').assign(15);
      field('123').assign(20);
      field('131').assign(100);
      field('132').assign(25);
      field('133').assign(25);
      field('134').assign(25);
      field('135').assign(300000);
    });
  });
})();
