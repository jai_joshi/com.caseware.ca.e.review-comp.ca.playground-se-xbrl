 (function() {

  function getCalculationTableHeading() {
    return [
      {type: 'none', disabled: true},
      {type: 'none', colClass: 'std-padding-width'},
      {colClass: 'std-input-width', header: '<b>Current Year</b>', cellClass: 'alignRight', cannotOverride: true},
      {type: 'none', colClass: 'std-padding-width'},
      {colClass: 'std-input-width', header: '<b>Prior Year</b>', cellClass: 'alignRight'},
      {type: 'none', colClass: 'std-spacing-width'},
      {colClass: 'std-input-width', header: '<b>2rd Prior Year</b>', cellClass: 'alignRight'},
      {type: 'none', colClass: 'std-spacing-width'},
      {colClass: 'std-input-width', header: '<b>3rd Prior Year</b>', cellClass: 'alignRight'},
      {type: 'none', colClass: 'std-spacing-width'},
      {colClass: 'std-input-width', header: '<b>4th Prior Year</b>', cellClass: 'alignRight'},
      {type: 'none', colClass: 'std-spacing-width'},
      {colClass: 'std-input-width', header: '<b>5th Prior Year</b>', cellClass: 'alignRight'},
      {type: 'none', colClass: 'std-spacing-width'}
    ]
  }

  function getSectionHeading(sectionHeading) {
    return [
      {
        0: {label: sectionHeading, cellClass: 'singleUnderline', labelClass: 'bold'},
        2: {type: 'none'},
        4: {type: 'none'},
        6: {type: 'none'},
        8: {type: 'none'},
        10: {type: 'none'},
        12: {type: 'none'}
      }
    ]
  }

  function getNgShowOption() {
    return [
      {
        0: {label: 'Show details ?', labelClass: 'bold'},
        2: {type: 'radio', init: '1'},
        4: {type: 'none'},
        6: {type: 'none'},
        8: {type: 'none'},
        10: {type: 'none'},
        12: {type: 'none'},
        isNgShowOption: true
      }
    ]
  }

  /**
   * Default formID : T2J
   * Default fieldId: same as Tn
   * @param labelArray
   * @param isAutoFill - to determine which rows should be hidden
   * @returns {Array}
   */
  function getTableRows(labelArray, isAutoFill) {
    var tableRows = [];

    if (!angular.isArray(labelArray))
      labelArray = [labelArray];

    for (var i = 0; i < labelArray.length; i++) {
      var rowDataObj = labelArray[i];
      var inputRow = {
        0: {label: rowDataObj.label},
        1: {tn: rowDataObj.tn},
        3: {indicator: rowDataObj.indicator},
        4: {type: rowDataObj.type}
      };
      //to hide prior year data and current column for Autofill since cra only provide data for 1st prior year data
      [2,6,8,10,12].forEach(function(colIndex) {
        inputRow[colIndex] = {type: isAutoFill ? 'none' : rowDataObj.type}
      });

      inputRow.isConditionalRow = true;
      inputRow.isNgShowRow = rowDataObj.isNgShowRow;
      inputRow.autoFillTag = rowDataObj.autoFillTag;

      if (rowDataObj.isGifi) {
        inputRow.isGifi = true
      }

      if (!rowDataObj.formId) {
        inputRow.formId = 'T2J'
      }
      else {
        inputRow.formId = rowDataObj.formId;
      }

      if (!rowDataObj.fieldId) {
        if (rowDataObj.tn) {
          inputRow.fieldId = rowDataObj.tn;
        }
      }
      else {
        inputRow.fieldId = rowDataObj.fieldId;
      }

      tableRows.push(inputRow);
    }
    return tableRows
  }

  function applyNgShowToCells() {
    var updatedCells = [];

    var prevConditionRIndex = 0;
    for (var rIndex = 0; rIndex < tableCells.length; rIndex++) {
      var cellRow = tableCells[rIndex];
      var updatedCellRowObj = angular.merge({}, cellRow);

      //check if it is the form Section Heading
      if (cellRow[0].cellClass == 'bold singleUnderline' || cellRow[0].labelClass == 'bold') {
        if (cellRow.isNgShowOption) {
          prevConditionRIndex = rIndex;
        }
        updatedCells.push(updatedCellRowObj);
        continue;
      }

      if (cellRow.isConditionalRow && !cellRow.isNgShowRow) {
        updatedCells.push(updatedCellRowObj);
        continue;
      }

      updatedCellRowObj.showWhen = {fieldId: '1010', row: prevConditionRIndex, col: '2', compare: {is: '1'}};
      updatedCells.push(updatedCellRowObj);
    }
    return updatedCells;
  }

  var tableCells = getSectionHeading('Income from Financial Statements').concat
  (
      getTableRows([
        {
          label: 'Net income/loss before taxes and extraordinary items (9970)',
          isGifi: true,
          formId: 'T2S140',
          fieldId: '9970'
        },
        {
          label: 'Extraordinary item(s) (9975) ',
          isGifi: true,
          formId: 'T2S140',
          fieldId: '9975'
        },
        {
          label: 'Legal settlements (9976) ',
          isGifi: true,
          formId: 'T2S140',
          fieldId: '9976'
        },
        {
          label: 'Unrealized gains/losses (9980) ',
          isGifi: true,
          formId: 'T2S140',
          fieldId: '9980'
        },
        {
          label: 'Unusual items (9985) ',
          isGifi: true,
          formId: 'T2S140',
          fieldId: '9985'
        },
        {
          label: 'Current income taxes (9990) ',
          isGifi: true,
          formId: 'T2S140',
          fieldId: '9990'
        },
        {
          label: 'Future (deferred) income tax provision (9995) ',
          isGifi: true,
          formId: 'T2S140',
          fieldId: '9995'
        },
        {
          label: 'Total other comprehensive income (9998) ',
          isGifi: true,
          formId: 'T2S140',
          fieldId: '9998'
        },
        {
          label: 'Net income/loss after taxes and extraordinary items (9999)',
          isGifi: true,
          formId: 'T2S140',
          fieldId: '9999'
        }
      ]),
      getSectionHeading('Taxable income'),
      getTableRows([
        {
          label: 'Net income or (loss) for income tax purposes from Schedule 1, financial statements, or GIFI',
          tn: '300',
          indicator: 'A'
        }
      ]),
      getSectionHeading('Deduct:'),
      getNgShowOption(),
      getTableRows([
        {
          label: 'Charitable donations from Schedule 2',
          tn: '311',
          isNgShowRow: true
        },
        {
          label: 'Gifts to Canada, a province, or a territory from Schedule 2 ',
          tn: '312',
          isNgShowRow: true
        },
        {
          label: 'Cultural gifts from Schedule 2 ',
          tn: '313',
          isNgShowRow: true
        },
        {
          label: 'Ecological gifts from Schedule 2',
          tn: '314',
          isNgShowRow: true
        },
        {
          label: 'Gifts of medicine from Schedule 2',
          tn: '315',
          isNgShowRow: true
        },
        {
          label: 'Taxable dividends deductible under section 112 or 113, or subsection 138(6) from Schedule 3',
          tn: '320',
          isNgShowRow: true
        },
        {
          label: 'Part VI.1 tax deduction',
          tn: '325',
          isNgShowRow: true
        },
        {
          label: 'Non-capital losses of previous tax years from Schedule 4',
          tn: '331',
          isNgShowRow: true
        },
        {
          label: 'Net capital losses of previous tax years from Schedule 4',
          tn: '332',
          isNgShowRow: true
        },
        {
          label: 'Restricted farm losses of previous tax years from Schedule 4',
          tn: '333',
          isNgShowRow: true
        },
        {
          label: 'Farm losses of previous tax years from Schedule 4',
          tn: '334',
          isNgShowRow: true
        },
        {
          label: 'Limited partnership losses of previous tax years from Schedule 4',
          tn: '335',
          isNgShowRow: true
        },
        {
          label: 'Taxable capital gains or taxable dividends allocated from a central credit union',
          tn: '340',
          isNgShowRow: true
        },
        {
          label: 'Prospector\'s and grubstaker\'s shares',
          tn: '350',
          isNgShowRow: true
        },
        {
          label: 'Deduction subtotal',
          fieldId: '372',
          indicator: 'B'
        },
        {
          label: 'Subtotal (amount A minus amount B)',
          fieldId: '352',
          indicator: 'C'
        },
        {
          label: 'Add: Section 110.5 additions or subparagraph 115(1)(a)(vii) additions ',
          tn: '355',
          indicator: 'D'
        },
        {
          label: '<b>Taxable income</b> (amount C plus amount D)',
          tn: '360'
        },
        {
          label: 'Less: Income exempt under paragraph 149(1)(t)',
          tn: '370'
        },
        {
          label: 'Taxable income for a corporation with exempt income under paragraph 149(1)(t)',
          fieldId: '371',
          indicator: 'Z'
        }
      ]),
      getSectionHeading('Small business deduction'),
      getTableRows([
        {
          label: 'Small business deduction',
          fieldId: '430',
          tn: '430'
        }
      ]),
      getSectionHeading('Federal tax:'),
      getNgShowOption(),
      getTableRows([
        {
          label: 'Part I tax payable',
          tn: '700',
          isNgShowRow: true,
          indicator: 'K'
        },
        {
          label: 'Part II surtax payable',
          tn: '708',
          isNgShowRow: true
        },
        {
          label: 'Part III.1 tax payable ',
          tn: '710',
          isNgShowRow: true
        },
        {
          label: 'Part IV tax payable',
          tn: '712',
          isNgShowRow: true
        },
        {
          label: 'Part IV.1 tax payable',
          tn: '716',
          isNgShowRow: true
        },
        {
          label: 'Part VI tax payable',
          tn: '720',
          isNgShowRow: true
        },
        {
          label: 'Part VI.1 tax payable',
          tn: '724',
          isNgShowRow: true
        },
        {
          label: 'Part XIII.1 tax payable',
          tn: '727',
          isNgShowRow: true
        },
        {
          label: 'Part XIV tax payable',
          tn: '728',
          isNgShowRow: true
        },
        {
          label: '<b>Total federal tax</b>',
          fieldId: '730'
        }
      ]),
      getSectionHeading('Provincial or Territorial Tax:'),
      getTableRows([
        {
          label: '<b>Net provincial or territorial tax payable (except Quebec and Alberta)</b>',
          tn: '760'
        },
        {
          label: '<b>Total tax payable</b>',
          tn: '770'
        }
      ]),
      getSectionHeading('Deduct other credits:'),
      getNgShowOption(),
      getTableRows([
        {
          label: 'Investment tax credit refund from Schedule 31 ',
          tn: '780',
          isNgShowRow: true
        },
        {
          label: 'Dividend refund from amount U on page 6 ',
          tn: '784',
          isNgShowRow: true
        },
        {
          label: 'Federal capital gains refund from Schedule 18 ',
          tn: '788',
          isNgShowRow: true
        },
        {
          label: 'Federal qualifying environmental trust tax credit refund ',
          tn: '792',
          isNgShowRow: true
        },
        {
          label: 'Canadian film or video production tax credit refund (Form T1131) ',
          tn: '796',
          isNgShowRow: true
        },
        {
          label: 'Film or video production services tax credit refund (Form T1177) ',
          tn: '797',
          isNgShowRow: true
        },
        {
          label: 'Tax withheld at source ',
          tn: '800',
          isNgShowRow: true
        },
        {
          label: 'Total payments on which tax has been withheld',
          tn: '801',
          isNgShowRow: true
        },
        {
          label: 'Provincial and territorial capital gains refund from Schedule 18 ',
          tn: '808',
          isNgShowRow: true
        },
        {
          label: 'Provincial and territorial refundable tax credits from Schedule 5',
          tn: '812',
          isNgShowRow: true
        },
        {
          label: 'Tax instalments paid ',
          tn: '840',
          isNgShowRow: true
        },
        {
          label: '<b>Total Credits</b>',
          tn: '890'
        },
        {
          label: '<b>Balance</b>',
          fieldId: '891'
        }
      ]),
      getSectionHeading('RDTOH'),
      getTableRows([
        {
          label: 'Balance at end of tax year amount',
          autoFillTag: 'RefundableDividendTaxOnHandBalance.RefundableDividendTaxOnHandBalanceAmount',
          formId: 'T2J',
          fieldId: '460'
        },
        {
          label: 'Dividend refund for tax year amount',
          autoFillTag: 'RefundableDividendTaxOnHandBalance.DividendRefundAmount',
          formId: 'T2J',
          fieldId: '465'
        }
      ], true),
      getSectionHeading('GRIP'),
      getTableRows([
        {
          label: 'Balance at end of tax year amount',
          autoFillTag: 'GeneralRateIncomePoolBalance.GeneralRateIncomePoolBalanceAmount',
          formId: 'T2S53',
          fieldId: '100'
        },
        {
          label: 'Eligible dividends paid in the tax year amount',
          autoFillTag: 'GeneralRateIncomePoolBalance.EligibleDividendsPaidAmount',
          formId: 'T2S53',
          fieldId: '300'
        },
        {
          label: 'Excessive eligible dividend designations in tax year amount',
          autoFillTag: 'GeneralRateIncomePoolBalance.ExcessiveEligibleDividendDesignationAmount',
          formId: 'T2S53',
          fieldId: '310'
        }
      ], true),
      getSectionHeading('NCLB'),
      getTableRows([
        {
          label: 'Balance amount',
          autoFillTag: 'NonCapitalLossBalance.NonCapitalLossBalanceAmount',
          formId: 'T2S4',
          fieldId: '5020',
          rowIndex: '19',
          colIndex: '2'
        }
      ], true),
      getSectionHeading('CGLA'),
      getTableRows([
        // {
        //   label: 'Allowable business investment loss amount',
        //   autoFillTag: 'AllowableBusinessInvestmentLossAmount',
        //   formId: '',
        //   fieldId: ''
        // },
        {
          label: 'Capital Gains or losses excluding ABIL amount',
          autoFillTag: 'CapitalGainLoss.CapitalGainLossExclAllowableBusinessInvestmentLossAmount',
          formId: 'T2S6',
          fieldId: '890'
        },
        {
          label: 'Gain on donation of a share, debt obligation, or right listed on a designated stock ' +
          'exchange and other amounts under paragraph 38(a.1) amount',
          autoFillTag: 'CapitalGainLoss.GainRightOtherDonationAmount',
          formId: 'T2S6',
          fieldId: '895'
        },
        {
          label: 'Gain on donation of ecologically sensitive land amount',
          autoFillTag: 'CapitalGainLoss.GainOnDonationEcologicallySensitiveLandAmount',
          formId: 'T2S6',
          fieldId: '896'
        },
        {
          label: 'Exemption threshold at the time of disposition amount',
          autoFillTag: 'CapitalGainLoss.ExemptionThresholdTimeOfDispositionAmount',
          formId: 'T2S6',
          fieldId: '897'
        },
        {
          label: 'Total of all capital gains from the disposition of the actual property',
          autoFillTag: 'CapitalGainLoss.TotalCapitalGainDispositionOfPropertyAmount',
          formId: 'T2S6',
          fieldId: '898'
        }
      ], true),
      getSectionHeading('CDA'),
      getTableRows([
        {
          label: 'Non-taxable portion of capital gains and non-deductible portion of capital losses',
          autoFillTag: 'CapitalDividendAccountBalance.NonTaxablePortionCapitalGainAmount',
          formId: 'T2S89',
          fieldId: '390'
        },
        {
          label: 'Capital dividends received',
          autoFillTag: 'CapitalDividendAccountBalance.CapitalDividendReceivedAmount',
          formId: 'T2S89',
          fieldId: '395'
        },
        {
          label: 'Eligible capital property',
          autoFillTag: 'CapitalDividendAccountBalance.EligibleCapitalPropertyAmount',
          formId: 'T2S89',
          fieldId: '400'
        },
        {
          label: 'Life insurance proceeds',
          autoFillTag: 'CapitalDividendAccountBalance.LifeInsuranceProceedsAmount',
          formId: 'T2S89',
          fieldId: '405'
        },
        {
          label: 'Life insurance CDA',
          autoFillTag: 'CapitalDividendAccountBalance.LifeInsuranceCapitalDividendAccountAmount',
          formId: 'T2S89',
          fieldId: '410'
        },
        {
          label: 'Non-taxable portion of capital gains from a trust',
          autoFillTag: 'CapitalDividendAccountBalance.NonTaxablePortionOfCapitalGainTrustAmount',
          formId: 'T2S89',
          fieldId: '415'
        },
        {
          label: 'Capital dividends from a trust',
          autoFillTag: 'CapitalDividendAccountBalance.CapitalDividendTrustAmount',
          formId: 'T2S89',
          fieldId: '416'
        },
        {
          label: 'Amounts from predecessor corporation and wound-up subsidiaries',
          autoFillTag: 'CapitalDividendAccountBalance.PredecessorCorporationsWoundUpSubsidiariesAmount',
          formId: 'T2S89',
          fieldId: '420'
        },
        {
          label: 'Subtotal',
          autoFillTag: 'CapitalDividendAccountBalance.SubTotalAmount',
          formId: 'T2S89',
          fieldId: '421'
        },
        {
          label: 'Deduct: capital dividends that previously became payable',
          autoFillTag: 'CapitalDividendAccountBalance.CapitalDividendsPreviouslyBecamePayableAmount',
          formId: 'T2S89',
          fieldId: '422'
        },
        {
          label: 'Balance available for payment from capital dividend account',
          autoFillTag: 'CapitalDividendAccountBalance.BalanceAvailableForPaymentCapitalDividendAccountAmount',
          formId: 'T2S89',
          fieldId: '430'
        }
        // {label: 'Deduct: Dividend payable on:', type: 'date', autoFillTag: '', formId:'', fieldId: ''},
        // {label: 'Dividend Payable Amount', autoFillTag: '', formId:'', fieldId: ''},
        // {label: 'Subtotal', autoFillTag: '', formId:'', fieldId: ''},
        // {label: 'Deduct: Amount subject to Part III tax.', autoFillTag: '', formId:'', fieldId: ''},
        // {label: 'Deduct: Amount identified as election pursuant to 184(3) of the ITA', autoFillTag: '', formId:'', fieldId: ''},
        // {label: 'Capital dividend account balance as of processed date:', type: 'date', autoFillTag: '', formId:'', fieldId: ''},
        // {label: 'Capital dividend account balance', autoFillTag: '', formId:'', fieldId: ''}
      ], true)
  );

  wpw.tax.global.tableCalculations.com5Workchart = {
    '1010': {
      fixedRows: true, infoTable: true, executeAtEnd: 'true', scrollx: true, num: '1010',
      columns: getCalculationTableHeading(),
      cells: applyNgShowToCells()
    }
  }
})();
