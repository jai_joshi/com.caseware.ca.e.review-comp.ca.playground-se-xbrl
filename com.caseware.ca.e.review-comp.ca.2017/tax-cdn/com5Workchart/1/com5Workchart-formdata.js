(function () {
  'use strict';

  wpw.tax.global.formData.com5Workchart = {
    formInfo: {
      abbreviation: '5Y',
      title: '5 Years Comparative Workchart',
      showCorpInfo: true,
      headerImage: 'cw',
      category: 'Workcharts',
      dynamicFormWidth: true
    },
    sections: [
      {
        header: ' ',
        hideFieldset: true,
        rows: [
          {type: 'table', num: '1010'}
        ]
      }
    ]
  };
})();
