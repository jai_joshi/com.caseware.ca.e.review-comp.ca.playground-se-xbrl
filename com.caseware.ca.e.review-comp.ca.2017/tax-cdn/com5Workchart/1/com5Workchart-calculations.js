(function() {

  wpw.tax.create.calcBlocks('com5Workchart', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field) {
      var table = field('1010');
      table.getRows().forEach(function(row, rIndex) {
        var rowInfo = table.getRowInfo(rIndex);
        var formId = rowInfo.formId;
        var fieldId = rowInfo.fieldId;
        var sourceRowIndex = rowInfo.rowIndex;
        var sourceColIndex = rowInfo.colIndex;
        var sourceField;
        var globalVal;
        if (formId && fieldId) {
          if (rowInfo.isConditionalRow && !rowInfo.isAutoFillRow) {
            if (sourceColIndex) {
              sourceField = calcUtils.form(formId).field(fieldId).cell(sourceRowIndex, sourceColIndex);
            } else {
              sourceField = calcUtils.form(formId).field(fieldId);
            }
            if (rowInfo.isGifi) {
              globalVal = sourceField.getKey(0)
            } else {
              globalVal = sourceField.get()
            }
            row[2].assign(globalVal);
            row[2].source(sourceField)
          }
        }
      });
    });
  });
})();
