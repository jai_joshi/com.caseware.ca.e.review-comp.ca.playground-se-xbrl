(function() {
  'use strict';

  wpw.tax.create.formData('mealsEnt', {
    formInfo: {
      abbreviation: 'mealsEnt',
      title: 'Meals and Entertainment Expenses - Details',
      neededRepeatForms: ['T2S125'],
      headerImage: 'cw',
      isRepeatForm: true,
      repeatFormData: {
        linkedRepeatTable: 'mealsEntS.1000',
        titleNum: '101'
      },
      category: 'Workcharts',
      dynamicFormWidth: true
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {type: 'infoField', label: 'GL A/C # (if applicable)', width: '15%', num: '100'},
          {type: 'infoField', label: 'Description of GL A/C', inputClass: 'std-input-col-padding-width-3', num: '101'},
          {labelClass: 'fullLength'},
          {
            label: '<b>Note for 2</b>: If such expenditures are not classified, ' +
            '$50/day is construed to be a reasonable amount',
            labelClass: 'fullLength center',
            showWhen: 'showNotes2'
          },
          {
            label: '<b>Note for 7</b>: Maximum 6 for such expenditures are allowed. ' +
            'Others should be classified as \'1\', subject to 50% rule',
            labelClass: 'fullLength center',
            showWhen: 'showNotes7'
          },
          {
            label: '<b>Note for 10 & 11</b>: Tax preparer must be aware of CRA rules determining eligibility ',
            labelClass: 'fullLength center',
            showWhen: 'showNotes10'
          },
          {
            type: 'table', num: '200'
          }
        ]
      },
      {
        header: 'Total Summary of transferred data using TaxBridge',
        rows: [
          {
            type: 'table', num: '500'
          }
        ]
      },
      {

        rows: [
          {
            'label': 'Meals and Entertainment amount (Code 8523) in GIFI 125 - Income Statement',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true
            },
            'columns': [
              {
                'input': {
                  'num': 'gifiValue',
                  'disabled': true,
                  'cannotOverride': true
                }
              }
            ]
          }
        ]

      }
    ]
  });
})();
