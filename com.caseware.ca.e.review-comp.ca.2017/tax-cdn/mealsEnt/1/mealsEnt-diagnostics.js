(function() {
  wpw.tax.create.diagnostics('mealsEnt', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('MNE.type8', common.prereq(function(tools) {
          return tools.field('200').size().rows > 5;
        },
        function(tools) {
          var numType8 = 0;
          var table = tools.field('200');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[2].get() == '8' && numType8 < 6) {
              numType8++;
              return true;
            } else if (numType8 >= 6) {
              return row[2].require(function() {
                return this.get() != '8'
              })
            }
            return true;
          });
        }
    ));

  });
})();
