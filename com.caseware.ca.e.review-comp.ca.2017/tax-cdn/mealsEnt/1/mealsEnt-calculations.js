(function() {

  function setAddBack(tableRow) {
    var deductibleType = (tableRow[2] && '' + tableRow[2].get()) || 1;
    var deductible = tableRow[4].get();
    var deduction = deductible;
    var addBackFunc = {
      '1': 0.50,
      '2': 0.50,
      '3': 0.80,
      '4': 1,
      '5': 1,
      '6': 1,
      '7': 1,
      '8': 1,
      '9': 1,
      '10': 1,
      '11': 1
    };
    if (addBackFunc[deductibleType])
      deduction *= addBackFunc[deductibleType];

    return deductible - deduction;
  }

  wpw.tax.create.calcBlocks('mealsEnt', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('showQuebec', 'cp', 'prov_residence');
      calcUtils.field('gifiValue').assign(calcUtils.sumRepeatGifiValue('t2s125', 300, 8523));
      calcUtils.field('gifiValue').source(field('t2s125.8523'));
    });

    calcUtils.calc(function(calcUtils, field) {
      field('showNotes2').assign(false);
      field('showNotes7').assign(false);
      field('showNotes10').assign(false);
      field('200').getRows().forEach(function(row) {
        switch ('' + row[2].get()) {
          case '2':
            field('showNotes2').assign(true);
            break;
          case '7':
            field('showNotes7').assign(true);
            break;
          case '11':
          case '10':
            field('showNotes10').assign(true);
            break;
        }
        var addBack = setAddBack(row);
        row[5].assign(addBack); //non-quebec add back
        row[6].assign(addBack); //quebec add back
      });
    });
  });
})();
