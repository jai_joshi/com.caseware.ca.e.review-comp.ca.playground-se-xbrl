(function() {

  wpw.tax.create.tables('mealsEnt', {
    '200': {
      'hasTotals': true,
      'columns': [
        {
          'header': 'Transaction #',
          colClass: 'std-input-width',
          'canSort': true,
          'num': '201',
          type: 'text'
        },
        {
          'header': 'Date',
          colClass: 'std-input-width',
          'type': 'date',
          'canSort': true,
          'num': '202'
        },
        {
          'header': 'Type',
          type: 'n-dropdown',
          colClass: 'std-input-col-width-2',
          'showValues': 'before',
          'num': '204',
          'options': [
            {
              option: 'Partially Deductible', options: [
              {
                'option': ' All expenditures subject to 50% rule',
                'value': '1'
              },
              {
                'option': ' Portion of Conference, convention or seminar fees which can be reasonably ' +
                'construed as M & E',
                'value': '2'
              },
              {
                'option': ' All expenditures subject to 80% rule (for long haul truck drivers)',
                'value': '3'
              }
            ]
            },
            {
              option: 'Fully Deductible', options: [
              {
                'option': ' Expenditures incurred by a food service business',
                'value': '4'
              },
              {
                'option': ' Expenditures related to a fund raising event benefiting a registered charity',
                'value': '5'
              },
              {
                'option': ' Expenditures will be invoiced to a customer or clients',
                'value': '6'
              },
              {
                'option': ' Amount required to be included in the income of the taxpayer\'s employee',
                'value': '7'
              },
              {
                'option': ' Office party type expenditures',
                'value': '8'
              },
              {
                'option': ' Expenses incurred by employees while flying on business',
                'value': '9'
              },
              {
                'option': ' Eligible meals to an employee working at a remote location',
                'value': '10'
              },
              {
                'option': ' Eligible meals and entertainment paid to employees in temporary work sites',
                'value': '11'
              }
            ]
            }
          ],
          'canSort': true
        },
        {
          'header': 'Description',
          'canSort': true,
          'num': '203',
          'type': 'text'
        },
        {
          'header': '$',
          filters: 'prepend $',
          'total': true,
          'canSort': true,
          colClass: 'std-input-width',
          'init': '0',
          'num': '205',
          'totalNum': '1001'
        },
        {
          'header': '$ Add back<br>(All provinces except Quebec)',
          filters: 'prepend $',
          'disabled': true,
          colClass: 'std-input-width',
          'init': '0',
          'total': true,
          'canSort': true,
          'num': '206',
          'totalNum': '1002'
        },
        {
          'header': '$ Add back<br>(Quebec)',
          'num': '207',
          'totalNum': '1003',
          colClass: 'std-input-width',
          filters: 'prepend $',
          'disabled': true,
          'init': '0',
          'total': true,
          showWhen: {fieldId: 'CP.prov_residence', has: {QC: true}},
          'canSort': true
        }
      ]
    },
    '500': {
      'fixedRows': true,
      'columns': [
        {
          'header': 'Description of GL A/C',
          'disabled': true,
          'type': 'text'
        },
        {
          'header': 'Account balance transferred',
          colClass: 'std-input-width',
          'disabled': true
        }
      ]
    }
  });
})();
