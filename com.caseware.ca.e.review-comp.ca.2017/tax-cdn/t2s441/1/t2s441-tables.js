(function() {
  var tnWidth = '25px';
  var percentWidth = '50px';
  var formFieldWidth = '107px';
  var desWidth = '300px';

  wpw.tax.create.tables('t2s441', {

    '1100': {
      'infoTable': 'true',
      'fixedRows': 'true',
      'columns': [
        {
          'width': '48%',
          cellClass: 'alignLeft',
          'maxLength': 80
        },
        {},
        {
          'width': '48%',
          cellClass: 'alignLeft',
          'maxLength': 80
        }
      ],
      'cells': [
        {
          '0': {
            'num': '020',
            'tn': '020',
            type: 'text'
          },
          '1': {
            'type': 'none'
          },
          '2': {
            'num': '040',
            'tn': '040',
            type: 'text'
          }
        },
        {
          '0': {
            'num': '030',
            'tn': '030',
            type: 'text'
          },
          '1': {
            'type': 'none'
          },
          '2': {
            'num': '050',
            'tn': '050',
            type: 'text'
          }
        }
      ]
    },
    '1200': {
      'infoTable': false,
      'showNumbering': true,
      'maxLoop': 100000,
      'columns': [
        {
          'header': '<b>Project name</b>',
          'width': '35%',
          'num': '070',
          'tn': '070',
          'maxLength': 80,
          type: 'text'
        },
        {
          'header': '<b>Claim grant number (if any)</b>',
          'width': '30%',
          'num': '080',
          'tn': '080',
          'maxLength': 80,
          type: 'text'
        },
        {
          'header': '<b>Mining district</b>',
          'width': '35%',
          'num': '090',
          'tn': '090',
          'maxLength': 80,
          type: 'text'
        }
      ]
    },
    '1300': {
      hasTotals: true,
      'showNumbering': 'alphabetic',
      'maxLoop': 100000,
      paddingRight: 'std-input-col-padding-width',
      'columns': [
        {
          'header': '<b>Description</b>',
          'num': '140',
          'tn': '140',
          'maxLength': 80,
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          'header': '<b>Amount</b>',
          'num': '150',
          'tn': '150',
          colClass: 'std-input-width',
          'total': true,
          hiddenTotal: true
        }
      ]
    },
    '1400': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'

        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'

        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: 'Amount 12'},
          '1': {num: '1401'},
          '2': {label: 'x'},
          '3': {num: '1402'},
          '4': {label: '='},
          '5': {num: '1403'},
          '6': {label: '13'}
        },
        {
          '1': {type: 'none'},
          '3': {
            num: '1404',
            fractionDenominator: true
          },
          '5': {type: 'none'}
        }
      ]
    },
    '1500': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: desWidth,
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',

        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          width: percentWidth,

        },
        {
          type: 'none',
          width: percentWidth
        },
        {type: 'none'},
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',

        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: 'Expenditures before April 1, 2006, from line 21'},
          '1': {tn: '325'},
          '2': {num: '325'},
          '3': {label: 'x'},
          '4': {num: '1501'},
          '5': {label: '% 22'},
          '7': {label: '='},
          '8': {num: '1502'},
          '9': {label: '23'}
        },
        {
          '0': {label: 'Expenditures after March 31, 2006, from line 21 *'},
          '1': {tn: '460'},
          '2': {num: '460'},
          '3': {label: 'x'},
          '4': {num: '1506'},
          '5': {label: '% 24'},
          '7': {label: '='},
          '8': {num: '1507'},
          '9': {label: '25'}
        }
      ]
    }
  })
})();
