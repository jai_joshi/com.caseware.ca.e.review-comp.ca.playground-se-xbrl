(function() {

  wpw.tax.create.calcBlocks('t2s441', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      field('142').assign(field('1300').total(1).get());
      field('143').assign(field('142').get());
      field('160').assign(
          field('100').get() +
          field('110').get() +
          field('120').get() +
          field('130').get() +
          field('143').get()
      );
    });
    calcUtils.calc(function(calcUtils, field) {
      //part 3
      field('190').assign(field('170').get() - field('180').get());
    });
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('1402', 'ratesYt', '131');
      calcUtils.getGlobalValue('1404', 'ratesYt', '132');
      //part 4
      field('220').assign(field('200').get() - field('210').get());
      field('1403').assign(field('1401').get() * field('1402').get() / field('1404').get());
      field('250').assign(field('1403').get());
      field('260').assign(field('240').get() - field('250').get());
    });
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('1501', 'ratesYt', '133');
      calcUtils.getGlobalValue('1506', 'ratesYt', '134');
      //part 5
      field('1001').assign(field('160').get());
      field('1002').assign(field('190').get());
      field('1003').assign(field('260').get());
      field('1004').assign(field('1002').get() + field('1003').get());
      field('1005').assign(field('1004').get());
      field('1006').assign(field('1001').get() - field('1005').get());
      field('1502').assign(field('325').get() * field('1501').get() / 100);
      field('1507').assign(field('460').get() * field('1506').get() / 100);
      field('1009').assign(field('1502').get() + field('1507').get());
    });
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('1011', 'ratesYt', '135');
      //part 6
      field('1013').assign(field('1507').get());
      field('1014').assign(field('1012').get() + field('1013').get());
      field('1015').assign(field('1014').get());
      field('1016').assign(Math.max(field('1011').get() - field('1015').get(), 0));
    });

  });
})();
