(function() {

  wpw.tax.create.formData('t2s441', {
    formInfo: {
      abbreviation: 'T2S441',
      title: 'Yukon Mineral Exploration Tax Credit',
      schedule: 'Schedule 441',
      showCorpInfo: true,
      code: 'Code 0601',
      formFooterNum: 'T2 SCH 441 E (06)',
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule to claim the Yukon mineral exploration tax credit. To be eligible ' +
              'for the credit, a corporation must have a permanent establishment (as defined in section ' +
              '400 of the federal Income Tax Regulations) in the Yukon and must have incurred eligible mineral ' +
              'exploration expenses in the Yukon during the tax year. Mineral exploration expenses ' +
              'must have been incurred after March 31, 1999, and before April 1, 2007, to be ' +
              'eligible for this credit'
            },
            {
              label: 'A corporation that is a member of a partnership and that has a permanent establishment ' +
              'in the Yukon, may be eligible to claim its appropriate portion of the Yukon mineral ' +
              'exploration tax credit earned on eligible mineral exploration expenses incurred by the ' +
              'partnership in the Yukon in the year.'
            },
            {
              label: 'You are not eligible to claim the Yukon mineral exploration tax credit if:',
              sublist: [
                'the corporation did not maintain a permanent establishment in the Yukon at any time in the tax year; or',
                'the corporation is exempt from tax under subsection 149(1) of the federal <i>Income Tax Act</i>, or at ' +
                'any time of the year was controlled by one or more persons, all or part of whose ' +
                'income is exempt from tax'
              ]
            },
            {
              label: 'The maximum amount of Yukon mineral exploration tax credit you can claim for ' +
              'expenditures incurred after March 31, 2006, and before April 1, 2007, is $300,000.'
            },
            {
              label: 'To claim the Yukon mineral exploration tax credit, eligible expenditures must be ' +
              'identified on this schedule and filed with the Canada Revenue Agency no later than 12 months ' +
              'after the tax year in which the expenditures were incurred.'
            },
            {
              label: 'Corporations that received amounts from a security issued for the small business investment ' +
              'tax credit certificate—for which a business plan, filed with the application for the certificate, ' +
              'indicated the amounts were intended to be used for a purpose described under eligible mineral ' +
              'exploration expense (subsection 20(4) of the Yukon <i>Income Tax Act</i> )—must reduce their total ' +
              'eligible mineral exploration expenses by the amount calculated in Part 4 of this schedule.'
            },
            {
              label: 'File one completed copy of this schedule with your T2 Corporation Income Tax Return. ' +
              'Also, send a completed copy to: Yukon Department of Finance, Box 2703, Whitehorse YK Y1A 2C6.'
            }
          ]
        }
      ],
      category: 'Yukon Forms'
    },
    sections: [
      {
        'rows': [
          {
            'label': 'Access to Information and Protection of <i>Privacy Act</i>: The personal information requested on this form is collected under the authority of and used for the purpose of administering the Yukon <i>Income Tax Act</i>. Questions about the collection or use of this information can be directed to the Yukon Department of Finance at 867-667-5343, Box 2703, Whitehorse YK Y1A 2C6.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 1 - Exploration information',
        'rows': [
          {
            'label': 'A mineral resource that qualifies for the credit means: a base or precious metal deposit; a coal deposit; a bituminous sands deposit or oil shale deposit; deposits where the principal mineral extracted is ammonite gemstone, calcium chloride, diamond, gypsum, halite, kaolin, sylvite, or silica extracted from sandstone or quartzite; and deposits certified by the Minister of Natural Resources for which the principal mineral extracted is an industrial mineral contained in a non-bedded deposit.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'List mineral resource(s) for which exploration has taken place.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For eligible mineral exploration expenses reported in Part 2, identify each project, claim grant number, and mining district where title is registered. Attach additional schedules if more space is required.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1200'
          }
        ]
      },
      {
        'header': 'Part 2 - Eligible mineral exploration expenses',
        'rows': [
          {
            'label': 'Expenses must be incurred by the corporation after March 31, 1999, and before April 1, 2007, for the purpose of determining the existence, location, extent, or quality of a mineral resource in the Yukon.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'By category, enter the total eligible mineral exploration expenses incurred in the tax year for mineral titles listed in Part 1. (Include the corporation\'s appropriate portion of eligible mineral exploration expenses incurred by a partnership).',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Prospecting',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '100'
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              }
            ],
            'indicator': '1'
          },
          {
            'label': 'Geological, geophysical, or geochemical surveys',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '110'
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              }
            ],
            'indicator': '2'
          },
          {
            'label': 'Drilling by rotary, diamond, percussion, or other methods',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '120'
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              }
            ],
            'indicator': '3'
          },
          {
            'label': 'Trenching, digging test pits, and preliminary sampling',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '130'
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              }
            ],
            'indicator': '4'
          },
          {
            'label': 'Other eligible mineral exploration expenses. Attach additional schedules if more space is required.'
          },
          {
            'type': 'table',
            'num': '1300'
          },
          {
            'label': 'Total other eligible mineral exploration expenses',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '142'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '143'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '5'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Total eligible mineral exploration expenses (add </b>lines 1 to 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '160'
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              }
            ],
            'indicator': '6'
          },
          {
            'label': 'Note: Expenses that do not qualify for this credit include:'
          },
          {
            'label': '- any expenses related to a mine that has come into production in reasonable commercial quantities, or to a potential or actual extension of such a mine, or any expense incurred before the coming into production of the new mine (including clearing, removing overburden and stripping, as well as sinking a mine shaft or constructing an adit or other underground entry);',
            'labelClass': 'tabbed2 fullLength'
          },
          {
            'label': '- a Canadian development expense (CDE) or a Canadian exploration and development overhead expense (CEDOE) as defined in the federal <i>Income Tax Act</i> and Regulations. A CEDOE includes administration, management, or financing of the corporation, and salary, wages, or other remuneration or related benefits paid to a person employed by the corporation whose duties were not all or substantially all directed toward exploration or development activities. It also includes payments for taxes, insurance, maintenance, and leases for property on which there were no substantial exploration activities;',
            'labelClass': 'tabbed2 fullLength'
          },
          {
            'label': '- an outlay or expense included in the capital cost of depreciable property;',
            'labelClass': 'tabbed2 fullLength'
          },
          {
            'label': '- any consideration given by the corporation for any share or any interest in or right to a share;',
            'labelClass': 'tabbed2 fullLength'
          },
          {
            'label': '- the cost of, or for the use of, seismic data referred to in paragraph 66(12.6)(b.1)of the federal <i>Income Tax Act</i>;',
            'labelClass': 'tabbed2 fullLength'
          },
          {
            'label': '- those incurred in drilling or completing an oil or gas well, including the cost of building a temporary access road or preparing the site; and',
            'labelClass': 'tabbed2 fullLength'
          },
          {
            'label': '- an expense renounced to the corporation under section 66 of the federal <i>Income Tax Act</i>.',
            'labelClass': 'tabbed2 fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Determining the amount of assistance',
        'rows': [
          {
            'label': 'Total of all assistance (grants, subsidies, rebates, and forgivable loans) or reimbursements that the corporation has received or is entitled to receive in respect of the amounts referred to in Part 2 above',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '170'
                },
                'padding': {
                  'type': 'tn',
                  'data': '170'
                }
              }
            ],
            'indicator': '7'
          },
          {
            'label': '<b>Deduct:</b> Total of amounts which have been repaid in respect of line 7 above',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '180'
                },
                'padding': {
                  'type': 'tn',
                  'data': '180'
                }
              }
            ],
            'indicator': '8'
          },
          {
            'label': '<b>Net assistance</b> (line 7 <b>minus</b> line 8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '190'
                },
                'padding': {
                  'type': 'tn',
                  'data': '190'
                }
              }
            ],
            'indicator': '9'
          }
        ]
      },
      {
        'header': 'Part 4 - Determining the assistance from the Yukon small business investment tax credit',
        'rows': [
          {
            'label': 'Your claim for the Yukon mineral exploration tax credit will be reduced if the corporation has benefited from both the Yukon mineral exploration tax credit and the Yukon small business investment tax credit. Complete Part 4 only if the corporation has financed mineral exploration with funds for which a tax credit was issued under authority of the Yukon small business investment tax credit.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The total of all amounts that would have been the Yukon mineral exploration tax credits for all preceding years if there were no adjustment for the Yukon small business investment tax credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '200'
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ],
            'indicator': '10'
          },
          {
            'label': '<b>Deduct:</b> The actual amount of the corporation\'s Yukon mineral exploration tax credits claimed for all preceding years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '210'
                },
                'padding': {
                  'type': 'tn',
                  'data': '210'
                }
              }
            ],
            'indicator': '11'
          },
          {
            'label': 'Subtotal (line 10 minus line 11)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '220'
                },
                'padding': {
                  'type': 'tn',
                  'data': '220'
                }
              }
            ],
            'indicator': '12'
          },
          {
            'type': 'table',
            'num': '1400'
          },
          {
            'label': 'The total of all amounts received either in the year or in any preceding year for the issuance of security in respect of a Yukon small business investment tax credit certificate, for which a business plan filed with the application for the certificate stated that the amount was intended to be used for a purpose described in the definition of eligible mineral exploration expense in subsection 20(4) or was so used',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '240'
                },
                'padding': {
                  'type': 'tn',
                  'data': '240'
                }
              }
            ],
            'indicator': '14'
          },
          {
            'label': '<b>Deduct:</b> The amount from line 13',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '250'
                },
                'padding': {
                  'type': 'tn',
                  'data': '250'
                }
              }
            ],
            'indicator': '15'
          },
          {
            'label': '<b>Assistance from the small business investment tax credit </b> (line 14 minus line 15)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '260'
                },
                'padding': {
                  'type': 'tn',
                  'data': '260'
                }
              }
            ],
            'indicator': '16'
          }
        ]
      },
      {
        'header': 'Part 5 - Determining the Yukon mineral exploration tax credit',
        'rows': [
          {
            'label': 'Total eligible mineral exploration expenses (amount from line 6 of Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1001'
                }
              }
            ],
            'indicator': '17'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Net assistance (amount from line 9 of Part 3)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '1002'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '18'
                }
              }
            ]
          },
          {
            'label': 'Assistance from the small business tax credit (amount from line 16 of Part 4)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '1003'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '19'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 18 plus line 19)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1004'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1005'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '20'
          },
          {
            'label': 'Net eligible mineral exploration expenses (line 17 minus line 20)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1006'
                }
              }
            ],
            'indicator': '21'
          },
          {
            'type': 'table',
            'num': '1500'
          },
          {
            'label': 'Yukon mineral exploration tax credit (total of line 23 plus line 25)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1009'
                }
              }
            ],
            'indicator': '26'
          },
          {
            'label': 'Enter the amount from line 26 on line 697 in Part 2 of Schedule 5.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* The maximum amount of Yukon mineral exploration tax credit you can claim for expenditures incurred after March 31, 2006, is $300,000.'
          }
        ]
      },
      {
        'header': 'Part 6 - Cumulative amount of the Yukon mineral exploration tax credit after March 31, 2006',
        'rows': [
          {
            'label': 'Maximum Yukon mineral exploration tax credit on expenses incurred after March 31, 2006 and before April 1, 2007:',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1011'
                }
              }
            ]
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Total of all prior-year Yukon mineral exploration tax credits on expenses incurred after March 31, 2006:',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1012'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'A'
                }
              }
            ]
          },
          {
            'label': 'Yukon mineral exploration tax credits on expenses incurred after March 31, 2006 (amount from line 25 in Part 5):',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1013'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line A plus line B)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1014'
                }
              },
              {
                'input': {
                  'num': '1015'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Balance (if negative enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1016'
                }
              }
            ]
          }
        ]
      }
    ]
  });
})();
