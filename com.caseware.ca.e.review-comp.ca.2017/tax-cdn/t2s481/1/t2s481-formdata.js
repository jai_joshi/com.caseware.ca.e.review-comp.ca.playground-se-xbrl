(function() {

  wpw.tax.create.formData('t2s481', {
    formInfo: {
      abbreviation: 't2s481',
      title: 'Nunavut Corporation Tax Calculation',
      schedule: 'Schedule 481',
      formFooterNum: 'T2 SCH 481 E (16)',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            'Use this schedule if your corporation had a permanent establishment (as defined under section 400' +
            ' of the federal <i>Income Tax Regulations</i>) in Nunavut, and had taxable income earned in' +
            ' the year in Nunavut.',
            'This schedule is a worksheet only and is not required to be filed with your ' +
            '<i>T2 Corporation Income Tax Return</i>.'
          ]
        }
      ],
      category: 'Nunavut Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Calculation of income subject to Nunavut lower and higher tax rates',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable income for the Nunavut *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '500'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Income eligible for the Nunavut lower tax rate:',
            'labelClass': 'bold'
          },
          {
            'label': 'Amount from line 400 of the T2 return ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '501'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 405 of the T2 return ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '502'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 427 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '503'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'label': 'Amount B, C, or D, whichever is the least ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '504'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Income subject to   Nunavut higher tax rate </b>(amount A <b>minus </b>amount F)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '510'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Enter amount F and/or amount G on the applicable line(s) in Part 2.'
          },
          {
            'label': '* If the corporation has a permanent establishment only in Nunavut, enter the taxable income ' +
            'from line 360 of the T2 return. Otherwise, enter the taxable income allocated to Nunavut from column F ' +
            'in Part 1 of Schedule 5, <i>Tax Calculation Supplementary – Corporations</i>.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '** Includes the territories and the offshore areas for Nova Scotia and Newfoundland and Labrador.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of Nunavut tax before credits',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'labelClass'
          },
          {
            'label': ' Nunavut tax at the lower rate:',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '700'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': ' Nunavut tax at the higher rate:',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '600'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Nunavut tax before credits </b> (amount H <b>plus</b> amount I)*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '811'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* If the corporation has a permanent establishment in more than one province, or is claiming a' +
            ' Nunavut tax credit, enter amount J on line 260 of Schedule 5. Otherwise, enter it on line 760 of the' +
            ' T2 return.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  });
})();
