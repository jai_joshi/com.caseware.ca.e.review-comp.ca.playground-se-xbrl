(function() {

  var transferFunc = function() {
    var autoFillTable = wpw.tax.create.retrieve('tables', 'autoFillWorkchart')['999'];
    for(var i = 0; i < autoFillTable.cells.length; i++){
      var row = autoFillTable.cells[i];
      var transferCheckbox = wpw.tax.form('autoFillWorkchart').field('999').cell(i, 8);
      //If the field is mapped and is selected for transfer then set it
      if (!wpw.tax.utilities.isEmptyOrNull(row.desFormId) && transferCheckbox.get()) {
        var transferVal = wpw.tax.form('autoFillWorkchart').field('999').cell(i, 2).get();
        wpw.tax.utilities.isEmptyOrNull(row.desRowIndex) ?
            wpw.tax.form(row.desFormId).field(row.desFieldId).set(transferVal) :
            wpw.tax.form(row.desFormId).field(row.desFieldId).cell(row.desRowIndex, row.desColIndex).set(transferVal);
        //Set checkboxes to false after import
        transferCheckbox.set(false);
      }
    }
    wpw.tax.closeModal();
    wpw.tax.resetForm('autoFillWorkchart');
  };

  wpw.tax.create.formData('autoFillWorkchart', {
    formInfo: {
      title: 'AutoFill Workchart',
      showCorpInfo: true,
      headerImage: 'cw',
      category: 'Workcharts',
      dynamicFormWidth: true
    },
    sections: [
      {
        header: 'Auto Fill Data Center',
        rows: [
          {
            label: 'Note: This workchart only shows downloaded information which are not blank or 0 from CRA',
            labelClass: 'fullLength bold'
          },
          {
            label: 'All selected values will be imported into the current year',
            labelClass: 'fullLength bold'
          },
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            num: '1337',
            linkedCheckboxes: ['999-8'],
            label: 'Select/Unselect All',
            labelClass: 'bold'
          },
          {labelClass: 'fullLength'},
          // {type: 'table', num: '500'},
          {type: 'table', num: '999'},
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'functionButton',
            btnLabel: 'Transfer',
            btnClass: 'btn-primary btn-lg floatRight',
            paramFn: transferFunc
          }
        ]
      }
    ]
  });
})
();
