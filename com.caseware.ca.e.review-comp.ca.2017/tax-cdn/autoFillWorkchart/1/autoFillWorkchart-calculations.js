(function() {

  var corpTaxColIndex = 4;
  var tableNum = '999';

  wpw.tax.create.calcBlocks('autoFillWorkchart', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field) {
      if (wpw.tax.isAutoFill) {
        var table = field(tableNum);
        table.getRows().forEach(function(row, rIndex) {
          var rowInfo = table.getRowInfo(rIndex);
          if (!wpw.tax.utilities.isEmptyOrNull(rowInfo.desFormId)) {
            var sourceField = calcUtils.form(rowInfo.desFormId).field(rowInfo.desFieldId);
            if (!wpw.tax.utilities.isEmptyOrNull(rowInfo.desRowIndex)) {
              sourceField = sourceField.cell(rowInfo.desRowIndex, rowInfo.desColIndex)
            }
            row[corpTaxColIndex].assign(sourceField.get());
            row[corpTaxColIndex].source(sourceField);
            row[corpTaxColIndex].config('cannotOverride', true);
          }
        });
      }

    });

  });
})();
