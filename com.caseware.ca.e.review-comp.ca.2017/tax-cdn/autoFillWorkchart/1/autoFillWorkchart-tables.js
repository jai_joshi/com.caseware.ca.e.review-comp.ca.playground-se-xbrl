(function() {

  var rowCount = 0;

  function getCalculationTableHeading() {
    return [
      {
        header: '<b>Description</b>',
        colClass: 'std-input-col-width',
        type: 'none'
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        header: '<b>AutoFill Value</b>',
        disabled: true,
        cannotOverride: true
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        header: '<b>Current Value</b>',
        disabled: true,
        cannotOverride: true
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {colClass: 'std-spacing-width', type: 'none'},
      {colClass: 'std-spacing-width', type: 'none'},
      {
        header: '<b>Check to transfer AutoFill Value</b>',
        type: 'singleCheckbox',
        colClass: 'small-input-width',
        cellClass: 'alignCenter'
      }
    ]
  }

  function getSectionHeading(sectionHeading) {
    return [
      {
        0: {label: sectionHeading, cellClass: 'singleUnderline', labelClass: 'bold'},
        2: {type: 'none'},
        4: {type: 'none'},
        6: {type: 'none'},
        8: {type: 'none'}
      }
    ]
  }

  function getCalculationTableRows(labelArray) {
    //For header
    rowCount++;

    var tableRows = [];

    if (!angular.isArray(labelArray))
      labelArray = [labelArray];

    labelArray.forEach(function(rowDataObj) {
      var inputRow = {0: {label: rowDataObj.label}};
      var cellType = rowDataObj.type;
      var colValArr = [2, 4];

      if (cellType == 'dropdown') {
        var dropdownOption = rowDataObj.dropdownOption;
        colValArr.forEach(function(colIndex) {
          inputRow[colIndex] = {type: cellType, options: dropdownOption};
        });
      }
      else if (cellType == 'custom' && rowDataObj.isBn) {
        colValArr.forEach(function(colIndex) {
          inputRow[colIndex] = {type: cellType, format: ['{N9}RC{N4}', 'NR']};
        });
      }
      else {
        colValArr.forEach(function(colIndex) {
          inputRow[colIndex] = {type: cellType};
        });
      }

      inputRow.isConditionalRow = true;
      inputRow.isBn = rowDataObj.isBn;
      inputRow.isProvinces = rowDataObj.isProvinces;
      inputRow.showWhen = {fieldId: 'autoFillWorkchart.999', row: rowCount, col: 2, check: 'isNonZero'};
      inputRow.autoFillTag = rowDataObj.autoFillTag;
      inputRow.desFormId = rowDataObj.desFormId;
      inputRow.desFieldId = rowDataObj.desFieldId;
      if(rowDataObj.desRowIndex){
        inputRow.desRowIndex = rowDataObj.desRowIndex;
        inputRow.desColIndex = rowDataObj.desColIndex;
      }

      tableRows.push(inputRow);
      rowCount++;
    });
    return tableRows
  }

  wpw.tax.global.tableCalculations.autoFillWorkchart = {
    '999': {
      fixedRows: true, infoTable: true, scrollx: true,
      columns: getCalculationTableHeading(),
      cells: getSectionHeading('Business address').concat
      (
          getCalculationTableRows([
            {label: 'Address 1', type: 'text', autoFillTag: 'AddressLine1Text', desFormId:'cp', desFieldId: '011'},
            {label: 'Address 2', type: 'text', autoFillTag: 'AddressLine2Text', desFormId:'cp', desFieldId: '012'},
            {label: 'City', type: 'text', autoFillTag: 'CityName', desFormId:'cp', desFieldId: '015'},
            {label: 'Country', type: 'text', autoFillTag: 'CountryCode', desFormId:'cp', desFieldId: '017'},
            {label: 'Province/Territory/State', type: 'text', autoFillTag: 'ProvinceStateCode', desFormId:'cp', desFieldId: '016'},
            {label: 'Postal Code/Zip Code', type: 'text', autoFillTag: 'PostalZipCode', desFormId:'cp', desFieldId: '018'},
            // {label: 'Care of', type: 'text', autoFillTag: 'CareOfLine', desFormId:'cp', desFieldId: '021'},
            {label: 'Electronic Address', type: 'text', autoFillTag: 'ElectronicAddressText', desFormId:'cp', desFieldId: '089'}
          ]),
          getSectionHeading('Mailing Address'),
          getCalculationTableRows([
            {label: 'Address 1', type: 'text', autoFillTag: 'AddressLine1Text', desFormId:'cp', desFieldId: '022'},
            {label: 'Address 2', type: 'text', autoFillTag: 'AddressLine2Text', desFormId:'cp', desFieldId: '023'},
            {label: 'City', type: 'text', autoFillTag: 'CityName', desFormId:'cp', desFieldId: '025'},
            {label: 'Country', type: 'text', autoFillTag: 'CountryCode', desFormId:'cp', desFieldId: '027'},
            {label: 'Province/Territory/State', type: 'text', autoFillTag: 'ProvinceStateCode', desFormId:'cp', desFieldId: '026'},
            {label: 'Postal Code/Zip Code', type: 'text', autoFillTag: 'PostalZipCode', desFormId:'cp', desFieldId: '028'},
            {label: 'Care of', type: 'text', autoFillTag: 'CareOfLine', desFormId:'cp', desFieldId: '021'}
          ]),
          getSectionHeading('Books And Records Address'),
          getCalculationTableRows([
            {label: 'Address 1', type: 'text', autoFillTag: 'AddressLine1Text', desFormId:'cp', desFieldId: '031'},
            {label: 'Address 2', type: 'text', autoFillTag: 'AddressLine2Text', desFormId:'cp', desFieldId: '032'},
            {label: 'City', type: 'text', autoFillTag: 'CityName', desFormId:'cp', desFieldId: '035'},
            {label: 'Country', type: 'text', autoFillTag: 'CountryCode', desFormId:'cp', desFieldId: '037'},
            {label: 'Province/Territory/State', type: 'text', autoFillTag: 'ProvinceStateCode', desFormId:'cp', desFieldId: '036'},
            {label: 'Postal Code/Zip Code', type: 'text', autoFillTag: 'PostalZipCode', desFormId:'cp', desFieldId: '038'}
          ]),
          // getSectionHeading('Contact Information'),
          // getCalculationTableRows([
          //   {label: 'Last name', type: 'text', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   {label: 'First name', type: 'text', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   {label: 'Position', type: 'text', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   {label: 'Work Phone', type: 'text', autoFillTag: '', desFormId:'', desFieldId: ''}
          // ]),
          // getSectionHeading('Directors/Officers Information'),
          // getCalculationTableRows([
          //   {label: 'Director', type: 'text', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   {label: 'President', type: 'text', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   {label: 'Chair', type: 'text', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   {label: 'Vice-Chair', type: 'text', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   {label: 'Chief Executive Officer', type: 'text', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   {label: 'Chief Financial Officer', type: 'text', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   {label: 'Controller', type: 'text', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   {label: 'Authorized Signing Officer', type: 'text', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   {label: 'Other', type: 'text', autoFillTag: '', desFormId:'', desFieldId: ''}
          // ]),
          getSectionHeading('RDTOH'),
          getCalculationTableRows([
            {label: 'Balance at end of tax year amount', autoFillTag: 'RefundableDividendTaxOnHandBalanceAmount', desFormId:'t2j', desFieldId: '460'},
            {label: 'Dividend refund for tax year amount', autoFillTag: 'DividendRefundAmount', desFormId:'t2j', desFieldId: '465'}
          ]),
          getSectionHeading('GRIP'),
          getCalculationTableRows([
            {label: 'Balance at end of tax year amount', autoFillTag: 'GeneralRateIncomePoolBalanceAmount', desFormId:'T2S53', desFieldId: '100'},
            {label: 'Eligible dividends paid in the tax year amount', autoFillTag: 'EligibleDividendsPaidAmount', desFormId:'T2S53', desFieldId: '300'},
            {label: 'Excessive eligible dividend designations in tax year amount', autoFillTag: 'ExcessiveEligibleDividendDesignationAmount', desFormId:'T2S53', desFieldId: '310'}
          ]),
          getSectionHeading('NCLB'),
          getCalculationTableRows([
            {label: 'Balance amount', autoFillTag: 'NonCapitalLossBalanceAmount', desFormId:'T2S4', desFieldId: '5020', desRowIndex: '19', desColIndex: '2'}
          ])
          // getSectionHeading('CGLA'),
          // getCalculationTableRows([
          //   {label: 'Allowable business investment loss amount', autoFillTag: 'AllowableBusinessInvestmentLossAmount', desFormId:'', desFieldId: ''},
          //   {label: 'Capital Gains or losses excluding ABIL amount', autoFillTag: 'CapitalGainLossExclAllowableBusinessInvestmentLossAmount', desFormId:'T2S6', desFieldId: '890'},
          //   {
          //     label: 'Gain on donation of a share, debt obligation, or right listed on a designated stock ' +
          //     'exchange and other amounts under paragraph 38(a.1) amount', autoFillTag: 'GainRightOtherDonationAmount', desFormId:'T2S6', desFieldId: '895'
          //   },
          //   {label: 'Gain on donation of ecologically sensitive land amount', autoFillTag: 'GainOnDonationEcologicallySensitiveLandAmount', desFormId:'T2S6', desFieldId: '896'},
          //   {label: 'Exemption threshold at the time of disposition amount', autoFillTag: 'ExemptionThresholdTimeOfDispositionAmount', desFormId:'T2S6', desFieldId: '897'},
          //   {label: 'Total of all capital gains from the disposition of the actual property', autoFillTag: 'TotalCapitalGainDispositionOfPropertyAmount', desFormId:'T2S6', desFieldId: '898'}
          // ]),
          // getSectionHeading('CDA'),
          // getCalculationTableRows([
          //   {label: 'Non-taxable portion of capital gains and non-deductible portion of capital losses', autoFillTag: 'NonTaxablePortionCapitalGainAmount', desFormId:'T2S89', desFieldId: '390'},
          //   {label: 'Capital dividends received', autoFillTag: 'CapitalDividendReceivedAmount', desFormId:'T2S89', desFieldId: '395'},
          //   {label: 'Eligible capital property', autoFillTag: 'EligibleCapitalPropertyAmount', desFormId:'T2S89', desFieldId: '400'},
          //   {label: 'Life insurance proceeds', autoFillTag: 'LifeInsuranceProceedsAmount', desFormId:'T2S89', desFieldId: '405'},
          //   {label: 'Life insurance CDA', autoFillTag: 'LifeInsuranceCapitalDividendAccountAmount', desFormId:'T2S89', desFieldId: '410'},
          //   {label: 'Non-taxable portion of capital gains from a trust', autoFillTag: 'NonTaxablePortionOfCapitalGainTrustAmount', desFormId:'T2S89', desFieldId: '415'},
          //   {label: 'Capital dividends from a trust', autoFillTag: 'CapitalDividendTrustAmount', desFormId:'T2S89', desFieldId: '416'},
          //   {label: 'Amounts from predecessor corporation and wound-up subsidiaries', autoFillTag: 'PredecessorCorporationsWoundUpSubsidiariesAmount', desFormId:'T2S89', desFieldId: '420'},
          //   {label: 'Subtotal', autoFillTag: 'SubTotalAmount', desFormId:'T2S89', desFieldId: '421'},
          //   {label: 'Deduct: capital dividends that previously became payable', autoFillTag: 'CapitalDividendsPreviouslyBecamePayableAmount', desFormId:'T2S89', desFieldId: '422'},
          //   {label: 'Balance available for payment from capital dividend account', autoFillTag: 'BalanceAvailableForPaymentCapitalDividendAccountAmount', desFormId:'T2S89', desFieldId: '430'},
          //   // {label: 'Deduct: Dividend payable on:', type: 'date', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   // {label: 'Dividend Payable Amount', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   // {label: 'Subtotal', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   // {label: 'Deduct: Amount subject to Part III tax.', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   // {label: 'Deduct: Amount identified as election pursuant to 184(3) of the ITA', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   // {label: 'Capital dividend account balance as of processed date:', type: 'date', autoFillTag: '', desFormId:'', desFieldId: ''},
          //   // {label: 'Capital dividend account balance', autoFillTag: '', desFormId:'', desFieldId: ''}
          // ])
      )
    }
  }
})();
