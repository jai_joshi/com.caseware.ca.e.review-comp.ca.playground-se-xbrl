(function() {
  wpw.tax.create.diagnostics('autoFillWorkchart', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('autoFill.diffCheck', function(tools) {
      var table = tools.field('999');
      return tools.checkAll(table.getRows(), function(row) {
        //If autoFill value has been imported and the row is not selected for import
        if (row[2].isFilled() && row[8].get() !== true && row[2].get() != row[4].get()) {
          return row[2].require(function() {
            return this.get() == row[4].mark().get();
          })
        } else return true;
      })
    });

  });
})();
