(function() {
  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  wpw.tax.create.calcBlocks('t2s391', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      // to bring years of origin based on tax year end
      var tableArray = [1000, 2000, 3000];
      var taxationYearTable = field('tyh.200');
      tableArray.forEach(function(num) {
        field(num).getRows().forEach(function(row, rowIndex) {
          if (num == 1000) {
            var dateCol = Math.abs(rowIndex - 19);
            row[1].assign(taxationYearTable.cell(dateCol, 6).get());
          }
          else {
            row[1].assign(taxationYearTable.getRow(rowIndex + 10)[6].get())
          }
        })
      });
    });
    calcUtils.calc(function(calcUtils, field, form) {
      //part 1
      field('101').assign(30);
      field('102').assign(field('100').get() * field('101').get() / 100);
      field('106').assign(field('102').get() - field('105').get());
      field('108').assign(Math.min(field('106').get(), 15000));
      //part 2
      field('109').assign(field('501').get());
      field('110').assign(field('3000').cell(0, 3).get());
      field('120').assign(field('109').get() - field('110').get());
      field('123').assign(field('120').get());
      field('121').assign(field('108').get());
      field('122').assign(field('121').get() + field('123').get());
      field('140').assign(Math.min(field('122').get(), field('T2S383.301').get()));
      field('141').assign(field('904').get());
      field('142').assign(field('140').get() + field('141').get());
      field('143').assign(field('142').get());
      field('200').assign(Math.max(field('122').get() - field('143').get(), 0));
      field('904').assign(field('901').get() + field('902').get() + field('903').get());
      //part 4
      var summaryTable = field('3000');
      field('2000').getRows().forEach(function(row, rowIndex) {
        row[3].assign(summaryTable.getRow(rowIndex)[13].get());

        // historical data chart
        var limitOnCredit = field('142').get();

        summaryTable.getRows().forEach(function(row, rowIndex) {
          if (rowIndex == 0) {
            // to avoid the first row being calculated to total
            row[7].assign(0);
            row[9].assign(0);
            row[11].assign(0);
            row[13].assign(0);
          }
          else {
            row[9].assign(Math.max(row[3].get() + row[5].get() - row[7].get(), 0));
            row[11].assign(returnAmountApplied(limitOnCredit, row[9].get()));
            row[13].assign(Math.max(row[9].get() - row[11].get(), 0));
            limitOnCredit -= row[11].get();
          }
        });
        summaryTable.cell(10, 5).assign(field('108').get())

      });
    });

  });
})();

