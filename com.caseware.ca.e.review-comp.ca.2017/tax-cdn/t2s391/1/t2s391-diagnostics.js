(function() {
  wpw.tax.create.diagnostics('t2s391', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('055.200', common.prereq(common.check('t2s5.610', 'isNonZero'), common.requireFiled('T2S391')));

  });
})();
