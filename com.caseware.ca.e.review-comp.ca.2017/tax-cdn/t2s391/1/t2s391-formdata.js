(function() {
  'use strict';
  wpw.tax.create.formData('t2s391', {
    formInfo: {
      abbreviation: 't2s391',
      title: 'Manitoba Neighbourhoods Alive! Tax Credit ',
      //subTitle: '(2011 and later tax years)',
      schedule: 'Schedule 391',
      code: 'Code 1102',
      formFooterNum: 'T2 SCH 391 E (14)',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'The Manitoba Neighbourhoods Alive! tax credit (MNA!TC) is a non-refundable credit available ' +
              'to corporations that make an <b>eligible donation of money</b> to a <b>Manitoba charity</b>' +
              ' to help establish and operate an <b>eligible social enterprise</b>' +
              ' that will assist Manitobans facing barriers to employment. To qualify for the credit, ' +
              '<b>the eligible donations</b> must be made after April 12, 2011, and before January 1, 2020, ' +
              'and the <b>eligible social enterprise</b> must have began operation after April 12, 2011.'
            },
            {
              label: 'You can claim a MNA!TC under section 7.18 of the <i>Income Tax Act</i> (Manitoba) if the corporation:',
              sublist: [
                'is a taxable Canadian corporation with a permanent establishment in Manitoba;',
                'made an <b>eligible donation</b> of not less than $50,000 to a <b>Manitoba charity</b>' +
                ' within the previous four tax years;',
                'made an <b>eligible service contribution</b> to the <b>Manitoba charity</b> in the tax year for ' +
                'the benefit of the <b>eligible social enterprise</b> assisted by the <b>eligible donation</b>;',
                'did not donate more than $200,000 to the Manitoba charity in support of the same <b>eligible social enterprise;</b> and',
                'received a receipt from the <b>Manitoba charity</b> to whom the <b>eligible service contribution</b>' +
                ' was provided in a form approved by the Minister of Finance for Manitoba. '
              ]
            },
            {
              label: 'The terms <b>eligible donation, eligible service contribution, and eligible social enterprise</b>' +
              ' are defined in section 7.17 of the <i>Income Tax Act</i> (Manitoba). <b>Manitoba charity</b>' +
              ' means a registered charity that is resident in Manitoba or has a permanent establishment in Manitoba.'
            },
            {
              label: 'Your corporation may claim a maximum annual tax credit of $15,000 based on a total <b>eligible ' +
              'donation</b> of $50,000 made to the same <b>Manitoba charity</b> in the previous four tax years. ' +
              'Your corporation must also provide an <b>eligible service contribution</b> to the <b>Manitoba charity</b>' +
              ' for each year it claims the tax credit starting after the date of the <b>eligible donation</b>' +
              ' and prior to year five of the <b>eligible social enterprise</b>'
            },
            {
              label: 'The credit earned in the tax year may be used to reduce your Manitoba tax otherwise payable ' +
              'for that year. Any unused credit can be carried forward for 10 years or carried back up to three years, ' +
              'but not to a tax year-ending before 2012.'
            },
            {
              label: 'Use this schedule to:',
              sublist: [
                'claim the credit to reduce Manitoba income tax otherwise payable in the current tax year;',
                'calculate the credit you have available to carry forward; or',
                'request a carryback of the credit.'
              ]
            },
            {
              label: 'Include a completed copy of this schedule with your <i>T2 Corporation Income Tax Return.</i>' +
              ' Keep a copy of the MNA!TC receipts to support your claim but do not include the receipts with your T2 return.'
            }
          ]
        }
      ],
      category: 'Manitoba Forms'
    },
    sections: [
      {
        'header': 'Part 1 -  Credit earned in the current tax year',
        'rows': [
          {
            'type': 'table',
            'num': '150'
          },
          {
            'label': '<b>Deduct</b>'
          },
          {
            'label': 'Credits earned in the previous four tax years (for the eligible donations at line 100)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Subtotal (amount A <b>minus</b> amount B)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '106'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': '<b>Credit earned in the current tax year</b> (amount C or $15,000, whichever is less)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '108',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '108'
                }
              }
            ],
            'indicator': 'D'
          }
        ]
      },
      {
        'header': 'Part 2 -  Credit available for the year and credit available for carryforward',
        'rows': [
          {
            'label': 'Unused credit at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '109'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Deduct:</b> Credit expired after 10 tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              },
              null
            ]
          },
          {
            'label': 'Unused credit at the beginning of this tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '120',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '123',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Add',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit earned in the current tax year (amount D from Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '121'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': '<b>Total credit available for the year</b> (amount E <b>plus</b> amount F)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '122'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit claimed in the current year* (enter on line 610 of Schedule 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '140',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '140'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'H'
                }
              }
            ]
          },
          {
            'label': 'Credit carried back to the previous three tax years (complete Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '141'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'I'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount H <b>plus</b> amount I)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '142'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '143'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': '<b>Closing balance - credit available for carryforward</b> (amount G <b>minus</b> amount J)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '200'
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ],
            'indicator': ' '
          },
          {
            'label': '* The maximum credit that can be claimed in the current year is equal to the Manitoba income tax otherwise payable or amount G, whichever is less.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Request for carryback of credit',
        'rows': [
          {
            'label': 'Complete this part to ask for a carryback of a current-year credit earned to a tax year that ends <b>after 2011</b>. The total credit claimed in each previous year cannot exceed the Manitoba income tax otherwise payable for that year.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'label': '<b>Total</b> (enter on line I in Part 2)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '904',
                  'disabled': true
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 4 - Analysis of credit available for carryforward by year of origin',
        'rows': [
          {
            'label': 'You can complete this part to show all the credits from previous tax years available for carryforward, by year of origin. This will help you determine the amount of credit that could expire in following years.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The amount available from the 10th previous tax year will expire after this tax year. When you file your return for the next year, you will enter the expired amount on line 110 of Schedule 391 for that year.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Historical Data and Calculation for MNA!TC',
        'rows': [
          {
            'type': 'table',
            'num': '3000'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  });
})();
