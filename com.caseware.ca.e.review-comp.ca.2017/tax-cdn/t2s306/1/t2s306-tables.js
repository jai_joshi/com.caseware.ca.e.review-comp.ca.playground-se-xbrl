(function() {

  var columnWidth = '107px';

  wpw.tax.create.tables('t2s306', {
    '100': {
      fixedRows: true,
      showNumbering: true,
      hasTotals: true,
      columns: [
        {
          header: 'Name of each corporation that is a member of the related group<br>',
          tn: '200',"validate": {"or":[{"length":{"min":"1","max":"175"}},{"check":"isEmpty"}]}, 
          type: 'text'
        },
        {
          header: 'Business Number <br>(if a corporation is not registered, enter "NR")<br>',
          tn: '300',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          header: 'Expenditure limit',
          colClass: 'std-input-width'
        },
        {
          header: 'Allocation of capital deduction for the year<br>$<br>',
          tn: '400',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          total: true,
          totalNum: '410',
          totalTn: '410',
          totalMessage: 'Total (not to exceed $5,000,000)'
        }
      ]
    }
  })
})();

