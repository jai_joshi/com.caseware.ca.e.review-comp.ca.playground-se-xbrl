(function() {
  wpw.tax.create.diagnostics('t2s306', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S306'), forEach.row('100', forEach.bnCheckCol(1, true))));

    diagUtils.diagnostic('3060002', common.prereq(common.and(
        common.requireFiled('T2S306'),
        common.check('t2s305.120', 'isPositive')),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            return row[0].require('isFilled');
          });
        }));

    diagUtils.diagnostic('3060003', common.prereq(common.requireFiled('T2S306'),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[2].isNonZero())
              return tools.requireAll([row[0], row[1]], 'isNonZero');
            else return true
          });
        }));

    diagUtils.diagnostic('306.410', common.prereq(common.requireFiled('T2S306'),
        function(tools) {
          return tools.field('410').require(function() {
            return this.get() <= '5000000';
          })
        }));

  });
})();
