(function() {

  wpw.tax.create.formData('t2s306', {
    formInfo: {
      abbreviation: 'T2S306',
      title: 'NEWFOUNDLAND AND LABRADOR CAPITAL TAX ON FINANCIAL INSTITUTIONS – AGREEMENT AMONG RELATED' +
      ' CORPORATIONS<br> (2008 and later tax years)',
      schedule: 'Schedule 306',
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Related financial institutions that are liable to pay the Newfoundland and ' +
              'Labrador capital tax should use this schedule to allocate the capital deduction of ' +
              '$5,000,000 among the members of the related group. The $5,000,000 capital deduction ' +
              'is only available if the combined capital for the year for all members of the related ' +
              'group, using line 190 or 290 of Schedule 38, <i>Part VI Tax on Capital of Financial Institutions</i>,' +
              ' is $10,000,000 or less. If the combined capital for the year for related corporations is ' +
              'more than $10,000,000, none of the corporations are entitled to a deduction.'
            },
            {
              label: 'A related corporation that has more than one tax year-ending in a calendar year ' +
              'is required to file an agreement for each tax year-ending in that calendar year.'
            },
            {
              label: 'Subsections 190.15(4), (5), and (6) apply to this allocation.'
            },
            {
              label: 'According to subsection 190.15(5), when a corporation has more than one tax year' +
              ' ending in the same calendar year and is related in two or more of those tax years to ' +
              'another corporation that has a tax year-ending in that calendar year, the capital deduction' +
              ' of the first corporation for each such tax year at the end of which it is related to the ' +
              'other corporation is an amount equal to its capital deduction for the first such tax year.'
            },
            {
              label: 'Except as otherwise stated, sections and subsections referred to on this schedule' +
              ' are those from the federal <i>Income Tax Act</i>.'
            },
            {
              label: 'Provide details below. If you need more space, continue on a separate schedule.'
            },
            {
              label: 'File this schedule with your <i>T2 Corporation Income Tax Return</i>.'
            }
          ]
        }
      ],
      category: 'Newfoundland and Labrador Forms'
    },
    sections: [
      {
        header: 'Allocation of capital deduction for related corporations',
        rows: [
          {
            type: 'table',
            num: '100'
          }
        ]
      }
    ]
  });
})();
