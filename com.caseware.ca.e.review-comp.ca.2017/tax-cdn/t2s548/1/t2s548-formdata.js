(function() {
  'use strict';

  wpw.tax.global.formData.t2s548 = {
    formInfo: {
      abbreviation: 'T2S548',
      title: '' + '<i>' + ' Corporations Information Act ' + '</i>' +
      ' Annual Return for Foreign Business Corporations',
      //TODO: DO NOT DELETE THESE 2 LINE: subtitle and code
      //subTitle: '(2009 and later tax years)',
      schedule: 'Schedule 548',
      //code: 'Code 0902',
      headerImage: 'canada-federal',
      showCorpInfo: true,
      description: [
        {
          type: 'list',
          items: [
            'This schedule should be completed by a business corporation that is incorporated, ' +
            'continued, or amalgamated in a jurisdiction <b>outside Canada</b> with a licence under the Ontario ' +
            '<i>Extra-Provincial Corporations Act</i> to carry on business in Ontario. This completed schedule serves' +
            ' as a <i>Corporations Information Act</i> Annual Return under the Ontario ' +
            '<i>Corporations Information Act</i>.',
            'Complete parts 1 to 4. Complete parts 5 to 9 only to report change(s) in the information recorded on' +
            ' the Ontario Ministry of Government Services (MGS) public record.',
            'This schedule must set out the required information for the corporation as of the date of delivery of' +
            ' this schedule.',
            'A completed Ontario <i>Corporations Information Act</i> Annual Return must be delivered within' +
            ' six months after the end of the corporation\'s tax year-end. The MGS considers this return to be ' +
            'delivered on the date that it is filed with the Canada Revenue Agency together with the ' +
            'corporation\'s income tax return.',
            'It is the corporation\'s responsibility to ensure that the information shown on the MGS public record' +
            ' is accurate and up-to-date. To review the information shown for the corporation on the public record' +
            ' maintained by the MGS, obtain a Corporation Profile Report. Visit' + '<b>www.ServiceOntario.ca</b>'.link('https://www.ontario.ca/welcome-serviceontario') +
            ' for more information.',
            'This schedule contains non-tax information collected under the authority of the Ontario ' + '<i>' +
            ' Corporations Information Act ' + '</i>' + '. This information will be sent to the MGS for the ' +
            'purposes of recording the information on the public record maintained by the MGS.'

          ]
        }
      ],
      category: 'Ontario Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Identification',
        'rows': [
          {
            'label': 'Corporation\'s name (exactly as shown on the MGS public record)'
          },
          {
            'type': 'infoField',
            'num': '100',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '1',
                    'max': '168'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'tn': '100',
            'maxLength': '168'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'table',
            'num': '105'
          }
        ]
      },
      {
        'header': 'Part 2 - Head or registered office address (P.O. box not acceptable as stand-alone address)',
        'rows': [
          {
            'type': 'table',
            'num': '195'
          },
          {
            'type': 'table',
            'num': '205'
          },
          {
            'type': 'table',
            'num': '235'
          },
          {
            'type': 'table',
            'num': '245'
          }
        ]
      },
      {
        'header': 'Part 3 - Change Identifier',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            label: 'Have there been any changes in any of the information most recently filed for the public record ' +
            'maintained by the MGS for the corporation with respect to the address of principal office in Ontario, ' +
            'if any, the language of preference, the date commenced in Ontario and, if applicable, the date ceased ' +
            'in Ontario, the chief officer or manager in Ontario, if any, or the agent for service in Ontario?' +
            ' To review the information shown for the corporation on the public record maintained by the MGS, ' +
            'obtain a Corporation Profile Report. For more information, visit' +
            '<b>www.ServiceOntario.ca</b>'.link('https://www.ontario.ca/welcome-serviceontario'),
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'labelEnd': true,
            'label': 'If there have been no changes, enter <b>1</b> in this box and then go to "Part 4 – Certification."' +
            '<br> If there are changes, enter <b>2</b> in this box and complete the applicable parts on the next page, ' +
            'and then go to "Part 4 – Certification."',
            'num': '300',
            'tn': '300',
            'inputType': 'dropdown',
            'options': [
              {
                'value': '1',
                'option': '1'
              },
              {
                'value': '2',
                'option': '2'
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 4 - Certification',
        'rows': [
          {
            'label': 'I certify that all information given in this <i> Corporations Information Act </i> Annual Return is true, correct, and complete.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '445'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'labelEnd': true,
            'label': 'Please enter one of the following numbers in this box for the above-named person:' +
            ' <b> 1 </b> for director, <b> 2</b> for officer, or <b> 3</b> for other individual having knowledge' +
            ' of the affairs of the corporation. If you are a director and an officer, enter <b> 1</b> or <b>2</b>.',
            'labelWidth': '90%',
            'num': '460',
            'rf': true,
            'tn': '460',
            'inputType': 'dropdown',
            'options': [
              {
                'value': '1',
                'option': '1'
              },
              {
                'value': '2',
                'option': '2'
              },
              {
                'value': '3',
                'option': '3'
              }
            ],
            'init': '1'
          },
          {
            label: 'Note: Sections 13 and 14 of the Ontario Corporations Information Act provide penalties for making' +
            ' false or misleading statements or omissions.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {
            label: 'Complete the applicable parts to report changes in the information recorded on the MGS public record.',
            labelClass: 'fullLength bold center'
          }
        ]
      },
      {
        'header': 'Part 5 - Address of principal office in Ontario, if any (P.O. box not acceptable as stand-alone address)',
        'rows': [
          {
            'label': 'Please enter one of the following two numbers in this box:'
          },
          {
            'type': 'infoField',
            'labelEnd': 'true',
            'label': ' <b> 1 </b> -  Show no address of principal office in Ontario on the MGS public record. <br> <b> 2 </b> - The corporation\'s complete address in Ontario of the principal office is as follows:',
            'labelWidth': '90%',
            'num': '500',
            'tn': '500',
            'inputType': 'dropdown',
            'options': [
              {
                'value': '1',
                'option': '1'
              },
              {
                'value': '2',
                'option': '2'
              }
            ]
          },
          {
            'type': 'table',
            'num': '595'
          },
          {
            'type': 'table',
            'num': '515'
          },
          {
            'type': 'table',
            'num': '535'
          },
          {
            'type': 'table',
            'num': '545'
          }
        ]
      },
      {
        'header': 'Part 6 - Language of Preference',
        'rows': [
          {
            'type': 'infoField',
            'labelEnd': true,
            label: 'Indicate your language of preference by entering <b>1</b> for English or <b>2</b> for French. ' +
            'This is the language of preference recorded on the MGS public record for communications with the ' +
            'corporation. It may be different from line 990 on the T2 return.',
            'labelWidth': '90%',
            'num': '600',
            'tn': '600',
            'inputType': 'dropdown',
            'options': [
              {
                'value': '1',
                'option': '1'
              },
              {
                'value': '2',
                'option': '2'
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 7 - Date activity commenced or ceased, if applicable, in Ontario',
        'rows': [
          {
            'type': 'table',
            'num': '750'
          }
        ]
      },
      {
        'header': 'Part 8 - Chief officer or manager in Ontario, if any',
        'rows': [
          {
            'label': 'Name and office address of the chief officer or manager in Ontario, if any'
          },
          {
            'type': 'table',
            'num': '801'
          },
          {
            'type': 'table',
            'num': '815'
          },
          {
            'type': 'table',
            'num': '835'
          },
          {
            'type': 'table',
            'num': '845'
          },
          {
            'type': 'table',
            'num': '880'
          }
        ]
      },
      {
        'header': 'Part 9 - Agent for Service in Ontario',
        'rows': [
          {
            'label': 'If the agent for service is an individual, complete Parts 9a and 9c. If the agent for service is a corporation, complete Parts 9b and 9c. The address entered must be the head or registered office address of the corporation.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Part 9a (individual)',
            'labelClass': 'tabbed2 bold'
          },
          {
            'type': 'table',
            'num': '901'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Part 9b (corporation)',
            'labelClass': 'tabbed2 bold'
          },
          {
            'type': 'table',
            'num': '912'
          },
          {
            'type': 'table',
            'num': '913'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Part 9c (individual or corporation - must be an Ontario address',
            'labelClass': 'fullLength tabbed2 bold'
          },
          {
            'type': 'table',
            'num': '918'
          },
          {
            'type': 'table',
            'num': '935'
          },
          {
            'type': 'table',
            'num': '945'
          },
          {labelClass: 'fullLength'},
          {
            label: 'After you complete this page, complete the certification in Part 4 of this schedule.',
            labelClass: 'bold center'
          }
        ]
      }
    ]
  };
})();
