(function() {
  var jurisdictionOutside = wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.jurisdictionOutsideCA, 'value');
  wpw.tax.global.tableCalculations.t2s548 = {
    '105': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1, 2],
      'columns': [
        {
          type: 'dropdown',
          options: jurisdictionOutside,
          'width': '30%'
        },
        {
          'type': 'date',
          cellClass: 'alignCenter'
        },
        {
          'width': '20%',
          cellClass: 'alignCenter'
        }],
      'cells': [{
        '0': {
          'label': 'Jurisdiction incorporated, continued, or amalgamated, whichever is the most recent (see first bullet)',
          cellClass: 'alignCenter',
          'num': '110', "validate": {"or": [{"length": {"min": "2", "max": "3"}}, {"check": "isEmpty"}]},
          'rf': true,
          'tn': '110'
        },
        '1': {
          'label': 'Date of incorporation or amalgamation, whichever is the most recent',
          'saveNum': 'true',
          'num': '120',
          'tn': '120'
        },
        '2': {
          'label': 'Ontario Corporation No.',
          'num': '130', "validate": {"or": [{"matches": "^-?[.\\d]{2,9}$"}, {"check": "isEmpty"}]},
          'tn': '130',
          type: 'text'
        }
      }]
    },
    '195': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [{}],
      'cells': [{
        '0': {
          'num': '200', "validate": {"or": [{"length": {"min": "1", "max": "42"}}, {"check": "isEmpty"}]},
          'rf': true,
          'tn': '200',
          'label': 'Care of (if applicable)',
          type: 'text'
        }
      }]
    },
    '205': {
      'infoTable': true,
      'fixedRows': true,
      'dividers': [1,
        2],
      'columns': [{
        cellClass: 'alignCenter'
      },
        {
          'width': '75%',
          cellClass: 'alignCenter'
        },
        {
          cellClass: 'alignCenter'
        }],
      'cells': [{
        '0': {
          'label': 'Street number',
          'num': '210', "validate": {"or": [{"length": {"min": "1", "max": "8"}}, {"check": "isEmpty"}]},
          'rf': true,
          'tn': '210',
          type: 'text'
        },
        '1': {
          'label': 'Street name/Rural route/Lot and Concession number',
          'num': '220', "validate": {"or": [{"length": {"min": "1", "max": "29"}}, {"check": "isEmpty"}]},
          'rf': true,
          'tn': '220',
          type: 'text'
        },
        '2': {
          'label': 'Suite number',
          'num': '230', "validate": {"or": [{"length": {"min": "1", "max": "10"}}, {"check": "isEmpty"}]},
          'rf': true,
          'tn': '230',
          type: 'text'
        }
      }]
    },
    '235': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [{}],
      'cells': [{
        '0': {
          'label': 'Additional address information if applicable (line 220 must be completed first)',
          'tn': '240',
          'num': '240', "validate": {"or": [{"length": {"min": "1", "max": "42"}}, {"check": "isEmpty"}]},
          'rf': true,
          type: 'text'
        }
      }]
    },
    '245': {
      'infoTable': true,
      'fixedRows': true,
      'dividers': [1,
        2,
        3],
      'columns': [{
        cellClass: 'alignCenter'
      },
        {
          cellClass: 'alignCenter'
        },
        {
          cellClass: 'alignCenter'
        },
        {
          'width': '12%',
          cellClass: 'alignCenter'
        }],
      'cells': [{
        '0': {
          'label': 'Municipality (e.g., city, town)',
          'num': '250', "validate": {"or": [{"length": {"min": "1", "max": "30"}}, {"check": "isEmpty"}]},
          'rf': true,
          'tn': '250',
          type: 'text'
        },
        '1': {
          'label': 'Province/state',
          'num': '260', "validate": {"or": [{"length": {"min": "2", "max": "2"}}, {"check": "isEmpty"}]},
          'rf': true,
          'tn': '260',
          type: 'text'
        },
        '2': {
          'label': 'Country',
          'num': '270', "validate": {"or": [{"length": {"min": "2", "max": "2"}}, {"check": "isEmpty"}]},
          'rf': true,
          'tn': '270',
          type: 'text'
        },
        '3': {
          'label': 'Postal/zip code',
          'num': '280', "validate": {"or": [{"length": {"min": "1", "max": "10"}}, {"check": "isEmpty"}]},
          'rf': true,
          'tn': '280',
          type: 'text'
        }
      }]
    },
    '445': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{},
        {},
        {}],
      'cells': [{
        '0': {
          'num': '450', "validate": {"or": [{"length": {"min": "1", "max": "35"}}, {"check": "isEmpty"}]},
          'tn': '450',
          type: 'text'
        },
        '1': {
          'num': '451', "validate": {"or": [{"length": {"min": "1", "max": "20"}}, {"check": "isEmpty"}]},
          'tn': '451',
          type: 'text'
        },
        '2': {
          'num': '454', "validate": {"or": [{"length": {"min": "1", "max": "20"}}, {"check": "isEmpty"}]},
          'rf': true,
          'tn': '454',
          type: 'text'
        }
      },
      {
        '0': {
          'type': 'none',
          'label': 'Last name',
          'labelClass': 'center bold'
        },
        '1': {
          'type': 'none',
          'label': 'First name',
          'labelClass': 'center bold'
        },
        '2': {
          'type': 'none',
          'label': 'Middle name(s)',
          'labelClass': 'center bold'
        }
      }]
    },
    '555': {
      'infoTable': true,
      'fixedRows': true,
      'dividers': [1,
        2,
        3],
      'columns': [{
        cellClass: 'alignCenter'
      },
        {
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          cellClass: 'alignCenter'
        }],
      'cells': [{
        '0': {
          'label': 'Municipality (e.g., city, town)',
          'num': '560', "validate": {"or": [{"length": {"min": "1", "max": "30"}}, {"check": "isEmpty"}]},
          'tn': '560',
          type: 'text'
        },
        '1': {
          'label': 'Province <br><br> <b> Ontario </b>'
        },
        '2': {
          'label': 'Country <br><br> <b> Canada </b>'
        },
        '3': {
          'label': 'Postal code',
          'num': '570', "validate": {"or": [{"length": {"min": "6", "max": "6"}}, {"check": "isEmpty"}]},
          'tn': '570',
          type: 'text'
        }
      }]
    },
    '750': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [{
        'width': '50%',
        'type': 'none'
      },
        {
          'type': 'date',
          cellClass: 'alignCenter',
          'header': 'Commenced in Ontario'
        },
        {
          'type': 'date',
          cellClass: 'alignCenter',
          'header': 'Ceased in Ontario, if applicable'
        }],
      'cells': [{
        '0': {
          'label': 'Enter the date the corporation commenced activities in Ontario and,' +
          ' if applicable, the date activities ceased in Ontario'
        },
        '1': {
          'num': '700',
          'tn': '700'
        },
        '2': {
          'num': '710',
          'tn': '710'
        }
      }]
    },
    '801': {
      'infoTable': true,
      'fixedRows': true,
      'dividers': [1,
        2],
      'columns': [{
        cellClass: 'alignCenter'
      },
        {
          cellClass: 'alignCenter'
        },
        {
          'width': '12%',
          cellClass: 'alignCenter'
        }],
      'cells': [{
        '0': {
          'label': 'Last name',
          'num': '800', "validate": {"or": [{"length": {"min": "1", "max": "35"}}, {"check": "isEmpty"}]},
          'tn': '800',
          type: 'text'
        },
        '1': {
          'label': 'First name',
          'num': '805', "validate": {"or": [{"length": {"min": "1", "max": "20"}}, {"check": "isEmpty"}]},
          'tn': '805',
          type: 'text'
        },
        '2': {
          'label': 'Middle name(s)',
          'num': '810', "validate": {"or": [{"length": {"min": "1", "max": "20"}}, {"check": "isEmpty"}]},
          'tn': '810',
          type: 'text'
        }
      }]
    },
    '815': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1,
        2],
      'columns': [{
        'width': '12%',
        cellClass: 'alignCenter'
      },
        {
          cellClass: 'alignCenter'
        },
        {
          'width': '12%',
          cellClass: 'alignCenter'
        }],
      'cells': [{
        '0': {
          'num': '820', "validate": {"or": [{"length": {"min": "1", "max": "8"}}, {"check": "isEmpty"}]},
          'tn': '820',
          'label': 'Street number',
          type: 'text'
        },
        '1': {
          'num': '825', "validate": {"or": [{"length": {"min": "1", "max": "29"}}, {"check": "isEmpty"}]},
          'tn': '825',
          'label': 'Street name/Rural route/ Lot and Concession number',
          type: 'text'
        },
        '2': {
          'label': 'Suite number',
          'num': '830', "validate": {"or": [{"length": {"min": "1", "max": "10"}}, {"check": "isEmpty"}]},
          'tn': '830',
          type: 'text'
        }
      }]
    },
    '835': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{}],
      'cells': [{
        '0': {
          'num': '840', "validate": {"or": [{"length": {"min": "1", "max": "42"}}, {"check": "isEmpty"}]},
          'tn': '840',
          'label': 'Additional address information if applicable (line 825 must be completed first)',
          type: 'text'
        }
      }]
    },
    '845': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        cellClass: 'alignCenter'
      },
        {
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          cellClass: 'alignCenter',
          'width': '12%'
        }],
      'cells': [{
        '0': {
          'label': 'Municipality (e.g., city, town)',
          'num': '850', "validate": {"or": [{"length": {"min": "1", "max": "30"}}, {"check": "isEmpty"}]},
          'tn': '850',
          type: 'text'
        },
        '1': {
          'label': 'Province <br><br> <b> Ontario </b>'
        },
        '2': {
          'label': 'Country <br><br> <b> Canada </b>'
        },
        '3': {
          'label': 'Postal code',
          'num': '860', "validate": {"or": [{"length": {"min": "6", "max": "6"}}, {"check": "isEmpty"}]},
          'tn': '860',
          type: 'text'
        }
      }]
    },
    '880': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        'width': '50%',
        'type': 'none'
      },
        {
          'type': 'date',
          'header': 'Date appointed',
          cellClass: 'alignCenter'
        },
        {
          'type': 'date',
          'header': 'Date ceased, if applicable',
          cellClass: 'alignCenter'
        }],
      'cells': [{
        '0': {
          'label': 'Enter the date the person assumed this position and, if applicable,' +
          ' the date the person ceased to hold this position'
        },
        '1': {
          'num': '890',
          'tn': '890'
        },
        '2': {
          'num': '895',
          'tn': '895'
        }
      }]
    },
    '901': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1,
        2],
      'columns': [{
        cellClass: 'alignCenter'
      },
        {
          cellClass: 'alignCenter'
        },
        {
          cellClass: 'alignCenter'
        }],
      'cells': [{
        '0': {
          'label': 'Last name',
          'num': '900', "validate": {"or": [{"length": {"min": "1", "max": "35"}}, {"check": "isEmpty"}]},
          'tn': '900',
          type: 'text'
        },
        '1': {
          'label': 'First name',
          'num': '905', "validate": {"or": [{"length": {"min": "1", "max": "20"}}, {"check": "isEmpty"}]},
          'tn': '905',
          type: 'text'
        },
        '2': {
          'label': 'Middle name(s)',
          'num': '910', "validate": {"or": [{"length": {"min": "1", "max": "20"}}, {"check": "isEmpty"}]},
          'tn': '910',
          type: 'text'
        }
      }]
    },
    '912': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{}],
      'cells': [{
        '0': {
          'label': 'Corporation\'s name (exactly as shown on the MGS public record)',
          'num': '915', "validate": {"or": [{"length": {"min": "1", "max": "168"}}, {"check": "isEmpty"}]},
          'tn': '915',
          type: 'text'
        }
      }]
    },
    '913': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1, 2],
      'columns': [{
        'width': '80%',
        cellClass: 'alignCenter'
      },
        {
          cellClass: 'alignCenter'
        }],
      'cells': [{
        '0': {
          'label': 'Care of (if applicable)',
          'num': '916', "validate": {"or": [{"length": {"min": "1", "max": "42"}}, {"check": "isEmpty"}]},
          'tn': '916',
          type: 'text'
        },
        '1': {
          'label': 'Ontario Corporation No.',
          'num': '917', "validate": {"or": [{"matches": "^-?[.\\d]{1,9}$"}, {"check": "isEmpty"}]},
          'tn': '917',
          type: 'text'
        }
      }]
    },
    '918': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1,
        2],
      'columns': [{
        'width': '20%',
        cellClass: 'alignCenter'
      },
        {
          cellClass: 'alignCenter'
        },
        {
          'width': '20%',
          cellClass: 'alignCenter'
        }],
      'cells': [{
        '0': {
          'label': 'Street number',
          'num': '920', "validate": {"or": [{"length": {"min": "1", "max": "8"}}, {"check": "isEmpty"}]},
          'tn': '920',
          type: 'text'
        },
        '1': {
          'label': 'Street name/Rural route/Lot and Concession number',
          'num': '925', "validate": {"or": [{"length": {"min": "1", "max": "29"}}, {"check": "isEmpty"}]},
          'tn': '925',
          type: 'text'
        },
        '2': {
          'label': 'Suite number',
          'num': '930', "validate": {"or": [{"length": {"min": "1", "max": "10"}}, {"check": "isEmpty"}]},
          'tn': '930',
          type: 'text'
        }
      }]
    },
    '935': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        cellClass: 'alignCenter'
      }],
      'cells': [{
        '0': {
          'label': 'Additional address information if applicable (line 925 must be completed first)',
          'num': '940', "validate": {"or": [{"length": {"min": "1", "max": "42"}}, {"check": "isEmpty"}]},
          'tn': '940',
          type: 'text'
        }
      }]
    },
    '945': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1,
        2,
        3],
      'columns': [{
        cellClass: 'alignCenter'
      },
        {
          cellClass: 'alignCenter'
        },
        {
          cellClass: 'alignCenter'
        },
        {
          'width': '20%',
          cellClass: 'alignCenter'
        }],
      'cells': [{
        '0': {
          'label': 'Municipality (e.g., city, town)',
          'num': '950', "validate": {"or": [{"length": {"min": "1", "max": "30"}}, {"check": "isEmpty"}]},
          'tn': '950',
          type: 'text'
        },
        '1': {
          'label': 'Province',
          'num': '960', "validate": {"or": [{"length": {"min": "2", "max": "2"}}, {"check": "isEmpty"}]},
          'tn': '960',
          type: 'text'
        },
        '2': {
          'label': 'Country',
          'num': '970', "validate": {"or": [{"length": {"min": "2", "max": "2"}}, {"check": "isEmpty"}]},
          'tn': '970',
          type: 'text'
        },
        '3': {
          'label': 'Postal code',
          'num': '980', "validate": {"or": [{"length": {"min": "6", "max": "6"}}, {"check": "isEmpty"}]},
          'tn': '980',
          type: 'text'
        }
      }]
    },
    '515': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1,
        2],
      'columns': [
        {
          'width': '12%',
          cellClass: 'alignCenter'
        },
        {
          cellClass: 'alignCenter'
        },
        {
          'width': '12%',
          cellClass: 'alignCenter'
        }],
      'cells': [
        {
          '0': {
            'num': '520', "validate": {"or": [{"length": {"min": "1", "max": "8"}}, {"check": "isEmpty"}]},
            'tn': '520',
            'label': 'Street number',
            type: 'text'
          },
          '1': {
            'num': '530', "validate": {"or": [{"length": {"min": "1", "max": "29"}}, {"check": "isEmpty"}]},
            'tn': '530',
            'label': 'Street name/Rural route/ Lot and Concession number',
            type: 'text'
          },
          '2': {
            'label': 'Suite number',
            'num': '540', "validate": {"or": [{"length": {"min": "1", "max": "10"}}, {"check": "isEmpty"}]},
            'tn': '540',
            type: 'text'
          }
        }]
    },
    '535': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{}],
      'cells': [{
        '0': {
          'num': '550', "validate": {"or": [{"length": {"min": "1", "max": "42"}}, {"check": "isEmpty"}]},
          'tn': '550',
          'label': 'Additional address information if applicable (line 530 must be completed first)',
          type: 'text'
        }
      }]
    },
    '545': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          cellClass: 'alignCenter'
        },
        {
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          cellClass: 'alignCenter',
          'width': '12%'
        }],
      'cells': [
        {
          '0': {
            'label': 'Municipality (e.g., city, town)',
            'num': '560', "validate": {"or": [{"length": {"min": "1", "max": "30"}}, {"check": "isEmpty"}]},
            'tn': '560',
            type: 'text'
          },
          '1': {
            'label': 'Province <br><br> <b> Ontario </b>'
          },
          '2': {
            'label': 'Country <br><br> <b> Canada </b>'
          },
          '3': {
            'label': 'Postal code',
            'num': '570', "validate": {"or": [{"length": {"min": "6", "max": "6"}}, {"check": "isEmpty"}]},
            'tn': '570',
            type: 'text'
          }
        }]
    },
    '595': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [{}],
      'cells': [{
        '0': {
          'num': '510', "validate": {"or": [{"length": {"min": "1", "max": "42"}}, {"check": "isEmpty"}]},
          'rf': true,
          'tn': '510',
          'label': 'Care of (if applicable)',
          type: 'text'
        }
      }]
    }
  }
})();
