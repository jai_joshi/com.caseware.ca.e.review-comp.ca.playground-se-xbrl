(function() {
  wpw.tax.create.diagnostics('t2s548', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var addressFields = ['210', '220', '250', '270'];
    var part5List = ['510', '520', '530', '540', '550', '560', '570'];
    var part5ListRequired = ['500', '520', '530', '560', '570'];
    var part8List = ['800', '805', '810', '520', '825', '830', '840', '850', '860', '890', '895'];
    var part8ListRequired = ['800', '805', '820', '825', '850', '860', '890'];
    var part9List = ['900', '905', '910', '915', '916', '917', '920', '925', '930', '940', '950', '960', '970', '980'];
    var part9ListRequired = ['920', '925', '950', '970'];

    diagUtils.diagnostic('5480001', common.prereq(common.requireFiled('T2S548'), common.requireNotFiled('T2S546')));

    diagUtils.diagnostic('5480001', common.prereq(common.requireFiled('T2S546'), common.requireNotFiled('T2S548')));

    diagUtils.diagnostic('5480002', common.prereq(common.requireFiled('T2S548'), common.check(['100', '110', '120', '130'], 'isFilled', true)));

    diagUtils.diagnostic('5480003', common.prereq(common.requireFiled('T2S548'),
        function(tools) {
          if (tools.field('270').get() == 'CA' || tools.field('270').get() == 'US')
            return tools.requireAll(tools.list(addressFields.concat('260', '280')), 'isNonZero');
          else
            return tools.requireAll(tools.list(addressFields), 'isNonZero');
        }));

    diagUtils.diagnostic('5480004', common.prereq(common.and(
        common.requireFiled('T2S548'),
        function(tools) {
          return (tools.field('300').get() == 2);
        }), function(tools) {
      if (tools.field('970').get() == 'CA' || tools.field('970').get() == 'US') {
        return (tools.requireAll(tools.list(part5ListRequired), 'isNonZero') ||
            tools.requireAll(tools.list('600'), 'isNonZero') ||
            tools.requireAll(tools.list(['700', '710']), 'isNonZero') ||
            tools.requireAll(tools.list(part8ListRequired), 'isNonZero') ||
            (tools.requireAll(tools.list(['900', '905']), 'isNonZero') ||
            tools.requireAll(tools.list(['915', '917']), 'isNonZero') &&
            tools.requireAll(tools.list(part9ListRequired.concat(['960', '980'])), 'isNonZero')));
      }
      else {
        return (tools.requireAll(tools.list(part5ListRequired), 'isNonZero') ||
            tools.requireAll(tools.list('600'), 'isNonZero') ||
            tools.requireAll(tools.list(['700', '710']), 'isNonZero') ||
            tools.requireAll(tools.list(part8ListRequired), 'isNonZero') ||
            (tools.requireAll(tools.list(['900', '905']), 'isNonZero') ||
            tools.requireAll(tools.list(['915', '917']), 'isNonZero') &&
            tools.requireAll(tools.list(part9ListRequired), 'isNonZero')));
      }
    }));

    diagUtils.diagnostic('5480005', common.prereq(common.and(
        common.requireFiled('T2S548'),
        function(tools) {
          return (tools.field('500').get() == 1);
        }), common.check(part5List, 'isEmpty', true)));

    diagUtils.diagnostic('5480006', common.prereq(common.and(
        common.requireFiled('T2S548'),
        function(tools) {
          return (tools.field('500').get() == 2);
        }), common.check(['530', '560', '570', '520'], 'isNonZero', true)));

    diagUtils.diagnostic('5480007', common.prereq(common.and(
        common.requireFiled('T2S548'),
        common.check('710', 'isNonZero')),
        common.check('700', 'isNonZero')));

    diagUtils.diagnostic('5480008', common.prereq(common.and(
        common.requireFiled('T2S548'),
        common.check(['800', '805', '810'], 'isNonZero')),
        common.check(['825', '850', '860', '820'], 'isNonZero', true)));

    diagUtils.diagnostic('5480009', common.prereq(common.and(
        common.requireFiled('T2S548'),
        common.check(['820', '825', '830', '840', '850', '860'], 'isNonZero')),
        common.check(['800', '805', '890'], 'isNonZero', true)));

    diagUtils.diagnostic('5480010', common.prereq(common.and(
        common.requireFiled('T2S548'),
        common.check('890', 'isNonZero')),
        common.check(['800', '805', '825', '850', '860', '820'], 'isNonZero', true)));

    diagUtils.diagnostic('5480011', common.prereq(common.and(
        common.requireFiled('T2S548'),
        common.check('895', 'isNonZero')),
        common.check('890', 'isNonZero')));

    diagUtils.diagnostic('5480012', common.prereq(common.and(
        common.requireFiled('T2S548'),
        common.check(['900', '905', '910'], 'isNonZero')),
        common.check(['915', '916', '917'], 'isEmpty', true)));

    diagUtils.diagnostic('5480013', common.prereq(common.and(
        common.requireFiled('T2S548'),
        common.check(['915', '916', '917'], 'isNonZero')),
        common.check(['900', '905', '910'], 'isEmpty', true)));

    diagUtils.diagnostic('5480014', common.prereq(common.and(
        common.requireFiled('T2S548'),
        common.check(['900', '905', '910'], 'isNonZero')),
        function(tools) {
          if (tools.field('970').get() == 'CA' || tools.field('970').get() == 'US')
            return tools.requireAll(tools.list(['925', '950', '970', '920', '960', '980']), 'isNonZero');
          else
            return tools.requireAll(tools.list(part9ListRequired), 'isNonZero');
        }));

    diagUtils.diagnostic('5480015', common.prereq(common.and(
        common.requireFiled('T2S548'),
        common.check(['915', '916', '917'], 'isNonZero')),
        function(tools) {
          if (tools.field('970').get() == 'CA' || tools.field('970').get() == 'US')
            return tools.requireAll(tools.list(['925', '950', '970', '920', '960', '980']), 'isNonZero');
          else
            return tools.requireAll(tools.list(part9ListRequired), 'isNonZero');
        }));

    diagUtils.diagnostic('5480016', common.prereq(common.requireFiled('T2S548'),
        common.check(['300', '450', '451', '460'], 'isNonZero', true)));

    diagUtils.diagnostic('5480017', common.prereq(common.and(
        common.requireFiled('T2S548'),
        common.check(['925', '950', '960', '970', '980'], 'isNonZero')),
        common.or(
            common.check(['900', '905'], 'isNonZero', true),
            common.check(['915', '917'], 'isNonZero', true))));

    diagUtils.diagnostic('5480018', common.prereq(common.and(
        common.requireFiled('T2S548'),
        function(tools) {
          return (tools.field('300').get() == 1);
        }), function(tools) {
      return (tools.requireAll(tools.list(part5List.concat('500')), 'isEmpty') &&
      tools.requireAll(tools.list(['600']), 'isEmpty') &&
      tools.requireAll(tools.list(['700', '710']), 'isEmpty') &&
      tools.requireAll(tools.list(part8List), 'isEmpty') &&
      tools.requireAll(tools.list(part9List), 'isEmpty'));
    }));

    diagUtils.diagnostic('5480019', common.prereq(common.requireFiled('T2S548'),
        function(tools) {
          return tools.field('110').require(function() {
            return this.get() != 'CA';
          })
        }));

  });
})();
