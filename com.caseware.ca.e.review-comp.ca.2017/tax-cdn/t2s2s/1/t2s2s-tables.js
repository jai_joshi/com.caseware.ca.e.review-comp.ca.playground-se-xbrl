(function() {
  wpw.tax.global.tableCalculations.t2s2s = {
    "100": {
      hasTotals: true,
      "executeAtEnd": true,
      "linkedRepeatForm": "T2S2W",
      "maxLoop": 100000,
      "columns": [{
        "header": "Description",
        "width": "30%",
        'type': 'text',
        cellClass: 'alignCenter',
        "disabled": true,
        "num": "300",
        "linkedFieldId": "101"
      },
        {
          "header": "1. Charitable Donations <br>(Part 1 of Sch. 2)",
          filters: 'prepend $',
          "disabled": true,
          "total": true,
          "totalNum": "401",
          "linkedFieldId": "300"
        },
        {
          "header": "2. Gifts of certified cultural property <br>(Part 3 of Sch. 2)",
          filters: 'prepend $',
          "disabled": true,
          "total": true,
          "totalNum": "403",
          "linkedFieldId": "302"
        },
        {
          "header": "3a. Gifts of certified ecologically sensitive land made before February 11, 2014<br>(Part 4 of Sch. 2)",
          filters: 'prepend $',
          "disabled": true,
          "total": true,
          "totalNum": "404",
          "linkedFieldId": "303"
        },
        {
          "header": "3b. Gifts of certified ecologically sensitive land made after February 10, 2014<br>(Part 4 of Sch. 2)",
          filters: 'prepend $',
          "disabled": true,
          "total": true,
          "totalNum": "405",
          "linkedFieldId": "304"
        }]
    }
  }
})();
