(function() {
  'use strict';
  wpw.tax.global.formData.t2s2s = {
      formInfo: {
        abbreviation: 'T2S2S',
        title: 'Donations Workchart Summary',
        neededRepeatForms: ['T2S2W'],
        headerImage: 'cw',
        category: 'Workcharts'
      },
      sections: [
        {
          hideFieldset: true,
          rows: [
            {labelClass: 'fullLength'},
            {labelClass: 'fullLength'},
            {
              type: 'table',
              num: '100'
            },
            {labelClass: 'fullLength'},
            {
              'label': 'Total of all TB GL Accounts:',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '999',
                    'disabled': true,
                    'cannotOverride': true
                  }
                }
              ]
            }
          ]
        }
      ]
    };
})();
