(function() {
  wpw.tax.create.diagnostics('t2s556', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S556'), common.bnCheck('480')));

    diagUtils.diagnostic('5560005', common.prereq(common.check(['t2s5.458'], 'isNonZero'), common.requireFiled('T2S556')));

    diagUtils.diagnostic('5560010', common.prereq(common.and(
        common.requireFiled('T2S556'),
        common.check(['450'], 'isNonZero')),
        common.check(['t2s5.458', '755', '815', '915'], 'isEmpty', true)));

    diagUtils.diagnostic('5560015', common.prereq(common.and(
        common.requireFiled('T2S556'),
        common.check(['755', '815', '915'], 'isNonZero')),
        common.check(['200', '205', '210', '215', '220', '225', '230', '235', '240', '245', '250'], 'isNonZero', true)));

    diagUtils.diagnostic('5560020', common.prereq(common.and(
        common.requireFiled('T2S556'),
        common.check(['755', '815', '915'], 'isNonZero')),
        common.check(['300', '310', '320', '330', '340', '350'], 'isNonZero', true)));

    diagUtils.diagnostic('5560025', common.prereq(common.and(
        common.requireFiled('T2S556'),
        common.check(['755', '815', '915'], 'isNonZero')),
        common.check('400', 'isNonZero')));

    diagUtils.diagnostic('556.450', common.prereq(common.and(
        common.requireFiled('T2S556'),
        common.check(['450'], 'isNonZero')),
        common.check(['470', '480'], 'isNonZero', true)));

    diagUtils.diagnostic('556.800', common.prereq(common.requireFiled('T2S556'),
        function(tools) {
          return tools.field('800').require(function() {
            return this.get() < 50000;
          })
        }));

  });
})();
