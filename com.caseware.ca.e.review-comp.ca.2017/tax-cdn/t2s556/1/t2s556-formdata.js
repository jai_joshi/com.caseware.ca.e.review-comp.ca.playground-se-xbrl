(function() {

  wpw.tax.create.formData('t2s556', {
    formInfo: {
      abbreviation: 't2s556',
      isRepeatForm: true,
      repeatFormData: {
        titleNum: '205'
      },
      title: 'Ontario Film and Television Tax Credit',
      schedule: 'Schedule 556',
      showCorpInfo: true,
      code: 'Code 1701',
      formFooterNum: 'T2 SCH 556 E (17)',
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule to claim an Ontario film and television tax credit (OFTTC) under ' +
              'section 91 of the <i>Taxation Act</i>, 2007 (Ontario). Complete a separate Schedule 556 ' +
              'for each eligible Ontario production.'
            },
            {
              label: 'The OFTTC is a refundable tax credit based upon qualifying labour expenditures incurred ' +
              'by a qualifying production company in a tax year for eligible Ontario productions.'
            },
            {
              label: 'Before claiming an OFTTC, a qualifying production company must apply online to' +
              ' the Ontario Media Development Corporation (OMDC) for a certificate of eligibility.' +
              ' If the production is eligible, the OMDC will issue a certificate certifying that' +
              ' the production is an eligible Ontario production and certifying the estimated amount of the ' +
              'corporation\'s OFTTC for the production. Enter the certificate information for ' +
              'this production in Part 2 of this schedule.'
            },
            {
              label: 'If you claim, or have claimed, the Ontario production services tax credit for' +
              ' a production for any tax year, the OFTTC you are entitled to is nil for that same ' +
              'eligible Ontario production.'
            },
            {
              label: 'To claim the OFTTC, you must meet the eligibility requirements in Part 3 of this schedule.'
            },
            {
              label: 'To claim the OFTTC, include a completed copy of this schedule for each production ' +
              'with your T2 Corporation Income Tax Return for the tax year as well as a ' +
              'copy of the certificate of eligibility issued by the OMDC.'
            }
          ]
        }
      ],
      category: 'Ontario Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Contact Information',
        'rows': [
          {
            'type': 'table',
            'num': '099'
          }
        ]
      },
      {
        'header': 'Part 2 - Identifying the eligible Ontario production',
        'rows': [
          {
            'type': 'infoField',
            'label': 'Certificate of eligibility number',
            'width': '70%',
            'num': '200',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '8',
                    'max': '9'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'tn': '200'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'infoField',
            'label': 'Production title',
            'width': '70%',
            'num': '205',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '1',
                    'max': '175'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'tn': '205'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'splitTable',
            'fieldAlignRight': true,
            'side1': [
              {
                'type': 'infoField',
                'label': 'Date principal photography commenced',
                'inputType': 'date',
                'num': '210',
                'tn': '210',
                'labelWidth': '65%'
              }
            ],
            'side2': [
              {
                'type': 'infoField',
                'label': 'Production commencement time',
                'inputType': 'date',
                'num': '215',
                'tn': '215',
                'labelWidth': '5%'
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'label': 'Is this a first-time production?',
            'inputType': 'radio',
            'num': '220',
            'tn': '220'
          },
          {
            'type': 'infoField',
            'label': 'Did the production receive a regional bonus?',
            'inputType': 'radio',
            'num': '225',
            'tn': '225'
          },
          {
            'type': 'infoField',
            'label': 'Was the production a treaty co-production?',
            'inputType': 'radio',
            'num': '230',
            'tn': '230'
          },
          {
            'type': 'infoField',
            'label': 'Was the production an interprovincial co-production?',
            'inputType': 'radio',
            'num': '235',
            'tn': '235'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'multiColumn',
            'columns': [
              [
                {
                  'type': 'infoField',
                  'label': 'Total estimated production costs',
                  'valueType': 'number',
                  'num': '240',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'tn': '240'
                }
              ],
              [
                {
                  'type': 'infoField',
                  'label': 'Estimated qualifying Ontario labour expenditure',
                  'valueType': 'number',
                  'num': '245',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'tn': '245'
                }
              ],
              [
                {
                  'type': 'infoField',
                  'label': 'Estimated OFTTC',
                  'valueType': 'number',
                  'num': '250',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'tn': '250'
                }
              ]
            ]
          },
          {
            'type': 'horizontalLine'
          }
        ]
      },
      {
        'header': 'Part 3 - Eligibility',
        'rows': [
          {
            'type': 'infoField',
            'label': '1. Was the corporation a Canadian-controlled corporation throughout the tax year as determined under sections 26 to 28 of the <i>Investment Canada Act</i>?',
            'inputType': 'radio',
            'init': '2',
            'num': '300',
            'tn': '300'
          },
          {
            'type': 'infoField',
            'label': '2. Were the activities of the corporation in the tax year primarily the carrying on of a Canadian film or video production business through a permanent establishment in Canada?',
            'inputType': 'radio',
            'init': '2',
            'num': '310',
            'tn': '310'
          },
          {
            'type': 'infoField',
            'label': '3. Did the corporation have a permanent establishment in Ontario throughout the tax year?',
            'inputType': 'radio',
            'init': '2',
            'num': '320',
            'tn': '320'
          },
          {
            'type': 'infoField',
            'label': '4. Was the corporation exempt from tax for the tax year under Part III of the <i>Taxation Act</i>, 2007 (Ontario) or Part I of the federal <i>Income Tax Act</i>?',
            'inputType': 'radio',
            'init': '1',
            'num': '330',
            'tn': '330'
          },
          {
            'type': 'infoField',
            'label': '5. Was the corporation, at any time in the tax year, controlled directly or indirectly, in any manner, by one or more persons, all or part of whose taxable income was exempt from tax under Part I of the federal <i>Income Tax Act</i>, Part II of the <i>Corporations Tax Act</i> (Ontario), or Part III of the <i>Taxation Act</i>, 2007 (Ontario)?',
            'inputType': 'radio',
            'init': '1',
            'num': '340',
            'tn': '340'
          },
          {
            'type': 'infoField',
            'label': '6. Was the corporation, at any time in the tax year, a prescribed labour-sponsored venture capital corporation under 6701 of the <i>Federal Income Tax Regulations</i>?',
            'inputType': 'radio',
            'init': '1',
            'num': '350',
            'tn': '350'
          },
          {
            'label': 'If you answered <b>no</b> to question 1, 2, or 3, or <b>yes</b> to question 4, 5, or 6, <b>you are not eligible</b> for the OFTTC.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Part 4 - Ontario labour expenditures for the eligible Ontario production for the tax year',
        'spacing': 'R_tn2',
        'rows': [
          {
            'type': 'infoField',
            'label': 'Have you claimed an Ontario production services tax credit for the same production identified in Part 2?',
            'inputType': 'radio',
            'init': '2',
            'num': '400',
            'tn': '400'
          },
          {
            'label': 'If you answered <b>yes</b> to this question, the OFTTC for this production <b>is nil</b>.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Salaries and wages paid to Ontario-based individuals that are directly attributable to the eligible Ontario production',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '405',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '405'
                }
              }
            ]
          },
          {
            'label': 'Remuneration that is directly attributable to the eligible Ontario production paid to:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- Ontario-based individuals',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '410',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '410'
                }
              },
              null
            ]
          },
          {
            'label': '- other taxable Canadian corporations (for their Ontario-based employees)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '420',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '420'
                }
              },
              null
            ]
          },
          {
            'label': '-  other taxable Canadian corporations (solely owned by an Ontario-based individual)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '430',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '430'
                }
              },
              null
            ]
          },
          {
            'label': '- partnerships carrying on business in Canada (for their Ontario-based members or employees)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '440',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '440'
                }
              },
              null
            ]
          },
          {
            'label': 'Amounts paid to a parent corporation for Ontario labour expenditures incurred by it for the subsidiary wholly-owned corporation under a reimbursement agreement',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '450',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '450'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of lines 410 to 450)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1010',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '460',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '460'
                }
              }
            ]
          },
          {
            'label': 'Complete lines 470 and 480 if there is an entry on line 450:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'none',
            'label': 'Name of parent corporation'
          },
          {
            'type': 'infoField',
            'inputType': 'textArea',
            'tn': '470',
            'num': '470',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '1',
                    'max': '175'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          {
            'type': 'infoField',
            'inputType': 'custom',
            'format': [
              '{N9}RC{N4}',
              'NR'
            ],
            'label': 'Business Number of parent corporation',
            'num': '480',
            'tn': '480'
          },
          {
            'label': '<b>Ontario labour expenditures for the eligible Ontario production for the tax year</b> (line 405 <b>plus</b> line 460)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '490',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '490'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 5 - Assistance',
        'rows': [
          {
            'label': 'Assistance for the cost of the eligible Ontario production that the qualifying production company, or other person, or partnership has received, is entitled to receive, or may reasonably expect to receive:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount of assistance directly attributable to and that has not caused a reduction in the Ontario labour expenditures for the eligible Ontario production',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '500',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '500'
                }
              }
            ]
          },
          {
            'label': 'Amount of assistance not directly attributable to and that has not caused a reduction in the Ontario labour expenditures for the eligible Ontario production',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '510',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '510'
                }
              },
              null
            ]
          },
          {
            'label': 'Cost of the eligible Ontario production',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '520',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '520'
                }
              },
              null
            ]
          },
          {
            'type': 'table',
            'num': '2000'
          },
          {
            'label': 'Subtotal (line 500 <b>plus</b> line 530)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '540',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '540'
                }
              }
            ]
          },
          {
            'label': 'Amounts repaid under a legal obligation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '545',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '545'
                }
              },
              null
            ]
          },
          {
            'label': 'Certain amounts of assistance to have been paid/received, as applicable, for the eligible<br>' +
            ' production and included on line 540:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario refundable tax credits and assistance*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '550',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '550'
                }
              },
              null
            ]
          },
          {
            'label': 'Federal tax credits **',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '555',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '555'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of lines 545, 550 and 555)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1011'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1012'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': '<b>Net assistance </b>(line 540 <b>minus</b> amount A)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '560',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '560'
                }
              }
            ]
          },
          {
            type: 'description',
            lines: [
              {
                type: 'list', listType: 'asterisk',
                items: [
                  {
                    label: 'Include only the following Ontario refundable tax credits and assistance:',
                    sublist: [
                      'Ontario Book Publishing Tax Credit',
                      'Ontario Computer Animation and Special Effects Tax Credit',
                      'Ontario Sound Recording Tax Credit',
                      'Ontario Film and Television Tax Credit',
                      'Payments from the 2015 Ontario Production Services and Computer' +
                      ' Animation and Special Effects Transitional Fund administered by the OMDC.'
                    ]
                  },
                  {
                    label: 'Include only the following Federal tax credits: Film or Video Production' +
                    ' Services Tax Credit, Investment tax credit, and Investment tax credit of cooperative ' +
                    'corporation.'
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 6 - Qualifying labour expenditures (QLE) for the tax year',
        'spacing': 'R_tn2',
        'rows': [
          {
            label: 'These amounts should be determined without reference to any equity investment held by' +
            ' a person prescribed under subsection 1106(10) of the Federal <i>Income Tax Regulations</i>.',
            labelClass: 'fullLength'
          },
          {
            'label': 'Ontario labour expenditures for the eligible Ontario production for the tax year <br>' +
            '(line 490 from Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1015'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Ontario labour expenditures for the eligible Ontario production for all previous tax years<br>' +
            '(total of lines 490 from previous years)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '610',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '610'
                }
              },
              null
            ]
          },
          {
            'label': 'QLE for the eligible Ontario production for all previous tax years <br>' +
            '(total of lines 675 from previous years)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '620',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '620'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 610 <b>minus</b> line 620)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1016',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '630',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '630'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount B <b>plus</b> line 630)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '640',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '640'
                }
              }
            ]
          },
          {
            'label': 'Net assistance (line 560 from Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1017'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Ontario labour expenditures transferred under a reimbursement agreement from the parent ' +
            'corporation to its subsidiary wholly-owned corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '660',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '660'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (amount C <b>plus</b> line 660)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1018',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '670',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '670'
                }
              }
            ]
          },
          {
            'label': '<b>QLE for the tax year</b> (line 640 <b>minus</b> line 670) (if negative, enter "0") .',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline':'double',
                'input': {
                  'num': '675',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '675'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Determine the portion of QLE from line 675 that relates to expenditures incurred before January 1, 2008 (pre-2008), and the portion that relates to expenditures incurred after December 31, 2007 (post-2007). Enter the respective amounts on lines 680 and 685 below.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Pre-2008 portion of QLE for the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '680',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '680'
                }
              },
              null
            ]
          },
          {
            'label': 'Post-2007 portion of QLE for the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '685',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '685'
                }
              },
              null
            ]
          }
        ]
      },
      {
        'header': 'Part 7 - Tax credit calculation for a first-time production',
        'rows': [
          {
            'label': 'Complete this part if the production is a first-time production and the QLE is more than $50,000 at the time the production is completed or the QLE is $50,000 or less and no OFTTC is being claimed or has been claimed under Part 8. Otherwise, complete Part 8 or 9, as applicable.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Basic amount',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1019'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              },
              null
            ]
          },
          {
            'label': 'QLE for the eligible Ontario production for all previous tax years(line 620 from Part 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '700',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '700'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (amount D <b>minus</b> line 700) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1020'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              },
              null
            ]
          },
          {
            'label': 'Pre-2008 portion of QLE for the tax year(line 680 from Part 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '705',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '705'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'The lesser of amount E and line 705',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '710',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '710'
                }
              },
              null,
              null
            ]
          },
          {
            'type': 'table',
            'num': 'di_table_1'
          },
          {
            'type': 'table',
            'num': 'di_table_2'
          },
          {
            'label': 'Post-2007 portion of QLE for the tax year (Line 685 from Part 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '721',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '721'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Amount E',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1023'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F'
                }
              },
              null
            ]
          },
          {
            'label': 'Line 710',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '1024'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (amount F <b>minus</b> amount G)<br>(if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1025'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'H'
                }
              },
              null
            ]
          },
          {
            'label': 'The lesser of line 721 and the amount H',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '722',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '722'
                }
              },
              null,
              null
            ]
          },
          {
            'type': 'table',
            'num': 'di_table_3'
          },
          {
            'type': 'table',
            'num': 'di_table_4'
          },
          {
            'label': 'If the first-time production is a regional Ontario production, you can claim the additional credit below:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': 'di_table_5'
          },
          {
            'label': '<b>Ontario film and television tax credit for a first-time production</b><br>(total of lines 715, 720, 723, 724 and 750)',
            'forceBreakAfter': true,
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '755',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '755'
                }
              }
            ]
          },
          {
            'label': 'Enter the amount on line 755 on line 458 of Schedule 5,' +
            ' <i>Tax Calculation Supplementary – Corporations</i>. If you are filing more than one Schedule 556, ' +
            'add the total of all amounts on line 755, all of the amounts on line 815 from Part 8, and all' +
            ' of the amounts on line 915 from Part 9 on all of the schedules and enter ' +
            'the result on line 458 of Schedule 5.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 8 - Tax credit calculation for a small first-time production',
        'rows': [
          {
            'label': 'Complete this part if the production is a first-time production, the QLE is $50,000 or less at the time the production is completed, and no OFTTC for this production has been claimed or is being claimed in Part 7. If this is not a first-time production, complete Part 9 instead.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'label': 'Is the small first-time production a regional Ontario production ?',
            'inputType': 'radio',
            'num': '1041',
            'init': '2'
          },
          {
            'label': 'QLE for the tax year (line 675 from Part 6) (cannot be more than $50,000)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '800',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '800'
                }
              }
            ]
          },
          {
            'label': 'Maximum eligible credit for the small first-time production*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '805',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '805'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario film and television tax credit claimed and allowed in previous tax years for the eligible Ontario production',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '810',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '810'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 805 <b>minus</b> line 810) (if negative, enter "0")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1031'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1032'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': '<b>Ontario film and television tax credit for a small first-time production</b><br>' +
            '(enter line 800 or amount I, whichever is less)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '815',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '815'
                }
              }
            ]
          },
          {
            'label': 'Enter the amount on line 815 on line 458 of Schedule 5, ' +
            '<i>Tax Calculation Supplementary – Corporations</i>. If you are filing more than one Schedule 556, add ' +
            'the total of all amounts on line 815, all of the amounts on line 755 from Part 7, and ' +
            'all of the amounts on line 915 from Part 9 on all of the schedules and enter ' +
            'the result on line 458 of Schedule 5.',
            'labelClass': 'fullLength'
          },
          {
            'label': '* If the small first-time production is a regional Ontario production, enter $20,000 on line 805; otherwise, enter $15,000.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 9 - Tax credit calculation for an eligible Ontario production other than a first-time production',
        'rows': [
          {
            'label': 'Complete this part if the production is not a first-time production.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': 'di_table_6'
          },
          {
            'type': 'table',
            'num': 'di_table_7'
          },
          {
            'type': 'table',
            'num': 'di_table_8'
          },
          {
            'label': '<b>Ontario film and television tax credit for an eligible Ontario production other than a first-time production</b>' +
            '<br>(total of lines 900, 903 and 910)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '915',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '915'
                }
              }
            ]
          },
          {
            'label': 'Enter the amount on line 915 on line 458 of Schedule 5, <i>Tax Calculation Supplementary ' +
            '– Corporations</i>. If you are filing more than one Schedule 556, add the total of all amounts on ' +
            'line 915, all of the amounts on line 755 from Part 7, and all of the amounts on line 815 from ' +
            'Part 8 on all of the schedules and enter the result on line 458 of Schedule 5.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  });
})();
