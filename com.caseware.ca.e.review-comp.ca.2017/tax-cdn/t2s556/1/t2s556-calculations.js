(function() {

  wpw.tax.create.calcBlocks('t2s556', function(calcUtils) {
    calcUtils.calc(function(calcUtils) {
      var num = '1021';
      var num2 = '715';
      var midnum = '715Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '1022';
      var num2 = '720';
      var midnum = '720Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '1026';
      var num2 = '723';
      var midnum = '723Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '1027';
      var num2 = '724';
      var midnum = '724Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '1028';
      var num2 = '750';
      var midnum = '750Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '1033';
      var num2 = '900';
      var midnum = '900Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '1034';
      var num2 = '903';
      var midnum = '903Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '1035';
      var num2 = '910';
      var midnum = '910Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('1019', 'ratesOn', '720');
      calcUtils.getGlobalValue('715Mult', 'ratesOn', '721');
      calcUtils.getGlobalValue('720Mult', 'ratesOn', '722');
      calcUtils.getGlobalValue('723Mult', 'ratesOn', '723');
      calcUtils.getGlobalValue('724Mult', 'ratesOn', '724');
      calcUtils.getGlobalValue('750Mult', 'ratesOn', '725');

      calcUtils.getGlobalValue('900Mult', 'ratesOn', '729');
      calcUtils.getGlobalValue('903Mult', 'ratesOn', '730');
      calcUtils.getGlobalValue('910Mult', 'ratesOn', '731');

      //part 1 calcs
      field('099').cell(0, 0).assign(field('T2J.951').get() + ' ' + field('T2J.950').get());
      field('099').cell(0, 1).assign(field('T2J.956').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 3 calcs
      var isEligible = (
          field('300').get() === '1' &&
          field('310').get() === '1' &&
          field('320').get() === '1' &&
          field('330').get() === '2' &&
          field('340').get() === '2' &&
          field('350').get() === '2' &&
          field('400').get() === '2'
      );

      calcUtils.sumBucketValues('1010', ['410', '420', '430', '440', '450']);
      field('460').assign(field('1010').get());

      if (field('450').get() > 0) {
        field('470').disabled(false);
        field('480').disabled(false)
      } else {
        calcUtils.removeValue(['470', '480'], true)
      }

      calcUtils.sumBucketValues('490', ['405', '460']);

      //part 5
      var table2000 = field('2000');
      table2000.cell(0, 1).assign(field('510').get());
      table2000.cell(0, 5).assign(field('640').get() - field('660').get());
      var amountK = field('520').get();
      table2000.cell(1, 5).assign(amountK !== 0 ? amountK : 0);
      table2000.cell(0, 9).assign(table2000.cell(1, 5).get() == 0 ? 0 :
          table2000.cell(0, 1).get() * table2000.cell(0, 5).get() / table2000.cell(1, 5).get());
      field('540').assign(field('500').get() + table2000.cell(0, 9).get());
      calcUtils.sumBucketValues('1011', ['545', '550', '555']);
      field('1012').assign(field('1011').get());
      field('560').assign(field('540').get() - field('1012').get());
      //part 6
      field('1015').assign(field('490').get());
      field('1016').assign(Math.max(0, field('610').get() - field('620').get()));
      field('630').assign(field('1016').get());
      field('640').assign(field('1015').get() + field('630').get());
      field('1017').assign(field('560').get());
      field('1018').assign(field('1017').get() + field('660').get());
      field('670').assign(field('1018').get());
      field('675').assign(Math.max(field('640').get() - field('670').get(), 0));
      var amount680 = field('680').get();
      var amount675 = field('675').get();
      var amount685 = amount675 - amount680;
      field('685').assign(amount685 > 0 ? amount685 : 0);

      //calcs for part 7,8,9 eligible
      var isFirstTimeProduction = (field('220').get() === '1');
      var qleAmount = field('675').get();
      var isPart7Applicable = false;
      var isPart8Applicable = false;
      var isPart9Applicable = false;

      if (isFirstTimeProduction && (qleAmount > 50000 || (qleAmount <= 50000 && field('815').get() !== 0))) {
        isPart7Applicable = true;
      } else if (isFirstTimeProduction && qleAmount <= 50000 && field('755').get() !== 0) {
        isPart8Applicable = true;
      } else {
        isPart9Applicable = true;
      }

      field('700').assign(isPart7Applicable ? field('620').get() : 0);
      field('1020').assign(Math.max(field('1019').get() - field('700').get(), 0));
      field('705').assign(isPart7Applicable ? field('680').get() : 0);
      field('710').assign(Math.min(field('1020').get(), field('705').get()));
      field('1021').assign(field('705').get() - field('710').get());
      field('715').assign(field('1021').get() * field('715Mult').get() / 100);
      field('1022').assign(field('710').get());
      field('720').assign(field('1022').get() * field('720Mult').get() / 100);
      field('721').assign(isPart7Applicable ? field('685').get() : 0);
      field('1023').assign(field('1020').get());
      field('1024').assign(field('710').get());
      field('1025').assign(Math.max(field('1023').get() - field('1024').get(), 0));
      field('722').assign(Math.min(field('721').get(), field('1025').get()));
      field('1026').assign(field('721').get() - field('722').get());
      field('723').assign(field('1026').get() * field('723Mult').get() / 100);
      field('1027').assign(field('722').get());
      field('724').assign(field('1027').get() * field('724Mult').get() / 100);
      field('1028').assign(isPart7Applicable ? field('675').get() : 0);
      field('750').assign(field('1028').get() * field('750Mult').get() / 100);

      //part 8
      var isONProduction = (field('1041').get() === '1');
      field('800').assign(isPart8Applicable ? Math.min(field('675').get(), 50000) : 0);
      field('805').assign(isPart8Applicable ? (isONProduction ? 20000 : 15000) : 0);
      field('1031').assign(Math.max(field('805').get() - field('810').get(), 0));
      field('1032').assign(field('1031').get());

      //part 9
      field('1033').assign(isPart9Applicable ? field('680').get() : 0);
      field('1034').assign(isPart9Applicable ? field('685').get() : 0);
      field('1035').assign(isPart9Applicable ? field('675').get() : 0);
      field('900').assign(field('1033').get() * field('900Mult').get() / 100);
      field('903').assign(field('1034').get() * field('903Mult').get() / 100);
      field('910').assign(field('1035').get() * field('910Mult').get() / 100);

      if (isEligible) {
        if (isPart7Applicable) {
          calcUtils.sumBucketValues('755', ['715', '720', '723', '724']);
        } else {
          field('755').assign(0)
        }
        if (isPart8Applicable) {
          field('815').assign(Math.min(field('800').get(), field('1032').get()));
        } else {
          field('815').assign(0)
        }
        if (isPart9Applicable) {
          calcUtils.sumBucketValues('915', ['900', '903', '910']);
        } else {
          field('915').assign(0)
        }
      } else {
        field('755').assign(0);
        field('815').assign(0);
        field('915').assign(0);
      }
    });

  });
})
();
