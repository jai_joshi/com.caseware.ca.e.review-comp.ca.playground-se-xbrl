(function() {

  var descriptionWidth = '285px';
  var spacingWidth = '10px';

  wpw.tax.create.tables('t2s556', {
    'di_table_1': {
      'num': 'di_table_1',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Pre-2008 QLE at the 30% rate (line 705 <b>minus</b> line 710)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '1021',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' doubleUnderline'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '715Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '715'
          },
          '7': {
            'num': '715',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_2': {
      'num': 'di_table_2',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Pre-2008 QLE at the 40% rate (line 710)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '1022',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' doubleUnderline'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '720Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '720'
          },
          '7': {
            'num': '720',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_3': {
      'num': 'di_table_3',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Post-2007 QLE at the 35% rate (line 721 <b>minus</b> line 722)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '1026',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' doubleUnderline'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '723Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '723'
          },
          '7': {
            'num': '723',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_4': {
      'num': 'di_table_4',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Post-2007 QLE at the 40% rate (line 722)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '1027',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' doubleUnderline'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '724Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '724'
          },
          '7': {
            'num': '724',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_5': {
      'num': 'di_table_5',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'QLE for the tax year (line 675 from Part 6)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '1028',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' doubleUnderline'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '750Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '750'
          },
          '7': {
            'num': '750',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' singleUnderline'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_6': {
      'num': 'di_table_6',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Pre-2008 portion of QLE for the tax year (line 680 in Part 6)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '1033',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '900Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '900'
          },
          '7': {
            'num': '900',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_7': {
      'num': 'di_table_7',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Post-2007 portion of QLE for the tax year (line 685 in Part 6)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '1034',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '903Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '903'
          },
          '7': {
            'num': '903',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_8': {
      'num': 'di_table_8',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'QLE for the tax year (line 675 in Part 6)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '1035',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '910Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '910'
          },
          '7': {
            'num': '910',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' singleUnderline'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    '099': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1],
      'columns': [
        {
          'width': '60%',
          cellClass: 'alignLeft'
        },
        {
          cellClass: 'alignLeft'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Name of contact person',
            'num': '110',
            'tn': '110',
            type: 'text'
          },
          '1': {
            'label': 'Telephone number',
            'num': '120',
            'tn': '120',
            'telephoneNumber': true, type: 'text'
          }
        }
      ]
    },
    '2000': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none',
          'textAlign': 'center'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          width: descriptionWidth,
          type: 'none',
          'textAlign': 'center'
        },
        {
          width: spacingWidth,
          type: 'none'
        },
        {
          colClass: 'std-input-width'

        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width',

        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {label: 'Line 510',
            cellClass: 'singleUnderline'
          },
          '2': {label: ' x '},
          '3': {label: 'Line 640 of part 6 <b>minus</b> line 660 of part 6',
            cellClass: 'singleUnderline'
          },
          '6': {label: ' = '},
          '8': {tn: '530'}
        },
        {
          '1': {type: 'none'},
          '3': {
            label: 'Amount from line 520',

            labelClass: 'center'
          },
          '5': {},
          '9': {type: 'none'}
        }
      ]
    }
  })
})();
