(function() {

  wpw.tax.create.calcBlocks('t2s461', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //Part 1 Cals
      var taxableIncomeAllocation = calcUtils.getTaxableIncomeAllocation('NT');
      field('500').assign(taxableIncomeAllocation.provincialTI);
      field('112').assign(taxableIncomeAllocation.allProvincesTI);

      field('500').source(taxableIncomeAllocation.provincialTISourceField);
      field('112').source(taxableIncomeAllocation.allProvincesTISourceField);

      field('111').assign(field('500').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('501', 'T2J', '400');
      calcUtils.getGlobalValue('502', 'T2J', '405');
      calcUtils.getGlobalValue('503', 'T2J', '427');
      calcUtils.min('504', ['501', '502', '503']);
      calcUtils.equals('110', '504');
      calcUtils.divide('113', '111', '112', '110');
      calcUtils.subtract('510', '500', '113');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('703', 'ratesNt', '703');
      calcUtils.getGlobalValue('705', 'ratesNt', '705');
      //Part 2 Calcs
      calcUtils.equals('701', '113');
      calcUtils.equals('704', '510');
      calcUtils.multiply('702', ['701', '703'], 1 / 100);
      calcUtils.multiply('706', ['704', '705'], 1 / 100);
      calcUtils.sumBucketValues('811', ['702', '706']);
    });


  });
})();
