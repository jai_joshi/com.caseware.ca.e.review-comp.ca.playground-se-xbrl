(function() {

  wpw.tax.create.formData('t2s461', {
    formInfo: {
      abbreviation: 't2s461',
      title: 'Northwest Territories Corporation Tax Calculation',
      schedule: 'Schedule 461',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 461 E (17)',
      description: [
        {
          type: 'list',
          items: [
            'Use this schedule if your corporation had a permanent establishment (as defined in section ' +
            '400 of the federal <i>Income Tax Regulations</i>) in the Northwest Territories, and had ' +
            'taxable income earned in the year in the Northwest Territories.',
            'This schedule is a worksheet only and is not required to be filed with your <i>T2 ' +
            'Corporation Income Tax Return</i>.'
          ]
        }
      ],
      category: 'Northwest Territories Forms'
    },
    sections: [
      {
        'header': 'Part 1 -  Calculation of income subject to Northwest Territories lower and higher tax rates',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable income for the Northwest Territories *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '500'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Income eligible for the Northwest Territories lower tax rate:',
            'labelClass': 'bold'
          },
          {
            'label': 'Amount from line 400 of the T2 return ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '501'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 405 of the T2 return ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '502'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 427 of the T2 return ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '503'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'label': 'Amount B, C, or D, whichever is the least ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '504'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': ' <b> Income subject to   Northwest Territories higher tax rate </b>(amount A <b>minus </b>amount F)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '510'
                }
              }
            ],
            'indicator': 'G'
          },
          {labelClass: 'fullLength'},
          {
            'label': '* If the corporation has a permanent establishment only in the Northwest Territories, enter' +
            ' the taxable income from line 360 of the T2 return. Otherwise, enter the taxable income allocated to the ' +
            'Northwest Territories from column F in Part 1 of Schedule 5,' +
            ' <i>Tax Calculation Supplementary – Corporations</i>.',
            'labelClass': 'fullLength tabbed'
          },
          {labelClass: 'fullLength'},
          {
            'label': '**  Includes the territories and the offshore jurisdictions for Nova Scotia and Newfoundland and Labrador.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of  Northwest Territories tax before credits',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': ' Northwest Territories tax at the lower rate:',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '700'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': ' Northwest Territories tax at the higher rate:',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '600'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Northwest Territories tax before credits </b> (amount H <b>plus</b> amount I)*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '811'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '*   If the corporation has a permanent establishment in more than one jurisdiction, or is claiming a Northwest Territories tax credit, enter amount J on line 250 of Schedule 5. Otherwise, enter it on line 760 of the T2 return.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      }
    ]
  });
})();
