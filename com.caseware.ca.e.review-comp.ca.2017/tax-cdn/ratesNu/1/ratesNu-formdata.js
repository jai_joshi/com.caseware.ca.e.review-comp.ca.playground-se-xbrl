(function() {

  wpw.tax.create.formData('ratesNu', {
    formInfo: {
      abbreviation: 'ratesNu',
      title: 'Nunavut Table of Rates and Values',
      showCorpInfo: true,
      description: [
        {type: 'heading', text: 'Northwest Territories'}
      ],
      category: 'Rates Tables'
    },
    sections: [
      {
        'header': 'Schedule 481 - Nunavut Corporation Tax Calculation ',
        'rows': [
          {
            'label': 'Lower rate from 2014 to present',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '703'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Higher rate from 2014 to present',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '705'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 490 - Nunavut Business Training Tax Credit',
        'rows': [
          {
            'label': 'Basic BTTC rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '706'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'For eligible employees rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '707'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      }
    ]
  });
})();
