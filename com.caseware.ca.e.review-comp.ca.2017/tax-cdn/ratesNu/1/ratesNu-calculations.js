(function() {

  wpw.tax.create.calcBlocks('ratesNu', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      field('703').assign(4);
      field('705').assign(12);
      field('706').assign(30);
      field('707').assign(20);
    });
  });
})();
