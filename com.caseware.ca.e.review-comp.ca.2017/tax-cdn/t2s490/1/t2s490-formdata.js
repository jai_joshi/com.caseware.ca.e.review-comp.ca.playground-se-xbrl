(function() {

  wpw.tax.create.formData('t2s490', {
    formInfo: {
      abbreviation: 't2s490',
      title: 'Nunavut business training tax credit',
      schedule: 'Schedule 490',
      showCorpInfo: true,
      isRepeatForm: true,
      repeatFormData: {
        titleNum: '110'
      },
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule to claim a Nunavut business training tax credit (BTTC) under ' +
              'section 6.3 of the <i>Income Tax Act</i> (Nunavut)'
            },
            {
              label: 'A BTTC can be claimed for any business training completed successfully by an eligible employee.'
            },
            {
              label: 'The BTTC is a refundable tax credit that is equal to 30% of the corporation\'s business ' +
              'training expenses for each eligible employee, plus an additional 20% of the business ' +
              'training expenses if the eligible employee is a beneficiary under the Nunavut Land Claims Agreement.'
            },
            {
              label: 'Eligible business training expenses are direct and indirect costs incurred by a ' +
              'corporation, less any financial assistance.'
            },
            {
              label: 'A corporation is not eligible for the BTTC if it meets any of the following conditions:',
              sublist: [
                'it does not have a permanent establishment in Nunavut in the tax year;',
                'it is exempt from tax under the <i>Income Tax Act</i> (Nunavut);',
                'it is controlled directly or indirectly by one or more persons who are exempt from tax ' +
                'under the <i>Income Tax Act</i> (Nunavut);',
                'it has received more than half of its total revenue in the form of non-repayable grants or ' +
                'contributions from the Goverment of Nunavut; or',
                'it is in arrears to the Goverment of Nunavut for overdue taxes under any enactment.'
              ]
            },
            {
              label: 'The BTTC is not available for any business training provided or completed after March 31, 2014.'
            },
            {
              label: 'Complete a separate schedule for each certificate received. Keep a copy of the ' +
              'certificate with your records. Do not submit the certificate with the T2 Corporation Income Tax Return.'
            },
            {
              label: 'File this schedule with your T2 Corporation Income Tax Return.'
            }
          ]
        }
      ],
      category: 'Nunavut Forms'
    },
    sections: [
      {
        'header': 'Calculation of the Nunavut business training tax credit (BTTC)',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Business training tax credit certificate',
            'labelClass': 'bold'
          },
          {
            'label': 'Certificate number',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '110'
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Amount of BTTC indicated on the certificate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '120'
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Calculation of the business training tax credit',
            'labelClass': 'bold'
          },
          {
            'label': 'Business training expenses*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '130'
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Basic BTTC'
          },
          {
            'type': 'table',
            'num': '1001'
          },
          {
            'label': 'Plus',
            'labelClass': 'bold'
          },
          {
            'label': 'For eligible employees who are a beneficiary under the Nunavut Land Claims Agreement'
          },
          {
            'type': 'table',
            'num': '1011'
          },
          {
            'label': 'Business training tax credit (amount D plus amount E)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1031'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Business training tax credit claim (lesser of amount B and amount F)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '160'
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'or, if the corporation is a member of a partnership, determine its share of amount G:'
          },
          {
            'type': 'table',
            'num': '1021'
          },
          {
            'label': 'Enter amount G or H, whichever applies, on line 740** of Schedule 5, Tax Calculation Supplementary - Corporations. If you are filing more than one Schedule 490, add the amount from line G or H, whichever applies, on all the schedules, and enter the total amount on line 740** of Schedule 5.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Business training expenses as defined in subsection 6.3(6) of the <i>Income Tax Act</i> (Nunavut)',
            'labelClass': 'fullLength'
          },
          {
            'label': '** If the corporation is eligible for the small business deduction (SBD) under the federal <i>Income Tax Act</i>, the total amount cannot be more than $10,000. If the corporation is not eligible for the SBD under the federal Act, the total amount cannot be more than $50,000.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  });
})();


