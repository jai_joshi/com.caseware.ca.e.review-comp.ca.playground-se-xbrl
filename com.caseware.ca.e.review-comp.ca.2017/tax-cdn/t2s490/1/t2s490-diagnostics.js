(function() {
  wpw.tax.create.diagnostics('t2s490', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('4900005', common.prereq(common.check(['t2s5.740'], 'isNonZero'),
        common.requireFiled('T2S490')));

    diagUtils.diagnostic('4900015', common.prereq(common.requireFiled('T2S490'), common.check('130', 'isNonZero')));

  });
})();