(function() {

  wpw.tax.create.calcBlocks('t2s490', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('1003', 'ratesNu', '706');
      calcUtils.getGlobalValue('1013', 'ratesNu', '707');

      var amountC = field('130').get();
      field('1002').assign(amountC);
      field('1012').assign(amountC);
      field('140').assign(amountC * field('1003').get() / 100);
      field('150').assign(amountC * field('1013').get() / 100);
      field('1031').assign(field('140').get() + field('150').get());
      field('160').assign(Math.min(field('120').get(), field('1031').get()));
      field('1022').assign(field('160').get());
      field('180').assign(field('1022').get() * field('170').get() / 100);

    });
  })
})();