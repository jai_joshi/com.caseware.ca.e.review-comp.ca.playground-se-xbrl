(function() {

  var tnWidth = '25px';
  var descriptionWidth = '300px';
  var formFieldWidth = '107px';

  function getTableColumns() {
    return [
      {type: 'none', colClass: 'std-input-width'},
      {
        colClass: 'std-input-width',

      },
      {
        colClass: 'std-padding-width',
        type: 'none'
      },
      {
        colClass: 'std-input-width',

      },
      {
        colClass: 'std-padding-width',
        type: 'none'
      },
      {
        type: 'none'
      },
      {
        colClass: 'std-padding-width',
        type: 'none'
      },
      {
        colClass: 'std-input-width',

      },
      {
        colClass: 'std-padding-width',
        type: 'none'
      }
    ]
  }

  wpw.tax.create.tables('t2s490', {
    '1001': {
      infoTable: true,
      fixedRows: true,
      columns: getTableColumns(),
      cells: [
        {
          0: {label: 'Amount C'},
          1: {num: '1002'},
          2: {label: 'x'},
          3: {num: '1003'},
          4: {label: '%='},
          6: {tn: '140'},
          7: {num: '140'},
          8: {label: 'D'}
        }
      ]
    },
    '1011': {
      infoTable: true,
      fixedRows: true,
      columns: getTableColumns(),
      cells: [
        {
          0: {label: 'Amount C'},
          1: {num: '1012'},
          2: {label: 'x'},
          3: {num: '1013'},
          4: {label: '%='},
          6: {tn: '150'},
          7: {num: '150'},
          8: {label: 'E'}
        }
      ]
    },
    '1021': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {type: 'none', colClass: 'std-input-width'},
        {
          colClass: 'std-input-width',

        },
        {
          width: descriptionWidth,
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width',

        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width',

        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      cells: [
        {
          0: {label: 'Amount G'},
          1: {num: '1022'},
          2: {label: 'x percentage of BTTC allocated from a partnership'},
          3: {tn: '170'},
          4: {num: '170'},
          5: {label: '%='},
          7: {tn: '180'},
          8: {num: '180'},
          9: {label: 'H'}
        }
      ]
    }
  })
}());