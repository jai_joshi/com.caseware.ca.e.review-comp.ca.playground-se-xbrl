(function() {
  'use strict';

  wpw.tax.global.formData.rentalsWcSummary = {
      formInfo: {
        abbreviation: 'rentalsWcSummary',
        isRepeatForm: true,
        neededRepeatForms: ['t2rentalworkchart'],
        title: '5 Year Summary for Each Property',
        showCorpInfo: true,
        description: [
          {
            text: 'This form shows the summary of EACH real estate rental property.'
          }
        ],
        category: 'Workcharts'
      },
      sections: [
        {

          rows: [
            {
              type: 'infoField',
              label: 'Address of property',
              num: '101'
            },
            {
              labelClass: 'fullLength'
            },
            {
              type: 'table', num: '2000'}
          ]
        }
      ]
    };
})();
