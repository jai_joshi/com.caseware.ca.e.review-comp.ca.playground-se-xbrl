(function() {

  wpw.tax.create.calcBlocks('rentalsWcSummary', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('201', 'CP', 'tax_end');

      var sequence = calcUtils.form().sequence;
      var repeatForm = form('t2rentalworkchart', sequence);
      if (repeatForm.seq() == sequence) {
        calcUtils.getGlobalValue('205', repeatForm, '8141');
        calcUtils.getGlobalValue('206', repeatForm, '8231');
        calcUtils.getGlobalValue('207', repeatForm, '8299');
        //Non-corporation portion (%)
        if (calcUtils.getGlobalValue(false, repeatForm, '1021') == 1) {
          field('208').assign(0)
        }
        else {
          if (calcUtils.getGlobalValue(false, repeatForm, '1023') !== 0) {
            calcUtils.getGlobalValue('208', repeatForm, '1023');
          }
          else {
            calcUtils.getGlobalValue('208', repeatForm, '1024');
          }
        }

        //Expense section
        calcUtils.getGlobalValue('208', repeatForm, '8520');
        calcUtils.getGlobalValue('209', repeatForm, '8521');
        calcUtils.getGlobalValue('210', repeatForm, '8690');
        calcUtils.getGlobalValue('211', repeatForm, '8710');
        calcUtils.getGlobalValue('212', repeatForm, '8960');
        calcUtils.getGlobalValue('213', repeatForm, '8871');
        calcUtils.getGlobalValue('214', repeatForm, '9281');
        calcUtils.getGlobalValue('215', repeatForm, '8810');
        calcUtils.getGlobalValue('216', repeatForm, '8860');
        calcUtils.getGlobalValue('217', repeatForm, '9180');
        calcUtils.getGlobalValue('218', repeatForm, '9060');
        calcUtils.getGlobalValue('219', repeatForm, '9200');
        calcUtils.getGlobalValue('220', repeatForm, '9220');
        calcUtils.getGlobalValue('220-5', repeatForm, '9000');
        //Total expenses before deductible postion
        calcUtils.sumBucketValues('221', [
          calcUtils.getGlobalValue(false, repeatForm, '9949'),
          calcUtils.getGlobalValue(false, repeatForm, '9000')
        ], true);
        //calcUtils.getGlobalValue('222', repeatForm, ''); //TODO: enter total non-rental income
        calcUtils.getGlobalValue('223', repeatForm, '9368');
        //Calculation section
        calcUtils.getGlobalValue('224', repeatForm, '9369');
        calcUtils.getGlobalValue('225', repeatForm, '9370');
        calcUtils.getGlobalValue('226', repeatForm, '9945');
        calcUtils.getGlobalValue('227', repeatForm, '9944');
        calcUtils.getGlobalValue('228', repeatForm, '9947');
        calcUtils.getGlobalValue('229', repeatForm, '9496');
        calcUtils.getGlobalValue('230', repeatForm, '9948');
        calcUtils.getGlobalValue('231', repeatForm, '9950');
        calcUtils.getGlobalValue('232', repeatForm, '9936');
        calcUtils.getGlobalValue('233', repeatForm, '9937');
        calcUtils.getGlobalValue('234', repeatForm, '9938');
        calcUtils.getGlobalValue('234-5', repeatForm, '9974');
        calcUtils.getGlobalValue('235', repeatForm, '9943');
        calcUtils.getGlobalValue('236', repeatForm, '9946');
      }
    });
  });
})();
