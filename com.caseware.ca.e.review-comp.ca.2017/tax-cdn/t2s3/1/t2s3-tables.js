(function() {

  var dateTimeConditionArr = [
    {
      switchIf: {
        formId: 'cp',
        fieldId: '063',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '066',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '071',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '072',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '076',
        value: '1'
      }
    }
  ];

  wpw.tax.global.tableCalculations.t2s3 = {
    'di_table_1': {
      'num': 'di_table_1',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Amount 2',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '1031'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '1033',
            'init': '115',
            'colClass': 'singleUnderline'
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '1032'
          },
          '8': {
            'label': 'i'
          }
        },
        {
          '2': {
            'type': 'none'
          },
          '4': {
            'num': '1033_D',
            'init': '300'
          },
          '7': {
            'type': 'none'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_2': {
      'num': 'di_table_2',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Amount 3',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '1034'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '1036',
            'init': '1',
            'colClass': 'singleUnderline'
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '1037',
            'cellClass': ' singleUnderline'
          },
          '8': {
            'label': 'j'
          }
        },
        {
          '2': {
            'type': 'none'
          },
          '4': {
            'num': '1036_D',
            'init': '300'
          },
          '7': {
            'type': 'none'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    '541': {
      repeats: ['542', '545'],
      specialComplete: true,
      messageIndex: '2',
      showNumbering: true,
      columns: [
        {
          header: 'Name of payer corporation<br>(from which the corporation received the dividend)',
          tn: '200', "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]},
          indicator: 'A',
          type: 'text'
        },
        {
          header: 'Enter 1 if payer corporation is connected',
          tn: '205',
          indicator: 'B',
          type: 'dropdown',
          showValues: 'before',
          width: '150px',
          init: '0',
          options: [
            {
              option: 'Yes',
              value: '1'
            },
            {
              option: 'No',
              value: '2'
            }
          ]
        },
        {
          header: 'Foreign income',
          type: 'dropdown',
          showValues: 'before',
          init: '2',
          options: [
            {
              value: '1',
              option: 'Yes'
            },
            {
              value: '2',
              option: 'No'
            }
          ],
          width: '70px'
        },
        {
          header: 'Business Number of <b>connected</b> corporation',
          tn: '210',
          indicator: 'C',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        }
      ]
    },
    '542': {
      hasTotals: true,
      specialComplete: true,
      messageIndex: '5',
      keepButtonsSpace: true,
      showNumbering: true,
      columns: [
        {
          header: 'Tax year-end of the payer corporation in which the sections 112/113 and subsection 138(6) dividends in column F were paid',
          width: '200px',
          tn: '220',
          indicator: 'D',
          type: 'date'
        },
        {
          header: 'Non-taxable dividend under section 83',
          tn: '230', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          indicator: 'E',
          totalMessage: ' ',
          total: true,
          totalNum: '600',
          type: 'text'
        },
        {
          header: 'Taxable dividends deductible from taxable income under section 112, subsections 113(2) ' +
          'and 138(6), and paragraphs 113(1)(a), (a.1), (b), or (d)<sup>note 1</sup>',
          tn: '240', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          indicator: 'F',
          total: true,
          totalNum: '240'
        },
        {
          header: 'Eligible dividends (included in column F)',
          indicator: 'F1',
          total: true,
          totalNum: '1241'
        },
        {
          type: 'dropdown',
          indicator: 'F2',
          showValues: 'before',
          init: '1',
          canSort: true,
          num: '242',
          options: [
            {
              value: '1',
              option: 'Taxable dividend deductible under section 112'
            },
            {
              value: '2',
              option: 'Taxable dividend deductible under section 113 (1) and (2)'
            },
            {
              value: '3',
              option: 'Other deductible taxable dividend'
            }
          ]
        },
        {
          header: 'Amount of dividend included in column F that was received <b>before</b> 2016',
          tn: '241', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          indicator: 'G'
        }
      ],
      startTable: '541'
    },
    '545': {
      hasTotals: true,
      specialComplete: true,
      messageIndex: '5',
      keepButtonsSpace: true,
      showNumbering: true,
      columns: [
        {
          header: 'Total taxable dividends paid by <b>connected</b> payer corporation (for tax year in column D)',
          tn: '250', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          indicator: 'H'
        },
        {
          header: 'Dividend refund of the connected payer corporation (for tax year in column D) <sup>note 2</sup>',
          tn: '260', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          indicator: 'I'
        },
        {
          header: 'Dividends (from Column G) received <b>before</b> 2016 <b>multiplied</b> by 33 1/3% <sup>note 3</sup>',
          'disabled': true,
          tn: '270', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          indicator: 'J',
          total: true,
          totalMessage: ' ',
          totalNum: '270'
        },
        {
          header: 'Dividends received after 2015 (column F <b>minus</b> column G) multiplied by 38 1/3% <sup>note 4</sup>',
          'disabled': true,
          tn: '275', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          indicator: 'K',
          total: true,
          // totalMessage: 'Total of column K<br>(enter amount on line b in Part 2)',
          totalNum: '275'
        }
      ],
      startTable: '541'
    },
    // '5500': {
    //   type: 'table',
    //   num: '5500',
    //   fixedRows: true,
    //   infoTable: 'true',
    //   columns: [
    //     {
    //       type: 'none',
    //       width: '335px',
    //       cellClass: 'alignLeft'
    //     },
    //     {
    //       type: 'none',
    //       width: '90px',
    //       cellClass: 'alignRight'
    //     },
    //     {
    //       type: 'none',
    //       width: '150px',
    //       cellClass: 'alignCenter'
    //     },
    //     {
    //       type: 'none',
    //       cellClass: 'alignRight'
    //     }],
    //   cells: [
    //     {
    //       '0': {
    //         'label': '*** For dividends received from connected corporations:'
    //       },
    //       '1': {
    //         'label': 'Part IV Tax ='
    //       },
    //       '2': {
    //         'label': 'Column F x Column H',
    //         cellClass: 'alignCenter singleUnderline'
    //       },
    //       '3': {}
    //     },
    //     {
    //       '0': {},
    //       '1': {},
    //       '2': {
    //         'label': 'Column G',
    //         cellClass: 'alignCenter'
    //       },
    //       '3': {}
    //     }
    //   ]
    // },
    '543': {
      hasTotals: true,
      showNumbering: true,
      columns: [{
        header: 'Name of connected recipient corporation',
        tn: '400', "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]},
        indicator: 'O',
        type: 'text'
      },
        {
          header: 'Business Number',
          num: '410',
          tn: '410',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR'],
          indicator: 'P'
        },
        {
          header: 'Tax year-end of connected recipient corporation in which the dividends in column R were received ',
          tn: '420',
          indicator: 'Q',
          type: 'date',
          dateTimeShowWhen: {
            or: dateTimeConditionArr
          }
        },
        {
          header: 'Taxable dividends paid to connected corporations',
          tn: '430', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          indicator: 'R',
          total: true,
          totalNum: '430',
          'totalMessage': 'Totals'
        },
        {
          header: 'Eligible dividends (included in column R)',
          indicator: 'R1',
          total: true,
          totalMessage: ' '
        }
      ]
    },
    'Part2after': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          width: '210px',
          type: 'none'
        },
        {
          width: '70px',
          'formField': true
        },
        {
          width: '50px',
          'formField': true
        },
        {
          width: '20px',
          type: 'none'
        },
        {
          width: '50px',
          'formField': true
        },
        {
          width: '20px',
          type: 'none'
        },
        {
          width: '200px',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          width: '7px',
          type: 'none'
        },
        {
          width: '220px',
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {label: 'Amount g <b>multiplied</b> by'},
          '1': {num: '1000'},
          '2': {num: '1001'},
          '3': {label: '/'},
          '4': {num: '1002'},
          '5': {label: '%'},
          '7': {num: '1003'},
          '8': {label: 'h'}
        }
      ]
    },
    'Part2before': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          width: '90px',
          'formField': true
        },
        {
          width: '20px',
          type: 'none'
        },
        {
          width: '60px',
          'formField': true
        },
        {
          width: '50px',
          'formField': true
        },
        {
          width: '20px',
          type: 'none'
        },
        {
          width: '50px',
          'formField': true
        },
        {
          width: '20px',
          type: 'none'
        },
        {
          width: '48px',
          type: 'none'
        },
        {
          width: '90px',
          'formField': true
        },
        {
          width: '7px',
          type: 'none'
        },
        {
          width: '351px',
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {num: '1009'},
          '1': {label: '÷'},
          '2': {num: '1010'},
          '3': {num: '1011'},
          '4': {label: '/'},
          '5': {num: '1012'},
          '6': {label: '%'},
          '8': {num: '1013'},
          '9': {label: '1'}
        }
      ]
    },
    '5421': {
      infoTable: true,
      fixedRows: true,
      specialComplete: true,
      messageIndex: '5',
      keepButtonsSpace: true,
      columns: [
        {
          width: '220px',
          type: 'none'
        },
        {
          header: 'Total of column E',
          type: 'none'
        },
        {
          header: 'Total of column F',
          type: 'none'
        },
        {
          type: 'none'
        },
        {
          type: 'none'
        },
        {
          type: 'none'
        }
      ],
      cells: [
        {
          1: {
            label: '(enter amount on line 402 of Schedule 1)'
          },
          2: {
            label: '(include this amount on line 320 of the T2 Return)'
          }
        }
      ]
    },
    '5451': {
      specialComplete: true,
      messageIndex: '5',
      keepButtonsSpace: true,
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none'
        },
        {
          type: 'none'
        },
        {
          header: 'Total of column J',
          type: 'none'
        },
        {
          header: 'Total of column K',
          type: 'none'
        }
      ],
      cells: [
        {
          2: {
            label: '(enter amount on line a in Part 2)'
          },
          3: {
            label: '(enter amount on line b in Part 2)'
          }
        }
      ]
    }
  }
})();
