(function() {
  'use strict';

  wpw.tax.global.formData.t2s3 = {
    'formInfo': {
      'abbreviation': 'T2S3',
      'title': 'Dividends Received, Taxable Dividends Paid and Part IV Tax Calculation',
      formFooterNum: 'T2SCH3 E (16)',
      //'subTitle': '(2004 and later tax years)',
      'schedule': 'SCHEDULE 3',
      'code': 'Code 1601',
      'showCorpInfo': true,
      'description': [
        {
          type: 'list',
          items: [
            {
              label: ' Corporations must use this schedule to report:',
              sublist: ['non-taxable dividends under section 83;',
                'deductible dividends under subsection 138(6);',
                'taxable dividends deductible from income under section 112, subsection 113(2)' +
                ' and paragraphs 113(1)(a),(a.1), (b) or (d); or',
                'taxable dividends paid in the tax year that qualify for a dividend refund (see page 3).'
              ]
            },
            {
              label: 'All legislative references are to the federal <i>Income Tax Act</i>.'
            },
            {
              label: 'The calculations in this schedule apply only to private or subject corporations.'
            },
            {
              label: 'A recipient corporation is <b>connected</b> with a payer corporation at any time in a tax year, ' +
              'if at that time the recipient corporation:',
              sublist: [
                'controls the payer corporation, other than because of a right referred to in paragraph ' +
                '251(5)(b); or',
                'owns more than 10% of the issued share capital (with full voting rights), and shares that have ' +
                'a fair market value of more than 10% of the fair market value of all shares of the payer corporation.'
              ]
            },
            {
              label: 'If you need more space, continue on a separate schedule.'
            },
            {
              label: 'File one completed copy of this schedule with your <i>T2 Corporation Income Tax Return</i>.'
            }
          ]
        }
      ],
      'descriptionHighlighted': true,
      'highlightFieldsets': true,
      category: 'Federal Tax Forms'
    },
    'sections': [
      {
        'header': 'Part 1 - Dividends received in the tax year',
        'rows': [
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'items': [
                  {
                    'label': 'Do <b>not</b> include dividends received from foreign non-affiliates.'
                  },
                  {
                    'label': 'Complete columns B, C, D, H and I <b>only if</b> the payer corporation is <b>connected</b>'
                  }
                ]
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Important instructions to follow if the payer corporation is connected',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'items': [
                  {
                    label: 'If your corporation\'s tax year-end is different than that of the connected payer' +
                    ' corporation, dividends could have been received from more than one tax year of the payer ' +
                    'corporation. If so, use a separate line to provide the information according ' +
                    'to each tax year of the payer corporation.'
                  },
                  {
                    'label': 'When completing Column J and K use the <b>special calculations provided in the notes</b>.'
                  }
                ]
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '541'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '542'
          },
          {
            type: 'table',
            num: '5421'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: '1 If taxable dividends are received, enter the amount in column 240, but if the corporation is' +
            ' not subject to Part IV tax (such as a public corporation other than a subject corporation as defined ' +
            'in subsection 186(3)), enter "0" in column 270 or column 275 as applicable according to the date' +
            ' received. Life insurers are not subject to Part IV tax on subsection 138(6) dividends.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '545'
          },
          {
            type: 'table',
            num: '5451'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: '2 If the connected payer corporation’s tax year ends after the corporation’s balance-due day' +
            ' for the tax year (two or three months, as applicable), you have to estimate the payer’s dividend refund' +
            ' when you calculate the corporation’s Part IV tax payable.',
            'labelClass': 'fullLength'
          },
          {
            label: '3 For dividends received <b>before</b> 2016 from <b>connected</b> corporations,' +
            ' Part IV tax on dividends is' +
            ' equal to: column G <b>multiplied</b> by column I <b>divided</b> by column H.',
          },
          {
            label: '4 For dividends received <b>after</b> 2015 from connected corporations, Part IV tax on dividends' +
            ' is equal to: column I <b>divided</b> by column H <b>multiplied</b> by the result' +
            ' of column F <b>minus</b> column G.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of Part IV tax payable',
        'rows': [
          {
            'label': 'Part IV tax on dividends received <b>before</b> 2016, before deductions (Total of column J in part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '306'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'Part IV tax on dividends received <b>after</b> 2015, before deductions (Total of column K in part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '307'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': 'Part IV tax before deductions (amount a <b>plus</b> amount b)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '310'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '308'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Part IV.I tax payable on dividends subject to Part IV tax (from line 360 of Schedule 43)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '320',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '320'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount L <b>minus</b> line 320)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '321'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Current-year non-capital loss claimed to reduce Part IV tax',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '330',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '330'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'Non-capital losses from previous years claimed to reduce Part IV tax',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '335',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '335'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Current-year farm loss claimed to reduce Part IV tax',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '340',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '340'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': 'Farm losses from previous years claimed to reduce Part IV tax',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '345',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '345'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'label': 'Total losses applied against Part IV tax (total of amounts c to f)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '347'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'g'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If your tax year begins after December 31, 2015:',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': 'Part2after'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'If your tax year begins before January 1, 2016:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Amount b or M whichever is less',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': 'Part2before'
          },
          {
            'label': 'Amount 1 or g, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1020'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '2'
                }
              },
              null
            ]
          },
          {
            'label': 'Amount g <b>minus</b> amount 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1030',
                  'disabled': true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '3'
                }
              },
              null
            ]
          },
          {
            'type': 'table',
            'num': 'di_table_1'
          },
          {
            'type': 'table',
            'num': 'di_table_2'
          },
          {
            'label': 'Subtotal (amount i <b>plus</b> amount j',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1035'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'k'
                }
              }
            ]
          },
          {
            'label': 'Amount h or amount k, whichever applies depending on your tax year start date',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1038'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'label': 'Part IV tax payable (amount M <b>minus</b> amount N, if negative enter "0")',
            'labelClass': 'formLabelTabbed',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '360',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '360'
                }
              }
            ]
          },
          {
            'label': '(enter amount on line 712 of the T2 return)',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Taxable dividends paid in the tax year that qualify for a dividend refund',
        'rows': [
          {
            label: 'If your corporation\'s tax year-end is different than that of the connected recipient corporation,' +
            ' your corporation could have paid dividends in more than one tax year of the recipient corporation.' +
            ' If so, use a separate line to provide the information according to each tax year of the' +
            ' recipient corporation.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '543'
          },
          {
            'label': 'Total taxable dividends paid in the tax year to other than connected corporations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '450',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '450'
                }
              }
            ]
          },
          {
            'label': 'Eligible dividends included in line 450',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '455'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Total taxable dividends paid in the tax year that qualify for a dividend refund</b>' +
            '<br>(total of column R <b>plus</b> line 450)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '460',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '460'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 4 - Total dividends paid in the tax year',
        'rows': [
          {
            'label': 'Complete this part <b>if</b> the total taxable dividends paid in the tax year that qualify for a dividend refund (line 460) is different from the total dividends paid  in the tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total taxable dividends paid in the tax year for the purpose of a dividend refund (from above)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '490'
                }
              }
            ]
          },
          {
            'label': 'Other dividends paid in the tax year (total of 510 to 540)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '491'
                }
              }
            ]
          },
          {
            'label': 'Total dividends paid in the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '500',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '500'
                }
              }
            ]
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Dividends paid out of capital dividend account',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '510',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '510'
                }
              },
              null
            ]
          },
          {
            'label': 'Capital gains dividends',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '520',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '520'
                }
              },
              null
            ]
          },
          {
            'label': 'Dividends paid on shares described in subsection 129(1.2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '530',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '530'
                }
              },
              null
            ]
          },
          {
            'label': 'Taxable dividends paid to a controlling corporation that was bankrupt at any time in the year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '540',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '540'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of lines 510 to 540)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '550'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '551'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'S'
          },
          {
            'label': 'Total taxable dividends paid in the tax year that qualify for a dividend refund (line 500 <b>minus</b> amount S)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '552'
                }
              }
            ],
            'indicator': 'T'
          }
        ]
      }
    ]
  };
})();
