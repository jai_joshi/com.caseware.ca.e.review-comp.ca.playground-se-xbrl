(function() {

  function ifEntryThenColumn(entryLine, tableNum, colIndex) {
    // If there is an entry at `entryLine` then there must be an entry in `colIndex` of `tableNum`
    return function(tools) {
      if (tools.field(entryLine).isNonZero()) {
        var col = tools.field(tableNum).getCol(colIndex);
        return tools.markIfNot(col, 'isNonZero');
      }
      return true;
    }
  }

  wpw.tax.create.diagnostics('t2s3', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S3'), forEach.row('541', forEach.bnCheckCol(3, true))));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S3'), forEach.row('543', forEach.bnCheckCol(1, true))));

    diagUtils.diagnostic('0030001', common.prereq(
        common.check(['t2s1.402', 't2j.320', 't2j.784'], 'isNonZero'), common.requireFiled('T2S3')));

    diagUtils.diagnostic('0030002', common.prereq(common.requireFiled('T2S3'), ifEntryThenColumn('t2s1.402', '542', 1)));

    diagUtils.diagnostic('0030003', common.prereq(common.requireFiled('T2S3'), ifEntryThenColumn('t2j.320', '542', 2)));

    diagUtils.diagnostic('0030004', common.prereq(common.requireFiled('T2S3'), ifEntryThenColumn('t2j.712', '542', 2)));

    diagUtils.diagnostic('0030005', common.prereq(common.and(
        common.requireFiled('T2S3'),
        common.check('t2j.784', 'isNonZero')),
        function(tools) {
          return tools.field('450').require('isNonZero') ||
              tools.requireOne(tools.field('543').getCol(3), 'isNonZero')
        }));

    diagUtils.diagnostic('0030006', common.prereq(common.requireFiled('T2S3'), function(tools) {
      var table = tools.mergeTables(tools.field('541'), tools.field('542'));
      return tools.checkAll(table.getRows(), function(row) {
        if (row[5].isNonZero())
          return tools.requireAll([row[0], row[3]], 'isNonZero');
        else return true;
      });
    }));

    diagUtils.diagnostic('0030007', common.prereq(common.requireFiled('T2S3'), function(tools) {
      var table = tools.mergeTables(tools.field('541'), tools.field('542'));
      table = tools.mergeTables(table, tools.field('545'));
      return tools.checkAll(table.getRows(), function(row) {
        if (row[1].isYes() && row[6].isNonZero())
          return tools.requireAll([row[0], row[3], row[4], row[10]], 'isNonZero');
        else return true;
      });
    }));

    diagUtils.diagnostic('0030008', common.prereq(common.requireFiled('T2S3'), function(tools) {
      var table = tools.mergeTables(tools.field('541'), tools.field('542'));
      return tools.checkAll(table.getRows(), function(row) {
        if (row[1].isNo() && row[6].isNonZero())
          return row[0].require('isNonZero');
        else return true;
      });
    }));

    diagUtils.diagnostic('0030009', common.prereq(common.requireFiled('T2S3'), function(tools) {
      var table = tools.field('543');
      return tools.checkAll(table.getRows(), function(row) {
        if (row[3].isNonZero())
          return tools.requireAll([row[0], row[1], row[2]], 'isNonZero');
        else return true;
      });
    }));

    diagUtils.diagnostic('0030010', common.prereq(common.and(
        common.check('460', 'isNonZero'),
        common.requireFiled('T2S3')), function(tools) {
      return tools.field('450').require('isNonZero') ||
          tools.requireOne(tools.field('543').getCol(3), 'isNonZero')
    }));

    diagUtils.diagnostic('003.455', common.prereq(
        common.requireFiled('T2S3'),
        function(tools) {
          return tools.field('455').require(function() {
            return this.get() <= tools.field('t2s53.590').get();
          })
        }));

    diagUtils.diagnostic('054.540', common.prereq(common.and(
         common.check('t2s3.460', 'isNonZero'),
        common.requireFiled('T2S54')),
        function(tools) {
          var table = tools.mergeTables(tools.form('t2s54', 1).field('199'), tools.form('t2s54', 1).field('205'));
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[4], row[8], tools.form('t2s54', 1).field('540'), tools.form('t2s54', 1).field('352')], 'isZero'))
              return tools.requireAll([row[4], row[8], tools.form('t2s54', 1).field('540'), tools.form('t2s54', 1).field('352')], 'isNonZero');
            else return true;
          });
        }));
  });
})();

