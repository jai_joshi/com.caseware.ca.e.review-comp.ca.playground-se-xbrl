wpw.tax.create.importCalcs('T2S3', function (importTools) {
  function getImportObj(taxPrepIds) {
    var output = {};
    taxPrepIds = wpw.tax.utilities.ensureArray(taxPrepIds, true);
    taxPrepIds.forEach(function (taxPrepId, index) {
      importTools.intercept(taxPrepId, function (importObj) {
        if (!output[importObj.rowIndex]) {
          output[importObj.rowIndex] = {};
        }
        if (angular.isDefined(importObj.rowIndex)) {
          output[importObj.rowIndex][index] = importObj;
        }
      });
    });
    return output;
  }

  var s3Obj = getImportObj(['FDDIV.SLIPA[1].TtadivA1', 'FDDIV.SLIPA[1].TtadivA3']);

  importTools.after(function () {
    function isRowEmpty(rowObj) {
      var empty = true;
      Object.keys(rowObj).forEach(function (key) {
        if (key === '2') {
          //ignore the connected dropdown
          return;
        }
        if (angular.isDefined(rowObj[key]) && rowObj[key] !== 0 && rowObj[key] !== null) {
          empty = false;
        }
      });
      return empty;
    }

    var rowIndex = 0;
    if (angular.isDefined(s3Obj[0][0])) {
      if (!s3Obj[0][0]['value']) {
        s3Obj[0][1]['value'] = ' '
      }
      importTools.form('T2S3').setValue('541', s3Obj[0][0]['value'], 0, rowIndex);
      importTools.form('T2S3').setValue('541', s3Obj[0][1]['value'], 1, rowIndex);
    }
    else if (s3Obj[0][1]['value'] == 2) {
      if (s3Obj[0][2] == undefined) {
        s3Obj[0][1]['value'] = ' ';
      }
      importTools.form('T2S3').setValue('541', s3Obj[0][1]['value'], 1, rowIndex);
    }

    // Object.keys(s3Obj[row]).forEach(function(col) {
    //   //table 541
    //   // if (col == 0) {
    //   //   if (!s3Obj[row][col]['value']) {
    //   //     s3Obj[row][col + 2]['value'] = ' ';
    //   //   }
    //   //   else {
    //   //     s3Obj[row][col + 2]['value'] = ['value']
    //   //   }
    //   //
    //   // }
    //   // // if (col < 4) {
    //   //   if (col == 1) {
    //   //     if (s3Obj[row][col]['value'] == 'Y') {
    //   //       s3Obj[row][col]['value'] = '1';
    //   //     }
    //   //     else {
    //   //       s3Obj[row][col]['value'] = ' ';
    //   //     }
    //   //     importTools.form('T2S3').setValue('541', s3Obj[row][col]['value'], 1, rowIndex);
    //   //   }
    //   //   else if (col == 2) {
    //   //     importTools.form('T2S3').setValue('541', s3Obj[row][col]['value'], 2, rowIndex);
    //   //   }
    //   //   else {
    //   //     importTools.form('T2S3').setValue('541', s3Obj[row][col]['value'], col, rowIndex);
    //   //   }
    //   // }
    //   //table 545
    //   // else if (col > 9) {
    //   //   importTools.form('T2S3').setValue('545', s3Obj[row][col]['value'], col - 10, rowIndex);
    //   // }
    //   // //table 542
    //   // else {
    //   //   importTools.form('T2S3').setValue('542', s3Obj[row][col]['value'], col - 4, rowIndex);
    //   // }
    // });

  });
});


