(function() {

  wpw.tax.create.calcBlocks('t2s3', function(calcUtils) {
    calcUtils.calc(function(calcUtils) {
      var num = '1031';
      var num2 = '1032';
      var midnum = '1033';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / calcUtils.field(midnum + '_D').get();
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '1034';
      var num2 = '1037';
      var midnum = '1036';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / calcUtils.field(midnum + '_D').get();
      calcUtils.field(num2).assign(product);
    });
    //table541 calc
    calcUtils.calc(function(calcUtils, field) {
      field('541').getRows().forEach(function(row) {
        row[3].disabled(row[1].get() == 2);
      });
    });

    //table542 calc
    calcUtils.calc(function(calcUtils, field) {
      var rate2015 = 1 / 100 * (field('ratesFed.156-0').get() + (field('ratesFed.156-1').get() / field('ratesFed.156-2').get()));
      var rate2016 = 1 / 100 * (field('ratesFed.157-0').get() + (field('ratesFed.157-1').get() / field('ratesFed.157-2').get()));

      field('545').getRows().forEach(function(row, rowIndex) {
        var table542Row = field('542').getRow(rowIndex);
        row[2].assign(table542Row[5].get() * rate2016);
        row[3].assign((table542Row[2].get() - table542Row[5].get()) * rate2015)
      });
    });

    //part 2
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.equals('306', '270');
      calcUtils.equals('307', '275');
      calcUtils.sumBucketValues('310', ['306', '307']);

      calcUtils.equals('308', '310');
      calcUtils.getGlobalValue('320', 't2s43', '360');
      calcUtils.subtract('321', '308', '320');
      field('330').assign(field('t2s4.5020').cell(20, 12).get());
      field('335').assign(field('t2s4.135').get() - field('330').get());
      field('340').assign(field('t2s4.5040').cell(20, 12).get());
      field('345').assign(field('t2s4.335').get() - field('340').get());
      calcUtils.sumBucketValues('347', ['330', '335', '340', '345']);
      // calcUtils.equals('347', '346');

      calcUtils.getGlobalValue('1000', 'ratesFed', '156-0');
      calcUtils.getGlobalValue('1001', 'ratesFed', '156-1');
      calcUtils.getGlobalValue('1002', 'ratesFed', '156-2');
      field('1003').assign(field('347').get() * (field('1000').get() + (field('1001').get() / field('1002').get())) / 100);

      field('1009').assign(Math.min(field('307').get(), field('321').get()));
      calcUtils.getGlobalValue('1010', 'ratesFed', '157-0');
      calcUtils.getGlobalValue('1011', 'ratesFed', '157-1');
      calcUtils.getGlobalValue('1012', 'ratesFed', '157-2');
      field('1013').assign(field('1009').get() / (field('1010').get() + (field('1011').get() / field('1012').get())) / 100);

      field('1020').assign(Math.min(field('1013').get(), field('347').get()));
      if (field('347').get() && field('1020').get()) {
        calcUtils.subtract('1030', '347', '1020');
      }
      calcUtils.equals('1031', '1020');
      calcUtils.equals('1034', '1030');
      calcUtils.sumBucketValues('1035', ['1032', '1037']);
      if (calcUtils.dateCompare.greaterThan(field('CP.tax_start').get(), wpw.tax.date(2015, 12, 31))) {
        calcUtils.equals('1038', '1003');
      }
      else {
        calcUtils.equals('1038', '1035');
      }
      calcUtils.subtract('360', '321', '1038');
    });

    //part 3
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.sumBucketValues(460, [430, 450]);
    });
    //part 4
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.equals('490', '460');
      calcUtils.equals('491', '551');
      calcUtils.subtract('500', '490', '491');
      calcUtils.sumBucketValues('550', ['510', '520', '530', '540']);
      calcUtils.equals(551, 550);
      calcUtils.subtract(552, 500, 551);
    });
  });
})
();
