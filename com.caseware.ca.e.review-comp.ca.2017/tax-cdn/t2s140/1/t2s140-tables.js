(function() {
  wpw.tax.global.tableCalculations.t2s140 = {
    "100": {
      "type": "gifiWorkchart",
      "gifiCodes": "otherComprehensiveIncome",
      "fixedRows": true,
      "keepButtonsSpace": true,
      "range": [9970, 9999],
      "fixedGifiRows": [
        {
          "code": "9970",
          "description": "Net income/loss before taxes and extraordinary items"
        },
        {
          "code": "9975",
          "description": "Extraordinary item(s)"
        },
        {
          "code": "9976",
          "description": "Legal settlements"
        },
        {
          "code": "9980",
          "description": "Unrealized gains/losses"
        },
        {
          "code": "9985",
          "description": "Unusual items"
        },
        {
          "code": "9990",
          "description": "Current income taxes"
        },
        {
          "code": "9995",
          "description": "Future (deferred) income tax provision"
        },
        {
          "code": "9998",
          "description": "Total other comprehensive income"
        }],
      "postTotalRows": [{
        "code": "9999",
        "description": "Net income/loss after taxes and extraordinary items"
      }],
      "numberOfYearsToShow": 5
    }
  }
})();
