(function() {
  'use strict';

  var gifiCodes = new wpw.tax.codes.IncomeStatement();
  wpw.tax.global.formData.t2s140 = {
    formInfo: {
      abbreviation: 'T2S140',
      title: 'Summary Statement of the Income Statements',
      schedule: 'Schedule 140',
      neededRepeatForms: ['T2S125'],
      category: 'GIFI',
      disclaimerShowWhen: {fieldId: 'cp.184', check: 'isYes'},
      disclaimerTextField: {formId: 'cp', fieldId: '183'},
      description: [
        {
          'type': 'list',
          'items': [
            'Use this section of the schedule <b>only</b> to report the summary statement when you are submitting supplementary income statements. ',
            'For information on supplementary income statements, see Guide RC4088, Appendix B – <i>Reporting multiple lines of business.</i>',
            'If you need more space, attach additional schedules.'
          ]
        }
      ]
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {label: 'Summary Statement', labelClass: 'fullLength bold center'},
          {
            type: 'gifiWorkchart', gifiCodes: 'otherComprehensiveIncome',
            num: '100', fixedRows: true, keepButtonsSpace: true, range: [9970, 9999],
            fixedGifiRows: [
              {code: '9970', description: 'Net income/loss before taxes and extraordinary items'},
              {code: '9975', description: 'Extraordinary item(s)'},
              {code: '9976', description: 'Legal settlements'},
              {code: '9980', description: 'Unrealized gains/losses'},
              {code: '9985', description: 'Unusual items'},
              {code: '9990', description: 'Current income taxes'},
              {code: '9995', description: 'Future (deferred) income tax provision'},
              {code: '9998', description: 'Total other comprehensive income'}
            ],
            postTotalRows: [
              {code: '9999', description: 'Net income/loss after taxes and extraordinary items'}
            ]
          },
          {labelClass: 'fullLength'},
          {
            label: 'Note: Schedule 140 is used to summarize information if multiple' +
            ' Schedule 125 are filed. If there is only one S125, then the information' +
            ' on this form will be e-filed with S125 ',
            labelClass: 'fullLength tabbed'
          }
        ]
      }
    ]
  };
})();
