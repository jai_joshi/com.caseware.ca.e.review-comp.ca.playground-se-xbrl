(function() {
  function calculateNetIncome(calcUtils, colIndex) {
    var valueIndex = colIndex - 2;

    var total = 0;
    var col = calcUtils.field('100').getCol(colIndex);

    total += col[0].getNumber();
    total -= col[1].getNumber();
    total -= col[2].getNumber();
    total += col[3].getNumber();
    total -= col[4].getNumber();
    total -= col[5].getNumber();
    total -= col[6].getNumber();
    total += col[7].getNumber();
    calcUtils.field('9999-' + valueIndex).assign(total);
    calcUtils.field('9999').setKey(valueIndex, total);
  }

  wpw.tax.create.calcBlocks('t2s140', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      for (var yearIndex = 0; yearIndex < 5; yearIndex++) {
        var sum = calcUtils.sumRepeatGifiValue('t2s125', '550', '9970', yearIndex);
        calcUtils.field('100').cell(0, yearIndex + 2).assign(sum);
        calcUtils.field('100').cell(0, yearIndex + 2).source(field('t2s125.9970'));
        calcUtils.field('9970').setKey(yearIndex, sum);

        sum = calcUtils.sumRepeatGifiValue('t2s125', '600', '9998', yearIndex);
        calcUtils.field('100').cell(7, yearIndex + 2).assign(sum);
        calcUtils.field('100').cell(7, yearIndex + 2).source(field('t2s125.9998'));
        calcUtils.field('9998').setKey(yearIndex, sum);
      }

      for (var colIndex = 2; colIndex < 7; colIndex++) {
        calculateNetIncome(calcUtils, colIndex);
      }
    });
  });
})();
