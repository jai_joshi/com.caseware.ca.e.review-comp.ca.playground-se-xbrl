(function() {
  wpw.tax.create.diagnostics('t2s140', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    //Line 9999 is always displayed on s140
    diagUtils.diagnostic('1250002', common.prereq(common.and(
        common.requireFiled('T2S125'),
        function() {
          return wpw.tax.form.allRepeatForms('t2s125').length < 2;
        }), common.check('9999', 'isFilled')));

    //1250003: If more than one Schedule 125 is filed, no amounts should be entered at any of GIFI line codes 9975 to 9999.
    //GIFI line codes 9975 to 9999 are exclusively displayed on s140 in our application thus these lines will never have values displayed in s125 making this diagnostic redundant
    //when creating COR file, s140 is attached to s125 if there is only one s125 form
    //if more than one s125 form are created, s140 will be reported as a separate form
    diagUtils.diagnostic('1250003', common.prereq(common.and(
        common.requireFiled('T2S125'),
        function() {
          return wpw.tax.form.allRepeatForms('t2s125').length > 1;
        }), function(tools) {
      return tools.requireAll(tools.list(['9975', '9976', '9980', '9985', '9990', '9995', '9998', '9999']), function() {
        return !angular.isUndefined(this.getKey(0));
      })
    }));

    diagUtils.diagnostic('1400001', common.prereq(
        function() {
          return wpw.tax.form.allRepeatForms('t2s125').length > 1;
        }, function(tools) {
      return tools.field('9970').require(function() {
        return !angular.isUndefined(this.getKey(0));
      });
    }));

    diagUtils.diagnostic('1400002', common.prereq(common.and(
        common.requireFiled('T2S140'),
        function() {
          return wpw.tax.form.allRepeatForms('t2s125').length > 1;
        }), function(tools) {
      return tools.field('9999').require(function() {
        return !angular.isUndefined(this.getKey(0));
      })
    }));

    //1401015: When there is only one Schedule 125 filed, Schedule 140 must not be filed.
    //This diagnostic is redundant as the Cor file creation handles situations with multiple/single s125's
/*    diagUtils.diagnostic('1401015', common.prereq(function() {
      return wpw.tax.form.allRepeatForms('t2s125').length < 2;
    }, common.requireNotFiled('t2s140')));*/

  });
})();
