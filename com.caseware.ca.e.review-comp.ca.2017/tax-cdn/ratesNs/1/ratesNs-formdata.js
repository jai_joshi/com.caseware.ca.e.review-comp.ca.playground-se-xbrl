(function () {
  'use strict';

  wpw.tax.global.formData.ratesNs = {
    formInfo: {
      abbreviation: 'ratesNs',
      title: 'Nova Scotia Table of Rates and Values',
      showCorpInfo: true,
      category: 'Rates Tables',
      description: [
        { type: 'heading', text: 'Nova Scotia' }
      ]
    },
    sections: [
      {
        'header': 'Schedule 340 - Nova Scotia Research and Development Tax Credit',
        'rows': [
          {
            'label': '<b>Part 1 - Calculation of refundable Nova Scotia R&D tax credit</b>'
          },
          {
            'label': 'Rate for total eligible expenditures for R&D in the taxation year',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '100'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 341 - Nova Scotia Corporate Tax Reduction for New Small Businesses',
        'rows': [
          {
            'label': 'Rate for amount B, before January 1, 2013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for amount B, after December 31, 2012, and before January 1, 2014',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '201',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for amount B, after December 31, 2013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '202',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'num': '302',
            'num2': '303'
          },
          {
            'label': 'Part 2 - Calculation of income from active business when there is partnership income',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Rate for amount K1',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '304'
                }
              },
              {
                'input': {
                  'num': '305'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Rate for amount K2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '306'
                }
              },
              {
                'input': {
                  'num': '307'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Part 3 - Calculation of Nova Scotia tax before credits and of Nova Scotia offshore tax',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Rate for before January 1, 2013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '308',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for after December 31, 2012, and before January 1, 2014',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '309',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for after December 31, 2013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '310',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for amount BB',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '311'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 341 - Nova Scotia Corporate Tax Reduction for New Small Businesses',
        'rows': [
          {
            "label": "Part 2 – Calculation of total credit available and credit available for carryforward",
            "labelClass": 'fullLength'
          },
          {
            "label": "Acquisitions before January 1, 2001, from amount A above",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true,
              "paddingRight": false
            },
            "columns": [
              {
                "input": {
                  "num": "500"
                }
              }
            ],
            "indicator": "%"
          },
          {
            "label": "Acquisitions after December 31, 2000, from amount A above",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true,
              "paddingRight": false
            },
            "columns": [
              {
                "input": {
                  "num": "501"
                }
              }
            ],
            "indicator": "%"
          }
        ]
      },
      {
        'header': 'Schedule 346 - Nova Scotia Corporation Tax Calculation',
        'rows': [
          {
            'label': 'Lower rate from 2014 to present',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '400'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Higher rate from 2014 to present',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '401'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      }
    ]
  };
})();
