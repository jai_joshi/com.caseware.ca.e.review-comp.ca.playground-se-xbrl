(function () {

  wpw.tax.create.calcBlocks('ratesNs', function (calcUtils) {
    calcUtils.calc(function (calcUtils, field) {
      field('100').assign(15);
      field('200').assign(4.0);
      field('201').assign(3.5);
      field('202').assign(3.0);
      field('300').assign(400000);
      field('301').assign(500000);
      field('302').assign(350000);
      field('303').assign(500000);

      field('304').assign(400000);
      field('305').assign(500000);
      field('306').assign(350000);
      field('307').assign(500000);

      field('308').assign(4.0);
      field('309').assign(3.5);
      field('310').assign(3.0);

      field('311').assign(16);

      field('400').assign(3);
      field('401').assign(16);

      field('500').assign(30);
      field('501').assign(15);
    });
  });
})();
