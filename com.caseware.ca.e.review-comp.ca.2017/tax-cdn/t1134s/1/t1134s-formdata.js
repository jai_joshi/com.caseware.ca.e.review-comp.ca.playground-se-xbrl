(function() {
  'use strict';
  var addCanada = wpw.utilities.clone(wpw.tax.codes.countryCodes);
  addCanada['CAN'] = {value: 'CAN Canada'};
  var countryCodesAddCanada = new wpw.tax.actions.codeToDropdownOptions(addCanada, 'value', 'value');

  wpw.tax.global.formData.t1134s = {
    formInfo: {
      abbreviation: 't1134s',
      title: 'Foreign Income Verification Statement Supplement',
      subtitle: 't1134s Supplement',
      printTitle: 'T1134 Supplement',
      hideHeaderBox: true,
      hideProtectedB: true,
      repeatFormData:{
        titleNum: '112'
      },
      // printNum: 'T1134S',
      isRepeatForm: true,
      description: [
        {text: ''}
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            'label': 'Complete a separate supplement for each foreign affiliate and/or controlled foreign affiliate. (see attached instructions)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'dropdown',
            'labelWidth': '70%',
            'num': '100',
            'options': [
              {
                'option': '',
                'value': ''
              },
              {
                'option': 'New',
                'value': '1'
              },
              {
                'option': 'Unmodified',
                'value': '2'
              },
              {
                'option': 'Amended',
                'value': '3'
              },
              {
                'option': 'Delete (Close)',
                'value': '4'
              }
            ],
            'label': 'What type of supplement is this?'
          },
          {
            'type': 'infoField',
            'inputType': 'dropdown',
            'labelWidth': '70%',
            'showWhen': {
              'fieldId': '100',
              'compare': {
                'is': '4'
              }
            },
            'num': '101',
            'options': [
              {
                'option': '',
                'value': ''
              },
              {
                'option': 'Duplicate',
                'value': '1'
              },
              {
                'option': 'Filed in Error',
                'value': '2'
              },
              {
                'option': 'Other',
                'value': '3'
              }
            ],
            'label': 'What is the reason for deleting this supplement?'
          },
          {
            'type': 'infoField',
            'labelWidth': '70%',
            'width': '16%',
            'num': '102',
            'label': 'Sequence Number:'
          }
        ]
      },
      {
        header: 'Part II – Foreign affiliate information',
        rows: [
          {
            label: 'Section 1 – Reporting entity information',
            labelClass: 'bold'
          },
          {
            type: 'table',
            num: '1130'
          },
          {
            labelClass: 'fullLength'
          },
          {
            type: 'infoField', inputType: 'dateRange',
            label: 'For what taxation year are you filing this form?',
            'label1': 'Tax year-start', num: '110', disabled: true,
            source1: 't1134-110',
            'label2': 'Tax year-end', 'num2': '111', source2: 't1134-111'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            labelClass: 'fullLength'
          },
          {
            label: 'Section 2 – Foreign affiliate information',
            labelClass: 'bold'
          },
          {
            label: 'Where the foreign affiliate has more than one tax year-ending in the reporting entity\'s tax year,'
            + ' report the required information for the second and subsequent tax year(s)' +
            ' of the foreign affiliate in a separate supplement.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'A. Identification of foreign affiliate',
        rows: [
          {
            label: 'Name of foreign affiliate: ',
            type: 'infoField',
            num: '112',
            inputClass: 'std-input-col-width-3'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Address of head office (Check box to use same address as T1134 Summary)',
            type: 'infoField',
            inputType: 'singleCheckbox',
            num: '333'
          },
          {
            type: 'infoField',
            num: '104'
          },
          {
            type: 'splitTable', fieldAlignRight: true,
            side1: [
              {type: 'horizontalLine'},
              {
                'type': 'infoField',
                'inputType': 'fixedSizeBox',
                'boxes': 4,
                num: '113',
                label: 'Year in which the corporation became a foreign affiliate of the reporting entity',
                labelWidth: '70%'
              },
              {type: 'horizontalLine'}
            ],
            side2: [
              {type: 'horizontalLine'},
              {
                type: 'infoField',
                inputType: 'radio',
                canClear: true,
                label: 'Did the corporation cease to be a foreign affiliate of the reporting entity in the year ?',
                labelWidth: '65%',
                num: '114'
              },
              {type: 'horizontalLine'}
            ]
          },
          {
            label: 'Specify the principal activity(ies) of the foreign affiliate using the appropriate ' +
            'North American Industrial Classification System (NAICS) code(s).' +
            '(see attached instructions for NAICS codes).',
            labelClass: 'fullLength'
          },
          {
            type: 'table',
            num: '1113'
          },
          {type: 'horizontalLine'},

          {
            label: 'Specify the countries or jurisdictions in which the foreign affiliate' +
            ' carries on a business or other income earning activity.' +
            ' Enter the appropriate country code(s). (see attached instructions for country codes).',
            labelClass: 'fullLength'
          },
          {
            type: 'table',
            num: '1114'
          },
          {type: 'horizontalLine'},
          {
            label: 'Country or jurisdiction of residence of the foreign affiliate.' +
            ' Enter the appropriate country code (see attached instructions).',
            num: '123',
            type: 'infoField',
            inputType: 'dropdown',
            options: countryCodesAddCanada
          },
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'Is this the first time that the reporting entity has filed Form T1134 for this foreign affiliate?',
            labelWidth: '70%',
            num: '124'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'Is the foreign affiliate a controlled foreign affiliate as defined in subsection 95(1)? ',
            labelWidth: '70%',
            num: '125'
          },
          {labelClass: 'fullLength'}
        ]
      },
      {
        header: 'B. Capital stock of foreign affiliate',
        rows: [
          {
            label: '(i) Total book cost of shares of the foreign affiliate\'s capital stock owned by the reporting entity as of the end of reporting entity\'s taxation year:'
          },
          {
            type: 'table',
            num: '1050'
          },

          {labelClass: 'fullLength'},
          {
            label: '(ii) Total book cost of shares of the foreign affiliate\'s capital stock at the end of reporting entity\'s taxation year' +
            ' owned by a controlled foreign affiliate of the reporting entity or other person related to the reporting entity:'
          },
          {
            type: 'table',
            num: '1060'
          }
        ]
      },
      {
        forceBreakAfter: true,
        header: 'C. Other information of foreign affiliate',
        rows: [
          {
            'label': '(i) What was the reporting entity\'s equity percentage in the foreign affiliate at the beginning of the reporting entity\'s taxation year?',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '129',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '((ii) What was the reporting entity\'s equity percentage in the foreign affiliate at the end of the reporting entity\'s taxation year?',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '(iii) If the Act were read without paragraph 95(2.2)(a), would the reporting entity have a qualifying interest in the foreign affiliate:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            canClear: true,
            'label': 'a) At the beginning of the reporting entity\'s taxation year?',
            'num': '131'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            canClear: true,
            'label': 'b) At the end of the reporting entity\'s taxation year? ',
            'num': '132'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '(iv) Specify the gross amount of the debt (state in Canadian dollars or the elected functional currency-see attached instructions):'
          },
          {
            'label': 'a) the foreign affiliate owed to the reporting entity at the end of the reporting entity\'s taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '133',
                  'decimals': 0
                }
              }
            ]
          },
          {
            'label': 'b) the reporting entity owed to the foreign affiliate at the end of the reporting entity\'s taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '134',
                  'decimals': 0
                }
              }
            ]
          }
        ]
      },
      {
        header: 'Section 3 – Financial information of the foreign affiliate',
        rows: [
          {
            'type': 'infoField',
            'inputType': 'dateRange',
            'label': 'Give the taxation year of the foreign affiliate for which the information on this return is reported:',
            'labelClass': 'fullLength',
            'label1': 'From',
            'num': '135',
            'disabled': false,
            'label2': 'To',
            'num2': '136'
          },
          {
            'label': 'For each taxation year of the foreign affiliate ending in the reporting entity\'s taxation year,provide the following information for the affiliate:',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            canClear: true,
            'label': '- Unconsolidated financial statements (including the notes to the financial statements) or,if unavailable, the financial information that is available to you as a shareholder ',
            'labelWidth': '85%',
            'num': '137'
          },
          {
            showWhen: {
              fieldId: '137',
              compare: {is: 1}
            },
            'labelClass': 'fullLength'
          },
          {
            showWhen: {
              fieldId: '137',
              compare: {is: 1}
            },
            'label': 'Please send <b>all related financial statements by paper copy</b> to the following address:<br><br>' +
            'Winnipeg Taxation Centre<br>' +
            'Data Assessment & Evaluation Programs<br>' +
            'Validation & Verification Section<br>' +
            'Foreign Reporting Returns<br>' +
            '66 Stapon Road<br>' +
            'Winnipeg MB  R3C 3M2',
            labelClass: 'fullLength alignCenter'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1070'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1080'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            label: 'Section 4 – Surplus accounts',
            labelClass: 'bold'
          }
        ]
      },
      {
        header: 'A. Surplus accounts of foreign affiliates',
        rows: [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            canClear: true,
            'label': '1. Did the reporting entity, at any time in the taxation year,receive a dividend on a share of the capital stock of the foreign affiliate? ',
            'labelWidth': '85%',
            'num': '148'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If <b>yes</b>, provide the amount of dividend(stated in Canadian dollars or the elected functional currency) and from which surplus account:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1090'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If <b>yes</b>, and the reporting entity is a corporation, the reporting entity must maintain summary calculations of the exempt surplus, exempt deficit, taxable surplus, taxable deficit, hybrid surplus,hybrid deficit, and underlying foreign tax of the foreign affiliate at the end of the affiliate\'slast taxation year-ending in the reporting entity\'s taxation year in support of the dividend deduction claimed.Documentation supporting these calculations need not be filed but should be retained as it may berequested for examination. Surplus calculations should be made in the calculating currency under subsection5907(6) of the<i> Income Tax Regulations</i>.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            canClear: true,
            'label': '2. Was a subsection 93(1) election made or will such an election be made for the disposition of shares of the foreign affiliate in the year?',
            'labelWidth': '85%',
            'num': '153'
          },
          {
            'type': 'table',
            'num': '1110'
          }
        ]
      },
      {
        forceBreakAfter: true,
        header: 'B. Surplus accounts and share transactions of controlled foreign affiliates',
        rows: [
          {
            'label': '(for not-controlled foreign affiliates, only complete "A" above and go to Part IV)'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': '1. At any time in the taxation year of the reporting entity, was the reporting entity or any foreign affiliate of the reporting entity involved in a corporate or other organization, reorganization, amalgamation, merger, winding-up, liquidation, dissolution, division, or an issuance, redemption, or cancellation of share capital or a similar transaction in a manner that affected the exempt surplus, exempt deficit, taxable surplus, taxable deficit, hybrid surplus, hybrid deficit, or underlying foreign tax of the affiliate for the reporting entity?',
            'labelWidth': '85%',
            'num': '156'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the answer is <b>yes</b>, provide a summary description of each transaction or event.'
          },
          {
            'type': 'infoField',
            'inputType': 'textArea',
            'num': '157'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': '2. At any time in the taxation year of the reporting entity, did the reporting entity or another foreign affiliate of the reporting entity acquire or dispose of a share of the capital stock of the foreign affiliate?',
            'labelWidth': '85%',
            'num': '158'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the answer is <b>yes</b>, provide a summary description of each transaction or event.'
          },
          {
            'type': 'infoField',
            'inputType': 'textArea',
            'num': '159'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            label: 'Part III – Nature of income of "controlled" foreign affiliate',
            labelClass: 'bold'
          }
        ]
      },
      {
        header: 'Section 1 – Employees per business',
        rows: [
          {
            labelClass: 'fullLength'
          },
          {
            label: 'How many full-time employees or employee equivalents (as defined in subparagraphs (c)(i)' +
            'and (ii) of the <b> investment business</b> definition in subsection 95(1) of the Act) did the ' +
            'foreign affiliate employ on a business by business basis throughout each taxation year of the affiliate ' +
            'ending in the reporting entity\'s taxation year?' +
            '(Enter the appropriate NAICS code(s) from the link in the attached instructions)',
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            type: 'table',
            num: '1112'
          }
        ]
      },
      {
        header: 'Section 2 – Composition of revenue',
        rows: [
          {
            label: 'Give the amount of the controlled foreign affiliate\'s gross revenue' +
            'from a business or property for the affiliate\'s taxation year(s) ending in the reporting entity\'s taxation year,' +
            'derived from each of the following sources:',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '1115'
          },
          {
            type: 'table',
            num: '1119'
          }
        ]
      },
      {
        header: 'Section 3 – Foreign accrual property income (FAPI)',
        rows: [
          {
            'type': 'infoField',
            'inputType': 'radio',
            canClear: true,
            'label': '(i) Did the foreign affiliate earn FAPI in any taxation year of the affiliate that ended in the reporting entity\'s taxation year?',
            'num': '183',
            'labelWidth': '85%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '(ii) If yes, give the reporting entity\'s total participating percentage for the foreign affiliate for that year ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '184',
                  decimals: 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Also, give the gross amount of FAPI the affiliate earned that year in respect of each of the following:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1120'
          }
        ]
      },
      {
        forceBreakAfter: true,
        header: 'Section 4 – Capital gains and losses',
        rows: [
          {
            label: '(i) Excluded property',
            labelClass: 'bold'
          },
          {
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'Did the foreign affiliate dispose of a share in another foreign affiliate that was excluded' +
            ' property or an interest in a partnership that was excluded property in a taxation year of the affiliate' +
            ' that ended in the reporting entity\'s taxation year?',
            num: '194',
            labelWidth: '85%'
          },
          {
            labelClass: 'fullLength'
          },
          {
            label: '(ii) Property that is not excluded property',
            labelClass: 'bold'
          },
          {
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'Did the foreign affiliate dispose of capital property that was not excluded property in' +
            ' a taxation year of the affiliate that ended in the reporting entity\'s taxation year?',
            num: '195',
            labelWidth: '85%'
          }
        ]

      },
      {
        header: 'Section 5 – Income included in income from an active business',
        rows: [
          {
            label: 'Was income of the foreign affiliate that would otherwise have been included in its income' +
            'from property included in its income from an active business?' + 'If <b>yes</b>,' +
            'please specify which of the below apply by ticking the appropriate "yes" or "no" box',
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'because of subparagraph 95(2)(a)(i) of the Act?',
            num: '196',
            labelWidth: '85%'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'because of subparagraph 95(2)(a)(ii) of the Act?',
            num: '197',
            labelWidth: '85%'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'because of subparagraph 95(2)(a)(iii) of the Act?',
            num: '198',
            labelWidth: '85%'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'because of subparagraph 95(2)(a)(iv) of the Act?',
            num: '199',
            labelWidth: '85%'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'because of subparagraph 95(2)(a)(v) of the Act?',
            num: '200',
            labelWidth: '85%'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'because of subparagraph 95(2)(a)(vi) of the Act?',
            num: '201',
            labelWidth: '85%'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'because of the type of business carried on and the number of persons' +
            'employed by the foreign affiliate in the business pursuant to paragraphs (a)' +
            'and (b) of the definition of investment business in subsection 95(1) of the Act?',
            num: '202',
            labelWidth: '85%'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'because of paragraph 95(2)(l) of the Act?',
            num: '203',
            labelWidth: '85%'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Was income of the foreign affiliate that would otherwise have been included in its income' +
            'from a business other than an active business included in its income from an active business?' +
            'If <b>yes</b>, please specify which of the below apply by ticking the appropriate "yes" or "no" box.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'because of the 90% test in paragraphs 95(2)(a.1) through (a.4) of the Act?',
            num: '204',
            labelWidth: '85%'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'because of subsection 95(2.3) of the Act?',
            num: '205',
            labelWidth: '85%'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'because of subsection 95(2.4) of the Act?',
            num: '206',
            labelWidth: '85%'
          }
        ]
      },
      {
        header: 'Part IV – Disclosure',
        rows: [
          {
            label: 'To be completed for both not-controlled foreign affiliates and controlled foreign affiliates'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: 'Is any information requested in this return not available?',
            num: '207',
            labelWidth: '85%'
          },
          {labelClass: 'fullLength'},
          {
            label: 'If <b>yes</b>, please specify below',
            showWhen: {fieldId: '207', compare: {is: '1'}}
          },
          {
            type: 'infoField',
            inputType: 'textArea',
            showWhen: {fieldId: '207', compare: {is: '1'}},
            num: '208'
          }
        ]
      }
    ]
  }
})
();