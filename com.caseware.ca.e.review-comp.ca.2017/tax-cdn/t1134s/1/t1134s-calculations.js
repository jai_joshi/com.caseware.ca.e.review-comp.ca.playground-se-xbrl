(function() {

  function removeOtherIdentification(calcUtils, valueArrayNum, checkboxArrayNum) {
    for (var i = 0; i < valueArrayNum.length; i++) {
      calcUtils.field(valueArrayNum[i]).assign(undefined);
      calcUtils.field(checkboxArrayNum[i]).assign(undefined);
    }
  }

  wpw.tax.create.calcBlocks('t1134s', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      var addressFields = ['104', '105', '106', '107', '108', '109'];
      var identificationType = calcUtils.form('T1134').field('103').get();
      var foreignAccrual = ['185', '186', '187', '188', '189', '190', '191', '192'];

      //Individual//
      /*      if (identificationType == 1) {
       calcUtils.field('1136').assign(true);
       calcUtils.getGlobalValue('1137', 'T1134', '1100');
       removeOtherIdentification(calcUtils, [1134, 1140, 1135], [1131, 1132, 1138])
       }*/

      // Corporation//
      calcUtils.field('1131').assign(true);
      calcUtils.getGlobalValue('1134', 'cp', 'bn');
      removeOtherIdentification(calcUtils, [1140, 1135, 1137], [1132, 1138, 1136]);
      //Trust//
      /*      if (identificationType == 3) {
       calcUtils.field('1132').assign(true);
       calcUtils.getGlobalValue('1140', 'T1134', '1200');
       removeOtherIdentification(calcUtils, [1134, 1135, 1137], [1131, 1138, 1136])
       }*/

      //Partnership//
      /*     if (identificationType == 4) {
       calcUtils.field('1138').assign(true);
       calcUtils.getGlobalValue('1135', 'T1134', '1201');
       removeOtherIdentification(calcUtils, [1134, 1140, 1137], [1131, 1132, 1136])
       }*/

      calcUtils.getGlobalValue('110', 'T1134', '110');
      calcUtils.getGlobalValue('111', 'T1134', '111');
      field('102').assign(calcUtils.form().sequence);

      calcUtils.sumBucketValues('193', foreignAccrual);

      calcUtils.sumBucketValues('1337', ['165', '167', '169', '171', '173', '175', '177', '179', '181']);

      if (field('333').get()) {
        field('104').assign(
            field('T1134.104').get() + ' ,' +
            field('T1134.105').get() + ' ' +
            field('T1134.106').get() + ' ,' +
            field('T1134.107').get() + ' ,' +
            field('T1134.108').get() + ' ,' +
            field('T1134.109').get()
        );
      } else {
        for (var i = 0; i < addressFields.length; i++) {
          field(addressFields[i]).disabled(false);
        }
      }

    });
  });
})();
