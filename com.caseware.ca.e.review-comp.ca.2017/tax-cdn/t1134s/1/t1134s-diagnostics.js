(function() {
  wpw.tax.create.diagnostics('t1134s', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var percentFields = ['129', '130', '184'];
    var currencyCodeChecks = [
        {field: 164, errorCode: 'interestCurrencyCodeCheck'},
        {field: 166, errorCode: 'interestOtherCurrencyCodeCheck'},
        {field: 168, errorCode: 'dividendCurrencyCodeCheck'},
        {field: 170, errorCode: 'dividendOtherCurrencyCodeCheck'},
        {field: 172, errorCode: 'royaltyCurrencyCodeCheck'},
        {field: 174, errorCode: 'rentalCurrencyCodeCheck'},
        {field: 176, errorCode: 'loanLendingCurrencyCodeCheck'},
        {field: 178, errorCode: 'insuranceRiskCurrencyCodeCheck'},
        {field: 180, errorCode: 'accountsReceivableCurrencyCode'},
        {field: 182, errorCode: 'investmentPropertyCurrencyCode'}];

    diagUtils.diagnostic('BN', common.prereq(common.check('1134', 'isFilled'), common.bnCheck('1134')));
    diagUtils.diagnostic('BN', common.prereq(common.check('1135', 'isFilled'), common.bnCheck('1135')));

    diagUtils.diagnostic('1134s.requiredToFile', common.prereq(common.check('t1134.102', 'isNonZero'), common.check('100', 'isNonZero')));

    diagUtils.diagnostic('1134s.headOfficeAddressCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      return tools.field('104').require('isFilled');
    }));

    diagUtils.diagnostic('1134s.nameCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      return tools.field('112').require('isFilled');
    }));

    diagUtils.diagnostic('1134s.section1TaxYearStartCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      return tools.field('113').require('isFilled');
    }));

    diagUtils.diagnostic('1134s.ceaseAffiliateCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      return tools.field('114').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.part2NAICSCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      return tools.requireOne(tools.list(['115', '116', '117', '118']), 'isFilled');
    }));

    diagUtils.diagnostic('1134s.countryCodeCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      return tools.requireOne(tools.list(['119', '120', '121', '122']), 'isFilled');
    }));

    diagUtils.diagnostic('1134s.foreignCountryCodeCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      return tools.field('123').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.firstTimeFilingCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      return tools.field('124').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.controlledAffiliate', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      return tools.field('125').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.currencyCodeCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('127').isNonZero();
    }, function(tools) {
      return tools.field('128').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.percentFilledCheck1', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      return tools.field('129').require('isFilled');
    }));

    diagUtils.diagnostic('1134s.percentFilledCheck2', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      return tools.field('130').require('isFilled');
    }));

    diagUtils.diagnostic('1134s.qualifyingInterestTaxYearStartCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      return tools.field('131').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.qualifyingInterestTaxYearEndCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      return tools.field('132').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.section3YearStartCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      if (tools.field('136').isFilled())
        return tools.field('135').require('isFilled') && wpw.tax.utilities.dateCompare.lessThan(tools.field('135').get(), tools.field('136').get());
      else
        return tools.field('135').require('isFilled')
    }));

    diagUtils.diagnostic('1134s.section3YearEndCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      if (tools.field('135').isFilled())
        return tools.field('136').require('isFilled') && wpw.tax.utilities.dateCompare.lessThan(tools.field('135').get(), tools.field('136').get());
      else
        return tools.field('136').require('isFilled')
    }));

    diagUtils.diagnostic('1134s.financialIndicatorCheck', common.prereq(common.check('100', 'isFilled'), common.check('137', 'isNonZero')));

    diagUtils.diagnostic('1134s.totalAssetCurrencyCodeCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('138').isNonZero();
    }, function(tools) {
      return tools.field('139').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.netIncomeCurrencyCodeCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('140').isNonZero();
    }, function(tools) {
      return tools.field('141').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.incomeProfitCurrencyCodeCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('142').isNonZero();
    }, function(tools) {
      return tools.field('143').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.incomeProfitCountryCodeCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('142').isNonZero();
    }, function(tools) {
      return tools.requireOne(tools.list(['144', '145', '146', '147']), 'isFilled');
    }));

    diagUtils.diagnostic('1134s.capitalStockForeignCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      return tools.field('148').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.surplusCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('148').isYes();
    }, function(tools) {
      return tools.requireOne(tools.list(['149', '150', '151', '152']), 'isNonZero');
    }));

    diagUtils.diagnostic('1134s.subsection93IndicatorCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      return tools.field('153').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.subsection93AmountCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('153').isYes();
    }, function(tools) {
      return tools.field('154').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.subsection93CurrencyCodeCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('154').isNonZero();
    }, function(tools) {
      return tools.field('155').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.involvedIndicatorCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('125').isYes();
    }, function(tools) {
      return tools.field('156').require('isFilled');
    }));

    diagUtils.diagnostic('1134s.involvedDescriptionCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('156').isYes();
    }, function(tools) {
      return tools.field('157').require('isFilled');
    }));

    diagUtils.diagnostic('1134s.acquireDisposeIndicatorCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('125').isYes();
    }, function(tools) {
      return tools.field('158').require('isFilled');
    }));

    diagUtils.diagnostic('1134s.acquireDisposeDescriptionCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('158').isYes();
    }, function(tools) {
      return tools.field('159').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.part3NAICSCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('125').isYes();
    }, function(tools) {
      var table = tools.field('1112');
      return tools.checkAll(table.getRows(), function(row) {
        return row[0].require('isFilled');
      })
    }));

    diagUtils.diagnostic('1134s.employeeCountIndicatorCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled();
    }, function(tools) {
      var table = tools.field('1112');
      return tools.checkAll(table.getRows(), function(row) {
        if (row[0].isFilled())
          return tools.requireOne(row[1], 'isFilled');
        else return true;
      })
    }));

    diagUtils.diagnostic('1134s.accrualPropertyIncomeCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('125').isYes();
    }, function(tools) {
      return tools.field('183').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.foreignAccrualPercentFilledCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('125').isYes() && tools.field('183').isYes();
    }, function(tools) {
      return tools.field('184').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.excludedPropertyCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('125').isYes();
    }, function(tools) {
      return tools.field('194').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.notExcludedPropertyCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('125').isYes();
    }, function(tools) {
      return tools.field('195').require('isNonZero');
    }));

    diagUtils.diagnostic('1134s.act95Check', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('125').isYes();
    }, function(tools) {
      return tools.requireAll(tools.list(['196', '197', '198', '199', '200', '201', '202', '203', '204', '205', '206']), 'isNonZero');
    }));

    diagUtils.diagnostic('1134s.summaryDescriptionCheck', common.prereq(function(tools) {
      return tools.field('100').isFilled() && tools.field('125').isYes() && tools.field('207').isYes();
    }, function(tools) {
      return tools.field('208').require('isFilled');
    }));

    function percentCheck(fields) {
      angular.forEach(fields, function(field) {
        diagUtils.diagnostic('1134s.percentFormatCheck', common.prereq(function(tools) {
          return tools.field('100').isFilled();
        }, function(tools) {
          if (tools.field(field).isNonZero()) {
            return tools.field(field).require(function() {
              if (tools.field(field).get().toString().indexOf('.') > -1) {
                return tools.field(field).get() > 0 && tools.field(field).get() <= 100 &&
                    tools.field(field).get().toString().split('.')[0].length <= 3 &&
                    tools.field(field).get().toString().split('.')[1].length <= 2;
              } else {
                return tools.field(field).get() > 0 && tools.field(field).get() <= 100 && tools.field(field).get().toString().length <= 3;
              }
            });
          } else return true;
        }));
      });
    }

    function currencyCodeCheck(diagObjectArray) {
      angular.forEach(diagObjectArray, function(diagObject) {
        diagUtils.diagnostic('1134s.' + diagObject.errorCode, common.prereq(common.and(
            common.check(['100', diagObject.field-1], 'isNonZero', true),
            common.check('125', 'isYes')),
            common.check(diagObject.field, 'isFilled')));
      });
    }
    percentCheck(percentFields);
    currencyCodeCheck(currencyCodeChecks);

  });
})();
