(function() {
  var foreignCurrencyCodes = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.foreignCurrencyCodes, 'value');
  var addCanada = wpw.utilities.clone(wpw.tax.codes.countryCodes);
  addCanada['CAN'] = {value: 'CAN Canada'};
  var countryCodesAddCanada = new wpw.tax.actions.codeToDropdownOptions(addCanada, 'value', 'value');

  wpw.tax.global.tableCalculations.t1134s = {
    '1050': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        'width': '10px',
        'type': 'none'
      },
        {
          'width': '180px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter'

        },
        {
          'width': '800px',
          cellClass: 'alignLeft',
          'type': 'none'
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        }],
      'cells': [{
        '1': {
          'label': 'Book (historical) cost amount:',
          decimals: 0
        },
        '2': {
          'num': '126',
          decimals: 0
        },
        '3': {
          'label': ' (state in Canadian dollars or the elected functional currency – see attached instructions)'
        }
      }]
    },
    '1060': {
      fixedRows: true,
      infoTable: true,
      'columns': [
        {
          'width': '10px',
          'type': 'none'
        },
        {
          'type': 'none',
          width: '180px'
        },
        {
          colClass: 'std-input-width',

          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignLeft',
          'type': 'none'
        },
        {
          'header': '',
          colClass: 'std-input-width'
        }
      ],
      'cells': [
        {
          '1': {
            'label': 'Book (historical) cost amount:'
          },
          '2': {
            'num': '127'
          },
          '3': {
            label: 'Currency code:'
          },
          '4': {
            'type': 'infoField',
            'inputType': 'dropdown',
            'num': '128',
            'options': foreignCurrencyCodes
          }
        }
      ]
    },
    '1070': {
      'fixedRows': true,
      'columns': [{
        'header': '',
        'type': 'none',
        'num': '034'
      },
        {
          header: 'Amount',
          colClass: 'std-input-width',
          decimals: 0
        },
        {
          'header': 'Currency Code'
        }
      ],
      'cells': [{
        '0': {
          'label': 'Total Assets'
        },
        '1': {
          num: '138'
        },
        '2': {
          type: 'infoField',
          num: '139',
          inputType: 'dropdown',
          options: foreignCurrencyCodes
        }
      },
        {
          '0': {
            'label': 'Accounting net income before tax'
          },
          '1': {
            num: '140'
          },
          '2': {
            type: 'infoField',
            num: '141',
            inputType: 'dropdown',
            options: foreignCurrencyCodes
          }
        },
        {
          '0': {
            'label': 'Income or profits tax paid or payable on income'
          },
          '1': {
            num: '142'
          },
          '2': {
            type: 'infoField',
            num: '143',
            inputType: 'dropdown',
            options: foreignCurrencyCodes
          }
        }]
    },
    '1080': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'header': '',
          'type': 'none',
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          'header': '',
          colClass: 'std-input-col-width',
          type: 'dropdown',
          options: countryCodesAddCanada
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          'header': '',
          colClass: 'std-input-col-width',
          type: 'dropdown',
          options: countryCodesAddCanada
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          'header': '',
          colClass: 'std-input-col-width',
          type: 'dropdown',
          options: countryCodesAddCanada
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          'header': '',
          colClass: 'std-input-col-width',
          type: 'dropdown',
          options: countryCodesAddCanada
        }
      ],
      'cells': [
        {
          '0': {
            label: 'Country Code(s):'
          },
          '2': {
            num: '144'
          },
          '4': {
            num: '145'
          },
          '6': {
            num: '146'
          },
          '8': {
            num: '147'
          }
        }
      ]
    },
    '1090': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        'header': '',
        'type': 'none',
        'width': '8%'
      },
        {
          'header': ' ',
          type: 'infoField',
          'width': '15%',
          decimals: 0
        },
        {
          'header': ' ',
          'type': 'none',
          'width': '15%',
          'align': 'center'
        },
        {
          'header': ' ',
          'type': 'none',
          'width': '5%'
        },
        {
          'header': ' ',
          'type': 'none',
          'width': '8%'
        },
        {
          'header': ' ',
          'type': 'infoField',
          'width': '15%',
          decimals: 0
        },
        {
          'header': ' ',
          'type': 'none',
          'width': '15%'
        }
      ],
      'cells': [{
        '0': {
          'label': 'Amount'
        },
        '1': {
          'decimals': 0,
          'type': 'infoField',
          valueType: 'number',
          num: '149'
        },
        '2': {
          'label': 'Exempt surplus'
        },
        '4': {
          'label': 'Amount'
        },
        '5': {
          'type': 'infoField',
          valueType: 'number',
          decimals: 0,
          num: '150'
        },
        '6': {
          'label': 'Taxable surplus'
        }
      },
        {
          '0': {
            'label': 'Amount'
          },
          '1': {
            decimals: 0,
            type: 'infoField',
            valueType: 'number',
            num: '151'
          },
          '2': {
            'label': 'Pre-acquisition surplus'
          },
          '4': {
            'label': 'Amount'
          },
          '5': {
            decimals: 0,
            type: 'infoField',
            valueType: 'number',
            num: '152'
          },
          '6': {
            'label': 'Hybrid surplus'
          }
        }
      ]
    },
    '1110': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        'type': 'none'
      },
        {
          'type': 'infoField',
          colClass: 'std-input-width'
        },
        {
          'type': 'none',
          'width': '5px'
        },
        {
          'type': 'none',
          colClass: 'std-input-width'
        },
        {
          'type': 'infoField',
          colClass: 'std-input-width'
        }
      ],
      'cells': [{
        '0': {
          'label': 'If <b>yes</b>, provide the actual or estimated amount elected on:'
        },
        '1': {
          type: 'infoField',
          valueType: 'number',
          'num': '154'
        },
        '3': {
          'label': 'Currency code'
        },
        '4': {
          type: 'infoField',
          inputType: 'dropdown',
          options: foreignCurrencyCodes,
          'num': '155'
        }
      }]
    },
    '1112': {
      infoTable: false,
      showNumbering: true,
      maxLoop: 4,
      'columns': [
        {
          'header': 'Business (NAICS) code',
          'num': '160',
          "type": "selector",
          selectorOptions: {
            title: 'NAICS Codes',
            items: wpw.tax.codes.naicsCodes
          }
        },
        {
          'header': 'Number of full-time employees or employee equivalents',
          'type': 'dropdown',
          'options': [
            {option: '', value: ''},
            {option: '1 to 5', value: '1'},
            {option: 'More than 5', value: '2'}
          ],
          'num': '161'
        }]
    },
    '1113': {
      fixedRows: true,
      infoTable: true,
      'columns': [
        {
          'header': '',
          'type': 'none'
        },
        {
          "header": "NAICS code",
          "type": "selector",
          selectorOptions: {
            title: 'NAICS Codes',
            items: wpw.tax.codes.naicsCodes
          }
        },
        {
          "header": "NAICS code",
          "type": "selector",
          selectorOptions: {
            title: 'NAICS Codes',
            items: wpw.tax.codes.naicsCodes
          }
        },
        {
          "header": "NAICS code",
          "type": "selector",
          selectorOptions: {
            title: 'NAICS Codes',
            items: wpw.tax.codes.naicsCodes
          }
        },
        {
          "header": "NAICS code",
          "type": "selector",
          selectorOptions: {
            title: 'NAICS Codes',
            items: wpw.tax.codes.naicsCodes
          }
        }
      ],
      'cells': [{
        '0': {
          label: 'NAICS Code(s) (6 digits):'
        },
        '1': {
          'num': '115',
          cellClass: 'alignLeft'
        },
        '2': {
          'num': '116',
          cellClass: 'alignLeft'
        },
        '3': {
          'num': '117',
          cellClass: 'alignLeft'
        },
        '4': {
          'num': '118',
          cellClass: 'alignLeft'
        }
      }]
    },
    '1114': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'header': '',
          'type': 'none',
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          'header': '',
          colClass: 'std-input-col-width',
          type: 'dropdown',
          options: countryCodesAddCanada
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          'header': '',
          colClass: 'std-input-col-width',
          type: 'dropdown',
          options: countryCodesAddCanada
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          'header': '',
          colClass: 'std-input-col-width',
          type: 'dropdown',
          options: countryCodesAddCanada
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          'header': '',
          colClass: 'std-input-col-width',
          type: 'dropdown',
          options: countryCodesAddCanada
        }
      ],
      'cells': [
        {
          '0': {
            label: 'Country Code(s):'
          },
          '2': {
            num: '119'
          },
          '4': {
            num: '120'
          },
          '6': {
            num: '121'
          },
          '8': {
            num: '122'
          }
        }
      ]
    },
    '1115': {
      'fixedRows': true,
      infoTable: true,
      'columns': [{
        'header': '<b>Source</b>',
        'type': 'none',
        width: '557px'
      },
        {
          'header': '<b>Foreign affiliate\'s gross revenue amount and currency code</b>',
          'type': 'none'
        }]
    },
    '1119': {
      fixedRows: true,
      infoTable: true,
      'columns': [
        {
          'type': 'none'
        },
        {
          'type': 'none',
          width: '20px'
        },
        {
          'type': 'none',
          width: '80px'
        },
        {
          colClass: 'std-input-width'

        },
        {
          'type': 'none',
          'width': '20px'
        },
        {
          'type': 'none',
          colClass: 'std-input-width'
        },
        {
          'header': '',
          'type': 'infoField',
          'inputType': 'dropdown',
          'options': foreignCurrencyCodes,
          colClass: 'std-input-width'
        }
      ],
      'cells': [{
        '0': {
          label: '(i) Interest – From other foreign affiliates of the reporting entity'
        },
        '2': {
          label: 'Amount'
        },
        '3': {
          decimals: 0,
          'num': '163'
        },
        '5': {
          label: 'Currency code'
        },
        '6': {
          'num': '164'
        }
      },
        {
          '0': {
            label: '<b>Interest – Other</b>'
          },
          '2': {
            label: 'Amount'
          },
          '3': {
            'num': '165'
          },
          '5': {
            label: 'Currency code'
          },
          '6': {
            'num': '166'
          }
        },
        {
          '0': {
            label: '(ii) Dividends – From other foreign affiliates of the reporting entity'
          },
          '2': {
            label: 'Amount'
          },
          '3': {
            'num': '167'
          },
          '5': {
            label: 'Currency code'
          },
          '6': {
            'num': '168'
          }
        },
        {
          '0': {
            label: '<b>Dividends – Other</b>'
          },
          '2': {
            label: 'Amount'
          },
          '3': {
            'num': '169'
          },
          '5': {
            label: 'Currency code'
          },
          '6': {
            'num': '170'
          }
        },
        {
          '0': {
            label: '(iii) Royalties'
          },
          '2': {
            label: 'Amount'
          },
          '3': {
            'num': '171'
          },
          '5': {
            label: 'Currency code'
          },
          '6': {
            'num': '172'
          }
        },
        {
          '0': {
            label: '(iv) Rental and leasing activities'
          },
          '2': {
            label: 'Amount'
          },
          '3': {
            'num': '173'
          },
          '5': {
            label: 'Currency code'
          },
          '6': {
            'num': '174'
          }
        },
        {
          '0': {
            label: '(v) Loans or lending activities'
          },
          '2': {
            label: 'Amount'
          },
          '3': {
            'num': '175'
          },
          '5': {
            label: 'Currency code'
          },
          '6': {
            'num': '176'
          }
        },
        {
          '0': {
            label: '(vi) Insurance or reinsurance of risks'
          },
          '2': {
            label: 'Amount'
          },
          '3': {
            'num': '177'
          },
          '5': {
            label: 'Currency code'
          },
          '6': {
            'num': '178'
          }
        },
        {
          '0': {
            label: '(vii) Factoring of trade accounts receivable'
          },
          '2': {
            label: 'Amount'
          },
          '3': {
            'num': '179'
          },
          '5': {
            label: 'Currency code'
          },
          '6': {
            'num': '180'
          }
        },
        {
          '0': {
            label: '(viii) Disposition of investment property'
          },
          '2': {
            label: 'Amount'
          },
          '3': {
            'num': '181'
          },
          '5': {
            label: 'Currency code'
          },
          '6': {
            'num': '182'
          }
        }
      ]
    },
    '1120': {
      fixedRows: true,
      infoTable: true,
      hasTotals: true,
      'columns': [
        {
          type: 'none'
        },
        {
          header: '<b>Amount</b>',
          colClass: 'std-input-width'
        }
      ],
      'cells': [
        {
          '0': {
            label: '(i) FAPI that is income from property under subsection 95(1) of the Act',
            labelClass: 'tabbed fullLength'
          },
          '1': {
            num: '185',
            decimals: 0
          }
        },
        {
          '0': {
            label: '(ii) FAPI from the sale of property under paragraph 95(2)(a.1) of the Act',
            labelClass: 'tabbed fullLength'
          },
          '1': {
            num: '186',
            decimals: 0
          }
        },
        {
          '0': {
            label: '(iii) FAPI from the insurance or reinsurance of risks under paragraph 95(2)(a.2) of the Act',
            labelClass: 'tabbed fullLength'
          },
          '1': {
            num: '187',
            decimals: 0
          }
        },
        {
          '0': {
            label: '(iv) FAPI from indebtedness and lease obligations under paragraph 95(2)(a.3) of the Act',
            labelClass: 'tabbed fullLength'
          },
          '1': {
            num: '188',
            decimals: 0
          }
        },
        {
          '0': {
            label: '(v) FAPI from indebtedness and lease obligations under paragraph 95(2)(a.4) of the Act',
            labelClass: 'tabbed fullLength'
          },
          '1': {
            num: '189',
            decimals: 0
          }
        },
        {
          '0': {
            label: '(vi) FAPI from providing services under paragraph 95(2)(b) of the Act',
            labelClass: 'tabbed fullLength'
          },
          '1': {
            num: '190',
            decimals: 0
          }
        },
        {
          '0': {
            label: '(vii) FAPI from the disposition of capital property',
            labelClass: 'tabbed fullLength'
          },
          '1': {
            num: '191',
            decimals: 0
          }
        },
        {
          '0': {
            label: '(viii) FAPI under the description of C in the definition of FAPI in subsection 95(1) of the Act',
            labelClass: 'tabbed fullLength'
          },
          '1': {
            num: '192',
            decimals: 0
          }
        },
        {
          '0': {
            label: '<b>Total</b>'
          },
          '1': {
            num: '193',
            decimals: 0
          }
        }
      ]
    },
    '1130': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '100px'
        },
        {},
        {
          width: '100px'
        },
        {}
      ],
      cells: [
        {
          '0': {
            label: 'Corporation',
            type: 'infoField',
            inputType: 'singleCheckbox',
            num: '1131',
            init: true
          },
          '1': {
            type: 'infoField',
            inputType: 'custom',
            format: ['{N9}RC{N4}', 'NR'],
            label: 'Business number',
            num: '1134'
          },
          '2': {
            label: 'Trust',
            type: 'infoField',
            inputType: 'singleCheckbox',
            disabled: true,
            cannotOverride: true,
            num: '1132'
          },
          '3': {
            label: 'Trust account number',
            type: 'infoField',
            inputType: 'text',
            disabled: true,
            cannotOverride: true,
            num: '1140'
          }
        },
        {
          '0': {
            label: 'Partnership',
            type: 'infoField',
            inputType: 'singleCheckbox',
            disabled: true,
            cannotOverride: true,
            num: '1138'
          },
          '1': {
            label: 'Partnership account number',
            num: '1135',
            type: 'infoField',
            inputType: 'custom',
            disabled: true,
            cannotOverride: true,
            format: ['{N9}RZ{N4}', 'NR']
          },
          '2': {
            label: 'Individual',
            type: 'infoField',
            inputType: 'singleCheckbox',
            disabled: true,
            cannotOverride: true,
            num: '1136'
          },
          '3': {
            type: 'infoField',
            label: 'Social insurance number',
            inputType: 'text',
            validate: {check: 'mod10'},
            disabled: true,
            cannotOverride: true,
            num: '1137'
          }
        }
      ]
    }
  }
})();
