(function() {
  function table200Calcs(calcUtils, rowIndex) {
    var field = calcUtils.field;
    var table205Row = field('200').getRow(rowIndex);

    if (!table205Row)
      return;
    table205Row[2].assign(Math.min(table205Row[0].get(), table205Row[1].get()))
  }

  function table210Calcs(calcUtils, rowIndex) {
    var field = calcUtils.field;
    var table205Row = field('205').getRow(rowIndex);
    var table210Row = field('210').getRow(rowIndex);

    if (!table205Row || !table210Row)
      return;
    table210Row[0].assign(Math.max(table205Row[0].get() * table205Row[1].get() - table205Row[2].get(), 0));
    table210Row[2].assign(Math.min(table210Row[0].get(), table210Row[1].get()))
  }
  wpw.tax.create.calcBlocks('t2s360', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field) {
      field('996').assign(field('200').total(2).get());
      field('997').assign(field('210').total(2).get());
      field('998').assign(field('760').get());
      field('999').assign(
          field('996').get() +
          field('997').get() +
          field('998').get());
    });
    calcUtils.calc(function(calcUtils, field) {
      //part1
      field('121').assign(field('106').get() * 0.15);
      field('150').assign(field('131').get() + field('141').get());
      field('151').assign(field('150').get());
      field('190').assign(field('121').get() + field('151').get());
    });
    calcUtils.calc(function(calcUtils, field) {
      //part2
      //calculation 1
      field('200').getRows().forEach(function(row, rowIndex) {
        table200Calcs(calcUtils, rowIndex)
      });
      field('210').getRows().forEach(function(row, rowIndex) {
        table210Calcs(calcUtils, rowIndex)
      })
    });
  });
})();
