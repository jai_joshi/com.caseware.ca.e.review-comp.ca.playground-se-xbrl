(function() {
  wpw.tax.global.tableCalculations.t2s360 = {
    200: {
      hasTotals: true,
      showNumbering: true,
      superHeaders: [
        {
          header: '<b>Calculation 1</b> – Complete this if you meet conditions 1, 2, 3a, and 4 above',
          colspan: 3
        }
      ],
      columns: [
        {
          header: 'Amount of New Brunswick R&D tax credit<br>' +
          'you originally calculated for the property you<br>' +
          'acquired, or the original user\'s tax credit where<br>' +
          'you acquired the property from a<br>' +
          'non-arm\'s-length party',
          tn: 700,"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
        },
        {
          header: 'Amount calculated using the New Brunswick<br>' +
          'R&D tax credit rate on the date of acquisition<br>' +
          '(or the original user\'s date of acquisition) on<br>' +
          'either the proceeds of disposition (if sold in an<br>' +
          'arm\'s-length transaction) or the<br>' +
          'fair market value of the property<br>' +
          '(in any other case)',
          tn: 710,"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
        },
        {
          header: 'Amount from column 700 or 710,<br>' +
          'whichever is less',
          total: true,
          totalMessage: '<b>Subtotal</b> (enter amount E on line N on page 3)',
          totalIndicator: 'E',
          totalNum: 201
        }
      ]
    },
    205: {
      hasTotals: true,
      showNumbering: true,
      repeats: [
        '210'
      ],
      superHeaders: [
        {
          header: '<b>Calculation 2</b> – Complete this if you meet conditions 1, 2, 3b, and 4 on page 2',
          colspan: 3
        }
      ],
      columns: [
        {
          header: '<b>F</b><br><br>' +
          'The rate the transferee used to determine its<br>' +
          'New Brunswick R&D tax credit for eligible<br>' +
          'expenditures under a<br>' +
          'subsection 127(13) agreement',
          tn: 720,"validate":"percent",
          decimals: 3,
          format: {
            suffix: '%'
          }
        },
        {
          header: '<b>G</b><br><br>' +
          'The proceeds of disposition of the property<br>' +
          'if you dispose of it to a person at arm\'s length;<br>' +
          'or, in any other case, the fair market value of<br>' +
          'the property at conversion or disposition',
          tn: 730,"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
        },
        {
          header: '<b>H</b><br><br>' +
          'The amount, if any, already provided for in<br>' +
          'calculation 1 (where only part of the<br>' +
          'cost of a property is transferred under a<br>' +
          'subsection 127(13) agreement))',
          tn: 740,"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
        }
      ]
    },
    210: {
      hasTotals: true,
      fixedRows: true,
      keepButtonsSpace: true,
      showNumbering: true,
      superHeaders: [
        {
          colspan: 3
        }
      ],
      columns: [
        {
          header: '<b>I</b><br><br>' +
          'Amount determined by the formula<br>' +
          '(F x G) <br>' +
          '(using the columns above)'
        },
        {
          header: '<b>J</b><br><br>' +
          'New Brunswick R&D tax credit earned by<br>' +
          'the transferee for the eligible expenditures<br>' +
          'that were transferred',
          tn: 750,"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
        },
        {
          header: '<b>K</b><br><br>' +
          'Amount from column I or J,<br>' +
          'whichever is less ',
          total: true,
          totalMessage: '<b>Subtotal</b> (enter amount L on line O below)',
          totalIndicator: 'L',
          totalNum: 211
        }
      ]
    }
  };
})();
