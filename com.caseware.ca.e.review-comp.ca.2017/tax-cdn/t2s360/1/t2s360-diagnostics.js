(function() {
  wpw.tax.create.diagnostics('t2s360', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3600002', common.prereq(common.and(
        common.requireFiled('T2S360'),
        common.check('t2s5.597', 'isNonZero')),
        common.check(['106', '131', '141'], 'isNonZero')));

    diagUtils.diagnostic('3600003', common.prereq(common.and(
        common.requireFiled('T2S360'),
        common.check('t2s5.573', 'isNonZero')),
        function(tools) {
          var table = tools.mergeTables(tools.field('200'), tools.field('205'));
          table = tools.mergeTables(table, tools.field('210'));
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne([row[0], row[1], row[3], row[4], row[5], row[7]], 'isNonZero');
          })
        }));

  });
})();
