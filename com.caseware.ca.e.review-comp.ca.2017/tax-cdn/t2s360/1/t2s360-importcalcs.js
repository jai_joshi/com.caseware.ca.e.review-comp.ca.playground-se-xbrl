wpw.tax.create.importCalcs('t2s360', function(tools) {
  tools.intercept('NBNBR.SLIPB[1].TtwnbrB1', function(importObj) {
    tools.form('t2s360').setValue('205', importObj.value * 100, 0, importObj.rowIndex)
  })
});
