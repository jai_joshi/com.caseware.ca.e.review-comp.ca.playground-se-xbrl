(function() {
  wpw.tax.create.formData('t2s360', {
    formInfo: {
      abbreviation: 't2s360',
      title: 'New Brunswick Corporation Tax Calculation',
      schedule: 'Schedule 360',
      headerImage: 'canada-federal',
      showCorpInfo: true,
      code: 'Code 1101',
      category: 'New Brunswick Forms',
      formFooterNum: 'T2 SCH 360 E (11)',
      description: [
        {
          'text': '• Use this schedule if you are a corporation with a permanent establishment in New Brunswick ' +
          'that has made eligible expenditures for scientific research and experimental development carried out in ' +
          'the province, and you want to:'
        },
        {
          'type': 'list',
          'items': [
            'calculate a refundable New Brunswick research and development (R&D) tax credit on eligible expenditures;',
            'show a credit allocated to a corporation that is a member of a partnership, or a credit allocated to a' +
            ' corporation that is a beneficiary under a trust; or',
            'calculate a recapture of the New Brunswick R&D tax credit.'
          ]
        },
        {
          'text': '• An eligible expenditure for R&D is one that meets the definition of a <b>qualified expenditure ' +
          'in</b> subsection 127(9) of the federal <i>Income Tax Act</i>. Deduct the amount of any government ' +
          'assistance, non-government assistance, or contract payment in calculating the amount of a qualified ' +
          'expenditure for <b>investment tax credit</b> purposes in subsection 127(9) of the federal <i>Act</i>.'
        },
        {
          'text': '• Credits earned on eligible expenditures are applied to reduce New Brunswick income tax otherwise ' +
          'payable for the year, as well as amounts owing under the federal and provincial <i>Income Tax Act</i>s, the ' +
          '<i>Canada Pension Plan</i>, and the <i>Employment Insurance Act</i>. Any remaining balance will be refunded.'
        },
        {
          'text': '• File one completed copy of this schedule with your <i>T2 Corporation Income Tax Return</i>'
        }
      ]
    },
    sections: [
      {
        'header': 'Part 1 - New Brunswick refundable R&D tax credit',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total eligible expenditures for R&D made in the current year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '106',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '106'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Refundable current-year credit earned (amount A <b>multiplied by</b> 15%)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '121',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '121'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Plus:</b>',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Refundable credit allocated to a corporation that is a member of a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '131',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '131'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Refundable credit allocated to a corporation that is a beneficiary under a trust',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '141',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '141'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal (amount a <b>plus</b> amount b)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '150'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '151'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Total New Brunswick refundable R&D tax credit</b> (amount B <b>plus</b> amount C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '190',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '190'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': '(enter amount D on line 597 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>)',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 – Recapture of New Brunswick R&D tax credit',
        'rows': [
          {
            'label': '• You will have a recapture of New Brunswick R&D tax credit in a year when you meet <b>all</b> the following conditions:'
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'number',
                'items': [
                  {
                    'label': 'you acquired a particular property in the current year or in any of the four preceding tax years;'
                  },
                  {
                    'label': 'the cost of the property was an eligible expenditure to the corporation;'
                  },
                  {
                    'label': ' ',
                    'sublist': [
                      'the cost of the property was included in calculating your R&D tax credit at the end of the tax year: or',
                      'all or part of the eligible expenditure is the subject of an agreement made under subsection 127(13) of the federal <i>Income Tax Act</i> by you and another corporation (the "transferee"); and'
                    ]
                  },
                  {
                    'label': 'you disposed of the property or converted it to commercial use in the current year. You also meet this condition if you disposed of, or converted to commercial use, a property that incorporates the property previously referred to.<br><br><b>Note:</b><br>The recapture <b>does not apply</b> if you disposed of the property to a non-arm\'s-length buyer who intended to use it all or substantially all for R&D. When the non-arm\'s-length buyer later sells or converts the property to commercial use, the recapture rules will apply to the non-arm\'s length buyer based on the historical amount of the original user\'s New Brunswick R&D tax credit. '
                  }
                ]
              }
            ]
          },
          {
            'label': '• If the corporation is a member of a partnership or a beneficiary under a trust, report its share of the recapture at line 760 in Calculation 3.'
          },
          {
            'label': '• If you have more than one property for calculations 1 and 2, complete the columns for each property for which a recapture applies, using the calculation formats below'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '200'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '205'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '210'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'label': 'Calculation 3',
            'labelClass': 'bold fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'As a member of a partnership or a beneficiary under a trust, you have to report your share of the recapture on line M below:',
            'labelClass': 'fullLength tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s share of the recapture of New Brunswick R&D tax credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '760',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '760'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'label': '(enter amount M on line P below)',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'header': 'Part 3 - Total recapture of New Brunswick R&D tax credit',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Recaptured New Brunswick R&D tax credit for calculation 1 from line E on page 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '996'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Recaptured New Brunswick R&D tax credit for calculation 2 from line L above',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '997'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Recaptured New Brunswick R&D tax credit for calculation 3 from line M above',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '998'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Total recapture of New Brunswick R&D tax credit</b>(total of lines N, O, and P)<br> (enter amount Q on line 573 of Schedule 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '999'
                }
              }
            ],
            'indicator': 'Q'
          }
        ]
      }
    ]
  })
})();
