(function() {

  wpw.tax.create.calcBlocks('t2a17', function(calcUtils) {
    calcUtils.calc(function (calcUtils, field) {
      //todo when roll forward is implemented, calculations need to reflect the roll forward number over federal number when they are different
      ///
      calcUtils.getGlobalValue('001', 'T2S13', '110');
      calcUtils.getGlobalValue('003', 'T2S13', '130');
      calcUtils.getGlobalValue('005', 'T2S13', '150');
      calcUtils.getGlobalValue('009', 'T2S13', '190');
      calcUtils.getGlobalValue('011', 'T2S13', '210');
      calcUtils.getGlobalValue('013', 'T2S13', '221');
      calcUtils.getGlobalValue('015', 'T2S13', '224');
      calcUtils.getGlobalValue('017', 'T2S13', '230');
      calcUtils.getGlobalValue('031', 'T2S13', '115');
      calcUtils.getGlobalValue('033', 'T2S13', '135');
      calcUtils.getGlobalValue('035', 'T2S13', '155');
      calcUtils.getGlobalValue('039', 'T2S13', '195');
      calcUtils.getGlobalValue('041', 'T2S13', '215');
      calcUtils.getGlobalValue('043', 'T2S13', '222');
      calcUtils.getGlobalValue('045', 'T2S13', '225');
      calcUtils.getGlobalValue('047', 'T2S13', '235');
      calcUtils.getGlobalValue('061', 'T2S13', '120');
      calcUtils.getGlobalValue('063', 'T2S13', '140');
      calcUtils.getGlobalValue('065', 'T2S13', '160');
      calcUtils.getGlobalValue('069', 'T2S13', '200');
      calcUtils.getGlobalValue('071', 'T2S13', '220');
      calcUtils.getGlobalValue('073', 'T2S13', '223');
      calcUtils.getGlobalValue('075', 'T2S13', '226');
      calcUtils.getGlobalValue('077', 'T2S13', '240');

      field('091').assign(field('021').get() + field('051').get())
    })


  });
})();
