(function() {

  wpw.tax.create.tables('t2a17', {
    '999': {
      fixedRows: true,
      hasTotals: true,
      columns: [
        {
          header: 'Description',
          type: 'none'
        },
        {
          header: 'Balance at the beginning of the year',
          colClass: 'std-input-col-width',
          total: true,
          totalTn: '021',
          totalNum: '021'
        },
        {
          header: 'Transfer on amalgamation or wind-up of subsidiary',
          colClass: 'std-input-col-width',
          total: true,
          totalTn: '051',
          totalNum: '051'
        },
        {
          header: 'Balance at the end of the year',
          colClass: 'std-input-col-width',
          total: true,
          totalTn: '081',
          totalNum: '081'
        }
      ],
      cells: [
        {
          0: {label: 'Reserve for doubtful debts'},
          1: {tn: '001', num: '001'},
          2: {tn: '031', num: '031'},
          3: {tn: '061', num: '061'}
        },
        {
          0: {label: 'Reserve for undelivered goods and services not rendered'},
          1: {tn: '003', num: '003'},
          2: {tn: '033', num: '033'},
          3: {tn: '063', num: '063'}
        },
        {
          0: {label: 'Reserve for prepaid rent'},
          1: {tn: '005', num: '005'},
          2: {tn: '035', num: '035'},
          3: {tn: '065', num: '065'}
        },
        {
          0: {label: 'Reserve for returnable containers'},
          1: {tn: '009', num: '009'},
          2: {tn: '039', num: '039'},
          3: {tn: '069', num: '069'}
        },
        {
          0: {label: 'Reserve for unpaid amounts'},
          1: {tn: '011', num: '011'},
          2: {tn: '041', num: '041'},
          3: {tn: '071', num: '071'}
        },
        {
          0: {label: 'Insurance Corporations Policy Reserves'},
          1: {tn: '013', num: '013'},
          2: {tn: '043', num: '043'},
          3: {tn: '073', num: '073'}
        },
        {
          0: {label: 'Bank Reserves'},
          1: {tn: '015', num: '015'},
          2: {tn: '045', num: '045'},
          3: {tn: '075', num: '075'}
        },
        {
          0: {label: 'Other Tax Reserves\n'},
          1: {tn: '017', num: '017'},
          2: {tn: '047', num: '047'},
          3: {tn: '077', num: '077'}
        }
      ]
    }
  })
})();
