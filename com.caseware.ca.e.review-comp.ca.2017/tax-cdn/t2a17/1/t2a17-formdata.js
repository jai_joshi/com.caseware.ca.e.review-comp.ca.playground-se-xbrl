(function () {
  wpw.tax.create.formData('t2a17', {
    formInfo: {
      abbreviation: 't2a17',
      title: 'Alberta Reserves',
      schedule: 'Schedule 17',
      formFooterNum: 'AT170 (Mar-14)',
      // headerImage: 'canada-alberta',
      showCorpInfo: true,
      category: 'Alberta Tax Forms',
      showDots: false
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: 'This schedule is required if the opening balance or the claim for Alberta purposes differs from that for federal purposes.',
            labelClass: 'bold'
          },
          {
            label: 'Report all monetary amounts in dollars; DO NOT include cents.'
          }
        ]
      },
      {
        'header': 'RESERVES',
        'rows': [
          {
            'type': 'table',
            'num': '999'
          },
          {
            'label': 'Line 021 + line 051 =',
            'labelCellClass': 'alignRight bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '091'
                },
                'padding': {
                  'type': 'tn',
                  'data': '091'
                }
              }
            ]
          },
          {
            label: 'Carry forward the amount at line 091 to Schedule 12, line 036',
            labelClass: 'bold italic'
          },
          {
            label: 'Carry forward the amount at line 081 to Schedule 12, line 038',
            labelClass: 'bold italic'
          }
        ]
      }
    ]
  });
})();
