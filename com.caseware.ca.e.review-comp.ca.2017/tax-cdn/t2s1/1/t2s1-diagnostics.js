(function() {

  wpw.tax.create.diagnostics('t2s1', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S1'), forEach.row('003', forEach.bnCheckCol(2, true))));

    diagUtils.diagnostic('0010001', common.prereq(common.check('t2j.201', 'isNonZero'), common.requireFiled('T2S1')));

    diagUtils.diagnostic('0010002', common.prereq(common.and(
        common.requireNotFiled('T2S1'),
        common.check('t2s140.9999', 'isFilled')),
        function(tools) {
          return ((tools.field('t2j.300').get() || 0) == (tools.field('t2s140.9999').getKey(0) || 0));
        }));

    diagUtils.diagnostic('001.113', common.prereq(common.and(
        common.check('t2s1.113', 'isNonZero'),
        common.requireFiled('T2S1')),
        common.requireFiled('T2S7')));
    //Above also needs to check if there is taxable income

    diagUtils.diagnostic('001.114', common.prereq(common.requireFiled('T2S1'), function(tools) {
      return tools.field('114').require(function() {
        return (this.get() || 0) >=
            parseFloat(tools.field('t2s525.120').get() +
                tools.field('t2s5.890').get() +
                tools.field('t2s5.899').get());
      });
    }));

    diagUtils.diagnostic('001.216', common.prereq(common.and(
        common.requireFiled('T2S1'),
        function(tools) {
          return tools.field('751').cell(0, 2).isNonZero();
        }),
        common.check('216', 'isNonZero')));
  });
})();
