wpw.tax.create.importCalcs('T2S1', function(importTools) {
  function getImportObjTable(taxPrepIdsObj) {
    //get and sort by row and key of taxPrepIdsObj
    var output = {};
    Object.keys(taxPrepIdsObj).forEach(function(key) {
      importTools.intercept(taxPrepIdsObj[key], function(importObj) {
        if (angular.isDefined(importObj.rowIndex) && !output[importObj.rowIndex]) {
          output[importObj.rowIndex] = {};
        }
        output[importObj.rowIndex][key] = importObj;
      });
    });
    return output;
  }

  function getImportObj(taxPrepIdsObj) {
    //get and sort by key of taxPrepIdsObj
    var output = {};
    Object.keys(taxPrepIdsObj).forEach(function(key) {
      importTools.intercept(taxPrepIdsObj[key], function(importObj) {
        output[key] = importObj;
      });
    });
    return output;
  }

  var s1Obj = {
    650: getImportObjTable({
      description: 'FDONE.SLIPA[1].TtwoneA1',
      amount: 'FDONE.SLIPA[1].TtwoneA2',
      priorYear: 'FDONE.SLIPA[1].TtwoneA3'
    }),
    751: getImportObjTable({
      description: 'FDONE.SLIPD[1].TtwoneD1',
      amount: 'FDONE.SLIPD[1].TtwoneD2',
      priorYear: 'FDONE.SLIPD[1].TtwoneD3'
    })
  };

  var m_eObj = getImportObj({
    sourceSelector: 'FDONE.Ttwone440',
    gifi8523: 'FDONE.Ttwone441',
    nonDeduct50: 'FDONE.Ttwone442',
    nonDeduct20: 'FDONE.Ttwone406',
    tn121: 'FDONE.Ttwone20'
  });

  importTools.after(function() {
    function getPropIfExists(obj, properties) {
      //recursively return either the object if some multilayer property exists, or an empty string
      //i.e. if foo[1][2][3], return it; if not, return ''
      properties = wpw.tax.utilities.ensureArray(properties, false);
      return properties.length < 1 ? (obj || '') : getPropIfExists((obj || '')[properties[0]], properties.splice(1));
    }

    function isRowEmpty(rowObj) {
      var ignoreAlways = [
        '12(1)(x.2)',
        '13(38)(d)(iii)',
        'Recapture from Schedule 12'

      ];
      var ignoreUnlessVal = [
        '12(1)(x)',
        'Deductible Crown payments',
        '20(1)(e)'
      ];
      var description = getPropIfExists(rowObj, ['description', 'value']);
      var amount = getPropIfExists(rowObj, ['amount', 'value']);
      var priorYear = getPropIfExists(rowObj, ['priorYear', 'value']);

      for (var i in ignoreAlways) {
        if (description && (-1 != description.indexOf(ignoreAlways[i]))) {
          return true; //flag as empty if there is a description that matches an ignore condition
        }
      }

      for (i in ignoreUnlessVal) {
        if (description && (-1 != description.indexOf(ignoreUnlessVal[i]))) {
          return !(amount || priorYear); //flag as empty if no cur or py amount and description matches condition
        }
      }
      return !(description || amount || priorYear); //flag as empty if no values exist for the row, else nonempty
    }

    var rowIndex = 0;
    var s1 = importTools.form('T2S1');
    var colsObj = {
      '650_751': {
        description: 0,
        amount: 2,
        priorYear: 6
      }
    };

    Object.keys(s1Obj['650']).forEach(function(row) {
      if (!isRowEmpty(s1Obj['650'][row])) {
        Object.keys(s1Obj['650'][row]).forEach(function(key) {
          s1.setValue('650', getPropIfExists(s1Obj, ['650', row, key, 'value']), colsObj['650_751'][key], rowIndex);
        });
        rowIndex++;
      }
    });

    rowIndex = 0;

    Object.keys(s1Obj['751']).forEach(function(row) {
      if (!isRowEmpty(s1Obj['751'][row])) {
        Object.keys(s1Obj['751'][row]).forEach(function(key) {
          s1.setValue('751', getPropIfExists(s1Obj, ['751', row, key, 'value']), colsObj['650_751'][key], rowIndex);
        });
        rowIndex++;
      }
    });
  });

  importTools.after(function() {
    var m_e = importTools.form('mealsEnt', 0);
    var s1 = importTools.form('T2S1');
    var rowIndex = 0;

    function getPropIfExists(obj, properties) {
      //recursively return either the object if some multilayer property exists, or an empty string
      //i.e. if foo[1][2][3], return it; if not, return ''
      properties = wpw.tax.utilities.ensureArray(properties, false);
      return properties.length < 1 ? (obj || '') : getPropIfExists((obj || '')[properties[0]], properties.splice(1));
    }

    function returnValue(fieldName) {
      return getPropIfExists(m_eObj, [fieldName, 'value'])
    }

    function returnZeroOrEmply(fieldName) {
      return returnValue(fieldName) == '0' || returnValue(fieldName) == ''
    }

    var nonDeduct20Val = m_eObj['nonDeduct20'] ? m_eObj['nonDeduct20']['value'] : 0;

    function setMeValue(fieldName) {
      var fieldVal = m_eObj[fieldName] ? m_eObj[fieldName]['value'] : 0;
      if (!returnZeroOrEmply('nonDeduct20')) {
        m_e.setValue(200, '3', 2, rowIndex);
        m_e.setValue(200, nonDeduct20Val, 4, rowIndex);
        rowIndex++;
        if (!returnZeroOrEmply(fieldName)) {
          m_e.setValue(200, '1', 2, rowIndex);
          m_e.setValue(200, Math.max(fieldVal - nonDeduct20Val, 0), 4, rowIndex);
        }
      } else {
        m_e.setValue(200, '1', 2, rowIndex);
        m_e.setValue(200, fieldVal, 4, rowIndex);
      }
    }

    if (returnValue('sourceSelector') == '1') {
      setMeValue('gifi8523');
    } else {
      setMeValue('nonDeduct50');
    }

    s1.setValue(121, returnValue('tn121'));
  });

  //based on value of FDONE.Ttwone439, import to correct checkbox for tn112
  importTools.intercept('FDONE.Ttwone439', function(importObj) {
    var s1 = importTools.form('T2S1');

    switch(importObj.value) {
      case '1':
          s1.setValue('112-1', true);
        break;
      case '2':
        s1.setValue('112-2', true);
        break;
      case '3':
        s1.setValue('112-3', true);
        break;
    }
  });

  //based on value of FDONE.Ttwone435, import to correct checkbox for tn118
  importTools.intercept('FDONE.Ttwone435', function(importObj) {
    var s1 = importTools.form('T2S1');
    switch(importObj.value) {
      case '1':
        s1.setValue('118-1', true);
        break;
      case '2':
        s1.setValue('118-2', true);
        break;
      case '3':
        s1.setValue('118-3', true);
        break;
    }
  });

});