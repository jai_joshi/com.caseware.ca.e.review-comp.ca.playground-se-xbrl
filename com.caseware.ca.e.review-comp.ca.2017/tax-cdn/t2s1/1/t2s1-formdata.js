(function () {

  var allowNegativeFieldArr =[100,101,102,104,105,106,129,130,132,499,510,340,341,342,344,345,395,396, 512, 502];

  var rowsArr = [
    [
      {
        "label": "Amount calculated on line 9999 from Schedule 125",
        "num": "100",
        "colObjArr": [
          {
            "input": {
              "num": "100"
            }
          },
          {
            padding: {type: 'text', data: 'A'},
            "input": {
              "num": "100-P"
            }
          }
        ]
      },
      {
        "label": "Add:",
        "labelClass": "bold",
        "showDots": false
      },
      {
        "label": "Provision for income taxes - current",
        "tn": "101",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "101"
            },
            "input": {
              "num": "101"
            }
          },
          {},
          {
            "input": {
              "num": "101-P"
            }
          }
        ]
      },
      {
        "label": "Provision for income taxes - deferred",
        "labelClass": "tabbed",
        "tn": "102",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "102"
            },
            "input": {
              "num": "102"
            }
          },
          {},
          {
            "input": {
              "num": "102-P"
            }
          }
        ]
      },
      {
        "label": "Interest and penalties on taxes",
        "labelClass": "tabbed",
        "tn": "103",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "103"
            },
            "input": {
              "num": "103"
            }
          },
          {},
          {
            "input": {
              "num": "103-P"
            }
          }
        ]
      },
      {
        "label": "Amortization of tangible assets",
        "labelClass": "tabbed",
        "tn": "104",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "104"
            },
            "input": {
              "num": "104"
            }
          },
          {},
          {
            "input": {
              "num": "104-P"
            }
          }
        ]
      },
      {
        "label": "Amortization of natural resource assets",
        "labelClass": "tabbed",
        "tn": "105",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "105"
            },
            "input": {
              "num": "105"
            }
          },
          {},
          {
            "input": {
              "num": "105-P"
            }
          }
        ]
      },
      {
        "label": "Amortization of intangible assets",
        "labelClass": "tabbed",
        "tn": "106",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "106"
            },
            "input": {
              "num": "106"
            }
          },
          {},
          {
            "input": {
              "num": "106-P"
            }
          }
        ]
      },
      {
        "label": "Recapture of capital cost allowance from Schedule 8",
        "labelClass": "tabbed",
        "tn": "107",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "107"
            },
            "input": {
              "num": "107"
            }
          },
          {},
          {
            "input": {
              "num": "107-P"
            }
          }
        ]
      },
      {
        "label": "Income inclusion under subparagraph 13(38)(d)(iii) from Schedule 10",
        "labelClass": "tabbed",
        "tn": "108",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "108"
            },
            "input": {
              "num": "108"
            }
          },
          {},
          {
            "input": {
              "num": "108-P"
            }
          }
        ]
      },
      {
        "label": "Loss in equity of subsidiaries and affiliates",
        "labelClass": "tabbed",
        "tn": "110",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "110"
            },
            "input": {
              "num": "110"
            }
          },
          {},
          {
            "input": {
              "num": "110-P"
            }
          }
        ]
      },
      {
        "label": "Loss on disposal of assets (amounts from GIFI 125)",
        "labelClass": "tabbed",
        "type": "none",
        "showDots": false
      },
      {
        "label": "Realized gains/losses on disposal of assets (GIFI 8210)",
        "labelClass": "tabbed2",
        "num": "1001",
        "disabled": true,
        "colObjArr": [
          {
            "input": {
              "num": "1001"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "1001-P"
            }
          }
        ]
      },
      {
        "label": "Realized gains/losses on sale of investments (GIFI 8211)",
        "labelClass": "tabbed2",
        "num": "1002",
        "disabled": true,
        "colObjArr": [
          {
            "input": {
              "num": "1002"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "1002-P"
            }
          }
        ]
      },
      {
        "label": "Realized gains/losses on sale of resource properties (GIFI 8212)",
        "labelClass": "tabbed2",
        "num": "1003",
        "disabled": true,
        "colObjArr": [
          {
            "input": {
              "num": "1003"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "1003-P"
            }
          }
        ]
      },
      {
        "label": "Gains/losses on disposal of assets (GIFI 9609)",
        "labelClass": "tabbed2",
        "num": "1004",
        "disabled": true,
        "colObjArr": [
          {
            "input": {
              "num": "1004"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "1004-P"
            }
          }
        ]
      },
      {
        "label": "Total loss on disposal of assets",
        "labelClass": "tabbed",
        "tn": "111",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "111"
            },
            "input": {
              "num": "111"
            }
          },
          {},
          {
            "input": {
              "num": "111-P"
            }
          }
        ]
      },
      {
        "label": "Charitable donations and gifts from Schedule 2 <br> <i>(Please check applicable box for tn112)</i>",
        "labelClass": "tabbed",
        "type": "none",
        "showDots": false
      }
    ],
    //table 1120
    [
      {
        "label": "Total charitable donations and gifts",
        "labelClass": "tabbed",
        "tn": "112",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "112"
            },
            "input": {
              "num": "112",
              disabled: true
            }
          },
          {},
          {
            "input": {
              "num": "112-P"
            }
          }
        ]
      },
      {
        "label": "Taxable capital gains from Schedule 6",
        "labelClass": "tabbed",
        "tn": "113",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "113"
            },
            "input": {
              "num": "113"
            }
          },
          {},
          {
            "input": {
              "num": "113-P"
            }
          }
        ]
      },
      {
        "label": "Political contributions",
        "labelClass": "tabbed",
        "showDots": false
      },
      {
        "label": "- Political donations from all provinces <b>excluding</b> Quebec",
        "showDots": false,
        "colObjArr": [
          {
            "input": {
              "num": "114NQ"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "114NQ-P"
            }
          }
        ]
      },
      {
        "label": "- Quebec political contributions",
        "colObjArr": [
          {
            "input": {
              "num": "114Q"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "114Q-P"
            }
          }
        ]
      },
      {
        "label": "Total political contributions",
        "labelClass": "tabbed",
        "tn": "114",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "114"
            },
            "input": {
              "num": "114"
            }
          },
          {},
          {
            "input": {
              "num": "114-P"
            }
          }
        ]
      },
      {
        "label": "Holdbacks",
        "labelClass": "tabbed",
        "tn": "115",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "115"
            },
            "input": {
              "num": "115"
            }
          },
          {},
          {
            "input": {
              "num": "115-P"
            }
          }
        ]
      },
      {
        "label": "Deferred and prepaid expenses",
        "labelClass": "tabbed",
        "tn": "116",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "116"
            },
            "input": {
              "num": "116"
            }
          },
          {},
          {
            "input": {
              "num": "116-P"
            }
          }
        ]
      },
      {
        "label": "Depreciation in inventory – end of year",
        "labelClass": "tabbed",
        "tn": "117",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "117"
            },
            "input": {
              "num": "117"
            }
          },
          {},
          {
            "input": {
              "num": "117-P"
            }
          }
        ]
      },
      {
        "label": "Scientific research expenditures deducted per financial statements <br> <i>(Please check applicable box for tn118)</i>",
        "labelClass": "tabbed",
        "type": "none",
        "showDots": false
      }
    ],
    //table 1180
    [
      {
        "label": " Total scientific research expenditures deducted",
        "labelClass": "tabbed",
        "tn": "118",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "118"
            },
            "input": {
              "num": "118"
            }
          },
          {},
          {
            "input": {
              "num": "118-P"
            }
          }
        ]
      },
      {
        "label": "Capitalized interest",
        "labelClass": "tabbed",
        "tn": "119",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "119"
            },
            "input": {
              "num": "119"
            }
          },
          {},
          {
            "input": {
              "num": "119-P"
            }
          }
        ]
      },
      {
        "label": "Non-deductible club dues and fees",
        "labelClass": "tabbed",
        "tn": "120",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "120"
            },
            "input": {
              "num": "120"
            }
          },
          {},
          {
            "input": {
              "num": "120-P"
            }
          }
        ]
      },
      {
        "label": "Non-deductible meals and entertainment expenses",
        "labelClass": "tabbed",
        "type": "none",
        "showDots": false
      },
      {
        "label": "- Amount from GIFI 125 (Code 8523)",
        "labelClass": "tabbed",
        "type": "none",
        "colObjArr": [
          {
            "input": {
              "num": "121-W"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "121-W-P"
            }
          }
        ]
      },
      {
        "label": "- To verify the correct amount, please use <i>Meals & Entertainment Work chart</i>",
        "labelClass": "tabbed",
        "type": "none",
        "showDots": false
      },
      {
        "label": "Total Non-deductible meals and entertainment expenses",
        "labelClass": "tabbed",
        "tn": "121",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "121"
            },
            "input": {
              "num": "121"
            }
          },
          {},
          {
            "input": {
              "num": "121-P"
            }
          }
        ]
      },
      {
        "label": "Non-deductible automobile expenses",
        "labelClass": "tabbed",
        "tn": "122",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "122"
            },
            "input": {
              "num": "122"
            }
          },
          {},
          {
            "input": {
              "num": "122-P"
            }
          }
        ]
      },
      {
        "label": "Non-deductible life insurance premiums",
        "labelClass": "tabbed",
        "tn": "123",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "123"
            },
            "input": {
              "num": "123"
            }
          },
          {},
          {
            "input": {
              "num": "123-P"
            }
          }
        ]
      },
      {
        "label": "Non-deductible company pension plans",
        "labelClass": "tabbed",
        "tn": "124",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "124"
            },
            "input": {
              "num": "124"
            }
          },
          {},
          {
            "input": {
              "num": "124-P"
            }
          }
        ]
      },
      {
        "label": "Other reserves on lines 270 and 275 from Schedule 13",
        "labelClass": "tabbed",
        "tn": "125",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "125"
            },
            "input": {
              "num": "125"
            }
          },
          {},
          {
            "input": {
              "num": "125-P"
            }
          }
        ]
      },
      {
        "label": "Reserves from financial statements - balance at the end of the year",
        "labelClass": "tabbed",
        "tn": "126",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "126"
            },
            "input": {
              "num": "126"
            }
          },
          {},
          {
            "input": {
              "num": "126-P"
            }
          }
        ]
      },
      {
        "label": "Soft costs on construction and renovation of buildings",
        "labelClass": "tabbed",
        "tn": "127",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "127"
            },
            "input": {
              "num": "127"
            }
          },
          {},
          {
            "input": {
              "num": "127-P"
            }
          }
        ]
      },
      {
        "label": "Non-deductible fines and penalties under section 67.6",
        "labelClass": "tabbed",
        "tn": "128",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "128"
            },
            "input": {
              "num": "128"
            }
          },
          {},
          {
            "input": {
              "num": "128-P"
            }
          }
        ]
      },
      {
        "label": "Income or loss for tax purposes - partnerships",
        "labelClass": "fullLength tabbed",
        "showDots": false
      }
    ],
    //table003
    [
      {
        "label": "Total partnerships",
        "labelClass": "fullLength text-right",
        "tn": "129",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "129"
            },
            "input": {
              "num": "129"
            }
          },
          {},
          {
            "input": {
              "num": "129-P"
            }
          }
        ]
      },
      {
        "label": "Amounts calculated under section 34.2 from Schedule 73",
        "labelClass": "tabbed",
        "tn": "130",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "130"
            },
            "input": {
              "num": "130"
            }
          },
          {},
          {
            "input": {
              "num": "130-P"
            }
          }
        ]
      },
      {
        "label": "Income shortfall adjustment and additional amount from Schedule 73",
        "labelClass": "tabbed",
        "tn": "131",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "131"
            },
            "input": {
              "num": "131"
            }
          },
          {},
          {
            "input": {
              "num": "131-P"
            }
          }
        ]
      },
      {
        forceBreakAfter: true,
        "label": "Income or loss for tax purposes - joint ventures",
        "labelClass": "tabbed",
        "tn": "132",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "132"
            },
            "input": {
              "num": "132"
            }
          },
          {},
          {
            "input": {
              "num": "132-P"
            }
          }
        ]
      },
      {
        "label": "Other Additional Items",
        "labelClass": "fullLength bold",
        "showDots": false
      },
      {
        "label": "Accounts payable and accruals for cash basis - closing",
        "labelClass": "tabbed",
        "tn": "201",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "201"
            },
            "input": {
              "num": "201"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "201-P"
            }
          }
        ]
      },
      {
        "label": "Accounts receivable and prepaid for cash basis - opening",
        "labelClass": "tabbed",
        "tn": "202",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "202"
            },
            "input": {
              "num": "202"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "202-P"
            }
          }
        ]
      },
      {
        "label": "Accrual inventory - opening",
        "labelClass": "tabbed",
        "tn": "203",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "203"
            },
            "input": {
              "num": "203"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "203-P"
            }
          }
        ]
      },
      {
        "label": "Accrued dividends - prior year",
        "labelClass": "tabbed",
        "tn": "204",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "204"
            },
            "input": {
              "num": "204"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "204-P"
            }
          }
        ]
      },
      {
        "label": "Capital items expensed",
        "labelClass": "tabbed",
        "tn": "206",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "206"
            },
            "input": {
              "num": "206"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "206-P"
            }
          }
        ]
      },
      {
        "label": "Debt issue expense",
        "labelClass": "tabbed",
        "tn": "208",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "208"
            },
            "input": {
              "num": "208"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "208-P"
            }
          }
        ]
      },
      {
        "label": "Deemed dividend income",
        "labelClass": "tabbed",
        "tn": "209",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "209"
            },
            "input": {
              "num": "209"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "209-P"
            }
          }
        ]
      },
      {
        "label": "Deemed interest on loans to non-residents",
        "labelClass": "tabbed",
        "tn": "210",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "210"
            },
            "input": {
              "num": "210"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "210-P"
            }
          }
        ]
      },
      {
        "label": "Deemed interest received",
        "labelClass": "tabbed",
        "tn": "211",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "211"
            },
            "input": {
              "num": "211"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "211-P"
            }
          }
        ]
      },
      {
        "label": "Development expenses claimed in current year",
        "labelClass": "tabbed",
        "tn": "212",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "212"
            },
            "input": {
              "num": "212"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "212-P"
            }
          }
        ]
      },
      {
        "label": "Dividend stop-loss adjustment",
        "labelClass": "tabbed",
        "tn": "213",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "213"
            },
            "input": {
              "num": "213"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "213-P"
            }
          }
        ]
      },
      {
        "label": "Dividends credited to the investment account",
        "labelClass": "tabbed",
        "tn": "214",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "214"
            },
            "input": {
              "num": "214"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "214-P"
            }
          }
        ]
      },
      {
        "label": "Exploration expenses claimed in current year",
        "labelClass": "tabbed",
        "tn": "215",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "215"
            },
            "input": {
              "num": "215"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "215-P"
            }
          }
        ]
      },
      {
        "label": "Financing fees deducted in books",
        "labelClass": "tabbed",
        "tn": "216",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "216"
            },
            "input": {
              "num": "216"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "216-P"
            }
          }
        ]
      },
      {
        "label": "Foreign accrual property income",
        "labelClass": "tabbed",
        "tn": "217",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "217"
            },
            "input": {
              "num": "217"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "217-P"
            }
          }
        ]
      },
      {
        "label": "Foreign affiliate property income",
        "labelClass": "tabbed",
        "tn": "218",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "218"
            },
            "input": {
              "num": "218"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "218-P"
            }
          }
        ]
      },
      {
        "label": "Foreign exchange included in retained earnings",
        "labelClass": "tabbed",
        "tn": "219",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "219"
            },
            "input": {
              "num": "219"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "219-P"
            }
          }
        ]
      },
      {
        "label": "Gain on settlement of debt",
        "labelClass": "tabbed",
        "tn": "220",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "220"
            },
            "input": {
              "num": "220"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "220-P"
            }
          }
        ]
      },
      {
        "label": "Interest paid on income debentures",
        "labelClass": "tabbed",
        "tn": "221",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "221"
            },
            "input": {
              "num": "221"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "221-P"
            }
          }
        ]
      },
      {
        "label": "Limited partnership losses from Schedule 4",
        "labelClass": "tabbed",
        "tn": "222",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "222"
            },
            "input": {
              "num": "222"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "222-P"
            }
          }
        ]
      },
      {
        "label": "Mandatory inventory adjustment – included in current year",
        "labelClass": "tabbed",
        "tn": "224",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "224"
            },
            "input": {
              "num": "224"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "224-P"
            }
          }
        ]
      },
      {
        "label": "Non-deductible advertising",
        "labelClass": "tabbed",
        "tn": "226",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "226"
            },
            "input": {
              "num": "226"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "226-P"
            }
          }
        ]
      },
      {
        "label": "Non-deductible interest",
        "labelClass": "tabbed",
        "tn": "227",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "227"
            },
            "input": {
              "num": "227"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "227-P"
            }
          }
        ]
      },
      {
        "label": "Non-deductible legal and accounting fees",
        "labelClass": "tabbed",
        "tn": "228",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "228"
            },
            "input": {
              "num": "228"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "228-P"
            }
          }
        ]
      },
      {
        "label": "Optional value of inventory - included in current year",
        "labelClass": "tabbed",
        "tn": "229",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "229"
            },
            "input": {
              "num": "229"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "229-P"
            }
          }
        ]
      }
    ],
    //table017
    [
      {
        "label": "Other expenses from financial statements ",
        "labelClass": "tabbed",
        "tn": "230",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "230"
            },
            "input": {
              "num": "230"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "230-P"
            }
          }
        ]
      },
      {
        "label": "Recapture of SR&ED expenditures from Form T661",
        "labelClass": "tabbed",
        "tn": "231",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "231"
            },
            "input": {
              "num": "231"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "231-P"
            }
          }
        ]
      },
      {
        "label": "Resource amounts deducted ",
        "labelClass": "tabbed",
        "tn": "232",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "232"
            },
            "input": {
              "num": "232"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "232-P"
            }
          }
        ]
      },
      {
        "label": "Restricted farm losses - current year from Schedule 4",
        "labelClass": "tabbed",
        "tn": "233",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "233"
            },
            "input": {
              "num": "233"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "233-P"
            }
          }
        ]
      },
      {
        "label": "Sales tax assessments ",
        "labelClass": "tabbed",
        "tn": "234",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "234"
            },
            "input": {
              "num": "234"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "234-P"
            }
          }
        ]
      },
      {
        "label": "Share issue expense ",
        "labelClass": "tabbed",
        "tn": "235",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "235"
            },
            "input": {
              "num": "235"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "235-P"
            }
          }
        ]
      },
      {
        "label": "Write-down of capital property ",
        "labelClass": "tabbed",
        "tn": "236",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "236"
            },
            "input": {
              "num": "236"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "236-P"
            }
          }
        ]
      },
      {
        "label": "Amounts received in respect of qualifying environmental trust per paragraphs 12(1)(z.1) and 12(1)(z.2)",
        "labelClass": "tabbed",
        "tn": "237",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "237"
            },
            "input": {
              "num": "237"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "237-P"
            }
          }
        ]
      },
      {
        "label": "Contractors' completion method adjustment: revenue net of costs on contracts under 2 years - previous year",
        "labelClass": "tabbed",
        "tn": "238",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "238"
            },
            "input": {
              "num": "238"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "238-P"
            }
          }
        ]
      },
      {
        "label": "Taxable/non-deductible other comprehensive income items ",
        "labelClass": "tabbed",
        "tn": "239",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "239"
            },
            "input": {
              "num": "239"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "239-P"
            }
          }
        ]
      },
      {
        "label": "Book loss on joint ventures",
        "labelClass": "tabbed",
        "tn": "248",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "248"
            },
            "input": {
              "num": "248"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "248-P"
            }
          }
        ]
      },
      {
        "label": "Book loss on partnerships",
        "labelClass": "tabbed",
        "tn": "249",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "249"
            },
            "input": {
              "num": "249"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "249-P"
            }
          }
        ]
      },
      {
        "label": "Other additions:",
        "labelClass": "fullLength tabbed bold",
        "showDots": false
      }
    ],
    //table 650
    [
      {
        "label": "<b>Total</b> of lines 201 to 249 and line 296",
        "labelClass": "fullLength text-right",
        "tn": "199",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "199"
            },
            "input": {
              "num": "199", cannotOverride: true
            }
          },
          {
            "padding": {
              "type": "text",
              "data": "F"
            }
          },
          {
            "input": {
              "num": "199-P", cannotOverride: true
            }
          }
        ]
      },
      {
        "label": "<b>Total</b> (lines 101 to 199)",
        "labelClass": "fullLength text-right",
        "tn": "500",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "500"
            },
            "input": {
              "num": "500", cannotOverride: true
            }
          },
          {
            "input": {
              "num": "520", cannotOverride: true
            }
          },
          {
            "padding": {
              "type": "text",
              "data": "B"
            },
            "input": {
              "num": "500-P", cannotOverride: true
            }
          }
        ]
      },
      {
        forceBreakAfter: true,
        "label": "Amount A <b>plus</b> amount B",
        "colObjArr": [
          {
            "input": {
              "num": "502"
            }
          },
          {
            "padding": {
              "type": "text",
              "data": "C"
            },
            "input": {
              "num": "502-P"
            }
          }
        ]
      }
    ],
    //section2
    [

      {
        "label": "Deduct:",
        "labelClass": "fullLength bold",
        "showDots": false
      },
      {
        "label": "Gain on disposal of assets (amounts from GIFI 125)",
        "labelClass": "tabbed",
        "type": "none",
        "showDots": false
      },
      {
        "label": "Realized gains/losses on disposal of assets (Code 8210)",
        "labelClass": "tabbed2",
        "num": "1011",
        "disabled": true,
        "colObjArr": [
          {
            "input": {
              "num": "1011"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "1011-P"
            }
          }
        ]
      },
      {
        "label": "Realized gains/losses on sale of investments (Code 8211)",
        "labelClass": "tabbed2",
        "num": "1012",
        "disabled": true,
        "colObjArr": [
          {
            "input": {
              "num": "1012"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "1012-P"
            }
          }
        ]
      },
      {
        "label": "Realized gains/losses on sale of resource properties (Code 8212)",
        "labelClass": "tabbed2",
        "num": "1013",
        "disabled": true,
        "colObjArr": [
          {
            "input": {
              "num": "1013"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "1013-P"
            }
          }
        ]
      },
      {
        "label": "Gains/losses on disposal of assets (Code 9609)",
        "labelClass": "tabbed2",
        "num": "1014",
        "disabled": true,
        "colObjArr": [
          {
            "input": {
              "num": "1014"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "1014-P"
            }
          }
        ]
      },
      {
        "label": "Gain on disposal of assets per financial statements",
        "labelClass": "tabbed",
        "tn": "401",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "401"
            },
            "input": {
              "num": "401"
            }
          },
          {},
          {
            "input": {
              "num": "401-P"
            }
          }
        ]
      },
      {
        "label": "Non-taxable dividends under section 83 from Schedule 3",
        "labelClass": "tabbed",
        "tn": "402",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "402"
            },
            "input": {
              "num": "402"
            }
          },
          {},
          {
            "input": {
              "num": "402-P"
            }
          }
        ]
      },
      {
        "label": "Capital cost allowance from Schedule 8",
        "labelClass": "tabbed",
        "tn": "403",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "403"
            },
            "input": {
              "num": "403"
            }
          },
          {},
          {
            "input": {
              "num": "403-P"
            }
          }
        ]
      },
      {
        "label": "Terminal loss from Schedule 8",
        "labelClass": "tabbed",
        "tn": "404",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "404"
            },
            "input": {
              "num": "404"
            }
          },
          {},
          {
            "input": {
              "num": "404-P"
            }
          }
        ]
      },
      {
        "label": "Cumulative eligible capital deduction from Schedule 10",
        "labelClass": "tabbed",
        "tn": "405",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "405"
            },
            "input": {
              "num": "405"
            }
          },
          {},
          {
            "input": {
              "num": "405-P"
            }
          }
        ]
      },
      {
        "label": "Allowable business investment loss from Schedule 6",
        "labelClass": "tabbed",
        "tn": "406",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "406"
            },
            "input": {
              "num": "406"
            }
          },
          {},
          {
            "input": {
              "num": "406-P"
            }
          }
        ]
      },
      {
        "label": "Foreign non-business tax deduction under subsection 20(12)",
        "labelClass": "tabbed",
        "tn": "407",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "407"
            },
            "input": {
              "num": "407"
            }
          },
          {},
          {
            "input": {
              "num": "407-P"
            }
          }
        ]
      },
      {
        "label": "Holdbacks",
        "labelClass": "tabbed",
        "tn": "408",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "408"
            },
            "input": {
              "num": "408"
            }
          },
          {},
          {
            "input": {
              "num": "408-P"
            }
          }
        ]
      },
      {
        "label": "Deferred and prepaid expenses",
        "labelClass": "tabbed",
        "tn": "409",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "409"
            },
            "input": {
              "num": "409"
            }
          },
          {},
          {
            "input": {
              "num": "409-P"
            }
          }
        ]
      },
      {
        "label": "Depreciation in inventory – end of prior year",
        "labelClass": "tabbed",
        "tn": "410",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "410"
            },
            "input": {
              "num": "410"
            }
          },
          {},
          {
            "input": {
              "num": "410-P"
            }
          }
        ]
      },
      {
        "label": "SR&ED expenditures claimed in the year on line 460 from Form T661",
        "labelClass": "tabbed",
        "tn": "411",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "411"
            },
            "input": {
              "num": "411"
            }
          },
          {},
          {
            "input": {
              "num": "411-P"
            }
          }
        ]
      },
      {
        "label": "Other reserves on line 280 from Schedule 13",
        "labelClass": "tabbed",
        "tn": "413",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "413"
            },
            "input": {
              "num": "413"
            }
          },
          {},
          {
            "input": {
              "num": "413-P"
            }
          }
        ]
      },
      {
        "label": "Reserves from financial statements - balance at the beginning of the year",
        "labelClass": "tabbed",
        "tn": "414",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "414"
            },
            "input": {
              "num": "414"
            }
          },
          {},
          {
            "input": {
              "num": "414-P"
            }
          }
        ]
      },
      {
        "label": "Patronage dividend deduction from Schedule 16",
        "labelClass": "tabbed",
        "tn": "416",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "416"
            },
            "input": {
              "num": "416"
            }
          },
          {},
          {
            "input": {
              "num": "416-P"
            }
          }
        ]
      },
      {
        "label": "Contributions to deferred income plans from Schedule 15",
        "labelClass": "tabbed",
        "tn": "417",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "417"
            },
            "input": {
              "num": "417"
            }
          },
          {},
          {
            "input": {
              "num": "417-P"
            }
          }
        ]
      },
      {
        "label": "Incorporation expenses under paragraph 20(1)(b)",
        "labelClass": "tabbed",
        "tn": "418",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "418"
            },
            "input": {
              "num": "418"
            }
          },
          {},
          {
            "input": {
              "num": "418-P"
            }
          }
        ]
      },
      {
        "label": "Other Deductional Items:",
        "labelClass": "fullLength bold",
        "showDots": false
      },
      {
        "label": "Accounts payable and accruals for cash basis – opening",
        "labelClass": "tabbed",
        "tn": "300",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "300"
            },
            "input": {
              "num": "300"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "300-P"
            }
          }
        ]
      },
      {
        "label": "Accounts receivable and prepaid for cash basis – closing ",
        "labelClass": "tabbed",
        "tn": "301",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "301"
            },
            "input": {
              "num": "301"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "301-P"
            }
          }
        ]
      },
      {
        "label": "Accrual inventory - closing",
        "labelClass": "tabbed",
        "tn": "302",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "302"
            },
            "input": {
              "num": "302"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "302-P"
            }
          }
        ]
      },
      {
        "label": "Accrued dividends - current year",
        "labelClass": "tabbed",
        "tn": "303",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "303"
            },
            "input": {
              "num": "303"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "303-P"
            }
          }
        ]
      },
      {
        "label": "Bad debt ",
        "labelClass": "tabbed",
        "tn": "304",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "304"
            },
            "input": {
              "num": "304"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "304-P"
            }
          }
        ]
      },
      {
        "label": "Equity in income from subsidiaries or affiliates ",
        "labelClass": "tabbed",
        "tn": "306",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "306"
            },
            "input": {
              "num": "306"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "306-P"
            }
          }
        ]
      },
      {
        "label": "Exempt income under section 81 ",
        "labelClass": "tabbed",
        "tn": "307",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "307"
            },
            "input": {
              "num": "307"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "307-P"
            }
          }
        ]
      },
      {
        "label": "Mandatory inventory adjustment - included in prior year",
        "labelClass": "tabbed",
        "tn": "309",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "309"
            },
            "input": {
              "num": "309"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "309-P"
            }
          }
        ]
      },
      {
        "label": "Contributions to a qualifying environmental trust",
        "labelClass": "tabbed",
        "tn": "310",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "310"
            },
            "input": {
              "num": "310"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "310-P"
            }
          }
        ]
      },
      {
        "label": "Non-Canadian advertising expenses - broadcasting",
        "labelClass": "tabbed",
        "tn": "311",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "311"
            },
            "input": {
              "num": "311"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "311-P"
            }
          }
        ]
      },
      {
        "label": "Non-Canadian advertising expenses - printed materials",
        "labelClass": "tabbed",
        "tn": "312",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "312"
            },
            "input": {
              "num": "312"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "312-P"
            }
          }
        ]
      },
      {
        "label": "Optional value of inventory - included in prior year",
        "labelClass": "tabbed",
        "tn": "313",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "313"
            },
            "input": {
              "num": "313"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "313-P"
            }
          }
        ]
      },
      {
        "label": "Other income from financial statements",
        "labelClass": "tabbed",
        "tn": "314",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "314"
            },
            "input": {
              "num": "314"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "314-P"
            }
          }
        ]
      },
      {
        "label": "Payments made for allocations in proportion to borrowing and bonus interest payments from Schedule 17",
        "labelClass": "tabbed",
        "tn": "315",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "315"
            },
            "input": {
              "num": "315"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "315-P"
            }
          }
        ]
      },
      {
        "label": "Contractors' completion method adjustment: revenue net of costs on contracts under 2 years - current year",
        "labelClass": "tabbed",
        "tn": "316",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "316"
            },
            "input": {
              "num": "316"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "316-P"
            }
          }
        ]
      },
      {
        "label": "Non-taxable/deductible other comprehensive income items",
        "labelClass": "tabbed",
        "tn": "347",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "347"
            },
            "input": {
              "num": "347"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "347-P"
            }
          }
        ]
      },
      {
        "label": "Book income on joint venture",
        "labelClass": "tabbed",
        "tn": "348",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "348"
            },
            "input": {
              "num": "348"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "348-P"
            }
          }
        ]
      },
      {
        "label": "Book income on partnership",
        "labelClass": "tabbed",
        "tn": "349",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "349"
            },
            "input": {
              "num": "349"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "349-P"
            }
          }
        ]
      },
      {
        "showDots": false
      },
      {
        "showDots": false
      },
      {
        "label": "Resource Deductions:",
        "labelClass": "fullLength bold",
        "showDots": false
      },
      {
        "showDots": false
      },
      {
        "label": "Canadian development expenses from Schedule 12",
        "labelClass": "tabbed",
        "tn": "340",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "340"
            },
            "input": {
              "num": "340"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "340-P"
            }
          }
        ]
      },
      {
        "label": "Canadian exploration expenses from Schedule 12",
        "labelClass": "tabbed",
        "tn": "341",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "341"
            },
            "input": {
              "num": "341"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "341-P"
            }
          }
        ]
      },
      {
        "label": "Canadian oil and gas property expenses from Schedule 12 ",
        "labelClass": "tabbed",
        "tn": "342",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "342"
            },
            "input": {
              "num": "342"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "342-P"
            }
          }
        ]
      },
      {
        "label": "Depletion from Schedule 12 ",
        "labelClass": "tabbed",
        "tn": "344",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "344"
            },
            "input": {
              "num": "344"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "344-P"
            }
          }
        ]
      },
      {
        "label": "Foreign exploration and development expenses from Schedule 12 ",
        "labelClass": "tabbed",
        "tn": "345",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "345"
            },
            "input": {
              "num": "345"
            }
          },
          {},
          {},
          {
            "input": {
              "num": "345-P"
            }
          }
        ]
      },
      {labelClass: 'fullLength'},
      {labelClass: 'fullLength'},
      {labelClass: 'fullLength'},
      {
        "label": "Other Deductions:",
        "labelClass": "fullLength tabbed bold",
        "showDots": false
      }
    ],
    //table751
    [
      {
        "label": "<b>Total</b> of lines 300 to 345, and line 396",
        "labelClass": "fullLength text-right",
        "tn": "499",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "499"
            },
            "input": {
              "num": "499", cannotOverride: true
            }
          },
          {
            "padding": {
              "type": "text",
              "data": "G"
            }
          },
          {
            "input": {
              "num": "499-P", cannotOverride: true
            }
          }
        ]
      },
      {
        "label": "<b>Total</b> (lines 401 to 499)",
        "labelClass": "fullLength text-right",
        "tn": "510",
        "colObjArr": [
          {
            "padding": {
              "type": "tn",
              "data": "510"
            },
            "input": {
              "num": "510", cannotOverride: true
            }
          },
          {
            "input": {
              "num": "530", cannotOverride: true
            }
          },
          {
            "padding": {
              "type": "text",
              "data": "D"
            },
            "input": {
              "num": "510-P", cannotOverride: true
            }
          }
        ]
      },
      {
        "label": "<b>Net Income (Loss) for Income Tax Purposes</b> (amount C minus amount D)",
        "colObjArr": [
          {
            "input": {
              "num": "512", cannotOverride: true
            }
          },
          {
            "padding": {
              "type": "text",
              "data": "E"
            },
            "input": {
              "num": "511", cannotOverride: true
            }
          }
        ]
      },
      {
        "label": "Enter amount E on line 300 on page 3 of the T2 return",
        "showDots": false
      },
      {
        "label": "Restricted Farm Income (loss) included in the above Net income (loss) ",
        "labelClass": "bold",
        "colObjArr": [
          {
            underline: 'double',
            "input": {
              "num": "farmIncome"
            }
          },
          {}
        ]
      }
    ]
  ];

  function getRows(rowsArr) {
    var rows = [];
    rowsArr.forEach(function (rowObj) {
      if (Object.keys(rowObj).length === 1 && rowObj.labelClass == 'fullLength') {
        return rows.push(rowObj);
      }
      var inputObj = {
        layout: 'alignInput',
        label: rowObj.label,
        labelClass: rowObj.labelClass,
        layoutOptions: {indicatorColumn: rowObj.indicatorColumn, showDots: rowObj.showDots == false ? false : true},
        indicator: rowObj.indicator
      };

      if (rowObj.forceBreakAfter)
        inputObj['forceBreakAfter'] = true;

      var colObjArr = rowObj.colObjArr;
      if (colObjArr) {
        var columnsArr = [];
        colObjArr.forEach(function (colObj, index) {
          if (Object.keys(colObj).length == 0) {
            columnsArr[index] = null;
          } else {
            var input = colObj.input;

            if (input && input.num) {
              if (allowNegativeFieldArr.indexOf(parseFloat(input.num)) != -1) {
                //allow negative
                input.validate = {
                  'or': [
                    {
                      'matches': '^-?[.\\d]{1,13}$'
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                }
              } else {
                input.validate = {
                  'or': [
                    {
                      'matches': '^[.\\d]{1,13}$'
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                };
              }
            }

            columnsArr[index] = {
              padding: colObj.padding,
              input: input
            }
          }
        })
      }
      inputObj.columns = columnsArr;

      rows.push(inputObj)
    });
    return rows
  }

  wpw.tax.global.formData.t2s1 = {
    formInfo: {
      abbreviation: 'T2S1',
      title: 'Net Income (Loss) for Income Tax Purposes',
      schedule: 'Schedule 1',
      code: 'Code 1701',
      neededRepeatForms: ['T2S125'],
      showCorpInfo: true,
      description: [
        {
          'type': 'list',
          'items': [
            'The purpose of this schedule is to provide a reconciliation between the corporation\'s net income ' +
            '(loss) as reported on the financial statements and its net income ' +
            '(loss) for tax purposes. For more information,' +
            ' see the ' + '<i>' + 'T2 Corporation – Income Tax Guide' + '</i>.',
            'All legislative references are to the <i>Income Tax Act</i>'
          ]
        }
      ],
      'descriptionHighlighted': true,
      'highlightFieldsets': true,
      category: 'Federal Tax Forms',
      formFooterNum: 'T2 SCH 1 E (17)'
    },
    'sections': [
      {
        rows: [{type: 'table', num: '001'}]
            .concat(getRows(rowsArr[0]))
            .concat([{type: 'table', num: '1120'}])
            .concat(getRows(rowsArr[1]))
            .concat([{type: 'table', num: '1180'}])
            .concat(getRows(rowsArr[2]))
            .concat([{type: 'table', num: '003'}])
            .concat(getRows(rowsArr[3]))
            .concat([{type: 'table', num: '017'}])
            .concat(getRows(rowsArr[4]))
            .concat([{type: 'table', num: '650'}])
            .concat(getRows(rowsArr[5]))
      },
      {
        rows: getRows(rowsArr[6])
            .concat([{type: 'table', num: '751'}])
            .concat(getRows(rowsArr[7]))
      }
    ]
  };
})();
