(function() {

  function calculationsNum199(calcs, suffix) {
    var num199 = '199' + suffix;
    var num201 = '201' + suffix;
    var num202 = '202' + suffix;
    var num203 = '203' + suffix;
    var num204 = '204' + suffix;
    var num205 = '205' + suffix;
    var num206 = '206' + suffix;
    var num208 = '208' + suffix;
    var num209 = '209' + suffix;
    var num210 = '210' + suffix;
    var num211 = '211' + suffix;
    var num212 = '212' + suffix;
    var num213 = '213' + suffix;
    var num214 = '214' + suffix;
    var num215 = '215' + suffix;
    var num216 = '216' + suffix;
    var num217 = '217' + suffix;
    var num218 = '218' + suffix;
    var num219 = '219' + suffix;
    var num220 = '220' + suffix;
    var num221 = '221' + suffix;
    var num222 = '222' + suffix;
    var num223 = '223' + suffix;
    var num224 = '224' + suffix;
    var num226 = '226' + suffix;
    var num227 = '227' + suffix;
    var num228 = '228' + suffix;
    var num229 = '229' + suffix;
    var num230 = '230' + suffix;
    var num231 = '231' + suffix;
    var num232 = '232' + suffix;
    var num233 = '233' + suffix;
    var num234 = '234' + suffix;
    var num235 = '235' + suffix;
    var num236 = '236' + suffix;
    var num237 = '237' + suffix;
    var num238 = '238' + suffix;
    var num239 = '239' + suffix;
    var num248 = '248' + suffix;
    var num249 = '249' + suffix;
    var num296 = '296' + suffix;
    calcs.sumBucketValues(num199, [num201, num202, num203, num204, num206, num208, num209, num210, num211,
      num212, num213, num214, num215, num216, num217, num218, num219, num220, num221, num222, num224,
      num226, num227, num228, num229, num230, num231, num232, num233, num234, num235, num236, num237, num238,
      num239, num248, num249, num296]);
  }

  function calculationsNum500(calcs, suffix) {
    var num101 = '101' + suffix;
    var num102 = '102' + suffix;
    var num103 = '103' + suffix;
    var num104 = '104' + suffix;
    var num105 = '105' + suffix;
    var num106 = '106' + suffix;
    var num107 = '107' + suffix;
    var num108 = '108' + suffix;
    var num110 = '110' + suffix;
    var num111 = '111' + suffix;
    var num112 = '112' + suffix;
    var num113 = '113' + suffix;
    var num114 = '114' + suffix;
    var num115 = '115' + suffix;
    var num116 = '116' + suffix;
    var num117 = '117' + suffix;
    var num118 = '118' + suffix;
    var num119 = '119' + suffix;
    var num120 = '120' + suffix;
    var num121 = '121' + suffix;
    var num122 = '122' + suffix;
    var num123 = '123' + suffix;
    var num124 = '124' + suffix;
    var num125 = '125' + suffix;
    var num126 = '126' + suffix;
    var num127 = '127' + suffix;
    var num128 = '128' + suffix;
    var num129 = '129' + suffix;
    var num130 = '130' + suffix;
    var num131 = '131' + suffix;
    var num132 = '132' + suffix;
    var num199 = '199' + suffix;
    var num500 = '500' + suffix;

    calcs.sumBucketValues(num500, [num101, num102, num103, num104, num105, num106, num107, num108, num110,
      num111, num112, num113, num114, num115, num116, num117, num118, num119, num120, num121, num122, num123,
      num124, num125, num126, num127, num128, num129, num130, num131, num132, num199]);
  }

  function calculationsNum499(calcs, suffix) {
    var num300 = '300' + suffix;
    var num301 = '301' + suffix;
    var num302 = '302' + suffix;
    var num303 = '303' + suffix;
    var num304 = '304' + suffix;
    var num305 = '305' + suffix;
    var num306 = '306' + suffix;
    var num307 = '307' + suffix;
    var num308 = '308' + suffix;
    var num309 = '309' + suffix;
    var num310 = '310' + suffix;
    var num311 = '311' + suffix;
    var num312 = '312' + suffix;
    var num313 = '313' + suffix;
    var num314 = '314' + suffix;
    var num315 = '315' + suffix;
    var num316 = '316' + suffix;
    var num347 = '347' + suffix;
    var num348 = '348' + suffix;
    var num349 = '349' + suffix;
    var num340 = '340' + suffix;
    var num341 = '341' + suffix;
    var num342 = '342' + suffix;
    var num344 = '344' + suffix;
    var num345 = '345' + suffix;
    var num499 = '499' + suffix;
    var num396 = '396' + suffix;

    calcs.sumBucketValues(num499, [num300, num301, num302, num303, num304, num305, num306, num307, num308,
      num309, num310, num311, num312, num313, num314, num315, num316, num347, num340, num341, num342, num344,
      num345, num348, num349, num396], true);
  }

  function calculationsNum510(calcs, suffix) {
    var num401 = '401' + suffix;
    var num402 = '402' + suffix;
    var num403 = '403' + suffix;
    var num404 = '404' + suffix;
    var num405 = '405' + suffix;
    var num406 = '406' + suffix;
    var num407 = '407' + suffix;
    var num408 = '408' + suffix;
    var num409 = '409' + suffix;
    var num410 = '410' + suffix;
    var num411 = '411' + suffix;
    var num413 = '413' + suffix;
    var num414 = '414' + suffix;
    var num416 = '416' + suffix;
    var num417 = '417' + suffix;
    var num499 = '499' + suffix;
    var num510 = '510' + suffix;
    calcs.sumBucketValues(num510, [num401, num402, num403, num404, num405, num406, num407, num408, num409,
      num410, num411, num413, num414, num416, num417, num499], true);
  }

  function gainOrLossApplied(field, codeTotal, negativeNum, positiveNum) {
    if (codeTotal < 0) {
      codeTotal = codeTotal * (-1);
      field(negativeNum).assign(codeTotal);
      field(positiveNum).assign(0);
    }
    else if (codeTotal > 0) {
      field(negativeNum).assign(0);
      field(positiveNum).assign(codeTotal);
    }
    else {
      field(negativeNum).assign(0);
      field(positiveNum).assign(0);
    }
  }

  function setTableValues(calcUtils, tableNum, rowIndex, total, description) {
    var field = calcUtils.field;
    var table = field(tableNum);
    var descriptionField = table.cell(rowIndex, 0);
    var amount = table.cell(rowIndex, 2);
    if (total == 0) {
      descriptionField.assign();
      amount.assign()
    }
    else {
      descriptionField.assign(description);
      amount.assign(total)
    }
  }

  function getApplicableValue(calcUtils, formId, num1, num2) {
    var value = 0;
    var num1Val = calcUtils.form(formId).field(num1).get();
    var num2Val = calcUtils.form(formId).field(num2).get();
    if (num1Val > 0 && num2Val == 0) {
      value = num1Val
    } else if (num1Val >= 0 && num2Val > 0) {
      value = num2Val
    } else {
      value = num1Val
    }
    return value;
  }

  wpw.tax.create.calcBlocks('t2s1', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      var sec20Total = field('sec_20_1_e.1000').total(8).get();
      if (sec20Total > 0) {
        setTableValues(calcUtils, '751', 0, sec20Total, 'Section 20(1)(e) deduction')
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('108').assign(getApplicableValue(calcUtils, 'T2S10', '475', '410'));
    });

    calcUtils.calc(function(calcUtils, field, form) {

      calcUtils.getGlobalValue('provinces', 'cp', 'prov_residence');

      calcUtils.getGlobalValue(100, 'T2S140', {fieldId: 9999, key: 0});
      calcUtils.getGlobalValue(101, 'T2S140', {fieldId: 9990, key: 0});
      calcUtils.getGlobalValue(102, 'T2S140', {fieldId: 9995, key: 0});

      var s125RepeatForms = wpw.tax.actions.getFilteredRepeatIds('T2S125');
      var val9791 = calcUtils.sumRepeatGifiValue('T2S125', 500, 9791);
      var val8670 = calcUtils.sumRepeatGifiValue('T2S125', 300, 8670);
      field('104').assign(val9791 + val8670);
      field('104').source(field('t2s125.9791'));

      var val8650 = calcUtils.sumRepeatGifiValue('T2S125', 300, 8650);
      var val8460 = calcUtils.sumRepeatGifiValue('T2S125', 200, 8460);
      calcUtils.field('105').assign(val8650 + val8460);
      field('105').source(field('t2s125.8650'));

      field('1001').assign(Math.min(0, calcUtils.sumRepeatGifiValue('T2S125', null, '8210', 0)) * -1);
      field('1001').source(field('t2s125.8210'));
      field('1002').assign(Math.min(0, calcUtils.sumRepeatGifiValue('T2S125', null, '8211', 0)) * -1);
      field('1002').source(field('t2s125.8211'));
      field('1003').assign(Math.min(0, calcUtils.sumRepeatGifiValue('T2S125', null, '8212', 0)) * -1);
      field('1003').source(field('t2s125.8212'));
      field('1004').assign(Math.min(0, calcUtils.sumRepeatGifiValue('T2S125', null, '9609', 0)) * -1);
      field('1004').source(field('t2s125.9609'));

      field('111').assign(field('1001').get() + field('1002').get() + field('1003').get() + field('1004').get());

      field('1011').assign(Math.max(0, calcUtils.sumRepeatGifiValue('T2S125', null, '8210', 0)));
      field('1011').source(field('t2s125.8210'));
      field('1012').assign(Math.max(0, calcUtils.sumRepeatGifiValue('T2S125', null, '8211', 0)));
      field('1012').source(field('t2s125.8211'));
      field('1013').assign(Math.max(0, calcUtils.sumRepeatGifiValue('T2S125', null, '8212', 0)));
      field('1013').source(field('t2s125.8212'));
      field('1014').assign(Math.max(0, calcUtils.sumRepeatGifiValue('T2S125', null, '9609', 0)));
      field('1014').source(field('t2s125.9609'));

      field('401').assign(field('1011').get() + field('1012').get() + field('1013').get() + field('1014').get());

      //calcUtils.setRepeatSummaryValue(105, s125RepeatForms, {fieldId: 8650, key: 0});
      var val8570 = calcUtils.sumRepeatGifiValue('T2S125', 300, 8570);
      var val9832 = calcUtils.sumRepeatGifiValue('T2S125', 500, 9832);
      field('106').assign(val8570 + val9832);
      field('106').source(field('t2s125.8570'));

      //tn 118
      //from GIFI9282
      field('1180').cell(0,1).assign(calcUtils.sumRepeatGifiValue('T2S125', 300, 9282));
      field('1180').cell(0,1).source(field('T2S125.9282'));
      //from T661
      field('1180').cell(1, 1).assign(field('T661.380').get());
      field('1180').cell(1, 1).source(field('T661.380'));
      //based on checkbox assign value to num 112
      var tn118FieldIdsArr = ['118-1', '118-2', '118-3'];
      tn118FieldIdsArr.forEach(function(fieldId, rowIndex) {
        if (!calcUtils.hasChanged(fieldId)) {
          field(fieldId).assign(false);
          field(fieldId).disabled(false);
        }

        if (calcUtils.hasChanged(fieldId) && field(fieldId).get()) {
          field('118').assign(field('1180').cell(rowIndex, 1).get())
        }
      });

      calcUtils.setRepeatSummaryValue(false, s125RepeatForms, {fieldId: 8235, key: 0}, null, function(code8235Total) {
        gainOrLossApplied(field, code8235Total, 205, 305)
      });
      calcUtils.setRepeatSummaryValue(false, s125RepeatForms, {fieldId: 8232, key: 0}, null, function(code8232Total) {
        gainOrLossApplied(field, code8232Total, 110, 306)
      });

      calcUtils.getGlobalValue('107', 'T2S8', '213');
      //tn 112
      //from GIFI8522
      field('1120').cell(0,1).assign(calcUtils.sumRepeatGifiValue('T2S125', 300, 8522));
      field('1120').source(field('t2s125.8522'));
      //from S2
      var s2DonationFieldIds = ['210', '410', '510', '520'];
      var totalDonation = 0;
      s2DonationFieldIds.forEach(function(fieldId) {
        totalDonation += form('T2S2').field(fieldId).get();
      });
      field('1120').cell(1, 1).assign(totalDonation);
      field('1120').cell(1, 1).source(form('T2S2').field('210'));
      //based on checkbox assign value to num 112
      var donationFieldIdsArr = ['112-1', '112-2', '112-3'];
      donationFieldIdsArr.forEach(function(fieldId, rowIndex) {
        if (!calcUtils.hasChanged(fieldId)) {
          field(fieldId).assign(false);
          field(fieldId).disabled(false);
        }

        if (calcUtils.hasChanged(fieldId) && field(fieldId).get()) {
          field('112').assign(field('1120').cell(rowIndex, 1).get())
        }
      });

      calcUtils.getGlobalValue('113', 'T2S6', '987');
      //114-NQ calculation
      var nonQuebec = '114-NQ';
      var jurisdiction = field('cp.750').get();

      var map = {
        NL: '891',
        PE: '892',
        NS: '893',
        NB: '894',
        SK: '890',
        BC: '896',
        YT: '897',
        NT: '898',
        NU: '899'
      };

      var nonQuebecField = field(nonQuebec);
      var formS5 = calcUtils.form('T2S5');
      var onPolitical = field('T2S525.120').get();
      for (var prov in map) {
        if (jurisdiction == prov) {
          nonQuebecField.assign(formS5.field(map[prov]).get());
        }
      }
      if (jurisdiction == 'ON') {
        field(nonQuebec).assign(onPolitical)
      }
      if (jurisdiction == 'MJ') {
        field(nonQuebec).assign(
            onPolitical +
            field('T2S5.890').get() +
            field('T2S5.891').get() +
            field('T2S5.892').get() +
            field('T2S5.893').get() +
            field('T2S5.894').get() +
            field('T2S5.896').get() +
            field('T2S5.897').get() +
            field('T2S5.898').get() +
            field('T2S5.899').get()
        )
      }

      calcUtils.sumBucketValues('114', [nonQuebec, '114-Q']);
      field('114').source(form('T2S5').field('891'));
      //completed. please refer the aboveTODO: get 114_NQ from donation WC --> no political donation w/c for now

      //if there is no value in meal and ent, take 50% of gifi value
      field('121-W').assign(calcUtils.sumRepeatGifiValue('t2s125', 300, 8523));
      field('121-W').source(field('t2s125.8523'));

      var mealEntValue = field('mealsEntS.addBack').get();
      if (mealEntValue) {
        field('121').assign(mealEntValue)
      }
      else {
        field('121').assign(field('121-W').get() * 50 / 100)
      }
      field('121').source(field('mealsEntS.addBack'));

      calcUtils.getGlobalValue('122', 'automobileSummary', '300');
      calcUtils.getGlobalValue('line270', 'T2S13', '270');
      calcUtils.getGlobalValue('line275', 'T2S13', '275');
      calcUtils.sumBucketValues('125', ['line270', 'line275']);
      field('125').source(form('T2S13').field('201').cell(0, 1));
      //getglobalvalue to 126, taxprep getting values from t2s13s, which is a w/c
      //CorpTax getting value from hidden table 998 in T2S13
      var reserveEndValueFromFs = form('T2S13').field('998').total(4).get();
      field('126').assign(reserveEndValueFromFs);
      field('126').source(form('T2S13').field('201').cell(0, 1));
      field('130').assign(
          form('T2S73').field('270').get() +
          form('T2S73').field('280').get() +
          form('T2S73').field('290').get());
      field('130').source(form('T2S73').field('270'));
      calcUtils.getGlobalValue('131', 'T2S73', '323');
      calcUtils.getGlobalValue('222', 'T2S4', '701');
      //Non-canadian advertising expenses
      calcUtils.getGlobalValue('226', 'advertisingWorkchartS', '101');
      calcUtils.getGlobalValue('233', 'T2S4', '410');
      calcUtils.field('248').assign(calcUtils.sumRepeatGifiValue('t2s125', 150, 8234) * -1);
      field('248').source(field('t2s125.8234'));
      calcUtils.field('249').assign(calcUtils.sumRepeatGifiValue('t2s125', 150, 8235) * -1);
      field('249').source(field('t2s125.8235'));
      calcUtils.getGlobalValue('402', 'T2S3', '600');
      calcUtils.getGlobalValue('403', 'T2S8', '217');
      calcUtils.getGlobalValue('404', 'T2S8', '215');
      if (field('T2S10.1000').get() == 1) {
        field('405').assign(0);
        field('405').source(field('T2S10.113'));
      }
      else {
        calcUtils.getGlobalValue('405', 'T2S10', '113');
      }
      calcUtils.getGlobalValue('406', 'T2S6', '715', function(value) {
        calcUtils.field('406').assign(value * (-1));
      });
      calcUtils.getGlobalValue('407', 'T2S21', '130');
      calcUtils.getGlobalValue('411', 'T661', '460');
      calcUtils.getGlobalValue('413', 'T2S13', '280');
      field('413').source(form('T2S13').field('201').cell(0, 1));
      //getting global values for 414, Tax Prep is getting values from t2s13s, but this is a w/c
      //corpTax getting value from T2s13 table 201 and 998
      var reserveBegValueFromFs = form('T2S13').field('998').total(1).get() + form('T2S13').field('998').total(2).get();
      field('414').assign(reserveBegValueFromFs);
      field('414').source(form('T2S13').field('201').cell(0, 1));
      calcUtils.getGlobalValue('416', 'T2S16', '116');
      calcUtils.getGlobalValue('417', 'T2S15', '203');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var s125buckets = ['8670', '8650', '8570', '8232', '8210', '9282', '8210', '8235'];
      var t2s1buckets = [104, 105, 106, 110, 111, 118, 401, 205];

      for (var n = 0; n < s125buckets; n++) {
        calcUtils.setRepeatSummaryValue(t2s1buckets[n], s125RepeatForms, {fieldId: s125buckets[n], key: 0});
      }

    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.equals('230', '240');
      calcUtils.equals('129', '141');
      calcUtils.equals('129-P', '141-P');
      calcUtils.equals('294', '653');

      field('231').assign(Math.min(0, field('t661.1045').get()) * (-1));
      field('231').source(field('t661.1045'));

      calculationsNum199(calcUtils, '');
      calculationsNum199(calcUtils, '-P');
      calculationsNum500(calcUtils, '');
      calculationsNum500(calcUtils, '-P');

      calcUtils.equals('394', '753');
      calcUtils.equals('520', '500', true);
      calcUtils.sumBucketValues('502', ['100', '520']);
      calcUtils.sumBucketValues('502-P', ['100-P', '500-P']);

      calculationsNum499(calcUtils, '');
      calculationsNum499(calcUtils, '-P');
      calculationsNum510(calcUtils, '');
      calculationsNum510(calcUtils, '-P');

      calcUtils.equals('530', '510', true);
      calcUtils.subtract('512', '502', '530', true); // Net Income 512 = 502 - 510, where 510 = 530
      calcUtils.subtract('511', '502-P', '510-P', true);
    });
  });
})();
