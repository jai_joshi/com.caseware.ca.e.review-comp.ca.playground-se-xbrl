(function() {

  function getTable(totalNum, isPartnershipTable, desTn, amountTn, totalTn) {
    var columns = [];

    if (isPartnershipTable) {
      columns.push(
          {
            header: 'Name of Partnership',
            type: 'text'
          },
          {
            colClass: 'std-spacing-width',
            'type': 'none'
          },
          {
            header: 'Partnership Number',
            type: 'custom',
            format: ['{N9}RZ{N4}', 'NR']
          }
      )
    }
    else {
      columns.push(
          {
            header: 'Description',
            'tn': desTn,
            type: 'text',
            "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]}
          }
      )
    }

    columns.push(
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          header: 'Amount',
          'tn': amountTn,
          'total': true,
          totalNum: totalNum,
          totalTn: totalTn,
          "validate": {"or": [{"matches": "^-?[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          header: 'Amount',
          'total': true,
          colClass: 'std-input-width',
          totalNum: totalNum + '-P',
          "validate": {"or": [{"matches": "^-?[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
        }
    );

    return {
      'infoTable': true,
      hasTotals: true,
      'maxLoop': 100000,
      'showNumbering': true,
      'columns': columns
    }
  }

  wpw.tax.global.tableCalculations.t2s1 = {
    '001': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'header': 'Current Year',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'header': 'Previous Year',
          'type': 'none'
        }
      ]
    },
    '1120': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'singleCheckbox'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-col-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        }
      ],
      cells: [
        {0: {label: '- Charitable donations and gifts from GIFI 8522'}, 3: {num: '112-1'}},
        {0: {label: '- Charitable donations and gifts from Schedule 2'}, 3: {num: '112-2'}},
        {0: {label: '- Charitable donations and gifts: Other'}, 3: {num: '112-3'}}
      ]
    },
    '1180': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'singleCheckbox'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-col-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        }
      ],
      cells: [
        {0: {label: '- Scientific research expenditures from GIFI 9282'}, 3: {num: '118-1'}},
        {0: {label: '- Scientific research expenditures from T661 line 380'}, 3: {num: '118-2'}},
        {0: {label: '- Scientific research expenditures: Other'}, 3: {num: '118-3'}}
      ]
    },
    '017': {
      'infoTable': true,
      'maxLoop': 100000,
      hasTotals: true,
      'showNumbering': true,
      'columns': [
        {
          colClass: 'std-spacing-width',
          'type': 'none'
        },
        {
          'header': 'Description',
          colClass: 'std-input-col-width',
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          'header': 'Amount',
          'total': true,
          totalNum: '240',
          colClass: 'std-input-width'
        },
        {
          'type': 'none'
        }
      ]
    },
    '003': getTable('141', true),
    '650': getTable('296', false, '605', '295', '296'),
    '751': getTable('396', false, '705', '395', '396')
  }
})
();
