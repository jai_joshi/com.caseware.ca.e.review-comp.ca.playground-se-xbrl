(function() {

  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  wpw.tax.create.calcBlocks('t2a3', function(calcUtils) {
    //part 1
    calcUtils.calc(function(calcUtils, field, form) {
      field('104').assign(Math.min(field('100').get()+field('102').get(),field('t2a.068').get()-field('t2a.070').get()-field('t2a.071').get()-field('t2a.072').get()-field('t2a.074').get()));
      field('106').assign(field('400').cell(4,6).get());
      field('108').assign(Math.max(field('100').get()+field('102').get()-field('104').get()-field('106').get(),0));
      //===Capital Investment Tax Credit===//
      field('204').assign(Math.min(field('200').get()+field('202').get(),field('t2a.068').get()-field('t2a.070').get()-field('t2a.071').get()-field('t2a.072').get()-field('t2a.074').get()-field('104').get()));
      field('206').assign(field('500').cell(10,6).get());
      field('208').assign(Math.max(field('200').get()+field('202').get()-field('204').get()-field('206').get(),0));

      field('600').assign(field('104').get()+field('204').get());
      field('602').assign(field('t2a.068').get()-field('t2a.070').get()-field('t2a.071').get()-field('t2a.072').get()-field('t2a.074').get());
      field('604').assign(Math.min(field('600').get(),field('602').get()))
    });

    // historical data chart, assumes that maximum credit is applied in each year
    calcUtils.calc(function(calcUtils, field) {
      var HistoryArray = [400, 500];
      HistoryArray.forEach(function(num) {
          var creditReceived;
          var limitOnCredit;
          var totalBB=0; //total beginning balance credit
          var taxationYearTable = field('tyh.200');

          for(var i=1;i<field(num).size().rows;i++){
              totalBB +=field(num).cell(i,3).get();
          }
          if (num==400){
            creditReceived = field('100').get();
            limitOnCredit = field('104').get();
            field('102').assign(totalBB);
          }
          if(num==500){
            creditReceived = field('200').get();
            limitOnCredit = field('204').get();
            field('202').assign(totalBB);
          }
          field(num).getRows().reverse().forEach(function(row, rowIndex) {
            if (rowIndex == field(num).size().rows-1) {
              row[4].assign(creditReceived);
              row[5].assign(returnAmountApplied(limitOnCredit, row[4].get()));
            }else{
              row[5].assign(returnAmountApplied(limitOnCredit, row[3].get()));
              limitOnCredit-=returnAmountApplied(limitOnCredit, row[3].get());
            }
            if(rowIndex==0){//amount expired
                row[6].assign(row[3].get()-row[5].get())
            }
          });
          field(num).getRows().forEach(function(row, rowIndex) {
            row[2].assign(taxationYearTable.getRow(-rowIndex + 20)[6].get());
            row[7].assign(row[3].get()+row[4].get()-row[5].get()-row[6].get());
          })
      });
    })
  });
})();

