(function() {
  wpw.tax.create.tables('t2a3', {
    '400': {
      'hasTotals': true,
      fixedRows: true,
      infoTable: false,
      superHeaders: [
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
        },
        {
          colClass: 'qtr-col-width',
          header:'#120',//delete when tn is added to superHeaders
          tn:"120"

        },
        {
          colClass: 'std-input-width',
          header: '#122', //delete when tn is added to superHeaders
          tn:'122'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: '#124', //delete when tn is added to superHeaders
          tn:'124'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: '#125', //delete when tn is added to superHeaders
          tn:'125'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: '#126', //delete when tn is added to superHeaders
          tn:'126'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: '#128', //delete when tn is added to superHeaders
          tn:'128'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: '#130', //delete when tn is added to superHeaders
          tn:'130'
        },
        ],
      'columns': [
        {
          colClass: 'std-input-width',
          'type': 'none',
          cellClass: 'alignCenter',
          header: 'Year of origin'
        },
        {
          colClass: 'qtr-col-width',
          type:'none',
          cellClass: 'alignCenter',
          'disabled': true
        },
        {
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true,
          header: 'Tax Year End <br> YYYY/MM/DD',
          superheader:"test"
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: 'Investor Tax Credit balance at beginning of the year and transfers *'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: 'Investor Tax Credit received during the year'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: 'Investor Tax Credit applied to reduce tax payable',
          'total': true
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: 'Investor Tax Credit expired during the year',
          'total': true
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: 'Investor Tax Credit available for carry forward <br> 124 + 125 - 126 - 128',
          total: true,
          totalNum: '2001'
        },
      ],
      'cells': [
        {
          '0': {'label': 'Current tax year-ending on'},
          '1': {'label': '0'},
          '3': {type: 'none'},
          '6': {type: 'none'}
        },
        {
          '0': {'label': '1st preceding taxation year'},
          '1': {'label': '1'},
          '4': {type: 'none'},
          '6': {type: 'none'}
        },
        {
          '0': {'label': '2nd preceding taxation year'},
          '1': {'label': '2'},
          '4': {type: 'none'},
          '6': {type: 'none'}
        },
        {
          '0': {'label': '3rd preceding taxation year'},
          '1': {'label': '3'},
          '4': {type: 'none'},
          '6': {type: 'none'}
        },
        {
          '0': {label: '4th preceding taxation year'},
          '1': {'label': '4'},
          '4': {type: 'none'},
        }
      ]
    },
      //=========Capital Investment Tax Credit Calculation=============//

    '500': {
      fixedRows: true,
      hasTotals: true,
      infoTable: false,
      superHeaders: [
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
        },
        {
          colClass: 'qtr-col-width',
          header:'#220',//delete when tn is added to superHeaders
          tn:"220"

        },
        {
          colClass: 'std-input-width',
          header: '#222', //delete when tn is added to superHeaders
          tn:'222'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: '#224', //delete when tn is added to superHeaders
          tn:'224'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: '#225', //delete when tn is added to superHeaders
          tn:'225'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: '#226', //delete when tn is added to superHeaders
          tn:'226'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: '#228', //delete when tn is added to superHeaders
          tn:'228'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: '#230', //delete when tn is added to superHeaders
          tn:'230'
        },
      ],
      columns: [
        {
          colClass: 'std-input-width',
          'type': 'none',
          cellClass: 'alignCenter',
          header: 'Year of origin'
        },
        {
          colClass: 'qtr-col-width',
          type:'none',
          cellClass: 'alignCenter',
          'disabled': true
        },
        {
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true,
          header: 'Tax Year End <br> YYYY/MM/DD'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: 'Capital Investment Tax Credit balance at beginning of the and transfers**'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: 'Capital Investment Tax Credit received during the year'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: 'Capital Investment Tax Credit applied to reduce tax payable',
          'total': true
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          header: 'Capital Investment Tax Credit expired in the current year',
          'total': true
        },
        {
          colClass: 'std-input-width',
          'header': 'Capital Investment Tax Credit available for carry forward <br> 224 + 225 - 226 - 228',
          'total': true,
          'totalNum': '3001'
        },
      ],
      cells: [
        {
          '0': {'label': 'Current tax year-ending on'},
          '1': {'label': '0'},
          '3': {type: 'none'},
          '6': {type: 'none'}
        },
        {
          '0': {'label': '1st preceding taxation year'},
          '1': {'label': '1'},
          '4': {type: 'none'},
          '6': {type: 'none'}
        },
        {
          '0': {'label': '2nd preceding taxation year'},
          '1': {'label': '2'},
          '4': {type: 'none'},
          '6': {type: 'none'}
        },
        {
          '0': {'label': '3rd preceding taxation year'},
          '1': {'label': '3'},
          '4': {type: 'none'},
          '6': {type: 'none'}
        },
        {
          '0': {'label': '4th preceding taxation year'},
          '1': {'label': '4'},
          '4': {type: 'none'},
          '6': {type: 'none'}
        },
        {
          '0': {'label': '5th preceding taxation year'},
          '1': {'label': '5'},
          '4': {type: 'none'},
          '6': {type: 'none'}
        },
        {
          '0': {'label': '6th preceding taxation year'},
          '1': {'label': '6'},
          '4': {type: 'none'},
          '6': {type: 'none'}
        },
        {
          '0': {'label': '7th preceding taxation year'},
          '1': {'label': '7'},
          '4': {type: 'none'},
          '6': {type: 'none'}
        },
        {
          '0': {'label': '8th preceding taxation year'},
          '1': {'label': '8'},
          '4': {type: 'none'},
          '6': {type: 'none'}
        },
        {
          '0': {'label': '9th preceding taxation year'},
          '1': {'label': '9'},
          '4': {type: 'none'},
          '6': {type: 'none'}
        },
        {
          '0': {'label': '10th preceding taxation year'},
          '1': {'label': '10'},
          '4': {type: 'none'}
        }
      ]
    }
  })
})();
