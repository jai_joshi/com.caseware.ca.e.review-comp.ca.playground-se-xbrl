(function () {
  'use strict';
  wpw.tax.create.formData('t2a3', {
    formInfo: {
      abbreviation: 'AT3',
      title: 'ALBERTA OTHER TAX DEDUCTIONS AND CREDITS - AT1 SCHEDULE 3',
      schedule: 'Schedule 3',
      //code: '',
      showCorpInfo: true,
      //headerImage: 'canada-alberta',
      formFooterNum: 'AT3 (Sep-17)',
      category: 'Alberta Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: 'In order to be eligible for any deduction listed on this form, a corporation must have Investor Tax Credit Certificates or ' +
            'Capital Investment Tax Credit Certificates issued by the Minister of Economic Development and Trade.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'rows': [
          {
            label: 'Investor Tax Credit',
            labelClass: 'titleFont',
          },
          {
            'label': 'Total amounts shown on all Investor Tax Credit certificates issued to the corporation during the year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              }
            ]
          },
          {
            'label': 'Total Investor Tax Credit amount carried forward from prior year(s)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '102',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '102'
                }
              }
            ]
          },
          {
            'label': '<b>Amount applied to current taxation year</b> <br>[maximum credit is equal to AT1 page 2 line 068 - (lines 070 + 071 + 072 + 074)]',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              }
            ]
          },
          {
            'label': 'Total Investor Tax Credit Expired',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '106',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '106'
                }
              }
            ]
          },
          {
            'label': 'Amount available for carry forward (line 100 + line 102 - line 104 - line 106)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '108',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '108'
                }
              }
            ]
          },
          //================CAPITAL INVESTMENT TAX CREDIT===================//
          {
            label: ''
          },
          {
            label: 'Capital Investment Tax Credit',
            labelClass: 'titleFont',
          },
          {
            'label': 'Total amounts shown on all Capital Investment Tax Credit certificates issued to the corporation during the year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ]
          },
          {
            'label': 'Total Capital Investment Tax Credit amount carried forward from prior year(s)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '202',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '202'
                }
              }
            ]
          },
          {
            'label': '<b>Amount applied to current taxation year</b> (note: Investor Tax Credit must be fully utilized including any ' +
            'carry forward amounts before Capital Investment Tax Credit can be claimed)<br> ' +
            '[maximum credit is equal to AT1 page 2 line 068 - (lines 070 + 071 + 072 + 074) - Schedule 3 line 104]',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '204',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '204'
                }
              }
            ]
          },
          {
            'label': 'Total Capital Investment Tax Credit Expired',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '206',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '206'
                }
              }
            ]
          },
          {
            'label': 'Amount available for carry forward (line 200 + line 202 - line 204 - line 206)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '208',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '208'
                }
              }
            ]
          },
          //================Maximum Allowable Deduction===================//
          {
            label: ''
          },
          {
            label: 'Maximum Allowable Deduction',
            labelClass: 'titleFont',
          },
          {
            'label': '<b>Investor Tax Credit and Capital Investment Tax Credit</b> (line 104 + line 204)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '600',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '600'
                }
              }
            ]
          },
          {
            'label': 'From AT1 page 2, line 068 - (lines 070 + 071 + 072 + 074)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '602',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '602'
                }
              }
            ]
          },
          {
            'label': 'Total Deduction (Alberta Other Tax Deductions and Credits): <br> the lesser of amounts on line 600 and 602',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '604',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '604'
                }
              }
            ]
          },
          {
            label: '<b>Enter this amount on AT1 page 2, line 076</b>'
          }
        ]
      },

      //================Investor Tax Credit Calculation ===================//
      {
        'rows': [
          {
            label: 'Investor Tax Credit Calculation - Investor Tax Credit carry forward year of origin',
            labelClass: 'titleFont',
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '400'
          },
          {
            label: '* On eligible amalgamation under section 25.01(7) or eligible winding-up'+
            'of a subsidiary under section 25.01(8) of the <i>Alberta Corporate Tax Act</i>',
            labelClass: 'fullLength',
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: 'Capital Investment Tax Credit Calculation - Capital Investment Tax Credit carry forward year of origin',
            labelClass: 'titleFont',
          },
          {
            'type': 'table',
            'num': '500'
          },
          {
            label: '** On eligible amalgamation under section 25.02(7) or eligible winding-up '+
            'of a subsidiary under section 25.02(8) of the <i>Alberta Corporate Tax Act</i>',
            labelClass: 'fullLength',
          }
        ]
      }
    ]
  });
})();
