(function() {
  wpw.tax.global.tableCalculations.t2s33 = {
    "400": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          cellClass: 'alignCenter',
          colClass: 'std-input-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width'
        },
        {type: 'none', colClass: 'std-padding-width'},
        {
          cellClass: 'alignCenter',
          "type": "none"
        },
        {type: 'none', colClass: 'std-padding-width'},
        {
          colClass: 'std-input-width'
        },
        {type: 'none', colClass: 'std-padding-width'},
        {
          "type": "none",
          colClass: 'std-input-width'
        },
        {type: 'none', colClass: 'std-padding-width'},
        {colClass: 'std-input-width'},
        {type: 'none', colClass: 'std-padding-width'}
      ],
      "cells": [
        {
          "0": {
            "label": "Taxable capital for the year (line 500)"
          },
          "1": {
            "num": "600"
          },
          "2": {
            "label": " x "
          },
          "3": {
            "label": "Taxable income earned in Canada",
            cellClass: 'singleUnderline'
          },
          "4": {
            "tn": "610"
          },
          "5": {
            "num": "610",
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          "6": {
            "label": " = "
          },
          "7": {
            "label": "Taxable capital employed in Canada",
            "labelClass": "bold center"
          },
          "8": {
            // "num": "690"
            "tn": "690"
          },
          "9": {
            "num": "690",
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},

          }
        },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "Taxable income"
          },
          "5": {
            "num": "620"
          },
          "9": {
            "type": "none"
          }
        }
      ]
    },
    "700": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {"type": "none"},
        {type: 'none', colClass: 'small-input-width'},
        {type: 'none', colClass: 'std-input-width'},
        {colClass: 'std-input-width'},
        {type: 'none', colClass: 'small-input-width'},
        {colClass: 'std-input-width'},
        {type: 'none', colClass: 'std-padding-width'}
      ],
      "cells": [
        {
          "0": {
            "label": "<b>Calculation for purposes of the small business deduction </b>"
          },
          "2": {
            "label": "(amount H x"
          },
          "3": {
            "num": "840",
            decimals: 3
          },
          "4": {
            "label": " %) = "
          },
          "5": {
            "num": "830"
          },
          "6": {
            "label": "I"
          }
        }
      ]
    }
  }
})();