(function() {
  'use strict';

  wpw.tax.global.formData.t2s33 = {
      'formInfo': {
        'abbreviation': 'T2S33',
        'title': 'TAXABLE CAPITAL EMPLOYED IN CANADA - LARGE CORPORATIONS',
        //'subTitle': '(2010 and later tax years)',
        'schedule': 'Schedule 33',
        'code': 'Code 1402',
        formFooterNum: 'T2 SCH33 E (15)',
        'showCorpInfo': true,
        'description': [
          {
            'type': 'list',
            'items': [
                'Use this schedule in determining if the total taxable capital employed in Canada of the ' +
                'corporation (other than a financial institution or an insurance corporation) and ' +
                'its related corporations is greater than $10,000,000.',
                'If the total taxable capital employed in Canada of the corporation and its related corporations ' +
                'is greater than $10,000,000, file a completed Schedule 33 with your T2 Corporation Income ' +
                'Tax Return no later than six months from the end of the tax year.',
                'Unless otherwise noted, all legislative references are to the <i>Income Tax Act</i> and the ' +
                '<i>Income Tax Regulations</i>.',
                'Subsection 181(1) defines the terms <b>financial institution, long-term debt,</b> and ' +
                '<b>reserves.</b>',
                'Subsection 181(3) provides the basis to determine the carrying value of a corporation\'s ' +
                'assets or any other amount under Part I.3 for its capital, investment allowance, taxable ' +
                'capital, or taxable capital employed in Canada, or for a partnership in which it has an interest.',
                'If the corporation was a non-resident of Canada throughout the year and carried on a business ' +
                'through a permanent establishment in Canada, go to Part 4, <b>Taxable capital employed in Canada.</b>'
            ]
          }
        ],
        category: 'Federal Tax Forms'
      },
      'sections': [
        {
          'header': 'Part 1 - Capital',
          'rows': [
            {
              'label': '<b>Add</b> the following year-end amounts:',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Reserves that have not been deducted in calculating income for the year under Part I ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '101',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '101'
                  }
                },
                null
              ]
            },
            {
              'label': 'Capital stock (or members\' contributions if incorporated without share capital',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '103',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '103'
                  }
                },
                null
              ]
            },
            {
              'label': 'Retained earnings',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '104',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '104'
                  }
                },
                null
              ]
            },
            {
              'label': 'Contributed surplus',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '105',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '105'
                  }
                },
                null
              ]
            },
            {
              'label': 'Any other surpluses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '106',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '106'
                  }
                },
                null
              ]
            },
            {
              'label': 'Deferred unrealized foreign exchange gains',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '107',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '107'
                  }
                },
                null
              ]
            },
            {
              'label': 'All loans and advances to the corporation',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '108',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '108'
                  }
                },
                null
              ]
            },
            {
              'label': 'All indebtedness of the corporation represented by bonds, debentures, notes, mortgages, hypothecary claims, bankers\' acceptances, or similar obligations',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '109',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '109'
                  }
                },
                null
              ]
            },
            {
              'label': 'Any dividends declared but not paid by the corporation before the end of the year ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '110',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '110'
                  }
                },
                null
              ]
            },
            {
              'label': 'All other indebtedness of the corporation (other than any indebtedness for a lease) that has been outstanding for more than 365 days before the end of the year',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '111',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '111'
                  }
                },
                null
              ]
            },
            {
              'label': 'The total of all amounts, each of which is the amount, if any, in respect of a partnership in which the corporation held a membership interest at the end of the year, either directly or indirectly through  another partnership (see note below)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '112',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '112'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal (add lines 101 to 112)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '113'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '114'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'A'
            },
            {
              'label': '',
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Deduct </b> the following amounts:',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Deferred tax debit balance at the end of the year',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '121',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '121'
                  }
                },
                null
              ]
            },
            {
              'label': 'Any deficit deducted in calculating its shareholders\' equity (including, for this purpose, the amount of any provision for the redemption of preferred shares) at the end of the year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '122',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '122'
                  }
                },
                null
              ]
            },
            {
              'label': 'To the extent that the amount may reasonably be regarded  as being included in any of lines 101 to 112 above for the year, any amount deducted under subsection 135(1) in calculating income under Part I for the year',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '123',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '123'
                  }
                },
                null
              ]
            },
            {
              'label': 'Deferred unrealized foreign exchange losses at the end of the year',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '124',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '124'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal (add lines 121 to 124)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '125'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '126'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'B'
            },
            {
              'label': '<b> Capital for the year</b> (amount A <b>minus</b> amount B) (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '190',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '190'
                  }
                }
              ]
            },
            {
              'label': '',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note:',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Line 112 is determined by the formula (A - B) x C/D (as per paragraph 181.2(3)(g)) where:',
              'labelClass': 'fullLength'
            },
            {
              'label': 'A is the total of all amounts that would be determined for lines 101, 107, 108, 109, and 111 in respect of the partnership for its last fiscal period that ends at or before the end of the year if',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': '(a) those lines applied to partnerships in the same manner that they apply to corporations, and',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': '(b) those amounts were computed without reference to amounts owing by the partnership',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': '(i) to any corporation that held a membership interest in the partnership either directly or indirectly through another partnership, or',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': '(ii) to any partnership in which a corporation described in subparagraph (i) held a membership interest either directly or indirectly through another partnership.',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': 'B is the partnership\'s deferred unrealized foreign exchange losses at the end of the period,',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': 'C is the share of the partnership\'s income or loss for the period to which the corporation is entitled either directly or indirectly through another partnership, and',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': 'D is the partnership\'s income or loss for the period.',
              'labelClass': 'fullLength tabbed'
            }
          ]
        },
        {
          'header': 'Part 2 - Investment allowance',
          'rows': [
            {
              'label': '<b> Add </b> the carrying value at the end of the year of the following assets of the corporation:',
              'labelClass': 'fullLength'
            },
            {
              'label': 'A share of another corporation',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '401',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '401'
                  }
                }
              ]
            },
            {
              'label': 'A loan or advance to another corporation (other than a financial institution)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '402',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '402'
                  }
                }
              ]
            },
            {
              'label': 'A bond, debenture, note, mortgage, hypothecary claim, or similar obligation of another corporation (other than a financial institution)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '403',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '403'
                  }
                }
              ]
            },
            {
              'label': 'Long-term debt of a financial institution',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '404',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '404'
                  }
                }
              ]
            },
            {
              'label': 'A dividend payable on a share of the capital stock of another corporation',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '405',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '405'
                  }
                }
              ]
            },
            {
              'label': 'A loan or advance to, or a bond, debenture, note, mortgage, hypothecary claim or similar obligation of, a partnership each member of which was, throughout the year, another corporation (other than a financial institution) that was not exempt from tax under this Part (otherwise than because of paragraph 181.1(3)(d)), or another partnership described in paragraph 181.2(4)(d.1)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '406',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '406'
                  }
                }
              ]
            },
            {
              'label': 'An interest in a partnership (see note 2 below)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '407',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '407'
                  }
                }
              ]
            },
            {
              'label': ' <b> Investment allowance for the year </b> (add lines 401 to 407)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '490',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '490'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Notes:',
              'labelClass': 'bold fullLength'
            },
            {
              'label': '1. Lines 401 to 405 should not include the carrying value of a share of the capital ' +
              'stock of, a dividend payable by, or indebtedness of a corporation that is exempt from tax under' +
              ' Part I.3 (other than a non-resident corporation that at no time in the year carried on business in ' +
              'Canada through a permanent establishment).',
              'labelClass': 'fullLength'
            },
            {
              'label': '2. Where the corporation has an interest in a partnership held either directly or ' +
              'indirectly through another partnership, refer to subsection 181.2(5) for additional rules' +
              ' regarding the carrying value of an interest in a partnership.',
              'labelClass': 'fullLength'
            },
            {
              'label': '3. Where a trust is used as a conduit for loaning money from a corporation to another' +
              ' related corporation (other than a financial institution), the loan will be considered to ' +
              'have been made directly from the lending corporation to the borrowing corporation. Refer to ' +
              'subsection 181.2(6) for special rules that may apply.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 3- Taxable capital',
          'rows': [
            {
              'label': 'Capital for the year (line 190)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '495'
                  }
                }
              ],
              'indicator': 'C'
            },
            {
              'label': ' <b> Deduct: </b> Investment allowance for the year (line 490)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '498'
                  }
                }
              ],
              'indicator': 'D'
            },
            {
              'label': '<b> Taxable capital for the year</b> (amount C <b>minus</b> amount D) (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '500',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '500'
                  }
                }
              ]
            }
          ]
        },
        {
          'header': 'Part 4 - Taxable capital employed in Canada',
          'rows': [
            {
              'label': 'To be completed by a corporation that was resident in Canada at any time in the year',
              'labelClass': 'fullLength bold center'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '400'
            },
            {
              'label': 'Notes:',
              'labelClass': 'fullLength bold'
            },
            {
              'label': '1. Regulation 8601 gives details on calculating the amount of taxable income earned in Canada.',
              'labelClass': 'fullLength'
            },
            {
              'label': '2. Where a corporation\'s taxable income for a tax year is \'0\', it shall, for the purposes of the above calculation, be deemed to have a taxable income for that year of $1,000.',
              'labelClass': 'fullLength'
            },
            {
              'label': '3. In the case of an airline corporation, Regulation 8601 should be considered when completing the above calculation.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'horizontalLine'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> To be completed by a corporation that was a non-resident of Canada throughout the year and carried on a business through a permanent establishment in Canada',
              'labelClass': 'fullLength center'
            },
            {
              'label': 'Total of all amounts each of which is the carrying value at the end of the year of an asset' +
              ' of the corporation used in the year or held in the year, in the course of carrying on any business ' +
              'during the year through a permanent establishment in Canada ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '701',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '701'
                  }
                }
              ]
            },
            {
              'label': '<b> Deduct </b> the following amounts:',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Corporation\'s indebtedness at the end of the year [other than indebtedness described in any of paragraphs 181.2(3)(c) to (f)] that may reasonably be regarded as relating to a business it carried on during the year through a permanent establishment in Canada',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '711',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '711'
                  }
                },
                null
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total of all amounts each of which is the carrying value at the end of year of an asset described in subsection 181.2(4) of the corporation that it used in the year, or held in the year, in the course of carrying on any business during the year through a permanent establishment in Canada',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '712',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '712'
                  }
                },
                null
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total of all amounts each of which is the carrying value at the end of year of an asset of the corporation that is a ship or aircraft the corporation operated in international traffic, or personal or movable property used or held by the corporation in carrying on any business during the year through a permanent establishment in Canada (see note below)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '713',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '713'
                  }
                },
                null
              ]
            },
            {
              'label': 'Total deductions (<b>add</b> lines 711, 712, and 713)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '714'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '715'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'E'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Taxable capital employed in Canada</b> (line 701 <b>minus</b> amount E)' +
              ' (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '790',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '790'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Note: </b> Complete line 713 only if the country in which the corporation is resident did not impose a capital tax for the year on similar assets, or a tax for the year on the income from the operation of a ship or aircraft in international traffic, of any corporation resident in Canada during the year',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 5 – Calculation for purposes of the small business deduction',
          'rows': [
            {
              'label': 'This part is applicable to corporations that are not associated in the current year, but were associated in the prior year.',
              'labelClass': 'fullLength bold'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Taxable capital employed in Canada (amount from line 690)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '800'
                  }
                }
              ],
              'indicator': 'F'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Deduct:',
              'labelClass': 'bold',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '810'
                  }
                }
              ],
              'indicator': 'G'
            },
            {
              'label': 'Excess (amount F <b>minus</b> amount G) (if negative, enter "0")',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '820'
                  }
                }
              ],
              'indicator': 'H'
            },
            {
              'type': 'table',
              'num': '700'
            },
            {
              'label': 'Enter this amount at line 415 of the T2 return.',
              'labelClass': 'fullLength'
            }
          ]
        }
      ]
    };
})();
