(function() {
  wpw.tax.create.diagnostics('t2s33', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0330015', common.prereq(common.and(
        function(tools) {return tools.field('cp.tax_end').getKey('year') > 2013},
        common.check(['t2j.233'], 'isChecked')),
        common.requireFiled('T2S33')));

    diagUtils.diagnostic('0330020', common.prereq(common.requireFiled('T2S33'),
        function(tools) {
          return tools.requireOne(tools.list(['101', '103', '104', '105', '106', '107', '108', '109',
                '110', '111', '112']), 'isNonZero');
              //As of 2017-6 610 and 701 are used in the flagging contions
          // && tools.requireOne(tools.list(['610', '701']), 'isNonZero');
        }));

/*    diagUtils.diagnostic('0331000', common.prereq(function(tools) {
      common.requireFiled('T2S33');
    }, common.and(
        common.requireNotFiled('T2S34'),
        common.requireNotFiled('T2S35'))));*/
    //TODO: We don't have s34 or s35 forms in the system as of version 1.1
  });
})();