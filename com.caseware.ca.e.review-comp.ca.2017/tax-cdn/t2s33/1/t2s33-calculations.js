(function() {

  wpw.tax.create.calcBlocks('t2s33', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //Part 1
      //Transfer from GIFI/FS
      field('103').assign(
          calcUtils.gifiValue('t2s100', '300', 3500) +
          calcUtils.gifiValue('t2s100', '300', 3520)
      ); //3000
      field('103').source(field('t2s100.3500'));

      field('104').assign(Math.max(calcUtils.gifiValue('t2s100', null, 3849), 0)); //5000
      field('104').source(field('t2s100.3849'));

      field('105').assign(calcUtils.gifiValue('t2s100', '300', 3541)); //3000
      field('105').source(field('t2s100.3541'));

      field('106').assign(
          calcUtils.gifiValue('t2s100', '300', 3542) +
          calcUtils.gifiValue('t2s100', '300', 3543) +
          calcUtils.gifiValue('t2s100', '300', 3540)
      ); //10000
      field('106').source(field('t2s100.3542'));

      calcUtils.sumBucketValues('113', ['101', '103', '104', '105', '106', '107', '108', '109', '110', '111', '112']);
      calcUtils.equals('114', '113', true);
      calcUtils.sumBucketValues('125', ['121', '122', '123', '124']);
      calcUtils.equals('126', '125', true);
      calcUtils.subtract('190', '114', '126');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 2
      calcUtils.sumBucketValues('490', ['401', '402', '403', '404', '405', '406', '407']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 3
      calcUtils.equals('495', '190', true);
      calcUtils.equals('498', '490', true);
      calcUtils.subtract('500', '495', '498');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 4
      calcUtils.equals('600', '500', true);
      if (field('CP.750').get() != 'MJ') {
        field('610').assign(field('T2J.371').get());
        field('610').source(field('T2J.371'));
      }
      else {
        field('610').assign(field('T2S5.1031').get());
        field('610').source(field('T2S5.1031'));
      }
      calcUtils.getGlobalValue('615', 'T2S5', '1031');
      if (field('T2J.371').get() == 0) {
        field('620').assign(1000);
        field('610').assign(1000)
      }
      else {
        calcUtils.getGlobalValue('620', 'T2J', '371');
      }
      field('690').assign(
          field('600').get() *
          field('610').get() /
          field('620').get());
      calcUtils.sumBucketValues('714', ['711', '712', '713']);
      calcUtils.equals('715', '714', true);
      calcUtils.subtract('790', '701', '715');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 5 Calculation for purposes of the small business deduction
      if (field('CP.226').get() == 2 && field('CP.126').get() == 1) {
        calcUtils.equals('800', '695', true);
        calcUtils.getGlobalValue('810', 'ratesFed', '811');
        calcUtils.subtract('820', '800', '810');
        calcUtils.getGlobalValue('840', 'ratesFed', '322');
        calcUtils.multiply('830', ['820', '840'], 1 / 100, true);
      }
      else {
        field('800').disabled(true);
        field('810').disabled(true);
        field('820').disabled(true);
        field('800').disabled(true);
        field('840').disabled(true);
        field('830').disabled(true);

      }
    });
  });
})();
