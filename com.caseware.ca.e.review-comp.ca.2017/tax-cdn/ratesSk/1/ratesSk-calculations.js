(function () {

  wpw.tax.create.calcBlocks('ratesSk', function (calcUtils) {
    calcUtils.calc(function (calcUtils, field) {

      field('101').assign(0.6);
      field('102').assign(0.3);
      field('103').assign(0.15);
      field('111').assign(3.6);
      field('112').assign(3.3);
      field('113').assign(3.1);
      field('114').assign(3.0);
      field('120').assign(0.6);
      field('130').assign(2.0);
      field('131').assign(1.85);
      field('132').assign(1.75);
      field('133').assign(1.70);
      field('135').assign(10000000.0);
      field('140').assign(5000000.0);
      field('141').assign(7500000.0);
      field('142').assign(10000000.0);
      field('145').assign(100 / 6);
      field('146').assign(25.0);
      field('150').assign(100 / 8);
      field('151').assign(100000000.0);
      field('152').assign(2500000.0);
      field('155').assign(3.25);
      field('156').assign(0.7);
      field('160').assign(1000000000.0);
      field('161').assign(1500000000.0);
      field('162').assign(10000000.0);
      field('163').assign(7500000.0);
      field('164').assign(10000000.0);
      field('170').assign(400.0);
      field('171').assign(75.0);
      field('172').assign(350.0);
      field('173').assign(50.0);
      field('174').assign(525.0);
      field('175').assign(33.3333);
      field('176').assign(650.0);
      field('177').assign(0.0);
      field('180').assign(400.0);

      field('400').assign(90);
      field('401').assign(75);
      field('402').assign(65);
      field('403').assign(35);
      field('404').assign(0);
      field('410').assign(10);
      field('411').assign(25);
      field('412').assign(35);
      field('413').assign(65);
      field('414').assign(100);

      field('500').assign(7);
      field('501').assign(7);
      field('502').assign(5);
      field('503').assign(6);

      field('600').assign(1 / 3);
      field('601').assign(15);
      field('602').assign(10);
      field('603').assign(3000000);
      field('604').assign(1000000);
      field('605').assign(1000000);


      field('700').assign(4);

      field('701').assign(2);

      field('800').assign(2);
      field('801').assign(12);
      field('802').assign(11.5);
      field('803').assign(12);
      field('804').assign(600000);
      field('810').assign(75);
      field('811').assign(50);
      field('812').assign(25);

      var july2017 = wpw.tax.date(2017, 7, 1);
      var date2017Comparisons = calcUtils.compareDateAndFiscalPeriod(july2017);

      //days in tax year
      field('070').assign(date2017Comparisons.daysBeforeDate);
      field('080').assign(date2017Comparisons.daysAfterDate);
      //pro-rate
      // Notes: the last 2 months in 2017 are not supported for now
      field('090').assign(field('801').get() * field('070').get() / 366 + field('802').get() * field('080').get() / 366)
    });
  });
})();
