(function () {
  'use strict';

  wpw.tax.create.formData('ratesSk', {
    formInfo: {
      abbreviation: 'ratesSk',
      title: 'Rates Table - SK',
      showCorpInfo: true,
      category: 'Rates Tables'
    },
    sections: [
      {
        'header': 'SCT 1 - Corporation capital tax return',
        'rows': [
          {
            'label': '<b>Capital tax payable</b>'
          },
          {
            'label': 'General corporations rate',
            'labelClass': 'tabbed'
          },
          {
            'label': 'Taxation year',
            'labelClass': 'tabbed'
          },
          {
            'label': '2004-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '101',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2006-07-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '102',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2007-07-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '103',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '<br>Resource surcharge rate for general corporations',
            'labelClass': 'tabbed'
          },
          {
            'label': 'Taxation year',
            'labelClass': 'tabbed'
          },
          {
            'label': '2004-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '111',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2006-07-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '112',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2007-07-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '113',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2008-07-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '114',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '<br>Crown corporations rate',
            'labelClass': 'tabbed'
          },
          {
            'label': 'Taxation year',
            'labelClass': 'tabbed'
          },
          {
            'label': '2004-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '120',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '<br>Resource surcharge rate for all oil and gas wells',
            'labelClass': 'tabbed'
          },
          {
            'label': 'Taxation year',
            'labelClass': 'tabbed'
          },
          {
            'label': '2004-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '130',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2006-07-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '131',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2007-07-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '132',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2008-07-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '133',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '<br><b>Taxable paid-up capital</b>'
          },
          {
            'label': 'Standard exemption',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '135'
                }
              }
            ]
          },
          {
            'label': '<br>Additional exemption',
            'labelClass': 'tabbed'
          },
          {
            'label': 'Taxation year',
            'labelClass': 'tabbed',
            'input2Header': 'Deduction amount'
          },
          {
            'label': '2002-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '140'
                }
              }
            ]
          },
          {
            'label': '2004-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '141'
                }
              }
            ]
          },
          {
            'label': '2005-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '142'
                }
              }
            ]
          },
          {
            'label': '<br><b>Goodwill allowance</b>'
          },
          {
            'label': 'Capitalized value',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '145',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Goodwill allowance rate',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '146',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '<br><b>Taxable capital employed in Canada</b>'
          },
          {
            'label': 'Capitalized value',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '150',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': '<br><b>Resources sales deduction</b>'
          },
          {
            'label': 'Maximum limit of assets',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '151'
                }
              }
            ]
          },
          {
            'label': 'Maximum deduction',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '152'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'SCT B1 - Corporation capital tax return - Financial institution',
        'rows': [
          {
            'label': '<b>Capital tax payable</b>'
          },
          {
            'label': 'Financial institution rate',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '155',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Small financial institution rate',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '156',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '<br><b>Small financial institutions threshold</b>'
          },
          {
            'label': '2003-10-31   Taxation year-ending on or after October 31, 2003, and before October 31, 2008',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '160'
                }
              }
            ]
          },
          {
            'label': '2008-10-31   Taxation year-ending on or after October 31, 2008',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '161'
                }
              }
            ]
          },
          {
            'label': '<br><b>Taxable paid-up capital</b>'
          },
          {
            'label': 'Standard exemption',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '162'
                }
              }
            ]
          },
          {
            'label': '<br>Additional exemption',
            'labelClass': 'tabbed'
          },
          {
            'label': 'Taxation year',
            'labelClass': 'tabbed',
            'input2Header': 'Exemption'
          },
          {
            'label': '2004-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '163'
                }
              }
            ]
          },
          {
            'label': '2005-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '164'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Political contributions tax credit',
        'rows': [
          {
            'label': 'Bracket 1',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '170',
                  'decimals': 1
                }
              },
              {
                'input': {
                  'num': '171',
                  'decimals': 1
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Bracket 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '172',
                  'decimals': 1
                }
              },
              {
                'input': {
                  'num': '173',
                  'decimals': 1
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Bracket 3',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '174',
                  'decimals': 1
                }
              },
              {
                'input': {
                  'num': '175',
                  'decimals': 1
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Maximum allowable credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '176'
                }
              },
              {
                'input': {
                  'num': '177'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Tax instalments',
        'rows': [
          {
            'label': 'Instalments threshold amount',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '180'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'S400 - SK Royalty Tax Rebate Calculation',
        'rows': [
          {
            'label': 'Percentage of Crown royalties and taxes on resource production prohibited from deduction, and percentage of resource allowance allowed as a deduction',
            'labelClass': 'bold fullLength'
          },
          {
            'label': '2003',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '400'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2004',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '401'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2005',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '402'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2006',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '403'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'After 2006',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '404'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Deductible percentage of Crown royalties and mining taxes',
            'labelClass': 'bold fullLength'
          },
          {
            'label': '2003',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '410'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2004',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '411'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2005',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '412'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2006',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '413'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'After 2006',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '414'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'S402 - SK Manufacturing and Processing Investment Tax Credit',
        'rows': [
          {
            'label': 'Qualified property acquired in the current tax year after March 31, 2004, and before April 7, 2006:',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '500'
                }
              }
            ]
          },
          {
            'label': 'Qualified property acquired in the current tax year:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'After April 6, 2006, and before October 28, 2006 (amount C from Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '501'
                }
              }
            ]
          },
          {
            'label': 'After October 27, 2006',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '502'
                }
              }
            ]
          },
          {
            'label': 'After March 22, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '503'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'S403 - SK Research and Development Tax Credit',
        'rows': [
          {
            'label': 'Expenditure limit after March 31, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '600',
                  'decimals': 5
                }
              }
            ]
          },
          {
            label: 'Refundable credit limit:',
            labelClass: 'bold'
          },
          {
            'label': 'Before April 1, 2015',
            'layout': 'alignInput',
            labelCellClass: 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '601'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'After March 31, 2017',
            'layout': 'alignInput',
            labelCellClass: 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '602'
                }
              }
            ],
            'indicator': '%'
          },
          {
            label: 'Expenditure limit',
            labelClass: 'bold fullLength'
          },
          {
            'label': 'Before April 1, 2015',
            'layout': 'alignInput',
            labelCellClass: 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '603'
                }
              }
            ]
          },
          {
            'label': 'After March 31, 2017',
            'layout': 'alignInput',
            labelCellClass: 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '604'
                }
              }
            ]
          },
          {
            label: 'Non-refundable credit limit:',
            labelClass: 'bold'
          },
          {
            'label': 'After March 31, 2017',
            'layout': 'alignInput',
            labelCellClass: 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '605'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'S404 - SK Manufacturing and Processing Profits Tax Reduction',
        'rows': [
          {
            'label': 'Federal foreign business income tax credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '700'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Part 2 - SK manufacturing and processing profits tax reduction',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '701'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'S411 - SK Corporation Tax Calculation',
        'rows': [
          {
            'label': 'SK tax at the lower rate from 2014 to present: ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '800'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'SK tax at the higher rate from 2014 to July 1, 2017: ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '801'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'SK tax at the higher rate after June 30, 2017: ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '802',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'SK tax at the higher rate after June 30, 2019: ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '803',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Small business income tax threshold effective after December 31, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '804'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount F calculation',
            'labelClass': 'fullLength'
          },
          {
            "label": "For days in the tax year in 2017",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true,
              "paddingRight": false
            },
            "columns": [
              {
                "input": {
                  "num": "810"
                }
              }
            ],
            "indicator": "%"
          },
          {
            "label": "For days in the tax year in 2018",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true,
              "paddingRight": false
            },
            "columns": [
              {
                "input": {
                  "num": "811"
                }
              }
            ],
            "indicator": "%"
          },
          {
            "label": "For days in the tax year in 2019",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true,
              "paddingRight": false
            },
            "columns": [
              {
                "input": {
                  "num": "812"
                }
              }
            ],
            "indicator": "%"
          }
        ]
      },
      {
        'header': 'S21W - Provincial or Territorial Foreign Income Tax Credits and Federal Logging Tax Credit',
        'rows': [
          {
            'label': 'Number of days in the tax year after January 1, 2015 and before July 1, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '070'
                }
              }
            ]
          },
          {
            'label': 'Number of days in the tax year after July 1, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '080'
                }
              }
            ]
          },
          {
            'label': 'Prorated provincial or territorial tax rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '090',
                  'decimals': 4
                }
              }
            ],
            'indicator': '%'
          }
        ]
      }
    ]
  });
})();
