(function() {
  wpw.tax.create.diagnostics('t2s73', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S73'), forEach.row('1000', forEach.bnCheckCol(0, true))));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S73'), forEach.row('1005', forEach.bnCheckCol(0, true))));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S73'), forEach.row('1010', forEach.bnCheckCol(0, true))));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S73'), forEach.row('1020', forEach.bnCheckCol(0, true))));

    //Diagnostic will never trigger because formflagger will always flag t2s73 if one these values ('t2s1.130', 't2s1.131') are greater than 0
    diagUtils.diagnostic('0730010', common.prereq(common.check(['t2s1.130', 't2s1.131'], 'isNonZero'), common.requireFiled('T2S73')));

    diagUtils.diagnostic('0730020', common.prereq(common.requireFiled('T2S73'),
        function(tools) {
          var table = tools.mergeTables(tools.field('1000'), tools.field('1005'));
          table = tools.mergeTables(table, tools.field('1010'));
          table = tools.mergeTables(table, tools.field('1020'));
          return tools.checkAll(table.getRows(), function(row) {
            var cols = [row[1], row[2], row[3], row[4], row[5], row[7],
              row[8], row[9], row[10], row[11], row[12], row[14], row[15],
              row[16], row[17], row[18], row[19], row[20], row[22], row[23]];
            if (tools.checkMethod(cols, 'isNonZero'))
              return tools.requireOne(row[0], 'isNonZero');
            else return true;
          });
        }));
  });
})();
