(function () {

  var t2s71FieldIdsArr = ['101', '313', '314', '315', '330', '428', '434', '210', '503', '620', '715', '780'];
  var t2s72FieldIdsArr = ['101', '369', '370', '371', '385', '437', '443', '219', '503', '627', '714', '780'];
  var tableNumArray = ['1005', '1010', '1020'];

  wpw.tax.create.calcBlocks('t2s73', function (calcUtils) {

    calcUtils.calc(function (calcUtils) {
      var num = '321';
      var num2 = '322';
      var midnum = '324';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get();
      calcUtils.field(num2).assign(product);
    });

    //link s71 to table 1000
    calcUtils.calc(function (calcUtils, field) {
      calcUtils.filterLinkedTable('t2s71', '1000', t2s71FieldIdsArr, function () {
        var _arguments = arguments;
        var ret = false;
        Object.keys(_arguments).forEach(function (index) {
          if (_arguments[index] !== 0 && !wpw.tax.utilities.isEmptyOrNull(_arguments[index])) {
            ret = true;
          }
        });
        return ret;
      });
    });

    //link s72 to table 1000
    calcUtils.calc(function (calcUtils, field) {
      calcUtils.filterLinkedTable('t2s72', '1000', t2s72FieldIdsArr, function () {
        var _arguments = arguments;
        var ret = false;
        Object.keys(_arguments).forEach(function (index) {
          if (_arguments[index] !== 0 && !wpw.tax.utilities.isEmptyOrNull(_arguments[index])) {
            ret = true;
          }
        });
        return ret;
      });
    });

    //link table 1000 to other tables in S73
    calcUtils.calc(function (calcUtils, field) {
      var mainTableNum = '1000';
      tableNumArray.forEach(function (tableNum) {
        calcUtils.filterTableRow(field(tableNum), function (table, rowIndex) {
          // We return false to delete this row if there is no link
          return !calcUtils.checkLinkDeleted(table, rowIndex);
        });

        calcUtils.fillAndLinkTable(field(tableNum), field(mainTableNum));

        calcUtils.forEachLinkedRow(field(tableNum), function (mainTable, mainRowInd, linkedTable, linkedRowInd) {
          var mainRow = mainTable.getRow(mainRowInd);
          var linkRow = linkedTable.getRow(linkedRowInd);
          mainRow[0].assign(linkRow[0].get());
        });
      });
    });

    //set value for tn120, tn150 and other link table
    calcUtils.calc(function (calcUtils, field, form) {
      var table1000 = field('1000');
      var table1005 = field('1005');
      var table1010 = field('1010');
      var totalFields = [0, 0, 0, 0, 0];

      //Check if linked rows are created first
      if (table1000.size().rows == table1005.size().rows) {
        table1000.getRows().forEach(function (row, rIndex) {
          var linkedForm = table1000.getRowInfo(rIndex).linkedForm;
          var repeatId = table1000.getRowInfo(rIndex).repeatId;
          var formId = linkedForm + '-' + repeatId;
          var tn120Val = form(formId).field('103').get();

          row[2].assign(tn120Val);
          row[5].assign(linkedForm === 't2s71' ? '2' : '1');

          var isS71 = (linkedForm === 't2s71');
          //calcs for each link table
          var table1005Row = table1005.getRow(rIndex);
          var table1010Row = table1010.getRow(rIndex);
          //mapping for s73 table to know which fields it should grab from s71 or s72
          var mappingArrObj = {
            '1005': [
              {colIndex: 1, s71FieldId: '313', s72FieldId: '369'},
              {colIndex: 2, s71FieldId: '314', s72FieldId: '370'},
              {colIndex: 3, s71FieldId: '315', s72FieldId: '371'},
              {colIndex: 5, s71FieldId: '330', s72FieldId: '385'},
              {colIndex: 6, s71FieldId: '428', s72FieldId: '437'}
            ],
            '1010': [
              {colIndex: 1, s71FieldId: '434', s72FieldId: '443'},
              {colIndex: 2, s71FieldId: '210', s72FieldId: '219'},
              {colIndex: 3, s71FieldId: '503', s72FieldId: '503'},
              {colIndex: 4, s71FieldId: '620', s72FieldId: '627'},
              {colIndex: 5, s71FieldId: '715', s72FieldId: '714'},
              {colIndex: 6, s71FieldId: '780', s72FieldId: '780'}
            ],
            '1020': [
              {colIndex: 1, s71FieldId: '831', s72FieldId: '835'},
              {colIndex: 2, s71FieldId: '834', s72FieldId: '832'}
            ]
          };

          for (var tableNum in mappingArrObj) {
            mappingArrObj[tableNum].forEach(function (mappingObj) {
              var sourceField = isS71 ? form(formId).field(mappingObj.s71FieldId) : form(formId).field(mappingObj.s72FieldId);
              field(tableNum).getRow(rIndex)[mappingObj.colIndex].assign(sourceField.get());
              field(tableNum).getRow(rIndex)[mappingObj.colIndex].source(sourceField);
            });
          }

          table1005Row[4].assign(Math.max(0,
              table1005Row[1].get() -
              table1005Row[2].get() -
              table1005Row[3].get()));

          table1010Row[7].assign(Math.max(0,
              table1005Row[4].get() -
              table1005Row[5].get() +
              table1005Row[6].get() -
              table1010Row[1].get() -
              table1010Row[5].get() +
              table1010Row[6].get()));


          totalFields[0] += field(formId + '.tn270').get();
          totalFields[1] += field(formId + '.tn275').get();
          totalFields[2] += field(formId + '.tn280').get();
          totalFields[3] += field(formId + '.tn285').get();
          totalFields[4] += field(formId + '.tn290').get();
        });
      }
      field('270').assign(totalFields[0]);
      field('275').assign(totalFields[1]);
      field('280').assign(totalFields[2]);
      field('285').assign(totalFields[3]);
      field('290').assign(totalFields[4]);
    });

    calcUtils.calc(function (calcUtils, field, form) {
      //part 3
      field('310').assign(Math.max(field('1020').total(1).get(), 0));
      field('311').assign(field('310').get());
      field('312').assign(Math.min(field('1020').total(1).get(), field('1020').total(2).get()));
      field('321').assign(Math.max(field('310').get() - field('312').get(), 0));
      calcUtils.getGlobalValue('324', 'ratesFed', '324-73');
      field('320').assign(field('321').get() * field('324').get());
      field('323').assign(Math.max(field('311').get() + field('320').get(), 0));
    });
  });
})();
