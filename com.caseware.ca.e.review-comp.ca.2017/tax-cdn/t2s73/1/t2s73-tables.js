(function() {
  wpw.tax.global.tableCalculations.t2s73 = {
    'di_table_1': {
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          disabled: true,
          'textAlign': 'center'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Subtotal (amount a <b>minus</b> amount b) (if negative, enter "0")',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '321',
            'decimals': 1,
            'cellClass': ' doubleUnderline'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '324', decimals: 1
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': '320'
          },
          '7': {
            'num': '320',
            'decimals': 1
          },
          '8': {
            'label': 'B'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    "1000": {
      "showNumbering": true,
      "infoTable": false,
      initRows: 0,
      "fixedRows": true,
      "columns": [
        {
          "header": "Partnership account number",
          type: 'custom',
          format: ['{N9}RZ{N4}', 'NR'],
          "tn": "100",
          colClass: 'std-input-col-width',
          linkedFieldId: '102'
        },
        {
          "header": "Partnership's name",
          "tn": "110",
          "maxLength": 175,
          "type": "text",
          linkedFieldId: '101'
        },
        {
          "header": "Alignment election<br>1 = yes<br>2 = no<br>(see note 1)",
          "tn": "120",
          "maxLength": 1,
          colClass: 'small-input-width',
          type: 'dropdown',
          options: [
            {value: '1', option: '1'},
            {value: '2', option: '2'}
          ]
        },
        {
          "header": "Fiscal period-start<br>Year-Month-Day",
          "tn": "130",
          "type": "date",
          colClass: 'std-input-width',
          linkedFieldId: '106'
        },
        {
          "header": "Fiscal period-end<br>Year-Month-Day",
          "tn": "140",
          "type": "date",
          colClass: 'std-input-width',
          linkedFieldId: '107'
        },
        {
          "header": "Part of a<br>multi-tiered structure <br>1 = yes <br>2 = no",
          "tn": "150",
          "maxLength": 1,
          colClass: 'small-input-width',
          type: 'dropdown',
          options: [
            {value: '1', option: '1'},
            {value: '2', option: '2'}
          ]
        }
      ]
    },
    "1005": {
      "showNumbering": true,
      "fixedRows": true,
      hasTotals: true,
      "hiddenTotals": true,
      "infoTable": false,
      initRows: 0,
      "columns": [
        {
          "header": "Partnership account number<br>from Part 1",
          type: 'custom',
          format: ['{N9}RZ{N4}', 'NR']
        },
        {
          "header": "Stub period accrual<br><br><br><br><br>(see note 2)",
          "tn": "200",
          filters: 'prepend $',
          "maxLength": 13,
          total: true,
          "indicator": "1",
          colClass: 'std-input-width',
          totalMessage: 'Totals:'
        },
        {
          "header": "Designated qualified resource expenses<br><br><br>(see note 3)",
          "tn": "205",
          filters: 'prepend $',
          "maxLength": 13,
          total: true,
          "indicator": "2",
          colClass: 'std-input-width'
        },
        {
          "header": "Discretionary amount designated<br><br><br><br>(see note 4)",
          "tn": "210",
          filters: 'prepend $',
          "maxLength": 13,
          total: true,
          "indicator": "3",
          colClass: 'std-input-width'
        },
        {
          "header": "Adjusted stub period accrual (column 1 minus column 2 minus column 3)<br><br><br>",
          "tn": "215",
          filters: 'prepend $',
          "maxLength": 13,
          total: true,
          "indicator": "4",
          colClass: 'std-input-width'
        },
        {
          "header": "Previous-year adjusted stub period accrual<br><br><br><br>(see note 5)",
          "tn": "220",
          filters: 'prepend $',
          "maxLength": 13,
          total: true,
          "indicator": "5",
          colClass: 'std-input-width'
        },
        {
          "header": "Income inclusion for a new corporate member of a partnership <br><br>(see note 6)",
          "tn": "225",
          filters: 'prepend $',
          "maxLength": 13,
          total: true,
          "indicator": "6",
          colClass: 'std-input-width'
        }
      ]
    },
    "1010": {
      "showNumbering": true,
      "infoTable": false,
      hasTotals: true,
      "fixedRows": true,
      initRows: 0,
      "columns": [
        {
          "header": "Partnership account number<br>from Part 1",
          type: 'custom',
          format: ['{N9}RZ{N4}', 'NR']
        },
        {
          total: true,
          "header": "Previous-year income inclusion for a new corporate member of a partnership (see note 7)<br>",
          "tn": "230",
          filters: 'prepend $',
          "maxLength": 13,
          "indicator": "7",
          totalMessage: 'Totals:'
        },
        {
          total: true,
          "header": "Eligible alignment income<br><br><br><br>(see note 8)",
          "tn": "235",
          filters: 'prepend $',
          "maxLength": 13,
          "indicator": "8"
        },
        {
          total: true,
          "header": "Qualifying transitional income (QTI)<br><br><br><br>(see note 9)",
          "maxLength": 13,
          filters: 'prepend $',
          "tn": "240",
          "indicator": "9"
        },
        {
          total: true,
          "header": "Adjusted amount of QTI)<br><br><br><br>(see note 10)",
          "tn": "245",
          filters: 'prepend $',
          "maxLength": 13,
          "indicator": "10"
        },
        {
          total: true,
          "header": "Current-year transitional reserve <br><br><br><br>(see note 11)",
          "tn": "250",
          filters: 'prepend $',
          "maxLength": 13,
          "indicator": "11"
        },
        {
          total: true,
          "header": "Previous-year transitional reserve <br><br><br><br>(see note 12)",
          "tn": "255",
          filters: 'prepend $',
          "indicator": "12",
          "maxLength": 13
        },
        {
          total: true,
          "header": "Net amount to be included in income for the tax year <br><br><br>(see note 13)",
          "tn": "260",
          filters: 'prepend $',
          "indicator": "13",
          "maxLength": 13
        }
      ]
    },
    "1015": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-col-width',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignLeft'
        },
        {
          "type": "none",
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-col-width',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignLeft'
        },
        {
          "type": "none",
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-col-width',
          cellClass: 'alignRight'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Total active business income"
          },
          "1": {
            "num": "270",
            "tn": "270",
            filters: 'prepend $',
            "maxLength": 13
          },
          "3": {
            "label": "Total taxable capital gain"
          },
          "4": {
            "num": "275",
            "maxLength": 13,
            filters: 'prepend $',
            "tn": "275"
          },
          "6": {
            "label": "Total property income"
          },
          "7": {
            "num": "280",
            "maxLength": 13,
            filters: 'prepend $',
            "tn": "280"
          }
        },
        {
          "0": {
            "label": "Total allowable capital loss"
          },
          "1": {
            "num": "285",
            "maxLength": 13,
            filters: 'prepend $',
            "filters": "",
            "tn": "285"
          },
          "3": {
            "label": "Total other income"
          },
          "4": {
            "num": "290",
            "maxLength": 13,
            filters: 'prepend $',
            "tn": "290"
          },
          "7": {
            "type": "none"
          }
        }]
    },
    "1020": {
      "showNumbering": true,
      "infoTable": false,
      hasTotals: true,
      "fixedRows": true,
      initRows: 0,
      "maxLoop": 100000,
      paddingRight: 'std-input-col-padding-width',
      "columns": [
        {
          "header": "Partnership account number<br>(from Part 1)",
          type: 'custom',
          format: ['{N9}RZ{N4}', 'NR']
        },
        {
          total: true,
          "header": "Income shortfall adjustment<br>(see note 14)",
          "tn": "300",
          "maxLength": 13,
          filters: 'prepend $',
          "indicator": "14",
          colClass: 'std-input-width',
          totalMessage: 'Totals:'
        },
        {
          total: true,
          "header": "Threshold amount <br>(see note 15)",
          "tn": "310",
          "indicator": "15",
          filters: 'prepend $',
          "maxLength": 13,
          colClass: 'std-input-width'
        }
      ]
    }
  }
})();
