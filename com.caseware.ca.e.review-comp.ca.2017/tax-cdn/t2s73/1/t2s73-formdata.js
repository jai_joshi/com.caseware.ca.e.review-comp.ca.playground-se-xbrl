(function() {
  'use strict';

  wpw.tax.global.formData.t2s73 = {
      'formInfo': {
        'abbreviation': 't2s73',
        'title': 'Income Inclusion Summary for Corporations that are Members of Partnerships',
        //TODO: DO NOT DELETE THIS LINE: subtitle
        //'subTitle': ' (2011 and later tax years)',
        'schedule': 'Schedule 73',
        'showCorpInfo': true,
        code: 'Code 1104',
        formFooterNum: 'T2 SCH 73 E (15)',
        headerImage: 'canada-federal',
        category: 'Federal Tax Forms',
        'description': [
          {text: ''},
          {
            type: 'list',
            items: [
              {
                label: 'Use this schedule to calculate the income inclusion resulting from limiting' +
                ' the deferral of tax on income earned through a partnership under sections 34.2 and ' +
                '34.3, and amended section 249.1 of the <i>Income Tax Act</i>. If applicable, complete ' +
                'Part 3 of this schedule to calculate the income shortfall inclusion and additional' +
                ' amount.'
              },
              {
                label: 'This schedule is a summary of the amounts calculated on Schedule 71,<i> Income ' +
                'Inclusion for Corporations that are Members of Single-Tier Partnerships</i>, and ' +
                'Schedule 72,<i> Income Inclusion for Corporations that are Members of Multi-Tier ' +
                'Partnerships</i>.'
              },
              {
                label: 'This schedule does not replace the law. For more information, see sections 34.2 and 34.3 ' +
                'of the Income Tax Act, and the explanatory notes at <b>www.fin.gc.ca</b>.'
              },
              {
                label: '<b>If the corporation reported previous-year amounts of stub period accrual, alignment ' +
                'income, or transitional reserve on Schedule 1, complete a Schedule 73 for the year, and file it' +
                ' separately.</b>'
              },
              {label: 'All legislative references are to the <i>Income Tax Act</i>.'},
              {label: 'If you need more space, attach additional schedules'},
              {
                label: 'Include a completed copy of this schedule with your <i>T2 Corporation ' +
                'Income Tax Return</i>'
              }
            ]
          }
        ]
      },
      'sections': [
        {
          'forceBreakAfter': true,
          'header': 'Part 1 - Partnership information',
          'rows': [
            {
              'type': 'table',
              'num': '1000'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 1. Enter 1 (yes) if the corporation elected under subsection 249.1(8) or 249.1(9) to end the fiscal period of the partnership on a particular day, or if a valid multi-tier alignment election is deemed to have been made under subsection 249.1(11). Enter 2 (no) if no election was made or deemed to be made.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'forceBreakAfter': true,
          'header': 'Part 2 - Income inclusion',
          'rows': [
            {
              'type': 'table',
              'num': '1005'
            },
            {
              'type': 'table',
              'num': '1010'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b>Generally, amounts included under subsections 34.2(2), 34.2(3) and 34.2(12) or claimed under subsections 34.2(4) and 34.2(11) are deemed to have the same character and be in the same proportions as the partnership income to which they relate.</b> Enter the breakdown of the total of column 13 on lines 270, 275, 280, 285 and 290 below. For lines 270, 280, and 290, amounts can be negative.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '1015'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 2. Enter amount I from Part 3 of Schedule 71, or amount I, O, or S, ' +
              'whichever applies, from Part 3 of Schedule 72.',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 3. Enter amount j from Part 3 of Schedule 71, or amount t from Part 3 of Schedule 72.',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 4. Enter amount k from Part 3 of Schedule 71, or amount u from Part 3 of Schedule 72.',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 5. Enter amount K from Part 3 of previous year\'s Schedule 71, or amount V ' +
              'from Part 3 of previous year\'s Schedule 72.',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 6. Enter amount N from Part 4 of Schedule 71, or amount Y from Part 4 of Schedule 72.',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 7. Enter amount N from Part 4 of previous year\'s Schedule 71, or amount Y ' +
              'from Part 4 of previous year\'s Schedule 72.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 8. Enter amount E from Part 2 of Schedule 71, or enter amount E from Part 2 of ' +
              'Schedule 72. Enter this amount only once, for the year in which eligible alignment income arises. ' +
              '<b>Do not</b> enter an amount for other years. The eligible alignment income is included in the ' +
              'QTI to calculate the transitional reserve, but it is not included in the income inclusion under ' +
              'subsection 34.2(2).',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 9. Enter amount Q from Part 5 of Schedule 71, or amount BB from Part 5 of Schedule 72.' +
              ' Enter this amount only once, for the year in which the QTI arises. Do not enter an amount for other ' +
              'years. If an adjusted amount of QTI was calculated this year, see column 10.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 10. If the QTI is adjusted in this tax year, enter amount Y from Part 6 of this ' +
              'year\'s Schedule 71, or amount LL from Part 6 of this year\'s Schedule 72.'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 11. Enter amount EE from Part 7 of Schedule 71, or amount QQ from Part 7 of Schedule 72.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 12. Enter amount EE from Part 7 of previous year\'s Schedule 71, or amount QQ ' +
              'from Part 7 of previous year\'s Schedule 72.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 13. <b>Add</b> columns 4, 6, and 12, and <b>subtract</b> columns 5, 7, and 11. Enter ' +
              'the total of lines 270, 280, and 290 on line 130 of Schedule 1. Use the amount from line 275 in the' +
              ' calculation of line 899 of Schedule 6. Use the amount from line 285 in the calculation of line' +
              ' 901 of Schedule 6.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'forceBreakAfter': true,
          'header': 'Part 3 - Income shortfall adjustment and additional amount',
          'spacing': 'R_mult_tn2_complete',
          'rows': [
            {
              'label': 'For a tax year, the corporation is required under section 34.3 to include in ' +
              'its income an <b>income shortfall adjustment</b> to account for under-reported income ' +
              'in circumstances where the corporation <b>has made</b> a discretionary designation, for ' +
              'any <b>qualifying partnership</b> it is a member of, to reduce the adjusted stub period accrual ' +
              '(ASPA) inclusion for a base year. The base year is the preceding tax year of the corporation in ' +
              'which began the fiscal period of the qualifying partnership that ends in the corporation\'s tax year',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Where the corporation has QTI, the corporation has to include in its income an income ' +
              'shortfall adjustment only for tax years that are after the first tax year of the corporation to ' +
              'which the adjustment of QTI applied * for any qualifying partnership.',
              'labelClass': 'fullLength bold'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'When the corporation is a member of more than one qualifying partnership, it can, in ' +
              'determining its income inclusion under section 34.3 for a tax year, offset an over-reported ASPA ' +
              'in respect of a qualifying partnership against an under-reported ASPA of another qualifying partnership.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b>A qualifying partnership</b> is a partnership that has a fiscal period that began in ' +
              'the previous tax year and ended in the tax year, and in respect of which the corporation had to ' +
              'calculate an ASPA for the previous tax year in which the fiscal period of the partnership began.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '1020'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total of column 14 (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '309'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'a'
                  }
                },
                {
                  'input': {
                    'num': '311'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'A'
            },
            {
              'label': 'Deduct:',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Total of column 14 or, column 15, whichever is less',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  underline: 'single',
                  'input': {
                    'num': '312'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'b'
                  }
                },
                {}
              ]
            },
            {
              'type': 'table',
              'num': 'di_table_1'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b>Income shortfall adjustment and additional amount</b> (amount A <b>plus</b> amount B) ' +
              '(if negative enter "0") ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '323'
                  }
                }
              ],
              'indicator': 'C'
            },
            {
              'label': 'Enter amount C on line 131 of Schedule 1.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 14. Enter amount RR from Part 8 of Schedule 71, or amount DDD from Part 8 of Schedule 72',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 15. Enter amount SS from Part 8 of Schedule 71, or amount EEE from Part 8 of Schedule 72',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              label: '* When the conditions in subsection 34.2(16) are met, the adjustment of QTI applies, even if ' +
              'the recalculation of the QTI results in no adjustment to the amount of QTI. ',
              labelClass: 'fullLength'
            }
          ]
        }
      ]
    };
})();
