(function() {
  'use strict';

  wpw.tax.global.formData.t2s524 = {
    formInfo: {
      abbreviation: 'T2S524',
      title: 'Ontario Specialty Types',
      //subTitle: '(2009 and later years)',
      schedule: 'Schedule 524',
      code: 'Code 0901',
      formFooterNum: 'T2 SCH 524',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule to identify the specialty type of a corporation carrying on business in the ' +
              'province of Ontario through a permanent establishment if:',
              sublist: ['its tax year includes January 1, 2009;',
                'the tax year is the first year after incorporation or an amalgamation; or',
                'there is a change to the specialty type']
            },
            'If none of the listed specialty types applies, tick box 99 "Other."',
            'Unless otherwise noted, references to sections, subsections, and clauses are from the <i>Taxation Act, ' +
            '2007</i> (Ontario).'
          ]
        }
      ],
      category: 'Ontario Forms'
    },
    sections: [
      {
        header: 'Specialty Types',
        rows: [
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            labelWidth: '50%',
            num: '101',
            label: 'If there is a change to the specialty type, check this box. '
          },
          {
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            label: 'Identify the specialty type that applies to your corporation:',
            inputType: 'dropdown',
            labelAfterNum: true,
            num: '100', "validate": {"or": [{"matches": "^-?[.\\d]{2,2}$"}, {"check": "isEmpty"}]},
            tn: '100',
            options: [
              {option: '', value: ''},
              {option: '01 - Family farm corporation - See subsection 64(3)', value: '01'},
              {option: '02 - Family fishing corporation - See subsection 64(3)', value: '02'},
              {
                option: '03 - Mortgage investment corporation – See subsection 130.1(6) of the federal Income Tax Act.',
                value: '03'
              },
              {option: '04 - Credit union - See subsection 137(6) of the federal Act', value: '04'},
              {option: '06 - Bank - See subsection 248(1) of the federal Act', value: '06'},
              {option: '08 - Financial institution prescribed by regulation only - See clause 66(2)(f) ', value: '08'},
              {option: '09 - Registered securities dealer - See subsection 248(1) of the federal Act  ', value: '09'},
              {option: '10 - Farm feeder finance co-operative corporation ', value: '10'},
              {option: '11 - Insurance corporation - See subsection 248(1) of the federal Act', value: '11'},
              {
                option: '12 - Mutual insurance - See subsection 27(2) of the Taxation Act, 2007 (Ontario) ' +
                'and paragraph 149(1)(m) of the federal Act', value: '12'
              },
              {option: '13 - Specialty mutual insurance', value: '13'},
              {option: '14 - Mutual fund corporation - See subsection 131(8) of the federal Act', value: '14'},
              {option: '15 - Bare trustee corporation', value: '15'},
              {
                option: '16 - Professional corporation (incorporated professional only) - ' +
                'See subsection 248(1) of the federal Act n', value: '16'
              },
              {option: '17 - Limited liability corporation', value: '17'},
              {
                option: '18 - Generator of electrical energy for sale, or producer of steam for use in the generation' +
                ' of electrical energy for sale - See subsection 33(7) ', value: '18'
              },
              {
                option: '19 - Hydro successor, municipal electrical utility, or subsidiary of either - ' +
                'See subsection 91.1(1) and section 88 of the Electricity Act, 1998 (Ontario)', value: '19'
              },
              {
                option: '20 - Producer and seller of steam for uses other than for the generation of electricity - See subsection 33(7) ',
                value: '20'
              },
              {option: '21 - Mining corporation ', value: '21'},
              {option: '22 - Non-resident corporation ', value: '22'},
              {option: '99 - Other (if none of the previous descriptions apply)', value: '99'}
            ]
          }
        ]
      }
    ]
  };
})();
