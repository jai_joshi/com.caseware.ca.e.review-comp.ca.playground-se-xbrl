(function() {
  wpw.tax.create.diagnostics('t2s524', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('524.100', common.prereq(
        common.requireFiled('T2S524'),
        function(tools) {
          return tools.requireAll(tools.list(['100', 'cp.147']), 'isNonZero');
        }));

  });
})();
