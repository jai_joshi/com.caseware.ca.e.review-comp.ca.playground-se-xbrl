(function() {
  wpw.tax.create.diagnostics('t2s303', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3030004', common.prereq(
        common.check(['t2s5.505'], 'isNonZero'),
        common.requireFiled('T2S303')));

    diagUtils.diagnostic('3030005', common.prereq(common.and(
        common.requireFiled('T2S303'),
        common.check(['120'], 'isNonZero')),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            return row[1].require('isNonZero');
          });
        }));

    diagUtils.diagnostic('3030006', common.prereq(common.and(
        common.requireFiled('T2S303'),
        common.check(['901', '902', '903'], 'isNonZero')),
        common.check('120', 'isNonZero')));

    diagUtils.diagnostic('3030007', common.prereq(
        common.requireFiled('T2S303'),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[1].isNonZero())
              return row[0].require('isFilled');
            else return true;
          });
        }));

    //CRA has separate error codes for corresponding fields. (3030007, 3030008)
    diagUtils.diagnostic('3030008', common.prereq(
        common.requireFiled('T2S303'),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[0].isFilled())
              return row[1].require('isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('303.160', common.prereq(
        common.requireFiled('T2S303'),
        function(tools) {
          return tools.field('160').require(function() {
            return this.get() < 50000;
          })
        }));

  });
})();

