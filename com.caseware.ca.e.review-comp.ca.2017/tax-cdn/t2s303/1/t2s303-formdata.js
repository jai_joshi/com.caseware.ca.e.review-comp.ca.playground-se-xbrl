(function() {

  wpw.tax.create.formData('t2s303', {
    formInfo: {
      abbreviation: 'T2S303',
      title: 'Newfoundland and Labrador Direct Equity Tax credit',
      schedule: 'Schedule 303',
      headerImage: 'canada-federal',
      code: 'Code 1701',
      formFooterNum: 'T2 SCH 303 E (17)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'You can claim a Newfoundland and Labrador direct equity tax credit if you are ' +
              'an <b>eligible investor</b> that purchased <b>eligible shares</b> from a corporation ' +
              'carrying on an <b>eligible business</b> in Newfoundland and Labrador. (The terms in bold are ' +
              'defined in Section 2 of the <i>Direct Equity Tax Credit Regulations</i>.)'
            },
            {
              label: 'Use this schedule to: ',
              sublist: ['claim the credit to reduce Newfoundland and Labrador income tax payable ' +
              'in the current tax year (before deducting the amount for the small business ' +
              'tax holiday and the refundable credits);',
                'calculate the credit you have available to carry forward;',
                'request a carryback of the credit; or',
                'transfer an unused credit after an amalgamation or the wind-up of a subsidiary,' +
                ' under subsections 87 and 88 of the federal <i>Income Tax Act</i>. ' +
                'In this situation, you are considered to have acquired the unused credit in the ' +
                'tax year in which the credit was originally issued to the corporate investor.']
            },
            {
              label: 'An unused credit earned in the current tax year is not refundable. ' +
              'Unused credits can be carried forward seven years and carried back three years.'
            },
            {
              label: 'You may claim a credit equal to the lowest of the following amounts: ',
              sublist: ['Newfoundland and Labrador income tax payable (before deducting the ' +
              'amount for the small business tax holiday and the refundable credits);',
                'the amount indicated on the tax credit receipts issued in the current year ' +
                'plus unused credits from previous years; or',
                '$50,000 (this is the <b>maximum credit</b> you may claim in a year).']
            },
            {
              label: 'File this schedule, Schedule 5 <i>Tax Calculation Supplementary – Corporations</i>,' +
              ' and each tax credit receipt issued by the Newfoundland and Labrador ' +
              'Department of Finance with your <i>T2 Corporation Income Tax Return</i>.'
            }
          ]
        }
      ],
      category: 'Newfoundland and Labrador Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Total tax credit earned in the current tax year',
        'rows': [
          {
            'type': 'table',
            'num': '100'
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of credit available for carryforward',
        'rows': [
          {
            'label': 'Unused credit at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '102'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Credit expired after seven tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit at the beginning of the tax year (amount B <b>minus</b> line 104)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '106',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit transferred on an amalgamation or wind-up of a subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              },
              null
            ]
          },
          {
            'label': 'Current-year credit earned (amount A)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '120',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 110 <b>plus</b> line 120)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '121'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '122'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Total credit available (line 105 <b>plus</b> amount C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '123'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Credit claimed in the current year (cannot exceed $50,000) <br>Enter on line 505 of Schedule 5.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit carried back to previous tax years (complete Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '161'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 160 <b>plus</b> amount E)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '162'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '163'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            indicator: 'F'
          },
          {
            'label': '<b>Closing balance</b> – total credit available for carryforward (amount D <b>minus</b> amount F)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 3 - Request for carryback of credit',
        'rows': [
          {
            'label': 'The maximum amount you can apply is the portion of your current-year credit earned that exceeds one of the following amounts, whichever is less:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- Newfoundland and Labrador income tax payable (before deducting the amount for the small business tax holiday and the refundable credits); or',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '- $50,000.',
            'labelClass': 'tabbed fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '300'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '*Newfoundland and Labrador income tax payable is defined as the amount payable before the deduction for the small business tax holiday and the refundable credits. (see paragraph 2(l) of the Regulations)',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        forceBreakAfter: true,
        'header': 'Part 4 – Credit available for carryforward by tax year of origin',
        'rows': [
          {
            'label': 'The carryforward period is 7 years. The amount available from the 7th previous tax year will expire after this tax year. <br>' +
            'When you file your return for the next year, you will enter the expired amount on line 104 of Schedule 303 for that year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '400'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for Newfoundland and Labrador Direct Equity Tax Credit',
        'rows': [
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  });
})();
