(function() {

  wpw.tax.create.calcBlocks('t2s8', function(calcUtils) {

    var straighLineClasses = [13, 14, 15, 19, 20, 21, 24, 27, 29, 34];
    var subClass43 = [43.1, 43.2];

    calcUtils.calc(function(calcUtils, field, form) {
      var table100 = field('100');
      var table700 = field('700');
      var table700DataArr = [];

      calcUtils.calcOnFormChange('T2S8W');

      table100.getRows().forEach(function(row, rIndex) {
        /**calcs below is for this tax rule: Enter a rate only if the declining balance method is used.
         For any other method (ex: straight-line method), enter NA.
         */
        if (straighLineClasses.indexOf(row[0].get()) !== -1) {
          row[3].valueType('text');
          row[3].assign('NA');
        } else {
          row[3].valueType('number');
        }

        /**
         * calcs below is to populate table 700 for class 43.1 and 43.2
         */
        //search for row with class 43.1, 43.2
        if (subClass43.indexOf(row[0].get()) !== -1) {
          //get the repeatId for linked form
          var linkedRepeatId = 'T2S8W-' + table100.getRowInfo(rIndex).repeatId;
          // check if there is additional in current year - 203 > 0 in related form
          if (form(linkedRepeatId).field('203').get() > 0) {
            //get info of table 700 in linked t2s8w then push it to table 700
            form(linkedRepeatId).field('700').getRows().forEach(function(row) {
              //if there is no value in the row then dont push it in
              if (row.every(function(cell) {
                    return !cell.get()
                  })) {
                return;
              }
              //push in all other info from linked form
              table700DataArr.push({
                0: rIndex + 1,
                1: row[0].get(),
                2: row[1].get(),
                3: row[2].get(),
                linkedRepeatId: linkedRepeatId
              });
            });
          }
          field('showTable700').assign(true);
        }
      });
      var table700Size = table700.size().rows;
      //remove all table 700 rows
      for (var i = 0; i < table700Size; i++) {
        calcUtils.removeTableRow('T2S8', '700', 0);
      }
      var count = 0;
      //add a row to an empty table
      calcUtils.addTableRow('T2S8', '700', count);
      //populate table value
      table700DataArr.forEach(function(dataObj) {
        var tableRow = table700.getRow(count);
        tableRow.forEach(function(cell, cIndex) {
          cell.assign(dataObj[cIndex]);
          cell.source(calcUtils.form(dataObj.linkedRepeatId).field('700'));
          cell.config('cannotOverride', true);
        });
        count++;
        calcUtils.addTableRow('T2S8', '700', count);
      });
      //remove last row
      calcUtils.removeTableRow('T2S8', '700', count);
    });
  });
})();
