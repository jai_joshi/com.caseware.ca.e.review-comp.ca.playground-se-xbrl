(function() {
  'use strict';

  wpw.tax.global.formData.t2s8 = {
    formInfo: {
      abbreviation: 'T2S8',
      title: 'Capital Cost Allowance (CCA)',
      //TODO: DO NOT DELETE THESE 2 LINE: subtitle and code
      //subTitle: '(2006 and later tax years)',
      schedule: 'Schedule 8',
      code: 'Code 0603',
      headerImage: 'canada-federal',
      neededRepeatForms: ['t2s8w'],
      showCorpInfo: true,
      formFooterNum: 'T2 SCH 8 (17)',
      category: 'Federal Tax Forms',
      dynamicWidth: true
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: 'For more information, see the section called "Capital Cost Allowance" in the ' +
            '<i>T2 Corporation Income Tax Guide.</i>',
            labelClass: 'fullLength'
          },
          {
            label: 'Is the corporation electing under <i>Regulation</i> 1101(5q)?',
            type: 'infoField',
            inputType: 'radio',
            num: '101',
            tn: '101',
            init: '2',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            type: 'table',
            num: '090'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table', num: '100'
          },
          {
            'type': 'table', 'num': '110'
          }
        ]
      },
      {
        header: 'Classes 43.1 and 43.2 only',
        showWhen: 'showTable700',
        rows: [
          {
            type: 'table',
            num: '700'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {
            label: 'Note 1. Include any property acquired in previous years that has now become available for use. ' +
            'This property would have been previously excluded from column 3. List separately any acquisitions that ' +
            'are not subject to the 50% rule, see <i>Regulation</i> 1100(2) and (2.2).',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Note 2. Enter in column 4, "Adjustments and transfers", amounts that increase or reduce the ' +
            'undepreciated capital cost (column 6). Items that <b>increase</b> the undepreciated capital cost include ' +
            'amounts transferred under section 85, or transferred on amalgamation or winding-up of a subsidiary. Items ' +
            'that <b>reduce</b> the undepreciated capital cost (show amounts that reduce the undepreciated capital ' +
            'cost in brackets) include government assistance received or entitled to be received in the year,' +
            ' or a reduction of capital cost after the application of section 80. See the ' +
            '<i>T2 Corporation Income Tax Guide</i> for other examples of adjustments and transfers' +
            ' to include in column 4.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Note 3. The net cost of acquisitions is the cost of acquisitions (column 3) <b>plus</b> or ' +
            '<b>minus</b> certain adjustments and transfers from column 4. For exceptions to the 50% rule, see' +
            ' Interpretation Bulletin IT-285, <i>Capital Cost Allowance – General Comments</i>.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Note 4. Enter a rate only if you are using the declining balance method. For any other method' +
            ' (for example the straight-line method, where calculations are always based on the cost of acquisitions), ' +
            'enter N/A. Then enter the amount you are claiming in column 12.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Note 5. For every entry in column 10, the "Recapture of capital cost allowance" there must be a' +
            ' corresponding entry in column 5, "Proceeds of dispositions during the year". The recapture and terminal' +
            ' loss rules do not apply to passenger vehicles in Class 10.1.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Note 6. If the tax year is shorter than 365 days, prorate the CCA claim. Some classes of ' +
            'property do not have to be prorated. See the <i>T2 Corporation Income Tax Guide</i> for more information.',
            labelClass: 'fullLength'
          }
        ]
      }
    ]
  };
})();
