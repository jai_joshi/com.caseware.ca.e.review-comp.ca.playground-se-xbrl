(function() {
  wpw.tax.create.diagnostics('t2s8', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var duplicateCCA = [];
    var allowedDuplicates = [1, 2, 3, 7, 8, 10, 10.1, 13, 14, 28, 30, 31, 32, 35, 36, 38, 41, 41.1, 41.2, 43, 47, 49];

    function getDuplicates(tools, table) {
      var duplicateClasses = [];
      var classNumbers = [];
      tools.checkAll(table.getRows(), function(row) {
        if (allowedDuplicates.indexOf(row[0].get()) == -1) {
          if (classNumbers.indexOf(row[0].get()) > -1 && duplicateClasses.indexOf(row[0].get()) == -1) {
            duplicateClasses.push(row[0].get());
          }
        }
        classNumbers.push(row[0].get());
      });
      return duplicateClasses;
    }

    diagUtils.diagnostic('0080001', common.prereq(common.check(['t2j.208'], 'isChecked'), common.requireFiled('T2S8')));

    diagUtils.diagnostic('0080002', common.prereq(common.and(
        common.requireFiled('T2S8'),
        common.check(['t2s1.403'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.field('100').getCol(6), 'isNonZero');
        }));

    diagUtils.diagnostic('0080003', common.prereq(common.and(
        common.requireFiled('T2S8'),
        common.check(['t2s1.404'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.field('100').getCol(5), 'isNonZero');
        }));

    diagUtils.diagnostic('0080004', common.prereq(common.and(
        common.requireFiled('T2S8'),
        common.check(['t2s1.107'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.field('100').getCol(4), 'isNonZero');
        }));

    diagUtils.diagnostic('0080005', common.prereq(common.requireFiled('T2S8'),
        function(tools) {
          var table = tools.mergeTables(tools.field('090'), tools.field('100'));
          return tools.checkAll(table.getRows(), function(row) {
            var cols = [row[2], row[3], row[4], row[5], row[7], row[11], row[12], row[13], row[14], row[15]];
            if (tools.checkMethod(cols, 'isNonZero'))
              return row[0].require('isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0080006', common.prereq(common.requireFiled('T2S8'),
        function(tools) {
          var table = tools.mergeTables(tools.field('090'), tools.field('100'));
          return tools.checkAll(table.getRows(), function(row) {
            if (row[14].isNonZero())
              return tools.requireOne([row[2], row[3], row[4]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0080007', common.prereq(common.requireFiled('T2S8'),
        function(tools) {
          var table = tools.mergeTables(tools.field('090'), tools.field('100'));
          return tools.checkAll(table.getRows(), function(row) {
            if (row[0].get() != 10.1 && row[13].isNonZero())
              return tools.requireOne([row[2], row[3], row[4]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0080008', common.prereq(common.requireFiled('T2S8'),
        function(tools) {
          var table = tools.mergeTables(tools.field('090'), tools.field('100'));
          return tools.checkAll(table.getRows(), function(row) {
            if ((row[0].get() != 10.1 && row[0].get() != 14.1) && row[12].isNonZero())
              return row[5].require('isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0080009', common.prereq(common.requireFiled('T2S8'),
        function(tools) {
          var table = tools.field('090');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[7].isNonZero())
              return tools.requireOne([row[3], row[4]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0080300', common.prereq(common.requireFiled('T2S8'),
        function(tools) {
          var table = tools.field('700');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[0].isNonZero())
              return tools.requireAll([row[1], row[2], row[3]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0080303', common.prereq(common.requireFiled('T2S8'),
        function(tools) {
          var table = tools.field('700');
          var total = table.total().valueObj['2'];
          if (tools.field('700').getRows().length) {
            return tools.checkAll(table.getRows(), function(row) {
              if (row[3].isNonZero())
                return tools.requireOne([tools.field('700')], function() {
                  return total == 100;
                });
              else return true;
            });
          } else return true;
        }));

    diagUtils.diagnostic('008.duplicateClasses', common.prereq(common.and(
        common.requireFiled('T2S8'),
        function(tools) {
          var table = tools.field('090');
          duplicateCCA = getDuplicates(tools, table);
          return duplicateCCA.length;
        }), function(tools) {
      var table = tools.field('090');
      return tools.checkAll(table.getRows(), function(row, key) {
        if (duplicateCCA.indexOf(row[0].get()) > -1) {
          return row[0].require(function() {
            tools.field('100').cell(key, 0).mark();
            return false;
          })
        }
        return true;
      });
    }));

    diagUtils.diagnostic('008.terminalLoss', common.prereq(common.requireFiled('T2S8'),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            return row[5].require('isZero');
          });
        }));

    diagUtils.diagnostic('008.terminalLossDuplicate', common.prereq(common.requireFiled('T2S8'),
        function(tools) {
          var table = tools.field('100');
          var duplicates = getDuplicates(tools, table);
          return tools.checkAll(table.getRows(), function(row) {
            if (duplicates.indexOf(row[0].get()) > -1 && allowedDuplicates.indexOf(row[0].get()) == -1) {
              return row[5].require('isZero');
            }
            return true;
          });
        }));

    diagUtils.diagnostic('008.recapturedDepreciation', common.prereq(common.requireFiled('T2S8'),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            return row[4].require('isZero');
          });
        }));

    diagUtils.diagnostic('008.recapturedDepreciationDuplicate', common.prereq(common.requireFiled('T2S8'),
        function(tools) {
          var table = tools.field('100');
          var duplicates = getDuplicates(tools, table);
          return tools.checkAll(table.getRows(), function(row) {
            if (duplicates.indexOf(row[0].get()) > -1 && allowedDuplicates.indexOf(row[0].get()) == -1) {
              return row[4].require('isZero');
            }
            return true;
          });
        }));
  });
})();
