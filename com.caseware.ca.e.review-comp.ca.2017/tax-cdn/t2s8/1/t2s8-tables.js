(function() {
  var assetCode = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.assetCode, 'value');
  var canadaProvinces = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.canadaProvinces, 'EN');

  wpw.tax.global.tableCalculations.t2s8 = {
    '100': {
      'infoTable': false,
      'showNumbering': true,
      'fixedRows': true,
      'keepButtonsSpace': true,
      'linkedRepeatForm': 't2s8w',
      'columns': [
        {
          'header': '1<br>Class number',
          'tn': '200',
          'disabled': true,
          colClass: 'small-input-width',
          'linkedFieldId': '101',
          decimals: 1
        },
        {
          'header': '<br>Description',
          colClass: 'std-input-col-width',
          'disabled': true,
          'linkedFieldId': '103',
          'type': 'text'
        },
        {
          'header': '8<br>Reduced undepreciated capital cost (column 6 minus column 7)',
          'disabled': true,
          'linkedFieldId': '210',
          filters: 'number'
        },
        {
          'header': '9<br>CCA rate % <br>(see note 4 below)',
          colClass: 'small-input-width',
          'tn': '212',
          'disabled': true,
          'linkedFieldId': '212',
          num: '212'
        },
        {
          'header': '10<br>Recapture of capital cost allowance (see note 5 below)',
          'tn': '213',
          'total': true,
          'totalNum': '213',
          'totalMessage': 'Totals : ',
          'disabled': true,
          'linkedFieldId': '213',
          num: '213'
        },
        {
          'header': '11<br>Terminal loss',
          'tn': '215',
          'total': true,
          'totalNum': '215',
          'disabled': true,
          'linkedFieldId': '215',
          num: '215'
        },
        {
          'header': '12<br>Capital cost allowance (for declining balance method, column 8 multiplied by column 9, or a lower amount<br>(see note 6 below)',
          'tn': '217',
          'total': true,
          'totalNum': '217',
          'disabled': true,
          'linkedFieldId': '217',
          num: '217'
        },
        {
          'header': '13<br>Undepreciated capital cost at the end of the year (column 6 minus column 12)',
          'tn': '220',
          'disabled': true,
          'linkedFieldId': '220',
          'total': true,
          num: '220'
        }
      ],
      'startTable': '090',
      'hasTotals': true
    },
    '110': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {
          'width': '50%',
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          'type': 'none',
          cellClass: 'alignLeft'
        }
      ],
      'cells': [
        {
          '1': {
            'label': 'Enter the total of column 10 on line 107 of Schedule 1.'
          }
        },
        {
          '1': {
            'label': 'Enter the total of column 11 on line 404 of Schedule 1.'
          }
        },
        {
          '1': {
            'label': 'Enter the total of column 12 on line 403 of Schedule 1.'
          }
        }
      ]
    },
    '090': {
      'infoTable': false,
      'showNumbering': true,
      hasTotals: true,
      'repeats': [100],
      'linkedRepeatForm': 't2s8w',
      'maxLoop': 100000,
      'columns': [
        {
          'header': '1<br>Class number',
          'tn': '200',
          num: '200',
          'disabled': true,
          colClass: 'small-input-width',
          'linkedFieldId': '101',
          decimals: 1
        },
        {
          'header': '<br>Description',
          colClass: 'std-input-col-width',
          'disabled': true,
          'linkedFieldId': '103',
          'type': 'text'
        },
        {
          'header': '2<br>Undepreciated capital cost at the beginning of the year (amount from column 13 of last year\'s schedule 8)',
          'tn': '201',
          'disabled': true,
          'linkedFieldId': '201',
          num: '201',
          filters: 'number',
          'total': true
        },
        {
          'header': '3<br>Cost of acquisitions during the year (new property must be available for use)<br>(see note 1 below)',
          'tn': '203',
          'disabled': true,
          'linkedFieldId': '203',
          num: '203',
          filters: 'number',
          'total': true
        },
        {
          'header': '4<br>Adjustments and transfers (show amounts that will reduce the underpreciated capital cost in brackets)<br>(see note 2 below',
          'tn': '205',
          'disabled': true,
          'linkedFieldId': '205',
          num: '205',
          filters: 'number',
          'total': true
        },
        {
          'header': '5<br>Proceeds of dispositions during the year (amount not to exceed the capital cost)',
          'tn': '207',
          'disabled': true,
          'linkedFieldId': '207',
          num: '207',
          filters: 'number',
          'total': true
        },
        {
          'header': '6<br>Undepreciated capital cost (column 2 plus column 3 plus or minus column 4 minus column 5)',
          'disabled': true,
          'linkedFieldId': '208',
          filters: 'number',
          'total': true
        },
        {
          'header': '7<br>50% rule (1/2 of the amount, if any, by which the net cost of acquisitions exceeds column 5)<br>(see note 3 below)',
          'tn': '211',
          'disabled': true,
          'linkedFieldId': '211',
          num: '211',
          filters: 'number',
          'total': true

        }
      ]
    },
    '700': {
      showNumbering: true,
      fixedRows: true,
      initRows: 0,
      columns: [
        {
          header: 'CCA class row number from column 200',
          num: '300',
          tn: '300',
          colClass: 'small-input-width',
          disabled: true,
          decimals: 1
        },
        {
          header: 'Type of asset code',
          disabled: true,
          num: '301',
          tn: '301',
          type: 'dropdown',
          options: assetCode
        },
        {
          header: 'Province where the asset is located',
          num: '302',
          tn: '302',
          colClass: 'std-input-col-width',
          disabled: true,
          type: 'dropdown',
          options: canadaProvinces
        },
        {
          header: 'Percentage allocated to the asset(%)',
          num: '303',
          tn: '303',
          filters: 'decimals 2',
          decimals: 3,
          disabled: true,
          colClass: 'std-input-width'
        }
      ]
    }
  }
})();
