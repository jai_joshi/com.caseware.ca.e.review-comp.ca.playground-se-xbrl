(function() {
  var programIdentifier = [
    {
      option: 'RC',
      value: '1'
    },
    {
      option: 'RT',
      value: '2'
    },
    {
      option: 'RP',
      value: '3'
    },
    {
      option: 'RR',
      value: '4'
    },
    {
      option: 'RM',
      value: '5'
    },
    {
      option: 'RE',
      value: '6'
    },
    {
      option: 'RN',
      value: '7'
    },
    {
      option: 'RD',
      value: '8'
    },
    {
      option: 'RG',
      value: '9'
    },
    {
      option: 'SL',
      value: '10'
    },
    {
      option: 'RZ',
      value: '11'
    }
  ];

  wpw.tax.create.tables('rc59', {
    '100': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          type: 'none'
        },
        {
          colClass: 'std-input-width-2',
          type: 'text'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          type: 'none'
        },
        {
          type: 'custom',
          format: ['{N9}']
        }
      ],
      cells: [
        {
          '0': {
            label: 'Business name'
          },
          '1': {
            num: '101'
          },
          '3': {
            label: 'Business number'
          },
          '4': {
            num: '102'
          }
        }
      ]
    },
    '200': {
      fixedRows: true,
      dividers: [1],
      infoTable: true,
      columns: [
        {
          width: '80%'
        },
        {
          width: '20%'
        }
      ],
      cells: [
        {
          '0': {
            label: 'Name of individual:',
            num: '201',
            type: 'text'
          },
          '1': {
            label: 'Telephone number:',
            num: '202',
            'telephoneNumber': true, type: 'text'
          }
        },
        {
          '0': {
            label: 'Name of firm:',
            num: '203',
            type: 'text'
          },
          '1': {
            label: 'BN:',
            num: '204',
            type: 'custom',
            format: ['{N9}RC{N4}']
          }
        }
      ]
    },
    '320': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          header: '<b>All program accounts</b> ',
          type: 'singleCheckbox',
          width: '100px'
        },
        {
          type: 'none'
        },
        {
          header: '<b>Allow update access</b><br>(view only if not ticked)',
          type: 'singleCheckbox',
          width: '100px'
        },
        {
          type: 'none', colClass: 'std-padding-width'
        },
        {
          header: '<b>Optional expiry date</b> <br> (MM-DD-YYYY)',
          type: 'date',
          width: '150px'
        },
        {
          type: 'none', colClass: 'std-padding-width'
        }
      ]
    },
    '350': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          header: '<b>Program identifier</b> <br> (two letters)',
          width: '70px',
          type: 'dropdown',
          options: programIdentifier
        },
        {
          type: 'none', width: '50px'
        },
        {
          header: '<b>All reference numbers</b> ',
          type: 'singleCheckbox',
          width: '100px',
          cellClass: 'alignCenter'

        },
        {
          type: 'none', width: '50px'
        },
        {
          header: '<b>Specific reference number</b> <br> (four digits)',
          width: '200px',
          type: 'custom',
          format: ['{N4}']
        },
        {type: 'none'},
        {
          header: '<b>Allow update access</b><br>(view only if not ticked)',
          type: 'singleCheckbox',
          width: '100px'
        },
        {
          type: 'none', colClass: 'std-padding-width'
        },
        {
          header: '<b>Optional expiry date</b> <br> (MM-DD-YYYY)',
          type: 'date',
          width: '150px'
        },
        {
          type: 'none', colClass: 'std-padding-width'
        }
      ],
      cells: [
        {'3': {label: 'or'}},
        {'3': {label: 'or'}},
        {'3': {label: 'or'}},
        {'3': {label: 'or'}}
      ]
    },
    '510': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '50%',
          cellClass: 'alignLeft'
        },
        {
          cellClass: 'alignLeft'
        }
      ],
      cells: [
        {
          '0': {
            label: 'First name:',
            num: '501'
          },
          '1': {
            label: 'Last name:',
            num: '502'
          }
        },
        {
          '0': {
            label: 'Title:',
            num: '503'
          },
          '1': {
            label: 'Telephone number:',
            num: '504',
            'telephoneNumber': true, type: 'text'
          }
        }
      ]
    },
    '520': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          width: '10%'
        },
        {},
        {
          width: '150px',
          type: 'none'
        },
        {
          width: '200px'
        }
      ],
      cells: [
        {
          '0': {
            label: 'Signature:'
          },
          '2': {
            label: 'Date (YYYY-MM-DD):'
          },
          '3': {
            type: 'date',
            num: '550'
          }
        }
      ]
    }
  });
})();
