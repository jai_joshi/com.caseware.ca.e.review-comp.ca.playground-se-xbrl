(function() {

  function table350DisableRows(calcUtils, checkBoxRow, disabledRow) {
    calcUtils.field('350').getRows().forEach(function(row) {
      if (row[checkBoxRow].get()) {
        row[disabledRow].disabled(true)
      }
      else {
        row[disabledRow].disabled(false)
      }
    })
  }
  wpw.tax.create.calcBlocks('rc59', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //part 1
      calcUtils.getGlobalValue('101', 'CP', '002');
      var bnSourceField = field('cp.bn');
      var bn = bnSourceField.get().substring(0, 9);
      field('102').assign(bn);
      field('102').source(bnSourceField);
      //part 2
      calcUtils.getGlobalValue('203', 'TPP', '101');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 3
      //table 350 calcs
      table350DisableRows(calcUtils, 2, 4);
    });

    calcUtils.calc(function(calcUtils, field) {
      //part 5
      calcUtils.getGlobalValue('501', 'CP', '951');
      calcUtils.getGlobalValue('502', 'CP', '950');
      calcUtils.getGlobalValue('503', 'CP', '954');
      calcUtils.getGlobalValue('504', 'CP', '956');
      calcUtils.getGlobalValue('550', 'CP', '955');
    })
  })
})();
