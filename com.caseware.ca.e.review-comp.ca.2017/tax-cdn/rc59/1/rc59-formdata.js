(function() {
  wpw.tax.create.formData('rc59', {
    formInfo: {
      abbreviation: 'RC59',
      title: 'Business Consent',
      printNum: 'RC59',
      headerImage: 'canada-federal',
      category: 'Client Communication'
    },
    sections: [
      {
        rows: [
          {
            type: 'multiColumn',
            dividers: [false],
            columns: [
              [
                {
                  type: 'infoField',
                  inputType: 'none',
                  label: 'Representatives',
                  labelClass: 'titleFont bold'
                },
                {
                  type: 'infoField',
                  inputType: 'none',
                  label: '<b>Get access to your clients\' business information faster</b> online using ' +
                  '"Represent a Client." Go to<b>cra.gc.ca/loginservices</b>  and log in. On the ' +
                  '"Welcome" page, select "Review and update", then your "RepID", "Group ID", ' +
                  'or "Business number." Open the "Manage clients" tab, then select ' +
                  '"Authorization request" and follow the instructions.',
                  labelClass: 'fullLength'
                }
              ],
              [
                {
                  type: 'infoField',
                  inputType: 'none',
                  label: 'Business owners',
                  labelClass: 'titleFont bold'
                },
                {
                  type: 'infoField',
                  inputType: 'none',
                  label: '<b>Give your representative instant</b> access to your business information online ' +
                  'using "My Business Account." Go to <b>cra.gc.ca/loginservices</b> and log in. On the "Welcome" ' +
                  'page, select "Manage", then "Representatives" and follow the instructions.',
                  labelClass: 'fullLength'
                }
              ]
            ]
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {
            label: '<b>Use this form to</b> give a representative access to your business number program accounts.',
            labelClass: 'fullLength'
          },
          {
            label: 'The Canada Revenue Agency (CRA) needs your permission to deal with a representative. ' +
            'There are two kinds of representatives: an individual or a firm. Some examples of individual ' +
            'representatives include accountants, lawyers, or employees.',
            labelClass: 'fullLength'
          },
          {
            label: '<b>Do not use</b> this form<b> to</b>:',
            labelClass: 'fullLength'
          },
          {
            label: '• Authorize a representative for your individual tax and benefit, or trust accounts. Use Form ' +
            'T1013, Authorizing or Cancelling a Representative or use the "Authorize my representative" ' +
            'service at <b>cra.gc.ca/myaccount</b>. Online access is not available for trust accounts.',
            labelClass: 'fullLength tabbed'
          },
          {
            label: '• Authorize a third party to act on your behalf for tax ruling or interpretation requests. ' +
            'For more information, see Income Tax Information Circular IC70-6R7, Advance Income Tax Rulings ' +
            'and Technical Interpretations, or GST/HST Memorandum 1.4, Excise and GST/HST Rulings and ' +
            'Interpretations Service.',
            labelClass: 'fullLength tabbed'
          },
          {
            label: '<b>Do not use</b> this form <b>if both </b>of the following apply:',
            labelClass: 'fullLength'
          },
          {
            label: '• you are a selected listed financial institution (SLFI) for goods and ' +
            'services tax/harmonized sales tax (GST/HST) purposes, or Quebec sales tax (QST) purposes, or both; <b>and</b>',
            labelClass: 'fullLength tabbed'
          },
          {
            label: '• you have a GST/HST (RT) program account that includes QST information.',
            labelClass: 'fullLength tabbed'
          },
          {
            label: 'Instead, use Form RC7259, Business Consent for Certain Selected Listed Financial ' +
            'Institutions. For more information, including the definition of an SLFI for GST/HST <b>and</b> QST ' +
            'purposes, go to <b>cra.gc.ca/slfi.</b>',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'Part 1 - Business information',
        rows: [
          {
            label: 'Enter the business name and the business number (BN) as registered with the CRA.',
            labelClass: 'fullLength'
          },
          {
            type: 'table', num: '100'
          }
        ]
      },
      {
        header: 'Part 2 - Authorize a representative',
        rows: [
          {
            label: 'Authorize access by telephone and mail',
            labelClass: 'titleFont bold'
          },
          {
            label: 'If you are giving consent to an individual, enter their full name. If you are giving consent to' +
            ' a firm, enter the name and BN of the firm. If you want us to deal with a specific individual in that' +
            ' firm, enter the individual\'s name <b>and</b> the firm\'s name and BN. If you do not identify an' +
            ' individual of the firm, then you are giving us consent to deal with anyone from that firm.',
            labelClass: 'fullLength'
          },
          {
            type: 'table',
            num: '200'
          },
          {
            label: '<b>Note</b>: Online access must be requested through My Business Account at ' +
            '<b>cra.gc.ca/mybusinessaccount</b> ' + 'or Represent a Client at <b>cra.gc.ca/representatives</b>.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'Part 3 - Select the program accounts and authorization level',
        fieldAlignRight: true,
        rows: [
          {
            label: 'Complete either Option 1 <b>or</b> Option 2.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: 'For update and view options, tick the <b>allow update access</b> box. Your representative can ' +
            'view and make changes to your information and CRA can disclose and accept changes to information ' +
            'on your program accounts. Otherwise, your representative will have view only access by default ' +
            'and CRA will only disclose information to your representative',
            labelClass: 'fullLength'
          },
          {
            label: 'Option 1 – Give access to all your program accounts',
            labelClass: 'fullLength bold'
          },
          {
            type: 'table',
            num: '320'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Option 2 – Give access to certain program accounts',
            labelClass: 'fullLength bold'
          },
          {
            label: 'For a <b>list of supported program identifiers</b>, see page 2.',
            labelClass: 'fullLength'
          },
          {
            type: 'table',
            num: '350'
          },
          {labelClass: 'fullLength'},
          {
            label: 'If more than four program identifiers are required, fill in another RC59 form.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'Part 4 - Certification',
        rows: [
          {
            label: 'You must sign and date this form. The CRA must receive this form within six months of the date ' +
            'it was signed or it will not be processed. This form must only be signed by an individual with proper ' +
            'authority for the business (see the choices below). An authorized representative cannot sign this ' +
            'form unless they have delegated authority. If the name of the individual signing this form does ' +
            'not exactly match CRA records, this form will not be processed. Forms that cannot be processed, ' +
            'for any reason, will be returned to the business. To avoid processing delays and before you sign ' +
            'this form, you must make sure that the CRA has complete and valid information on file for ' +
            'your business. We may contact you to confirm the information you have given.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {label: 'The individual signing this form is (tick one box only):'},
          {
            type: 'splitInputs',
            inputType: 'checkbox',
            divisions: '4',
            num: '500',
            showValues: 'false',
            items: [
              {label: 'an owner', value: '1'},
              {
                label: 'an officer of a non-profit organization',
                value: '2'
              },
              {label: 'a partner of a partnership', value: '3'},
              {label: 'a trustee', value: '4'},
              {label: 'a corporate director', value: '5'},
              {
                label: 'an individual with delegated authority',
                value: '6'
              },
              {
                label: 'a corporate officer',
                value: '7'
              }
            ]
          },
          {labelClass: 'fullLength'},
          {
            type: 'table', num: '510'
          },
          {labelClass: 'fullLength'},
          {label: 'I certify that the information given on this form is correct and complete.'},
          {
            type: 'table', num: '520'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Once completed, send this form to your tax centre. For more information, go to <b>cra.gc.ca/taxcentre.</b>',
            labelClass: 'fullLength'
          },
          {
            label: 'Our goal is to process RC59 forms within 15 business days from when we get them.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            type: 'infoField', inputType: 'template',
            content: 'mod/com.caseware.ca.tax.abstract/directives/templateInjector/rc59add.htm'
          }
        ]
      }
    ]
  })
})();
