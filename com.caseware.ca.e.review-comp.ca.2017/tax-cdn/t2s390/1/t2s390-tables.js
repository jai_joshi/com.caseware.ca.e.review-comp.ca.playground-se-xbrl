(function() {
  wpw.tax.create.tables('t2s390', {
    '300': {
      'fixedRows': true,
      hasTotals: true,
      'infoTable': true,
      'columns': [
        {
          'width': '190px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'total': true,
          formField: true,
          'totalNum': '904',
          'totalMessage': 'Total (enter on line D)',
          totalIndicator: 'F'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        }],
      'cells': [
        {
          '0': {
            'label': '1st previous tax year '
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '901'
          },
          '5': {
            'num': '901', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '2nd previous tax year '
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '902'
          },
          '5': {
            'num': '902'
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '3rd previous tax year '
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '903'
          },
          '5': {
            'num': '903', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }]
    },
    '400': {
      'fixedRows': true,
      hasTotals: true,
      'infoTable': true,
      'columns': [
        {
          type: 'none'
        },
        {
          header: 'Year of origin',
          'width': '190px',
          'type': 'date',
          'disabled': true
        },
        {
          type: 'none'
        },
        {
          header: 'Credit available for carryforward',
          colClass: 'std-input-width',
          total: true,
          totalNum: '401',
          totalIndicator: 'G',
          formField: true,
          totalMessage: 'Total credit available for carryforward (equals line 230)'
        },
        {
          type: 'none',
          colClass: 'std-input-width'
        }
      ],
      'cells': [
        {'0': {label: '10th previous tax year ending on'}},
        {'0': {label: '9th previous tax year ending on'}},
        {'0': {label: '8th previous tax year ending on'}},
        {'0': {label: '7th previous tax year ending on'}},
        {'0': {label: '6th previous tax year ending on'}},
        {'0': {label: '5th previous tax year ending on'}},
        {'0': {label: '4th previous tax year ending on'}},
        {'0': {label: '3rd previous tax year ending on'}},
        {'0': {label: '2nd previous tax year ending on'}},
        {'0': {label: '1st previous tax year ending on'}},
        {'0': {label: 'Current tax year ending on'}}
      ]
    },

    '500': {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {type: 'none'},
        {
          colClass: 'std-input-width',
          type: 'date',
          disabled: true,
          header: '<b>Year of origin</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '501',
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>',
          formField: true,
          colClass: 'std-input-width'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '502',
          totalMessage: ' ',
          header: '<b>Current year contribution</b>',
          formField: true,
          colClass: 'std-input-width'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '503',
          totalMessage: ' ',
          colClass: 'std-input-width',
          header: '<b>Carryback Amount</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', totalNum: '504',
          colClass: 'std-input-width',
          header: '<b>Amount available to apply</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          colClass: 'std-input-width',
          totalMessage: ' ', disabled: true, totalNum: '505',
          header: '<b>Applied<b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          colClass: 'std-input-width',
          totalMessage: ' ', disabled: true, totalNum: '506',
          header: '<b>Balance to carry forward</b>',
          formField: true
        },
        {type: 'none', colClass: 'std-padding-width'}
      ],
      cells: [
        {
          '4': {label: '*'},
          '5': {type: 'none'},
          '7': {type: 'none'},
          '9': {type: 'none'},
          '11': {type: 'none'},
          '13': {type: 'none', label: 'N/A'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'},
          '14': {label: '**'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '3': {type: 'none'}
        }
      ]
    }
  })
})();
