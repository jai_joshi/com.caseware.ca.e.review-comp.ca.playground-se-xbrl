(function() {
  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  wpw.tax.create.calcBlocks('t2s390', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //part 2
      field('201').assign(field('501').get());
      field('205').assign(field('500').cell(0, 3).get());
      field('210').assign(field('201').get() - field('205').get());
      field('211').assign(field('210').get());
      field('212').assign(field('103').get());
      field('213').assign(field('211').get() + field('212').get());
      field('220').assign(Math.min(field('T2S383.301').get(), field('213').get()));
      field('221').assign(field('904').get());
      field('222').assign(field('220').get() + field('221').get());
      field('223').assign(field('222').get());
      field('230').assign(field('213').get() - field('223').get());
    });
    calcUtils.calc(function(calcUtils, field, form) {
      //part3
      //to get year for table summary
      var tableArray = [300, 400, 500];
      var taxationYearTable = field('tyh.200');
      tableArray.forEach(function(num) {
        field(num).getRows().forEach(function(row, rowIndex) {
          if (num == 300) {
            var dateCol = Math.abs(rowIndex - 19);
            row[1].assign(taxationYearTable.cell(dateCol, 6).get());
          }
          else {
            row[1].assign(taxationYearTable.getRow(rowIndex + 10)[6].get())
          }
        })
      });
    });
    calcUtils.calc(function(calcUtils, field, form) {
      //part4
      var summaryTable = field('500');

      field('400').getRows().forEach(function(row, rowIndex) {
        row[3].assign(summaryTable.getRow(rowIndex)[13].get());
      });

      // historical data chart
      var limitOnCredit = field('222').get();
      summaryTable.cell(10, 3).assign(0);

      summaryTable.getRows().forEach(function(row, rowIndex) {
        if (rowIndex == 0) {
          // to avoid the first row being calculated to total
          row[7].assign(0);
          row[9].assign(0);
          row[11].assign(0);
          row[13].assign(0);
        } else {
          row[9].assign(Math.min(row[3].get() + row[5].get() - row[7].get()), 0);
          row[11].assign(returnAmountApplied(limitOnCredit, row[9].get()));
          row[13].assign(row[9].get() - row[11].get());
          limitOnCredit -= row[11].get();
        }
      });
      summaryTable.cell(10, 5).assign(field('103').get())
    });
  });
})();
