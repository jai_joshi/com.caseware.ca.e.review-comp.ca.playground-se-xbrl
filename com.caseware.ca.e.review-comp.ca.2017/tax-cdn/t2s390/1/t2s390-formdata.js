(function() {
  'use strict';
  wpw.tax.create.formData('t2s390', {
    formInfo: {
      abbreviation: 't2s390',
      title: 'Manitoba Cooperative Development Tax Credit',
      //subTitle: '(2010 and later tax years)',
      schedule: 'Schedule 390',
      code: 'Code 1701',
      formFooterNum: 'T2 SCH 390 E (17)',
      headerImage: 'canada-federal',
      showCorpInfo: true,
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if you are an eligible contributor who made a monetary contribution ' +
              'before April 12, 2017 to a cooperative development fund and you want to claim a Manitoba' +
              ' cooperative development tax credit under section 7.14 of the <i>Income Tax Act</i> (Manitoba).',
              labelClass: 'fullLength'
            },
            {
              label: 'You can reduce your Manitoba income tax payable for the current tax year, claim a refund ' +
              'of the current-year credit, and calculate your credit available for carryforward and carryback. ' +
              'The tax credit will first be applied to reduce Manitoba income tax otherwise payable. ' +
              'The balance will be refunded up to $750. The remaining credit can be carried forward 10 tax years and ' +
              'carried back 3 tax years. However, you cannot carry back to tax years ending before 2010.',
              labelClass: 'fullLength'
            },
            {
              label: 'File this schedule and the applicable tax credit receipts with your' +
              '<i> T2 – Corporation Income Tax Return.</i>',
              labelClass: 'fullLength'
            }
          ]
        }
      ],
      category: 'Manitoba Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Tax credit earned in the current tax year',
        'rows': [
          {
            'label': 'Complete this part with information from Form T2CDTC(MB). If you received more than one receipt for the year, use the information from the amended receipt',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Receipt number',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '100',
                  'type': 'text',
                  'validate': {
                    'or': [
                      {
                        'length': {
                          'min': '9',
                          'max': '9'
                        }
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              }
            ]
          },
          {
            'label': 'Date of the contribution',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '101',
                  'type': 'date'
                },
                'padding': {
                  'type': 'tn',
                  'data': '101'
                }
              }
            ]
          },
          {
            'label': 'Amount of contribution',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '102',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '102'
                }
              }
            ]
          },
          {
            'label': 'Non-refundable credit (enter at amount B)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '103',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '103'
                }
              }
            ]
          },
          {
            'label': 'Refundable credit (cannot exceed $750)<br>enter on line 612 of Schedule 5',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of total non-refundable credit available and credit available for carryforward',
        'rows': [
          {
            'label': 'Non-refundable credit at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '201'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'A'
                }
              }
            ]
          },
          {
            'label': 'Non-refundable credit expired after 10 tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '205',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '205'
                }
              },
              null
            ]
          },
          {
            'label': 'Non-refundable credit at the beginning of the tax year (amount A <b>minus</b> line 205)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '210',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '210'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '211',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Non-refundable current-year credit earned (amount from line 103)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '212'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Total non-refundable credit available (line 210 <b>plus</b> amount B)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '213'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Non-refundable credit claimed in the current tax year <br>' +
            '(lesser of amount C and Manitoba tax otherwise payable)<br> enter on line 609 of Schedule 5',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '220',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '220'
                }
              },
              null
            ]
          },
          {
            'label': 'Non-refundable credit carried back to previous tax years (complete Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '221'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 220 <b>plus</b> amount D)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '222'
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '223'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': '<b>Non-refundable credit available for carryforward at the end of the current tax year</b>' +
            ' <br>(amount C <b>minus</b> amount E)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '230',
                  'underline': 'double',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '230'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 3 - Request for carryback of credit',
        'rows': [
          {
            'label': 'You can carry back to tax years ending after 2009. The credit applied to ' +
            'a particular year cannot be more than the Manitoba tax otherwise payable for that year. ' +
            'The total credit that can be carried back to all three previous years cannot be more than the' +
            ' lesser of line 220 minus <b>amount</b> C, and amount B in Part 2.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '300'
          }
        ]
      },
      {
        'header': 'Part 4 -  Analysis of non-refundable credit available for carryforward by year of origin',
        'rows': [
          {
            'label': 'You can complete this part to show all the non-refundable credits from previous tax years ' +
            'available for carryforward, by year of origin. <br>The carryforward period is <b>10 years</b>.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '400'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for Manitoba Cooperative Development Tax Credit',
        'rows': [
          {
            'type': 'table',
            'num': '500'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  });
})();
