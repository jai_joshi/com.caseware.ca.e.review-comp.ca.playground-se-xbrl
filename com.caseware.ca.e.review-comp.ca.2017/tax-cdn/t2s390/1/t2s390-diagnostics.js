(function() {
  wpw.tax.create.diagnostics('t2s390', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('3900001', common.prereq(function(tools) {
      return tools.checkMethod(tools.list(['t2s5.609', 't2s5.612']), 'isNonZero');
    }, common.requireFiled('T2S390')));

    diagUtils.diagnostic('3900003', common.prereq(common.and(
        common.check('103', 'isNonZero'),
        common.requireFiled('T2S390')),
        common.check(['100', '101', '102'], 'isNonZero', true)));

    diagUtils.diagnostic('3900005', common.prereq(common.and(
        common.check('104', 'isNonZero'),
        common.requireFiled('T2S390')),
        common.check(['100', '101', '102'], 'isNonZero', true)));

    diagUtils.diagnostic('390.104', common.prereq(common.and(
        common.check('104', 'isNonZero'),
        common.requireFiled('T2S390')),
        function(tools) {
          return tools.field('104').require(function() {
            return this.get() < 751;
          })
        }));

  });
})();

