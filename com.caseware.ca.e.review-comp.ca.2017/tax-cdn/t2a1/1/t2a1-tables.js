(function () {

  var labelArray = ['After March 31, 2001 <br>& before April 1, 2002:', 'After March 31, 2002 <br>& before April 1, 2003:',
    'After March 31, 2003 <br>& before April 1, 2006:', 'After March 31, 2006 <br>& before April 1, 2007:', 'After March 31, 2007 <br>& before April 1, 2008:',
    'After March 31, 2008 <br>& before April 1, 2009:', 'After March 31, 2009 <br>& before July 1, 2015:', 'After June 30, 2015:'];

  function getTable100cell(labelArray) {
    var data = [];
    labelArray.forEach(function (labelitem) {
      data.push({
        '0': {
          label: labelitem,
          cellClass: 'alignLeft'
        }
      });
    });
    return data;
  }

  wpw.tax.create.tables('t2a1', {
    '100': {
      fixedRows: true,
      hasTotals: true,
      superHeaders: [
        {
          header: '<b>A <br> Days in Taxation Year </b>',
          colClass: 'std-input-col-width',
          colspan: '2'
        },
        {
          header: '<b>B <br> Percentage <br> (%) </b>',
          colClass: 'half-col-width',
          cellClass: 'alignCenter',
        },
        {
          header: '<b>C <br> Alberta Small Business Threshold <br> Line 015X(B)</b>',
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {
          header: '<b>D <br> Least of amounts: <br> 007, 013 and C</b>',
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {
          header: '<b>E <br> D X line 021*</b>',
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {
          header: '<b>F <br> SBD Rate</b>',
          colClass: 'third-col-width',
          cellClass: 'alignCenter'
        },
        {
          header: '<b>G <br> Alberta Small Business  <br>E X (A/Total A) X F</b>',
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
      ],
      columns: [
        {
          colClass: 'std-input-col-width',
          type: 'none',
          cellClass: 'alignCenter',
        },
        {
          colClass: 'qtr-col-width',
          formField: true,
          cellClass: 'alignLeft',
          total: true,
          totalNum: '101',
          totalMessage: 'Total Days in the <br>Taxation Year:'

        },
        {
          colClass: 'half-col-width',
          formField: true,
          cellClass: 'alignCenter',
        },
        {
          colClass: 'std-input-width',
          formField: true,
          cellClass: 'alignCenter',
          'cannotOverride': true
        },
        {
          colClass: 'std-input-width',
          formField: true,
          cellClass: 'alignCenter',
        },
        {
          colClass: 'std-input-width',
          formField: true,
          cellClass: 'alignCenter',
          'cannotOverride': true
        },
        {
          colClass: 'third-col-width',
          cellClass: 'alignCenter',
          decimals: 4
        },
        {
          colClass: 'std-input-width',
          formField: true,
          cellClass: 'alignCenter',
          'cannotOverride': true
        }
      ],
      cells: [
        {
          '0': {
            label: 'After March 31, 2007 <br>& before April 1, 2008:',
            cellClass: 'alignLeft'
          }
        },
        {
          '0': {
            label: 'After March 31, 2008 <br>& before April 1, 2009:',
            cellClass: 'alignLeft'
          }
        },
        {
          '0': {
            label: 'After March 31, 2009 <br>& before July 1, 2015:',
            cellClass: 'alignLeft'
          }
        },
        {
          '0': {
            label: 'After June 30, 2015 <br>& before January 1, 2017:',
            cellClass: 'alignLeft'
          }
        },
        {
          '0': {
            label: 'After December 31, 2016:',
            cellClass: 'alignLeft'
          }
        }
      ]
    },
    '100a': {
      fixedRows: true,
      hasTotals: true,
      superHeaders: [
        {
          header: '<b>A <br> Days in Taxation Year </b>',
          colClass: 'std-input-col-width',
          colspan: '2'
        },
        {
          header: '<b>B <br> Percentage <br> (%) </b>',
          colClass: 'half-col-width',
          cellClass: 'alignCenter',
        },
        {
          header: '<b>C <br> Alberta Small Business Threshold <br> Line 015X(B)</b>',
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {
          header: '<b>D <br> Least of amounts: <br> 007, 013 and C</b>',
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {
          header: '<b>E <br> D X line 021*</b>',
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {
          header: '<b>F <br> SBD Rate</b>',
          colClass: 'third-col-width',
          cellClass: 'alignCenter'
        },
        {
          header: '<b>G <br> Alberta Small Business  <br>E X (A/Total A) X F</b>',
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
      ],
      columns: [
        {
          colClass: 'std-input-col-width',
          type: 'none',
          cellClass: 'alignCenter',
        },
        {
          colClass: 'qtr-col-width',
          formField: true,
          cellClass: 'alignLeft',
          total: true,
          totalNum: '101a',
          totalMessage: 'Total Days in the <br>Taxation Year:'

        },
        {
          colClass: 'half-col-width',
          formField: true,
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          formField: true,
          cellClass: 'alignCenter',
          'cannotOverride': true
        },
        {
          colClass: 'std-input-width',
          formField: true,
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          formField: true,
          cellClass: 'alignCenter',
          'cannotOverride': true
        },
        {
          colClass: 'third-col-width',
          cellClass: 'alignCenter',
          decimals: 4
        },
        {
          colClass: 'std-input-width',
          formField: true,
          cellClass: 'alignCenter',
          'cannotOverride': true
        }
      ],
      cells: getTable100cell(labelArray)
    },
    '200': {
      fixedRows: true,
      hasTotals: true,
      columns: [
        {
          header: 'Name of the <br> Associated Canadian-controlled <br> Private Corporations',
          colClass: 'std-input-col-width',
          disabled: true,
          type: 'text',
          cellClass: 'alignCenter',
          num: '041', "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]},
          tn: '041',
          'cannotOverride': true
        },
        {
          header: 'Alberta Corporate <br> Account Number (CAN) <br> if applicable',
          colClass: 'std-input-width',
          formField: true,
          cellClass: 'alignLeft',
          num: '043',
          tn: '043'

        },
        {
          header: 'Percentage of the <br> Business Limit**',
          colClass: 'half-col-width',
          formField: true,
          cellClass: 'alignCenter',
          tn: '044',
          num: '044',
          total: true,
          totalMessage: 'Totals ',
          'cannotOverride': true
        },
        {
          header: 'Allocation of the <br> Base Amount*** <br> ($200,000 X % in Col 044)',
          colClass: 'std-input-width',
          formField: true,
          cellClass: 'alignCenter',
          tn: '045',
          num: '045',
          total: true,
          'cannotOverride': true,
          totalNum: '245'
        }
      ],
    },
    //===AREA B
    'ProrateShortYear': {
      'columns': [
        {
          'colClass': 'std-input-width',
          'type': 'none',
          cellClass: 'alignRight',
        },
        {
          'colClass': 'std-input-width',
          cellClass: 'alignRight',
        },
        {
          'colClass': 'std-padding-width',
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-padding-width',
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'textAlign': 'center',
          cellClass: 'alignCenter',
          'disabled': true
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
      ],
      'cells': [
        {
          '0': {
            'label': 'Amount (a)',
            'trailingDots': true
          },
          '1': {
            'num': '51b',
            'cannotOverride': true,
          },
          '2': {
            'label': 'X',
            cellClass: 'alignCenter'
          },
          '3': {
            label: 'Number of days in tax year',
            labelClass: 'singleUnderline'
          },
          '5': {
            'num': '52b',
            cellClass: 'alignCenter',
            'cannotOverride': true,
          },
          '6': {
            'label': '=',
            cellClass: 'alignCenter'
          },
          '7': {
            'num': '54b',
            'cannotOverride': true,
          },
          '8': {
            'label': '(b)',
          }
        },
        {
          '1': {
            'type': 'none'
          },
          '3': {
            label: '365',
            'type': 'none'
          },
          '5': {
            label: '365',
            'type': 'none',
            labelClass: 'alignCenter',
          },
          '7': {
            type: 'none',
          },
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'ReductionLargeCorp1': {
      'columns': [
        {
          'type': 'none',
        },
        {
          'colClass': 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter',
        },
        {
          'colClass': 'half-col-width',
          cellClass: 'alignCenter valign-bottom',
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter',
        },
        {
          'colClass': 'small-input-width',
          cellClass: 'alignCenter',
          type: 'none',
        },
        {
          'colClass': 'half-col-width',
          cellClass: 'alignCenter',
          'disabled': true
        },
        {
          'colClass': 'std-spacing-width',
          'type': 'none',
          cellClass: 'alignCenter',
        },
        {
          'colClass': 'std-input-width',
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
      ],
      'cells': [
        {
          '0': {
            'label': '(1) where the corporation is not associated with any other corporations in both the current or previous taxation year',
            cellClass: 'indent'
          },
          '1': {
            'label': 'A',
            cellClass: 'valign-bottom',
          },
          '2': {
            'num': '60b',
            'disabled': true
          },
          '3': {
            label: 'X',
            cellClass: 'valign-bottom alignCenter'
          },
          '4': {
            label: '&nbsp&nbsp&nbsp B &nbsp&nbsp&nbsp',
            labelClass: 'singleUnderline valign-bottom'
          },
          '5': {
            'num': '61b',
            'disabled': true,
            labelClass: 'singleUnderline valign-bottom alignCenter'
          },
          '7': {
            'num': '62b',
            'disabled': true,
            cellClass: 'valign-bottom'
          },
          '8': {
            'label': '(c)',
            cellClass: 'valign-bottom'
          }
        },
        {
          '1': {
            'type': 'none'
          },
          '2': {
            type: 'none',
          },
          '3': {
            type: 'none',
          },
          '4': {
            label: '11,250',
            'type': 'none'
          },
          '5': {
            label: '11,250',
            'type': 'none'
          },
          '7': {
            type: 'none',
          },
          '8': {
            type: 'none',
          },
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'ReductionLargeCorp2': {
      'columns': [
        {
          'type': 'none',
        },
        {
          'colClass': 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter',
        },
        {
          'colClass': 'half-col-width',
          cellClass: 'alignCenter valign-bottom',
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter',
        },
        {
          'colClass': 'small-input-width',
          cellClass: 'alignCenter',
          type: 'none',
        },
        {
          'colClass': 'half-col-width',
          cellClass: 'alignCenter',
          'disabled': true
        },
        {
          'colClass': 'std-spacing-width',
          'type': 'none',
          cellClass: 'alignCenter',
        },
        {
          'colClass': 'std-input-width',
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
      ],
      'cells': [
        {
          '0': {
            'label': '(2) where the corporation is not associated with any other corporations in the ' +
            'current taxation year but was associated in the previous taxation year',
            cellClass: 'indent'
          },
          '1': {
            'label': 'A',
            cellClass: 'valign-bottom',
          },
          '2': {
            'num': '60c',
            'disabled': true
          },
          '3': {
            label: 'X',
            cellClass: 'valign-bottom alignCenter'
          },
          '4': {
            label: '&nbsp&nbsp&nbsp C &nbsp&nbsp&nbsp',
            labelClass: 'singleUnderline valign-bottom'
          },
          '5': {
            'num': '61c',
            labelClass: 'singleUnderline valign-bottom alignCenter',
            'disabled': true
          },
          '7': {
            'num': '62c',
            cellClass: 'valign-bottom',
            'disabled': true
          },
          '8': {
            'label': '(c)',
            cellClass: 'valign-bottom'
          }
        },
        {
          '1': {
            'type': 'none'
          },
          '2': {
            type: 'none',
          },
          '3': {
            type: 'none',
          },
          '4': {
            label: '11,250',
            'type': 'none'
          },
          '5': {
            label: '11,250',
            'type': 'none'
          },
          '7': {
            type: 'none',
          },
          '8': {
            type: 'none',
          },
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'ReductionLargeCorp3': {
      'columns': [
        {
          'type': 'none',
        },
        {
          'colClass': 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter',
        },
        {
          'colClass': 'half-col-width',
          cellClass: 'alignCenter valign-bottom',
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter',
        },
        {
          'colClass': 'small-input-width',
          cellClass: 'alignCenter',
          type: 'none',
        },
        {
          'colClass': 'half-col-width',
          cellClass: 'alignCenter',
          'disabled': true
        },
        {
          'colClass': 'std-spacing-width',
          'type': 'none',
          cellClass: 'alignCenter',
        },
        {
          'colClass': 'std-input-width',
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
      ],
      'cells': [
        {
          '0': {
            'label': '(3) where the corporation is associated with another corporation in the current year\n',
            cellClass: 'indent'
          },
          '1': {
            'label': 'A',
            cellClass: 'valign-bottom',
          },
          '2': {
            'num': '60d',
            'disabled': true
          },
          '3': {
            label: 'X',
            cellClass: 'valign-bottom alignCenter'
          },
          '4': {
            label: '&nbsp&nbsp&nbsp D &nbsp&nbsp&nbsp',
            labelClass: 'singleUnderline valign-bottom'
          },
          '5': {
            'num': '61d',
            labelClass: 'singleUnderline valign-bottom alignCenter',
            'disabled': true
          },
          '7': {
            'num': '62d',
            cellClass: 'valign-bottom',
            'disabled': true
          },
          '8': {
            'label': '(c)',
            cellClass: 'valign-bottom'
          }
        },
        {
          '1': {
            'type': 'none'
          },
          '2': {
            type: 'none',
          },
          '3': {
            type: 'none',
          },
          '4': {
            label: '11,250',
            'type': 'none'
          },
          '5': {
            label: '11,250',
            'type': 'none'
          },
          '7': {
            type: 'none',
          },
          '8': {
            type: 'none',
          },
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'orTable': {
      'columns': [
        {
          'type': 'none',
          cellClass: 'alignCenter',
        },
        {
          'colClass': 'qtr-col-width',
          type: 'none',
          cellClass: 'alignCenter',
        },
        {
          'colClass': 'half-col-width',
          type: 'none',
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none',
        },
        {
          'colClass': 'half-col-width',
          cellClass: 'alignCenter',
          type: 'none',
        },
        {
          'colClass': 'half-col-width',
          type: 'none',
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          type: 'none',
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
      ],
      'cells': [
        {
          '0': {
            'label': 'or',
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'orTable1': {
      'columns': [
        {
          'type': 'none',
          cellClass: 'alignCenter',
        },
        {
          'colClass': 'qtr-col-width',
          type: 'none',
          cellClass: 'alignCenter',
        },
        {
          'colClass': 'half-col-width',
          type: 'none',
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none',
        },
        {
          'colClass': 'half-col-width',
          cellClass: 'alignCenter',
          type: 'none',
        },
        {
          'colClass': 'half-col-width',
          type: 'none',
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          type: 'none',
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
      ],
      'cells': [
        {
          '0': {
            'label': 'or',
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
  })
})();
