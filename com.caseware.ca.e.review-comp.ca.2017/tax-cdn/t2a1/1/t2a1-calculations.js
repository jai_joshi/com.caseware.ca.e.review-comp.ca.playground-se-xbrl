(function () {

  wpw.tax.create.calcBlocks('t2a1', function (calcUtils) {
    //todo need to add trigger to fill in this form
    calcUtils.calc(function (calcUtils, field, form) {
      var dateCompare = calcUtils.dateCompare;

      if (field('t2j.160').get()) {
        field('001').assign(1);
        field('001').source(field('t2j.160'));
      }
      if (field('t2a12.100').get() == 1) {
        field('003').assign(Math.max(field('t2a12.102').get() + field('t2a12.104').get(), 0));
        field('003').source(field('t2a12.102'));
      } else {
        field('003').assign(Math.max(field('t2j.400').get(), 0));
        field('003').source(field('t2j.400'));
      }
      calcUtils.getGlobalValue('005', 't2a5', '021');
      field('007').assign(Math.max(field('003').get() - field('005').get(), 0));
      var A4Field8 = field('t2a4.990');
      if (A4Field8.get() && A4Field8.cell(0, 0).get()) {
        var ss4008 = 0; //to get the total of t2a4 field 8
        for (var i = 0; i < A4Field8.size().rows; i++) {
          ss4008 += A4Field8.cell(i, 6).get();
        }
        field('009').assign(Math.max(field('t2a.062').get() - (2.5 * field('t2j.636').get() + 10 / 3 * ss4008), 0));
      } else {
        field('009').assign(Math.max(field('t2a.062').get() - (2.5 * field('t2j.636').get() + 10 / 3 * field('t2s21.180').get()), 0));
      }
      calcUtils.getGlobalValue('011', 't2a5', '021');
      field('013').assign(Math.max(field('009').get() - field('011').get(), 0));
      field('015').assign(field('070').get());
      if (field('t2j.750').get() == 'MJ') {
        calcUtils.getGlobalValue('019', 't2s5', '127');
        calcUtils.getGlobalValue('020', 't2s5', '167');
      } else {
        field('019').assign(0);
        field('020').assign(0);
      }
      calcUtils.getGlobalValue('021', 't2a', '065');

      //== Calculation of AB SBD
      var startDate = calcUtils.getGlobalValue('060', 'cp', 'tax_start');
      var endDate = calcUtils.getGlobalValue('061', 'cp', 'tax_end');
      var Sdate = new Date(startDate.year, startDate.month - 1, startDate.day);
      var numberOfDays;
      var totalG = 0;
      var tablePercentage = [];
      var tableDates = [];
      var ABFieldNumber = [];
      var tableID;
      var daysTotalID;
      if (field('cp.tax_start').get().year > 2016) {
        field('100Type').assign(true)
        tableID = '100';
        daysTotalID = '101';
        tablePercentage = [215, 230, 250, 250, 250];
        tableDates = [[2007, 2, 31], [2008, 2, 31], [2009, 2, 31], [2015, 5, 30], [2016, 11, 31]];
        ABFieldNumber = ['009', '010', '011', '012', '013', '014'];
      } else { //create table if fiscal year start before 2017
        field('100Type').assign(false)
        tableID = '100a';
        daysTotalID = '101a';
        tablePercentage = [150, 175, 200, 200, 215, 230, 250, 250];
        tableDates = [[2001, 2, 31], [2002, 2, 31], [2003, 2, 31], [2006, 2, 31], [2007, 2, 31], [2008, 2, 31], [2009, 2, 31], [2015, 5, 30]];
        ABFieldNumber = ['005', '006', '007', '008', '009', '010', '011', '012', '013'];
      }
      //== Calculation of AB SBD
      for (var i = 0; i < tablePercentage.length; i++) {
        field(tableID).cell(i, 1).assign(0);
        field(tableID).cell(i, 2).assign(tablePercentage[i]);
        var pdate = i - 1;
        if (i == 0 && !field('100Type').get()) {
          if (Sdate <= new Date(tableDates[i][0], tableDates[i][1], tableDates[i][2])) {
            numberOfDays = (365 - (new Date(tableDates[i][0], tableDates[i][1], tableDates[i][2]) - Sdate) / 86400000);
            field(tableID).cell(0, 1).assign(Math.max(numberOfDays, 0));
          }
        }
        if (i == tablePercentage.length - 1) { //fill in last row
          if (new Date(tableDates[i][0], tableDates[i][1], tableDates[i][2]) < Sdate) {
            field(tableID).cell(i, 1).assign(Math.round((new Date(endDate.year, endDate.month - 1, endDate.day) - Sdate) / 86400000) + 1);
          }
        }
        if (i !== 0 && i != field(tableID).size().rows - 1) {
          if (new Date(tableDates[pdate][0], tableDates[pdate][1], tableDates[pdate][2]) < Sdate && Sdate <= new Date(tableDates[i][0], tableDates[i][1], tableDates[i][2])) {
            numberOfDays = (new Date(tableDates[i][0], tableDates[i][1], tableDates[i][2]) - Sdate) / 86400000 + 1;
            field(tableID).cell(pdate, 1).assign(Math.min(numberOfDays, 365));
            field(tableID).cell(i, 1).assign(Math.max(365 - numberOfDays, 0));
          }
        }

      }
      //fill in table
      field(tableID).getRows().forEach(function (row, rowIndex) {
        var fieldAB = 'ratesAb.' + ABFieldNumber[rowIndex];
        row[3].assign(field(tableID).cell(rowIndex, 2).get() / 100 * field('015').get());
        row[4].assign(Math.min(field('007').get(), field('013').get(), row[3].get()));
        row[5].assign(Math.min(field('021').get() * row[4].get()));
        row[6].assign(field(fieldAB).get() / 100);
        row[7].assign(row[5].get() * (row[1].get() / field(daysTotalID).get()) * row[6].get());
        totalG += row[7].get();
      });
      field('031').assign(totalG);

      //AREA A
      calcUtils.getGlobalValue('040', 'T2J', '061');
      calcUtils.setCellValue('200', wpw.tax.field('t2s23.085').size().rows - 1, 2, 0) // this set the row length
      field('200').getRows().forEach(function (row, rowIndex) {
        row[0].assign(field('t2s23.085').cell(rowIndex, 0).get());
        row[0].source(field('t2s23.085').cell(rowIndex, 0));
        row[2].assign(field('t2s23.085').cell(rowIndex, 4).get());
        row[2].source(field('t2s23.085').cell(rowIndex, 4));
        row[3].assign(row[2].get() / 100 * 200000);
      })


      //AREA B
      if (field('245').get() != 0) {
        field('050').assign(field('200').cell(0, 3).get());
      } else {
        field('050').assign(200000);
      }
      field('51b').assign(field('050').get());
      field('52b').assign(field(daysTotalID).get());
      field('54b').assign(field('51b').get() * field('52b').get() / 365);
      var LargeCorpOpt = field('entityProfileSummary.081').get() - 1;
      var AreaBarray = ['b', 'c', 'd'];
      if (field('54b').get() != 0) {
        field('60' + AreaBarray[LargeCorpOpt]).assign(field('54b').get());
      } else {
        field('60' + AreaBarray[LargeCorpOpt]).assign(field('51b').get());
      }
      field('61' + AreaBarray[LargeCorpOpt]).assign(field('t2j.415').get());
      field('61' + AreaBarray[LargeCorpOpt]).source(field('t2j.415'));
      field('62' + AreaBarray[LargeCorpOpt]).assign(field('60' + AreaBarray[LargeCorpOpt]).get() * field('61' + AreaBarray[LargeCorpOpt]).get() / 11250);
      if (field('CP.Days_Fiscal_Period').get() < 365) {
        field('070').assign(field('54b').get() - Math.max(field('62b').get(), field('62c').get(), field('62d').get()));
      } else {
        field('070').assign(field('050').get() - Math.max(field('62b').get(), field('62c').get(), field('62d').get()));
      }
    })
  });
})();

