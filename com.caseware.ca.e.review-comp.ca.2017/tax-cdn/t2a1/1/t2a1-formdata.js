(function () {
  'use strict';
  wpw.tax.create.formData('t2a1', {
    formInfo: {
      abbreviation: 'AT2',
      title: 'ALBERTA SMALL BUSINESS DEDUCTION - AT1 SCHEDULE 1',
      schedule: 'Schedule 1',
      pdfPaint: true,
      showCorpInfo: true,
      //headerImage: 'canada-alberta',
      formFooterNum: 'AT2 (Jun-16)',
      category: 'Alberta Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: "For corporations which were Canadian-controlled private corporations throughout the taxation year and which had income from active " +
            "businesses carried on in Canada. Report all monetary values in dollars; DO NOT include cents",
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'rows': [
          {
            'label': 'Association for Purposes of the Alberta Small Business Deduction',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'infoField',
            'num': '001',
            'label': 'Is the corporation associated with one or more Canadian-controlled private corporations?',
            'inputType': 'radio',
            'init': false,
            tn: '001'
          },
          {
            'label': 'If "Yes", complete AREA A on page 2.',
            'labelClass': 'fullLength bold'
          },
        ]
      },
      {
        'rows': [
          {
            'label': 'Alberta Small Business Deduction',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Income from active businesses carried on in Canada as reported on the T2 line 400* OR on Schedule 12, line 106',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '003',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '003'
                }
              },
              null
            ]
          },
          {
            'label': 'Deduct: Royalty Tax Deduction for the year (Schedule 5, line 021)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '005',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '005'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Balance</b> line 003 minus line 005 (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '007',
                  'cannotOverride': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '007'
                }
              }
            ]
          },
          {
            'label': '<br>Taxable Income (less adjustments for foreign tax credits and ' +
            'amounts included in Amount Taxable in Alberta not subject to ' +
            'Alberta corporate income tax. See Guide for calculation details)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '009'
                },
                'padding': {
                  'type': 'tn',
                  'data': '009'
                }
              },
              null
            ]
          },
          {
            'label': 'Deduct: Royalty Tax Deduction for the year (Schedule 5, line 021)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '011'
                },
                'padding': {
                  'type': 'tn',
                  'data': '011'
                }
              },
              null
            ]
          },
          {
            'label': '<br>Balance line 009 minus line 011 (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '013',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '013'
                }
              }
            ]
          },
          {
            'label': 'Complete AREA B on page 2 to determine the base amount used to calculate the Alberta Small Business Threshold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '015',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '015'
                }
              }
            ]
          },
          {
            'label': '* If the corporation has income (loss) from partnership(s) with fiscal period(s) ending after March 31, 2001, then the Income from active businesses must be ' +
            'recalculated for Alberta purposes by increasing the business limit at column G on page 2 of federal Schedule 7 to $300,000 on April 1, 2001, $350,000 on ' +
            'April 1, 2002, $400,000 on April 1, 2003, $430,000 on April 1, 2007, $460,000 on April 1, 2008 and $500,000 on April 1, 2009, prorating the increase by ' +
            'the number of days in the partnership\'s fiscal period straddling March 31, 2001, March 31, 2002, March 31, 2003, March 31, 2007, March 31, 2008 and ' +
            'March 31, 2009. (See Guide for more details)',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'rows': [
          {
            'label': 'Income Eligible for the Alberta Small Business Deduction ',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Corporations with permanent establishments only in Alberta, ignore lines 019, 020 and 021 and go directly to the table below. Other corporations complete the following:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount reported on federal Schedule 5, line 127',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '019',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '019'
                }
              },
              null
            ]
          },
          {
            'label': 'Amount reported on federal Schedule 5, line 167',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '020',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '020'
                }
              },
              null
            ]
          },
          {
            'label': 'Alberta Small Business Allocation Factor:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'If both line 019 and line 020 are "0", enter the Alberta Allocation Factor from Alberta Schedule 2. ' +
            'If either line 019 or line 020 have a value greater than zero and the corporation is filing under ITA Regulation 402, ' +
            '403, 404, 405, 408, 409 or 411, then the Alberta Allocation Factor from Schedule 2 must be calculated to reduce ' +
            'Amount B by the amount at line 019 and to reduce Amount D by the amount at line 020. If the corporation is filing ' +
            'under any other ITA Regulation, then enter the Allocation Factor calculated on Schedule 2 directly onto line 021',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '021',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '021'
                }
              }
            ]
          }
        ]
      },
      {
        'rows': [
          {
            'label': 'Calculation of the Alberta Small Business Deduction',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '100',
            showWhen: {fieldId: '100Type', compare: {is: true}},
          },
          {
            'type': 'table',
            'num': '100a',
            showWhen: {fieldId: '100Type', compare: {is: false}},
          },
          {
            label: '* If the corporation only has a permanent establishment in Alberta, ' +
            'use "1" as the value for line 021 in the calculation of column E.'
          },
          {
            'label': 'Alberta Small Business Deduction: ',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Total of column G',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '031',
                  'cannotOverride': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '031'
                }
              }
            ]
          },
          {
            'label': 'Enter this amount on AT1 page 2, line 070',
            'labelClass': 'fullLength bold'
          },
        ]
      },
      {
        'header': 'AREA A - Agreement Among Associated Corporations',
        'rows': [
          {
            'label': 'Allocation Agreement:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'To arrive at the Alberta Small Business Threshold, the "base amount"* of $200,000 is used to determine the allocation among ' +
            'associated corporations. It is hereby agreed that the $200,000 base amount for the year is to be allocated as shown below for ' +
            'the taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '040',
                  'type': 'date'
                },
                'padding': {
                  'data': '040'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '200'
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'asterisk',
                'items': [
                  {
                    label: 'The percentage in Column B in the Calculation of the Alberta Small Business Deduction on page 1, adjusts the base amount for ' +
                    'changes to the Alberta Small Business Threshold. The Alberta Small Business Thresholds are as follows: Before April 1, 2001: ' +
                    '$200,000; After March 31, 2001 and before April 1, 2002: $300,000; After March 31, 2002 and before April 1, 2003: $350,000; ' +
                    'After March 31, 2003 and before April 1, 2007: $400,000; After March 31, 2007 and before April 1, 2008: $430,000; After March ' +
                    '31, 2008 and before April 1, 2009: $460,000 and after March 31, 2009: $500,000.'
                  },
                  {
                    label: 'This percentage must be the same as that used to determine the business limit on the federal Schedule 23, form T2 SCH23 for all ' +
                    'taxation years ending after December 4, 2002. The total of all percentages cannot exceed 100%.'
                  },
                  {
                    label: 'The amount in column 045 must be rounded to the nearest dollar; rounding up at $.50 and over.'
                  }
                ]
              }
            ]
          },
        ]
      },
      {
        'header': 'AREA B - Determination of the Value for Line 015',
        'rows': [
          {
            'label': 'The base amount to be used by a corporation for line 015 on page 1, is $200,000 or its allocated base amount as specified in AREA A, ' +
            '<b>adjusted</b>, if required, as follows:',
            'labelClass': 'fullLength'
          },
          {
            label: '(i) <b>Prorated Base Amount for Short Taxation Year:</b>'
          },
          {
            label: 'If the taxation year is shorter than 51 weeks, the corporation\'s base amount is the amount allocated to it multiplied by the ratio that the number of days in the year is to 365.',
            'labelClass': 'fullLength indent-2'
          },
          {
            label: '(ii) <b>Reduction for Large Corporations:</b>'
          },
          {
            label: 'If in the preceding year, the associated group (whether Canadian-controlled private or not) had total taxable capital employed in ' +
            'Canada exceeding $10,000,000 the base amount of each associated corporation is reduced or eliminated.',
            'labelClass': 'fullLength indent-2'
          },
          {
            'label': 'Enter $200,000 or, if associated, the corporation\'s allocated base amount from AREA A ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '050',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ],
            'indicator': '(a)'
          },
          {
            'label': 'If adjustments are not required, enter Amount (a) on line 015 on page 1.',
            'labelClass': 'fullLength bold'
          },
          {
            label: '(i) <b>Prorated Base Amount for Short Taxation Year:</b>'
          },
          {
            'type': 'table',
            'num': 'ProrateShortYear'
          },
          {
            'label': 'If the corporation has a short tax year but the associated group had total taxable capital employed in Canada less than ' +
            '$10,000,000, enter Amount (b) on line 015 on page 1.',
            'labelClass': 'fullLength bold'
          },
          {
            label: '(ii) <b> Reduction for Large Corporations:</b>'
          },
          {
            'type': 'table',
            'num': 'ReductionLargeCorp1'
          },
          {
            'type': 'table',
            'num': 'orTable'
          },
          {
            'type': 'table',
            'num': 'ReductionLargeCorp2'
          },
          {
            'type': 'table',
            'num': 'orTable1'
          },
          {
            'type': 'table',
            'num': 'ReductionLargeCorp3'
          },
          {
            label: 'Where:',
            'labelClass': 'fullLength indent-2'
          },
          {
            label: 'A is the small business threshold otherwise determined, adjusted if necessary for a short taxation year',
            'labelClass': 'fullLength indent-2'
          },
          {
            label: 'B is the lesser of $11,250 and (0.225% X Total taxable capital employed in Canada for the prior taxation year minus $10,000,000)',
            'labelClass': 'fullLength indent-2'
          },
          {
            label: 'C is the lesser of $11,250 and (0.225% X Total taxable capital employed in Canada for the current taxation year minus $10,000,000) ',
            'labelClass': 'fullLength indent-2'
          },
          {
            label: 'D is the lesser of $11,250 and (0.225% X Total taxable capital employed in Canada of each corporation in the associated group for ' +
            'its last tax year ending in the preceding calendar year minus $10,000,000) ',
            'labelClass': 'fullLength indent-2'
          },
          {
            'label': 'Amount (a) or (b)* - Amount (c)',
            'layout': 'alignInput',
            'labelClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '070',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ],
            'indicator': '(d)'
          },
          {
            label: 'Enter Amount (d) on line 015 on page 1',
            'labelClass': 'fullLength indent bold'
          },
          {
            label: '* If the corporation has a short taxation year, use Amount (b). Otherwise, use Amount (a). ',
            'labelClass': 'fullLength indent'
          },
        ]
      }
    ]
  });
})();
