(function() {
  function getTableProRate(labelsObj) {
    labelsObj.num = labelsObj.num || [];

    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'half-col-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          formField: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width',
          formField: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width',
          formField: true
        },
        {
          colClass: 'small-input-width',
          type: 'none',
          cellClass: 'alignCenter',
          formField: true
        },
        {
          colClass: 'std-input-width',
          formField: true
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {type: 'none', colClass: 'std-spacing-width'}
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '1': {num: labelsObj.num[0]},
          '2': {label: 'x', labelClass: 'center'},
          '3': {
            label: labelsObj.label[1],
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          '5': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '6': {label: 'x'},
          '7': {num: labelsObj.num[2], decimals: labelsObj.decimals},
          '8': {label: '%='},
          '9': {num: labelsObj.num[3]},
          '10': {label: labelsObj.indicator}
        },
        {
          '1': {type: 'none'},
          '3': {
            label: labelsObj.label[2],
            labelClass: 'center'
          },
          '5': {num: labelsObj.num[4]},
          '7': {type: 'none'},
          '9': {type: 'none'}
        }
      ]
    }
  }

  wpw.tax.global.tableCalculations.t2s500 = {
    "100": {
      "fixedRows": true,
      "infoTable": true,
      "border": "1px solid black",
      "columns": [
        {
          colClass: 'std-input-col-width',
          "type": "none"
        },
        {
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Ontario domestic factor (ODF):",
            "labelClass": "fullLength"
          },
          "1": {
            "label": "Ontario taxable income *",
            cellClass: 'singleUnderline alignCenter'
          },
          "3": {
            "num": "325",
            decimals: 2
          },
          "4": {
            "label": " = "
          },
          "5": {
            "num": "330",
            decimals: 5
          },
          "6": {
            "label": "E"
          }
        },
        {
          "1": {
            "label": "Taxable income earned in all provinces and territories **"
          },
          "3": {
            "num": "335"
          },
          "5": {
            "type": "none"
          }
        }
      ]
    },
    "200": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "200px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      "cells": [{
        "0": {
          "label": "Amount from line E on <br> page 4 of the T2 return "
        },
        "1": {
          "num": "317"
        },
        '2': {
          "label": " x ",
          "labelClass": "center"
        },
        "3": {
          "label": "Number of days in the tax <br> year after May 1, 2014",
          cellClass: 'singleUnderline',
          labelClass: 'center'
        },
        "5": {
          "num": "318",
          cellClass: 'singleUnderline'
        },
        "7": {
          "type": "none"
        },
        "8": {
          "label": " = "
        },
        "9": {
          "num": "319"
        },
        "10": {
          "label": "b"
        }
      },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "5": {
            "num": "321",
            "disabled": true
          },
          "7": {
            "type": "none"
          },
          "9": {
            "type": "none"
          }
        }
      ]
    },
    '400': getTableProRate({
      label: ['Amount F', 'Number of days in the tax year before Jan 1, 2018', 'Number of days in the tax year'],
      indicator: 'G1',
      decimals: 1
    }),
    '500': getTableProRate({
      label: ['Amount F', 'Number of days in the tax year after Dec 31, 2017', 'Number of days in the tax year'],
      indicator: 'G2',
      decimals: 1
    })
  }
})();
