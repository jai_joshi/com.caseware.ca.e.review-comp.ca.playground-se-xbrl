(function() {
  function runTableRateCalcs(calcUtils, tableNum, rateField, isAfter) {
    var field = calcUtils.field;
    var table = field(tableNum);
    var form = calcUtils.form;
    var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();
    table.cell(0, 1).assign(field('365').get());
    table.cell(1, 5).assign(daysFiscalPeriod);
    table.cell(0, 5).assign(isAfter ?
        calcUtils.compareDateAndFiscalPeriod(wpw.tax.date(2018, 1, 1)).daysAfterDate :
        calcUtils.compareDateAndFiscalPeriod(wpw.tax.date(2018, 1, 1)).daysBeforeDate
    );

    table.cell(0, 7).assign(form('ratesOn').field(rateField).get());
    table.cell(0, 7).source(form('ratesOn').field(rateField));
    table.cell(0, 9).assign(
        table.cell(0, 1).get() *
        table.cell(0, 5).get() /
        table.cell(1, 5).get() *
        table.cell(0, 7).get() / 100);
  }

  wpw.tax.create.calcBlocks('t2s500', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //Part 1 calculations //
      calcUtils.getGlobalValue('175', 'ratesOn', '602');

      //Part 2 calculations //
      var taxableIncomeAllocation = calcUtils.getTaxableIncomeAllocation('ON');
      field('205').assign(taxableIncomeAllocation.provincialTI);
      field('335').assign(taxableIncomeAllocation.allProvincesTI);

      field('205').source(taxableIncomeAllocation.provincialTISourceField);
      field('335').source(taxableIncomeAllocation.allProvincesTISourceField);

      calcUtils.multiply('210', ['175', '205'], 1 / 100);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 3 calculations //
      calcUtils.getGlobalValue('305', 'T2J', '400');
      calcUtils.getGlobalValue('310', 'T2J', '405');
      calcUtils.getGlobalValue('315', 'T2J', '427');
      calcUtils.min('320', ['305', '310', '315']);
      calcUtils.equals('325', '205');
      calcUtils.divide('330', '325', '335');
      calcUtils.multiply('355', ['330', '320']);
      calcUtils.equals('360', '205');
      calcUtils.min('365', ['355', '360']);
      field('399').assign(field('400').cell(0, 9).get() + field('500').cell(0, 9).get());
      runTableRateCalcs(calcUtils, '400', '605', false);
      runTableRateCalcs(calcUtils, '500', '6051', true);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 4 calculations //
      calcUtils.min('450', ['320', '360']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 5 calculations
      //check if corporation is credit union
      var isCreditUnion = (field('CP.1011').get() == 1);
      if (isCreditUnion) {
        // TODO: globalValue needed for num:505 because schedule 17 not yet created
        //implement when s17 is available
        //calcs.equals('510', '450');
        calcUtils.subtract('515', '505', '510');
        calcUtils.equals('520', '398');
        calcUtils.multiply('525', ['515', '398'], 1 / 100);
        field('530').assign(field('330').get());
      }
      else {
        field('505').disabled(true);
        field('510').disabled(true);
        field('515').disabled(true);
        field('520').disabled(true);
        field('525').disabled(true);
        field('535').disabled(true);
        field('530').disabled(true);
      }
    });

  });
})();
