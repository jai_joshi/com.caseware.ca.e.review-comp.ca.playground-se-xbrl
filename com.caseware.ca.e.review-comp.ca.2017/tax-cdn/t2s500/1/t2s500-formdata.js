(function () {
  'use strict';

  wpw.tax.global.formData.t2s500 = {
    'formInfo': {
      'abbreviation': 'T2S500',
      'title': 'Ontario Corporation Tax Corporation',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      //'subTitle': '(2012 and later tax years)',
      'schedule': 'Schedule 500',
      'showCorpInfo': true,
      headerImage: 'canada-federal',
      'formFooterNum': 'T2 SCH 500 E (17)',
      'description': [
        {
          'type': 'list',
          'items': [
            'Use this schedule if the corporation had a permanent establishment, under section 400 of the federal <i>Income Tax Regulations</i>, in Ontario at any time in the tax year and had Ontario taxable income in the year.',
            'Legislative references are to the federal <i>Income Tax Act</i> and <i>Income Tax Regulations</i>.',
            'This schedule is a worksheet only and is not required to be filed with your <i>T2 Corporation Income Tax Return</i>.'
          ]
        }
      ],
      category: 'Ontario Forms'
    },
    'sections': [
      {
        'header': 'Part 1 - Calculation of Ontario basic rate of tax for the year',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': ' <b>Ontario basic rate of tax for the year</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '175',
                  'disabled': true,
                  'decimals': 1,
                  'filters': 'decimals 2'
                }
              }
            ],
            'indicator': '%A'
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of Ontario basic income tax',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario taxable income*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '205'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Ontario basic income tax </b>: Amount B <b> multiplied </b> by Ontario basic rate of tax for the year (amount A from Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '210'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the corporation has a permanent establishment in more than one jurisdiction, or is claiming an Ontario tax credit in addition to Ontario basic income tax, or has Ontario corporate minimum tax or Ontario special additional tax on life insurance corporations payable, enter amount C on line 270 of Schedule 5, <i>Tax Calculation Supplementary – Corporations</i>. Otherwise, enter it on line 760 of the T2 return.',
            'labelClass': 'fullLength'
          },
          {
            'label': '* If the corporation has a permanent establishment only in Ontario, enter the amount from line 360 or amount Z, whichever applies, from page 3 of the T2 return. Otherwise, enter the taxable income allocated to Ontario from column F in Part 1 of Schedule 5.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Ontario small business deduction (OSBD)',
        'rows': [
          {
            'label': 'Complete this part if the corporation claimed the federal small business deduction under subsection 125(1) or would have claimed it if subsection 125(5.1) had not been applicable in the tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount from line 400 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '305'
                }
              }
            ],
            'indicator': '1'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount from line 405 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '310'
                }
              }
            ],
            'indicator': '2'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount from line 427 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '315'
                }
              }
            ],
            'indicator': '3'
          },
          // {
          //   'labelClass': 'fullLength'
          // },
          // {
          //   'label': 'Ontario business limit reduction:',
          //   'labelClass': 'bold'
          // },
          // {
          //   'label': 'Amount from line 3',
          //   'layout': 'alignInput',
          //   'layoutOptions': {
          //     'indicatorColumn': true,
          //     'showDots': true,
          //     'rightPadding': false
          //   },
          //   'columns': [
          //     {
          //       'input': {
          //         'num': '316'
          //       }
          //     },
          //     {
          //       'padding': {
          //         'type': 'text',
          //         'data': 'a'
          //       }
          //     }
          //   ]
          // },
          // {
          //   'label': 'Deduct:',
          //   'labelClass': 'bold'
          // },
          // {
          //   'type': 'table',
          //   'num': '200'
          // },
          // {
          //   'label': 'Reduced Ontario business limit (amount a <b> minus</b> amount b) (if negative, enter "0")',
          //   'layout': 'alignInput',
          //   'layoutOptions': {
          //     'indicatorColumn': true,
          //     'showDots': true,
          //     'rightPadding': false
          //   },
          //   'columns': [
          //     {
          //       'input': {
          //         'num': '322'
          //       }
          //     },
          //     {
          //       'input': {
          //         'num': '323'
          //       },
          //       'padding': {
          //         'type': 'symbol',
          //         'data': 'glyphicon-triangle-right'
          //       }
          //     }
          //   ],
          //   'indicator': '4'
          // },
          // {
          //   'labelClass': 'fullLength'
          // },
          {
            'label': 'Enter the least of amounts 1, 2 or 3',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '320'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount D x amount E',
            'labelClass': ' text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '355'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Ontario taxable income (Amount B from Part 2)',
            'labelClass': ' text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '360'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              },
              null,
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario small business income. Lesser of amount a or amount b',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '365'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            type: 'table',
            num: '400'
          },
          {
            type: 'table',
            num: '500'
          },

          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Ontario small business deduction</b> (Amount G1 <b>plus</b> amount G2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '399'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter amount H on line 402 of Schedule 5.',
            'labelClass': 'fullLength'
          },
          { labelClass: 'fullLength' },
          {
            'label': '* Enter amount B from Part 2.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** Includes the offshore areas for Nova Scotia and Newfoundland and Labrador',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 4 - Ontario Adjusted small business income',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Complete this part if the corporation was a Canadian-controlled private corporation throughout the tax year is claiming the Ontario tax credit for manufacturing and processing or the Ontario credit union tax reduction',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': ' <b>Ontario adjusted small business income.</b> Lesser of amount D and amount b',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '450'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter amount I at amount K in Part 5 of this schedule or at amount B in Part 2 of Schedule 502, <i>Ontario Tax Credit for Manufacturing and Processing</i>, whichever applies.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 5 - Calculation of credit union tax reduction',
        'rows': [
          {
            'label': 'Complete this part and Schedule 17, <i>Credit Union Deductions</i>, if the corporation was a credit union throughout the year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount D from Part 3 of Schedule 17',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '505'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'J'
                }
              }
            ]
          },
          // {
          //   'label': 'Deduct:',
          //   'labelClass': 'fullLength'
          // },
          {
            'label': 'Ontario adjusted small business income. Amount I',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '510'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'K'
                }
              }
            ]
          },
          {
            'label': 'Subtotal. Amount J <b>minus</b> amount K (if negative, enter "0").',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '515'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'L'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          // {
          //   'label': 'OSBD rate for the year (rate G from Part 3)',
          //   'labelClass': '',
          //   'layout': 'alignInput',
          //   'layoutOptions': {
          //     'indicatorColumn': true,
          //     'showDots': true,
          //     'rightPadding': false
          //   },
          //   'columns': [
          //     {
          //       'underline': 'double',
          //       'input': {
          //         'num': '520'
          //       }
          //     },
          //     {
          //       'padding': {
          //         'type': 'text',
          //         'data': '%'
          //       }
          //     },
          //     null
          //   ]
          // },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount L <b>multiplied</b> by amount G',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '525'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario domestic factor. Amount E',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '530',
                  'decimals': 5
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Ontario credit union tax reduction. </b> Amount M <b>multiplied</b> by amount N',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '535'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter amount O on line 410 of Schedule 5.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  };
})();
