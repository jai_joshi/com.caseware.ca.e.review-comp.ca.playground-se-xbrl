(function() {
  wpw.tax.create.tables('t2s389', {
    '131': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {
            label: 'Proportion of the total salaries and wages that is paid to Manitoba resident employees ' +
            'in the tax year (amount A <b>divided by</b> amount B) '
          },
          '1': {
            num: '132',
            decimals: '3'
          },
          '2': {
            label: '%C'
          }
        }
      ]
    },
    '200': {
      repeats: ['300'],
      showNumbering: true,
      columns: [
        {
          header: 'A <br>Title of eligible book',
          type: 'text',
          tn: '200', "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]},
        },
        {
          header: 'B <br>Author(s)',
          tn: '210', "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]},
          type: 'text'
        }
      ]
    },
    '300': {
      keepButtonsSpace: true,
      hasTotals: true,
      fixedRows: true,
      showNumbering: true,
      startTable: '200',
      columns: [
        {
          header: 'C <br>International standard book number ISBN',
          tn: '220', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          type: 'text'
        },
        {
          header: 'D <br>Date of publication',
          tn: '230',
          type: 'date'
        },
        {
          header: 'E <br>Number of pages',
          tn: '240', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          total: true,
          totalNum: '280',
          type: 'text'
        },
        {
          header: 'F <br>Number of books in print run',
          tn: '250', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          type: 'text'
        },
        {
          header: 'G <br>Format*',
          tn: '260',
          type: 'dropdown',
          options: [
            {
              value: '1',
              option: '1 - Hardcover'
            },
            {
              value: '2',
              option: '2 - Paperback'
            },
            {
              value: '3',
              option: '3 - Electronic'
            }
          ]
        },
        {
          header: 'H <br>Classification**',
          tn: '270',
          type: 'dropdown',
          options: [
            {
              value: '1',
              option: '1 - Fiction'
            },
            {
              value: '2',
              option: '2 - Non-fiction'
            },
            {
              value: '3',
              option: '3 - Poetry'
            },
            {
              value: '4',
              option: '4 - Drama'
            },
            {
              value: '5',
              option: '5 - Biography'
            },
            {
              value: '6',
              option: '6 - Children'
            }
          ]
        }
      ]
    },
    '330': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Contracted Manitoba labour costs paid in the tax year **'
          },
          '1': {
            'tn': '400'
          },
          '2': {
            'num': '400'
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          '3': {
            label: 'x'
          },
          '4': {
            num: '401'
          },
          '5': {
            label: '%='
          },
          '6': {
            tn: '405'
          },
          '7': {
            num: '405', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'doubleUnderline'
          },
          '8': {
            'label': 'E'
          }
        }
      ]
    },
    '801': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        'type': 'none',
        'width': '85px',
        cellClass: 'alignCenter'
      },
        {
          colClass: 'std-input-width'
        },
        {
          'width': '20px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '250px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          'width': '30px',
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }],
      'cells': [{
        '0': {
          'label': 'Amount P'
        },
        '1': {
          'num': '810'
        },
        '2': {
          'label': 'x'
        },
        '3': {
          'label': 'percentage on line 170 (in Part 1)'
        },
        '4': {
          'num': '820',
          decimals: 3
        },
        '5': {
          'label': '%='
        },
        '7': {
          'num': '830'
        },
        '8': {
          'label': 'G'
        }
      }]
    },
    '350': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'type': 'none',
          cellClass: 'alignCenter',
          width: '455px'
        },
        {
          type: 'none',
          cellClass: 'alignRight',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'small-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Eligible printing costs incurred before April 13, 2011'
          },
          '1': {
            'tn': '450'
          },
          '2': {
            'num': '450', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          '3': {
            label: 'x'
          },
          '4': {
            num: '451'
          },
          '5': {
            label: '%='
          },
          '6': {
            'num': '452',
            cellClass: 'doubleUnderline'
          },
          '7': {
            'label': 'd',
            'labelClass': 'left'
          }
        },
        {
          '0': {
            'label': 'Eligible printing costs incurred after April 12, 2011'
          },
          '1': {
            'tn': '455'
          },
          '2': {
            'num': '455', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          '3': {
            label: 'x'
          },
          '4': {
            num: '456'
          },
          '5': {
            label: '%='
          },
          '6': {
            'num': '457',
            cellClass: 'doubleUnderline'
          },
          '7': {
            'label': 'e',
            'labelClass': 'left'
          }
        }
      ]
    },
    '370': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          type: 'none'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {
            label: 'Proportion of labour related to the production of eligible books (amount a <b>divided by</b> amount b)'
          },
          '1': {
            num: '371',
            decimals: 3
          },
          '2': {
            label: '%c'
          }
        }
      ]
    },
    '380': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }],
      'cells': [
        {
          '0': {
            'label': '<b>Eligible printing costs bonus</b> (amount d <b>plus</b> amount e)'
          },
          '1': {
            tn: '460'
          },
          '2': {
            num: '460'
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          '3': {
            'label': 'K'
          }
        }
      ]
    }
  })
})();
