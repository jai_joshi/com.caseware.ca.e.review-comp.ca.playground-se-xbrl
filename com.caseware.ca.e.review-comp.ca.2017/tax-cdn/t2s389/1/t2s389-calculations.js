(function() {
  wpw.tax.create.calcBlocks('t2s389', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //eligibility
      field('132').assign(field('130').get() == 0 ? 0 : field('140').get() / field('130').get() * 100);
      if (field('CP.750').get() == 'MB' || (field('CP.750').get() == 'MJ' && field('T2S5.015').get())) {
        field('115').assign(1)
      }
      else {
        field('115').assign(2)
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //rates update
      field('401').assign(65);
      field('451').assign(10);
      field('456').assign(15);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //run calc only if credit is eligible
      var eligibilityQuestions = field('110').get() == 1 && field('115').get() == 1 && field('120').get() == 1;
      var eligibilityPercentage = field('132').get() > 25;
      if (eligibilityPercentage || eligibilityQuestions) {
        //part 3

        field('405').assign(field('400').get() * field('401').get() / 100);
        field('406').assign(field('405').get() + field('301').get());
        field('407').assign(field('280').get());
        field('371').assign(field('407').get() / field('410').get());
        field('415').assign(field('406').get() * field('371').get());
        field('430').assign(field('415').get() + field('420').get());
        field('440').assign(Math.max(field('430').get() * 0.4), 100000);
        field('452').assign(field('450').get() * field('451').get() / 100);
        field('457').assign(field('455').get() * field('456').get() / 100);
        field('460').assign(field('452').get() + field('457').get());
        field('461').assign(field('440').get() + field('460').get())
      }
      else if (calcUtils.hasChanged('110') || calcUtils.hasChanged('115') || calcUtils.hasChanged('120')) {
        calcUtils.removeValue([402, 406, 407, 371, 415, 440, 452, 457, 459, 460, 461, 450, 455, 410, 400, 301]);
      }
    });

  });
})();
