(function() {
  wpw.tax.create.diagnostics('t2s389', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3890001', common.prereq(function(tools) {
      return tools.field('t2s5.615').isNonZero();
    }, common.requireFiled('T2S389')));

    diagUtils.diagnostic('3890002', common.prereq(common.requireFiled('T2S389'),
        common.check(['110', '115', '120', '130', '140'], 'isFilled', true)));

    diagUtils.diagnostic('3890003', common.prereq(common.and(
        common.check('415', 'isNonZero'),
        common.requireFiled('T2S389')),
        function(tools) {
          var table = tools.mergeTables(tools.field('200'), tools.field('300'));
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne([row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7]], 'isNonZero');
          })
        }));

    diagUtils.diagnostic('3890004', common.prereq(common.requireFiled('T2S389'),
        function(tools) {
          var table = tools.mergeTables(tools.field('200'), tools.field('300'));
          return tools.checkAll(table.getRows(), function(row) {
            var cols = [row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7]];
            if (tools.checkMethod(cols, 'isNonZero'))
              return tools.requireAll(cols, 'isNonZero');
            else return true;
          })
        }));

    diagUtils.diagnostic('3890005', common.prereq(common.and(
        common.check('t2s5.615', 'isNonZero'),
        common.requireFiled('T2S389')),
        common.check(['400', '415', '420', '450', '455'], 'isNonZero', true)));

    diagUtils.diagnostic('389.bookCredit', common.prereq(common.requireFiled('T2S389'),
        function(tools) {
          return tools.field('132').require(function() {
            return this.get() > 25 ||
                (tools.field('110').isYes() && tools.field('115').isYes() && tools.field('120').isYes())
          })
        }));

  });
})();

