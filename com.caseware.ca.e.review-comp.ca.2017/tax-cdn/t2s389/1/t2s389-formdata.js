(function() {
  'use strict';
  wpw.tax.create.formData('t2s389', {
      formInfo: {
        abbreviation: 't2s389',
        title: 'Manitoba Book Publishing Tax Credit ',
        //subTitle: '(2011 and later tax years)',
        schedule: 'Schedule 389',
        code: 'Code 1102',
        formFooterNum: 'T2 SCH 389 E (14)',
        headerImage: 'canada-federal',
        showCorpInfo: true,
        description: [
          {
            type: 'list',
            items: [
              {
                label: 'Use this schedule to claim the Manitoba book publishing tax credit under section 10.4 ' +
                'of the <i>Manitoba <i>Income Tax Act</i></i>. You must apply no later than 18 months after the end of the ' +
                'tax year for which you are claiming the credit'
              },
              {
                label: 'You are eligible to claim this tax credit if you are a book publisher with ' +
                'a permanent establishment in Manitoba, you paid at least 25% of the wages' +
                ' and salaries to Manitoba residents, and you answered <b>yes</b> to all the' +
                ' questions in Part 1 of this schedule.'
              },
              {
                label: 'The Manitoba book publishing tax credit is a refundable credit that is equal to 40' +
                '% of eligible book publishing labour costs and non-refundable monetary advances made to authors, ' +
                'to a maximum of $100,000. An additional 10% credit is available for eligible printing costs ' +
                'incurred and paid before April 13, 2011, which increases to 15% after April 12, 2011.'
              },
              {
                label: '<b>Book publishing labour costs</b> includes reasonable salaries and wages paid in the tax' +
                ' year and before 2018 to employees resident in Manitoba on December 31 of that tax year,' +
                ' and 65% of contracted Manitoba labour costs paid in the tax year and before 2018. ' +
                'These costs are related to the publication of hardcover, paperback, or electronic books, ' +
                'but not to the marketing or promotion of books. For electronic books, the costs only ' +
                'include those incurred after April 12, 2011.'
              },
              {
                label: '<b>Eligible printing costs</b> are the costs paid in the tax year for printing, ' +
                'assembling, and binding eligible books that are printed on at least 30% post-consumer recycled' +
                ' paper and published in the tax year or in the immediately preceding year (no earlier than 2008). ' +
                'The costs must be incurred after April 9, 2008, and before 2019.' +
                ' The costs are reasonable in the circumstances and are paid by the publisher in the tax year.'
              },
              {
                label: 'An <b>eligible book</b> is a first edition, non-periodical publication that meets all' +
                ' the criteria outlined in subsection 10.4(3) of the Manitoba <i>Income Tax Act</i>.'
              },
              {
                label: 'Include a completed copy of this schedule with your <i>T2 Corporation Income Tax Return</i>.'
              }
            ]
          }
        ],
        category: 'Manitoba Forms'
      },
      sections: [
        {
          'header': 'Part 1 - Eligible publisher',
          'rows': [
            {
              'label': '1. Does the corporation primarily carry on the business of book publishing?',
              'type': 'infoField',
              'inputType': 'radio',
              'num': '110',
              'tn': '110',
              'labelWidth': '80%'
            },
            {
              'label': '2. Does the corporation have a permanent establishment in Manitoba in the tax year? ',
              'type': 'infoField',
              'inputType': 'radio',
              'num': '115',
              'tn': '115',
              'labelWidth': '80%'
            },
            {
              'label': '3. Has the corporation published at least two eligible books within the two-year period ending at the end of the tax year?',
              'type': 'infoField',
              'inputType': 'radio',
              'num': '120',
              'tn': '120',
              'labelWidth': '80%'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Proportion of Manitoba labour costs to total labour costs: '
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Salaries and wages paid to <b>Manitoba resident</b> employees in the tax year (keep a list of names, addresses and social insurance numbers, as you may be required to provide this information at a future date)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '140',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '140'
                  }
                }
              ],
              'indicator': 'A'
            },
            {
              'label': 'Total salaries and wages the corporation has paid to employees in the tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '130',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '130'
                  }
                }
              ],
              'indicator': 'B'
            },
            {
              'type': 'table',
              'num': '131'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If you answered <b>no</b> to any of questions 1 to 3, or if the percentage calculated at line C is less than 25%, you are <b>not</b> eligible for the Manitoba book publishing tax credit.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 2 - Eligible books',
          'rows': [
            {
              'type': 'table',
              'num': '200'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '300'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* Enter the number corresponding to the format of the eligible book: <b>1</b>. Hardcover <b>2</b>. Paperback <b>3</b>. Electronic',
              'labelClass': 'fullLength'
            },
            {
              'label': '** Enter the number corresponding to the classification of the eligible book: <b>1</b>. Fiction <b>2</b>. Non-fiction <b>3</b>. Poetry <b>4</b>. Drama <b>5</b>. Biography <b>6</b>. Children',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 3 - Calculation of the Manitoba book publishing tax credit',
          'rows': [
            {
              'label': '<b>Book publishing labour costs*</b> (exclude marketing, promotion, legal and accounting costs):',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Salaries and wages paid to Manitoba resident employees',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '301'
                  }
                }
              ],
              'indicator': 'D'
            },
            {
              'type': 'table',
              'num': '330'
            },
            {
              'label': 'Subtotal (amount D <b>plus</b> amount E)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '406'
                  }
                }
              ],
              'indicator': 'F'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total number of pages attributable to eligible books published in the tax year *** <br>(total from column E in table in Part 2)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '407'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'a'
                  }
                }
              ]
            },
            {
              'label': 'Total number of pages attributable to books published in the tax year ***',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '410',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '410'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'b'
                  }
                }
              ]
            },
            {
              'type': 'table',
              'num': '370'
            },
            {
              'label': '<b>Total book publishing labour costs</b> (amount F <b>multiplied by</b> amount c) ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '415',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '415'
                  }
                }
              ],
              'indicator': 'G'
            },
            {
              'label': 'Add:',
              'labelClass': 'bold'
            },
            {
              'label': 'Total non-refundable monetary advances made to authors of eligible books in the tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '420',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '420'
                  }
                }
              ],
              'indicator': 'H'
            },
            {
              'label': '<b>Total costs</b> (amount G <b>plus</b> amount H)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '430',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '430'
                  }
                }
              ],
              'indicator': 'I'
            },
            {
              'label': '<b>Manitoba book publishing tax credit</b> (amount I <b>multiplied by</b> 40%). This amount cannot exceed $100,000',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '440',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '440'
                  }
                }
              ],
              'indicator': 'J'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Add eligible printing costs bonus:',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Eligible printing costs paid in the tax year for eligible books printed on paper with at least 30% recycled content comprised of post-consumer waste paper',
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '350'
            },
            {
              'type': 'table',
              'num': '380'
            },
            {
              'label': '<b>Total Manitoba book publishing tax credit</b> (amount J <b>plus</b> amount K)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '461'
                  }
                }
              ],
              'indicator': 'L'
            },
            {
              'label': 'Enter amount L on line 615 of Schedule 5'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* Any part of a salary, wage or fee that is determined by reference to profits or revenues is deemed not to be related to the publication of books.',
              'labelClass': 'fullLength'
            },
            {
              'label': '** Contracted Manitoba labour costs are amounts paid by the publisher in the tax year and before 2018 to an individual, resident in Manitoba, who is not an employee of the publisher, or to a corporation with a permanent establishment in Manitoba. These labour costs are related to the publication of hardcover, paperback, or electronic books, but are not related to the marketing or promotion of books. For electronic books, the costs only include those incurred after April 12, 2011.',
              'labelClass': 'fullLength'
            },
            {
              'label': '*** The number of pages that make up an electronic book is to be determined by the number of pages in the most recent printed edition.',
              'labelClass': 'fullLength'
            }
          ]
        }
      ]
    });
})();
