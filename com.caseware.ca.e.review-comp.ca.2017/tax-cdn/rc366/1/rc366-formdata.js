(function() {
  'use strict';

  wpw.tax.global.formData.rc366 = {
    formInfo: {
      abbreviation: 'RC366',
      title: 'Direct Deposit Request for Businesses',
      printNum: 'RC366',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this form for one of the following reasons:',
              sublist: [
                '<b>start</b> to have your refunds and rebates deposited directly into your bank ' +
                'account at a Canadian financial institution; <b>or</b>',
                '<b>change</b> the direct deposit information that you have already given us.'
              ]
            },
            {
              label: 'You can use this form to start direct deposit if:',
              sublist: [
                'you are filling in a refund or rebate application (attach this form to your refund ' +
                'or rebate application); <b>or</b>',
                'you have a business number and at least one of the program accounts listed on the ' +
                'back of this form.'
              ]
            },
            {
              label: 'Once filled in, send this form to your tax centre. For more information, go to ' +
              'www.cra.gc.ca/taxcentre'.link('http://www.cra-arc.gc.ca/taxcentre/') + ' or ' +
              'www.cra.gc.ca/directdeposit'.link('http://www.cra-arc.gc.ca/directdeposit/')
            },
            {
              label: 'A business owner can manage direct deposit information through ' +
              '"My Business Account" at ' + 'www.cra.gc.ca/mybusinessaccount'.link('http://www.cra-arc.gc.ca/mybusinessaccount/')
            }
          ]
        }
      ],
      category: 'Client Communication'
    },
    sections: [
      {
        header: 'Part B - Direct deposit routing information - Fill in either option 1 or option 2, not both',
        rows: [
          {
            label: '<b>Option 1. All amounts from all program accounts into one bank account.</b> ' +
            'Fill in this option ' +
            'if you want the direct deposit of all refunds and rebates from all program accounts, including ' +
            'the primary account and all division or branch accounts, to be deposited in one bank account.',
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            label: 'Tick one box only',
            'inputType': 'dropdown',
            num: '900', disabled: 'true', cannotOverride: true,
            inputClass: 'std-input-col-width-2',
            'options': [
              {'option': 'Current Method of Payment', 'value': '0'},
              {'option': 'Start Refunds via Direct Deposit', 'value': '1'},
              {'option': 'Change Direct Deposit Banking Information', 'value': '2'}
            ]
          },
          {
            type: 'table', num: 'Direct_Deposit_Info'
          },
          {labelClass: 'fullLength'},
          {
            label: '<b>OR<br> Option 2. Amounts from specific program accounts into specific bank accounts.</b> ' +
            'Fill in this option to have refunds or rebates for one or more specific program accounts deposited ' +
            'into a specific bank account',
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            label: 'Tick one box only',
            'inputType': 'dropdown',
            num: '1900', disabled: 'true', cannotOverride: true,
            inputClass: 'std-input-col-width-2',
            'options': [
              {'option': 'Current Method of Payment', 'value': '0'},
              {'option': 'Start Refunds via Direct Deposit', 'value': '1'},
              {'option': 'Change Direct Deposit Banking Information', 'value': '2'}
            ]
          },
          {
            type: 'table', num: 'Direct_Deposit_Info_2'
          },
          {labelClass: 'fullLength'},
          {
            label: '<b>Other program accounts </b><br>For other program accounts, write the name and the two ' +
            'letters and last four digits of the program account in the spaces provided. For more ' +
            'information on which program accounts you can enter, read the information and instructions ' +
            'on page 2 of form RC366.',
            labelClass: 'fullLength'
          },
          {
            type: 'table', num: 'Direct_Deposit_Info_3'
          }
        ]
      },
      {
        header: 'Part C - Certification', fieldAlignRight: true,
        rows: [
          {
            label: 'You must<b> sign and date</b> this form. The CRA<b> must,</b> receive this form<b> within six ' +
            'months</b> of the date it was signed or it will<b> not</b> be processed. This form<b> must only</b> be ' +
            'signed by an individual with,<b> proper authority </b>for the business, for example, an owner, ' +
            'a partner of a partnership, a corporate director, an officer of a non-profit organization, ' +
            'a trustee of an estate, or an individual with delegated authority. An<b> authorized representative ' +
            'cannot</b> sign this form<b> unless</b> they have<b> delegated authority</b>. If the name of ' +
            'the individual signing this form does not<b> exactly </b>match CRA records, this form will ' +
            'not be processed. Forms that cannot be processed, for any reason, will be returned to the business. ' +
            'To avoid processing delays, you <b>must</b> make sure that the CRA has complete and valid ' +
            'information on your business files<b> before </b>you sign this form.<br> By<b> signing and dating</b> ' +
            'this form, you authorize the CRA to deposit payments directly into the accounts shown in Part B.',
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            label: 'The individual signing this form is:',
            labelWidth: '50%',
            width: '20%',
            inputType: 'dropdown',
            num: '400',
            textAlign: 'right', rf: true,
            options: [
              {option: 'an owner', value: '1'},
              {option: 'a partner of a partnership', value: '2'},
              {option: 'a corporate director', value: '3'},
              {option: 'an officer of a non-profit organization', value: '4'},
              {option: 'a trustee of an estate', value: '5'},
              {option: 'an individual with delegated authority', value: '6'}
            ],
            init: '1'
          },
          {
            type: 'table', num: '950'
          },
          {
            label: 'I certify that the information given on this form is correct and complete.',
            labelClass: 'fullLength'
          },
          {
            type: 'table', num: '960'
          }
        ]
      },
      {
        header: 'Information and Instructions', fieldAlignRight: true,
        rows: [
          {
            label: 'Refunds and rebates',
            labelClass: 'bold'
          },
          {
            label: 'Only refunds and rebates for the program accounts identified in Part B will be ' +
            'deposited directly into the bank accounts associated with them. ' +
            'This form only supports direct deposit from the following program accounts:',
            labelClass: 'fullLength'
          },
          {
            type: 'table', num: '999'
          }
        ]
      }
    ]
  };
})();
