(function() {
  var institutionNumber = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.bankNumber, 'value').sort(function(opt1, opt2) {
    return opt1.value - opt2.value;
  });
  wpw.tax.global.tableCalculations.rc366 = {
    '950': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          'width': '80px',
          cellClass: 'alignLeft',
          'type': 'none'
        },
        {
          'width': '320px',
          cellClass: 'alignCenter'
        },
        {
          'type': 'none'
        },
        {
          'width': '120px',
          cellClass: 'alignLeft',
          'type': 'none'
        },
        {
          'width': '280px',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width',
          cellClass: 'alignLeft',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'First name:'
          },
          '1': {
            'num': '600'
          },
          '3': {
            'label': 'Last name:'
          },
          '4': {
            'num': '601'
          }
        },
        {
          '0': {
            'label': 'Title:'
          },
          '1': {
            'num': '602'
          },
          '3': {
            'label': 'Telephone number:'
          },
          '4': {
            'num': '603', 'telephoneNumber': true, type: 'text'
          }
        },
        {
          '1': {
            type: 'none'
          },
          '3': {
            'label': 'Extension'
          },
          '4': {
            'num': '1603', type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
            telephoneNumber: true,
            validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
          }
        }
      ]
    },
    '960': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        'width': '80px',
        cellClass: 'alignLeft',
        'type': 'none'
      },
        {
          'width': '320px',
          cellClass: 'alignCenter'
        },
        {
          'type': 'none'
        },
        {
          'width': '120px',
          cellClass: 'alignLeft',
          'type': 'none'
        },
        {
          'width': '280px',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width',
          cellClass: 'alignLeft',
          'type': 'none'
        }],
      'cells': [{
        '0': {
          'label': 'Signature:'
        },
        '1': {
          'num': '604',
          type: 'text'
        },
        '3': {
          'label': 'Date:'
        },
        '4': {
          'num': '605',
          'type': 'date'
        }
      }]
    },
    '999': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        'width': '400px',
        cellClass: 'alignLeft',
        'type': 'none'
      },
        {
          'type': 'none'
        },
        {
          'width': '400px',
          cellClass: 'alignLeft',
          'type': 'none'
        },
        {
          colClass: 'small-input-width',
          cellClass: 'alignLeft',
          'type': 'none'
        }],
      'cells': [{
        '0': {
          'label': '- RC corporation income tax'
        },
        '2': {
          'label': '- RP payroll deductions'
        }
      },
        {
          '0': {
            'label': '- RD excise duty'
          },
          '2': {
            'label': '- RT goods and services tax/harmonized sales tax (GST/HST)'
          }
        },
        {
          '0': {
            'label': '- RE excise tax'
          },
          '2': {
            'label': '- RZ information returns'
          }
        },
        {
          '0': {
            'label': '- RG air travellers security charge'
          },
          '2': {
            'label': '- SL softwood lumber products export charge'
          }
        },
        {
          '0': {
            'label': '- RN insurance premium tax'
          }
        }]
    },
    'Direct_Deposit_Info': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        'width': '419px',
        'type': 'none'
      },
        {
          'header': 'Branch Number',
          'width': '100px',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          'header': 'Institution number',
          width: '78px',
          type: 'dropdown',
          options: institutionNumber,
          showValues: 'before'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          'header': 'Account Number',
          'width': '200px',
          cellClass: 'alignLeft'
        },
        {
          'type': 'none'
        }],
      'cells': [{
        '1': {
          'type': 'infoField',
          'num': '910',
          'cannotOverride': true
        },
        '2': {
          type: 'none'
        },
        '3': {
          'type': 'infoField',
          'num': '914',
          'cannotOverride': true
        },
        '5': {
          'type': 'infoField',
          'num': '918',
          'cannotOverride': true
        }
      }]
    },
    'Direct_Deposit_Info_2': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        'width': '300px',
        cellClass: 'alignCenter',
        'type': 'none'
      },
        {
          'width': '10px',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          'width': '60px',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          'header': 'Branch Number',
          'width': '100px',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          'header': 'Institution number',
          width: '55px',
          type: 'dropdown',
          options: institutionNumber,
          showValues: 'before'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          'header': 'Account Number',
          'width': '200px',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          'type': 'none'
        }],
      'cells': [{
        '0': {
          'label': 'GST/HST program account (RT)',
          'labelClass': 'bold'
        },
        '2': {
          'label': 'RT',
          'labelClass': 'bold'
        },
        '3': {
          'type': 'infoField',
          'num': '905',
          'cannotOverride': true
        },
        '5': {
          'type': 'infoField',
          'num': '906',
          'cannotOverride': true
        },
        '7': {
          'type': 'infoField',
          'num': '907',
          'cannotOverride': true
        },
        '9': {
          'type': 'infoField',
          'num': '908',
          'cannotOverride': true
        }
      },
        {
          '0': {
            'label': 'Payroll deductions program account (RP)',
            'labelClass': 'bold'
          },
          '2': {
            'label': 'RP',
            'labelClass': 'bold'
          },
          '3': {
            'type': 'infoField',
            'num': '915',
            'cannotOverride': true
          },
          '5': {
            'type': 'infoField',
            'num': '916',
            'cannotOverride': true
          },
          '7': {
            'type': 'infoField',
            'num': '917',
            'cannotOverride': true
          },
          '9': {
            'type': 'infoField',
            'num': '919',
            'cannotOverride': true
          }
        },
        {
          '0': {
            'label': 'Corporation income tax program account (RC)',
            'labelClass': 'bold'
          },
          '2': {
            'label': 'RC',
            'labelClass': 'bold'
          },
          '3': {
            'type': 'infoField',
            'num': '901',
            'cannotOverride': true
          },
          '5': {
            'type': 'infoField',
            'num': '902',
            'cannotOverride': true
          },
          '7': {
            'type': 'infoField',
            'num': '903',
            'cannotOverride': true
          },
          '9': {
            'type': 'infoField',
            'num': '904',
            'cannotOverride': true
          }
        }]
    },
    'Direct_Deposit_Info_3': {
      'showNumbering': true,
      fixedRows: true,
      'infoTable': true,
      'cannotOverride': true,
      'columns': [{
        'header': 'Name of the program account',
        'width': '270px',
        cellClass: 'alignCenter',
        'disabled': true,
        type: 'text'
      },
        {
          'width': '10px',
          'type': 'none'
        },
        {
          'header': 'Two Letters ',
          'width': '40px',
          cellClass: 'alignCenter',
          'disabled': true,
          type: 'text'
        },
        {
          'header': 'Four Digits',
          'width': '60px',
          cellClass: 'alignLeft',
          'disabled': true,
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          'header': 'Branch Number',
          'width': '100px',
          cellClass: 'alignLeft',
          'disabled': true,
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          'header': 'Institution number',
          width: '55px',
          type: 'dropdown',
          options: institutionNumber,
          showValues: 'before',
          'disabled': true
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          'header': 'Account Number',
          'width': '200px',
          cellClass: 'alignLeft',
          'disabled': true,
          type: 'text'
        },
        {
          'type': 'none'
        }]
    }
  }
})();
