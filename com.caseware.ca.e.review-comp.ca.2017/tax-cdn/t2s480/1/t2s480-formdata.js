(function() {

  wpw.tax.create.formData('t2s480', {
    formInfo: {
      abbreviation: 't2s480',
      title: 'Nunavut Investment Tax Credit',
      schedule: 'Schedule 480',
      headerImage: 'canada-federal',
      showCorpInfo: true,
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if you are a corporation with a permanent establishment (as defined in ' +
              'Regulation 400 of the federal Income Tax Regulations) in Nunavut at any time in the year ' +
              'in which the investment tax credit arose, and have made investments eligible for the investment ' +
              'tax credit under the <i>Risk Capital Investment Tax Credits Act</i>.'
            },
            {
              label: 'The credit is eligible for a seven-year carryforward and a three-year carryback. ' +
              'You cannot carry the credit back to any taxation year-ending before September 25, 1998.'
            },
            {
              label: 'The credit is limited to a maximum of $30,000 that can be claimed in a taxation year.'
            },
            {
              label: 'File one completed copy of this schedule with your T2 Corporation Income Tax Return.'
            }
          ]
        }
      ],
      category: 'Nunavut Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Total tax credit earned in the current taxation year',
        'rows': [
          {
            'type': 'table',
            'num': '100'
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of total credit available and credit available for carryforward',
        'rows': [
          {
            'label': 'Credit at end of preceding taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1001'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Deduct:</b> Credit expired after seven taxation years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '104'
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit at beginning of taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105'
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1002'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Add',
            'labelClass': 'bold'
          },
          {
            'label': 'Current year credit earned (enter amount A)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '120'
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              }
            ]
          },
          {
            'label': 'Total credit available',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1007'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Deduct: ',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit claimed in the current year (enter on line 735 in Part 2 of Schedule 5).',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '160'
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit carried back to preceding taxation year(s) (complete Part 3) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1008'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1009'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1010'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Closing balance (amount B minus amount D)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '200'
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 3 - Request for carryback of credit',
        'rows': [
          {
            'type': 'table',
            'num': '1000'
          }
        ]
      },
      {
        'header': 'Part 4 - Analysis of credit available for carryforward by year of origin',
        'rows': [
          {
            'type': 'table',
            'num': '2000'
          }
        ]
      },
      {
        'header': 'Historical Data and Calculation for PEI Corporate Investment Tax Credit',
        'rows': [
          {
            'type': 'table',
            'num': '3000'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  });
})();
