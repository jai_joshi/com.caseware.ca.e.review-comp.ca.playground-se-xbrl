(function() {

  var labelWidth= '200px';

  wpw.tax.create.tables('t2s480', {
    '100': {
      infoTable: true,
      fixedRows: true,
      hasTotals: true,
      columns: [
        {
          header: 'Type of investment',
          type: 'none'
        },
        {type: 'none', colClass: 'std-padding-width'},//space
        {type: 'none', colClass: 'std-padding-width'},//tn
        {
          header: ' Certificate number ',
          colClass: 'std-input-col-width',
          type: 'text'
        },
        {type: 'none', colClass: 'std-input-width'},//space
        {type: 'none', colClass: 'std-padding-width'},//tn
        {
          header: ' Amount of credit',
          colClass: 'std-input-width',
          total: true,
          totalMessage: 'Total tax credit ',
          totalIndicator: 'A'
        },
        {type: 'none', colClass: 'std-padding-width'}//space
      ],
      cells: [
        {
          0: {label: 'Labour-sponsored venture capital corporations'},
          2: {tn: '001'},
          3: {num: '001'},
          5: {tn: '050'},
          6: {num: '050'}
        },
        {
          0: {label: 'Community-endorsed venture capital corporations'},
          2: {tn: '002'},
          3: {num: '002'},
          5: {tn: '051'},
          6: {num: '051'}
        },
        {
          0: {label: 'Direct investment in territorial business corporations'},
          2: {tn: '003'},
          3: {num: '003'},
          5: {tn: '052'},
          6: {num: '052', cellClass: 'singleUnderline'}
        }
      ]
    },
    '1000': {
      fixedRows: true,
      infoTable: true,
      hasTotals: true,
      columns: [
        {
          width: labelWidth,
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          type: 'date'
        },
        {
          type: 'none'
        },
        {
          colClass: 'std-input-col-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',

          total: true,
          totalMessage: 'Total (enter on line C in Part 2)'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {
            label: '1st preceding taxation year'
          },
          '3': {
            label: ' Credit to be applied ',
            labelClass: 'center'
          },
          '4': {
            tn: '901'
          },
          '5': {
            num: '901'
          }
        },
        {
          '0': {
            label: '2nd preceding taxation year'
          },
          '3': {
            label: ' Credit to be applied ',
            labelClass: 'center'
          },
          '4': {
            tn: '902'
          },
          '5': {
            num: '902'
          }
        },
        {
          '0': {
            label: '3rd preceding taxation year'
          },
          '3': {
            label: ' Credit to be applied ',
            labelClass: 'center'
          },
          '4': {
            tn: '903'
          },
          '5': {
            num: '903',
            cellClass: 'singleUnderline'
          }
        }]
    },
    '2000': {
      hasTotals: true,
      fixedRows: true,
      infoTable: true,
      columns: [

        {
          width: labelWidth,
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-input-width',
          type: 'date',
          header: '<b>Year of origin</b>'
        },
        {
          type: 'none'
        },
        {
          header: '<b>Credit available for carryforward</b>',
          colClass: 'std-input-width',
          total: true,
          totalMessage: 'Total (equals line 200 in Part 2)'
        },
        {type: 'none'}
      ],
      cells: [
        {
          '0': {
            label: '7th preceding taxation year'
          }
        },
        {
          '0': {
            label: '6th preceding taxation year'
          }
        },
        {
          '0': {
            label: '5th preceding taxation year'
          }
        },
        {
          '0': {
            label: '4th preceding taxation year'
          }
        },
        {
          '0': {
            label: '3rd preceding taxation year'
          }
        },
        {
          '0': {
            label: '2nd preceding taxation year'
          }
        },
        {
          '0': {
            label: '1st preceding taxation year'
          }
        },
        {
          '0': {
            label: 'Current tax year-ending on '
          },
          3: {cellClass: 'singleUnderline'}
        }
      ]
    },
    '3000': {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {type: 'none'},
        {
          colClass: 'std-input-width',
          type: 'date',
          header: '<b>Year of origin</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',

          total: true,
          totalNum: '501',
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',
          total: true,
          totalNum: '502',

          totalMessage: ' ',
          header: '<b>Current year contribution</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',
          total: true,
          totalNum: '503',

          totalMessage: ' ',
          header: '<b>Transfer</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',  total: true, totalMessage: ' ', totalNum: '504',
          header: '<b>Amount available to apply</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',  total: true, totalMessage: ' ', disabled: true, totalNum: '505',
          header: '<b>Applied<b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',  total: true, totalMessage: ' ', disabled: true, totalNum: '506',
          header: '<b>Balance to carry forward</b>'
        },
        {type: 'none'}
      ],
      cells: [
        {
          '4': {label: '*'},
          '5': {type: 'none'},
          '7': {type: 'none'},
          '9': {type: 'none'},
          '11': {type: 'none'},
          '13': {type: 'none', label: 'N/A'}
        },
        {
          '5': {type: 'none'},
          '14': {label: '**'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '3': {type: 'none', cellClass: 'singleUnderline'},
          5: {cellClass: 'singleUnderline'},
          '7': {type: 'none'},
          9: {cellClass: 'singleUnderline'},
          11: {cellClass: 'singleUnderline'},
          13: {cellClass: 'singleUnderline'}
        }
      ]
    }
  })
})();
