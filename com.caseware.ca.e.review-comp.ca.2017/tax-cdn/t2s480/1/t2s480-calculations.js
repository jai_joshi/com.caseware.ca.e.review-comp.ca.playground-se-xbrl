(function() {

  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  wpw.tax.create.calcBlocks('t2s480', function(calcUtils) {
    //cals years for all table
    calcUtils.calc(function(calcUtils, field, form) {
      var taxEndDate = field('CP.tax_end').get();
      var tableArrays1 = [1000, 2000, 3000];
      tableArrays1.forEach(function(num) {
        var dateCol = field(num).getCol(1);
        for (var i = 0; i < dateCol.length; i++) {
          if (num == 1000) {
            var year = i - 3;
            dateCol[dateCol.length - i - 1].assign(calcUtils.addToDate(taxEndDate, year, 0, 0));
          }
          else {
            dateCol[dateCol.length - i - 1].assign(calcUtils.addToDate(taxEndDate, -i, 0, 0));
          }
        }
      })
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 4
      var summaryTable = field('3000');
      var limitOnCredit = field('1007').get();
      // historical data chart
      summaryTable.getRows().forEach(function(row, rowIndex) {
        if (rowIndex == 0) {
          // to avoid the first row being calculated to total
          row[7].assign(0);
          row[9].assign(0);
          row[11].assign(0);
          row[13].assign(0);
        } else {
          row[9].assign(Math.max(row[3].get() + row[5].get() + row[7].get(), 0));
          row[11].assign(returnAmountApplied(limitOnCredit, row[9].get()));
          row[13].assign(Math.max(row[9].get() - row[11].get(), 0));
          limitOnCredit -= row[11].get()
        }
      });
      summaryTable.cell(8, 5).assign(field('120').get());

      field('2000').getRows().forEach(function(row, rowIndex) {
        var rIndex = rowIndex + 1;
        row[3].assign(summaryTable.cell(rIndex, 13).get());
      });
    });

    //Part 2 calcs
    calcUtils.calc(function(calcUtils, field) {
      field('1001').assign(field('3000').total(3).get());
      field('104').assign(field('3000').cell(0, 3).get());
      field('105').assign(field('1001').get() - field('104').get());
      field('1002').assign(field('105').get());

      field('120').assign(field('100').total(6).get());
      field('1007').assign(field('1002').get() + field('120').get());
      field('160').assign(field('3000').total(11).get());
      field('1008').assign(field('1000').total(5).get());
      field('1009').assign(
          field('160').get() +
          field('1008').get()
      );
      field('1010').assign(field('1009').get());
      field('200').assign(Math.max(field('1007').get() - field('1010').get(), 0));
    });

  });
})
();
