(function () {

  var countries = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.countryAddressCodes, 'value');

  var columnsA = [
    {header: '', type: 'none', colClass: 'std-input-col-width-2'},
    {header: 'Regular Expenses ($)'},
    {header: 'Successor Expenses ($)'}
  ];

  function getColumnsB(columnObjArr) {
    var ret = [];

    columnObjArr.forEach(function () {

    });

    return ret
  }

  wpw.tax.create.tables('t2a15', {
    '999': {
      fixedRows: true,
      columns: columnsA,
      cells: [
        {
          0: {label: 'Balance at end of preceding taxation year'},
          1: {tn: '001', num: '001'},
          2: {tn: '011', num: '011'}
        },
        {
          0: {
            label: 'Add: transferred on amalgamation or\n' +
            'wind-up of subsidiary *'
          },
          1: {tn: '003', num: '003'},
          2: {tn: '013', num: '013'}
        },
        {
          0: {
            label: 'Add: transferred other than on amalgamation or\n' +
            'wind-up of subsidiary *'
          },
          1: {type: 'none'},
          2: {tn: '015', num: '015'}
        },
        {
          0: {
            label: 'Deduct: transferred on sale of resource property\n' +
            'to successor'
          },
          1: {tn: '005', num: '005'},
          2: {tn: '017', num: '017'}
        },
        {
          0: {label: 'Amount Available **'}
        },
        {
          0: {
            label: 'Deduct: Claim for the year per federal\n' +
            'Regulation 1202(2)'
          },
          1: {type: 'none'},
          2: {tn: '019', num: '019'}
        },
        {
          0: {
            label: 'Deduct: Claim for the year per federal\n' +
            'Regulation 1201'
          },
          1: {tn: '007', num: '007'},
          2: {type: 'none'}
        },
        {
          0: {label: 'Closing balance'},
          1: {tn: '009', num: '009'},
          2: {tn: '021', num: '021'}
        }
      ]
    },
    '998': {
      fixedRows: true,
      columns: [
        {colClass: 'std-input-col-width-2', type: 'none'},
        {}
      ],
      cells: [
        {
          0: {label: 'Balance at end of preceding taxation year'},
          1: {tn: '023', num: '023'}
        },
        {
          0: {
            label: 'Add: transferred on amalgamation or\n' +
            'wind-up of subsidiary'
          },
          1: {tn: '025', num: '025'}
        },
        {
          0: {
            label: 'Add: transferred other than on amalgamation\n' +
            'or wind-up of subsidiary'
          },
          1: {tn: '027', num: '027'}
        },
        {
          0: {
            label: 'Deduct: transferred on disposal of resource property\n' +
            'to successor'
          },
          1: {tn: '029', num: '029'}
        },
        {
          0: {label: 'Amount Available *'}
        },
        {
          0: {
            label: 'Deduct: claim for the year per federal Regulation\n' +
            '1203(1)'
          },
          1: {tn: '031', num: '031'}
        },
        {
          0: {label: 'Closing balance'},
          1: {tn: '033', num: '033'}
        }
      ]
    },
    '997': {
      fixedRows: true,
      columns: columnsA,
      cells: [
        {
          0: {label: 'Balance at end of preceding taxation year'},
          1: {tn: '041', num: '041'},
          2: {tn: '064', num: '064'}
        },
        {
          0: {label: 'Add: current year expenses excluding expenses incurred under look-back rule'},
          1: {tn: '043', num: '043'},
          2: {type: 'none'}
        },
        {
          0: {label: 'Add: current year expenses under look-back rule [federal subsection 66(12.66)]'},
          1: {tn: '044', num: '044'},
          2: {type: 'none'}
        },
        {
          0: {
            label: 'Add: reclassified Canadian development expenses\n' +
            '[federal subsections 66.1(9) and 66.7(9)]\n'
          },
          1: {tn: '045', num: '045'},
          2: {tn: '065', num: '065'}
        },
        {
          0: {
            label: 'Add: transferred on amalgamation or wind-up\n' +
            'of subsidiary *'
          },
          1: {tn: '047', num: '047'},
          2: {tn: '067', num: '067'}
        },
        {
          0: {
            label: 'Add: transferred other than on amalgamation or\n' +
            'wind-up of subsidiary *'
          },
          1: {type: 'none'},
          2: {tn: '069', num: '069'}
        },
        {
          0: {
            label: 'Add: Canadian renewable and conservation\n' +
            'expenses'
          },
          1: {tn: '049', num: '049'},
          2: {type: 'none'}
        },
        {
          0: {label: 'Add: other additions'},
          1: {tn: '051', num: '051'},
          2: {type: 'none'}
        },
        {
          0: {label: 'Deduct: government assistance and grants'},
          1: {tn: '053', num: '053'},
          2: {type: 'none'}
        },
        {
          0: {label: 'Deduct: other deductions or transfers\n'},
          1: {tn: '055', num: '055'},
          2: {tn: '077', num: '077'}
        },
        {
          0: {
            label: 'Deduct: transferred on disposition of\n' +
            'resource property to successor\n'
          },
          1: {tn: '059', num: '059'},
          2: {tn: '079', num: '079'}
        },
        {
          0: {
            label: 'Deduct: current and previous year Canadian exploration\n' +
            'expenses renounced in the year pursuant to a\n' +
            'flow-through share agreement'
          },
          1: {tn: '058', num: '058'},
          2: {type: 'none'}
        },
        {
          0: {
            label: 'Deduct: expenses renounced under look-back rule\n' +
            '[federal subsection 66(12.66)]\n'
          },
          1: {tn: '060', num: '060'},
          2: {type: 'none'}
        },
        {
          0: {label: 'Amount Available **'}
        },
        {
          0: {label: 'Deduct: current year claim per federal subsections 66.1(2) and 66.7(3)***'},
          1: {tn: '061', num: '061'},
          2: {tn: '081', num: '081'}
        },
        {
          0: {label: 'Closing balance'},
          1: {tn: '063', num: '063'},
          2: {tn: '083', num: '083'}
        }
      ]
    },
    '996': {
      fixedRows: true,
      columns: columnsA,
      cells: [
        {
          0: {label: 'Balance at end of preceding taxation year'},
          1: {tn: '091', num: '091'},
          2: {tn: '119', num: '119'}
        },
        {
          0: {label: 'Add: current year expenses excluding expenses incurred under look-back rule'},
          1: {tn: '093', num: '093'},
          2: {type: 'none'}
        },
        {
          0: {label: 'Add: current year expenses under look-back rule [federal subsection 66(12.66)]\n'},
          1: {tn: '094', num: '094'},
          2: {type: 'none'}
        },
        {
          0: {
            label: 'Add: transferred on amalgamation or wind-up\n' +
            'of subsidiary *'
          },
          1: {tn: '095', num: '095'},
          2: {tn: '121', num: '121'}
        },
        {
          0: {
            label: 'Add: transferred other than on amalgamation or\n' +
            'wind-up of subsidiary *'
          },
          1: {type: 'none'},
          2: {tn: '123', num: '123'}
        },
        {
          0: {label: 'Add: other additions'},
          1: {tn: '097', num: '097'},
          2: {type: 'none'}
        },
        {
          0: {
            label: 'Deduct: reclassified Canadian exploration expenses\n' +
            '[federal subsections 66.1(9) and 66.7(9)]'
          },
          1: {tn: '099', num: '099'},
          2: {tn: '127', num: '127'}
        },
        {
          0: {label: 'Deduct: government assistance and grants'},
          1: {tn: '101', num: '101'},
          2: {type: 'none'}
        },
        {
          0: {
            label: 'Deduct: receivable on disposition of underground\n' +
            'oil and gas storage rights or mining property'
          },
          1: {tn: '103', num: '103'},
          2: {type: 'none'}
        },
        {
          0: {
            label: 'Deduct: credit balance in the cumulative Canadian\n' +
            'oil and gas property expense pool'
          },
          1: {tn: '105', num: '105'},
          2: {tn: '133', num: '133'}
        },
        {
          0: {label: 'Deduct: other deductions or transfers'},
          1: {tn: '107', num: '107'},
          2: {tn: '135', num: '135'}
        },
        {
          0: {
            label: 'Deduct: transferred on disposition of\n' +
            'resource property to successor'
          },
          1: {tn: '111', num: '111'},
          2: {tn: '137', num: '137'}
        },
        {
          0: {
            label: 'Deduct: current and previous year Canadian development\n' +
            'expenses renounced in the year pursuant to a\n' +
            'flow-through share agreement'
          },
          1: {tn: '110', num: '110'},
          2: {type: 'none'}
        },
        {
          0: {
            label: 'Deduct: expenses renounced under look-back rule\n' +
            '[federal subsection 66(12.66)]'
          },
          1: {tn: '112', num: '112'},
          2: {type: 'none'}
        },
        {
          0: {label: 'Amount Available **'}
        },
        {
          0: {
            label: 'Deduct: current year claim per federal subsection\n' +
            '66.2(2)***'
          },
          1: {tn: '115', num: '115'},
          2: {tn: '141', num: '141'}
        },
        {
          0: {label: 'Closing balance'},
          1: {tn: '117', num: '117'},
          2: {tn: '143', num: '143'}
        }
      ]
    },
    '995': {
      fixedRows: true,
      columns: columnsA,
      cells: [
        {
          0: {label: 'Balance at the end of preceding taxation year'},
          1: {tn: '151', num: '151'},
          2: {tn: '173', num: '173'}
        },
        {
          0: {label: 'Add: current year expenses'},
          1: {tn: '153', num: '153'},
          2: {type: 'none'}
        },
        {
          0: {label: 'Add: transferred on amalgamation or wind-up of subsidiary *'},
          1: {tn: '155', num: '155'},
          2: {tn: '175', num: '175'}
        },
        {
          0: {label: 'Add: transferred other than on amalgamation or wind-up of subsidiary *'},
          1: {type: 'none'},
          2: {tn: '177', num: '177'}
        },
        {
          0: {label: 'Add: other additions'},
          1: {tn: '157', num: '157'},
          2: {type: 'none'}
        },
        {
          0: {
            label: 'Deduct: received or receivable on disposition of ' +
            'Canadian oil and gas property'
          },
          1: {tn: '159', num: '159'},
          2: {tn: '181', num: '181'}
        },
        {
          0: {label: 'Deduct: government assistance and grants'},
          1: {tn: '161', num: '161'},
          2: {type: 'none'}
        },
        {
          0: {
            label: 'Deduct: transferred on disposition of resource property\n' +
            'to successor'
          },
          1: {tn: '165', num: '165'},
          2: {tn: '185', num: '185'}
        },
        {
          0: {label: 'Deduct: other deductions or transfers'},
          1: {tn: '167', num: '167'},
          2: {tn: '187', num: '187'}
        },
        {
          0: {label: 'Amount Available **'}
        },
        {
          0: {
            label: 'Deduct: current year claim per federal subsections 66.4(2)\n' +
            'and 66.7(5) ***'
          },
          1: {tn: '169', num: '169'},
          2: {tn: '189', num: '189'}
        },
        {
          0: {label: 'Closing balance'},
          1: {tn: '171', num: '171'},
          2: {tn: '191', num: '191'}
        }
      ]
    },
    '994': {
      fixedRows: true,
      columns: columnsA,
      cells: [
        {
          0: {label: 'Balance at end of preceding taxation year'},
          1: {tn: '201', num: '201'},
          2: {tn: '213', num: '213'}
        },
        {
          0: {label: 'Add: transferred on amalgamation or wind-up of subsidiary'},
          1: {tn: '205', num: '205'},
          2: {tn: '215', num: '215'}
        },
        {
          0: {label: 'Add: transferred other than on amalgamation or wind-up of subsidiary *'},
          1: {type: 'none'},
          2: {tn: '217', num: '217'}
        },
        {
          0: {label: 'Deduct: other deductions or transfers'},
          1: {tn: '207', num: '207'},
          2: {tn: '219', num: '219'}
        },
        {
          0: {label: 'Amount Available **'}
        },
        {
          0: {label: 'Deduct: current year claim per federal subsections 66(4) and 66.7(2) ***'},
          1: {tn: '209', num: '209'},
          2: {tn: '221', num: '221'}
        },
        {
          0: {label: 'Closing balance'},
          1: {tn: '211', num: '211'},
          2: {tn: '223', num: '223'}
        }
      ]
    },
    '993': {
      fixedRows: true,
      columns: columnsA,
      cells: [
        {
          0: {label: 'Foreign-source resource income '},
          1: {tn: '231', num: '231'},
          2: {tn: '233', num: '233'}
        }
      ]
    },
    '992': {
      showNumbering: true,
      repeats: [991],
      superHeaders: [
        {
          divider: false
        },
        {
          header: 'Regular Expenses',
          divider: true,
          colspan: '4'
        }
      ],
      columns: [
        {header: 'Country in which regular expenses were incurred', tn: '241', type: 'dropdown', options: countries},
        {header: 'A<br>Balance at the end of the preceding taxation year', tn: '243'},
        {header: 'B<br>Amount transferred on amalgamation or wind-up of subsidiary*', tn: '247'},
        {header: 'C<br>Other additions', tn: '249'},
        {header: 'D<br>Other deductions or transfers', tn: '251'}
      ]
    },
    '991': {
      fixedRows: true,
      keepButtonsSpace: true,
      showNumbering: true,
      hasTotals: true,
      columns: [
        {type: 'none'},
        {header: 'E<br>Amount available<br>(A+B+C-D)**', total: true},
        {
          header: 'F<br>Current year claim per federal subsection 66(4)***',
          tn: '253',
          total: true,
          totalNum: '253T',
          totalIndicator: 'I'
        },
        {header: 'G<br>Closing balance<br>(E-F)', tn: '255'},
        {header: 'H<br>Foreign resource income****', tn: '257'}
      ]
    },
    '990': {
      showNumbering: true,
      repeats: [989],
      superHeaders: [
        {
          divider: false
        },
        {
          header: 'Successor Expenses',
          divider: true,
          colspan: '4'
        }
      ],
      columns: [
        {header: 'Country in which successor expenses were incurred', tn: '261', type: 'dropdown', options: countries},
        {header: 'J<br>Balance at the end of the preceding taxation year', tn: '263'},
        {header: 'K<br>Amount transferred on amalgamation or wind-up of subsidiary*', tn: '265'},
        {header: 'L<br>Amount transferred other than on amalgamation or wind-up of subsidiary', tn: '267'},
        {header: 'M<br>Other deductions or transfers', tn: '269'}
      ]
    },
    '989': {
      fixedRows: true,
      keepButtonsSpace: true,
      showNumbering: true,
      hasTotals: true,
      columns: [
        {type: 'none'},
        {header: 'N<br>Amount available<br>(J+K+L-M)**', total: true},
        {
          header: 'O<br>Current year claim per federal subsection 66.7(2)***',
          tn: '273',
          total: true,
          totalNum: '273T',
          totalIndicator: 'R'
        },
        {header: 'P<br>Closing balance<br>(N-O)', tn: '275'},
        {header: 'Q<br>Foreign resource income', tn: '277'}
      ]
    },
    '988': {
      showNumbering: true,
      repeats: [987],
      superHeaders: [
        {
          divider: false
        },
        {
          header: 'Regular Expenses',
          divider: true,
          colspan: '4'
        }
      ],
      columns: [
        {header: 'Country in which regular expenses were incurred', tn: '281', type: 'dropdown', options: countries},
        {header: 'AA<br>Balance at the end of the preceding taxation year', tn: '283'},
        {header: 'BB<br>Current year expenses', tn: '285'},
        {header: 'CC<br>Amount transferred on amalgamation or wind-up of subsidiary*', tn: '287'},
        {header: 'DD<br>Other additions', tn: '289'}
      ]
    },
    '987': {
      fixedRows: true,
      keepButtonsSpace: true,
      showNumbering: true,
      hasTotals: true,
      columns: [
        {header: 'EE<br>Other deductions or transfers', tn: '291'},
        {header: 'FF<br>Amount available<br>(AA+BB+CC+DD-EE)**', total: true},
        {
          header: 'GG<br>Current year claim per federal subsection 66.21(4)***',
          tn: '293',
          total: true,
          totalNum: '293T',
          totalIndicator: 'JJ'
        },
        {header: 'HH<br>Closing balance<br>(FF-GG)', tn: '295'},
        {header: 'II<br>Foreign resource income(loss)****', tn: '297'}
      ]
    },
    '986': {
      showNumbering: true,
      repeats: [985],
      superHeaders: [
        {
          divider: false
        },
        {
          header: 'Successor Expenses',
          divider: true,
          colspan: '4'
        }
      ],
      columns: [
        {header: 'Country in which successor expenses were incurred', tn: '301', type: 'dropdown', options: countries},
        {header: 'KK<br>Balance at the end of the preceding taxation year', tn: '303'},
        {header: 'LL<br>Amount transferred on amalgamation or wind-up of subsidiary*', tn: '305'},
        {header: 'MM<br>Amount transferred other than on amalgamation or wind-up of subsidiary', tn: '307'},
        {header: 'NN<br>Other deductions or transfers', tn: '309'}
      ]
    },
    '985': {
      fixedRows: true,
      keepButtonsSpace: true,
      showNumbering: true,
      hasTotals: true,
      columns: [
        {type: 'none'},
        {header: 'OO<br>Amount available<br>(KK+LL+MM-NN)**', total: true},
        {
          header: 'PP<br>Current year claim per federal subsection 66.7(2.3)***',
          tn: '313',
          total: true,
          totalNum: '313T',
          totalIndicator: 'SS'
        },
        {header: 'QQ<br>Closing balance<br>(OO-PP)', tn: '315'},
        {header: 'RR<br>Foreign resource income (loss)', tn: '317'}
      ]
    }
  })
})();
