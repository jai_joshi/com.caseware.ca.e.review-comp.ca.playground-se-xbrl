(function () {
  wpw.tax.create.formData('t2a15', {
    formInfo: {
      abbreviation: 't2a15',
      title: 'Alberta Resource Related Deductions',
      schedule: 'Schedule 15',
      formFooterNum: 'AT237 (Mar-14)',
      // headerImage: 'canada-alberta',
      showCorpInfo: true,
      category: 'Alberta Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: 'This schedule is required if the balance at the end of the preceding taxation year or the ' +
            'claim for Alberta purposes differs from that for federal purposes.',
            labelClass: 'bold'
          },
          {
            label: 'Report all monetary amounts in dollars; DO NOT include cents. Show negative amounts in brackets ( ).'
          }
        ]
      },
      {
        'header': 'AREA A - CONTINUITY OF EARNED DEPLETION BASE',
        'rows': [
          {
            'type': 'table',
            'num': '999'
          },
          {
            label: '* Earned depletion base transferred on amalgamation or wind-up to which federal subsections 87(1.2) and 88(1.5) are applicable\n' +
            'should be entered in the regular expenses column if the expenses were regular expenses in the hands of the amalgamating company\n' +
            'or the subsidiary being wound up',
            labelClass: 'fullLength indent-1'
          },
          {
            label: '** If the amount is negative, enter "0" at lines 007 and 009 and/or lines 019 and 021.',
            labelClass: 'fullLength indent-1'
          }
        ]
      },
      {
        'header': 'AREA B - CONTINUITY OF MINING EXPLORATION DEPLETION BASE',
        'rows': [
          {
            'type': 'table',
            'num': '998'
          },
          {
            label: 'Total lines 007 + 019 + 031 and carry this amount forward to Schedule 12, line 022.',
            labelClass: 'fullLength bold italic'
          },
          {
            label: '* If the amount is negative, enter "0" at lines 031 and 033.\n',
            labelClass: 'fullLength indent-1'
          }
        ]
      },
      {
        'header': 'AREA C - CUMULATIVE CANADIAN EXPLORATION EXPENSES',
        'rows': [
          {
            'type': 'table',
            'num': '997'
          },
          {
            label: 'Total lines 061 + 081 and carry this amount forward to Schedule 12, line 026.',
            labelClass: 'fullLength bold italic'
          },
          {
            label: '* Canadian exploration expenses transferred on amalgamation or wind-up to which federal subsections 87(1.2) and 88(1.5)\n' +
            'are applicable should be entered in the regular expenses column if the expenses were regular expenses in the hands of\n' +
            'the amalgamating company or the subsidiary being wound up',
            labelClass: 'fullLength indent-1'
          },
          {
            label: '** If the amount in the regular expenses column is negative, include it in income in the "Other" ' +
            'area on line 040 on Schedule 12 and enter "0" at lines 061 and 063 above. If the amount in the successor ' +
            'expenses column is negative, include it in income in the "Other" area at line 040 on Schedule 12 and enter "0" at lines 081 and 083 above.',
            labelClass: 'fullLength indent-1'
          },
          {
            label: '*** The maximum deduction is the amount available in the regular expenses column plus ' +
            'the lesser of the amount available is the successor expenses column and the amount determined ' +
            'pursuant to federal paragraph 66.7(3)(b), which in most cases will be the income attributable to ' +
            'the disposition of successored properties and the production income from successored properties.',
            labelClass: 'fullLength indent-1'
          }
        ]
      },
      {
        'header': 'AREA D - CUMULATIVE CANADIAN DEVELOPMENT EXPENSES\n',
        'rows': [
          {
            'type': 'table',
            'num': '996'
          },
          {
            label: 'Total lines 115 + 141 and carry this amount forward to Schedule 12, line 028.',
            labelClass: 'fullLength bold'
          },
          {
            label: '* Canadian development expenses transferred on amalgamation or wind-up to which federal ' +
            'ubsections 87(1.2) and 88(1.5) are applicable should be entered in the regular expenses column ' +
            'if the expenses were regular expenses in the hands of the amalgamating company or the subsidiary being wound up',
            labelClass: 'fullLength indent-1'
          },
          {
            label: '** (i) When the amount available in the successor expenses column is negative and there is no ' +
            'designation pursuant to federal subparagraph 66.7(4)(a)(iii), enter the amount at line 107. ' +
            'However, if a designation pursuant to federal subparagraph 66.7(4)(a)(iii) has been made, ' +
            'enter the negative amount available from the successor expenses column at line 167 in Area E ' +
            '"Cumulative Canadian Oil and Gas Property Expenses". If this results in a negative amount in the ' +
            'regular expenses column of Area E, enter the amount at line 105 above. In both instances, ' +
            'enter "0" at lines 141 and 143 above.',
            labelClass: 'fullLength indent-1'
          },
          {
            label: '(ii) If the amount in the regular expenses column is negative, include it in ' +
            'income in the "Other" area at line 040 on Schedule 12 and enter "0" at lines 115 and 117 above.',
            labelClass: 'fullLength indent-2'
          },
          {
            label: '*** The maximum deduction is 30% of the amount available in the regular expenses column ' +
            'plus the lesser of 30% of the amount available in the successor expenses column and ' +
            'the amount determined pursuant to federal paragraph 66.7(4)(b). In most cases, this will be the ' +
            'income attributed to the production income from successored properties. For a fiscal period of less ' +
            'than 51 weeks, the amount that can be claimed as a deduction is prorated based on the proportion ' +
            'that the number of days in the taxation year is of 365.',
            labelClass: 'fullLength indent-1'
          }
        ]
      },
      {
        header: 'AREA E - CUMULATIVE CANADIAN OIL AND GAS PROPERTY EXPENSES (CCOGPE)',
        rows: [
          {
            'type': 'table',
            'num': '995'
          },
          {
            label: 'Total lines 169 + 189 and carry this amount forward to Schedule 12, line 032.',
            labelClass: 'fullLength bold italic'
          },
          {
            label: '* Canadian oil and gas property expenses transferred on amalgamation or wind-up to which federal subsections 87(1.2) and 88(1.5) are applicable should be entered in\n' +
            'the regular expenses column if the expenses were regular expenses in the hands of the amalgamating company or the subsidiary being wound up.',
            labelClass: 'fullLength indent-1'
          },
          {
            label: '** (i) When the amount available in the successor expenses column is negative and there is no designation pursuant to subparagraph 66.7(4)(a)(iii), enter the amount at\n' +
            'line 167 and enter "0" at lines 189 and 191. If this results in the amount available in the regular expenses column becoming negative, enter the negative amount at line 133 in\n' +
            'Area D " Cumulative Canadian Development Expenses" and enter "0" at lines 169 and 171 above. If the amount available in the successor expenses column of Area D\n' +
            'becomes negative, enter the amount at line 107 in Area D.\n' +
            'When a designation pursuant to subparagraph 66.7(4)(a)(iii) has been made, enter the negative amount available from the successor expenses column at line 133 in Area\n' +
            'D "Cumulative Canadian Development Expenses" and enter "0" at lines 189 and 191 above. If the amount available in the successor expenses column in Area D becomes\n' +
            'negative, enter the negative amount at line 167 above. If this results in a negative amount in the regular expenses column of Area E above,\n' +
            'enter the amount at line 105 in Area D.',
            labelClass: 'fullLength indent-1'
          },
          {
            label: '(ii) When the amount available in the regular expenses column is negative due to other than (i) above, enter the amount at line 105, Area D "Cumulative Canadian\n' +
            'Development Expenses" and enter "0" at lines 169 and 171',
            labelClass: 'fullLength indent-2'
          },
          {
            label: '*** The maximum deduction is 10% of the amount available in the regular expenses column plus the lesser of 10% of the amount available in the successor expenses\n' +
            'column and the amount determined pursuant to federal paragraph 66.7(5)(b). In most cases, this will be the income attributable to the production income from successored\n' +
            'properties. For a fiscal period of less than 51 weeks, the amount that can be claimed as a deduction is prorated based on the proportion that the number of days in the taxation\n' +
            'year is of 365',
            labelClass: 'fullLength indent-1'
          }
        ]
      },
      {
        header: 'AREA F - FOREIGN EXPLORATION AND DEVELOPMENT EXPENSES',
        rows: [
          {
            label: 'Foreign exploration and development expenses are those that are not in respect of a country. If they are in respect of a country, complete AREA G or H',
            labelClass: 'fullLength'
          },
          {
            'type': 'table',
            'num': '994'
          },
          {
            label: '* Foreign exploration and development expenses transferred on amalgamation or wind-up to which federal subsections 87(1.2) and 88(1.5) apply should be\n' +
            'entered in the regular expenses column if the expenses were regular expenses in the hands of the amalgamating company or the subsidiary being wound up.',
            labelClass: 'fullLength indent-1'
          },
          {
            label: '** If the amount is negative, include it in income in the "Other" area at line 040 on Schedule 12 and enter "0" at lines 209 and 211 and/or lines 221 and 223.',
            labelClass: 'fullLength indent-1'
          },
          {
            label: '*** The maximum deduction for regular expenses is the lesser of: (a) the amount available in the regular expenses column; and (b) the greater of foreign-source\n' +
            'resource income and 10% of the amount available in the regular expenses column. For successor expenses, the maximum allowable is the lesser of the amount available\n' +
            'and foreign-source resource income attributable to successored properties. Foreign-source resource income includes income from oil and gas wells or mines outside\n' +
            'Canada and proceeds less applicable expenses and reserves on disposition of foreign resource property. For a fiscal period of less than 51 weeks, 10% is prorated based\n' +
            'on the proportion that the number of days in the taxation year is of 365.',
            labelClass: 'fullLength indent-1'
          },
          {
            'type': 'table',
            'num': '993'
          }
        ]
      },
      {
        header: 'AREA G - SPECIFIED FOREIGN EXPLORATION AND DEVELOPMENT EXPENSES',
        rows: [
          {
            label: 'Specified foreign exploration and development expenses are those that are in respect of a specific country and have been incurred before 2001. If they are in respect\n' +
            '  of two or more countries, determine a reasonable allocation to each country and maintain a consistent allocation in the following years.',
            labelClass: 'fullLength'
          },
          {
            'type': 'table',
            'num': '992'
          },
          {
            'type': 'table',
            'num': '991'
          },
          {
            'type': 'table',
            'num': '990'
          },
          {
            'type': 'table',
            'num': '989'
          },
          {
            label: '* Foreign exploration and development expenses transferred on amalgamation or wind-up to which federal subsections 87(1.2) and 88(1.5) apply\n' +
            'should be entered in column C if the expenses were regular expenses in the hands of the amalgamating company or the subsidiary being wound up.',
            labelClass: 'fullLength indent-1'
          },
          {
            label: '** If an amount in column E is negative, include it as income in the "Other" area at line 040 on Schedule 12, and enter "0" at the respective\n' +
            'lines 253 and 255 above. If an amount in column N is negative, include it as income in the "Other" area at line 040 on Schedule 12, and\n' +
            'enter "0" at the respective lines 273 and 275 above.',
            labelClass: 'fullLength indent-1'
          },
          {
            label: '*** (i) The maximum deduction for regular expenses is the lesser of:',
            labelClass: 'fullLength indent-1'
          },
          {
            label: 'a) the total of all amounts available in column E; and ',
            labelClass: 'fullLength indent-3'
          },
          {
            label: 'b) the greater of the total of all amounts in column H and 10% of the total of all amounts available in column E (for a fiscal period of less than\n' +
            '51 weeks, 10% is prorated based on the number of days in the taxation year divided by 365).\n' +
            'The deduction claimed must be allocated to a particular country according to federal subsection 66(4.2)\n',
            labelClass: 'fullLength indent-3'
          },
          {
            label: '(ii) The maximum deduction for successor expenses is the lesser of:',
            labelClass: 'fullLength indent-2'
          },
          {
            label: 'a) the total of all amounts available in column N; and ',
            labelClass: 'fullLength indent-3'
          },
          {
            label: 'b) the total of all amounts in column Q attributable to successored properties [foreign resource income is calculated in accordance\n' +
            'with federal paragraph 66.7(2)(b)].\n' +
            'The deduction claimed must be allocated to a particular country according to federal subsection 66.7(2.2).',
            labelClass: 'fullLength indent-3'
          },
          {
            label: '**** The amount in column H is the excess of foreign resource income over the amount claimed under federal subsection 66.7(2).',
            labelClass: 'fullLength indent-1'
          }
        ]
      },
      {
        header: 'AREA H - CUMULATIVE FOREIGN RESOURCE EXPENSES',
        rows: [
          {
            label: 'Foreign resource expenses are those that are in respect of a specific country and that have been incurred in a taxation year beginning in 2001 or after. If there are two\n' +
            'Enter the total of lines 209, 221, I, R, JJ, and SS at line 030 of Schedule 12.\n' +
            'or more countries, determine a reasonable allocation to each country and maintain a consistent allocation in the following years.',
            labelClass: 'fullLength'
          },
          {
            'type': 'table',
            'num': '988'
          },
          {
            'type': 'table',
            'num': '987'
          },
          {
            'type': 'table',
            'num': '986'
          },
          {
            'type': 'table',
            'num': '985'
          },
          {
            label: '* Foreign resource expenses transferred on amalgamation or wind-up to which federal subsections 87(1.2) and 88(1.5) apply should be entered in ' +
            'column CC if the expenses were regular expenses in the hands of the amalgamating company or the subsidiary being wound up.',
            labelClass: 'fullLength indent-1'
          },
          {
            label: '** If an amount in column FF is negative, include it as income in the "Other" area at line 040 on Schedule 12, and enter "0" at the respective lines ' +
            '293 and 295 above. If an amount in column OO is negative, include it as income in the "Other" area at line 040 on Schedule 12, and enter "0" at ' +
            'the respective lines 313 and 315 above',
            labelClass: 'fullLength indent-1'
          },
          {
            label: '*** (i) The maximum deduction for regular expenses is the total of A and B where:',
            labelClass: 'fullLength indent-2'
          },
          {
            label: 'A = the greater of: (i) 10% of the amount available in column FF; and (ii) the least of the following amounts: (a) 30% of the amount\n' +
            'available in column FF; (b) the foreign resource income for the particular country in column II; or (c) the total of all amounts in column II. For ' +
            ' a fiscal period of less than 51 weeks, 10% and 30% are prorated based on the number of days in the taxation year divided by 365.\n',
            labelClass: 'fullLength indent-3'
          },
          {
            label: 'B = the lesser of: (i) the excess of the amount in column FF minus the amount A above; and (ii) the global foreign resource limit for\n' +
            'the year designated for that country.',
            labelClass: 'fullLength indent-3'
          },
          {
            label: '(ii) The maximum deduction for successor expenses is the lesser of:',
            labelClass: 'fullLength indent-2'
          },
          {
            label: 'a) 30% of the amount available in column OO (for a fiscal period of less than 51 weeks, 30% is prorated based on the number of days in the taxation year divided by 365); and',
            labelClass: 'fullLength indent-3'
          },
          {
            label: 'b) the foreign resource income in column RR attributable to successored properties [foreign resource income is calculated in accordance with\n' +
            'federal paragraph 66.7(2.3)(b)].',
            labelClass: 'fullLength indent-3'
          },
          {
            label: '**** Column II is the excess of foreign resource income over the total of any amount designated under federal subparagraph 59(1)(b)(ii) and claimed\n' +
            'under federal subsections 66(4), 66.7(2), and 66.7(2.3).',
            labelClass: 'fullLength indent-1'
          },
          {
            label: 'Enter the total of lines 209, 221, I, R, JJ, and SS at line 030 of Schedule 12.',
            labelClass: 'fullLength bold'
          }
        ]
      }
    ]
  });
})();
