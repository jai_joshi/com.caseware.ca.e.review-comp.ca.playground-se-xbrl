(function () {

  wpw.tax.create.calcBlocks('t2a15', function (calcUtils) {
    calcUtils.calc(function (calcUtils, field) {
      for (var i = 1; i < 3; i++) {
        var table = field('999');
        table.cell(4, i).assign(
          table.cell(0, i).get() +
          table.cell(1, i).get() +
          table.cell(2, i).get() -
          table.cell(3, i).get()
        );
        if (table.cell(4, i).get() >= 0) {
          table.cell(5, i).disabled(false);
          table.cell(6, i).disabled(false);
          table.cell(7, i).assign(
            table.cell(4, i).get() -
            table.cell(5, i).get() -
            table.cell(6, i).get()
          )
        } else {
          table.cell(5, i).assign(0);
          table.cell(6, i).assign(0);
          table.cell(7, i).assign(0);
        }
      }
    });

    calcUtils.calc(function (calcUtils, field) {
      field('998').cell(4, 1).assign(
        field('023').get() +
        field('025').get() +
        field('027').get() -
        field('029').get()
      );

      if (field('998').cell(4, 1) >= 0) {
        field('998').cell(5, 1).disabled(false);
        field('033').assign(field('998').cell(4, 1).get() - field('031').get())
      } else {
        field('031').assign(0);
        field('033').assign(0);
      }
    });

    calcUtils.calc(function (calcUtils, field) {
      for (var i = 1; i < 3; i++) {
        var table = field('997');
        table.cell(13, i).assign(
          table.cell(0, i).get() +
          table.cell(1, i).get() +
          table.cell(2, i).get() +
          table.cell(3, i).get() +
          table.cell(4, i).get() +
          table.cell(5, i).get() +
          table.cell(6, i).get() +
          table.cell(7, i).get() -
          table.cell(8, i).get() -
          table.cell(9, i).get() -
          table.cell(10, i).get() -
          table.cell(11, i).get() -
          table.cell(12, i).get()
        );
        if (table.cell(13, i).get() >= 0) {
          table.cell(14, i).disabled(false);
          table.cell(15, i).assign(
            table.cell(13, i).get() -
            table.cell(14, i).get()
          )
        } else {
          table.cell(14, i).assign(0);
          table.cell(15, i).assign(0);
        }
      }
    });

    calcUtils.calc(function (calcUtils, field) {
      for (var i = 1; i < 3; i++) {
        var table = field('996');
        table.cell(14, i).assign(
          table.cell(0, i).get() +
          table.cell(1, i).get() +
          table.cell(2, i).get() +
          table.cell(3, i).get() +
          table.cell(4, i).get() +
          table.cell(5, i).get() -
          table.cell(6, i).get() -
          table.cell(7, i).get() -
          table.cell(8, i).get() -
          table.cell(9, i).get() -
          table.cell(10, i).get() -
          table.cell(11, i).get() -
          table.cell(12, i).get() -
          table.cell(13, i).get()
        );
        if (table.cell(14, i).get() >= 0) {
          table.cell(15, i).disabled(false);
          table.cell(16, i).assign(
            table.cell(14, i).get() -
            table.cell(15, i).get()
          )
        } else {
          table.cell(15, i).assign(0);
          table.cell(16, i).assign(0);
        }
      }
    });

    calcUtils.calc(function (calcUtils, field) {
      for (var i = 1; i < 3; i++) {
        var table = field('995');
        table.cell(9, i).assign(
          table.cell(0, i).get() +
          table.cell(1, i).get() +
          table.cell(2, i).get() +
          table.cell(3, i).get() +
          table.cell(4, i).get() -
          table.cell(5, i).get() -
          table.cell(6, i).get() -
          table.cell(7, i).get() -
          table.cell(8, i).get()
        );
        if (table.cell(9, i).get() >= 0) {
          table.cell(10, i).disabled(false);
          table.cell(11, i).assign(
            table.cell(9, i).get() -
            table.cell(10, i).get()
          )
        } else {
          table.cell(10, i).assign(0);
          table.cell(11, i).assign(0);
        }
      }
    });
    calcUtils.calc(function (calcUtils, field) {
      for (var i = 1; i < 3; i++) {
        var table = field('994');
        table.cell(4, i).assign(
          table.cell(0, i).get() +
          table.cell(1, i).get() +
          table.cell(2, i).get() -
          table.cell(3, i).get()
        );
        if (table.cell(4, i).get() >= 0) {
          table.cell(5, i).disabled(false);
          table.cell(6, i).assign(
            table.cell(4, i).get() -
            table.cell(5, i).get()
          )
        } else {
          table.cell(5, i).assign(0);
          table.cell(6, i).assign(0);
        }
      }
    });

    calcUtils.calc(function (calcUtils, field) {
      field('991').getRows().forEach(function (row, rIndex) {
        var mainTableRow = field('992').getRow(rIndex);
        row[1].assign(
          mainTableRow[1].get() +
          mainTableRow[2].get() +
          mainTableRow[3].get() -
          mainTableRow[4].get()
        );
        row[3].assign(row[1].get() - row[2].get());
      });
    });

    calcUtils.calc(function (calcUtils, field) {
      field('989').getRows().forEach(function (row, rIndex) {
        var mainTableRow = field('990').getRow(rIndex);
        row[1].assign(
          mainTableRow[1].get() +
          mainTableRow[2].get() +
          mainTableRow[3].get() -
          mainTableRow[4].get()
        );
        row[3].assign(row[1].get() - row[2].get());
      });
    });

    calcUtils.calc(function (calcUtils, field) {
      field('987').getRows().forEach(function (row, rIndex) {
        var mainTableRow = field('988').getRow(rIndex);
        row[1].assign(
          mainTableRow[1].get() +
          mainTableRow[2].get() +
          mainTableRow[3].get() +
          mainTableRow[4].get() -
          row[0].get()
        );
        row[3].assign(row[1].get() - row[2].get());
      });
    });

    calcUtils.calc(function (calcUtils, field) {
      field('985').getRows().forEach(function (row, rIndex) {
        var mainTableRow = field('986').getRow(rIndex);
        row[1].assign(
          mainTableRow[1].get() +
          mainTableRow[2].get() +
          mainTableRow[3].get() -
          mainTableRow[4].get()
        );
        row[3].assign(row[1].get() - row[2].get());
      });
    });

  });
})();
