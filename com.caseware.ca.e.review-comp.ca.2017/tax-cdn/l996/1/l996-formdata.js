(function() {

  var saveFunc = function() {
    wpw.tax.field('save').set(1);
  };

  wpw.tax.global.formData.l996 = {
      formInfo: {
        abbreviation: 'L996',
        title: 'Line 996 - Amended Tax Return - Description of Changes',
        showCorpInfo: true,
        description: [
          {text: 'If you answer "Yes" to the question on line 997: Is this an amended ' +
          'federal income tax return?, you must ' +
          'complete line 996 by providing a detailed description of the changes'
          }
        ],
        category: 'Workcharts'
      },
      sections: [
        {
          hideFieldset: true,
          rows: [
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'infoField',
              'label': 'Filing date of the amended tax return',
              'num': '100',
              'inputType': 'date',
              'labelWidth': '40%'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'infoField',
              'label': 'Short summary of changes',
              'num': '101',
              'inputClass': 'std-input-col-padding-width-3'

            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'infoField',
              'label': 'Detailed description of changes (Maximum 500 lines)',
              'inputType': 'none',
              'labelWidth': '90%'
            },
            {
              'type': 'infoField',
              'inputType': 'textArea',
              'maxLength': 500,
              'lineLength': 78,
              'width': '100%',
              'tn': '996',
              'num': '996'
            },
            {
              'type': 'infoField',
              'inputType': 'functionButton',
              'label': 'Save to history form',
              'labelClass': 'bold',
              'btnLabel': 'Save',
              'num': '999',
              'paramFn': saveFunc
            }
          ]
        }
      ]
    };
})();
