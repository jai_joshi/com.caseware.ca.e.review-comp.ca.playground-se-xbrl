(function () {

  wpw.tax.create.calcBlocks('l996', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field) {
      if (!field('history').get()) {
        field('history').set({});
      }

      if (field('save').get() === 1) {
        field('history').setKey(wpw.tax.utilities.milliseconds(field('100').get()), {
          '100': field('100').get(),
          '101': field('101').get(),
          '996': field('996').get()
        });
        field('save').assign(0);
      }
    });
  });
})();
