(function() {

  var tableCols = [
    {
      colClass: 'std-input-col-padding-width',
      'type': 'none'
    },
    {
      colClass: 'small-input-width',
      'formField': true
    },
    {
      'type': 'none'
    },
    {
      'type': 'none',
      colClass: 'std-padding-width'
    },
    {
      colClass: 'std-input-width',
      'formField': true
    },
    {
      'type': 'none',
      colClass: 'std-padding-width'
    }
  ];

  wpw.tax.global.tableCalculations.t2s53 = {
    '700': {
      'fixedRows': true,
      'infoTable': true,
      'columns': tableCols,
      'cells': [
        {
          '0': {
            'label': '(line 150 <b>multiplied</b> by',
            'labelClass': 'fullLength'
          },
          '1': {
            'num': '191', decimals: 2
          },
          '2': {
            'label': '(the general rate factor for the tax year))', cellClass: 'alignLeft'
          },
          '3': {
            tn: '190'
          },
          '4': {
            'num': '190', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          '5': {
            indicator: 'D'
          }
        }
      ]
    },
    '800': {
      'fixedRows': true,
      'infoTable': true,
      'columns': tableCols,
      'cells': [
        {
          '0': {
            'label': '(amount M1 <b>multiplied</b> by',
            'labelClass': 'fullLength'
          },
          '1': {
            'num': '501', decimals: 2
          },
          '2': {
            'label': ')'
          },
          '3': {
            tn: '500'
          },
          '4': {
            cellClass: 'doubleUnderline',
            'num': '500', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        }
      ]
    },
    '900': {
      'fixedRows': true,
      'infoTable': true,
      'columns': tableCols,
      'cells': [
        {
          '0': {
            'label': '(amount M2 <b>multiplied</b> by',
            'labelClass': 'fullLength'
          },
          '1': {
            'num': '521', decimals: 2
          },
          '2': {
            'label': ')'
          },
          '3': {
            tn: '520'
          },
          '4': {
            cellClass: 'doubleUnderline',
            'num': '520', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        }
      ]
    },
    '1000': {
      'fixedRows': true,
      'infoTable': true,
      'columns': tableCols,
      'cells': [
        {
          '0': {
            'label': '(amount M3<b> multiplied</b> by',
            'labelClass': 'fullLength'
          },
          '1': {
            'num': '541', decimals: 2
          },
          '2': {
            'label': ')'
          },
          '3': {
            tn: '540'
          },
          '4': {
            cellClass: 'doubleUnderline',
            'num': '540', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        }
      ]
    },
    //Future tax consequences carried back
    //First previous year
    '750': {
      fixedRows: 'true',
      hasTotals: 'true',
      superHeaders: [
        {
          header: 'Future tax consequences that occur for the current year' +
          '<br>Amount carried back from the current year to the first prior year',
          colspan: '6'
        }
      ],
      columns: [
        {
          header: 'Non-capital loss carry-back (paragraph 111(1)(a) ITA)'
        },
        {
          header: 'Capital loss carry-back'
        },
        {
          header: 'Restricted farm loss carry-back'
        },
        {
          header: 'Farm loss carry-back'
        },
        {
          header: 'Other'
        },
        {
          header: '<b>Total carrybacks</b>'
        }]
    },
    //Second previous year
    '850': {
      fixedRows: 'true',
      hasTotals: 'true',
      superHeaders: [
        {
          header: 'Future tax consequences that occur for the current year' +
          '<br>Amount carried back from the current year to the second prior year',
          colspan: '6'
        }
      ],
      columns: [
        {
          header: 'Non-capital loss carry-back (paragraph 111(1)(a) ITA)'
        },
        {
          header: 'Capital loss carry-back'
        },
        {
          header: 'Restricted farm loss carry-back'
        },
        {
          header: 'Farm loss carry-back'
        },
        {
          header: 'Other'
        },
        {
          header: '<b>Total carrybacks</b>'
        }]
    },
    //Third previous year
    '950': {
      fixedRows: 'true',
      hasTotals: 'true',
      superHeaders: [
        {
          header: 'Future tax consequences that occur for the current year' +
          '<br>Amount carried back from the current year to the third prior year',
          colspan: '6'
        }
      ],
      columns: [
        {
          header: 'Non-capital loss carry-back (paragraph 111(1)(a) ITA)'
        },
        {
          header: 'Capital loss carry-back'
        },
        {
          header: 'Restricted farm loss carry-back'
        },
        {
          header: 'Farm loss carry-back'
        },
        {
          header: 'Other'
        },
        {
          header: '<b>Total carrybacks</b>'
        }]
    }
  }
})();
