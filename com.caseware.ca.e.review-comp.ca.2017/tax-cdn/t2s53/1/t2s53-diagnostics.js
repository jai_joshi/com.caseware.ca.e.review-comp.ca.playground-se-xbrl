(function() {
  wpw.tax.create.diagnostics('t2s53', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('053.200', common.prereq(common.and(
        common.requireFiled('T2S53'),
        common.check(['200', 't2j.320'], 'isFilled', true)),
        function(tools) {
          return tools.field('200').require(function() {
            return this.get() <= tools.field('t2j.320').get();
          })
        }));

    diagUtils.diagnostic('0530030', common.prereq(common.and(
        common.requireFiled('T2S53'),
        common.check(['490'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(
              ['100', '110', '120', '130', '140', '200', '210', '220', '230', '240', '300', '310']),
              'isNonZero');
        }));

    diagUtils.diagnostic('0530040', common.prereq(common.and(
        common.requireFiled('T2S53'),
        common.check(['290'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['220', '230', '240']), 'isNonZero');
        }));

    diagUtils.diagnostic('0070007', common.prereq(
        common.and(
          common.requireFiled('T2S53'),
          common.check(['t2j.440'], 'isPositive'),
            common.check(['110'], 'isNonZero')),
        common.requireFiled('T2S7')));

    diagUtils.diagnostic('053.1100', common.prereq(common.check(['4350'], 'isNonZero'), common.check('1100', 'isFilled')));

    diagUtils.diagnostic('053.1101', common.prereq(common.check(['4427'], 'isNonZero'), common.check('1101', 'isFilled')));

    diagUtils.diagnostic('053.checkBoxes', common.prereq(
        function(tools) {
          return tools.field('cp.Corp_Type').get() != '1';
        },
        function(tools) {
          return tools.requireAll(tools.list(['010', '011', '012', '013', '014', '015', '016', '017']), function() {
            return this.get() == '2' || wpw.tax.utilities.isNullUndefined(this.get());
          })
        }));

  });
})();
