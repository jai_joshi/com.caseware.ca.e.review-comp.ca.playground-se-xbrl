(function() {
  'use strict';

  wpw.tax.global.formData.t2s53 = {
    'formInfo': {
      'abbreviation': 'T2S53',
      isRepeatForm: true,
      'title': 'General Rate Income Pool (GRIP) Calculation',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      'schedule': 'Schedule 53',
      'showCorpInfo': true,
      code: 'Code 1602',
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH53 E (17)',
      'description': [
        {
          'type': 'list',
          'items': [
            {
              label: 'If you are a Canadian-controlled private corporation (CCPC) or a deposit insurance ' +
              'corporation (DIC), use this schedule to determine the general rate income pool (GRIP).'
            },
            {
              label: 'All legislative references are to the <i>Income Tax Act</i> and the <i>Income Tax Regulations.</i>'
            },
            {
              label: 'When an eligible dividend was paid in the tax year, file a completed copy of this schedule ' +
              'with your T2 <i>Corporation Income Tax Return</i>. Do not send your worksheets with your return, ' +
              'but keep them in your records in case we ask to see them later.'
            },
            {
              label: 'Subsection 89(1) defines the terms <b>eligible dividend, excessive eligible dividend designation, ' +
              'general rate income pool</b>, and <b>low rate income pool.</b>'
            }
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    'sections': [
      {
        header: 'Eligibility Questions',
        rows: [
          {
            label: 'Change in the type of corporation',
            labelClass: 'fullLength bold'
          },
          {
            type: 'infoField',
            num: '010',
            label: '1. Was the corporation a CCPC during its preceding taxation year?',
            labelClass: 'tabbed',
            inputType: 'radio',
            init: false
          },
          {
            type: 'infoField',
            num: '011',
            label: '2. Corporations that become a CCPC or a DIC',
            labelClass: 'tabbed',
            inputType: 'radio',
            canClear: true
          },
          {
            label: 'If the answer to question 2 is yes, complete Part 4.',
            labelClass: 'bold fullLength tabbed2'
          },
          {
            labelClass: 'fullLength'
          },
          {
            label: 'Amalgamation (first year of filling after amalgamation)',
            labelClass: 'fullLength bold'
          },
          {
            type: 'infoField',
            num: '012',
            label: '3. Corporations that were formed as a result of an amalgamation',
            labelClass: 'tabbed',
            inputType: 'radio',
            init: false
          },
          {
            label: 'If the answer to question 3 is yes, answer questions 4 and 5. If the answer is' +
            ' no, go to question 6.',
            labelClass: 'fullLength bold tabbed2'
          },
          {
            type: 'infoField',
            num: '013',
            label: '4. Was one or more of the predecessor corporations neither a CCPC nor a DIC',
            labelClass: 'tabbed',
            inputType: 'radio',
            canClear: true
          },
          {
            label: 'If the answer to question 4 is yes, complete Part 4.',
            labelClass: 'fullLength bold tabbed2'
          },
          {
            type: 'infoField',
            num: '014',
            label: '5. Was one or more of the predecessor corporation a CCPC or a DIC during the ' +
            'taxation year that ended immediately before amalgamation?',
            labelClass: 'tabbed',
            inputType: 'radio',
            canClear: true
          },
          {
            label: 'If the answer to question 5 is yes, complete Part 3.',
            labelClass: 'fullLength tabbed2 bold'
          },
          {
            labelClass: 'fullLength'
          },
          {
            label: 'Winding-up',
            labelClass: 'fullLength bold'
          },
          {
            type: 'infoField',
            num: '015',
            label: '6. Has the corporation wound-up a subsidiary in the preceding taxation year?',
            labelClass: 'tabbed',
            inputType: 'radio',
            init: false
          },
          {
            label: 'If the answer to question 6 is yes, answer questions 7 and 8. If the answer is' +
            ' no, go to Part 1',
            labelClass: 'fullLength bold tabbed2'
          },
          {
            type: 'infoField',
            num: '016',
            label: '7. Was the subsidiary neither a CCPC or a DIC during its last taxation year?',
            labelClass: 'tabbed',
            inputType: 'radio',
            canClear: true
          },
          {
            label: 'If the answer to question 7 is yes, complete Part 4.',
            labelClass: 'fullLength tabbed2 bold'
          },
          {
            type: 'infoField',
            num: '017',
            label: '8. Was the subsidiary a CCPC or a DIC during its last taxation year?',
            labelClass: 'tabbed',
            inputType: 'radio',
            canClear: true
          },
          {
            label: 'If the answer to question 8 is yes, complete Part 3.',
            labelClass: 'fullLength tabbed2 bold'
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Part 1 - General rate income pool (GRIP)',
        'spacing': 'R_tn2',
        'rows': [
          {
            'label': 'GRIP at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number'
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Taxable income for the year (DICs enter \'0\')*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number '
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Income for the credit union deduction *<br>' +
            '(amount E in Part 3 of Schedule 17)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '120',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number '
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Amount on line 400, 405, 410, or 427 of the T2 return, whichever is less *',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number '
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'For a CCPC, the lesser of aggregate investment income (line 440 of the T2 return) and taxable income *',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '140',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number '
                },
                'padding': {
                  'type': 'tn',
                  'data': '140'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> lines 120, 130, and 140)',
            'labelClass': 'text-right ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1105',
                  'filters': 'number '
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1142',
                  'filters': 'number '
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Income taxable at the general corporate rate (amount B<b> minus</b> amount C) (if negative enter \'0\')',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '150',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number '
                },
                'padding': {
                  'type': 'tn',
                  'data': '150'
                }
              },
              null
            ]
          },
          {
            'label': 'After-tax income',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '700'
          },
          {
            'label': 'Eligible dividends received in the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number '
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              },
              null
            ]
          },
          {
            'label': 'Dividends deductible under section 113 received in the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '210',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number '
                },
                'padding': {
                  'type': 'tn',
                  'data': '210'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 200 <b>plus</b> line 210)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1110'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1111'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Becoming a CCPC (amount W5 in Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '220',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number '
                },
                'padding': {
                  'type': 'tn',
                  'data': '220'
                }
              },
              null
            ]
          },
          {
            'label': 'Post-amalgamation (total of amounts E4 in Part 3 and amounts W5 in Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '230',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number'
                },
                'padding': {
                  'type': 'tn',
                  'data': '230'
                }
              },
              null
            ]
          },
          {
            'label': 'Post-wind-up (total of amounts E4 in Part 3 and amounts W5 in Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '240',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number'
                },
                'padding': {
                  'type': 'tn',
                  'data': '240'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> lines 220, 230, and 240)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '290',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number'
                },
                'padding': {
                  'type': 'tn',
                  'data': '290'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1291',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Subtotal (<b>add</b> amounts A, D, E, and F)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1117'
                }
              }
            ],
            'indicator': 'G'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Eligible dividends paid in the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '300',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number '
                },
                'padding': {
                  'type': 'tn',
                  'data': '300'
                }
              },
              null
            ]
          },
          {
            'label': 'Excessive eligible dividend designations made in the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '310',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number '
                },
                'padding': {
                  'type': 'tn',
                  'data': '310'
                }
              },
              null
            ]
          },
          {
            'label': '(If becoming a CCPC (subsection 89(4) applies), enter "0" on lines 300 and 310.)',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal (line 300 <b>minus</b> line 310)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1355'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1312'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'H'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'GRIP before adjustment for specified future tax consequences (amount G <b>minus</b> amount H) ' +
            '(amount can be negative)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '490',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number'
                },
                'padding': {
                  'type': 'tn',
                  'data': '490'
                }
              }
            ]
          },
          {
            'label': 'Total GRIP adjustment for specified future tax consequences to previous tax years (amount N3 in Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '560',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number '
                },
                'padding': {
                  'type': 'tn',
                  'data': '560'
                }
              }
            ]
          },
          {
            'label': '<b>GRIP at the end of the tax year</b> (line 490 <b>minus</b> line 560)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '590',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'number'
                },
                'padding': {
                  'type': 'tn',
                  'data': '590'
                }
              }
            ]
          },
          {
            'label': 'Enter this amount on line 160 of Schedule 55.'
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'asterisk',
                'items': [
                  {
                    'label': 'For lines 110, 120, 130, and 140, the income amount is the amount before considering ' +
                    'specified future tax consequences. This phrase is defined in subsection 248(1). It includes the ' +
                    'deduction of a loss carryback from subsequent tax years, a reduction of Canadian exploration ' +
                    'expenses and Canadian development expenses that were renounced in subsequent tax years' +
                    ' (e.g., flow-through share renunciations), reversals of income inclusions where an option is' +
                    ' exercised in subsequent tax years, and the effect of certain foreign tax credit adjustments.',
                    'labelClass': 'fulLLength'
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 2 - GRIP adjustment for specified future tax consequences to previous tax years',
        'rows': [
          {
            'label': 'Complete this part if the corporation\'s taxable income of any of the previous three tax years' +
            ' took into account the specified future tax consequences defined  in subsection 248(1) from the current' +
            ' tax year. Otherwise, enter "0" on line 560 on page 1.',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'First previous tax year',
            'labelClass': 'bold'
          },
          {
            'label': 'Taxable income before specified future tax consequences from the current tax year',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1201'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'A1'
                }
              },
              null
            ]
          },
          {
            'label': 'Enter the following amounts before specified future tax consequences from the current tax year:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Income for the credit union deduction (amount E in Part 3 of Schedule 17)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1202'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B1'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Amount on line 400, 405, 410, or 427 of the T2 return, whichever is less',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1203'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C1'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Aggregate investment income (line 440 of the T2 return)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1204'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D1'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> amounts B1, C1, and D1)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1205'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1206'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E1'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (amount A1 <b>minus</b> amount E1) (if negative, enter \"0\")',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1207'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1208'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F1'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '750'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable income after specified future tax consequences',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1209'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G1'
                }
              },
              null
            ]
          },
          {
            'label': 'Enter the following amounts after specified future tax consequences:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Income for the credit union deduction (amount E in Part 3 of Schedule 17)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1210'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'H1'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Amount on line 400, 405, 410, or 427 of the T2 return, whichever is less',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1211'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'I1'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Aggregate investment income (line 440 of the T2 return)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1212'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'J1'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> amounts H1, I1, and J1)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1213'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1214'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'K1'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (amount G1 <b>minus</b> amount K1) (if negative, enter \'0\')',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1215'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1216'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'L1'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount F1<b> minus</b> amount L1) (if negative, enter \"0\")',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1217'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'M1'
                }
              }
            ]
          },
          {labelClass: 'fullLength'},
          {
            'label': 'GRIP adjustment for specified future tax consequences to the first ' +
            'previous tax year',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '800'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Second previous tax year',
            'labelClass': 'bold'
          },
          {
            'label': 'Taxable income before specified future tax consequences from the current tax year',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2219'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'A2'
                }
              },
              null
            ]
          },
          {
            'label': 'Enter the following amounts before specified future tax consequences from the current tax year:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Income for the credit union deduction (amount E in Part 3 of Schedule 17)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2220'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B2'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Amount on line 400, 405, 410, or 427 of the T2 return, whichever is less',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2221'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C2'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Aggregate investment income (line 440 of the T2 return)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2222'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D2'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> amounts B2, C2, and D2)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '2223'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '2224'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E2'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (amount A2 <b>minus</b> amount E2) (if negative, enter \"0\")',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '2225'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '2226'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F2'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '850'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable income after specified future tax consequences',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2227'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G2'
                }
              },
              null
            ]
          },
          {
            'label': 'Enter the following amounts after specified future tax consequences:',
            'labelClass': 'fullLength',
            'labelWidth': '40%'
          },
          {
            'label': 'Income for the credit union deduction (amount E in Part 3 of Schedule 17)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2228'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'H2'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Amount on line 400, 405, 410, or 427 of the T2 return, whichever is less',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2229'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'I2'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Aggregate investment income (line 440 of the T2 return)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2230'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'J2'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> amounts H2, I2, and J2)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '2231'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '2232'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'K2'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (amount G2<b> minus</b> amount K2) (if negative, enter \"0\")',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '2233'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '2234'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'L2'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount F2<b> minus</b> amount L2) (if negative, enter \"0\")',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '2235'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'M2'
                }
              }
            ]
          },
          {labelClass: 'fullLength'},
          {
            'label': 'GRIP adjustment for specified future tax consequences to the second previous tax year',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '900'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Third previous tax year',
            'labelClass': 'bold fullLength',
            'forceBreakAfter': true
          },
          {
            'label': 'Taxable income before specified future tax consequences from the current tax year',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '3237'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'A3'
                }
              },
              null
            ]
          },
          {
            'label': 'Enter the following amounts before specified future tax consequences from the current tax year:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Income for the credit union deduction (amount E in Part 3 of Schedule 17)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '3238'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B3'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Amount on line 400, 405, 410, or 427 of the T2 return, whichever is less',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '3239'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C3'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Aggregate investment income (line 440 of the T2 return)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '3240'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D3'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> amounts B3, C3, and D3)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '3241'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '3242'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E3'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (amount A3 <b>minus</b> amount E3) (if negative, enter \"0\")',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '3243'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '3244'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F3'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '950'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable income after specified future tax consequences',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '3245'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G3'
                }
              },
              null
            ]
          },
          {
            'label': 'Enter the following amounts after specified future tax consequences:',
            'labelClass': 'fullLength',
            'labelWidth': '40%'
          },
          {
            'label': 'Income for the credit union deduction (amount E in Part 3 of Schedule 17)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '3246'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'H3'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Amount on line 400, 405, 410, or 427 of the T2 return, whichever is less',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '3247'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'I3'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Aggregate investment income (line 440 of the T2 return)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '3248'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'J3'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> amounts H3, I3, and J3)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '3249'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '3250'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'K3'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (amount G3 <b>minus</b> amount K3) (if negative, enter \"0\")',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '3251'
                }
              },
              {
                'input': {
                  'num': '3252'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'L3'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount F3<b> minus</b> amount L3) (if negative, enter \"0\")',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '3253'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'M3'
                }
              }
            ]
          },
          {labelClass: 'fullLength'},
          {
            'label': 'GRIP adjustment for specified future tax consequences to the third previous tax year',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {labelClass: 'fullLength'},
          {
            'label': '<b>Total GRIP adjustment for specified future tax consequences to previous tax years: </b>' +
            '(<b>add </b>lines 500, 520, and 540) (if negative, enter \"0\")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'double',
                'input': {
                  'num': '3255'
                }
              }
            ],
            'indicator': 'N3'
          },
          {
            'label': 'Enter amount N3 on line 560 in part 1.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Part 3 - Worksheet to calculate the GRIP addition post-amalgamation or post-wind-up (predecessor or subsidiary was a CCPC or a DIC in its last tax year)',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Choose an applicable option for further calculation:</b>',
            'num': '1100',
            'type': 'infoField',
            'inputType': 'dropdown',
            'init': ' ',
            'options': [
              {
                'option': '0. N/A',
                'value': '0'
              },
              {
                'option': '1. Post amalgamation',
                'value': '1'
              },
              {
                'option': '2. Post wind-up',
                'value': '2'
              }
            ]
          },
          {
            label: 'Select 1 for <i>Post Amalgamation</i>, 2 for <i>Post Wind-up</i> or 0 for <i>N/A</i>',
            labelClass: 'tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Complete this part when there has been an amalgamation (within the meaning assigned by subsection ' +
            '87(1)) or a wind-up (to which subsection 88(1) applies) and the predecessor or subsidiary corporation was' +
            ' a CCPC or a DIC in its last tax year. In the calculation below, <b>corporation</b> means a predecessor ' +
            'or a subsidiary. The last tax year for a predecessor corporation was its tax year that ended immediately ' +
            'before the amalgamation and for a subsidiary corporation was its tax year during which its assets were ' +
            'distributed to the parent on the wind-up.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For a post-wind-up, include the GRIP addition in calculating the parent\'s GRIP at the end of its' +
            ' tax year that immediately follows the tax year during which it receives the assets of the subsidiary.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Complete a separate worksheet for <b>each</b> predecessor and <b>each</b> subsidiary that was a' +
            ' CCPC or a DIC in its last tax year. Keep a copy of this calculation for your records, ' +
            'in case we ask to see it later.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s GRIP at the end of its last tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4300'
                }
              }
            ],
            'indicator': 'A4'
          },
          {
            'label': 'Eligible dividends paid by the corporation in its last tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4310'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B4'
                }
              }
            ]
          },
          {
            'label': 'Excessive eligible dividend designations made by the corporation in its last tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4320'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C4'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount B4<b> minus</b> amount C4)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '4330'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '4340'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D4'
          },
          {
            'label': '<b>GRIP addition post-amalgamation or post-wind-up</b>' +
            ' (predecessor or subsidiary was a CCPC or a DIC in its last tax year)',
            'labelClass': 'fullLength'
          },
          {
            'label': ' (amount A4<b> minus</b> amount D4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4350'
                }
              }
            ],
            'indicator': 'E4'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'After you complete this calculation for each predecessor and each subsidiary, calculate the total ' +
            'of all the E4 amounts. Enter this total amount on:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- line 230 on page 1 for post-amalgamation; or',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '- line 240 on page 1 for post-wind-up.',
            'labelClass': 'tabbed fullLength'
          }
        ]
      },
      {
        'header': 'Part 4 - Worksheet to calculate the GRIP addition post-amalgamation, post-wind-up (predecessor or subsidiary was not a CCPC or a DIC in its last tax year), or the corporation is becoming a CCPC',
        // 'showWhen': {
        //   'or': [
        //     {
        //       'fieldId': '011'
        //     },
        //     {
        //       'fieldId': '013'
        //     },
        //     {
        //       'fieldId': '016'
        //     }
        //   ]
        // },
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Choose an applicable option for further calculation:</b>',
            'num': '1101',
            'type': 'infoField',
            'init': ' ',
            'inputType': 'dropdown',
            'options': [
              {
                'option': '0. N/A',
                'value': '0'
              },
              {
                'option': '1. Post amalgamation',
                'value': '1'
              },
              {
                'option': '2. Post wind-up',
                'value': '2'
              },
              {
                'option': '3. Corporation becoming CCPC',
                'value': '3'
              }
            ]
          },
          {
            label: 'Select 1 for <i>Post Amalgamation</i>, 2 for <i>Post Wind-up</i>, 3 for <i>Corporation becoming CCPC</i>, or 0 for <i>N/A</i>',
            labelClass: 'tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Complete this part when there has been an amalgamation (within the meaning assigned by subsection' +
            ' 87(1)) or a wind-up (to which subsection 88(1) applies) and the predecessor or subsidiary was not a ' +
            'CCPC or a DIC in its last tax year. Also, use this part for a corporation becoming a CCPC.' +
            ' In the calculation below, corporation means a <b>corporation</b> becoming a CCPC, a predecessor,' +
            ' or a subsidiary.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For a post-wind-up, include the GRIP addition in calculating the parent\'s GRIP at the end of' +
            ' its tax year that immediately follows the tax year during which it receives the assets of the subsidiary',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Complete a separate worksheet for <b>each</b> predecessor and <b>each</b> subsidiary that' +
            ' was not a CCPC or a DIC in its last tax year. Keep a copy of this calculation for your records,' +
            ' in case we ask to see it later.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Cost amount to the corporation of all property immediately before the end of its' +
            ' previous/last tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4401'
                }
              }
            ],
            'indicator': 'A5'
          },
          {
            'label': 'The corporation\'s money on hand immediately before the end of its previous/last tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4402'
                }
              }
            ],
            'indicator': 'B5'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Total of subsection 111(1) losses that would have been deductible in calculating ' +
            'the corporation\'s taxable income for the previous/last tax year if the corporation had had unlimited ' +
            'income from each business carried on and each property held and had realized an unlimited amount of' +
            ' capital gains for the previous/last tax year:',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Non-capital losses',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '4403'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C5'
                }
              },
              null
            ]
          },
          {
            'label': 'Net capital losses',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '4404'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D5'
                }
              },
              null
            ]
          },
          {
            'label': 'Farm losses',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '4405'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E5'
                }
              },
              null
            ]
          },
          {
            'label': 'Restricted farm losses',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '4406'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F5'
                }
              },
              null
            ]
          },
          {
            'label': 'Limited partnership losses',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '4407'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G5'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> amounts C5 to G5)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '4408'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '4409'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'H5'
                }
              }
            ]
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Total of all amounts deducted under subsection 111(1) in calculating the corporation\'s ' +
            'taxable income for the previous/last tax year:',
            'labelClass': 'fullLength'
          },
          {'labelClass': 'fullLength'},
          {
            'label': 'Non-capital losses',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '4410'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'I5'
                }
              },
              null
            ]
          },
          {
            'label': 'Net capital losses',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '4411'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'J5'
                }
              },
              null
            ]
          },
          {
            'label': 'Farm losses',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '4412'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'K5'
                }
              },
              null
            ]
          },
          {
            'label': 'Restricted farm losses',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '4413'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'L5'
                }
              },
              null
            ]
          },
          {
            'label': 'Limited partnership losses',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '4414'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'M5'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal(<b>add</b> amounts I5 to M5)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '4415'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '4416'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'N5'
                }
              }
            ]
          },
          {
            'label': 'Unused and unexpired losses at the end of the corporation\'s previous/last tax year <br> ' +
            '(amount H5 <b>minus</b> amount N5)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '4417'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '4418'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'O5'
          },
          {
            'label': 'Subtotal (<b>add </b>amounts A5, B5, and O5)',
            'layout': 'alignInput',
            labelCellClass: 'alignRight',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4419'
                }
              }
            ],
            'indicator': 'P5'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'All the corporation\'s debts and other obligations to pay that were outstanding immediately' +
            ' before the end of its previous/last tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4420'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'Q5'
                }
              }
            ]
          },
          {
            'label': 'Paid-up capital of all the corporation\'s issued and outstanding shares of capital stock ' +
            'immediately before the end of its previous/last tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4421'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'R5'
                }
              }
            ]
          },
          {
            'label': 'All the corporation\'s reserves deducted in its previous/last tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4422'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'S5'
                }
              }
            ]
          },
          {
            'label': 'The corporation\'s capital dividend account immediately before the end of its previous/last tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4423'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'T5'
                }
              }
            ]
          },
          {
            'label': 'The corporation\'s low rate income pool immediately before the end of its previous/last tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4424'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'U5'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> amounts Q5 to U5))',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '4425'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '4426'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'V5'
          },
          {
            'label': '<b>GRIP addition post-amalgamation or post-wind-up (predecessor or subsidiary was not a CCPC or ' +
            'a DIC in its last tax year), or the corporation is becoming a CCPC</b>' +
            ' (amount P5<b> minus</b> amount V5) (if negative, enter \"0\")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4427'
                }
              }
            ],
            'indicator': 'W5'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'After you complete this worksheet for each predecessor and each subsidiary, ' +
            'calculate the total of all the W5 amounts. Enter this total amount on:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- line 220 on page 1 for a corporation becoming a CCPC;',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '- line 230 on page 1 for post-amalgamation; or',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '- line 240 on page 1 for post-wind-up.',
            'labelClass': 'tabbed fullLength'
          }
        ]
      }
      //{
      //header: 'Part 5 - General rate factor for the tax year',
      //  rows: [
      //    {
      //      label: 'Complete this part to calculate the general rate factor for the tax year. ' +
      //      'Calculate your results to four decimal places',
      //      labelClass: 'fullLength'
      //    },
      //    {
      //      type: 'table', num:'540'
      //    },
      //    {
      //      type: 'table', num: '550'
      //    },
      //    {
      //      type: 'table', num: '560'
      //    },
      //    {
      //      type: 'table', num: '530'
      //    },
      //    {
      //       labelClass: 'fullLength'
      //    },
      //    {
      //       labelClass: 'fullLength'
      //    },
      //    {
      //       labelClass: 'fullLength'
      //    },
      //    {
      //      label: '<b>General rate factor for the tax year</b> (total of amounts QQ to TT)',
      //      input1: true,
      //      input1Style: 'doubleUnderline',
      //      num: '580',
      //      indicator: 'UU'
      //    }
      //  ]
      //}
    ]
  };
})();
