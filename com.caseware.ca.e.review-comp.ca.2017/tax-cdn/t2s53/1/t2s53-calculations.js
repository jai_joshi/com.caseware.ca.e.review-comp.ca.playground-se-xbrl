(function() {
  function enableNums(field, numArray) {
    for (var i = 0; i < numArray.length; i++) {
      field(numArray[i]).disabled(false);
    }
  }

  wpw.tax.create.calcBlocks('t2s53', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      var part1Num = [100, 110, 120, 130, 140, 1105, 1142, 150, 190, 200, 210, 1110, 1111, 220, 230, 240, 290, 1291,
        1117, 1355, 1312, 490, 560, 590];
      var part3Num = [1100, 4300, 4310, 4320, 4330, 4340, 4350];
      var part4Num = ['1101', '4401', '4402', '4403', '4404', '4405', '4406', '4407', '4408', '4409', '4410',
        '4411', '4412', '4413', '4414', '4415', '4416', '4417', '4418', '4419', '4420', '4421', '4422',
        '4423', '4424', '4425', '4426', '4427'];

      var isAmalgamation = (field('CP.071').get() == 1);
      var isCCPC = (field('CP.Corp_Type').get() == 1);
      var isDIC = field('CP.189').get() == 1;
      var shouldCompletePart3 = false;
      var shouldCompletePart4 = false;

      ////Eligibility Question
      //1. Was the corporation a CCPC during its preceding taxation year?
      // if (isCCPC) {
      //   field('010').assign(1);
      //   field('010').source(field('CP.120'));
      // }
      // else {
      //   field('010').assign(2);
      //   field('010').source(field('CP.120'));
      // } comment out as there's no way to check type of business for previous year

      //2. Corporations that become a CCPC or a DIC ( we dont have the option to check it yet so comment out for now-Bucky)
      /*if (field('CP.119').get() == '1' && field('010').get() == '1') {
       field('011').assign(1);
       }
       else {
       field('011').assign(2);
       }*/

      //3. Corporations that were formed as a result of an amalgamation?
      if (isAmalgamation) {
        field('012').assign(1);
        field('012').source(field('CP.071'));
        //get answer for question 4 and 5
        shouldCompletePart4 = shouldCompletePart4 || (field('013').get() == 1);
        shouldCompletePart3 = shouldCompletePart3 || (field('014').get() == 1);
      }
      else {
        field('012').assign(2);
        field('012').source(field('CP.071'));

      }
      //answer question 6. Has the corporation wound-up a subsidiary in the preceding taxation year?
      if (field('CP.072').get() == '1') {
        field('015').assign(1);
        field('015').source(field('CP.072'));
        //answer questions 7 and 8
        //If the answer to question 7 is yes, complete Part 4
        shouldCompletePart4 = shouldCompletePart4 || (field('016').get() == 1);
        //If the answer to question 8 is yes, complete Part 3.
        shouldCompletePart3 = shouldCompletePart3 || (field('017').get() == 1);
      }
      else {
        field('015').assign(2);
        field('015').source(field('CP.072'));
      }

      //Part 1
      if (isCCPC || isDIC) {
        var line110T2J = field('T2J.360').get();
        //calcUtils.getGlobalValue('120', 'T2S17', '');//TODO:s17 not available
        var line400T2J = field('T2J.400').get();
        var line405T2J = field('T2J.405').get();
        var line410T2J = field('T2J.410').get();
        var line425T2J = field('T2J.425').get();
        var line427T2J = field('T2J.427').get();

        field('100').disabled(false);
        if (isDIC) {
          field('110').assign(0)
        }
        else {
          field('110').assign(line110T2J)
        }
        calcUtils.field('130').assign(Math.min(line400T2J, line405T2J, line410T2J, line427T2J));
        var line440T2J = field('T2J.440').get();
        var line371T2J = field('T2J.371').get();
        calcUtils.field('140').assign(Math.min(line440T2J, line371T2J));
        calcUtils.sumBucketValues('1105', ['120', '130', '140'], true);
        calcUtils.equals('1142', '1105');
        calcUtils.subtract('150', '110', '1142');
        calcUtils.getGlobalValue('191', 'ratesFed', '456');
        calcUtils.multiply('190', ['191', '150']);
        calcUtils.getGlobalValue('200', 'T2S3', '1241');
        //for 210
        var sch3Table542 = form('T2S3').field('542');
        var data = [];
        sch3Table542.getRows().forEach(function(row, rowIndex) {
          var dividendEligibility = row[3].get();
          var dividendAmount = sch3Table542.cell(rowIndex, 1).get();
          var eligibleDividend = sch3Table542.cell(rowIndex, 2).get();
          data.push({'type': dividendEligibility, 'amount': dividendAmount, 'eligibility': eligibleDividend});
          var totalAmount = calcUtils.sumByKey(data, 'type', '2', 'amount');
          var eligibleAmount = calcUtils.sumByKey(data, 'type', '2', 'eligibility');

          field('210').assign(totalAmount - eligibleAmount);
          field('210').source(field('T2S3.240'));
        });
        //if (line040CP == 1) {
        //  calcUtils.subtract('210', fS3, f1S3);
        //}
        //else {
        //  calcUtils.removeValue(['210']);
        //  delete bucket.disabledNums['210'];
        //}
        calcUtils.sumBucketValues('1110', ['200', '210'], true);
        calcUtils.equals('1111', '1110');

        var becomingCCPC = field('1101').get() == 3;
        var part3PostAmalgamation = field('1100').get() == 1;
        var part4PostAmalgamation = field('1101').get() == 1;
        var part3PostWindUp = field('1100').get() == 2;
        var part4PostWindUp = field('1101').get() == 2;
        if (becomingCCPC) {
          field('220').assign(field('4427').get());
        }
        else {
          field('220').assign(0)
        }
        //post-amalgamation calc
        if (part3PostAmalgamation && part4PostAmalgamation) {
          calcUtils.sumBucketValues('230', ['4350', '4427'], true);
        }
        else {
          if (part3PostAmalgamation) {
            field('230').assign(field('4350').get())
          }
          else if (part4PostAmalgamation) {
            field('230').assign(field('4427').get())
          }
          else {
            field('230').assign(0)
          }
        }
        //post windup calc
        if (part3PostWindUp && part4PostWindUp) {
          calcUtils.sumBucketValues('240', ['4350', '4427'], true);
        }
        else {
          if (part3PostWindUp) {
            field('240').assign(field('4350').get())
          }
          else if (part4PostWindUp) {
            field('240').assign(field('4427').get())
          }
          else {
            field('240').assign(0)
          }
        }

        calcUtils.sumBucketValues('290', ['220', '230', '240'], true);
        calcUtils.equals('1291', '290');
        calcUtils.sumBucketValues('1117', ['100', '190', '1111', '1291']);
        field('1355').assign(field('300').get() - field('310').get());
        calcUtils.equals('1312', '1355');
        calcUtils.subtract('490', '1117', '1312');
        calcUtils.equals('560', '3255');
        calcUtils.subtract('590', '490', '560');
      }
      else {
        calcUtils.removeValue(part1Num, true)
      }

      ////Part2(first previous tax year)
      calcUtils.sumBucketValues('1205', ['1202', '1203', '1204']);
      calcUtils.equals('1206', '1205');
      calcUtils.subtract('1207', '1201', '1206');
      calcUtils.equals('1208', '1207');
      // calcUtils.equals('1211', '1203'); //should be left as an input field
      calcUtils.equals('1212', '1204');
      calcUtils.sumBucketValues('1213', ['1210', '1211', '1212']);
      calcUtils.equals('1214', '1213');
      calcUtils.subtract('1215', '1209', '1214');
      calcUtils.equals('1216', '1215');
      calcUtils.subtract('1217', '1208', '1216');
      calcUtils.getGlobalValue('501', 'ratesFed', '456');
      calcUtils.multiply('500', ['1217', '501']);

      field('750').cell(0, 0).assign(field('T2S4.901').get());
      field('750').cell(0, 0).source(field('T2S4.901'));
      field('750').cell(0, 1).assign(field('T2S4.951').get());
      field('750').cell(0, 1).source(field('T2S4.951'));
      field('750').cell(0, 2).assign(field('T2S4.941').get());
      field('750').cell(0, 2).source(field('T2S4.941'));
      field('750').cell(0, 3).assign(field('T2S4.921').get());
      field('750').cell(0, 3).source(field('T2S4.921'));
      field('750').cell(0, 5).assign(field('750').cell(0, 0).get() +
          field('750').cell(0, 1).get() +
          field('750').cell(0, 2).get() +
          field('750').cell(0, 3).get() +
          field('750').cell(0, 4).get());
      field('1209').assign(Math.max(field('1208').get() - field('750').cell(0, 5).get(), 0));

      ////Part2(second previous tax year)
      calcUtils.sumBucketValues('2223', ['2220', '2221', '2222']);
      calcUtils.equals('2224', '2223');
      calcUtils.subtract('2225', '2219', '2224');
      calcUtils.equals('2226', '2225');
      calcUtils.sumBucketValues('2231', ['2228', '2229', '2230']);
      calcUtils.equals('2232', '2231');
      calcUtils.subtract('2233', '2227', '2232');
      calcUtils.equals('2234', '2233');
      calcUtils.subtract('2235', '2226', '2234');
      calcUtils.getGlobalValue('521', 'ratesFed', '456');
      calcUtils.multiply('520', ['2235', '521'], '1');

      field('850').cell(0, 0).assign(field('T2S4.902').get());
      field('850').cell(0, 0).source(field('T2S4.902'));
      field('850').cell(0, 1).assign(field('T2S4.952').get());
      field('850').cell(0, 1).source(field('T2S4.952'));
      field('850').cell(0, 2).assign(field('T2S4.942').get());
      field('850').cell(0, 2).source(field('T2S4.942'));
      field('850').cell(0, 3).assign(field('T2S4.922').get());
      field('850').cell(0, 3).source(field('T2S4.922'));
      field('850').cell(0, 5).assign(field('850').cell(0, 0).get() +
          field('850').cell(0, 1).get() +
          field('850').cell(0, 2).get() +
          field('850').cell(0, 3).get() +
          field('850').cell(0, 4).get());
      field('2227').assign(Math.max(field('2226').get() - field('850').cell(0, 5).get(), 0));

      //Part2(third previous year)
      calcUtils.sumBucketValues('3241', ['3238', '3239', '3240']);
      calcUtils.equals('3242', '3241');
      calcUtils.subtract('3243', '3237', '3242');
      calcUtils.equals('3244', '3243');
      calcUtils.sumBucketValues('3249', ['3246', '3247', '3248']);
      calcUtils.equals('3250', '3249');
      calcUtils.subtract('3251', '3245', '3250');
      calcUtils.equals('3252', '3251');
      calcUtils.subtract('3253', '3244', '3252');
      calcUtils.getGlobalValue('541', 'ratesFed', '456');
      calcUtils.multiply('540', ['3253', '541']);
      calcUtils.sumBucketValues('3255', ['500', '520', '540']);

      field('950').cell(0, 0).assign(field('T2S4.903').get());
      field('950').cell(0, 0).source(field('T2S4.903'));
      field('950').cell(0, 1).assign(field('T2S4.953').get());
      field('950').cell(0, 1).source(field('T2S4.953'));
      field('950').cell(0, 2).assign(field('T2S4.943').get());
      field('950').cell(0, 2).source(field('T2S4.943'));
      field('950').cell(0, 3).assign(field('T2S4.923').get());
      field('950').cell(0, 3).source(field('T2S4.923'));
      field('950').cell(0, 5).assign(field('950').cell(0, 0).get() +
          field('950').cell(0, 1).get() +
          field('950').cell(0, 2).get() +
          field('950').cell(0, 3).get() +
          field('950').cell(0, 4).get());
      field('3245').assign(Math.max(field('3244').get() - field('950').cell(0, 5).get(), 0));

      //Part 3
      if (shouldCompletePart3) {
        enableNums(field, part3Num);
        calcUtils.subtract('4330', '4310', '4320');
        calcUtils.equals('4340', '4330');
        calcUtils.subtract('4350', '4300', '4340');
      }
      else {
        calcUtils.removeValue(part3Num, true)
      }
      //part 4
      if (shouldCompletePart4) {
        enableNums(field, part4Num);
        calcUtils.sumBucketValues('4408', ['4403', '4404', '4405', '4406', '4407']);
        calcUtils.equals('4409', '4408');
        calcUtils.sumBucketValues('4415', ['4410', '4411', '4412', '4413', '4414']);
        calcUtils.equals('4416', '4415');
        calcUtils.subtract('4417', '4409', '4416');
        calcUtils.equals('4418', '4417');
        calcUtils.sumBucketValues('4419', ['4401', '4402', '4418']);
        //calcUtils.getGlobalValue('4423', 'T2S89', '');   //TODO: S89 is not available//
        calcUtils.sumBucketValues('4425', ['4420', '4421', '4422', '4423', '4424']);
        calcUtils.equals('4426', '4425');
        calcUtils.subtract('4427', '4419', '4426');
      }
      else {
        calcUtils.removeValue(part4Num, true)
      }
      calcUtils.sumBucketValues('290', ['220', '230', '240'], true);
      calcUtils.equals('1291', '290');
      calcUtils.sumBucketValues('1117', ['100', '190', '1111', '1291']);
      calcUtils.subtract('1355', '300', '310');
      calcUtils.equals('1312', '1355');
      calcUtils.subtract('490', '1117', '1312');
      calcUtils.equals('560', '3255');
      calcUtils.subtract('590', '490', '560');

      ////Part2(first previous tax year)
      calcUtils.sumBucketValues('1205', ['1202', '1203', '1204']);
      calcUtils.equals('1206', '1205');
      calcUtils.subtract('1207', '1201', '1206');
      calcUtils.equals('1208', '1207');
      calcUtils.equals('1211', '1203');
      calcUtils.equals('1212', '1204');
      calcUtils.sumBucketValues('1213', ['1210', '1211', '1212']);
      calcUtils.equals('1214', '1213');
      calcUtils.subtract('1215', '1209', '1214');
      calcUtils.equals('1216', '1215');
      calcUtils.subtract('1217', '1208', '1216');
      calcUtils.getGlobalValue('501', 'ratesFed', '456');
      calcUtils.multiply('500', ['1217', '501']);

      ////Part2(second previous tax year)
      calcUtils.sumBucketValues('2223', ['2220', '2221', '2222']);
      calcUtils.equals('2224', '2223');
      calcUtils.subtract('2225', '2219', '2224');
      calcUtils.equals('2226', '2225');
      calcUtils.sumBucketValues('2231', ['2228', '2229', '2230']);
      calcUtils.equals('2232', '2231');
      calcUtils.subtract('2233', '2227', '2232');
      calcUtils.equals('2234', '2233');
      calcUtils.subtract('2235', '2226', '2234');
      calcUtils.getGlobalValue('521', 'ratesFed', '456');
      calcUtils.multiply('520', ['2235', '521'], '1');

      //Part2(third previous year)
      calcUtils.sumBucketValues('3241', ['3238', '3239', '3240']);
      calcUtils.equals('3242', '3241');
      calcUtils.subtract('3243', '3237', '3242');
      calcUtils.equals('3244', '3243');
      calcUtils.sumBucketValues('3249', ['3246', '3247', '3248']);
      calcUtils.equals('3250', '3249');
      calcUtils.subtract('3251', '3245', '3250');
      calcUtils.equals('3252', '3251');
      calcUtils.subtract('3253', '3244', '3252');
      calcUtils.getGlobalValue('541', 'ratesFed', '456');
      calcUtils.multiply('540', ['3253', '541']);
      calcUtils.sumBucketValues('3255', ['500', '520', '540']);

      //Part 3
      if (shouldCompletePart3) {
        enableNums(field, part3Num);
        calcUtils.subtract('4330', '4310', '4320');
        calcUtils.equals('4340', '4330');
        calcUtils.subtract('4350', '4300', '4340');
      }
      else {
        calcUtils.removeValue(part3Num, true)
      }
      //part 4
      if (shouldCompletePart4) {
        enableNums(field, part4Num);
        calcUtils.sumBucketValues('4408', ['4403', '4404', '4405', '4406', '4407']);
        calcUtils.equals('4409', '4408');
        calcUtils.sumBucketValues('4415', ['4410', '4411', '4412', '4413', '4414']);
        calcUtils.equals('4416', '4415');
        calcUtils.subtract('4417', '4409', '4416');
        calcUtils.equals('4418', '4417');
        calcUtils.sumBucketValues('4419', ['4401', '4402', '4418']);
        //calcUtils.getGlobalValue('4423', 'T2S89', '');   //TODO: S89 is not available//
        calcUtils.sumBucketValues('4425', ['4420', '4421', '4422', '4423', '4424']);
        calcUtils.equals('4426', '4425');
        calcUtils.subtract('4427', '4419', '4426');
      }
      else {
        calcUtils.removeValue(part4Num, true)
      }
    });
  });
}());
