(function () {
  'use strict';

  wpw.tax.global.formData.t2a10 = {
    'formInfo': {
      'abbreviation': 'AT293',
      'title': 'Alberta Loss Carry-Back Application - Schedule 10',
      hideHeaderBox: true,
      schedule: 'AT293',
      printTitle: 'ALBERTA LOSS CARRY-BACK APPLICATION - AT1 SCHEDULE 10',
      //'headerImage': 'canada-federal',
      agencyUseOnlyBox: {
        tn: '080',
        headerClass: 'alignRight',
        textAbove: ' '
      },
      description: [
        {
          'text': 'For use by a corporation to request a loss carry-back to prior taxation years. This schedule may be completed and submitted with the corporation\'s AT1 or may be\n' +
          'completed and filed separately after the loss year. A Loss Carry-Back Application must be filed even if the corporation is exempt from filing its AT1. The application\n' +
          'of losses is at the corporation\'s discretion, therefore, the application of losses for federal purposes will not apply for Alberta purposes See page 2 for further\n' +
          'instructions. The Alberta Loss Carry-Back Application must be mailed or delivered to: TAX AND REVENUE ADMINISTRATION, 9811 109 ST, EDMONTON AB\n' +
          'T5K 2L5. FAX: 780-427-0348'
        }
      ],
      category: 'Alberta Tax Forms',
      formFooterNum: 'AT293 (Jul-12)'

    },
    sections: [
      {
        hideFieldset: true,
        'rows': [
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'type': 'table',
            'num': '1001'
          },
          {
            'type': 'infoField',
            'label': 'Are monetary amounts reported in functional currency other than Canadian? ',
            'labelClass': 'indent',
            'num': '090',
            'inputType': 'radio',
            'width': '30%',
            'tn': '090',
            'trailingDots': true,
            'init': '2',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false
            },
          },
          {
            label: ' Report all monetary amounts in dollars; DO NOT include cents'
          }
        ]
      },
      {
        'rows': [
          {
            'type': 'table',
            'num': '1002'
          },
          {
            'type': 'table', //select type of other losses
            'num': '1002b'
          },
          {
            'type': 'table',
            'num': '1003'
          },
        ]
      },
      {
        'header': 'APPLICANT IDENTIFICATION ',
        'rows': [
          {
            'type': 'table',
            'num': '1004'
          },
          {
            'type': 'infoField',
            'inputType': 'address',
            'add1Num': '058',
            'add2Num': '060',
            'provNum': '064',
            'cityNum': '062',
            'countryNum': '066',
            'postalCodeNum': '068',
            'add1Tn': '058',
            'add2Tn': '060',
            'cityTn': '062',
            'provTn': '064',
            'countryTn': '066',
            'postalTn': '068',
            'cannotOverride': true
          },
          {
            'label': 'CERTIFICATION',
            'labelClass': 'bold center'
          },
          {
            'type': 'table',
            'num': '1010'
          },
          {
            'label': 'am an authorized signing officer of the corporation. I hereby certify that the information given is true, correct and complete in every respect.',
            'labelClass': 'fullLength'
          },
          {},
          {
            'type': 'table',
            'num': '1020'
          },
        ]
      },
      {
        'header': 'Instructions',
        'rows': [
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'dash',
                'items': [
                  {
                    'label': ' The amount of loss claimed in a year for Alberta tax purposes may differ from the amount claimed for federal purposes. Limitations on ' +
                    'deductibility provided in the federal Act apply for Alberta purposes, except that an amount of non-capital loss or farm loss used to reduce the ' +
                    'federal Part IV tax base does not reduce the loss balance available for Alberta purposes.'
                  },
                  {
                    'label': ' Any late filing penalty for a year to which a loss is carried back will not be reduced by the loss carry-back. '
                  },
                  {
                    'label': 'For interest calculations, a loss carried back is given effect as of the latest of:',
                  },
                ]
              }
            ]
          },
          {
            type: 'table',
            num: '1100'
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'dash',
                'items': [
                  {
                    'label': 'Refunds arising from loss carry-back adjustments will be first applied to debts outstanding in the corporation\'s account.',
                  },
                ]
              }
            ]
          },
          {
            label: ''
          },
          {
            label: ''
          },
          {
            label: 'Inclusion Rate Instructions:',
            labelClass: 'bold'
          },
          {
            label: 'The net capital loss that can be applied to a given taxation year is the gross amount of capital loss multiplied by the inclusion rate for the\n' +
            'corresponding year of application'
          },
          {
            label: 'The inclusion rates are as follows: '
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'dash',
                'items': [
                  {
                    'label': '3/4 for dispositions of property prior to February 28, 2000 ',
                  },
                  {
                    'label': '2/3 for dispositions of property after February 27, 2000 and before October 18, 2000 ',
                  },
                  {
                    'label': '1/2 for dispositions of property after October 17, 2000',
                  },
                  {
                    'label': ' where dispositions in a taxation year occurred in more than one inclusion rate period, then an effective inclusion rate would have been ' +
                    'computed on the federal schedule 6 or on the supporting documentation submitted with the Alberta schedule 18 for that taxation year. ' +
                    'Refer to the applicable schedule to obtain this rate. Note that the inclusion rate must be entered on page one in a 6 decimal format. ' +
                    'For example, an inclusion rate of 2/3 is to be entered as .666667, the rate of 1/2 is to be entered as .500000, etc.',
                  }
                ]
              }
            ]
          },
        ]
      },
    ]
  };
})();
