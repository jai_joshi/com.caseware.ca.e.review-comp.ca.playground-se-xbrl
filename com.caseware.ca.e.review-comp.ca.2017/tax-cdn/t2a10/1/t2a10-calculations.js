(function () {

  wpw.tax.create.calcBlocks('t2a10', function (calcUtils) {

    calcUtils.calc(function (calcUtils, field) {
      calcUtils.getGlobalValue('001', 'cp', '002');
      calcUtils.getGlobalValue('082', 'cp', '135');
      calcUtils.getGlobalValue('084', 'cp', 'idtax_end');
      if (!field('cp.079').get()) {
        field('090').assign(2);
      } else {
        field('090').assign(1);
      }
      var OtherLoss = 0;
      if (wpw.tax.utilities.flagger().T2A21 == false) {
        calcUtils.getGlobalValue('002', 'T2S4', '100');
        calcUtils.getGlobalValue('012', 'T2S4', '310');
        calcUtils.getGlobalValue('042', 'T2S4', '210');
        if (field('025').get()) {
          OtherLoss += field('T2S4.510');
          field('032').source(field('T2S4.510'));
        }
        if (field('023').get()) {
          OtherLoss += field('T2S4.410');
          field('032').source(field('T2S4.410'));
        }
      } else {
        calcUtils.getGlobalValue('002', 'T2A21', '037');
        calcUtils.getGlobalValue('012', 'T2A21', '077');
        calcUtils.getGlobalValue('012', 'T2A21', '057');
        if (field('025').get()) {
          OtherLoss += field('T2A21.117');
          field('032').source(field('T2A21.117'));
        }
        if (field('023').get()) {
          OtherLoss += field('T2A21.097');
          field('032').source(field('T2A21.097'));
        }
      }
      field('032').assign(OtherLoss);
      field('023').assign(false);
      field('025').assign(false);
      if (field('t2j.233').get()) {//todo need to add T2A21 field 097 option if true
        field('023').assign(true);
      }
      if (field('t2a21.117') != 0 || field('t2s4.510' != 0).get()) {
        field('025').assign(true);
      }
      var taxationYearTable = field('tyh.200');
      var NCL = 0;//non-capital loss
      var FL = 0;//farm loss
      var OL = 0; //other loss
      var CapL = 0;//Capital loss
      field('1003').getRows().forEach(function (row, rowIndex) { // fill in table data
        var dateCol = Math.abs(rowIndex - 19);
        row[0].assign(taxationYearTable.cell(dateCol, 6).get());
        if (rowIndex < 3) {
          NCL += row[1].value();
          FL += row[2].value();
          OL += row[3].value();
          row[4].assign(.5);
          CapL += row[5].value();
          row[6].assign(row[4].value() * row[5].value());
        }
        if (rowIndex == 3) {
          row[1].assign(NCL);
          row[2].assign(FL);
          row[3].assign(OL);
          row[5].assign(CapL);
        }
      });
      field('010').assign(field('002').get() - field('800').get());
      field('020').assign(field('012').get() - field('801').get());
      field('040').assign(field('032').get() - field('802').get());
      field('050').assign(field('042').get() - field('803').get());
      //APPLICANT Indentification
      calcUtils.getGlobalValue('052', 'T2A', '025')
      calcUtils.getGlobalValue('054', 'T2A', '026');
      calcUtils.getGlobalValue('056', 'T2A', '027');
      calcUtils.getGlobalValue('058', 'cp', '011');
      calcUtils.getGlobalValue('060', 'cp', '012');
      calcUtils.getGlobalValue('062', 'cp', '015');
      calcUtils.getGlobalValue('064', 'cp', '016');
      calcUtils.getGlobalValue('066', 'cp', '017');
      calcUtils.getGlobalValue('068', 'cp', '018');
      calcUtils.getGlobalValue('070', 'cp', '950');
      calcUtils.getGlobalValue('072', 'cp', '951');
      calcUtils.getGlobalValue('074', 'cp', '954');
      calcUtils.getGlobalValue('955', 'cp', '955');
    });


  });
})
();