(function () {

  wpw.tax.global.tableCalculations.t2a10 = {
    '1000': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          "type": "none"
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          "type": "text",
          cellClass: 'alignLeft',
        },
        {
          colClass: 'std-input-col-padding-width',
          type: 'none'
        }
      ],
      "cells": [
        {
          "0": {
            label: 'Corporation Name:'
          },
          "2": {
            "num": "001",
            'cannotOverride': true
          },
        }
      ]
    },
    "1001": {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-col-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
        },
        {
          cellClass: 'alignLeft',
          colClass: 'std-input-col-width',
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          colClass: 'std-input-col-width',
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          cellClass: 'alignLeft',
          colClass: 'std-input-col-width',
        },
      ],
      cells: [
        {
          '0': {
            label: 'Alberta Corporate Account Number:<br>' +
            '(9 or 10 digit account number'
          },
          '1': {
            tn: '082',
          },
          '2': {
            num: '082',
            type: 'text',
            'disabled': true,
            'cannotOverride': true
          },
          '4': {
            label: 'Current Taxation Year Ending'
          },
          '5': {
            tn: '084',
          },
          '6': {
            num: '084',
            inputType: 'date',
            'disabled': true,
            'cannotOverride': true
          }
        }
      ],
    },
    "1002": {
      fixedRows: true,
      columns: [
        {
          colClass: 'std-input-col-padding-width',
          header: 'Application of current year losses'
        },
        {
          colClass: 'std-input-width',
          header: 'Non-capital Loss'
        },
        {
          colClass: 'std-input-width',
          header: 'Farm Loss'
        },
        {
          colClass: 'std-input-width',
          header: 'Other Losses'
        },
        {
          width: '95px',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          header: 'Capital Loss - Gross Amount Available'
        },
        {
          colClass: 'std-input-width',
          type: 'none'
        },
      ],
      cells: [
        {
          '0': {
            type: 'none',
            label: 'Amount of current year loss <br>available for carry-back: &nbsp&nbsp&nbsp&nbsp&nbsp <font size="4"><b>A</b></font>'
          },
          '1': {
            tn: '002',
            num: '002',
            'disabled': true,
            'cannotOverride': true
          },
          '2': {
            tn: '012',
            num: '012',
            'disabled': true,
            'cannotOverride': true
          },
          '3': {
            tn: '032',
            num: '032',
            'disabled': true,
            'cannotOverride': true
          },
          '5': {
            tn: '042',
            num: '042',
            'disabled': true,
            'cannotOverride': true
          },
        }
      ],
    },
    "1002b": {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
        },
        {
          width: '18px',
          type: 'singleCheckbox'
        },
        {
          colClass: 'std-input-width',
          type: 'none',
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
        },
        {
          'width': '18px',
          "type": "singleCheckbox"
        },
        {
          colClass: 'std-input-width',
          type: 'none',
        },
      ],
      cells: [
        {
          '0': {
            label: 'Type of Other Losses (check box(es)):'
          },
          '1': {
            tn: '023',
          },
          '2': {
            num: '023',
          },
          '3': {
            label: 'Restricted Farm'
          },
          '4': {
            tn: '025',
          },
          '5': {
            num: '025',
          },
          '6': {
            label: 'Listed Personal Property'
          },
        }
      ],
    },
    "1003": {
      fixedRows: true,
      superHeaders: [
        {
          "header": 'Deduct loss to be applied under the Alberta Corporate Tax Act to:',
          "colspan": "4"
        },
        {
          "header": "Capital Loss",
          "colspan": "3"
        },
      ],
      columns: [
        {
          colClass: 'std-input-col-padding-width',
          header: 'Application of current year losses'
        },
        {
          colClass: 'std-input-width',
          header: 'Non-capital Loss'
        },
        {
          colClass: 'std-input-width',
          header: 'Farm Loss'
        },
        {
          colClass: 'std-input-width',
          header: 'Other Losses'
        },
        {
          width: '95px',
          header: '<b>Inclusion Rate</b>',
          decimals: 6,
        },
        {
          colClass: 'std-input-width',
          header: 'Gross Amount'
        },
        {
          colClass: 'std-input-width',
          header: 'Amount of Loss Applied <font size="1">(Inclusion Rate X Capital Loss)</font>'
        },
      ],
      cells: [
        {
          '0': {
            label: '1st preceding taxation year ending:',
            labelClass: 'font-small',
            tn: '003',
            inputType: 'date',
            'disabled': true,
            'cannotOverride': true
          },
          '1': {
            tn: '004',
            num: '004',

          },
          '2': {
            tn: '014',
            num: '014',

          },
          '3': {
            tn: '034',
            num: '034',
          },
          '4': {
            tn: '043',
            num: '043',
          },
          '5': {
            tn: '044',
            num: '044',
          },
          '6': {
            num: '300',
          },
        },
        {
          '0': {
            label: '2nd preceding taxation year ending:',
            labelClass: 'font-small',
            inputType: 'date',
            tn: '005',
            'disabled': true,
            'cannotOverride': true
          },
          '1': {
            tn: '006',
            num: '006',

          },
          '2': {
            tn: '016',
            num: '016',

          },
          '3': {
            tn: '036',
            num: '036',
          },
          '4': {
            tn: '045',
            num: '045',
          },
          '5': {
            tn: '046',
            num: '046',
          },
          '6': {
            num: '500',
          },
        },
        {
          '0': {
            label: '3rd preceding taxation year ending:',
            labelClass: 'font-small',
            inputType: 'date',
            tn: '007',
            'disabled': true,
            'cannotOverride': true
          },
          '1': {
            tn: '008',
            num: '008',

          },
          '2': {
            tn: '018',
            num: '018',

          },
          '3': {
            tn: '038',
            num: '038',
          },
          '4': {
            tn: '047',
            num: '047',
          },
          '5': {
            tn: '048',
            num: '048',
          },
          '6': {
            num: '700',
          },
        },
        {
          '0': {
            label: 'Total loss carried back:  &nbsp&nbsp&nbsp&nbsp&nbsp<font size="4"><b>B</b></font>',
            type: 'none',
          },
          '1': {
            num: '800',

          },
          '2': {
            num: '801',

          },
          '3': {
            num: '802',
          },
          '4': {
            type: 'none'
          },
          '5': {
            num: '803',
          },
          '6': {
            type: 'none'
          },
        },
        {
          '0': {
            label: 'Balance of current year loss available for carry forward (Amount A minus amount B)',
            type: 'none',
          },
          '1': {
            tn: '010',
            num: '010',

          },
          '2': {
            tn: '020',
            num: '020',
          },
          '3': {
            tn: '040',
            num: '040',
          },
          '4': {
            type: 'none'
          },
          '5': {
            tn: '050',
            num: '050',
          },
          '6': {
            type: 'none'
          },
        }
      ],
    },
    '1004': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'text',
        },
        {
          type: 'none',
          colClass: 'third-col-width'
        },
        {
          colClass: 'std-input-col-width',
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-col-width',
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
      ],
      "cells": [
        {
          "0": {
            label: 'Person to contact to discuss this application:',
            type: 'none'
          },
          "2": {
            label: 'Telephone Number <br>(Area Code)',
            type: 'none'
          },
          "4": {
            label: 'Fax Number <br>(Area Code)',
            type: 'none'
          },
        },
        {
          "0": {
            "tn": "052",
            num: '052'
          },
          "2": {
            "tn": "054",
            num: '054',
            'telephoneNumber': true,
            type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}'],
            validate: 'phone'
          },
          "4": {
            "tn": "056",
            num: '056',
            'telephoneNumber': true,
            type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}'],
            validate: 'phone'
          },
        }
      ]
    },


    //continue here

    '1010': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none'},
        {type: 'none', colClass: 'std-padding-width'},
      ],
      'cells': [
        {
          '0': {label: 'I,'},
          '1': {tn: '070'},
          '2': {num: '070', type: 'text', 'cannotOverride': true},
          '4': {tn: '072'},
          '5': {num: '072', type: 'text', 'cannotOverride': true},
          '7': {tn: '074'},
          '8': {num: '074', type: 'text', 'cannotOverride': true},
          '9': {label: ','},
        },
        {
          '2': {label: 'Print Surname', cellClass: 'alignCenter'},
          '5': {label: 'Print First Name', cellClass: 'alignCenter'},
          '8': {label: 'Position, office or rank', cellClass: 'alignCenter'}
        }
      ]
    },
    '1020': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {type: 'none', colClass: 'half-col-width'},
        {type: 'none'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'none', colClass: 'third-col-width'},
        {type: 'none', colClass: 'std-input-width'},
        {type: 'none', colClass: 'std-padding-width'},
      ],
      'cells': [
        {
          '0': {label: 'Signature: '},
          '1': {type: 'text'},
          '3': {label: 'Date:'},
          '4': {num: '955', type: 'date', 'cannotOverride': true},
        },
      ]
    },
    '1100': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {type: 'none', colClass: 'std-input-col-padding-width'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'none'},
        {type: 'none', colClass: 'third-col-width'},
      ],
      'cells': [
        {
          '3': {label: 'a) the first day following the loss year;'},
        },
        {
          '3': {label: 'b) the day on which the tax return for the loss year is filed; '},
        },
        {
          '3': {label: 'c) the day on which the application for the loss carry-back is filed; and '},
        },
        {
          '3': {label: 'd) the day on which a request was made for the loss carry-back.'},
        },
      ]
    },
  }
})();
