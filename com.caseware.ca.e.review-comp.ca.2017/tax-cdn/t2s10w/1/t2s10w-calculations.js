(function() {

  wpw.tax.create.calcBlocks('t2s10w', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field, form) {
      // update from rate table
      calcUtils.getGlobalValue('101Mult', 'ratesFed', '245');
      calcUtils.getGlobalValue('228Mult', 'ratesFed', '246');
      calcUtils.getGlobalValue('106Mult', 'ratesFed', '247');
      calcUtils.getGlobalValue('210Mult', 'ratesFed', '248');
      calcUtils.getGlobalValue('213Mult-0', 'ratesFed', '249-0');
      calcUtils.getGlobalValue('213Mult-1', 'ratesFed', '249-1');

      var dateCompare = calcUtils.dateCompare;
      //update from the additional table
      var taxStart = calcUtils.getGlobalValue(false, 'CP', 'tax_start');
      var taxEnd = calcUtils.getGlobalValue(false, 'CP', 'tax_end');
      var repeatFormIds = [];

      field('1002').getRows().forEach(function(row, rIndex) {
        repeatFormIds.push('T2S10ADD-' + field('1002').getRowInfo(rIndex).repeatId);
      });

      var totalNum = {
        '222': 0,
        '226': 0,
        '242': 0,
        '246': 0
      };

      var disableAdd = false;
      var disableDed = false;

      repeatFormIds.forEach(function(formId) {
        //only update if the date of addition is between fiscal period, otherwise update to historical table
        //additional
        var dateAddition = form(formId).field('1120').get();
        if (dateCompare.betweenInclusive(dateAddition, taxStart, taxEnd)) {
          totalNum['222'] += form(formId).field('1123').get();
          totalNum['226'] += form(formId).field('1124').get();
          disableAdd = true;
        }

        //disposals
        var dateDisposal = form(formId).field('1131').get();
        if (dateCompare.betweenInclusive(dateDisposal, taxStart, taxEnd)) {
          totalNum['242'] += form(formId).field('1128').get();
          totalNum['246'] += form(formId).field('1129').get();
          disableDed = true;
        }
      });

      Object.keys(totalNum).forEach(function(fieldId) {
        if (totalNum[fieldId] != 0) {
          field(fieldId).assign(totalNum[fieldId]);
        }
      });

      field('222').disabled(disableAdd);
      field('226').disabled(disableAdd);
      field('242').disabled(disableDed);
      field('246').disabled(disableDed);
    });
    calcUtils.calc(function(calcUtils, field, form) {
      // Part 1 calculations //
      calcUtils.sumBucketValues('101', ['222', '226']);
      field('102').assign(field('101').get() * field('101Mult').get() / 100);
      field('103').assign(field('228').get() * field('228Mult').get() / 100);
      calcUtils.subtract('104', '102', '103');
      calcUtils.equals('105', '104');
      calcUtils.sumBucketValues('230', ['200', '105', '224']);
      var sum106 = [242, 244, 246];
      calcUtils.sumBucketValues('106', sum106);
      field('248').assign(field('106').get() * field('106Mult').get() / 100);
      calcUtils.subtract('107', '230', '248', true);
      if (field('107').get() < 0) {
        calcUtils.removeValue(['108', '109', '110', '250', '112', '113', '300'], true);
      }
      else {
        var part2 = [400, 199, 201, 202, 203, 409, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 410,
          401, 402, 408, 204, 205];
        calcUtils.equals('108', '107');
        calcUtils.equals('109', '249');
        calcUtils.subtract('110', '108', '109');
        calcUtils.getGlobalValue('110Mult', 'ratesFed', '241');
        var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();
        var num250BeforeProrated = (field('110').get() * field('110Mult').get() / 100);
        field('250').assign(num250BeforeProrated * Math.min(daysFiscalPeriod / 365, 1));
        calcUtils.sumBucketValues('112', ['249', '250']);
        calcUtils.equals('113', '112');
        calcUtils.subtract('300', '107', '113');
        calcUtils.removeValue(part2, true);
      }
    })
  });
})();
