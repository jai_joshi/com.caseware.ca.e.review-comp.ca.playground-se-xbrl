(function() {
  wpw.tax.create.diagnostics('t2s10w', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('010W.1000', common.prereq(common.and(
        common.requireFiled('T2S10'),
        function(tools) {
          var forms = wpw.tax.form.allRepeatForms('t2s10w');
          return angular.forEach(forms, function(form) {
            return form.field('1000').get() == '6';
          });
        }), function(tools) {
      var forms = wpw.tax.form.allRepeatForms('t2s10w');
      var fields = [];
      angular.forEach(forms, function(form) {
        fields.push(tools.field(form.repeatFormId + '.1000'));
      });
      return tools.requireAll(fields, function() {
        return this.get() != '6';
      })
    }));

  });
})();
