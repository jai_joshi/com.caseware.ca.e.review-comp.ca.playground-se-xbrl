(function() {

  function createMultTable(rowObjArr, startCol) {
    var columns = [
      {
        type: 'none'
      },
      {
        colClass: 'std-padding-width',
        type: 'none'
      },
      {
        colClass: 'std-input-width'
      },
      {
        colClass: 'std-padding-width',
        type: 'none'
      },
      {
        width: '107px'
      },
      {
        width: '37px',
        type: 'none'
      },
      {
        colClass: 'std-padding-width',
        type: 'none'
      },
      {
        colClass: 'std-input-width'
      },
      {
        colClass: 'std-padding-width',
        type: 'none'
      }
    ];

    var cells = [
      {
        0: {label: rowObjArr[0].label, cellClass: rowObjArr[0].cellClass},
        1: {tn: rowObjArr[1].tn, label: rowObjArr[1].label},
        2: {num: rowObjArr[2].num},
        3: {tn: rowObjArr[3].tn, label: rowObjArr[3].label},//x
        4: {num: rowObjArr[4].num},
        5: {tn: rowObjArr[5].tn, label: rowObjArr[5].label},//%=
        6: {tn: rowObjArr[6].tn, label: rowObjArr[6].label},
        7: {num: rowObjArr[7].num},
        8: {tn: rowObjArr[8].tn, label: rowObjArr[8].label}
      }
    ];

    switch (startCol) {
      case 'left':
        columns.push(
            {
              colClass: 'std-input-width',
              type: 'none'
            },
            {
              colClass: 'std-padding-width',
              type: 'none'
            },
            {
              colClass: 'std-input-width',
              type: 'none'
            },
            {
              colClass: 'std-padding-width',
              type: 'none'
            }
        );
        break;
      case 'center':
        columns.push(
            {
              colClass: 'std-input-width',
              type: 'none'
            },
            {
              colClass: 'std-padding-width',
              type: 'none'
            }
        );
        break;
    }

    return {
      infoTable: true,
      fixedRows: true,
      columns: columns,
      cells: cells
    }
  }

  wpw.tax.global.tableCalculations.t2s10w = {
    '1002': {
      'showNumbering': true,
      'linkedRepeatForm': 't2s10add',
      'maxLoop': 100000,
      'columns': [
        {
          'header': 'Description',
          'num': '1010',
          'linkedFieldId': '1025',
          'disabled': true,
          type: 'text'
        },
        {
          'header': 'Acquisition date',
          'num': '1012',
          'type': 'date',
          'linkedFieldId': '1120',
          'disabled': true,
          colClass: 'std-input-width'
        },
        {
          'header': 'Cost of acquisition',
          'num': '1013',
          'linkedFieldId': '1123',
          'disabled': true,
          colClass: 'std-input-width'
        },
        {
          'header': 'Other adjustments',
          'num': '1014',
          'linkedFieldId': '1124',
          'disabled': true,
          colClass: 'std-input-width'
        },
        {
          'header': 'Disposal date',
          'num': '1015',
          'type': 'date',
          'linkedFieldId': '1131',
          'disabled': true,
          colClass: 'std-input-width'
        },
        {
          'header': 'Proceeds of disposition',
          'num': '1016',
          'linkedFieldId': '1128',
          'disabled': true,
          colClass: 'std-input-width'
        },
        {
          'header': 'Other adjustments',
          'num': '1017',
          'linkedFieldId': '1129',
          'disabled': true,
          colClass: 'std-input-width'
        }
      ]
    },
    '991': createMultTable(
        [
          {label: 'Subtotal (line 222 plus line 226)', cellClass: 'alignRight'},
          {}, {num: '101'}, {label: 'x'}, {num: '101Mult'}, {label: '%='}, {}, {num: '102'}, {label: 'B'}
        ],
        'center'
    ),
    '992': createMultTable(
        [
          {
            label: 'Non-taxable portion of a non-arm\'s length transferor\'s gain realized on the transfer of an' +
            ' eligible capital property to the corporation after December 20, 2002.'
          },
          {tn: '228'}, {num: '228'}, {label: 'x'}, {num: '228Mult'}, {label: '%='}, {}, {num: '103'}, {label: 'C'}],
        'center'
    ),
    '993': createMultTable(
        [{label: '(add amounts G, H, and I)', cellClass: 'alignRight'},
          {}, {num: '106'}, {label: 'x'}, {num: '106Mult'}, {label: '%='}, {tn: '248'}, {num: '248'}, {label: 'J'}]
    ),
    '994': createMultTable(
        [{label: 'Current year deduction', cellClass: 'bold alignRight'},
          {}, {num: '110'}, {label: 'x'}, {num: '110Mult'}, {label: '%='}, {tn: '250'}, {num: '250'}, {label: '*'}],
        'center'
    )
  }
})();
