(function() {
  wpw.tax.create.formData('t2s10w', {
    formInfo: {
      abbreviation: 'T2S10W',
      isRepeatForm: true,
      repeatFormData: {
        titleNum: '1001'
      },
      neededRepeatForms: ['t2s10add'],
      title: 'CUMULATIVE ELIGIBLE CAPITAL DEDUCTION WORKCHART',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      //subTitle: '(2002 and later taxation years)',
      schedule: 'SCHEDULE 10 WORKCHART',
      showCorpInfo: true,
      description: [
        {
          text: '• For use by a corporation that has eligible capital property. For more information, see ' +
          'the <i> T2 Corporation Income Tax Tax Guide </i>.'
        },
        {text: '• A separate cumulative eligible capital account must be kept for each business.'},
        {
          text: '<b>Complete one workchart for each type of eligible capital. The total amounts for each line ' +
          'will be posted on Schedule 10</b>'
        }
      ],
      category: 'Workcharts'

    },
    sections: [
      {
        'header': 'Calculation of current year deduction and carry-forward',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Fiscal period of the corporation include January 1, 2017',
            'type': 'infoField',
            'inputType': 'radio',
            'labelWidth': '80%',
            'num': '100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'link',
            'label': '<b>ECP should be updated using an asset class 14.1 as of January 1, 2017</b><br><br> Go to Schedule 8 and update Class 14.1',
            'labelClass': 'fullLength ',
            'labelWidth': '60%',
            'note': 'Please Click here to go to Schedule 8',
            'formId': 't2s8',
            'showWhen': {
              'fieldId': '100'
            }
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'indicatorColumn': true,
            'label': 'Select type:',
            'num': '1000',
            'inputType': 'dropdown',
            'options': [
              {
                'option': 'Goodwill',
                'value': '1'
              },
              {
                'option': 'Customer lists',
                'value': '2'
              },
              {
                'option': 'Ledger accounts',
                'value': '3'
              },
              {
                'option': 'Trademarks',
                'value': '4'
              },
              {
                'option': 'Patents, franchises and licences with no limited period',
                'value': '5'
              },
              {
                'option': 'Expenses of incorporation, reorganization or amalgamation',
                'value': '6'
              },
              {
                'option': 'Milk and eggs quotas',
                'value': '7'
              },
              {
                'option': 'Initiation or admission fees',
                'value': '8'
              },
              {
                'option': 'Appraisal costs',
                'value': '9'
              },
              {
                'option': 'Legal and accounting fees',
                'value': '10'
              },
              {
                'option': 'Government rights',
                'value': '11'
              },
              {
                'option': 'Other',
                'value': '12'
              }
            ],
            'description': 'Type of eligible capital property'
          },
          {
            'label': 'Business name:',
            'type': 'infoField',
            'indicatorColumn': true,
            'num': '1001'
          },
          {
            'label': '<b> Cumulative eligible capital - Balance at the end of the preceding taxation year </b> (if negative, enter \'0\')',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200'
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': '<b> Add: </b> Cost of eligible capital property acquired during the taxation year',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '222'
                },
                'padding': {
                  'type': 'tn',
                  'data': '222'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Other adjustment',
            'labelClass': '  4',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '226'
                },
                'padding': {
                  'type': 'tn',
                  'data': '226'
                }
              },
              null,
              null
            ]
          },
          {
            'type': 'table',
            'num': '991'
          },
          {
            'type': 'table',
            'num': '992'
          },
          {
            'label': 'Amount B minus amount C (if negative, enter \'0\')',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '104'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '105'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Amount transferred on amalgamation or wind-up of subsidiary',
            'labelClass': '4',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '224'
                },
                'padding': {
                  'type': 'tn',
                  'data': '224'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Subtotal (add amounts A, D, and E)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '230'
                },
                'padding': {
                  'type': 'tn',
                  'data': '230'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': '<b> Deduct: </b> Proceeds of sales (less outlays and expenses not otherwise deductible) from the disposition of all eligible capital property during the taxation year',
            'labelClass': '4',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '242'
                },
                'padding': {
                  'type': 'tn',
                  'data': '242'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G'
                }
              }
            ]
          },
          {
            'label': 'The gross amount of a reduction in respect of a forgiven debt obligation as provided for in subsection 80(7)',
            'labelClass': '4',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '244'
                },
                'padding': {
                  'type': 'tn',
                  'data': '244'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'H'
                }
              }
            ]
          },
          {
            'label': 'Other adjustments',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '246'
                },
                'padding': {
                  'type': 'tn',
                  'data': '246'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'I'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '993'
          },
          {
            'label': '<b> Cumulative eligible capital balance </b> (amount F minus amount J)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '107'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': '(if amount K is negative, enter \'0\' at line M)'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Cumulative eligible capital for a property no longer owned after ceasing to carry on that business',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '249'
                },
                'padding': {
                  'type': 'tn',
                  'data': '249'
                }
              },
              null
            ]
          },
          {
            'label': 'Amount K',
            'labelClass': 'text-right ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '108'
                }
              },
              null,
              null
            ]
          },
          {
            'label': '<b> less </b> amount from line 249',
            'labelClass': 'text-right ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '109'
                }
              },
              null,
              null
            ]
          },
          {
            'type': 'table',
            'num': '994'
          },
          {
            'label': '(line 249 plus line 250)(enter this amount at line 405 of Schedule1',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '112'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '113'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Cumulative eligible capital - Closing balance </b> (amount K minus amount L)(if negative, enter \'0\')',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '300'
                },
                'padding': {
                  'type': 'tn',
                  'data': '300'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'label': '*You can claim any amount up to the maximum deduction of 7%. The deduction may not exceed the maximum amount prorated by the number of days in the taxation year divided by 365',
            'labelWidth': '75%'
          }
        ]
      },
      {
        'header': 'History of eligible capital property by type',
        'linkedRepeatForm': 't2s10add',
        'rows': [
          {
            'type': 'table',
            'num': '1002'
          }
        ]
      }
    ]
  })
})();
