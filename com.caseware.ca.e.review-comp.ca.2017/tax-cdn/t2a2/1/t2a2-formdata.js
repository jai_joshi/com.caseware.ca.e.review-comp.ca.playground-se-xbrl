(function () {
  'use strict';

  wpw.tax.global.formData.t2a2 = {
    'formInfo': {
      'abbreviation': 'AT271',
      'title': 'Alberta Income Allocation Factor - Schedule 2',
      hideHeaderBox: true,
      schedule: 'AT271',
      printTitle: 'ALBERTA INCOME ALLOCATION FACTOR - AT1 SCHEDULE 2',
      //'headerImage': 'canada-federal',
      description: [
        {
          'text': 'For corporations with taxable income that is in part allocable to permanent establishments outside Alberta. Report\n' +
          'all monetary values in dollars; DO NOT include cents.'
        }
      ],
      category: 'Alberta Tax Forms',
      formFooterNum: 'AT271 (Jul-12)'

    },
    sections: [
      {
        'rows': [
          {
            label: 'Review the types of operations listed in AREA B.',
            labelClass: 'fullLength indent'
          },
          {
            'type': 'infoField',
            'label': 'Is the corporation in any of these special allocation categories?',
            labelClass: 'indent',
            'num': '001',
            'tn': '001',
            'inputType': 'radio',
            'disabled': true,
            'cannotOverride': true
          },
          {
            label: 'If "No", complete AREA A - General Allocation Formula to determine the corporation\'s Alberta Allocation Factor. ' +
            'If "Yes", complete the applicable line in AREA B - Special Allocation Formula to determine the corporation\'s Alberta ' +
            'Allocation Factor.',
            labelClass: 'fullLength indent'
          },
          {
            label: '<b>Divided Businesses (ITA Reg 412):</b> where more than one special allocation formula applies to a corporation, complete only the ' +
            'calculation for Divided Businesses at the bottom of page 2.',
            labelClass: 'fullLength indent'
          },
          {
            label: '<b>Non-resident Corporations (ITA Reg 413):</b> Where a corporation is not resident in Canada, "salaries and wages paid in all ' +
            'jurisdictions" by the corporation does not include salaries and wages paid to employees of a permanent establishment outside of ' +
            'Canada. When calculating using the general allocation formula under ITA Reg. 402(3)(a), "gross revenue in all jurisdictions" does not ' +
            'include gross revenue reasonably attributable to a permanent establishment outside Canada.',
            labelClass: 'fullLength indent'
          },
          {
            label: 'Use the amounts from the federal Schedule 5 to complete the applicable formula.',
            labelClass: 'bold indent'
          },
          {
            label: 'References to Regulations below are to those of the Income Tax Act (Canada), as adopted by the Alberta Corporate Tax Act.',
            labelClass: 'font-small'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'type': 'table',
            'num': '2000'
          },
          {
            label: '* Salaries & wages paid by the corporation to employees of its permanent establishments (other than ships) in Canada',
            labelClass: 'font-small'
          },
          {
            'type': 'table',
            'num': '3000'
          },
        ]
      },
    ]
  };
})();
