(function () {
  var areaBInfo = [['Bus and Truck Operators (ITA Reg 409)', ['012', 'Salaries & wages paid in Alberta <br> &nbsp; <br> &nbsp;'], ['014', 'Total salaries & wages paid <br> &nbsp;<br> &nbsp;<br> &nbsp;'], ['016', 'Kilometres traveled in Alberta <br> &nbsp; <br> &nbsp;'], ['018', 'Total kilometres traveled in jurisdictions where corporation has permanent establishment'], ['019', '(A/B + C/D) x 1/2 <br> &nbsp; <br> &nbsp; <br> &nbsp;']],
    ['Grain Elevator Operators (ITA Reg 408)', ['022', 'Salaries & wages paid in Alberta  <br> &nbsp;'], ['024', 'Total salaries & wages paid <br> &nbsp; <br> &nbsp;'], ['026', 'Bushels of grain received<br> at Alberta elevators <br> &nbsp;'], ['028', 'Bushels of grain received <br> at all elevators <br> &nbsp;'], ['029', '(A/B + C/D) x 1/2 <br> &nbsp; <br> &nbsp;']],
    ['Pipeline Operators (ITA Reg 411)', ['032', 'Salaries & wages paid in Alberta <br> &nbsp; <br> &nbsp;'], ['034', 'Total salaries & wages paid <br> &nbsp;  <br> &nbsp;<br> &nbsp;'], ['036', 'Miles of pipeline in Alberta<br> &nbsp; <br> &nbsp;<br> &nbsp;'], ['038', 'Total miles of pipeline in provinces where corporation has permanent establishment'], ['039', '(A/B + C/D) x 1/2 <br> &nbsp; <br> &nbsp; <br> &nbsp;']],
    ['Insurance Corporations (ITA Reg 403)', [true, ''], [true, ''], ['046', 'Net premiums in Alberta <br> &nbsp;'], ['048', 'Total net premiums earned <br> &nbsp;'], ['049', 'C/D <br> &nbsp;']],
    ['Chartered Banks (ITA Reg 404)', ['052', 'Salaries and wages paid in Alberta <br> &nbsp;'], ['054', 'Total salaries and wages paid <br> &nbsp;'], ['056', 'Loans & deposits in Alberta <br> &nbsp; <br> &nbsp;'], ['058', 'Total loans & deposits <br> &nbsp; <br> &nbsp;'], ['059', '(A/B + 2C/D) x 1/3 <br> &nbsp; <br> &nbsp;']],
    ['Trust & Loan Corporations (ITA Reg 405)', [true, ''], [true, ''], ['066', 'Gross revenue earned in Alberta <br> &nbsp;'], ['068', 'Total gross revenue <br> &nbsp; <br> &nbsp;'], ['069', 'C/D <br> &nbsp; <br> &nbsp;']],
    ['Airline Corporations (ITA Reg 407)', ['072', 'Fixed asset cost (other than aircraft) in Alberta<br> &nbsp; <br> &nbsp;'], ['074', 'Fixed asset cost (other than aircraft) in Canada<br> &nbsp;<br> &nbsp;'], ['076', 'Revenue plane miles flown in Alberta<br> &nbsp;<br> &nbsp;'], ['078', 'Revenue plane miles flown in Canada where the corporation has permanent establishment'], ['079', '(A/B + 3C/D) x 1/4<br> &nbsp;<br> &nbsp;<br> &nbsp;']],
    ['Railway Corporations (ITA Reg 406)', ['082', 'Equated track miles in Alberta<br> &nbsp;'], ['084', 'Total equated track miles in Canada<br> &nbsp;'], ['086', 'Gross ton miles in Alberta<br> &nbsp;<br> &nbsp;'], ['088', 'Total gross ton miles in Canada<br> &nbsp;'], ['089', '(A/B + C/D) x 1/2<br> &nbsp;<br> &nbsp;']],
    ['Ship Operators: (ITA Reg 410)', ['090', '<b>A</b><br> Salaries and wages paid in Alberta<br> &nbsp;'], ['092', '<b>B</b><br> Total salaries and wages paid in Canada*<br> &nbsp;'], ['094', '<b>C</b><br> Port-call-tonnage in Alberta<br> &nbsp;<br> &nbsp;'], ['096', '<b>D</b><br> Total port-call-tonnage in all provinces with permanent establishments'], true],
    ['Ship Operators: (ITA Reg 410)', ['098', '<b>E</b><br> Total port-call-tonnage in Canada<br> &nbsp;'], ['100', '<b>F</b><br> Total port-call-tonnage in all countries<br> &nbsp;'], ['102', '<b>G</b><br> (E/F) x (AT1 lines 062 - 064)<br> &nbsp;'], ['104', '<b>H</b><br> (A/B) x [(AT1 lines 062 - 064) - G]<br> &nbsp;'], ['105', '[(G x C/D) + H]/<br>(AT1 lines 062 - 064)<br> &nbsp;<br> &nbsp;']],

  ];

  function fillAreaB(infoArray) {
    var data = [];
    for (var i in infoArray) {
      var temp = {};
      for (var j in infoArray[i]) {
        if (j == 0) {
          if (infoArray[i][j] == true) {
            temp[j] = {
              type: 'none'
            }
          } else {
            temp[j] = {
              label: infoArray[i][j]
            }
          }
        }
        if (j > 0 && j < infoArray[i].length - 1) {
          if (infoArray[i][j][0] == true) {
            temp[j] = {
              type: 'none'
            }
          } else {
            temp[j] = {
              tn: infoArray[i][j][0],
              num: infoArray[i][j][0],
              label: infoArray[i][j][1],
              'disabled': true,
              cannotOverride: true
            }
            if (infoArray[i][j][0] == '096') {
              temp[j] = {
                tn: infoArray[i][j][0],
                num: infoArray[i][j][0],
                label: infoArray[i][j][1],
                'disabled': true
              }
            }
          }
        }
        ;
        if (j == infoArray[i].length - 1) {
          if (infoArray[i][j] == true) {
            temp[j] = {
              type: 'none'
            }
          } else {
            temp[j] = {
              num: infoArray[i][j][0],
              label: infoArray[i][j][1],
              decimals: 6,
              'disabled': true,
              cannotOverride: true
            }
          }
        }
      }
      data.push(temp);
    }
    return data
  }


  wpw.tax.global.tableCalculations.t2a2 = {
    '1000': {
      fixedRows: true,
      superHeaders: [
        {
          "header": 'AREA A - General Allocation Formula (ITA Reg 402)<br>' +
          '<font size="1">* If either amount B or D is nil, do not multiple by 1/2.</font>',
          headerClass: 'leftAlign',
          "colspan": "4"
        },
        {
          "header": 'Alberta Allocation Factor<br>' +
          '<font size="1">(calculate to 6 decimal places)</font><br>' +
          '<i>Carry this amount forward to AT1 line 065</i>',
        },
      ],
      columns: [
        {
          colClass: 'std-input-width',
          header: 'A'
        },
        {
          colClass: 'std-input-width',
          header: 'B'
        },
        {
          colClass: 'std-input-width',
          header: 'C',
        },
        {
          colClass: 'std-input-width',
          header: 'D'
        },
        {
          colClass: 'std-input-width',
          header: 'I'
        },
      ],
      "cells": [
        {
          "0": {
            label: 'Salaries and wages paid in Alberta',
            tn: '002',
            num: '002',
            'disabled': true,
          },
          "1": {
            label: 'Total salaries and wages paid in all jurisdictions',
            tn: '004',
            num: '004',
            'disabled': true,
          },
          "2": {
            label: ' Gross revenue in Alberta <br> &nbsp;',
            tn: '006',
            num: '006',
            'disabled': true,
          },
          "3": {
            label: 'Gross revenue in all jurisdictions',
            tn: '008',
            num: '008',
            'disabled': true,
          },
          "4": {
            label: '(A/B + C/D) x 1/2*<br> &nbsp;',
            num: '009',
            decimals: 6,
            'disabled': true, cannotOverride: true
          },
        }
      ]
    },
    '2000': {
      fixedRows: true,
      superHeaders: [
        {
          "header": 'AREA B - Special Allocation Formulas<br>',
          headerClass: 'leftAlign',
          "colspan": "5"
        },
        {
          "header": 'Alberta Allocation Factor<br>' +
          '<font size="1">(calculate to 6 decimal places)</font><br>' +
          '<i>Carry this amount forward to AT1 line 065</i>',
        },
      ],
      columns: [
        {
          colClass: 'half-col-width',
          header: 'Type of operation',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          header: 'A'
        },
        {
          colClass: 'std-input-width',
          header: 'B'
        },
        {
          colClass: 'std-input-width',
          header: 'C',
        },
        {
          colClass: 'std-input-width',
          header: 'D'
        },
        {
          colClass: 'std-input-width',
          header: 'I'
        },
      ],
      "cells": fillAreaB(areaBInfo)
    },
    '3000': {
      fixedRows: true,
      columns: [
        {
          width: '107px',
          type: 'none'
        },
        {
          colClass: 'std-address-width',
        },
        {},
        {
          width: '173px',
        },
      ],
      "cells": [
        {
          "0": {
            label: 'Divided Businesses (ITA Reg 412)'
          },
          "1": {
            label: '<b>A</b><br> Taxable in Alberta <br>(See Guide for details)',
            tn: '106',
            num: '106',
            'disabled': true,
            cannotOverride: true
          },
          "2": {
            label: '<b>B</b><br>AT1 line 062 - AT1 line 064<br> &nbsp;',
            tn: '108',
            num: '108',
            'disabled': true,
            cannotOverride: true
          },
          "3": {
            label: 'A/B<br> &nbsp;<br> &nbsp;',
            num: '109',
            'disabled': true,
            cannotOverride: true
          }
        }
      ]
    },
  }
})();
