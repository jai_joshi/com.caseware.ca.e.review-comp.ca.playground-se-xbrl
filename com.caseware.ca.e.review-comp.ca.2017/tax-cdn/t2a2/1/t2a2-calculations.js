(function () {

  function basicAllocation(calcUtils, field, ColAB, criteria, fieldArr, minus167, normfactor) {
    if (field('t2s5.100').get() == criteria) {
      calcUtils.getGlobalValue(fieldArr[0], 't2s5', '119');
      field(fieldArr[1]).assign(ColAB[0]);
      field(fieldArr[1]).source(field('T2S5.129'));
      calcUtils.getGlobalValue(fieldArr[2], 't2s5', '159');
      if (minus167) {
        field(fieldArr[3]).assign(ColAB[1]);
        field(fieldArr[3]).source(field('T2S5.129'));
      } else {
        calcUtils.getGlobalValue(fieldArr[3], 't2s5', '169');
      }
    }
    var tempfactor = 0;
    if (normfactor) {
      if (field(fieldArr[1]).get() != 0) {
        tempfactor += field(fieldArr[0]).get() / field(fieldArr[1]).get();
      }
      if (field(fieldArr[3]).get() != 0) {
        tempfactor += field(fieldArr[2]).get() / field(fieldArr[3]).get();
      }
      if (field(fieldArr[1]).get() != 0 && field(fieldArr[3]).get() != 0) {
        tempfactor = tempfactor / 2;
      }
      field(fieldArr[4]).assign(tempfactor);
    }
  }


  wpw.tax.create.calcBlocks('t2a2', function (calcUtils) {
    var removeAreaValue = ['002', '004', '008', '009', '012', '014', '016', '018', '019', '022', '024', '026', '028', '029', '032', '034', '036', '038', '039',
      '046', '048', '049', '052', '054', '056', '058', '059', '066', '068', '069', '072', '074', '076', '078', '079', '082', '084', '086', '088', '089',
      '090', '092', '094', '096', '098', '100', '102', '104', '105', '106', '108', '109'];


    calcUtils.calc(function (calcUtils, field) {
      var ColAB=[0,0];
      if(field('Cp.080').get!='2'){
        ColAB[0]=field('T2S5.129').get();
        ColAB[1]=field('T2S5.169').get();
      }else{
        ColAB[0]=field('T2S5.129').get() - field('T2S5.127').get();
        ColAB[1]=field('T2S5.169').get() - field('T2S5.167').get();
      }
      calcUtils.removeValue(removeAreaValue);
      field('001').assign(1);
      if (field('t2s5.100').get() == '402') {
        field('001').assign(2);
      }
      field('001').source(field('T2S5.100'))
      if (field('001').get() == 2 && field('t2s5.100').get() == '402') {
        calcUtils.getGlobalValue('002', 't2s5', '119');
        field('004').assign(ColAB[0]);
        field('004').source(field('T2S5.129'));
        calcUtils.getGlobalValue('006', 't2s5', '159');
        field('008').assign(ColAB[1]);
        field('008').source(field('T2S5.169'));
      }
      //basic link to T2S5.100
      field('2000').getRows().forEach(function (row) {
        row[1].source(field('T2S5.100'));
        row[2].source(field('T2S5.100'));
        row[3].source(field('T2S5.100'));
        row[4].source(field('T2S5.100'));
        row[5].source(field('T2S5.100'));
      });
      var factor009 = 0;
      if (field('004').get() != 0) {
        factor009 += field('002').get() / field('004').get();
      }
      if (field('008').get() != 0) {
        factor009 += field('006').get() / field('008').get();
      }
      if (field('008').get() != 0 && field('004').get() != 0) {
        factor009 = factor009 / 2;
      }
      field('009').assign(factor009);

      basicAllocation(calcUtils, field, ColAB, '409', ['012', '014', '016', '018', '019'], false, true);
      basicAllocation(calcUtils, field, ColAB, '408', ['022', '024', '026', '028', '029'], false, true);
      basicAllocation(calcUtils, field, ColAB, '411', ['032', '034', '036', '038', '039'], true, true);
      if (field('t2s5.100').get() == '403') {
        calcUtils.getGlobalValue('046', 't2s5', '159');
        calcUtils.getGlobalValue('048', 't2s5', '169');
      }
      field('049').assign(0);
      if (field('048').get() != 0) {
        field('049').assign(field('046').get() / field('048').get());
      }
      basicAllocation(calcUtils, field, ColAB, '404', ['052', '054', '056', '058', '059'], false, false);
      var field059 = 0;
      if (field('054').get() != 0) {
        field059 += field('052').get() / field('054').get();
      }
      if (field('058').get() != 0) {
        field059 += 2 * field('056').get() / field('058').get();
      }
      if (field('058').get() != 0 && field('054').get() != 0) {
        field059 = field059 / 3;
      }
      field('059').assign(field059);

      if (field('t2s5.100').get() == '405') {
        calcUtils.getGlobalValue('066', 't2s5', '159');
        calcUtils.getGlobalValue('068', 't2s5', '169');
      }
      field('069').assign(0);
      if (field('068').get() != 0) {
        field('069').assign(field('066').get() / field('068').get());
      }
      basicAllocation(calcUtils, field, ColAB, '407', ['072', '074', '076', '078', '079'], true, false);
      var field079 = 0;
      if (field('074').get() != 0) {
        field079 += field('072').get() / field('074').get();
      }
      if (field('078').get() != 0) {
        field079 += 3 * field('076').get() / field('078').get();
      }
      if (field('078').get() != 0 && field('074').get() != 0) {
        field079 = field079 / 4;
      }
      field('079').assign(field079);
      basicAllocation(calcUtils, field, ColAB, '406(1)', ['082', '084', '086', '088', '089'], true, true);
      basicAllocation(calcUtils, field, ColAB, '406(2)', ['082', '084', '086', '088', '089'], true, true);
      if (field('t2s5.100').get() == '410') {
        calcUtils.getGlobalValue('090', 't2s5', '119');
        field('092').assign(ColAB[0]);
        field('092').source(field('T2S5.129'));
        calcUtils.getGlobalValue('094', 't2s5', '159');
        field('098').assign(ColAB[1]);
        field('098').source(field('T2S5.129'));
        calcUtils.getGlobalValue('100', 't2s5', '169');
        if (field('100').get() != 0) {
          field('102').assign(field('098').get() / field('100').get() * (field('t2a.062').get() - field('t2a.064').get()));
        }
        if (field('092').get() != 0) {
          field('104').assign(field('090').get() / field('092').get() * (field('t2a.062').get() - field('t2a.064').get() - field('102').get()));
        }
        if (field('096').get() != 0 && (field('t2a.062').get() - field('t2a.064').get()) != 0) {//prevent division by 0;
          field('105').assign(((field('102').get() * field('094').get() / field('096').get()) + field('104').get()) / (field('t2a.062').get() - field('t2a.064').get()));
        }
      }
      if (field('t2s5.100').get() == '412') {
        calcUtils.getGlobalValue('106', 't2a', '066');
        field('108').assign(field('T2a.062').get() - field('T2a.064').get());
        field('108').source(field('T2a.062'));
        if (field('108').get() != 0) {
          field('109').assign(field('106').get() / field('108').get());
        }
      }

    });


  });
})
();