(function() {
  'use strict';

  wpw.tax.global.formData.t2s8rec = {
      formInfo: {
        abbreviation: 't2s8rec',
        title: 'Fixed Assets Reconciliation Workchart',
        showCorpInfo: true,
        category: 'Workcharts'
      },
      sections: [
        {
        header: ' ', hideFieldset: true,
          rows: [
            {
              label: 'Use this workchart to reconcile changes in fixed assets per financial ' +
              'statements and amounts used per tax return',
              labelClass: 'fullLength'
            },
            {
              label: 'Do you want to attached this form to your T2 Income Tax Return?',
              labelWidth: '75%',
              type: 'infoField',
              inputType: 'radio',
              num: '101',
              init: '2',
              labelClass: 'fullLength'
            }
          ]
        },
        {
          'header': 'Tax Return',
          'rows': [
            {
              'label': 'Additions for tax purposes - Schedule 8 regular classes',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '200'
                  }
                },
                null
              ]
            },
            {
              'label': 'Additions for tax purposes - Schedule 8 class 13 leasehold improvements',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '201'
                  }
                },
                null
              ]
            },
            {
              'label': 'Operating leases capitalized for book purposes',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '202'
                  }
                },
                null
              ]
            },
            {
              'label': 'Capital gain deferred',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '203'
                  }
                },
                null
              ]
            },
            {
              'label': 'Recapture deferred',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '204'
                  }
                },
                null
              ]
            },
            {
              'label': 'Deductible expenses capitalized for book purposes - Schedule 1',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '205'
                  }
                },
                null
              ]
            },
            {
              'type': 'table',
              'num': '210'
            },
            {
              'label': '<b>Total additions per books :</b>',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '220'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '221'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Proceeds up to original cost - Schedule 8 regular classes',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '230'
                  }
                },
                null
              ]
            },
            {
              'label': 'Proceeds up to original cost - Schedule 8 class 13 leasehold improvements',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '231'
                  }
                },
                null
              ]
            },
            {
              'label': 'Proceeds in excess of original cost - capital gain',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '232'
                  }
                },
                null
              ]
            },
            {
              'label': 'Recapture deferred - as above',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '233'
                  }
                },
                null
              ]
            },
            {
              'label': 'capital gain deferred - as above',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '234'
                  }
                },
                null
              ]
            },
            {
              'label': 'Pre V-day appreciation',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '235'
                  }
                },
                null
              ]
            },
            {
              'type': 'table',
              'num': '240'
            },
            {
              'label': '<b>Total proceeds per books :</b>',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '250'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '251'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ]
            },
            {
              'label': '<b>Less: </b>Depreciation and amortization per accounts - Schedule 1',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '260'
                  }
                }
              ]
            },
            {
              'label': '<b>Less: </b>Loss on disposal of fixed assets per accounts',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '261'
                  }
                }
              ]
            },
            {
              'label': '<b>Add: </b>Gain on disposal of fixed assets per accounts',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '262'
                  }
                }
              ]
            },
            {
              'label': 'Net change per tax return',
              'labelClass': 'text-right bold',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '263'
                  }
                }
              ]
            }
          ]
        },
        {
          'header': 'Financial statements',
          'rows': [
            {
              'label': 'Fixed assets (excluding land) per financial statements',
              'labelClass': 'bold'
            },
            {
              'label': 'Closing net book value',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '301'
                  }
                }
              ]
            },
            {
              'label': 'Opening net book value',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '302'
                  }
                }
              ]
            },
            {
              'label': 'Net change per financial statements',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '303'
                  }
                }
              ]
            }
          ]
        },
        {
        header: 'Financial statements',
          rows: [
            {
              label: 'If the amounts from the tax return and the financial statements differ, explain why below :',
              labelClass: 'fullLength'
            },
            {
              type: 'infoField',
              inputType: 'textArea',
              num: '304'
            }
          ]
        }
      ]
    };
})();
