(function() {
  wpw.tax.global.tableCalculations.t2s8rec = {
    "210": {
      "infoTable": true,
      hasTotals: true,
      "maxLoop": 100000,
      "showNumbering": true,
      "width": "calc((100% - 105px))",
      "columns": [{
        "header": "<b>Other</b>",
        "width": "60%",
        type: 'text'
      },
        {
          "type": "none"
        },
        {
          "header": "<b>Amount</b>",
          "hiddenTotal": true,
          "total": true,
          "totalNum": "211",
          colClass: 'std-input-width'
        }]
    },
    "240": {
      hasTotals: true,
      "infoTable": true,
      "maxLoop": 100000,
      "showNumbering": true,
      "width": "calc((100% - 105px))",
      "columns": [{
        "header": "<b>Other</b>",
        "width": "60%",
        type: 'text'
      },
        {
          "type": "none"
        },
        {
          "header": "<b>Amount</b>",
          "hiddenTotal": true,
          "total": true,
          "totalNum": "241",
          colClass: 'std-input-width',
          "filter": "currency"
        }]
    }
  }
})();
