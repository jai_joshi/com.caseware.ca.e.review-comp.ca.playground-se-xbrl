(function() {

  wpw.tax.create.calcBlocks('t2s8rec', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      var ccaInfo = [];
      var ccaWorkchartForms = wpw.tax.form.allRepeatForms('t2s8w');

      ccaWorkchartForms.forEach(function(form) {
        if (!wpw.tax.utilities.isEmptyOrNull(form.field('101').get())) {
          ccaInfo.push(
              {
                class: form.field('101').get(),
                additionAmount: form.field('203').get(),
                proceedAmount: form.field('207').get()
              });
        }
      });

      //num 200 - all addition on CCA workchart except class 13
      //num 201 - all addition on CCA workchart only for class 13
      //num 230 - all proceed on CCA workchart except class 13
      //num 231 - all proceed on CCA workchart only for class 13
      var num200 = 0;
      var num201 = 0;
      var num230 = 0;
      var num231 = 0;

      ccaInfo.forEach(function(data) {
        if (data.class == 13) {
          num201 += data.additionAmount;
          num231 += data.proceedAmount;
        }
        else {
          num200 += data.additionAmount;
          num230 += data.proceedAmount;
        }
      });

      field('200').assign(num200);
      field('201').assign(num201);
      field('230').assign(num230);
      field('231').assign(num231);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('260').assign(
          form('T2S1').field('104').get() +
          form('T2S1').field('105').get()
      );
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var gainLossOnDisposal = calcUtils.sumRepeatGifiValue('t2s125', 150, 8210);
      if (gainLossOnDisposal) {
        if (gainLossOnDisposal >= 0) {
          calcUtils.removeValue([261], true);
          field('262').assign(gainLossOnDisposal)
        }
        else {
          calcUtils.removeValue([262], true);
          gainLossOnDisposal = gainLossOnDisposal * (-1);
          field('261').assign(gainLossOnDisposal)
        }
      }
      field('261').source(field('t2s125.8210'));
    });


    calcUtils.calc(function(calcUtils, field, form) {
      field('301').assign(
          calcUtils.gifiValue('T2S100', null, 2008) +
          calcUtils.gifiValue('T2S100', null, 2009) -
          calcUtils.gifiValue('T2S100', '101', 1600)
      );

      field('301').source(field('t2s100.2008'));

      calcUtils.sumBucketValues('220', ['200', '201', '202', '203', '204', '205', '211']);
      calcUtils.equals('221', '220');
      calcUtils.sumBucketValues('250', ['230', '231', '232', '233', '234', '235', '241']);
      calcUtils.equals('251', '250');
      field('263').assign(
          field('221').get() -
          field('251').get() -
          field('260').get() -
          field('261').get() +
          field('262').get()
      );
      calcUtils.subtract('303', '301', '302')
    });
  });
})();
