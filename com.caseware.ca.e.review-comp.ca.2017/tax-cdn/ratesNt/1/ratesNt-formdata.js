(function() {

  wpw.tax.create.formData('ratesNt', {
    formInfo: {
      abbreviation: 'ratesNt',
      title: 'Northwest Territories Table of Rates and Values',
      showCorpInfo: true,
      description: [
        {type: 'heading', text: 'Northwest Territories'}
      ],
      category: 'Rates Tables'
    },
    sections: [
      {
        header: 'Schedule 461 – Northwest Territories Corporation Tax Calculation ',
        rows: [
          {
            'label': 'Lower rate from 2014 to present',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '703'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Higher rate from 2014 to present',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '705',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          }
        ]
      }
    ]
  });
})();
