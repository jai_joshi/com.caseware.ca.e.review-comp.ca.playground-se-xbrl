(function() {

  wpw.tax.create.calcBlocks('ratesNt', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      field('703').assign(4);
      field('705').assign(11.5);
    });
  });
})();
