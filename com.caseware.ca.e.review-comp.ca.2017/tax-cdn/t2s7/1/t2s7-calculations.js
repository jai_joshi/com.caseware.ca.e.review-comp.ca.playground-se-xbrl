(function() {
  function getTable3000Values(calcUtils, tableNum, tableRow, table3000Row) {
    var field = calcUtils.field;
    var table = field(tableNum);
    var sums = {};
    table.getRows().forEach(function(row) {
      var partnershipName = row[0].get();

      if (!sums[partnershipName]) {
        sums[partnershipName] = 0;
      }

      sums[partnershipName] += row[tableRow].get();
    });

    var nameIndex = {};

    field('1000').getRows().forEach(function(row, rIndex) {
      nameIndex[rIndex] = row[0].get();
    });

    field('3000').forEachRow(function(row, rIndex) {
      var partnershipName = nameIndex[rIndex];
      row[table3000Row].assign(sums[partnershipName]);
    });

  }

  wpw.tax.create.calcBlocks('t2s7', function(calcUtils) {
    //table summary from s3
    calcUtils.calc(function(calcUtils, field) {
      //net taxable dividend deductible amount
      var sourceTable541 = field('T2S3.541');
      var sourceTable542 = field('T2S3.542');
      var data = [];

      sourceTable541.getRows().forEach(function(row, rowIndex) {
        var dividendType = row[2].get();
        var dividendAmount = sourceTable542.cell(rowIndex, 2).get();
        data.push({'type': dividendType, 'amount': dividendAmount});
      });

      field('399').cell(0, 2).assign(calcUtils.sumByKey(data, 'type', '1', 'amount'));
      field('399').cell(0, 1).assign(calcUtils.sumByKey(data, 'type', '2', 'amount'));

      field('399').cell(0, 3).assign(
          field('399').cell(0, 1).get() +
          field('399').cell(0, 2).get()
      );

      field('559').cell(0, 1).assign(
          field('399').cell(0, 1).get() -
          field('410').get()
      );
      field('559').cell(0, 2).assign(
          field('399').cell(0, 2).get() -
          field('411').get()
      );
      field('559').cell(0, 3).assign(
          field('559').cell(0, 1).get() +
          field('559').cell(0, 2).get()
      );
    });

    //part 1 calcs
    calcUtils.calc(function(calcUtils, field) {
      //part1 calculations//
      var t2s6 = calcUtils.form('t2s6');
      var tables = ['101', '201', '301', '401', '501', '601', '700'];
      var positiveSum = 0;
      var negativeSum = 0;
      tables.forEach(function(tableNum) {
        var table = t2s6.field(tableNum);
        var numCols = table.size().cols;
        table.getCol(numCols - 4).forEach(function(cell) {
          if (cell.get() > 0) {
            positiveSum += cell.get();
          } else {
            negativeSum += cell.get();
          }
        });
      });
      var taxableCapitalGains = field("T2S6.989").get();
      if(taxableCapitalGains>0){
        positiveSum = Math.max(taxableCapitalGains,positiveSum);
      }

      field('002').assign(positiveSum * 0.5);
      field('012').assign(negativeSum * -0.5);

      calcUtils.getGlobalValue('022', 'T2J', '332');
      calcUtils.sumBucketValues('025', ['012', '022']);
      calcUtils.equals('027', '025');
      calcUtils.subtract('030', '002', '027');
      // TODO: tn32 updates according to sum table total + property income (need to know what's considered property income)
      // TODO: num52 needs to be updated when income from AgriInvest fund No.2 is defined//
      field('062').assign(field('T2S3.240').get());

      calcUtils.sumBucketValues('073', ['042', '052', '062', '072']);
      calcUtils.equals('074', '073');
      calcUtils.subtract('075', '032', '074');
      calcUtils.equals('076', '075');
      calcUtils.sumBucketValues('080', ['030', '076']);
      calcUtils.subtract('092', '080', '082');
    });

    //part 2 calcs
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.subtract('010', '001', '009', true);
      // TODO: num019 updates according to sum table foreign section + foreign rental income//
      field('049').assign(field('559').cell(0, 2).get());
      calcUtils.sumBucketValues('060', ['029', '049', '059']);
      calcUtils.equals('063', '060');
      calcUtils.subtract('064', '019', '063');
      calcUtils.equals('065', '064');
      calcUtils.sumBucketValues('066', ['010', '065']);
      calcUtils.subtract('079', '066', '069', true);
    });

    //part 3 calculations with tables only
    calcUtils.calc(function(calcUtils, field) {

      getTable3000Values(calcUtils, '4000', 7, 0);
      getTable3000Values(calcUtils, '5000', 5, 1);

      //table 1000 calc
      field('1000').getRows().forEach(function(row, rowIndex) {
        var table2000Row = field('2000').getRow(rowIndex);
        var table3000Row = field('3000').getRow(rowIndex);
        var table4000Row = field('4000').getRow(rowIndex);
        var table5000Row = field('5000').getRow(rowIndex);

        var tn325Val = wpw.tax.actions.calculateDaysDifference(table2000Row[2].get(), table2000Row[3].get());
        var typeOfPartnership = field('2001').get();

        if (!table2000Row || !table3000Row || !table4000Row || !table5000Row)
          return;

        if (typeOfPartnership == 1) {
          table5000Row.forEach(function(cell) {
            cell.assign();
            cell.disabled(true)
          });
          table4000Row.forEach(function(cell) {
            cell.disabled(false)
          });

        } else {
          table4000Row.forEach(function(cell) {
            cell.assign();
            cell.disabled(true)
          });
          table5000Row.forEach(function(cell) {
            cell.disabled(false)
          });
        }
        table2000Row[4].assign(tn325Val);
        //table2000 calcs
        var isC1Negative = (row[2].get() < 0);
        table2000Row[1].assign(row[2].get() + row[3].get() + table2000Row[0].get());
        table2000Row[5].assign((isC1Negative || row[1].get() == 0) ? 0 : ((row[2].get() / row[1].get()) * 500000 * (table2000Row[4].get() / 365)));

        table3000Row[2].assign(Math.max(table2000Row[5].get() + table3000Row[0].get() - table3000Row[1].get(), 0));
        table3000Row[3].assign(Math.max(table2000Row[1].get() - table3000Row[2].get(), 0));
        table3000Row[4].assign(isC1Negative ? 0 : (Math.min(table2000Row[1].get(), table3000Row[2].get())));
      });
    });
    //part 3 calculations - without tables
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.sumBucketValues('382', ['370', '380']);
      calcUtils.min('390', ['382', '385']); // 385 from table
      calcUtils.sumBucketValues('400', ['360', '390']); //360 from table
    });

    //part 4 calcs
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.equals('401', '350');
      calcUtils.equals('405', '380');
      calcUtils.sumBucketValues('409', ['401', '405']);
      calcUtils.equals('1415', '400');
      calcUtils.subtract('450', '409', '1415');
    });

    //part 5 calcs
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('501', 'T2J', '300');
      calcUtils.getGlobalValue('502', 'T2S1', '406');
      calcUtils.sumBucketValues('503', ['501', '502']);
      calcUtils.equals('505', '503');
      calcUtils.getGlobalValue('510', 'T2S1', '113');
      field('115').assign(
          field('032').get() - field('042').get() - field('052').get() - field('082').get());
      calcUtils.sumBucketValues('521', ['500', '510', '115', '520']);
      calcUtils.equals('522', '521');
      calcUtils.subtract('525', '505', '522', true);
      calcUtils.equals('528', '450');
      // calc for 530
      field('541').assign(field('6000').total(1).get());
      calcUtils.sumBucketValues('535', ['528', '530', '540', '541']);
      calcUtils.equals('542', '535');
      calcUtils.equals('543', '625');
      field('545').assign(Math.max(field('525').get() - field('542').get() + field('543').get(), 0));
    });

  })
}());
