(function() {
  'use strict';

  wpw.tax.create.formData('t2s7', {
    formInfo: {
      abbreviation: 'T2S7',
      title: 'Aggregate Investment Income and Active Business Income <br> (2016 and later tax years)',
      schedule: 'Schedule 7',
      highlightFieldsets: true,
      headerImage: 'cw',
      dynamicFormWidth: true,
      code: 'Code 1601',
      formFooterNum: 'T2 SCH 7 E (17)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if you are a Canadian-controlled private corporation (CCPC) to calculate:',
              sublist: [
                'your aggregate investment income and foreign investment income, as defined in subsection 129(4), to determine the refundable portion of Part I tax;',
                'your <b>specified partnership income</b>, if you are a member or <b>designated member</b> of one or more partnerships as defined under subsection 125(7); and',
                'your income from an active business carried on in Canada eligible for the small business deduction including any <b>specified corporate income</b> as defined in subsection 125(7).'
              ]
            },
            {
              label: 'Use this schedule if another CCPC is making an assignment of <b>business limit</b> under subsection 125(3.2) to you.',
              labelClass: 'fullLength'
            },
            {
              label: 'Use this schedule if you are a member of a partnership to assign <b>specified partnership business limit</b> to a <b>designated member</b> under subsection 125(8).\n <br><b>Note</b>: If you are a corporation that is not a CCPC, <b>only</b> complete Table 1 (columns A1, B1, C1, G1 and J1) and Table 3 to make this assignment',
              labelClass: 'fullLength'
            },
            {
              label: 'All legislative references are to the <i>Income Tax Act</i>',
              labelClass: 'fullLength'
            },
            {
              label: 'For more information, see <b>Small Business Deduction</b> and <b>Refundable Portion of Part I Tax</b> in Guide T4012, <i>T2 Corporation – Income Tax Guide.</i>',
              labelClass: 'fullLength'
            },
            {
              label: 'Use the note links to see the notes in a pop-up or see the notes at the end of the form.',
              labelClass: 'fullLength'
            }
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        header: 'Taxable dividend deductible amounts',
        rows: [
          {
            type: 'table',
            num: '399'
          },
          {
            label: '<b>Less:</b> Related expenses',
            labelClass: 'tabbed'
          },
          {
            type: 'table',
            num: '449'
          },
          {
            type: 'table',
            num: '559'
          }
        ]
      },
      {
        'header': 'Part 1 - Aggregate investment income',
        'forceBreakAfter': true,
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Aggregate investment income is all <b>world</b> source income',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible portion of taxable capital gains for the year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '002',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '002'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible portion of allowable capital losses for the year <br>(including allowable business investment losses)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '012',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '012'
                }
              },
              null
            ]
          },
          {
            'label': 'Net capital losses of previous years claimed on line 332 on the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '022',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '022'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 012 <b>plus</b> line 022)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '025'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '027'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Line 002 <b>minus</b> amount A (if negative, enter "0")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '030'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total income from property (include income from a specified investment business carried on in Canada other than income from a source outside Canada)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '032',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '032'
                }
              },
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Exempt income',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '042',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '042'
                }
              },
              null,
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amounts received from AgriInvest Fund No. 2 that were included in computing the corporation\'s income for the year',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '052',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '052'
                }
              },
              null,
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable dividends deductible<br>(total of column F on Schedule 3 minus related expenses)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '062',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '062'
                }
              },
              null,
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Business income from an interest in a trust that is considered property income under paragraph 108(5)(a)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '072',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '072'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> lines 042, 052, 062 and 072)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '073'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '074'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 032 <b>minus</b> amount C)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '075'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '076'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Amount B <b>plus</b> amount D ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '080'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total losses from property (include losses from a specified investment business carried on in Canada other than a loss from a source outside Canada)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '082',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '082'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount E <b>minus</b> line 082 (if negative, enter "0") (enter on line 440 of the T2 return) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '092',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '092'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - Foreign Investment Income',
        'rows': [
          {
            'label': 'Foreign investment income is all income from sources <b>outside Canada</b>.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible portion of taxable capital gains for the year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '001',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '001'
                }
              }
            ]
          },
          {
            'label': 'Eligible portion of allowable capital losses for the year (including allowable business investment losses)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '009',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '009'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 001 <b>minus</b> line 009) (if negative, enter "0")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '010'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total income from property from a source <b>outside Canada</b> (net of related expenses)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '019',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '019'
                }
              },
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Exempt income',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '029',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '029'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Taxable dividends deductible<br>(total of column F on Schedule 3 <b>minus</b> related expenses)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '049',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '049'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Business income from an interest in a trust that is considered property income under paragraph 108(5)(a)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '059',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '059'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> lines 029, 049, and 059)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '060'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '063'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 019 <b>minus</b> amount G)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '064'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '065'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': 'Amount F <b>plus</b> I amount H',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '066'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Total losses from property from a source <b>outside Canada</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '069',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '069'
                }
              }
            ]
          },
          {
            'label': 'Amount I <b>minus</b> line 069 (if negative, enter "0") (enter on line 445 of the T2 return)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '079',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '079'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 3 - Specified partnership income',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '3000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s losses for the year from an active business carried on in Canada<br>(other than as a member of a partnership) - enter as a positive amount',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '370',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '370'
                }
              },
              null
            ]
          },
          {
            'label': 'Specified partnership loss of the corporation for the year - enter as a positive amount<br>(total of all negative amounts in column F1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '380',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '380'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 370 <b>plus</b> line 380)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '382'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'J'
                }
              }
            ]
          },
          {
            'label': 'Amount at line 385 or amount J, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '390',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '390'
                }
              }
            ]
          },
          {
            'label': '<b>Specified partnership income</b> (line 360 <b>plus</b> line 390)<br>(enter at amount N in Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '400',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '400'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: 'Tables 2 and 3 are used to make an assignment of <b>specified partnership business limit</b> ' +
            'under subsection 125(8). A person that is a member of a partnership can make an assignment of <b>specified' +
            ' partnership business limit</b> under subsection 125(8) to a designated member for any tax year that <b>starts' +
            ' after</b> March 21, 2016. Also, that person can make an assignment for its tax year that <b>starts' +
            ' before</b> March 22, 2016 and <b>ends after</b> March 21, 2016 if the tax year of the <b>designated member' +
            ' starts after</b> March 21, 2016.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'label': 'Select type of partnership member:',
            'num': '2001',
            'inputType': 'dropdown',
            'options': [
              {
                'option': '1',
                'value': '1'
              },
              {
                'option': '2',
                'value': '2'
              }
            ],
            'init': '1',
            'description': 'Type of partnership member'
          },
          {
            'label': 'Type 1: You are a designated member and <b>receiving</b> specified partnership business limit from a person that is a member of the partnership? If so, complete part 2',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Type 2: You a member of the partnership and <b>assigning</b> specified partnership business limit to a designated member? If so, complete part 3',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '4000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '5000'
          }
        ]
      },
      {
        'header': 'Part 4 - Partnership income not eligible for the small business deduction',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s income from active businesses carried on in Canada as a member or designated member of a partnership (after deducting related expenses) - from line 350 in Part 3 (if the net amount is negative, enter "0" on line 450)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '401'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Specified partnership loss (from line 380 in Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '405'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal (amount K <b>plus</b> amount L)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '409'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Specified partnership income (from line 400 in Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1415'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Partnership income not eligible for the small business deduction</b> (amount M <b>minus</b> amount N)<br>(enter at amount V in Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '450',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '450'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 5 - Income from active business carried on in Canada',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Net income for income tax purposes from line 300 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '501'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'O'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Allowable business investment loss from line 406 of Schedule 1',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '502'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'P'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal (amount O <b>plus</b> amount P)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '503'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '505'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'Q'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Foreign business income after deducting related expenses <sup>note 11</sup>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '500',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '500'
                }
              },
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable capital gains from line 113 of Schedule 1',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '510'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'R'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Net property income (line 032 <sup>note 12</sup> <b>minus</b> the total of lines 042, 052 and 082 <sup>note 11</sup> in Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '115'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'S'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Personal services business income and other income after deducting related expenses <sup>note 11</sup>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '520',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '520'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> line 500, amount R, amount S and line 520)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '521'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '522'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'T'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Net amount (amount Q <b>minus</b> amount T)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '525'
                }
              }
            ],
            'indicator': 'U'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Partnership income not eligible for the small business deduction (line 450 in Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '528'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'V'
                }
              }
            ]
          },
          {
            'label': 'Partnership income allocated to your corporation under subsection 96(1.1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '530',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '530'
                }
              },
              null
            ]
          },
          {
            'label': 'Income referred to in clause 125(1)(a)(i)(C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '540',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '540'
                }
              },
              null
            ]
          },
          {
            'label': 'Income referred to in clause 125(1)(a)(i)(B) (from line 615 in Part 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '541'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'W'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> amount V, line 530, line 540 and amount W)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '535'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '542'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'X'
          },
          {
            'label': 'Specified corporate income (from line 625 in Part 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '543'
                }
              }
            ],
            'indicator': 'Y'
          },
          {
            'label': '<b>Income from active business carried on in Canada</b> (amount U <b>minus</b> amount X <b>plus</b> amount Y)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '545'
                }
              }
            ],
            'indicator': 'Z'
          },
          {
            'label': '(enter amount Z on line 400 of the T2 return - if negative, enter "0")',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        header: 'Part 6 – Specified corporate income and assignment under subsection 125(3.2)',
        rows: [
          {
            label: 'Applies to tax years that begin after March 21, 2016.',
            labelClass: 'fullLength bold'
          },
          {
            label: 'A CCPC can also make an assignment of <b>business limit to you for its tax year that <b>starts before</b> March 22, 2016, and <b>ends after</b> March 21, 2016, if your tax year <b>starts after</b> March 21, 2016.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '6000'
          }
        ]
      },
      {
        hideFieldset: true,
        'forceBreakAfter': true,
        rows: [
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {
            label: 'Notes',
            labelClass: 'fullLength bold center'
          },
          {
            type: 'table',
            num: '7000'
          },
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'}
        ]
      }
    ]
  });
})();