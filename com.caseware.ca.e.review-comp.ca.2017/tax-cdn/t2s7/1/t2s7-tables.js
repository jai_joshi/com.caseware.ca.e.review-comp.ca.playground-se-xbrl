(function() {
  var dateTimeConditionArr = [
    {
      switchIf: {
        formId: 'cp',
        fieldId: '063',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '066',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '071',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '072',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '076',
        value: '1'
      }
    }
  ];

  wpw.tax.global.tableCalculations.t2s7 = {
    '399': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '500px',
          cellClass: 'alignLeft',
          type: 'none'
        },
        {
          header: '<b>Canadian</b>',
          cellClass: 'alignRight'
        },
        {
          header: '<b>Foreign</b>',
          cellClass: 'alignRight'
        },
        {
          header: '<b>Total</b>',
          cellClass: 'alignRight'
        },
        {
          colClass: 'small-input-width',
          type: 'none'
        }
      ],
      cells: [
        {'0': {label: 'Taxable dividends from Schedule 3'}}
      ]
    },
    '449': {
      infoTable: true,
      hasTotals: true,
      columns: [
        {
          width: '490px',
          type: 'text'
        },
        {
          width: '10px',
          type: 'none'
        },
        {
          total: true,
          totalNum: '410',
          totalMessage: 'Total Expenses:'
        },
        {
          total: true,
          totalNum: '411'
        },
        {
          total: true,
          disabled: true,
          totalNum: '412'
        }
      ]
    },
    '559': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          width: '500px',
          cellClass: 'alignRight'
        },
        {
          cellClass: 'alignRight'
        },
        {
          cellClass: 'alignRight'
        },
        {
          cellClass: 'alignRight'
        },
        {
          colClass: 'small-input-width',
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {
            label: '<b>Net taxable dividend deductible amounts</b>',
            type: 'none'
          }
        }
      ]
    },
    '1000': {
      repeats: ['2000', '3000'],
      'showNumbering': true,
      'superHeaders': [
        {
          'header': 'Table 1 – Specified partnership income',
          'colspan': '4'
        }
      ],
      'columns': [
        {
          'header': 'A1<br>Partnership name',
          'tn': '200',
          'width': '30%',
          type: 'text'
        },
        {
          'header': 'B1<br>Total income (loss) of partnership from an active business',
          'tn': '300',
          'width': '20%'
        },
        {
          'header': 'C1<br>Corporation\'s share of amount in column B1',
          'tn': '310'
        },
        {
          'header': 'D1<br>Income of the corporation from providing (directly or indirectly) services or property to the partnership ' + 'note 1'.sup(),
          'tn': '311'
        }
      ]
    },
    '2000': {
      'showNumbering': true,
      'fixedRows': true,
      'keepButtonsSpace': true,
      'columns': [
        {
          'header': 'E1<br>Adjustments <br> (add or deduct the prorated amounts calculated under section 34.2 ' +
          'note 2'.sup() +
          ' and deduct expenses the corporation incurred to earn partnership income)',
          'tn': '315',
          'width': '30%'
        },
        {
          'header': 'F1<br>Corporation\'s income(loss) in respect of the partnership' + 'note 3'.sup() +
          '( <b> add </b> columns C1, D1 and E1)',
          'tn': '320',
          'total': true,
          'totalTn': '350',
          'totalNum': '350'
        },
        {
          header: 'Partnership\'s year-start',
          type: 'date',
          dateTimeShowWhen: {
            or: dateTimeConditionArr
          },
          colClass: 'std-input-width'
        },
        {
          header: 'Partnership\'s year-end',
          type: 'date',
          dateTimeShowWhen: {
            or: dateTimeConditionArr
          },
          colClass: 'std-input-width'
        },
        {
          'header': 'G1<br>Number of days in the partnership\'s fiscal period',
          'tn': '325'
        },
        {
          'header': 'H1<br>Prorated business limit ' + 'notes 3 and 4'.sup() + ' (column C1 ÷ column B1) ' +
          'x [$500,000 x (column G1 ÷ 365)] (if column C1 is negative, enter \'0\')',
          'tn': '330'
        }
      ],
      'startTable': '1000',
      hasTotals: true
    },
    "3000": {
      "showNumbering": true,
      "fixedRows": true,
      "keepButtonsSpace": true,
      "columns": [{
        "header": "I1<br>Specified partnership business limit <b>assigned to you</b> (from H2 in table 2) " + "notes 1, 6 and 7".sup(),
        "tn": "335"
      },
        {
          "header": "J1<br>Specified partnership business limit <b>assigned by you</b> (from F3 in table 3) " + "notes 1, 6 and 8".sup(),
          "tn": "336"
        },
        {
          'header': 'K1<br>Specified partnership business limit amount (column H1 <b>plus</b> column I1 <b>minus</b> column J1)',
          'width': '25%'
        },
        {
          'header': 'L1<br>Column F1 <b>minus</b> column K1 (if negative, enter \'0\')',
          'total': true,
          'totalTn': '385',
          'totalNum': '385'
        },
        {
          'header': 'M1<br>Lesser of columns F1 and K1 (if column F1 is negative, enter \'0\') ' + 'note 5'.sup(),
          'total': true,
          tn: '340',
          'totalTn': '360',
          'totalNum': '360'
        }
      ],
      'startTable': '1000',
      hasTotals: true
    },
    '4000': {
      infoTable: true,
      scrollx: true,
      'superHeaders': [
        {
          'header': 'Table 2 – A member is assigning to you specified partnership business limit under subsection 125(8)',
          'colspan': '8'
        }
      ],
      'columns': [
        {
          'header': 'A2<br>Partnership name',
          'tn': '405',
          type: 'text'
        },
        {
          'header': 'B2<br>Name of the member',
          'tn': '406',
          type: 'text'
        },
        {
          'header': 'C2<br>Business number of the member (if applicable)',
          'tn': '410',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          'header': 'D2<br>Social insurance number of the member (if applicable)',
          'tn': '411',
          type: 'custom',
          format: ['{N9}'],
          validate: {check: 'mod10'}
        },
        {
          'header': 'E2<br>Trust account number of the member (if applicable)',
          'tn': '412',
          type: 'custom',
          format: ['TN{N7}', 'NA']
        },
        {
          'header': 'F2<br>Tax year start of the member<br>(mm/dd/yyyy)',
          'tn': '415',
          type: 'date',
          dateTimeShowWhen: {
            or: dateTimeConditionArr
          }
        },
        {
          'header': 'G2<br>Tax year-end of the member <br>(mm/dd/yyyy)',
          'tn': '416',
          type: 'date',
          dateTimeShowWhen: {
            or: dateTimeConditionArr
          }
        },
        {
          'header': 'H2<br>Specified partnership business limit assigned to you by the member ' + 'note 9'.sup(),
          'tn': '420'
        }
      ]
    },
    '5000': {
      infoTable: true,
      scrollx: true,
      'keepButtonsSpace': true,
      'superHeaders': [
        {
          'header': 'Table 3 – You are assigning to a designated member (CCPC) specified partnership business limit under subsection ',
          'colspan': '6'
        }
      ],
      'columns': [
        {
          'header': 'A3<br>Partnership name',
          'tn': '425',
          type: 'text'
        },
        {
          'header': 'B3<br>Name of the designated member',
          'tn': '426',
          type: 'text'
        },
        {
          'header': 'C3<br>Business number of the designated member',
          'tn': '430',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          'header': 'D3<br>Tax year start of the designated member<br>(mm/dd/yyyy)',
          'tn': '435',
          dateTimeShowWhen: {
            or: dateTimeConditionArr
          },
          type: 'date'
        },
        {
          'header': 'E3<br>Tax year-end of the designated member<br>(mm/dd/yyyy)',
          'tn': '436',
          dateTimeShowWhen: {
            or: dateTimeConditionArr
          },
          type: 'date'
        },
        {
          'header': 'F3<br>Specified partnership business limit assigned by you to the designated member ' + 'note 10'.sup(),
          'tn': '440'
        }
      ]
    },
    '6000': {
      infoTable: true,
      'columns': [
        {
          'header': 'AA<br>Business number of the corporation',
          'tn': '600',
          'type': 'custom',
          'format': ['{N9}RC{N4}', 'NR']
        },
        {
          'header': 'BB<br>Income described under clause 125(1)(a)(i)(B) <b>from</b> the corporation identified in column AA ' + 'note 13'.sup(),
          'tn': '610',
          'total': true,
          'totalTn': '615',
          'totalNum': '615'
        },
        {
          'header': 'CC<br>Business limit assigned <b>from</b> the corporation identified in column AA ' + 'note 14'.sup(),
          'tn': '620',
          'total': true,
          'totalTn': '625',
          'totalNum': '625'
        }
      ],
      hasTotals: true
    },
    '7000': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {'width': '10%', 'labelClass': 'fulllLength bold', 'type': 'none', verticalAlign: 'top'},
        {'width': '90%', 'labelClass': 'fullLength', 'type': 'none'}
      ],
      'cells': [
        {
          0: {label: 'Note 1'},
          1: {label: 'Applies to tax years that <b>begin after</b> March 21, 2016. For tax years beginning before March 22, 2016 leave blank.'}
        },
        {
          0: {label: 'Note 2'},
          1: {
            label: 'Do <b>not</b> include expenses that were deducted in computing the income of the corporation in column D1.<br><br>' +
            'In general, amounts included under subsections 34.2(2) and 34.2(3) or claimed under subsection 34.2(4) are deemed to have the <b>same character</b> and be in the <b>same proportions</b> as the partnership income they relate to. Amounts claimed under subsection 34.2(11) and included under subsection 34.2(12) are deemed to have the <b>same character</b> and be in the <b>same proportions</b> as the qualifying transitional income. For example, if a corporation receives $100,000 of partnership income for the partnership\'s fiscal period ending in its tax year, and that income is made up of $40,000 of active business income, $30,000 of income from property, and $30,000 as a taxable capital gain, the corporation\'s adjusted stub period accrual (ASPA) in respect of the partnership would be 40% active business income, 30% property income, and 30% taxable capital gains. Add or deduct only the portion of the following amounts that are characterized as <b>active business income</b> in accordance with subsection 34.2(5):<br><br>' +
            '<b>Add:</b><br>' +
            '– the ASPA under subsection 34.2(2) (column 4 of Schedule 73)<br>' +
            '– the income inclusion for a new corporate member of a partnership under subsection 34.2(3) (column 6 of Schedule 73)<br>' +
            '– the previous-year transitional reserve under subsection 34.2(12) (column 12 of Schedule 73)<br><br>' +
            '<b>Deduct:</b><br>' +
            '– the previous-year ASPA under subsection 34.2(4) (column 5 of Schedule 73)<br>' +
            '– the previous-year income inclusion for a new corporate member of a partnership under subsection 34.2(4) (column 7 of Schedule 73)<br>' +
            '– the current-year transitional reserve under subsection 34.2(11) (column 11 of Schedule 73)<br><br>'
          }
        },
        {
          0: {label: 'Note 3'},
          1: {label: 'When a partnership carries on more than one business, one of which generates income and another of which realizes a loss, the loss is <b>not</b> netted against the partnership\'s income when calculating the prorated business limit (column H1). Enter on line 380 the total of all losses from column F1.'}
        },
        {
          0: {label: 'Note 4'},
          1: {label: 'For tax years that begin after March 21, 2016, <b>if</b> you are a <b><b>designated member</b></b> of the partnership, enter <q>0</q>.'}
        },
        {
          0: {label: 'Note 5'},
          1: {
            label: 'For tax years that begin after March 21, 2016, you must enter \'0\' <b>if</b> the partnership provides services or property to either:<br><br>(A) a private corporation (directly or indirectly in any manner whatever) in the year, if:'
          }
        },
        {
          1: {
            cellClass: 'indent',
            label: '• you (or one of your shareholders) or a person that does <b>not</b> deal at arm\'s length with you (or one of your shareholders) holds a direct or indirect interest in the private corporation, and <br>• it is not the case that all or substantially all of the partnership\'s income for the year from an active business is from providing services or property to'
          }
        },
        {
          1: {
            cellClass: 'indent-2',
            label: '– persons (other than the private corporation) that deal at arm\'s length with the partnership and each person that holds a direct or indirect interest in the partnership, or <br>– partnerships with which the partnership deals at arm\'s length, other than a partnership in which a person that does not deal at arm\'s length with you holds a direct or indirect interest, or'
          }
        },
        {
          1: {label: '(B) a particular partnership (directly or indirectly in any manner whatever) in the year, if:'}
        },
        {
          1: {
            cellClass: 'indent',
            label: '• you (or one of your shareholders) do <b>not</b> deal at arm\'s length with the particular partnership or a person that holds a direct or indirect interest in the particular partnership, and <br>• it is not the case that all or substantially all of the partnership\'s income for the year from an active business is from providing services or property to'
          }
        },
        {
          1: {
            cellClass: 'indent-2',
            label: '– persons that deal at arm\'s length with the partnership and each person that holds a direct or indirect interest in the partnership, or <br>– partnerships (other than the particular partnership) with which the partnership deals at arm\'s length, other than a partnership in which a person that does not deal at arm\'s length with you holds a direct or indirect interest'
          }
        },
        {
          0: {label: 'Note 6'},
          1: {label: 'A person that is a member of a partnership can make an assignment of <b>specified partnership business limit</b> under subsection 125(8) to a <b>designated member</b> for any tax year that <b>starts after</b> March 21, 2016. Also, that person can make an assignment for its tax year that <b>starts before</b> March 22, 2016 and <b>ends after</b> March 21, 2016 if the tax year of the <b>designated member starts after</b> March 21, 2016.'}
        }, {
          0: {label: 'Note 7'},
          1: {label: 'If you are a <b>designated member</b> receiving an assignment of <b>specified partnership business limit</b>, complete Table 2 to determine the amounts to enter in Table 1 column I1.'}
        },
        {
          0: {label: 'Note 8'},
          1: {label: '8 If you are a corporation that is a <b>member</b> of the partnership and you are assigning <b>specified partnership business limit</b>, complete Table 3 to determine the amounts to enter in Table 1 column J1.'}
        },
        {
          0: {label: 'Note 9'},
          1: {label: 'Add the amounts in column H2 that are for the same partnership and enter it in Table 1 column I1, in the row of the applicable partnership'}
        },
        {
          0: {label: 'Note 10'},
          1: {label: 'Add the amounts in column F3 that are for the same partnership and enter it in Table 1 column J1, in the row of the applicable partnership. This amount <b>cannot</b> be higher than the amount of prorated business limit you would otherwise be entitled to in Table 1 column H1 for that partnership. '}
        },
        {
          0: {label: 'Note 11'},
          1: {label: 'If negative, enter amount in brackets, and <b>add</b> instead of subtracting.'}
        },
        {
          0: {label: 'Note 12'},
          1: {label: 'Net of related expenses.'}
        },
        {
          0: {label: 'Note 13'},
          1: {
            label: 'This amount is [as defined in subsection 125(7) <b>specified corporate income</b> (a)(i)] the total of all amounts, each of which is your income from an active business for the year from providing services or property to a private corporation (directly or indirectly, in any manner whatever) if <br><br>' +
            '(A) at any time in the year, you (or one of your shareholders) or a person that does not deal at arm\'s length with you (or one of your shareholders) holds a direct or indirect interest in the private corporation, and<br><br>' +
            '(B) it is not the case that all or substantially all of your income for the year from an active business is from providing services or property to'
          }
        },
        {
          1: {
            cellClass: 'indent',
            label: '(I) persons (other than the private corporation) with which you deal at arm\'s length, or<br>' +
            '(II) partnerships with which you deal at arm\'s length, other than a partnership in which a person that does not deal at arm\'s length with you holds a direct or indirect interest.'
          }
        },
        {
          1: {label: 'Do <b>not</b> include income from an associated corporation if the conditions described in subsection 125(10) are met.'}
        },
        {
          0: {label: 'Note 14'},
          1: {label: 'The amount of business limit that a CCPC can assign to you cannot be greater than the amount in column BB that is from providing services or property <b>directly</b> to that CCPC. If there is an amount included in column BB that is deductible by that CCPC in respect of the amount of its income referred to in clause 125(1)(a)(i)(A) or (B) for its tax year, you need to deduct it from column BB for the purpose of determining the amount that can be assigned to you.'}
        }
      ]
    }
  };
})();
