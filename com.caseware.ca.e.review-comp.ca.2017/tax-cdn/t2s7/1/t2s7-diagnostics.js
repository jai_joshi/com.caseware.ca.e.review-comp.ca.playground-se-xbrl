(function() {
  wpw.tax.create.diagnostics('t2s7', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S6'), forEach.row('4000', forEach.bnCheckCol(2, true))));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S6'), forEach.row('5000', forEach.bnCheckCol(2, true))));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S6'), forEach.row('6000', forEach.bnCheckCol(0, true))));

    diagUtils.diagnostic('0070001', common.prereq(common.and(
        common.requireFiled('T2S7'),
        function(tools) {
          return tools.field('t2j.207').isChecked() && tools.field('t2j.430').isNonZero() && common.requireNotFiled('T2S16');
        }), function(tools) {
      return tools.requireOne(tools.list(['002', '032', '082', '400', '450', '500', '520', '530', '540', '615', '625']), 'isNonZero');
    }));

    diagUtils.diagnostic('0070002', common.prereq(common.and(
        common.requireFiled('T2S6'),
        common.check(['t2j.450', 't2j.445'], 'isNonZero', true)),
        function(tools) {
          return tools.requireOne(tools.list(['001', '019']), 'isNonZero');
        }));

    diagUtils.diagnostic('0070003', common.prereq(common.and(
        common.requireFiled('T2S6'),
        common.check(['t2j.450'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['002', '032']), 'isNonZero');
        }));

    diagUtils.diagnostic('0070004', common.prereq(common.and(
        common.requireFiled('T2S6'),
        function(tools) {
          return tools.field('t2j.430').isNonZero() &&
              tools.checkMethod(tools.list(['380', '385', '400', '450']), 'isNonZero');
        }), function(tools) {
      var table = tools.mergeTables(tools.field('1000'), tools.field('2000'));
      table = tools.mergeTables(table, tools.field('3000'));
      return tools.checkAll(table.getRows(), function(row) {
        return tools.requireAll([row[0], row[1], row[2], row[5], row[8], row[9], row[14]], 'isNonZero') ||
            tools.requireAll([row[0], row[3], row[5], row[14]], 'isNonZero')
      });
    }));

    //0070007 is implemented in s53 diagnostics

    diagUtils.diagnostic('0070008', common.prereq(common.requireFiled('T2S6'),
        function(tools) {
          var table = tools.mergeTables(tools.field('3000'), tools.field('4000'));
          return tools.checkAll(table.getRows(), function(row) {
            if (row[0].isNonZero() || row[12].isNonZero())
              return tools.requireAll([row[0], row[5], row[6], row[10], row[11], row[12]], 'isNonZero') &&
                  tools.requireOne([row[7], row[8], row[9]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0070009', common.prereq(common.requireFiled('T2S6'),
        function(tools) {
          var table = tools.mergeTables(tools.field('3000'), tools.field('5000'));
          return tools.checkAll(table.getRows(), function(row) {
            if (row[1].isNonZero() || row[10].isNonZero())
              return tools.requireAll([row[1], row[5], row[6], row[7], row[8], row[9], row[10]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0070010', common.prereq(common.requireFiled('T2S6'),
        function(tools) {
          var table = tools.field('6000');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[0].isNonZero())
              return tools.requireOne([row[1], row[2]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0070010', {
      label: 'There is an entry in column 007610, but for the same row there is no corresponding entry in column 007600.'
    }, common.prereq(common.requireFiled('T2S6'),
        function(tools) {
          var table = tools.field('6000');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[1].isNonZero())
              return row[0].require('isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0070010', {
      label: 'There is an entry in column 007620, but for the same row there is no corresponding entry in columns 007600 and/or 007610.'
    }, common.prereq(common.requireFiled('T2S6'),
        function(tools) {
          var table = tools.field('6000');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[2].isNonZero())
              return tools.requireAll([row[0], row[1]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0071023', common.prereq(common.requireFiled('T2S6'),
        function(tools) {
          var table = tools.field('1000');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[1].isFilled() && row[2].isFilled())
              return row[2].require(function() {
                return this.get() < row[1].get();
              });
            else return true;
          });
        }));

    diagUtils.diagnostic('007.310', common.prereq(common.and(
        common.check(['310'], 'isNonZero'),
        common.requireFiled('T2S6')),
        function(tools) {
          return tools.field('310').require(function() {
            return this.get() < tools.field('300').get()
          })
        }));

  });
})();
