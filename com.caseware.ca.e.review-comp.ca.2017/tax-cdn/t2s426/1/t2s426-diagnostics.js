(function() {
  wpw.tax.create.diagnostics('t2s426', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('4260001', common.prereq(common.and(
        common.requireFiled('T2S426'),
        common.check(['t2s5.660'], 'isNonZero')),
        function(tools) {
          var table = tools.field('1000');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireAll([row[0], row[3], row[4]], 'isNonZero') ||
                tools.requireOne(tools.list(['100', '105', '110', '130']), 'isNonZero');
          });
        }));

    diagUtils.diagnostic('4260002', common.prereq(common.requireFiled('T2S426'),
        function(tools) {
          var table = tools.field('1000');
          return tools.checkAll(table.getRows(), function(row) {
            var columns = [row[0], row[3], row[4]];
            if (tools.checkMethod(columns, 'isNonZero'))
              return tools.requireAll(columns, 'isNonZero');
            else return true;
          });
        }));

  });
})();
