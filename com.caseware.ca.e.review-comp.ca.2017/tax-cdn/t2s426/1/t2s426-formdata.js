(function() {
  'use strict';

  wpw.tax.global.formData.t2s426 = {
    formInfo: {
      abbreviation: 'T2S426',
      title: 'British Columbia Manufacturing and Processing Tax Credit',

      //subTitle: '(2000 and later tax years)',
      schedule: 'Schedule 426',
      //code: '0101',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if you are a qualifying corporation that has acquired ' +
              'BC qualified property after March 31, 2000 and before July 31, 2001, for use ' +
              'in British Columbia, and you want to do any of the following: ',
              sublist: [
                'calculate the credit;',
                'claim the credit to reduce British Columbia income tax otherwise payable ' +
                'in the current taxation year;',
                'request a carryback to reduce British Columbia income tax payable in any ' +
                'of the three preceding taxation years;',
                'renounce the current-year credit in whole or in part. The renouncement must ' +
                'be made in the year the credit was earned or acquired, and filed on or ' +
                'before the filing due date of the federal<i> T2 Corporation Income Tax Return.</i>'
              ]
            },
            {
              label: 'A corporation is <b>not</b> a qualifying corporation and is therefore<b> not eligible</b> ' +
              'to claim the credit if any of the following apply: ',
              sublist: [//TODo: sublist of subslist
                'the corporation did not maintain a permanent establishment (as defined in ' +
                'Regulation 400 of the federal<i> Income Tax Regulations</i>) in ' +
                'British Columbia at any time in the taxation year.',
                'the corporation is:',
                '<space><space><space>a non-resident-owned investment corporation, or at any time of the year ' +
                'was controlled directly or indirectly in any manner whatever by a ' +
                'non-resident-owned investment corporation; ',
                '<space><space><space>exempt from tax under subsection 149(1) of the federal <i>Income Tax Act</i>, or ' +
                'at any time of the year, was controlled directly or indirectly in any manner ' +
                'whatever by one or more persons, all or part of whose income is exempt from tax; or ',
                '<space><space><space>sof a type or class of corporation prescribed by regulation.',
                'the corporation was at any time in the year:',
                '<space><space><space>a small business venture capital corporation registered under section 3 ' +
                'of the<i> Small Business Venture Capital Act</i>; or ',
                '<space><space><space>an employee venture capital corporation registered under section 8 of ' +
                'the <i>Employee Investment Act.</i>'
              ]
            },
            {
              label: 'If the corporation has claimed the two-year income tax holiday for new ' +
              'small businesses under section 17 of the<i> Income Tax Act (British Columbia)</i> in ' +
              'the taxation year, it will not be allowed to calculate the credit for the same taxation year.'
            },
            {
              label: 'BC qualified property consists of buildings, machinery, and equipment ' +
              'that is prescribed for the purposes of the definition of qualified property in ' +
              'subsection 127(9) of the federal Act, that was not acquired for use or lease, ' +
              'for any purpose before it was acquired by the corporation and that is to be ' +
              'used by the corporation in British Columbia primarily for the purpose of ' +
              'manufacturing or processing of goods for sale or lease. Property leased ' +
              'by the corporation to a related qualifying corporation may also qualify for ' +
              'the credit. Manufacturing or processing is defined in subsection 125.1(3) ' +
              'of the federal Act. BC qualified property has to be available for use (as ' +
              'defined in subsections 13(27) and 13(28) of the federal Act) before it is ' +
              'eligible for the credit. '
            },
            {
              label: 'BC qualified property cannot include excluded property. Excluded property ' +
              'is property acquired by a qualifying corporation in a taxation year in the ' +
              'course of earning income in the taxation year if any of the income is ' +
              'exempt income (as defined in subsection 248(1) of the federal Act), ' +
              'or is exempt from tax under Part I of the federal Act. '
            },
            {
              label: 'Capital cost of BC qualified property must be identified on this ' +
              'schedule and filed with the Canada Customs and Revenue Agency no later ' +
              'than 18 months after the end of the taxation year in which the property was acquired. '
            },
            {
              label: 'Capital cost of BC qualified property must be calculated without any ' +
              'amount for cost of borrowing, (section 21 of the federal Act) without applying ' +
              'capital cost of certain property, (subsections 13(7.1) and 13(7.4) of the ' +
              'federal Act) and by subtracting amounts of government assistance and ' +
              'non-government assistance (as defined in subsection 104(1) of the ' +
              '<i>Income Tax Act (British Columbia)</i>).'
            },
            {
              label: 'The credit may be carried forward ten years and carried back three ' +
              'years. You can carry the tax credit back to a taxation year-ending before April 1, 2000.'
            },
            {
              label: 'Use this schedule to show the credit allocated from a partnership. You can ' +
              'also use this schedule to show a credit transfer following an amalgamation or ' +
              'windup of a subsidiary as described under subsections 87(1) and 88(1) of the federal Act. '
            },
            {
              label: 'File one completed copy of this schedule with your<i> T2 Corporation Income Tax Return</i>'
            }
          ]
        }
      ],
      category: 'British Columbia Forms'
    },
    sections: [
      {
        'rows': [
          {
            'label': '<i><b>Freedom of Information and Protection of Privacy Act </i></b><br>The personal information requested on this form is collected under the authority of and used for the purpose of administering the <i><i>Income Tax Act</i> (British Columbia)</i>. Questions about the collection or use of this information can be directed to the Income Taxation Branch at 250 387-3332, PO Box 9434, Stn Prov Gov\'t, Victoria BC V8W 9V3. ',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 1 - Qualified property (acquired in current taxation year) eligible for the credit',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Please open this link for detailed CCA classes ',
            'labelClass': 'bold fullLength',
            'labelWidth': '55%',
            'type': 'infoField',
            'inputType': 'link',
            'link': 'http://laws-lois.justice.gc.ca/eng/regulations/C.R.C.%2C_c._945/page-25.html#h-106'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Subtotal of capital cost</b>',
            'labelClass': 'text-right ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed alignRight',
            'columns': [
              {
                'input': {
                  'num': '091'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': '<b>Add</b>: amount of assistance repaid',
            'labelClass': 'text-right ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed alignRight',
            'columns': [
              {
                'input': {
                  'num': '100',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': '<b>Total capital cost </b>(amount A plus amount B)',
            'labelClass': 'text-right ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed alignRight',
            'columns': [
              {
                'input': {
                  'num': '101',
                  'validate': {
                    'or': [
                      {
                        'length': {
                          'min': '1',
                          'max': '13'
                        }
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ],
            'indicator': 'C'
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of total credit available and credit available for carryforward',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit at end of preceding taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '102'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Deduct:</b> Credit expired after ten taxation years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit at beginning of taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '106',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Add:</b>'
          },
          {
            'label': 'Credit transferred on amalgamation or windup of subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              },
              null
            ]
          },
          {
            'type': 'table',
            'num': '1100'
          },
          {
            'label': 'Credit allocated from a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal',
            'labelClass': ' text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '131'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '132'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '<b>Total credit available</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '133'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Deduct:</b>'
          },
          {
            'label': 'Credit renounced ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '150'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit claimed in the current year (enter on line 660 in Part 2 of Schedule 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit carried back to preceding taxation year(s) (complete Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '161'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'label': 'Subtotal',
            'labelClass': ' text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '162'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '163'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '<b>Closing balance </b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 3 - Request for carryback of credit',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1200'
          },
          {
            'label': '<b>Total</b> (enter on line E in Part 2)',
            'labelClass': ' text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed alignRight',
            'columns': [
              {
                'input': {
                  'num': '904',
                  'disabled': true
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 4 - Analysis of credit available for carryforward by year of origin',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2000'
          },
          {
            'type': 'table',
            'num': '2010'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Total</b> (equals line 200 in Part 2)',
            'labelClass': ' text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed alignRight',
            'columns': [
              {
                'input': {
                  'num': '1062'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'British Columbia Manufacturing And Processing Tax Credit Workchart Summary',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2030'
          }
        ]
      }
    ]
  };
})();
