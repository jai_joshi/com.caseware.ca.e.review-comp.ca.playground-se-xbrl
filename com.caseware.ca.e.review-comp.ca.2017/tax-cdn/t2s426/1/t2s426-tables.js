(function() {

  wpw.tax.create.tables('t2s426', {
    '1000': {
      'showNumbering': true,
      hasTotals: true,
      'maxLoop': 100000,
      'columns': [
        {
          'header': 'CCA Class',
          cellClass: 'alignCenter',
          type: 'selector',
          'width': '109px',
          tn: '101',
          num: '101',
          selectorOptions: {
            title: 'CCA Classes',
            items: wpw.tax.codes.ccaClasses,
            hideKeys: true
          }
        },
        {
          'header': 'Description of BC qualified property',
          'num': '010',
          'disabled': true
        },
        {
          'header': 'Other description',
          'num': '011'
        },
        {
          'header': 'Acquisition date',
          'num': '102',
          'tn': '102',
          'width': '155px',
          'type': 'date'
        },
        {
          'header': 'Capital cost',
          'num': '103', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          'tn': '103',
          colClass: 'std-input-width',
          'total': true,
          'totalNum': '090'
        }]
    },
    '1100': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          'width': '20px',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          'width': '45px',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          'width': '158px',
          'type': 'none'
        }],
      'cells': [
        {
          '0': {
            'label': 'Current year credit earned: amount C from above'
          },
          '1': {
            'num': '111'
          },
          '2': {
            'label': ' x ',
            cellClass: 'alignCenter'
          },
          '3': {
            'num': '112',
            'disabled': true
          },
          '4': {
            'label': ' %= ',
            cellClass: 'alignCenter'
          },
          '5': {
            'tn': '120'
          },
          '6': {
            'num': '120'
, "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          }
        }]
    },
    '1200': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          'width': '190px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        }],
      'cells': [
        {
          '0': {
            'label': '1st preceding taxation year'
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '901'
          },
          '5': {
            'num': '901'
, "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '2nd preceding taxation year'
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '902'
          },
          '5': {
            'num': '902'
, "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '3rd preceding taxation year'
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '903'
          },
          '5': {
            'num': '903', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }]
    },
    '2000': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        }],
      'cells': [
        {
          '0': {
            'label': 'Year of origin<br> (earliest year first)',
            'labelClass': 'bold center'
          },
          '2': {
            'label': 'Credit available',
            'labelClass': 'bold center'
          },
          '4': {
            'label': 'Year of origin <br>(earliest year first)',
            'labelClass': 'bold center'
          },
          '6': {
            'label': 'Credit available',
            'labelClass': 'bold center'
          }
        }]
    },
    '2010': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true,
          'disabled': true
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true,
          'disabled': true
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        }],
      'cells': [
        {
          '0': {},
          '2': {},
          '4': {},
          '6': {}
        },
        {
          '0': {},
          '2': {},
          '4': {},
          '6': {}
        },
        {
          '0': {},
          '2': {},
          '4': {},
          '6': {}
        },
        {
          '0': {},
          '2': {},
          '4': {},
          '6': {}
        },
        {
          '0': {},
          '2': {},
          '4': {},
          '6': {}
        }
      ]
    },
    '2030': {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {type: 'none'},
        {
          colClass: 'std-input-width',
          type: 'date',
          disabled: true,
          header: '<b>Year of origin</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',

          total: true,
          totalNum: '1480',
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',
          total: true,
          totalNum: '1520',

          totalMessage: ' ',
          header: '<b>Current year contribution</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',
          total: true,
          totalNum: '1580',

          totalMessage: ' ',
          header: '<b>Amount transferred</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',  total: true, totalMessage: ' ', totalNum: '1590',
          header: '<b>Amount available to apply</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',  total: true, totalMessage: ' ', disabled: true, totalNum: '1620',
          header: '<b>Applied<b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',  total: true, totalMessage: ' ', disabled: true, totalNum: '1680',
          header: '<b>Balance to carry forward</b>'
        },
        {type: 'none'}
      ],
      cells: [
        {
          '4': {label: '*'},
          '5': {type: 'none'},
          '7': {type: 'none'},
          '9': {type: 'none'},
          '11': {type: 'none'},
          '13': {type: 'none', label: 'N/A'}
        },
        {
          '5': {type: 'none'},
          '14': {label: '**'}
        },
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {
          '3': {type: 'none'},
          '7': {type: 'none'}
        }
      ]
    }
  })
})();
