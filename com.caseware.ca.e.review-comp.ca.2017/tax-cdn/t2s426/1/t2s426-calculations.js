 (function() {
   function returnAmountApplied(limit, available) {
     var applied = 0;
     if (available == 0) {
       applied = available;
     }
     else {
       if (limit > available) {
         applied = available;
       }
       else {
         applied = limit;
       }
     }
     return applied;
   }

   wpw.tax.create.calcBlocks('t2s426', function(calcUtils) {
     calcUtils.calc(function(calcUtils, field) {
       //update from rate table
       calcUtils.getGlobalValue('112', 'ratesBc', '322');
       //Part 1 calcs
       calcUtils.equals('091', '090');
       calcUtils.sumBucketValues('101', ['091', '100']);
       //Part 2 calcs
       calcUtils.equals('102', '1480');
       calcUtils.equals('104', '1450');
       calcUtils.equals('110', '1580');
       calcUtils.subtract('105', '102', '104');
       calcUtils.equals('106', '105');
       calcUtils.equals('111', '101');
       calcUtils.multiply('120', ['111', '112'], 1 / 100);
       calcUtils.sumBucketValues('131', ['110', '120', '130']);
       calcUtils.equals('132', '131');
       calcUtils.sumBucketValues('133', ['106', '132']);
       calcUtils.sumBucketValues('162', ['150', '160', '161']);//TODO: get 160 from S5
       calcUtils.equals('163', '162');
       calcUtils.subtract('200', '133', '163');
       //Part 3 calcs
       calcUtils.sumBucketValues('904', ['901', '902', '903']);
       calcUtils.equals('161', '904');
       //Part 4 calcs

       //TODO: dianogtic num 1062 must equal num 200
     });
     calcUtils.calc(function(calcUtils, field) {
       //to get tax year summary
       var tableArray = [1200, 2010, 2030];
       var taxationYearTable = field('tyh.200');
       tableArray.forEach(function(num) {
         field(num).getRows().forEach(function(row, rowIndex) {
           if (num == 1200) {
             var dateCol = Math.abs(rowIndex - 19);
             row[1].assign(taxationYearTable.cell(dateCol, 6).get());
           }
           else if (num == 2010) {
             row[0].assign(taxationYearTable.getRow(rowIndex + 11)[6].get());
             row[4].assign(taxationYearTable.getRow(rowIndex + 16)[6].get());
           }
           else {
             row[1].assign(taxationYearTable.getRow(rowIndex + 10)[6].get())
           }
         })
       })
     });
     calcUtils.calc(function(calcUtils, field) {
       var limitOnCredit = field('133').get();
       var summaryTable = field('2030');

       summaryTable.getRows().forEach(function(row, rowIndex) {
         if (rowIndex == 0) {
           // to avoid the first row being calculated to total
           row[7].assign(0);
           row[9].assign(0);
           row[11].assign(0);
           row[13].assign(0);
         } else {
           row[9].assign(Math.max(row[3].get() + row[5].get() + row[7].get(), 0));
           row[11].assign(returnAmountApplied(limitOnCredit, row[9].get()));
           row[13].assign(Math.max(row[9].get() - row[11].get(), 0));
           limitOnCredit -= row[11].get();
         }
       });
     });


   })
 })();
