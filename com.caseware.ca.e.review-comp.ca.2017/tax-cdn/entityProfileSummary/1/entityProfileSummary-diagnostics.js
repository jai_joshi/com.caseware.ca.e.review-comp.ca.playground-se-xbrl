(function() {
  wpw.tax.create.diagnostics('entityProfileSummary', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', forEach.row('010', forEach.bnCheckCol(1, true)));
    diagUtils.diagnostic('BN', forEach.row('050', forEach.bnCheckCol(2, true)));
    diagUtils.diagnostic('BN', forEach.row('085', forEach.bnCheckCol(1, true)));
    diagUtils.diagnostic('BN', forEach.row('090', forEach.bnCheckCol(1, true)));

    diagUtils.diagnostic('009.priorYearTaxableCapital', common.prereq(function(tools) {
      var table = tools.field('010');
      //If any column is filled an entry is required at row[4]
      return tools.checkAll(table.getRows(), function(row) {
        return tools.checkMethod([row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9]], 'isNotDefault');
      });
    }, function(tools) {
          var table = tools.field('010');
          return tools.checkAll(table.getRows(), function(row) {
            if(row[4].isDefault())
              return row[4].require('isNotDefault');

            return true;
          });
        }));

    diagUtils.diagnostic('009.taxYearEndCheck', function(tools) {
      var table = tools.field('010');
      return tools.checkAll(table.getRows(), function(row) {
        if (row[3].isNonZero())
          return row[3].require(function() {
            return this.get()['year'] == tools.field('cp.tax_end').getKey('year');
          });
        return true;
      });
    });

    diagUtils.diagnostic('009.datesFilledCheck', function(tools) {
      var table = tools.field('010');
      return tools.checkAll(table.getRows(), function(row) {
        if (row[0].isFilled() || row[1].isFilled())
          return tools.requireAll([row[2], row[3]], 'isNonZero');
        return true;
      });
    });

  });
})();
