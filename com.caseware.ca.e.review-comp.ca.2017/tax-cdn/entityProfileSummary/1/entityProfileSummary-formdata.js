(function() {
  'use strict';

  wpw.tax.create.formData('entityProfileSummary', {
    formInfo: {
      abbreviation: 'entityProfileSummary',
      title: 'Group Entities Data Workchart',
      headerImage: 'cw',
      dynamicFormWidth: true,
      category: 'Workcharts'
    },
    sections: [
      /*
       {
       hideFieldset: true,
       rows: [
       {labelClass: 'fullLength'},
       {
       label: 'Related Entities Data Center<br> (Common form for all engagement in the group)',
       labelClass: 'fullLength titleFont center'
       },
       {
       label: 'Do you want to show all Related Entities Data?',
       labelClass: 'fullLength bold',
       type: 'infoField',
       inputType: 'radio',
       num: '997',
       init: '1'
       }
       ]
       },
       {
       header: 'Related Entities Listing',
       showWhen: {fieldId: '997', compare: {is: '1'}},
       rows: [
       {type: 'table', num: '100'},
       {labelClass: 'fullLength'},
       {type: 'table', num: '200'},
       {labelClass: 'fullLength'},
       {type: 'table', num: '300'},
       {labelClass: 'fullLength'},
       {type: 'table', num: '400'},
       {labelClass: 'fullLength'}
       ]
       },
       {
       header: 'Relationships between the entities in the group',
       showWhen: {fieldId: '997', compare: {is: '1'}},
       rows: [
       {type: 'table', num: '500'},
       {labelClass: 'fullLength'}
       ]
       },
       {
       header: 'Related and associated corporation listing',
       showWhen: {fieldId: '997', compare: {is: '1'}},
       rows: [
       {
       label: 'all Related and associated corporations will be ' +
       'listed below based on information above graphically or table.....WIP',
       labelClass: 'bold'
       },
       {labelClass: 'fullLength'}
       ]
       },
       */
      {
        header: 'Group financial information',
        rows: [
          {
            label: 'Data below will be used by all other related and associated corporations within ' +
            'the group (Manually input OR will be populated from SE)',
            labelClass: 'bold center'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table', num: '010'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Note 1: Enter \'NR\' if the corporation is not registered or does not have a business number.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'}
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {
            label: 'Impact of related and associated entities on this engagement',
            labelClass: 'fullLength titleFont center'
          },
          {
            label: 'Do you want to show Impact of related and associated entities on this engagement?',
            labelClass: 'fullLength bold',
            type: 'infoField',
            inputType: 'radio',
            num: '998',
            init: '1'
          }
        ]
      },
      {
        header: 'Schedule 9 - Related and Associated Corporations',
        showWhen: {fieldId: '998', compare: {is: '1'}},
        rows: [
          {type: 'table', num: '050'},
          {
            label: 'Note 2: Total taxable capital employed in Canada for the year will be used to calculate ' +
            'tn 233 and 234 in T2Jacket',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'}
        ]
      },
      {
        header: 'Schedule 23 - Agreement Among Associated Canadian- Controlled Private ' +
        'Corporation to Allocate the Business Limit',
        showWhen: {fieldId: '998', compare: {is: '1'}},
        rows: [
          {type: 'table', num: '085'},
          {
            type: 'infoField',
            inputType: 'dropdown',
            label: 'Please select option which applicable for current filling corporation',
            num: '081',
            init: '1',
            options: [
              {option: '1', value: '1'},
              {option: '2', value: '2'},
              {option: '3', value: '3'}
            ]
          },
          {
            type: 'description',
            lines: [
              {
                type: 'list', listType: 'asterisk',
                items: [
                  {
                    label: 'Amount D calculation:',
                    labelClass: 'bold',
                    sublist: [
                      'Option 1: If the corporation is not associated with any corporations in both the current and ' +
                      'previous tax years: <br>' +
                      '(total taxable capital employed in Canada for the <b>prior year</b> minus $10,000,000) x 0.225%.',
                      'Option 2: If the corporation is not associated with any corporations in the current tax year, but ' +
                      'was associated in the previous tax year:<br> ' +
                      '(total taxable capital employed in Canada for the <b>current year</b>  minus $10,000,000) ' +
                      'x 0.225%.',
                      'Option 3: If the corporation associated in the current tax year:<br>' +
                      '(total taxable capital employed in Canada for the <b>last tax year-ending in the ' +
                      'previous calendar year</b>  minus $10,000,000) x 0.225%.'
                    ]
                  }
                ]
              }
            ]
          },
          {label: 'Business limit reduction', labelClass: 'bold'},
          {type: 'table', num: '101'},
          {labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {
            label: 'The table below is for CURRENT filling corporation to show the user the optimized percentage of ' +
            'the business limit allocation to maximize the SBD',
            labelClass: 'bold fullLength'
          },
          {type: 'table', num: '086'},
          {labelClass: 'fullLength'}
        ]
      },
      {
        header: 'Schedule 49 - Agreement Among Associated Canadian-Controlled Private Corporations ' +
        'to Allocate the Expenditure Limit',
        showWhen: {fieldId: '998', compare: {is: '1'}},
        rows: [
          {
            'type': 'table', 'num': '090'
          },
          {labelClass: 'fullLength'}
        ]
      },
      // {
      //   header: 'Schedule 306 - NL Capital Tax on Financial Institutions - Agreement among Related Corporations',
      //   showWhen: {
      //     or: [
      //       {fieldId: 'provinces', has: {NL: true}},
      //       {fieldId: 'provinces', has: {XO: true}},
      //       {fieldId: '998', compare: {is: '1'}},
      //       {fieldId: 'CP.750', compare: {is: 'NL'}},
      //       {fieldId: 'CP.750', compare: {is: 'XO'}}
      //     ]
      //   },
      //   rows: [
      //     {
      //       'type': 'table', 'num': '306'
      //     },
      //     {labelClass: 'fullLength'}
      //   ]
      // },
      {
        header: 'Schedule 343 - NS Tax on Large Corporations - Agreement Among Related Corporations',
        showWhen: {
          or: [
            {fieldId: 'provinces', has: {NS: true}},
            {fieldId: 'provinces', has: {NO: true}},
            {fieldId: '998', compare: {is: '1'}},
            {fieldId: 'CP.750', compare: {is: 'NS'}},
            {fieldId: 'CP.750', compare: {is: 'NO'}}
          ]
        },
        rows: [
          {
            'type': 'table', 'num': '343'
          },
          {labelClass: 'fullLength'}
        ]
      },
      {
        header: 'Schedule 511 - ON Corporate Minimum Tax - Total Assets And Revenue for Associated Corporations',
        showWhen: {
          or: [
            {fieldId: 'provinces', has: {ON: true}},
            {fieldId: '998', compare: {is: '1'}},
            {fieldId: 'CP.750', compare: {is: 'ON'}}
          ]
        },
        rows: [
          {
            'type': 'table', 'num': '511'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Note 2: If the associated corporation does not have a tax year that ends in the filing' +
            ' corporation\'s current tax year but was associated with the filing corporation in the previous' +
            ' tax year of the filing corporation, enter the total revenue and total assets from the tax year' +
            ' of the associated corporation that ends in the previous tax year of the filing corporation.',
            labelClass: 'fullLength tabbed'
          },
          { labelClass: 'fullLength'},
          {
            type: 'description',
            lines: [
              {
                type: 'list', listType: 'asterisk', labelClass: 'bold',
                items: [
                  {
                    label: 'Rules for total assets',
                    sublist: [
                      'Report total assets in accordance with generally accepted accounting principles,' +
                      ' adjusted so that consolidation and equity methods are not used.',
                      'Include the associated corporation\'s share of the total assets of partnership(s)' +
                      ' and joint venture(s) but exclude the recorded asset(s) for the investment in' +
                      ' partnerships and joint ventures.',
                      'Exclude unrealized gains and losses on assets that are included in net income for' +
                      ' accounting purposes but not in income for corporate income tax purposes.'
                    ]
                  },
                  {
                    label: 'Rules for total revenue',
                    sublist: [
                      'Report total revenue in accordance with generally accepted accounting principles,' +
                      ' adjusted so that consolidation and equity methods are not used.',
                      'If the associated corporation has 2 or more tax years ending in the filing corporation\'s' +
                      ' tax year, multiply the sum of the total revenue for each of those tax years by 365' +
                      ' and divide by the total number of days in all of those tax years.',
                      'If the associated corporation\'s tax year is less than 51 weeks and is the only tax year' +
                      ' of the associated corporation that ends in the filing corporation\'s tax year, multiply' +
                      ' the associated corporation\'s total revenue by 365 and divide by the number of days in' +
                      ' the associated corporation\'s tax year.',
                      'Include the associated corporation\'s share of the total revenue of partnerships' +
                      ' and joint ventures.',
                      'If the partnership or joint venture has 2 or more fiscal periods ending in the associated' +
                      ' corporation\'s tax year, multiply the sum of the total revenue for each of the fiscal' +
                      ' periods by 365 and divide by the total number of days in all the fiscal periods.'
                    ]

                  }
                ]
              }
            ]
          }
        ]
      },
      {
        header: 'Schedule 566 - ON Innovation Tax Credit',
        showWhen: {
          or: [
            {fieldId: 'provinces', has: {ON: true}},
            {fieldId: '998', compare: {is: '1'}},
            {fieldId: 'CP.750', compare: {is: 'ON'}}
          ]
        },
        rows: [
          {
            'type': 'table', 'num': '566'
          },
          {labelClass: 'fullLength'}
        ]
      },
      {
        header: 'Schedule 568 - ON Business-Research Institute Tax Credit',
        showWhen: {
          or: [
            {fieldId: 'provinces', has: {ON: true}},
            {fieldId: '998', compare: {is: '1'}},
            {fieldId: 'CP.750', compare: {is: 'ON'}}
          ]
        },
        rows: [
          {
            'type': 'table', 'num': '568'
          },
          {labelClass: 'fullLength'}
        ]
      }
    ]
  });
})();
