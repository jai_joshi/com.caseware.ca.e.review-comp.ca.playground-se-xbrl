(function() {

  var mainTableAttrArr = ['corpName', 'corpBn', 'taxStart', 'taxEnd', 'priorTaxableCapital', 'pTaxableCapital',
    'pTaxableIncome', 'cActiveIncome', 'cTaxableCapital', 'cTaxableIncome'];

  //s9 table
  var corpNameCIndex = 0;
  var s9CountryCIndex = 1;
  var s9BnCIndex = 2;
  var s9RelationshipCIndex = 3;
  var s9NumComCIndex = 4;
  var s9PercentComCIndex = 5;
  var s9NumPrefCIndex = 6;
  var s9PercentPrefCIndex = 7;
  var s9BookValueCIndex = 8;
  var s9RelatedCorpCIndex = 9;
  var s9AssociatedCorpCIndex = 10;

  //s23 table
  var s23BnCIndex = 1;
  var s23AssociationCodeCIndex = 2;
  var s23BusinessLimitCIndex = 3;
  var s23PercentageCIndex = 4;
  var s23BusinessAllocatedCIndex = 5;
  var s23priorYearCIndex = 6;
  var s23currentYearCIndex = 7;
  var s23lastYearCIndex = 8;

  //s49 table
  var s49BnCIndex = 1;
  var s49corporationCodeCIndex = 2;
  var s49ExpLimitCIndex = 3;
  var s49ExpAllocatedCIndex = 4;

  //s306 table
  var s306BnCIndex = 1;
  var s306ExpLimitCIndex = 2;
  var s306ExpAllocatedCIndex = 3;

  //s343 table
  var s343BnCIndex = 1;
  var s343ExpLimitCIndex = 2;
  var s343ExpAllocatedCIndex = 3;

  //s511 table
  var s511BnCIndex = 1;
  var s511AssetCIndex = 2;
  var s511RevenueCIndex = 3;

  //s566 table
  var s566BnCIndex = 1;
  var s566CapitalCIndex = 2;
  var s566ExpLimitCIndex = 3;
  var s566ExpAllocatedCIndex = 4;

  //s568 table
  var s568BnCIndex = 1;
  var s568ExpLimitCIndex = 2;
  var s568ExpAllocatedCIndex = 3;

  function disableColsCalc(cell) {
    cell.assign(0);
    cell.disabled(true);
    cell.config('cannotOverride', true);
  }

  function getTotalValFromOtherForm(field, formId, fieldNumArr) {
    var total = 0;
    fieldNumArr.forEach(function(fieldId) {
      total += field((formId + '.' + fieldId).toString()).get();
    });
    return total;
  }

  wpw.tax.create.calcBlocks('entityProfileSummary', function(calcUtils) {
    /*todo: comment out for now until SE provide us linkage data
     //get data from linked repeated forms
     calcUtils.calc(function(calcUtils, field, form) {
     calcUtils.filterLinkedTable('entityProfile', '100', ['101'], function(x) {
     return (x == 1)
     });
     calcUtils.filterLinkedTable('entityProfile', '200', ['101'], function(x) {
     return (x == 2)
     });
     calcUtils.filterLinkedTable('entityProfile', '300', ['101'], function(x) {
     return (x == 3)
     });
     calcUtils.filterLinkedTable('entityProfile', '400', ['101'], function(x) {
     return (x == 4)
     });

     var tableArray = ['100', '200', '300', '400'];
     var entityListOptions = [];

     tableArray.forEach(function(tableNum, tableIndex) {
     if (field(tableNum).size().rows == 0)
     return;
     calcUtils.field(tableNum).getRows().forEach(function(summaryRow, rIndex) {
     entityListOptions.push({
     value: summaryRow[3].get(),
     option: summaryRow[1].get()
     });
     })
     });
     //update entity dropdown option value based on value from entity profile tables
     field('500').getRows().forEach(function(row, rIndex) {
     row[1].config('options', entityListOptions);
     row[5].config('options', entityListOptions);
     });
     });
     */

    //transfer value from integration data center to related corp data center
    calcUtils.calc(function(calcUtils, field, form) {
      var mainTable = field('010');
      var s9Table = field('050');
      var s23Table = field('085');
      var s23TablePart2 = field('086');
      var s49Table = field('090');
      var s306Table = field('306');
      var s343Table = field('343');
      var s511Table = field('511');
      var s566Table = field('566');
      var s568Table = field('568');

      var associatedCorpsData = [];
      var nonAssociatedCorpsData = [];

      var fillingCorpName = field('CP.002').get();

      var expLimitValS49 = field('t2s49.608').get();
      var expLimitValS566 = field('t2s566.435').get();
      var expLimitValS23 = 100;
      var expLimitValS306 = 5000000;
      var expLimitValS343 = 5000000;
      var expLimitValS568 = 20000000;

      mainTable.getRows().forEach(function(row, rIndex) {
        if (rIndex == 0) {
          row[0].assign(fillingCorpName);//buz name
          row[1].assign(field('CP.bn').get());//buz number
          row[2].assign(field('CP.tax_start').get());//tax start
          row[3].assign(field('CP.tax_end').get());//tax end

          row[7].assign(field('T2S7.545').get());//Income from active business carried on in Canada(Amount Z Schedule 7)
          row[7].source(field('T2S7.545'));
          row[8].assign(field('T2S33.690').get());//Taxable capital employed in Canada for the year
          row[8].source(field('T2S33.690'));
          row[9].assign(field('T2J.360').get());//Taxable income(Tn360 T2Jacket)
          row[9].source(field('T2J.360'));
        }
        var infoObj = {};
        row.forEach(function(cell, cIndex) {
          infoObj[mainTableAttrArr[cIndex]] = cell.get();//to push all corp info from main table to an array of obj
        });
        //populate S9 table
        s9Table.cell(rIndex, corpNameCIndex).assign(infoObj.corpName);//corpName
        s9Table.cell(rIndex, s9BnCIndex).assign(infoObj.corpBn);//corpBn
        var relationshipCode = s9Table.cell(rIndex, s9RelationshipCIndex).get();
        if (rIndex == 0) {
          //disabled some cols for current filling corp
          for (var i = 3; i < 9; i++) {
            disableColsCalc(s9Table.cell(rIndex, i));
          }
        }
        else {
          //get input value from S9 then push to infoObj
          infoObj.relationshipCode = relationshipCode;
          infoObj.countryOfResidence = s9Table.cell(rIndex, s9CountryCIndex).get();
          infoObj.numComShare = s9Table.cell(rIndex, s9NumComCIndex).get();
          infoObj.percentComShare = s9Table.cell(rIndex, s9PercentComCIndex).get();
          infoObj.numPrefShare = s9Table.cell(rIndex, s9NumPrefCIndex).get();
          infoObj.percentPrefShare = s9Table.cell(rIndex, s9PercentPrefCIndex).get();
          infoObj.bookValueCapitalStock = s9Table.cell(rIndex, s9BookValueCIndex).get();
          infoObj.rowIndex = rIndex;
        }
        //table s9 calcs
        s9Table.cell(rIndex, s9RelatedCorpCIndex).assign(infoObj.cTaxableCapital);//all corps are related corps
        if (relationshipCode == '4') {//not an associated corp
          s9Table.cell(rIndex, s9AssociatedCorpCIndex).assign(0);
          nonAssociatedCorpsData.push(infoObj)
        } else {//is an associated corp
          s9Table.cell(rIndex, s9AssociatedCorpCIndex).assign(infoObj.cTaxableCapital);
          associatedCorpsData.push(infoObj);
        }

        // //populate S306 table
        // s306Table.cell(rIndex, corpNameCIndex).assign(infoObj.corpName);//corpName
        // s306Table.cell(rIndex, s306BnCIndex).assign(infoObj.corpBn);//corpBn
        // s306Table.cell(rIndex, s306ExpLimitCIndex).assign(expLimitValS306);//expenditure limit

        //populate S343 table
        s343Table.cell(rIndex, corpNameCIndex).assign(infoObj.corpName);//corpName
        s343Table.cell(rIndex, s343BnCIndex).assign(infoObj.corpBn);//corpBn
        s343Table.cell(rIndex, s343ExpLimitCIndex).assign(expLimitValS343);//expenditure limit
      });

      //to assign init value if there is no related corps
      var tableRelatedObjArr = [
        // {
        //   table: '306',
        //   cIndex: s306ExpAllocatedCIndex,
        //   limit: expLimitValS306
        // },
        {
          table: '343',
          cIndex: s343ExpAllocatedCIndex,
          limit: expLimitValS343
        }
      ];

      tableRelatedObjArr.forEach(function(tableObj) {
        var table = field(tableObj.table);
        var cIndex = tableObj.cIndex;
        var limit = tableObj.limit;
        if (mainTable.size().rows == 1) {
          table.cell(0, cIndex).assign(limit);
        } else {
          var totalOther = 0;
          for (var i = 1; i < mainTable.size().rows; i++) {
            totalOther += table.cell(i, cIndex).get();
          }
          table.cell(0, cIndex).assign(Math.max(limit - totalOther, 0));
        }
      });

      // var linkRows = function(table1, row1, table2, row2) {
      //   var counter = calcUtils.field('counter').get();
      //   if (!counter) {
      //     counter = 0;
      //   }
      //   calcUtils.field('counter').assign(++counter);
      //   table1.setRowInfo(row1, 'linkId', counter);
      //   table2.setRowInfo(row2, 'linkId', counter);
      // };
      //
      // var removeLink = function(table1, row1, table2, row2) {
      //   table1.setRowInfo(row1, 'linkId', 0);
      //   table2.setRowInfo(row2, 'linkId', 0);
      // };
      //
      // var getLinkedRowIndex = function(table1, table2, row2) {
      //   var id = table2.getRowInfo(row2).linkId;
      //   for (var i = 0; i < table1.size().rows; i++) {
      //     if (table1.getRowInfo(i).linkId == id) {
      //       return i;
      //     }
      //   }
      //   return -1;
      // };
      //
      // var hasLink = function(table, row) {
      //   return !!table.getRowInfo(row).linkId;
      // };

      // var i;
      // //go through linked table to remove row does not have linkId
      // for (i = 0; i < s23Table.size().rows; i++) {
      //   if (!hasLink(s23Table, i)) {
      //     calcUtils.removeTableRow('entityProfileSummary', '085', i);
      //   }
      //   var linkedRowIndex = getLinkedRowIndex(s9Table, s23Table, i);
      //   if (linkedRowIndex == -1) {
      //     calcUtils.removeTableRow('entityProfileSummary', '085', i);
      //   } else {
      //     var linkedRow = s9Table.getRow(linkedRowIndex);
      //     if (linkedRow[s9RelationshipCIndex].get() == 4) {
      //       removeLink(s9Table, linkedRowIndex, s23Table, i);
      //       calcUtils.removeTableRow('entityProfileSummary', '085', i);
      //       i--;
      //     }
      //   }
      // }
      //
      // //go through main table to add row to linked table if pass condition then link that row
      // for (i = 0; i < s9Table.size().rows; i++) {
      //   if (!hasLink(s9Table, i) && s9Table.cell(i, s9RelationshipCIndex).get() != 4) {
      //     // add a row to link table, check for rIndex, if rIndex is
      //     calcUtils.addTableRow('entityProfileSummary', '085');
      //     linkRows(s9Table, i, s23Table, calcUtils.field('085').size().rows - 1);
      //   }
      // }
      //
      // associatedCorpsData.sort(function(a, b) {
      //   return getLinkedRowIndex(s23Table, s9Table, a.rowIndex) - getLinkedRowIndex(s23Table, s9Table, b.rowIndex);
      // });


      // We want to remove rows from S23Table if the linked row has been deleted.
      calcUtils.filterTableRow(s23Table, function(table, rowIndex) {
        // checkLinkDeleted will return true if the link has been deleted. We need to return false to
        // delete this row.
        var link = table.getRowLinks(rowIndex).filter(function(link) {
          return link.type === 'src';
        })[0];

        // if link is deleted, remove row
        if (calcUtils.checkLinkDeleted(table, rowIndex, link)) {
          return false;
        }

        // Also need to remove row if the linked row's relationship code is 4
        if (link) {
          var linkObj = calcUtils.followLink(link);
          if (linkObj.table.cell(linkObj.rowIndex, s9RelationshipCIndex).get() == 4) {
            return false;
          }
        }
        return true;
      });

      // We want to link the two table based on very specific conditions.
      calcUtils.linkTable(s23Table, s9Table, function(srcTable, rowIndex) {
        return srcTable.cell(rowIndex, s9RelationshipCIndex).get() != 4;
      }, function(dstTable, srcTable, rowIndex) {
        // We want to check if there are any rows in dstTable that aren't linked first.
        //

        var linked = false;
        var numRows = dstTable.size().rows;
        if (numRows > rowIndex) {
          var link = dstTable.getRowLinks(rowIndex).filter(function(link) { return link.type === 'src' })[0];
          // If no link, then link the source row to this row.
          if (!link) {
            calcUtils.linkTableRow(dstTable, rowIndex, srcTable, rowIndex);
            linked = true;
          }
        }
        // If we didn't link an existing row then we create a new row for s23Table and link that row to s9Table
        if (!linked) {
          calcUtils.addTableRow('entityProfileSummary', '085');
          calcUtils.linkTableRow(dstTable, dstTable.size().rows - 1, srcTable, rowIndex);
        }
      });

      associatedCorpsData.sort(function(a, b) {
        return calcUtils.followLink(s9Table.getRowLinks(a.rowIndex || 0)[0]).rowIndex -
            calcUtils.followLink(s9Table.getRowLinks(b.rowIndex || 0)[0]).rowIndex;
      });

      //to assign init value if there is no associated corps
      var tableAssociatedObjArr = [
        {
          table: '085',
          cIndex: s23PercentageCIndex,
          limit: expLimitValS23
        },
        {
          table: '090',
          cIndex: s49ExpAllocatedCIndex,
          limit: expLimitValS49
        },
        {
          table: '566',
          cIndex: s566ExpAllocatedCIndex,
          limit: expLimitValS566
        },
        {
          table: '568',
          cIndex: s568ExpAllocatedCIndex,
          limit: expLimitValS568
        }
      ];

      tableAssociatedObjArr.forEach(function(tableObj) {
        var table = field(tableObj.table);
        var cIndex = tableObj.cIndex;
        var limit = tableObj.limit;
        if (associatedCorpsData.length == 1) {
          table.cell(0, cIndex).assign(limit);
        } else {
          var totalOther = 0;
          for (var i = 1; i < associatedCorpsData.length; i++) {
            totalOther += table.cell(i, cIndex).get();
          }
          table.cell(0, cIndex).assign(Math.max(limit - totalOther, 0));
        }
      });

      associatedCorpsData.forEach(function(infoObj, rIndex) {
        //populate S23 table - ONLY ASSOCIATED CORPS
        s23Table.cell(rIndex, corpNameCIndex).assign(infoObj.corpName);//corpName
        s23Table.cell(rIndex, s23BnCIndex).assign(infoObj.corpBn);//corpBn

        s23Table.cell(rIndex, s23priorYearCIndex).assign(infoObj.priorTaxableCapital);//prior year
        s23Table.cell(rIndex, s23currentYearCIndex).assign(infoObj.cTaxableCapital);//current year
        s23Table.cell(rIndex, s23lastYearCIndex).assign(infoObj.pTaxableCapital);//last tax year

        var percentBusinessLimit = s23Table.cell(rIndex, s23PercentageCIndex).get() / 100;
        var amountBusinessLimit = s23Table.cell(rIndex, s23BusinessLimitCIndex).get() * percentBusinessLimit;
        s23Table.cell(rIndex, s23BusinessAllocatedCIndex).assign(amountBusinessLimit);//business limit allocated
        //Business limit reduction
        //amount C
        var amountCVal = s23Table.cell(0, s23BusinessAllocatedCIndex).get();
        field('101').cell(0, 2).assign(amountCVal);
        //amount D
        var amountDOption = field('081').get();
        var amountDVal;
        var taxableCapEmpVal;

        switch (amountDOption) {
          case '1':
            taxableCapEmpVal = s23Table.total(6).get();
            break;
          case '2':
            taxableCapEmpVal = s23Table.total(7).get();
            break;
          case '3':
            taxableCapEmpVal = s23Table.total(8).get();
            break;
        }

        amountDVal = Math.min(Math.max((taxableCapEmpVal - 10000000) * 0.225 / 100, 0), 11250);
        //amount E
        field('101').cell(0, 5).assign(amountDVal);
        var amountEVal = field('101').cell(1, 5).get() == 0 ? 0 : amountCVal * amountDVal / field('101').cell(1, 5).get();
        field('101').cell(0, 9).assign(amountEVal);
        //Reduced business limit
        var s23TablePart2Row = s23TablePart2.getRow(0);
        s23TablePart2Row[0].assign(fillingCorpName);
        //get amount A,B from T2J
        var amountAVal = field('T2J.400').get();
        var amountBVal = field('T2J.405').get();

        //compare A,B to get minVal
        var minVal = Math.min(amountAVal, amountBVal);

        //compare SBD to part1tax payable
        var dateCompare = calcUtils.dateCompare;
        var taxStartDate = field('CP.tax_start').get();
        var taxEndDate = field('CP.tax_end').get();
        var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();

        if (taxStartDate) {
          var daysBefore2016 = wpw.tax.actions.calculateDaysDifference(taxStartDate, wpw.tax.date(2015, 12, 31));
          if (daysBefore2016 >= daysFiscalPeriod) {
            daysBefore2016 = daysFiscalPeriod
          }

          if (dateCompare.greaterThan(taxEndDate, wpw.tax.date(2017, 1, 1))) {
            taxEndDate = wpw.tax.date(2017, 1, 1);
          }
          var daysAfter2015 = wpw.tax.actions.calculateDaysDifference(wpw.tax.date(2016, 1, 1), taxEndDate);

          if (daysAfter2015 >= daysFiscalPeriod) {
            daysAfter2015 = daysFiscalPeriod
          }
        }
        else {
          daysBefore2016 = 0;
        }

        var sbdVal1 = minVal * daysBefore2016 / daysFiscalPeriod * 17 / 100;
        var sbdVal2 = minVal * daysAfter2015 / daysFiscalPeriod * 17.5 / 100;

        var sbdVal = sbdVal1 + sbdVal2;
        //get part1taxpayable
        var part1TaxVal = field('t2j.605').get() -
            getTotalValFromOtherForm(field, 't2j', [608, 616, 620, 628, 632, 636, 638, 639, 640, 641, 648, 652]);
        //get optimized % allocated
        if (sbdVal >= part1TaxVal) {
          sbdVal = part1TaxVal;
          minVal = sbdVal / (daysBefore2016 / daysFiscalPeriod * 17 / 100 + daysAfter2015 / daysFiscalPeriod * 17.5 / 100);
        }
        //get % allocated
        var percentAllocated;
        //to make sure thr percent is between 0% and 100%
        percentAllocated = Math.max(0, Math.min(100, minVal / (500000 - (1 - amountDVal / 11250)) * 100));

        s23TablePart2Row[2].assign(percentAllocated);
        s23TablePart2Row[3].assign(s23Table.cell(0, s23PercentageCIndex).get());
        s23TablePart2Row[4].assign(s23Table.cell(0, s23BusinessAllocatedCIndex).get());
        s23TablePart2Row[5].assign(field('T2J.425').get());
        s23TablePart2Row[6].assign(field('T2J.430').get());
        s23TablePart2Row[7].assign(field('T2J.654').get());

        //populate S49 table
        s49Table.cell(rIndex, corpNameCIndex).assign(infoObj.corpName);//corpName
        s49Table.cell(rIndex, s49BnCIndex).assign(infoObj.corpBn);//corpBn
        s49Table.cell(rIndex, s49ExpLimitCIndex).assign(expLimitValS49);//expenditure limit

        //populate S511 table
        s511Table.cell(rIndex, corpNameCIndex).assign(infoObj.corpName);//corpName
        s511Table.cell(rIndex, s511BnCIndex).assign(infoObj.corpBn);//corpBn

        //populate S566 table
        s566Table.cell(rIndex, corpNameCIndex).assign(infoObj.corpName);//corpName
        s566Table.cell(rIndex, s566BnCIndex).assign(infoObj.corpBn);//corpBn
        s566Table.cell(rIndex, s566ExpLimitCIndex).assign(expLimitValS566);//expenditure limit

        //populate S568 table
        s568Table.cell(rIndex, corpNameCIndex).assign(infoObj.corpName);//corpName
        s568Table.cell(rIndex, s568BnCIndex).assign(infoObj.corpBn);//corpBn
        s568Table.cell(rIndex, s568ExpLimitCIndex).assign(20000000);//expenditure limit

      });
    });
  });
})();
