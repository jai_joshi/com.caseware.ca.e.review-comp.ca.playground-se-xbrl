(function() {

  var corpTypeOption = [
    {option: '1. Canadian Controlled Private Corporation - CCPC', value: '1'},
    {option: '2. Other private corporation', value: '2'},
    {option: '3. Public Corporation', value: '3'},
    {option: '4. Corporation controlled by a public corporation', value: '4'},
    {option: '5. Other', value: '5'}
  ];

  var relationshipTypeOption = [
    {
      'value': '0',
      'option': ''
    },
    {
      'value': '1',
      'option': '1- Parent'
    },
    {
      'value': '2',
      'option': '2- Subsidiary'
    },
    {
      'value': '3',
      'option': '3- Associated'
    },
    {
      'value': '4',
      'option': '4- Related but not associated'
    }
  ];

  var associationTypeOption = [
    {
      'value': '0',
      'option': ''
    },
    {
      'value': '1',
      'option': '1- Associated for purposes of allocating the business limit (unless association code 5 applies)'
    },
    {
      'value': '2',
      'option': '2- CCPC that is a \'3rd corp.\' that has elected (subsection 256(2)) not to be ' +
      'associated for purposes of the small business deduction'
    },
    {
      'value': '3',
      'option': '3- Non-CCPC that is a \'3rd corporation\' as defined in subsection 256(2)'
    },
    {
      'value': '4',
      'option': '4- Associated non-CCPC'
    },
    {
      'value': '5',
      'option': '5- Associated CCPC to which code 1 does not apply because of a subsection ' +
      '256(2) election made by a \'third corporation\''
    }
  ];

  var countryAddressCodes = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.countryAddressCodes, 'value');

  var relationshipTypeOptions = [
    {option: 'Parent', value: '1'},//for person to person
    {option: 'Child', value: '2'},//for person to person
    {option: 'Sibling', value: '3'},//for person to person
    {option: 'Spouse/Common-law', value: '4'},//for person to person
    {option: 'Trustee', value: '5'},//for person to trust
    {option: 'Executor', value: '6'},//for person to trust
    {option: 'Beneficiaries', value: '7'},//for person to trust
    {option: 'General Partner', value: '8'},//for person to partnership
    {option: 'Limited Partner', value: '9'},//for person to partnership
    {option: 'Shareholder', value: '10'},//for person to corporation
    {option: 'De jure control', value: '11'},//for person to corporation
    {option: 'De facto control', value: '12'}//for person to corporation
  ];

  var otherInfoOptions = [
    {option: '% voting owned (For corporation)', value: '1'},
    {option: '% income allocated at year-end (For partnership)', value: '2'},
    {option: '% FMV of the trust interest (For non-discretionary trust)', value: '3'}
  ];

  function getTableEntityLists(entityType, repeatFormFieldIdArr) {
    var entityName = 'Name of the ' + entityType;
    var entityId;
    switch (entityType) {
      case 'Person':
        entityId = 'SIN';
        break;
      case 'Corporation':
        entityId = 'Business Number';
        break;
      case 'Trust':
        entityId = 'Trust Number';
        break;
      case 'Partnership':
        entityId = 'Partnership Number';
        break;
    }

    return {
      hasTotals: true,
      fixedRows: true,
      infoTable: true,
      initRows: 0,
      columns: [
        {
          type: 'none',
          colClass: 'std-spacing-width'
        },
        {
          header: entityName,
          colClass: 'std-input-col-width',
          type: 'text',
          linkedFieldId: repeatFormFieldIdArr[0]
        },
        {
          type: 'none',
          colClass: 'std-spacing-width'
        },
        {
          header: entityId,
          colClass: 'std-input-col-width',
          type: 'text',
          linkedFieldId: repeatFormFieldIdArr[1]
        },
        {
          type: 'none',
          width: '25%'
        }
      ]
    }
  }

  wpw.tax.create.tables('entityProfileSummary', {
    /*
     '100': getTableEntityLists('Person', ['201', '207']),
     '200': getTableEntityLists('Corporation', ['301', '303']),
     '300': getTableEntityLists('Trust', ['401', '415']),
     '400': getTableEntityLists('Partnership', ['501', '511']),
     '500': {
     hasTotals: true,
     showNumbering: true,
     maxLoop: 100000,
     infoTable: true,
     columns: [
     {
     type: 'none',
     width: '2%'
     },
     {
     header: 'Entity 1',
     width: '20%',
     type: 'dropdown'
     },
     {
     type: 'none',
     width: '2%'
     },
     {
     header: 'Relationship type',
     width: '20%',
     type: 'dropdown',
     options: relationshipTypeOptions
     },
     {
     type: 'none',
     width: '2%'
     },
     {
     header: 'Entity 2',
     width: '20%',
     type: 'dropdown'
     },
     {
     type: 'none',
     width: '2%'
     },
     {
     header: 'Other information',
     width: '20%',
     type: 'dropdown',
     options: otherInfoOptions
     },
     {
     type: 'none',
     width: '2%'
     },
     {
     header: 'Other value',
     width: '10%'
     },
     {
     type: 'none'
     }
     ]
     },
     */
    //data center for related corps
    '010': {
      scrollx: true,
      showNumbering: true,
      hasTotals: true,
      repeats: ['050', '343'],
      linkedExternalTable: [
        {formId: 't2s9', fieldId: '050'},
        // {formId: 't2s306', fieldId: '100'},
        {formId: 't2s343', fieldId: '900'}
      ],
      superHeaders: [
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {
          header: 'The last tax year-ending in the previous calendar year data',
          divider: true,
          colspan: '2'
        },
        {
          header: 'The current year data',
          divider: true,
          colspan: '3'
        }
      ],
      columns: [
        {
          header: 'Corporation\'s name',
          colClass: 'std-input-width',
          type: 'text'
        },
        {
          header: 'Business number (see note 1)',
          type: 'custom',
          colClass: 'std-input-width',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          header: 'Tax year-start',
          type: 'date',
          colClass: 'std-input-width'
        },
        {
          header: 'Tax year-end',
          type: 'date',
          colClass: 'std-input-width'
        },
        {
          header: '<b>Prior year</b> - Taxable capital employed in Canada',
          colClass: 'std-input-width',
          total: true
        },
        {
          header: 'Taxable capital employed in Canada for the year',
          colClass: 'std-input-width',
          total: true
        },
        {
          header: 'Taxable income',
          colClass: 'std-input-width',
          total: true
        },
        {
          header: 'Income from active business carried on in Canada<br>(Amount Z Schedule 7)',
          colClass: 'std-input-width',
          total: true
        },
        {
          header: 'Taxable capital employed in Canada for the year<br>(Tn690 Schedule 33)',
          colClass: 'std-input-width',
          total: true
        },
        {
          header: 'Taxable income<br>(Tn360 T2Jacket)',
          colClass: 'std-input-width',
          total: true
        }
      ]
    },
    //S9 table
    '050': {
      fixedRows: true,
      scrollx: true,
      hasTotals: true,
      superHeaders: [
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {
          header: 'Taxable capital employed in Canada for the year of the corporation and its',
          divider: true,
          colspan: '2'
        }
      ],
      columns: [
        {
          header: 'Corporation\'s name',
          type: 'text',
          colClass: 'std-input-width',
          cannotOverride: true
        },
        {
          header: 'Country of residence (other than Canada)',
          colClass: 'std-input-width',
          type: 'dropdown',
          options: countryAddressCodes
        },
        {
          header: 'Business Number of Associated Corporations',
          type: 'custom',
          colClass: 'std-input-width',
          format: ['{N9}RC{N4}', 'NR'],
          cannotOverride: true
        },
        {
          header: 'Relationship code',
          colClass: 'std-input-width',
          type: 'dropdown',
          options: relationshipTypeOption,
          init: '1'
        },
        {
          header: 'Number of common shares you own',
          colClass: 'std-input-width'
        },
        {
          header: '% of com. shares you own',
          colClass: 'small-input-width',
          format: {suffix: '%'},
          filters: 'decimals 2'
        },
        {
          header: 'Number of pref. shares you own',
          colClass: 'std-input-width'
        },
        {
          header: '% of pref. shares you own',
          format: {suffix: '%'},
          colClass: 'small-input-width',
          filters: 'decimals 2'
        },
        {
          header: 'Book value of capital stock',
          format: {prefix: '$'},
          colClass: 'std-input-width'
        },
        {
          header: 'Related corporations<br>(Note2)',
          format: {prefix: '$'},
          colClass: 'std-input-width',
          total: true,
          cannotOverride: true
        },
        {
          header: 'Associated corporations<br>(Note2)',
          format: {prefix: '$'},
          colClass: 'std-input-width',
          total: true,
          cannotOverride: true
        }
      ]
    },
    //ASSOCIATED CORPS
    //s23 table
    '085': {
      fixedRows: true,
      scrollx: true,
      linkedExternalTable: [
        {formId: 't2s23', fieldId: '085'},
        {formId: 't2s49', fieldId: '085'},
        {formId: 't2s566', fieldId: '550'},
        {formId: 't2s566', fieldId: '650'},
        {formId: 't2s511', fieldId: '100'},
        {formId: 't2s568', fieldId: '1100'}
      ],
      repeats: ['090', '511', '566', '568'],
      superHeaders: [
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {
          header: 'Taxable capital employed in Canada for the',
          divider: true,
          colspan: '3'
        }
      ],
      columns: [
        {
          header: 'Name of Associated Corporations',
          type: 'text',
          colClass: 'std-input-width',
          cannotOverride: true
        },
        {
          header: 'Business Number of Associated Corporations',
          type: 'custom',
          colClass: 'std-input-width',
          format: ['{N9}RC{N4}', 'NR'],
          cannotOverride: true
        },
        {
          header: 'Association code',
          colClass: 'std-input-width',
          type: 'dropdown',
          options: associationTypeOption
        },
        {
          header: 'Business limit for the year (before the allocation) $',
          format: {prefix: '$'},
          colClass: 'std-input-width',
          init: '500000',
          disabled: true,
          cannotOverride: true
        },
        {
          header: 'Percentage of the business limit %',
          colClass: 'small-input-width',
          filters: 'decimals 2',
          total: true,
          format: {suffix: '%'},
          decimals: 2,
          totalLimit: '100',
          validate: 'percent'
        },
        {
          header: 'Business limited allocated* $',
          format: {prefix: '$'},
          colClass: 'std-input-width',
          total: true,
          totalLimit: '500000',
          cannotOverride: true
        },
        {
          header: 'Prior year',
          colClass: 'std-input-width',
          total: true
        },
        {
          header: 'Current year',
          colClass: 'std-input-width',
          total: true
        },
        {
          header: 'Last tax year-ending in the previous calendar year',
          colClass: 'std-input-width',
          total: true
        }
      ],
      hasTotals: true
    },
    '086': {
      fixedRows: true,
      scrollx: true,
      columns: [
        {
          header: 'Name of Current Filling Corporations',
          colClass: 'std-input-width',
          type: 'text'
        },
        {
          header: 'Business limit for the year (before the allocation) $',
          format: {prefix: '$'},
          colClass: 'std-input-width',
          init: '500000',
          disabled: true
        },
        {
          header: 'Percentage of the business limit % to maximize the SBD (calculated by CorpTax)',
          colClass: 'std-input-width',
          filters: 'decimals 2',
          total: true,
          format: {suffix: '%'},
          decimals: 2,
          totalLimit: '100'
        },
        {
          header: 'Percentage of the business limit % claimed',
          colClass: 'std-input-width',
          filters: 'decimals 2',
          total: true,
          format: {suffix: '%'},
          decimals: 2,
          totalLimit: '100',
          validate: 'percent'
        },
        {
          header: 'Business limited allocated* $',
          format: {prefix: '$'},
          colClass: 'std-input-width',
          total: true,
          totalLimit: '500000'
        },
        {
          header: 'Reduced business limit $',
          format: {prefix: '$'},
          colClass: 'std-input-width'
        },
        {
          header: 'Small business deduction $',
          format: {prefix: '$'},
          colClass: 'std-input-width'
        },
        {
          header: 'Part I tax payable $',
          format: {prefix: '$'},
          colClass: 'std-input-width'
        }
      ],
      hasTotals: true
    },
    //s49 table
    '090': {
      fixedRows: true,
      scrollx: true,
      hasTotals: true,
      columns: [
        {
          header: 'Name of Associated Corporations',
          type: 'text',
          cannotOverride: true
        },
        {
          header: 'Business Number of Associated Corporations',
          type: 'custom',
          colClass: 'std-input-width',
          format: ['{N9}RC{N4}', 'NR'],
          cannotOverride: true
        },
        {
          header: 'Type of corporation code',
          colClass: 'std-input-width',
          type: 'dropdown',
          options: corpTypeOption
        },
        {
          header: 'Expenditure limit',
          colClass: 'std-input-width',
          format: {prefix: '$'},
          cannotOverride: true
        },
        {
          header: 'Expenditure limit allocated',
          format: {prefix: '$'},
          colClass: 'std-input-width',
          total: true,
          totalMessage: 'The expenditure limit (cannot be more than $3,000,000)',
          totalLimit: '3000000'
        }
      ]
    },
    //amount D table
    '101': {
      fixedRows: true,
      infoTable: true,
      verticalAlign: 'middle',
      columns: [
        {colClass: 'std-input-col-width', type: 'none'},//des
        {colClass: 'std-spacing-width', type: 'none', cellClass: 'alignCenter'}, //space
        {colClass: 'std-input-width', cellClass: 'alignCenter'},//formfield
        {colClass: 'std-padding-width', type: 'none', cellClass: 'alignCenter'}, //x
        {colClass: 'std-padding-width', type: 'none'},//tn
        {colClass: 'std-input-width', cellClass: 'alignCenter'},//formfield
        {colClass: 'std-spacing-width', type: 'none', cellClass: 'alignCenter'}, //space
        {colClass: 'std-padding-width', type: 'none'},//indicator
        {colClass: 'std-spacing-width', type: 'none', cellClass: 'alignCenter'}, //space
        {colClass: 'std-input-width', cellClass: 'alignCenter'},//formfield
        {colClass: 'std-padding-width', type: 'none', cellClass: 'alignCenter'} //end
      ],
      cells: [
        {
          0: {label: 'Amount C'},
          3: {label: 'x'},
          4: {tn: '415'},
          7: {label: 'D ='}
        },
        {
          2: {type: 'none'},
          5: {init: '11250', disabled: true},
          9: {type: 'none'}
        }
      ]
    },
    //tables306
    // '306': {
    //   fixedRows: true,
    //   showNumbering: true,
    //   hasTotals: true,
    //   columns: [
    //     {
    //       header: 'Name of each corporation that is a member of the related group<br>',
    //       tn: '200', 'validate': {'or': [{'length': {'min': '1', 'max': '175'}}, {'check': 'isEmpty'}]},
    //       type: 'text',
    //       cannotOverride: true
    //     },
    //     {
    //       header: 'Business Number <br>(if a corporation is not registered, enter 'NR')<br>',
    //       tn: '300',
    //       type: 'custom',
    //       format: ['{N9}RC{N4}', 'NR'],
    //       cannotOverride: true
    //     },
    //     {
    //       header: 'Expenditure limit',
    //       colClass: 'std-input-width',
    //       cannotOverride: true
    //     },
    //     {
    //       header: 'Allocation of capital deduction for the year<br>$<br>',
    //       tn: '400', 'validate': {'or': [{'matches': '^[.\\d]{1,13}$'}, {'check': 'isEmpty'}]},
    //       total: true,
    //       totalMessage: 'Total (not to exceed $5,000,000)',
    //       totalLimit: '5000000'
    //     }
    //   ]
    // },

    // table 511
    '511': {
      fixedRows: true,
      hasTotals: true,
      showNumbering: true,
      infoTable: false,
      maxLoop: 100000,
      columns: [
        {
          header: 'Names of associated corporations',
          type: 'text'
        },
        {
          header: 'Business number <br> (Canadian corporation only)',
          type: 'custom',
          colClass: 'std-input-width',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          header: 'Total assets * <br> (see Note 2)',
          colClass: 'std-input-width',
          tn: '400',
          total: true
        },
        {
          header: 'Total revenue ** <br> (see Note 2)',
          colClass: 'std-input-width',
          total: true
        }
      ]
    },
    //table s343
    '343': {
      fixedRows: true,
      hasTotals: true,
      showNumbering: true,
      columns: [
        {
          header: 'Name of each corporation that is a member of the related group',
          validate: {'or': [{'length': {'min': '1', 'max': '175'}}, {'check': 'isEmpty'}]},
          tn: '200',
          type: 'text',
          cannotOverride: true
        },
        {
          header: 'Business Number<br>(if a corporation is not registered, enter \'NR\')',
          tn: '300',
          colClass: 'std-input-width',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR'],
          cannotOverride: true
        },
        {
          header: 'Expenditure limit',
          colClass: 'std-input-width',
          cannotOverride: true
        },
        {
          header: 'Allocation of capital deduction for the year<br>$',
          tn: '400',
          colClass: 'std-input-width',
          totalLimit: '5000000',
          totalMessage: 'Subtotal'
        }
      ]
    },
    // table 566
    '566': {
      fixedRows: true,
      showNumbering: true,
      hasTotals: true,
      columns: [
        {
          header: '1<br>Names of associated corporations<br>',
          tn: '600',
          type: 'text',
          cannotOverride: true
        },
        {
          header: '2<br>Business Number of associated corporations<br>(enter \'NR\' if a corporation is not registered)<br>',
          tn: '605',
          colClass: 'std-input-width',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR'],
          cannotOverride: true
        },
        {
          header: '3<br>Specified capital amount<br>',
          tn: '520',
          total: true,
          colClass: 'std-input-width'
        },
        {
          header: 'Business limit for the year (before the allocation) $',
          format: {prefix: '$'},
          colClass: 'std-input-width',
          disabled: true,
          cannotOverride: true
        },
        {
          header: '3<br>Expenditure limit allocated * (allocate the amount of the expenditure ' +
          'limit from line 435 in Part 4 to each associated corporation)<br>',
          colClass: 'std-input-width',
          tn: '610',
          total: true
        }
      ]
    },
    // table 568
    '568': {
      fixedRows: true,
      showNumbering: true,
      hasTotals: true,
      columns: [
        {
          header: 'Name of all associated corporations, including the filing corporation ',
          tn: '300',
          type: 'text',
          cannotOverride: true
        },
        {
          header: 'Business Number of associated corporations<br>(enter \'NR\' if a corporation is not registered)',
          tn: '305',
          colClass: 'std-input-width',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR'],
          cannotOverride: true
        },
        {
          header: 'Business limit for the year (before the allocation) $',
          format: {prefix: '$'},
          colClass: 'std-input-width',
          disabled: true,
          cannotOverride: true
        },
        {
          header: 'Expenditure limit allocated',
          colClass: 'std-input-width',
          tn: '310',
          total: true
        }
      ]
    }
  })
})();
