(function() {

  wpw.tax.create.formData('t1146', {
    formInfo: {
      isRepeatForm: true,
      abbreviation: 'T1146',
      title: 'Agreement to Transfer Qualified Expenditures Incurred in Respect of SR&ED Contracts Between ' +
      'Persons Not Dealing at Arm\'s Length',
      schedule: 'Schedule 62',
      showCorpInfo: true,
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        rows: [
          {
            'label': 'Agreement',
            'labelClass': 'fullLength titleFont center'
          },
          {
            'label': 'The transferor and the transferee identified below hereby agree to transfer the amount of SR&ED qualified expenditures (per line 106 on page 2 of this Form) to the transferee.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '010',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'format': {
                    'prefix': '$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '010'
                }
              }
            ]
          },
          {
            'label': 'The breakdown of the transferred amount is:'
          },
          {
            'label': 'Current expenditures',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '015',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'format': {
                    'prefix': '$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '015'
                }
              }
            ]
          },
          {
            'label': 'Capital expenditures (Ensure that you only include expenditures made before 2014)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '020',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'format': {
                    'prefix': '$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '020'
                }
              }
            ]
          },
          {
            'label': 'Carry the transferred amounts on lines 015 and 020 above over to Form T661, lines 508 and 510 for the transferee, and to lines 544 and 546 of Form T661 for the transferor.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'label': 'Is this an amended agreement?',
            'inputType': 'radio',
            'num': '025',
            'tn': '025'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'label': 'Authorization of Transfer',
            'labelClass': 'fullLength titleFont center'
          },
          {
            'label': 'The transferor and the transferee must file with Form T1146: ',
            'labelClass': 'fullLength'
          },
          {
            'label': '- certified copies of the resolutions of the Directors authorizing the agreement; or',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '- a Directors\' resolution delegating authority to an authorized officer of each corporation signing this form.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'The Directors\' resolution will be in effect for all subsequent years until it is rescinded.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'If two corporations are owned exclusively by one shareholder, a T1146 signed by authorized officers of each corporation will be accepted if a signed confirmation by the shareholder is filed with the form stating that he is the only shareholder of both corporations, and that he has authorized the transfer of the SR&ED qualified expenditure from one corporation to the other. A Directors\' resolution will not be required.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'label': 'Were copies of the resolutions/confirmation authorizing the transfer submitted in a previous year?',
            'inputType': 'radio',
            'num': '030',
            'tn': '030'
          },
          {
            'type': 'infoField',
            'label': 'If you answered yes to line 030, in what tax year was it submitted?',
            'inputType': 'fixedSizeBox',
            'boxes': 4,
            'num': '035',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '4',
                    'max': '4'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'tn': '035'
          },
          {
            'label': 'If you answered no to line 030:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- If you are filing a paper return, attach the required documents to Form T1146.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '- If you are filing electronically, refer to the "Paper Documentation" section of RC4018, Electronic Filers Manual, for instructions on how to file paper documents in support of electronically filed forms.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'splitTable',
            'side1': [
              {
                'label': 'Name of transferor (print)'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '040',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '175'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '040'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Address (head office if corporation)',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'inputType': 'address',
                'add1Num': '1001',
                'add2Num': '1002',
                'provNum': '1003',
                'cityNum': '1004',
                'countryNum': '1005',
                'postalCodeNum': '1006'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Name of individual or authorized signing officer of the corporation',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '055',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '60'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '055'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Signature of individual or authorized signing officer of the corporation',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '1008'
              }
            ],
            'side2': [
              {
                'type': 'infoField',
                'inputType': 'custom',
                'format': [
                  '{N9}RC{N4}',
                  'NR',
                  '{N9}',
                  'NA'
                ],
                'label': 'Business number or Social Insurance Number',
                'labelWidth': '20%',
                'num': '045',
                'tn': '045',
                'validate': {
                  'check': 'mod10'
                }
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'inputType': 'date',
                'label': 'Tax year-end',
                'num': '050',
                'tn': '050'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Title',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '060',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '60'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '060'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Date',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'inputType': 'date',
                'labelWidth': '0%',
                'width': '90%',
                'num': '065',
                'tn': '065'
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'splitTable',
            'side1': [
              {
                'label': 'Name of transferee (print)'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '070',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '175'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '070'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Address (head office if corporation)',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'inputType': 'address',
                'add1Num': '1021',
                'add2Num': '1022',
                'provNum': '1023',
                'cityNum': '1024',
                'countryNum': '1025',
                'postalCodeNum': '1026'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Name of individual or authorized signing officer of the corporation',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '085',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '60'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '085'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Signature of individual or authorized signing officer of the corporation',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '1028'
              }
            ],
            'side2': [
              {
                'type': 'infoField',
                'inputType': 'custom',
                'format': [
                  '{N9}RC{N4}',
                  'NR',
                  '{N9}',
                  'NA'
                ],
                'label': 'Business number or Social Insurance Number',
                'labelWidth': '20%',
                'num': '075',
                'tn': '075',
                'validate': {
                  'check': 'mod10'
                }
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'inputType': 'date',
                'label': 'Tax year-end',
                'num': '080',
                'tn': '080'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Title',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '090',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '60'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '090'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Date',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'inputType': 'date',
                'labelWidth': '0%',
                'width': '90%',
                'num': '095',
                'tn': '095'
              }
            ]
          }
        ]
      },
      {
        header: 'Calculation of qualified SR&ED expenditures to be transferred for the tax year of the transferor',
        rows: [
          {
            'label': 'Transferor\'s SR&ED qualified expenditure pool at the end of the tax year, before subtracting the transferred amount',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '100',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'format': {
                    'prefix': '$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              }
            ]
          },
          {
            'label': 'Notional contract payments (NCP) - the total of all amounts that would be contract payments to the transferor for SR&ED performed for, or on behalf of, the transferee, if the two parties were dealing at arm\'s length (For a definition of contract payment see the SR&ED Glossary located at cra.gc.ca/txcrdt/sred-rsde/clmng/glssry-eng.html)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'format': {
                    'prefix': '$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              },
              null
            ]
          },
          {
            'label': 'Notional contract payments on line 110 that are not paid by the transferee 112 within 180 days of the tax year-end of the transferor',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '112',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'format': {
                    'prefix': '$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '112'
                }
              },
              null
            ]
          },
          {
            'label': 'Maximum notional contract payments (line 110 minus line 112)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '114',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'format': {
                    'prefix': '$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '114'
                }
              },
              null
            ]
          },
          {
            'label': 'Qualified expenditures (related to the NCP) incurred and paid in the year by the transferor for the portion of SR&ED performed at non-arm\'s length. (The expenditures must be paid by the performer on or before the day that is 180 days after the end of the tax year in which they are incurred. Do not include qualified expenditures that are not paid within that time.)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1041',
                  'format': {
                    'prefix': '$'
                  }
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'Amounts transferred to the transferor for qualified expenditures attributable to the SR&ED performed for the transferee (This would be the case where the transferor has subcontracted all or a portion of the SR&ED performed for the transferee to another non-arm\'s length subcontractor, and the subcontractor transferred its qualified expenditures attributable to that SR&ED to the transferor.)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1042',
                  'format': {
                    'prefix': '$'
                  }
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': 'Total ( amount a plus amount b)',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1043',
                  'format': {
                    'prefix': '$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '1043'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '(A)'
                }
              }
            ]
          },
          {
            'label': 'Qualified expenditures (related to the NCP) incurred in the year by the transferor for the portion of SR&ED performed at non-arm\'s length, before subtracting unpaid amounts. (unpaid amounts per subsections 127(26) and 78(4) of the ITA)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1044',
                  'format': {
                    'prefix': '$'
                  }
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'Amounts transferred to the transferor for qualified expenditures attributable to the SR&ED performed for the transferee',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1045',
                  'format': {
                    'prefix': '$'
                  }
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Total ( amount c plus amount d)',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1046',
                  'format': {
                    'prefix': '$'
                  }
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '(B)'
                }
              }
            ]
          },
          {
            'label': 'Divide amount (A) by amount (B)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1047',
                  'format': {
                    'prefix': '$'
                  }
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '(C)'
                }
              }
            ]
          },
          {
            'label': 'Multiply amount on line 114 by amount (C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1048',
                  'format': {
                    'prefix': '$'
                  }
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '(D)'
                }
              }
            ]
          },
          {
            'label': 'Enter amount calculated in (D) above',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '102',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'format': {
                    'prefix': '$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '102'
                }
              }
            ]
          },
          {
            'label': 'Maximum amount that may be transferred: enter the amount from line 100 or 102, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'format': {
                    'prefix': '$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              }
            ]
          },
          {
            'label': 'Amount specified for the transfer: ',
            'labelClass': 'bold'
          },
          {
            'label': 'You may transfer an amount up to the amount on line 104.  '
          },
          {
            'label': 'Carry this amount over to line 010 on page 1 of this form.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '106',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'format': {
                    'prefix': '$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '106'
                }
              }
            ]
          }
        ]
      },
      {
        hideFieldset: true,
        header: 'Privacy Notice',
        rows: [
          {
            label: 'Personal information is collected pursuant to the <i>Income Tax Act</i> and the Income ' +
            'Tax Regulations and is used for verification of compliance, administration and enforcement ' +
            'of the Scientific Research and Experimental Development (SR&ED) program requirements. ',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Information may also be used for the administration and enforcement of other provisions ' +
            'of the Act, including assessment, audit, enforcement, collections, and appeals, and may be ' +
            'disclosed under information-sharing agreements in accordance with the Act. Incomplete or inaccurate ' +
            'information may result in delays in processing SR&ED claims.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: 'The social insurance number is collected pursuant to section 237 of the Act and is ' +
            'used for identification purposes.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Information is described in Scientific Research and Experimental Development CRA PPU 441 ' +
            'and is protected under the <i>Privacy Act</i>. Individuals have a right of protection, access to, and ' +
            'correction or notation of their personal information. Please be advised that you are entitled to ' +
            'complain to the Privacy Commissioner of Canada regarding our handling of your information.',
            labelClass: 'fullLength'
          }
        ]
      }
    ]
  });
})();
