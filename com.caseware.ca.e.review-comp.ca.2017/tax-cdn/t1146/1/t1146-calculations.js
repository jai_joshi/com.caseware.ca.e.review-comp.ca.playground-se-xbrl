(function() {

  wpw.tax.create.calcBlocks('t1146', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field, form) {
      field('114').assign(field('110').get() - field('112').get());
      field('1043').assign(field('1041').get() + field('1042').get());
      field('1046').assign(field('1044').get() + field('1045').get());
      field('1047').assign(field('1046').get() == 0 ? 0 : field('1043').get() / field('1046').get());
      field('1048').assign(field('114').get() * field('1047').get());
      field('102').assign(field('1048').get());
      field('104').assign(Math.min(field('100').get(), field('102').get()));
      field('010').assign(field('106').get());

    });
  });
})();
