(function() {
  wpw.tax.create.diagnostics('t1146', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0620001', common.prereq(common.and(
        common.requireFiled('T1146'),
        common.check('010', 'isNonZero')),
        common.check(['015', '020'], 'isNonZero')));

    diagUtils.diagnostic('0620003', common.prereq(common.and(
        common.requireFiled('T1146'),
        common.check(['015', '020'], 'isNonZero')),
        common.check('010', 'isNonZero')));

    diagUtils.diagnostic('0620005', common.prereq(common.requireFiled('T1146'), common.and(
        common.check('010', 'isNonZero'),
        common.check(['015', '020'], 'isNonZero'))));

    diagUtils.diagnostic('0620009', common.prereq(common.and(
        common.requireFiled('T1146'),
        common.check('030', 'isYes')),
        common.check('035', 'isFilled')));

    diagUtils.diagnostic('0620011', common.prereq(common.requireFiled('T1146'),
        function(tools) {
          return tools.requireAll(tools.list(['040', '050', '055', '060', '070', '080', '085', '090', '100', '102', '106', '110', '114']), 'isNonZero') &&
              tools.requireOne(tools.list(['1011', '1012']), 'isFilled') &&
              tools.requireOne(tools.list(['1031', '1032']), 'isFilled');
        }));

    diagUtils.diagnostic('062.010', common.prereq(common.requireFiled('T1146'),
        function(tools) {
          return tools.field('010').require(function() {
            return this.get() == parseFloat(tools.field('106').get());
          })
        }));

    diagUtils.diagnostic('062.106', common.prereq(common.requireFiled('T1146'),
        function(tools) {
          return tools.field('106').require(function() {
            return this.get() < parseFloat(tools.field('104').get());
          })
        }));

  });
})();
