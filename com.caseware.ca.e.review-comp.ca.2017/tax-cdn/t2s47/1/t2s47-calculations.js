(function() {
  wpw.tax.create.calcBlocks('t2s47', function(calcUtils) {
    calcUtils.calc(function(calcUtils) {
      var num = '425';
      var num2 = '426';
      var midnum = '422';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '621';
      var num2 = '620';
      var midnum = '622';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils, field, form) {
      //eligibility
      var isNotEligible = field('330').get() == 2 || field('335').get() == 1 || field('340').get() == 1 ||
          field('345').get() == 1 || field('350').get() == 1;
      field('351').assign(isNotEligible ? 2 : 1);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      if (field('351').get() == 1) {
        //contact info //
        field('151').assign(field('T2J.951').get() + ' ' + field('T2J.950').get());
        field('153').assign(field('T2J.956').get());
        //part4
        field('651').assign(field('302').get());
        field('654').assign(field('651').get() == null ? null : wpw.tax.utilities.addToDate(field('651').get(), -2, 0, 0));
        //Part 5 calcs
        calcUtils.subtract('424', '421', '423');
        calcUtils.equals('425', '424');
        calcUtils.getGlobalValue('422', 'ratesFed', '420');
        calcUtils.multiply('426', ['425', '422'], (1 / 100));
        calcUtils.subtract('430', '426', '427');
        //part 6 calcs
        calcUtils.sumBucketValues('608', ['601', '603', '605', '606', '607', '609']);
        calcUtils.equals('610', '608');
        calcUtils.sumBucketValues('612', ['610', '611']);
        calcUtils.sumBucketValues('616', ['613', '615']);
        calcUtils.equals('617', '616');
        calcUtils.subtract('618', '612', '617');
        calcUtils.min('619', ['430', '618']);
        //Part 7 calcs
        calcUtils.equals('621', '619');
        calcUtils.getGlobalValue('622', 'ratesFed', '421');
        calcUtils.multiply('620', ['621', '622'], (1 / 100));
      }
      else {
        field('620').assign();
        field('620').disabled(true);
        field('654').assign();
      }
    });
  });
})();
