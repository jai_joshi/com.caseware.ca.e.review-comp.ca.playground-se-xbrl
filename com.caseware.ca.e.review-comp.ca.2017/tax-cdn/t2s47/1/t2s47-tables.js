(function() {
  wpw.tax.create.tables('t2s47', {
    'di_table_1': {
      'num': 'di_table_1',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Amount C',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '425'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '422',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '426'
          },
          '8': {
            'label': 'D'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_2': {
      'num': 'di_table_2',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': '<b>Canadian film or video production tax credit</b> (amount L in Part 6',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '621',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '622',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '620'
          },
          '7': {
            'num': '620',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' doubleUnderline'
          },
          '8': {
            'label': 'M'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    '100': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1],
      'columns': [
        {
          'width': '60%',
          cellClass: 'alignLeft'
        },
        {
          cellClass: 'alignLeft'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Name of person to contact for more information',
            'num': '151', "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
            'tn': '151',
            type: 'text'
          },
          '1': {
            'label': 'Telephone number including area code',
            'num': '153',
            'tn': '153',
            type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
            validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
          }
        }
      ]
    },
    '106': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'qtr-col-width',
          type: 'none'
        },
        {
          type: 'custom',
          format: ['{N5}']
        }
      ],
      cells: [
        {
          '0': {
            tn: '303'
          },
          '1': {
            label: 'PCH'
          },
          '2': {
            num: '303', "validate": {"or": [{"matches": "^-?[.\\d]{5,5}$"}, {"check": "isEmpty"}]}
          }
        }
      ]
    }
  })
})();
