(function() {
  wpw.tax.create.diagnostics('t2s47', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0470001', common.prereq(common.check(['t2j.796'], 'isNonZero'), common.requireFiled('T2S47')));

    diagUtils.diagnostic('0470002', common.prereq(common.requireFiled('T2S47'),function(tools) {
      return tools.requireAll(tools.list(['151', '153', '301', '311', '312']), 'isFilled')
    }));

    diagUtils.diagnostic('0470003', common.prereq(common.requireFiled('T2S47'),
        function(tools) {
          return tools.requireOne(tools.list(['302']), 'isNonZero') &&
              (tools.requireOne(tools.list(['304']), 'isNonZero') ||
              tools.requireAll(tools.list(['305', '306']), 'isNonZero'));
        }));

    diagUtils.diagnostic('0470007', common.prereq(common.and(
        common.requireFiled('T2S47'),
        common.check(['620'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['421']), 'isNonZero') &&
              tools.requireOne(tools.list(['601', '603', '605', '606', '607', '609', '611']), 'isNonZero');
        }));

    diagUtils.diagnostic('0470010', common.prereq(common.requireFiled('T2S47'),
        function(tools) {
          return tools.requireAll(tools.list(['t2j.796', '620']), 'isEmpty');
        }));

    diagUtils.diagnostic('0470015', common.prereq(common.and(
        common.requireFiled('T2S47'),
        common.check(['620'], 'isNonZero')),
        function(tools) {
          return tools.requireAll(tools.list(['330', '335', '340', '345', '350']), 'isNonZero');
        }));

    diagUtils.diagnostic('047.304', common.prereq(common.and(
        common.requireFiled('T2S47'),
        common.check(['305', '306'], 'isZero', true)),
        function(tools) {
          return tools.requireOne(tools.list(['304']), 'isNonZero');
        }));

    diagUtils.diagnostic('047.305', common.prereq(common.and(
        common.requireFiled('T2S47'),
        common.check(['304'], 'isZero')),
        function(tools) {
          return tools.requireOne(tools.list(['305']), 'isNonZero');
        }));

    diagUtils.diagnostic('047.306', common.prereq(common.and(
        common.requireFiled('T2S47'),
        common.check(['305'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['306']), 'isNonZero');
        }));

  });
})();
