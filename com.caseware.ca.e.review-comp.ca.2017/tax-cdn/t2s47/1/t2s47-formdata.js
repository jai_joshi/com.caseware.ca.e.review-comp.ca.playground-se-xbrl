(function() {
  var labelWidth = '75%';

  wpw.tax.global.formData.t2s47 = {
    formInfo: {
      abbreviation: 'T2S47',
      title: 'Canadian Film or Video Production Tax Credit',
      code: 'Code 1101',
      schedule: 'Schedule 47',
      hideProtectedB: true,
      showCorpInfo: true,
      isRepeatForm: true,
      headerImage: 'canada-federal',
      agencyUseOnlyBox: {
        text: 'Code number',
        height: '80px',
        width: '190px',
        tn: '047',
        textAfterTn: true,
        tnPosition: 'centre',
        textAbove: ' '
      },
      repeatFormData: {
        titleNum: '301'
      },
      formFooterNum: 'T1131 E (11)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this form to claim a tax credit for qualified labour expenditures of a qualified ' +
              'corporation. The corporation must have incurred the expenditures for a production that the ' +
              'Minister of Canadian Heritage certified as a Canadian film or video production.'
            },
            {
              label: 'To claim this credit, include the following with your <i>T2 Corporation Income Tax Return</i> ' +
              'for the tax year:',
              sublist: [
                'Canadian film or video production certificate "A" (or a copy) issued by the Canadian ' +
                'Audio-Visual Certification Office (CAVCO);',
                'if applicable, the certificate of completion "B" (or a copy) issued by CAVCO and a copy of ' +
                'the audited statement of production costs and accompanying notes provided to CAVCO; and',
                'a completed copy of this form for each film or video production. We consider each episode ' +
                'in a series to be a production. However, we will accept one form for episodes in a series ' +
                'that are certified Canadian film or video productions.'
              ]
            },
            {
              label: 'For information on claiming this tax credit, go to' +
              ' www.cra.gc.ca/filmservices'.link('http://www.cra-arc.gc.ca/filmservices/') +
              ' or refer to Guide RC4164, <i>Canadian Film or Video Production Tax Credit - Guide to Form T1131</i>.'
            }
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Contact Information (please print)',
        'rows': [
          {
            'type': 'table',
            'num': '100'
          }
        ]
      },
      {
        'header': 'Part 2 - Identifying the Canadian film or video production',
        'rows': [
          {
            'type': 'splitTable',
            'fieldAlignRight': true,
            'side1': [
              {
                'label': 'Title of production'
              },
              {
                'type': 'infoField',
                'num': '301',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '175'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '301'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'CAVCO reference number (for a certificate issued before April 1, 2010)',
                'labelClass': 'fullLength'
              },
              {
                type: 'table',
                num: '106'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'For a series of episodes, enter range of CAVCO certificate numbers<br> (that start with A or B) that were issued before April 1, 2010',
                'labelClass': 'tabbed2 fullLength'
              }
            ],
            'side2': [
              {
                'label': 'Date principal photography began'
              },
              {
                'type': 'infoField',
                'inputType': 'date',
                'num': '302',
                'tn': '302'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'CAVCO certificate number',
                'num': '304',
                'tn': '304',
                'inputType': 'custom',
                'format': [
                  'A{N6}',
                  'B{N6}',
                  '{N9}'
                ]
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'From',
                'num': '305',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '7',
                        'max': '7'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '305',
                'inputType': 'custom',
                'format': [
                  'AC{N5}'
                ]
              },
              {
                'type': 'infoField',
                'label': 'To',
                'num': '306',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '7',
                        'max': '7'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '306',
                'inputType': 'custom',
                'format': [
                  'AC{N5}'
                ]
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'label': '1 . Is the production a Canadian co-production involving only qualified corporations?',
            'inputType': 'radio',
            'num': '311',
            'tn': '311'
          },
          {
            'type': 'infoField',
            'label': '2 . Is the production a treaty co-production?',
            'inputType': 'radio',
            'num': '312',
            'tn': '312'
          },
          {
            'type': 'infoField',
            'label': '3 . Is the production co-owned by a prescribed person?',
            'inputType': 'radio',
            'num': '313',
            'tn': '313'
          }
        ]
      },
      {
        'header': 'Part 3 - Eligibility',
        'rows': [
          {
            'type': 'infoField',
            'label': '1 . Were the activities of the corporation primarily the carrying on of a Canadian film or video production business through a permanent establishment in Canada?',
            'inputType': 'radio',
            'num': '330',
            'tn': '330',
            'init': '2'
          },
          {
            'type': 'infoField',
            'label': '2 . Was all or part of the corporation\'s taxable income exempt from Part I tax at any time in the tax year?',
            'inputType': 'radio',
            'num': '335',
            'tn': '335',
            'init': '2'
          },
          {
            'type': 'infoField',
            'label': '3 . Was the corporation at any time in the tax year controlled directly or indirectly in any way by one or more persons, all or part of whose taxable income was exempt from Part I tax?',
            'inputType': 'radio',
            'num': '340',
            'tn': '340',
            'init': '2'
          },
          {
            'type': 'infoField',
            'label': '4 . Was the corporation at any time in the tax year a prescribed labour-sponsored venture capital corporation?',
            'inputType': 'radio',
            'num': '345',
            'tn': '345',
            'init': '2'
          },
          {
            'type': 'infoField',
            'label': '5 . Is the production, or an interest in a person or partnership that directly or indirectly has an interest in the production, a tax shelter investment for purposes of section 143.2?',
            'inputType': 'radio',
            'num': '350',
            'tn': '350',
            'init': '2'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If you answered <b>no</b> to question 1 or <b>yes</b> to any other question, you are <b>not eligible</b> for the Canadian film or video production tax credit.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'type': 'infoField',
            'label': 'Is the corporation eligible for the Canadian film or video production tax credit.',
            'inputType': 'radio',
            'num': '351',
            'init': '1'
          }
        ]
      },
      {
        'header': 'Part 4 - Production commencement time',
        'rows': [
          {
            'label': 'The production commencement time, as defined in subsection 125.4(1) of the <i>Income Tax Act</i>,' +
            ' is the <b>earliest</b> of these dates:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'label': 'a) the date principal photography began; and',
            'inputType': 'date',
            'num': '651',
            'tn': '651'
          },
          {
            'label': 'b) the <b>latest</b> of:'
          },
          {
            'type': 'infoField',
            'label': 'i) the date the first script labour expenses were incurred;',
            'labelCellClass': 'tabbed',
            'inputType': 'date',
            'num': '652',
            'tn': '652'
          },
          {
            'type': 'infoField',
            'label': 'ii) the date the production rights were acquired; <b>and</b>',
            'labelClass': 'tabbed',
            'inputType': 'date',
            'num': '653',
            'tn': '653'
          },
          {
            'type': 'infoField',
            'label': 'iii) two years before the date principal photography began',
            'labelClass': 'tabbed',
            'inputType': 'date',
            'num': '654',
            'tn': '654'
          }
        ]
      },
      {
        'header': 'Part 5 - Production cost limit',
        'rows': [
          {
            'label': 'Cumulative production cost as at the end of the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '421',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '421'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Total government and non-government assistance that the corporation has not repaid',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '423',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '423'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Subtotal (amount A <b>minus</b> amount B)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '424'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'type': 'table',
            'num': 'di_table_1'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Qualified labour expenditures for all previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '427',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '427'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Production cost limit (amount D minus amount E)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '430',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '430'
                }
              }
            ],
            'indicator': 'F'
          }
        ]
      },
      {
        'header': 'Part 6 - Qualified labour expenditure',
        'rows': [
          {'label': '<b>Labour expenditure for the tax year</b> is the total of:'},
          {
            'label': 'Salary or wages paid that are directly attributable to the production',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '601',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '601'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Remuneration directly attributable to the production and paid to:'
          },
          {
            'label': '- individuals',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '603',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '603'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': '- other taxable Canadian corporations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '605',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '605'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': '- taxable Canadian corporations (solely owned by an individual)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '606',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '606'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': '- partnerships carrying on business in Canada (for their members or employees)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '607',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '607'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': 'Labour expenditure transferred under a reimbursement agreement by the corporation, a wholly owned subsidiary, to the parent corporation that is a taxable Canadian corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '609',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '609'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'label': 'Labour expenditure incurred in the tax year (total of amounts a to f)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '608'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '610'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Labour expenditures for all previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '611',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '611'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': 'Total labour expenditures (amount G <b>plus</b> amount H)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '612'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Qualified labour expenditures for all previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '613',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '613'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'g'
                }
              }
            ]
          },
          {
            'label': 'Labour expenditure transferred under a reimbursement agreement by the parent corporation, that is a taxable Canadian corporation, to the corporation, a wholly owned subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '615',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '615'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'h'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount g <b>plus</b> amount h)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '616'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '617'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': 'Labour expenditure for the tax year (amount I minus amount J)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '618',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '618'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': 'Qualified labour expenditure (the lesser of amount F in Part 5 and amount K)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '619'
                }
              }
            ],
            'indicator': 'L'
          }
        ]
      },
      {
        'header': 'Part 7 - Canadian film or video production tax credit',
        'rows': [
          {
            'type': 'table',
            'num': 'di_table_2'
          },
          {
            'label': 'Enter amount M on line 796 of your <i>T2 Corporation Income Tax Return</i>. ' +
            'If you are filing more than one Form T1131, add amount M from all the forms and enter the total on line 796 of your T2 return. ',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  };
})();
