(function() {

  wpw.tax.create.calcBlocks('t2a4960', function(calcUtils) {
    calcUtils.calc(function (calcUtils, field) {
      var numArr = [135,137,139,141,143,145,147,149,151,153,155,157,159,161];
      var total = 0;
      numArr.forEach(function (num) {
        total+= field(num).get()
      });
      field('170').assign(total);
    });
  });
})();
