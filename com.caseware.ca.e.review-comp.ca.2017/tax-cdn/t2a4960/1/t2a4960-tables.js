(function() {
  wpw.tax.create.tables('t2a4960', {
    '998':{
      hasTotals: true,
      columns: [
        {
          header: '101<br>Project title<br>(use same information as line 200 from part 2 of federal T661)'
        },
        {
          header: '103<br>Project code<br>(line 206 of federal T661)'
        },
        {
          header: '105<br>Portion of federal T661 line 559 incurred in Alberta for each project before Alberta SR & ED tax credit for the year',
          total: true
        },
        {
          header: '107<br>Portion of federal T661 line 559 incurred outside of Alberta for each project',
          total: true
        },
        {
          header: '109<br>Total salaries and wages incurred in SR & ED in Alberta for each project',
          total: true
        },
        {
          header: '111<br>Total prescribed proxy amount included in Alberta portion of federal line 559 (if claimed federally)',
          total: true
        },
        {
          header: '113<br>Alberta proxy amount for each project(if field 111 is applicable)',
          total: true
        }
      ]
    },
    '999': {
      fixedRows: true,
      columns: [
        {header: 'Jurisdiction', type: 'none'},
        {header: 'Amount Incurred'},
        {header: 'Jurisdiction', type: 'none'},
        {header: 'Amount Incurred'}
      ],
      cells: [
        {
          0: {label: 'Alberta'},
          1: {tn: '135', num: '135'},
          2: {label: 'Nunavut'},
          3: {tn: '149', num: '149'}
        },
        {
          0: {label: 'British Columbia'},
          1: {tn: '137', num: '137'},
          2: {label: 'Ontario'},
          3: {tn: '151', num: '151'}
        },
        {
          0: {label: 'Manitoba'},
          1: {tn: '139', num: '139'},
          2: {label: 'Prince Edward Island'},
          3: {tn: '153', num: '153'}
        },
        {
          0: {label: 'New Brunswick'},
          1: {tn: '141', num: '141'},
          2: {label: 'Quebec'},
          3: {tn: '155', num: '155'}
        },
        {
          0: {label: 'Newfoundland and Labrador'},
          1: {tn: '143', num: '143'},
          2: {label: 'Saskatchewan'},
          3: {tn: '157', num: '157'}
        },
        {
          0: {label: 'Northwest Territories'},
          1: {tn: '145', num: '145'},
          2: {label: 'Yukon'},
          3: {tn: '159', num: '159'}
        },
        {
          0: {label: 'Nova Scotia'},
          1: {tn: '147', num: '147'},
          2: {label: 'Other'},
          3: {tn: '161', num: '161'}
        },
        {
          1: {type: 'none'},
          2: {label: 'Total of all Jurisdictions:', labelClass: 'bold'},
          3: {tn: '170', num: '170'}
        }
      ]
    }
  })
})();
