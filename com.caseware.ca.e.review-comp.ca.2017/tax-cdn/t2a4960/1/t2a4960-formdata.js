(function () {
  wpw.tax.create.formData('t2a4960', {
    formInfo: {
      abbreviation: 't2a4960',
      title: 'Listing of Sr&ED Projects Claimed In Alberta',
      formFooterNum: 'AT4960.0901',
      // headerImage: 'canada-alberta',
      showCorpInfo: true,
      category: 'Alberta Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            labelClass: 'fullLength'
          },
          {
            label: 'This form is to be filed with the Alberta Scientific Research & Experimental Development (SR & ED) Tax Credit Schedule 9.',
            labelClass: 'bold fullLength'
          },
          {
            label: 'Report all monetary values in dollars; DO NOT include cents.'
          },
          {
            label: 'For each Alberta SR & ED project, provide the following information.'
          },
          {labelClass: 'fullLength'},
          {type: 'table', num: '998'},
          {labelClass: 'fullLength'},
          {
            label: 'If there are more projects, please use additional forms as necessary.'
          },
          {label: 'Jurisdiction(s) where federal qualified expenditures (line 559 of federal T661) were incurred.'},
          {type: 'table', num: '999'}
        ]
      }
    ]
  });
})();
