(function() {
  wpw.tax.global.tableCalculations.t2s421 = {
    "1000": {
      "infoTable": "true",
      "fixedRows": "true",
      "columns": [{
        "width": "75%",
        "type": "none"
      },
        {
          "maxLength": 13
        }],
      "cells": [{
        "0": {
          "label": "British Columbia Free Miner Certificate Number",
          "labelClass": "fullLength"
        },
        "1": {
          "num": "010", "validate": {"or": [{"length": {"min": "1", "max": "13"}}, {"check": "isEmpty"}]},
          "tn": "010",
          type: 'text'
        }
      }]
    },
    "1100": {
      "infoTable": "true",
      "fixedRows": "true",
      "columns": [{
        "width": "48%",
        cellClass: 'alignLeft',
        "maxLength": 80
      },
        {},
        {
          "width": "48%",
          cellClass: 'alignLeft',
          "maxLength": 80
        }],
      "cells": [{
        "0": {
          "num": "020", "validate": {"or": [{"length": {"min": "1", "max": "80"}}, {"check": "isEmpty"}]},
          "tn": "020",
          type: 'text'
        },
        "1": {
          "type": "none"
        },
        "2": {
          "num": "040", "validate": {"or": [{"length": {"min": "1", "max": "80"}}, {"check": "isEmpty"}]},
          "tn": "040",
          type: 'text'
        }
      },
        {
          "0": {
            "num": "030", "validate": {"or": [{"length": {"min": "1", "max": "80"}}, {"check": "isEmpty"}]},
            "tn": "030",
            type: 'text'
          },
          "1": {
            "type": "none"
          },
          "2": {
            "num": "050", "validate": {"or": [{"length": {"min": "1", "max": "80"}}, {"check": "isEmpty"}]},
            "tn": "050",
            type: 'text'
          }
        }]
    },
    "1200": {
      "infoTable": false,
      "showNumbering": true,
      "maxLoop": 100000,
      "columns": [
        {
          "header": "<b>Project name</b>",
          "width": "35%",
          "num": "070",
          "tn": "070",
          "maxLength": 80,
          type: 'text'
        },
        {
          "header": "<b>Mineral title</b>",
          "width": "30%",
          "num": "080",
          "tn": "080",
          "maxLength": 80,
          type: 'text'
        },
        {
          "header": "<b>Mining division</b>",
          "width": "35%",
          "num": "090",
          "tn": "090",
          "maxLength": 80,
          type: 'text'
        }
      ]
    },
    "1300": {
      hasTotals: true,
      "showNumbering": "",
      "maxLoop": 100000,
      paddingRight: 'std-input-col-padding-width',
      "columns": [
        {
          "header": "<b>Description</b>",
          "num": "135",
          "tn": "135",
          "maxLength": 80,
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          "header": "<b>Amount</b>",
          "num": "140",
          "tn": "140",
          colClass: 'std-input-width',
          "total": true,
          "totalNum": "141",
          hiddenTotal: true
        }
      ]
    },
    "1400": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [{
        "width": "20px",
        "type": "none"
      },
        {
          "width": "30%"
        },
        {
          "width": "10px",
          "type": "none"
        },
        {
          "width": "30%"
        },
        {
          "width": "10px",
          "type": "none"
        },
        {}],
      "cells": [{
        "0": {
          "label": "I,",
          "labelClass": "fullLength"
        },
        "1": {
          "num": "300",
          type: 'text'
        },
        "3": {
          "num": "301",
          type: 'text'
        },
        "5": {
          "num": "302",
          type: 'text'
        }
      },
        {
          "1": {
            "label": "(Surname in block letters)",
            "labelClass": "center",
            "type": "none"
          },
          "3": {
            "label": "(First name in block letters)",
            "labelClass": "center",
            "type": "none"
          },
          "5": {
            "label": "Position, office, or rank",
            "labelClass": "center",
            "type": "none"
          }
        }]
    },
    "1500": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "width": "20px",
          "type": "none"
        },
        {
          "width": "25%",
          "type": "date",
          cellClass: 'alignCenter'
        },
        {
          "width": "10px",
          "type": "none"
        },
        {"type": "text"},
        {
          "width": "10px",
          "type": "none"
        },
        {
          "width": "25%"
        }],
      "cells": [
        {
          "1": {
            "num": "303",
            "labelClass": "center"
          },
          "3": {
            "num": "304"
          },
          "5": {
            "num": "305"
          }
        },
        {
          "1": {
            "label": "Date",
            "labelClass": "center",
            "type": "none"
          },
          "3": {
            "label": "Signature of an authorized signing officer of the corporation",
            "labelClass": "center",
            "type": "none"
          },
          "5": {
            "label": "Telephone number",
            "labelClass": "center",
            "type": "none"
          }
        }
      ]
    }
  }
})();
