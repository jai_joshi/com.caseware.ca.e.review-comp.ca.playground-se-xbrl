(function() {
  'use strict';

  wpw.tax.global.formData.t2s421 = {
    formInfo: {
      abbreviation: 'T2S421',
      title: 'British Columbia Mining Exploration Tax Credit',
      //TODO: DO NOT DELETE THESE 2 LINE: subtitle and code
      //subTitle: '(2008 and later tax years)',
      schedule: 'Schedule 421',
      code: 'Code 1602',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 421 E (17)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule to claim the British Columbia mining exploration tax credit (METC). This tax credit is available to a corporation that has a permanent establishment (under section 400 of the federal <i>Income Tax Regulations</i>) in British Columbia, and that has incurred qualified mining exploration expenses in the tax year. For tax years ending before January 1, 2017, you must apply for the credit no later than 36 months after the end of the tax year for which you are claiming the credit. For tax years ending after December 31, 2016, you must apply for the credit no later than 18 months after the end of the tax year for which you are claiming the credit.'
            },
            {
              label: 'The British Columbia METC for the tax year is equal to 20% of the amount by which the total qualified ' +
              'mining exploration expenses incurred in the tax year<b> exceed</b> the total ' +
              'assistance received or receivable for these expenses.'
            },
            {
              label: 'Qualified mining exploration expenses incurred after February 20, ' +
              '2007, in prescribed areas affected by the Mountain Pine Beetle are eligible ' +
              'for a METC enhanced by 10% The prescribed Mountain Pine Beetle affected areas are defined ' +
              'by the BC Mining Exploration Tax Credit Regulation.'
            },
            {
              label: 'You are<b> not eligible</b> to claim the METC if, at any time in the tax ' +
              'year:',
              sublist: [
                'the corporation is exempt from tax under section 27 of the <i>British Columbia Income Tax Act</i>;',
                'all or part of the corporation\'s taxable income was exempt from tax under ' +
                'Part I of the federal<i>Income Tax Act</i>;',
                'the corporation is controlled directly or indirectly in any manner ' +
                'whatever by one or more persons, all or part of whose income is exempt from tax under section 27' +
                'of the <i>British Columbia Income Tax Act</i> or under Part I of the federal <i>Income Tax Act</i>; or',
                'the corporation was:',
                ' a prescribed labour-sponsored venture capital corporation ' +
                'under subsection 6701(c) of the federal <i>Income Tax Regulations;</i>',
                'a small business venture capital corporation registered under section ' +
                '3 of the <i>Small Business Venture Capital Act</i>; or',
                'a corporation that has registered an employee share ownership plan ' +
                'under section 2 of the <i>Employee Investment Act</i>, or is an employee venture ' +
                'capital corporation registered under section 8 of that same act.'
              ]
            },
            {
              label: 'If you are a member of a partnership, other than a specified member under subsection 248(1) of the federal <i>Income Tax Act</i>, you can claim your appropriate portion of the METC earned on qualified mining exploration expenses incurred by the partnership. A specified member includes any limited partner. Complete and attach Form T1249, <i>British Columbia Mining Exploration Tax Credit Partnership Schedule</i> to calculate your proportionate share, and enter this amount on line' +
              '220. If your only source of the METC is from a partnership, <b>only</b> enter the calculated amount from Form T1249 at line 220 ' +
              'of this schedule.'
            },
            {
              label: 'File a copy of this schedule with your T2 Corporation <i>Income Tax Return.</i>'
            }
          ]
        }
      ],
      category: 'British Columbia Forms'
    },
    sections: [
      {
        'header': 'Freedom of Information and Protection of <i>Privacy Act</i> (FOIPPA)',
        'rows': [
          {
            'label': 'The personal information on this form is collected for the purpose of administering the <i>Income Tax Act</i> (British Columbia) under the authority of paragraph 26(a) of the FOIPPA. Questions about the collection or use of this information can be directed to the Manager,  Intergovernmental Relations, PO Box 9444 Stn Prov Govt, Victoria BC V8W 9W8. (Telephone: Victoria at 250-387-3332 or toll-free at 1-877-387-3332) and ask to be re-directed. Email: ITBTaxQuestions@gov.bc.ca',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 1 - Exploration information',
        'rows': [
          {
            'label': 'A mineral resource that qualifies for the credit means: a deposit of base or precious metal, coal, bituminous sands or oil shale, and certain other deposits in which the principal mineral extracted is ammonite gemstone, calcium chloride, diamond, gypsum, halite, kaolin, sylvite, silica extracted from sandstone or quartzite, and deposits certified by the Minister of Natural Resources that the principal mineral extracted therefrom is an industrial mineral contained in a non-bedded deposit.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'label': 'List mineral(s) for which exploration has taken place:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For qualified mining exploration expenses reported in Part 2, identify each project, mineral title, and mining division where title is registered. If there were no mineral titles, identify the project and mining division only. Attach additional schedules if more space is required.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1200'
          }
        ]
      },
      {
        'header': 'Part 2 - Qualified mining exploration expenses*',
        'rows': [
          {
            'label': 'Qualified mining exploration expenses are expenses that the corporation incurred after July 31, 1998, and before January 1, 2020, for goods and services that are all or substantially all provided in British Columbia. and that are incurred for the purpose of determining the existence, location, extent or quality of a\n' +
            'mineral resource in British Columbia. The Budget 2017 Update proposed to include in qualified mining exploration expenses such expenses that are' +
            'incurred after February 28, 2015 for environmental studies or community consultations undertaken to obtain a right, licence or privilege.' +
            'Expenses must be reasonable in the circumstances and not be an expense that has been claimed by another person, other than an eligible taxpayer claiming their proportionate share as a member of a partnership.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'By category, enter the total qualified mining exploration expenses incurred in the tax year for mineral titles listed in Part 1.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Prospecting',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              }
            ]
          },
          {
            'label': 'Geological, geophysical, or geochemical surveys',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              }
            ]
          },
          {
            'label': 'Drilling by rotary, diamond, percussion, or other methods',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '120',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              }
            ]
          },
          {
            'label': 'Trenching, digging test pits, and preliminary sampling',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              }
            ]
          },
          {
            'label': 'Other qualified mining exploration expenses. Attach additional schedules if more space is required.'
          },
          {
            'type': 'table',
            'num': '1300'
          },
          {
            'label': 'Total other qualified mining exploration expenses',
            'labelClass': 'text-right ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '142'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '143'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'forceBreakAfter': true,
            'label': '<b>Total qualified mining exploration expenses (add </b>lines 100 to 130, and Amount A)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '180',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '180'
                }
              }
            ]
          },
          {
            type: 'description',
            lines: [
              {
                type: 'list', listType: 'asterisk',
                items: [
                  {
                    'label': 'Expenses that <b>do not</b> qualify for this credit include:',
                    sublist: [
                      'an amount that is renounced under subsection 66(12.6) of the federal Income Tax Act,' +
                      ' in respect of an expense incurred after July 30, 2001 and before January 1, 2018, or ' +
                      'an amount to which subsection 66(12.66) of the federal Income Tax Act applies after' +
                      ' December 31, 2017 and before January 1, 2019;',
                      'any amount renounced and reported to an investor on a T101, ' +
                      '<i>Statement of Resource Expenses</i> or T5013, <i>Statement of Partnership Income</i>;',
                      'any expenses related to a mine that has come into production in reasonable commercial ' +
                      'quantities, or to a potential or actual extension of such a mine;',
                      'a Canadian development expense (CDE) under subsection 66.2(5) of the federal <i>Income' +
                      ' Tax Act</i>, other than an expense incurred after February 28, 2015, for environmental ' +
                      'studies or community consultations undertaken to obtain a right, licence or privilege for ' +
                      'the purpose of determining the existence, location, extent or quality of mineral resource' +
                      ' in British Columbia;',
                      'a Canadian exploration and development overhead expense (CEDOE), under the federal ' +
                      '<i>Income Tax Act and Regulations</i>. A CEDOE includes an expense for the administration,' +
                      ' management, or financing of the corporation, and salary, wages, or other remuneration' +
                      ' or related benefits paid to a person employed by the corporation whose duties were' +
                      ' not all or substantially all directed towards exploration or development activities.' +
                      ' It also includes payments for taxes, insurance and maintenance for rental or leasing ' +
                      'property on which there has been no substantial exploration or development activities;',
                      'an outlay or expense included in the capital cost of depreciable property;',
                      'any consideration for any share or any interest in or right to a share;',
                      'a cost of, or for the use of, seismic data referred to in paragraph 66(12.6)(b.1) of the ' +
                      'federal <i>Income Tax Act</i>;',
                      'those expenses incurred in drilling or completing an oil or gas well,' +
                      ' including the cost of building a temporary access road or in preparing the site; and',
                      'an outlay or expense incurred in the course of earning income if any of the income' +
                      ' is exempt income, under subsection 248(1), or is exempt from tax ' +
                      'under Part I, of the federal <i>Income Tax Act</i>.'
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 3 - Determining the amount of assistance',
        'rows': [
          {
            'label': 'All assistance* received in respect of qualified mining exploration expenses for the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '190',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '190'
                }
              }
            ]
          },
          {
            'label': 'Assistance that have been repaid in respect of qualified mining exploration expenses for the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ]
          },
          {
            'label': '<b>Net assistance</b> (line 190 <b>minus</b> line 200)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '210',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '210'
                }
              }
            ]
          },
          {
            'label': '* Grants, subsidies, rebates, forgivable loans, and reimbursements the corporation received, is entitled to receive, or can reasonably be expected to receive' +
            ' other than an amount deemed to have been paid under the section 25.1 of the <i>Income Tax Act </i>(British Columbia).',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 4 - Determining the mining exploration tax credit',
        'rows': [
          {
            'label': 'Total qualified mining exploration expenses (line 180 in Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '211'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Net assistance (line 210 in Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '212'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Net qualified mining exploration expenses (amount B <b>minus</b> amount C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '213'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'label': 'Amount D<b> multiplied by</b> the applicable rate <b>x</b> ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '214',
                  'disabled': true
                }
              },
              {
                'input': {
                  'num': '216',
                  'disabled': true
                },
                'padding': {
                  'type': 'text',
                  'data': '<b>%=</b>'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Qualified mining exploration expenses incurred after February 20, 2007, in prescribed' +
            ' areas affected by the Mountain Pine Beetle (included at line 180) <b>minus</b> net assistance' +
            ' for these expenses (included in at line 210).',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '215',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '215'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': ''
                }
              }
            ]
          },
          {
            'label': 'Line 215 <b>multiplied by</b> the applicable rate <b>x </b>',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '217',
                  'disabled': true
                }
              },
              {
                'input': {
                  'num': '218',
                  'disabled': true
                },
                'padding': {
                  'type': 'text',
                  'data': '%='
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'METC allocated from a partnership. Enter your proportionate share from Form T1249, from line 22 of Part 5',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '220',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '220'
                }
              }
            ],
          },
          {
            'label': '<b>Mining exploration tax credit (add </b>amounts E, F, and line 220)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '221'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            label: 'Enter this amount on line 673 in Part 2 of Schedule 5 <i>Tax Calculation Supplementary - Corporations</i>.'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            label: 'See the privacy notice on your return.',
            labelClass: 'absoluteAlignRight'
          }
        ]
      }
      // {
      //   'header': 'Certification',
      //   'fieldAlignRight': true,
      //   'rows': [
      //     {
      //       'type': 'table',
      //       'num': '1400'
      //     },
      //     {
      //       'labelClass': 'fullLength'
      //     },
      //     {
      //       'label': 'am an authorized signing officer of the corporation. I certify that I have examined this
      // schedule, and that the information given on this schedule, and all attached documents, is to the best of my
      // knowledge, correct and complete.', 'labelClass': 'fullLength' }, { 'type': 'table', 'num': '1500' }, { 'type':
      // 'table', 'num': '1600' } ] }
    ]
  };
})();
