(function() {
  wpw.tax.create.diagnostics('t2s421', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('4210001', common.prereq(common.check(['t2s5.673'], 'isNonZero'), common.requireFiled('T2S421')));

    diagUtils.diagnostic('4210002', common.prereq(common.and(
        common.requireFiled('T2S421'),
        common.check(['t2s5.673'], 'isNonZero')),
        function(tools) {
          var table = tools.field('1200');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne([row[0], row[1], row[2]], 'isNonZero') &&
                tools.requireOne(tools.list(['020', '030', '040', '050']), 'isNonZero');
          });
        }));

    diagUtils.diagnostic('4210003', common.prereq(common.and(
        common.requireFiled('T2S421'),
        common.check(['t2s5.673'], 'isNonZero')),
        common.check(['100', '110', '120', '130', '140', '220'], 'isNonZero')));

    diagUtils.diagnostic('4210004', common.prereq(common.requireFiled('T2S421'),
        function(tools) {
          var table = tools.field('1300');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[0].isEmpty() && row[1].isNonZero())
              return row[0].require('isFilled');
            else return true;
          });
        }));

    diagUtils.diagnostic('421.180', common.prereq(common.and(
        common.requireFiled('T2S421'),
        common.check(['180'], 'isNonZero')),
        function(tools) {
          var table = tools.field('1300');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne(tools.list(['100', '110', '120', '130']).concat(row[1]), 'isNonZero');
          });
        }));
  });
})();
