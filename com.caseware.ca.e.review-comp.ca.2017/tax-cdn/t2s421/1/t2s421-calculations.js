(function() {

  wpw.tax.create.calcBlocks('t2s421', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //update from rate table
      calcUtils.getGlobalValue('214', 'ratesBc', '2030');
      calcUtils.getGlobalValue('217', 'ratesBc', '2031');

      //Part2 calcUtils
      calcUtils.equals('142', '141');
      calcUtils.equals('143', '142');
      calcUtils.sumBucketValues('180', ['100', '110', '120', '130', '143']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part3 calcUtils
      calcUtils.subtract('210', '190', '200');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part4 calcUtils
      calcUtils.equals('211', '180');
      calcUtils.equals('212', '210');
      calcUtils.subtract('213', '211', '212');
      calcUtils.multiply('216', ['213', '214'], 1 / 100);
      calcUtils.multiply('218', ['215', '217'], 1 / 100);
      //TODO; get num 220 from T1249
      calcUtils.sumBucketValues('221', ['216', '218', '220']);
      calcUtils.getGlobalValue('303', 'CP', '955');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //certification
      calcUtils.getGlobalValue('300', 'CP', '950');
      calcUtils.getGlobalValue('301', 'CP', '951');
      calcUtils.getGlobalValue('302', 'CP', '954');
      calcUtils.getGlobalValue('303', 'CP', '955');
      calcUtils.getGlobalValue('305', 'CP', '956');
      calcUtils.getGlobalValue('306', 'CP', '957');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      if (field('306').get() == 1) {
        calcUtils.getGlobalValue('307', 'CP', '958');
        calcUtils.getGlobalValue('308', 'CP', '959');
      }
      else {
        calcUtils.getGlobalValue('307', 'CP', '958');
        calcUtils.getGlobalValue('308', 'CP', '959');
      }
    });


  });
})();
