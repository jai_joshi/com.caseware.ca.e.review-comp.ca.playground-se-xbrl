(function () {
  wpw.tax.create.formData('t2a12', {
    formInfo: {
      abbreviation: 't2a12',
      title: 'Alberta Income/Loss Reconciliation',
      schedule: 'Schedule 12',
      formFooterNum: 'AT112 (Mar-14)',
      // headerImage: 'canada-alberta',
      showCorpInfo: true,
      category: 'Alberta Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: 'The Alberta Corporate Tax Act adopts most rules of the federal Income Tax Act for determining ' +
            'income and taxable income. However, some of the rules are elective and allow corporations to claim ' +
            'different amounts for Alberta purposes than they have for federal purposes. This results in different ' +
            'discretionary Alberta and federal tax account balances. The differences between the Alberta and ' +
            'federal resource tax regimes from 2003 onwards also lead to different income federally and for ' +
            'Alberta purposes.<b> If the corporation elects to differ its claim for Alberta purposes in the ' +
            'current year, if the opening balances for Alberta and federal purposes differ, or if the ' +
            'corporation has crown charges, Alberta Royalty Tax Credit claims or resource allowance in 2003 ' +
            'and onwards, then Schedule 12 MUST be completed and submitted with the applicable supporting Alberta schedule(s).</b> ',
            labelClass: 'fullLength'
          },
          {
            label: 'Report all monetary amounts in dollars; DO NOT indicate cents. Show negative amounts in brackets ( ).'
          }
        ]
      },
      {
        header: 'AREA A - NET INCOME FOR ALBERTA CORPORATE INCOME TAX PURPOSES',
        rows: [
          {
            'label': '<b>Net Income (Loss) for federal purposes</b> from T2 line 300',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '002'
                },
                'padding': {
                  'type': 'tn',
                  'data': '002'
                }
              }
            ]
          },
          {
            label: 'Only specify the federal and Alberta amount of the items that are calculated differently for Alberta purposes or where the opening\n' +
            'balance for Alberta purposes differs from the federal opening balance.',
            labelClass: 'fullLength'
          },
          {
            label: 'If these amounts are the <b>same</b>, DO NOT indicate the amount for either federal or Alberta purposes.',
            labelClass: 'fullLength'
          },
          {
            type: 'table',
            num: '999'
          },
          {
            label: '<b>Net Income (Loss) for Alberta purposes:</b> Line 002 - line 050 + line 052',
            num: '054',
            tn: '054',
            inputClass: 'std-input-col-padding-width',
            type: 'infoField',
            showDots: false,
            filters: 'number',
            indicatorColumn: true,
            textAlign: 'right'
          },
          {
            type: 'infoField',
            label: 'If amount in Line 040, provide explanation:',
            inputClass: 'std-input-col-width-3',
            tn: '048',
            'indicatorColumn': true
          }
        ]
      },
      {
        header: 'AREA B - TAXABLE INCOME FOR ALBERTA',
        rows: [
          {
            label: 'For this section, all of the following items that determine taxable income must be specified. If the opening balance or the claim for the\n' +
            'current year for donations, gifts or losses are different for Alberta purposes than for federal purposes, complete the applicable Alberta\n' +
            'schedule(s) and enter the amount from those schedule(s) below. Otherwise, enter the amounts from the federal T2 for these items and any\n' +
            'other applicable line items.',
            labelClass: 'fullLength'
          },
          {
            type: 'table', num: '998'
          },
          {
            type: 'table', num: '997'
          },
          {
            label: '<b>Taxable Income for Alberta purposes or (Loss)</b>  Lines 054 - 080 + 082',
            num: '090',
            tn: '090',
            inputClass: 'std-input-col-padding-width',
            type: 'infoField',
            valueType: 'number',
            filters: 'number',
            indicatorColumn: true,
            textAlign: 'right'
          },
          {
            label: 'If there is an amount at line 082 and line 054 - line 080 is negative, then line 090 must equal line 082'
          },
          {
            label: 'Enter this amount on AT1 page 2, line 062',
            labelClass: 'bold italic'
          }
        ]
      },
      {
        rows: [
          {
            label: 'To be completed only by insurers of farmers and fishermen:<br>Income exempt under ITA paragraph 149(1)(t) (T2 Line 370)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '092'
                },
                'padding': {
                  'type': 'tn',
                  'data': '092'
                }
              }
            ]
          },
          {
            label: 'Carry forward the amount of line 090 - 092 to the AT1 page 2, line 062.'
          }
        ]
      },
      {
        header: 'Reconciliation of Active Business Income (ABI):',
        rows: [
          {
            label: 'Does the corporation\'s calculation of ABI for Alberta purposes differ from its federal ABI? ',
            type: 'infoField',
            inputType: 'radio',
            num: '100',
            tn: '100'
          },
          {
            label: 'If "Yes", complete the following to reconcile the two amounts'
          },
          {
            label: 'Active Business Income from Federal Schedule 7, the calculated value* of amount "Q" or federal Schedule 16, line 124',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '102'
                },
                'padding': {
                  'type': 'tn',
                  'data': '102'
                }
              }
            ]
          },
          {
            label: '* If the calculated amount is negative, enter the negative amount in brackets ( ). If this includes\n' +
            'specified partnership income, ensure the correct Alberta small business threshold is used (See Guide)'
          },
          {
            label: 'Adjustment to ABI for Alberta purposes due to discretionary items (See Guide)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '104'
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              }
            ]
          },
          {
            label: 'Show a negative amount in brackets ( ).'
          },
          {
            label: 'Active Business Income for Alberta purposes (ABI)<br> Line 102 + 104 (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '106'
                },
                'padding': {
                  'type': 'tn',
                  'data': '106'
                }
              }
            ]
          },
          {
            label: 'This amount is to be used for Schedule 1, line 003'
          }
        ]
      }
    ]
  });
})();
