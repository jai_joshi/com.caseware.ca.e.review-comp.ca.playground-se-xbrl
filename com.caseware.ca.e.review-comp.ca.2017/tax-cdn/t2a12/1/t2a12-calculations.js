(function () {

  function checkFlagInput(fieldNum, flag, TrueArray, FalseArray, calcUtils, field) {
    //TrueArray is an array where the filedNum is filled with [form name, field number] when flag is true
    //FalseArray is an array where the filedNum is filled with [form name, field number] when flag is false/undefined
    if (flag) {
      calcUtils.getGlobalValue(fieldNum, TrueArray[0], TrueArray[1]);
    } else {
      if (FalseArray.length == 1) {
        field(fieldNum).assign(field(FalseArray[0]).get());
      } else {
        calcUtils.getGlobalValue(fieldNum, FalseArray[0], FalseArray[1]);
      }
    }
  }

  wpw.tax.create.calcBlocks('t2a12', function (calcUtils) {
    calcUtils.calc(function (calcUtils, field) {
      calcUtils.getGlobalValue('002', 'T2J', '300');
      calcUtils.getGlobalValue('002-1', 'T2J', '300');
      calcUtils.getGlobalValue('005', 'T2S1', '403');
      calcUtils.getGlobalValue('007', 'T2S1', '107');
      calcUtils.getGlobalValue('009', 'T2S1', '404');
      calcUtils.getGlobalValue('011', 'T2S1', '405');
      calcUtils.getGlobalValue('013', 'T2S1', '108');
      calcUtils.getGlobalValue('015', 'T2S1', '224');
      calcUtils.getGlobalValue('017', 'T2S1', '309');
      calcUtils.getGlobalValue('019', 'T2S1', '229');
      calcUtils.getGlobalValue('021', 'T2S1', '313');
      calcUtils.getGlobalValue('023', 'T2S1', '344');
      calcUtils.getGlobalValue('027', 'T2S1', '341');
      calcUtils.getGlobalValue('029', 'T2S1', '340');
      calcUtils.getGlobalValue('031', 'T2S1', '345');
      calcUtils.getGlobalValue('033', 'T2S1', '342');
      field('035').assign(
        field('t2s1.231').get() -
        field('t2s1.411').get()
      );
      field('035').source(field('t2s1.231'));
      calcUtils.getGlobalValue('037', 'T2S1', '125');
      calcUtils.getGlobalValue('039', 'T2S1', '413');
      field('41a').assign(
        field('t2s1.113').get() -
        field('t2s1.406').get() +
        field('t2s1.296').get()
      );
      field('41a').source(field('t2s1.113'));
      field('41b').assign((-1) * field('t2s21.101').total(3).get());
      field('41b').source(field('t2s21.101'));
      field('41c').assign((-1) * field('t2s1.218').get());
      field('41c').source(field('t2s1.218'));
      field('041').assign(
        field('t2s1.113').get() -
        field('t2s1.406').get() +
        field('t2s1.296').get() -
        field('t2s21.101').total(3).get() -
        field('t2s1.218').get()
      );
      field('050').assign(
        field('005').get() * (-1) +
        field('007').get() -
        field('009').get() -
        field('011').get() +
        field('013').get() +
        field('015').get() -
        field('017').get() +
        field('019').get() -
        field('021').get() -
        field('023').get() -
        field('027').get() -
        field('029').get() -
        field('031').get() -
        field('033').get() +
        field('035').get() +
        field('037').get() -
        field('039').get() +
        field('041').get()
      );
    });
    //AREA A ALBERTA FIELDS
    calcUtils.calc(function (calcUtils, field) {
      calcUtils.getGlobalValue('004', 'T2A13', '027');
      calcUtils.getGlobalValue('006', 'T2A13', '023');
      calcUtils.getGlobalValue('008', 'T2A13', '025');
      field('010').assign(field('T2A14.023').get() + field('T2A14.024').get());
      field('010').source(field('T2A14.023'));
      calcUtils.getGlobalValue('012', 'T2A14', '040');
      field('022').assign(field('T2A15.007').get() + field('T2A15.019').get() + field('T2A15.031').get());
      field('022').source(field('T2A15.007'));
      field('026').assign(field('T2A15.061').get() + field('T2A15.081').get());
      field('026').source(field('T2A15.061'));
      field('028').assign(field('T2A15.115').get() + field('T2A15.141').get());
      field('028').source(field('T2A15.115'));
      field('030').assign(
        field('T2A15.209').get() +
        field('T2A15.221').get() +
        field('T2A15.253T').get() +
        field('T2A15.273T').get() +
        field('T2A15.293T').get() +
        field('T2A15.313T').get()
      );
      field('030').source(field('T2A15.209'));
      field('032').assign(field('T2A15.169').get() + field('T2A15.189').get());
      field('032').source(field('T2A15.169'));
      if (field('T2A16.16').get() < 0) {
        calcUtils.getGlobalValue('034', 'T2A16', '016');
      } else {
        calcUtils.getGlobalValue('034', 'T2A16', '020');
      }
      calcUtils.getGlobalValue('036', 'T2A17', '091');
      calcUtils.getGlobalValue('038', 'T2A17', '081');
      var Sch15Totals = 0;
      //todo T2A15 was removed, those the area below is commented out. This will be added back in if we implement T2A15
      //add negative of T2A15 AREA C
      /*if (field('T2A15.997').cell(13, 1).get() < 0) {
        Sch15Totals -= field('T2A15.997').cell(13, 1).get();
      }
      if (field('T2A15.997').cell(13, 2).get() < 0) {
        Sch15Totals -= field('T2A15.997').cell(13, 2).get();
      }
      //add negative of T2A15 AREA D
      if (field('T2A15.996').cell(14, 1).get() < 0) {
        Sch15Totals -= field('T2A15.996').cell(14, 1).get();
      }
      if (field('T2A15.996').cell(14, 2).get() < 0) {
        Sch15Totals -= field('T2A15.996').cell(14, 2).get();
      }
      //add negative of T2A15 AREA F
      if (field('T2A15.994').cell(4, 1).get() < 0) {
        Sch15Totals -= field('T2A15.994').cell(4, 1).get();
      }
      if (field('T2A15.994').cell(4, 2).get() < 0) {
        Sch15Totals -= field('T2A15.994').cell(4, 2).get();
      }
      // T2A15 AREA G & H
      if (field('T2A15.989').value().totals[1] < 0) {
        Sch15Totals -= field('T2A15.989').value().totals[1];
      }
      if (field('T2A15.985').value().totals[1] < 0) {
        Sch15Totals -= field('T2A15.985').value().totals[1];
      }*/
      field('40a').assign(
        field('T2A18.076').get() +
        field('T2A18.094').get() +
        Sch15Totals
      );
      field('040').assign(
        field('40a').get() +
        field('40b').get() +
        field('40c').get()
      );
      field('052').assign(
        field('004').get() * (-1) +
        field('006').get() -
        field('008').get() -
        field('010').get() +
        field('012').get() +
        field('014').get() -
        field('016').get() +
        field('018').get() -
        field('020').get() -
        field('022').get() -
        field('026').get() -
        field('028').get() -
        field('030').get() -
        field('032').get() -
        field('034').get() +
        field('036').get() -
        field('038').get() +
        field('042').get() +
        field('040').get()
      );
    });

    calcUtils.calc(function (calcUtils, field) {
      field('054').assign(
        field('002').get() -
        field('050').get() +
        field('052').get()
      );
      field('054-1').assign(field('054').get());
    });
    //AREA B

    calcUtils.calc(function (calcUtils, field) {
      calcUtils.getGlobalValue('057', 'T2J', '311');
      field('059').assign(
        field('T2J.312').get() +
        field('T2J.313').get() +
        field('T2J.314').get()
      );
      field('059').source(field('T2J.312'));
      calcUtils.getGlobalValue('061', 'T2J', '320');
      calcUtils.getGlobalValue('063', 'T2J', '325');
      calcUtils.getGlobalValue('065', 'T2J', '331');
      calcUtils.getGlobalValue('067', 'T2J', '332');
      calcUtils.getGlobalValue('069', 'T2J', '333');
      calcUtils.getGlobalValue('071', 'T2J', '334');
      calcUtils.getGlobalValue('073', 'T2J', '335');
      calcUtils.getGlobalValue('075', 'T2J', '340');
      calcUtils.getGlobalValue('079', 'T2J', '350');
      field('081').assign(
        field('057').get() +
        field('059').get() +
        field('061').get() +
        field('063').get() +
        field('065').get() +
        field('067').get() +
        field('069').get() +
        field('071').get() +
        field('073').get() +
        field('075').get() +
        field('079').get()
      );
      calcUtils.getGlobalValue('083', 'T2J', '355');
    });

    calcUtils.calc(function (calcUtils, field) {
      var AlbertaFlag = wpw.tax.utilities.flagger(calcUtils);
      checkFlagInput('056', AlbertaFlag['T2A20'], ['T2A20', '016'], ['057'], calcUtils, field);
      checkFlagInput('058', AlbertaFlag['T2A20'], ['T2A20', '076'], ['059'], calcUtils, field);
      calcUtils.getGlobalValue('060', 'T2J', '320');
      calcUtils.getGlobalValue('062', 'T2J', '325');
      checkFlagInput('064', AlbertaFlag['T2A21'], ['T2A21', '041'], ['065'], calcUtils, field);
      field('066').assign(field('T2A21.061').get() * field('ratesAb.003').get() / 100);
      field('066').source(field('T2A21.061'));
      checkFlagInput('068', AlbertaFlag['T2A21'], ['T2A21', '099'], ['069'], calcUtils, field);
      checkFlagInput('070', AlbertaFlag['T2A21'], ['T2A21', '079'], ['071'], calcUtils, field);
      if (AlbertaFlag['T2A21']) {
        field('072').assign(field('T2A21.996').value().totals[4]);
      } else {
        field('072').assign(field('073').get());
      }
      calcUtils.getGlobalValue('074', 'T2J', '340');
      calcUtils.getGlobalValue('078', 'T2J', '350');
      field('080').assign(
        field('056').get() +
        field('058').get() +
        field('060').get() +
        field('062').get() +
        field('064').get() +
        field('066').get() +
        field('068').get() +
        field('070').get() +
        field('072').get() +
        field('074').get() +
        field('078').get()
      );
      checkFlagInput('082', AlbertaFlag['T2A21'], ['T2A21', '017'], ['083'], calcUtils, field);
    });

    calcUtils.calc(function (calcUtils, field) {
      field('091').assign(
        field('002').get() -
        field('081').get() +
        field('083').get()
      );

      if (field('082').get() != 0 && ((field('054').get() - field('080').get()) < 0)) {
        field('090').assign(field('082').get())
      } else {
        field('090').assign(
          field('054').get() -
          field('080').get() +
          field('082').get()
        )
      }

      field('092').assign(field('T2J.370').get());
    });

    calcUtils.calc(function (calcUtils, field) {
      if (field('100').get() == 1) {
        var fed400 = field('t2j.400').get();
        if (fed400 > 0) {
          field('102').assign(fed400);
        } else {
          //todo: add calcs when T2S16 is created
          calcUtils.removeValue('102');
        }
        // field('104').assign(field('052').get() - field('050').get() - field('040').get() + field('041').get());//todo investigate
        field('104').assign(field('052').get() - field('050').get());
        field('106').assign(Math.max(field('102').get() + field('104').get(), 0));
      } else {
        calcUtils.removeValue(['102', '104', '106'], true);
      }
    });

  });
})();
