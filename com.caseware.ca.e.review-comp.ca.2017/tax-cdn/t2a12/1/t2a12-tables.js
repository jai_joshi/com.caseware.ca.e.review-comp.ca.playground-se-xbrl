(function () {

  wpw.tax.create.tables('t2a12', {
    '999': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'none'},
        {type: 'none', colClass: 'small-input-width', labelClass: 'font-large bold'},
        {type: 'none', colClass: 'std-padding-width'},
        {header: 'Federal Dollar Amount', labelClass: 'font-xsmall', colClass: 'std-input-col-padding-width'},
        {type: 'none', colClass: 'small-input-width', labelClass: 'font-large bold'},
        {type: 'none', colClass: 'std-padding-width'},
        {header: 'Alberta Dollar Amount', labelClass: 'font-xsmall', colClass: 'std-input-col-padding-width'},
        {type: 'none', colClass: 'std-padding-width'}
      ],
      cells: [
        {
          0: {label: 'Capital Cost Allowance'},
          1: {label: '-'},
          2: {tn: '005'},
          3: {label: 'Federal Schedule 1 line 403', num: '005'},
          4: {label: '-'},
          5: {tn: '004'},
          6: {label: 'Schedule 13 line 027', num: '004'}
        },
        {
          0: {label: 'Recapture of CCA'},
          1: {label: '+'},
          2: {tn: '007'},
          3: {label: 'Federal Schedule 1 line 107', num: '007'},
          4: {label: '+'},
          5: {tn: '006'},
          6: {label: 'Schedule 13 line 023', num: '006'}
        },
        {
          0: {label: 'Terminal Loss'},
          1: {label: '-'},
          2: {tn: '009'},
          3: {label: 'Federal Schedule 1 line 404', num: '009'},
          4: {label: '-'},
          5: {tn: '008'},
          6: {label: 'Schedule 13 line 025', num: '008'}
        },
        {
          0: {label: 'Cumulative Eligible Capital Deduction'},
          1: {label: '-'},
          2: {tn: '011'},
          3: {label: 'Federal Schedule 1 line 405', num: '011'},
          4: {label: '-'},
          5: {tn: '010'},
          6: {label: 'Schedule 14 line 023 + 024', num: '010'}
        },
        {
          0: {label: 'Gain on Sale of Eligible Capital Property'},
          1: {label: '+'},
          2: {tn: '013'},
          3: {label: 'Federal Schedule 1 line 108', num: '013'},
          4: {label: '-'},
          5: {tn: '012'},
          6: {label: 'Schedule 14 line 040', num: '012'}
        },
        {
          0: {label: 'Farming Inventory:<br> - Mandatory inventory adjustment included in current year'},
          1: {label: '+'},
          2: {tn: '015'},
          3: {label: 'Federal Schedule 1 line 224', num: '015'},
          4: {label: '+'},
          5: {tn: '014'},
          6: {label: '', num: '014'}
        },
        {
          0: {label: '- Mandatory inventory adjustment included in prior year', labelClass: 'indent-2'},
          1: {label: '-'},
          2: {tn: '017'},
          3: {label: 'Federal Schedule 1 line 309', num: '017'},
          4: {label: '-'},
          5: {tn: '016'},
          6: {label: '', num: '016'}
        },
        {
          0: {label: '- Optional value of inventory included in current year', labelClass: 'indent-2'},
          1: {label: '+'},
          2: {tn: '019'},
          3: {label: 'Federal Schedule 1 line 229', num: '019'},
          4: {label: '+'},
          5: {tn: '018'},
          6: {label: '', num: '018'}
        },
        {
          0: {label: '- Optional value of inventory included in prior year', labelClass: 'indent-2'},
          1: {label: '-'},
          2: {tn: '021'},
          3: {label: 'Federal Schedule 1 line 313', num: '021'},
          4: {label: '-'},
          5: {tn: '020'},
          6: {label: '', num: '020'}
        },
        {
          0: {label: 'Depletion'},
          1: {label: '-'},
          2: {tn: '023'},
          3: {label: 'Federal Schedule 1 line 344', num: '023'},
          4: {label: '-'},
          5: {tn: '022'},
          6: {label: 'Schedule 15 lines 007 + 019 + 031', num: '022'}
        },
        {
          0: {label: 'Canadian Exploration Expenses (CEE)'},
          1: {label: '-'},
          2: {tn: '027'},
          3: {label: 'Federal Schedule 1 line 341', num: '027'},
          4: {label: '-'},
          5: {tn: '026'},
          6: {label: 'Schedule 15 lines 061 + 081', num: '026'}
        },
        {
          0: {label: 'Canadian Development Expenses (CDE)'},
          1: {label: '-'},
          2: {tn: '029'},
          3: {label: 'Federal Schedule 1 line 340', num: '029'},
          4: {label: '-'},
          5: {tn: '028'},
          6: {label: 'Schedule 15 lines 115 + 141', num: '028'}
        },
        {
          0: {label: 'Foreign Exploration and Development Expenses'},
          1: {label: '-'},
          2: {tn: '031'},
          3: {label: 'Federal Schedule 1 line 345', num: '031'},
          4: {label: '-'},
          5: {tn: '030'},
          6: {label: 'Schedule 15 lines 209 + 221 + I + R + JJ + SS', num: '030'}
        },
        {
          0: {label: 'Canadian Oil and Gas Property Expenses (COGPE)'},
          1: {label: '-'},
          2: {tn: '033'},
          3: {label: 'Federal Schedule 1 line 342', num: '033'},
          4: {label: '-'},
          5: {tn: '032'},
          6: {label: 'Schedule 15 lines 169 + 189', num: '032'}
        },
        {
          0: {label: 'Scientific Research Expenses claimed in year'},
          1: {label: ''},
          2: {tn: '035'},
          3: {label: '- Federal Schedule 1 line 411<br>+ Federal Schedule 1 line 231', num: '035'},
          4: {label: '-'},
          5: {tn: '034'},
          6: {label: 'Schedule 16 line 016 OR line 020', num: '034'}
        },
        {
          0: {label: 'Tax Reserves deducted in prior year'},
          1: {label: '+'},
          2: {tn: '037'},
          3: {label: 'Federal Schedule 1 line 125', num: '037'},
          4: {label: '+'},
          5: {tn: '036'},
          6: {label: 'Schedule 17 line 091', num: '036'}
        },
        {
          0: {label: 'Tax Reserves claimed in current year'},
          1: {label: '-'},
          2: {tn: '039'},
          3: {label: 'Federal Schedule 1 line 413', num: '039'},
          4: {label: '-'},
          5: {tn: '038'},
          6: {label: 'Schedule 17 line 081', num: '038'}
        },
        {
          0: {label: 'Capital Tax Liability in other provinces'},
          1: {label: ''},
          3: {type: 'none'},
          4: {label: '+'},
          5: {tn: '042'},
          6: {label: 'Capital tax liability in other provinces', num: '042'}
        },
        {
          0: {label: '<b>Other:</b>'},
          3: {
            label: '+ Fed Schedule 1 line 113 minus 406 <br>' +
            '+ Fed Schedule 1 Other Additions<br>',
            num: '41a'
          },
          6: {label: '+ Schedule 18 line 076 + 094<br>' +
          '+ Schedule15 AREAs C, D, F, G & H<br>', num: '40a'}
        },
        {
          3: {
            label: '- Fed Schedule 21 Part 1 column D<br>',
            num: '41b'
          },
          6: {label: '- ACTA subsection 8(2.2) deduction<br>', num: '40b'}
        },
        {
          3: {
            label: '- Fed Schedule 1 line 218',
            num: '41c'
          },
          6: {label: '+ Foreign affiliate property income<br>' +
            'after ITA s.152(6.1) adjustment', num: '40c'}
        },
        {
          0: {label: 'Other - Attach supporting schedule'},
          2: {tn: '041'},
          3: {num: '041'},
          5: {tn: '040'},
          6: {num: '040'}
        },
        {
          1: {label: 'Total Federal Amount', labelClass: 'font-medium'},
          2: {tn: '050'},
          3: {label: '', num: '050'},
          4: {label: 'Total Alberta Amount', labelClass: 'font-medium'},
          5: {tn: '052'},
          6: {label: '', num: '052'}
        }
      ]
    },
    '998': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'none'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {header: 'Federal', labelClass: 'font-xsmall', colClass: 'std-input-col-padding-width'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {header: 'Alberta', labelClass: 'font-xsmall', colClass: 'std-input-col-padding-width'},
        {type: 'none', colClass: 'std-padding-width'}
      ],
      cells: [
        {
          0: {label: 'Net Income (Loss) for carried forward from page 1:', labelClass: 'bold'},
          2: {tn: '002'},
          3: {num: '002-1'},
          5: {tn: '054'},
          6: {num: '054-1'}
        }
      ]
    },
    '997': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {header: 'Deduct:', type: 'none', headerClass: 'leftAlign'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {header: 'Federal Dollar Amount of Deduction', labelClass: 'font-xsmall', colClass: 'std-input-col-padding-width'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {header: 'Alberta Dollar Amount of Deduction', labelClass: 'font-xsmall', colClass: 'std-input-col-padding-width'},
        {type: 'none', colClass: 'std-padding-width'}
      ],
      cells: [
        {
          0: {label: 'Charitable Donations'},
          2: {tn: '057'},
          3: {num: '057', label: 'T2 line 311 '},
          5: {tn: '056'},
          6: {num: '056', label: 'Schedule 20 line 016 '}
        },
        {
          0: {label: 'Gifts to Canada or a province, cultural gifts and ecological gifts'},
          2: {tn: '059'},
          3: {num: '059', label: 'T2 lines 312 + 313 + 314 '},
          5: {tn: '058'},
          6: {num: '058', label: 'Schedule 20 line 076 '}
        },
        {
          0: {label: 'Taxable dividends deductible under ITA section 112, 113 or 138(6)'},
          2: {tn: '061'},
          3: {num: '061', label: 'T2 line 320'},
          5: {tn: '060'},
          6: {num: '060', label: 'T2 line 320'}
        },
        {
          0: {label: 'Part VI.1 tax deduction'},
          2: {tn: '063'},
          3: {num: '063', label: 'T2 line 325'},
          5: {tn: '062'},
          6: {num: '062', label: 'T2 line 325'}
        },
        {
          0: {label: 'Non-capital losses of preceding taxation years'},
          2: {tn: '065'},
          3: {num: '065', label: 'T2 line 331 '},
          5: {tn: '064'},
          6: {num: '064', label: 'Schedule 21 line 041'}
        },
        {
          0: {label: 'Net-capital losses of preceding taxation years<br>(See Guide for the Inclusion Rate calculation)'},
          2: {tn: '067'},
          3: {num: '067', label: 'T2 line 332'},
          5: {tn: '066'},
          6: {num: '066', label: 'Schedule 21 line 061 x Inclusion Rate '}
        },
        {
          0: {label: 'Restricted farm losses of preceding taxation years'},
          2: {tn: '069'},
          3: {num: '069', label: 'T2 line 333'},
          5: {tn: '068'},
          6: {num: '068', label: 'Schedule 21 line 099'}
        },
        {
          0: {label: 'Farm losses of preceding taxation years'},
          2: {tn: '071'},
          3: {num: '071', label: 'T2 line 334 '},
          5: {tn: '070'},
          6: {num: '070', label: 'Schedule 21 line 079'}
        },
        {
          0: {label: 'Limited partnership losses of preceding taxation years'},
          2: {tn: '073'},
          3: {num: '073', label: 'T2 line 335 '},
          5: {tn: '072'},
          6: {num: '072', label: 'Schedule 21 total of column 139 '}
        },
        {
          0: {label: 'Taxable capital gains or taxable dividends allocated from a central credit union\n'},
          2: {tn: '075'},
          3: {num: '075', label: 'T2 line 340'},
          5: {tn: '074'},
          6: {num: '074', label: 'T2 line 340'}
        },
        {
          0: {label: 'Prospector\'s and grubstaker\'s shares'},
          2: {tn: '079'},
          3: {num: '079', label: 'T2 line 350'},
          5: {tn: '078'},
          6: {num: '078', label: 'T2 line 350'}
        },
        {
          0: {label: 'Subtotal:', cellClass: 'alignRight'},
          2: {tn: '081'},
          3: {num: '081', label: ''},
          5: {tn: '080'},
          6: {num: '080', label: ''}
        },
        {
          0: {label: '<b>Add:</b> ITA section 110.5 and/or subparagraph<br>' +
          '115(1)(a)(vii) additions (see Guide)'},
          2: {tn: '083'},
          3: {num: '083', label: 'T2 line 355'},
          5: {tn: '082'},
          6: {num: '082', label: 'Schedule 21 line 017 '}
        },
        {
          0: {label: '<b>Taxable Income for Federal purposes\n' +
          'or (Loss)</b> Lines 002 - 081 + 083'},
          2: {tn: '091'},
          3: {num: '091'},
          6: {type: 'none'}
        }
      ]
    }
  })
})();
