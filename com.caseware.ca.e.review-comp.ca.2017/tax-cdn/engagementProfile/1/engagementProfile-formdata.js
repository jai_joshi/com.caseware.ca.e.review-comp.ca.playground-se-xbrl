(function() {
  'use strict';

  wpw.tax.global.formData.engagementProfile = {
    formInfo: {
      abbreviation: 'engagementProfile',
      title: 'Engagement Profile',
      headerImage: 'cw',
      highlightFieldsets: true,
      description: [
        //{type: 'infoField', inputType: 'radio', label: 'Would you like to enter staff directly?'}, //TODO: default N
        //{type: 'infoField', inputType: 'radio', label: 'Allocate staff working on this return in Collaborate'}, //TODO: default Y
        //{text: 'This is the LINK to go to Collaborate'}
      ],
      category: 'Start Here'
    },
    sections: [
      {
        header: 'Tax Monitor Window',
        rows: [
          {
            type: 'table',
            num: 'taxWindowControl'
          }
        ]
      }
      // {
      //   hideFieldset: true,
      //   rows: [
      //     {
      //       label: 'Allocate staff working on this return in Collaborate?',
      //       type: 'infoField',
      //       inputType: 'radio',
      //       num: '102',
      //       labelClass: 'fullLength',
      //       init: '1'
      //     },
      //     {label: 'This is the LINK to go to Collaborate.'}
      //   ]
      // },
      // {
      //   header: '1 - Staff Working On This Return',
      //   rows: [
      //     {
      //       type: 'infoField',
      //       inputType: 'functionButton',
      //       btnLabel: 'Get collaborate data',
      //
      //       labelWidth: '1%',
      //       paramFn: wpw.tax.actions.getTimeData
      //     },
      //     {label: ''},
      //     {
      //       label: 'These names will be included in a drop-down list that you can use to designate the partner, ' +
      //       'preparer, reviewer and the person assigned to the file in the \'Internal information for the ' +
      //       'preparer\' section of Form Identification, Identification and Other Client Information.',
      //       labelClass: 'fullLength'
      //     },
      //     {type: 'table', num: '200'}
      //   ]
      // },
      // {
      //   header: '2 - Budget vs Actual For This Return',
      //   rows: [
      //     {type: 'horizontalLine'},
      //     {labelClass: 'fullLength boldUnderline center', label: 'Budget vs Actual - # of Hours'},
      //     {type: 'horizontalLine'},
      //     {type: 'table', num: '300'},
      //     {labelClass: 'fullLength'},
      //     {type: 'horizontalLine'},
      //     {type: 'table', num: '1011'},
      //     {labelClass: 'fullLength'},
      //     {type: 'horizontalLine'},
      //     {labelClass: 'fullLength boldUnderline center', label: 'Budget vs Actual - $'},
      //     {type: 'horizontalLine'},
      //     {type: 'table', num: '345'},
      //     {type: 'table', num: '1012'}
      //   ]
      // }
        //todo: hide it for Beta1 -2/8/2018 Bucky as per Alnoor
      /*{
        header: '3 - Return Processing Deadlines, etc.',
        rows: [
          {
            type: 'infoField',
            label: 'Date return has to be filed with CRA',
            inputType: 'date',
            num: '405'
          },
          {
            type: 'infoField',
            label: 'Internal date when return has to be completed',
            inputType: 'date',
            num: '410'
          },
          {type: 'infoField', label: 'Date Actually Filed', inputType: 'date', num: '415'},
          {
            type: 'infoField',
            label: 'Will the client come to the office to sign the T2 BCR or printed return',
            inputType: 'radio',
            num: '420'
          },
          {
            type: 'infoField',
            label: 'How will the return be delivered',
            inputType: 'dropdown',
            num: '425',
            options: [
              {option: 'N/A', value: '0'},
              {option: 'Mail', value: '1'},
              {option: 'Courier', value: '2'},
              {option: 'Delivered Personally', value: '3'},
              {option: 'Collaborate', value: '4'},
              {option: 'Email', value: '5'},
              {option: 'Other', value: '6'}
            ]
          }
        ]
      },
      {
        header: '4 - Tax Return Processing Status',
        rows: [
          {
            type: 'infoField',
            label: 'Date Audit or Review Commenced in WP',
            inputType: 'date',
            num: '505'
          },
          {
            type: 'infoField',
            label: 'Date Roll Forward or Data Conversion of Tax Return',
            inputType: 'date',
            num: '510'
          }
          /!* //todo: comment it out for now as per Alnoor for the Demo - Bucky Jan 23/2017
          { labelClass: 'fullLength bold'},
          {type: 'horizontalLine'},
          {
            labelClass: 'fullLength boldUnderline center',
            label: 'Forms Flagged As Applicable To Complete This Tax Return:'
          },
          {type: 'horizontalLine'},
          {
            type: 'table', num: '515'
          },
          { labelClass: 'fullLength'},
          {type: 'infoField', label: 'Date Client Approval Received', inputType: 'date', num: '530'},
          {type: 'infoField', label: 'Date E-Filed', inputType: 'date', num: '535'},
          {type: 'infoField', label: 'Date Approved By CRA', inputType: 'date', num: '540'}
          *!/
        ]
      }*/
    ]
  };
})();
