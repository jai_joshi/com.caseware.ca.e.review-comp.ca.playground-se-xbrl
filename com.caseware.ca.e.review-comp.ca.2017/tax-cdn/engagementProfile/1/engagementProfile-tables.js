(function() {

  var columnWidth = '80px';
  var columnSpaceWidth = '25px';

  var preConditionIndex = 0;
  var listPercentage = 0;

  function getCalculationTableHeading() {
    return [
      {type: 'none', disabled: true},
      {type: 'none', colClass: 'std-padding-width'},
      {
        colClass: 'std-input-width',
        header: '<b>Not Started<b>',
        type: 'singleRadioButton',
        value: 'ns',
        name: 'status'
      },
      {type: 'none', colClass: 'std-spacing-width'},
      {
        colClass: 'std-input-width',
        header: '<b>Work In Progress<b>',
        type: 'singleRadioButton',
        value: 'wip',
        name: 'status'
      },
      {type: 'none', colClass: 'std-spacing-width'},
      {
        colClass: 'std-input-width',
        header: '<b>Completed<b>',
        type: 'singleRadioButton',
        value: 'completed',
        name: 'status'
      },
      {type: 'none', colClass: 'std-spacing-width'},
      {
        colClass: 'std-input-width',
        header: '<b>Percentage of completion</b>',

        cellClass: 'alignRight',
        disabled: true,
        init: 0,
        total: true,
        totalNum: '1515'
      },
      {type: 'none', colClass: 'std-spacing-width'}
    ]
  }

  function getSectionHeading(sectionHeading, assignedPercentage, isList) {
    return [
      {
        0: {label: sectionHeading, labelClass: 'bold'},
        2: {type: isList ? 'none' : 'singleRadioButton'},
        4: {type: isList ? 'none' : 'singleRadioButton'},
        6: {type: isList ? 'none' : 'singleRadioButton'},
        assignedPercentage: assignedPercentage,
        isList: isList
      }
    ]
  }

  function getNgShowOption() {
    return [
      {
        0: {label: 'Show details ?', labelClass: 'bold tabbed'},
        2: {type: 'radio', init: '2'},
        4: {type: 'none'},
        6: {type: 'none'},
        8: {type: 'none'},
        isNgShowOption: true
      }
    ]
  }

  function getTableRows(labelArray) {
    var tableRows = [];

    if (!angular.isArray(labelArray))
      labelArray = [labelArray];

    for (var i = 0; i < labelArray.length; i++) {
      var rowDataObj = labelArray[i];
      var inputRow = {
        0: {label: rowDataObj.label, labelClass: 'tabbed'},
        8: {type: 'none'}
      };

      inputRow.isConditionalRow = true;
      inputRow.isNgShowRow = true;
      inputRow.listLength = labelArray.length;

      tableRows.push(inputRow);
    }
    return tableRows
  }

  //Commented out as unused code while working on STAX-2428

  // function applyNgShowToCells() {
  //   var updatedCells = [];
  //
  //   var prevConditionRIndex = 0;
  //   for (var rIndex = 0; rIndex < tableCells.length; rIndex++) {
  //     var cellRow = tableCells[rIndex];
  //     var updatedCellRowObj = angular.merge({}, cellRow);
  //
  //     //check if it is the form Section Heading
  //     if (cellRow[0].labelClass == 'bold' || cellRow[0].labelClass == 'bold tabbed') {
  //       if (cellRow.isNgShowOption) {
  //         prevConditionRIndex = rIndex;
  //       }
  //       updatedCells.push(updatedCellRowObj);
  //       continue;
  //     }
  //
  //     if (cellRow.isConditionalRow && !cellRow.isNgShowRow) {
  //       updatedCells.push(updatedCellRowObj);
  //       continue;
  //     }
  //
  //     updatedCellRowObj.showWhen = '515-show' + prevConditionRIndex;
  //     updatedCells.push(updatedCellRowObj);
  //   }
  //
  //   return updatedCells;
  // }

  // var tableCells = getSectionHeading('Created', 5).concat
  // (
  //     getSectionHeading('Start Checklist', 15, true),
  //     getNgShowOption(),
  //     getTableRows([
  //       {label: 'RC59'},
  //       {
  //         label: 'Has GIFI coding been checked for reasonableness and comparability ' +
  //         '(manually input OR import from other package such as WP or Taxprep)?'
  //       },
  //       {label: 'Receipts'},
  //       {label: 'Tax Documents'},
  //       {label: 'Other items'}
  //     ]),
  //     getSectionHeading('Prepare tax return', 25),
  //     getSectionHeading('Waiting on Queries', 7.5),
  //     getSectionHeading('Queries Ready', 7.5),
  //     getSectionHeading('Review', 10),
  //     getSectionHeading('Approved for EFILE', 5),
  //     getSectionHeading('EFILE Accepted', 5),
  //     getSectionHeading('Client Communication', 15),
  //     getSectionHeading('Completed / Locked', 5)
  // );

  wpw.tax.global.tableCalculations.engagementProfile = {
    // "200": {
    //   "infoTable": true,
    //   "repeats": ["300", "345"],
    //   "superHeaders": [
    //     {
    //       "colspan": "4"
    //     },
    //     {
    //       "header": " ",
    //       "colspan": "2"
    //     }
    //   ],
    //   "columns": [
    //     {
    //       "header": "First Name",
    //       "width": "17%",
    //       cellClass: 'alignCenter',
    //       type: 'text'
    //     },
    //     {
    //       "header": "Last Name",
    //       "width": "22%",
    //       cellClass: 'alignCenter',
    //       type: 'text'
    //     },
    //     {
    //       "header": "Staff Number",
    //       "width": "10%",
    //       cellClass: 'alignCenter',
    //       type: 'text'
    //     },
    //     {
    //       "header": "Rate",
    //       "width": "7%",
    //       filters: 'prepend $'
    //     },
    //     {
    //       "header": "# of hours",
    //       "width": "7%",//,
    //       filters: 'prepend $'
    //     },
    //     {
    //       "header": "Amount",
    //       "width": "7%",
    //       filters: 'prepend $'
    //     },
    //     {
    //       "header": "Partner",
    //       "type": "singleRadioButton",
    //       "value": "partner",
    //       "name": "position"
    //     },
    //     {
    //       "header": "Reviewer",
    //       "type": "singleRadioButton",
    //       "value": "reviewer",
    //       "name": "position"
    //     },
    //     {
    //       "header": "Preparer",
    //       "type": "singleRadioButton",
    //       "value": "preparer",
    //       "name": "position"
    //     }
    //   ]
    // },
    // "300": {
    //   "infoTable": true,
    //   hasTotals: true,
    //   "dividers": [4],
    //   "fixedRows": true,
    //   "superHeaders": [
    //     {
    //       "index": "1"
    //     },
    //     {
    //       "header": "Current Year",
    //       "colspan": "3",
    //       "divider": true
    //     },
    //     {
    //       "header": "Previous Year",
    //       "colspan": "2"
    //     }],
    //   "columns": [
    //     {
    //       "header": "",
    //       "type": "none",
    //       "width": "20%"
    //     },
    //     {
    //       "header": "Budget",
    //       "total": true,
    //       cellClass: 'alignCenter'
    //     },
    //     {
    //       "header": "W.I.P",
    //       "total": true,
    //       cellClass: 'alignCenter'
    //     },
    //     {
    //       "header": "Final",
    //       "total": true,
    //       cellClass: 'alignCenter'
    //     },
    //     {
    //       "header": "Budget",
    //       "total": true,
    //       cellClass: 'alignCenter'
    //     },
    //     {
    //       "header": "Final",
    //       "total": true,
    //       cellClass: 'alignCenter'
    //     }
    //   ],
    //   "startTable": "200"
    // },
    "1011": {
      "infoTable": true,
      "dividers": [4],
      "fixedRows": true,
      "columns": [
        {
          "type": "none",
          "width": "20%"
        },
        {"type": "none"},
        {header: '<b>Budget vs. Final for the Current Year</b>'},
        {"type": "none"},
        {"type": "none"},
        {header: '<b>Budget vs. Final for the Prior Year</b>'},
        {"type": "none", colClass: 'small-input-width'}
      ],
      cells: [
        {
          2: {num: '335', decimals: 2},
          3: {label: '%'},
          5: {type: 'none'}
        },
        {
          2: {type: 'none'},
          5: {num: '340', decimals: 2},
          6: {label: '%'}
        }
      ]
    },
    // "345": {
    //   "infoTable": true,
    //   hasTotals: true,
    //   "dividers": [4],
    //   "fixedRows": true,
    //   "superHeaders": [
    //     {
    //       "index": "1"
    //     },
    //     {
    //       "header": "Current Year",
    //       "colspan": "3",
    //       "divider": true
    //     },
    //     {
    //       "header": "Previous Year",
    //       "colspan": "2"
    //     }
    //   ],
    //   "columns": [
    //     {
    //       "header": "",
    //       "type": "none",
    //       "width": "20%",
    //       "disabled": true
    //     },
    //     {
    //       "header": "Budget",
    //       "total": true,
    //       filters: 'prepend $',
    //       "disabled": true
    //     },
    //     {
    //       "header": "W.I.P",
    //       "total": true,
    //       filters: 'prepend $',
    //       "disabled": true
    //     },
    //     {
    //       "header": "Final",
    //       "total": true,
    //       filters: 'prepend $',
    //       "disabled": true
    //     },
    //     {
    //       "header": "Budget",
    //       "total": true,
    //       filters: 'prepend $',
    //       "disabled": true
    //     },
    //     {
    //       "header": "Final",
    //       "total": true,
    //       filters: 'prepend $',
    //       "disabled": true
    //     }
    //   ],
    //   "startTable": "200"
    // },
    "1012": {
      "infoTable": true,
      "dividers": [4],
      "fixedRows": true,
      "columns": [
        {
          "header": "",
          "type": "none",
          "width": "20%"
        },
        {"type": "none"},
        {header: '<b>Budget vs. Final for the Current Year</b>'},
        {"type": "none"},
        {"type": "none"},
        {header: '<b>Budget vs. Final for the Prior Year</b>'},
        {"type": "none", colClass: 'small-input-width'}
      ],
      cells: [
        {
          2: {num: '380', decimals: 2},
          3: {label: '%'},
          5: {type: 'none'}
        },
        {
          2: {type: 'none'},
          5: {num: '385', decimals: 2},
          6: {label: '%'}
        },
        {
          0: {label: 'Amount billed to date - 1', labelClass: 'bold'},
          2: {num: '390'},
          3: {label: '$'},
          5: {type: 'none'}
        },
        {
          0: {label: 'Contract amount', labelClass: 'bold'},
          2: {num: '395'},
          3: {label: '$'},
          5: {type: 'none'}
        }
      ]
    },
    "taxWindowControl": {
      "infoTable": true,
      "fixedRows": true,
      "columns": [
        {
          "type": "none",
          "width": "70%",
          cellClass: 'alignLeft'
        },
        {
          "header": "Tax Window",
          "width": "15%",
          type: 'singleCheckbox'
        },
        {
          "header": "5 Year History (Maximum of 4 will be displayed)",
          "width": "15%",
          type: 'singleCheckbox'
        }
      ],
      cells: [
        {
          0: {
            label: 'Net Income'
          },
          1: {
            num: "NetIncomeWindow",
            init: true
          },
          2: {
            num: "NetIncomeHistory",
            init: true
          }
        },
        {
          0: {
            label: 'Taxable Income'
          },
          1: {
            num: "TaxableIncomeWindow",
            init: true
          },
          2: {
            num: "TaxableIncomeHistory",
            init: true
          }
        },
        {
          0: {
            label: 'Part 1 Tax Payable'
          },
          1: {
            num: "PartITaxWindow",
            init: true
          },
          2: {
            num: "PartITaxHistory",
            init: true
          }
        },
        {
          0: {
            label: 'Other Federal Tax'
          },
          1: {
            num: "FederalTaxWindow",
            init: true
          },
          2: {
            num: "FederalTaxHistory",
            init: true
          }
        },
        {
          0: {
            label: 'Provincial Tax Payable'
          },
          1: {
            num: "ProvincialTaxWindow",
            init: true
          },
          2: {
            num: "ProvincialTaxHistory",
            init: true
          }
        },
        {
          0: {
            label: 'Small Business Deduction'
          },
          1: {
            num: "SmallBusinessDeductionWindow",
            init: true
          },
          2: {
            num: "SmallBusinessDeductionHistory",
            init: true
          }
        },
        {
          0: {
            label: 'Total Credits'
          },
          1: {
            num: "TotalCreditsWindow",
            init: true
          },
          2: {
            num: "TotalCreditsHistory",
            init: true
          }
        },
        {
          0: {
            label: 'Tax Payable/Refund'
          },
          1: {
            num: "TaxPayableWindow",
            init: true
          },
          2: {
            num: "TaxPayableHistory",
            init: true
          }
        }
      ]
    }
    // '515': {
    //   fixedRows: true, infoTable: true, executeAtEnd: 'true', scrollx: true, num: '515',
    //   hasTotals: true,
    //   columns: getCalculationTableHeading(),
    //   cells: applyNgShowToCells(),
    //   tableCalculations: function(paramsObj) {
    //
    //     var summaryRow = paramsObj.summaryRow;
    //     var summaryRowInfo = paramsObj.summaryRowInfo;
    //     var tableCalcs = paramsObj.tableCalcUtils;
    //     var numChanged = paramsObj.numChanged;
    //     var bucket = paramsObj.bucket;
    //     var rowIndex = paramsObj.rowIndex;
    //     var colIndex = paramsObj.colIndex;
    //     var calcUtils = paramsObj.calcUtils;
    //
    //     //assign percentage to completed task
    //     if (!summaryRowInfo.isList && !summaryRowInfo.isNgShowRow && !summaryRowInfo.isNgShowOption) {
    //       if (summaryRowInfo.status == 'completed') {
    //         summaryRow[8].assign(summaryRowInfo.assignedPercentage);
    //       }
    //     }
    //     else{
    //       //assign percentage to completed listed tasks
    //       if (summaryRowInfo.isList) {
    //         preConditionIndex = rowIndex;
    //         listPercentage = 0;
    //       }
    //       if (summaryRowInfo.isNgShowRow) {
    //         if (summaryRowInfo.status == 'completed') {
    //           listPercentage += bucket['1515'].value.cells[preConditionIndex].assignedPercentage / summaryRowInfo.listLength
    //         }
    //       }
    //       tableCalcs.field('515').getRow(preConditionIndex)[8].assign(listPercentage);
    //     }
    //   }
    // }
  }
})();
