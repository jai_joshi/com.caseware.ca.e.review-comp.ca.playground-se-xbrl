// (function() {
//
//   function sendAPIRequest(link, payload) {
//     try {
//       var xhr = new XMLHttpRequest();
//       xhr.open('POST', link, false); //jshint ignore: line
//       xhr.setRequestHeader('Content-Type', 'application/json');
//       var myJSON = JSON.stringify(payload);
//       xhr.send(myJSON);
//       if (xhr.status !== 200) {
//         return Error(xhr.status + ': ' + xhr.statusText);
//       }
//       var d = JSON.parse(xhr.responseText) || {};
//       return (d.d.result.Uuid || d);
//     } catch (e) {
//       return e;
//     }
//   }
//
//   function getUuid(calcUtils) {
//     var /** @type Object */ payload = {MachineID: wpw.tax.global.machineId};
//     return calcUtils._getCollabData('SessionService', 'LightweightLogin', payload);
//   }
//
//   function addStaff(list, item) {
//     var /** @type boolean */ newEntry = true;
//     for (var i = 0; i < list.length; i++) {
//       if (item.StaffId == list[i].StaffId) {
//         list[i].Amount = list[i].Amount + item.Amount;
//         list[i].Quantity = list[i].Quantity + item.Quantity;
//         list[i].BilledAmount = list[i].BilledAmount + item.BilledAmount;
//         newEntry = false;
//         break;
//       }
//     }
//     if (newEntry) {
//       list.push({
//         StaffId: item.StaffId, StaffNo: item.StaffNo, StaffFirstName: item.StaffFirstName,
//         StaffLastName: item.StaffLastName, Amount: item.Amount, Quantity: item.Quantity, BilledAmount: item.BilledAmount
//       });
//     }
//     return list;
//   }
//
//   /**
//    * Updates the table using information from newstaff compared to oldstaff (current staff);
//    * only changed fields will be updated, fields / rows that exist in oldstaff that does not
//    * exist in newstaff will be removed; overridden value will be un-oerridden and repalced with
//    * value from newstaff. Note: oldstaff should be an object whose properties are the staff objects,
//    * the keys are the first name of the staff
//    * @param {calcUtils} [calcUtils]
//    * @param {table} [table]
//    * @param {Object} [oldstaff]
//    * @param {array} [newstaff]
//    */
//   function updateTable(calcUtils, table, oldstaff, newstaff) {
//     var numRows = table.size().rows;
//     var name;
//     for (var i = 0; i < newstaff.length; i++) { // updating existing staff
//       if (oldstaff[newstaff[i].StaffFirstName]) {
//         name = newstaff[i].StaffFirstName;
//         if (table.cell(oldstaff[name].rowNo, 2).get() !== newstaff[i].StaffNo) {
//           table.cell(oldstaff[name].rowNo, 2).assign(newstaff[i].StaffNo);
//         }
//         if (table.cell(oldstaff[name].rowNo, 3).get() !== newstaff[i].Rate) {
//           table.cell(oldstaff[name].rowNo, 3).assign(newstaff[i].Rate);
//         }
//         if (table.cell(oldstaff[name].rowNo, 4).get() !== newstaff[i].Quantity) {
//           table.cell(oldstaff[name].rowNo, 4).assign(newstaff[i].Quantity);
//         }
//         if (table.cell(oldstaff[name].rowNo, 5).get() !== newstaff[i].Amount) {
//           table.cell(oldstaff[name].rowNo, 5).assign(newstaff[i].Amount);
//         }
//         if (table.cell(oldstaff[name].rowNo, 6).get() !== newstaff[i].BilledAmount) {
//           table.cell(oldstaff[name].rowNo, 6).assign(newstaff[i].BilledAmount);
//         }
//
//         oldstaff[newstaff[i].StaffFirstName] = undefined;
//       } else { // adding rows that are DNE
//         calcUtils.addTableRow('engagementProfile', '200');
//         table.cell(numRows, 0).assign(newstaff[i].StaffFirstName);
//         table.cell(numRows, 1).assign(newstaff[i].StaffLastName);
//         table.cell(numRows, 2).assign(newstaff[i].StaffNo);
//         table.cell(numRows, 3).assign(newstaff[i].Rate);
//         table.cell(numRows, 4).assign(newstaff[i].Quantity);
//         table.cell(numRows, 5).assign(newstaff[i].Amount);
//         table.cell(numRows, 6).assign(newstaff[i].BilledAmount);
//         numRows++;
//       }
//     }
//
//     // now we remove old staff DNE in new staff
//     for (var key in oldstaff) {
//       if (oldstaff[key]) // the staff is DNE if we changed / exists in new staff
//         calcUtils.removeTableRow('engagementProfile', '200', oldstaff[key].rowNo);
//     }
//
//     if (table.size().rows === 0)
//       calcUtils.addTableRow('engagementProfile', '200');
//   }
//
//   function calculateVariance(field, storeTableNum, calcsTableNum, numeratorCIndex, denominatorCIndex) {
//     var result = 0;
//     if (field(calcsTableNum).total(denominatorCIndex).get() != 0) {
//       field(storeTableNum).assign(field(calcsTableNum).total(numeratorCIndex).get() * 100 /
//           field(calcsTableNum).total(denominatorCIndex).get());
//     }
//   }
//
//   wpw.tax.create.calcBlocks('engagementProfile', function(calcUtils) {
//
//     calcUtils.calc(function(calcUtils) {
//       calcUtils.getGlobalValue('120', 'cp', '300');
//       // calcUtils.field('515').forEachRow(function(row, rowIndex) {
//       //   calcUtils.field('515-show' + rowIndex).assign(row[2].get());
//       // })
//     });
//
//     calcUtils.calc(function(calcUtils) {
//
//       calcUtils.field('getCollabData');
//
//       if (calcUtils.hasChanged('getCollabData')) { // prevent table change affecting imports
//         //Level of details possible parameter values
//         var /** @type number */ DETAILED_REPORT = 0;
//         var /** @type number */ SUMMARY = 1;
//         var /** @type string */  path = wpw.global._staticRoot + '../../../../../cwcoreservice/restricted/TransactionService.svc/GetEngagementTimeExpenseAnalysisList';
//         var /** @type Object */ payload = {};
//
//         payload.DetailLevel = DETAILED_REPORT;
//         //payload.DetailLevel = SUMMARY;
//         var /** @type number */ objectId = wpw.tax.global.engagementProperties['collaborateBundleId'];
//         payload.F = {ObjectId: objectId, ObjectType: 'CustomBundle'};
//         //payload.transactionFilter = {};
//         // payload.Uuid = getUuid();
//
//         return calcUtils.request.collaborateUuid().then(function(uuid) {
//           payload.Uuid = uuid;
//
//           var p = calcUtils.request.collaborate('TransactionService', 'GetEngagementTimeExpenseAnalysisList', payload);
//           return p.then(function(data) {
//             var /** @type Object[] */ staff = [];
//             // staff objects are in data.result, not the return object
//             data = data.result;
//
//             for (var key in data) {
//               if (data[key].ObjectId == objectId) {
//                 staff = addStaff(staff, data[key]);
//               }
//             }
//             var table200 = calcUtils.field('200');
//
//             // don't know what this does but it was here earlier
//             calcUtils.removeDependency(table200);
//
//             var numRows = table200.size().rows;
//             var i;
//             var oldstaff = {};
//
//             // getting current table values to compare
//             for (i = 0; i < numRows; i++) {
//               oldstaff[table200.cell(i, 0).get()] = {
//                 StaffLastName: table200.cell(i, 1).get(),
//                 StaffNo: table200.cell(i, 2).get(),
//                 Rate: table200.cell(i, 3).get(),
//                 Quantity: table200.cell(i, 4).get(),
//                 Amount: table200.cell(i, 5).get(),
//                 BilledAmount: table200.cell(i, 6).get(),
//                 rowNo: i
//               };
//             }
//
//             updateTable(calcUtils, table200, oldstaff, staff);
//           });
//         });
//       }
//     });
//
//     //calcs for table 300 and 345
//     calcUtils.calc(function(calcUtils, field) {
//       var table200 = field('200');
//       table200.getRows().forEach(function(row, rIndex) {
//         var firstName = row[0].get();
//         var lastName = row[1].get();
//         if (firstName || lastName) {
//           angular.extend(field('300').cell(rIndex, 0).valueObj, {
//             label: '# of ' + firstName + ' ' + lastName + ' Hrs',
//             labelClass: 'bold'
//           });
//           angular.extend(field('345').cell(rIndex, 0).valueObj, {
//             label: '$ for ' + firstName + ' ' + lastName,
//             labelClass: 'bold'
//           });
//         }
//
//         field('300').cell(rIndex, 2).assign(row[4].get());//WIP current
//
//         field('345').cell(rIndex, 1).assign(field('300').cell(rIndex, 1).get() * row[3].get());
//         field('345').cell(rIndex, 2).assign(field('300').cell(rIndex, 2).get() * row[3].get());
//         field('345').cell(rIndex, 3).assign(field('300').cell(rIndex, 3).get() * row[3].get());
//         field('345').cell(rIndex, 4).assign(field('300').cell(rIndex, 4).get() * row[4].get());
//         field('345').cell(rIndex, 5).assign(field('300').cell(rIndex, 5).get() * row[4].get());
//       });
//     });
//
//     calcUtils.calc(function(calcUtils, field) {
//       //calculation for variance
//       calculateVariance(field, '335', '300', 3, 1);
//       calculateVariance(field, '340', '300', 5, 4);
//       calculateVariance(field, '380', '345', 3, 1);
//       calculateVariance(field, '385', '345', 5, 4);
//     });
//
//   });
//
// })();
