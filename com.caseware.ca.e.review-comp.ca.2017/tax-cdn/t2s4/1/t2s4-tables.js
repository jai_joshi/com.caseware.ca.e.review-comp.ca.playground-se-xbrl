(function() {

  function getTableSuperHeader() {
    return [
      {
        divider: false
      },
      {
        divider: false
      },
      {
        divider: false
      },
      {
        divider: false
      },
      {
        divider: false
      },
      {
        divider: false
      },
      {
        divider: false
      },
      {
        divider: false
      },
      {
        divider: false
      },
      {
        header: 'Applied to reduce',
        divider: true,
        colspan: '4'
      },
      {
        divider: false
      }
    ]
  }

  function getTableColumns(partSection) {
    var tableColumns = [
      {
        colClass: 'std-input-width',
        type: 'date',
        disabled: true,
        header: '<b>Year of origin</b>'
      },
      {width: '5px', type: 'none'},
      {
        total: true,
        totalMessage: 'Total : ',
        header: '<b>Balance at the beginning of the tax year </b>'
      },
      {width: '5px', type: 'none'},
      {
        type: 'none',
        total: true,
        totalMessage: ' ',
        disabled: true,
        header: '<b>Loss incurred in current year</b>'
      },
      {width: '5px', type: 'none'},
      {
        total: true,
        totalMessage: ' ',
        header: '<b>Adjustments and transfers</b>'
      },
      {width: '5px', type: 'none'}
    ];
    if (!partSection) {
      partSection = ''
    }

    if (partSection === 'part4' || partSection === 'part5') {
      tableColumns.push(
          {
            type: 'none',
            total: true,
            disabled: true,
            totalMessage: ' ',
            header: '<b>Loss carried back Parts I</b>'
          },
          {width: '5px', type: 'none'}
      );
      if (partSection === 'part4') {
        tableColumns.push(
            {
              total: true,
              totalMessage: ' ',
              disabled: true,
              header: '<b>Applied to reduce Taxable Income</b>'
            },
            {width: '5px', type: 'none'}
        )
      }
      if (partSection === 'part5') {
        tableColumns.push(
            {
              total: true,
              totalMessage: ' ',
              disabled: true,
              header: '<b>Applied to reduce Listed personal property gain</b>'
            },
            {width: '5px', type: 'none'}
        )
      }
    }
    else {
      tableColumns.push(
          {
            type: 'none',
            total: true,
            disabled: true,
            totalMessage: ' ',
            header: '<b>Loss carried back Parts I & IV</b>'
          },
          {width: '5px', type: 'none'},
          {
            total: true,
            totalMessage: ' ',
            disabled: true,
            header: '<b>Taxable Income</b>'
          },
          {width: '5px', type: 'none'},
          {
            total: true,
            totalMessage: ' ',
            header: '<b>Part IV tax</b>'
          },
          {width: '5px', type: 'none'}
      )
    }
    tableColumns.push(
        {
          total: true,
          totalMessage: ' ',
          disabled: true,
          header: '<b>Balance at the end of year</b>'
        }
    );
    return tableColumns
  }

  function getTable7YearsCells() {
    return [
      {
        2: {type: 'none'},
        6: {type: 'none'},
        10: {type: 'none'},
        12: {disabled: false},
        13: {label: '*'}
      },
      {13: {label: '**'}},
      {},
      {},
      {},
      {},
      {},
      {},
      {
        2: {type: 'none'},
        4: {type: ''},
        6: {disabled: true},
        8: {type: ''},
        10: {type: 'none'}
      }
    ]
  }

  function getTable10YearsCells(isPart4Or5) {
    var table10YearsCells = [];
    if (!isPart4Or5) {
      table10YearsCells.push(
          {
            2: {type: 'none'},
            6: {type: 'none'},
            10: {type: 'none'},
            12: {type: 'none'},
            14: {disabled: false},
            15: {label: '*'}
          },
          {15: {label: '**'}}
      )
    }
    else {
      table10YearsCells.push(
          {
            2: {type: 'none'},
            6: {type: 'none'},
            10: {type: 'none'},
            12: {disabled: false},
            13: {label: '*'}
          },
          {13: {label: '**'}}
      )
    }
    table10YearsCells.push(
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {}
    );
    if (!isPart4Or5) {
      table10YearsCells.push(
          {
            2: {type: 'none'},
            4: {type: 'none'},
            6: {disabled: true},
            8: {type: 'none'},
            10: {type: 'none'},
            12: {type: 'none'},
            14: {type: 'none'}
          }
      )
    }
    else {
      table10YearsCells.push(
          {
            2: {type: 'none'},
            4: {type: 'none'},
            6: {disabled: true},
            8: {type: 'none'},
            10: {type: 'none'},
            12: {type: 'none'}
          }
      )
    }

    return table10YearsCells
  }

  function getTable20YearsCells(isPart4Or5) {
    var table20YearsCells = [];
    if (!isPart4Or5) {
      table20YearsCells.push(
          {
            2: {type: 'none'},
            6: {type: 'none'},
            10: {type: 'none'},
            12: {type: 'none'},
            14: {disabled: false},
            15: {label: '*'}
          },
          {15: {label: '**'}}
      )
    }
    else {
      table20YearsCells.push(
          {
            2: {type: 'none'},
            6: {type: 'none'},
            10: {type: 'none'},
            12: {disabled: false},
            13: {label: '*'}
          },
          {13: {label: '**'}}
      )
    }
    table20YearsCells.push(
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {}
    );
    if (!isPart4Or5) {
      table20YearsCells.push(
          {
            2: {type: 'none'},
            4: {type: ''},
            6: {disabled: true},
            8: {type: ''},
            10: {type: 'none'}
          }
      )
    }
    else {
      table20YearsCells.push(
          {
            2: {type: 'none'},
            4: {type: ''},
            6: {disabled: true},
            8: {type: ''},
            10: {type: 'none'}
          }
      )
    }
    return table20YearsCells
  }

  wpw.tax.global.tableCalculations.t2s4 = {
    "401": {
      type: 'table', infoTable: true, fixedRows: true, num: '401',
      columns: [
        {type: 'none'},
        {type: 'none', cellClass: 'alignRight', width: '120px'},
        {colClass: 'std-input-width'},
        {width: '180px', type: 'none', cellClass: 'alignCenter'},
        {width: '15px', type: 'none', cellClass: 'alignCenter'},
        {colClass: 'std-input-width'},
        {colClass: 'std-padding-width', type: 'none', cellClass: 'alignCenter'},
        {width: '265px', type: 'none'}
      ],
      cells: [
        {
          1: {label: '(Amount A above'},
          2: {num: '866'},
          3: {label: '- $2,500) <b>divided</b> by 2 ='},
          5: {num: '865'},
          6: {label: 'a'}
        }
      ]
    },
    "700": {
      repeats: ['710', '720'],
      showNumbering: true,
      hasTotals: true,
      columns: [
        {header: 'Partnership account number', num: '600', tn: '600', indicator: '1',
          type: 'custom',
          format: ['{N9}RZ{N4}', 'NR']
        },
        {header: 'Tax year-ending <br> yyyy/mm/dd', num: '602', tn: '602', indicator: '2', type: 'date'},
        {
          header: 'Corporation\'s share of limited partnership loss', indicator: '3',
          tn: '604',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          num: '604',
          width: '10%'
        },
        {header: 'Corporation\'s at-risk amount', num: '606', tn: '606', width: '12%', indicator: '4'},
        {
          header: 'Total of corporation\'s share of partnership investment tax credit, farming losses, ' +
          'and resource expenses',
          indicator: '5',
          num: '608',
          tn: '608',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          width: '10%'
        },
        {
          header: 'Column 4 <b> minus</b> column 5 <br> (if negative, enter \'0\')', indicator: '6',
          num: '610', width: '10%', disabled: true
        },
        {
          header: 'Current-year limited partnership losses (column 3 <b> minus </b> column 6)',
          indicator: '7',
          num: '620',
          tn: '620',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          total: true,
          totalNum: '701',
          totalMessage: 'Total (enter this amount on line 222 of Schedule 1)',
          disabled: true,
          width:'10%'
        }
      ]
    },
    "710": {
      showNumbering: true,
      keepButtonsSpace: true,
      hasTotals: true,
      fixedRows: true,
      columns: [
        {
          header: 'Partnership account number',
          num: '630',
          tn: '630',
          indicator: '1',
          type: 'custom',
          format: ['{N9}RZ{N4}', 'NR'],
          disabled: true
        },
        {
          header: 'Tax year-ending <br> yyyy/mm/dd', tn: '632', num: '632', indicator: '2', type: 'date',
          disabled: true, width: '145px'
        },
        {
          header: 'Limited partnership losses at the end of the previous tax year and amounts transferred on ' +
          'an amalgamation or on the wind-up of a subsidiary',
          tn: '634', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},num: '634', indicator: '3', totalnum: '634-t'
        },
        {header: 'Corporation\'s at-risk amount', num: '636', tn: '636', indicator: '4'},
        {
          header: 'Total of corporation\'s share of partnership investment tax credit, business or ' +
          'property losses, and resource expenses',
          num: '638', tn: '638', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}, indicator: '5'
        },
        {
          header: 'Column 4 <b> minus </b> column 5 <br> (if negative, enter \'0\')',
          indicator: '6', disabled: true
        },
        {
          header: 'Limited partnership losses that may be applied in the year (the lesser of column 3 and 6)',
          num: '650', tn: '650', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},indicator: '7', disabled: true
        }
      ]
    },
    "720": {
      showNumbering: true,
      keepButtonsSpace: true,
      hasTotals: true,
      fixedRows: true,
      columns: [
        {
          header: 'Partnership account number', num: '660', tn: '660',
          type: 'custom',
          format: ['{N9}RZ{N4}', 'NR'],
          disabled: true, width: '170px'
        },
        {
          header: 'Limited partnership losses at the end of the previous tax year',
          indicator: '2',
          num: '662',
          tn: '662',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          width: '15%',
          disabled: true
        },
        {
          indicator: '3',
          header: 'Limited partnership losses transferred in the year on an amalgamation or on the wind-up of a subsidiary',
          width: '15%',
          num: '664',
          tn: '664',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        },
        {
          indicator: '4', header: 'Current-year limited partnership losses (from line 620)',
          num: '670',
          tn: '670',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          width: '10%',
          disabled: true
        },
        {
          indicator: '5',
          header: 'Limited partnership losses applied in the current year (must be equal to or ' +
          'less than line 650)',
          totalNum: '675',
          tn: '675',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          width:'10%',
          total: true,
          totalMessage: 'Total (enter this amount on line 335 of the T2 return)',
          disabled: true
        },
        {
          indicator: '6',
          header: 'Current year limited partnership losses closing balance to be carried forward to future years' +
          ' (column 2 plus column 3 plus column 4 minus column 5)',
          num: '680',
          tn: '680',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          total: true,
          "hiddenTotal": true,
          'totalNum': '1680',
          disabled: true,
          width:'10%'
        }
      ]
    },
    "5010": {
      type: 'table', fixedRows: true, num: '5010', infoTable: true, hasTotals: true,
      superHeaders: getTableSuperHeader(),
      columns: getTableColumns(),
      cells: getTable10YearsCells()
    },
    "5020": {
      type: 'table', fixedRows: true, num: '5020', infoTable: true, hasTotals: true,
      superHeaders: getTableSuperHeader(),
      columns: getTableColumns(),
      cells: getTable20YearsCells()
    },
    "5030": {
      type: 'table', fixedRows: true, num: '5030', infoTable: true, hasTotals: true,
      superHeaders: getTableSuperHeader(),
      columns: getTableColumns(),
      cells: getTable10YearsCells()
    },
    "5040": {
      type: 'table', fixedRows: true, num: '5040', infoTable: true, hasTotals: true,
      superHeaders: getTableSuperHeader(),
      columns: getTableColumns(),
      cells: getTable20YearsCells()
    },
    "5050": {
      type: 'table', fixedRows: true, num: '5050', infoTable: true, hasTotals: true,
      columns: getTableColumns('part4'),
      cells: getTable10YearsCells(true)
    },
    "5060": {
      type: 'table', fixedRows: true, num: '5060', infoTable: true, hasTotals: true,
      columns: getTableColumns('part4'),
      cells: getTable20YearsCells(true)
    },
    "5070": {
      type: 'table', fixedRows: true, num: '5070', infoTable: true, hasTotals: true,
      columns: getTableColumns('part5'),
      cells: getTable7YearsCells()
    }
  }
})();
