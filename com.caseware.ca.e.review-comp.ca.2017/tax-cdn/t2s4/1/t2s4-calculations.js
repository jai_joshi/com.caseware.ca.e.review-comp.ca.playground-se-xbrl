(function() {

  function fillArray(array, start, end) {
    for (var i = start; i <= end; i++) {
      array.push(i)
    }
  }

  function getEndPreYearAmount(field, store, tableNum, isPart4Or5) {
    if (!angular.isArray(tableNum)) {
      tableNum = [tableNum]
    }

    var value = 0;
    for (var i = 0; i < tableNum.length; i++) {
      if (isPart4Or5) {
        value +=
            field(tableNum[i].toString()).total(2).get() +
            field(tableNum[i].toString()).cell(0, 12).get();
      }
      else {
        value +=
            field(tableNum[i].toString()).total(2).get() +
            field(tableNum[i].toString()).cell(0, 14).get();
      }
    }

    field(store.toString()).assign(value);
  }

  function getExpiredAmount(field, store, tableNum, isPart4Or5) {
    if (!angular.isArray(tableNum)) {
      tableNum = [tableNum]
    }

    var value = 0;
    for (var i = 0; i < tableNum.length; i++) {
      if (isPart4Or5) {
        value += field(tableNum[i].toString()).cell(0, 12).get();
      }
      else {
        value += field(tableNum[i].toString()).cell(0, 14).get();
      }
    }
    field(store.toString()).assign(value);
  }

  function getTotalAppliedAmount(field, store, tableNum) {
    if (!angular.isArray(tableNum))
      tableNum = [tableNum];

    var value = 0;
    for (var i = 0; i < tableNum.length; i++) {
      value += field(tableNum[i].toString()).total(10).get();
    }
    field(store.toString()).assign(value);
  }

  function getTotalAppliedPartIVTaxAmount(field, store, tableNum) {
    if (!angular.isArray(tableNum))
      tableNum = [tableNum];

    var value = 0;
    for (var i = 0; i < tableNum.length; i++) {
      value += field(tableNum[i].toString()).total(12).get();
    }
    field(store.toString()).assign(value);
  }

  function getAmountApplied(available, limitAmount) {
    var applied = 0;
    if (available == 0 || angular.isUndefined(available) || isNaN(available)) {
      applied = 0;
    }
    else {
      if (limitAmount > available) {
        applied = available;
      }
      else {
        applied = limitAmount;
      }
    }
    return applied;
  }

  function tableAppliedCalcs(field, tableArrays, limitAmountNumObj, colIndexMap) {

    var limitAmount = 0;
    //determine if the limit amount is local value or global value
    if (angular.isObject(limitAmountNumObj)) {
      limitAmount = field(limitAmountNumObj.formId + '.' + limitAmountNumObj.fieldId).get();
    }
    else {
      limitAmount = field(limitAmountNumObj.toString()).get();
    }

    var allTableData = {};

    tableArrays.forEach(function(tableNum) {
      if (!allTableData[tableNum])
        allTableData[tableNum] = {};

      field(tableNum).getRows().forEach(function(tableRow, rIndex) {
        if (colIndexMap.appliedToPartIVTaxCol) {
          allTableData[tableNum][rIndex] = {
            beginningAmount: tableRow[colIndexMap.beginningAmountCol].get(),
            adjAmount: tableRow[colIndexMap.adjAmountCol].get(),
            appliedToTaxableIncome: tableRow[colIndexMap.appliedToTaxableIncomeCol].get(),
            appliedToPartIVTax: colIndexMap.appliedToPartIVTaxCol && tableRow[colIndexMap.appliedToPartIVTaxCol].get()
          };
        }
        else {
          allTableData[tableNum][rIndex] = {
            beginningAmount: tableRow[colIndexMap.beginningAmountCol].get(),
            adjAmount: tableRow[colIndexMap.adjAmountCol].get(),
            appliedToTaxableIncome: tableRow[colIndexMap.appliedToTaxableIncomeCol].get(),
          };
        }
      });
    });

    //calcs for applied amount
    //loop thru allTableData to calculate amount Applied and limitAmount left
    tableArrays.forEach(function(tableNum) {
      var tableRows = field(tableNum).getRows();
      tableRows.forEach(function(tableRow, rIndex) {
        if (rIndex == 0)
          return;

        var cellRowAllData = allTableData[tableNum][rIndex];

        if (limitAmount > 0) {
          if (!colIndexMap.appliedToPartIVTaxCol) {
            cellRowAllData.appliedToPartIVTax = 0;
          }
          cellRowAllData.appliedToTaxableIncome = getAmountApplied(
              cellRowAllData.beginningAmount +
              cellRowAllData.adjAmount -
              cellRowAllData.appliedToPartIVTax,
              limitAmount
          );

          tableRow[colIndexMap.appliedToTaxableIncomeCol].assign(cellRowAllData.appliedToTaxableIncome);

          //calcs ending Balance on table
          if (rIndex == 0 || rIndex == (tableRows.length - 1)) {
            //do nothing for first row and last row
          }
          else {
            tableRow[colIndexMap.endingAmountCol].assign(
                tableRow[colIndexMap.beginningAmountCol].get() +
                tableRow[colIndexMap.adjAmountCol].get() -
                tableRow[colIndexMap.appliedToTaxableIncomeCol].get()
            );
            if (colIndexMap.appliedToPartIVTaxCol) {
              tableRow[colIndexMap.endingAmountCol].assign(
                  tableRow[colIndexMap.endingAmountCol].get() -
                  tableRow[colIndexMap.appliedToPartIVTaxCol].get()
              );
            }
          }
          //update new limitAmount
          limitAmount = limitAmount - cellRowAllData.appliedToTaxableIncome;
        }
        else {
          //when limitAmount reaches 0, assign all the rest of appliedCol value to 0
          tableRow[colIndexMap.appliedToTaxableIncomeCol].assign(0);
          //calcs ending Balance on table
          if (rIndex == 0 || rIndex == (tableRows.length - 1)) {
            //do nothing for first row and last row
          }
          else {
            if (angular.isDefined(colIndexMap.appliedToPartIVTaxCol)) {
              tableRow[colIndexMap.endingAmountCol].assign(
                  tableRow[colIndexMap.beginningAmountCol].get() +
                  tableRow[colIndexMap.adjAmountCol].get() -
                  tableRow[colIndexMap.appliedToTaxableIncomeCol].get() -
                  tableRow[colIndexMap.appliedToPartIVTaxCol].get()
              )
            }
            else {
              tableRow[colIndexMap.endingAmountCol].assign(
                  tableRow[colIndexMap.beginningAmountCol].get() +
                  tableRow[colIndexMap.adjAmountCol].get() -
                  tableRow[colIndexMap.appliedToTaxableIncomeCol].get()
              )
            }
          }
        }
      });
    });
  }

  wpw.tax.create.calcBlocks('t2s4', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      //to get year for table summary
      var table10YearsArrays = [5010, 5030, 5050, 5070];
      var table20YearsArrays = [5020, 5040, 5060];
      var summaryTable = field('tyh.200');
      table20YearsArrays.forEach(function(tableNum) {
        field(tableNum).getRows().forEach(function(row, rowIndex) {
          row[0].assign(summaryTable.getRow(rowIndex)[6].get())
        });
      });
      table10YearsArrays.forEach(function(num) {
        field(num).getRows().forEach(function(row, rowIndex) {
          if (num == 5070) {
            row[0].assign(summaryTable.getRow(rowIndex + 12)[6].get())
          }
          else {
            row[0].assign(summaryTable.getRow(rowIndex + 9)[6].get())
          }
        })
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 1 Calculations //
      calcUtils.getGlobalValue('999', 'T2J', '300');
      field('998').assign(Math.abs(Math.min(field('T2J.332').get(), 0)));
      field('998').source(field('T2J.332'));
      calcUtils.getGlobalValue('997', 'T2J', '320');
      calcUtils.getGlobalValue('996', 'T2J', '325');
      calcUtils.getGlobalValue('995', 'T2J', '350');
      var sum994 = [];
      fillArray(sum994, 995, 998);
      calcUtils.sumBucketValues('994', sum994);
      calcUtils.equals('993', '994');

      var num992Value = field('999').get() - field('993').get();
      if (num992Value > 0) {
        field('992').assign(0)
      }
      else {
        field('992').assign(num992Value)
      }

      calcUtils.getGlobalValue('991', 'T2J', '355');
      calcUtils.subtract('990', '992', '991', true);

      var farmIncome = calcUtils.form('t2s1').field('farmIncome').get();
      if (farmIncome < 0) {
        //use max because comparing 2 negative value
        field('899').assign(Math.max(field('990').get(), farmIncome) * (-1));
      }

      calcUtils.sumBucketValues('898', ['990', '899']);

      getEndPreYearAmount(field, '897', ['5010', '5020']);
      getExpiredAmount(field, '100', ['5010', '5020']);
      calcUtils.subtract('102', '897', '100');
      calcUtils.equals('896', '102');
      field('110').assign(field('898').get() * (-1));
      calcUtils.sumBucketValues('895', ['105', '110']);
      calcUtils.equals('894', '895');
      calcUtils.sumBucketValues('893', ['896', '894']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //calcs for 130
      tableAppliedCalcs(field, ['5010', '5020', '5030', '5040'],
          {formId: 'T2J', fieldId: 'S4CreditLimit'},
          {
            beginningAmountCol: 2,
            adjAmountCol: 6,
            appliedToTaxableIncomeCol: 10,
            appliedToPartIVTaxCol: 12,
            endingAmountCol: 14
          });

      getTotalAppliedAmount(field, '130', ['5010', '5020']);
      getTotalAppliedPartIVTaxAmount(field, '135', ['5010', '5020']);

      var sum892 = [];
      fillArray(sum892, 130, 150);
      calcUtils.sumBucketValues('892', sum892);
      calcUtils.equals('891', '892');
      calcUtils.subtract('890', '893', '891');
      var sum889 = [];
      fillArray(sum889, 901, 913);
      calcUtils.sumBucketValues('889', sum889);
      //update Loss carried back Parts I & IV to the summary table
      field('5020').cell(field('5020').size().rows - 1, 8).assign(field('889').get());
      calcUtils.equals('888', '889');
      calcUtils.subtract('180', '890', '888');
      //update Loss incurred in current year to the summary table
      field('5020').cell(field('5020').size().rows - 1, 4).assign(field('110').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 2 Calculations //
      calcUtils.sumBucketValues('887', ['200', '205']);
      calcUtils.equals('886', '887');
      calcUtils.sumBucketValues('885', ['250', '240']);
      calcUtils.equals('884', '885');
      calcUtils.subtract('209', '886', '884');
      calcUtils.getGlobalValue('210', 'T2S6', '988');
      calcUtils.equals('212', '100');
      calcUtils.min('215', ['212', '213']);
      calcUtils.multiply('220', ['215'], 2);
      calcUtils.sumBucketValues('880', ['209', '210', '220']);
      if (field('T2S6.989').get() > 0) {
        calcUtils.getAmountApplied('225', '880', {formId: 'T2S6', fieldId: '989'});
      }
      else {
        field('225').assign(0)
      }
      calcUtils.subtract('879', '880', '225');
      var sum878 = [];
      fillArray(sum878, 951, 953);
      calcUtils.sumBucketValues(878, sum878);
      calcUtils.equals('877', '878');
      calcUtils.subtract('280', '879', '877');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 3 Calculations //
      getEndPreYearAmount(field, '876', ['5030', '5040']);
      getExpiredAmount(field, '300', ['5030', '5040']);
      calcUtils.subtract('302', '876', '300');

      calcUtils.equals('875', '302');
      calcUtils.equals('310', '899');
      calcUtils.sumBucketValues('874', ['305', '310']);
      calcUtils.equals('873', '874');
      calcUtils.sumBucketValues('872', ['875', '873']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //update Loss incurred in current year to the summary table
      field('5040').cell(field('5040').size().rows - 1, 4).assign(field('310').get());
      getTotalAppliedAmount(field, '330', ['5030', '5040']);
      getTotalAppliedPartIVTaxAmount(field, '335', ['5030', '5040']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var sum871 = [];
      fillArray(sum871, 330, 350);
      calcUtils.sumBucketValues(871, sum871);
      calcUtils.equals('870', '871');
      calcUtils.subtract('869', '872', '870');
      var sum868 = [];
      fillArray(sum868, 921, 933);
      calcUtils.sumBucketValues(868, sum868);
      //update Loss carried back Parts I & IV to the summary table
      field('5040').cell(field('5040').size().rows - 1, 8).assign(field('868').get());
      calcUtils.equals('867', '868');
      calcUtils.subtract('380', '869', '867');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 4 Calculation //
      //todo: get num485 from S1- restricted farm loss
      calcUtils.getGlobalValue('2500', 'ratesFed', '160');
      calcUtils.equals('866', '485');
      field('865').assign((field('485').get() - field('2500').get()) * (1 / 2));
      if (field('865').get() < 0) {
        field('865').assign(0);
      }
      field('864').assign(Math.min(field('865').get(), 15000));
      calcUtils.equals('863', '864');
      calcUtils.sumBucketValues('862', ['863', '2500']);
      calcUtils.equals('861', '862');
      calcUtils.subtract('860', '485', '861');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      getEndPreYearAmount(field, '859', ['5050', '5060'], true);
      getExpiredAmount(field, '400', ['5050', '5060'], true);
      calcUtils.subtract('402', '859', '400');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.equals('858', '402');
      calcUtils.equals('410', '860');
      field('5060').cell(field('5060').size().rows - 1, 4).assign(field('410').get());
      calcUtils.sumBucketValues('857', ['405', '410']);
      calcUtils.equals('856', '857');
      calcUtils.sumBucketValues('855', ['858', '856']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      tableAppliedCalcs(field, ['5050', '5060'],
          {formId: 'T2S1', fieldId: 'farmIncome'},
          {
            beginningAmountCol: 2,
            adjAmountCol: 6,
            appliedToTaxableIncomeCol: 10,
            endingAmountCol: 12
          }
      );
    });

    calcUtils.calc(function(calcUtils, field, form) {
      getTotalAppliedAmount(field, '430', ['5050', '5060']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var sum854 = [];
      fillArray(sum854, 430, 450);
      calcUtils.sumBucketValues(854, sum854);
      calcUtils.equals('853', '854');
      calcUtils.subtract('852', '855', '853');
      calcUtils.sumBucketValues('851', ['941', '942', '943']);
      field('5060').cell(field('5060').size().rows - 1, 8).assign(field('851').get());
      calcUtils.equals('850', '851');
      calcUtils.subtract('480', '852', '850');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 5 calculations //
      getEndPreYearAmount(field, '549', ['5070'], true);
      getExpiredAmount(field, '500', ['5070'], true);
      calcUtils.subtract('502', '549', '500');

      calcUtils.equals('848', '502');
      var value = field('T2S6.650').get();
      if (value < 0) {
        field('510').assign(value * (-1))
      }
      else {
        field('510').assign(0)
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.sumBucketValues('847', ['848', '510']);
      tableAppliedCalcs(field, ['5070'],
          {formId: 'T2S6', fieldId: '650'},
          {
            beginningAmountCol: 2,
            adjAmountCol: 6,
            appliedToTaxableIncomeCol: 10,
            endingAmountCol: 12
          }
      );

      getTotalAppliedAmount(field, '530', ['5070']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.sumBucketValues('846', ['530', '550']);
      calcUtils.equals('845', '846');
      calcUtils.subtract('844', '847', '845');
      calcUtils.sumBucketValues('843', ['961', '962', '963']);
      field('5070').cell(field('5070').size().rows - 1, 8).assign(field('843').get());
      calcUtils.equals('842', '843');
      calcUtils.subtract('580', '844', '842');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 7 calcs
      field('700').getRows().forEach(function(row, rowIndex) {
        var repeatedTableRow = field('710').getRow(rowIndex);
        var repeatedTableRow2 = field('720').getRow(rowIndex);

        repeatedTableRow[0].assign(row[0].get());
        repeatedTableRow[1].assign(row[1].get());
        repeatedTableRow2[0].assign(row[0].get());
        repeatedTableRow2[1].assign(repeatedTableRow[2].get());
        repeatedTableRow2[3].assign(row[6].get());
        repeatedTableRow2[4].assign(repeatedTableRow[6].get());

        row[5].assign(Math.max(row[3].get() - row[4].get(), 0));
        row[6].assign(row[2].get() - row[5].get());
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('710').getRows().forEach(function(row) {
        row[5].assign(Math.max(row[3].get() - row[4].get(), 0));
        row[6].assign(Math.min(row[2].get(), row[5].get()));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('720').getRows().forEach(function(row, rowIndex) {
        var repeatedTableRow = field('710').getRow(rowIndex);
        //Limited partnership losses applied in the current year (must be equal to or less than line 650)
        if (row[4].get() > repeatedTableRow[6].get()) {
          row[4].assign(repeatedTableRow[6].get())
        }

        row[5].assign(
            row[1].get() +
            row[2].get() +
            row[3].get() -
            row[4].get());
      });

    });
  });
})();
