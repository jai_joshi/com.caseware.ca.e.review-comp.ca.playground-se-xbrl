(function() {
  'use strict';

  wpw.tax.global.formData.t2s4 = {
    'formInfo': {
      abbreviation: 't2s4',
      'title': 'Corporation Loss Continuity and Application',
      //'subTitle': '(2013 and later tax years)',
      'schedule': 'Schedule 4',
      'code': 'Code 1302',
      formFooterNum: 'T2 SCH 4 E (15)',
      headerImage: 'canada-federal',
      dynamicFormWidth: false,
      'showCorpInfo': true,
      'description': [
        {
          'type': 'list', 'items': [
          'Use this form to determine the continuity and use of available losses; to determine a current-year' +
          ' non-capital loss, farm loss, restricted farm loss, or limited partnership loss; to determine' +
          ' the amount of restricted farm loss and limited partnership loss that can be applied in a year; and to ' +
          'ask for a loss carryback to previous years.',
          'A corporation can choose whether or not to deduct an available loss from income in a tax year. The' +
          ' corporation can deduct losses in any order. However, for each type of loss, deduct the oldest loss ' +
          'first.',
          'According to subsection 111(4) of the <i>Income Tax Act</i>, when control has been acquired, no amount' +
          ' of capital loss incurred for a tax year ending before that time is deductible in computing taxable' +
          ' income in a tax year ending after that time. Also, no amount of capital loss incurred in a tax year ' +
          'ending after that time is deductible in computing taxable income of a tax year ending before that time.',
          'When control has been acquired, subsection 111(5) provides for similar treatment of non-capital and' +
          ' farm losses, except as listed in paragraphs 111(5)(a) and (b).',
          'For information on these losses, see the <i> T2 Corporation - Income Tax Guide</i>.',
          'File one completed copy of this schedule with the T2 return, or send the schedule by itself to the tax ' +
          'centre where the return is filed',
          'All legislative references are to the <i>Income Tax Act</i>.'
        ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    'sections': [
      {
        'header': 'Historical Data and calculation for Non-capital losses ',
        rows: [
          {labelClass: 'fullLength'},
          {
            label: 'Losses arose in a tax year-ending after March 22, 2004, and before 2006',
            labelClass: 'fullLength boldUnderline'
          },
          {
            type: 'table', num: '5010'
          },
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          {
            label: 'Losses arose in a tax year-ending after 2005',
            labelClass: 'fullLength boldUnderline'
          },
          {
            type: 'table', num: '5020'
          },
          {labelClass: 'fullLength'},
          {
            label: '* Expired'
          },
          {
            label: '** Expiring if not use this year'
          }
        ]
      },
      {
        'header': 'Part 1 – Non-capital losses',
        rows: [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Determination of current-year non-capital loss',
            'labelClass': 'bold'
          },
          {
            'label': 'Net income (loss) for income tax purposes',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '999'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': '<b> Deduct: </b> (increase a loss)'
          },
          {
            'label': 'Net capital losses deducted in the year (enter as a positive amount)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '998'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'Taxable dividends deductible under section 112 or subsections 113(1) or 138(6)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '997'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': 'Amount of Part VI.1 tax deductible under paragraph 110(1)(k)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '996'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'Amount deductible as prospector\'s and grubstaker\'s shares – Paragraph 110(1)(d.2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '995'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts a to d)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '994'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '993'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Subtotal (amount A <b>minus</b> amount B; if positive, enter "0")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '992'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': '<b> Deduct</b>: (increase a loss)'
          },
          {
            'label': 'Section 110.5 or subparagraph 115(1)(a)(vii) - Addition for foreign tax deductions',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '991'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Subtotal (amount C <b> minus </b> amount D)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '990'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Add</b>: (decrease a loss)'
          },
          {
            'label': 'Current-year farm loss (the lesser of: the net loss from farming or fishing included in income ' +
            'and the non-capital loss before deducting the farm loss) .',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '899'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Current-year non-capital loss (amount E <b>plus</b> amount F; if positive, enter "0") ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '898'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'If amount G is negative, enter it on line 110 as a positive.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Continuity of non-capital losses and request for a carryback',
            'labelClass': 'bold'
          },
          {
            'label': 'Non-capital loss at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '897'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': '<b> Deduct: </b> Non-capital loss expired (note 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '100',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'label': 'Non-capital losses at the beginning of the tax year (amount e <b> minus </b> amount f)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '102',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '102'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '896',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Non-capital losses transferred on an amalgamation or on the wind-up of a subsidiary (note 2) corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'g'
                }
              }
            ]
          },
          {
            'label': 'Current-year non-capital loss (from amount G)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'h'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount g <b>plus</b> amount h)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '895'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '894'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Subtotal (amount H <b> plus </b> amount I)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '893'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 1: A non-capital loss expires as follows: ',
            'labelClass': 'indent'
          },
          {
            'label': '• after <b>10</b> tax years if it arose in a tax year ending after March 22, 2004, and before 2006; and',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• after <b>20</b> tax years if it arose in a tax year ending after 2005.',
            'labelClass': 'fullLength tabbed2'
          },
          {
            label: 'An allowable business investment loss becomes a net capital loss after 10 tax years if it arose' +
            ' in a tax year ending after March 22, 2004.',
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Note 2: Subsidiary is defined in subsection 88(1) as a taxable Canadian corporation of which 90%' +
            ' or more of each class of issued shares are owned by its parent corporation and the remaining shares are ' +
            'owned by persons that deal at arm\'s length with the parent corporation.',
            'labelClass': 'fullLength indent'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Other adjustments (including adjustments for an acquisition of control)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '150',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '150'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'i'
                }
              }
            ]
          },
          {
            'label': 'Section 80 - Adjustments for forgiven amounts',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '140',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '140'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'j'
                }
              }
            ]
          },
          {
            'label': 'Non-capital losses of previous tax years applied in the current tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '130',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'k'
                }
              }
            ]
          },
          {
            'label': 'Enter amount k on line 331 of the T2 Return.',
            'labelClass': 'indent'
          },
          {
            'label': 'Current and previous year non-capital losses applied against current-year taxable dividends subject to Part IV tax (note 3)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '135',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '135'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'l'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts i to l)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '892'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '891'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': 'Non-capital losses before any request for a carryback (amount J <b> minus </b> amount K)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '890'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': 'Deduct - Request to carry back non-capital loss to:',
            'labelClass': 'bold'
          },
          {
            'label': 'First previous tax year to reduce taxable income',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '901',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '901'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'm'
                }
              }
            ]
          },
          {
            'label': 'Second previous tax year to reduce taxable income',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '902',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '902'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'n'
                }
              }
            ]
          },
          {
            'label': 'Third previous tax year to reduce taxable income',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '903',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '903'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'o'
                }
              }
            ]
          },
          {
            'label': 'First previous tax year to reduce taxable dividends subject to Part IV tax',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '911',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '911'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'p'
                }
              }
            ]
          },
          {
            'label': 'Second previous tax year to reduce taxable dividends subject to Part IV tax',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '912',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '912'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'q'
                }
              }
            ]
          },
          {
            'label': 'Third previous tax year to reduce taxable dividends subject to Part IV tax',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '913',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '913'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'r'
                }
              }
            ]
          },
          {
            'label': 'Total of requests to carry back non-capital losses to previous tax years (total of amounts m to r)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '889'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '888'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'label': 'Closing balance of non-capital losses to be carried forward to future tax years (amount L <b> minus </b> amount M)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '180'
                },
                'padding': {
                  'type': 'tn',
                  'data': '180'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 3: Amount I is the total of lines 330 and 335 from Schedule 3, <i>Dividends Received, Taxable Dividends Paid, and Part IV Tax Calculation</i>.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 – Capital losses',
        'rows': [
          {
            'label': 'Continuity of capital losses and request for a carryback',
            'labelClass': 'bold'
          },
          {
            'label': 'Capital losses at the the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'Capital losses transferred on an amalgamation or on the wind-up of a subsidiary corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '205',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '205'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount a <b> plus </b> amount b)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '887'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '886'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Other adjustments (include adjustments for an acquisition of control)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '250',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '250'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'Section 80 - Adjustments for forgiven amounts',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '240',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '240'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount c <b> plus </b> amount d)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '885'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '884'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Subtotal (amount A <b> minus </b> amount B)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '209'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': '<b> Add</b>: Current-year capital loss (from the calculation on Schedule 6, <i> Summary of Dispositions of Capital Property</i>)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '210',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '210'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Unused non-capital losses that expired in the tax year (note 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '212'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': 'Allowable business investment losses (ABILs) that expired as non-capital losses at the end of ' +
            'the previous tax year (note 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '213'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'label': 'Enter amount e or f, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '215',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '215'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'g'
                }
              }
            ]
          },
          {
            'label': 'ABILs expired as non-capital losses: line 215 <b>multiplied</b> by 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '220'
                },
                'padding': {
                  'type': 'tn',
                  'data': '220'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Subtotal (total of amounts C to E)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '880'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note',
            'labelClass': 'bold tabbed'
          },
          {
            'label': 'If there has been an amalgamation or a wind–up of a subsidiary, do a separate calculation of the' +
            ' ABIL expired as non-capital loss for each predecessor or subsidiary corporation. Add all these amounts ' +
            'and enter the total on line 220 above.',
            'labelClass': 'indent fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 4: If the loss was incurred in a tax year ending after March 22, 2004, determine ' +
            'the amount of the loss from the 11th previous tax year and enter the part of that loss that was not ' +
            'used in previous years and the current year on line e.',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 5: If the ABILs were incurred in a tax year ending after March 22, 2004, enter the amount of the ABILs from the 11th previous tax year. Enter the full amount on line f.',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Deduct</b>: Capital losses from previous tax years applied against the current-year net capital gain (note 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '225'
                },
                'padding': {
                  'type': 'tn',
                  'data': '225'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Capital losses before any request for a carryback (amount F <b> minus </b> amount G)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '879'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': '<b>Deduct - Request to carry back capital loss to</b> (note 7):',
            'labelClass': 'fullLength'
          },
          {
            'label': 'First previous tax year',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '951',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '951'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'h'
                }
              }
            ]
          },
          {
            'label': 'Second previous tax year',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '952',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '952'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'i'
                }
              }
            ]
          },
          {
            'label': 'Third previous tax year',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '953',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '953'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'j'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts h to j)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '878'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '877'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Closing balance of capital losses to be carried forward to future tax years (amount H <b> minus </b> amount I)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '280',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '280'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'labelClass': 'fulLLength'
          },
          {
            'label': 'Note 6: To get the net capital losses required to reduce the taxable capital gain included in the net income (loss) for the current-year tax, enter the amount from line 225 <b>divided</b> by 2 at line 332 of the T2 return.',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': 'Note 7: On line 225, 951, 952, or 953, whichever applies, enter the actual amount of the loss. When the loss is applied, divide this amount by 2. The result represents the 50% inclusion rate.',
            'labelClass': 'fullLength tabbed2'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for Farm losses ',
        rows: [
          {labelClass: 'fullLength'},
          {
            label: 'Losses arose in a tax year-ending after March 22, 2004, and before 2006',
            labelClass: 'fullLength boldUnderline'
          },
          {
            type: 'table', num: '5030'
          },
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          {
            label: 'Losses arose in a tax year-ending after 2005',
            labelClass: 'fullLength boldUnderline'
          },
          {
            type: 'table', num: '5040'
          },
          {labelClass: 'fullLength'},
          {
            label: '* Expired'
          },
          {
            label: '** Expiring if not use this year'
          }
        ]
      },
      {
        'header': 'Part 3 - Farm losses',
        'rows': [
          {
            'label': 'Continuity of farm losses and request for a carryback',
            'labelClass': 'bold'
          },
          {
            'label': 'Farm losses at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '876'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': '<b> Deduct</b>: Farm loss expired (note 8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '300',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '300'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': 'Farm losses at the beginning of the tax year (amount a <b> minus </b> amount b)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '302',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '302'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '875',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Farm losses transferred on an amalgamation or on the wind–up of a subsidiary corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '305',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '305'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'Current-year farm loss (amount F in Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '310',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '310'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount c <b> plus </b> amount d)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '874'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '873'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Subtotal (amount A <b> plus </b> amount B)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '872'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Other adjustments (includes adjustments for an acquisition of control)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '350',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '350'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': 'Section 80 - Adjustments for forgiven amounts',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '340',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '340'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'label': 'Farm losses of previous tax years applied in the current tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '330',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '330'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'g'
                }
              }
            ]
          },
          {
            'label': 'Enter amount g on line 334 of the T2 Return.',
            'labelClass': 'indent'
          },
          {
            'label': 'Current and previous year farm losses applied against current-year taxable dividends subject to Part IV tax (note 9)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '335',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '335'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'h'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts e to h)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '871'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '870'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Farm losses before any request for a carryback (amount C <b>minus</b> amount D)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '869'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Deduct - Request to carry back farm loss to:',
            'labelClass': 'bold'
          },
          {
            'label': 'First previous tax year to reduce taxable income',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '921',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '921'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'i'
                }
              }
            ]
          },
          {
            'label': 'Second previous tax year to reduce taxable income',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '922',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '922'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'j'
                }
              }
            ]
          },
          {
            'label': 'Third previous tax year to reduce taxable income',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '923',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '923'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'k'
                }
              }
            ]
          },
          {
            'label': 'First previous tax year to reduce taxable dividends subject to Part IV tax',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '931',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '931'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'l'
                }
              }
            ]
          },
          {
            'label': 'Second previous tax year to reduce taxable dividends subject to Part IV tax',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '932',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '932'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'm'
                }
              }
            ]
          },
          {
            'label': 'Third previous tax year to reduce taxable dividends subject to Part IV tax',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '933',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '933'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'n'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts i to n)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '868'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '867'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Closing balance of farm losses to be carried forward to future tax years (amount E <b>minus</b> amount F)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '380'
                },
                'padding': {
                  'type': 'tn',
                  'data': '380'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 8: A farm loss expires as follows:',
            'labelClass': 'fullLength indent'
          },
          {
            'label': '• after <b>10</b> tax years if it arose in a tax year ending before 2006; and',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• after <b>20</b> tax years if it arose in a tax year ending after 2005.',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': 'Note 9: Amount h is the total of lines 340 and 345 from Schedule 3.',
            'labelClass': 'fullLength indent'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for Restricted farm losses',
        rows: [
          {labelClass: 'fullLength'},
          {
            label: 'Losses arose in a tax year-ending after March 22, 2004, and before 2006',
            labelClass: 'fullLength boldUnderline'
          },
          {
            type: 'table', num: '5050'
          },
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          {
            label: 'Losses arose in a tax year-ending after 2005',
            labelClass: 'fullLength boldUnderline'
          },
          {
            type: 'table', num: '5060'
          },
          {labelClass: 'fullLength'},
          {
            label: '* Expired'
          },
          {
            label: '** Expiring if not use this year'
          }
        ]
      },
      {
        'header': 'Part 4 - Restricted farm losses',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Current-year restricted farm loss',
            'labelClass': 'bold'
          },
          {
            'label': 'Total losses for the year from farming business',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '485'
                },
                'padding': {
                  'type': 'tn',
                  'data': '485'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': '<b> Minus </b> the deductible farm loss:',
            'labelClass': 'indent'
          },
          {
            'type': 'table',
            'num': '401'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount a or $15,000 (note 10), whichever is less',
            'labelClass': ' text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '864'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '863'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '2500',
                  'disabled': true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount b <b> plus </b> amount c)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '862'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '861'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Current-year restricted farm loss (amount A <b> minus </b> amount B)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '860'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Continuity of restricted farm losses and request for a carryback',
            'labelClass': 'bold'
          },
          {
            'label': 'Restricted farm losses at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '859'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': '<b> Deduct</b> : Restricted farm loss expired (note 11)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '400',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '400'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': 'Restricted farm losses at the beginning of the tax year (amount d <b> minus </b> amount e)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '402',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '402'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '858',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Restricted farm losses transferred on an amalgamation or on the wind-up of a subsidiary corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '405',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '405'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'label': 'Current-year restricted farm loss (from amount C) <br> Enter amount g on line 233 of Schedule 1, <i> Net Income (Loss) for Income Tax Purposes </i>.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '410',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '410'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'g'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount f <b> plus </b> amount g)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '857'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '856'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Subtotal (amount D <b> plus </b> amount E)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '855'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Restricted farm losses from previous tax years applied against current farming income <br> Enter amount h on line 333 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '430',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '430'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'h'
                }
              }
            ]
          },
          {
            'label': 'Section 80 - Adjustments for forgiven amounts',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '440',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '440'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'i'
                }
              }
            ]
          },
          {
            'label': 'Other adjustments',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '450',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '450'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'j'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts h to j)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '854'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '853'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Restricted farm losses before any request for a carryback (amount F <b>minus</b> amount G) ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '852'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': 'Deduct – Request to carry back restricted farm loss to:',
            'labelClass': 'bold'
          },
          {
            'label': 'First previous tax year to reduce farming income',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '941',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '941'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'k'
                }
              }
            ]
          },
          {
            'label': 'Second previous tax year to reduce farming income',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '942',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '942'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'l'
                }
              }
            ]
          },
          {
            'label': 'Third previous tax year to reduce farming income',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '943',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '943'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'm'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts k to m)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '851'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '850'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Closing balance of restricted farm losses to be carried forward to future tax years (amount H <b> minus </b> amount I)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '480',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '480'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note',
            'labelClass': 'bold tabbed'
          },
          {
            'label': 'The total losses for the year from all farming businesses are calculated without including scientific research expenses',
            'labelClass': 'fullLength indent'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 10: For tax years that end before March 21, 2013, use $6,250 instead of $15,000.',
            'labelClass': 'indent fullLength'
          },
          {
            'label': 'Note 11: A restricted farm loss expires as follows:',
            'labelClass': 'indent fullLength'
          },
          {
            'label': '• after <b>10</b> tax years if it arose in a tax year ending before 2006; and',
            'labelClass': 'tabbed2 fullLength'
          },
          {
            'label': '• after <b>20</b> tax years if it arose in a tax year ending after 2005.',
            'labelClass': 'fullLength tabbed2'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for Listed personal property losses',
        rows: [
          {labelClass: 'fullLength'},
          {
            type: 'table', num: '5070'
          },
          {labelClass: 'fullLength'},
          {
            label: '* Expired'
          },
          {
            label: '** Expiring if not use this year'
          }
        ]
      },
      {
        header: 'Part 5 - Listed personal property losses',
        rows: [
          {
            'label': 'Continuity of listed personal property loss and request for a carryback',
            'labelClass': 'bold'
          },
          {
            'label': 'Listed personal property losses at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '549'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': '<b> Deduct</b> : Listed personal property loss expired after 7 tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '500',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '500'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': 'Listed personal property losses at the beginning of the tax year (amount a <b> minus </b> amount b)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '502',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '502'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '848',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': '<b> Add: </b> Current-year listed personal property loss (from Schedule 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '510'
                },
                'padding': {
                  'type': 'tn',
                  'data': '510'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Subtotal (amount A <b> plus </b> amount B)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '847'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Listed personal property losses from previous tax years applied against listed personal property gains',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '530',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '530'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'Enter amount c on line 655 of Schedule 6.',
            'labelClass': 'indent'
          },
          {
            'label': 'Other adjustments',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '550',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '550'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount c <b> plus </b> amount d)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '846'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '845'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Listed personal property losses remaining before any request for a carryback (amount C <b> minus </b> amount D)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '844'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Deduct - Request to carry back listed personal property loss to:',
            'labelClass': 'bold'
          },
          {
            'label': 'First previous tax year to reduce listed personal property gains',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '961',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '961'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': 'Second previous tax year to reduce listed personal property gains',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '962',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '962'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'label': 'Third previous tax year to reduce listed personal property gains',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '963',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '963'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'g'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts e to g)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '843'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '842'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Closing balance of listed personal property losses to be carried forward to future tax years ' +
            '(amount E <b>minus</b> amount F)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '580'
                },
                'padding': {
                  'type': 'tn',
                  'data': '580'
                }
              }
            ],
            'indicator': 'G'
          }
        ]
      },
      //todo: do not need part 6 since we have workchart already
      //{
      //header: 'Part 6 - Analysis of balance of losses by year of origin',
      //  rows: [
      //    {
      //      type: 'table', num: '6000'
      //    },
      //    {labelClass: 'fullLength'},
      //    {label: 'Note 12: A non-capital loss expires as follow: ', labelClass: 'indent'},
      //    {
      //      label: '- after <b> 10 </b> tax years if it arose in a tax year-ending after March 22, 2004, ' +
      //      'and before 2006; and',
      //      labelClass: 'tabbed2 fullLength'
      //    },
      //    {
      //      label: '- after <b> 20 </b> tax years if it arose in a tax year-ending after 2005.',
      //      labelClass: 'tabbed2'
      //    },
      //    //{labelClass: 'fullLength'},
      //    {
      //      label: 'An allowable business investment loss becomes a net capital loss after <b> 10 </b> tax ' +
      //      'years if it arose in a tax year-ending after March 22, 2004.',
      //      labelClass: 'tabbed2 fullLength'
      //    }
      //  ]
      //},
      {
        header: 'Part 7 - Limited partnership losses',
        rows: [
          {label: 'Current-year limited partnership losses', labelClass: 'bold tabbed2'},
          {
            type: 'table', num: '700'
          },
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {
            label: 'Limited partnership losses from previous tax years that may be applied in the current year',
            labelClass: 'tabbed2 bold fullLength'
          },
          {
            type: 'table', num: '710'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Continuity of limited partnership losses that can be carried forward to future tax years',
            labelClass: 'fullLength bold tabbed2'
          },
          {
            type: 'table', num: '720'
          }
        ]
      },
      {
        header: 'Part 8 - Election under paragraph 88(1.1)(f)',
        rows: [
          {labelClass: 'fullLength'},
          {
            label: 'If you are making an election under paragraph 88(1.1)(f), check the box',
            inputType: 'singleCheckbox',
            num: '190',
            tn: '190',
            type: 'infoField',
            labelWidth: '75%'
          },
          {labelClass: 'fullLength'},
          {
            label: 'In the case of the wind-up of a subsidiary, if the election is made, the non-capital loss, ' +
            'restricted farm loss, farm loss, or limited partnership loss of the subsidiary—that otherwise would ' +
            'become the loss of the parent corporation for a particular tax year starting after the the wind–up' +
            ' began—will be considered  as the loss of the parent corporation for its immediately preceding' +
            ' tax year and not for the particular year.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {label: 'Note', labelClass: 'indent bold'},
          {
            label: 'This election is only applicable for wind-ups under subsection 88(1) that are reported on' +
            ' Schedule 24, <i> First-Time Filer after Incorporation, Amalgamation, or Winding-Up of a Subsidiary' +
            ' into a Parent </i>.',
            //+ 'and the deemed provision is only for the tax years that start after the' +
            //' commencement of the wind-up.',
            labelClass: 'fullLength indent'
          }
        ]
      }
    ]
  };
})();
