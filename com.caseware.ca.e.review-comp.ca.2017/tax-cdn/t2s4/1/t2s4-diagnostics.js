(function() {
  wpw.tax.create.diagnostics('t2s4', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    var depFields = [
      't2j.331',
      't2j.332',
      't2j.333',
      't2j.334',
      't2j.335',
      't2s3.335',
      't2s3.345',
      't2s6.655'
    ];

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S4'), forEach.row('700', forEach.bnCheckCol(0, true))));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S4'), forEach.row('710', forEach.bnCheckCol(0, true))));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S4'), forEach.row('720', forEach.bnCheckCol(0, true))));

    diagUtils.diagnostic('0040001', common.prereq(common.check(depFields, 'isNonZero'), common.requireFiled('T2S4')));

    diagUtils.diagnostic('0040002', common.prereq(common.check('t2j.204', 'isChecked'), common.requireFiled('T2S4')));

    diagUtils.diagnostic('0040003', common.prereq(common.requireFiled('T2S4'), function(tools) {
      var table = tools.field('700');
      return tools.checkAll(table.getRows(), function(row) {
        if (row[2].isNonZero())
          return tools.requireAll([row[0], row[1]], 'isNonZero');
        else return true;
      });
    }));

    diagUtils.diagnostic('0040004', common.prereq(common.requireFiled('T2S4'), function(tools) {
      var table = tools.field('710');
      return tools.checkAll(table.getRows(), function(row, index) {
        if (tools.field('720').getRow(index)[4].isNonZero())
          return tools.requireAll([row[1], row[3], row[4]], 'isNonZero');
        else return true;
      });
    }));

    diagUtils.diagnostic('0040010', {
      label: 'There is an entry in one of columns 004634, 004636, 004638 or 04650, but for the same row ' +
      'there is no entry in column 004630.'
    }, common.prereq(common.requireFiled('T2S4'), function(tools) {
      var table = tools.field('710');
      return tools.checkAll(table.getRows(), function(row) {
        if (tools.checkMethod([row[2], row[3], row[4], row[6]], 'isNonZero')) {
          return row[0].require('isNonZero');
        }
        return true;
      });
    }));

    diagUtils.diagnostic('0040010', {
      label: 'There is an entry in one of columns 004662, 004664, 004670, 004675 or 04680, but for the ' +
      'same row there is no entry in column 004660.'
    }, common.prereq(common.requireFiled('T2S4'), function(tools) {
      var table = tools.field('720');
      return tools.checkAll(table.getRows(), function(row) {
        if (tools.checkMethod([row[1], row[2], row[3], row[4], row[5]], 'isNonZero')) {
          return row[0].require('isNonZero');
        }
        return true;
      });
    }));

    diagUtils.diagnostic('004.atRisk', common.prereq(
        common.requireFiled('T2S4'),
        function(tools) {
          var table = tools.field('700');
          table = tools.mergeTables(table, tools.field('710'));
          return tools.checkAll(table.getRows(), function(row) {
            if (row[2].isNonZero())
              return row[3].require('isNotDefault');
            else if (row[9].isNonZero())
              return row[10].require('isNotDefault');
            else return true;
          });
        }));

  });
})();
