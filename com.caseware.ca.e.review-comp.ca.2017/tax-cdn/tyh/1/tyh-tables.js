(function() {
  wpw.tax.create.tables('tyh', {
    '200': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          header: 'Previous Tax Year',
          type: 'none',
          cellClass: 'alignRight'
        },
        {colClass: 'std-padding-width', type: 'none'},
        {
          colClass: 'std-input-width',
          type: 'date',
          header: 'Tax year-start'
        },
        {colClass: 'std-padding-width', type: 'none'},
        {
          colClass: 'small-input-width',
          type: 'text',
          'textAlign': 'center',
          disabled: true
        },
        {colClass: 'std-padding-width', type: 'none'},
        {
          colClass: 'std-input-width',
          type: 'date',
          header: 'Tax year-end'
        },
        {colClass: 'std-padding-width', type: 'none'},
        {
          colClass: 'small-input-width',
          type: 'text',
          'textAlign': 'center',
          disabled: true
        },
        {colClass: 'std-padding-width', type: 'none'},
        {
          colClass: 'std-input-width',
          header: '# of days in tax year',
          type: 'text',
          'textAlign': 'center',
          disabled: true
        },
        {colClass: 'small-input-width', type: 'none'}
      ],
      cells: [
        {0: {label: 'Twentieth'}},
        {0: {label: 'Nineteenth'}},
        {0: {label: 'Eighteenth'}},
        {0: {label: 'Seventeenth'}},
        {0: {label: 'Sixteenth'}},
        {0: {label: 'Fifteenth'}},
        {0: {label: 'Fourteenth'}},
        {0: {label: 'Thirteenth'}},
        {0: {label: 'Twelfth'}},
        {0: {label: 'Eleventh'}},
        {0: {label: 'Tenth'}},
        {0: {label: 'Ninth'}},
        {0: {label: 'Eighth'}},
        {0: {label: 'Seventh'}},
        {0: {label: 'Sixth'}},
        {0: {label: 'Fifth'}},
        {0: {label: 'Fourth'}},
        {0: {label: 'Third'}},
        {0: {label: 'Second'}},
        {0: {label: 'First'}},
        {0: {label: 'Current'}}
      ]
    }
  })
})();

