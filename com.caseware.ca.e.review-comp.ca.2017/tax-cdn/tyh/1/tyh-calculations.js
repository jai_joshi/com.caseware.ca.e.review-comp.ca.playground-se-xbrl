(function() {

  function table200Calcs(calcUtils, field) {
    var table = field('200');


    table.cell(20, 2).assign(field('CP.tax_start').get());
    table.cell(20, 4).assign(table.cell(20, 2).getKey('year'));
    table.cell(20, 6).assign(field('CP.tax_end').get());
    table.cell(20, 8).assign(table.cell(20, 6).getKey('year'));
    table.cell(20, 10).assign(wpw.tax.actions.calculateDaysDifference(
        table.cell(20, 2).get(), table.cell(20, 6).get()));

    for (var row = 19; row > -1; row--) {
      table.cell(row, 4).assign(table.cell(row, 2).getKey('year'));
      if (!wpw.tax.utilities.isNullUndefined(table.cell(row + 1, 2).get()) &&
          !wpw.tax.utilities.isNullUndefined(table.cell(row, 2).get())) {
        table.cell(row, 6).assign(calcUtils.addToDate(table.cell(row + 1, 2).get(), 0, 0, -1));
      }
      else {
        table.cell(row, 6).assign(null);
      }
      table.cell(row, 8).assign(table.cell(row, 6).getKey('year'));
      if (!wpw.tax.utilities.isNullUndefined(table.cell(row, 2).get()) &&
          !wpw.tax.utilities.isNullUndefined(table.cell(row, 2).get())) {
        table.cell(row, 10).assign(wpw.tax.actions.calculateDaysDifference(
            table.cell(row, 2).get(), table.cell(row, 6).get()));
      }
      else {
        table.cell(row, 10).assign(null)
      }
    }
  }

  wpw.tax.create.calcBlocks('tyh', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      var limitDate = field('CP.105').get();
      if (wpw.tax.utilities.isNullUndefined(limitDate)) {
        limitDate = wpw.tax.date(1900, 1, 1);
      }
      var row;
      if (field('100').get()) {
        for (row = 19; row > -1; row--) {
          if (!wpw.tax.utilities.isNullUndefined(field('200').cell(row + 1, 2).get())) {
            field('200').cell(row, 2).assign(calcUtils.addToDate(
                field('200').cell(row + 1, 2).get(), -1, 0, 0));
          }
          if (!wpw.tax.actions.calculateDaysDifference(limitDate, field('200').cell(row, 2).get()) > 0) {
            field('200').cell(row, 2).assign(limitDate);
            row--;
            while(row > -1) {
              field('200').cell(row, 2).assign(null);
              row--;
            }
          }
        }
      }
      else if (calcUtils.hasChanged('100') && !calcUtils.hasChanged('200') && !field('100').get()) {
        for (row = 19; row > -1; row--) {
          field('200').cell(row, 2).assign(null);
          field('200').cell(row, 2).disabled(false);
        }
      }
    });
    calcUtils.calc(function(calcUtils, field) {
      table200Calcs(calcUtils, field);
    });
  });
})();
