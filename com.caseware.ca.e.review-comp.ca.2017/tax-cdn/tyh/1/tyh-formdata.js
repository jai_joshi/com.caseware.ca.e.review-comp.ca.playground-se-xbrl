(function() {

  wpw.tax.create.formData('tyh', {
    'formInfo': {
      abbreviation: 'tyh',
      'title': 'Taxation Year History',
      'showCorpInfo': true,
      category: 'Workcharts',
      'description': [
        {
          'type': 'list',
          'items': [
            'Use this form to define the dates of the prior taxation years',
            'The updated preceding taxation year in this form will be automatically updated to the applicable ' +
            'schedules']
        }
      ]
    },
    'sections': [
      {
        hideFieldset: true,
        rows: [
          {
            'type': 'horizontalLine'
          },
          {
            'label': 'Select the box to update the dates of the prior taxation years<br>',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'init': true,
            'num': '100'
          },
          {
            'label': '<b>NOTE</b>:',
            'labelClass': 'tabbed'
          },
          {
            'label': '- By deselecting the box, all of previously updated dates will be deleted <br>- To calculate taxation year history after changing tax year-start date or date of incorporation, deselect the box and select it again.',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '200'
          }
        ]
      }
    ]
  });
})();
