/**
 * Created by philip.boutin on 3/6/2017.
 */
wpw.tax.create.importCalcs('TYH', function(importTools) {

  var importedValues = [[], [], []];

  importTools.intercept('FDLOS.SLIPA[1].TtwlosA1', function(importObj) {
    if (angular.isDefined(importObj.value) && importedValues[0].indexOf(importObj.value) == -1) {
      importTools.form('TYH').setValue('100', false);
      importedValues[0].push(importObj.value);
      importTools.form('TYH').setValue('200', importObj.value, 2, 19 - importObj.rowIndex, 'date');
    }
  });

  importTools.intercept('FDLOS.SLIPA[1].TtwlosA2', function(importObj) {
    if (angular.isDefined(importObj.value) && importedValues[1].indexOf(importObj.value) == -1) {
      importedValues[1].push(importObj.value);
      importTools.form('TYH').setValue('200', importObj.value, 4, 19 - importObj.rowIndex);
    }
  });

  importTools.intercept('FDLOS.SLIPA[1].TtwlosA4', function(importObj) {
    if (angular.isDefined(importObj.value) && importedValues[2].indexOf(importObj.value) == -1) {
      importedValues[2].push(importObj.value);
      importTools.form('TYH').setValue('200', importObj.value, 8, 19 - importObj.rowIndex);
    }
  });
});
