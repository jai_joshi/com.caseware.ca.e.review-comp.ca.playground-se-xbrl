(function() {
  wpw.tax.create.diagnostics('tyh', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('TYH.priorIncorporation', common.prereq(common.and(function(tools) {
          var table = tools.field('200');
          return tools.checkAll(table.getRows(), function(row) {
            return row[4].isNonZero() && row[8].isNonZero();
          });
        }, common.check('cp.105', 'isNonZero')),
        function(tools) {
          var table = tools.field('200');
          var yearIncorporated = tools.field('cp.105').getKey('year');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[4].get() < yearIncorporated || row[8].get() < yearIncorporated) {
              return tools.requireAll([row[4], row[8]], function() {
                row[2].mark();
                return this.get() >= yearIncorporated;
              });
            }return true;
          });
        }));

    diagUtils.diagnostic('TYH.afterCurrentDate', function(tools) {
      var table = tools.field('200');
      var currentYear = wpw.tax.date.today().year;
      return tools.checkAll(table.getRows(), function(row) {
        if (row[8].get() > currentYear) {
          return row[8].require(function() {
            return this.get() <= currentYear;
          })
        }return true;
      });
    });

    diagUtils.diagnostic('TYH.numberDaysInYear', function(tools) {
      var table = tools.field('200');
      return tools.checkAll(table.getRows(), function(row) {
          if (row[2].get()) {
            return row[10].require(function() {
              //If cp.063 is yes then the tax year can be max 378, otherwise max is 371
              return this.get() >= '365' &&
                  this.get() <= (tools.field('cp.063').isYes() ? 378 : 371);
            })
          }return true;
      });
    });

  });
})();
