(function () {
  var columnWidth = '107px';
  var columnSpaceWidth = '5px';
  var userDefinedWidth = '50px';

  function getColumns(isFirstTable, isUserDefinedTable) {
    var tableColumns = [];

    if (isUserDefinedTable) {
      tableColumns.push(
        {header: '<b>Field description</b>', type: 'text'},
        {colClass: 'std-spacing-width', type: 'none'},
        {width: userDefinedWidth, cellClass: 'alignRight', header: '<b>Form ID</b>', type: 'text'},
        {colClass: 'std-spacing-width', type: 'none'},
        {width: userDefinedWidth, cellClass: 'alignRight', header: '<b>Field ID</b>', type: 'text'}
      )
    }
    else {
      tableColumns.push(
        {type: 'none'}
      )
    }

    tableColumns.push(
      {colClass: 'std-spacing-width', type: 'none'},
      {
        header: isFirstTable ? '<b>Current Scenario</b>' : '',
        colClass: 'std-input-width',
        cellClass: 'alignRight',
        disabled: true,
        decimals: 0,
        filters: 'number'
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        header: isFirstTable ? '<b>Frozen Scenario 1</b>' : '',
        colClass: 'std-input-width',
        cellClass: 'alignRight',
        disabled: true,
        decimals: 0,
        filters: 'number'
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        header: isFirstTable ? '<b>Frozen Scenario 2</b>' : '',
        colClass: 'std-input-width',
        cellClass: 'alignRight',
        disabled: true,
        decimals: 0,
        filters: 'number'
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        header: isFirstTable ? '<b>$</b>' : '',
        colClass: 'std-input-width',
        cellClass: 'alignRight',
        disabled: true,
        decimals: 0,
        filters: 'number'
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        header: isFirstTable ? '<b>%</b>' : '',
        colClass: 'std-input-width',
        cellClass: 'alignRight',
        disabled: true,
        decimals: 2,
        filters: 'number'
      },
      {type: 'none', width: isUserDefinedTable ? '21px' :'72px'}
    );

    return tableColumns
  }

  function getCells(heading, labelArray, isProvince) {
    if (!angular.isArray(labelArray))
      labelArray = [labelArray];

    var tableRows = [
      {
        0: {label: heading, labelClass: 'bold'},
        2: {type: 'none'},
        4: {type: 'none'},
        6: {type: 'none'},
        8: {type: 'none'},
        10: {type: 'none'}
      }
    ];

    if (isProvince) {
      labelArray = [
        {label: '% Allocation'},
        {label: 'Attributed taxable income'},
        {label: 'Tax payable before deduction'},
        {label: 'Deductions/credits'},
        {label: 'Net tax payable'},
        {label: 'Taxable capital'},
        {label: 'Capital tax payable'},
        {label: 'Instalments and refundable credits'},
        {label: 'Capital tax balance due/refund'}
      ]
    }

    labelArray.forEach(function (rowDataObj) {
      var inputRow = {0: {label: rowDataObj.label}};
      var colScenarioArray = [2, 4, 6];
      var colDiffArray = [8, 10];

      colScenarioArray.forEach(function (cIndex) {
        inputRow[cIndex] = {type: rowDataObj.type};
      });

      if (rowDataObj.type == 'text') {
        colDiffArray.forEach(function (cIndex) {
          inputRow[cIndex] = {type: 'none'};
        });
      } else {
        colDiffArray.forEach(function (cIndex) {
          inputRow[cIndex] = {type: rowDataObj.type};
        });
      }

      inputRow.formId = rowDataObj.formId;
      inputRow.fieldId = rowDataObj.fieldId;

      tableRows.push(inputRow);
    });

    return tableRows;
  }

  wpw.tax.global.tableCalculations.scenarioWorkchart = {
    "500": {
      type: 'table', fixedRows: true, num: '500', infoTable: true, dividers: [8],
      scrollx: true,
      superHeaders: [
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {
          header: 'Differences',
          divider: true,
          colspan: '5'
        }
      ],
      columns: getColumns(true),

      cells: getCells('',
        [
          {label: 'Scenario description', type: 'text'},
          {label: 'Net income (loss) for income tax purpose', formId: 'T2J', fieldId: '360'},
          {
            label: 'Taxable income for a corporation with exempt income under paragraph 149(1)(t)',
            formId: 'T2J',
            fieldId: '371'
          },

          {label: 'Part I tax payable', formId: 'T2J', fieldId: '700'},
          {label: 'Dividend refund', formId: 'T2J', fieldId: '503'}
        ]
      )
    },
    "600": {
      type: 'table', num: '600', showNumbering: true,
      infoTable: true,
      dividers: [12],
      columns: getColumns(false, true)
    },
    "701": {
      type: 'table', num: '701', fixedRows: true, infoTable: true, dividers: [8],
      showWhen: '700',
      columns: getColumns(),
      cells: getCells('Federal taxes', [
        {label: 'Part I', formId: 'T2J', fieldId: '700'},
        {label: 'Part IV', formId: 'T2J', fieldId: '712'},
        {label: 'Part III.1', formId: 'T2J', fieldId: '710'}
      ])
    },
    "711": {
      type: 'table', num: '711', fixedRows: true, infoTable: true, dividers: [8], executeAtEnd: true,
      showWhen: '700',
      columns: getColumns(),
      cells: getCells('Credits against part I tax', [
        {label: 'Small business deduction', formId: 'T2J', fieldId: '607'},
        {label: 'M&P deduction', formId: 'T2J', fieldId: '616'},
        {label: 'Foreign tax credit', formId: 'T2J', fieldId: '636'},
        {label: 'Investment tax credit', formId: 'T2J', fieldId: '652'},
        {label: 'Abatement tax credit', formId: 'T2J', fieldId: '608'}
      ])
    },
    "721": {
      type: 'table', num: '721', fixedRows: true, infoTable: true, dividers: [8],
      showWhen: '700',
      columns: getColumns(),
      cells: getCells('Refunds/credits', [
        {label: 'ITC refund', formId: 'T2J', fieldId: '780'},
        {label: 'Dividend refund', formId: 'T2J', fieldId: '784'},
        {label: 'Instalments', formId: 'T2J', fieldId: '840'},
        {label: 'Surtax credit', formId: 'T2J', fieldId: ''}//TODO: from S37 but not avaiable yet -surtax credit
      ])
    },
    "731": {
      type: 'table', num: '731', fixedRows: true, infoTable: true, dividers: [8],
      showWhen: '700',
      columns: getColumns(),
      cells: getCells('Newfoundland and Labrador', [], true)
    },
    "741": {
      type: 'table', num: '741', fixedRows: true, infoTable: true, dividers: [8],
      showWhen: '700',
      columns: getColumns(),
      cells: getCells('Nova Scotia', [], true)
    },
    "751": {
      type: 'table', num: '751', fixedRows: true, infoTable: true, dividers: [8],
      showWhen: '700',
      columns: getColumns(),
      cells: getCells('New Brunswick', [], true)
    },
    "761": {
      type: 'table', num: '761', fixedRows: true, infoTable: true, dividers: [8],
      showWhen: '700',
      columns: getColumns(),
      cells: getCells('Ontario', [], true)
    },
    "771": {
      type: 'table', num: '771', fixedRows: true, infoTable: true, dividers: [8],
      showWhen: '700',
      columns: getColumns(),
      cells: getCells('Manitoba', [], true)
    },
    "781": {
      type: 'table', num: '781', fixedRows: true, infoTable: true, dividers: [8],
      showWhen: '700',
      columns: getColumns(),
      cells: getCells('Saskatchewan', [], true)
    },
    "791": {
      type: 'table', num: '791', fixedRows: true, infoTable: true, dividers: [8],
      showWhen: '700',
      columns: getColumns(),
      cells: getCells('British Columbia', [], true)
    },
    "801": {
      type: 'table', num: '801', fixedRows: true, infoTable: true, dividers: [8],
      showWhen: '700',
      columns: getColumns(),
      cells: getCells('Yukon', [], true)
    },
    "811": {
      type: 'table', num: '811', fixedRows: true, infoTable: true, dividers: [8],
      showWhen: '700',
      columns: getColumns(),
      cells: getCells('Alberta', [], true)
    },
    "821": {
      type: 'table', num: '821', fixedRows: true, infoTable: true, dividers: [8],
      showWhen: '700',
      columns: getColumns(),
      cells: getCells('Quebec', [], true)
    }
  }
})
();
