(function() {

  function calcsDif(calcUtils, summaryRow, isUserDefinedTable) {
    var num103 = calcUtils.field('103').get();
    if (num103 == 1) {
      if (isUserDefinedTable) {
        summaryRow[12].assign(summaryRow[6].get() - summaryRow[8].get());
        summaryRow[14].assign(summaryRow[6].get() == 0 ? 0 : summaryRow[12].get() / summaryRow[6].get() * 100);
      }
      else {
        summaryRow[8].assign(summaryRow[2].get() - summaryRow[4].get());
        summaryRow[10].assign(summaryRow[2].get() == 0 ? 0 : summaryRow[8].get() / summaryRow[2].get() * 100);
      }
    }
    else if (num103 == 2) {
      if (isUserDefinedTable) {
        summaryRow[12].assign(summaryRow[6].get() - summaryRow[10].get());
        summaryRow[14].assign(summaryRow[6].get() == 0 ? 0 : summaryRow[12].get() / summaryRow[6].get() * 100);
      }
      else {
        summaryRow[8].assign(summaryRow[2].get() - summaryRow[6].get());
        summaryRow[10].assign(summaryRow[2].get() == 0 ? 0 : summaryRow[8].get() / summaryRow[2].get() * 100);
      }
    }
    else {
      if (isUserDefinedTable) {
        summaryRow[12].assign(summaryRow[8].get() - summaryRow[10].get());
        summaryRow[14].assign(summaryRow[8].get() == 0 ? 0 : summaryRow[12].get() / summaryRow[8].get() * 100);
      }
      else {
        summaryRow[8].assign(summaryRow[4].get() - summaryRow[6].get());
        summaryRow[10].assign(summaryRow[4].get() == 0 ? 0 : summaryRow[8].get() / summaryRow[4].get() * 100);
      }
    }
  }

  function frozenScenario(calcUtils, conditionFieldId, tableNumArray, userDefinedTableNum) {
    var field = calcUtils.field;
    if (field(conditionFieldId).get() == 1) {
      tableNumArray.forEach(function(tableNum) {
        field(tableNum).getRows().forEach(function(row) {
          if (tableNum == userDefinedTableNum) {
            if (conditionFieldId === 'freeze1') {
              row[8].assign(row[6].get());
            } else if (conditionFieldId === 'freeze2') {
              row[10].assign(row[6].get());
            }
          }
          else {
            if (conditionFieldId === 'freeze1') {
              row[4].assign(row[2].get());
            } else if (conditionFieldId === 'freeze2') {
              row[6].assign(row[2].get());
            }
          }
        });
      });
      field(conditionFieldId).assign(2);
    }
  }

  wpw.tax.create.calcBlocks('scenarioWorkchart', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field, form) {
      //update description
      field('500').cell(1, 2).assign(field('106').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var userDefinedTableNum = 600;
      var tableNumArray = [500, 600, 701, 711, 721, 731, 741, 751, 761, 771, 781, 791, 801, 811, 821];
      //populate Global Value to current scenario
      tableNumArray.forEach(function(tableNum) {
        field(tableNum).getRows().forEach(function(row, rowIndex) {
          if (tableNum == userDefinedTableNum) {
            row[6].assign(form(row[2].get()).field(row[4].get()).get());
            row[6].source(form(row[2].get()).field(row[4].get()));
          } else {
            var rowInfo = field(tableNum).getRowInfo(rowIndex);
            var formId = rowInfo.formId;
            var fieldId = rowInfo.fieldId;
            if (formId && fieldId) {
              row[2].assign(form(formId).field(fieldId).get());
              row[2].source(form(formId).field(fieldId));
            }
          }
        });
      });

      //frozen current scenario to frozen scenario 1
      frozenScenario(calcUtils, 'freeze1', tableNumArray, userDefinedTableNum);
      //frozen current scenario to frozen scenario 2
      frozenScenario(calcUtils, 'freeze2', tableNumArray, userDefinedTableNum);

      //calcs diff
      tableNumArray.forEach(function(tableNum) {
        var isUserDefinedTable = (tableNum == userDefinedTableNum);
        field(tableNum).getRows().forEach(function(row) {
          calcsDif(calcUtils, row, isUserDefinedTable)
        });
      });
    });
  });
})();
