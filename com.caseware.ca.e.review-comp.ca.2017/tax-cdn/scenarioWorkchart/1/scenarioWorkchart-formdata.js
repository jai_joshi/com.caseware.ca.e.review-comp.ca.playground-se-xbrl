(function() {

  var freezeFunc1 = function() {
    wpw.tax.field('freeze1').set(1);
  };
  var freezeFunc2 = function() {
    wpw.tax.field('freeze2').set(1);
  };

  wpw.tax.global.formData.scenarioWorkchart = {
      formInfo: {
        abbreviation: 'TS',
        title: 'Tax Scenario Workchart',
        showCorpInfo: true,
        category: 'Workcharts'
      },
      sections: [
        {

          rows: [
            {labelClass: 'fullLength'},
            {
              type: 'infoField',
              label: 'Show difference between :',
              labelWidth: '30%',
              num: '103',
              inputType: 'dropdown',
              init: '1',
              options: [
                {option: 'Current Scenario vs Frozen scenario 1', value: '1'},
                {option: 'Current Scenario vs Frozen scenario 2', value: '2'},
                {option: 'Frozen scenario 1 vs Frozen scenario 2', value: '3'}
              ]
            },
            {
              type: 'infoField',
              inputType: 'functionButton',
              label: 'Freeze current scenario To Frozen scenario 1',
              labelClass: 'bold',
              btnLabel: 'Freeze',
              paramFn: freezeFunc1
            },
            {
              type: 'infoField',
              inputType: 'functionButton',
              label: 'Freeze current scenario To Frozen scenario 2',
              labelClass: 'bold',
              btnLabel: 'Freeze',
              paramFn: freezeFunc2
            },
            {labelClass: 'fullLength'},
            {
              type: 'infoField',
              label: 'Current scenario description',
              num: '106'
            },
            {
              type: 'table', num: '500'
            },
            {labelClass: 'fullLength'},
            {
              label: 'Users defined',
              labelClass: 'bold'
            },
            {
              type: 'table', num: '600'
            },
            {labelClass: 'fullLength'},
            {
              type: 'infoField',
              label: 'Show more tax details ?',
              labelClass: 'bold',
              num: '700',
              inputType: 'radio',
              init: '2'
            },
            {
              type: 'table', num: '701'
            },
            {
              type: 'table', num: '711'
            },
            {
              type: 'table', num: '721'
            },
            {
              type: 'table', num: '731'
            },
            {
              type: 'table', num: '741'
            },
            {
              type: 'table', num: '751'
            },
            {
              type: 'table', num: '761'
            },
            {
              type: 'table', num: '771'
            },
            {
              type: 'table', num: '781'
            },
            {
              type: 'table', num: '791'
            },
            {
              type: 'table', num: '801'
            },
            {
              type: 'table', num: '811'
            },
            {
              type: 'table', num: '821'
            }
          ]
        }
      ]
    };
})();
