(function() {
  'use strict';

  wpw.tax.global.formData.t2s24 = {
    'formInfo': {
      'abbreviation': 'T2S24',
      'title': 'First Time Filer after Incorporation, Amalgamation or Winding-up of a Subsidiary into a Parent',
      'schedule': 'Schedule 24',
      'code': 'Code 0401',
      formFooterNum: 'T2 SCH 24 (04)',
      'showCorpInfo': true,
      'description': [
        {
          'text': 'This schedule must be filed by corporations for the first year of filing after incorporation, ' +
          'amalgamation, or by parent corporations filing for the first time after winding-up a subsidiary' +
          ' corporation(s) under section 88 of the <i>Income Tax Act</i> during the current taxation year.'
        }
      ],
      category: 'Federal Tax Forms'
    },
    'sections': [
      {
        'header': 'Part 1 – Type of operation',
        'rows': [
          {
            'label': 'For those corporations filing for the first time after incorporation or amalgamation, ' +
            'please identify the type of operation that applies to your corporation:',
            'tn': '100',
            'num': '100',
            type: 'infoField', inputType: 'dropdown',
            options: [
              {value: '1', option: '01  Crown corporation that is an agent of Her Majesty'},
              {value: '2', option: '02  Life insurance corporation'},
              {value: '3', option: '03  Deposit insurance corporation'},
              {value: '4', option: '04  General insurance corporation'},
              {value: '5', option: '05  Co-operative corporation'},
              {value: '6', option: '06  Credit union'},
              {value: '7', option: '07  Bank'},
              {value: '9', option: '09  Investment public corporation'},
              {value: '10', option: '10  Mutual fund corporation'},
              {value: '11', option: '11  Mortgage investment corporation'},
              {value: '12', option: '12  Travelling corporation'},
              {value: '13', option: '13  Subject corporation'},
              {value: '14', option: '14  Labour-sponsored venture capital corporation'},
              {value: '15', option: '15  Investment public corporation subject to Part IV tax'},
              {value: '16', option: '16  Crown corporation that is not an agent of Her Majesty'},
              {value: '17', option: '17  Non-resident insurer exempt from Part XIII withholding tax'},
              {value: '99', option: '99  Other – if none of the previous descriptions apply'}
            ]
          }
        ]
      },
      {
        'header': 'Part 2 - First year of filing after amalgamation',
        'rows': [
          {'label': 'For the first year of filing after an amalgamation, please provide the following information: '},
          {
            'type': 'table', 'num': '150'
          }
        ]
      },
      {
        'header': 'Part 3 – First year of filing after wind-up of subsidiary corporation(s) ',
        'rows': [
          {
            'label': 'For the parent corporation filing for the first time after winding-up ' +
            'a subsidiary corporation(s) under section 88 of the <i>Income Tax Act</i>,' +
            ' please provide the following information:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table', 'num': '350'
          }
        ]
      }
    ]
  };
})();
