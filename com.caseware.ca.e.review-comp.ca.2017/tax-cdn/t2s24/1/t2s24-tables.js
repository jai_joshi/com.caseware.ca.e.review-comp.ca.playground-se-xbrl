(function() {
  wpw.tax.global.tableCalculations.t2s24 = {
    '150': {
      'showNumbering': true,
      'columns': [{
        'header': 'Name of predecessor corporation(s)',
        'num': '200','validate': {'or':[{'length':{'min':'1','max':'175'}},{'check':'isEmpty'}]},
        'width': '70%',
        'tn': '200',
        type: 'text'
      },
        {
          'header': 'Business number (If a corporation is not registered, enter "NR")',
          'num': '300',
          'tn': '300',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        }]
    },
    '350': {
      'showNumbering': true,
      'columns': [{
        'header': 'Name of subsidiary corporation(s)',
        'num': '400','validate': {'or':[{'length':{'min':'1','max':'175'}},{'check':'isEmpty'}]},
        'tn': '400',
        type: 'text'
      },
        {
          'header': 'Business number (if a corporation is not registered, enter "NR")',
          'num': '500',
          'width': '25%',
          'tn': '500',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          'header': 'Commencement date of wind-up',
          'num': '600',
          'width': '18%',
          'tn': '600',
          'type': 'date'
        },
        {
          'header': 'Date of wind-up',
          'num': '700',
          'width': '18%',
          'tn': '700',
          'type': 'date'
        }]
    }
  }
})();
