(function() {
  wpw.tax.create.diagnostics('t2s24', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S24'), forEach.row('150', forEach.bnCheckCol(1, true))));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S24'), forEach.row('350', forEach.bnCheckCol(1, true))));

    diagUtils.diagnostic('0240001', common.prereq(common.and(
        common.requireFiled('T2S24'),
        common.check(['t2j.070', 't2j.071'], 'isYes')),
        function(tools) {
          return tools.field('100').require('isFilled');
        }));

    diagUtils.diagnostic('0240002', common.prereq(common.and(
        common.requireFiled('T2S24'),
        common.check(['t2j.070'], 'isYes')),
        common.requireFiled('T2S101')));

    diagUtils.diagnostic('0240003', common.prereq(
        common.check(['t2j.071'], 'isYes'),
        common.requireFiled('T2S24')));

    diagUtils.diagnostic('0240005', common.prereq(common.and(
        common.requireFiled('T2S24'),
        common.check(['t2j.071'], 'isYes')), function(tools) {
      var table = tools.field('150');
      return tools.checkAll(table.getRows(), function(row) {
        return tools.requireAll([row[0], row[1]], 'isNonZero');
      });
    }));

    diagUtils.diagnostic('0240006', common.prereq(common.check(['t2j.072'], 'isYes'), common.requireFiled('T2S24')));

    diagUtils.diagnostic('0240008', common.prereq(common.and(
        common.requireFiled('T2S24'),
        common.check(['t2j.072'], 'isYes')),
        function(tools) {
          var table = tools.field('350');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireAll([row[0], row[2], row[3]], 'isNonZero') &&
                tools.requireOne([row[0], row[1], row[2], row[3]], 'isNonZero') &&
                (tools.requireAll([row[0], row[2], row[3]], 'isNonZero') && tools.requireOne([row[1]], 'isNonZero'));
          });
        }));

    diagUtils.diagnostic('0241009', {label: 'There must be at least 2 rows and for every row there must be corresponding values in each column. The corporation has answered Yes (1) at line 200071, but the information provided on Schedule 24 is incomplete and/or is not correct.'},
        common.prereq(common.and(
            common.requireFiled('T2S24'),
            common.check(['t2j.071'], 'isYes')),
            function(tools) {
              var table = tools.field('150');
              return tools.checkAll(table.getRows(), function(row) {
                if (table.size().rows < 2)
                  return tools.requireAll([row[0], row[1]], function() {
                    return false;
                  });

                if (tools.checkMethod([row[0], row[1]], 'isNonZero'))
                  return tools.requireAll([row[0], row[1]], 'isNonZero');
              })
            }));

    diagUtils.diagnostic('0241009', {label: 'The business number reported in column 300 is the same as the filing corporation. The corporation has answered Yes (1) at line 200071, but the information provided on Schedule 24 is incomplete and/or is not correct.'},
        common.prereq(common.and(
            common.requireFiled('T2S24'),
            common.check(['t2j.071'], 'isYes')),
            function(tools) {
              var corpBN = tools.field('cp.bn').get();
              var table = tools.field('150');
              return tools.checkAll(table.getRows(), function(row) {
                if (row[1].get() == corpBN)
                  return row[1].require(function() {
                    return false;
                  });
                else return true;
              })
            }));

    diagUtils.diagnostic('0241009', {label: 'There are duplicate business numbers entered in column 300. The corporation has answered Yes (1) at line 200071, but the information provided on Schedule 24 is incomplete and/or is not correct.'},
        common.prereq(common.and(
            common.requireFiled('T2S24'),
            common.check(['t2j.071'], 'isYes')),
            function(tools) {
              var table = tools.field('150');
              var duplicate = false;
              var bn = {};
              for (var i = 0; i < table.size().rows; i++) {
                var rowBN = table.cell(i, 1).get();
                if (rowBN && bn[rowBN]) {
                  table.cell(i, 0).mark();
                  table.cell(i, 1).mark();
                  duplicate = true;
                }
                bn[rowBN] = true;
              }
              return !duplicate;
            }));

  });
})();

