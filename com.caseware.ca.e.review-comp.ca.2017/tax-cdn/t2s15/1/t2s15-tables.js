(function() {
  wpw.tax.global.tableCalculations.t2s15 = {
    "101": {
      "showNumbering": true,
      "columns": [{
        "header": "Type of plan <br> (see note 1)",
        "num": "100",
        "tn": "100",
        "width": "10%",
        "type": "dropdown",
        "options": [{
          "value": "1",
          "option": "1 - RPP"
        },
          {
            "value": "2",
            "option": "2 - RSUBP"
          },
          {
            "value": "3",
            "option": "3 - DPSP"
          },
          {
            "value": "4",
            "option": "4 - EPSP"
          },
          {
            "value": "5",
            "option": "5 - PRPP"
          }]
      },
        {
          "header": "Amount of contribution <br> $ <br> (see note 2)",
          "num": "200",
          "tn": "200",
          "total": true,
          "totalNum": "205",
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        },
        {
          "header": "Registration number <br> (RPP, RSUBP, PRPP, and DPSP only)",
          "num": "300",
          "tn": "300",
        "validate": {"or":[{"length":{"min":"1","max":"20"}},{"check":"isEmpty"}]},
          type: 'text'
        },
        {
          "header": "Name of EPSP trust <br> <br>",
          "tn": "400",
          "num": "400",
          "width": "22%",
          "validate": {"or":[{"length":{"min":"1","max":"175"}},{"check":"isEmpty"}]},
          type: 'text'
        },
        {
          "header": "Address of EPSP trust <br> <br>",
          "tn": "500",
          "num": "500",
          "width": "33%",
          "type": "address"
        },
        {
          "header": "T4PS slip(s) filed by: <br> 1 - Trustee <br> 2 - Employer <br> (EPSP only)",
          "tn": "600",
          "num": "600",
          "type": "dropdown",
          "options": [{
            "value": "1",
            "option": "1 - Trustee"
          },
            {
              "value": "2",
              "option": "2 - Employer"
            }]
        }],
      "hasTotals": true
    }
  }
})();
