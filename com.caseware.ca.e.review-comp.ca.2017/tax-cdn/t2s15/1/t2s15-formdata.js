(function() {
  'use strict';

  wpw.tax.global.formData.t2s15 = {
    'formInfo': {
      'abbreviation': 't2s15',
      'title': 'Deferred Income Plans',
      'schedule': 'Schedule 15',
      'code': 'Code 1301',
      formFooterNum: 'T2 SCH 15 E (13)',
      'showCorpInfo': true,
      headerImage: 'canada-federal',
      'description': [
        {
          'type': 'list',
          'items': [
            'Complete the information below if the corporation deducted payments from its income made to a ' +
            'registered pension plan (RPP), a registered supplementary unemployment benefit plan (RSUBP),' +
            ' a deferred profit sharing plan (DPSP), a pooled registered pension plan (PRPP), ' +
            'or an employee profit sharing plan (EPSP).',
            'If the trust that governs an employee profit sharing plan is <b>not resident</b> in Canada, ' +
            'please indicate if the T4PS, <i>Statement of Employees Profit Sharing Plan Allocations and Payments,</i>' +
            ' Supplementary slip(s) were filed for the last calendar year, and whether they were filed ' +
            'by the trustee or the employer.'
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    'sections': [
      {
        hideFieldset: true,
        'rows': [
          {
            'type': 'table',
            'num': '101'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Note 1</b> Enter the applicable code number:'
          },
          {
            'label': '1 - RPP',
            'labelClass': 'tabbed'
          },
          {
            'label': '2 - RSUBP',
            'labelClass': 'tabbed'
          },
          {
            'label': '3 - DPSP',
            'labelClass': 'tabbed'
          },
          {
            'label': '4 - EPSP',
            'labelClass': 'tabbed'
          },
          {
            'label': '5 - PRPP',
            'labelClass': 'tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 2',
            'labelClass': 'bold'
          },
          {
            'label': 'You do not need to add to Schedule 1 any payments you made to deferred income plans. To reconcile such payments, calculate the following amount:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total of all amounts indicated in column 200 of this schedule',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '201'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Less</b>: Total of all amounts for deferred income plans deducted in your financial statements',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '202'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': '<b>Deductible amount for contributions to deferred income plans </b>(amount A minus amount B) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '203'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Enter amount C on line 417 of Schedule 1.'
          }
        ]
      }
    ]
  };
})();
