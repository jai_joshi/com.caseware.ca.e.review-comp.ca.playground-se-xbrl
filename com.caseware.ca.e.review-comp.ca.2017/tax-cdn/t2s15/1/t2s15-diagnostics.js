(function() {
  wpw.tax.create.diagnostics('t2s15', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0150001', common.prereq(common.check(['t2j.165'], 'isChecked'), common.requireFiled('T2S15')));

    diagUtils.diagnostic('0150002', {
      label: 'For each type of plan identified in column 015100, an entry is required in column ' +
      '015200 and for every entry in column 015200; a type of plan is required in column 015100.',
      category: 'EFILE'
    }, common.prereq(common.requireFiled('T2S15'),
        function(tools) {
          var table = tools.field('101');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[0], row[1]], 'isNonZero'))
              return tools.requireAll([row[0], row[1]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0150002', {
      label: 'There is an entry of a 1 (RPP), 2 (RSUBP), 3 (DPSP), or 5 (PRPP) in column 015100, but ' +
      'for the same row there is no corresponding entry in columns 015200 and/or 015300.',
      category: 'EFILE'
    }, common.prereq(common.requireFiled('T2S15'),
        function(tools) {
          var table = tools.field('101');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[0].get() == '1' ||
                row[0].get() == '2' ||
                row[0].get() == '3' ||
                row[0].get() == '5')
              return tools.requireAll([row[1], row[2]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0150002', {
      label: 'There is an entry of a 4 (EPSP) in column 015100, but for the same row there is ' +
      'no corresponding entry in one or more of columns 015200, 015400, 015500, or 015600.',
      category: 'EFILE'
    }, common.prereq(common.requireFiled('T2S15'),
        function(tools) {
          var table = tools.field('101');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[0].get() == '4')
              return tools.requireAll([row[1], row[3], row[4], row[5]], 'isFilled');
            else return true;
          });
        }));

    diagUtils.diagnostic('015.202', common.prereq(common.and(
        common.requireFiled('T2S15'),
        common.check(['201'], 'isNonZero')),
        common.check(['202'], 'isNonZero')));
  });
})();

