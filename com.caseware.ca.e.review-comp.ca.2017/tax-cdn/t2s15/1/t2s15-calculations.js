(function() {

  wpw.tax.create.calcBlocks('t2s15', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.equals('201', '205');

      var sum = 0;
      calcUtils.allRepeatForms('t2s125').forEach(function(t2s125) {
        sum += calcUtils.gifiValue(t2s125, 300, 8623);
      });

      field('202').assign(sum);
      field('202').source(field('T2S125.8623'));
      calcUtils.subtract('203', '201', '202')
    });
  });
})();
