(function() {
  wpw.tax.create.diagnostics('t2s11', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0110001', common.prereq(common.check(['t2j.162'], 'isChecked'), common.requireFiled('T2S11')));

    diagUtils.diagnostic('0110002', {
      label: 'For each relationship code in column 011100, a corresponding entry is required ' +
      'in at least one of columns 011200, 011300, 011400, or 011500.'
    }, common.prereq(common.requireFiled('T2S11'),
        function(tools) {
      var table = tools.field('050');
      return tools.checkAll(table.getRows(), function(row) {
        if (row[0].isNonZero())
          return tools.requireOne([row[1], row[2], row[3], row[4]], 'isNonZero');
        else return true;
      });
    }));

    diagUtils.diagnostic('0110002', {
      label: 'For each asset sold or purchased reported in column 011500, a corresponding ' +
      'entry is required in column 011550.'
    }, common.prereq(common.requireFiled('T2S11'),
        function(tools) {
      var table = tools.field('050');
      return tools.checkAll(table.getRows(), function(row) {
        if (row[4].isNonZero())
          return row[5].require('isNonZero');
        else return true;
      });
    }));

    diagUtils.diagnostic('0110002', {
      label: 'For each entry at one of columns 011200, 011300, 011400, 011500, or 011550, ' +
      'a corresponding entry is required in column 011100.'
    }, function(tools) {
      var table = tools.field('050');
      return tools.checkAll(table.getRows(), function(row) {
        if (tools.checkMethod([row[1], row[2], row[3], row[4], row[5]], 'isNonZero'))
          return row[0].require('isNonZero');
        else return true;
      });
    });
  });
})();
