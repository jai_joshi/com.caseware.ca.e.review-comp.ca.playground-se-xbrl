(function() {
  wpw.tax.global.tableCalculations.t2s11 = {
    "050": {
      "showNumbering": true,
      "columns": [{
        "header": "Relationship code <br>(see note)",
        "num": "100",
        "width": "18%",
        "tn": "100",
        "type": "dropdown",
        "options": [{
          "value": "1",
          "option": "1 - Shareholder"
        },
          {
            "value": "2",
            "option": "2 - Officer"
          },
          {
            "value": "3",
            "option": "3 - Employee"
          }]
      },
        {
          "header": "Payments <br><br> $",
          "num": "200","validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          "tn": "200"
        },
        {
          "header": "Reimbursement<br> (Other than reimbursement of expenses) <br><br> $ ",
          "num": "300","validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          "tn": "300"
        },
        {
          "header": "Loans receivable from, or debts owing to <br> <br> $",
          "num": "400","validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          "tn": "400"
        },
        {
          "header": "Assets sold or purchased <br> <br> $",
          "num": "500","validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          "tn": "500"
        },
        {
          "header": "Does section 85 apply to assets sold or purchased?",
          "num": "550",
          "tn": "550",
          "type": "radio"
        }]
    }
  }
})();
