(function() {
  wpw.tax.global.formData.t2s11 = {
    formInfo: {
      abbreviation: 'T2S11',
      title: 'Transactions with Shareholders, Officers or Employees',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      //subTitle: '(1998 and later taxation years)',
      schedule: 'Schedule 11',
      formFooterNum: 'T2 SCH 11 (00)',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Provide the details of any transactions with shareholders, officers, or employees that involve:',
              sublist:[
                'payments the corporation made or amounts credited to the account of shareholders, officers, or ' +
                'employees, which were not part of their remuneration or reimbursement of expenses',
                'assets the corporation sold to or purchased from shareholders, officers, or employees, including' +
                ' those for which an election was made under section 85; or',
                'loans or indebtedness to shareholders, officers, or employees,  or persons connected with a ' +
                'shareholder, which were not repaid by the end of the taxation year.'
              ]
            }
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            type: 'table', num: '050'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {label: 'Note: ', labelClass: 'bold'},
          {
            label: 'Enter the code number of the relationship that applies: (if more than one relationship exists,' +
            ' enter the lowest applicable number)',
            labelClass: 'fullLength'
          },
          {label: '1- Shareholder'},
          {label: '2- Officer'},
          {label: '3- Employee'}
        ]
      }

    ]
  };
})();
