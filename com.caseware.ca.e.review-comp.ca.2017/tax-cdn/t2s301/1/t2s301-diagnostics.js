(function() {
  wpw.tax.create.diagnostics('t2s301', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3010001', common.prereq(common.and(
        common.requireFiled('T2S301'),
        common.check(['t2s5.520'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['103', '130', '140']), 'isNonZero');
        }));

  });
})();

