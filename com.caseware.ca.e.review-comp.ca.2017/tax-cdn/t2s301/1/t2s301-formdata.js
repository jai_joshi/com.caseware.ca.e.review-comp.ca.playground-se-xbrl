(function() {

  wpw.tax.create.formData('t2s301', {
    formInfo: {
      abbreviation: 'T2S301',
      title: 'Newfoundland and Labrador Research and Development Tax Credit<br> (2004 and later tax year)',
      schedule: 'Schedule 301',
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if you are a corporation with a permanent establishment in ' +
              'Newfoundland and Labrador that has made eligible expenditures for scientific research ' +
              'and experimental development carried out in the province and you want to:',
              sublist: ['calculate a refundable Newfoundland and Labrador research and development ' +
              '(R&D) tax credit on eligible expenditures made in the tax year; or',
                'claim a credit allocated to you as a member of a partnership, or as a beneficiary under a trust.']
            },
            {
              label: 'An eligible expenditure is one that meets the definition of a "qualified expenditure" ' +
              'under subsection 127(9) of the federal <i>Income Tax Act</i>, without reference to paragraph (d) but, ' +
              'with respect to paragraph (h) of the definition.'
            },
            {
              label: 'Eligible expenditures must be identified on this schedule and filed no later than ' +
              '12 months after the <i>T2 Corporation Income Tax Return</i> is due for the tax year in which the ' +
              'expenditures were incurred.'
            },
            {
              label: 'If you apply for the Newfoundland and Labrador R&D tax credit on or after ' +
              'December 1, 2005, reduce the eligible expenditure by the following amounts you have ' +
              'received or claimed in respect of the eligible expenditure:',
              sublist: ['contract payments received after December 31, 2003;',
                'GST/HST input tax credits claimed after December 31, 2003.']
            },
            {
              label: 'Credits earned in the year are applied to reduce Newfoundland and Labrador ' +
              'income tax otherwise payable for the year, as well as tax, interest, or penalty owing ' +
              'by the corporation for that or any prior tax year under the federal and provincial ' +
              '<i>Income Tax Act</i>s, and amounts owing under the <i>Canada Pension Plan</i> and the ' +
              '<i>Employment Insurance Act</i>. Any remaining balance will be refunded.'
            },
            {
              label: 'The credit can only be refunded to corporations that are not exempt from tax under ' +
              'section 149 of the federal <i>Income Tax Act</i>.'
            },
            {
              label: 'File one completed copy of this schedule with your <i>T2 Corporation Income Tax Return</i>.'
            }
          ]
        }
      ],
      category: 'Newfoundland and Labrador Forms'
    },
    sections: [
      {
        'header': 'Calculation of current-year refundable credit',
        'rows': [
          {
            'label': 'Total eligible expenditures for R&D in the current tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '103',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '103'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'type': 'table',
            'num': '110'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Add: ',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit allocated to the corporation that is a member of a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'Credit allocated to the corporation that is a beneficiary under a trust',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '140',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '140'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '150'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '155'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Total current-year refundable credit</b> (Amount B <b>plus</b> amount C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter amount D on line 520 in Part 2 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>.'
          }
        ]
      }
    ]
  });
})();
