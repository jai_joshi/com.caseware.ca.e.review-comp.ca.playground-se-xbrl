(function() {

  wpw.tax.create.tables('t2s301', {
    110: {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'small-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      cells: [
        {
          0: {
            label: 'Current-year credit earned (Amount A <b>multiplied</b> by'
          },
          1: {
            num: '200'
          },
          2: {
            label: '%) ='
          },
          3: {
            num: '120', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          4: {
            label: 'B'
          }
        }
      ]
    }
  })
})();
