(function() {

  wpw.tax.create.calcBlocks('t2s301', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field) {
      field('200').assign(field('ratesNl.200').get());
      field('200').source(field('ratesNl.200'));
      //(t2s32 is not available for now) field('103').assign(field('t2s32.400').get());
      field('120').assign(field('103').get() * field('200').get() / 100);
      calcUtils.sumBucketValues('150', ['130', '140']);
      field('155').assign(field('150').get());
      calcUtils.sumBucketValues('160', ['120', '155']);
    });

  });
})();
