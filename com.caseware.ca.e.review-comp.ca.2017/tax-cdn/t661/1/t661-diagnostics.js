(function() {

  wpw.tax.create.diagnostics('t661', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;
    var part8Tables = ['1016', '1017'];

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T661'), forEach.row('1001', forEach.bnCheckCol(2, true))));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T661'), forEach.row('1019', forEach.bnCheckCol(1, true))));

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T661'), common.bnCheck('016')));

    diagUtils.diagnostic('0320001', common.prereq(
        common.check(['t2s1.231', 't2s1.411', 't2s31.350', 't2s31.360', 't2s31.370', 't2s31.420',
          't2s31.430', 't2s31.440', 't2s31.450', 't2s31.460', 't2s31.480', 't2s31.490'], 'isNonZero'),
        common.requireFiled('T661')));

    diagUtils.diagnostic('0320003', common.prereq(common.and(
        common.requireFiled('T661'),
        common.check('t2s1.411', 'isNonZero')),
        common.check(['300', '305', '310', '320', '325', '340', '345', '350', '355', '360', '370',
          '390', '445', '450', '452', '453'], 'isNonZero')));

    diagUtils.diagnostic('0320005', common.prereq(common.and(
        common.requireFiled('T661'),
        common.check(['t2s31.350', 't2s31.420', 't2s31.430'], 'isNonZero')),
        common.check(['300', '305', '307', '309', '310', '320', '325', '340', '345', '350', '355',
          '360', '370', '500', '502', '508'], 'isNonZero')));

    diagUtils.diagnostic('0320006', common.prereq(common.and(
        common.requireFiled('T661'),
        common.check(['t2s31.360', 't2s31.440', 't2s31.450'], 'isNonZero')),
        common.check(['390', '504', '510'], 'isNonZero')));

    diagUtils.diagnostic('0320007', common.prereq(common.and(
        common.requireFiled('T661'),
        common.check(['t2s31.370', 't2s31.460', 't2s31.480', 't2s31.490'], 'isNonZero')),
        common.check('560', 'isNonZero')));

    diagUtils.diagnostic('0320023', common.prereq(common.requireFiled('T661'),
        function(tools) {
          var table = tools.field('1012');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[6].isNonZero())
              return tools.requireAll([row[0], row[1], row[2], row[3], row[5]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0320027', common.prereq(common.and(
        common.requireFiled('T661'),
        common.check('151', 'isNo')), function(tools) {
      var table = tools.field('1001');
      return tools.checkAll(table.getRows(), function(row) {
        return tools.requireAll([row[0], row[1], row[2]], 'isFilled');
      });
    }));

    diagUtils.diagnostic('0320028', common.prereq(common.requireFiled('T661s'),
        function(tools) {
          return tools.checkMethod(tools.list(['160', '162']), 'isChecked') == 1;
        }));

    diagUtils.diagnostic('0320029', common.prereq(common.and(
        common.requireFiled('T661'),
        common.check('429', 'isNonZero')),
        common.check(['513', '514'], 'isNonZero')));

    diagUtils.diagnostic('0320030', common.prereq(common.requireFiled('T661s'),
        function(tools) {
          var table = tools.field('1001');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[0], row[1], row[2]], 'isNonZero'))
              return tools.field('151').require('isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0320032', common.prereq(common.requireFiled('T661'), common.check(['100', '105', '115', '120', '165'], 'isFilled')));

    diagUtils.diagnostic('0320033', common.prereq(common.and(
        common.requireFiled('T661'),
        common.check(['300', '305', '307', '309', '320', '325', '340', '345', '350', '360', '390'], 'isNonZero')),
        common.requireFiled('T661s')));

    diagUtils.diagnostic('0320034', common.prereq(common.and(
        common.requireFiled('T661'),
        common.check('t2s1.231', 'isNonZero')),
        common.check(['429', '431', '432', '435', '440'], 'isNonZero')));

    diagUtils.diagnostic('0320040', common.prereq(common.requireFiled('T661'),
        function(tools) {
          var table = tools.field('1013');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[0], row[1], row[2], row[3]], 'isNonZero')) {
              if (row[0].isFilled()) {
                return tools.requireOne([row[1], row[2], row[3]], 'isNonZero');
              }
              if (tools.checkMethod([row[1], row[2], row[3]], 'isNonZero')) {
                tools.diagnostic.diagnosticObj.label = 'There is an entry at one or more of lines 032752, 032754, or ' +
                    '032756, but there is not, for each row, a corresponding entry at line 032750.';
                return row[0].require('isFilled');
              }
              return true;
            } else return true;
          });
        }));

    diagUtils.diagnostic('0320045', common.prereq(common.requireFiled('T661'), common.check('935', 'isFilled')));

    diagUtils.diagnostic('0320046', common.prereq(common.and(
        common.requireFiled('T661'),
        common.check('935', 'isYes')),
        function(tools) {
          var table = tools.field('1019');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireAll([row[0], row[1], row[2], row[5]], 'isNonZero');
          });
        }));

    diagUtils.diagnostic('0320047', common.prereq(common.requireFiled('T661'),
        function(tools) {
          var table = tools.field('1019');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[0], row[1], row[2], row[5]], 'isNonZero'))
              return tools.requireAll([row[0], row[1], row[2], row[5]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0320049', common.prereq(common.requireFiled('T661'),
        function(tools) {
          var table = tools.field('1019');
          return tools.checkAll(table.getRows(), function(row) {
            if (['1', '2', '3', '4'].indexOf(row[2].get()) > -1)
              return row[3].require('isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0320100', common.prereq(common.requireFiled('T661'),
        function(tools) {
          var table = tools.field('1019');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[2].get() == '5')
              return row[4].require('isFilled');
            else return true;
          });
        }));

    diagUtils.diagnostic('0320103', common.prereq(common.requireFiled('T661'), common.check('970', 'isFilled')));

    diagUtils.diagnostic('661.355', common.prereq(common.requireFiled('T661'), common.check('355', 'isZero')));

    diagUtils.diagnostic('661.370', common.prereq(common.requireFiled('T661'), common.check('370', 'isZero')));

    diagUtils.diagnostic('661.percentCheck', common.prereq(common.and(
        common.requireFiled('T661'),
        function(tools) {
          return tools.field('1012').getRow(0)[2].isNonZero();
        }), function(tools) {
      var table = tools.field('1012');
      return tools.checkAll(table.getRows(), function(row) {
        return row[2].require(function() {
          return this.get() > 0 && this.get() <= 75;
        })
      });
    }));

    var checkboxTable = function(tableNum) {
      diagUtils.diagnostic('661.part8Check', common.prereq(
          common.requireFiled('T661'),
          function(tools) {
            var table = tools.field(tableNum);
            return tools.checkAll(table.getRows(), function(row) {
              return row[3].require('isChecked');
            });
          }));
    };

    for (var table in part8Tables) {
      checkboxTable(part8Tables[table]);
    }

  });
})();
