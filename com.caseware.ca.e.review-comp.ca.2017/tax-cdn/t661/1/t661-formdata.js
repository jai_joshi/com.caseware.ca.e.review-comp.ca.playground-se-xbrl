(function() {

  wpw.tax.create.formData('t661', {
    formInfo: {
      title: 'Scientific Research and Experimental Development Expenditures Claim',
      schedule: 'Schedule 32',
      abbreviation: 't661',
      code: 'Code 1501',
      hideHeaderBox: true,
      showCorpInfo: true,
      disclaimerShowWhen: {fieldId: 'cp.181', check: 'isYes'},
      disclaimerTextField: {formId: 'cp', fieldId: '183'},
      description: [
        {text: '<b>Use this form:</b>'},
        {
          type: 'list',
          items: [
            'to provide technical information on your SR&ED projects;',
            'to calculate your SR&ED expenditures; and',
            'to calculate your qualified SR&ED expenditures for investment tax credits (ITC).'
          ]
        },
        {text: '<b>To claim an ITC, use either:</b> '},
        {
          type: 'list',
          items: [
            'Schedule T2SCH31, Investment Tax Credit – Corporations , or ',
            'Form T2038(IND), Investment Tax Credit (Individuals).'
          ]
        },
        {
          text: 'The information requested in this form and documents supporting your expenditures and ' +
          'project information (Part 2) are prescribed information. '
        },
        {
          text: 'Your SR&ED claim must be filed within 12 months of the filing due date of your income tax return.'
        },
        {
          text: 'To help you fill out this form, use the T4088, Guide to Form T661, which is available on ' +
          'our Web site: ' + 'www.cra.gc.ca/sred'.link('http://www.cra-arc.gc.ca/sred/')
        }
      ],
      descriptionHighlighted: true,
      highlightFieldsets: true,
      formFooterNum: 'T661 E (15)',
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        header: 'Part 1 – General information',
        rows: [
          {
            type: 'splitTable', fieldAlignRight: true,
            side1: [
              {
                'label': 'Name of claimant'
              },
              {
                'type': 'infoField',
                'inputType': 'textArea',
                'tn': '010',
                'num': '010',
                'maxLength': 175
              },
              {
                'labelClass': 'fullLength'
              },
              {
                'type': 'table',
                'num': '011'
              },
              {
                'labelClass': 'fullLength'
              },
              {
                'label': 'Total number of projects you are claiming this tax year',
                'layout': 'alignInput',
                'layoutOptions': {
                  'indicatorColumn': false,
                  'showDots': true,
                  'rightPadding': false
                },
                'columns': [
                  {
                    'input': {
                      'num': '050'
                    },
                    'padding': {
                      'type': 'tn',
                      'data': '050'
                    }
                  }
                ]
              }
            ],
            side2: [
              {
                label: 'Enter one of the following',
                labelClass: 'fullLength'
              },
              {labelClass: 'fullLength'},
              {labelClass: 'fullLength'},
              {
                type: 'infoField',
                inputType: 'custom',
                format: ['{N9}RC{N4}', 'NR'],
                label: 'Business number (BN)',
                num: '016'
              },
              {labelClass: 'fullLength'},
              {labelClass: 'fullLength'},
              {
                type: 'infoField',
                inputType: 'text',
                label: 'Social insurance number (SIN)',
                num: '017'
              }
            ]
          },
          {type: 'horizontalLine'},
          {type: 'table', num: '101'},
          {type: 'horizontalLine'},
          {
            label: 'If this claim is filed for a partnership, was Form T5013 filed?',
            type: 'infoField',
            inputType: 'radio',
            labelWidth: '80%',
            tnPosition: 'middle',
            num: '151',
            tn: '151'
          },
          {type: 'horizontalLine'},
          {label: 'If you answered <b>no</b> to line 151, complete lines 153, 156 and 157.', labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          {type: 'table', num: '1001'}
        ]
      },
      {
        hiddenPrint: true,
        header: 'Part 2 – Project information',
        rows: [
          {
            type: 'infoField',
            inputType: 'link',
            note: 'Please Click here to go to T661 - Part 2',
            formId: 't661s'
          }
        ]
      },
      {
        header: 'Part 3 – Calculation of SR&ED expenditures <br> What did you spend on your SR&ED projects?',
        rows: [
          {
            'label': 'Section A - Select the method to calculate the SR&ED expenditures',
            'labelClass': 'bold fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'I elect (choose) to use the following method to calculate my SR&ED expenditures and related investment tax credits (ITC) for this tax year. <br> I understand that my election is irrevocable (cannot be changed) for this tax year.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1009'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'label': 'Section B - Calculation of allowable SR&ED expenditures (to the nearest dollar)',
            'labelClass': 'bold fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '- SR&ED portion of salary or wages of employees directly engaged in the SR&ED:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'a) Employees other than specified employees for work performed in Canada',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '300'
                },
                'padding': {
                  'type': 'tn',
                  'data': '300'
                }
              }
            ]
          },
          {
            'label': 'b) Specified employees for work performed in Canada',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '305'
                },
                'padding': {
                  'type': 'tn',
                  'data': '305'
                }
              }
            ]
          },
          {
            'label': '<b>Subtotal</b> (add lines 300 and 305)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '306'
                },
                'padding': {
                  'type': 'tn',
                  'data': '306'
                }
              }
            ]
          },
          {
            'label': 'c) Employees other than specified employees for work performed outside Canada (subject to limitations - see guide)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '307'
                },
                'padding': {
                  'type': 'tn',
                  'data': '307'
                }
              }
            ]
          },
          {
            'label': 'd) Specified employees for work performed outside Canada (subject to limitations - see guide)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '309'
                },
                'padding': {
                  'type': 'tn',
                  'data': '309'
                }
              }
            ]
          },
          {
            'label': '- Salary or wages identified on line 315 in prior years that were paid in this tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '310'
                },
                'padding': {
                  'type': 'tn',
                  'data': '310'
                }
              }
            ]
          },
          {
            'label': '- Salary or wages incurred in the year but not paid within 180 days of the tax year-end',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '315'
                },
                'padding': {
                  'type': 'tn',
                  'data': '315'
                }
              },
              null
            ]
          },
          {
            'label': '- Cost of materials consumed in performing SR&ED',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '320'
                },
                'padding': {
                  'type': 'tn',
                  'data': '320'
                }
              }
            ]
          },
          {
            'label': '- Cost of materials transformed in performing SR&ED',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '325'
                },
                'padding': {
                  'type': 'tn',
                  'data': '325'
                }
              }
            ]
          },
          {
            'label': '- Contract expenditures for SR&ED performed on your behalf:'
          },
          {
            'label': 'a) Arm\'s length contracts (see note 1)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '340'
                },
                'padding': {
                  'type': 'tn',
                  'data': '340'
                }
              }
            ]
          },
          {
            'label': 'b) Non-arm\'s length contracts (see note 1)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '345'
                },
                'padding': {
                  'type': 'tn',
                  'data': '345'
                }
              }
            ]
          },
          {
            'label': '- Lease costs of equipment used before 2014:'
          },
          {
            'label': 'a) All or substantially all (90% of the time or more) for SR&ED',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '350'
                },
                'padding': {
                  'type': 'tn',
                  'data': '350'
                }
              }
            ]
          },
          {
            'label': 'b) Primarily (more than 50% of the time but less than 90%) for SR&ED. (Enter 50% of lease costs if you use the proxy method or enter "0" if you use the traditional method)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '355'
                },
                'padding': {
                  'type': 'tn',
                  'data': '355'
                }
              }
            ]
          },
          {
            'label': '- Overhead and other expenditures (enter "0" if you use the proxy method)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '360'
                },
                'padding': {
                  'type': 'tn',
                  'data': '360'
                }
              }
            ]
          },
          {
            'label': '- Third-party payments (see note 2) (complete Form T1263*)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '370'
                },
                'padding': {
                  'type': 'tn',
                  'data': '370'
                }
              }
            ]
          },
          {
            'label': 'Total current SR&ED expenditures (add lines 306 to 370; do not add line 315<br>(Corporations may need to adjust line 118 of schedule T2SCH1)',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '380'
                },
                'padding': {
                  'type': 'tn',
                  'data': '380'
                }
              }
            ]
          },
          {
            'label': '- Capital expenditures for depreciable property available for use before 2014<br>(Do not include these capital expenditures on schedule T2SCH8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '390'
                },
                'padding': {
                  'type': 'tn',
                  'data': '390'
                }
              }
            ]
          },
          {
            'label': 'Total allowable SR&ED expenditures (add lines 380 and 390)',
            forceBreakAfter: true,
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '400'
                },
                'padding': {
                  'type': 'tn',
                  'data': '400'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'label': 'Section C - Calculation of pool of deductible SR&ED expenditures (to the nearest dollar)',
            'labelClass': 'bold'
          },
          {
            'label': 'Amount from line 400',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '420'
                },
                'padding': {
                  'type': 'tn',
                  'data': '420'
                }
              }
            ]
          },
          {
            'label': 'Deduct',
            'labelClass': 'bold'
          },
          {
            'label': '- provincial government assistance for expenditures included on line 400',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '429'
                },
                'padding': {
                  'type': 'tn',
                  'data': '429'
                }
              },
              null
            ]
          },
          {
            'label': '- other government assistance for expenditures included on line 400',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '431'
                },
                'padding': {
                  'type': 'tn',
                  'data': '431'
                }
              },
              null
            ]
          },
          {
            'label': '- non-government assistance for expenditures included on line 400',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '432'
                },
                'padding': {
                  'type': 'tn',
                  'data': '432'
                }
              },
              null
            ]
          },
          {
            'label': '- SR&ED ITCs applied and/or refunded in the prior year (see guide)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '435'
                },
                'padding': {
                  'type': 'tn',
                  'data': '435'
                }
              },
              null
            ]
          },
          {
            'label': '- sale of SR&ED capital assets and other deductions',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '440'
                },
                'padding': {
                  'type': 'tn',
                  'data': '440'
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1041'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1042'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '<b>Subtotal</b> (line 420 minus lines 429 to 440)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '442'
                },
                'padding': {
                  'type': 'tn',
                  'data': '442'
                }
              }
            ]
          },
          {
            'label': 'Add',
            'labelClass': 'bold'
          },
          {
            'label': '- repayments of government and non-government assistance that previously reduced the SR&ED expenditure pool',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '445'
                },
                'padding': {
                  'type': 'tn',
                  'data': '445'
                }
              },
              null
            ]
          },
          {
            'label': '- prior year\'s pool balance of deductible SR&ED expenditures (from line 470 of prior year T661)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '450'
                },
                'padding': {
                  'type': 'tn',
                  'data': '450'
                }
              },
              null
            ]
          },
          {
            'label': '- SR&ED expenditure pool transfer from amalgamation or wind-up',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '452'
                },
                'padding': {
                  'type': 'tn',
                  'data': '452'
                }
              },
              null
            ]
          },
          {
            'label': '- amount of SR&ED ITC recaptured in the prior year',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '453'
                },
                'padding': {
                  'type': 'tn',
                  'data': '453'
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1043'
                }
              },
              {
                'input': {
                  'num': '1044'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (If the amount is negative, carry over to line 231 of Schedule 1. If the amount is positive,carry over to line 455 below)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1045'
                }
              }
            ]
          },
          {
            'label': '<b>Amount available for deduction (add lines 442 to 453)</b> <br>(enter positive amount only, include negative amount in income)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '455'
                },
                'padding': {
                  'type': 'tn',
                  'data': '455'
                }
              }
            ]
          },
          {
            'label': '- Deduction claimed in the year<br>(Corporations should enter this amount on line 411 of schedule T2SCH1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '460'
                },
                'padding': {
                  'type': 'tn',
                  'data': '460'
                }
              }
            ]
          },
          {
            'label': 'Pool balance of deductible SR&ED expenditures to be carried forward to future years (line 455 minus 460) .',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '470'
                },
                'padding': {
                  'type': 'tn',
                  'data': '470'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Form T1263, Third-Party Payments for Scientific Research and Experimental Development (SR&ED)',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 1 - For contract expenditures made after 2013, no amounts for purchasing or leasing capital property can be included. ',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 2 - For third-party payments made after 2013, no amounts for purchasing or leasing capital property can be included',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        header: 'Part 4 – Calculation of qualified SR&ED expenditures for investment tax credit (ITC) purposes',
        rows: [
          {
            label: 'The resulting amount is used to calculate your refundable and/or non refundable ITC.',
            labelClass: 'fullLength bold'
          },
          {
            label: 'Enter the breakdown between current and capital expenditures (to the nearest dollar),',
            labelClass: 'fullLength bold'
          },
          {type: 'table', num: '1011'},
          {
            label: '* Form T1145, Agreement to Allocate Assistance for SR&ED Between Persons ' +
            'Not Dealing at Arm\'s Length',
            labelClass: 'fullLength'
          },
          {
            label: '** Form T1146, Agreement to Transfer Qualified Expenditures Incurred ' +
            'in Respect of SR&ED Contracts Between Persons Not Dealing at Arm\'s Length',
            labelClass: 'fullLength'
          },
          {
            label: 'Note 3 – On line 510 (capital) – Only include expenditures made ' +
            'before 2014 by the transferor (performer). Complete the latest version of Form T1146. ',
            labelClass: 'fullLength'
          },
          {
            label: 'Note 4 – On lines 514, 516, 518, 532, 535, 540, 543 and 546 –' +
            ' Only include amounts related to expenditures of a capital nature made before 2014. ',
            labelClass: 'fullLength'
          },
          {
            label: 'Note 5 – For arm’s length contracts, only include 80% of the contract amount.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'Part 5 – Calculation of prescribed proxy amount (PPA)',
        rows: [
          {
            'label': 'A notional amount representing your overhead and other expenditures.',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'This part calculates the PPA to enter on line 502 in Part 4. Do not complete this part if you have chosen to use the traditional method in Part 3 (line 162). You can only claim a PPA if you elected to use the proxy method for the year in Part 3 (line 160). ',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Special rules apply for specified employees. Calculate your salary base in Section A and the PPA in Section B.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'label': 'Section A - Salary base',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Salary or wages of employees other than specified employees (from lines 300 and 307)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '810'
                },
                'padding': {
                  'type': 'tn',
                  'data': '810'
                }
              }
            ]
          },
          {
            'label': 'Deduct',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Bonuses, remuneration based on profits, and taxable benefits that were included on line 810',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '812'
                },
                'padding': {
                  'type': 'tn',
                  'data': '812'
                }
              }
            ]
          },
          {
            'label': '<b>Subtotal</b> (line 810 minus 812)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '814'
                },
                'padding': {
                  'type': 'tn',
                  'data': '814'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Salary or wages of specified employees',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '1012'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '816'
                },
                'padding': {
                  'type': 'tn',
                  'data': '816'
                }
              }
            ]
          },
          {
            'label': '*A = Maximum Canada Pension Plan pensionable earnings',
            'labelClass': 'text-right bold'
          },
          {
            'label': 'Taxation year',
            'labelClass': 'text-right bold'
          },
          {
            'label': '2012',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1373'
                }
              },
              null
            ]
          },
          {
            'label': '2013',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1374'
                }
              },
              null
            ]
          },
          {
            'label': '2014',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1375'
                }
              },
              null
            ]
          },
          {
            'label': '2015',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1376'
                }
              },
              null
            ]
          },
          {
            'label': '2016',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1377'
                }
              },
              null
            ]
          },
          {
            'label': '2017',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1378'
                }
              },
              null
            ]
          },
          {
            'label': '2018',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1379'
                }
              },
              null
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Salary base</b>(total of lines 814 and 816)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '818'
                },
                'padding': {
                  'type': 'tn',
                  'data': '818'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'label': 'Section B - Prescribed proxy amount (PPA)',
            'labelClass': 'fullLength bold'
          },
          {
            labelClass: 'fullLength'
          },
          {
            label: 'Enter 65% of the salary base (line 818) less 5% of the salary base for the number of 2013 calendar' +
            ' days in the tax year, and less 10% of the salary base for number of days after 2013 in the tax year' +
            '(use the formula in the guide-line 820) ',
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'type': 'table',
            'num': '1051'
          },
          {
            'label': 'PPA (a less b less c)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '820'
                },
                'padding': {
                  'type': 'tn',
                  'data': '820'
                }
              }
            ]
          },
          {
            'label': 'Enter the amount from line 820 on line 502 in Part 4 unless the overall cap on PPA applies to you. (See the guide for explanation and example of the overall cap on PPA)',
            'labelClass': 'fullLength bold'
          }
        ]
      },
      {
        forceBreakAfter: true,
        header: 'Part 6 – Project costs',
        rows: [
          {
            label: 'Information requested in this part must be provided for <b>all</b>' +
            ' SR&ED projects claimed in the year.',
            labelClass: 'fullLength'
          },
          {
            label: 'Expenditures should be recorded and allocated on a project basis.',
            labelClass: 'fullLength'
          },
          {type: 'table', num: '1013'}
        ]
      },
      {
        header: 'Part 7 – Additional information',
        rows: [
          {
            'label': '<b>Expenditures for SR&ED performed by you in Canada</b>  (line 400 minus lines 307, 309, 340, 345, and 370)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '605'
                },
                'padding': {
                  'type': 'tn',
                  'data': '605'
                }
              }
            ]
          },
          {
            'label': 'From the total you entered on line 605, estimate the percentage of distribution of the sources of funds for SR&ED performed within your organization'
          },
          {
            'type': 'table',
            'num': '1014'
          },
          {
            'label': 'For statistical purposes indicate whether the work you performed falls within the realm of Basic or Applied research (to advance scientific knowledge) or Experimental development (to achieve a technological advancement):',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1015'
          },
          {
            'label': 'Enter the number of SR&ED personnel in full-time equivalents (FTE):',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Scientists and engineers',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  "validate": {"or":[{"matches":"^[.\\d]{1,6}$"},{"check":"isEmpty"}]},
                  'num': '632'
                },
                'padding': {
                  'type': 'tn',
                  'data': '632'
                }
              }
            ]
          },
          {
            'label': 'Technologists and technicians',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  "validate": {"or":[{"matches":"^[.\\d]{1,6}$"},{"check":"isEmpty"}]},
                  'num': '634'
                },
                'padding': {
                  'type': 'tn',
                  'data': '634'
                }
              }
            ]
          },
          {
            'label': 'Managers and administrators',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  "validate": {"or":[{"matches":"^[.\\d]{1,6}$"},{"check":"isEmpty"}]},
                  'num': '636'
                },
                'padding': {
                  'type': 'tn',
                  'data': '636'
                }
              }
            ]
          },
          {
            'label': 'Other technical supporting staff',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  "validate": {"or":[{"matches":"^[.\\d]{1,6}$"},{"check":"isEmpty"}]},
                  'num': '638'
                },
                'padding': {
                  'type': 'tn',
                  'data': '638'
                }
              }
            ]
          }
        ]
      },
      {
        header: 'Part 8 – Claim checklist',
        rows: [
          {
            label: 'To ensure your claim is complete, make sure you have:',
            labelClass: 'fullLength'
          },
          {type: 'table', num: '1016'},
          {
            label: 'To expedite the processing of your claim, make sure you have:',
            labelClass: 'fullLength'
          },
          {type: 'table', num: '1017'},
          {labelClass: 'fullLength'},
          {
            label: '* Form T1145, Agreement to Allocate Assistance for SR&ED Between Persons Not Dealing ' +
            'at Arm\'s Length',
            labelClass: 'fullLength'
          },
          {
            label: '** Form T1146, Agreement to Transfer Qualified Expenditures Incurred ' +
            'in Respect of SR&ED Contracts Between Persons Not Dealing at Arm\'s Length',
            labelClass: 'fullLength'
          },
          {
            label: '*** Form T1174, Agreement Between Associated Corporations to Allocate .' +
            'Salary or Wages of Specified Employees for Scientific Research and Experimental Development (SR&ED)',
            labelClass: 'fullLength'
          },
          {
            label: '**** Form T1263, Third-Party Payments for Scientific Research and Experimental Development (SR&ED)',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'Part 9 – Claim preparer information',
        rows: [
          {
            label: 'Information requested in this part must be provided for each claim preparer ' +
            'that has accepted consideration to prepare or assist in the preparation of this SR&ED claim. ' +
            'Certification is required on lines 935, 970, and 975.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: 'A $1000 penalty may be assessed if the information requested below about the ' +
            'claim preparer(s) and billing arrangement(s), is missing, incomplete, or inaccurate. ' +
            'Where a claim preparer has prepared or assisted in the preparation of this SR&ED form, ' +
            'the claimant and the claim preparer will be jointly and severally, or solidarily, ' +
            'liable for the penalty.',
            labelClass: 'fullLength bold'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            label: ' Was a claim preparer engaged in any aspect of the preparation of this SR&ED claim? ',
            labelClass: 'bold',
            num: '935',
            tn: '935',
            inputType: 'dropdown',
            options: [
              {value: '0', option: ' '},
              {value: '1', option: 'Yes (complete the claim preparer information table and lines 970 and 975 below)'},
              {value: '2', option: 'No (complete lines 970 and 975)'}
            ]
          },
          {
            label: 'Claim preparer information table ',
            labelClass: 'fullLength bold'
          },
          {type: 'table', num: '1019'},
          {
            label: 'If the answer at line 950 is (1), there must be a percentage in this line: <br>' +
            '• Format = Percentage including fractional percentages Minimum = 0.001% Maximum = 100%<br>' +
            'If the answer at line 950 is (2, 3, or 4), there must be an amount at this line, this amount ' +
            'can be with cents.<br> ' +
            '• Format = amount including fractional cents<br>' +
            'A hourly/daily/flat rate of $65.75 = 65.75 <br>' +
            'A hourly/daily/flat rate of $325 = 325',
            labelClass: 'fullLength'
          },
          {
            label: '* Billing arrangement codes',
            labelClass: 'fullLength'
          },
          {
            type: 'table', num: '999'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'I certify that the information provided in this part is complete and accurate.',
            'labelClass': 'fullLength'
          },
          {
            type: 'table', num: '1022'
          },
          {labelClass: 'fullLength'}
        ]
      },
      {
        header: 'Part 10 – Certification',
        rows: [
          {
            'label': 'I certify that I have examined the information provided on this form and on the ' +
            'attachments and it is true, correct, and complete.',
            'labelClass': 'fullLength'
          },
          {
            type: 'table', num: '1021'
          }
        ]
      },
      {
        'hideFieldset': true,
        rows: [
          {
            'label': 'Privacy Notice ',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Personal information is collected pursuant to subsections 37(1), 37(11), and ' +
            '162(5.1) of the <i>Income Tax Act</i> (the Act) and is used for verification of compliance, administration ' +
            'and enforcement of the Scientific Research and Experimental Development (SR&ED) program requirements. ',
            'labelClass': 'fullLength'
          },
          {
            label: 'Information may also be used for the administration and enforcement of other ' +
            'provisions of the Act, including assessment, audit, enforcement, collections, and appeals, and ' +
            'may be disclosed under information-sharing agreements in accordance with the Act. Incomplete ' +
            'or inaccurate information may result in assessment of monetary penalties and delays in processing ' +
            'SR&ED claims. ',
            labelClass: 'fullLength'
          },
          {
            label: 'The social insurance number is collected pursuant to section 237 of the' +
            ' Act and is used for identification purposes. ',
            labelClass: 'fullLength'
          },
          {
            label: 'Information is described in personal information bank CRA PPU 441 “Scientific Research ' +
            'and Experimental Development” in the Canada Revenue Agency (CRA) chapter of Info Source. ' +
            'Personal information is protected under the <i>Privacy Act</i>, and individuals have a right of ' +
            'access to, correction, and protection of their personal information. Further details regarding ' +
            'requests for personal information at the CRA and our Info Source chapter ' +
            'can be found at www.cra.gc.ca/atip.',
            labelClass: 'fullLength'
          }
        ]
      }

    ]
  });
})();
