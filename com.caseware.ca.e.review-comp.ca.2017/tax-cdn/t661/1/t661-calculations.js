(function() {
  wpw.tax.create.calcBlocks('t661', function(calcUtils) {

    //Part 1
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('010', 'cp', '002');
      calcUtils.getGlobalValue('012', 'cp', 'tax_start');
      calcUtils.getGlobalValue('013', 'cp', 'tax_end');
      calcUtils.getGlobalValue('016', 'cp', 'bn');
      field('110').assign(form('CP').field('619').get());
      if(field('CP.957').get()=='1'){
        field('110').source(field('CP.639'));
        field('100').assign(field('CP.951').get() + ' ' + field('CP.950').get());
        field('100').source(field('CP.950'));
        field('105').assign(field('CP.956').get());
        field('105').source(field('CP.956'));
      }else{
        field('100').assign(field('CP.958').get());
        if(field('CP.958').get()){
          field('100').assign(field('CP.958').get().replace(/(\w+)\s(\w+)/, '$2 $1'));
        }
        field('100').source(field('CP.958'));
        field('110').source(field('CP.619'));
        field('105').assign(field('CP.959').get());
        field('105').source(field('CP.959'));
      }

      field('1001').getRows().forEach(function(row) {
        if (field('151').get() == 1) {
          row[0].assign();
          row[1].assign();
          row[2].assign();
          row[0].disabled(true);
          row[1].disabled(true);
          row[2].disabled(true);
        }
        else {
          row[0].disabled(false);
          row[1].disabled(false);
          row[2].disabled(false);
        }
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //value from rate table for part 5
      calcUtils.getGlobalValue('1370', 'ratesFed', '370');
      calcUtils.getGlobalValue('1371', 'ratesFed', '371');
      calcUtils.getGlobalValue('1372', 'ratesFed', '372');
      calcUtils.getGlobalValue('1373', 'ratesFed', '373');
      calcUtils.getGlobalValue('1374', 'ratesFed', '374');
      calcUtils.getGlobalValue('1375', 'ratesFed', '375');
      calcUtils.getGlobalValue('1376', 'ratesFed', '376');
      calcUtils.getGlobalValue('1377', 'ratesFed', '377');
      calcUtils.getGlobalValue('1378', 'ratesFed', '378');
      calcUtils.getGlobalValue('1379', 'ratesFed', '379');
      //part 3
      //section A
      var isProxyMethodUsed = (field('160').get() == true);
      var isTraditionalMethodUsed = (field('162').get() == true);

      //section B
      field('306').assign(field('300').get() + field('305').get());

      if (isProxyMethodUsed) {
        field('162').disabled(true);
        field('355').disabled(false);
        field('360').assign(0);
        field('360').disabled(true);
        calcUtils.setRepeatSummaryValue('370', 't2s1263', '704', 'add');

        //part 5 - Calculation of prescribed proxy amount (PPA)
        //part A
        field('810').assign(
            field('300').get() +
            field('307').get()
        );
        field('814').assign(
            field('810').get() -
            field('812').get()
        );
        field('1012').getRows().forEach(function(row, rIndex) {
          row[3].assign(row[1].get() * row[2].get() / 100);
          var taxYear = field('CP.tax_end').getKey('year');
          var amountA;
          switch (taxYear) {
            case 2012:
              amountA = field('1373').get();
              break;
            case 2013:
              amountA = field('1374').get();
              break;
            case 2014:
              amountA = field('1375').get();
              break;
            case 2015:
              amountA = field('1376').get();
              break;
            case 2016:
              amountA = field('1377').get();
              break;
            case 2017:
              amountA = field('1378').get();
              break;
              //TODO: Update 2018 max CPP. Using the same amount as 2017 since it hasn't been publicized yet
            case 2018:
              amountA = field('1379').get();
              break;
            default:
              amountA = 0;
          }
          row[5].assign(2.5 * amountA * row[4].get() / 365);
          row[6].assign(Math.min(row[3].get(), row[5].get()))
        });
        field('816').assign(field('1012').total(6).get());
        field('818').assign(field('814').get() + field('816').get());

        //section B
        var salarybaseAmount = field('818').get();
        field('1052').assign(salarybaseAmount);
        field('1055').assign(salarybaseAmount);
        field('1060').assign(salarybaseAmount);
        field('1057').assign(0);//our application is handling 2015 forward so assign number of days to 0
        calcUtils.getGlobalValue('1059', 'CP', 'Days_Fiscal_Period');
        calcUtils.getGlobalValue('1064', 'CP', 'Days_Fiscal_Period');
        //CompTax handling 2015 forward; therefore, it'll always be same as # of days in a year
        field('1062').assign(field('1064').get());
        field('1054').assign(field('1052').get() * field('1370').get() / 100);
        field('1058').assign(field('1055').get() * field('1371').get() / 100 * field('1057').get() / field('1059').get());
        field('1063').assign(field('1060').get() * field('1372').get() / 100 * field('1062').get() / field('1064').get());
        field('820').assign(field('1054').get() - field('1058').get() - field('1063').get());
        field('502').assign(field('820').get());
      }
      else {
        if (calcUtils.hasChanged(field('160'))) {
          field('162').disabled(false)
        }
      }

      if (isTraditionalMethodUsed) {
        field('160').disabled(true);
        field('355').disabled(true);
        field('355').assign(0);
        field('502').disabled(true);
        field('502').assign(0);
        calcUtils.removeValue([810, 814, 818], true);
      }
      else {
        if (calcUtils.hasChanged(field('162'))) {
          field('160').disabled(false)
        }
      }

      field('380').assign(
          field('306').get() +
          field('307').get() +
          field('309').get() +
          field('310').get() +
          field('320').get() +
          field('325').get() +
          field('340').get() +
          field('345').get() +
          field('350').get() +
          field('355').get() +
          field('360').get() +
          field('370').get()
      );
      field('400').assign(field('380').get() + field('390').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 3 - section C
      field('420').assign(field('400').get());
      field('1041').assign(
          field('429').get() +
          field('431').get() +
          field('432').get() +
          field('435').get() +
          field('440').get()
      );
      field('1042').assign(field('1041').get());
      field('442').assign(field('420').get() - field('1042').get());

      field('1043').assign(
          field('445').get() +
          field('450').get() +
          field('452').get() +
          field('453').get()
      );
      field('1044').assign(field('1043').get());
      field('1045').assign(field('442').get() + field('1044').get());
      field('455').assign(Math.max(0, field('1045').get()));
      //todo: calcs for 460- optimization calcs
      field('470').assign(field('455').get() - field('460').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 4
      field('492').assign(field('380').get());
      field('496').assign(field('390').get());
      field('511').assign(
          field('492').get() +
          field('500').get() +
          field('502').get() +
          field('508').get()
      );
      field('512').assign(
          field('496').get() +
          field('504').get() +
          field('510').get()
      );
      field('529').assign((field('340').get() + field('370').get()) * 20 / 100);
      field('557').assign(Math.max(0, field('511').get() - (
              field('513').get() +
              field('515').get() +
              field('517').get() +
              field('520').get() +
              field('528').get() +
              field('529').get() +
              field('530').get() +
              field('533').get() +
              field('538').get() +
              field('541').get() +
              field('542').get() +
              field('544').get()
          ))
      );
      field('558').assign(Math.max(0, field('512').get() - (
              field('514').get() +
              field('516').get() +
              field('518').get() +
              field('532').get() +
              field('535').get() +
              field('540').get() +
              field('543').get() +
              field('546').get()
          ))
      );
      field('559').assign(
          field('557').get() +
          field('558').get()
      );
      field('570').assign(
          field('559').get() +
          field('560').get()
      );
      //part5
      field('810').assign(field('300').get() + field('307').get());
      field('814').assign(field('810').get() - field('812').get());
      field('816').assign(field('1012').total(6).get());
      field('818').assign(field('814').get() + field('816').get());
    });
    calcUtils.calc(function(calcUtils, field, form) {
      //part 6
      calcUtils.filterLinkedTable('t661s', '1013', ['200'], function(x) {
        return (x != undefined)
      });
      field('050').assign(field('1013').size().rows);
      if(field('1013').value().cells[0][0].value==""){
          field('050').assign(0);
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 7
      field('605').assign(field('400').get() -
          field('307').get() -
          field('309').get() -
          field('340').get() -
          field('345').get() -
          field('370').get()
      );
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 9
      field('1019').getRows().forEach(function(row) {
        if (field('935').get() == 2) {
          row[0].assign();
          row[1].assign();
          row[2].assign();
          row[3].assign();
          row[4].assign();
          row[5].assign();
          row[0].disabled(true);
          row[1].disabled(true);
          row[2].disabled(true);
          row[3].disabled(true);
          row[4].disabled(true);
          row[5].disabled(true);
        }
        else {
          row[0].disabled(false);
          row[1].disabled(false);
          row[2].disabled(false);
          row[3].disabled(false);
          row[4].disabled(false);
          row[5].disabled(false);
        }
      });
      field('970').assign(form('T2J').field('951').get() + ' ' + form('T2J').field('950').get());
      field('970').source(form('T2J').field('950'));
      calcUtils.getGlobalValue('975', 'T2J', '955')
    });
    calcUtils.calc(function(calcUtils, field, form) {
      //part 10
      field('165').assign(form('T2J').field('951').get() + ' ' + form('T2J').field('950').get());
      field('165').source(form('T2J').field('950'));
      calcUtils.getGlobalValue('170', 'T2J', '955')

    });

  });
})();
