(function() {

  function getExpandableColumns(header1, header2) {
    return [
      {type: 'none'},//description
      {type: 'none', colClass: 'std-spacing-width'},//space
      {colClass: 'std-padding-width', type: 'none'},//tn
      {type: 'none', colClass: 'std-spacing-width'},//space
      {
        colClass: 'std-input-width',
        inputClass: 'noBorder',
        header: header1,
        "validate": (header1 == 'Canadian (%)') ? {
          "and": [
            'percent',
            {
              "or": [
                {"length": {"min": "1", "max": "6"}},
                {"check": "isEmpty"}
              ]
            }
          ]
        } : ''
      },//field
      {type: 'none', colClass: 'std-spacing-width'},//space
      {colClass: 'std-padding-width', type: 'none'},//tn
      {type: 'none', colClass: 'std-spacing-width'},//space
      {
        colClass: 'std-input-width',
        inputClass: 'noBorder',
        header: header2,
        "validate": (header1 == 'Canadian (%)') ? {
          "and": [
            'percent',
            {
              "or": [
                {"length": {"min": "1", "max": "6"}},
                {"check": "isEmpty"}
              ]
            }
          ]
        } : ''
      },//field
      {type: 'none', colClass: 'std-spacing-width'}//end
    ];
  }

  wpw.tax.create.tables('t661', {
    '011': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          header: 'Tax year-start date',
          type: 'date',
          cellClass: 'alignCenter'
        },
        {colClass: 'std-padding-width', type: 'none'},
        {
          header: 'Tax year-end date',
          type: 'date',
          cellClass: 'alignCenter'
        }
      ],
      cells: [
        {
          0: {num: '012'},
          2: {num: '013'}
        }
      ]
    },
    '101': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          width: '50%',
          cellClass: 'alignLeft'
        },
        {type: 'none', width: '2%'},
        {
          width: '23%',
          cellClass: 'alignLeft'
        },
        {type: 'none', width: '2%'},
        {
          width: '23%',
          cellClass: 'alignLeft'
        }
      ],
      cells: [
        {
          0: {
            label: 'Contact person for the financial information',
            tn: '100',
            num: '100',
            type: 'text'
          },
          2: {
            label: 'Telephone number/extension',
            tn: '105',
            num: '105',
            'telephoneNumber': true, type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
            validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
          },
          4: {
            label: 'Fax number',
            tn: '110',
            num: '110',
            'telephoneNumber': true, type: 'text'
          }
        },
        {
          0: {
            label: 'Contact person for the technical information',
            tn: '115',
            num: '115',
            type: 'text'
          },
          2: {
            label: 'Telephone number/extension',
            tn: '120',
            num: '120',
            'telephoneNumber': true,
            type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
            validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
          },
          4: {
            label: 'Fax number',
            tn: '125',
            num: '125',
            'telephoneNumber': true, type: 'text'
          }
        }
      ]
    },
    '1001': {
      maxLoop: 5,
      showNumbering: true,
      columns: [
        {
          header: 'Names of the partners',
          tn: '153',
          type: 'text'
        },
        {
          header: '%',
          tn: '156',
          "validate": {
            "and": [
              'percent',
              {
                "or": [
                  {"length": {"min": "1", "max": "6"}},
                  {"check": "isEmpty"}
                ]
              }
            ]
          },
          colClass: 'std-input-width',
          format: {suffix: '%'}
        },
        {
          header: 'BN or SIN ',
          tn: '157',
          width: '150px',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR', 'NA', '{N9}'],
          validate: {check: 'mod10'}
        }
      ]
    },
    '1009': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'singleCheckbox', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'none'}
      ],
      cells: [
        {
          0: {tn: '160'},
          2: {num: '160'},
          4: {
            label: 'I elect to use the proxy method <br>(Enter \'0\' on line 360 and complete Part 5.)'
          }
        },
        {
          0: {tn: '162'},
          2: {num: '162'},
          4: {
            label: 'I choose to use the traditional method <br> ' +
            '(Enter \'0\' on lines 355 and 502. Complete line 360.)'
          }
        }
      ]
    },
    '1011': {
      infoTable: true,
      fixedRows: true,
      columns: getExpandableColumns('Current Expenditures', 'Capital Expenditures'),
      cells: [
        {
          0: {label: '<b>Total expenditures for SR&ED</b> (from lines 380 and 390)'},
          2: {tn: '492'},
          4: {num: '492'},
          6: {tn: '496'},
          8: {num: '496'}
        },
        {
          0: {label: 'Add:', labelClass: 'fullLength bold'},
          4: {type: 'none'},
          8: {type: 'none'}
        },
        {
          0: {
            label: '• payment of prior years\' unpaid amounts (other than salary or wages) (see note 5)',
            labelClass: 'tabbed'
          },
          2: {tn: '500'},
          4: {num: '500'},
          8: {type: 'none'}
        },
        {
          0: {
            label: '• prescribed proxy amount (complete Part 5)<br>' +
            '(Enter \'0\' if you use the traditional method)',
            labelClass: 'tabbed'
          },
          2: {tn: '502'},
          4: {num: '502'},
          8: {type: 'none'}
        },
        {
          0: {
            label: '• expenditures on shared-use equipment for property acquired before 2014',
            labelClass: 'tabbed'
          },
          4: {type: 'none'},
          6: {tn: '504'},
          8: {num: '504'}
        },
        {
          0: {
            label: '• qualified expenditures transferred to you (see note 3) (complete Form T1146**)',
            labelClass: 'tabbed'
          },
          2: {tn: '508'},
          4: {num: '508'},
          6: {tn: '510'},
          8: {num: '510'}
        },
        {
          0: {
            label: '<b>Subtotal</b> (add lines 492 to 508, and add lines 496 to 510)'
          },
          2: {tn: '511'},
          4: {num: '511'},
          6: {tn: '512'},
          8: {num: '512'}
        },
        {
          0: {label: 'Deduct (see note 4)', labelClass: 'fullLength bold'},
          4: {type: 'none'},
          8: {type: 'none'}
        },
        {
          0: {
            label: '• provincial government assistance',
            labelClass: 'tabbed'
          },
          2: {tn: '513'},
          4: {num: '513'},
          6: {tn: '514'},
          8: {num: '514'}
        },
        {
          0: {
            label: '• other government assistance',
            labelClass: 'tabbed'
          },
          2: {tn: '515'},
          4: {num: '515'},
          6: {tn: '516'},
          8: {num: '516'}
        },
        {
          0: {
            label: '• non-government assistance and contract payments',
            labelClass: 'tabbed'
          },
          2: {tn: '517'},
          4: {num: '517'},
          6: {tn: '518'},
          8: {num: '518'}
        },
        {
          0: {
            label: '• current expenditures (other than salary or wages) not paid within ' +
            '180 days of the tax year-end (see note 5)',
            labelClass: 'tabbed'
          },
          2: {tn: '520'},
          4: {num: '520'},
          6: {type: 'none'},
          8: {type: 'none'}
        },
        {
          0: {
            label: '• amounts paid in respect of an SR&ED contract to a person or partnership ' +
            'that is not a taxable supplier',
            labelClass: 'tabbed'
          },
          2: {tn: '528'},
          4: {num: '528'},
          8: {type: 'none'}
        },
        {
          0: {
            label: '• 20% of expenditures included on lines 340 and 370',
            labelClass: 'tabbed'
          },
          2: {tn: '529'},
          4: {num: '529'},
          8: {type: 'none'}
        },
        {
          0: {
            label: '• prescribed expenditures not allowed by regulations (see guide)',
            labelClass: 'tabbed'
          },
          2: {tn: '530'},
          4: {num: '530'},
          6: {tn: '532'},
          8: {num: '532'}
        },
        {
          0: {
            label: '• other deductions (see guide)',
            labelClass: 'tabbed'
          },
          2: {tn: '533'},
          4: {num: '533'},
          6: {tn: '535'},
          8: {num: '535'}
        },
        {
          0: {label: '• non-arm\'s length transactions', labelClass: 'tabbed'},
          4: {type: 'none'},
          8: {type: 'none'}
        },
        {
          0: {
            label: '- assistance allocated to you (complete Form T1145*)',
            labelClass: 'tabbed2'
          },
          2: {tn: '538'},
          4: {num: '538'},
          6: {tn: '540'},
          8: {num: '540'}
        },
        {
          0: {
            label: '-  expenditures for non-arm\'s length SR&ED contracts (from line 345)',
            labelClass: 'tabbed2'
          },
          2: {tn: '541'},
          4: {num: '541'},
          8: {type: 'none'}
        },
        {
          0: {
            label: '-  adjustments to purchases (limited to costs) of goods and services from ' +
            'non-arm\'s length suppliers (see guide)',
            labelClass: 'tabbed2'
          },
          2: {tn: '542'},
          4: {num: '542'},
          6: {tn: '543'},
          8: {num: '543'}
        },
        {
          0: {
            label: '-  qualified expenditures you transferred (complete Form T1146**)',
            labelClass: 'tabbed2'
          },
          2: {tn: '544'},
          4: {num: '544'},
          6: {tn: '546'},
          8: {num: '546'}
        },
        {
          0: {
            label: '<b>Subtotal</b> (line 511 minus lines 513 to 544 and line 512 minus lines 514 to 546)',
            labelClass: 'fullLength'
          },
          2: {tn: '557'},
          4: {num: '557'},
          6: {tn: '558'},
          8: {num: '558'}
        },
        {
          0: {
            label: '<b>Qualified SR&ED expenditures </b>(add lines 557 and 558)',
            labelClass: 'fullLength'
          },
          4: {type: 'none'},
          6: {tn: '559'},
          8: {num: '559'}
        },
        {
          0: {label: 'Add', labelClass: 'fullLength bold'},
          4: {type: 'none'},
          8: {type: 'none'}
        },
        {
          0: {
            label: '• repayments of assistance and contract payments made in the year',
            labelClass: 'tabbed'
          },
          4: {type: 'none'},
          6: {tn: '560'},
          8: {num: '560'}
        },
        {
          0: {
            label: '<b>Total qualified SR&ED expenditures for ITC purposes</b> (add lines 559 and 560)',
            labelClass: 'fullLength'
          },
          4: {type: 'none'},
          6: {tn: '570'},
          8: {num: '570'}
        }
      ]
    },
    '1012': {
      maxLoop: 100000,
      showNumbering: true,
      hasTotals: true,
      columns: [
        {
          header: 'Name of specified employee',
          tn: '850',
          type: 'text'
        },
        {
          header: 'Total salary or wages for the year (SR&ED and non-SR&ED) excluding bonuses, ' +
          'remuneration based on profits, and taxable benefits (to the nearest dollar)',
          tn: '852',
          colClass: 'std-input-width'
        },
        {
          header: '% of time spent on SR&ED (maximum 75%)',
          colClass: 'small-input-width',
          tn: '854',
          "validate": {
            "and": [
              'percent',
              {
                "or": [
                  {"length": {"min": "1", "max": "6"}},
                  {"check": "isEmpty"}
                ]
              }
            ]
          },
          format: {
            suffix: '%'
          },
          decimals: 3
        },
        {
          header: 'Amount in column 2 multiplied by percentage in column 3',
          tn: '856',
          colClass: 'std-input-width'
        },
        {
          header: 'B = Number of days employed in tax year',
          colClass: 'small-input-width'
        },
        {
          header: '2.5 x A x B/365<br> A = Year\'s maximum pensionable earnings <br>B = Number of days employed in tax year',
          tn: '858',
          colClass: 'std-input-width'
        },
        {
          header: 'Amount in column 4 or 5, whichever amount is less',
          tn: '860',
          total: true,
          totalMessage: '(Enter total of column 6 on line 816)',
          colClass: 'std-input-width'
        }
      ]
    },
    '1013': {
      fixedRows: true,
      paddingRight: 'std-input-col-padding-width',
      hasTotals: true,
      initRows: 0,
      columns: [
        {
          header: 'Project title or identification code',
          tn: '750',
          type: 'text',
          linkedFieldId: '200'
        },
        {
          header: 'Salary or wages in the tax year<br> (Total of lines 306 to 309)',
          tn: '752',
          colClass: 'std-input-width',
          total: true,
          linkedFieldId: '752'
        },
        {
          header: 'Cost of materials in the tax year<br> (Total of lines 320 and 325',
          colClass: 'std-input-width',
          tn: '754',
          total: true,
          linkedFieldId: '754'
        },
        {
          header: 'Contract expenditures for SR&ED performed on your behalf in the tax year <br> ' +
          '(Total of lines 340 and 345)',
          colClass: 'std-input-width',
          tn: '756',
          total: true,
          linkedFieldId: '756'
        }
      ]
    },
    '1014': {
      infoTable: true,
      fixedRows: true,
      columns: getExpandableColumns('Canadian (%)', 'Foreign (%)'),
      cells: [
        {
          0: {label: 'Internal'},
          2: {tn: '600'},
          4: {num: '600', decimals: 3},
          6: {type: 'none'},
          8: {num: '601', decimals: 3}
        },
        {
          0: {label: 'Parent companies, subsidiaries, and affiliated companies'},
          2: {tn: '602'},
          4: {num: '602', decimals: 3},
          6: {tn: '604'},
          8: {num: '604', decimals: 3}
        },
        {
          0: {label: 'Federal grants (do not include funds or tax credits from SR&ED tax incentives)'},
          2: {tn: '606'},
          4: {num: '606', decimals: 3},
          8: {type: 'none'}
        },
        {
          0: {label: 'Federal contracts'},
          2: {tn: '608'},
          4: {num: '608', decimals: 3},
          8: {type: 'none'}
        },
        {
          0: {label: 'Provincial funding'},
          2: {tn: '610'},
          4: {num: '610', decimals: 3},
          8: {type: 'none'}
        },
        {
          0: {label: 'SR&ED contract work performed for other companies on their behalf'},
          2: {tn: '612'},
          4: {num: '612', decimals: 3},
          6: {tn: '614'},
          8: {num: '614', decimals: 3}
        },
        {
          0: {label: 'Other funding (e.g., universities, foreign governments)'},
          2: {tn: '616'},
          4: {num: '616', decimals: 3},
          6: {tn: '618'},
          8: {num: '618', decimals: 3}
        }
      ]
    },
    '1015': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'none', colClass: 'std-padding-width'},//tn
        {type: 'none', colClass: 'std-spacing-width'},//space
        {type: 'singleCheckbox', colClass: 'std-padding-width'},//checkbox
        {type: 'none', colClass: 'std-spacing-width'},//space
        {type: 'none', colClass: 'std-input-col-width-2'},//des
        {type: 'none', colClass: 'std-spacing-width'},//space
        {type: 'none', colClass: 'std-padding-width'},//tn
        {type: 'none', colClass: 'std-spacing-width'},//space
        {type: 'singleCheckbox', colClass: 'std-padding-width'},//checkbox
        {type: 'none', colClass: 'std-spacing-width'},//space
        {type: 'none', colClass: 'std-input-col-width-2'},//des
        {type: 'none'}
      ],
      cells: [
        {
          0: {tn: '620'},
          2: {num: '620'},
          4: {label: 'Basic or Applied research'},
          6: {tn: '622'},
          8: {num: '622'},
          10: {label: 'Experimental development'}
        }
      ]
    },
    '1016': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'none', colClass: 'std-padding-width'},//space
        {type: 'none'},//des
        {type: 'none', colClass: 'std-spacing-width'},//space
        {type: 'singleCheckbox', colClass: 'std-padding-width'},//checkbox
        {type: 'none', colClass: 'std-input-width'}//space ending
      ],
      cells: [
        {1: {label: '1. used the current version of this form'}},
        {
          1: {
            label: '2. entered the method you have chosen for reporting your SR&ED ' +
            'expenditures in Section A of Part 3'
          }
        },
        {1: {label: '3. completed Part 2 for each project'}},
        {
          1: {
            label: '4. filed a completed Schedule T2SCH31 or Form T2038(IND) to claim ITCs on your ' +
            'qualified SR&ED expenditures'
          }
        },
        {
          1: {
            label: '5. filed a completed Form T1145*, T1146**, T1174*** and/or T1263**** ' +
            'including any required attachments, if applicable '
          }
        }
      ]
    },
    '1017': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'none', colClass: 'std-padding-width'},//space
        {type: 'none'},//des
        {type: 'none', colClass: 'std-spacing-width'},//space
        {type: 'singleCheckbox', colClass: 'std-padding-width'},//checkbox
        {type: 'none', colClass: 'std-input-width'}//space ending
      ],
      cells: [
        {1: {label: '1. completed Form T2, Corporation Income Tax Return or Form T1, Income Tax and Benefit Return'}},
        {1: {label: '2. filed the appropriate provincial and/or territorial tax credit forms, if applicable'}},
        {1: {label: '3. retained documents to support the SR&ED work performed and SR&ED expenditures you claimed'}},
        {1: {label: '4. checked boxes 231 and 232 on page 2 of your T2 return to indicate attachment of Form T661 and Schedule T2SCH31'}}
      ]
    },
    '1018': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'none', colClass: 'std-spacing-width'},//space
        {type: 'singleCheckbox', colClass: 'std-padding-width'},//checbox
        {type: 'none', colClass: 'std-spacing-width'},//space
        {type: 'none'}//des
      ],
      cells: [
        {3: {label: 'Yes (complete the claim preparer information table and lines 970 and 975 below)'}},
        {3: {label: 'No (complete lines 970 and 975)'}}
      ]
    },
    '1019': {
      maxLoop: 100000,
      showNumbering: true,
      hasTotals: true,
      columns: [
        {
          header: 'Name of claim preparer<br>(company or individual)',
          tn: '940',
          type: 'text',
          colClass: 'std-input-col-width'
        },
        {
          header: 'Business number',
          tn: '945',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR'],
          colClass: 'std-input-width'
        },
        {
          header: 'Billing arrangement code',
          colClass: 'std-input-width',
          type: 'dropdown',
          options: [
            {
              value: "0",
              option: ' '
            },
            {
              value: "1",
              option: 'Contingency fee arrangement – where the fee is based on a percentage of the investment tax credit earned'
            },
            {value: "2", option: 'Hourly rate'},
            {value: "3", option: 'Daily rate'},
            {value: "4", option: 'Flat fee arrangement (lump sum)'},
            {value: "5", option: 'Other arrangements – describe the arrangement in box 960 in 10 words or less'}
          ],
          tn: '950'
        },
        {
          header: 'Billing rate (percentage, hourly/daily rate or flat fee',
          colClass: 'std-input-width',
          tn: '955',
          decimals: 2
        },
        {
          header: 'Other billing arrangement(s) <br> (Maximum 10 words)',
          tn: '960'
        },
        {
          header: 'Total fee paid, payable or expected to pay',
          colClass: 'std-input-width',
          tn: '965',
          total: true
        }
      ]
    },
    '1021': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'none', colClass: 'std-padding-width'},//space
        {type: 'none', colClass: 'std-padding-width'},//tn
        {type: 'none', colClass: 'std-padding-width'},//space
        {type: 'text'},//name
        {type: 'none', colClass: 'std-padding-width'},//space
        {type: 'text'},//signature
        {type: 'none', colClass: 'std-padding-width'},//space
        {type: 'none', colClass: 'std-padding-width'},//tn
        {type: 'none', colClass: 'std-padding-width'},//space
        {type: 'date', colClass: 'std-input-col-width'},//date
        {type: 'none', colClass: 'std-padding-width'}//ending space
      ],
      cells: [
        {
          1: {tn: '165'},
          3: {num: '165'},
          7: {tn: '170'},
          9: {num: '170'}
        },
        {
          3: {
            type: 'none',
            label: 'Name of authorized signing officer of the corporation, or individual',
            labelClass: 'center'
          },
          5: {type: 'none', label: 'Signature', labelClass: 'center', cellClass: 'verticalAlignTop'},
          9: {type: 'none', label: 'Date', labelClass: 'center', cellClass: 'verticalAlignTop'}
        },
        {
          1: {tn: '175'},
          3: {num: '175'},
          5: {type: 'none'},
          9: {type: 'none'}
        },
        {
          3: {
            type: 'none',
            label: 'Name of person/firm who completed this form ',
            labelClass: 'center',
            cellClass: 'verticalAlignTop'
          },
          5: {type: 'none'},
          9: {type: 'none'}
        }
      ]
    },
    '1022': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'none', colClass: 'std-padding-width'},//space
        {type: 'none', colClass: 'std-padding-width'},//tn
        {type: 'none', colClass: 'std-padding-width'},//space
        {type: 'text'},//name
        {type: 'none', colClass: 'std-padding-width'},//space
        {type: 'text'},//signature
        {type: 'none', colClass: 'std-padding-width'},//space
        {type: 'none', colClass: 'std-padding-width'},//tn
        {type: 'none', colClass: 'std-padding-width'},//space
        {type: 'date', colClass: 'std-input-col-width'},//date
        {type: 'none', colClass: 'std-padding-width'}//ending space
      ],
      cells: [
        {
          1: {tn: '970'},
          3: {num: '970'},
          7: {tn: '975'},
          9: {num: '975'}
        },
        {
          3: {
            type: 'none', label: 'Name of authorized signing officer of the corporation, or individual (print)',
            labelClass: 'center'
          },
          5: {type: 'none', label: 'Signature', labelClass: 'center', cellClass: 'verticalAlignTop'},
          9: {type: 'none', label: 'Date', labelClass: 'center', cellClass: 'verticalAlignTop'}
        }
      ]
    },
    '1051': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        }
      ],
      cells: [
        {
          0: {label: 'Salary base'},
          1: {num: '1052'},
          2: {label: 'x'},
          3: {num: '1370'},
          4: {label: '%'},
          7: {type: 'none'},
          8: {label: ' ='},
          9: {num: '1054'},
          10: {label: 'a'}
        },
        {
          0: {label: 'Salary base'},
          1: {num: '1055'},
          2: {label: 'x'},
          3: {num: '1371'},
          4: {label: '%x'},
          5: {
            label: 'Number of 2013 calendar days in the tax year',
            labelClass: 'center',
            cellClass: 'singleUnderline'
          },
          7: {num: '1057', cellClass: 'singleUnderline'},
          8: {label: ' ='},
          9: {num: '1058'},
          10: {label: 'b'}
        },
        {
          1: {type: 'none'},
          3: {type: 'none'},
          5: {label: 'Number of days in the tax year ', labelClass: 'center'},
          7: {num: '1059'},
          9: {type: 'none'}
        },
        {
          0: {label: 'Salary base'},
          1: {num: '1060'},
          2: {label: 'x'},
          3: {num: '1372'},
          4: {label: '%x'},
          5: {label: 'Number of days after 2013 in the tax year', labelClass: 'center', cellClass: 'singleUnderline'},
          7: {num: '1062', cellClass: 'singleUnderline'},
          8: {label: ' ='},
          9: {num: '1063'},
          10: {label: 'c'}
        },
        {
          1: {type: 'none'},
          3: {type: 'none'},
          5: {label: 'Number of days in the tax year ', labelClass: 'center'},
          7: {num: '1064'},
          9: {type: 'none'}
        }
      ]
    },
    '999': {
      fixedRows: true,
      columns: [
        {
          header: 'Code',
          type: 'none',
          colClass: 'small-input-width'
        },
        {
          header: 'Type of billing arrangement',
          type: 'none'
        }
      ],
      cells: [
        {
          0: {label: '1'},
          1: {label: 'Contingency fee arrangement – where the fee is based on a percentage of the investment tax credit earned'}
        },
        {
          0: {label: '2'},
          1: {label: 'Hourly rate'}
        },
        {
          0: {label: '3'},
          1: {label: 'Daily rate'}
        },
        {
          0: {label: '4'},
          1: {label: 'Flat fee arrangement (lump sum)'}
        },
        {
          0: {label: '5'},
          1: {label: 'Other arrangements – describe the arrangement in box 960 in 10 words or less'}
        }
      ]
    }
  })
})();