(function() {

  wpw.tax.create.calcBlocks('t2s393', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('501', 'ratesMb', '122');
      field('105').assign(field('104').get());
      calcUtils.sumBucketValues('111',['105', '110', '115']);
      field('500').assign(field('111').get());

      field('200').assign(field('500').get() * field('501').get() * 0.01);
    });

  });
})();
