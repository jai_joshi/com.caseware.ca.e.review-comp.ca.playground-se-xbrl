(function() {
  'use strict';
  wpw.tax.global.formData.t2s393 = {
    formInfo: {
      abbreviation: 't2s393',
      title: 'Manitoba Nutrient Management Tax Credit',
      //subTitle: '(2012 and later tax years)',
      schedule: 'Schedule 393',
      code: 'Code 1203',
      formFooterNum: 'T2 SCH 393 E (17)',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'You can claim a Manitoba nutrient management tax credit under section 10.2.1 of the ' +
              '<i>Income Tax Act</i> (Manitoba) if the corporation:',
              sublist: [
                'has a permanent establishment in Manitoba and is carrying on the business of farming in Manitoba;',
                'had eligible expenditures in the current tax year; and',
                'has not included any amount in respect of the property in computing a tax credit' +
                ' under any other section of the <i>Income Tax Act</i> (Manitoba)'
              ]
            },
            {
              label: 'This credit is a refundable credit equal to 10% of the total eligible' +
              ' expenditures of the corporation for the tax year.'
            },
            {
              label: 'Eligible expenditures refer to the capital cost of an acquired ' +
              'depreciable capital property that:',
              sublist: [
                'is an item of eligible equipment;',
                'was acquired by the corporation after April 17, 2012, and before April 12, 2017;',
                'became "available for use" by the corporation in the tax year and before April 12, 2017, ' +
                'as determined under subsections 13(27) and 13(28) of the federal <i>Income Tax Act</i>, ' +
                '<b>not including</b> the time just before the disposition of the property by the corporation ' +
                'as per paragraphs 13(27)(c) and 13(28)(d);',
                'was not used or acquired for use by anyone before it was acquired by the corporation; and',
                'was acquired for use in the corporation\'s farming business or in the farming business' +
                ' of a partnership or trust of which the corporation had an interest.'
              ]
            },
            {
              label: 'Eligible equipment includes any of the following types of equipment, if used by the corporation' +
              ' for the purpose of eliminating or significantly reducing the risk that nutrients or pathogens in ' +
              'the organic waste used or created in the course of its business in ' +
              'Manitoba will be transported to a waterway:',
              sublist: [
                'a solid-liquid separation system;',
                'an anaerobic digester;',
                'a gravity settling tank;',
                'a manure treatment system;',
                'a manure composting facility; and',
                'a storage tank used for winter manure storage in an agricultural operation with fewer ' +
                'than 300 animal units.'
              ]
            },
            {
              label: 'The credit is considered government assistance under paragraph 12(1)(x) of the federal ' +
              '<i>Income Tax Act</i> and must be included in income in the tax year the credit is received.' +
              ' The credit is not considered government assistance under section 10.2.1 of the <i>Income Tax Act</i>' +
              ' (Manitoba) for calculating the credit itself'
            },
            {
              label: 'To claim this credit, file this schedule no later than <b>one year</b> after the filing-due' +
              ' date for your <i>T2 Corporation Income Tax Return</i> for the tax year in which the ' +
              'property was acquired. File a completed copy of this schedule with your T2 Return.'
            }
          ]
        }
      ],
      category: 'Manitoba Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Eligible expenditures made in the current tax year',
        'rows': [
          {
            'type': 'table',
            'num': '100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Capital cost of depreciable capital property acquired by the corporation in the current tax year</b> (total of column 103)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Corporation\'s proportionate share of eligible expenditures allocated from a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Corporation\'s proportionate share of eligible expenditures allocated from a trust',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '115',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '115'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': '<b>Total eligible expenditures made in the current tax year</b> (total of amounts A to C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '111'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* The acquisition date is the date that the property became available for use. <br>** When you calculate the capital cost of the property, deduct the amount of any government assistance received or receivable for the property',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - Refundable Manitoba nutrient management tax credit',
        'rows': [
          {
            'type': 'table',
            'num': '150'
          },
          {
            label: '(enter amount E on line 325 of Schedule 5, <i>Tax Calculation Supplementary – Corporations</i>)',
            labelClass: 'fullLength'
          }
        ]
      }
    ]
  };
})();
