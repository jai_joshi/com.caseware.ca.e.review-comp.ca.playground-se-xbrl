(function() {

  wpw.tax.global.tableCalculations.t2s393 = {
    '100': {
      'maxLoop': 100000,
      hasTotals: true,
      'columns': [
        {
          'header': 'CCA Class',
          cellClass: 'alignCenter',
          type: 'selector',
          width: '111px',
          tn: '101',
          selectorOptions: {
            title: 'CCA Classes',
            items: wpw.tax.codes.ccaClasses,
            hideKeys: true
          }
        },
        {
          'header': 'Description of depreciable capital property',
          type: 'text'
        },
        {
          'header': 'Acquisition date*',
          'width': '150px',
          'type': 'date',
          'canSort': true,
          'tn': '102'
        },
        {
          'header': 'Capital cost**',
          'width': '150px',
          'total': true,
          'totalNum': '104',
          'tn': '103', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        }]
    },
    '150': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          colClass: 'std-input--col-width',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'small-input-width'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        }],
      'cells': [
        {
          '0': {
            'label': 'Manitoba nutrient management tax credit',
            labelClass: 'fullLength bold'
          },
          '1': {
            label: 'Amount D from Part 1',
            labelClass: 'right'
          },
          '2': {
            'num': '500',
            'disabled': true
          },
          '3': {
            label: 'x'
          },
          '4': {
            num: '501'
          },
          '5': {
            label: '%='
          },
          '6': {
            'tn': '200'
          },
          '7': {
            'num': '200', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'doubleUnderline'
          },
          '8': {
            'label': 'E',
            'labelClass': 'left'
          }
        }
      ]
    }
  }
})();
