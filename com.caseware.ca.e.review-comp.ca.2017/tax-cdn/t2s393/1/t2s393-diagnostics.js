(function() {
  wpw.tax.create.diagnostics('t2s393', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3930001', common.prereq(common.check(['t2s5.325'], 'isNonZero'),
        common.requireFiled('T2S393')));

    diagUtils.diagnostic('3930003', common.prereq(common.requireFiled('T2S393'),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[3]], 'isFilled') && tools.field('t2s5.325').isNonZero())
              return tools.requireAll([row[0], row[2]], 'isFilled');
            else return true;
          });
        }));

  });
})();
