(function() {
  var descriptionWidth = '200px';
  var columnWidth = '145px';
  var businessNumberWidth = '150px';
  var columnDropdownWidth = '70px';

  var corpTypeOption = [
    {option: '1. Canadian Controlled Private Corporation - CCPC', value: '1'},
    {option: '2. Other private corporation', value: '2'},
    {option: '3. Public Corporation', value: '3'},
    {option: '4. Corporation controlled by a public corporation', value: '4'},
    {option: '5. Other', value: '5'}
  ];

  wpw.tax.global.tableCalculations.t2s49 = {
    '085': {
      repeats: ['095'],
      hasTotals: true,
      fixedRows: true,
      showNumbering: true,
      'columns': [
        {
          'header': '1<br> Names of associated corporations',
          'num': '100',
          'width': descriptionWidth,
          'tn': '100',
          type: 'text'
        },
        {
          'header': '2<br> Business Number of Associated Corporations',
          'num': '200',
          'width': businessNumberWidth,
          'tn': '200',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          'header': '3<br>Type of corporation code',
          'num': '300',
          'width': columnDropdownWidth,
          'tn': '300',
          type: 'dropdown',
          'options': corpTypeOption
        },
        {
          'header': 'Expenditure limit (Amount G below)',
          'width': columnWidth
        },
        {
          'header': 'Expenditure limit allocated *<br>$',
          'num': '400',
          'width': columnWidth,
          'total': true,
          'totalMessage': 'The expenditure limit (cannot be more than $3,000,000)',
          'totalLimit': '3000000',
          'tn': '400',
          'totalNum': '410',
          'totalTn': '410'
        }
      ]
    },
    '095': {
      scrollx: true,
      hasTotals: true,
      'showNumbering': true,
      'columns': [
        {
          'header': '1<br> Name of Associated Corporations',
          'width': descriptionWidth,
          type: 'text'
        },
        {
          'header': 'Taxable capital employed in Canada',
          'width': columnWidth,
          'total': true
        },
        {
          'header': 'Taxable income',
          'width': columnWidth,
          'total': true
        },
        {
          'header': 'Tax year-start',
          type: 'date',
          'width': columnWidth
        },
        {
          'header': 'Tax year-end',
          type: 'date',
          'width': columnWidth
        },
        {
          'header': 'Number of days in the fiscal year',
          'width': columnWidth
        },
        {
          'header': 'Grossed up taxable income<br>$',
          'width': columnWidth,
          'total': true
        }
      ]
    },
    '010': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          'width': '100px',
          'type': 'none'
        },
        {
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Column 1:',
            'labelClass': 'bold'
          },
          '1': {
            'label': 'Enter the legal name of each corporation in the associated group, including CCPCs ' +
            'and non-CCPCs. Do not include corporations deemed not  to be associated under subsection 127' +
            '(10.22) of the Income Tax Act.',
            'labelClass': 'fullLength'
          }
        },
        {
          '0': {
            'label': 'Column 2:',
            'labelClass': 'bold'
          },
          '1': {
            'label': 'Provide the business number for each corporation in column 1 (if a corporation is not registered,' +
            ' enter "NR").',
            'labelClass': 'fullLength'
          }
        },
        {
          '0': {
            'label': 'Column 3:',
            'labelClass': 'bold'
          },
          '1': {
            'label': 'Enter "1" for CCPC\'s or "2" for Non-CCPC\'s that applies for each corporation identified ' +
            'in columns 1 and 2.',
            'labelClass': 'fullLength'
          }
        },
        {
          '0': {
            'label': 'Column 4:',
            'labelClass': 'bold'
          },
          '1': {
            'label': 'Enter the amount of the expenditure limit allocated to each corporation that ' +
            'has type of corporation code 1 in column 3. The rules for determining the expenditure limit ' +
            'that can be allocated (subsection 127(10.2) of the Income Tax Act) are explained below.',
            'labelClass': 'fullLength'
          }
        }
      ]
    }
  }
})();
