(function() {
  wpw.tax.create.diagnostics('t2s49', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('0490002', common.prereq(function(tools) {
      return (tools.field('t2j.040').get() == '1' &&
      ((tools.field('t2s31.400').isPositive() || tools.field('t2s31.410').isPositive()) &&
      (tools.field('t2s31.350').isPositive() || tools.field('t2s31.360').isPositive())) &&
      (tools.field('t2s31.385').isZero() || tools.field('t2s31.385').get() == '1'))
    }, common.requireFiled('T2S49')));

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S49'), forEach.row('085', forEach.bnCheckCol(1, true))));

    diagUtils.diagnostic('0490010', common.prereq(common.and(
        common.requireFiled('T2S49'),
        common.check(['050'], 'isNotDefault')),
        function(tools) {
          return tools.field('050').require(function() {
            return this.isFilled() &&
                this.get() == tools.field('t2j.061').getKey('year');
          });
        }));

    diagUtils.diagnostic('0490150', common.prereq(common.requireFiled('T2S49'),
        function(tools) {
          var table = tools.field('085');
          return tools.checkAll(table.getRows(), function(row) {
            var columns = [row[0], row[1], row[2], row[4]];
            if (tools.checkMethod([row[0], row[1], row[2]], 'isFilled'))
              return tools.requireAll(columns, 'isFilled');
            else return true;
          });
        }));

  });
})();
