(function() {
  'use strict';

  wpw.tax.global.formData.t2s49 = {
    'formInfo': {
      'abbreviation': 'T2S49',
      'title': 'Agreement Among Associated Canadian-Controlled Private Corporations to Allocate the Expenditure Limit',
      'schedule': 'Schedule 49',
      'code': 'Code 1201',
      'formFooterNum': 'T2 SCH 49 E (13)',
      'headerImage': 'canada-federal',
      'description': [
        {
          'type': 'list',
          'items': [
            'Use this schedule to allocate the annual expenditure limit among associated Canadian-controlled private ' +
            'corporations (CCPCs), (subsection 127(10.2) of the <i>Income Tax Act</i>), in order to calculate the' +
            ' investment tax credit eligible for the 35% rate on qualifying scientific research and ' +
            'experimental development expenditures.',
            'An associated CCPC that has more than one tax year ending in a calendar year is required to file an' +
            ' agreement for each tax year ending in that calendar year.']
        }
      ],
      category: 'Federal Tax Forms'
    },
    'sections': [
      {
        'hideFieldset': true,
        'rows': [
          {
            'type': 'table',
            'num': '010'
          }
        ]
      },
      {
        'header': 'Allocating the expenditure limit',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Date filed (do not use this area)',
            'labelWidth': '75%',
            'num': '025',
            'type': 'infoField',
            'inputType': 'date',
            'tn': '025'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter the calendar year to which the agreement applies',
            'labelWidth': '75%',
            'num': '050',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '4',
                    'max': '4'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'type': 'infoField',
            'inputType': 'fixedSizeBox',
            'boxes': 4,
            'tn': '050'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Is this an amended agreement for the above-noted calendar year that is intended to replace an agreement previously filed by any of the associated corporations listed below?',
            'num': '075',
            'labelWidth': '75%',
            'type': 'infoField',
            'inputType': 'radio',
            'tn': '075'
          },
          {
            'type': 'table',
            'num': '085'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The last tax year-ending in the previous calendar year data',
            'labelClass': 'fullLength bold '
          },
          {
            'type': 'table',
            'num': '095'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': ' <b> The expenditure limit is calculated as follows: </b>',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '[($8,000,000 <b> minus </b> 10<b>A</b>) X (($40,000,000 <b> minus B) divided by </b> $40,000,000)], where',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> A= </b> the greater of:',
            'labelClass': 'fullLength'
          },
          {
            'label': '• $500,000; and',
            'labelClass': 'tabbed'
          },
          {
            'label': '• the total of all taxable incomes (prior to any loss carry-backs applied) of all associated corporations identified in columns 1 and 2 for their last tax years ** ending in the previous calendar year.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> B= </b> the total of all taxable capital employed in Canada of all associated corporations ' +
            'for their last tax year ending in the previous calendar year minus $10 million. If this amount is nil or' +
            ' negative, enter "0". If this amount is over $40 million, enter $40 million.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount A',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '425',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '425'
                }
              },
              null
            ]
          },
          {
            'label': 'Amount B',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '495',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '495'
                }
              },
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Special rules apply if a CCPC has more than one tax year ending in a calendar year and is' +
            ' associated in more than one of those years with another CCPC that has a tax year ending in the same' +
            ' calendar year. In this case, the expenditure limit for the second (and subsequent) tax year(s) ' +
            'will be equal to the expenditure limit allocated for the first tax year ending in the calendar year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '** If any of the tax years referred to in A above are less than 51 weeks, gross up the taxable' +
            ' incomes for those tax years by the ratio that 365 is of the number of days in those tax years.' +
            ' Use these grossed up amounts when calculating the expenditure limit.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Calculation of expenditure limit',
        'rows': [
          {
            'label': 'Limit amount of $8,000,000',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '601',
                  'type': 'number'
                }
              },
              null
            ]
          },
          {
            'label': 'Minus 10<b>A</b>',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '602',
                  'type': 'number'
                }
              },
              null
            ]
          },
          {
            'label': ' ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '603',
                  'type': 'number'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Limit amount of $40,000,000',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '604',
                  'type': 'number'
                }
              },
              null
            ]
          },
          {
            'label': 'Minutes <b>B</b>',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '605',
                  'type': 'number'
                }
              },
              null
            ]
          },
          {
            'label': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '606'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'label': 'Amount D divided by $40,000,000 = ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '609'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'label': 'Total (C x E)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '607',
                  'type': 'number'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The expenditure limit (Lesser of amount F and $3,000,000)',
            'labelClass': 'text-right bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '608',
                  'type': 'number'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G'
                }
              }
            ]
          }
        ]
      }
    ]
  };
})();
