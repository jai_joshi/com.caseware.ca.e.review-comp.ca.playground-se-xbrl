(function() {

  wpw.tax.create.calcBlocks('t2s49', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field, form) {

      var table = field('085');
      var linkedTable = form('entityProfileSummary').field('090');
      table.getRows().forEach(function(row, rIndex) {
        row.forEach(function(cell, cIndex) {
          cell.assign(linkedTable.cell(rIndex, cIndex).get());
          cell.source(linkedTable.cell(rIndex, cIndex));
        })
      });
    });

    calcUtils.calc(function(calcUtils, field) {

      //table 095 calcs-The last tax year ending in the previous calendar year data
      field('095').getRows().forEach(function(row, rIndex) {
        row[5].assign(wpw.tax.actions.calculateDaysDifference(row[3].get(), row[4].get()));
        row[6].assign(row[2] / row[5].get() * 365);
      });
    });

    calcUtils.calc(function(calcUtils, field) {
      //calculate amount A
      field('425').assign(Math.max(500000, field('095').total(6).get()));
      //calculate amount B
      var amountBValue = field('095').total(1).get() - 10000000;
      field('495').assign(Math.max(Math.min(40000000, amountBValue), 0));

      //calculation for expenditure limit
      field('601').assign(8000000);
      field('602').assign(field('425').get() * 10);
      field('603').assign(field('601').get() - field('602').get());
      field('604').assign(40000000);
      field('605').assign(field('495').get());
      field('606').assign(field('604').get() - field('605').get());
      field('609').assign(field('606').get() / 40000000);
      field('607').assign(field('603').get() * field('609').get());
      field('608').assign(Math.min(field('607').get(), 3000000));
    });

    calcUtils.calc(function(calcUtils, field) {
      //table 085 calcs
      field('085').getRows().forEach(function(row, rIndex) {
        row[3].assign(field('608').get());
      });
    });

    calcUtils.calc(function(calcUtils, field) {
      field('050').assign(field('cp.tax_end').getKey('year'));
    });
  });
})();
