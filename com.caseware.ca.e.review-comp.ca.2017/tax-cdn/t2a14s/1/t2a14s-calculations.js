(function() {
  function disableNums(field, numArray) {
    for (var i = 0; i < numArray.length; i++) {
      field(numArray[i]).disabled(true);
    }
  }

  wpw.tax.create.calcBlocks('t2a14s', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      //part 1
      field('100').assign(Math.max(field('T2A14.022').get(), 0));
      field('100').source(field('T2A14.022'));
      calcUtils.getGlobalValue('102', 'T2S10', '1580');
      field('104').assign(field('100').get() +  field('102').get());
      calcUtils.getGlobalValue('106', 'T2A14', '030');
      field('108').assign(field('T2A14.054').get() * 3/2);
      field('110').assign(field('106').get() - field('108').get());
      field('112').assign(Math.max(field('104').get() - field('110').get(), 0));
      field('114').assign(field('112').get() * 4/3);
      field('116').assign(field('114').get());
      field('118').assign(field('110').get());
      field('120').assign(field('116').get() + field('118').get());
      field('122').assign(field('100').get());
      field('124').assign(field('120').get() - field('122').get());
      field('126').assign(field('116').get() - field('124').get());
      //TODO: to transfer field126 to t2a13
    });
    calcUtils.calc(function (calcUtils, field) {
      calcUtils.getGlobalValue('200', 'T2S10', '101');
      calcUtils.getGlobalValue('202', 'T2S10', '102');
      var is200Selected = (field('200').get() == '1');
      var is202Selected = (field('202').get() == '1');
      if (is200Selected) {
        //If you answered yes at line 200, complete amounts 300 to 310.
        calcUtils.getGlobalValue('300', 'T2S10', '450');
        field('302').assign(field('300').get() * 0.5);
        calcUtils.getGlobalValue('304', 'T2A14', '054');
        field('306').assign(Math.min(field('302').get(), field('304').get()));
        field('308').assign(field('306').get() * 2);
        field('310').assign(field('300').get() - field('308').get());
      }
      else {
        //if you answer no at line 200, proceed to line 202
        calcUtils.removeValue(['302', '304', '306'], true);
      }
      if (is202Selected) {
        //If you answered yes at line 202, complete amounts 320 to 324.
        calcUtils.removeValue(['326', '328', '330', '332']);
        calcUtils.getGlobalValue('320', 'T2A14', '054');
        calcUtils.getGlobalValue('322', 'T2A14S', '306');
        field('324').assign(Math.max(field('320').get() - field('322').get(),0));
      }
      else  {
        //If you answered no at line 202,complete amounts 326 to 332.
        calcUtils.removeValue(['320', '322', '324'], true);
        calcUtils.getGlobalValue('326', 'T2A14', '054');
        calcUtils.getGlobalValue('328', 'T2A14S', '306');
        field('330').assign(Math.max(field('326').get() - field('328').get(),0));
        field('332').assign(field('330').get() * 2);

      }
    })
  })
}());