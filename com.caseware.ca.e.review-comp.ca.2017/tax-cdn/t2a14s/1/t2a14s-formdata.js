(function () {

  wpw.tax.create.formData('t2a14s', {
    formInfo: {
      abbreviation: 't2a14s',
      title: 'AT14 - Alberta Cumulative Eligible Capital Deduction - AT1 Schedule 14 Supplemental Worksheet',
      schedule: 'Schedule 14',
      category: 'Alberta Forms',
      formFooterNum: 'AT14S (Mar-18)',
      hideHeaderBox: true,
      hideProtectedB: true,
      description: [
        {
          type: 'list',
          items: [
            'Use this worksheet if you have or had eligible capital property in the taxation year.',
            'Do not complete this worksheet for taxation years that start after December 31, 2016.',
            'The cumulative eligible capital (CEC) regime ends December 31, 2016. Beginning January 1, 2017, ' +
            'the CEC is replaced with the capital cost allowance rules. Property that would be eligible capital' +
            ' property (ECP) prior to January 1, 2017 will be depreciable property in the new' +
            ' Class 14.1 after December 31, 2016.',
            'Prior to 2017, a separate CEC account must be kept for each business.',
            'Effective January 1, 2017, a separate Class 14.1 is to be used in respect of each business' +
            ' of the taxpayer, and the respective capital cost allowance for Class 14.1 is to be claimed' +
            ' on Schedule 13.'
          ]
        }
      ]
    },
    sections: [
      {
        header: 'Undepreciated capital cost of Class 14.1 at the beginning of January 1, 2017',
        rows: [
          {
            label: 'Traditional rules',
            labelClass: 'bold'
          },
          {
            label: 'The CEC pool balances are calculated and transferred to the new Class 14.1 ' +
            'as of January 1, 2017. The opening balance of Class 14.1 must equal the balance in the existing' +
            ' CEC pool as of December 31, 2016.'
          },
          {
            'label': 'CEC balance at the beginning of January 1, 2017<br>' +
            '(Schedule 14, line 022, if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              }
            ]
          },
          {
            'label': 'Total CEC deductions applied in prior years that have not been recaptured',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '102',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '102'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 100 + line 102)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              }
            ]
          },
          {
            'label': 'Negative CEC balance (Schedule 14, line 030)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '106',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '106'
                }
              },
              null
            ]
          },
          {
            'label': 'Line 054 from Schedule 14 X 3/2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '108',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '108'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 106 - line 108)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 104 - line 110, if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '112',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '112'
                }
              }
            ]
          },
          {
            'label': 'Deemed capital cost of former ECP (line 112 X 4/3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '114',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '114'
                }
              }
            ]
          },
          {
            'label': 'Total deemed capital cost of former ECP (line 114)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '116',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '116'
                }
              }
            ]
          },
          {
            'label': 'Line 110',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '118',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '118'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 116 + line 118)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '120',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              }
            ]
          },
          {
            'label': 'Line 100',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '122',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '122'
                }
              }
            ]
          },
          {
            'label': 'Amount deemed to have been allowed under ITA paragraph 20(1)(a) ' +
            'for Class 14.1 (line 120 - line 122)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '124',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '124'
                }
              }
            ]
          },
          {
            label: 'Undepreciated capital cost to Schedule 13 (line 116 - line 124)',
            labelClass: 'bold'
          },
          {
            'label': 'If positive, enter the amount on Schedule 13, line 003 in respect of Class 14.1<br>' +
            'If negative, enter the amount on Schedule 13, line 007 as net adjustment of capital cost allowance',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '126',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '126'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Transitional rules under ITA paragraph 13(38)(d)',
        rows: [
          {
            'label': 'Only complete the following if your taxation year ends on or after January 1, 2017 and ' +
            'includes December 31, 2016.',
            labelClass: 'bold'
          },
          {
            type: 'description',
            lines: [
              {
                type: 'list',
                items: [
                  {
                    label: 'There are two elections available for the amount calculated at line 054 of Schedule 14:',
                    sublist:[
                      'Election under ITA subparagraph 13(38)(d)(iv), as adopted by Alberta, to defer a deemed ' +
                      'capital gain or income inclusion by reporting the amount on Schedule 13 (see line 200), and',
                      'Election under ITA subparagraph 13(38)(d)(iii), as adopted by Alberta, to report an income' +
                      ' inclusion instead of a capital gain by reporting the amount on Schedule 12 (see line 202).'
                    ]
                  },
                  {
                    label: 'If neither election is made then complete lines 326 to 332 to calculate the capital' +
                    ' gain to be reported on Schedule 18.'
                  }
                ]
              }
            ]
          },
          {
            label: 'Election under ITA 13(38)(d)(iv)',
            labelClass: 'bold'
          },
          {
            'type': 'infoField',
            'label': 'Is the corporation electing under subparagraph 13(38)(d)(iv) to defer the deemed capital gain or income inclusion?',
            'num': '200',
            'tn': '200',
            'inputType': 'radio'
          },
          {
            label: 'You may elect only if, during the taxation year and after December 31, 2016,' +
            ' you acquired property included in Class 14.1, or are deemed by ITA subsection 13(35) ' +
            'to acquire goodwill in respect of the business.<br>' +
            'If you answered <b>yes</b> at line 200, complete amounts 300 to 310.<br>' +
            'If you answered <b>no</b> at line 200, proceed to line 202.'
          },
          {
            'label': 'Capital cost of goodwill or Class 14.1 property acquired during the taxation year and after December 31, 2016 (including goodwill)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '300',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '300'
                }
              }
            ]
          },
          {
            'label': 'Line 300 X 50%',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '302',
                  'disabled' :true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '302'
                }
              },
              null
            ]
          },
          {
            'label': 'Schedule 14, line 054',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '304',
                  'disabled' :true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '304'
                }
              },
              null
            ]
          },
          {
            'label': 'Lesser of line 302 and line 304',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '306',
                  'disabled' :true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '306'
                }
              },
              null
            ]
          },
          {
            'label': 'Line 306 X 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '308',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '308'
                }
              }
            ]
          },
          {
            label: 'Reduced capital cost of property, goodwill, or Class 14.1 property acquired under ITA clause 13(38)(d)(iv)(B)',
            labelClass: 'bold'
          },
          {
            'label': '(line 300 - line 308) (enter amount on Schedule 13, line 005)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '310',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '310'
                }
              }
            ]
          },
          {
            label: 'Election under ITA 13(38)(d)(iii)',
            labelClass: 'bold'
          },
          {
            'type': 'infoField',
            'label': 'Is the corporation electing under ITA subparagraph 13(38)(d)(iiii) to report an income inclusion instead of a capital gain?',
            'num': '202',
            'tn': '202',
            'inputType': 'radio'
          },
          {
            label: 'If you answered <b>yes</b> at line 202, complete amounts 320 to 324.<br>' +
            'If you answered <b>no</b> at line 202,complete amounts 326 to 332.'
          },
          {
            'label': 'Schedule 14, line 054',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '320',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '320'
                }
              }
            ]
          },
          {
            'label': 'Line 306 (if applicable)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '322',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '322'
                }
              }
            ]
          },
          {
            'label': '<b>Income inclusion under ITA subparagraph 13(38)(d)(iii)</b> (line 320 - line 322)<br>' +
            '(enter amount on Schedule 12, line 012)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '324',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '324'
                }
              }
            ]
          },
          {
            label: 'Proceeds of disposition',
            labelClass: 'bold'
          },
          {
            'label': 'Schedule 14, line 054',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '326',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '326'
                }
              },
              null
            ]
          },
          {
            'label': 'Line 306 (if applicable)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '328',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '328'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 326 - line 328)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '330',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '330'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Proceeds of disposition to be reported (line 330 X 2)</b><br>' +
            '(enter amount on Schedule 18, line 008)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '332',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '332'
                }
              }
            ]
          }
        ]
      }
    ]
  });
})();
