wpw.tax.create.importCalcs('t2s340', function(tools) {
  tools.intercept('NSNSR.SLIPB[1].TtwnsrB1', function(importObj) {
    tools.form('t2s340').setValue('300', importObj.value * 100, 0, importObj.rowIndex)
  })
});
