(function() {
  wpw.tax.create.diagnostics('t2s340', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3400002', common.prereq(common.and(
        common.requireFiled('T2S340'),
        common.check(['t2s5.566'], 'isNonZero')),
        common.check(['103', '130', '140'], 'isNonZero')));

  });
})();
