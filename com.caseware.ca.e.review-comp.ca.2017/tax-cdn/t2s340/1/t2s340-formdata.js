(function() {
  'use strict';

  wpw.tax.global.formData.t2s340 = {
      'formInfo': {
        abbreviation: 'T2S340',
        'title': 'Nova Scotia Research and Development Tax Credit',
        //TODO: DO NOT DELETE THIS LINE: subtitle
        //'subTitle': '(2002 and later tax years)',
        'schedule': 'Schedule 340',
        'showCorpInfo': true,
        code: 'Code 0202',
        formFooterNum: 'T2 SCH 340 E (04)',
        headerImage: 'canada-federal',
        'description': [
          {
            'type': 'list',
            'items': [
              {
                label: 'Use this schedule if you are a corporation with a permanent establishment in Nova ' +
                'Scotia that has made eligible expenditures for scientific research to be carried out ' +
                'in the province and you want to:',
                sublist: [
                  'calculate a refundable Nova Scotia research and development (R&D) tax credit for ' +
                  'the current taxation year;',
                  'calculate a recapture of the Nova Scotia R&D tax credit; or',
                  'renounce the credit. To renounce the credit, include all current-year credits; ' +
                  'partial renouncements are not allowed. You have to file the renouncement on or ' +
                  'before the filing date of the <i>T2 Corporation Income Tax Return</i>.'
                ]
              },
              {
                label: 'An eligible expenditure is one that meets the definition in subsection 41(1) ' +
                'of the Nova Scotia <i>Income Tax Act</i>.'
              },
              {
                label: 'The credit earned in the current taxation year is applied to reduce Nova Scotia ' +
                'income tax otherwise payable for the year, as well as amounts owing under the federal ' +
                'and provincial <i>Income Tax Act</i>s, the Canada Pension Plan, and the <i>Employment Insurance ' +
                'Act</i>. Any remaining balance will be refunded.'
              },
              {
                label: 'Only corporations that are not exempt from tax under section 149 of the federal ' +
                '<i>Income Tax Act</i> can get a refund of the credit.'
              },
              {
                label: 'You can also use this schedule to show a credit allocated to a corporation that is a ' +
                'member of a partnership or a credit allocated to a corporation that is a beneficiary under a trust.'
              },
              {
                label: 'File this schedule with your T2 return for the taxation year in which the credit ' +
                'is claimed or recaptured or a refund is requested.'
              }
            ]
          }
        ],
        category: 'Nova Scotia Forms'
      },
      'sections': [
        {
          'header': 'Part 1 - Calculation of refundable Nova Scotia R&D tax credit',
          'rows': [
            {
              'label': 'Total eligible expenditures for R&D in the taxation year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '103'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '103'
                  }
                }
              ],
              'indicator': 'A'
            },
            {
              'label': 'Rate',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '104'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Current-year credit earned',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '120'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '120'
                  }
                }
              ],
              'indicator': 'B'
            },
            {
              'label': 'Add:',
              'labelClass': 'bold fullLength'
            },
            {
              'label': 'Credit allocated to a corporation that is a member of a partnership',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '130',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '130'
                  }
                },
                null
              ]
            },
            {
              'label': 'Credit allocated to a corporation that is a beneficiary under a trust',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '140',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '140'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '141'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '142'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'C'
            },
            {
              'label': 'Subtotal (amount B plus amount C)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '143'
                  }
                }
              ],
              'indicator': 'D'
            },
            {
              'label': '<b>Deduct</b>: Credit renounced',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '150'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '150'
                  }
                }
              ],
              'indicator': 'E'
            },
            {
              'label': 'Total refundable Nova Scotia R&D tax credit (amount D minus amount E)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '190'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '190'
                  }
                }
              ],
              'indicator': 'F'
            },
            {
              'label': 'Enter amount F on line 566 of Schedule 5.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 2 - Calculation of a recapture of Nova Scotia R&D tax credit',
          'rows': [
            {
              'label': 'You will have a recapture of Nova Scotia R&D tax credit in a year when you meet all the following conditions:',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '- you acquired a particular property in the current year or in any of the four preceding taxation years;',
              'labelClass': 'tabbed fullLength'
            },
            {
              'label': '- you claimed the cost of the particular property as an eligible expenditure for the Nova Scotia R&D tax credit;',
              'labelClass': 'tabbed fullLength'
            },
            {
              'label': '- the cost of the particular property was included in computing your tax credit at the end of the taxation year or was subject to an agreement made under subsection 127(13) of the federal <i>Income Tax Act</i> to transfer qualified expenditures; and',
              'labelClass': 'tabbed fullLength'
            },
            {
              'label': '- you disposed of the particular property or converted it to commercial use after March 31, 2002. You also meet this condition if you disposed of or converted to commercial use a property which incorporates the property previously referred to.',
              'labelClass': 'tabbed fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b>Note</b>: The recapture does not apply if you disposed of the property to a non-arm\'s-length purchaser who intended to use it all or substantially all for R&D. When the purchaser later sells or converts the property to commercial use, the recapture rules will apply to the purchaser based on the historical Nova Scotia R&D tax credit rate of the original user.',
              'labelClass': 'tabbed2 fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'You have to report the recapture on Schedule 5 for the year in which you disposed of the property or converted it to commercial use. If the corporation is a member of a partnership, report its share of the recapture.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If you have more than one disposition for calculations 1 and 2, complete the columns for each disposition for which a recapture applies, using the calculation formats below.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '200'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Enter amount G on line P',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '300'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '400'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Enter amount N on line Q below.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'horizontalLine'
            },
            {
              'label': 'Calculation 3',
              'labelClass': 'bold fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'As a member of a partnership, you have to report your share of the recapture at line O below:',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Corporation\'s share of the recapture of Nova Scotia R&D tax credit',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '760'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '760'
                  }
                }
              ],
              'indicator': 'O'
            },
            {
              'label': 'Enter amount O on line R below.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 3 - Total recapture of Nova Scotia R&D tax credit',
          'rows': [
            {
              'label': 'Recaptured Nova Scotia R&D tax credit for calculation 1 from line G on page 2',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '800'
                  }
                }
              ],
              'indicator': 'P'
            },
            {
              'label': 'Recaptured Nova Scotia R&D tax credit for calculation 2 from line N above',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '801'
                  }
                }
              ],
              'indicator': 'Q'
            },
            {
              'label': 'Recaptured Nova Scotia R&D tax credit for calculation 3 from line O above',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '802'
                  }
                }
              ],
              'indicator': 'R'
            },
            {
              'label': '<b>Total recapture of Nova Scotia R&D tax credit</b> - total of lines P, Q, and R',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '803'
                  }
                }
              ],
              'indicator': 'S'
            },
            {
              'label': 'Enter amount S on line 221 of Schedule 5.',
              'labelClass': 'fullLength'
            }
          ]
        }
      ]
    };
})();
