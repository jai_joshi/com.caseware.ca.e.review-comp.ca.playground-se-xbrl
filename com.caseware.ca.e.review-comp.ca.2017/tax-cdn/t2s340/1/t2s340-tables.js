(function() {
  wpw.tax.global.tableCalculations.t2s340 = {
    "200": {
      hasTotals: true,
      "showNumbering": true,
      "maxLoop": 100000,
      "superHeaders": [{
        "header": "Calculation 1 - If you meet all the above conditions",
        "colspan": "3"
      }],
      "columns": [{
        "header": "Amount of Nova Scotia R&D tax credit you originally calculated for the particular property you acquired, or the original user's tax credit where you acquired the property from a non-arm's-length party, as described in the note above",
        "tn": "700",
        "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
      },
        {
          "header": "Amount calculated using the Nova Scotia R&D tax credit rate at the date of acquisition (or the original user's date of acquisition)on either the proceeds of disposition (if sold in an arm's-length transaction) or the fair market value of the property (in any other case)",
          "tn": "710",
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
        },
        {
          "header": "Amount from column 700 or 710, whichever is less",
          "width": "20%",
          "totalnum": "713",
          "total": true,
          "totalMessage": "Subtotal",
          "totalIndicator": "G",
          "disabled": true
        }
      ]
    },
    "300": {
      "showNumbering": true,
      "repeats": ["400"],
      "maxLoop": 100000,
      "superHeaders": [{
        "header": "Calculation 2 - Only if you acquired all or part of the eligible expenditure from another person under an agreement described in subsection 127(13) of the federal <i>Income Tax Act</i>. Otherwise, enter nil at line N below.",
        "colspan": "3"
      }],
      "columns": [{
        "header": "The rate the transferee used to determine its Nova Scotia R&D tax credit for eligible expenditures under a subsection 127(13) agreement",
        "tn": "720",
        "validator":"percent",
        decimals: 1,
        "indicator": "H",
        format: {
          suffix: '%'
        }
      },
        {
          "header": "The proceeds of disposition of the property if you dispose of it to a person at arm's length; or, in any other case, the fair market value of the property at conversion or disposition",
          "tn": "730","validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          "indicator": "I"
        },
        {
          "header": "The amount, if any, already provided for in calculation 1 (This allows for the situation where only part of the cost of a property is transferred under a subsection 127(13) agreement .)",
          "tn": "740","validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          "indicator": "J"
        }]
    },
    "400": {
      "infoTable": false,
      hasTotals: true,
      "showNumbering": true,
      "maxLoop": 100000,
      "columns": [{
        "header": "Amount determined by the formula<br><b>(H x I) - J</b><br>(using the columns on page 2)",
        "indicator": "K",
        "disabled": true
      },
        {
          "header": "Nova Scotia R&D tax credit earned by the transferee for the eligible expeditures that were transferred",
          "tn": "750","validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          "indicator": "L"
        },
        {
          "header": "Amount from column K or L, whichever is less",
          "indicator": "M",
          "totalnum": "755",
          "total": true,
          "totalMessage": "Subtotal",
          "totalIndicator": "N",
          "disabled": true
        }],
      "startTable": "300"
    }
  }
})();
