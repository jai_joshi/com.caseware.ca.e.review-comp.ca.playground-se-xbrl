(function() {

  wpw.tax.create.calcBlocks('t2s340', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('104', 'ratesNs', '100');
      //part 1
      calcUtils.multiply('120', ['103', '104'], (1 / 100));
      calcUtils.sumBucketValues('141', ['130', '140']);
      calcUtils.equals('142', '141');
      calcUtils.sumBucketValues('143', ['120', '142']);
      calcUtils.subtract('190', '143', '150');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 2
      field('200').getRows().forEach(function(row) {
        row[2].assign(Math.min(row[0].get(), row[1].get()));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('300').getRows().forEach(function(row) {
        if (row[0].get() > 100) {
          row[0].assign(100);
          row[0].disabled(false);
        }
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('400').getRows().forEach(function(row, rowIndex) {
        var repeatedTableRow = field('300').getRow(rowIndex);
        row[0].assign(
            repeatedTableRow[0].get() *
            repeatedTableRow[1].get() / 100 -
            repeatedTableRow[2].get()
        );
        row[2].assign(Math.min(row[0].get(), row[1].get()));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('800').assign(field('200').total(2).get());
      field('801').assign(field('400').total(2).get());
      calcUtils.equals('802', '760');
      calcUtils.sumBucketValues('803', ['800', '801', '802']);
    });
  });
})();
