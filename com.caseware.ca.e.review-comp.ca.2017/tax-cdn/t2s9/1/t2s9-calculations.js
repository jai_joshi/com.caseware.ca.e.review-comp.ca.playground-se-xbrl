(function() {

  wpw.tax.create.calcBlocks('t2s9', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field, form) {
      var table = field('050');
      var linkedTable = form('entityProfileSummary').field('050');
      table.getRows().forEach(function (row, rIndex) {
        row.forEach(function(cell, cIndex) {
          cell.assign(linkedTable.cell(rIndex, cIndex).get());
          cell.source(linkedTable.cell(rIndex, cIndex));
        });
      });
    });

  });
})();
