(function() {
  var countryAddressCodes = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.countryAddressCodes, 'value');

  wpw.tax.global.tableCalculations.t2s9 = {
    '050': {
      showNumbering: true,
      fixedRows: true,
      columns: [
        {
          header: 'Name',
          num: '100',
          tn: '100',
          maxLength: 175,
          disabled: true,
          type: 'text'
        },
        {
          header: 'Country of residence (other than Canada)',
          num: '200',
          colClass: 'std-input-width',
          tn: '200',
          type: 'dropdown',
          options: countryAddressCodes,
          disabled: true
        },
        {
          header: 'Business number (see note 1)',
          num: '300',
          colClass: 'std-input-col-width',
          tn: '300',
          disabled: true,
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          header: 'Relation - ship code (see note 2)',
          num: '400',
          colClass: 'small-input-width',
          tn: '400',
          disabled: true,
          type: 'dropdown',
          options: [
            {
              value: '1',
              option: '1- Parent'
            },
            {
              value: '2',
              option: '2- Subsidiary'
            },
            {
              value: '3',
              option: '3- Associated'
            },
            {
              value: '4',
              option: '4- Related but not associated'
            }
          ]
        },
        {
          header: 'Number of common shares you own',
          num: '500',
          colClass: 'std-input-width',
          tn: '500',
          disabled: true
        },
        {
          header: '% of com. shares you own',
          num: '550',
          colClass: 'small-input-width',
          tn: '550',
          disabled: true,
          "validate": {
            "and": [
              'percent',
              {
                "or": [
                  {"length": {"min": "1", "max": "6"}},
                  {"check": "isEmpty"}
                ]
              }
            ]
          },
          decimals: 3
        },
        {
          header: 'Number of preferred shares you own',
          num: '600',
          colClass: 'std-input-width',
          tn: '600',
          disabled: true
        },
        {
          header: '% of preferred shares you own',
          num: '650',
          colClass: 'small-input-width',
          tn: '650',
          disabled: true,
          "validate": {
            "and": [
              'percent',
              {
                "or": [
                  {"length": {"min": "1", "max": "6"}},
                  {"check": "isEmpty"}
                ]
              }
            ]
          },
          decimals: 3
        },
        {
          header: 'Book value of capital stock',
          num: '700',
          colClass: 'std-input-width',
          tn: '700',
          disabled: true
        }
      ]
    }
  }
})();
