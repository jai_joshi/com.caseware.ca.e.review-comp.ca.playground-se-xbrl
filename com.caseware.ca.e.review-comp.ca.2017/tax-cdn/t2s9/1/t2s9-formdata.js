(function() {
  'use strict';

  wpw.tax.global.formData.t2s9 = {
    formInfo: {
      abbreviation: 'T2S9',
      title: 'Related and Associated Corporations',
      //subTitle: '(2011 and later tax years)',
      schedule: 'Schedule 9',
      code: 'Code 1101',
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 9 (11)',
      showCorpInfo: true,
      description: [
        {
          type: 'list',
          items: [
            'Complete this schedule if the corporation is related to or associated with at least one other corporation.',
            'For more information, see the <i>T2 Corporation Income Tax Guide</i>.'
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            type: 'table', num: '050'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {label: ''},
          {
            label: 'Note 1: Enter \"NR\" if the corporation is not registered or does not have a business number.',
            labelClass: 'fullLength'
          },
          {
            label: 'Note 2: Enter the code number of the relationship that applies from the following' +
            ' order: 1- Parent       2- Subsidiary       3-Associated       4-Related but not associated',
            labelClass: 'fullLength'
          }
        ]
      }
    ]
  };
})();
