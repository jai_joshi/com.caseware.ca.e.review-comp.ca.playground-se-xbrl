(function() {
  wpw.tax.create.diagnostics('t2s9', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S9'), forEach.row('050', forEach.bnCheckCol(2, true))));

    diagUtils.diagnostic('0090001', common.prereq(common.check(['t2j.150'], 'isChecked'), common.requireFiled('T2S9')));

    diagUtils.diagnostic('0090002', {
      label: 'For each entry in column 009100, there must be corresponding entries in columns 009200 ' +
      'or 009300, and 009400.'
    }, common.prereq(common.requireFiled('T2S9'),
        function(tools) {
          var table = tools.field('050');
          return tools.checkAll(table.getRows(), function(row, index) {
            //Skip first row
            if (row[0].isFilled() && index)
              return tools.requireOne([row[1], row[2]], 'isNonZero') && row[3].require('isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0090002', {
      label: 'For each entry in column 009500 or 009550, there must be a corresponding entry in the other column.'
    }, common.prereq(common.requireFiled('T2S9'),
        function(tools) {
          var table = tools.field('050');
          return tools.checkAll(table.getRows(), function(row, index) {
            //Skip first row
            if (tools.checkMethod([row[4], row[5]], 'isNonZero') && index)
              return tools.requireAll([row[4], row[5]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0090002', {
      label: 'For each entry in column 009600 or 009650, there must be a corresponding entry in the other column.'
    }, common.prereq(common.requireFiled('T2S9'),
        function(tools) {
          var table = tools.field('050');
          return tools.checkAll(table.getRows(), function(row, index) {
            //Skip first row
            if (tools.checkMethod([row[6], row[7]], 'isNonZero') && index)
              return tools.requireAll([row[6], row[7]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0090002', {
      label: 'For each entry in column 009500, 009550, 009600 or 009650, there must be a corresponding ' +
      'entry in column 009700.'
    }, common.prereq(common.requireFiled('T2S9'),
        function(tools) {
          var table = tools.field('050');
          return tools.checkAll(table.getRows(), function(row, index) {
            //Skip first row
            if (tools.checkMethod([row[4], row[5], row[6], row[7]], 'isNonZero') && index)
              return row[8].require('isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0090002', {
      label: 'For each entry in column 009500, 009550, 009600, 009650 or 009700, there must be ' +
      'corresponding entry in column 009100.'
    }, common.prereq(common.requireFiled('T2S9'),
        function(tools) {
          var table = tools.field('050');
          return tools.checkAll(table.getRows(), function(row, index) {
            //Skip first row
            if (tools.checkMethod([row[4], row[5], row[6], row[7], row[8]], 'isNonZero') && index)
              return row[0].require('isNonZero');
            else return true;
          });
        }));
  });
})();
