(function() {
  var columnSpaceWidth = '15px';
  var formFieldWidth = '107px';

  wpw.tax.create.tables('t2s508', {
    '2000': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'type': 'none'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'small-input-width'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Repayments for tax years that end before June 1, 2016'
          },
          '1': {
            'tn': '210'
          },
          '2': {
            'num': '210', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          },
          '3': {
            label: 'x'
          },
          '4': {
            num: '214',
            decimals: 1
          },
          '5': {
            label: '%='
          },
          '6': {
            type: 'none'
          },
          '7': {
            tn: '215',
            num: '215', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            cellClass: 'doubleUnderline'
          },
          '8': {
            'label': 'H'
          }
        }
      ]
    },
    '2200': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {type: 'none'},
        {
          width: '10px',
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          width: '50px'
        },
        {
          width: '50px',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          width: '50px',
          type: 'none'
        },
        {
          type: 'none',
          width: '250px'
        }
      ],
      cells: [
        {
          '0': {
            label: 'Number of days in the tax year before June 1, 2016',
            cellClass: 'alignCenter singleUnderline'
          },
          '2': {
            tn: '240'
          },
          '3': {
            num: '240', "validate": {"or":[{"matches":"^-?[.\\d]{1,3}$"},{"check":"isEmpty"}]}
          },
          '4': {
            label: 'x'
          },
          '5': {
            num: '2401',
            decimals: 1
          },
          '6': {
            label: '%='
          },
          '7': {
            num: '2405'
          },
          '8': {
            label: '%1'
          }
        },
        {
          '0': {
            label: 'Number of days in the tax year',
            labelClass: 'center'
          },
          '2': {
            tn: '241'
          },
          '3': {
            num: '241', "validate": {"or":[{"matches":"^-?[.\\d]{1,3}$"},{"check":"isEmpty"}]}
          },
          '5': {
            type: 'none'
          },
          '7': {
            type: 'none'
          }
        },
        {
          '0': {
            label: 'Number of days in the tax year after May 31, 2016 ',
            cellClass: 'alignCenter singleUnderline'
          },
          '2': {
            tn: '242'
          },
          '3': {
            num: '242', "validate": {"or":[{"matches":"^-?[.\\d]{1,3}$"},{"check":"isEmpty"}]}
          },
          '4': {
            label: 'x'
          },
          '5': {
            num: '2402',
            decimals: 1
          },
          '6': {
            label: '%='
          },
          '7': {
            num: '2406'
          },
          '8': {
            label: '%2'
          }
        },
        {
          '0': {
            label: 'Number of days in the tax year',
            labelClass: 'center'
          },
          '2': {
            tn: '243'
          },
          '3': {
            num: '243', "validate": {"or":[{"matches":"^-?[.\\d]{1,3}$"},{"check":"isEmpty"}]}
          },
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '0': {label: ' '},
          '3': {type: 'none'},
          '4': {type: 'none'},
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '0': {
            label: 'Subtotal (percentage 1 <b>plus</b> percentage 2)',
            cellClass: 'alignRight'
          },
          '3': {type: 'none'},
          '4': {type: 'none'},
          '5': {type: 'none'},
          '7': {num: '2413'},
          '8': {
            label: '%3'
          }
        }
      ]
    },
    '250': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'type': 'none'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'small-input-width'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Repayment for tax years that end on or after June 1, 2016 and include May 31, 2016'
          },
          '1': {
            'tn': '211'
          },
          '2': {
            'num': '211', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          },
          '3': {
            label: 'x'
          },
          '4': {
            num: '2111',
            decimals: 4
          },
          '5': {
            label: '%='
          },
          '6': {
            type: 'none'
          },
          '7': {
            tn: '216',
            num: '216', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            cellClass: 'doubleUnderline'
          },
          '8': {
            'label': 'I'
          }
        },
        {
          '0': {
            'label': 'Repayments for tax years that start after May 31, 2016'
          },
          '1': {
            'tn': '212'
          },
          '2': {
            'num': '212', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          },
          '3': {
            label: 'x'
          },
          '4': {
            num: '213',
            decimals: 1
          },
          '5': {
            label: '%='
          },
          '6': {
            type: 'none'
          },
          '7': {
            tn: '217',
            num: '217', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            cellClass: 'doubleUnderline'
          },
          '8': {
            'label': 'J'
          }
        }
      ]
    },
    '260': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'type': 'none'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'small-input-width'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'small-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Repayment made in the tax year of government or non-government assistance or a contract payment' +
            ' that reduced an eligible expenditure for first term or second term shared-use ' +
            'equipment acquired before 2014'
          },
          '1': {
            'tn': '220'
          },
          '2': {
            'num': '220', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          },
          '3': {
            label: 'x'
          },
          '4': {
            num: '2201',
            decimals: 2
          },
          '5': {
            label: '='
          },
          '6': {
            num: '221'
          },
          '7': {
            type: 'none',
            label: 'x'
          },
          '8': {
            num: '222',
            decimals: 1
          },
          '9': {
            label: '%='
          },
          '10': {
            tn: '225',
            num: '225', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          },
          '11': {
            'label': 'K'
          }
        }
      ]
    },
    '2300': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'type': 'none'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'small-input-width'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Ontario SR&ED expenditure pool (amount G in Part 1)'
          },
          '2': {
            'num': '118'
          },
          '3': {
            label: 'x'
          },
          '4': {
            num: '119',
            decimals: 1
          },
          '5': {
            label: '%='
          },
          '6': {
            type: 'none'
          },
          '7': {
            tn: '200',
            num: '200', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            cellClass: 'doubleUnderline'
          },
          '8': {
            'label': 'M'
          }
        }
      ]
    },
    '2400': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {type: 'none'},
        {
          width: '10px',
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          width: '50px'
        },
        {
          width: '50px',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          width: '50px',
          type: 'none'
        },
        {
          type: 'none',
          width: '250px'
        }
      ],
      cells: [
        {
          '0': {
            label: 'Number of days in the tax year before June 1, 2016',
            cellClass: 'alignCenter singleUnderline'
          },
          '3': {
            num: '2407'
          },
          '4': {
            label: 'x'
          },
          '5': {
            num: '2403',
            decimals: 1
          },
          '6': {
            label: '%='
          },
          '7': {
            num: '2408'
          },
          '8': {
            label: '%  4'
          }
        },
        {
          '0': {
            label: 'Number of days in the tax year',
            labelClass: 'center'
          },
          '3': {
            num: '2409'
          },
          '5': {
            type: 'none'
          },
          '7': {
            type: 'none'
          }
        },
        {
          '0': {
            label: 'Number of days in the tax year after May 31, 2016 ',
            cellClass: 'alignCenter singleUnderline'
          },
          '3': {
            num: '2410'
          },
          '4': {
            label: 'x'
          },
          '5': {
            num: '2404',
            decimals: 1
          },
          '7': {
            num: '2411'
          },
          '6': {
            label: '%='
          },
          '8': {
            label: '%  5'
          }
        },
        {
          '0': {
            label: 'Number of days in the tax year',
            labelClass: 'center'
          },
          '3': {
            num: '2412'
          },
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '0': {label: ' '},
          '3': {type: 'none'},
          '4': {type: 'none'},
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '0': {
            label: 'Subtotal (percentage 4 <b>plus</b> percentage 5)'
          },
          '3': {type: 'none'},
          '4': {type: 'none'},
          '5': {type: 'none'},
          '7': { num: '2414'},
          '8': {
            label: '%  6'
          }
        }
      ]
    },
    '2500': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'type': 'none'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'small-input-width'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Ontario SR&ED expenditure pool (amount G in Part 1)'
          },
          '2': {
            'num': '2501'
          },
          '3': {
            label: 'x'
          },
          '4': {
            num: '2502',
            decimals: 4
          },
          '5': {
            label: '%='
          },
          '6': {
            tn: '201'
          },
          '7': {
            num: '201', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            cellClass: 'doubleUnderline'
          },
          '8': {
            'label': 'Q'
          }
        }
      ]
    },
    '2600': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'type': 'none'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'small-input-width'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          'formField': true,
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Ontario SR&ED expenditure pool (amount G in Part 1)'
          },
          '2': {
            'num': '2601'
          },
          '3': {
            label: 'x'
          },
          '4': {
            num: '2602',
            decimals: 1
          },
          '5': {
            label: '%='
          },
          '6': {
            tn: '202'
          },
          '7': {
            num: '202', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            cellClass: 'doubleUnderline'
          },
          '8': {
            'label': 'U'
          }
        }
      ]
    },
    '1000': {
      'fixedRows': true,
      'infoTable': true,
      hasTotals: true,
      'columns': [
        {
          'width': '190px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'formField': true,
          total: true,
          totalNum: '904',
          totalMessage: 'Total (enter amount on line HH in Part 4)'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        }],
      'cells': [
        {
          '0': {
            'label': '1st preceding taxation year'
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '901'
          },
          '5': {
            'num': '901', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '2nd preceding taxation year'
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '902'
          },
          '5': {
            'num': '902', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '3rd preceding taxation year'
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '903'
          },
          '5': {
            'num': '903', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }]
    },
    '400': {
      'fixedRows': true,
      'infoTable': true,
      hasTotals: true,
      'columns': [
        {
          type: 'none'
        },
        {
          header: 'Year of origin <br>(earliest year first)',
          'width': '160px',
          'type': 'date',
          'disabled': true
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Credit available',
          colClass: 'std-input-width',
          totalNum: '451',
          disabled: true,
          total: true,
          totalMessage: ' '
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      'cells': [
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {}
      ]
    },
    '450': {
      'fixedRows': true,
      'infoTable': true,
      hasTotals: true,
      'columns': [
        {
          type: 'none'
        },
        {
          header: 'Year of origin <br> (earliest year first)',
          'width': '160px',
          'type': 'date',
          'disabled': true
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Credit available',
          colClass: 'std-input-width',
          totalNum: '452',
          disabled: true,
          total: true,
          totalMessage: 'Total (equals line 325 in Part 4)'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      'cells': [
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {
          '0': {
            label: 'Current year'
          }
        }
      ]
    },
    '690': {
      hasTotals: true,
      keepButtonsSpace: true,
      columns: [
        {
          header: 'KK <br> Amount of federal ITC you originally calculated for the property you acquired, or the ' +
          'original user\'s federal ITC where you acquired the property from a non-arm\'s length party, as ' +
          'described in the note above',
          tn: '700'
        },
        {
          header: 'LL <br> Amount calculated using the federal ITC rate at the date of acquisition' +
          ' (or the original user\'s date of acquisition) on either the proceeds of disposition (if sold in an arm\'s ' +
          'length transaction) or the fair market value of the property (in any other case)',
          tn: '710'
        },
        {
          header: 'MM <br> Amount from column 700 or 710, <br> whichever is less',
          total: true,
          totalNum: '712',
          totalMessage: 'Subtotal (enter amount NN, on line WW in Part 8 on page 5)',
          totalIndicator: 'NN'
        }
      ]
    },
    '770': {
      repeats: ['780'],
      columns: [
        {
          header: 'OO <br> The rate percentage that the transferee used to determine its federal ITC for a qualified' +
          ' expenditure that was transferred under an agreement under subsection 127(13) of the federal Act',
          tn: '720',
          decimals: 3,
          filters: 'decimals 3'
        },
        {
          header: 'PP <br> The proceeds of disposition of the property if you dispose of it to a person at arm\'s ' +
          'length; or, in any other case, the fair market value of the property at conversion or disposition',
          tn: '730'
        },
        {
          header: 'QQ <br> The amount, if any, already provided for in Calculation 1 (this allows for the situation ' +
          'where only part of the cost of a property is transferred for an agreement under ' +
          'subsection 127(13) of the federal Act)',
          tn: '740'
        }
      ]
    },
    '780': {
      fixedRows: true,
      hasTotals: true,
      keepButtonsSpace: true,
      columns: [
        {
          header: 'RR <br> Amount determined by the formula <br> (OO x PP) - QQ <br> (using the columns above)'
        },
        {
          header: 'SS <br> The federal ITC earned by the transferee for the qualified expenditure that was transferred',
          tn: '750'
        },
        {
          header: 'TT <br> Amount from column RR or SS, whichever is less',
          total: true,
          totalMessage: 'Subtotal (enter amount UU on line XX below)',
          totalNum: '751',
          totalIndicator: 'UU'
        }
      ]
    },
    '810': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'type': 'none'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'small-input-width'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Amount WW <b>plus</b> amount XX'
          },
          '2': {
            'num': '811'
          },
          '3': {
            label: 'x'
          },
          '4': {
            num: '812',
            decimals: 2
          },
          '5': {
            label: '%='
          },
          '6': {
            type: 'none'
          },
          '7': {
            num: '813'
          },
          '8': {
            'label': 'YY'
          }
        }
      ]
    },
    '1600': {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {colClass: 'std-spacing-width', type: 'none'},
        {
          type: 'date',
          disabled: true,
          header: '<b>Year of origin</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '1601',
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '1602',
          totalMessage: ' ',
          header: '<b>Current year contribution</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '1603',
          totalMessage: ' ',
          header: '<b>Transfer amount</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', totalNum: '504', disabled: true,
          header: '<b>Amount available to apply</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '505',
          header: '<b>Applied<b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '506',
          header: '<b>Balance to carry forward</b>'
        },
        {colClass: 'std-padding-width', type: 'none'}
      ],
      cells: [
        {
          '4': {label: '*'},
          '5': {type: 'none'},
          '7': {type: 'none'},
          '9': {type: 'none'},
          '11': {type: 'none'},
          '13': {type: 'none', label: 'N/A'}
        },
        {
          '5': {type: 'none'},
          '14': {label: '**'}
        },
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {
          '3': {type: 'none'},
          '7': {type: 'none'}
        }
      ]
    },
    '3000': {
      infoTable: true,
      fixedRows: true,
      hasTotals: true,
      columns: [
        {type: 'none'},
        {
          colClass: 'std-input-width', inputClass: 'noBorder',  header: 'Current Expenditures',
          total: true, totalMessage: 'Subtotal', totalNum: '3100'
        },//field
        {type: 'none', colClass: 'std-padding-width'},//space
        {
          colClass: 'std-input-width', inputClass: 'noBorder',  header: 'Capital Expenditures',
          total: true, totalMessage: ' ', totalNum: '3101'
        },//field
        {type: 'none', colClass: 'std-padding-width'}//end
      ],
      cells: [
        {
          0: {label: '<b>Total expenditures for SR&ED</b>'}
        },
        {
          0: {label: 'Add:', labelClass: 'fullLength bold'},
          1: {type: 'none'},
          3: {type: 'none'}
        },
        {
          0: {
            label: '• payment of prior years\' unpaid amounts (other than salary or wages)',
            labelClass: 'tabbed'
          },
          3: {type: 'none'}
        },
        {
          0: {
            label: '• prescribed proxy amount',
            labelClass: 'tabbed'
          },
          3: {type: 'none'}
        },
        {
          0: {
            label: '• expenditures on shared-use equipment for property acquired before 2014',
            labelClass: 'tabbed'
          },
          1: {type: 'none'}
        },
        {
          0: {
            label: '• qualified expenditures transferred to you',
            labelClass: 'tabbed'
          }
        }
      ]
    },
    '3001': {
      infoTable: true,
      fixedRows: true,
      hasTotals: true,
      columns: [
        {type: 'none'},
        {
          colClass: 'std-input-width', inputClass: 'noBorder',
          total: true, totalMessage: 'Total Deductions', totalNum: '3102'
        },//field
        {type: 'none', colClass: 'std-padding-width'},//space
        {
          colClass: 'std-input-width', inputClass: 'noBorder',
          total: true, totalMessage: ' ', totalNum: '3103'
        },//field
        {type: 'none', colClass: 'std-padding-width'}//end
      ],
      cells: [
        {
          0: {
            label: '• other government assistance',
            labelClass: 'tabbed'
          }
        },
        {
          0: {
            label: '• non-government assistance and contract payments',
            labelClass: 'tabbed'
          }
        },
        {
          0: {
            label: '• current expenditures (other than salary or wages) not paid within ' +
            '180 days of the tax year-end (see note 5)',
            labelClass: 'tabbed'
          },
          3: {type: 'none'}
        },
        {
          0: {
            label: '• amounts paid in respect of an SR&ED contract to a person or partnership ' +
            'that is not a taxable supplier',
            labelClass: 'tabbed'
          },
          8: {type: 'none'}
        },
        {
          0: {
            label: '• 20% of for SR&ED performed on your behalf',
            labelClass: 'tabbed'
          },
          3: {type: 'none'}
        },
        {
          0: {
            label: '• prescribed expenditures not allowed by regulations (see guide)',
            labelClass: 'tabbed'
          }
        },
        {
          0: {
            label: '• Other deductions',
            labelClass: 'tabbed'
          }
        },
        {
          0: {label: '• non-arm\'s length transactions', labelClass: 'tabbed'},
          1: {type: 'none'},
          3: {type: 'none'}
        },
        {
          0: {
            label: '- assistance allocated to you (complete Form T1145*)',
            labelClass: 'tabbed2'
          }
        },
        {
          0: {
            label: '-  expenditures for non-arm\'s length SR&ED contracts',
            labelClass: 'tabbed2'
          },
          3: {type: 'none'}
        },
        {
          0: {
            label: '-  adjustments to purchases (limited to costs) of goods and services from ' +
            'non-arm\'s length suppliers',
            labelClass: 'tabbed2'
          }
        },
        {
          0: {
            label: '-  qualified expenditures you transferred (complete Form T1146**)',
            labelClass: 'tabbed2'
          }
        }
      ]
    },
    '3002': {
      infoTable: true,
      fixedRows: true,
      hasTotals: true,
      columns: [
        {type: 'none'},
        {colClass: 'std-input-width', inputClass: 'noBorder', },//field
        {type: 'none', colClass: 'std-padding-width'},//space
        {colClass: 'std-input-width', inputClass: 'noBorder', },
        {type: 'none', colClass: 'std-padding-width'}//end
      ],
      cells: [
        {
          0: {label: 'Subtotal', labelClass: 'bold'},
          1: {num: '3003'},
          3: {num: '3004'}
        },
        {
          0: {label: '<b>Ontario qualified expenditures </b><br> Enter the amount on line 100', labelClass: ''},
          1: {type: 'none'},
          3: {num: '3005'}
        }
      ]
    }

  });
})();
