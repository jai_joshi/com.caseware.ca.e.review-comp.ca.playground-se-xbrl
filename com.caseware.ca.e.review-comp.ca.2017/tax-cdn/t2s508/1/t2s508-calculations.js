(function() {
  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  function calculateDaysDiffBeforeJune(calcUtils, numerator, denominator, rate, calculatedField) {
    var field = calcUtils.field;
    var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();
    var taxStartDate = field('CP.tax_start').get();

    if (taxStartDate) {
      field(numerator).assign(wpw.tax.actions.calculateDaysDifference(taxStartDate, wpw.tax.date(2016, 5, 31)));
      field(denominator).assign(daysFiscalPeriod);
      if (field(numerator).get() >= field(denominator).get()) {
        calcUtils.equals(numerator, denominator, true)
      }
    }
    else {
      field(numerator).assign(0);
    }
    field(calculatedField).assign((field(numerator).get() / field(denominator).get()) * field(rate).get())
  }

  function calculateDaysDiffAfterMay(calcUtils, numerator, denominator, rate, calculatedField) {
    var field = calcUtils.field;
    var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();
    var taxStartDate = field('CP.tax_start').get();
    var taxEndDate = field('CP.tax_end').get();
    var date = wpw.tax.date(2016, 6, 1);

    if (taxStartDate) {
      field(numerator).assign(wpw.tax.actions.calculateDaysDifference(date, taxEndDate));
      field(denominator).assign(daysFiscalPeriod);
      if (field(numerator).get() >= field(denominator).get()) {
        calcUtils.equals(numerator, denominator, true)
      }
    }
    else {
      field(numerator).assign(0);
    }
    field(calculatedField).assign(field(numerator).get() / field(denominator).get() * field(rate).get())
  }

  wpw.tax.create.calcBlocks('t2s508', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      // to bring years of origin based on tax year end
      var tableArray = [1000, 400, 450, 1600];
      var taxationYearTable = field('tyh.200');
      tableArray.forEach(function(num) {
        field(num).getRows().forEach(function(row, rowIndex) {
          if (num == 1000) {
            var dateCol = Math.abs(rowIndex - 19);
            row[1].assign(taxationYearTable.cell(dateCol, 6).get());
          }
          else if (num == 400) {
            row[1].assign(taxationYearTable.getRow(rowIndex)[6].get())
          }
          else if (num == 450) {
            row[1].assign(taxationYearTable.getRow(rowIndex + 10)[6].get())
          }
          else {
            row[1].assign(taxationYearTable.getRow(rowIndex)[6].get())
          }
        })
      });
    });
    calcUtils.calc(function(calcUtils, field, form) {
      //to update ratesOn
      //part 2
      field('214').assign(field('ratesOn.492').get());
      field('214').source(field('ratesOn.492'));
      field('2401').assign(field('ratesOn.492').get());
      field('2401').source(field('ratesOn.492'));
      field('2402').assign(field('ratesOn.4921').get());
      field('2402').source(field('ratesOn.4921'));
      field('213').assign(field('ratesOn.4921').get());
      field('213').source(field('ratesOn.4921'));
      field('2201').assign(field('ratesOn.493').get());
      field('2201').source(field('ratesOn.493'));
      field('222').assign(field('ratesOn.492').get());
      field('222').source(field('ratesOn.492'));
      field('119').assign(field('ratesOn.492').get());
      field('119').source(field('ratesOn.492'));
      field('2403').assign(field('ratesOn.492').get());
      field('2403').source(field('ratesOn.492'));
      field('2404').assign(field('ratesOn.4921').get());
      field('2404').source(field('ratesOn.4921'));
      field('2602').assign(field('ratesOn.4921').get());
      field('2602').source(field('ratesOn.4921'));
      //part 8
      field('812').assign(field('ratesOn.494').get());
      field('812').source(field('ratesOn.494'));

      //part 1
      field('100').assign(field('3005').get());
      field('106').assign(Math.max(field('100').get() - field('105').get(), 0));
      field('111').assign(field('106').get() + field('110').get());
      field('112').assign(field('111').get());
      field('120').assign(Math.max(field('112').get() - field('115').get(), 0));
      //part2
      field('215').assign(field('210').get() * field('214').get() / 100);

      if (field('211').get() > 0) {
        calculateDaysDiffBeforeJune(calcUtils, '240', '241', '2401', '2405');
        calculateDaysDiffAfterMay(calcUtils, '242', '243', '2402', '2406');
      } else {
        calcUtils.removeValue(['240', '241', '242', '243', '2405', '2406']);
      }
      field('2413').assign(field('2405').get() + field('2406').get());
      field('2111').assign(field('2413').get());
      field('216').assign(field('211').get() * field('2111').get() / 100);
      field('217').assign(field('212').get() * field('213').get() / 100);
      field('221').assign(field('220').get() * field('2201').get());
      field('225').assign(field('221').get() * field('222').get() / 100);
      calcUtils.sumBucketValues('229', ['215', '216', '217', '225'])
    });
    calcUtils.calc(function(calcUtils, field) {
      //part3
      field('118').assign(field('120').get());
      //check if tax year end before June 1, 2016
      var taxStart = field('CP.tax_start').get();
      var taxEnd = field('CP.tax_end').get();
      var date = wpw.tax.date(2016, 6, 1);
      var isBeforeJune2016 = calcUtils.dateCompare.lessThan(taxEnd, date);
      var isStartAfterJune2016 = calcUtils.dateCompare.lessThan(date, taxStart);
      field('200').assign(isBeforeJune2016 ? field('118').get() * field('119').get() / 100 : 0);

      field('2071').assign(isBeforeJune2016 ? field('229').get() : 0);
      if (isBeforeJune2016) {
        calcUtils.sumBucketValues('230', ['200', '205', '2071'])
      }
      else {
        field('230').assign(0)
      }
      calculateDaysDiffBeforeJune(calcUtils, '2407', '2409', '2403', '2408');
      calculateDaysDiffAfterMay(calcUtils, '2410', '2412', '2404', '2411');
      field('2414').assign(field('2408').get() + field('2411').get());
      field('2501').assign(isBeforeJune2016 ? 0 : field('120').get());
      field('2502').assign(field('2414').get());
      field('201').assign(field('2501').get() * field('2502').get() / 100);
      field('208').assign(field('229').get());
      //For tax years that end on or after June 1, 2016 and include May 31, 2016
      field('231').assign(isBeforeJune2016 ? 0 : field('201').get() + field('206').get() + field('208').get());
      field('2601').assign(isStartAfterJune2016 ? field('120').get() : 0);
      field('202').assign(field('2601').get() * field('2602').get() / 100);
      field('209').assign(isStartAfterJune2016 ? field('229').get() : 0);
      field('232').assign(isStartAfterJune2016 ? field('202').get() + field('207').get() * field('209').get() : 0);
    });
    calcUtils.calc(function(calcUtils, field) {
      //part4
      field('299').assign(field('1601').get());
      field('300').assign(field('1600').cell(0, 3).get());
      field('305').assign(field('299').get() - field('300').get());
      calcUtils.getApplicableValue('3051', ['230', '231', '232']);
      if (field('315').get() == 1) {
        field('320').disabled(false)
      }
      else {
        field('320').assign(0);
        field('320').disabled(true)
      }
      field('321').assign(Math.max(field('3051').get() - field('320').get(), 0));
      field('322').assign(field('321').get());
      calcUtils.sumBucketValues('323', ['305', '310', '322']);
      field('324').assign(field('323').get());
      field('3251').assign(Math.min(field('324').get() - field('1000').total(5).get(), field('T2S5.497').get()));
      field('326').assign(field('904').get());
      field('327').assign(Math.max(field('3251').get() + field('326').get(), 0));
      field('328').assign(field('327').get());
      field('325').assign(Math.max(field('324').get() - field('328').get(), 0));
    });
    calcUtils.calc(function(calcUtils, field) {
      //part 6
      var summaryTable = field('1600');
      field('400').getRows().forEach(function(row, rowIndex) {
        row[3].assign(summaryTable.getRow(rowIndex)[13].get());
      });
      field('450').getRows().forEach(function(row, rowIndex) {
        var table450rIndex = rowIndex + 10;
        row[3].assign(summaryTable.getRow(table450rIndex)[13].get());
      });
    });
    calcUtils.calc(function(calcUtils, field) {
      //part 7
      //calculation 1
      var recapture = field('705').get() && field('706').get() && field('707').get() && field('708').get();

      field('690').getRows().forEach(function(row) {
        if (recapture) {
          row[0].disabled(false);
          row[1].disabled(false);
          row[2].disabled(false);
          row[2].assign(Math.min(row[0].get(), row[1].get()))
        }
        else {
          row[0].disabled(true);
          row[1].disabled(true);
          row[2].disabled(true);
          row[0].assign();
          row[1].assign();
          row[2].assign();
        }
      });
      field('780').getRows().forEach(function(row, rowIndex) {
        var table770Row = field('770').getRow(rowIndex);
        row[0].assign((table770Row[0].get() * table770Row[1].get()) - table770Row[2].get());
        row[2].assign(Math.min(row[0].get(), row[1].get()))
      })
    });
    calcUtils.calc(function(calcUtils, field) {
      //part 8
      field('800').assign(field('690').total(2).get());
      field('801').assign(field('770').total(2).get());
      field('811').assign(field('800').get() + field('801').get());
      field('813').assign(field('811').get() * field('812').get() / 100);
      field('815').assign(field('760').get());
      field('816').assign(field('813').get() + field('815').get())
    });
    calcUtils.calc(function(calcUtils, field) {
      //ON qualified expenditure
      var t661Table = field('T661.1011');
      var tableArray = [3000, 3001];
      tableArray.forEach(function(tableNum) {
        field(tableNum).getRows().forEach(function(row, rIndex) {
          if (tableNum == 3001) {
            row[1].assign(t661Table.getRow(rIndex + 9)[4].get());
            row[3].assign(t661Table.getRow(rIndex + 9)[8].get());
          }
          else {
            row[1].assign(t661Table.getRow(rIndex)[4].get());
            row[3].assign(t661Table.getRow(rIndex)[8].get());
          }
        })
      });
      field('3003').assign(Math.max(0,
          field('3000').total(1).get() -
          field('3001').total(1).get()
      ));
      field('3004').assign(Math.max(0, field('3000').total(3).get() - field('3001').total(3).get()));
      field('3005').assign(field('3003').get() + field('3004').get())
    });
    calcUtils.calc(function(calcUtils, field) {
      //historical data chart
      var summaryTable = field('1600');
      var limitOnCredit = field('327').get();

      summaryTable.getRows().forEach(function(row, rowIndex) {
        if (rowIndex == 0) {
          // to avoid the first row being calculated to total
          row[7].assign(0);
          row[9].assign(0);
          row[11].assign(0);
          row[13].assign(0);
        } else {
          row[9].assign(Math.min(row[3].get() + row[5].get() - row[7].get()), 0);
          row[11].assign(returnAmountApplied(limitOnCredit, row[9].get()));
          row[13].assign(row[9].get() - row[11].get());
          limitOnCredit -= row[11].get();
        }
      });
      summaryTable.cell(20, 5).assign(field('3051').get())
    });

  });
})();
