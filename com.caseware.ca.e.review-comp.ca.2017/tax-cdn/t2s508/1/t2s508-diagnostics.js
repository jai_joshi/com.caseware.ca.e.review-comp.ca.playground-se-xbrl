(function() {
  wpw.tax.create.diagnostics('t2s508', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('5080005', common.prereq(common.check('t2s5.416', 'isNonZero'), common.requireFiled('T2S508')));

    diagUtils.diagnostic('5080010', common.prereq(common.and(
        common.requireFiled('T2S508'),
        common.check('t2s5.277', 'isNonZero')),
        function(tools) {
          var table = tools.mergeTables(tools.field('690'), tools.field('770'));
          table = tools.mergeTables(table, tools.field('780'));
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne([row[0], row[1], row[3], row[4], row[5], row[7], tools.field('760')], 'isNonZero');
          });
        }));

    diagUtils.diagnostic('5080015', common.prereq(common.and(
        common.requireFiled('T2S508'),
        common.check(['200', '201', '202'], 'isNonZero')),
        common.check(['100', '110'], 'isNonZero')));

    diagUtils.diagnostic('5080020', common.prereq(common.and(
        common.requireFiled('T2S508'),
        common.check('215', 'isNonZero')),
        common.check('210', 'isNonZero')));

    diagUtils.diagnostic('5080021', common.prereq(common.and(
        common.requireFiled('T2S508'),
        common.check('216', 'isNonZero')),
        common.check('211', 'isNonZero')));

    diagUtils.diagnostic('5080022', common.prereq(common.and(
        common.requireFiled('T2S508'),
        common.check('217', 'isNonZero')),
        common.check('212', 'isNonZer0')));

    diagUtils.diagnostic('5080025', common.prereq(common.and(
        common.requireFiled('T2S508'),
        common.check('225', 'isNonZero')),
        common.check('220', 'isNonZero')));

    diagUtils.diagnostic('5080030', common.prereq(common.and(
        common.requireFiled('T2S508'),
        common.check('315', 'isYes')),
        common.check('320', 'isNonZero')));

    diagUtils.diagnostic('5080035', common.prereq(common.and(
        common.requireFiled('T2S508'),
        common.check(['900', '901', '902'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['230', '231', '232']), function() {
            return this.get() - tools.field('320').get() > 0;
          });
        }));

    diagUtils.diagnostic('percentTotal', common.prereq(common.and(
        common.requireFiled('T2S508'),
        function(tools) {
          return tools.field('770').getRow(0)[0].isNonZero();
        }), function(tools) {
      var table = tools.field('770');
      return tools.checkAll(table.getRows(), function(row) {
        return row[0].require(function() {
          return this.get() <= 100;
        })
      })
    }));
  });
})();
