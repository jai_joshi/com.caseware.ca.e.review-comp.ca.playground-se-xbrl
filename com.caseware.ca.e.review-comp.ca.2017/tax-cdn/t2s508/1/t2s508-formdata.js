(function() {
  wpw.tax.create.formData('t2s508', {
    formInfo: {
      abbreviation: 't2s508',
      title: 'Ontario Research and Development Tax Credit',
      //TODO: DO NOT DELETE THIS LINE: code
      schedule: 'Schedule 508',
      code: 'Code 1601',
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 508 E (16)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule to:',
              sublist: [
                'calculate an Ontario research and development tax credit (ORDTC);',
                'claim an ORDTC earned in the tax year or carried forward from any of the 20 previous tax years' +
                ' that are a tax year-ending after December 31, 2008, to reduce Ontario corporate income tax ' +
                'payable in the current tax year;',
                'carry back an ORDTC to reduce Ontario corporate income tax payable in any of the' +
                ' three previous tax years;',
                'add an ORDTC that was allocated to the corporation by a partnership of which it was a member;',
                'transfer an ORDTC after an amalgamation or windup; or',
                'calculate a recapture of the ORDTC'
              ]
            },
            {
              label: 'The ORDTC is a non-refundable tax credit on eligible expenditures incurred by a corporation ' +
              'in a tax year. The ORDTC rate is: ',
              sublist: [
                '4.5% for tax years that end before June 1, 2016',
                '3.5% for tax years that start after May 31, 2016',
                'prorated for tax years that end on or after June 1, 2016 and include May 31, 2016'
              ]
            },
            {
              label: 'An eligible expenditure is an expenditure for a permanent establishment in Ontario' +
              ' of a corporation, that is a qualified expenditure for the purposes of section 127 of the federal ' +
              '<i>Income Tax Act</i> for scientific research and experimental development (SR&ED) carried on in Ontario'
            },
            {
              label: 'Only corporations that are not exempt from Ontario corporate income tax and' +
              ' none of whose income is exempt income can claim the ORDTC'
            },
            {
              label: 'Attach a completed copy of this schedule to the <i>T2 Corporation Income Tax Return</i>'
            },
            {
              label: 'To claim this credit, you must also send in completed copies of the Form T661, ' +
              '<i>Scientific Research and Experimental Development Expenditures</i>, and the Schedule 31, ' +
              '<i>Investment Tax Credit - Corporations</i>, within 18 months of the tax year-end'
            }
          ]
        }
      ],
      category: 'Ontario Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Ontario SR&ED expenditure pool',
        'rows': [
          {
            'label': 'Total eligible expenditures incurred by the corporation in Ontario in the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '100',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'A'
                }
              }
            ]
          },
          {
            'label': '<b>Deduct</b>: Government assistance, non-government assistance, or a contract payment for eligible expenditures',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Net eligible expenditures for the tax year (amount A <b>minus</b> amount B) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '106'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': '<b>Add</b>: Eligible expenditures transferred to the corporation by another corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount C <b>plus</b> amount D)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '111'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '112'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': '<b>Deduct</b>: Eligible expenditures the corporation transferred to another corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '115',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '115'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': '<b>Ontario SR&ED expenditure pool</b> (amount E <b>minus</b> amount F) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '120',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              }
            ],
            'indicator': 'G'
          }
        ]
      },
      {
        'header': 'Part 2 - Eligible repayments',
        'rows': [
          {
            'label': 'The repayment of the ORDTC is calculated using the ORDTC rate that you used to determine your tax credit at the time your qualified expenditures were reduced because of the government or non-government assistance, or contract payments. Enter the amount of the repayment on the line that corresponds to the appropriate rate',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Repayment for tax years that end on or after June 1, 2016 and include May 31, 2016. Complete the proration calculation below',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2200'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '250'
          },
          {
            'type': 'table',
            'num': '260'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Eligible repayments</b> (Total amount H to amount K)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '229',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '229'
                }
              }
            ],
            'indicator': 'L'
          }
        ]
      },
      {
        'header': 'Part 3 - Calculation of the current part of the ORDTC',
        'rows': [
          {
            'label': 'For tax years that end before June 1, 2016',
            'labelClass': 'bold fullLength'
          },
          {
            'type': 'table',
            'num': '2300'
          },
          {
            'label': 'ORDTC allocated to a corporation by a partnership of which it is a member (other than a specified member) <br>for a fiscal period that ends in the corporation\'s tax year * ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '205',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '205'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'label': '* If there is a disposal or change of use of eligible property, see Part 7 on page 4',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'Eligible repayments (from amount L in Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '2071'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'label': '<b>Current part of the ORDTC for tax years that end before June 1, 2016</b> (total of amounts M to O)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '230',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '230'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For tax years that end on or after June 1, 2016 and include May 31, 2016',
            'labelClass': 'bold fullLength'
          },
          {
            'type': 'table',
            'num': '2400'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2500'
          },
          {
            'label': 'ORDTC allocated to a corporation by a partnership of which it is a member (other than a specified member)<br> for a fiscal period that ends in the corporation\'s tax year * ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '206',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '206'
                }
              }
            ],
            'indicator': 'R'
          },
          {
            'label': '* If there is a disposal or change of use of eligible property, see Part 7 on page 4',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'Eligible repayments (from amount L in Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '208'
                }
              }
            ],
            'indicator': 'S'
          },
          {
            'label': '<b>Current part of the ORDTC for tax years that end on or after June 1, 2016 and include May 31, 2016,</b> <br>(total of amounts Q to S)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '231',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '231'
                }
              }
            ],
            'indicator': 'T'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For tax years that start after May 31, 2016',
            'labelClass': 'bold fullLength'
          },
          {
            'type': 'table',
            'num': '2600'
          },
          {
            'label': 'ORDTC allocated to a corporation by a partnership of which it is a member (other than a specified member) <br> for a fiscal period that ends in the corporation\'s tax year *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '207',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '207'
                }
              }
            ],
            'indicator': 'V'
          },
          {
            'label': '* If there is a disposal or change of use of eligible property, see Part 7 on page 4',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'Eligible repayments (from amount L in Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '209'
                }
              }
            ],
            'indicator': 'W'
          },
          {
            'label': '<b>Current part of the ORDTC for tax years that start after May 31, 2016</b> (total of amounts U to W)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '232',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '232'
                }
              }
            ],
            'indicator': 'X'
          }
        ]
      },
      {
        'header': 'Part 4 - Calculation of ORDTC available for deduction and ORDTC balance ',
        'rows': [
          {
            'label': 'ORDTC balance at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '299'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'Y'
                }
              }
            ]
          },
          {
            'label': '<b>Deduct</b>: ORDTC expired after 20 tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '300',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '300'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'Z'
                }
              }
            ]
          },
          {
            'label': 'ORDTC at the beginning of the tax year (amount Y <b>minus</b> amount Z)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '305',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '305'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'AA'
                }
              }
            ]
          },
          {
            'label': '<b>Add</b>'
          },
          {
            'label': 'ORDTC transferred on amalgamation or windup',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '310',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '310'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'BB'
                }
              }
            ]
          },
          {
            'label': '<b>Current part of ORDTC</b><br> (amounts P, T or X in Part 3 whichever applies) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '3051'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'CC'
                }
              }
            ]
          },
          {
            'label': 'Are you waiving all or part of the current part of the ORDTC?',
            'labelWidth': '30%',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '315',
            'tn': '315'
          },
          {
            'label': 'If you answered <b>yes</b> at line 315, enter the amount of the tax credit waived on line 320'
          },
          {
            'label': 'If you answered <b>no</b> at line 315, enter "0" on line 320'
          },
          {
            'label': '<b>Deduct</b>: Waiver of the current part of the ORDTC',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '320',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '320'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'DD'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount CC <b>minus</b> amount DD)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '321'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '322'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'EE'
          },
          {
            'label': '<b>ORDTC available for deduction</b> (total of amounts AA, BB and EE)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '323'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '324'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'FF'
          },
          {
            'label': '<b>Deduct</b>'
          },
          {
            'label': 'ORDTC claimed * (Enter amount GG on line 416 on page 5 of Schedule 5, Tax Calculation Supplementary - Corporations)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '3251'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'GG'
                }
              }
            ]
          },
          {
            'label': 'ORDTC carried back to a previous tax year (from Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '326'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'HH'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount GG <b>minus</b> amount HH)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '327'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '328'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'II'
          },
          {
            'label': '<b>ORDTC balance at the end of the tax year</b> (amount FF <b>minus</b> amount II)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '325',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '325'
                }
              }
            ],
            'indicator': 'JJ'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* This amount cannot be more than the lesser of the following amounts:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- ORDTC available for deduction (amount FF); or <br> - Ontario corporate income tax payable before the ORDTC and the Ontario corporate minimum tax credit (amount from line E6 on page 5 of Schedule 5)',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'header': 'Part 5 - Request for carryback of tax credit',
        'rows': [
          {
            'type': 'table',
            'num': '1000'
          }
        ]
      },
      {
        'header': 'Part 6 - Analysis of tax credit available for carryforward by tax year of origin',
        'rows': [
          {
            'label': 'You can complete this part to show all the credits from preceding tax years available for carryforward, by year of origin. This will help you determine the amount of credit that could expire in following years',
            'labelClass': 'fullLength'
          },
          {
            'type': 'splitTable',
            'side1': [
              {
                'type': 'table',
                'num': '400'
              }
            ],
            'side2': [
              {
                'type': 'table',
                'num': '450'
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The amount available from the 20th preceding tax year will expire after this year. When you file your return for the next year, you will enter the expired amount on line 300 of Schedule 508 for that year',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 7 - Calculation of a recapture of ORDTC',
        'rows': [
          {
            'label': 'You will have a recapture of ORDTC in a tax year when you meet all of the following conditions:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'width': '50px',
            'label': '• you acquired a particular property in the current year or in any of the 20 previous tax years if the ORDTC was earned in a tax year-ending after 2008;',
            'labelClass': 'fullLength tabbed',
            'num': '705'
          },
          {
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'width': '50px',
            'label': '• you claimed the cost of the property as an eligible expenditure for the ORDTC',
            'labelClass': 'fullLength tabbed',
            'num': '706'
          },
          {
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'width': '50px',
            'label': '• the cost of the property was included in computing your ORDTC or was subject to an agreement made under subsection 127(13) of the federal Act to transfer qualified expenditures and section 42 of the <i>Taxation Act, 2007</i> (Ontario) applied',
            'labelClass': 'fullLength tabbed',
            'num': '707'
          },
          {
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'width': '50px',
            'label': '• you disposed of the property or converted it to commercial use in a tax year-ending after December 31, 2008. You also meet this condition if you disposed of or converted to commercial use a property which incorporates the particular property previously referred to',
            'labelClass': 'fullLength tabbed',
            'num': '708'
          },
          {
            'label': '<b>Note</b>: The recapture <b>does not apply</b> if you disposed of the property to a non-arm\'s length purchaser who intended to use it all or substantially all for SR&ED in Ontario. When the non-arm\'s length purchaser later sells or converts the property to commercial use, the recapture rules will apply to the purchaser based on the historical federal investment tax credit (ITC) rate * of the original user in Calculation 1 below.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'You have to report the recapture on Schedule 5 for the year in which you disposed of the property or converted it to commercial use. If the corporation is a member of a partnership, report its share of the recapture. <br> If you have more than one disposition for calculations 1 and 2, complete the columns for each disposition for which a recapture applies, using the calculation formats below. <br><br> * Federal ITC in calculations 1 and 2 should be determined without reference to paragraph (e) of the definition <b>investment tax credit</b> in subsection 127(9) of the federal Act.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Calculation 1</b> - If you meet all of the above conditions',
            'labelClass': 'tabbed'
          },
          {
            'type': 'table',
            'num': '690'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Calculation 2</b> - If the corporation is deemed by subsection 42(1) of the <i>Taxation Act, 2007</i> (Ontario) to have transferred all or part of the eligible expenditure to another corporation as a consequence of an agreement described in subsection 127(13) of the federal Act complete Calculation 2. Otherwise, enter nil on line SS.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '770'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '780'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Calculation 3</b>'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'As a member of a partnership, you will report your share of the ORDTC of the partnership after the ORDTC has been reduced by the amount of the recapture. If this is a positive amount, you will report it on line 205, 206, or 207 in Part 3, whichever applies. However, if the partnership does not have enough ORDTC otherwise available to offset the recapture, then the amount by which reductions to the ORDTC exceeds additions (the excess) will be determined and reported on line VV.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporate partner\'s share of the excess of ORDTC (enter amount VV at line ZZ below)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '760',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '760'
                }
              }
            ],
            'indicator': 'VV'
          }
        ]
      },
      {
        'header': 'Part 8 - Total recapture of ORDTC',
        'rows': [
          {
            'label': 'The corporation hereby requests the following tax credit to be applied to the current year tax payable.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Recaptured federal ITC for Calculation 1 (amount from line NN on page 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '800'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'WW'
                }
              }
            ]
          },
          {
            'label': 'Recaptured federal ITC for Calculation 2 (amount from line UU above) .',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '801'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'XX'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '810'
          },
          {
            'label': '<b>Add</b>: Corporate partner\'s share of the excess of ORDTC for Calculation 3 (amount from line VV in Part 7)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '815'
                }
              }
            ],
            'indicator': 'ZZ'
          },
          {
            'label': '<b>Recapture of ORDTC</b> (amount YY <b>plus</b> amount ZZ) (enter amount AAA on line 277 on page 5 of Schedule 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '816'
                }
              }
            ],
            'indicator': 'AAA'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for ORDTC',
        'rows': [
          {
            'type': 'table',
            'num': '1600'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      },
      {
        'header': 'Current and capital ON qualified expenditure',
        'rows': [
          {
            'type': 'table',
            'num': '3000'
          },
          {
            'label': '<b>Deduct: </b>'
          },
          {
            'type': 'table',
            'num': '3001'
          },
          {
            'type': 'table',
            'num': '3002'
          }
        ]
      }
    ]
  });
})();
