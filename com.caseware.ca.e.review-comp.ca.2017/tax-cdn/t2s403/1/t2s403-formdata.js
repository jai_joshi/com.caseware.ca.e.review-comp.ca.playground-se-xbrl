(function() {

  wpw.tax.create.formData('t2s403', {
    formInfo: {
      abbreviation: 'T2S403',
      title: 'Saskatchewan Research and Development Tax Credit',
      schedule: 'Schedule 403',
      code: 'Code 1701',
      formFooterNum: 'T2 SCH 403 E (17)',
      description: [
        {
          type: 'list',
          items: [
            {label: 'Use <b>Part 1</b> of this schedule to calculate a Saskatchewan <b>refundable</b> research and development (R&D) tax credit if the corporation is not exempt from tax under section 149 of the federal <i>Income Tax Act</i>, has a permanent establishment in Saskatchewan, and is a Canadian-controlled private corporation (CCPC) that incurred eligible expenditures for scientific research and experimental development (SR&ED) carried out in Saskatchewan. '},
            {
              label: 'Use <b>Parts 2 to 5</b> of this schedule to calculate a Saskatchewan <b>non-refundable</b> R&D tax credit under section 63.3 and 63.4 of the Saskatchewan <i>Income Tax Act</i> if the corporation has a permanent establishment in Saskatchewan, and meets any of the following criteria:',
              sublist: [
                'it incurred eligible expenditures for SR&ED carried out in Saskatchewan; ',
                'it has a non-refundable R&D tax credit transfer after an amalgamation or the wind-up of a subsidiary, as described in subsections 87(1) and 88(1) of the federal Act; ',
                'it has a non-refundable R&D tax credit allocated to the corporation as a member of a partnership or as a beneficiary under a trust; ',
                'it has a non-refundable R&D tax credit to reduce Saskatchewan income tax otherwise payable in the current tax year; ',
                'it has a non-refundable R&D tax credit to carry back to reduce Saskatchewan income tax otherwise payable in any of the three preceding tax years;',
                'it has a non-refundable R&D tax credit to carry forward to reduce Saskatchewan income tax otherwise payable in any of the 10 subsequent tax years; or',
                'it has a non-refundable R&D tax credit to renounce in whole or in part. The renouncement must be made in the year the credit was earned, and filed on or before the filing due date of the <i>T2 Corporation Income Tax Return</i>.'
              ]
            },
            {label: 'An eligible expenditure is an expenditure that is incurred for research and development carried out in Saskatchewan and that is a <b>qualified expenditure</b> within the meaning of subsections 127(9), (11.1), (11.5), (18), (19), and (20) of the federal <i>Income Tax Act</i>'},
            {label: 'Include a completed copy of this schedule with your <i>T2 Corporation Income Tax Return</i>.'}
          ]
        }
      ],
      category: 'Saskatchewan Forms'
    },
    sections: [
      {
        'forceBreakAfter': true,
        'header': 'Part 1 - Saskatchewan refundable R&D tax credit (CCPCs only)',
        'rows': [
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Refundable credit earned before April 1, 2015 ',
            'labelClass': 'bold fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Total eligible expenditures * incurred before April 1, 2015, for R&D in the current tax year ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '211',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '211'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Expenditure limit ** (enter amount from line 410 of schedule 31)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '212',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '212'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Eligible expenditures for refundable tax credit (amount A or B, whichever is less)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '214',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '214'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': '<b>Saskatchewan refundable R&D tax credit</b> (amount C multiplied by 15%)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '215',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '215'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Enter amount D on line 645 of Schedule 5.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'asterisk',
                'items': [
                  {
                    'label': 'Total eligible expenditures include repayments made in the current year before April 1, 2015 for line A. Each amount must relate to a repayment made by the corporation in the tax year and not in any other tax year. Repayments are the sum of the following: ',
                    'sublist': [
                      'a repayment made in the tax year of government or non-government assistance or a contract payment that reduced an eligible expenditure other than for first-term or second-term shared-use equipment; and',
                      'a repayment made in the tax year of government or non-government assistance, or a contract payment that reduced an eligible expenditure for first-term or second-term shared-use equipment, <b>multiplied</b> by 1/4. '
                    ]
                  },
                  {
                    'label': 'If the tax year includes March 31, 2015, complete the following calculation and enter the result on line 212 above:'
                  }
                ]
              }
            ]
          },
          {
            'type': 'table',
            'num': '3000'
          },
          {
            labelClass: 'fullLength'
          },
          {type: 'horizontalLine'},
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Refundable credit earned after March 31, 2017 ',
            'labelClass': 'bold fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Total eligible expenditures * incurred after March 31, 2017, in the current tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '216',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '216'
                }
              }
            ],
            'indicator': 'AA'
          },
          {
            'label': 'Expenditure limit (enter amount from line 410 of Schedule 31) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'paddingRight': {
                  'type': 'text',
                  'data': '1'
                },
                'input': {
                  'num': '2161',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Amount 1 <b>multiplied</b> by 1/3 **',
            'labelCellClass': 'text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': true
            },
            'columns': [
              {
                'paddingRight': {
                  'type': 'text',
                  'data': '2'
                },
                'input': {
                  'num': '2162',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '217',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  type: 'tn',
                  data: '217'
                }
              }
            ],
            'indicator': 'BB'
          },
          {
            'label': 'Eligible expenditures for refundable tax credit (amount A or B, whichever is less) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '219',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '219'
                }
              }
            ],
            'indicator': 'CC'
          },
          {
            'label': '<b>Saskatchewan refundable R&D tax credit</b> (amount C <b>multiplied</b> by 10%)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '221',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '221'
                }
              }
            ],
            'indicator': 'DD'
          },
          {
            'label': 'Enter amount D on line 645 of Schedule 5.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'asterisk',
                'items': [
                  {
                    'label': 'Total eligible expenditures include repayments made in the current year after March 31, 2017. Each amount must relate to a repayment made by the corporation in the tax year and not in any other tax year. Repayments are the sum of the following: ',
                    'sublist': [
                      'a repayment made in the tax year of government or non-government assistance or a contract payment that reduced an eligible expenditure other than for first-term or second-term shared-use equipment; and',
                      'a repayment made in the tax year of government or non-government assistance, or a contract payment that reduced an eligible expenditure for first-term or second-term shared-use equipment, <b>multiplied</b> by <b>1/3</b>. '
                    ]
                  },
                  {
                    'label': 'If the tax year includes March 31, 2017, complete the following calculation and enter the result on line 217 above:'
                  }
                ]
              }
            ]
          },
          {
            'type': 'table',
            'num': '3100'
          },
          {
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 – Eligible expenditures for non-refundable tax credit',
        'rows': [
          {
            'label': 'Corporations other than a CCPC ',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Total eligible expenditures * incurred in the current tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '101',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '101'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'CCPC',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Total excess eligible expenditures incurred for the non-refundable tax credit in the current tax year before April 1, 2015<br>(amount A <b>minus</b> amount C from Part 1) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '102',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '102'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Total excess eligible expenditures incurred for the non-refundable tax credit in the current' +
            ' tax year after March 31, 2017 <br> (amount AA <b>minus</b> amount CC from Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '107',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '107'
                }
              }
            ],
            'indicator': 'FF'
          },
          {
            'label': 'All corporations',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Total eligible expenditures incurred in the current year after March 31, 2015,' +
            ' and before April 1, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '106',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '106'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'asterisk',
                'items': [
                  {
                    'label': 'Total eligible expenditures include repayments made in the current year before ' +
                    'April 1, 2017. Each amount must relate to a repayment the corporation made in the tax year ' +
                    'and not in any other tax year. Repayments are the sum of the following: ',
                    'sublist': [
                      'a repayment made in the tax year of government or non-government assistance or a contract ' +
                      'payment that reduced an eligible expenditure other than for first-term or second-term ' +
                      'shared-use equipment; and',
                      'a repayment made in the tax year of government or non-government assistance,' +
                      ' or a contract payment that reduced an eligible expenditure for first-term or second-term' +
                      ' shared-use equipment, <b>multiplied</b> by <b>1/4</b>.'
                    ]
                  }
                ]
              }
            ]
          },
        ]
      },
      {
        'header': 'Part 3 – Non-refundable credit available and available for carryforward',
        'spacing': 'R_mult_tn2',
        'rows': [
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Non-refundable credit at end of previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '103'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'H'
                }
              }
            ]
          },
          {
            'label': 'Non-refundable credit expired after 10 tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'I'
                }
              }
            ]
          },
          {
            'label': 'Non-refundable credit at beginning of tax year (amount H <b>minus</b> amount I)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1061',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': 'Non-refundable credit transferred on an amalgamation or the windup of a subsidiary ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'K'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': 'di_table_1'
          },
          {
            'type': 'table',
            'num': 'di_table_2'
          },
          {
            'type': 'table',
            'num': 'di_table_3'
          },
          {
            'label': 'Non-refundable credit allocated to the corporation that is a member of a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'N'
                }
              }
            ]
          },
          {
            'label': 'Non-refundable credit allocated to the corporation that is a beneficiary under a trust',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '140',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '140'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'O'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts K to O)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '141'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '142'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'label': '<b>Non-refundable credit available</b> (amount J <b>plus</b> amount P)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '143'
                }
              }
            ],
            'indicator': 'Q'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: 'Non-refundable credit renounced:'
          },
          {
            'label': 'The renounced credit cannot be more than the total of amounts L, M, N and O. ' +
            '<br>Exclude credit earned on repayments of assistance or contract payment',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '150'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'R'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Amount Q <b>minus</b> amount R',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'paddingRight': {
                  'type': 'text',
                  'data': '1'
                },
                'input': {
                  'num': '1501',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Maximum available for non-refundable credit <br>($1,000,000 minus amount DD from part one) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'paddingRight': {
                  'type': 'text',
                  'data': '2'
                },
                'input': {
                  'num': '1502',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Non-refundable credit claimed in the current tax year:',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'S'
                }
              }
            ]
          },
          {
            label: '(The credit claimed in the current year cannot be more than the lesser of amount 1, amount 2,<br>' +
            ' or the Saskatchewan tax otherwise payable) (Enter amount S on line 631 in Part 2 of Schedule 5)',
            labelClass: 'fullLength'
          },
          {
            'label': 'Non-refundable credit carried back to previous tax year(s) (complete Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '161'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'T '
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts R to T)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '162'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '163'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'U'
          },
          {
            'label': '<b>Non-refundable credit available for carryforward</b> (amount Q <b>minus</b> amount U)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ],
            'indicator': 'V'
          },
          {
            label: '* Amount L1 cannot be more than – $3 million dollars minus amount D before April 1, 2015. '
          },
          {
            label: '** Amount M cannot be more than – $1 million dollars minus amount DD after March 31, 2017. '
          },
          {
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Part 4 - Request for carryback of credit',
        'rows': [
          {
            labelClass: 'fullLength'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 5 - Analysis of credit available for carryforward by year of origin',
        'rows': [
          {
            'type': 'table',
            'num': '2000'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for SK Research and Development Tax Credit',
        'rows': [
          {
            'type': 'table',
            'num': '5000'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  });
})();
