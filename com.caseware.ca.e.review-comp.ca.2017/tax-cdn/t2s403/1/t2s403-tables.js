(function() {

  wpw.tax.global.tableCalculations.t2s403 = {
    'di_table_1': {
      'columns': [
        {
          'colClass': 'std-input-col-width-2',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'width': '18px',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Non-refundable current-year credit earned before April 1, 2015: line 101 or 102, whichever applies*',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '111',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '112',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6' :{
            'tn': '120'
          },
          '7': {
            'num': '120',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'L1'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_2': {
      'columns': [
        {
          'colClass': 'std-input-col-width-2',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'width': '18px',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Non-refundable current-year credit earned before April 1, 2017: line 106',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '116',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '117',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '121'
          },
          '7': {
            'num': '121',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'L2'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_3': {
      'columns': [
        {
          'colClass': 'std-input-col-width-2',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'width': '18px',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Non-refundable current-year credit earned after March 31, 2017: line 101 or 107, whichever applies**',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '118',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '119',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '122'
          },
          '7': {
            'num': '122',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'M'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    '1000': {
      'fixedRows': true,
      'hasTotals': true,
      'infoTable': true,
      'columns': [
        {
          'width': '250px',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'date'
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-col-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'formField': true,
          'total': true,
          'totalNum': '904',
          'totalMessage': 'Total (enter on amount T in Part 3)'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        }
      ],
      'cells': [
        {
          '0': {'label': '1st previous tax year'},
          '1': {'num': '907'},
          '3': {'label': 'Credit to be applied'},
          '4': {'tn': '901'},
          '5': {'num': '901', 'validate': {'or': [{'matches': '^[.\\d]{1,13}$'}, {'check': 'isEmpty'}]}}
        },
        {
          '0': {'label': '2nd previous tax year'},
          '1': {'num': '905'},
          '3': {'label': 'Credit to be applied'},
          '4': {'tn': '902'},
          '5': {'num': '902', 'validate': {'or': [{'matches': '^[.\\d]{1,13}$'}, {'check': 'isEmpty'}]}}
        },
        {
          '0': {'label': '3rd previous tax year'},
          '1': {'num': '906'},
          '3': {'label': 'Credit to be applied'},
          '4': {'tn': '903'},
          '5': {'num': '903', 'validate': {'or': [{'matches': '^[.\\d]{1,13}$'}, {'check': 'isEmpty'}]}}
        }
      ]
    },
    '2000': {
      'fixedRows': true,
      'hasTotals': true,
      'infoTable': true,
      'columns': [
        {'type': 'none'},
        {
          'header': 'Year of origin <br>(earliest year first)',
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'header': 'Credit available',
          colClass: 'std-input-width',
          'formField': true,
          'total': true,
          'totalNum': '552',
          'totalMessage': '<b>Total</b> (equals line 200 in Part 3)'
        },
        {'type': 'none'}
      ],
      'cells': [
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {}
      ]
    },
    '3000': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          'width': '248px',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          'width': '30px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '200px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '10px',
          'type': 'none'
        },
        {
          'width': '100px',
          'formField': true
        },
        {
          'width': '0px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '10px',
          'formField': true
        },
        {
          'width': '112px',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        }
      ],
      'cells': [
        {
          '0': {'label': 'Amount from line 410 of schedule 31'},
          '1': {'num': '1420', 'disabled': true},
          '2': {'label': ' x ', 'labelClass': 'center'},
          '3': {
            'label': 'Number of days in the tax year before April 1, 2015',
            cellClass: 'singleUnderline',
            'labelClass': 'center'
          },
          '5': {'num': '1421', cellClass: 'singleUnderline', 'disabled': true},
          '7': {'type': 'none'},
          '8': {'label': ' = '},
          '9': {'num': '1424'}
        },
        {
          '1': {'type': 'none'},
          '3': {'label': 'Total number of days in the tax year', 'labelClass': 'center'},
          '5': {'num': '1422', 'disabled': true},
          '7': {'type': 'none'},
          '9': {'type': 'none'}
        }
      ]
    },
    '3100': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          'width': '248px',
          'type': 'none',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          'width': '30px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '200px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '10px',
          'type': 'none'
        },
        {
          'width': '100px',
          'formField': true
        },
        {
          'width': '0px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '10px',
          'formField': true
        },
        {
          'width': '112px',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        }
      ],
      'cells': [
        {
          '0': {'label': 'Amount 2 '},
          '1': {'num': '1425', 'disabled': true},
          '2': {'label': ' x ', 'labelClass': 'center'},
          '3': {
            'label': 'Number of days in the tax year after March 31, 2017',
            cellClass: 'singleUnderline',
            'labelClass': 'center'
          },
          '5': {'num': '1426', cellClass: 'singleUnderline', 'disabled': true},
          '7': {'type': 'none'},
          '8': {'label': ' = '},
          '9': {'num': '1427'}
        },
        {
          '1': {'type': 'none'},
          '3': {'label': 'Total number of days in the tax year', 'labelClass': 'center'},
          '5': {'num': '1428', 'disabled': true},
          '7': {'type': 'none'},
          '9': {'type': 'none'}
        }
      ]
    },
    '5000': {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',
          type: 'date',
          disabled: true,
          header: '<b>Year of origin</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalMessage: ' ',
          header: '<b>Current year contribution</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalMessage: ' ',
          header: '<b>Transfers</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true,
          header: '<b>Amount available to apply</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true,
          header: '<b>Applied<b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true,
          header: '<b>Balance to carry forward</b>'
        },
        {colClass: 'std-padding-width',type: 'none'}
      ],
      cells: [
        {
          '4': {label: '*'},
          '5': {type: 'none'},
          '7': {type: 'none'},
          '9': {type: 'none'},
          '11': {type: 'none'},
          '13': {type: 'none', label: 'N/A'}
        },
        {
          '5': {type: 'none'},
          '14': {label: '**'}
        },
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {
          '3': {type: 'none'},
          '7': {type: 'none'}
        }
      ]
    }
  };
})();