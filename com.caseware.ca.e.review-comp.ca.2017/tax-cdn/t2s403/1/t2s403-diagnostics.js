(function() {

  wpw.tax.create.diagnostics('t2s403', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('4030001', common.prereq(common.and(
        common.requireFiled('T2S403'),
        common.check(['t2s5.631'], 'isNonZero')),
        common.check(['101', '102', '105', '106', '107', '110', '130', '140', '211', '214', '216', '219'], 'isNonZero')));

    diagUtils.diagnostic('4030002', common.prereq(common.and(
        common.requireFiled('T2S403'),
        common.check(['t2s5.645'], 'isNonZero')),
        common.check(['211', '216'], 'isNonZero')));

    diagUtils.diagnostic('4030003', common.prereq(common.and(
        common.requireFiled('T2S403'),
        function(tools) {
          return wpw.tax.utilities.dateCompare.lessThan(tools.field('cp.tax_end').get(), {year: 2015, month: 4, day: 1}) &&
              tools.checkMethod(tools.list(['211', '212', '214', '215']), 'isNonZero') > 2;
        }), function(tools) {
          if (tools.field('cp.226').isNo())
            return !!wpw.tax.utilities.flagger()['T2S31'];
          else
            return !!wpw.tax.utilities.flagger()['T2S31'] && wpw.tax.utilities.flagger()['T2S49'];
        }));

    diagUtils.diagnostic('4030004', common.prereq(common.and(
        common.requireFiled('T2S403'),
        function(tools) {
          return wpw.tax.utilities.dateCompare.greaterThan(tools.field('cp.tax_end').get(), {year: 2017, month: 3, day: 31}) &&
              tools.checkMethod(tools.list(['216', '217', '219', '221']), 'isNonZero') > 2;
        }), function(tools) {
          if (tools.field('cp.226').isNo())
            return !!wpw.tax.utilities.flagger()['T2S31'];
          else
            return !!wpw.tax.utilities.flagger()['T2S31'] && wpw.tax.utilities.flagger()['T2S49'];
        }));

  });
})();
