(function() {

  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  function runHistoricalTableCalc(calcUtils, tableObj, carryBackAmount, limitOnCreditAmount) {
    var tableNum = tableObj.tableNum;
    var openingBalanceCindex = tableObj.openingBalanceCindex;
    var currentYearCindex = tableObj.currentYearCindex;
    var transferCindex = tableObj.transferCindex;
    var availableCindex = tableObj.availableCindex;
    var appliedCindex = tableObj.appliedCindex;
    var endingBalanceCindex = tableObj.endingBalanceCindex;

    var summaryTable = calcUtils.field(tableNum);
    var numRows = summaryTable.size().rows;

    summaryTable.getRows().forEach(function(row, rowIndex) {
      if (rowIndex == numRows - 1) {
        if (carryBackAmount > 0) {
          if (carryBackAmount > row[availableCindex].get()) {
            summaryTable.cell(10, 11).assign(row[availableCindex].get());
          }
          else {
            summaryTable.cell(10, 11).assign(carryBackAmount);
          }
          row[availableCindex].assign(row[openingBalanceCindex].get() + row[currentYearCindex].get() + row[transferCindex].get());
          row[endingBalanceCindex].assign(Math.max(row[availableCindex].get() - row[appliedCindex].get(), 0))
        }
        else {
          summaryTable.cell(10, 11).assign(0)
        }
      }
      else {
        if (rowIndex == 0) {
          // to avoid the first row being calculated to total
          row[currentYearCindex].assign(0);
          row[appliedCindex].assign(0);
          row[transferCindex].assign(0);
          row[availableCindex].assign(0);
          row[endingBalanceCindex].assign(0);
        } else {
          row[appliedCindex].assign(returnAmountApplied(limitOnCreditAmount, row[availableCindex].get()));
          row[availableCindex].assign(row[openingBalanceCindex].get() + row[currentYearCindex].get() + row[transferCindex].get());
          row[endingBalanceCindex].assign(Math.max(row[availableCindex].get() - row[appliedCindex].get(), 0));
          limitOnCreditAmount -= row[appliedCindex].get();
        }
      }
    });
  }

  function calculateRefundableCredit(calcUtils, wholeNumber, date, numerator, denominator, result, isAfter) {
    var field = calcUtils.field;
    var taxStart = field('CP.tax_start').get();
    var taxEnd = field('CP.tax_end').get();
    field(numerator).assign(isAfter ? wpw.tax.actions.calculateDaysDifference(date, taxEnd) : wpw.tax.actions.calculateDaysDifference(taxStart, date));
    calcUtils.getGlobalValue(denominator, 'CP', 'Days_Fiscal_Period');
    if (field(numerator).get() >= field(denominator).get()) {
      field(numerator).assign(field(denominator).get())
    }
    field(result).assign(
      field(wholeNumber).get() * (field(numerator).get() / field(denominator).get())
    )
  }

  function getExpenditureLimit(calcUtils, expenditureField, date, fractionResult, sourceForm, sourceField) {
    var field = calcUtils.field;
    var taxStart = field('CP.tax_start').get();
    var taxEnd = field('CP.tax_end').get();
    var isInclusive = calcUtils.dateCompare.betweenInclusive(date, taxStart, taxEnd);

    if (isInclusive) {
      calcUtils.getGlobalValue(expenditureField, 'T2S403', fractionResult)
    }
    else {
      calcUtils.getGlobalValue(expenditureField, sourceForm, sourceField)
    }
  }

  wpw.tax.create.calcBlocks('t2s403', function(calcUtils) {
    calcUtils.calc(function(calcUtils) {
      var num = '111';
      var num2 = '120';
      var midnum = '112';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.getApplicableValue('111', ['101', '102']);
      calcUtils.field(num2).assign(Math.min(product, (calcUtils.field('ratesSk.603').get() - calcUtils.field('215').get())));
    });
    calcUtils.calc(function(calcUtils) {
      var num = '116';
      var num2 = '121';
      var midnum = '117';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num).assign(calcUtils.field('106').get());
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils, field, form) {
      var num = '118';
      var num2 = '122';
      var midnum = '119';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.getApplicableValue('118', ['101', '107']);
      calcUtils.field(num2).assign(Math.max(Math.min(product, (calcUtils.field('ratesSk.604').get() - calcUtils.field('221').get())), 0));

      //calcs years for all table
      var tableArrays = [1000, 2000, 5000];
      var summaryTable = field('tyh.200');
      tableArrays.forEach(function(tableNum) {
        field(tableNum).getRows().forEach(function(row, rowIndex) {
          if (tableNum == 1000) {
            var year = Math.abs(rowIndex - 19);
            row[1].assign(summaryTable.cell(year, 6).get());
          }
          else if (tableNum == 2000) {
            row[1].assign(summaryTable.cell(rowIndex + 10, 6).get())
          }
          else {
            row[1].assign(summaryTable.cell(rowIndex + 9, 6).get())
          }
        });
      });
    });
    calcUtils.calc(function(calcUtils, field) {
      //part 1 before Apr 1, 2015
      var mar31 = wpw.tax.date(2015, 3, 31);
      var apr1 = wpw.tax.date(2015, 4, 1);
      calcUtils.getGlobalValue('1420', 'T2S31', '410');
      calculateRefundableCredit(calcUtils, '1420', apr1, '1421', '1422', '1424', false);
      if (field('211').get()) {
        getExpenditureLimit(calcUtils, '212', mar31, '1424', 'T2S31', '410');
      }
      else {
        field('212').assign(0)
      }
      field('214').assign(Math.min(field('211').get(), field('212').get()));
      field('215').assign(field('214').get() * field('ratesSK.601').get());
    });
    calcUtils.calc(function(calcUtils, field) {
      //part 1 after Mar 31 2017
      var mar31 = wpw.tax.date(2017, 3, 31);
      var apr1 = wpw.tax.date(2017, 4, 1);
      calcUtils.getGlobalValue('2161', 'T2S31', '410');
      field('2162').assign(field('2161').get() / 3);
      calcUtils.getGlobalValue('1425', 'T2S403', '2162');
      calculateRefundableCredit(calcUtils, '1425', apr1, '1426', '1428', '1427', true);
      getExpenditureLimit(calcUtils, '217', mar31, '1427', 'T2S403', '2162');
      field('219').assign(Math.min(field('216').get(), field('217').get()));
      field('221').assign(field('219').get() * field('ratesSK.602').get() / 100);
    });

    calcUtils.calc(function(calcUtils, field) {
      //part 2
      var isCCPC = field('CP.Corp_Type').get() == 1;
      if (!isCCPC) {
        field('102').assign(0);
        field('107').assign(0);
      }
      else {
        field('101').assign(0);
        field('102').assign(field('211').get() -  field('214').get());
        field('107').assign(field('216').get() -  field('219').get())
      }

      //part 3
      calcUtils.getGlobalValue('112', 'ratesSk', '601');
      calcUtils.getGlobalValue('117', 'ratesSk', '602');
      calcUtils.getGlobalValue('119', 'ratesSk', '602');
      field('103').assign(field('5000').get().totals[3]);
      field('104').assign(field('5000').cell(0, 3).get());
      calcUtils.subtract('105', '103', '104');
      calcUtils.equals('1061', '105');
      calcUtils.equals('116', '106');
      field('110').assign(field('5000').get().totals[7]);
      calcUtils.sumBucketValues('141', ['110', '120', '121', '130', '140', '122']);

      calcUtils.equals('142', '141');
      calcUtils.sumBucketValues('143', ['1061', '142']);
      field('1501').assign(Math.max(field('143').get() - field('150').get(), 0));
      field('1502').assign(Math.max(field('ratesSk.605').get() - field('221').get(),0));

      field('160').assign(Math.min(field('1501').get(), field('1502').get(), field('T2S411.306').get()));
      calcUtils.equals('161', '904');
      calcUtils.sumBucketValues('162', ['150', '160', '161']);
      calcUtils.equals('163', '162');
      calcUtils.subtract('200', '143', '163');
    });

    calcUtils.calc(function(calcUtils, field, form) {

      var summaryTable = field('5000');
      field('2000').getRows().forEach(function(row, rowIndex) {
        row[5].assign(summaryTable.getRow(rowIndex + 1)[13].get())
      });
      // historical data chart
      var carryBackAmount = field('1000').get().totals[5];
      var limitOnCreditAmount = Math.max(field('120').get() - carryBackAmount, 0);
      runHistoricalTableCalc(
        calcUtils,
        {
          tableNum: '5000',
          openingBalanceCindex: 3,
          currentYearCindex: 5,
          transferCindex: 7,
          availableCindex: 9,
          appliedCindex: 11,
          endingBalanceCindex: 13
        },
        carryBackAmount,
        limitOnCreditAmount
      );
      summaryTable.cell(10, 5).assign(field('120').get());
    });
  })
}());
