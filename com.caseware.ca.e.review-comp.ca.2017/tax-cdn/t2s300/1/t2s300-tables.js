(function() {

  function getTableTaxInc(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {type: 'none', colClass: 'std-padding-width'},
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {

          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '2': {num: labelsObj.num[0]},
          '3': {label: 'x'},
          '4': {
            label: labelsObj.label[1],
            labelClass: 'center',
            cellClass: 'singleUnderline'
          },
          '6': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '7': {label: '='},
          '8': {num: labelsObj.num[2]},
          '9': {label: labelsObj.indicator || ''}
        },
        {
          '2': {type: 'none'},
          '4': {
            label: labelsObj.label[2],
            labelClass: 'center fullLength'
          },
          '6': {num: labelsObj.num[3]},
          '8': {type: 'none'}
        }
      ]
    }
  }

  function getTableRate(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '2': {num: labelsObj.num[0], decimals: labelsObj.decimals},
          '3': {label: ' %= '},
          '4': {num: labelsObj.num[1]},
          '5': {label: labelsObj.indicator}
        }]
    }
  }

  wpw.tax.create.tables('t2s300', {

    '110': getTableTaxInc({
      label: ['Amount C or D, whichever is <b>less</b>', 'Taxable income earned in Newfoundland and Labrador', 'Taxable income for all provinces *'],
      indicator: 'E',
      num: ['105', '106', '107', '108']
    }),

    '1100': getTableTaxInc({
      label: ['Amount E', 'Number of days in the tax year before January 1, 2016', 'Number of days in the tax year'],
      indicator: 'F',
      num: ['111', '112', '113', '114']
    }),

    '120': getTableRate({
      label: ['<b>Newfoundland and Labrador manufacturing and processing profits tax credit</b> (Amount F multiply by '],
      indicator: 'G',
      num: ['150', '115']
    })
  })
})();
