(function() {

  wpw.tax.create.formData('t2s300', {
    formInfo: {
      abbreviation: 'T2S300',
      title: 'Newfoundland and Labrador Manufacturing and Processing Profits Tax Credit',
      schedule: 'Schedule 300',
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 300 E (16)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'For use by corporations that maintained a permanent establishment (as defined ' +
              'in Regulation 400 of the federal <i>Income Tax Regulations</i>) in Newfoundland and Labrador ' +
              'at any time in the tax year, and had:',
              sublist: ['taxable income earned in the year in Newfoundland and Labrador; and',
                'Canadian manufacturing and processing profits, as defined in subsection 125.1(3) ' +
                'of the federal <i>Income Tax Act</i>, earned in the tax year in Newfoundland and Labrador.']
            },
            {
              label: 'This credit may not be claimed unless the corporation has engaged in manufacturing ' +
              'or processing in the tax year from a permanent establishment in Newfoundland and Labrador. ' +
              'This credit will no longer be available for days after December 31, 2015.'
            },
            {
              label: 'This schedule is a worksheet only and is not required to be filed with your ' +
              '<i>T2 Corporation Income Tax Return</i>.'
            }
          ]
        }
      ],
      category: 'Newfoundland and Labrador Forms'
    },
    sections: [
      {
        'header': 'Calculation of Newfoundland and Labrador manufacturing and processing profits tax credit',
        'rows': [
          {
            'label': 'Canadian manufacturing and processing profits for the year (line 200 in Part 9 of Schedule 27)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'A'
                }
              }
            ]
          },
          {
            'label': 'Deduct: ',
            'labelClass': 'bold'
          },
          {
            'label': 'The least of the amounts on lines 400, 405, 410, and 427 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '101'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount A <b>minus</b> amount B)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '102'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '103'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Amount from line H9 in Part 9 of Schedule 27',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '104'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '110'
          },
          {
            'type': 'table',
            'num': '1100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '120'
          },
          {
            'label': 'Enter amount G on line 503 of Schedule 5.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Includes the territories and the offshore areas of Nova Scotia and Newfoundland and Labrador'
          }
        ]
      }
    ]
  });
})();
