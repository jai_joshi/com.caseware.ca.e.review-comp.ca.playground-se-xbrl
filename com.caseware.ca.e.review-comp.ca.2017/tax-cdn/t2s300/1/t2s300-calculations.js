(function() {

  wpw.tax.create.calcBlocks('t2s300', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('150', 'ratesNl', '100');

      calcUtils.getGlobalValue('104', 't2s27', '363');
      calcUtils.getGlobalValue('100', 't2s27', '200');

      field('101').assign(Math.min(field('t2j.400').get(), field('t2j.405').get(), field('t2j.410').get(), field('t2j.427').get()));
      calcUtils.subtract('102', '100', '101');
      field('103').assign(field('102').get());
      field('105').assign(Math.min(field('103').get(), field('104').get()));

      // If is multiple jurisdictions
      var taxableIncomeAllocationNL = calcUtils.getTaxableIncomeAllocation('NL');
      var taxableIncomeAllocationXO = calcUtils.getTaxableIncomeAllocation('XO');

      field('106').assign(taxableIncomeAllocationNL.provincialTI + taxableIncomeAllocationXO.provincialTI);
      field('108').assign(taxableIncomeAllocationNL.allProvincesTI);

      field('106').source(taxableIncomeAllocationNL.provincialTISourceField);
      field('108').source(taxableIncomeAllocationNL.allProvincesTISourceField);

      field('107').assign(
          field('105').get() *
          field('106').get() /
          field('108').get()
      );

      var janDate = wpw.tax.date(2016, 1, 1);
      var dateComparisons = calcUtils.compareDateAndFiscalPeriod(janDate);
      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();

      field('111').assign(field('107').get());
      field('112').assign(dateComparisons.daysBeforeDate);
      field('114').assign(daysFiscalPeriod);
      field('113').assign(
          field('111').get() *
          field('112').get() /
          field('114').get()
      );

      field('115').assign(
          field('113').get() *
          field('150').get() / 100
      );
    });
  });
})();
