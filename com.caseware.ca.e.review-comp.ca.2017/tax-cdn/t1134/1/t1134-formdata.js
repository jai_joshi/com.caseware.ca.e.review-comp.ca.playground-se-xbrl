(function() {
  'use strict';
  var currencyCodes = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.currencyCodes, 'value');

  wpw.tax.global.formData.t1134 = {
    formInfo: {
      abbreviation: 'T1134',
      title: 'Information Return Relating to Controlled and Not-Controlled Foreign Affiliates T1134 Summary Form',
      subtitle: 'T1134 Summary Form',
      // printNum: 'T1134',
      hideHeaderBox: true,
      hideProtectedB: true,
      headerImage: 'canada-federal',
      formFooterNum: 'T1134 E (17)',
      agencyUseOnlyBox: {
        width: '200px',
        height: '140px'
      },
      description: [
        {
          type: 'list',
          items: [
            'Use this version of the return for taxation years that begin after 2010.',
            'This revised form T1134 is a combination of former forms T1134A and T1134B.',
            'Refer to the attached instructions before you complete the T1134 Summary and Supplements.',
            'A separate supplement must be filed for each foreign affiliate.',
            'Do not file a return for "dormant" or "inactive" foreign affiliates. Refer to the attached ' +
            'instructions for the definition of dormant or inactive foreign affiliates.',
            'References on this return to the foreign affiliate or the affiliate refer to the foreign' +
            ' affiliate for which the reporting entity is filing a supplement.',
            'If you are reporting on a partnership, references to year or taxation year should be read ' +
            'as fiscal period.',
            'If you need more space to report information, you can use attachments.'
          ]
        }

      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            inputType: 'dropdown',
            options: currencyCodes,
            labelClass: 'fullLength',
            tabbed: true,
            num: '100',
            label: '• If an election has been made to use a functional currency (see attached instructions), state the ' +
            'elected functional currency code'
          },
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            labelWidth: '80%',
            labelClass: 'fullLength',
            width: '20px',
            num: 'amendedIndicator',
            init: false,
            label: '• If this is an amended return, check this box. '
          },
          {
            labelClass: 'fullLength'
          }
        ]
      },

      {
        header: 'Part I  - Identification',
        rows: [
          {
            label: 'Section 1 – Reporting entity information',
            labelClass: 'bold'
          },
          {
            type: 'infoField',
            inputType: 'dropdown',
            labelWidth: '70%',
            num: '102',
            options: [
              {option: '', value: ''},
              {option: 'New', value: '1'},
              {option: 'Unmodified', value: '2'},
              {option: 'Amended', value: '3'}
            ],
            label: 'What type of submission is this?'
          },
          {
            label: 'Choose a box to indicate who you are reporting for, and complete the areas that apply (please print)',
            labelWidth: '80%',
            type: 'infoField',
            inputType: 'dropdown',
            num: '103',
            disabled: true,
            init: '1',
            options: [
              {value: '', option: ''},
              {value: '1', option: 'Corporation'},
              {value: '2', option: 'Individual'},
              {value: '3', option: 'Trust'},
              {value: '4', option: 'Partnership'}
            ]
          },
          {
            type: 'table',
            num: '1000'
          },
          {
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength',
            label: 'Do you have a business number for other reporting purposes (for example: GST/HST remittances,Payroll etc.)?'
          },
          {
            type: 'infoField',
            inputType: 'custom',
            format: ['{N9}'],
            label: ' If so, please provide (9 digits):',
            num: '1001',
            labelWidth: '25%'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Reporting  entity\'s address'
          },
          {
            type: 'infoField',
            inputType: 'address',
            add1Num: '104', add2Num: '105', provNum: '106', cityNum: '107', countryNum: '108', postalCodeNum: '109'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField', inputType: 'dateRange',
            label: 'For what taxation year are you filing this form?',
            'label1': 'Tax year-start', num: '110', disabled: true,
            source1: 'cp-tax_start',
            'label2': 'Tax year-end', 'num2': '111', source2: 'cp-tax_end'
          },
          {
            type: 'infoField',
            inputType: 'radio',
            label: 'Does this period include 2 or more short taxation years? (see attached instructions) ',
            num: '112',
            labelWidth: '55%'
          },
          {
            type: 'infoField',
            label: 'Number of supplements attached (automatically tallied):',
            num: '113'
          }
        ]
      },
      {
        forceBreakAfter: true,
        header: 'Section 2 – Certification',
        rows: [
          {
            label: 'Person to contact for more information (please print)',
            labelClass: 'fullLength'
          },
          {
            type: 'table',
            num: '1116'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '1117'
          },
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '1118'
          }
        ]
      },
      {
        header: 'Section 3 – Organizational structure',
        rows: [
          {
            label: 'This information is required only if you are reporting on one or more "Controlled" ' +
            'Foreign Affiliate(s) as defined in subsection 95(1) of the Act (see attached instructions). You only have ' +
            'to produce the information required under this section once for a group of persons that are' +
            ' related to each other. If a person/partnership other than the reporting entity is filing the ' +
            'organizational structure, identify that person/partnership who is filing for the related group:',
            labelClass: 'fullLength'
          },
          {
            type: 'table', num: '1200'
          },
          {labelClass: 'fullLength'},
          {
            label: 'If the following table has insufficient space, attach a separate page with a continuation of the information.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: 'A. List the name and country code of the country of residence of each corporation ' +
            '(other than another foreign affiliate of the reporting entity) that is not dealing at arm\'s length' +
            ' with the reporting entity and that has an equity percentage (as defined in subsection 95(4) of the Act)' +
            ' in any foreign affiliate of the reporting entity. Where the reporting entity is a partnership, ' +
            'list the name and country code of the country of residence of each corporation that is not dealing at' +
            ' arm\'s length with the members of the partnership, and that has an equity percentage in any foreign' +
            ' affiliate of the reporting entity. Include the corporation\'s equity percentage and direct equity ' +
            'percentage, if any, in the foreign affiliate. (see attached instructions)',
            labelClass: 'fullLength'
          },
          {
            type: 'table', num: '1010'
          },
          {labelClass: 'fullLength'},
          {
            label: 'B. List the name and country code of the country of residence of each foreign affiliate' +
            ' of the reporting entity that has an equity percentage in any other foreign affiliate of the reporting ' +
            'entity. Include the foreign affiliate\'s equity percentage and direct equity percentage, if any,' +
            ' in the other foreign affiliate.',
            labelClass: 'fullLength'
          },
          {
            type: 'table', num: '1020'
          },
          {labelClass: 'fullLength'},
          {
            label: 'C. If the reporting entity is a partnership, list the name, address and country code' +
            ' of the country of residence of each member of the partnership.',
            labelClass: 'fullLength'
          },
          {
            type: 'table', num: '1030'
          },
          {labelClass: 'fullLength'},
          {
            label: 'D. List the name, address and country code of the business location' +
            ' of each partnership of which a foreign affiliate is a member',
            labelClass: 'fullLength'
          },
          {
            type: 'table', num: '1040'
          }
        ]
      }
    ]
  }
})
();