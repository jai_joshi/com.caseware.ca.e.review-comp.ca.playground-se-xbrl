(function() {
  wpw.tax.create.diagnostics('t1134', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;
    var disallowedAddressChars = ['-', "'", '/', '&', '(', ')', '#'];
    var addressFields = ['104', '105'];
    var percentFields = [{tableNum: '1010', col: '4'}, {tableNum: '1010', col: '5'}, {tableNum: '1020', col: '4'}, {tableNum: '1020', col: '5'}, {tableNum: '1040', col: '4'}];

    diagUtils.diagnostic('BN', common.prereq(common.check('1009', 'isFilled'), common.bnCheck('1009')));
    diagUtils.diagnostic('BN', common.prereq(common.check('1201', 'isFilled'), common.bnCheck('1201')));

    diagUtils.diagnostic('1134.requiredToFile', common.prereq(common.check('CP.to-27', 'isYes'), common.check('102', 'isNonZero')));

    diagUtils.diagnostic('1134.corpName', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.field('1013').require('isFilled');
    }));

    diagUtils.diagnostic('1134.businessNum', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.field('1002').require('isFilled');
    }));

    diagUtils.diagnostic('1134.corpOnly', function(tools) {
      return tools.field('103').require(function() {
        return this.get() == '1';
      });
    });

    diagUtils.diagnostic('1134.addressFilled', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.requireOne(tools.list(['104', '105']), 'isFilled');
    }));

    diagUtils.diagnostic('1134.cityCheck', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.field('107').require('isFilled');
    }));

    diagUtils.diagnostic('1134.provCheck', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.field('106').require('isFilled');
    }));

    diagUtils.diagnostic('1134.postalCheck', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.field('109').require('isFilled');
    }));

    diagUtils.diagnostic('1134.countryCheck', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.field('108').require('isFilled');
    }));

    diagUtils.diagnostic('1134.taxYearStartCheck', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.field('110').require('isFilled');
    }));

    diagUtils.diagnostic('1134.taxYearEndCheck', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.field('111').require('isFilled');
    }));

    diagUtils.diagnostic('1134.shortYearCheck', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.field('112').require('isNonZero');
    }));

    diagUtils.diagnostic('1134.suppFilledCheck', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.field('113').require('isNonZero');
    }));

    diagUtils.diagnostic('1134.suppCountCheck', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.field('113').require(function() {
        return this.get() == wpw.tax.form.allRepeatForms('t1134s').length;
      });
    }));

    diagUtils.diagnostic('1134.suppTotalCountCheck', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.field('113').require(function() {
            return this.get() <= 1500;
          });
    }));

    diagUtils.diagnostic('1134.certifierNameCheck', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.field('997').require('isFilled');
    }));

    diagUtils.diagnostic('1134.certifierPositionCheck', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.field('999').require('isFilled');
    }));

    diagUtils.diagnostic('1134.orgAccountNum', common.prereq(function(tools) {
      return tools.field('103').get() == '1' && tools.field('102').isFilled();
    }, function(tools) {
      return tools.field('121').require('isFilled');
    }));

    function percentCheck(fields) {
      angular.forEach(fields, function(field) {
        diagUtils.diagnostic('1134.percentFormatCheck', common.prereq(function(tools) {
          return tools.field('103').get() == '1' && tools.field('102').isFilled();
        }, function(tools) {
          return tools.checkAll(tools.field(field.tableNum).getRows(), function(row) {
            if (row[field.col].isFilled()) {
            return row[field.col].require(function() {
              if (row[field.col].get().toString().indexOf('.') > -1) {
                return row[field.col].get() >= 0 && row[field.col].get() <= 100 &&
                    row[field.col].get().toString().split('.')[0].length <= 3 &&
                    row[field.col].get().toString().split('.')[1].length <= 2;
              } else {
                return row[field.col].get() >= 0 && row[field.col].get() <= 100 && row[field.col].get().toString().length <= 3;
              }
            });
          }else return true;
          })
        }));
      });
    }

    percentCheck(percentFields);

    function addressCheck(fields) {
      angular.forEach(fields, function(field) {
        diagUtils.diagnostic('1135.addressCheck', common.prereq(function(tools) {
          return tools.field(field).isFilled();
        }, function(tools) {
          return tools.field(field).require(function() {
            var addressField = this.get().toString();
            var firstPos = addressField.charAt(0);
            var lastPos = addressField.charAt(addressField.length - 1);
            return !(disallowedAddressChars.indexOf(firstPos) > -1 ||
            disallowedAddressChars.indexOf(lastPos) > -1 ||
            firstPos == '.');
          })
        }));
      });
    }

    addressCheck(addressFields);

  });
})();
