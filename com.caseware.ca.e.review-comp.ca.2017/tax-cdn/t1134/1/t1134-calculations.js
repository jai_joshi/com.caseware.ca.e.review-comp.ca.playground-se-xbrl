(function() {

  wpw.tax.create.calcBlocks('t1134', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.field('1008').assign(true);
      calcUtils.getGlobalValue('1013', 'CP', '002');
      calcUtils.getGlobalValue('1002', 'CP', 'bn');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('104', 'cp', '011');
      calcUtils.getGlobalValue('105', 'cp', '012');
      calcUtils.getGlobalValue('106', 'cp', '016');
      calcUtils.getGlobalValue('107', 'cp', '015');
      calcUtils.getGlobalValue('108', 'cp', '017');
      calcUtils.getGlobalValue('109', 'cp', '018');
      calcUtils.getGlobalValue('110', 'cp', 'tax_start');
      calcUtils.getGlobalValue('111', 'cp', 'tax_end');
      if (field('CP.957').get() == 1) {
        calcUtils.getGlobalValue('114', 'cp', '951');
        calcUtils.getGlobalValue('115', 'cp', '950');
      }
      else {
        var firstName = field('cp.958').get().split(" ")[1];
        var lastName = field('cp.958').get().split(" ")[0];
        field('114').set(firstName);
        field('115').set(lastName)
      }
      calcUtils.getGlobalValue('116', 'cp', '959');
      calcUtils.getGlobalValue('998', 'cp', '955');
      calcUtils.getGlobalValue('999', 'cp', '954');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var repeatformIds = calcUtils.allRepeatForms('t1134s');
      field('113').assign(repeatformIds.length);
    });

    calcUtils.calc(function(calcUtils, field) {
      if (field('amendedIndicator').get()) {
        field('102').assign('3');
      }
    })
  });
})();
