(function() {
  var addCanada = wpw.utilities.clone(wpw.tax.codes.countryCodes);
  addCanada['CAN'] = {value: 'CAN Canada'};
  var countryCodesAddCanada = new wpw.tax.actions.codeToDropdownOptions(addCanada, 'value', 'value');

  wpw.tax.global.tableCalculations.t1134 = {
    '1000': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {width: '80px'},
        {},
        {}
      ],
      cells: [{
        '0': {
          type: 'infoField',
          inputType: 'singleCheckbox',
          num: '1008',
          label: 'Corporation'
        },
        '1': {
          label: 'Corporation\'s name',
          num: '1013',
          type: 'text'
        },
        '2': {
          label: 'Business number (BN)',
          type: 'infoField',
          inputType: 'custom',
          format: ['{N9}RC{N4}', 'NR'],
          num: '1002'
        }
      }]
    },
    /*   '1000': {
     infoTable: true,
     fixedRows: true,
     columns: [
     {
     width: '80px'
     },
     {
     },
     {
     },
     {
     width: '80px'
     },
     {

     }
     ],
     cells: [
     {
     '0': {
     type: 'infoField',
     inputType: 'singleCheckbox',
     num: '1007',
     label: 'Individual',
     disabled: true,
     cannotOverride: true
     },
     '1': {
     label: 'First Name',
     type: 'text',
     disabled: true,
     cannotOverride: true
     },
     '2': {
     label: 'Last Name',
     type: 'text',
     disabled: true,
     cannotOverride: true
     },
     '3': {
     label: 'Initial',
     type: 'text',
     disabled: true,
     cannotOverride: true
     },
     '4': {
     label: 'Social insurance number',
     validate: {check: 'mod10'},
     num: '1100',
     disabled: true,
     cannotOverride: true
     },
     showWhen: {
     fieldId: '103',
     compare: {is: '2'}
     }
     },
     {
     '0': {
     type: 'infoField',
     inputType: 'singleCheckbox',
     num: '1008',
     label: 'Corporation'
     },
     '1': {
     label: 'Corporation\'s name',
     num: '1013',
     colClass: 'std-input-col-padding-width-3',
     type: 'text'
     },
     '2': {
     type: 'none'
     },
     '3': {
     type: 'none'
     },
     '4': {
     label: 'Business number (BN)',
     type: 'infoField',
     inputType: 'custom',
     format: ['{N9}RC{N4}', 'NR'],
     colClass: 'std-input-col-padding-width-3',
     num: '1009'
     },
     showWhen: {
     fieldId: '103',
     compare: {is: '1'}
     }
     },
     {
     '0': {
     type: 'infoField',
     inputType: 'singleCheckbox',
     num: '1011',
     label: 'Trust',
     disabled: true,
     cannotOverride: true
     },
     '1': {
     label: 'Trust\'s name',
     type: 'text',
     disabled: true,
     cannotOverride: true
     },
     '2': {
     type: 'none'
     },
     '3': {
     type: 'none'
     },
     '4': {
     label: 'Account number',
     type: 'infoField',
     num: '1200',
     disabled: true,
     cannotOverride: true
     },
     showWhen: {
     fieldId: '103',
     compare: {is: '3'}
     }
     },
     {
     '0': {
     type: 'infoField',
     inputType: 'singleCheckbox',
     num: '1012',
     label: 'Partnership',
     disabled: true,
     cannotOverride: true
     },
     '1': {
     label: 'Partnership\'s name',
     type: 'text',
     disabled: true,
     cannotOverride: true
     },
     '2': {
     type: 'none'
     },
     '3':{
     type: 'none'
     },
     '4': {
     label: 'Partnership\'s account number',
     num:'1201',
     type: 'custom',
     format: ['{N9}RZ{N4}', 'NR'],
     disabled: true,
     cannotOverride: true
     },
     showWhen: {
     fieldId: '103',
     compare: {is: '4'}
     }
     }
     ]
     },*/
    '1010': {
      maxLoop: 1000,
      'showNumbering': true,
      'columns': [
        {
          'header': 'Name of related corporation',
          'num': '200',
          type: 'text'
        },
        {
          'header': 'Country code of residence of corporation',
          'type': 'dropdown',
          'num': '201',
          'options': countryCodesAddCanada
        },
        {
          'header': 'Name of foreign affiliate ',
          'num': '202',
          colClass: 'std-input-col-width',
          type: 'text'
        },
        {
          'header': 'Country code of residence of foreign affiliate',
          'type': 'dropdown',
          'num': '203',
          'options': countryCodesAddCanada
        },
        {
          'header': 'Corporation\'s equity percentage in foreign affiliate',
          colClass: 'std-input-width',
          'num': '204',
          decimals: 2
        },
        {
          'header': 'Corporation\'s direct equity percentage in foreign affiliate',
          colClass: 'std-input-width',
          'num': '205',
          decimals: 2
        }
      ]
    },
    '1020': {
      maxLoop: 1000,
      'showNumbering': true,
      'columns': [{
        'header': 'Name of foreign affiliate',
        'num': '300',
        type: 'text'
      },
        {
          'header': 'Country code of residence of foreign affiliate',
          'type': 'dropdown',
          'num': '301',
          'options': countryCodesAddCanada
        },
        {
          'header': 'Name of other foreign affiliate',
          'num': '302',
          colClass: 'std-input-col-width',
          type: 'text'
        },
        {
          'header': 'Country code of residence of other foreign affiliate',
          'type': 'dropdown',
          'num': '303',
          'options': countryCodesAddCanada
        },
        {
          'header': 'Foreign affiliate\'s equity percentage in other foreign affiliate',
          colClass: 'std-input-width',
          'num': '304',
          decimals: 2
        },
        {
          'header': 'Foreign affiliate\'s direct equity percentage in other foreign affiliate',
          colClass: 'std-input-width',
          'num': '305',
          decimals: 2
        }]
    },
    '1030': {
      maxLoop: 1000,
      'showNumbering': true,
      'columns': [
        {
          'header': 'Name of partner',
          'num': '400',
          type: 'text'
        },
        {
          'header': 'Address of partner',
          'num': '401',
          type: 'text'
        },
        {
          'header': 'Country code of country of residence of partner',
          'type': 'dropdown',
          colClass: 'std-input-width',
          'num': '402',
          'options': countryCodesAddCanada
        }
      ]
    },
    '1040': {
      maxLoop: 1000,
      'showNumbering': true,
      'columns': [{
        'header': 'Partnership name',
        'num': '500',
        type: 'text'
      },
        {
          'header': 'Address of partnership',
          'num': '501',
          type: 'text'
        },
        {
          'header': 'Country code of country of location of partnership',
          'type': 'dropdown',
          colClass: 'std-input-width',
          'num': '502',
          'options': countryCodesAddCanada
        },

        {
          'header': 'Foreign affiliate',
          'num': '503',
          type: 'text'
        },
        {
          'header': 'Foreign affiliate\'s interest percentage in the partnership',
          'num': '504',
          decimals: 2,
          colClass: 'std-input-width'
        }
      ]

    },
    '1116': {
      fixedRows: true,
      'columns': [{
        'header': 'First Name',
        type: 'text'
      },
        {
          'header': 'Last Name',
          type: 'text'
        },
        {
          'header': 'Telephone Number',
          'telephoneNumber': true, type: 'text'
        }],
      'cells': [{
        '0': {
          num: '114'
        },
        '1': {
          num: '115'
        },
        '2': {
          num: '116'
        }
      }]
    },
    '1117': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'type': 'none',
          width: '20px'
        },
        {
          colClass: 'std-input-col-width',
          type: 'text'
        },
        {
          'type': 'none'
        }],
      'cells': [{
        '0': {
          label: 'I,'
        },
        '1': {
          num: '997'
        },
        '2': {
          label: ' , certify that the information given on these T1134 Summary and Supplements are, ' +
          'to the best of my knowledge, correct and complete'
        }
      }]
    },
    '1118': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'header': 'Date',
          'type': 'date'
        },
        {
          'header': 'Authorized signing officer\'s, or representative\'s signature',
          type: 'text'
        },
        {
          'header': 'Position, title, officer\'s rank',
          type: 'text'
        }
      ],
      'cells': [
        {
          '0': {
            num: '998'
          },
          '2': {
            num: '999'
          }
        }
      ]
    },
    '1200': {
      fixedRows: true,
      columns: [
        {
          header: 'Organizational Structure Name'
        },
        {
          header: 'SIN / BN / Other account number'
        }
      ],
      cells: [
        {
          0: {
            num: '120',
            type: 'text'
          },
          1: {
            num: '121',
            type: 'custom',
            format: ['{N9}RC{N4}']
          }
        }
      ]
    }
  }
})();
