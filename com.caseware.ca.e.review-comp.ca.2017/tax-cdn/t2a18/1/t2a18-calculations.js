(function() {
  function calculateGainLoss(calcUtils, result, colA, colB, colC) {
    var field = calcUtils.field;
    if (result == '058') {
      field(result).assign(Math.max(field(colA).get() - (field(colB).get() + field(colC).get()),0));
    }
    else {
      field(result).assign(field(colA).get() - (field(colB).get() + field(colC).get()))
    }
  }

  wpw.tax.create.calcBlocks('t2a18', function(calcUtils) {
    calcUtils.calc(function (calcUtils, field) {
      var sch6Table101 = field('T2S6.101').get();
      var sch6Table201 = field('T2S6.201').get();
      var sch6Table301 = field('T2S6.301').get();
      var sch6Table401 = field('T2S6.401').get();
      var sch6Table501 = field('T2S6.501').get();
      var sch6Table601 = field('T2S6.601').get();

      //all shares
      field('002').assign(sch6Table101.totals[4]);
      field('002').source(field('T2S6.101').total(4));
      field('022').assign(sch6Table101.totals[5]);
      field('022').source(field('T2S6.101').total(5));
      field('042').assign(sch6Table101.totals[6]);
      field('042').source(field('T2S6.101').total(6));
      calculateGainLoss(calcUtils, '043', '002', '022', '042');
      field('053').assign(field('T2S6.160').get());
      field('053').source(field('T2S6.160'));
      field('054').assign(field('043').get() + field('053').get());
      //real estate
      field('004').assign(sch6Table201.totals[2]);
      field('004').source(field('T2S6.201').total(2));
      field('024').assign(sch6Table201.totals[3]);
      field('024').source(field('T2S6.201').total(3));
      field('044').assign(sch6Table201.totals[4]);
      field('044').source(field('T2S6.201').total(4));
      calculateGainLoss(calcUtils, '055', '004', '024', '044');
      //bonds
      field('006').assign(sch6Table301.totals[4]);
      field('006').source(field('T2S6.301').total(4));
      field('026').assign(sch6Table301.totals[5]);
      field('026').source(field('T2S6.301').total(5));
      field('046').assign(sch6Table301.totals[6]);
      field('046').source(field('T2S6.301').total(6));
      calculateGainLoss(calcUtils, '056', '006', '026', '046');
      //other properties
      field('008').assign(sch6Table401.totals[2]);
      field('008').source(field('T2S6.401').total(2));
      field('028').assign(sch6Table401.totals[3]);
      field('028').source(field('T2S6.401').total(3));
      field('048').assign(sch6Table401.totals[4]);
      field('048').source(field('T2S6.401').total(4));
      calculateGainLoss(calcUtils, '057', '008', '028', '048');
      //personal-use property
      field('010').assign(sch6Table501.totals[2]);
      field('010').source(field('T2S6.501').total(2));
      field('030').assign(sch6Table501.totals[3]);
      field('030').source(field('T2S6.501').total(3));
      field('050').assign(sch6Table501.totals[4]);
      field('050').source(field('T2S6.501').total(4));
      calculateGainLoss(calcUtils, '058', '010', '030', '050');
      //LPP
      field('012').assign(sch6Table601.totals[2]);
      field('012').source(field('T2S6.601').total(2));
      field('032').assign(sch6Table601.totals[3]);
      field('032').source(field('T2S6.601').total(3));
      field('052').assign(sch6Table601.totals[4]);
      field('052').source(field('T2S6.601').total(4));
      calculateGainLoss(calcUtils, '059', '012', '032', '052');
      calcUtils.getGlobalValue('060', 'T2S6', '655');
    });
    calcUtils.calc(function (calcUtils, field) {
      field('062').assign(field('054').get() + field('055').get() + field('056').get() + field('057').get() + field('058').get() + field('059').get() - field('060').get());
      calcUtils.getGlobalValue('064', 'T2S6', '875');
      calcUtils.getGlobalValue('066', 'T2S13', '008');
      calcUtils.getGlobalValue('068', 'T2S13', '010');
      field('070').assign(field('062').get() + field('064').get() + field('066').get() - field('068').get());
      calcUtils.getGlobalValue('0721', 'T2S6', '895');
      field('073').assign(field('071').get() + field('0721').get());
      calcUtils.getGlobalValue('075', 'T2S6', '896');
      field('0761').assign(field('0741').get() + field('075').get());
      field('072').assign(field('073').get() + field('0761').get());
      field('074').assign(field('070').get() - field('072').get());
      field('076').assign(field('074').get() * field('ratesAb.003').get() /100);

      var sch6Table = field('t2s6.700');
      var sch6TableRowSize=field('300').size().rows;
      var T2S6RowSize= field('t2s6.700').size().rows;
      for(sch6TableRowSize ;sch6TableRowSize<T2S6RowSize;sch6TableRowSize++){
        calcUtils.addTableRow('t2a18', '300');
      }
      for(sch6TableRowSize =field('300').size().rows;sch6TableRowSize>T2S6RowSize;sch6TableRowSize--){
        calcUtils.removeTableRow('t2a18', '300');
      }

      field('300').getRows().forEach(function (row, rowIndex) {
        if(sch6Table.getRow(rowIndex)){
          row[0].assign(sch6Table.getRow(rowIndex)[0].get());
          row[0].source(field('t2s6.700').cell(rowIndex,0));
          row[1].assign(sch6Table.getRow(rowIndex)[1].get());
          row[1].source(field('t2s6.700').cell(rowIndex,1));
          row[2].assign(sch6Table.getRow(rowIndex)[2].get());
          row[2].source(field('t2s6.700').cell(rowIndex,2));
          row[3].assign(sch6Table.getRow(rowIndex)[3].get());
          row[3].source(field('t2s6.700').cell(rowIndex,3));
          row[4].assign(sch6Table.getRow(rowIndex)[4].get());
          row[4].source(field('t2s6.700').cell(rowIndex,4));
          row[5].assign(sch6Table.getRow(rowIndex)[5].get());
          row[5].source(field('t2s6.700').cell(rowIndex,5));
          row[6].assign(sch6Table.getRow(rowIndex)[6].get());
          row[6].source(field('t2s6.700').cell(rowIndex,6));
        }
      });
      field('094').assign(field('300').get().totals[6] * field('ratesAb.003').get() / 100);
    })


  });
})();
