(function () {
  wpw.tax.create.formData('t2a18', {
    formInfo: {
      abbreviation: 't2a18',
      title: 'Alberta Dispositions of Capital Property',
      //subTitle: '(2006 and later tax years)',
      schedule: 'Schedule 18',
      formFooterNum: 'AT18 (Mar-14)',
      // headerImage: 'canada-alberta',
      showCorpInfo: true,
      category: 'Alberta Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            labelClass: 'fullLength'
          },
          {
            label: '<b>This schedule is <u>required</u> if the opening balance, proceeds of disposition, ' +
            'adjusted cost base or gain/loss for Alberta purposes differs from that for federal purposes.' +
            '<br>For taxation years that meet one or both of the following conditions:' +
            '<br>- the taxation year straddles February 28, 2000, and there were dispositions before February 28, 2000' +
            ', and after February 27, 2000, <br>- the taxation year straddles October 18, 2000, and there were ' +
            'dispositions before October 18, 2000, and after October 17, 2000, <br>' +
            'then supporting documentation detailing the straddle calculations must be submitted with Schedule 18.</b>'
          },
          {
            'type': 'infoField',
            'label': 'Is the corporation electing to transfer property as stated under ACTA section' +
            ' 14.1(3), 14.2(3) or 16.1(3)?',
            'num': '001',
            'tn': '001',
            'inputType': 'radio'
          },
          {
            label: 'If yes, the applicable Alberta election form (AT107, AT108 or AT109) must be completed ' +
            'and submitted by the corporation acquiring the property ("transferee").' +
            ' See the election form for filing instructions.'
          },
          {
            label: 'Report all monetary amounts in dollars; DO NOT include cents. Show negative amounts in brackets ( ).'
          }
        ]
      },
      {
        'header': 'CAPITAL PROPERTY DISPOSITIONS',
        'rows': [
          {
            'type': 'table',
            'num': '100'
          },
          {
            'label': 'Add: Line 160 of federal schedule 6',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '053',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '053'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '150'
          },
          {
            'label': 'Subtract: Unapplied listed personal property losses from other years up to the' +
            ' total listed personal property gains <br>(carry this amount forward to schedule 21, line 119, if applicable)',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '060',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '060'
                }
              }
            ]
          }
        ]
      },
      {
        hideFieldset: true,
        'rows': [
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Total of Column D (Do not include the amounts at lines ' +
            '059 and 060 if the difference is a net loss)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '062',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '062'
                }
              }
            ]
          },
          {
            'label': 'Capital gains dividends',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '064',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '064'
                }
              }
            ]
          },
          {
            'label': 'Add: capital gain reserve opening balance, <i>if any</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '066',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '066'
                }
              }
            ]
          },
          {
            'label': 'Deduct: capital gain reserve closing balance, <i>if any</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '068',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '068'
                }
              }
            ]
          },
          {
            'label': '<b>Total capital gain or (loss):</b> Line 062 + line 064 + line 066 - line 068',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '070',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '070'
                }
              }
            ]
          },
          {
            'label': 'Gain on donations of a share, debt obligation, or right listed on a' +
            ' designated stock exchange and amounts under paragraph. 38(a.1) of Act.'
          },
          {
            'type': 'table',
            'num': '200'
          },
          {
            'label': 'Realized before May 1, 2006',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '0721',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false
            },
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '073',
                  'disabled': true, cannotOverride: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '<b>A</b>'
                }
              }
            ]
          },
          {
            label: 'Gain on donation of ecologically sensitive land.'
          },
          {
            type: 'table',
            num: '250'
          },
          {
            'label': 'Realized before May 1, 2006',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '075',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false
            },
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '0761',
                  'disabled': true, cannotOverride: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '<b>B</b>'
                }
              }
            ]
          },
          {
            'label': 'Subtotal: amount A + amount B',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '072',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '072'
                }
              }
            ]
          },
          {
            'label': 'Line 070 minus line 072',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '074',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '074'
                }
              }
            ]
          },
          {
            label: '<b>If line 074 is negative, then carry this capital loss amount forward to Schedule 21, line 057.</b>'
          },
          {
            label: '<b>Taxable capital gain:</b>Line 074 X 50%'
          },
          {
            'label': 'For dispositions of property after October 17, 2000, use 50% otherwise see Guide. (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '076',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '076'
                }
              }
            ]
          },
          {
            label: '<b>If line 076 is positive, <i>then carry this taxable capital gain ' +
            'forward to Schedule 12, line 040.</i></b>'
          },
          {
            label: '* Net listed personal property losses may only be applied against listed personal ' +
            'property gains. Do not include listed personal property losses in total.'
          }
        ]
      },
      {
        header: 'Property qualifying for and resulting in an allowable business investment loss',
        rows: [
          {
            'type': 'table',
            'num': '300'
          },
          {
            label: '** See Inclusion Rate note at line 076.'
          },
          {
            'label': 'Allowable Business Investment Loss: total of column D X Inclusion Rate **:<br>' +
            '<b><i>Carry forward this amount to Schedule 12, and include it in line 040</i></b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '094'
                },
                'padding': {
                  'type': 'tn',
                  'data': '094'
                }
              }
            ]
          }
        ]
      }
    ]
  });
})();
