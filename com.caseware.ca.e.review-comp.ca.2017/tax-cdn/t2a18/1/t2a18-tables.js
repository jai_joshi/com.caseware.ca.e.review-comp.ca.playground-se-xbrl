(function() {
  var realizedGainColumn = [
    {
      type: 'none'
    },
    {
      type: 'none'
    },
    {
      type: 'none',
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-input-width',
      formField: true
    },
    {
      type: 'none',
      colClass: 'std-padding-width'
    },
    {
      type: 'none',
      colClass: 'std-input-width'
    },
    {
      type: 'none',
      colClass: 'std-padding-width'
    }
  ];
  wpw.tax.create.tables('t2a18', {
    '100':{
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          header: 'Description'
        },
        {
          header: 'A<br>Proceeds of disposition',
          colClass: 'std-input-col-width ',
          formField: true
        },
        {
          header: 'B<br>Adjusted cost base',
          colClass: 'std-input-col-width ',
          formField: true
        },
        {
          header: 'C<br>Outlays and Expenses<br>(re dispositions)',
          colClass: 'std-input-col-width ',
          formField: true
        },
        {
          header: 'D<br>Col. A - (Cols. B + C)',
          colClass: 'std-input-col-width',
          formField: true
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0':{
            label: 'Total of all shares'
          },
          '1':{
            tn: '002',
            num: '002'
          },
          '2':{
            tn: '022',
            num: '022'
          },
          '3':{
            tn: '042',
            num: '042'
          },
          '4':{
            num: '043'
          }
        }
      ]
    },
    '150':{
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none'
        },
        {
          colClass: 'std-input-col-width ',
          formField: true
        },
        {
          colClass: 'std-input-col-width',
          formField: true
        },
        {
          colClass: 'std-input-col-width',
          formField: true
        },
        {
          colClass: 'std-input-col-width',
          formField: true
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells:[
        {
          '1':{
            type: 'none'
          },
          '2':{
            type: 'none'
          },
          '3':{
            type: 'none',
            label: 'Gain or (loss)',
            cellClass: 'alignRight'
          },
          '4':{
            tn: '054',
            num: '054'
          }
        },
        {
          '0':{
            label: 'Total of all real estate'
          },
          '1':{
            tn: '004',
            num: '004'
          },
          '2':{
            tn: '024',
            num: '024'
          },
          '3':{
            tn: '044',
            num: '044'
          },
          '4':{
            tn: '055',
            num: '055'
          }
        },
        {
          '0':{
            label: 'Total of all bonds'
          },
          '1':{
            tn: '006',
            num: '006'
          },
          '2':{
            tn: '026',
            num: '026'
          },
          '3':{
            tn: '046',
            num: '046'
          },
          '4':{
            tn: '056',
            num: '056'
          }
        },
        {
          '0':{
            label: 'Total of all other properties'
          },
          '1':{
            tn: '008',
            num: '008'
          },
          '2':{
            tn: '028',
            num: '028'
          },
          '3':{
            tn: '048',
            num: '048'
          },
          '4':{
            tn: '057',
            num: '057'
          }
        },
        {
          '0':{
            label: 'Total of all personal-use property<br>(Note: losses are not deductible)'
          },
          '1':{
            tn: '010',
            num: '010'
          },
          '2':{
            tn: '030',
            num: '030'
          },
          '3':{
            tn: '050',
            num: '050'
          },
          '4':{
            tn: '058',
            num: '058'
          }
        },
        {
          '0':{
            label: 'Total of all listed personal property*'
          },
          '1':{
            tn: '012',
            num: '012'
          },
          '2':{
            tn: '032',
            num: '032'
          },
          '3':{
            tn: '052',
            num: '052'
          },
          '4':{
            tn: '059',
            num: '059'
          }
        }
      ]
    },
    '200':{
      fixedRows: true,
      infoTable: true,
      columns: realizedGainColumn,
      cells: [
        {
          '0':{
            label: 'Realized before May 2, 2006'
          },
          '2':{
            label: 'divided by 2 ='
          },
          '3':{
            num: '071'
          },
          '4':{
            label: '+'
          }
        }
      ]
    },
    '250':{
      fixedRows: true,
      infoTable: true,
      columns: realizedGainColumn,
      cells: [
        {
          '0':{
            label: 'Realized before May 2, 2006'
          },
          '2':{
            label: 'divided by 2 ='
          },
          '3':{
            num: '0741'
          },
          '4':{
            label: '+'
          }
        }
      ]
    },
    '300':{
      'fixedRows': true,
      'keepButtonsSpace': true,
      hasTotals: true,
      columns: [
        {
          header: 'Name of small business corporation',
          tn: '082',
          colClass: 'std-input-col-padding-width',
          type: 'text'
        },
        {
          header: 'Specify:<br>1 = shares or<br> 2 = debt',
          tn: '084',
          colClass: 'half-col-width ',
          type: 'dropdown',
          options: [
            {
              value: '1',
              option: '1'
            },
            {
              value: '2',
              option: '2'
            }
          ]
        },
        {
          header: 'Date of Acquisition<br>YYYYMMDD',
          tn: '086',
          type: 'date'
        },
        {
          header: 'A<br>Proceeds of disposition',
          tn: '088',
          total: true,
          totalMessage: 'Totals:'
        },
        {
          header: 'B<br>Adjusted cost base',
          tn: '090',
          total: true
        },
        {
          header: 'C<br>Outlays and expenses<br>(re dispositions)',
          tn: '092',
          total: true
        },
        {
          header: 'D<br>(Loss)<br>Col. A - <br>(Cols. B + C)',
          total: true
        }
      ]
    }
  })
})();
