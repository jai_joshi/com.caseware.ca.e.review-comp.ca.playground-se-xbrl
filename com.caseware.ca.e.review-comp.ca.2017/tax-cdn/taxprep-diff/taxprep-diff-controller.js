wpw.main.controller('taxprep-diff-controller', ['$scope', 'taxprepDiff', 'saveService', 'globalValue',
  '$q', '$timeout', 'fileReader', 'override', 'repeatFormService', 'basicDialogs', 'taxUtils', '$document',
  function($scope, taxprepDiff, saveService, globalValue, $q, $timeout, fileReader, override,
           repeatFormService, dialogs, taxUtils, $document) {
    $scope.jsonOutput = '';
    $scope.diffList = [];
    $scope.show_dev = false;

    var initializeDiff = function() {
      saveService.executeOnLoad(function() {
        var diffInfo = globalValue.get('config', 'taxprep_diff');
        if(diffInfo) {
          var parsedDiff = taxprepDiff.getImportDiff(diffInfo)
          if (parsedDiff && parsedDiff.diffList && parsedDiff.diffObj) {
            $scope.diffList = parsedDiff.diffList;
            $scope.jsonOutput = JSON.stringify(parsedDiff.diffObj, null, 2);
          }
        }
      });
    };

    $scope.displayRowCol = function(row) {
      return wpw.tax.utilities.isNullUndefined(row.rowIndex) ? 'N/A' : (row.rowIndex + '/' + row.colIndex);
    };

    var jumpToRow = function(row) {
      var formId;
      if (wpw.tax.utilities.isRepeatForm(row.form)) {
        var sortedRepeatIds = repeatFormService.assignOrderSequence(row.form);
        formId = row.form + '-' + sortedRepeatIds[row.sequence];
      } else {
        formId = row.form;
      }

      override.jumpToSource(formId+'.'+row.field);
    };

    $scope.rowJumpToSource = function(row) {
      var msg = 'Go to ' + row.form + '.' + row.sequence + '-' + row.field + '?';
      dialogs.confirm('Jump to source?', msg).result.then(function() {
        jumpToRow(row);
      });
    };

    /**
     * Loads a script dynamically (lazy loading).
     * @param {string} path The path of the script to be loaded.
     * @param {function=} namespaceExists A function that is evaluated to check of necessary namespace is available.
     *     when left out the promise will be resolved immediately after on load completes.
     * @return {promise}
     */
    function loadScript(path, namespaceExists) {
      var defer = $q.defer();

      /**
       * Creates an html script tag and appends it to the dom document loading it.
       * @param {string} path Path of the script to be loaded.
       * @param {function} onLoadFunction Function to be evaluated on load. This is mandatory.
       */
      function createTag(path, onLoadFunction) {
        var tag = document.createElement('script');
        tag.src = wpw.global.staticRoot + path;
        tag.onload = onLoadFunction;
        document.getElementsByTagName('body')[0].appendChild(tag);
      }

      if (!namespaceExists) {
        createTag(path, function() {
          defer.resolve();
        });
      } else if (!namespaceExists()) {
        createTag(path, function() {
          var maxRetries = 1000;
          function checkNamespace() {
            if (--maxRetries <= 0)
              defer.reject();
            else if (namespaceExists()) {
              defer.resolve();
            } else {
              $timeout(checkNamespace)
            }
          }
          $timeout(checkNamespace);
        });
      }
      else {
        $timeout(function() {
          defer.resolve();
        });
      }

      return defer.promise;
    }

    function padZeros(num) {
      if (isNaN(num) || num.toString().length >= 3)
        return num;

      return padZeros('0' + num.toString());
    }

    function autoFillSanitize(arr) {
      var exceptions = {
        'dropdown' : true,
        'singleCheckbox': true,
        'radio': true
      };

      arr.forEach(function(line, index) {
        var formAbbr = line[2];
        var fieldId = line[3];
        var colIndex = line[5];

        if (!formAbbr || !fieldId) {
          return;
        }

        var parser = wpw.tax.formParser.getFieldData(formAbbr, padZeros(fieldId));
        var fieldData = parser.data;
        var fieldLocalData = parser.init ? parser.init.localData : null;

        var type = fieldLocalData ? fieldLocalData.type : null;
        var valueType = fieldLocalData ? fieldLocalData.valueType : null;

        var cellType = null;
        if (type === 'table' && !wpw.tax.utilities.isNullUndefined(colIndex)) {
          cellType = fieldData.columns[colIndex] ? fieldData.columns[colIndex].type : null;
        }

        if (!exceptions[type] && (valueType == 'text' || cellType == 'text' || cellType == 'custom')) {
          line[0] = '1'; //set isSanitized
        }

        arr[index] = line.join(',');
      });
      arr = arr.join('\n');
      return arr;
    }

    $scope.setDiffThreshold = taxprepDiff.setDiffThreshold;

    $scope.refreshDiffs = function() {
      var diffInfo = taxprepDiff.runDiffs();
      if(diffInfo) {
        $scope.diffList = diffInfo.diffList;
        $scope.jsonOutput = JSON.stringify(diffInfo.diffObj, null, 2);
      }
    };

    $scope.options = {
      'Diff Threshold:': 'Will only display differences greater than this value for values <b>over 100</b>. Default value of 1 will be used if no value is provided.'
    };


    $scope.toggleInfo = function() {
      var element = document.getElementById('extraInfo');
      element.style.opacity = 1 - element.style.opacity;
    };

    $scope.$on('file.uploaded', function(e, result, type) {
      type = type || '';
      switch(type.toUpperCase()) {
        case 'TAXPREP-DIFF':
          fileReader.readAsText(result).then(function(file) {
            var diffInfo = taxprepDiff.generateOutput(file);
            $scope.diffList = diffInfo.diffList;
            $scope.jsonOutput = JSON.stringify(diffInfo.diffObj, null, 2);
          });
          break;
        case 'DIFF-JSON':
          fileReader.readAsText(result).then(function(file) {
            var diffInfo = JSON.parse(file);
            $scope.diffList = diffInfo.diffList;
            $scope.jsonOutput = JSON.stringify(diffInfo.diffObj, null, 2);
          });
          break;
        case 'SANITIZE':
          fileReader.readAsText(result).then(function(file) {
            var csvArr = taxUtils.parseCSV(file);
            var csvString = autoFillSanitize(csvArr);
            var download = new Blob([csvString], {type: 'text/plain'});
            saveAs(download, 'sanitized-mapping.csv');
          });
          break;
        default:
          // console.error('Unknown file type');
      }
    });

    $scope.$on('folder.uploaded', function(e, result, type) {
      type = type || '';
      switch(type.toUpperCase()) {
        case 'TAXPREP-FOLDER-DIFF':
          $scope.$applyAsync(function() {$scope.loading = true;});
          //Load JSZip
          var loadPromise = loadScript('/external/js-xlsx/jszip.js', function() {
            return angular.isFunction(window.JSZip);
          });

          var generateBatchPromise = taxprepDiff.generateBatchOutput(result);
          loadPromise.then(function() {
            return generateBatchPromise;
          }).then(function(batchDiffArr) {
            //Generate blob for each diff, then zip blobs together
            var zipFile = new window.JSZip();
            batchDiffArr.forEach(function(resultObj) {
              zipFile.file(resultObj.file.name + '.json', JSON.stringify(resultObj.diffInfo, null, 2));
            });
            $scope.$applyAsync(function() {$scope.loading = null;});
            saveAs(zipFile.generate({type:"blob"}), 'batch.zip');
          });
          break;
        default:
          // console.error('Unknown folder type');
      }
    });

    $scope.$on('diff.updated', function(e) {
      initializeDiff();
    });

    var keyPressHandler = function(e) {
      //Shift + S flips flag to display/hide show_dev buttons
      if (e.shiftKey && e.ctrlKey && e.code == "KeyS") {
        $scope.$apply(function() {
          $scope.show_dev = !$scope.show_dev;
        });
      }
    };
    $document.on('keypress', keyPressHandler);
    $scope.$on('$destroy', function() {
      $document.off('keypress', keyPressHandler);
    });

    initializeDiff();
  }
]);
