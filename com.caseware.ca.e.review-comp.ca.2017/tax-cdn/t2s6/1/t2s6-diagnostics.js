(function() {
  wpw.tax.create.diagnostics('t2s6', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0060001', common.prereq(common.check(['t2j.206'], 'isChecked'), common.requireFiled('T2S6')));

    diagUtils.diagnostic('0060002', common.prereq(common.and(
        common.requireFiled('T2S6'),
        common.check('t2s1.113', 'isNonZero')),
        function(tools) {
          var depFields = ['875', '880', '899'];
          return tools.requireOne(tools.list(depFields), 'isNonZero') ||
              tools.requireOne(tools.field('101').getCol(5), 'isNonZero') ||
              tools.requireOne(tools.field('201').getCol(3), 'isNonZero') ||
              tools.requireOne(tools.field('301').getCol(5), 'isNonZero') ||
              tools.requireOne(tools.field('401').getCol(3), 'isNonZero') ||
              tools.requireOne(tools.field('501').getCol(3), 'isNonZero') ||
              tools.requireOne(tools.field('601').getCol(3), 'isNonZero') ||
              tools.requireAll(tools.list(['897', '898']), 'isNonZero');
        }));

    diagUtils.diagnostic('0060003', common.prereq(common.and(
        common.requireFiled('T2S6'),
        common.check('t2s1.406', 'isNonZero')),
        function(tools) {
          var table = tools.field('700');
          var bothCols = table.getCol(5).concat(table.getCol(6));
          return tools.requireOne(bothCols, 'isNonZero');
        }));

    diagUtils.diagnostic('0060004', common.prereq(common.and(
        common.requireFiled('T2S6'),
        common.check('655', 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.field('601').getCol(3), 'isNonZero');
        }));

    diagUtils.diagnostic('0060005', common.prereq(common.and(
        common.requireFiled('T2S6'),
        common.check('t2s4.210', 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.field('101').getCol(6), 'isNonZero') ||
              tools.requireOne(tools.field('101').getCol(7), 'isNonZero') ||
              tools.requireOne(tools.field('201').getCol(4), 'isNonZero') ||
              tools.requireOne(tools.field('201').getCol(5), 'isNonZero') ||
              tools.requireOne(tools.field('301').getCol(6), 'isNonZero') ||
              tools.requireOne(tools.field('301').getCol(7), 'isNonZero') ||
              tools.requireOne(tools.field('401').getCol(4), 'isNonZero') ||
              tools.requireOne(tools.field('401').getCol(5), 'isNonZero') ||
              tools.requireOne(tools.field('885'), 'isNonZero') ||
              tools.requireOne(tools.field('901'), 'isNonZero');
        }));

    diagUtils.diagnostic('0060006', common.prereq(common.and(
        common.requireFiled('T2S6'),
        common.check('t2s4.510', 'isNonZero')),
        function(tools) {
          var table = tools.field('601');
          var bothCols = table.getCol(4).concat(table.getCol(5));
          return tools.requireOne(bothCols, 'isNonZero');
        }));

  });
})();
