(function() {

  var addOther = wpw.utilities.clone(wpw.tax.codes.countryCodes);
  addOther['OTH'] = {value: 'OTH Other'};
  var countryCodesAddOther = new wpw.tax.actions.codeToDropdownOptions(addOther, 'value', 'value');


  var t1135CategoryOptions = [
    {
      value: '1',
      option: 'Funds held outside Canada'
    },
    {
      value: '2',
      option: 'Shares of non-resident corporations (other than foreign affiliates)'
    },
    {
      value: '3',
      option: 'Indebtedness owed by non-resident'
    },
    {
      value: '4',
      option: 'Interests in non-resident trusts'
    },
    {
      value: '5',
      option: 'Real property outside Canada (other than personal use and real estate used in an active business)'
    },
    {
      value: '6',
      option: 'Other property outside Canada'
    }
  ];

  var checkboxColWidth = '53px';
  var dateColWidth = '150px';
  var formFieldColWidth = '107px';
  var descriptionColWidth = '250px';
  var addressColWidth = '300px';

  function createTable(partSection) {
    var colObjArr = [];

    switch (partSection) {
      case '1':
        colObjArr = [
          {label: '1 <br> Number of shares', tn: '100'},
          {label: '2 <br> Name of corporation in which the shares are held', type: 'text', tn: '105'},
          {label: '3 <br> Class of shares', type: 'text', tn: '106'},
          {label: '4 <br> Date of Acquisition', type: 'date', tn: '110'},
          {label: '5 <br> Proceeds of disposition', tn: '120', total: true, totalMessage: 'Totals:'},
          {label: '6 <br> Adjusted cost base', tn: '130', total: true},
          {label: '7 <br> Outlays and expenses from disposition', tn: '140', total: true},
          {label: '8 <br> Gain (or loss) (column 5 minus columns 6 and 7)', tn: '150', total: true}
        ];
        break;
      case '2':
        colObjArr = [
          {label: '1 <br> Municipal address of real estate', type: 'address', tn: '200', width: '200px'},
          {label: '2 <br> Date of Acquisition', type: 'date', tn: '210', width: '150px'},
          {label: '3 <br> Proceeds of disposition', tn: '220', total: true},
          {label: '4 <br> Adjusted cost base', tn: '230', total: true},
          {label: '5 <br> Outlays and expenses from disposition', tn: '240', total: true},
          {label: '6 <br> Gain (or loss) (column 3 <b> minus </b> columns 4 and 5)', tn: '250', total: true}
        ];
        break;
      case '3':
        colObjArr = [
          {label: '1 <br> Face value of bonds', tn: '300'},
          {label: '2 <br> Maturity Date', type: 'date', tn: '305'},
          {label: '3 <br> Name of bond issuer', type: 'text', tn: '307'},
          {label: '4 <br> Date of Acquisition', type: 'date', tn: '310'},
          {label: '5 <br> Proceeds of disposition', tn: '320', total: true},
          {label: '6 <br> Adjusted cost base', tn: '330', total: true},
          {label: '7 <br> Outlays and expenses from disposition', tn: '340', total: true},
          {label: '8 <br> Gain (or loss) (column 5 minus columns 6 and 7)', tn: '350', total: true}
        ];
        break;
      case '4':
        colObjArr = [
          {label: '1 <br> Description of other property', type: 'text', tn: '400'},
          {label: '2 <br> Date of Acquisition', type: 'date', tn: '410'},
          {label: '3 <br> Proceeds of disposition', tn: '420', total: true},
          {label: '4 <br> Adjusted cost base', tn: '430', total: true},
          {label: '5 <br> Outlays and expenses from disposition', tn: '440', total: true},
          {label: '6 <br> Gain (or loss) (column 3 <b> minus </b> column 4 and 5', tn: '450', total: true}
        ];
        break;
      case '5':
        colObjArr = [
          {label: '1 <br> Description of personal-use property', type: 'text', tn: '500'},
          {label: '2 <br> Date of Acquisition', type: 'date', tn: '510'},
          {label: '3 <br> Proceeds of disposition', tn: '520', total: true},
          {label: '4 <br> Adjusted cost base', tn: '530', total: true},
          {label: '5 <br> Outlays and expenses from disposition', tn: '540', total: true},
          {
            label: '6 <br> Gain only (column 3<b> minus</b> columns 4 and 5;<br> if negative, enter "0")',
            tn: '550',
            total: true
          }
        ];
        break;
      case '6':
        colObjArr = [
          {label: '1 <br> Description of listed personal property', type: 'text', tn: '600'},
          {label: '2 <br> Date of Acquisition', type: 'date', tn: '610'},
          {label: '3 <br> Proceeds of disposition', tn: '620', total: true},
          {label: '4 <br> Adjusted cost base', tn: '630', total: true},
          {label: '5 <br> Outlays and expenses from disposition', tn: '640', total: true},
          {label: '6 <br> Gain (or loss) (column 3 <b> minus </b> column 4 and 5', tn: '650', total: true}
        ];
        break;
      case '7':
        colObjArr = [
          {label: '1 <br> Name of small business corporation', type: 'text', tn: '900'},
          {
            label: '2 <br> Shares, enter 1; debt, enter 2.',
            type: 'dropdown',
            options: [
              {
                value: '1',
                option: 'Shares'
              },
              {
                value: '2',
                option: 'Debt'
              }
            ],
            tn: '905'
          },
          {label: '3 <br> Date of Acquisition', type: 'date', tn: '910'},
          {label: '4 <br> Proceeds of disposition', tn: '920', total: true},
          {label: '5 <br> Adjusted cost base', tn: '930', total: true},
          {label: '6 <br> Outlays and expenses from disposition', tn: '940', total: true},
          {label: '7 <br> Loss only (column 4 <b> minus </b> columns 5 and 6)', tn: '950', total: true}
        ];
        break;
    }

    var tableCols = [];
    var colWidth;

    colObjArr.forEach(function(colObj) {
      if (colObj.type == 'text') {
        colWidth = descriptionColWidth
      } else if (colObj.type == 'date') {
        colWidth = dateColWidth
      } else if (colObj.type == 'address') {
        colWidth = addressColWidth
      } else {
        colWidth = formFieldColWidth
      }

      tableCols.push(
          {
            header: colObj.label,
            tn: colObj.tn,
            num: colObj.tn,
            width: colWidth,
            type: colObj.type,
            options: colObj.options,
            total: colObj.total
          }
      );
    });

    tableCols.push(
        {
          header: 'Transfer to T1135',
          type: 'singleCheckbox',
          width: checkboxColWidth
        },
        {
          header: 'T1135 Category',
          width: dateColWidth,
          type: 'dropdown',
          options: t1135CategoryOptions
        },
        {
          header: 'T1135 Country',
          type: 'dropdown',
          options: countryCodesAddOther,
          width: dateColWidth
        }
    );

    return {
      hasTotals: true,
      initRows: 0,
      scrollx: true,
      columns: tableCols
    }
  }

  wpw.tax.global.tableCalculations.t2s6 = {
    'di_table_1': {
      'num': 'di_table_1',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Total of Column 7',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '710'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '705',
            'init': '1',
            'colClass': 'singleUnderline'
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '715',
            'cellClass': ' doubleUnderline'
          },
          '8': {
            'label': 'G'
          }
        },
        {
          '2': {
            'type': 'none'
          },
          '4': {
            'num': '705_D',
            'init': '2'
          },
          '7': {
            'type': 'none'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_2': {
      'num': 'di_table_2',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Taxable capital gains under section 34.2 of the Act (line 275 of Schedule 73, <i> Income Inclusion Summary for Corporations that are Members of Partnerships </i>)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '994',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '8891',
            'init': '2'
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': '899'
          },
          '7': {
            'num': '899',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' singleUnderline'
          },
          '8': {
            'label': 'R'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_3': {
      'num': 'di_table_3',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Allowable capital losses under section 34.2 of the Act (line 285 of Schedule 73, <i> Income Inclusion Summary for Corporations that are Members of Partnerships</i>)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '991'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '9011',
            'init': '2'
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': '901'
          },
          '7': {
            'num': '990',
            'cellClass': ' singleUnderline'
          },
          '8': {
            'label': 'T'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    '101': createTable('1'),
    '201': createTable('2'),
    '301': createTable('3'),
    '401': createTable('4'),
    '501': createTable('5'),
    '601': createTable('6'),
    '700': createTable('7'),
    '8000': {
      hasTotals: true,
      infoTable: true,
      fixedRows: true,
      scrollx: true,
      columns: [
        {
          type: 'none'
        },
        {
          width: '40px',
          type: 'none'
        },
        {
          colClass: 'std-input-width',

          total: true,
          totalNum: '8213',
          totalMessage: '* Total'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {
            label: 'Realized gains/losses on disposal of assets (GIFI 8210)'
          },
          '1': {
            type: 'none'
          },
          '2': {
            num: '8210'
          }
        },
        {
          '0': {
            label: 'Realized gains/losses on sale of investments (GIFI 8211)'
          },
          '1': {
            type: 'none'
          },
          '2': {
            num: '8211'
          }
        },
        {
          '0': {
            label: 'Realized gains/losses on sale of resource properties (GIFI 8212)'
          },
          '1': {
            type: 'none'
          },
          '2': {
            num: '8212'
          }
        }
      ]
    }
  }
})();
