(function() {

  var acbMappingObjArr = [
    {
      t2s6TableNum: '101',
      acbSumTableNum: '1100',
      t2s6cIndices: [0, 1, 2, 4, 5, 8, 9, 10],
      acbSumcIndices: [3, 0, 1, 4, 5, 6, 7, 8]
    },//Share
    {
      t2s6TableNum: '201',
      acbSumTableNum: '1200',
      t2s6cIndices: [0, 1, 2, 3, 4, 6, 7, 8],
      acbSumcIndices: [0, 1, 3, 4, 5, 6, 7, 8]
    },//RealEstate
    {
      t2s6TableNum: '301',
      acbSumTableNum: '1300',
      t2s6cIndices: [0, 2, 4, 5, 8, 9, 10],
      acbSumcIndices: [2, 0, 3, 4, 5, 6, 7]
    },//Bond
    {
      t2s6TableNum: '401',
      acbSumTableNum: '1400',
      t2s6cIndices: [0, 1, 2, 3, 4, 6, 7, 8],
      acbSumcIndices: [0, 1, 3, 4, 5, 6, 7, 8]
    },//OtherProperties
    {
      t2s6TableNum: '501',
      acbSumTableNum: '1500',
      t2s6cIndices: [0, 1, 2, 3, 4, 6, 7, 8],
      acbSumcIndices: [0, 1, 3, 4, 5, 6, 7, 8]
    },//PUP
    {
      t2s6TableNum: '601',
      acbSumTableNum: '1600',
      t2s6cIndices: [0, 1, 2, 3, 4, 6, 7, 8],
      acbSumcIndices: [0, 1, 3, 4, 5, 6, 7, 8]
    }//LPP
  ];

  function calculateGainLoss(field, tableNum, gainLossCIndex, gainOrLossOnly) {
    field(tableNum).getRows().forEach(function(row) {
      var result = row[gainLossCIndex - 3].get() - row[gainLossCIndex - 2].get() - row[gainLossCIndex - 1].get();
      if (gainOrLossOnly === 'Gain') {
        result = Math.max(0, result)
      } else if (gainOrLossOnly === 'Loss') {
        result = Math.min(0, result)
      }
      row[gainLossCIndex].assign(result)
    });
  }

  wpw.tax.create.calcBlocks('t2s6', function(calcUtils) {
    calcUtils.calc(function(calcUtils) {
      var num = '710';
      var num2 = '715';
      var midnum = '705';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / calcUtils.field(midnum + '_D').get();
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '994';
      var num2 = '899';
      var midnum = '8891';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get();
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '991';
      var num2 = '990';
      var midnum = '9011';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get();
      calcUtils.field(num2).assign(product);
    });
    //Transfer from GIFI/FS
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.calcOnFormChange('acbtracker');
      field('8210').assign(calcUtils.sumRepeatGifiValue('T2S125', 150, 8210));
      field('8210').source(field('t2s125.8210'));
      field('8211').assign(calcUtils.sumRepeatGifiValue('T2S125', 150, 8211));
      field('8211').source(field('t2s125.8211'));
      field('8212').assign(calcUtils.sumRepeatGifiValue('T2S125', 150, 8212));
      field('8212').source(field('t2s125.8212'));
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //link acbSum table current year data to s6 table part 1 to part 6
      acbMappingObjArr.forEach(function(mappingObj) {
        var t2s6TableNum = mappingObj.t2s6TableNum;
        var linkedTableNum = 'acbsum.' + mappingObj.acbSumTableNum;
        // If row in acb sum table is deleted, then we need to remove that row in s6 table
        // do that by filtering out any rows in sumTable whose link no longer exist.
        calcUtils.filterTableRow(field(t2s6TableNum), function(table, rowIndex) {
          // We return false to delete this row.
          return !calcUtils.checkLinkDeleted(table, rowIndex, table.getRowLinks(rowIndex).filter(function(link) {
            return link.type === 'src';
          }));
        });

        // Add rows to sumTable whenever new rows have been added to the linked table
        // Note that the first table passed into fillAndLinkTable is the one that will be populated with data from the
        // second table.
        calcUtils.fillAndLinkTable(field(t2s6TableNum), field(linkedTableNum));

        // We perform some calc with the linked rows to show it is actually linked
        calcUtils.forEachLinkedRow(field(t2s6TableNum), function(mainTable, mainRowInd, linkedTable, linkedRowInd) {
          var mainRow = mainTable.getRow(mainRowInd);
          var linkRow = linkedTable.getRow(linkedRowInd);
          for (var i = 0; i < mappingObj.t2s6cIndices.length; i++) {
            mainRow[mappingObj.t2s6cIndices[i]].assign(linkRow[mappingObj.acbSumcIndices[i]].get());
            mainRow[mappingObj.t2s6cIndices[i]].source(linkRow[mappingObj.acbSumcIndices[i]]);
          }
        });

        // The above functions do not guarantee the order of the rows when they get added, so we
        // have to rearrange the rows to ensure the order is correct.
        // var linkRowIdOrder = field(linkedTableNum).getRowIds();
        // calcUtils.sortTableByLinks(field(t2s6TableNum), linkRowIdOrder);
      });
    });

    //part1 table
    calcUtils.calc(function(calcUtils, field, form) {
      calculateGainLoss(field, '101', 7);
      field('151').assign(field('101').total(7).get());
      calcUtils.sumBucketValues('161', ['151', '160']);
    });

    //part2 table
    calcUtils.calc(function(calcUtils, field, form) {
      calculateGainLoss(field, '201', 5)
    });

    //part3 table
    calcUtils.calc(function(calcUtils, field, form) {
      calculateGainLoss(field, '301', 7)
    });

    //part4 table
    calcUtils.calc(function(calcUtils, field, form) {
      calculateGainLoss(field, '401', 5)
    });

    //part5 table
    calcUtils.calc(function(calcUtils, field, form) {
      calculateGainLoss(field, '501', 5, 'Gain')
    });

    //part6 table
    calcUtils.calc(function(calcUtils, field, form) {
      calculateGainLoss(field, '601', 5);
      field('650').assign(field('601').total(5).get());
      calcUtils.getGlobalValue('655', 'T2S4', '530');
      field('603').assign(field('650').get() - field('655').get());
    });

    //part  7 table
    calcUtils.calc(function(calcUtils, field) {
      calculateGainLoss(field, '700', 6, 'Loss');
      field('710').assign(field('700').total(6).get());
    });

    //part 8calcs
    calcUtils.calc(function(calcUtils, field) {
      field('800').assign(field('161').get() +
          field('201').total(5).get() +
          field('301').total(7).get() +
          field('401').total(5).get() +
          field('501').total(5).get() +
          Math.max(0, field('603').get()));
      calcUtils.sumBucketValues('820', ['810', '815']);
      calcUtils.equals('875', '820');
      field('835').assign(field('T2S13.008').get() + field('T2S13.009').get());
      calcUtils.sumBucketValues('836', ['825', '830', '835']);
      calcUtils.equals('880', '836');
      calcUtils.sumBucketValues('840', ['800', '875', '880']);
      calcUtils.getGlobalValue('855', 'T2S13', '010');
      calcUtils.equals('885', '855');
      calcUtils.subtract('890', '840', '885', true);
      calcUtils.sumBucketValues('894', ['890', '892', '893']);
    });

    //part  9 calcs
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.equals('999', '890');
      calcUtils.sumBucketValues('903', ['901', '902']);
      calcUtils.equals('895', '903');
      calcUtils.sumBucketValues('983', ['981', '982']);
      calcUtils.equals('896', '983');
      calcUtils.sumBucketValues('984', ['895', '896']);
      calcUtils.equals('985', '984');
      calcUtils.subtract('986', '999', '985', true);
      calcUtils.sumBucketValues('914', ['908', '909']);
      calcUtils.equals('897', '914');
      calcUtils.sumBucketValues('913', ['911', '912']);
      calcUtils.equals('898', '913');
      calcUtils.min('995', ['897', '898']);
      calcUtils.sumBucketValues('992', ['986', '995', '899']);
      field('994').assign(form('T2S73').field('275').get());
      field('994').source(form('T2S73').field('275'));
      field('991').assign(form('T2S73').field('285').get());
      field('991').source(form('T2S73').field('285'));
      calcUtils.subtract('989', '992', '990', true);
      if (field('989').get() < 0) {
        calcUtils.multiply('988', ['989'], -1);
        field('987').assign(0);
      } else {
        field('988').assign(0);
        field('987').assign(Math.round(field('989').get() / 2));
      }
    });
  });
})();
