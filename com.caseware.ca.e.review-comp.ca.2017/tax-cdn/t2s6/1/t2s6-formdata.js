(function() {
  'use strict';

  wpw.tax.global.formData.t2s6 = {
    formInfo: {
      abbreviation: 'T2S6',
      title: 'Summary of Dispositions of Capital Property',
      //subTitle: '(2011 and later tax years)',
      code: 'Code 1102',
      formFooterNum: 'T2 SCH 6 E (12/2014)',
      showCorpInfo: true,
      schedule: 'Schedule 6',
      headerImage: 'canada-federal',
      dynamicFormWidth: true,
      description: [
        {
          type: 'list',
          items: [
            'Use this schedule if your corporation disposed of (actual or deemed) capital property or ' +
            'claimed an allowable business investment loss (ABIL), or both, in the tax year.',
            'Also use this schedule to make a designation under paragraph 111(4)(e) of the <i> Income Tax ' +
            'Act </i> if control of the corporation has been acquired by a person or a group of persons.',
            'For more information, see the section called "<i>Schedule 6, Summary of Dispositions of Capital' +
            ' Property</i>" in Guide T4012, <i>T2 Corporation – Income Tax Guide.</i>',
            'If you need more space, attach additional schedules.']
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        'header': 'Designation under paragraph 111(4)(e) of the <i>Income Tax Act</i>',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Are any dispositions shown on this schedule related to deemed dispositions designated under paragraph 111(4)(e)?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '050',
            'tn': '050',
            'labelWidth': '80%',
            'init': '2'
          },
          {
            'label': 'If <b> yes </b>, attach a statement specifying which properties such a designation applies to. '
          }
        ]
      },
      {
        'header': 'Amount transferred from GIFI/FS',
        'rows': [
          {
            'type': 'table',
            'num': '8000'
          },
          {
            'label': '* if the total amount is positive, it will get updated to line 401 on Schedule 1'
          },
          {
            'label': '* if the total amount is negative, it will get updated to line 111 on Schedule 1 with a positive value'
          }
        ]
      },
      {
        'header': 'Part 1 - Shares',
        'spacing': 'R_tn2',
        'rows': [
          {
            'type': 'table',
            'num': '101'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total of column 8 - Gain (or Loss)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '151'
                }
              }
            ]
          },
          {
            'label': 'Total adjustment under subsection 112(3) of the Act to all losses identified in Part 1',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              }
            ]
          },
          {
            'label': 'Actual gain or loss from the disposition of shares (total of column 8 <b> plus </b> line 160)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '161'
                }
              }
            ],
            'indicator': 'A'
          }
        ]
      },
      {
        'header': 'Part 2 - Real Estate (Do not include losses on depreciable property)',
        'rows': [
          {
            'labelClass': 'fulLLength'
          },
          {
            'type': 'table',
            'num': '201'
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Bonds',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '301'
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 4 - Other properties (Do not include losses on depreciable property)',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '401'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note',
            'labelClass': 'bold'
          },
          {
            'label': 'Other property includes capital debts established as bad debts, as well as amounts that arise from foreign currency transactions.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 5 - Personal-use property (Do not include listed personal property)',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '501'
          },
          {
            'label': 'Note',
            'labelClass': 'bold'
          },
          {
            'label': 'You cannot deduct losses on dispositions of personal-use property (other than listed personal property) from your income',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 6 - Listed personal property',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '601'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total of column 6 - Gain (or loss)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '650'
                }
              }
            ]
          },
          {
            'label': '<b> Deduct: </b> Unapplied listed personal property losses from other years (amount from line 530 of Schedule 4, <i> Corporation Loss Continuity and Application </i>)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '655'
                },
                'padding': {
                  'type': 'tn',
                  'data': '655'
                }
              }
            ]
          },
          {
            'label': 'Net gains (or losses) from the disposition of listed personal property (total of column 6 <b> minus </b> line 655)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '603'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note',
            'labelClass': 'bold tabbed'
          },
          {
            'label': 'Net listed personal property losses can only be applied against listed personal property gains.',
            'labelClass': 'tabbed'
          }
        ]
      },
      {
        'header': 'Part 7 - Property qualifying for and resulting in an allowable business investment loss',

        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '700'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Allowable business investment losses (ABILs)'
          },
          {
            'type': 'table',
            'num': 'di_table_1'
          },
          {
            'label': 'Enter amount G on line 406 of Schedule 1, <i> Net Income (Loss) for Income Tax Purposes</i>.',
            'labelClass': 'fullLength'
          },
          {
            'labelCLass': 'fullLength'
          },
          {
            'label': 'Note',
            'labelClass': 'bold tabbed'
          },
          {
            'label': 'Properties listed in Part 7 should not be included in any other parts of this schedule.',
            'labelClass': 'tabbed fullLength'
          }
        ]
      },
      {
        'header': 'Part 8 – Capital gains or losses',
        'spacing': 'R_tn2',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total of amounts A to F (do not include amount F if it is a loss)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '800'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Capital gains dividend received from Canadian source',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '810'
                }
              },
              null
            ]
          },
          {
            'label': 'Capital gains dividend received from foreign source',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '815'
                }
              },
              null
            ]
          },
          {
            'label': 'Total capital gains dividend received in the year',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': true
            },
            'columns': [
              {
                'underline': 'double',
                'paddingRight': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                },
                'input': {
                  'num': '820',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '875',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '875'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Capital gains reserve opening balance from Canadian source',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '825'
                }
              },
              null
            ]
          },
          {
            'label': 'Capital gains reserve opening balance from foreign source',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '830'
                }
              },
              null
            ]
          },
          {
            'label': 'Capital gains reserve opening balance (from Part 1 of Schedule 13, <i>Continuity of Reserves</i>,' +
            ' enter the amount from line 8, <i>Balance at the beginning of the year</i> <b>plus</b> the amount from' +
            ' line 9, <i>Transfer on an amalgamation or the wind–up of a subsidiary)</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '835'
                }
              },
              null
            ]
          },
          {
            'label': 'Total capital gains reserve opening balance',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'underline': 'double',
                'paddingRight': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                },
                'input': {
                  'num': '836',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '880',
                  'paddingRight': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  },
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '880'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': 'Subtotal (total of amounts H to J)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '840'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': 'Deduct',
            'labelClass': 'bold'
          },
          {
            'label': 'Capital gains reserve closing balance from Canadian source',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '845'
                }
              },
              null
            ]
          },
          {
            'label': 'Capital gains reserve closing balance from foreign source',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '850'
                }
              },
              null
            ]
          },
          {
            'label': 'Deduct: Capital gains reserve closing balance (from Schedule 13)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'underline': 'double',
                'paddingRight': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                },
                'input': {
                  'num': '855',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '885',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '885'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': 'Capital gains or losses, excluding ABILs (amount K <b> minus </b> amount L)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '890'
                },
                'padding': {
                  'type': 'tn',
                  'data': '890'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Deemed gain/loss from partnerships:',
            'labelClass': 'bold'
          },
          {
            'label': 'Gain pursuant to paragraph 40(3.1)(a) of ITA for dispositions after October 31, 2011',
            'num': '891',
            'labelClass': 'bold',
            'type': 'infoField',
            'inputType': 'radio',
            'labelWidth': '82%'
          },
          {
            'label': 'Gain pursuant to paragraph 40(3.1)(a) of ITA for dispositions after October, 2011',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '892'
                }
              }
            ]
          },
          {
            'label': 'Loss pursuant to subsection 40(3.12) of the ITA for dispositions after October, 2011',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '893'
                }
              }
            ]
          },
          {
            'label': 'Net Capital Gain / Loss:',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '894'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 9 - Taxable capital gains and total capital losses',
        'spacing': 'R_mult_tn2',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Capital gains or losses, excluding ABILs (amount from line 890 in Part 8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '999'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'label': '<b> Deduct </b> the following amounts included in amount N, that are subject to the zero inclusion rate:'
          },
          {
            'label': 'Note',
            'labelClass': 'tabbed bold'
          },
          {
            'label': 'When a taxpayer is entitled to an advantage in respect of a donation, the zero inclusion rate is restricted to only part of the taxpayer\'s capital gain on disposition of the property. See section 38.2 of the Act for more information.',
            'labelClass': 'tabbed'
          },
          {
            'label': 'Gain on the donation to a qualified donee of a share, debt obligation, or right listed on a designated stock exchange and other securities under subparagraphs 38(a.1)(i) and (iii) of the Act',
            'labelClass': 'tabbed',
            'labelWidth': '65%'
          },
          {
            'label': 'Foreign source',
            'labelClass': ' text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '901',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Canadian source',
            'labelClass': ' text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '902'
                }
              },
              null,
              null
            ]
          },
          {
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': true
            },
            'columns': [
              {
                'underline': 'double',
                'paddingRight': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                },
                'input': {
                  'num': '903',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '895',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '895'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'Gain on the donation to a qualified donee of ecologically sensitive land under paragraph 38(a.2) of the Act* '
          },
          {
            'label': 'Foreign source',
            'labelCellClass': 'text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '981'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Canadian source',
            'labelClass': ' text-right ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '982'
                }
              },
              null,
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': true
            },
            'columns': [
              {
                'underline': 'double',
                'paddingRight': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                },
                'input': {
                  'num': '983',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '896',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '896'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount a <b>plus</b> amount b)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': true
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '984'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '985'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'label': 'Subtotal (amount N<b> minus</b> amount O)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '986'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Add: ',
            'labelClass': 'bold'
          },
          {
            'label': 'Deemed capital gain from the donation of property included in a flow-through share class of property to a qualified donee under subsection 40(12) of the Act.',
            'labelClass': 'tabbed'
          },
          {
            'label': 'Exemption threshold at time of disposition',
            'labelClass': 'tabbed'
          },
          {
            'label': 'Foreign source',
            'labelCellClass': 'text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '908'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Canadian source',
            'labelCellClass': 'text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '909'
                }
              },
              null,
              null
            ]
          },
          {
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': true
            },
            'columns': [
              {
                'underline': 'double',
                'paddingRight': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                },
                'input': {
                  'num': '914',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '897',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '897'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'The total of all capital gains from the disposition of the actual property',
            'labelClass': 'tabbed'
          },
          {
            'label': 'Foreign source of capital gains from disposition of actual property',
            'labelClass': '  text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '911'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Canadian source of capital gains from disposition of actual property',
            'labelClass': '  text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '912'
                }
              },
              null,
              null
            ]
          },
          {
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '913',
                  'paddingRight': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  },
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '898',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '898'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Amount c or amount d, whichever is less',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '995'
                }
              }
            ],
            'indicator': 'Q'
          },
          {
            'type': 'table',
            'num': 'di_table_2'
          },
          {
            'labelCLass': 'fullLength'
          },
          {
            'label': 'Subtotal (total of amounts P to R)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '992'
                }
              }
            ],
            'indicator': 'S'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': 'di_table_3'
          },
          {
            'labelClass': 'fulLLength'
          },
          {
            'label': 'Total capital gains or losses (amount S <b> minus </b> amountT)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': true
            },
            'columns': [
              {
                'input': {
                  'num': '989'
                }
              }
            ],
            'indicator': 'U'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable capital gains or total capital losses',
            'labelClass': 'bold'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total capital losses (amount U, if amount U is negative; if amount U is positive, enter "0") ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '988'
                }
              }
            ],
            'indicator': 'V'
          },
          {
            label: 'Enter amount V on line 210 of Schedule 4.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable capital gains (if amount U is positive, enter amount U multiplied by 50%; if amount U is negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': true
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '987'
                }
              }
            ],
            'indicator': 'W'
          },
          {
            'label': 'Enter amount W on line 113 of Schedule 1.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Do not include gains on donations of ecologically sensitive land to a private foundation.'
          }
        ]
      }
    ]
  };
})();
