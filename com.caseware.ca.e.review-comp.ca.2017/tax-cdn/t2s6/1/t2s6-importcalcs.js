// wpw.tax.create.importCalcs('T2S6', function(importTools) {
//
//   function getImportObj(taxPrepIdsObj) {
//     //get and sort by repeatId and key of taxPrepIdsObj
//     var output = {};
//     Object.keys(taxPrepIdsObj).forEach(function(key) {
//       importTools.intercept(taxPrepIdsObj[key], function(importObj) {
//         if (!output[importObj.rowIndex]) {
//           output[importObj.rowIndex] = {};
//         }
//         output[importObj.rowIndex][key] = importObj;
//       });
//     });
//     return output;
//   }
//
//   var acbForm = function acb() {
//     this.foreign = {
//       countryForeign: '111',
//       t1135Cat: '112',
//       xfer1135: '113'
//     };
//     this.shares = {
//       description: '101',
//       shareClass: '102',
//       table: {
//         tableId: '300',
//         transDate: 0,
//         transType: 1,
//         numDisp: 9,
//         procOfDisp: 10,
//         acbDisp: 11,
//         outlays: 12,
//         acqDate: 13
//       }
//     };
//     this.realEstate = {
//       address1: '401',
//       address2: '402',
//       city: '404',
//       prov: '403',
//       country: '405',
//       postal: '406',
//       acqDate: '411',
//       dispDate: '413',
//       procOfDisp: '414',
//       outlays: '415',
//       acb: '425'
//     };
//     this.bonds = {
//       description: '101', //not field 501, it's mapped to 101 - calcs should push this to field 501 after import
//       maturity: '502',
//       table: {
//         tableId: '510',
//         transDate: 0,
//         transType: 1,
//         numDisp: 10,
//         fvPerAsset: 11,
//         procOfDisp: 12,
//         acbDisp: 13,
//         outlays: 14,
//         acqDate: 15
//       }
//     };
//     this.standard = {
//       description: '101', //not field 501, it's mapped to 101 - calcs should push this to field 410 after import
//       acqDate: '411',
//       dispDate: '413',
//       procOfDisp: '414',
//       outlays: '415',
//       acb: '425'
//     };
//   };
//
//   //TODO: T1135: intercept t1135 tables
//
//   //intercept s6 tables
//   var s6Obj = {
//     shares: getImportObj({
//       numDisp: 'FDCAP.SLIPA[1].TtwcapA1',
//       description: 'FDCAP.SLIPA[1].TtwcapA2',
//       shareClass: 'FDCAP.SLIPA[1].TtwcapA3',
//       acqDate: 'FDCAP.SLIPA[1].TtwcapA4',
//       procOfDisp: 'FDCAP.SLIPA[1].TtwcapA5',
//       acbDisp: 'FDCAP.SLIPA[1].TtwcapA6',
//       outlays: 'FDCAP.SLIPA[1].TtwcapA7',
//       gainLoss: 'FDCAP.SLIPA[1].TtwcapA8',
//       xfer1135: 'FDCAP.SLIPA[1].TtwcapA9',
//       t1135Cat: 'FDCAP.SLIPA[1].TtwcapA25',
//       countryForeign: 'FDCAP.SLIPA[1].TtwcapA26'
//     }),
//     realEstate: getImportObj({
//       address1: 'FDCAP.SLIPB[1].TtwcapB1',
//       address2: 'FDCAP.SLIPB[1].TtwcapB2',
//       city: 'FDCAP.SLIPB[1].TtwcapB3',
//       prov: 'FDCAP.SLIPB[1].TtwcapB4',
//       country: 'FDCAP.SLIPB[1].TtwcapB5',
//       postal: 'FDCAP.SLIPB[1].TtwcapB6',
//       zip: 'FDCAP.SLIPB[1].TtwcapB7',
//       acqDate: 'FDCAP.SLIPB[1].TtwcapB8',
//       procOfDisp: 'FDCAP.SLIPB[1].TtwcapB9',
//       acb: 'FDCAP.SLIPB[1].TtwcapB10',
//       outlays: 'FDCAP.SLIPB[1].TtwcapB11',
//       gainLoss: 'FDCAP.SLIPB[1].TtwcapB12',
//       xfer1135: 'FDCAP.SLIPB[1].TtwcapB13',
//       t1135Cat: 'FDCAP.SLIPB[1].TtwcapB25',
//       countryForeign: 'FDCAP.SLIPB[1].TtwcapB26'
//     }),
//     bonds: getImportObj({
//       faceValue: 'FDCAP.SLIPC[1].TtwcapC1',
//       maturity: 'FDCAP.SLIPC[1].TtwcapC2',
//       description: 'FDCAP.SLIPC[1].TtwcapC3',
//       acqDate: 'FDCAP.SLIPC[1].TtwcapC4',
//       procOfDisp: 'FDCAP.SLIPC[1].TtwcapC5',
//       acbDisp: 'FDCAP.SLIPC[1].TtwcapC6',
//       outlays: 'FDCAP.SLIPC[1].TtwcapC7',
//       gainLoss: 'FDCAP.SLIPC[1].TtwcapC8',
//       xfer1135: 'FDCAP.SLIPC[1].TtwcapC9',
//       t1135Cat: 'FDCAP.SLIPC[1].TtwcapC25',
//       countryForeign: 'FDCAP.SLIPC[1].TtwcapC26'
//     }),
//     other: getImportObj({
//       description: 'FDCAP.SLIPD[1].TtwcapD1',
//       acqDate: 'FDCAP.SLIPD[1].TtwcapD2',
//       procOfDisp: 'FDCAP.SLIPD[1].TtwcapD3',
//       acb: 'FDCAP.SLIPD[1].TtwcapD4',
//       outlays: 'FDCAP.SLIPD[1].TtwcapD5',
//       gainLoss: 'FDCAP.SLIPD[1].TtwcapD6',
//       xfer1135: 'FDCAP.SLIPD[1].TtwcapD7',
//       t1135Cat: 'FDCAP.SLIPD[1].TtwcapD25',
//       countryForeign: 'FDCAP.SLIPD[1].TtwcapD26'
//     }),
//     pup: getImportObj({
//       description: 'FDCAP.SLIPE[1].TtwcapE1',
//       acqDate: 'FDCAP.SLIPE[1].TtwcapE2',
//       procOfDisp: 'FDCAP.SLIPE[1].TtwcapE3',
//       acb: 'FDCAP.SLIPE[1].TtwcapE4',
//       outlays: 'FDCAP.SLIPE[1].TtwcapE5',
//       gainLoss: 'FDCAP.SLIPE[1].TtwcapE6',
//       xfer1135: 'FDCAP.SLIPE[1].TtwcapE7'
//     }),
//     lpp: getImportObj({
//       description: 'FDCAP.SLIPF[1].TtwcapF1',
//       acqDate: 'FDCAP.SLIPF[1].TtwcapF2',
//       procOfDisp: 'FDCAP.SLIPF[1].TtwcapF3',
//       acb: 'FDCAP.SLIPF[1].TtwcapF4',
//       outlays: 'FDCAP.SLIPF[1].TtwcapF5',
//       gainLoss: 'FDCAP.SLIPF[1].TtwcapF6',
//       xfer1135: 'FDCAP.SLIPF[1].TtwcapF7'
//     })
//   };
//
//   importTools.after(function() {
//     var rfId = 0;
//     var s6 = importTools.form('T2S6');
//     var acbObj;
//     var acbFormIds;
//     var acbTracker;
//     var category;
//
//     function isCurrentYear(date) {
//       //returns true if within current year
//       return wpw.tax.utilities.dateCompare.between(date,
//           importTools.form('T2J').getValue('060'),
//           importTools.form('T2J').getValue('061'));
//     }
//
//     function getFormVal(form, obj) {
//       //gets the imported values where obj has a bunch of fieldIds
//       //if no value is being imported, the corresponding key is not included.
//       var returnObj = {};
//       Object.keys(obj).forEach(function(fieldKey) {
//         if (fieldKey == 'table') {
//           //getTable if table
//           returnObj[fieldKey] = getTableVal(form, obj[fieldKey]);
//         }
//         else if (!wpw.tax.utilities.isNullUndefined(obj[fieldKey]) && !wpw.tax.utilities.isNullUndefined(form.getValue(obj[fieldKey])) &&
//             typeof obj[fieldKey] != 'object') {
//           //get Value if fieldId
//           returnObj[fieldKey] = form.getValue(obj[fieldKey]);
//         }
//       });
//       return returnObj;
//     }
//
//     function getTableVal(form, obj) {
//       //gets the imported values where obj has a bunch of fieldIds
//       //if no value is being imported, the corresponding key is not included.
//       //specifically, organizes data by rows.
//       var returnObj = {};
//       var row = 0;
//       var size = form.size(obj['tableId']);
//       while (row < size) {
//         Object.keys(obj).forEach(function(fieldKey) {
//           if (fieldKey != 'tableId' && !wpw.tax.utilities.isNullUndefined(form.getValue(obj['tableId'], obj[fieldKey], row))) {
//             if (!returnObj[row]) {
//               returnObj[row] = {};
//             }
//             returnObj[row][fieldKey] = form.getValue(obj['tableId'], obj[fieldKey], row);
//           }
//         });
//         row++;
//       }
//       return returnObj;
//     }
//
//     function compareVal(arg1, arg2, round) {
//       //compares imported values, arg1 and arg2 can be strings, or importableObjs
//       if (wpw.tax.utilities.isNullUndefined(arg1) && wpw.tax.utilities.isNullUndefined(arg2)) {
//         return true;
//       }
//       else if (wpw.tax.utilities.isNullUndefined(arg1) || wpw.tax.utilities.isNullUndefined(arg2)) {
//         return false;
//       }
//       arg1 = getValue(arg1);
//       arg2 = getValue(arg2);
//       if (round && typeof arg1 == 'string' && typeof arg2 == 'string') {
//         arg1 = roundVal(arg1);
//         arg2 = roundVal(arg2);
//       }
//       return arg1 == arg2;
//     }
//
//     function getValue(obj) {
//       //gets the value of object / importObj
//       if (typeof obj != 'object') {
//         return obj;
//       }
//       else {
//         if (!wpw.tax.utilities.isNullUndefined(obj['value'])) {
//           return getValue(obj['value']);
//         }
//         else if (Object.keys(obj).length > 0) {
//           return getValue(obj[Object.keys(obj)[0]]);
//         }
//         else {
//           return undefined;
//         }
//       }
//     }
//
//     function roundVal(arg) {
//       //used in rounding number values from taxPrep ImportObj. Also strips commas.
//       return Math.round(parseFloat(arg.replace(/[^\d.]/g, ''))).toString()
//     }
//
//     function getPropIfExists(obj, properties) {
//       //recursively return either the object if some multilayer property exists, or an empty string
//       //i.e. if foo[1][2][3], return it; if not, return ''
//       properties = wpw.tax.utilities.ensureArray(properties, false);
//       return properties.length < 1 ? (obj || '') : getPropIfExists((obj || '')[properties[0]], properties.splice(1));
//     }
//
//     //match values from s6 imported values to acb imported values, remove from s6 to import to manual tables
//
//     while (rfId < importTools.allRepeatForms('ACBTRACKER').length) {
//       var s6RowInd = -1;
//       acbObj = new acbForm();
//       acbTracker = importTools.form('ACBTRACKER', rfId);
//       category = acbTracker.getValue('100');
//
//       acbObj['foreign'] = getFormVal(acbTracker, acbObj['foreign']);
//
//       switch (category) {
//         case '1':
//           //stocks
//           acbObj['shares'] = getFormVal(acbTracker, acbObj['shares']);
//           var sharesTable = acbObj['shares']['table'];
//
//           Object.keys(sharesTable).forEach(function(acbRow) {
//             if (sharesTable && isCurrentYear(sharesTable[acbRow]['transDate'])
//                 && sharesTable[acbRow]['transType'] == '2') {
//               //find the row that got transferred to s6
//               s6RowInd = -1;
//               Object.keys(s6Obj['shares']).forEach(function(s6Row) {
//                 if (compareVal(sharesTable[acbRow]['numDisp'], s6Obj['shares'][s6Row]['numDisp'], true) &&
//                     compareVal(acbObj['shares']['description'], s6Obj['shares'][s6Row]['description']) &&
//                     //compareVal(acbObj['shares']['shareClass'], s6Obj['shares'][s6Row]['shareClass']) && //not in TP ACB
//                     //compareVal(sharesTable[acbRow]['acqDate'], s6Obj['shares'][s6Row]['acqDate']) && //not in TP ACB
//                     compareVal(sharesTable[acbRow]['procOfDisp'], s6Obj['shares'][s6Row]['procOfDisp'], true) &&
//                     compareVal(sharesTable[acbRow]['acbDisp'], s6Obj['shares'][s6Row]['acbDisp'], true) &&
//                     //compareVal(sharesTable[acbRow]['outlays'], s6Obj['shares'][s6Row]['outlays']) && //not in TP ACB
//                     //compareVal(acbObj['foreign']['xfer1135'], s6Obj['shares'][s6Row]['xfer1135']) &&
//                     compareVal(acbObj['foreign']['t1135Cat'], s6Obj['shares'][s6Row]['t1135Cat']) &&
//                     compareVal(acbObj['foreign']['countryForeign'], s6Obj['shares'][s6Row]['countryForeign'])) {
//                   s6RowInd = s6Row;
//                 }
//               });
//
//               if (s6RowInd >= 0) {
//                 //if linked row,
//                 //assign values to corpTax ACB from s6 row
//                 acbFormIds = new acbForm();
//                 acbTracker.setValue(acbFormIds['shares']['shareClass'],
//                     getPropIfExists(s6Obj, ['shares', s6RowInd, 'shareClass', 'value']));
//                 acbTracker.setValue(acbFormIds['shares']['table']['tableId'],
//                     getPropIfExists(s6Obj, ['shares', s6RowInd, 'acqDate', 'value']),
//                     acbFormIds['shares']['table']['acqDate'], acbRow);
//                 acbTracker.setValue(acbFormIds['shares']['table']['tableId'],
//                     getPropIfExists(s6Obj, ['shares', s6RowInd, 'outlays', 'value']),
//                     acbFormIds['shares']['table']['outlays'], acbRow);
//                 //todo: T1135: find the linked row and remove from import
//                 //delete if matched on s6 import
//                 delete s6Obj['shares'][s6RowInd];
//               }
//             }
//           });
//           break;
//         case '2':
//           //real estate
//           acbObj['realEstate'] = getFormVal(acbTracker, acbObj['realEstate']);
//           if (acbObj['realEstate']['dispDate'] && isCurrentYear(acbObj['realEstate']['dispDate'])) {
//             //find the row that got transferred to s6
//             s6RowInd = -1;
//             Object.keys(s6Obj['realEstate']).forEach(function(s6Row) {
//               if (compareVal(getValue(acbObj['realEstate']['address1']), s6Obj['realEstate'][s6Row]['address1']) &&
//                   compareVal(getValue(acbObj['realEstate']['address2']), s6Obj['realEstate'][s6Row]['address2']) &&
//                   compareVal(getValue(acbObj['realEstate']['city']), s6Obj['realEstate'][s6Row]['city']) &&
//                   compareVal(getValue(acbObj['realEstate']['prov']), s6Obj['realEstate'][s6Row]['prov']) &&
//                   compareVal(getValue(acbObj['realEstate']['country']), s6Obj['realEstate'][s6Row]['country']) &&
//                   compareVal(getValue(acbObj['realEstate']['postal']), s6Obj['realEstate'][s6Row]['postal']) &&
//                   //compareVal(acbObj['realEstate']['acqDate'], s6Obj['realEstate'][s6Row]['acqDate']) &&
//                   compareVal(acbObj['realEstate']['procOfDisp'], s6Obj['realEstate'][s6Row]['procOfDisp'], true) &&
//                   compareVal(acbObj['realEstate']['acb'], s6Obj['realEstate'][s6Row]['acb'], true) &&
//                   //compareVal(acbObj['realEstate']['outlays'], s6Obj['realEstate'][s6Row]['outlays']) &&
//                   //compareVal(acbObj['foreign']['xfer1135'], s6Obj['realEstate'][s6Row]['xfer1135']) &&
//                   compareVal(acbObj['foreign']['t1135Cat'], s6Obj['realEstate'][s6Row]['t1135Cat']) &&
//                   compareVal(acbObj['foreign']['countryForeign'], s6Obj['realEstate'][s6Row]['countryForeign'])) {
//                 s6RowInd = s6Row;
//               }
//             });
//
//             if (s6RowInd >= 0) {
//               //if linked row,
//               //assign values to corpTax ACB from s6 row
//               acbFormIds = new acbForm();
//
//               acbTracker.setValue(acbFormIds['realEstate']['acqDate'],
//                   getPropIfExists(s6Obj, ['realEstate', s6RowInd, 'acqDate', 'value']));
//               acbTracker.setValue(acbFormIds['realEstate']['outlays'],
//                   getPropIfExists(s6Obj, ['realEstate', s6RowInd, 'outlays', 'value']));
//               //todo: T1135: find the linked row and remove from import
//               //delete if matched on s6 import
//               delete s6Obj['realEstate'][s6RowInd];
//             }
//           }
//           break;
//         case '3':
//           //bonds
//           acbObj['bonds'] = getFormVal(acbTracker, acbObj['bonds']);
//           var bondsTable = acbObj['bonds']['table'];
//
//           Object.keys(bondsTable).forEach(function(acbRow) {
//             if (bondsTable && isCurrentYear(bondsTable[acbRow]['transDate'])
//                 && bondsTable[acbRow]['transType'] == '2') {
//               //find the row that got transferred to s6
//               s6RowInd = -1;
//               Object.keys(s6Obj['bonds']).forEach(function(s6Row) {
//                 var fv = undefined;
//                 if (bondsTable[acbRow]['numDisp'] && bondsTable[acbRow]['fvPerAsset']) {
//                   fv = (parseFloat(roundVal(bondsTable[acbRow]['numDisp'])) *
//                   parseFloat(roundVal(bondsTable[acbRow]['fvPerAsset']))).toString();
//                 }
//                 if (compareVal(fv, s6Obj['bonds'][s6Row]['faceValue'], true) &&
//                     //compareVal(acbObj['bonds']['maturity'], s6Obj['bonds'][s6Row]['maturity']) &&
//                     compareVal(acbObj['bonds']['description'], s6Obj['bonds'][s6Row]['description']) &&
//                     //compareVal(bondsTable[acbRow]['acqDate'], s6Obj['bonds'][s6Row]['acqDate']) &&
//                     compareVal(bondsTable[acbRow]['procOfDisp'], s6Obj['bonds'][s6Row]['procOfDisp'], true) &&
//                     compareVal(bondsTable[acbRow]['acbDisp'], s6Obj['bonds'][s6Row]['acbDisp'], true) &&
//                     //compareVal(bondsTable[acbRow]['outlays'], s6Obj['bonds'][s6Row]['outlays']) &&
//                     //compareVal(acbObj['foreign']['xfer1135'], s6Obj['bonds'][s6Row]['xfer1135']) &&
//                     compareVal(acbObj['foreign']['t1135Cat'], s6Obj['bonds'][s6Row]['t1135Cat']) &&
//                     compareVal(acbObj['foreign']['countryForeign'], s6Obj['bonds'][s6Row]['countryForeign'])) {
//                   s6RowInd = s6Row;
//                 }
//               });
//
//               if (s6RowInd >= 0) {
//                 //if linked row,
//                 //assign values to corpTax ACB from s6 row
//                 acbFormIds = new acbForm();
//                 acbTracker.setValue('501',
//                     getPropIfExists(s6Obj, ['bonds', s6RowInd, 'description', 'value']));
//                 acbTracker.setValue('101', ''); //description
//                 acbTracker.setValue(acbFormIds['bonds']['maturity'],
//                     getPropIfExists(s6Obj, ['bonds', s6RowInd, 'maturity', 'value']));
//                 acbTracker.setValue(acbFormIds['bonds']['table']['tableId'],
//                     getPropIfExists(s6Obj, ['bonds', s6RowInd, 'acqDate', 'value']),
//                     acbFormIds['bonds']['table']['acqDate'], acbRow);
//                 acbTracker.setValue(acbFormIds['bonds']['table']['tableId'],
//                     getPropIfExists(s6Obj, ['bonds', s6RowInd, 'outlays', 'value']),
//                     acbFormIds['bonds']['table']['outlays'], acbRow);
//                 //todo: T1135: find the linked row and remove from import
//                 //delete if matched on s6 import
//                 delete s6Obj['bonds'][s6RowInd];
//               }
//             }
//           });
//           break;
//         default:
//           //other, pup, lpp
//           acbObj['standard'] = getFormVal(acbTracker, acbObj['standard']);
//           if (acbObj['standard']['dispDate'] && isCurrentYear(acbObj['standard']['dispDate'])) {
//             category = (category == 4) ? 'other' : (category == 5) ? 'pup' : 'lpp';
//             //find the row that got transferred to s6
//             s6RowInd = -1;
//             Object.keys(s6Obj[category]).forEach(function(s6Row) {
//               if (compareVal(acbObj['standard']['description'], s6Obj[category][s6Row]['description']) &&
//                   //compareVal(acbObj['other']['acqDate'], s6Obj[category][s6Row][1]) &&
//                   compareVal(acbObj['standard']['procOfDisp'], s6Obj[category][s6Row]['procOfDisp'], true) &&
//                   compareVal(acbObj['standard']['acb'], s6Obj[category][s6Row]['acb'], true) &&
//                   //compareVal(acbObj['other']['outlays'], s6Obj[category][s6Row]['outlays']) &&
//                   //compareVal(acbObj['foreign']['xfer1135'], s6Obj[category][s6Row]['xfer1135']) &&
//                   compareVal(acbObj['foreign']['t1135Cat'], s6Obj[category][s6Row]['t1135Cat']) &&
//                   compareVal(acbObj['foreign']['countryForeign'], s6Obj[category][s6Row]['countryForeign'])) {
//                 s6RowInd = s6Row;
//               }
//             });
//
//             if (s6RowInd >= 0) {
//               //assign values to corpTax ACB from s6 row
//               //if linked row,
//               acbFormIds = new acbForm();
//               acbTracker.setValue('407',
//                   getPropIfExists(s6Obj, [category, s6RowInd, 'description', 'value']));
//               acbTracker.setValue('101', ''); //description
//               acbTracker.setValue(acbFormIds['standard']['acqDate'],
//                   getPropIfExists(s6Obj, [category, s6RowInd, 'acqDate', 'value']));
//               acbTracker.setValue(acbFormIds['standard']['outlays'],
//                   getPropIfExists(s6Obj, [category, s6RowInd, 'outlays', 'value']));
//               //todo: T1135: find the linked row and remove from import
//               //delete if matched on s6 import
//               delete s6Obj[category][s6RowInd];
//             }
//           }
//       }
//       rfId++;
//     }
//
//     //todo: T1135: push out the remaining entries to t1135
//
//     //push out the remaining entries to s6
//     var exportObj = {
//       shares: '102',
//       realEstate: '202',
//       bonds: '302',
//       other: '402',
//       pup: '502',
//       lpp: '602'
//     };
//     //used to define the output col
//     var colsObj = {
//       shares: {
//         numDisp: 0,
//         description: 1,
//         shareClass: 2,
//         acqDate: 3,
//         procOfDisp: 4,
//         acbDisp: 5,
//         outlays: 6,
//         gainLoss: 7,
//         xfer1135: 8,
//         t1135Cat: 9,
//         countryForeign: 10
//       },
//       realEstate: {
//         address1: 0,
//         address2: 0,
//         city: 0,
//         prov: 0,
//         country: 0,
//         postal: 0,
//         zip: 0,
//         acqDate: 1,
//         procOfDisp: 2,
//         acb: 3,
//         outlays: 4,
//         gainLoss: 5,
//         xfer1135: 6,
//         t1135Cat: 7,
//         countryForeign: 8
//       },
//       bonds: {
//         faceValue: 0,
//         maturity: 1,
//         description: 2,
//         acqDate: 3,
//         procOfDisp: 4,
//         acbDisp: 5,
//         outlays: 6,
//         gainLoss: 7,
//         xfer1135: 8,
//         t1135Cat: 9,
//         countryForeign: 10
//       },
//       other: {
//         description: 0,
//         acqDate: 1,
//         procOfDisp: 2,
//         acb: 3,
//         outlays: 4,
//         gainLoss: 5,
//         xfer1135: 6,
//         t1135Cat: 7,
//         countryForeign: 8
//       },
//       pup: {
//         description: 0,
//         acqDate: 1,
//         procOfDisp: 2,
//         acb: 3,
//         outlays: 4,
//         gainLoss: 5,
//         xfer1135: 6
//       },
//       lpp: {
//         description: 0,
//         acqDate: 1,
//         procOfDisp: 2,
//         acb: 3,
//         outlays: 4,
//         gainLoss: 5,
//         xfer1135: 6
//       }
//     };
//     var col;
//     var mapType;
//     Object.keys(exportObj).forEach(function(table) {
//       Object.keys(s6Obj[table]).forEach(function(rowKey, row) {
//         Object.keys(s6Obj[table][rowKey]).forEach(function(colKey) {
//           col = colsObj[table][colKey];
//           mapType = colKey == 'address1' ?
//               'ADDRESS_1' : colKey == 'address2' ?
//                   'ADDRESS_2' : colKey == 'city' ?
//                       'ADDRESS_CITY' : colKey == 'prov' ?
//                           'ADDRESS_PROV' : colKey == 'country' ?
//                               'ADDRESS_COUNTRY' : colKey == 'postal' || colKey == 'zip' ?
//                                   'ADDRESS_AREA' : false;
//           if (mapType) {
//             s6.setValue(exportObj[table], s6Obj[table][rowKey][colKey]['value'], col, row, mapType);
//           }
//           else {
//             s6.setValue(exportObj[table], s6Obj[table][rowKey][colKey]['value'], col, row);
//           }
//         });
//       });
//     });
//   });
// });
