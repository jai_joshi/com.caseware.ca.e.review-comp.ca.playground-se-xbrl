(function () {
  wpw.tax.create.formData('t2a21', {
    formInfo: {
      abbreviation: 't2a21',
      title: 'Alberta Calculation Of Current Year Loss And Continuity Of Losses',
      schedule: 'Schedule 21',
      formFooterNum: 'AT173 (Mar-14)',
      // headerImage: 'canada-alberta',
      showCorpInfo: true,
      category: 'Alberta Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: 'This schedule is required if the opening balance or the claim for Alberta purposes differs from that for federal purposes.',
            labelClass: 'bold'
          },
          {
            label: 'The corporation may choose whether or not to deduct an available loss from income in a ' +
            'taxation year. It can deduct losses in any order. However, for each type of loss, ensure that the ' +
            'oldest loss is deducted first. See Guide for further information.',
            labelClass: 'fullLength'
          },
          {
            label: 'Report all monetary amounts in dollars; DO NOT include cents.'
          }
        ]
      },
      {
        header: 'CALCULATION OF CURRENT YEAR NON-CAPITAL LOSS',
        rows: [
          {
            "label": "Net Income (loss) per Alberta Schedule 12 line 054",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "001"
                },
                "padding": {
                  "type": "tn",
                  "data": "001"
                }
              }
            ]
          },
          {
            label: 'Deduct:'
          },
          {
            "label": "Net capital losses deducted in the year<br>(enter as a positive amount)",
            labelCellClass: 'indent-1',
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "003"
                },
                "padding": {
                  "type": "tn",
                  "data": "003"
                }
              }, null
            ]
          },
          {
            "label": "Taxable dividends deductible",
            labelCellClass: 'indent-1',
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "005"
                },
                "padding": {
                  "type": "tn",
                  "data": "005"
                }
              }, null
            ]
          },
          {
            "label": "Amount of Part VI.1 tax deductible",
            labelCellClass: 'indent-1',
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "007"
                },
                "padding": {
                  "type": "tn",
                  "data": "007"
                }
              }, null
            ]
          },
          {
            "label": "Amount deductible as prospector's and grubstaker's shares",
            labelCellClass: 'indent-1',
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "011"
                },
                "padding": {
                  "type": "tn",
                  "data": "011"
                }
              }, null
            ]
          },
          {
            "label": "Subtotal of lines 003 to 011",
            labelCellClass: 'alignRight',
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "013"
                },
                "padding": {
                  "type": "tn",
                  "data": "013"
                }
              }
            ]
          },
          {
            "label": "Line 001 - line 013: (if positive, enter \"0\")",
            labelCellClass: 'alignRight',
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "015"
                },
                "padding": {
                  "type": "tn",
                  "data": "015"
                }
              }
            ]
          },
          {
            "label": "Deduct: ITA section 110.5 or subparagraph 115(1)(a)(vii) additions for foreign tax credits<br><b>Carry forward to Schedule 12, line 082</b>",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "017"
                },
                "padding": {
                  "type": "tn",
                  "data": "017"
                }
              }
            ]
          },
          {
            "label": "Add: Current year farm loss",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "019"
                },
                "padding": {
                  "type": "tn",
                  "data": "019"
                }
              }
            ]
          },
          {
            "label": "<b>Non-capital loss for the current year: Line 015 - 017 + 019 (if positive, enter \"0\")</b><br> " +
            "<i>If negative, enter this amount into line 037 as a positive</i>",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "021"
                },
                "padding": {
                  "type": "tn",
                  "data": "021"
                }
              }
            ]
          }
        ]
      },
      {
        header: 'CONTINUITY OF LOSSES:',
        rows: [
          {type: 'table', num: '999'},
          {
            label: '* A non capital loss expires after 7 taxation years if it arose in a taxation year ending ' +
            'before March 23, 2004 or after 10 taxation years if it arose in a\n' +
            ' taxation year ending after March 22, 2004, and before 2006 or after 20 years if it arose in a ' +
            'taxation year after 2005',
            labelClass: 'fullLength'
          },
          {
            label: ' ** An allowable business investment loss becomes a net capital loss after ' +
            '7 taxation years if it arose in a taxation year ending before March 23, 2004 or ' +
            'after 10 taxation years if it arose in a taxation year ending after March 22, 2004.',
            labelClass: 'fullLength'
          },
          {type: 'horizontalLine'},
          {type: 'table', num: '998'},
          {type: 'horizontalLine'},
          {type: 'table', num: '997'}
        ]
      },
      {
        header: 'CONTINUITY OF LOSSES:',
        rows: [
          {type: 'table', num: '996'}
        ]
      },
      {
        header: 'Analysis of balance of non-capital losses by year of origin',
        rows: [
          {type: 'table', num: '995'}
        ]
      },
      {
        header: 'Analysis of balance of losses by year of origin',
        rows: [
          {type: 'table', num: '994'},
          {label: '* A farm loss or restricted farm loss expires as follows:'},
          {label: '&#9679; after 10 tax years if it arose in a tax year ending before 2006; and '},
          {label: '&#9679; after 20 tax years if it arose in a tax year ending after 2005.'}
        ]
      }
    ]
  });
})();
