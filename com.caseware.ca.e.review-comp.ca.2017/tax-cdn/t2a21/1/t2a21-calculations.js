(function () {

  function enableField(field, numArray) {
    for (var i = 0; i < numArray.length; i++) {
      field(numArray[i]).disabled(false);
    }
  }

  function fillArray(array, start, end, suffix) {
    for (var i = start; i <= end; i++) {
      if (suffix)
        array.push(i + suffix);
      else
        array.push(i)
    }
  }

  wpw.tax.create.calcBlocks('t2a21', function (calcUtils) {
    calcUtils.calc(function (calcUtils, field, form) {
      calcUtils.getGlobalValue('001', 'T2A12', '054');
      var capitalLossAmount = field('T2J.332').get();
      field('003').assign(capitalLossAmount > 0 ? capitalLossAmount : (capitalLossAmount * (-1)));
      field('003').source(field('T2J.332'));
      calcUtils.getGlobalValue('005', 'T2J', '320');
      calcUtils.getGlobalValue('007', 'T2J', '325');
      calcUtils.getGlobalValue('011', 'T2J', '350');

      field('013').assign(
          field('003').get() +
          field('005').get() +
          field('007').get() +
          field('011').get()
      );
      field('015').assign(Math.min(0, field('001').get() - field('013').get()));
      field('019').assign(Math.min(field('T2S4.310').get() - field('015').get()));
      field('021').assign(Math.min(0, field('015').get() - field('017').get() + field('019').get()));
      field('037').assign(field('021').get() * (-1));
      field('033').assign(field('031').get() - field('032').get());
      field('047').assign(field('T2A10.004').get() + field('T2A10.006').get() + field('T2A10.008').get());
      field('047').source(field('T2A10.004'));

      var isA18Exist;
      var a18FieldsArr = [];
      fillArray(a18FieldsArr, '054', '072');
      for (var i = 0; i< a18FieldsArr.length; i++) {
        if (form('t2a18').field(a18FieldsArr[i]).get() !== 0) {
          isA18Exist = true;
          break;
        }
      }

      if (isA18Exist) {
        field('057').assign(field('T2A18.074').get() < 0 ? field('T2A18.074').get() * (-1) : 0);
      } else {
        field('057').assign(Math.max((field('T2S6.890').get() - field('T2S6.895').get()) * (-1), 0));
      }

      if (field('057').get() > 0) {
        field('061').assign(0);
      }
      //todo: do optimization calcs to calculate amount to apply:
      //amount apply cannot be greater than calcualted value below
      /*else {
        if (isA18Exist) {
          if (field('T2A18.059').get() >= 0) {
            field('061').assign(field('T2A18.054').get() + field('T2A18.055').get() +
                field('T2A18.056').get() + field('T2A18.057').get() +
                field('T2A18.058').get() + field('T2A18.059').get() -
                field('T2A18.060').get() + field('T2A18.064').get() +
                field('T2A18.066').get() - field('T2A18.068').get() -
                field('T2A18.072').get()
            );
          } else {
            field('061').assign(field('T2A18.054').get() +
                field('T2A18.055').get() + field('T2A18.056').get() +
                field('T2A18.057').get() + field('T2A18.058').get() +
                field('T2A18.064').get() + field('T2A18.066').get() -
                field('T2A18.068').get() - field('T2A18.072').get()
            );
          }
        } else {
          field('061').assign(field('T2S6.890').get() - field('T2S6.895').get());
          field('061').source(field('T2S6.890'));
        }
      }*/
      field('067').assign(field('T2A10.044').get() + field('T2A10.046').get() + field('T2A10.048').get());
      field('067').source(field('T2A10.044'));
    });

    calcUtils.calc(function (calcUtils, field) {
      if (field('030y').get()) {
        calcUtils.getGlobalValue('031', 'T2S4', '897');
        calcUtils.getGlobalValue('032', 'T2S4', '100');
        calcUtils.getGlobalValue('035', 'T2S4', '105');
        calcUtils.getGlobalValue('041', 'T2S4', '130');
        calcUtils.getGlobalValue('043', 'T2S4', '140');
        calcUtils.getGlobalValue('045', 'T2S4', '150');
      } else {
        enableField(field, ['031', '032', '035', '041', '043', '045']);
      }
      if (field('050y').get()) {
        calcUtils.getGlobalValue('051', 'T2S4', '200');
        calcUtils.getGlobalValue('055', 'T2S4', '205');
        calcUtils.getGlobalValue('059', 'T2S4', '220');
        calcUtils.getGlobalValue('063', 'T2S4', '240')
        calcUtils.getGlobalValue('065', 'T2S4', '250');
      } else {
        enableField(field, ['051', '055', '059', '063', '065']);
      }

      for (var i = 1; i < 3; i++) {
        field('999').cell(7, i).assign(
            field('999').cell(1, i).get() -
            field('999').cell(2, i).get() +
            field('999').cell(4, i).get() +
            field('999').cell(5, i).get() +
            field('999').cell(6, i).get()
        );
        field('999').cell(13, i).assign(
            field('999').cell(7, i).get() -
            field('999').cell(8, i).get() -
            field('999').cell(9, i).get() -
            field('999').cell(10, i).get() -
            field('999').cell(11, i).get() -
            field('999').cell(12, i).get()
        );
        field('073').assign(field('071').get() - field('072').get());
        field('077').assign(field('019').get());
        if (field('070y').get()) {
          calcUtils.getGlobalValue('071', 'T2S4', '876');
          calcUtils.getGlobalValue('072', 'T2S4', '300');
          calcUtils.getGlobalValue('075', 'T2S4', '305');
          calcUtils.getGlobalValue('079', 'T2S4', '330');
          calcUtils.getGlobalValue('081', 'T2S4', '340');
          calcUtils.getGlobalValue('083', 'T2S4', '350');
        } else {
          enableField(field, ['071', '072', '075', '079', '081', '083']);
        }
        field('085').assign(field('T2A10.014').get() + field('T2A10.016').get() + field('T2A10.018').get());
        field('085').source(field('T2A10.014'));
        field('093').assign(field('091').get() - field('092').get());
        if (field('090y').get()) {
          calcUtils.getGlobalValue('091', 'T2S4', '859');
          calcUtils.getGlobalValue('092', 'T2S4', '440');
          calcUtils.getGlobalValue('095', 'T2S4', '405');
          calcUtils.getGlobalValue('097', 'T2S4', '410');
          calcUtils.getGlobalValue('099', 'T2S4', '430')
          calcUtils.getGlobalValue('101', 'T2S4', '440');
          field('105').assign(
              Math.min(field('T2S4.941').get() + field('T2S4.942').get() + field('T2S4.943').get(),
                  field('093').get() + field('095').get() + field('097').get() - field('099').get() - field('101').get() - field('103').get()
              )
          );
        } else {
          enableField(field, ['091', '092', '095', '097', '099', '101', '105']);
        }
        if (field('T2A10.023').get() && !field('T2A10.025').get()) {
          field('105').assign(field('T2A10.034').get() + field('T2A10.036').get() + field('T2A10.038').get());
          field('105').source(field('T2A10.034'));
        }
        if (field('T2A10.023').get() && field('T2A10.025').get()) {
          field('105').assign(Math.min(field('T2A10.034').get() + field('T2A10.036').get() + field('T2A10.038').get(), field('105').get()));
        }

        field('998').cell(6, i).assign(
            field('998').cell(3, i).get() +
            field('998').cell(4, i).get() +
            field('998').cell(5, i).get()
        );
        field('998').cell(12, i).assign(
            field('998').cell(6, i).get() -
            field('998').cell(7, i).get() -
            field('998').cell(8, i).get() -
            field('998').cell(9, i).get() -
            field('998').cell(10, i).get() -
            field('998').cell(11, i).get()
        );
      }
    });

    calcUtils.calc(function (calcUtils, field) {
      field('115').assign(field('111').get() - field('113').get());
      if (field('100y').get()) {
        calcUtils.getGlobalValue('111', 'T2S4', '549');
        calcUtils.getGlobalValue('113', 'T2S4', '500');
        calcUtils.getGlobalValue('121', 'T2S4', '550');
        field('123').assign(
            Math.min(field('T2S4.961').get() + field('T2S4.962').get() + field('T2S4.963').get(),
                field('115').get() + field('117').get() - field('119').get() - field('121').get()
            )
        );
      } else {
        enableField(field, ['111', '113', '121', '123']);
      }
      if (wpw.tax.utilities.flagger().T2A18) {
        field('117').assign(Math.max((field('T2A18.012').get() - field('T2A18.032').get() - field('T2A18.052').get()) * (-1), 0));
      } else {
        field('117').assign(field('T2S4.510').get());
      }
      if (field('117').get() > 0) {
        field('119').assign(0);
      } else {
        if (wpw.tax.utilities.flagger().T2A18) {
          field('119').assign(field('T2A18.060').get());
        } else {
          field('119').assign(field('T2S6.655').get());
        }
      }
      if (!field('T2A10.023').get() && field('T2A10.025').get()) {
        field('123').assign(field('T2A10.034').get() + field('T2A10.036').get() + field('T2A10.038').get());
        field('123').source(field('T2A10.034'));
      }
      if (field('T2A10.023').get() && field('T2A10.025').get()) {
        field('123').assign(Math.min(field('T2A10.034').get() + field('T2A10.036').get() + field('T2A10.038').get(), field('123').get()));
      }
      field('997').cell(5, 1).assign(
          field('111').get() -
          field('113').get() +
          field('117').get()
      );

      field('125').assign(
          field('997').cell(5, 1).get() -
          field('119').get() -
          field('121').get() -
          field('123').get()
      );
    });

    calcUtils.calc(function (calcUtils, field) {
      field('996').getRows().forEach(function (row) {
        row[5].assign(row[1].get() + row[2].get() + row[3].get() - row[4].get());
      });
    });

    calcUtils.calc(function (calcUtils, field) {
      field('995').getRows().forEach(function (row) {
        row[8].assign(row[3].get() + row[4].get() + row[5].get() - row[6].get() - row[7].get());
      });
    });


  });
})();
