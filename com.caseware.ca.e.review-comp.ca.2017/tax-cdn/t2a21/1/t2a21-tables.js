(function () {

  wpw.tax.create.tables('t2a21', {
    '999': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none'
        },
        {
          header: 'NON-CAPITAL LOSSES', colClass: 'std-input-col-width'
        },
        {
          header: 'CAPITAL LOSSES<br>(gross amount)', colClass: 'std-input-col-width'
        }
      ],
      cells: [
        {
          0: {label: '<i>Would you like to use the Federal balance from T2S4?</i>'},
          1: {num: '030y', label:'Yes', type: 'singleCheckbox'},
          2: {num: '050y', label:'Yes', type: 'singleCheckbox'}
        },
        {
          0: {label: 'Losses carried forward from preceding taxation year'},
          1: {tn: '031', num: '031'},
          2: {tn: '051', num: '051'}
        },
        {
          0: {label: 'Deduct: losses expired *', labelClass: 'indent-2'},
          1: {tn: '032', num: '032'},
          2: {type: 'none'}
        },
        {
          0: {label: 'Losses - beginning of taxation year'},
          1: {tn: '033', num: '033'},
          2: {type: 'none'}
        },
        {
          0: {
            label: 'Add: Losses transfer from wind-up of a wholly-owned\n' +
            'subsidiary or amalgamation'
          },
          1: {tn: '035', num: '035'},
          2: {tn: '055', num: '055'}
        },
        {
          0: {label: 'Current year loss', labelClass: 'indent-2'},
          1: {tn: '037', num: '037'},
          2: {tn: '057', num: '057'}
        },
        {
          0: {
            label: 'Allowable business investment loss expired as\n' +
            'reported on Federal Schedule 4 line 220 **', labelClass: 'indent-2'
          },
          1: {type: 'none'},
          2: {tn: '059', num: '059'}
        },
        {
          0: {label: 'Subtotal'}
        },
        {
          0: {
            label: 'Deduct: Amount applied against taxable income<br> <b>Carry forward to Schedule 12, lines 064</b>',
            labelClass: 'indent-2'
          },
          1: {tn: '041', num: '041'},
          2: {type: 'none'}
        },
        {
          0: {
            label: 'Amount applied against current year capital gain <b>Carry forward this amount X Inclusion\n' +
            'Rate to Schedule 12, lines 066</b>',
            labelClass: 'indent-2'
          },
          1: {type: 'none'},
          2: {tn: '061', num: '061'}
        },
        {
          0: {
            label: 'ITA section 80 adjustment',
            labelClass: 'indent-2'
          },
          1: {tn: '043', num: '043'},
          2: {tn: '063', num: '063'}
        },
        {
          0: {
            label: 'Other adjustments',
            labelClass: 'indent-2'
          },
          1: {tn: '045', num: '045'},
          2: {tn: '065', num: '065'}
        },
        {
          0: {
            label: 'Total loss carry back to prior taxation years<br>' +
            '(Schedule 10 must also be completed)',
            labelClass: 'indent-2 bold'
          },
          1: {tn: '047', num: '047'},
          2: {tn: '067', num: '067'}
        },
        {
          0: {label: 'Losses - closing balance', labelClass: 'bold'},
          1: {tn: '049', num: '049'},
          2: {tn: '069', num: '069'}
        }
      ]
    },
    '998': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none'
        },
        {
          header: 'FARM LOSSES', colClass: 'std-input-col-width'
        },
        {
          header: 'RESTRICTED FARM LOSSES', colClass: 'std-input-col-width'
        }
      ],
      cells: [
        {
          0: {label: '<i>Would you like to use the Federal balance from T2S4?</i>'},
          1: {num: '070y', label:'Yes', type: 'singleCheckbox'},
          2: {num: '090y', label:'Yes', type: 'singleCheckbox'}
        },
        {
          0: {label: 'Losses carried forward from preceding taxation year'},
          1: {tn: '071', num: '071'},
          2: {tn: '091', num: '091'}
        },
        {
          0: {label: 'Deduct: losses expired (see note on page 4)', labelClass: 'indent-2'},
          1: {tn: '072', num: '072'},
          2: {tn: '092', num: '092'}
        },
        {
          0: {label: 'Losses - beginning of taxation year'},
          1: {tn: '073', num: '073'},
          2: {tn: '093', num: '093'}
        },
        {
          0: {
            label: 'Losses transfer from wind-up of a wholly-owned\n' +
            'subsidiary and amalgamation\n'
          },
          1: {tn: '075', num: '075'},
          2: {tn: '095', num: '095'}
        },
        {
          0: {label: 'Current year loss'},
          1: {tn: '077', num: '077'},
          2: {tn: '097', num: '097'}
        },
        {
          0: {label: 'Subtotal'}
        },
        {
          0: {
            label: 'Deduct: Amount applied against taxable income<br>' +
            '<b>Carry forward to Schedule 12, line 070</b>'
          },
          1: {tn: '079', num: '079'},
          2: {type: 'none'}
        },
        {
          0: {
            label: 'Amount applied against farming income<br>' +
            '<b>Carry forward to Schedule 12, line 068</b>', labelClass: 'indent-2'
          },
          1: {type: 'none'},
          2: {tn: '099', num: '099'}
        },
        {
          0: {label: 'ITA section 80 adjustment', labelClass: 'indent-2'},
          1: {tn: '081', num: '081'},
          2: {tn: '101', num: '101'}
        },
        {
          0: {label: 'Other adjustments', labelClass: 'indent-2'},
          1: {tn: '083', num: '083'},
          2: {tn: '103', num: '103'}
        },
        {
          0: {
            label: 'Total loss carry back to prior taxation years<br>' +
            '(Schedule 10 must also be completed)', labelClass: 'bold indent-2'
          },
          1: {tn: '085', num: '085'},
          2: {tn: '105', num: '105'}
        },
        {
          0: {label: 'Losses - closing balance', labelClass: 'bold'},
          1: {tn: '087', num: '087'},
          2: {tn: '107', num: '107'}
        }
      ]
    },
    '997': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none'
        },
        {
          header: 'RESTRICTED FARM LOSSES', colClass: 'std-input-col-width'
        }
      ],
      cells: [
        {
          0: {label: '<i>Would you like to use the Federal balance from T2S4?</i>'},
          1: {num: '100y', label:'Yes', type: 'singleCheckbox'}
        },
        {
          0: {label: 'Losses carried forward from preceding taxation year'},
          1: {tn: '111', num: '111'}
        },
        {
          0: {label: 'Deduct: losses expired after seven taxation years', labelClass: 'indent-2'},
          1: {tn: '113', num: '113'}
        },
        {
          0: {label: 'Losses - beginning of taxation year'},
          1: {tn: '115', num: '115'}
        },
        {
          0: {label: 'Current year loss'},
          1: {tn: '117', num: '117'}
        },
        {
          0: {label: 'Subtotal'}
        },
        {
          0: {
            label: 'Deduct: Amount applied against listed personal property gain (If Schedule 18 exists, enter\n' +
            'amount from line 060. Otherwise, enter amount from federal Schedule 6, line 655)',
            labelClass: 'indent-2'
          },
          1: {tn: '119', num: '119'}
        },
        {
          0: {label: 'Adjustments', labelClass: 'indent-2'},
          1: {tn: '121', num: '121'}
        },
        {
          0: {
            label: 'Total loss carry back to prior taxation years<br>' +
            '(Schedule 10 must also be completed)', labelClass: 'indent-2 bold'},
          1: {tn: '123', num: '123'}
        },
        {
          0: {label: 'Losses - closing balance', labelClass: 'bold'},
          1: {tn: '125', num: '125'}
        }
      ]
    },
    '996': {
      hasTotals: true,
      columns: [
        {header: '131<br>Partnership Identifier<br>(if known)'},
        {
          header: '133<br>Limited partnership\n' +
          'losses at end of\n' +
          'preceding taxation year', colClass: 'std-input-width'
        },
        {
          header: '135<br>Limited partnership\n' +
          'losses transferred\n' +
          'from amalgamation or wind-up\n' +
          'of subsidiary', colClass: 'std-input-width'
        },
        {
          header: '137<br>Current year\n' +
          'limited\n' +
          'partnership loss', colClass: 'std-input-width'
        },
        {header: '139<br>Limited partnership loss applied', colClass: 'std-input-width', total: true},
        {
          header: '141<br>Limited partnership losses\n' +
          'closing balance<br>' +
          '(133 + 135 + 137 - 139)\n', colClass: 'std-input-width'
        }
      ]
    },
    '995': {
      hasTotals: true,
      fixedRows: true,
      columns: [
        {header: 'Year of origin', type: 'none', colClass: 'std-input-width'},
        {header: '', colClass: 'std-padding-width', tn: '151', type: 'none'},
        {header: 'Tax year end', colClass: 'std-input-width', tn: '153', type: 'date'},
        {
          header: 'Balance at the beginning of year',
          tn: '155',
          total: true,
          totalMessage: 'Totals'
        },
        {header: 'Loss incurred in current year', tn: '157'},
        {header: 'Adjustments and transfers', tn: '159', total: true, totalMessage: ''},
        {header: 'Loss carried back', tn: '165'},
        {
          header: 'Applied to reduce taxable income',
          tn: '167',
          total: true,
          totalMessage: ''
        },
        {
          header: 'Balance at end of year<br>155 + 157 + 159 - 165 - 167 ',
          tn: '169',
          total: true,
          totalMessage: ''
        }
      ],
      cells: [
        {
          0: {label: 'Current'},
          1: {label: '0'},
          3: {type: 'none'},
          7: {type: 'none'}
        },
        {
          0: {
            label: '1st preceding\n' +
            'taxation year'
          },
          1: {label: '1'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '2nd preceding\n' +
            'taxation year\n'
          },
          1: {label: '2'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '3rd preceding\n' +
            'taxation year\n'
          },
          1: {label: '3'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '4th preceding\n' +
            'taxation year\n'
          },
          1: {label: '4'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '5th preceding\n' +
            'taxation year\n'
          },
          1: {label: '5'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '6th preceding\n' +
            'taxation year\n'
          },
          1: {label: '6'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '7th preceding\n' +
            'taxation year\n'
          },
          1: {label: '7'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '8th preceding\n' +
            'taxation year\n'
          },
          1: {label: '8'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '9th preceding\n' +
            'taxation year\n'
          },
          1: {label: '9'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '10th preceding\n' +
            'taxation year\n'
          },
          1: {label: '10'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '11th preceding\n' +
            'taxation year\n'
          },
          1: {label: '11'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '12th preceding\n' +
            'taxation year\n'
          },
          1: {label: '12'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '13th preceding\n' +
            'taxation year\n'
          },
          1: {label: '13'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '14th preceding\n' +
            'taxation year\n'
          },
          1: {label: '14'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '15th preceding\n' +
            'taxation year\n'
          },
          1: {label: '15'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '16th preceding\n' +
            'taxation year\n'
          },
          1: {label: '16'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '17th preceding\n' +
            'taxation year\n'
          },
          1: {label: '17'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '18th preceding\n' +
            'taxation year\n'
          },
          1: {label: '18'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '19th preceding\n' +
            'taxation year\n'
          },
          1: {label: '19'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: '20th preceding\n' +
            'taxation year\n'
          },
          1: {label: '20'},
          4: {type: 'none'},
          6: {type: 'none'}
        }
      ]
    },
    '994': {
      hasTotals: true,
      fixedRows: true,
      columns: [
        {header: 'Year of origin', colClass: 'std-input-width', type: 'none'},
        {header: '', colClass: 'std-padding-width', tn: '181', type: 'none'},
        {header: 'Farm losses *', tn: '183', total: true, totalMessage: 'Totals'},
        {header: 'Restricted farm losses', tn: '185', total: true, totalMessage: ''},
        {header: 'Listed personal property losses', tn: '187', total: true, totalMessage: ''}
      ],
      cells: [
        {
          0: {label: 'Current'},
          1: {label: '0'}
        },
        {
          0: {
            label: '1st preceding\n' +
            'taxation year'
          },
          1: {label: '1'}
        },
        {
          0: {
            label: '2nd preceding\n' +
            'taxation year\n'
          },
          1: {label: '2'}
        },
        {
          0: {
            label: '3rd preceding\n' +
            'taxation year\n'
          },
          1: {label: '3'}
        },
        {
          0: {
            label: '4th preceding\n' +
            'taxation year\n'
          },
          1: {label: '4'}
        },
        {
          0: {
            label: '5th preceding\n' +
            'taxation year\n'
          },
          1: {label: '5'}
        },
        {
          0: {
            label: '6th preceding\n' +
            'taxation year\n'
          },
          1: {label: '6'}
        },
        {
          0: {
            label: '7th preceding\n' +
            'taxation year\n'
          },
          1: {label: '7'}
        },
        {
          0: {
            label: '8th preceding\n' +
            'taxation year\n'
          },
          1: {label: '8'},
          4: {type: 'none'}
        },
        {
          0: {
            label: '9th preceding\n' +
            'taxation year\n'
          },
          1: {label: '9'},
          4: {type: 'none'}
        },
        {
          0: {
            label: '10th preceding\n' +
            'taxation year\n'
          },
          1: {label: '10'},
          4: {type: 'none'}
        },
        {
          0: {
            label: '11th preceding\n' +
            'taxation year\n'
          },
          1: {label: '11'},
          4: {type: 'none'}
        },
        {
          0: {
            label: '12th preceding\n' +
            'taxation year\n'
          },
          1: {label: '12'},
          4: {type: 'none'}
        },
        {
          0: {
            label: '13th preceding\n' +
            'taxation year\n'
          },
          1: {label: '13'},
          4: {type: 'none'}
        },
        {
          0: {
            label: '14th preceding\n' +
            'taxation year\n'
          },
          1: {label: '14'},
          4: {type: 'none'}
        },
        {
          0: {
            label: '15th preceding\n' +
            'taxation year\n'
          },
          1: {label: '15'},
          4: {type: 'none'}
        },
        {
          0: {
            label: '16th preceding\n' +
            'taxation year\n'
          },
          1: {label: '16'},
          4: {type: 'none'}
        },
        {
          0: {
            label: '17th preceding\n' +
            'taxation year\n'
          },
          1: {label: '17'},
          4: {type: 'none'}
        },
        {
          0: {
            label: '18th preceding\n' +
            'taxation year\n'
          },
          1: {label: '18'},
          4: {type: 'none'}
        },
        {
          0: {
            label: '19th preceding\n' +
            'taxation year\n'
          },
          1: {label: '19'},
          4: {type: 'none'}
        },
        {
          0: {
            label: '20th preceding\n' +
            'taxation year\n'
          },
          1: {label: '20'},
          4: {type: 'none'}
        }
      ]
    }

  })
})();
