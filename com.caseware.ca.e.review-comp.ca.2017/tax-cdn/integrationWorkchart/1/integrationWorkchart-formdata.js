(function () {
  var transferFunc = function () {
    var autoFillTable = wpw.tax.create.retrieve('tables', 'integrationWorkchart')['999'];
    for (var i = 0; i < autoFillTable.cells.length; i++) {
      var row = autoFillTable.cells[i];
      var table = wpw.tax.form('integrationWorkchart').field('999');
      var transferCheckbox = table.cell(i, 8);
      //If the field is mapped and is selected for transfer then set it
      if (!wpw.tax.utilities.isEmptyOrNull(row.desFormId) && transferCheckbox.get()) {
        var transferVal = table.cell(i, 2).get();
        wpw.tax.utilities.isEmptyOrNull(row.desRowIndex) ?
          wpw.tax.form(row.desFormId).field(row.desFieldId).set(transferVal) :
          wpw.tax.form(row.desFormId).field(row.desFieldId).cell(row.desRowIndex, row.desColIndex).set(transferVal);
        //Set checkboxes to false after import
        transferCheckbox.set(false);
      }
    }
    // wpw.tax.resetForm('integrationWorkchart');
  };

  wpw.tax.global.formData.integrationWorkchart = {
    formInfo: {
      title: 'Integration Workchart',
      showCorpInfo: true,
      headerImage: 'cw',
      category: 'Workcharts',
      dynamicFormWidth: true
    },
    sections: [
      {
        header: 'Integration data center',
        rows: [
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            num: '1337',
            linkedCheckboxes: ['999-8'],
            label: 'Select/Unselect All',
            labelClass: 'bold'
          },
          {labelClass: 'fullLength'},
          {type: 'table', num: '999'},
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'functionButton',
            btnLabel: 'Transfer',
            btnClass: 'btn-primary btn-lg floatRight',
            paramFn: transferFunc
          }
        ]
      }
    ]
  };
})();