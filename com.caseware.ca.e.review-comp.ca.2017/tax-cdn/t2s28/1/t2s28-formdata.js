(function() {

  wpw.tax.global.formData.t2s28 = {
    formInfo: {
      abbreviation: 'T2s28',
      title: 'Election Not To Be An Associated Corporation',
      //subTitle: '(2008 and later tax years)',
      schedule: 'Schedule 28',
      code: 'Code 0801',
      formFooterNum: 'T2 SCH 28 E (08)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'For use by a Canadian-controlled private corporation (the "third corporation") to elect for a ' +
              'tax year under subsection 256(2) not to be associated with either of two other corporations' +
              ' for section 125 where these two corporations:',
              sublist: [
                'would, but for subsection 256(2), not be associated with each other at any time; and',
                'are associated, or are deemed by this subsection to be associated, with the third corporation at ' +
                'that time.'
              ]
            },
            'The third corporation may elect in respect of many corporations on the same schedule.',
            'The third corporation\'s business limit for that year shall be deemed to be nil.',
            'One completed copy of this schedule is to be filed for each corporation to which the election ' +
            'applies with the third corporation\'s income tax return.',
            'A new election is required to be filed for each tax year to which it is to apply',
            'Sections and subsections referred to on this schedule are from the federal <i>Income Tax Act</i>',
            'Attach additional schedules if more space is needed'
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        header: 'Elections', labelClass: 'text-center',
        rows: [
          {
            type: 'infoField', label: 'Date field (do not use this area)', labelWidth: '80%', num: '010',
            tn: '010', inputType: 'date'
          },
          {
            type: 'infoField', label: 'Is this an amended election?', labelWidth: '80%', num: '020',
            tn: '020', inputType: 'radio', init: '1'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table', num: '090'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {
            type: 'table', num: '080'
          }
        ]
      }
    ]
  };
})();
