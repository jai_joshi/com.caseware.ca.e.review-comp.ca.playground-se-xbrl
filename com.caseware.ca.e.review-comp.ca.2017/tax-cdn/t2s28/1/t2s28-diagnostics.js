(function() {
  wpw.tax.create.diagnostics('t2s28', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('0280002', common.prereq(function(tools) {
      return (tools.field('t2j.040').get() == 1 &&
      tools.field('t2s23.085').getCol(2)[0].get() == 2);
    }, common.requireFiled('T2S28')));

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S28'), forEach.row('080', forEach.bnCheckCol(1, true))));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S28'), forEach.row('090', forEach.bnCheckCol(1, true))));

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S28'), common.bnCheck('200')));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S28'), common.bnCheck('040')));

    diagUtils.diagnostic('0280150', common.prereq(common.requireFiled('T2S28'),
        function(tools) {
          var table = tools.field('080');
          return tools.checkAll(table.getRows(), function(row) {
            var columns = [row[0], row[1]];
            if (tools.checkMethod(columns, 'isNonZero'))
              return tools.requireAll(columns, 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0280010', common.prereq(common.requireFiled('T2S28'),
        function(tools) {
          var table = tools.field('090');
          return tools.checkAll(table.getRows(), function(row) {
            var columns = [row[0], row[1], row[2]];
            if (tools.checkMethod([row[0], row[1]], 'isNonZero'))
              return tools.requireAll(columns, 'isNonZero') &&
                  wpw.tax.utilities.dateCompare.equal(row[2].get(), tools.field('cp.tax_end').get());
            else return true;
          });
        }));

    diagUtils.diagnostic('0281010', common.prereq(common.requireFiled('T2S28'),
        function(tools) {
          var table = tools.field('080');
          var duplicate = false;
          var bn = {};
          for (var i = 0; i < table.size().rows; i++) {
            var rowBN = table.cell(i, 1).get();
            if (rowBN && bn[rowBN]) {
              table.cell(i, 1).mark();
              duplicate = true;
            }
            bn[rowBN] = true;
          }
          return !duplicate;
        }));

  });
})();

