(function() {

  var dateTimeConditionArr = [
    {
      switchIf: {
        formId: 'cp',
        fieldId: '063',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '066',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '071',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '072',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '076',
        value: '1'
      }
    }
  ];

  wpw.tax.global.tableCalculations.t2s28 = {
    '080': {
      'showNumbering': true,
      'columns': [{
        'header': 'Name of each corporation that would, but for this election, be deemed to be associated with each other',
        'num': '100', 'validate': {'or': [{'length': {'min': '1', 'max': '175'}}, {'check': 'isEmpty'}]},
        'tn': '100',
        'width': '65%',
        'type': 'text'
      },
        {
          'header': 'Business Number (if a corporation is not registered, enter "NR")',
          'num': '200',
          'tn': '200',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        }]
    },
    '090': {
      'fixedRows': true,
      'columns': [
        {
          'header': 'Name of the third corporation',
          'validate': {'or': [{'length': {'min': '1', 'max': '175'}}, {'check': 'isEmpty'}]},
          'tn': '030',
          'width': '55%',
          'type': 'text'
        },
        {
          'header': 'Business number',
          'tn': '040',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          'header': 'Tax-year-end',
          'tn': '050',
          'type': 'date',
          dateTimeShowWhen: {
            or: dateTimeConditionArr
          }
        }
      ],
      cells: [
        {
          0: {num: '030'},
          1: {num: '040'},
          2: {num: '050'}
        }
      ]
    }
  }
})();
