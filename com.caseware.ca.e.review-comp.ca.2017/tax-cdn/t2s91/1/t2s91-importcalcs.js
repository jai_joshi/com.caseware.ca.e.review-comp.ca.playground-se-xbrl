
wpw.tax.create.importCalcs('t2s91', function(tools) {
  var tn111Values = {
    1: 'BC',
    2: 'QC',
    3: 'NS',
    4: 'AB',
    5: 'YT',
    6: 'PE',
    7: 'SK',
    8: 'NT',
    9: 'NL',
    10: 'MB',
    11: 'NU',
    12: 'MJ',
    13: 'ON',
    14: 'NB'
  };
  var tn112Values = {
    1: '01',
    2: '02',
    3: '03',
    4: '04',
    5: '05',
    6: '06',
    7: '07',
    8: '08',
    9: '09',
    10: '10',
    11: '11'
  };
  function getDropdownValues(taxPrepId, corpTaxId, variables) {
    tools.intercept(taxPrepId, function(importObj) {
      tools.form('t2s91').setValue(corpTaxId, variables[importObj.value])
    })
  }
  //tn111
  getDropdownValues('NRTBE.Ttwtbe92', '111', tn111Values);
  //tn112
  getDropdownValues('NRTBE.Ttwtbe93', '112', tn112Values);

});
