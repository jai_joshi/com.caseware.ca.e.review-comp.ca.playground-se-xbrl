(function() {

  wpw.tax.create.formData('t2s91', {
    formInfo: {
      abbreviation: 'T2S91',
      title: 'Information Concerning Claims for Treaty-Based Exemptions',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      //subTitle: '(2008 and later tax years)',
      schedule: 'Schedule 91',
      code: 'Code 0803',
      formFooterNum: 'T2 SCH 91 E (14)',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'If you are a non-resident corporation that carried on a <b>treaty-protected business</b>' +
              ' in Canada,had a taxable capital gain, or disposed of a <b>taxable Canadian property</b> that was ' +
              'a <b>treaty-protected property</b> any time in the year (or a previous year, if a liability for Part ' +
              'I tax would result in the current year, but for the provisions of a tax treaty), complete and file ' +
              'this schedule with your <i>T2 Corporation Income Tax Return</i>. If you need more space, attach ' +
              'additional schedules'
            },
            {
              label: 'All legislative references are to the federal <i>Income Tax Act</i> and ' +
              '<i>Income Tax Regulations</i>'
            },
            {
              label: 'The terms <b>taxable Canadian property, treaty-protected property, business</b>, ' +
              'and <b>treaty-protected business</b> are defined in subsection 248(1) of the Act. The extended meaning' +
              ' of <b>carrying on business</b> is defined in section 253 of the Act.'
            }
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    'sections': [
      {
        hideFieldset: true,
        'rows': [
          {
            type: 'table',
            num: '100'
          },
          {labelClass: 'fullLength'},
          {
            label: '(Report all amounts in Canadian funds)',
            labelClass: 'bold center'
          }
        ]
      },
      {
        header: 'Part 1 – Carrying on business in Canada',
        rows: [
          {
            'label': '1. The province or territory where revenue was earned in Canada (Choose one option only. If more than one applies choose MJ.):',
            'tn': '111',
            'num': '111',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '2',
                    'max': '2'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'type': 'infoField',
            'inputType': 'dropdown',
            'options': [
              {
                'option': '',
                'value': ''
              },
              {
                'option': 'BC British Columbia',
                'value': 'BC'
              },
              {
                'option': 'AB Alberta',
                'value': 'AB'
              },
              {
                'option': 'SK Saskatchewan',
                'value': 'SK'
              },
              {
                'option': 'MB Manitoba',
                'value': 'MB'
              },
              {
                'option': 'ON Ontario',
                'value': 'ON'
              },
              {
                'option': 'QC Quebec',
                'value': 'QC'
              },
              {
                'option': 'YT Yukon',
                'value': 'YT'
              },
              {
                'option': 'NT Northwest Territories',
                'value': 'NT'
              },
              {
                'option': 'NU Nunavut',
                'value': 'NU'
              },
              {
                'option': 'NB New Brunswick',
                'value': 'NB'
              },
              {
                'option': 'NS Nova Scotia',
                'value': 'NS'
              },
              {
                'option': 'PE Prince Edward Island',
                'value': 'PE'
              },
              {
                'option': 'NL Newfoundland and Labrador',
                'value': 'NL'
              },
              {
                'option': 'MJ Multiple Jurisdictions',
                'value': 'MJ'
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '2. Type of business activity carried on in Canada:',
            'tn': '112',
            'num': '112',
            'validate': {
              'or': [
                {
                  'matches': '^-?[.\\d]{2,2}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'type': 'infoField',
            'inputType': 'dropdown',
            'options': [
              {
                'option': '',
                'value': ''
              },
              {
                'option': '01 Entertainment',
                'value': '01'
              },
              {
                'option': '02 Sports/recreation',
                'value': '02'
              },
              {
                'option': '03 Construction',
                'value': '03'
              },
              {
                'option': '04 Petroleum and gas',
                'value': '04'
              },
              {
                'option': '05 Transportation',
                'value': '05'
              },
              {
                'option': '06 Communications',
                'value': '06'
              },
              {
                'option': '07 Business professional',
                'value': '07'
              },
              {
                'option': '08 Architectural/engineering/scientific/technical',
                'value': '08'
              },
              {
                'option': '09 Education',
                'value': '09'
              },
              {
                'option': '10 Health',
                'value': '10'
              },
              {
                'option': '11 Other',
                'value': '11'
              }
            ]
          },
          {
            'label': 'You selected <b>Other</b> on line 112, please specify the type of business activity carried on in Canada:',
            'showWhen': {
              'fieldId': '112',
              'compare': {
                'is': '11'
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '113',
                  'validate': {
                    'or': [
                      {
                        'length': {
                          'min': '1',
                          'max': '80'
                        }
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '113'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '3. Canadian revenues derived by:'
          },
          {
            'label': 'sale of goods',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '115',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '115'
                }
              }
            ]
          },
          {
            'label': 'services provided in Canada',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '116',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '116'
                }
              }
            ]
          },
          {
            'label': 'financing activities',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '117',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '117'
                }
              }
            ]
          },
          {
            'label': 'other',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '118',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '118'
                }
              }
            ]
          },
          {
            'label': 'Total Canadian revenues',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '125',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '125'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '4. Did you rent, lease, or own any physical facilities* in Canada during the tax year?',
            'type': 'infoField',
            'inputType': 'radio',
            'tn': '135',
            'num': '135'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '5. Article and paragraph of the tax treaty under which an exemption is claimed',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '146',
                  'validate': {
                    'or': [
                      {
                        'length': {
                          'min': '1',
                          'max': '30'
                        }
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '146'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '6. Main corporate customers to whom goods were sold or services were provided in Canada (including non-residents) during the tax year <br>(attach copies of all T4A-NR slips.): ',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '200'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Indicate the nature and address of any owned, leased, or rented physical facilities located in Canada that are used by the non-resident corporation in carrying on its business activities in Canada. Examples of such facilities include an administrative or sales office, a warehouse, or a mine.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '7. Services provided in Canada by employees during the tax year:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'a) Salary, wages, and remuneration paid to:',
            'labelClass': 'tabbed'
          },
          {
            'type': 'table',
            'num': '300'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '8. Services provided in Canada by subcontractors during the tax year:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'a) Fees, charges, reimbursements of expenses, or other payments made to:',
            'labelClass': 'tabbed'
          },
          {
            'type': 'table',
            'num': '400'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '9. Has the corporation applied for a waiver of the withholding requirement under Regulation 105?',
            'type': 'infoField',
            'inputType': 'radio',
            'tn': '195',
            'num': '195'
          },
          {
            'label': 'If <b>yes</b>, did the Canada Revenue Agency waive or reduce the withholding requirement? ',
            'labelClass': 'tabbed',
            'type': 'infoField',
            'inputType': 'radio',
            'tn': '196',
            'num': '196'
          }
        ]
      },
      {
        header: 'Part 2 – Disposing of taxable Canadian property (TCP) (other than real property) and taxable ' +
        'capital gains',
        rows: [
          {
            type: 'table',
            num: '500'
          },
          {labelClass: 'fullLength'},
          {
            'label': '2. Article and paragraph of the tax treaty under which an exemption is claimed ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '211',
                  'validate': {
                    'or': [
                      {
                        'length': {
                          'min': '1',
                          'max': '30'
                        }
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '211'
                }
              }
            ]
          },
          {labelClass: 'fullLength'},
          {
            label: '3. Attach Form T2064, Certificate – <i>Proposed Disposition of Property by a Non-resident ' +
            'of Canada,</i> or Form T2068, <i>Certificate – The Disposition of Property by a Non-Resident' +
            ' of Canada</i>. See section 116 and Information Circular IC72-17, <i>Procedures concerning the ' +
            'disposition of taxable Canadian property by non-residents of Canada – Section 116</i>.',
            labelClass: 'fullLength'
          }
        ]
      }
    ]
  });
})();
