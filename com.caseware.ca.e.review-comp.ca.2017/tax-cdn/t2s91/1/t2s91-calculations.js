(function() {

  wpw.tax.create.calcBlocks('t2s91', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('110', 'CP', 'tax_end');
      calcUtils.sumBucketValues('125', ['115', '116', '117', '118']);
      if (field('195').get() == 1) {
        field('196').disabled(false)
      }
      else {
        field('196').disabled(true)
      }
    });
  });
})();
