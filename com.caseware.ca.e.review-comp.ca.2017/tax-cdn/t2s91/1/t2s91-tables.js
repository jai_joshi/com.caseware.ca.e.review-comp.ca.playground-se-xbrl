(function() {
  var part1TablesColumns = [
    {type: 'none'},
    {
      type: 'none',
      colClass: 'std-padding-width'
    },
    {colClass: 'std-input-width'},
    {
      type: 'none',
      colClass: 'std-padding-width'
    },
    {
      type: 'none',
      colClass: 'std-padding-width'
    },
    {colClass: 'std-input-width'},
    {
      type: 'none',
      colClass: 'small-input-width'
    }
  ];
  wpw.tax.create.tables('t2s91', {
    '100': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {type: 'none'},
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {colClass: 'std-input-width'},
        {
          type: 'none',
          width: '250px'
        }
      ],
      cells: [
        {
          '0': {
            label: 'Taxpayer identification number in country of residence'
          },
          '1': {
            tn: '105'
          },
          '2': {
            num: '105', "validate": {"or": [{"length": {"min": "1", "max": "55"}}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            label: 'Tax year-end in country of residence (mm/dd/yyyy)'
          },
          '1': {
            tn: '110'
          },
          '2': {
            type: 'date',
            num: '110'
          }
        }
      ]
    },
    '200': {
      fixedRows: true,
      columns: [
        {
          header: 'Corporation\'s name',
          tn: '155',
          type: 'text'
        },
        {
          header: 'Contract/project started',
          tn: '158',
          type: 'date',
          colClass: 'std-input-width'
        },
        {
          header: 'Contract/project completed',
          tn: '159',
          type: 'date',
          colClass: 'std-input-width'
        }
      ],
      cells: [
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {}
      ]
    },
    '300': {
      infoTable: true,
      fixedRows: true,
      columns: part1TablesColumns,
      cells: [
        {
          '2': {
            type: 'none',
            label: 'Number of employees',
            labelClass: 'bold center'
          },
          '5': {
            type: 'none',
            label: 'Amount paid',
            labelClass: 'bold center'
          }
        },
        {
          '0': {
            label: 'Canadian resident employees',
            labelClass: 'tabbed2'
          },
          '1': {
            tn: '160'
          },
          '2': {
            num: '160'
            , "validate": {"or": [{"matches": "^-?[.\\d]{1,6}$"}, {"check": "isEmpty"}]}
          },
          '4': {
            tn: '161'
          },
          '5': {
            num: '161'
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            label: 'non-resident employees',
            labelClass: 'tabbed2'
          },
          '1': {
            tn: '165'
          },
          '2': {
            num: '165'
            , "validate": {"or": [{"matches": "^-?[.\\d]{1,6}$"}, {"check": "isEmpty"}]}
          },
          '4': {
            tn: '166'
          },
          '5': {
            num: '166'
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '2': {
            type: 'none',
            label: '<br>Started',
            labelClass: 'bold center'
          },
          '5': {
            type: 'none',
            label: '<br>Completed',
            labelClass: 'bold center'
          }
        },
        {
          '0': {
            label: 'b) Employee employment period in Canada'
          },
          '1': {
            tn: '170'
          },
          '2': {
            num: '170',
            type: 'date'
          },
          '4': {
            tn: '171'
          },
          '5': {
            num: '171',
            type: 'date'
          }
        },
        {
          '0': {
            label: 'c) Number of calendar days, or part days, during the tax year, that any non-resident employee' +
            ' was physically present in Canada to provide services to, or for, the corporation'
          },
          '2': {
            type: 'none'
          },
          '4': {
            tn: '175'
          },
          '5': {
            num: '175'
            , "validate": {"or": [{"matches": "^-?[.\\d]{1,3}$"}, {"check": "isEmpty"}]}
          }
        }
      ]
    },
    '400': {
      infoTable: true,
      fixedRows: true,
      columns: part1TablesColumns,
      cells: [
        {
          '2': {
            type: 'none',
            label: 'Number of subcontractors',
            labelClass: 'bold center'
          },
          '5': {
            type: 'none',
            label: 'Amount paid',
            labelClass: 'bold center'
          }
        },
        {
          '0': {
            label: 'Canadian resident subcontractors',
            labelClass: 'tabbed2'
          },
          '1': {
            tn: '180'
          },
          '2': {
            num: '180'
            , "validate": {"or": [{"matches": "^-?[.\\d]{1,6}$"}, {"check": "isEmpty"}]}
          },
          '4': {
            tn: '181'
          },
          '5': {
            num: '181'
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            label: 'non-resident subcontractors',
            labelClass: 'tabbed2'
          },
          '1': {
            tn: '185'
          },
          '2': {
            num: '185'
            , "validate": {"or": [{"matches": "^-?[.\\d]{1,6}$"}, {"check": "isEmpty"}]}
          },
          '4': {
            tn: '186'
          },
          '5': {
            num: '186'
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '2': {
            type: 'none',
            label: '<br>Started',
            labelClass: 'bold center'
          },
          '5': {
            type: 'none',
            label: '<br>Completed',
            labelClass: 'bold center'
          }
        },
        {
          '0': {
            label: 'b) Subcontractor employment period in Canada'
          },
          '1': {
            tn: '190'
          },
          '2': {
            num: '190',
            type: 'date'
          },
          '4': {
            tn: '191'
          },
          '5': {
            num: '191',
            type: 'date'
          }
        },
        {
          '0': {
            label: 'c) Number of calendar days, or part days, during the tax year, that any non-resident subcontractor' +
            ' was physically present in Canada to provide services to, or for, the corporation'
          },
          '2': {
            type: 'none'
          },
          '4': {
            tn: '192'
          },
          '5': {
            num: '192'
            , "validate": {"or": [{"matches": "^-?[.\\d]{1,3}$"}, {"check": "isEmpty"}]}
          }
        }
      ]
    },
    '500': {
      showNumbering: true,
      fixedRows: true,
      columns: [
        {
          header: 'Description of TCP or other property disposed of',
          tn: '201',
          type: 'text'
        },
        {
          header: 'Proceeds',
          tn: '202',
          type: 'text'
        },
        {
          header: 'Cost or adjusted cost base',
          tn: '203',
          type: 'text'
        },
        {
          header: 'Income, gain, or loss',
          tn: '204',
          type: 'text'
        }
      ],
      cells: [
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {}
      ]
    }
  })
})();
