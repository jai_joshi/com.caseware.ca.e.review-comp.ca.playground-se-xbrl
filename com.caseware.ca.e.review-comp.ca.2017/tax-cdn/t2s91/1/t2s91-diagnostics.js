(function() {
  wpw.tax.create.diagnostics('t2s91', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0910001', common.prereq(common.check('t2j.082', 'isYes'), common.requireFiled('T2S91')));

    diagUtils.diagnostic('0910001', {
      label: 'The corporation has answered No (2) at line 200082 or line 200082 is blank and the entry at ' +
      'line 097300 is "01," but Schedule 91 is not completed.',
      category: 'EFILE'
    }, common.prereq(function(tools) {
      return (tools.field('t2j.082').isNo() || tools.field('082').isEmpty()) && tools.field('t2s97.001').get();
    }, common.requireFiled('T2S91')));

    diagUtils.diagnostic('0910002', common.prereq(common.and(
        common.requireFiled('T2S91'),
        common.check('125', 'isPositive')),
        function(tools) {
          return tools.field('135').require('isNonZero');
        }));

    diagUtils.diagnostic('0910004', common.prereq(common.requireFiled('T2S91'),
        function(tools) {
          return tools.requireAll(tools.list(['111', '112']), 'isFilled');
        }));

    diagUtils.diagnostic('091.113', common.prereq(common.and(
        common.requireFiled('T2S91'),
        function(tools) {
          return tools.field('112').get() == '11';
        }), function(tools) {
      return tools.field('113').require('isFilled');
    }));

  });
})();
