(function () {
  'use strict';

  wpw.tax.create.formData('dataTransferWorkchart', {
    formInfo: {
      abbreviation: 'dataTransferWorkchart',
      title: 'Non-imported data workchart',
      showCorpInfo: true,
      headerImage: 'cw',
      category: 'Workcharts'
    },
    sections: [
      {
        header: 'Non-imported data from Taxprep',
        hideFieldset: true,
        rows: [
          {
            label: 'The data below are data which are not imported from Taxprep csv import. ' +
            'Please update it to applicable forms and workcharts',
            labelClass: 'fullLength bold'
          },
          {type: 'table', num: '200'}
        ]
      }
    ]
  });

})();
