(function() {
  wpw.tax.create.calcBlocks('dataTransferWorkchart', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      var taxprepNonImportedJson = wpw.tax.global.taxprepNonImportedJson;

      if (taxprepNonImportedJson) {
        var flaggedForms = wpw.tax.utilities.flagger();
        var formId;
        var applicableForms = {};

        Object.keys(flaggedForms).forEach(function(key) {
          if (key in taxprepNonImportedJson) {
            applicableForms[key] = taxprepNonImportedJson[key]
          }
        });

        if (applicableForms) {
          //clear out the table
          var table = field('200');
          var tableSize = table.size().rows;
          for (var i = 0; i < tableSize; i++) {
            calcUtils.removeTableRow('dataTransferWorkchart', '200', 0);
          }
          var count = 0;
          //add a row to an empty table
          calcUtils.addTableRow('dataTransferWorkchart', '200', count);
          //populate table value
          for (formId in applicableForms) {
            //populate table
            table.cell(count, 0).assign(formId);
            //loop through the array for each key
            taxprepNonImportedJson[formId].forEach(function(dataObj) {
              table.cell(count, 1).assign(dataObj.id);
              table.cell(count, 2).assign(dataObj.description);
              table.cell(count, 3).assign(dataObj.value);
              count++;
              calcUtils.addTableRow('dataTransferWorkchart', '200', count);
            });
          }
          //remove last row
          calcUtils.removeTableRow('dataTransferWorkchart', '200', count);
        }
      }
    });
  });
})();
