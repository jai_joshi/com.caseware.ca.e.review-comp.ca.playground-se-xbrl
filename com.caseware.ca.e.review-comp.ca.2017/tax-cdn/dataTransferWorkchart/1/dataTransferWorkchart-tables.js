(function () {
  var descriptionWidth = 'std-input-col-width';

  wpw.tax.create.tables('dataTransferWorkchart', {
    '200': {
      fixedRows: true,
      columns: [
        {
          header: 'Form Id',
          colClass: descriptionWidth,
          type: 'text',
          cannotOverride: true
        },
        {
          header: 'Taxprep fieldId',
          colClass: descriptionWidth,
          type: 'text',
          cannotOverride: true
        },
        {
          header: 'Taxprep field description',
          type: 'text',
          cannotOverride: true
        },
        {
          header: 'Taxprep value',
          type: 'text',
          cannotOverride: true
        }
      ]
    }
  });
})();
