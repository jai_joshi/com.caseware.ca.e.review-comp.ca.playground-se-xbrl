(function() {
  'use strict';

  wpw.tax.global.formData.t2s428 = {
    formInfo: {
      abbreviation: 'T2S428',
      title: 'British Columbia Training Tax Credit',
      //subTitle: '(2010 and later tax years)',
      schedule: 'Schedule 428',
      code: '1501',
      formFooterNum: 'T2 SCH 428 E (15)',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Complete this schedule if, at any time in the tax year, you paid salary and wages to an employee ' +
              'enrolled in an eligible training program administered through the British Columbia Industry Training' +
              ' Authority (ITA) on account of employment or services related to the training program at a permanent' +
              ' establishment in British Columbia. Eligible programs and completion requirements are defined in the ' +
              '<i>Income Tax Act</i> (British Columbia) and by the <i>Training Tax Credits Regulation</i>.'
            },
            {
              label: 'To claim this credit, you must file a completed copy of this schedule ' +
              'with your' + '<i>' + ' T2 Corporation Income Tax Return' + '</i>' + ' within 36 months ' +
              'after the end of the tax ' +
              'year in which you paid the eligible salary and wages.'
            },
            {
              label: 'The British Columbia Training Tax Credit is available for eligible salary and wages ' +
              'payable before January 1, 2018.'
            },
            {
              label: 'There are three elements to the training tax credit program:',
              sublist: [
                '1. basic credit for an eligible recognized program (non-Red Seal)(see Part 1);',
                '2. completion credits for an eligible training program (Red Seal ' +
                'and non-Red Seal)(see Part 2); and',
                '3. enhanced credits for First Nations individuals and persons ' +
                'with disabilities (Red Seal and non-Red Seal)(see Part 3).'
              ]
            },
            {
              label: 'Enter the registration number provided by the ITA. If there is ' +
              'no registration number, enter the social insurance number (SIN) or the ' +
              'name of the employee. Also enter the name of the Red Seal or non-Red Seal ' +
              'program and the salary and wages payable in the period. Attach additional ' +
              'schedules if more space is required.'
            },
            {
              label: '<b>' + 'Do not complete Part 1 or Part 2 for an employee enrolled in an eligible ' +
              'program if you are claiming the enhanced tax credit in Part 3 for that employee.' + '</b>'
            }
          ]
        }
      ],
      category: 'British Columbia Forms'
    },
    sections: [
      {
        'header': '<i><i>Freedom of Information and Protection of Privacy Act (FOIPPA)</i></b>',
        'rows': [
          {
            'label': 'The personal information on this form is collected for the purpose of administering the <i>Income Tax Act</i> (British Columbia) under the authority of paragraph 26(a) of the FOIPPA. Questions about the collection or use of this information can be directed to the Manager, Intergovernmental Relations, PO Box 9444 Stn Prov Govt, Victoria BC V8W 9W8. (Telephone: Victoria at <b>250-387-3332</b> or toll-free at <b>1-877-387-3332</b> and ask to be re-directed). Email: ITBTaxQuestions@gov.bc.ca',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 1 - Basic tax credit (non-Red Seal apprenticeship programs only)',
        'rows': [

          {
            'label': 'The basic tax credit is available during an employee\'s first 24 months of a <b>non-Red Seal</b> apprenticeship program. To claim this credit, enter on line 110 the salary and wages* payable during the tax year where the employee was still within the first 24 months of a non-Red Seal program.',
            'labelClass': 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            label: 'If your employee is in a red seal program, you cannot claim this basic tax credit'
          },
          {
            'type': 'table',
            'num': '099'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Net of any other government or non-government assistance received, to be received, or that you are entitled to receive.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - Completion tax credits (Red Seal and non-Red Seal apprenticeship programs)',
        'rows': [
          {
            'label': 'Calculation for an employee who has completed level 3 of an eligible apprenticeship program',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'To claim this credit, enter on line 210 the salary and wages* payable in the 12 months just before the completion of the level.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '199'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Net of any other government or non-government assistance received, to be received, or that you are entitled to receive.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Calculation for an employee who has completed level 4 of an eligible apprenticeship program',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'To claim this credit, enter on line 310 the salary and wages* in the 12 months just before the completion of the level',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '299'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Net of any other government or non-government assistance received, to be received, or that you are entitled to receive.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Enhanced tax credits',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The enhanced training tax credits apply only to the following employees:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- eligible First Nations individuals (defined as persons registered as an Indian under the <i>Indian Act</i>); or',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '- persons with disabilities (defined as persons eligible for the federal disability amount on their income tax and benefit return)',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'Calculation for an employee\'s first 24 months of a Red Seal apprenticeship program',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'To claim this credit, you must be eligible for the federal apprenticeship job creation tax credit (Red Seal programs only). Enter on line 411 the salaries and wages* payable that are eligible for the federal apprenticeship job creation tax credit (included on line 603 from Schedule 31).',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '399'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Net of any other government or non-government assistance received, to be received, or that you are entitled to receive. Government assistance does not include the federal apprenticeship job creation tax credit.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Calculation for an employee\'s first 24 months of a non-Red Seal apprenticeship program',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'This credit includes the basic and enhanced tax credits. To claim this credit, enter on line 510 the salary and wages** payable during the tax year where the employee was still within the first 24 months of a non-Red Seal program.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '499'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '** Net of any other government or non-government assistance received, to be received, or that you are entitled to receive.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Calculation for an employee who has completed level 3 of an eligible apprenticeship program',
            'labelClass': 'fullLength bold'
          },
          {
            'label': '(Red Seal and non-Red Seal apprenticeship programs)',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'This credit includes the completion and enhanced tax credits. To claim this credit, enter on line 610 the salary and wages* payable in the 12 months just before the completion of the level.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '599'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Calculation for an employee who has completed level 4 of an eligible apprenticeship program (Red Seal and non-Red Seal apprenticeship programs)',
            'labelClass': 'fullLength bold'
          },
          {
            'label': '(Red Seal and non-Red Seal apprenticeship programs)',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'This credit includes the completion and enhanced tax credits. To claim this credit, enter on line 710 the salary and wages* payable in the 12 months just before the completion of the level.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '699'
          },
          {
            'label': '* Net of any other government or non-government assistance received, to be received, or that you are entitled to receive.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 4 - British Columbia training tax credit',
        'spacing': 'R_tn2',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Basic tax credit</b> (Total E1 from page 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '800',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '800'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Completion tax credit',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Line E2 from Part 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '801'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Line E3 from Part 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '802'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': '<b>Completion tax credit</b> (amount B <b>plus</b> amount C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '803',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '805',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '805'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enhanced tax credit',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Line E4 from Part 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '806'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'label': 'Line E5 from Part 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '807'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F'
                }
              }
            ]
          },
          {
            'label': 'Line E6 from Part 3',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '808'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G'
                }
              }
            ]
          },
          {
            'label': 'Line E7 from Part 3',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '809'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'H'
                }
              }
            ]
          },
          {
            'label': '<b>Enhanced tax credit</b> (total of amounts E to H)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '811',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '810',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '810'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': '<b>Credit allocated from a partnership**</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '815',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '815'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': '<b>British Columbia training tax credit</b> (<b>add</b> amounts A, D, I, and J)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '816'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter the amount from line K at line 679 in Part 2 of Schedule 5,<i> Tax Calculation Supplementary - Corporations.</i>',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '** A corporation that is a member of a partnership, other than a specified member as defined in subsection 248(1) of the federal<i>Income Tax Act</i>, can claim its appropriate portion of the British Columbia training tax credit on salary and wages payable for employment. The appropriate portion is that portion that may reasonably be considered to be in the same proportion in which the partners have agreed to share any income or loss.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  };
})();
