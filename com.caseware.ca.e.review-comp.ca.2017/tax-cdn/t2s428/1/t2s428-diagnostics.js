(function() {
  wpw.tax.create.diagnostics('t2s428', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var tableObjects = [
      {errorCode: '4280002', tableNum: '099'}, {errorCode: '4280003', tableNum: '199'},
      {errorCode: '4280004', tableNum: '299'}, {errorCode: '4280005', tableNum: '399'},
      {errorCode: '4280006', tableNum: '499'}, {errorCode: '4280007', tableNum: '599'},
      {errorCode: '4280008', tableNum: '699'}
    ];

    function checkRowFilled(diagObject) {
      diagUtils.diagnostic(diagObject.errorCode, common.prereq(common.requireFiled('T2S428'),
          function(tools) {
            var table = tools.field(diagObject.tableNum);
            return tools.checkAll(table.getRows(), function(row) {
              if (row[2].isPositive())
                return tools.requireAll([row[0], row[1]], 'isNonZero');
              else return true;
            });
          }));
    }

    for (var i = 0; i < tableObjects.length; i++) {
      checkRowFilled(tableObjects[i]);
    }

    diagUtils.diagnostic('4280001', common.prereq(common.check(['t2s5.679'], 'isNonZero'), common.requireFiled('T2S428')));

  });
})();
