(function() {

  wpw.tax.create.calcBlocks('t2s428', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {

      //part 1
      field('099').getRows().forEach(function(row) {
        row[3].assign(row[2].get() * 0.20);
        row[4].assign(Math.min(row[3].get(), 4000));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 2
      field('199').getRows().forEach(function(row) {
        row[3].assign(row[2].get() * 0.15);
        row[4].assign(Math.min(row[3].get(), 2500));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('299').getRows().forEach(function(row) {
        row[3].assign(row[2].get() * 0.15);
        row[4].assign(Math.min(row[3].get(), 3000));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 3
      field('399').getRows().forEach(function(row) {
        row[3].assign(row[2].get() * 0.055);
        row[4].assign(Math.min(row[3].get(), 1000));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('499').getRows().forEach(function(row) {
        row[3].assign(row[2].get() * 0.30);
        row[4].assign(Math.min(row[3].get(), 6000));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('599').getRows().forEach(function(row) {
        row[3].assign(row[2].get() * 0.225);
        row[4].assign(Math.min(row[3].get(), 3750));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('699').getRows().forEach(function(row) {
        row[3].assign(row[2].get() * 0.225);
        row[4].assign(Math.min(row[3].get(), 4500));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 4 calcs
      calcUtils.equals('800', '120-1');
      calcUtils.equals('801', '220-1');
      calcUtils.equals('802', '320-1');
      calcUtils.sumBucketValues('803', ['801', '802']);
      calcUtils.equals('805', '803');
      calcUtils.equals('806', '420-1');
      calcUtils.equals('807', '520-1');
      calcUtils.equals('808', '620-1');
      calcUtils.equals('809', '720-1');
      calcUtils.sumBucketValues('811', ['806', '807', '808', '809']);
      calcUtils.equals('810', '811');
      //ToDO get num 815
      calcUtils.sumBucketValues('816', ['800', '805', '810', '815']);
    });

  });
})();
