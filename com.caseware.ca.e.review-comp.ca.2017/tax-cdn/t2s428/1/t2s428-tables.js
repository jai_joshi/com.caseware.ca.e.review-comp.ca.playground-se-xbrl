(function () {
  wpw.tax.global.tableCalculations.t2s428 = {
    '199': {
      hasTotals: true,
      'showNumbering': true,
      'maxLoop': 100000,
      'columns': [
        {
          'header': 'A2<br>ITA registration number<br>(SIN or name of employee)',
          'num': '200',
          'tn': '200',
          'width': '25%',
          'maxLength': 60,
          type: 'text'
        },
        {
          'header': 'B2<br>Name of program',
          'num': '205',
          'tn': '205',
          'maxLength': 80,
          type: 'text'
        },
        {
          'header': 'C2<br>Salary and wages*',
          'num': '210',
          'tn': '210',
          'width': '13%'
        },
        {
          'header': 'D2<br>Column C2 x 15%',
          'num': '215',
          'tn': '215',
          'width': '13%',
          'disabled': true
        },
        {
          'header': 'E2<br>Lesser of column D2<br>or $2,500',
          'num': '220',
          'width': '13%',
          'total': true,
          'totalNum': '220-1',
          'totalMessage': 'Total E2',
          'tn': '220',
          'disabled': true
        }]
    },
    '299': {
      'showNumbering': true,
      hasTotals: true,
      'maxLoop': 100000,
      'columns': [{
        'header': 'A3<br>ITA registration number<br>(SIN or name of employee)',
        'num': '300',
        'tn': '300',
        'width': '25%',
        'maxLength': 60,
        type: 'text'
      },
        {
          'header': 'B3<br>Name of program',
          'num': '305',
          'tn': '305',
          'maxLength': 80,
          type: 'text'
        },
        {
          'header': 'C3<br>Salary and wages*',
          'num': '310',
          'tn': '310',
          'width': '13%'
        },
        {
          'header': 'D3<br>Column C3 x 15%',
          'num': '315',
          'tn': '315',
          'width': '13%',
          'disabled': true
        },
        {
          'header': 'E3<br>Lesser of column D3<br>or $3,000',
          'num': '320',
          'width': '13%',
          'total': true,
          'totalNum': '320-1',
          'totalMessage': 'Total E3',
          'tn': '320',
          'disabled': true
        }]
    },
    '399': {
      hasTotals: true,
      'showNumbering': true,
      'maxLoop': 100000,
      'columns': [{
        'header': 'A4<br>ITA registration number<br>(SIN or name of employee)',
        'num': '400',
        'tn': '400',
        'width': '25%',
        'maxLength': 60,
        type: 'text'
      },
        {
          'header': 'B4<br>Name of program',
          'num': '405',
          'tn': '405',
          'maxLength': 80,
          type: 'text'
        },
        {
          'header': 'C4<br>Salary and wages*',
          'num': '411',
          'tn': '411',
          'width': '20%'
        },
        {
          'header': 'D4<br>Column C4 x 5.5%',
          'num': '416',
          'tn': '416',
          'width': '20%',
          'disabled': true
        },
        {
          'header': 'E4<br>Lesser of column D4 or $1,000',
          'num': '420',
          'width': '13%',
          'total': true,
          'totalNum': '420-1',
          'totalMessage': 'Total E4',
          'tn': '420',
          'disabled': true
        }]
    },
    '499': {
      hasTotals: true,
      'showNumbering': true,
      'maxLoop': 100000,
      'columns': [{
        'header': 'A5<br>ITA registration number<br>(SIN or name of employee)',
        'num': '500',
        'tn': '500',
        'width': '25%',
        'maxLength': 60,
        type: 'text'
      },
        {
          'header': 'B5<br>Name of program',
          'num': '505',
          'tn': '505',
          'maxLength': 80,
          type: 'text'
        },
        {
          'header': 'C5<br>Salary and wages*',
          'num': '510',
          'tn': '510',
          'width': '13%'
        },
        {
          'header': 'D5<br>Column C5 x 30%',
          'num': '515',
          'tn': '515',
          'width': '13%',
          'disabled': true
        },
        {
          'header': 'E5<br>Lesser of column D5 or $6,000',
          'num': '520',
          'width': '13%',
          'total': true,
          'totalNum': '520-1',
          'totalMessage': 'Total E5',
          'tn': '520',
          'disabled': true
        }]
    },
    '599': {
      hasTotals: true,
      'showNumbering': true,
      'maxLoop': 100000,
      'columns': [{
        'header': 'A6<br>ITA registration number<br>(SIN or name of employee)',
        'num': '600',
        'tn': '600',
        'width': '25%',
        'maxLength': 60,
        type: 'text'
      },
        {
          'header': 'B6<br>Name of program',
          'num': '605',
          'tn': '605',
          'maxLength': 80,
          type: 'text'
        },
        {
          'header': 'C6<br>Salary and wages*',
          'num': '610',
          'tn': '610',
          'width': '13%'
        },
        {
          'header': 'D6<br>Column C6 x 22.5%',
          'num': '615',
          'tn': '615',
          'width': '13%',
          'disabled': true
        },
        {
          'header': 'E6<br>Lesser of column D6 or $3,750',
          'num': '620',
          'width': '13%',
          'total': true,
          'totalNum': '620-1',
          'totalMessage': 'Total E6',
          'tn': '620',
          'disabled': true
        }]
    },
    '699': {
      hasTotals: true,
      'showNumbering': true,
      'maxLoop': 100000,
      'columns': [{
        'header': 'A7<br>ITA registration number<br>(SIN or name of employee)',
        'num': '700',
        'tn': '700',
        'width': '25%',
        'maxLength': 60,
        type: 'text'
      },
        {
          'header': 'B7<br>Name of program',
          'num': '705',
          'tn': '705',
          'maxLength': 80,
          type: 'text'
        },
        {
          'header': 'C7<br>Salary and wages*',
          'num': '710',
          'tn': '710',
          'width': '13%'
        },
        {
          'header': 'D7<br>Column C7 x 22.5%',
          'num': '715',
          'tn': '715',
          'width': '13%',
          'disabled': true
        },
        {
          'header': 'E7<br>Lesser of column D7 or $4,500',
          'num': '720',
          'width': '13%',
          'total': true,
          'totalNum': '720-1',
          'totalMessage': 'Total E7',
          'tn': '720',
          'disabled': true
        }]
    },
    '099': {
      hasTotals: true,
      'showNumbering': true,
      'maxLoop': 100000,
      'columns': [{
        'header': 'A1<br>ITA registration number<br>(SIN or name of employee)',
        'num': '100',
        'tn': '100',
        'width': '25%',
        'maxLength': 60,
        type: 'text'
      },
        {
          'header': 'B1<br>Name of program',
          'num': '105',
          'tn': '105',
          'maxLength': 80,
          type: 'text'
        },
        {
          'header': 'C1<br>Salary and wages*',
          'num': '110',
          'tn': '110',
          'width': '13%'
        },
        {
          'header': 'D1<br>Column C1 x 20%',
          'num': '115',
          'tn': '115',
          'width': '13%',
          'disabled': true
        },
        {
          'header': 'E1<br>Lesser of column D1<br>or $4,000',
          'num': '120',
          'width': '13%',
          'total': true,
          'totalNum': '120-1',
          'totalMessage': 'Total E1',
          'tn': '120',
          'disabled': true
        }]
    }
  }
})();
