(function() {
  'use strict';

  wpw.tax.global.formData.t2s344 = {
    'formInfo': {
      'abbreviation': 'T2S344',
      'title': 'Nova Scotia Manufacturing and Processing Investment Tax Credit',
      //'subTitle': '(2006 and later tax years)',
      'schedule': 'Schedule 344',
      'code': 'Code 0601',
      formFooterNum: 'T2 SCH 344 (06)',
      'headerImage': 'canada-federal',
      'showCorpInfo': true,
      'description': [
        {
          'type': 'list',
          'items': [
            {
              label: 'For use by corporations that have acquired qualified property after December 31, 1996, ' +
              'and before January 1, 2003, and want to reduce Nova Scotia tax payable. Qualified property is ' +
              'defined in subsection 49(1) of the Nova Scotia <i>Income Tax Act</i> and in subsection 127(9) and ' +
              'related subsections 127(11) and (11.1) of the federal <i>Income Tax Act</i>. Deduct the amount of ' +
              'any government assistance or non-government assistance in calculating the capital cost of ' +
              'qualified property.'
            },
            {
              label: 'The qualified property has to be used by the corporation in Nova Scotia primarily ' +
              'for the purpose of manufacturing or processing of goods for sale or lease. Property leased ' +
              'by the corporation to a lessee for this purpose (other than a person exempt from tax under ' +
              'section 149 of the federal <i>Income Tax Act</i> ) may also qualify for the credit. ' +
              '"Manufacturing or processing" is defined in subsection 125.1(3) of the federal ' +
              '<i>Income Tax Act</i> and includes qualified activities as defined by Regulation 5202 of the federal ' +
              '<i>Income Tax Regulations</i> .'
            },
            {
              label: 'Expenditures made on or after January 1, 2003, may be added to the capital cost ' +
              'of a qualified property under the available-for-use provision in subsection 49(10) of the ' +
              'Nova Scotia <i>Income Tax Act</i>. See the Nova Scotia ' +
              '<i>Manufacturing and Processing Investment Tax Credit Regulations</i> for more details on the ' +
              'available-for-use provision. Expenditures ' +
              'incurred after May 9, 2006, are not eligible to be added to the total capital cost of q' +
              'ualified property.'
            },
            {
              label: 'Capital cost of qualified property must be identified on this schedule and filed ' +
              'no later than 12 months after the <i>T2 Corporation Income Tax Return</i> is due for the tax year ' +
              'in which the costs were incurred.'
            },
            {
              label: 'The credit is eligible for a seven-year carryforward and a three-year carryback. ' +
              'You cannot carry the credit forward to any tax year ending after December 31, 2009.'
            },
            {
              label: 'You can renounce the current year credit in whole or in part. The renouncement ' +
              'must be filed on or before the filing due date of the <i>T2 Corporation Income Tax Return</i> .'
            },
            {
              label: 'Use this schedule to show a credit transfer after an amalgamation or the wind-up ' +
              'of a subsidiary, as described under subsections 87(1) and 88(1) of the federal ' +
              '<i>Income Tax Act</i>. This schedule can also be used to show the credit allocated from a trust ' +
              'or a partnership.'
            },
            {
              label: 'File one completed copy of this schedule with your <i>T2 Corporation Income Tax Return</i>.'
            }
          ]
        }
      ],
      category: 'Nova Scotia Forms'
    },
    'sections': [
      {
        'header': 'Part 1 – Qualified property (acquired in current tax year) eligible for the credit',
        'rows': [
          {
            'type': 'table',
            'num': '100'
          }
        ]
      },
      {
        'header': 'Part 2 - Part 2 – Calculation of total credit available and credit available for carryforward',
        'rows': [
          {
            'label': 'Credit at end of preceding tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '300'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Deduct</b>: Credit expired after seven tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '104'
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit at beginning of tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105'
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '305'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Add:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Credit transferred on an amalgamation or the windup of a subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110'
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              },
              null
            ]
          },
          {type: 'table', num: '2000'},
          {
            'label': 'Credit allocated from a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '130'
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit allocated from a trust',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '140'
                },
                'padding': {
                  'type': 'tn',
                  'data': '140'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '340'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '341'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Total credit available',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': '',
            'columns': [
              {
                'input': {
                  'num': '342'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Credit renounced',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150'
                },
                'padding': {
                  'type': 'tn',
                  'data': '150'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit claimed in the current year (enter on line 561 in Part 2 of Schedule 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '160'
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit carried back to preceding tax year(s) (complete Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '370'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '380'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '390'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Closing balance',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': '',
            'columns': [
              {
                'input': {
                  'num': '200'
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 3 - Request for carryback of credit',
        'rows': [
          {
            'label': 'Complete this part to ask for a carryback of a current-year credit earned.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {type: 'table', num: '1000'}
        ]
      },
      {
        'header': 'Part 4 - Analysis of credit available for carryforward by year of origin',
        'rows': [
          {
            'type': 'table',
            'num': '1300'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for Manitoba R&E credits',
        'rows': [
          {
            'type': 'table',
            'num': '1600'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  };
})();
