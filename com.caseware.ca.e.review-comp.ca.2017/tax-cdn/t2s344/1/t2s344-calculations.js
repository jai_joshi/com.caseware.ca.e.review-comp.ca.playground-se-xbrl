(function() {

  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  function runHistoricalTableCalc(calcUtils, tableObj, carryBackAmount, limitOnCreditAmount) {
    var tableNum = tableObj.tableNum;
    var openingBalanceCindex = tableObj.openingBalanceCindex;
    var currentYearCindex = tableObj.currentYearCindex;
    var transferCindex = tableObj.transferCindex;
    var availableCindex = tableObj.availableCindex;
    var appliedCindex = tableObj.appliedCindex;
    var endingBalanceCindex = tableObj.endingBalanceCindex;

    var summaryTable = calcUtils.field(tableNum);
    var numRows = summaryTable.size().rows;

    summaryTable.getRows().forEach(function(row, rowIndex) {
      if (rowIndex == numRows - 1) {
        if (carryBackAmount > 0) {
          if (carryBackAmount > row[availableCindex].get()) {
            summaryTable.cell(6, 11).assign(row[availableCindex].get());
          }
          else {
            summaryTable.cell(6, 11).assign(carryBackAmount);
          }
          row[availableCindex].assign(row[openingBalanceCindex].get() + row[currentYearCindex].get() + row[transferCindex].get());
          row[endingBalanceCindex].assign(Math.max(row[availableCindex].get() - row[appliedCindex].get(), 0))
        }
        else {
          summaryTable.cell(6, 11).assign(0)
        }
      }
      else {
        if (rowIndex == 0) {
          // to avoid the first row being calculated to total
          row[currentYearCindex].assign(0);
          row[appliedCindex].assign(0);
          row[transferCindex].assign(0);
          row[availableCindex].assign(0);
          row[endingBalanceCindex].assign(0);
        } else {
          row[appliedCindex].assign(returnAmountApplied(limitOnCreditAmount, row[availableCindex].get()));
          row[availableCindex].assign(row[openingBalanceCindex].get() + row[currentYearCindex].get() + row[transferCindex].get());
          row[endingBalanceCindex].assign(Math.max(row[availableCindex].get() - row[appliedCindex].get(), 0));
          limitOnCreditAmount -= row[appliedCindex].get();
        }
      }
    });
  }

  wpw.tax.create.calcBlocks('t2s344', function(calcUtils) {
    //cals years for all table
    calcUtils.calc(function(calcUtils, field, form) {
      var tableArrays1 = [1000, 1300, 1600];
      var summaryTable = field('tyh.200');
      tableArrays1.forEach(function(tableNum) {
        field(tableNum).getRows().forEach(function(row, rowIndex) {
          if (tableNum == 1000) {
            var year = Math.abs(rowIndex - 19);
            row[1].assign(summaryTable.cell(year, 6).get());
          }
          else {
            row[1].assign(summaryTable.cell(rowIndex + 14, 6).get())
          }
        });
      });

    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('300', 'T2S344', '1601');
      calcUtils.subtract('105', '300', '104');
      calcUtils.equals('305', '105');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var tn120 = 0;
      // amount of acquisition before Jan 1st, 2001
      field('100').forEachRow(function(row) {
        if (wpw.tax.utilities.dateCompare.lessThan(row[2].get(), wpw.tax.date(2001, 1, 1))) {
          tn120 += row[3].get();
        }
      });

      field('202').assign(tn120);
      field('210').assign(field('100').total(3).get() - tn120);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('201').assign(field('ratesNs.500').get());
      field('211').assign(field('ratesNs.501').get());

      calcUtils.multiply('120', ['202', '201'], 0.01);
      calcUtils.multiply('121', ['210', '211'], 0.01);
      calcUtils.sumBucketValues('340', ['110', '120', '121', '130', '140']);
      calcUtils.equals('341', '340');
      calcUtils.sumBucketValues('342', ['305', '341']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.equals('370', '410');
      calcUtils.sumBucketValues('380', ['150', '160', '370']);
      calcUtils.equals('390', '380');
      calcUtils.subtract('200', '342', '390');
    });


    calcUtils.calc(function(calcUtils, field, form) {
      //part4
      var summaryTable = field('1600');
      field('1300').getRows().forEach(function(row, rowIndex) {
        row[3].assign(summaryTable.getRow(rowIndex)[13].get());
      });
      field('400').assign(field('1301').get() + field('1401').get());
    });

    calcUtils.calc(function(calcUtils, field) {
      //historical data table
      var summaryTable = field('1600');
      var carryBackAmount = field('1000').get().totals[5] + field('340').get();
      var limitOnCreditAmount = Math.max(field('340').get() - carryBackAmount, 0);

      runHistoricalTableCalc(
          calcUtils,
          {
            tableNum: '1600',
            openingBalanceCindex: 3,
            currentYearCindex: 5,
            transferCindex: 7,
            availableCindex: 9,
            appliedCindex: 11,
            endingBalanceCindex: 13
          },
          carryBackAmount,
          limitOnCreditAmount
      );
      summaryTable.cell(6, 5).assign(field('340').get());
    });

  });
})();
