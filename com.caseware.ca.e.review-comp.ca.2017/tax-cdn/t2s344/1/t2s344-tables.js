(function() {

  var descriptionWidth = '200px';

  wpw.tax.global.tableCalculations.t2s344 = {
    "100": {
      "showNumbering": true,
      "hasTotals": true,
      "columns": [
        {
          "header": "CCA class No.",
          "num": "101",
          "tn": "101",
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          type: 'selector',
          selectorOptions: {
            title: 'CCA Classes',
            items: wpw.tax.codes.ccaClasses,
            hideKeys: true
          }
        },
        {
          "header": "Description of qualified property",
          "num": "301",
          type: 'text'
        },
        {
          "header": "Acquisition date",
          "type": "date",
          "num": "102",
          "tn": "102",
          colClass: 'std-input-width'
        },
        {
          "header": "Capital cost",
          "num": "103",
          "total": true,
          "totalMessage": "Total capital cost",
          "totalIndicator": "A",
          "totalNum": "303",
          "tn": "103",
          colClass: 'std-input-width'
        }
      ]
    },
    '1300': {
      'fixedRows': true,
      hasTotals: true,
      'infoTable': true,
      'columns': [
        {
          type: 'none',
          width: 'std-padding-width'
        },
        {
          header: 'Year of origin<br> (earliest year first)',
          'width': '190px',
          'type': 'date',
          'disabled': true
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Credit available',
          colClass: 'std-input-width',
          total: true,
          totalNum: '1301',
          totalMessage: 'Total (equals line 200 in Part 2)'
        },
        {
          type: 'none'
        }
      ],
      'cells': [
        {},
        {},
        {},
        {},
        {},
        {},
        {}
      ]
    },
    '1000': {
      fixedRows: true,
      infoTable: true,
      hasTotals: true,
      columns: [
        {
          width: descriptionWidth,
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          type: 'date'
        },
        {
          type: 'none'
        },
        {
          width: descriptionWidth,
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          total: true,
          totalMessage: 'Total (enter on line C in Part 2)',
          totalNum: '410'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {
            label: '1st preceding tax year'
          },
          '3': {
            label: ' Credit to be applied ',
            labelClass: 'center'
          },
          '4': {
            tn: '901'
          },
          '5': {
            num: '901', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            label: '2nd preceding tax year'
          },
          '3': {
            label: ' Credit to be applied ',
            labelClass: 'center'
          },
          '4': {
            tn: '902'
          },
          '5': {
            num: '902', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            label: '3rd preceding tax year'
          },
          '3': {
            label: ' Credit to be applied ',
            labelClass: 'center'
          },
          '4': {
            tn: '903'
          },
          '5': {
            num: '903', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }]
    },
    '2000': {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none"
        },
        {
          "colClass": "std-spacing-width",
          "type": "none"
        },
        {
          "colClass": "std-input-width"
        },
        {
          "colClass": "std-padding-width",
          "type": "none",
          "cellClass": "alignCenter"
        },
        {
          "colClass": "small-input-width"
        },
        {
          "colClass": "small-input-width",
          "type": "none",
          "cellClass": "alignCenter"
        },
        {
          "colClass": "std-padding-width",
          "type": "none",
          "cellClass": "alignCenter"
        },
        {
          "colClass": "std-input-width"
        },
        {
          "type": "none",
          "colClass": "std-padding-width"
        },
        {
          "type": "none",
          "colClass": "std-input-col-width"
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Acquisitions before January 1, 2001, from amount A above"
          },
          "2": {
            "num": "202"
          },
          "3": {
            "label": " x ",
            "labelClass": "center"
          },
          "4": {
            "num": "201"
          },
          "5": {
            "label": "% = "
          },
          "6": {
            "tn": "120"
          },
          "7": {
            "num": "120"
          }
        },
        {
          "0": {
            "label": "Acquisitions after December 31, 2000, from amount A above"
          },
          "2": {
            "num": "210"
          },
          "3": {
            "label": " x ",
            "labelClass": "center"
          },
          "4": {
            "num": "211"
          },
          "5": {
            "label": "% = "
          },
          "6": {
            "tn": "121"
          },
          "7": {
            "num": "121"
          }
        }
      ]
    },
    '1600': {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {colClass: 'std-padding-width', type: 'none'},
        {
          colClass: 'std-input-width',
          type: 'date',
          disabled: true,
          header: '<b>Year of origin</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '1601',
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '502',
          totalMessage: ' ',
          header: '<b>Credit available for current year</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '503',
          totalMessage: ' ',
          header: '<b>Transfer amount</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', totalNum: '504',
          header: '<b>Amount available to apply</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '505',
          header: '<b>Applied<b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '506',
          header: '<b>Balance to carry forward</b>'
        },
        {colClass: 'std-padding-width', type: 'none'}
      ],
      cells: [
        {
          '4': {label: '*'},
          '5': {type: 'none'},
          '7': {type: 'none'},
          '9': {type: 'none'},
          '11': {type: 'none'},
          '13': {type: 'none', label: 'N/A'}
        },
        {
          '5': {type: 'none'},
          '14': {label: '**'}
        },
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {
          '3': {type: 'none'},
          '7': {type: 'none'}
        }
      ]
    }
  }
})();
