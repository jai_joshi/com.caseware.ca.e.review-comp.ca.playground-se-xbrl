(function() {

  var valueArray =
      [205, 206, 207, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223,
        224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236];

  wpw.tax.create.calcBlocks('rentalsWcSummaryTotal', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('201', 'CP', 'tax_end');
      var sums = {};
      var repeatFormsArr = calcUtils.allRepeatForms('rentalsWcSummary');
      repeatFormsArr.forEach(function(form) {
        for (var i = 0; i < valueArray.length; i++) {
          var fieldId = valueArray[i];
          sums[fieldId] = (sums[fieldId] || 0) + form.field(fieldId).get();
        }
      });

      for (var i = 0; i < valueArray.length; i++) {
        var fieldId = valueArray[i];
        field(fieldId).assign(sums[fieldId]);
      }
    });
  });
})();
