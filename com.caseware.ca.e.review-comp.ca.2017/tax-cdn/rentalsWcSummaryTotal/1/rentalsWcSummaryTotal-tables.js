(function() {
  wpw.tax.global.tableCalculations.rentalsWcSummaryTotal = {
    '2000': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        'type': 'none',
        'disabled': true
      },
        {
          'width': '20px',
          'type': 'none'
        },
        {
          'width': '128px',
          'header': '<b>Current Year</b>',
          cellClass: 'alignRight'
        },
        {
          'width': '20px',
          'type': 'none'
        },
        {
          'width': '128px',
          'header': '<b>Prior Year</b>',
          cellClass: 'alignRight'
        },
        {
          'width': '20px',
          'type': 'none'
        },
        {
          'width': '128px',
          'header': '<b>2rd Prior Year</b>',
          cellClass: 'alignRight'
        },
        {
          'width': '20px',
          'type': 'none'
        },
        {
          'width': '128px',
          'header': '<b>3rd Prior Year</b>',
          cellClass: 'alignRight'
        },
        {
          'width': '20px',
          'type': 'none'
        },
        {
          'width': '128px',
          'header': '<b>4th Prior Year</b>',
          cellClass: 'alignRight'
        },
        {
          'type': 'none',
          'width': '20px'
        }],
      'cells': [{
        '0': {
          'label': 'Fiscal period ending'
        },
        '2': {
          'num': '201',
          'type': 'date'
        },
        '4': {
          'type': 'date'
        },
        '6': {
          'type': 'date'
        },
        '8': {
          'type': 'date'
        },
        '10': {
          'type': 'date'
        }
      },
        {
          '0': {
            'label': 'Partnership',
            'labelClass': 'bold'
          },
          '2': {
            'type': 'none'
          },
          '4': {
            'type': 'none'
          },
          '6': {
            'type': 'none'
          },
          '8': {
            'type': 'none'
          },
          '10': {
            'type': 'none'
          }
        },
        {
          '0': {
            'label': 'Net income (net loss): your share<br> T5013/T5013A slip amount in box 23 or 26'
          },
          '2': {
            'num': '204'
          }
        },
        {
          '0': {
            'label': 'Income',
            'labelClass': 'bold'
          },
          '2': {
            'type': 'none'
          },
          '4': {
            'type': 'none'
          },
          '6': {
            'type': 'none'
          },
          '8': {
            'type': 'none'
          },
          '10': {
            'type': 'none'
          }
        },
        {
          '0': {
            'label': 'Gross rent'
          },
          '2': {
            'num': '205'
          }
        },
        {
          '0': {
            'label': 'Other Income'
          },
          '2': {
            'num': '206'
          }
        },
        {
          '0': {
            'label': 'Gross Income',
            'labelClass': 'bold'
          },
          '2': {
            'num': '207'
          }
        },
        {
          '0': {
            'label': 'Expenses',
            'labelClass': 'bold'
          },
          '2': {
            'type': 'none'
          },
          '4': {
            'type': 'none'
          },
          '6': {
            'type': 'none'
          },
          '8': {
            'type': 'none'
          },
          '10': {
            'type': 'none'
          }
        },
        {
          '0': {
            'label': '<b>Show detailed expenses ?</b>'
          },
          '2': {
            'num': '299',
            type: 'radio',
            'init': '2'
          },
          '4': {
            'type': 'none'
          },
          '6': {
            'type': 'none'
          },
          '8': {
            'type': 'none'
          },
          '10': {
            'type': 'none'
          }
        },
        {
          '0': {
            'label': 'Advertising'
          },
          '2': {
            'num': '209'
          },
          showWhen: '299'
        },
        {
          '0': {
            'label': 'Insurance'
          },
          '2': {
            'num': '210'
          },
          showWhen: '299'
        },
        {
          '0': {
            'label': 'Interest'
          },
          '2': {
            'num': '211'
          },
          showWhen: '299'
        },
        {
          '0': {
            'label': 'Maintenance and repairs'
          },
          '2': {
            'num': '212'
          },
          showWhen: '299'
        },
        {
          '0': {
            'label': 'Management and administration fees'
          },
          '2': {
            'num': '213'
          },
          showWhen: '299'
        },
        {
          '0': {
            'label': 'Motor vehicle expenses'
          },
          '2': {
            'num': '214'
          },
          showWhen: '299'
        },
        {
          '0': {
            'label': 'Office expenses'
          },
          '2': {
            'num': '215'
          },
          showWhen: '299'
        },
        {
          '0': {
            'label': 'Legal, accounting, etc'
          },
          '2': {
            'num': '216'
          },
          showWhen: '299'
        },
        {
          '0': {
            'label': 'Property taxes'
          },
          '2': {
            'num': '217'
          },
          showWhen: '299'
        },
        {
          '0': {
            'label': 'Salaries, wages'
          },
          '2': {
            'num': '218'
          },
          showWhen: '299'
        },
        {
          '0': {
            'label': 'Travel'
          },
          '2': {
            'num': '219'
          },
          showWhen: '299'
        },
        {
          '0': {
            'label': 'Utilities'
          },
          '2': {
            'num': '220'
          },
          showWhen: '299'
        },
        {
          '0': {
            'label': 'Total expenses',
            'labelClass': 'bold'
          },
          '2': {
            'num': '221'
          }
        },
        {
          '0': {
            'label': 'Expenses incurred for non-rental purposes'
          },
          '2': {
            'num': '222'
          }
        },
        {
          '0': {
            'label': 'Deductible expenses'
          },
          '2': {
            'num': '223'
          }
        },
        {
          '0': {
            'label': 'Net income (net loss) before adjusments',
            'labelClass': 'bold'
          },
          '2': {
            'num': '224'
          }
        },
        {
          '0': {
            'label': 'Co-owners: your share'
          },
          '2': {
            'num': '225'
          }
        },
        {
          '0': {
            'label': 'Other expenses'
          },
          '2': {
            'num': '226'
          }
        },
        {
          '0': {
            'label': 'Subtotal',
            'labelClass': 'bold'
          },
          '2': {
            'num': '227'
          }
        },
        {
          '0': {
            'label': 'Recaptured CCA'
          },
          '2': {
            'num': '228'
          }
        },
        {
          '0': {
            'label': 'Subtotal',
            'labelClass': 'bold'
          },
          '2': {
            'num': '229'
          }
        },
        {
          '0': {
            'label': 'Terminal loss'
          },
          '2': {
            'num': '230'
          }
        },
        {
          '0': {
            'label': 'Subtotal',
            'labelClass': 'bold'
          },
          '2': {
            'num': '231'
          }
        },
        {
          '0': {
            'label': 'CCA'
          },
          '2': {
            'num': '232'
          }
        },
        {
          '0': {
            'label': 'Net income (net loss)',
            'labelClass': 'bold'
          },
          '2': {
            'num': '233'
          }
        },
        {
          '0': {
            'label': 'Partnership: your share'
          },
          '2': {
            'num': '234'
          }
        },
        {
          '0': {
            'label': 'Other expenses'
          },
          '2': {
            'num': '235'
          }
        },
        {
          '0': {
            'label': 'Net Income (net loss)',
            'labelClass': 'bold'
          },
          '2': {
            'num': '236'
          }
        }]
    }
  }
})();
