(function() {
  'use strict';

  wpw.tax.global.formData.rentalsWcSummaryTotal = {
      formInfo: {
        abbreviation: 'rentalsWcSummaryTotal',
        title: 'Real Estate Rental Properties 5 Year Summary',
        neededRepeatForms: ['rentalsWcSummary'],
        showCorpInfo: true,
        description: [
          {
            text: 'This form shows the total summary of ALL real estate rental properties.'
          }
        ],
        category: 'Workcharts'
      },
      sections: [
        {

          rows: [
            {
              labelClass: 'fullLength'
            },
            {
              type: 'table', num: '2000'
            }
          ]
        }
      ]
    };
})();
