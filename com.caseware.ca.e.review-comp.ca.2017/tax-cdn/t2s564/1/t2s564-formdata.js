(function() {
  'use strict';

  wpw.tax.global.formData.t2s564 = {
    'formInfo': {
      abbreviation: 't2s564',
      'title': 'Ontario Book Publishing Tax Credit',
      //'subTitle': '(2009 and later tax years)',
      'schedule': 'Schedule 564',
      isRepeatForm: true,
      repeatFormData: {
        titleNum: '210'
      },
      'code': 'Code 0903',
      formFooterNum: 'T2 SCH 564 E (11)',
      headerImage: 'canada-federal',
      'showCorpInfo': true,
      'description': [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule to claim an Ontario book publishing tax credit (OBPTC) under section 95 ' +
              'of the <i> Taxation Act, 2007 </i>(Ontario). Complete a separate Schedule 564 for each eligible' +
              ' literary work.'
            },
            {
              label: 'The OBPTC is a refundable tax credit that is equal to 30% of the qualifying expenditures ' +
              'incurred during a tax year by an Ontario book publishing company (OBPC) for an eligible literary ' +
              'work up to a maximum credit of $30,000 per work. Qualifying expenditures include pre-production ' +
              'costs, marketing expenditures, and 50% of production costs paid by the OBPC for the publishing of ' +
              'an eligible literary work. After March 26th, 2009, 100% of expenditures that relate to publishing a ' +
              'digital or electronic version of the literary work and 50% of expenditures relating to transferring ' +
              'a prepared digital or electronic version of the literary work into or onto a form suitable for ' +
              'distribution are also eligible.'
            },
            {
              label: 'The criteria for a corporation to be eligible for the OBPTC include the eligibility ' +
              'requirements in Part 3 of this schedule.'
            },
            {
              label: 'Before claiming an OBPTC, the OBPC must obtain a Certificate of Eligibility from the ' +
              'Ontario Media Development Corporation (OMDC) for each eligible literary work. Enter the certificate ' +
              'information for this literary work in Part 2 of this schedule.'
            },
            {
              label: 'The OBPTC is considered government assistance under paragraph 12(1)(x) of the federal' +
              '<i> Income Tax Act</i> and must be included as income in the tax year the credit is received. ' +
              'The OBPTC is not considered government assistance under subsection 95(20) of the <i>Taxation' +
              ' Act 2007</i>, (Ontario) for the purposes of calculating the credit itself.'
            },
            {
              label: 'To claim the OBPTC, include the following with the <i> T2 Corporation Income Tax Return</i>:',
              sublist: [
                'a completed copy of this schedule for each literary work; and',
                'a certificate of eligibility (or copy) issued by the OMDC for each literary work.'
              ]
            }
          ]
        }
      ],
      category: 'Ontario Forms'
    },
    'sections': [
      {
        'header': 'Part 1 – Corporate information (please print)',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation name (from certificate of eligibility, if different from above)',
            'labelWidth': '80%'
          },
          {
            'type': 'infoField',
            'num': '100',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '1',
                    'max': '175'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'tn': '100'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'table',
            'num': '105'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Is the claim filed for an OBPTC earned through a partnership?*',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '150',
            'tn': '150',
            'labelWidth': '75%',
            'labelClass': 'tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '155'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '165'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '*When a corporate member of a partnership is claiming an amount for qualifying expenditures incurred by a partnership, complete a Schedule 564 for the partnership as if the partnership were a corporation. Each corporate partner, other than a limited partner, should file a separate Schedule 564 to claim the partner\'s share of the partnership\'s OBPTC. The allocated amounts can never exceed the amount of the partnership\'s OBPTC.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'header': 'Part 2 - Identifying the eligible literary work',
        'rows': [
          {
            'type': 'table',
            'num': '201'
          },
          {
            'type': 'table',
            'num': '225'
          },
          {
            'type': 'table',
            'num': '255'
          }
        ]
      },
      {
        'header': 'Part 3 - Eligibility',
        'fieldAlignRight': true,
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '1. Was the corporation a Canadian-controlled corporation throughout the tax year, as determined under sections 26 to 28 of the <i> Investment Canada Act</i>?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '300',
            'tn': '300'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '2. Did the corporation, for the tax year, carry on a book publishing business primarily through a permanent establishment in Ontario?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '310',
            'tn': '310'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '3. Was the corporation exempt from tax under Part III of the <i>Taxation Act, 2007</i> (Ontario)?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '320',
            'tn': '320'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '4. Is the corporation controlled by the author of the literary work, or by a person not dealing at arm\'s length with the author?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '330',
            'tn': '330'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If you answered <b>no</b> to question 1 or 2 or <b>yes</b> to question 3 or 4, then you are <b>not eligible</b> for the OBPTC. ',
            'labelClass': 'tabbed2'
          }
        ]
      },
      {
        'header': 'Part 4 - Qualifying expenditures',
        'spacing': 'R_mult_tn2',
        'rows': [
          {
            'label': 'Pre-production costs: Advances to author',
            'labelClass': 'bold'
          },
          {
            'label': 'Amount of non-refundable monetary advances made to the eligible Canadian author(s)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '400',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '400'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Pre-production costs: Technical expenditures*',
            'labelClass': 'bold'
          },
          {
            'label': 'Salaries and wages for editing, design, and project management',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '410',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '410'
                }
              },
              null
            ]
          },
          {
            'label': 'Freelance fees relating to editing, design, and research',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '420',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '420'
                }
              },
              null
            ]
          },
          {
            'label': 'Amounts for artwork, developing prototypes, set-up and typesetting',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '430',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '430'
                }
              },
              null
            ]
          },
          {
            'label': 'Salaries, wages, fees, and other amounts for scanning, editing, formatting, indexing, encryption, and establishing digital rights management or other technological protection measures (incurred after March 26, 2009)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '435',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '435'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of lines 410 to 435)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '437'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '438'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Production costs: Printing, assembling, binding and electronic distribution expenditures*',
            'labelClass': 'bold fullLength'
          },
          {
            'type': 'table',
            'num': 'multnum_table_1'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Marketing expenditures</b> (include expenditures incurred within 12 months following the publication date of the book)**',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Expenditures for book promotional tours of eligible Canadian authors (excluding costs for meals and entertainment)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '460',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '460'
                }
              },
              null
            ]
          },
          {
            'type': 'table',
            'num': 'multnum_table_2'
          },
          {
            'label': 'Salaries and wages of employees for the marketing of the book.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '480',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '480'
                }
              },
              null
            ]
          },
          {
            'label': 'Other amounts (not included above) incurred for promoting and marketing the book',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '490',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '490'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of lines 460 to 490)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '495'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '498'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': '<b> Total qualifying expenditures </b> (total of amounts A to D)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '500',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '500'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold tabbed'
          },
          {
            'label': 'Government assistance attributed to qualifying expenditures on line 500 (includes amounts received, entitled to be received, or reasonably expected to be received)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '510',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '510'
                }
              },
              null
            ]
          },
          {
            'label': 'Qualifying expenditures included in the amount on line 500 that were included in determining the available credit in a previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '520',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '520'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 510 <b> plus </b> line 520)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '525'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '528'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': '<b> Total qualifying expenditures eligible for the OBPTC </b> (amount E <b> minus</b> amount F)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '530',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '530'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Include only the expenditures for activities carried out primarily in Ontario.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'After March 26, 2009, include expenditures incurred for preparing a literary work for publication in one or more digital or electronic formats.',
            'labelClass': 'tabbed fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '** For expenditures incurred after March 29, 2011, include expenditures incurred within the period that begins 12 months before and ends 12 months after the date of publication of the literary work.',
            'labelClass': 'fullLength '
          }
        ]
      },
      {
        'header': 'Part 5 - Tax credit calculation',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': 'multnum_table_3'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit available to be claimed in the tax year',
            'labelClass': 'bold'
          },
          {
            'label': 'Maximum credit available',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '994',
                  'disabled': true,
                  'init': '30000'
                }
              },
              null
            ]
          },
          {
            'label': '<b> Deduct</b>: Total credit claimed in a previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '540',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '540'
                }
              },
              null
            ]
          },
          {
            'label': 'Unused credit available',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '996'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '995'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': '<b> Ontario book publishing tax credit</b> (lesser of amount H or amount I)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '550',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '550'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Or, if the corporation answered <b> yes</b> at line 150 (in Part 1), determine the partner\'s share of amount J:',
            'labelCLass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '901'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter amount J or K (as applicable) on line 466 of Schedule 5, <i>Tax Calculation' +
            ' Supplementary – Corporations</i>. If you are filing more than one Schedule 564, add the amounts' +
            ' from line J or K (as applicable) on all the schedules and enter the total amount on line 466 of ' +
            'Schedule 5. ',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Include the total of all credits previously claimed for the particular literary work by the filing corporation (and, in the case of OBPTCs earned through a partnership, include all amounts that were claimed by corporate partners.) When a corporate reorganization has occurred under section 87, subsection 88(1), or subsection 85(1) of the federal <i>Income Tax Act</i>, include the credits claimed for the particular literary work by the predecessor corporations, subsidiaries, and transferor corporations, respectively.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            label: '<i>Privacy Act</i>, Personal Information Bank number CRA PPU 047',
            labelClass: 'absoluteAlignRight'
          }
        ]
      }
    ]
  };
})();