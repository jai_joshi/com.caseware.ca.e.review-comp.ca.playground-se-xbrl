(function() {

  wpw.tax.create.calcBlocks('t2s564', function(calcUtils) {
    calcUtils.calc(function(calcUtils) {
      var num = '440';
      var num2 = '450';
      var midnum = '445';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '465';
      var num2 = '470';
      var midnum = '466';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '999';
      var num2 = '998';
      var midnum = '997';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils, field, form) {
      // Part 1: contact information//
      if (field('CP.950').get() || field('CP.951').get()) {
        field('110').assign(field('CP.951').get() + ' ' + field('CP.950').get());
      }
      else {
        field('110').assign('');
      }
      field('110').source(field('CP.950'));
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue(445, 'ratesOn', 780);
      calcUtils.getGlobalValue(466, 'ratesOn', 781);
      calcUtils.getGlobalValue(997, 'ratesOn', 782);
      calcUtils.getGlobalValue('120', 'CP', '959');

      // Part 4 Calculations //
      calcUtils.sumBucketValues('437', ['410', '420', '430', '435']);
      calcUtils.equals('438', '437');
      calcUtils.sumBucketValues('495', ['460', '470', '480', '490']);
      calcUtils.equals('498', '495');
      calcUtils.sumBucketValues('500', ['400', '438', '450', '498']);
      calcUtils.sumBucketValues('525', ['510', '520']);
      calcUtils.equals('528', '525');
      calcUtils.subtract('530', '500', '528');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 5 calculations //
      calcUtils.equals('999', '530');
      calcUtils.subtract('996', '994', '540');
      calcUtils.equals('995', '996');
      calcUtils.min('550', ['998', '995']);
      calcUtils.equals('910', '550');
      calcUtils.equals('920', '170');
      calcUtils.multiply('930', ['910', '920'], 1 / 100);
    });

  });
})();
