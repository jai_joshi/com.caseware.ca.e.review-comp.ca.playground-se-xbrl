(function() {
  wpw.tax.global.tableCalculations.t2s564 = {
    'multnum_table_1': {
      'num': 'multnum_table_1',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Expenditures',
            'trailingDots': true
          },
          '1': {
            'tn': '440'
          },
          '2': {
            'num': '440',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '445',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '450'
          },
          '7': {
            'num': '450',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'C'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'multnum_table_2': {
      'num': 'multnum_table_2',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Expenditures for meals and entertainment of eligible Canadian authors on promotional tours',
            'trailingDots': true
          },
          '1': {
            'tn': '465'
          },
          '2': {
            'num': '465',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '466',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '470'
          },
          '7': {
            'num': '470',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'multnum_table_3': {
      'num': 'multnum_table_3',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Total qualifying expenditures eligible for credit (amount G in Part 4)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '999'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '997',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '998'
          },
          '8': {
            'label': 'H'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    "105": {
      "fixedRows": true,
      "infoTable": true,
      "dividers": [1],
      "columns": [{
        "width": "60%",
        cellClass: 'alignLeft'
      },
        {
          cellClass: 'alignLeft'
        }],
      "cells": [{
        "0": {
          "label": "Name of person to contact for more information",
          "num": "110", "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
          "tn": "110",
          type: 'text'
        },
        "1": {
          "label": "Telephone number including area code",
          "num": "120",
          "tn": "120",
          'telephoneNumber': true,
          type: 'custom',
          format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
          validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
        }
      }]
    },
    "155": {
      "infoTable": true,
      "fixedRows": true,
      "columns": [{
        "width": "35%",
        "type": "none"
      },
        {}],
      "cells": [{
        "0": {
          "label": "If you answered <b> yes</b> to the question at line 150, what is the name of the partnership?",
          "labelWidth": "85%"
        },
        "1": {
          "num": "160", "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]},
          "tn": "160",
          type: 'text'
        }
      }]
    },
    "165": {
      "infoTable": true,
      "fixedRows": true,
      "columns": [
        {
          "type": "none"
        },
        { colClass: 'std-input-col-width'},
        {
          colClass: 'std-padding-width',
          type: 'none',
          label: '%'
        }
      ],
      "cells": [{
        "0": {
          "label": "Enter the percentage of the partnership's OBPTC allocated to the corporation",
          "labelClass": "fullLength"
        },
        "1": {
          "num": "170", "validate": {"or": [{"length": {"min": "1", "max": "6"}}, {"check": "isEmpty"}]},
          "tn": "170",
          decimals: 3,
          "filters": "decimals 2",
          cellClass: 'alignLeft',
          type: 'text'
        },
      }]
    },
    "201": {
      "infoTable": true,
      "fixedRows": true,
      "columns": [
        {colClass: 'std-padding-width', type: 'none'},
        {width: '250px', type: 'none'},
        {type: 'text'}
      ],
      "cells": [
        {
          '0': {
            'tn': '200'
          },
          '1': {
            'label': 'Certificate of eligibility number'
          },
          '2': {
            'num': '200', "validate": {"or": [{"length": {"min": "8", "max": "9"}}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'tn': '210'
          },
          '1': {
            "label": "Title of literary work"
          },
          '2': {
            'num': '210', "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'tn': '220'
          },
          '1': {
            "label": "Name(s) of author(s) /  iIlustrator(s)"
          },
          '2': {
            'num': '220', "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]}
          }
        }
      ]
    },
    "225": {
      "infoTable": true,
      "fixedRows": true,
      'dividers': [2, 3, 5],
      "columns": [
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'type': 'date',
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'type': 'date',
          colClass: 'std-input-width'
        }
      ],
      "cells": [
        {
          '0': {
            'tn': '230'
          },
          '1': {
            "label": "Date of publication"
          },
          '2': {
            'num': '230'
          },
          '3': {
            'tn': '240'
          },
          '4': {
            "label": "Date of certificate "
          },
          '5': {
            'num': '240'
          }
        }
      ]
    },
    "255": {
      "infoTable": true,
      "fixedRows": true,
      "columns": [{}],
      "cells": [{
        "0": {
          "label": "Genre",
          "num": "250", "validate": {"or": [{"length": {"min": "1", "max": "80"}}, {"check": "isEmpty"}]},
          "tn": "250",
          type: 'text'
        }
      }]
    },
    "901": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          colClass: 'small-input-width',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "250px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          cellClass: 'alignCenter',
          "type": "none"
        },
        {
          "type": "none"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        }],
      "cells": [{
        "0": {
          "label": "Amount J"
        },
        "1": {
          "num": "910"
        },
        "2": {
          "label": "x"
        },
        "3": {
          "label": "percentage (line 170 in Part 1)"
        },
        "4": {
          "num": "920",
          decimals: 3
        },
        "5": {
          "label": "%="
        },
        "7": {
          "num": "930",
          "input2Style": "doubleUnderline"
        },
        "8": {
          "label": "K"
        }
      }]
    }
  }
})();
