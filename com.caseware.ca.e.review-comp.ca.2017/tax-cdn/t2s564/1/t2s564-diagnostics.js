(function() {
  wpw.tax.create.diagnostics('t2s564', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('5640010', common.prereq(common.check('t2s5.466', 'isNonZero'), common.requireFiled('T2S564')));

    diagUtils.diagnostic('5640020', common.prereq(common.requireFiled('T2S564'),
        common.check(['550', 't2s5.466'], 'isZero', true)));

    diagUtils.diagnostic('5640030', common.prereq(common.and(
        common.requireFiled('T2S564'),
        common.check('550', 'isNonZero')),
        common.check(['200', '210', '220', '230', '240', '250'], 'isNonZero', true)));

    diagUtils.diagnostic('5640040', common.prereq(common.and(
        common.requireFiled('T2S564'),
        common.check('500', 'isNonZero')),
        common.check(['400', '410', '420', '430', '435', '440', '450', '460',
          '465', '470', '480', '490'], 'isNonZero')));

    diagUtils.diagnostic('5640050', common.prereq(common.and(
        common.requireFiled('T2S564'),
        common.check('150', 'isYes')),
        common.check('160', 'isNonZero')));

    diagUtils.diagnostic('5640060', common.prereq(common.and(
        common.requireFiled('T2S564'),
        common.check(['150'], 'isYes')),
        common.check('170', 'isNonZero')));

    diagUtils.diagnostic('5640070', common.prereq(common.and(
        common.requireFiled('T2S564'),
        common.check(['550'], 'isNonZero')),
        common.check(['300', '310', '320', '330'], 'isNonZero', true)));

  });
})();
