(function() {

  wpw.tax.create.formData('ratesNl', {
    formInfo: {
      abbreviation: 'ratesNl',
      title: 'Newfoundland and Labrador Rates Table',
      showCorpInfo: true,
      description: [
        {type: 'heading', text: 'Newfoundland and Labrador'}
      ],
      category: 'Rates Tables'
    },
    sections: [
      {
        'header': 'Schedule 300 - Newfoundland and Labrador Manufacturing and Processing Profits Tax Credit',
        'rows': [
          {
            'label': 'Rate for manufacturing and processing profits tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '100'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 301 - Newfoundland and Labrador Research and Development Tax Credit',
        'rows': [
          {
            'label': 'Rate for research and development tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '200'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 305 - Newfoundland and Labrador Capital Tax On Financial Institutions',
        'rows': [
          {
            'label': 'Rate before April 1, 2015',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '300'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate after March 31, 2015 and before January 1, 2016',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '400'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate after December 31, 2015',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '500'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Rates for the Calculation of Amount H',
            'labelClass': 'bold'
          },
          {
            'label': 'If Line 129 of Schedule 5 is Blank or Zero',
            'labelClass': 'bold tabbed'
          },
          {
            'label': 'Numerator',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '501'
                }
              }
            ]
          },
          {
            'label': 'Denominator',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '502'
                }
              }
            ]
          },
          {
            'label': 'If Line 169 of Schedule 5 is Blank or Zero',
            'labelClass': 'bold tabbed'
          },
          {
            'label': 'Numerator',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '505'
                }
              }
            ]
          },
          {
            'label': 'Denominator',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '506'
                }
              }
            ]
          },
          {
            'label': 'Otherwise',
            'labelClass': 'bold tabbed'
          },
          {
            'label': 'Numerator',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '503'
                }
              }
            ]
          },
          {
            'label': 'Denominator',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '504'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 307 - Newfoundland and Labrador Corporation Tax Calculation',
        'rows': [
          {
            'label': 'Tax at the lower rate',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '600'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Tax at the higher rate before January 1, 2016',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '700'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Tax at the higher rate after December 31, 2015',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '800'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'S21W - Provincial or Territorial Foreign Income Tax Credits and Federal Logging Tax Credit',
        'rows': [
          {
            'label': 'Number of days in the tax year after January 1, 2015 and before January 1, 2016',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '801'
                }
              }
            ]
          },
          {
            'label': 'Number of days in the tax year after December 31, 2015',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '802'
                }
              }
            ]
          },
          {
            'label': 'Prorated provincial or territorial tax rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '900',
                  'decimals': 4
                }
              }
            ],
            'indicator': '%'
          }
        ]
      }
    ]
  });
})();
