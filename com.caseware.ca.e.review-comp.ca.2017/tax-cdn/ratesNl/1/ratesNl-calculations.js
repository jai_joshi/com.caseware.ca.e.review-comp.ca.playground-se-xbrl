(function() {

  wpw.tax.create.calcBlocks('ratesNl', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field) {
     field('100').assign(9);
     field('200').assign(15);
     field('300').assign(4);
     field('400').assign(5);
     field('500').assign(6);
     field('501').assign(1);
     field('502').assign(2);
     field('503').assign(1);
     field('504').assign(3);
     field('505').assign(1);
     field('506').assign(1);
     field('600').assign(3);
     field('700').assign(14);
     field('800').assign(15);

      var date2016 = wpw.tax.date(2016, 1, 1);
      var date2016Comparisons = calcUtils.compareDateAndFiscalPeriod(date2016);

      //days in tax year
      field('801').assign(date2016Comparisons.daysBeforeDate);
      field('802').assign(date2016Comparisons.daysAfterDate);
      //pro-rate
      field('900').assign(field('801').get() * field('700').get() / 366 + field('802').get() * field('800').get() / 366)
    });
  });
})();
