(function() {

  wpw.tax.create.formData('t2s8w', {
    formInfo: {
      abbreviation: 'T2S8W',
      isRepeatForm: true,
      repeatFormData: {
        linkedRepeatTable: ['t2s8.090', 't2s8.100', 't2a13.090', 't2a13.100'],
        titleNum: 'title'
      },
      neededRepeatForms: ['t2s8add'],
      schedule: 'SCHEDULE 8 WORKCHART',
      title: 'CCA WORKCHART',
      showCorpInfo: true,
      category: 'Workcharts'
    },
    sections: [
      {
        'header': 'CCA Class Information',
        'rows': [
          {
            'label': 'Note 1: Use this link to access Income Tax Regulations, Schedule II, for detailed descriptions of CCA classes:',
            'labelClass': 'bold fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'link',
            'link': 'http://laws-lois.justice.gc.ca/eng/regulations/C.R.C.%2C_c._945/page-25.html#h-106'
          },
          {
            'label': 'Note 2: For all CCA Classes other than Classes 10.1, 13 and 14, you can access or add ' +
            'Depreciable Assets allocated to this class by using ' +
            '"Depreciable Assets allocated to this Class: Historical Summary & Current Year Additions / Disposals" ' +
            'table at the bottom of this workchart.',
            'labelClass': 'bold fullLength',
            'labelWidth': '55%'
          },
          {
            'label': 'Note 3:  For CCA class 10.1, use the section above "Class 10.1-Other asset Information" ' +
            'to add current year addition',
            'labelClass': 'bold fullLength',
            'labelWidth': '55%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'indicatorColumn': true,
            inputClass: 'std-input-col-width-3',
            'label': 'Your own CCA Class Description',
            'num': '103'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'indicatorColumn': true,
            'label': 'Business Name',
            inputClass: 'std-input-col-width-3',
            'num': '1031',
            'showWhen': {
              'fieldId': '101',
              'compare': {
                'is': 14.1
              }
            }
          },
          {
            'type': 'infoField',
            'label': 'Was this asset an "Eligible Capital Property" before  January 1st, 2017?',
            'num': '1032',
            'inputType': 'radio',
            tipGuidance: 'If this question is Yes and the tax year ends before 2027, the CCA rate is 7%. In all other cases, the CCA rate is 5%',
            'showWhen': {
              'fieldId': '101',
              'compare': {
                'is': 14.1
              }
            }
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'label': 'Does Terminal loss apply to this Class?',
            'num': '104',
            'inputType': 'radio',
            'init': '2'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'label': 'If it is rental property(under regulation 1100(11)), do you want to link this property to a Rental Income Workchart by Property?',
            'num': '105',
            'inputType': 'radio',
            'init': '2'
          },
          {
            'type': 'infoField',
            'label': 'If yes, please select the property to be linked ?',
            'indicatorColumn': true,
            'num': '106',
            'inputType': 'dropdown',
            'width': '58%'
          }
        ]
      },
      {
        'header': 'Asset Information',
        'showWhen': {
          'or': [
            {
              'fieldId': '101',
              'compare': {
                'isNot': '13'
              }
            },
            {
              'fieldId': '101',
              'compare': {
                'isNot': '14'
              }
            },
            {
              'fieldId': '101',
              'compare': {
                'isNot': '10.1'
              }
            }
          ]
        },
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'indicatorColumn': true,
            'label': 'GL Account # where this asset is recorded',
            'num': '401',
            'width': '20%'
          },
          {
            'type': 'infoField',
            'indicatorColumn': true,
            'label': 'GL Account name',
            'num': '402',
            'width': '40%'
          },
          {
            'type': 'infoField',
            'indicatorColumn': true,
            'label': 'Asset Description',
            'num': '403',
            'width': '40%'
          },
          {
            'type': 'infoField',
            'indicatorColumn': true,
            'label': 'Asset Serial #/ID #',
            'num': '404',
            'width': '20%'
          },
          {
            'type': 'infoField',
            'indicatorColumn': true,
            'label': 'Asset Location',
            'num': '405',
            'width': '40%'
          },
          {
            'type': 'infoField',
            'indicatorColumn': true,
            'label': 'PO #/Ref #',
            'num': '406',
            'width': '20%'
          },
          {
            'type': 'infoField',
            'indicatorColumn': true,
            'label': 'Warranty #(if applicable)',
            'num': '407',
            'width': '20%'
          },
          {
            'type': 'infoField',
            'indicatorColumn': true,
            'label': 'Warranty expiration date(if applicable)',
            'num': '408',
            'inputType': 'date'
          },
          {
            'label': 'Other Notes'
          },
          {
            'type': 'infoField',
            'num': '409',
            'inputType': 'textArea'
          }
        ]
      },
      {
        'header': 'Class 13 (Leaseholds) - Other Asset Information',
        'showWhen': {
          'fieldId': '101',
          'compare': {
            'is': '13'
          }
        },
        'rows': [
          {
            'type': 'infoField',
            'indicatorColumn': true,
            'label': 'Termination date (including next renewal term)',
            'num': '111',
            'inputType': 'date'
          },
          {
            'label': 'Number of amortization periods for current year additions<br>(Minimum 5, Maximum 40)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '112',
                  'disabled': true
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Class 14 - Other Asset Information',
        'showWhen': {
          'fieldId': '101',
          'compare': {
            'is': '14'
          }
        },
        'rows': [
          {
            'label': 'Current year additions only',
            'labelClass': 'bold'
          },
          {
            'type': 'infoField',
            'label': 'Date acquired',
            'indicatorColumn': true,
            'num': '130',
            'inputType': 'date',
            'labelWidth': '80%'
          },
          {
            'type': 'infoField',
            'label': 'Termination date',
            'indicatorColumn': true,
            'num': '131',
            'inputType': 'date',
            'labelWidth': '80%'
          },
          {
            'label': 'Number of days of ownership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '132'
                }
              }
            ]
          },
          {
            'label': 'Number of days remaining in current taxation year including the day of acquisition',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '133'
                }
              }
            ]
          },
          {
            'label': 'Number of amortization periods (Number of days of ownership / 365)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '134'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Class 10.1 (Passenger vehicle cost more than $30,000) - Other Asset Information',
        'showWhen': {
          'fieldId': '101',
          'compare': {
            'is': '10.1'
          }
        },
        'rows': [
          {
            'type': 'infoField',
            'indicatorColumn': true,
            'label': 'Date acquired',
            'num': '150',
            'inputType': 'date',
            'labelWidth': '80%'
          },
          {
            'type': 'infoField',
            'indicatorColumn': true,
            'label': 'Make /Model',
            'num': '151',
            'labelWidth': '74%',
            'width': '20%'
          },
          {
            'type': 'infoField',
            'indicatorColumn': true,
            'label': 'VIN #',
            'num': '152',
            'labelWidth': '74%',
            'width': '20%'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': 'Half-year rule applied?',
            'num': '153',
            'init': '1',
            'labelWidth': '823px'
          },
          {
            'type': 'infoField',
            'indicatorColumn': true,
            'label': 'Disposal date',
            'num': '154',
            'inputType': 'date',
            'labelWidth': '80%'
          },
          {
            'label': 'Cost before GST and PST, or HST',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '155'
                }
              }
            ]
          },
          {
            'label': 'GST or HST payable on purchase',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '156'
                }
              }
            ]
          },
          {
            'label': 'PST payable on purchase',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '157'
                }
              }
            ]
          },
          {
            'label': 'GST or HST input tax credit/rebate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '158'
                }
              }
            ]
          },
          {
            'label': 'PST input tax credit/rebate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '159'
                }
              }
            ]
          },
          {
            'label': 'Trade-in value',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '160'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Class 15 - Other Asset Information',
        'showWhen': {
          'fieldId': '101',
          'compare': {
            'is': '15'
          }
        },
        'rows': [
          {
            'label': 'Number of cords or board feet cut in the taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '170'
                }
              }
            ]
          },
          {
            'label': 'Number of cords or board feet available-to-use',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '171'
                }
              }
            ]
          },
          {
            'label': 'CCA rate to be used in current tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '172'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'CCA Workchart',
        'rows': [
          {
            'type': 'table',
            'num': '199'
          }
        ]
      },
      {
        'header': 'Classes 43.1 and 43.2 only',
        'showWhen': {
          'and': [
            {
              'or': [
                {
                  'fieldId': '101',
                  'compare': {
                    'is': '43.1'
                  }
                },
                {
                  'fieldId': '101',
                  'compare': {
                    'is': '43.2'
                  }
                }
              ]
            },
            {
              'fieldId': '203',
              'check': 'isPositive'
            }
          ]
        },
        'rows': [
          {
            'type': 'table',
            'num': '700'
          }
        ]
      },
      {
        'header': 'Depreciable Assets allocated to this Class: Historical Summary & Current Year Additions / Disposals',
        'showWhen': {
          'and': [
            {
              'fieldId': '101',
              'compare': {
                'isNot': '13'
              }
            },
            {
              'fieldId': '101',
              'compare': {
                'isNot': '14'
              }
            },
            {
              'fieldId': '101',
              'compare': {
                'isNot': '10.1'
              }
            }
          ]
        },
        'linkedRepeatForm': 't2s8add',
        'rows': [
          {
            'type': 'table',
            'num': '600'
          }
        ]
      },
      {
        'header': 'Prior year additions (Class 13)',
        'showWhen': {
          'fieldId': '101',
          'compare': {
            'is': '13'
          }
        },
        'rows': [
          {
            'type': 'table',
            'num': '400'
          }
        ]
      },
      {
        'header': 'Prior year additions (Class 14)',
        'showWhen': {
          'fieldId': '101',
          'compare': {
            'is': '14'
          }
        },
        'rows': [
          {
            'type': 'table',
            'num': '500'
          }
        ]
      }
    ]
  })
})();
