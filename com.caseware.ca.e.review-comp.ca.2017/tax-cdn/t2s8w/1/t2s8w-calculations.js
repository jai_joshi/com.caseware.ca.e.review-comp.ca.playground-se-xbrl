(function () {

  function ccaCalculation(calcUtils, field, form, provincial) {
    var taxStart = field('CP.tax_start').get();
    var taxEnd = field('CP.tax_end').get();

    var currentCcaClass = field('101').get();
    if (currentCcaClass) {
      var currentCCAClassValue = wpw.tax.codes.ccaClasses['CCA Classes'][currentCcaClass];
    }
    if (wpw.tax.utilities.isNullUndefined(provincial)) {
      provincial = '';
    }
    var ccaRateField = field('212' + provincial);
    var uccBegField = field('201' + provincial);

    var totalAdditionField = field('203' + provincial);
    var additionHalfYearField = field('203-1' + provincial);
    var additionFullYearField = field('203-2' + provincial);

    var totalAdjField = field('205' + provincial);
    var adjItcField = field('205-1' + provincial);
    var adjGstField = field('205-2' + provincial);
    var adjFullYearField = field('205-3' + provincial);
    var adjHalfYearField = field('205-4' + provincial);

    var annualCcaField = field('197' + provincial);
    var netPriorAddField = field('198' + provincial);
    var disposalField = field('207' + provincial);
    var uccBeforeCcaField = field('208' + provincial);
    var halfYearAdjField = field('211' + provincial);
    var baseForCcaField = field('210' + provincial);
    var maxCcaField = field('214' + provincial);
    var recaptureField = field('213' + provincial);
    var terminalLossField = field('215' + provincial);
    var ccaClaimedField = field('217' + provincial);
    var uccEndField = field('220' + provincial);

    var isHalfYear = additionHalfYearField.get() > 0 || adjHalfYearField.get() > 0;
    var isTerminalLoss = field('104').get() == 1;
    var isRecapture = field('209').get() == 1;

    var isStraighLineClasses =
        (
            currentCcaClass == '24' ||
            currentCcaClass == '27' ||
            currentCcaClass == '29' ||
            currentCcaClass == '34'
        );

    //to get cca rate num 212
    if (currentCcaClass) {
      if (currentCcaClass == 15) {
        field('172').assign(field('170').get() / field('171').get() * 100);
        ccaRateField.assign(field('172').get());
      }
      else {
        //Tax rule: Enter a rate only if the declining balance method is used. For any other method (ex: straight-line method), enter NA.
        var rate = currentCCAClassValue.substring(currentCCAClassValue.indexOf('(') + 1, currentCCAClassValue.indexOf('%'));
        ccaRateField.assign(parseInt(rate) || 'NA');
      }
    }
    else {
      ccaRateField.set(0);
    }

    //For class 10.1
    if (currentCcaClass == 10.1) {
      calcUtils.removeValue([annualCcaField, netPriorAddField, adjItcField, adjGstField, adjHalfYearField, additionHalfYearField, disposalField]);
      additionFullYearField.disabled(true);
      var hasCurrentYearAddition = wpw.tax.actions.isWithinFiscalYear(field('150').get());
      var hasCurrentYearDisposal = wpw.tax.actions.isWithinFiscalYear(field('154').get());
      field('153').assign(hasCurrentYearAddition ? '1' : '2');
      var isHalfYearRule = (field('153').get() == '1');
      //check for addition cost, anything less than 30k need to be on class 10 instead.
      if (field('155').get() > 30000 && hasCurrentYearAddition) {
        additionFullYearField.assign(30000 +
            (30000 * ((field('156').get() / field('155').get()) + (field('157').get() / field('155').get()))) -
            field('158').get() - field('159').get());
      } else {
        additionFullYearField.assign(0);
      }
      getUccBeforeCca();
      //refer to form T4044, employment expense
      if (hasCurrentYearAddition && hasCurrentYearDisposal) {
        //if buy and sell in the current year, cant not claim any CCA
        maxCcaField.assign(0);
        uccEndField.assign(0);
      }
      else {
        maxCcaField.assign(isHalfYearRule ?
            uccBeforeCcaField.get() / 2 * ccaRateField.get() / 100 :
            uccBeforeCcaField.get() * ccaRateField.get() / 100);
        //assign value to fields for S8 display purpose
        if (isHalfYearRule) {
          halfYearAdjField.assign(uccBeforeCcaField.get() / 2);
        }
        baseForCcaField.assign(uccBeforeCcaField.get() - halfYearAdjField.get());
      }
      //for short-year end
      applyProratedCcaAllowance(calcUtils);
      //cca claimed
      applyOptimizationCalcs(calcUtils);
      //ucc closing
      uccEndField.assign(hasCurrentYearDisposal ? 0 : uccBeforeCcaField.get() - ccaClaimedField.get());
    }
    //For class 13
    else if (currentCcaClass == 13) {
      calcUtils.removeValue([netPriorAddField, adjItcField, adjGstField, adjHalfYearField]);
      additionHalfYearField.disabled(false);
      additionFullYearField.disabled(false);
      disposalField.disabled(false);

      var amortizationPeriod = parseInt((wpw.tax.actions.calculateDaysDifference(taxStart, field('111').get())) / 365);
      amortizationPeriod = Math.max(Math.min(amortizationPeriod, 40), 5);
      field('112').assign(amortizationPeriod);

      field('400').getRows().forEach(function (row) {
        var amortizationPeriod = 0;
        if (wpw.tax.utilities.isNullUndefined(row[1].get())) {
          amortizationPeriod = 40;
        }
        else {
          amortizationPeriod = parseInt((wpw.tax.actions.calculateDaysDifference(row[1].get(), field('111').get()) / 365));
          amortizationPeriod = Math.max(Math.min(amortizationPeriod, 40), 5);
        }
        row[3].assign(amortizationPeriod);
        row[4].assign(Math.round(row[2].get() / row[3].get()));
      });

      annualCcaField.assign(field('400').total(4).get());
      getUccBeforeCca();
      //terminal loss incurred
      if (isTerminalLoss) {
        maxCcaField.assign(uccBeforeCcaField.get());
        ccaClaimedField.assign(0);
        terminalLossField.assign(maxCcaField.get());
        uccEndField.assign(0);
      }
      else {
        if (uccBeforeCcaField.get() > annualCcaField.get()) {
          var currentYearAllowance = (additionHalfYearField.get() / 2 + additionFullYearField.get()) / field('112').get();
          maxCcaField.assign((isNaN(currentYearAllowance) ? 0 : currentYearAllowance) + annualCcaField.get());
        }
        else {
          maxCcaField.assign(uccBeforeCcaField.get());
        }
        //for short-year end
        applyProratedCcaAllowance(calcUtils);
        //cca claimed
        applyOptimizationCalcs(calcUtils);
        //ucc closing
        getUccEnding()
      }
      //for num210 transfer to s8
      baseForCcaField.assign(uccBeforeCcaField.get() - halfYearAdjField.get());
    }
    //For class 14
    else if (currentCcaClass == 14) {
      calcUtils.removeValue([annualCcaField, netPriorAddField, adjItcField, adjGstField, adjHalfYearField, additionHalfYearField]);
      field('132').assign(wpw.tax.actions.calculateDaysDifference(field('130').get(), field('131').get()));
      field('133').assign(wpw.tax.actions.calculateDaysDifference(field('130').get(), taxEnd));
      field('134').assign(field('132').get() / 365);

      //calculate amortizationPeriod
      field('500').getRows().forEach(function (row) {
        var amortizationPeriod = parseInt((wpw.tax.actions.calculateDaysDifference(row[1].get(), row[2].get()) / 365));
        row[4].assign(amortizationPeriod);
        var annualCcaVal = 0;
        if (wpw.tax.actions.calculateDaysDifference(row[1].get(), field('cp.tax_start').get()) != 0) {
          var dayDiff = wpw.tax.actions.calculateDaysDifference(row[2].get(), field('cp.tax_start').get());
          if (dayDiff >= 0 && wpw.tax.actions.isWithinFiscalYear(row[2].get())) {
            annualCcaVal = row[3].get() / (row[4].get() * 365) * (dayDiff + 1);
          } else if (dayDiff >= 0 && !wpw.tax.actions.isWithinFiscalYear(row[2].get())) {
            annualCcaVal = row[3].get() / row[4].get()
          }
        }
        row[5].assign(Math.round(annualCcaVal));
      });
      annualCcaField.assign(field('500').total(5).get());
      getUccBeforeCca();

      if (isTerminalLoss) {//terminal loss incurred
        maxCcaField.assign(uccBeforeCcaField.get());
        ccaClaimedField.assign(0);
        terminalLossField.assign(maxCcaField.get());
        uccEndField.assign(0);
      }
      else {
        if (uccBeforeCcaField.get() > annualCcaField.get()) {
          if (field('134').get() != 0) {
            currentYearAllowance = totalAdditionField.get() / field('134').get() * field('133').get() / field('cp.Days_Fiscal_Period').get();
          }
          maxCcaField.assign((isNaN(currentYearAllowance) ? 0 : currentYearAllowance) + annualCcaField.get());
          //todo: flag diagnostic and calcs for last year before it expired-#of days in the year until it expired
          //todo: divide by life long day 8 original cost
        }
        else {
          maxCcaField.assign(uccBeforeCcaField.get());
        }
        //for short-year end
        applyProratedCcaAllowance(calcUtils);
        //cca claimed
        applyOptimizationCalcs(calcUtils);
        //ucc closing
        getUccEnding();
      }
      //for num210 transfer to s8
      baseForCcaField.assign(uccBeforeCcaField.get() - halfYearAdjField.get());
    }
    //For class 14.1
    else if (currentCcaClass == 14.1) {
      //For the first 10 years, the depreciation rate for Class 14.1 will be 7% in respect of expenditures incurred before January 1, 2017.
      var eligibleDate = wpw.tax.date(2027, 1, 1);
      var taxEndDate = field('cp.tax_end').get();
      var isExtraCcaEligible = calcUtils.dateCompare.lessThan(taxEndDate, eligibleDate) && (field('1032').get() == '1');

      if (isExtraCcaEligible) {
        ccaRateField.assign(7);
      }
      else {
        ccaRateField.assign(5);
      }
      //check if there is any value on s10, if not then allow user to manually enter value
      var tn445S10 = field('T2S10.445');
      var isS10Eligible = (tn445S10.get() == 0);
      if (!isS10Eligible) {
        //only run these calcs for the FIRST 14.1 form
        var allT2S8WForms = calcUtils.allRepeatForms('T2S8W').filter(function (form) {
          return form.field('101').get() == '14.1';
        });

        if (calcUtils.form().sequence == allT2S8WForms[0].sequence) {
          if (tn445S10.get() > 0) {
            uccBegField.assign(tn445S10.get());
            uccBegField.source(tn445S10);
            adjFullYearField.assign(0);
          } else {
            adjFullYearField.assign(tn445S10.get());
            adjFullYearField.source(tn445S10);
            uccBegField.assign(0);
          }
          additionHalfYearField.assign(field('T2S10.470').get());
          additionHalfYearField.source(field('T2S10.470'));
        }
      } else {
        uccBegField.disabled(false);
        adjFullYearField.disabled(false);
        additionHalfYearField.disabled(false);
      }

      getTotalAdj();
      getTotalAdd();

      uccBeforeCcaField.assign(
          uccBegField.get() +
          totalAdditionField.get() +
          totalAdjField.get() +
          field('809').get() -
          disposalField.get()
      );
      halfYearAdjField.assign(Math.max(0, (additionHalfYearField.get() - disposalField.get()) / 2));
      baseForCcaField.assign(uccBeforeCcaField.get() - halfYearAdjField.get());

      if (uccBeforeCcaField.get() < 0) {
        field('209').assign(1);
      }
      else {
        field('209').assign(2);
      }

      if (isRecapture || isTerminalLoss) {
        if (isRecapture) {
          //recapture
          calcUtils.removeValue([halfYearAdjField, baseForCcaField, terminalLossField, ccaClaimedField]);
          field('104').assign(2);
          maxCcaField.assign(uccBeforeCcaField.get());
          recaptureField.assign(maxCcaField.get() * (-1));
          uccEndField.assign(0);
        }
        else {
          maxCcaField.assign(uccBeforeCcaField.get());
          ccaClaimedField.assign(0);
          terminalLossField.assign(maxCcaField.get());
          uccEndField.assign(0);
        }
      }
      else {
        maxCcaField.assign(baseForCcaField.get() * ccaRateField.get() / 100);
        //for short-year end
        applyProratedCcaAllowance(calcUtils);
        //cca claimed
        applyOptimizationCalcs(calcUtils);
        //lesser of $500 rule applies for an asset acquired prior to 2017;
        if (isExtraCcaEligible) {
          var ccaClaimedVal;
          if (baseForCcaField.get(0) > 500) {
            ccaClaimedVal = Math.max(maxCcaField.get(), 500);
          } else {
            ccaClaimedVal = Math.min(Math.max(maxCcaField.get(), 500), maxCcaField.get());
          }
          ccaClaimedField.assign(ccaClaimedVal);
        }
        //ucc closing
        getUccEnding();
      }
    }
    //calculate CCA for all other classes:
    else {
      //remove disabled field when user change cra classes
      uccBegField.disabled(false);
      additionHalfYearField.disabled(false);
      adjFullYearField.disabled(false);
      runCcaCalcs(calcUtils)
    }

    function getTotalAdj() {
      totalAdjField.assign(-adjItcField.get() - adjGstField.get() + adjHalfYearField.get() + adjFullYearField.get());
    }

    function getTotalAdd() {
      totalAdditionField.assign(additionHalfYearField.get() + additionFullYearField.get());
    }

    function getUccBeforeCca() {
      getTotalAdj();
      getTotalAdd();
      uccBeforeCcaField.assign(uccBegField.get() + totalAdjField.get() + totalAdditionField.get() - disposalField.get());
    }

    function getUccEnding() {
      uccEndField.assign(uccBeforeCcaField.get() - ccaClaimedField.get())
    }

    function runCcaCalcs(calcUtils) {
      var totalAddition = 0;
      var totalDisposal = 0;
      var repeatFormIds = [];
      var field = calcUtils.field;
      var table600 = field('600');

      for (var i = 0; i < table600.size().rows; i++) {
        var docId = table600.getRowInfo(i).repeatId;
        if (docId) {
          repeatFormIds.push('T2S8ADD-' + docId);
        }
      }

      if (repeatFormIds.length > 0) {
        repeatFormIds.forEach(function (formId) {
          //only update if the date of addition is between fiscal period, otherwise update to historical table
          //additional
          var dateAddition = form(formId).field('201').get();
          if (wpw.tax.actions.isWithinFiscalYear(dateAddition)) {
            totalAddition += form(formId).field('306').get();
          }
          //disposals
          var dateDisposal = form(formId).field('401').get();
          if (wpw.tax.actions.isWithinFiscalYear(dateDisposal)) {
            totalDisposal += form(formId).field('407').get();
          }
        });
        additionHalfYearField.assign(totalAddition);
        disposalField.assign(totalDisposal);
      } else {
        //if there is no third layer, allow user to enter value for half year addition
        additionHalfYearField.disabled(false);
        disposalField.disabled(false);
      }

      if (isHalfYear) {
        if (additionHalfYearField.get() > 0 && disposalField.get() >= 0) {
          halfYearAdjField.assign(Math.max((additionHalfYearField.get() - disposalField.get()) * (1 / 2), 0));
        }
        else {
          halfYearAdjField.assign(0);
        }
      }
      else {
        halfYearAdjField.assign(0);
      }
      getUccBeforeCca();
      //determine if recapture occurred
      if (uccBeforeCcaField.get() < 0) {
        field('209').assign(1);
      }
      else {
        field('209').assign(2);
      }

      if (isRecapture || isTerminalLoss) {
        if (isRecapture) {
          //recapture
          calcUtils.removeValue(['211', '210', '215', '217']);
          field('104').assign(2);
          maxCcaField.assign(uccBeforeCcaField.get());
          recaptureField.assign(maxCcaField.get() * (-1));
          uccEndField.assign(0);
        }
        //check for terminal loss
        else if (isTerminalLoss) {
          calcUtils.removeValue(['211', '210', '213', '217']);
          field('209').assign(2);
          maxCcaField.assign(uccBeforeCcaField.get());
          terminalLossField.assign(maxCcaField.get());
          uccEndField.assign(0);
        }
      }
      else {
        calcUtils.removeValue(['213', '215'], true);

        if (isStraighLineClasses) {
          field('198').disabled(false);
          adjItcField.disabled(false);
          adjGstField.disabled(false);
          adjFullYearField.disabled(false);
          adjHalfYearField.disabled(false);
          additionHalfYearField.disabled(false);
          additionFullYearField.disabled(false);
          disposalField.disabled(false);
        }
        halfYearAdjField.assign(Math.max((additionHalfYearField.get() + adjHalfYearField.get() - disposalField.get()) / 2, 0));
        getUccBeforeCca();
        baseForCcaField.assign(uccBeforeCcaField.get() - halfYearAdjField.get());

        if (isStraighLineClasses) {
          if (baseForCcaField.get() < 0) {
            maxCcaField.assign(0);
          } else {
            maxCcaField.assign(baseForCcaField.get() - field('198').get() / 4);
          }
        } else {
          maxCcaField.assign(baseForCcaField.get() * ccaRateField.get() / 100);
        }
        //for short-year end
        applyProratedCcaAllowance(calcUtils);
        //cca claimed
        applyOptimizationCalcs(calcUtils);
        //ending ucc
        getUccEnding();
      }
    }

    function applyProratedCcaAllowance(calcUtils) {
      //for short-year end, only take a portion of CCA
      var dayFiscalPeriod = calcUtils.form('CP').field('Days_Fiscal_Period').get();
      if (dayFiscalPeriod < 365) {
        maxCcaField.assign(maxCcaField.get() * dayFiscalPeriod / 365);
      }
    }

    function applyOptimizationCalcs(calcUtils) {
      var optimizationOption = calcUtils.form('OPTIMIZATIONWORKCHART').field('101').get();
      // Maximize all discretionary deductions, even creating or increasing Net loss for income tax purposes
      if (optimizationOption == 1) {
        ccaClaimedField.assign(maxCcaField.get());
      }
      // Apply discretionary deductions to get to zero targeted taxable income
      else if (optimizationOption == 2) {
        // Do not claim any discretionary deductions
      }
      else {
        ccaClaimedField.assign(0);
      }
    }
  }

  wpw.tax.create.calcBlocks('t2s8w', function (calcUtils) {

    calcUtils.calc(function (calcUtils, field, form) {
      var currentCCAClass = wpw.tax.codes.ccaClasses['CCA Classes'][field('101').get()];
      var titleString = '';
      //update cca class description
      if (currentCCAClass) {
        var description = currentCCAClass.substring(currentCCAClass.indexOf('('), currentCCAClass.indexOf(')').length);
        field('1000').cell(0, 1).assign(description);
        //update repeated form title using cca class number and field 103 own description
        titleString = currentCCAClass.substring(0, currentCCAClass.indexOf(':'));
        if (field('103').get()) {
          titleString = titleString + ' - ' + field('103').get();
        }
      } else {
        field('1000').cell(0, 1).set('');
      }
      field('title').assign(titleString);

    });

    calcUtils.calc(function (calcUtils, field, form) {
      //calcs for federal column
      ccaCalculation(calcUtils, field, form);

      var provinces = field('CP.prov_residence').get();
      field('provinces').assign(provinces);
      if (provinces['AB']) {
        ccaCalculation(calcUtils, field, form, '-A');
      } else if (provinces['QC']) {
        ccaCalculation(calcUtils, field, form, '-Q');
      }

      /**if this workchart is linked to rental
       *if CCA claimed >= rental income, only claim up to that rental income amount
       *if CCA claimed < rental income, claim the max CCA
       */

      var isLinkedWithRental = (field('105').get() == 1);
      if (isLinkedWithRental) {
        var propertyList = [];
        var rentalCalcsData = [];

        var rentalFormIds = calcUtils.allRepeatForms('T2RENTALWORKCHART');
        rentalFormIds.forEach(function (form) {
          //update the rental list
          propertyList.push({
            value: form.sequence,
            option: form.sequence + ' - ' + form.field('1001').get()
          });
          //get info from rental forms
          rentalCalcsData.push({
            rentalFormSequence: form.sequence,
            netIncomeBeforeAdj: form.field('9944').get()
          })
        });

        wpw.tax.create.retrieve('formData', 't2s8w').sections[0].rows[13].options = propertyList;
        field('106').config('options', propertyList);

        var ccaWorkchartLinkedFormIds = calcUtils.allRepeatForms('T2S8W').filter(function (form) {
          return form.field('105').get() == '1';
        });

        var ccaLinkedData = [];

        //get all cca info from all S8W
        ccaWorkchartLinkedFormIds.forEach(function (form) {
          ccaLinkedData.push({
            ccaClass: form.field('101').get(),
            classDescription: form.field('103').get(),
            ucc: form.field('201').get(),
            acquisition: form.field('203').get(),
            netAdjustment: form.field('205').get(),
            disposition: form.field('207').get(),
            uccBeforeCca: form.field('208').get(),
            halfYear: form.field('211').get(),
            ccaBase: form.field('210').get(),
            ccaRate: form.field('212').get(),
            ccaWorkchartSequence: form.sequence,
            rentalLinkedSequence: form.field('106').get(),
            recapture: form.field('213').get(),
            terminalLoss: form.field('215').get(),
            ccaAvailable: form.field('217').get(),
            uccEnding: form.field('220').get()
          })
        });

        if (ccaWorkchartLinkedFormIds.length !== 0) {
          rentalCalcsData.forEach(function (rentalData) {
            var totalRecapture = 0;
            var totalTerminalLoss = 0;
            ccaLinkedData.forEach(function (ccaData) {
              if (rentalData.rentalFormSequence == ccaData.rentalLinkedSequence) {
                totalRecapture += ccaData.recapture;
                totalTerminalLoss += ccaData.terminalLoss;
              }
              rentalData.totalRecapture = totalRecapture;
              rentalData.totalTerminalLoss = totalTerminalLoss;
            });
          });

          var pooledNetIncome = 0;

          rentalCalcsData.forEach(function (rentalData) {
            rentalData.netIncomeBeforecca =
                rentalData.netIncomeBeforeAdj +
                rentalData.totalRecapture -
                rentalData.totalTerminalLoss;
            pooledNetIncome += rentalData.netIncomeBeforecca;
          });

          var ccaIndex = 0;
          var ccaClaimed = 0;
          var ccaLeft = 0;
          //use the cca up until net income/loss reach 0
          while (pooledNetIncome >= 0) {
            var ccaAvailable = ccaLinkedData[ccaIndex].ccaAvailable;
            if (ccaAvailable < pooledNetIncome) {
              ccaClaimed = ccaAvailable;
              pooledNetIncome = pooledNetIncome - ccaAvailable;
            }
            else if (ccaAvailable >= pooledNetIncome) {
              ccaLeft = ccaAvailable - pooledNetIncome;
              ccaClaimed = pooledNetIncome;
              pooledNetIncome = 0;
            }
            ccaLinkedData[ccaIndex].ccaClaimed = ccaClaimed;
            ccaLinkedData[ccaIndex].ccaLeft = ccaLeft;
            ccaIndex++;
            if (ccaIndex >= ccaLinkedData.length) {
              break;
            }
          }
          //to get total cca claimed in rental forms
          rentalCalcsData.forEach(function (rentalData) {
            var totalCcaClaimed = 0;
            ccaLinkedData.forEach(function (ccaData) {
              if (rentalData.rentalFormSequence == ccaData.rentalLinkedSequence) {
                totalCcaClaimed += ccaData.ccaClaimed;
              }
              rentalData.totalCcaClaimed = totalCcaClaimed;
            });
          });

          //create this to be used in rental forms calcs
          field('998').assign(ccaLinkedData);
          field('999').assign(rentalCalcsData);

          //assign new cca claimed amount to num217
          ccaLinkedData.forEach(function (ccaData) {
            if (calcUtils.form().sequence == ccaData.ccaWorkchartSequence) {
              field('217').assign(ccaData.ccaClaimed);
            }
          });

          var isRecaptureApplied = (field('209').get() == 1);
          var isTerminalLossApplied = (field('104').get() == 1);
          var ccaClass = field('101').get();
          //calcs for UCC closing
          if ((!(isRecaptureApplied || isTerminalLossApplied)) && ccaClass == 10.1) {
            field('220').assign(field('210').get() - field('217').get());
          }
          else if (!(isRecaptureApplied || isTerminalLossApplied)) {
            field('220').assign(field('208').get() - field('217').get());
          }
        }
      }
    })
  });
})
();
