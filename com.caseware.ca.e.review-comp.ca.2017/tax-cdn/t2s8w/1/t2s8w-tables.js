(function() {
  var canadaProvinces = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.canadaProvinces, 'EN');
  var assetCode = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.assetCode, 'value');

  var expandableColumns = [
    {type: 'none'},
    {colClass: 'std-padding-width', type: 'none'},
    {colClass: 'std-padding-width', type: 'none'},
    {colClass: 'std-input-width', header: 'Federal', labelClass: 'noBorder', 'formField': true},
    {colClass: 'std-padding-width', type: 'none'},
    {
      colClass: 'std-input-width',
      header: 'Quebec',
      labelClass: 'noBorder',
      showWhen: {fieldId: 'provinces', has: {QC: true}},
      'formField': true
    },
    {
      colClass: 'std-padding-width', type: 'none',
      showWhen: {fieldId: 'provinces', has: {QC: true}}
    },
    {
      colClass: 'std-input-width',
      header: 'Alberta',
      labelClass: 'noBorder',
      showWhen: {fieldId: 'provinces', has: {AB: true}},
      'formField': true
    },
    {
      colClass: 'std-padding-width', type: 'none',
      showWhen: {fieldId: 'provinces', has: {AB: true}}
    }
  ];
  wpw.tax.create.tables('t2s8w', {
    '199': {
      type: 'table', num: '199', infoTable: true, fixedRows: true,
      columns: expandableColumns,
      cells: [
        {
          showWhen: {
            or: [
              {fieldId: '101', compare: {is: 13}},
              {fieldId: '101', compare: {is: 14}}
            ]
          },
          0: {'label': 'Annual CCA on prior year additions'},
          3: {num: '197', disabled: true},
          7: {num: '197-A'}
        },
        {
          showWhen: {
            or: [
              {fieldId: '101', compare: {is: '24'}},
              {fieldId: '101', compare: {is: '27'}},
              {fieldId: '101', compare: {is: '29'}},
              {fieldId: '101', compare: {is: '34'}}
            ]
          },
          0: {'label': 'For class 24,27,29 and 34 only', labelClass: 'bold'},
          3: {type: 'none'},
          5: {type: 'none'},
          7: {type: 'none'}
        },
        {
          showWhen: {
            or: [
              {fieldId: '101', compare: {is: '24'}},
              {fieldId: '101', compare: {is: '27'}},
              {fieldId: '101', compare: {is: '29'}},
              {fieldId: '101', compare: {is: '34'}}
            ]
          },
          0: {'label': 'Net prior year additions (half-year rule)'},
          3: {num: '198'},
          7: {num: '198-A'}
        },
        {
          0: {'label': 'UCC, beginning'},
          1: {tn: '201'},
          3: {num: '201'},
          7: {num: '201-A'}
        },
        {
          showWhen: {
            and: [
              {fieldId: '101', compare: {isNot: '13'}},
              {fieldId: '101', compare: {isNot: '14'}},
              {fieldId: '101', compare: {isNot: '10.1'}}
            ]
          },
          0: {'label': 'Adjustment: Previous year ITC'},
          1: {tn: '205'},
          2: {label: '-'},
          3: {num: '205-1'},
          7: {num: '205-A'}
        },
        {
          showWhen: {
            and: [
              {fieldId: '101', compare: {isNot: '13'}},
              {fieldId: '101', compare: {isNot: '14'}},
              {fieldId: '101', compare: {isNot: '10.1'}}
            ]
          },
          0: {'label': 'Adjustment: GST/PST rebate'},
          1: {tn: '205'},
          2: {label: '-'},
          3: {num: '205-2'},
          7: {num: '205-2-A'}
        },
        {
          0: {'label': 'Adjustments which is <b>not</b> relating to 1/2 year rule'},
          1: {tn: '205'},
          2: {label: '+/-'},
          3: {num: '205-3'},
          7: {num: '205-3-A'}
        },
        {
          showWhen: {
            and: [
              {fieldId: '101', compare: {isNot: '13'}},
              {fieldId: '101', compare: {isNot: '14'}},
              {fieldId: '101', compare: {isNot: '10.1'}}
            ]
          },
          0: {'label': 'Adjustments which is relating to 1/2 year rule'},
          1: {tn: '205'},
          2: {label: '+/-'},
          3: {num: '205-4'},
          7: {num: '205-4-A'}
        },
        {
          showWhen: {fieldId: '101', compare: {is: '14.1'}},
          0: {'label': 'Adjustment: Under 13(39)', labelClass: 'tabbed'},
          2: {label: '+/-'},
          3: {num: '809'},
          7: {num: '809-A'}
        },
        {
          showWhen: {
            and: [
              {fieldId: '101', compare: {isNot: '14'}},
              {fieldId: '101', compare: {isNot: '10.1'}}
            ]
          },
          0: {'label': 'Current year additions (1/2 year rule) <b>(Note 2 & 3)</b>'},
          1: {tn: '203'},
          2: {label: '+'},
          3: {num: '203-1'},
          7: {num: '203-1-A'}
        },
        {
          0: {'label': 'Current year additions <b>(Note 2 & 3)</b>'},
          2: {label: '+'},
          1: {tn: '203'},
          3: {num: '203-2'},
          7: {num: '203-2-A'}
        },
        {
          showWhen: {fieldId: '101', compare: {isNot: '10.1'}},
          0: {'label': 'Proceeds of disposition'},
          1: {tn: '207'},
          2: {label: '-'},
          3: {num: '207'},
          7: {num: '207-A'}
        },
        {
          0: {'label': 'UCC before CCA'},
          2: {label: '='},
          3: {num: '208', disabled: true},
          7: {num: '208-A', disabled: true}
        },
        {
          showWhen: {
            and: [
              {fieldId: '101', compare: {isNot: '13'}},
              {fieldId: '101', compare: {isNot: '14'}},
              {fieldId: '101', compare: {isNot: '10.1'}}
            ]
          },
          0: {'label': 'Recapture occurred?'},
          3: {num: '209', type: 'radio', init: '2'},
          7: {num: '209-A', type: 'radio', init: '2'}
        },
        {
          showWhen: {
            and: [
              {fieldId: '101', compare: {isNot: '10.1'}},
              {fieldId: '101', compare: {isNot: '13'}},
              {fieldId: '101', compare: {isNot: '14'}}
            ]
          },
          0: {'label': '1/2 year adjustments'},
          1: {tn: '211'},
          3: {num: '211', disabled: true},
          7: {num: '211-A', disabled: true}
        },
        {
          showWhen: {
            and: [
              {fieldId: '101', compare: {isNot: '10.1'}},
              {fieldId: '101', compare: {isNot: '13'}},
              {fieldId: '101', compare: {isNot: '14'}}
            ]
          },
          0: {'label': 'Base for CCA'},
          3: {num: '210', disabled: true},
          7: {num: '210-A', disabled: true}
        },
        {
          showWhen: {
            and: [
              {fieldId: '101', compare: {isNot: '10.1'}},
              {fieldId: '101', compare: {isNot: '13'}},
              {fieldId: '101', compare: {isNot: '14'}}
            ]
          },
          0: {'label': 'Rate'},
          1: {tn: '212'},
          3: {num: '212', disabled: true, decimals: wpw.tax.utilities.workchartDecimal},
          4: {label: '%'},
          7: {num: '212-A', disabled: true, decimals: wpw.tax.utilities.workchartDecimal},
          8: {label: '%'}
        },
        {
          0: {'label': 'Maximum allowable CCA'},
          3: {num: '214', disabled: true},
          7: {num: '214-A', disabled: true}
        },
        {
          showWhen: {fieldId: '101', compare: {isNot: '10.1'}},
          0: {'label': 'Recapture of CCA', disabled: true},
          1: {tn: '213'},
          3: {num: '213', disabled: true},
          7: {num: '213-A', disabled: true}
        },
        {
          showWhen: {fieldId: '101', compare: {isNot: '10.1'}},
          0: {'label': 'Terminal loss', disabled: true},
          1: {tn: '215'},
          3: {num: '215', disabled: true},
          7: {num: '215-A', disabled: true}
        },
        {
          0: {'label': 'CCA claimed'},
          1: {tn: '217'},
          3: {num: '217', disabled: true},
          7: {num: '217-A', disabled: true}
        },
        {
          0: {'label': 'UCC, ending'},
          1: {tn: '220'},
          3: {num: '220', disabled: true},
          7: {num: '220-A', disabled: true}
        }
      ]
    },
    '600': {
      showNumbering: true,
      linkedRepeatForm: 't2s8add',
      maxLoop: 100000,
      'columns': [
        {'header': 'Asset Description', linkedFieldId: '106', disabled: true, type: 'text'},
        {'header': 'Location', linkedFieldId: '108', disabled: true, type: 'text'},
        {
          'header': 'Investment tax credit code', linkedFieldId: '113', disabled: true, type: 'dropdown',
          options: [
            {'option': 'NA : No Income tax credit available', 'value': '1'},
            {'option': '1a : Qualified property', 'value': '2'},
            {
              'option': '1b : Qualified resource property acquired after March 28, 2012, and before January 01, 2014',
              'value': '3'
            },
            {
              'option': '1c : Qualified resource property acquired after December 31, 2013, and before January 01, 2016',
              'value': '4'
            },
            {'option': '2 : Child Care Spaces', 'value': '5'}
          ]
        },
        {'header': 'Acquisition date', colClass: 'std-input-width', type: 'date', linkedFieldId: '201', disabled: true},
        {
          'header': 'Adjusted cost base',
          filters: 'prepend $', linkedFieldId: '306', disabled: true
        },
        {'header': 'Disposal date', colClass: 'std-input-width', type: 'date', linkedFieldId: '401', disabled: true},
        {
          'header': 'Net proceeds',

          filters: 'prepend $', linkedFieldId: '404', disabled: true
        },
        {
          'header': 'Lower of adjusted cost base and net proceeds',

          filters: 'prepend $', linkedFieldId: '407', disabled: true
        }
      ]
    },
    '400': {
      showNumbering: true,
      hasTotals: true,
      maxLoop: 100000,
      'columns': [
        {'header': 'Additional Description'},
        {'header': 'Taxation year beginning', colClass: 'std-input-width', type: 'date'},
        {'header': 'Additions', filters: 'prepend $', colClass: 'std-input-width'},
        {'header': 'Amortization period', disabled: true, colClass: 'std-input-width'},
        {
          'header': 'Annual CCA',
          disabled: true,
          filters: 'prepend $',
          colClass: 'std-input-width',
          total: true
        },
        {'header': 'CCA claimed to date', filters: 'prepend $', colClass: 'std-input-width'}
      ]
    },
    '500': {
      showNumbering: true,
      hasTotals: true,
      maxLoop: 100000,
      'columns': [
        {'header': 'Asset Description', type: 'text'},
        {'header': 'Date acquired', colClass: 'std-input-width', type: 'date'},
        {'header': 'Termination date', colClass: 'std-input-width', type: 'date'},
        {'header': 'Additions', filters: 'prepend $', colClass: 'std-input-width'},
        {'header': 'Amortization period', disabled: true, colClass: 'std-input-width', decimals: 3},
        {
          'header': 'Annual CCA',
          disabled: true,
          filters: 'prepend $',
          colClass: 'std-input-width',
          total: true
        },
        {'header': 'CCA claimed to date', filters: 'prepend $', colClass: 'std-input-width'}
      ]
    },
    '700': {
      showNumbering: true,
      hasTotals: true,
      maxLoop: 100000,
      columns: [
        {
          header: 'Type of asset code',
          num: '301',
          tn: '301',
          type: 'dropdown',
          options: assetCode
        },
        {
          header: 'Province where the asset is located',
          num: '302',
          tn: '302',
          type: 'dropdown',
          options: canadaProvinces,
          colClass: 'std-input-width'
        },
        {
          header: 'Percentage allocated to the asset(%)',
          num: '303',
          tn: '303',
          filters: 'decimals 2',
          colClass: 'std-input-width',
          decimals: 3,
          total: true
        }
      ]
    },
    '1000': {
      'fixedRows': true,
      'columns': [
        {
          'header': 'CCA Class',
          cellClass: 'alignCenter',
          type: 'selector',
          colClass: 'std-input-width',
          // 'init': '1',
          selectorOptions: {
            title: 'CCA Classes',
            items: wpw.tax.codes.ccaClasses,
            hideKeys: true
          }
        },
        {
          'header': 'Class description',
          'disabled': true,
          type: 'text'
        }
      ],
      'cells': [
        {
          '0': {
            'num': '101'
          },
          '1': {
            'num': '102'
          }
        }
      ]
    }
  })
})();