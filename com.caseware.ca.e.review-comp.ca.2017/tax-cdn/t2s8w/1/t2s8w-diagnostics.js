(function() {
  wpw.tax.create.diagnostics('t2s8w', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0080200', common.prereq(common.and(common.requireFiled('T2S8'), function(tools) {
      return tools.field('101').get() == '43.1' || tools.field('101').get() == '43.2' && tools.field('203').isPositive();
    }), function(tools) {
      var table = tools.field('700');
      return tools.checkAll(table.getRows(), function(row) {
        return tools.requireOne([row[0], row[1], row[2]], 'isNonZero');
      });
    }));

    diagUtils.diagnostic('0080300', common.prereq(common.and(common.requireFiled('T2S8'), function(tools) {
      return tools.field('101').get() == '43.1' || tools.field('101').get() == '43.2';
    }), function(tools) {
      var table = tools.field('700');
      return tools.checkAll(table.getRows(), function(row) {
        if (tools.checkMethod([row[0], row[1], row[2]], 'isNonZero'))
          return tools.requireAll([row[0], row[1], row[2]], 'isNonZero');
        else return true;
      });
    }));

    diagUtils.diagnostic('0080303', common.prereq(common.and(common.requireFiled('T2S8'), function(tools) {
      return tools.field('101').get() == '43.1' || tools.field('101').get() == '43.2';
    }), function(tools) {
      var table = tools.field('700');
      var total = table.total().valueObj['2'];
      return tools.checkAll(table.getRows(), function(row) {
        return tools.requireOne(row[2], function() {
          return total == 100;
        });
      });
    }));

    diagUtils.diagnostic('008W.155', common.prereq(common.and(
        common.requireFiled('T2S8'),
        function(tools) {
          return tools.field('101').get() == '10.1';
        }), function(tools) {
      return tools.field('155').require(function() {
        return this.get() > 30000 || this.get() == 0;
      })
    }));

    diagUtils.diagnostic('008W.14-1', common.prereq(common.and(
        common.requireFiled('T2S8'),
        function(tools) {
          var forms = wpw.tax.form.allRepeatForms('t2s8w');
          return angular.forEach(forms, function(form) {
            return form.field('101').get() == '14.1' &&
                wpw.tax.utilities.dateCompare.lessThan(tools.field('cp.tax_end').get(), {day: 1, month: 1, year: 2017})
          });
        }), function(tools) {
      var forms = wpw.tax.form.allRepeatForms('t2s8w');
      var fields = [];
      angular.forEach(forms, function(form) {
        fields.push(tools.field(form.repeatFormId + '.101'));
      });
      return tools.requireAll(fields, function() {
        return this.get() != '14.1' ||
            !wpw.tax.utilities.dateCompare.lessThan(tools.field('cp.tax_end').get(), {day: 1, month: 1, year: 2017});
      })
    }));

    diagUtils.diagnostic('008W.class1WithManufacturing', common.prereq(common.and(
        common.requireFiled('T2S8'),
        function(tools) {
          return tools.field('101').get() == '1' &&
              common.requireFiled('T2S27');
        }), function(tools) {
      return tools.field('101').require(function() {
        return tools.field('203').isEmpty() ||
            tools.field('203').isZero();
      });
    }));

    diagUtils.diagnostic('008W.class12', common.prereq(common.and(
        common.requireFiled('T2S8'),
        function(tools) {
          return tools.field('101').get() == '12';
        }), function(tools) {
      return tools.field('101').require(function() {
        return this.get() != '12';
      });
    }));

    diagUtils.diagnostic('008W.class13Date', common.prereq(common.and(
        common.requireFiled('T2S8'),
        function(tools) {
          return tools.field('101').get() == '13' && tools.field('111').isEmpty();
        }), common.check('111', 'isNonZero')));

    diagUtils.diagnostic('008W.217AB', common.prereq(common.and(
        common.requireFiled('T2S8'),
        function(tools) {
          return tools.field('cp.prov_residence').get()['AB'] && tools.field('217-A');
        }),
        function(tools) {
          return tools.field('217').require(function() {
            return this.get() == tools.field('217-A').get();
          })
        }));

    diagUtils.diagnostic('008W.class10.1-1', common.prereq(common.and(
        common.requireFiled('T2S8'),
        function(tools) {
          return tools.field('101').get() == '10.1';
        }), function(tools) {
      return tools.requireOne(tools.list(['150', '154']), function() {
        return !wpw.tax.utilities.dateCompare.between(this.get(), tools.field('cp.tax_start').get(), tools.field('cp.tax_end').get())
      })
    }));

    diagUtils.diagnostic('008W.class10.1-2', common.prereq(common.and(
        common.requireFiled('T2S8'),
        function(tools) {
          return tools.field('101').get() == '10.1';
        }), function(tools) {
      return tools.field('150').require(function() {
        return !wpw.tax.utilities.dateCompare.between(this.get(), tools.field('cp.tax_start').get(), tools.field('cp.tax_end').get())
      }) || tools.requireAll(tools.list(['201', '203-1', '203-2', '205-1', '205-2', '205-3', '205-4']), 'isZero');
    }));

    diagUtils.diagnostic('008W.class14Date', {
            label: 'An entry is required for acquisition and termination dates for CCA calculation',
            category: 'TAX RULE',
            severity: 'warning'
        }, common.prereq(
          function(tools) {
            return tools.field('101').get() == '14' && tools.field('203').isNonZero();
          },
        function(tools) {
          tools.field('130').require('isFilled');
          return tools.field('131').require('isFilled');
        }
      ));

  });
})();
