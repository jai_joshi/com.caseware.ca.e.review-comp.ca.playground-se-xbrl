wpw.tax.create.importCalcs('T2S8W', function(importTools) {
  function getImportObj(taxPrepIdsObj) {
    //get and sort by repeatId and key of taxPrepIdsObj
    var output = {};
    Object.keys(taxPrepIdsObj).forEach(function(key) {
      importTools.intercept(taxPrepIdsObj[key], function(importObj) {
        if (!output[importObj.repeatIndex]) {
          output[importObj.repeatIndex] = {};
        }
        if (!output[importObj.repeatIndex][importObj.rowIndex]) {
          output[importObj.repeatIndex][importObj.rowIndex] = {};
        }
        output[importObj.repeatIndex][importObj.rowIndex][key] = importObj;
      });
    });
    return output;
  }

  var s8w_13_14_Obj = { //for table 400 / 500
    13: getImportObj({
      description: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA1',
      yearBegin: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA2',
      amortFed: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA4'
      //amortQc: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA8',
      //amortAb: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA16',
    }),
    14: getImportObj({
      description: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA19',
      acqDate: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA20',
      termDate: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA21',
      //additionsFed: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA3',
      //additionsQc: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA7',
      //additionsAb: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA15',
      amortFed: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA22'
      //amortQc: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA24',
      //amortAb: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA25'
    }),
    both: getImportObj({
      additionsFed: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA3',
      //additionsQc: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA7',
      //additionsAb: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA15',
      annualCcaFed: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA5',
      //annualCcaQc: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA9',
      //annualCcaAb: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA17',
      priorCcaFed: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA6'
      //priorCcaQc: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA10',
      //priorCcaAb: 'CCACat.FD08C[1].FED.SLIPAA[1].Ttw08cAA18'
    })
  };

    //To adjust different import class name
    var CCArfid=0;
    importTools.intercept('CCACat.FD08C[1].FED.Ttw08cA1', function(importObj) {
      var s8w1 = importTools.form('T2S8W', CCArfid);;
        if(s8w1!=null){
          s8w1 = importTools.form('T2S8W', CCArfid);
          if (importObj.value == "1a") {
            s8w1.setValue('101', 1.2);
          } else {
            s8w1.setValue('101', importObj.value);
          }
        }
      CCArfid++
    });

  importTools.after(function() {
    var s8w;

    function getPropIfExists(obj, properties) {
      //recursively return either the object if some multilayer property exists, or an empty string
      //i.e. if foo[1][2][3], return it; if not, return ''
      properties = wpw.tax.utilities.ensureArray(properties, false);
      return properties.length < 1 ? (obj || '') : getPropIfExists((obj || '')[properties[0]], properties.splice(1));
    }

    //for s8w table 400 / 500
    //table 400
    Object.keys(s8w_13_14_Obj[13]).forEach(function(rfId) {
      s8w = importTools.form('T2S8W', rfId);
      Object.keys(s8w_13_14_Obj[13][rfId]).forEach(function(row) {
        s8w.setValue('400', getPropIfExists(s8w_13_14_Obj[13][rfId][row], ['description', 'value']), 0, row);
        s8w.setValue('400', getPropIfExists(s8w_13_14_Obj[13][rfId][row], ['yearBegin', 'value']), 1, row);
        s8w.setValue('400', getPropIfExists(s8w_13_14_Obj[13][rfId][row], ['amortFed', 'value']), 3, row);
        s8w.setValue('400', getPropIfExists(s8w_13_14_Obj['both'], [rfId, row, 'additionsFed', 'value']), 2, row);
        s8w.setValue('400', getPropIfExists(s8w_13_14_Obj['both'], [rfId, row, 'annualCcaFed', 'value']), 4, row);
        s8w.setValue('400', getPropIfExists(s8w_13_14_Obj['both'], [rfId, row, 'priorCcaFed', 'value']), 5, row);
      });
    });
    Object.keys(s8w_13_14_Obj[14]).forEach(function(rfId) {
      s8w = importTools.form('T2S8W', rfId);
      Object.keys(s8w_13_14_Obj[14][rfId]).forEach(function(row) {
        s8w.setValue('500', getPropIfExists(s8w_13_14_Obj[14][rfId][row], ['description', 'value']), 0, row);
        s8w.setValue('500', getPropIfExists(s8w_13_14_Obj[14][rfId][row], ['acqDate', 'value']), 1, row);
        s8w.setValue('500', getPropIfExists(s8w_13_14_Obj[14][rfId][row], ['termDate', 'value']), 2, row);
        s8w.setValue('500', getPropIfExists(s8w_13_14_Obj[14][rfId][row], ['amortFed', 'value']), 4, row);
        s8w.setValue('500', getPropIfExists(s8w_13_14_Obj['both'], [rfId, row, 'additionsFed', 'value']), 3, row);
        s8w.setValue('500', getPropIfExists(s8w_13_14_Obj['both'], [rfId, row, 'annualCcaFed', 'value']), 5, row);
        s8w.setValue('500', getPropIfExists(s8w_13_14_Obj['both'], [rfId, row, 'priorCcaFed', 'value']), 6, row);
      });
    });
  });
});
