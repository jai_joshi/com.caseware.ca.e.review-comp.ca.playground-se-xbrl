(function() {
  wpw.tax.create.diagnostics('t2s21', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0210001', common.prereq(
        common.check(['t2s1.407', 't2s5.408', 't2j.355', 't2j.632', 't2j.636', 't2j.640', 't2s510.550'], 'isNonZero'),
        common.requireFiled('T2S21')));

    diagUtils.diagnostic('0210005', common.prereq(common.and(
        common.requireFiled('T2S21'),
        common.check('t2j.632', 'isNonZero')),
        function(tools) {
          var table = tools.field('101');
          return tools.checkAll(table.getRows(), function(row) {
            return row[2].require('isNonZero')
          });
        }));

    diagUtils.diagnostic('0210010', common.prereq(common.and(
        common.requireFiled('T2S21'),
        common.check(['t2s1.407'], 'isNonZero')),
        function(tools) {
          var table = tools.field('101');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne([row[3]], 'isNonZero');
          });
        }));

    diagUtils.diagnostic('0210015', common.prereq(common.and(
        common.requireFiled('T2S21'),
        common.check('t2j.636', 'isNonZero')),
        function(tools) {
          var table = tools.field('201');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne([row[2], row[3]], 'isNonZero')
          });
        }));

    diagUtils.diagnostic('0210020', common.prereq(common.and(
        common.requireFiled('T2S21'),
        common.check('t2j.640', 'isNonZero')),
        function(tools) {
          return (tools.requireAll(tools.list(['500', '510']), 'isNonZero') ||
          tools.requireAll(tools.list(['520', '530']), 'isNonZero'))
        }));

    diagUtils.diagnostic('0210030', common.prereq(common.requireFiled('T2S21'),
        function(tools) {
          var table = tools.field('101');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[2].isNonZero()) {
              return tools.requireAll([row[0], row[1]], 'isNonZero');
            } else return true;
          });
        }));

    diagUtils.diagnostic('0210035', common.prereq(common.requireFiled('T2S21'),
        function(tools) {
          var table = tools.field('101');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[3].isNonZero()) {
              return tools.requireAll([row[0], row[1], row[2]], 'isNonZero');
            } else return true;
          });
        }));

    diagUtils.diagnostic('0210040', common.prereq(common.requireFiled('T2S21'),
        function(tools) {
          var table = tools.field('201');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[2], row[3]], 'isNonZero')) {
              return tools.requireAll([row[0], row[1]], 'isNonZero')
            } else return true;
          });
        }));

    diagUtils.diagnostic('0210045', common.prereq(common.requireFiled('T2S21'),
        function(tools) {
          var table = tools.mergeTables(tools.field('401'), tools.field('201'));
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[2], row[3], row[4]], 'isFilled')) {
              return tools.requireOne([row[0]], 'isFilled') &&
                  row[5].require(function() {
                    return this.get() == row[0].get();
                  });
            } else return true;
          });
        }));

    diagUtils.diagnostic('0210050', common.prereq(common.requireFiled('T2S21'),
        function(tools) {
          var table = tools.mergeTables(tools.field('300'), tools.field('301'));
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[2], row[3], row[4], row[8]], 'isNonZero')) {
              return tools.requireOne([row[0]], 'isFilled');
            } else return true;
          });
        }));
  });
})();

