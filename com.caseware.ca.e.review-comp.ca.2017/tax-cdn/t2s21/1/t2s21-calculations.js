(function() {

  function table202Calcs(calcUtils, rowIndex) {
    var field = calcUtils.field;
    var table201Row = field('201').getRow(rowIndex);
    var table202Row = field('202').getRow(rowIndex);

    if (!table201Row || !table202Row)
      return;

    table202Row[0].assign(field('600').get());
    table202Row[1].assign(field('620').get());
    table202Row[2].assign(table202Row[1].get() - field('180').get());

    table202Row[3].assign(table202Row[0].get() == 0 ? 0 :
        Math.max(0, table201Row[1].get()) * table202Row[1].get() / table202Row[0].get());

    table202Row[4].assign(Math.min(
        table201Row[4].get(),
        table202Row[2].get(),
        table202Row[3].get())
    );

    table300Calcs(calcUtils, rowIndex)
  }

  function table300Calcs(calcUtils, rowIndex) {
    var field = calcUtils.field;
    var table201Row = field('201').getRow(rowIndex);
    var table300Row = field('300').getRow(rowIndex);

    if (!table201Row || !table300Row) {
      return;
    }

    table300Row[0].assign(table201Row[0].get());
    var openingBalance = table300Row[1].get() - table300Row[2].get();
    table300Row[3].assign(Math.max(0, openingBalance));

    table301Calcs(calcUtils, rowIndex)
  }

  function table301Calcs(calcUtils, rowIndex) {
    var field = calcUtils.field;
    var table201Row = field('201').getRow(rowIndex);
    var table202Row = field('202').getRow(rowIndex);
    var table300Row = field('300').getRow(rowIndex);
    var table301Row = field('301').getRow(rowIndex);
    var table401Row = field('401').getRow(rowIndex);

    if (!table201Row || !table202Row || !table300Row || !table301Row || !table401Row)
      return;

    table301Row[0].assign(Math.max(0, table201Row[2].get())); //201 row[2] manual input field shouldn't allow negative
    table301Row[1].assign(table202Row[4].get());
    table301Row[2].assign(
        table401Row[2].get() +
        table401Row[3].get() +
        table401Row[4].get()
    );

    table301Row[3].assign(Math.max(0, (
        table300Row[3].get() +
        table300Row[4].get() +
        table301Row[0].get() -
        table301Row[1].get() -
        table301Row[2].get()))
    );

    table401Calcs(calcUtils, rowIndex)
  }

  function table401Calcs(calcUtils, rowIndex) {
    var field = calcUtils.field;
    var table201Row = field('201').getRow(rowIndex);
    var table301Row = field('301').getRow(rowIndex);
    var table401Row = field('401').getRow(rowIndex);

    if (!table201Row || !table301Row || !table401Row)
      return;

    table401Row[0].assign(table201Row[0].get());
    table401Row[1].assign(
        table301Row[0].get() -
        table301Row[1].get()
    );
  }

  wpw.tax.create.calcBlocks('t2s21', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      //part 1 calcs
      field('101').getRows().forEach(function(row, rIndex) {
        row[4].assign(Math.max(0,
            row[2].get() -
            row[3].get()
        ));

        var repeatedTableRow = field('102').getRow(rIndex);
        var amountG = repeatedTableRow[1].get();
        var amountH = Math.max(0, row[1].get()) * amountG / repeatedTableRow[0].get();

        if (amountH <= amountG) {
          repeatedTableRow[2].assign(amountH)
        }
        else {
          repeatedTableRow[2].assign(amountG)
        }
        repeatedTableRow[3].assign(Math.min(repeatedTableRow[2].get(), row[4].get()))
      });

      field('102').getRows().forEach(function(row) {
        row[0].assign(field('600').get());
        row[1].assign(field('610').get());
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //table201 calcs
      field('201').getRows().forEach(function(row, rIndex) {
        row[4].assign(Math.max(0, row[2].get() + row[3].get()));
        table202Calcs(calcUtils, rIndex)
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //table202 calcs
      field('202').getRows().forEach(function(row, rIndex) {
        table202Calcs(calcUtils, rIndex)
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //table300 calcs
      field('300').getRows().forEach(function(row, rIndex) {
        table300Calcs(calcUtils, rIndex)
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //table301 calcs
      field('301').getRows().forEach(function(row, rIndex) {
        table301Calcs(calcUtils, rIndex)
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //table401 calcs
      field('401').getRows().forEach(function(row, rIndex) {
        table401Calcs(calcUtils, rIndex)
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 5 calculations//
      //TODO: field('500').assign(field('t2s542.150').get()); T2S542 not available
      //TODO: field('520'); quebec forms not available
      field('507').assign(Math.max(0, field('500') * (20 / 3) / 100));
      calcUtils.multiply('508', ['510'], 2 / 3);
      calcUtils.min('509', ['507', '508']);
      calcUtils.multiply('511', ['520'], (20 / 3) / 100);
      calcUtils.multiply('512', ['530'], 2 / 3);
      calcUtils.min('513', ['511', '512']);
      calcUtils.getGlobalValue(false, 't2j', '371', function(taxableIncome) {
        field('515').assign(taxableIncome * (20 / 3) / 100)
      });
      calcUtils.min('580', ['515', '514']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 6 calculations//
      field('599').assign(Math.max(0, field('t2j.300').get()));
      field('599').source(field('t2j.300'));

      field('601').assign(field('t2j.332').get());
      field('602').assign(field('t2j.320').get());
      field('604').assign(field('601').get() + field('602').get() + field('603').get());
      calcUtils.equals('605', '604');
      field('598').assign(Math.max(0, field('599').get() - field('605').get()));
      calcUtils.getGlobalValue('606', 't2j', '355');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      if (field('099').get() == 2) {
        var disableFields = [608, 609, 616, 611, 612, 613];
        disableFields.forEach(function(fieldId) {
          field(fieldId).disabled(true);
        });
        calcUtils.removeValue([608, 609, 616, 611, 612, 613], true);
        calcUtils.sumBucketValues('607', ['598', '606']);
        calcUtils.equals('600', '607', true);
      }
      else {
        field('607').disabled(true);

        var num608 = field('t2j.360').get();
        if (num608 < 0)
          field('608').assign(0);
        else  field('608').assign(num608);
        calcUtils.equals('609', '599', true);
        calcUtils.equals('616', '606', true);
        calcUtils.sumBucketValues('611', ['609', '616']);
        calcUtils.equals('612', '611', true);
        calcUtils.min('613', ['608', '612'], true);
        calcUtils.equals('600', '613', true);
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 7 calculations//
      calcUtils.getGlobalValue('700', 't2j', '550');
      calcUtils.getGlobalValue('701', 't2j', '608');
      calcUtils.getGlobalValue('702', 't2j', '620');
      calcUtils.getGlobalValue('703', 't2j', '628');
      calcUtils.getGlobalValue('704', 't2j', '639');
      calcUtils.sumBucketValues('705', ['701', '702', '703', '704']);
      calcUtils.equals('706', '705');
      calcUtils.getGlobalValue('707', 't2j', '602');
      calcUtils.getGlobalValue('708', 't2j', '604');
      calcUtils.sumBucketValues('709', ['707', '708']);
      calcUtils.equals('710', '709');
      field('610').assign(field('700').get() - field('706').get() + field('710').get());
      if (field('610').get() < 0)
        field('610').assign(0);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 8 calculations//
      calcUtils.equals('800', '700');
      calcUtils.equals('801', '702');
      calcUtils.equals('802', '703');
      calcUtils.getGlobalValue('803', 't2j', '638');
      calcUtils.equals('804', '704');
      calcUtils.sumBucketValues('805', ['801', '802', '803', '804']);
      calcUtils.equals('806', '805');
      calcUtils.getGlobalValue('807', 't2j', '602');
      field('620').assign(Math.max(field('800').get() - field('806').get() + field('807').get(), 0));
    });
  });
})();
