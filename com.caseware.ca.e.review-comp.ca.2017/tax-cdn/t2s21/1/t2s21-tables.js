(function() {
  var countryAddressCodes = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.countryAddressCodes, 'value');
  var canadaProvinces = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.canadaProvinces, 'EN');
  var countryOptions = getDropdownOptions(countryAddressCodes);

  function getDropdownOptions(countryCodes) {
    var options = [{value: '', option: ''}];
    countryCodes.forEach(function(opt, index) {
      if (index == 0) {
        return;
      }
      options.push({value: opt.value, option: opt.value + ' : ' + opt.option});
    });
    return options;
  }

  function getProvincialLinkedTableID(srcForm, tableNum) {
    var linkArray = [];
    canadaProvinces.forEach(function(prov, index) {
      if (prov.value == 'AB') {
        linkArray.push({formId: 'T2A4', fieldId: '990'});
        return linkArray;
      }
      if (prov.value == 'QC') {
        return;
      }
      if (index != 0) {
        linkArray.push({formId: srcForm, fieldId: tableNum + prov.value});
      }
    });
    return linkArray;
  }

  function table202Calcs(field, rowIndex, tableCalcs) {
    var table201Row = field('201').getRow(rowIndex);
    var table202Row = field('202').getRow(rowIndex);

    if (!table201Row || !table202Row)
      return;

    if (rowIndex != field('202').size().rows - 1) {
      table202Row[0].assign(field('600').get());
      table202Row[1].assign(field('620').get());
      table202Row[2].assign(table202Row[1].get() - field('180').get());
    }

    table202Row[3].assign(
        table201Row[1].get() *
        table202Row[1].get() /
        table202Row[0].get()
    );
    table202Row[4].assign(Math.min(
        table201Row[4].get(),
        table202Row[2].get(),
        table202Row[3].get())
    );

    table300Calcs(field, rowIndex, tableCalcs)
  }

  function table300Calcs(field, rowIndex, tableCalcs) {
    var table201Row = field('201').getRow(rowIndex);
    var table300Row = field('300').getRow(rowIndex);

    if (!table201Row || !table300Row) {
      return;
    }

    table300Row[0].assign(table201Row[0].get());
    var openingBalance = table300Row[1].get() - table300Row[2].get();
    table300Row[3].assign(Math.max(0, openingBalance));

    table301Calcs(field, rowIndex, tableCalcs)
  }

  function table301Calcs(field, rowIndex, tableCalcs) {
    var table201Row = field('201').getRow(rowIndex);
    var table202Row = field('202').getRow(rowIndex);
    var table300Row = field('300').getRow(rowIndex);
    var table301Row = field('301').getRow(rowIndex);
    var table401Row = field('401').getRow(rowIndex);

    if (!table201Row || !table202Row || !table300Row || !table301Row || !table401Row)
      return;

    table301Row[0].assign(table201Row[2].get());
    table301Row[1].assign(table202Row[4].get());
    table301Row[2].assign(
        table401Row[2].get() +
        table401Row[3].get() +
        table401Row[4].get()
    );

    table301Row[3].assign(Math.max(0, (
        table300Row[3].get() +
        table300Row[4].get() +
        table301Row[0].get() -
        table301Row[1].get() -
        table301Row[2].get()))
    );

    table401Calcs(field, rowIndex, tableCalcs)
  }

  function table401Calcs(field, rowIndex, tableCalcs) {
    var table201Row = field('201').getRow(rowIndex);
    var table301Row = field('301').getRow(rowIndex);
    var table401Row = field('401').getRow(rowIndex);

    if (!table201Row || !table301Row || !table401Row)
      return;

    table401Row[0].assign(table201Row[0].get());
    table401Row[1].assign(
        table301Row[0].get() -
        table301Row[1].get()
    );
  }

  wpw.tax.global.tableCalculations.t2s21 = {
    "101": {
      showNumbering: true,
      repeats: ['102'],
      linkedExternalTable: getProvincialLinkedTableID('T2S21W', '950'),
      hasTotals: true,
      columns: [
        {
          header: 'A <br> Country of source of foreign non-business income', tn: '100', num: '100',
          width: '69px',
          type: 'dropdown', options: countryOptions
        },
        {
          header: 'B <br> Net foreign non-business income earned in the year *',
          tn: '110', total: true, totalNum: '110', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        },
        {
          header: 'C <br> Foreign non-business income tax paid for the year **',
          tn: '120',
          total: true,
          totalNum: '120', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        },
        {
          header: 'D <br> Foreign non-business income tax paid, deducted from income under subsection 20(12)',
          tn: '130', total: true, totalNum: '130', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        },
        {header: 'E <br> (C - D)', totalNum: '140', total: true, disabled: true}
      ]
    },
    "102": {
      keepButtonsSpace: true,
      showNumbering: true,
      hasTotals: true,
      fixedRows: true,
      columns: [
        {
          header: 'F <Br> Adjusted net income <br>(amount from line 600 in Part 6)',
          num: '140',
          disabled: true, cellClass: 'alignRight'
        },
        {
          header: 'G <br> Part I tax otherwise payable (amount from line 610 in Part 7)',
          num: '160',
          disabled: true, cellClass: 'alignRight'
        },
        {
          header: 'H <br> (B x G) &#xf7; F <br> (amount in column H cannot be more than amount in column G)',
          num: '170',
          disabled: true, cellClass: 'alignRight'
        },
        {
          header: 'I <br> Deductible credit: <br> lesser of amounts E or H',
          tn: '180',
          total: true,
          totalMessage: 'Total deductible federal foreign non-business income tax credit',
          totalNum: '180',
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          disabled: true
        }
      ]
    },
    "201": {
      showNumbering: true,
      repeats: ['202', '300', '301', '401'],
      hasTotals: true,
      columns: [
        {
          header: 'A <br> Country in which foreign business income was earned',
          tn: '200',
          num: '200',
          width: '69px',
          type: 'dropdown', options: countryOptions
        },
        {
          header: 'B <br> Net foreign business income earned in the year *',
          num: '210',
          tn: '210',
          total: true,
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        },
        {
          header: 'C <br> Foreign business income tax paid for the year **',
          num: '220',
          tn: '220',
          total: true,
          totalNum: '221',
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        },
        {
          header: 'D <br> Unused foreign income tax credits from previous tax years',
          tn: '230',
          num: '230',
          total: true,
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        },
        {header: 'E <br> Total of columns C and D', num: '240', total: true, disabled: true}
      ]
    },
    "202": {
      type: 'table', num: '202', showNumbering: true, hasTotals: true, fixedRows: true, executeAtEnd: true,
      columns: [
        {
          header: 'F <br> Adjusted net income <br> (amount from line 600 in Part 6)',
          num: '250',
          disabled: true, cellClass: 'alignRight'
        },
        {
          header: 'G <br> Part I tax otherwise payable (amount from line 620 in Part 8)',
          num: '260',
          disabled: true, cellClass: 'alignRight'
        },
        {
          header: 'H <br> Part I tax otherwise payable minus foreign non-business income tax credits claimed',
          num: '270', disabled: true, cellClass: 'alignRight'
        },
        {header: 'I <br> (B x G) &#xf7; F', num: '275', disabled: true, cellClass: 'alignRight'},
        {
          header: 'J <br> Deductible credit: <br> the least of amounts E, H, or I',
          tn: '280',
          total: true,
          totalNum: '280',
          totalMessage: 'Total deductible federal foreign business income tax credit',
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          disabled: true
        }
      ]
    },
    "300": {
      type: 'table', num: '300', showNumbering: true, repeats: ['301'], fixedRows: true, executeAtEnd: true,
      columns: [
        {
          header: 'K <br> Country in which foreign business income was earned',
          num: '345',
          tn: '345',
          width: '69px',
          type: 'dropdown', options: countryOptions
        },
        {header: 'L <br> Balance at end of the previous tax year', num: '346', cellClass: 'alignRight'},
        {
          header: 'M *** <br> Amount expired in the year',
          num: '348',
          tn: '348',
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}, cellClass: 'alignRight'
        },
        {
          header: 'N <Br> Opening balance <br> (L - M)',
          num: '350',
          tn: '350',
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          disabled: true, cellClass: 'alignRight'
        },
        {
          header: 'O <br> Credits transferred on an amalgamation or the wind-up of a subsidiary',
          num: '360',
          tn: '360',
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}, cellClass: 'alignRight'
        }
      ]
    },
    "301": {
      type: 'table', num: '301', showNumbering: true, fixedRows: true,
      columns: [
        {
          header: 'P <br> Foreign business income tax paid for the year <br> (from column C of Part 2)',
          num: '365',
          disabled: true, cellClass: 'alignRight'
        },
        {
          header: 'Q <Br> Foreign business income tax credit deductible in the year <Br> (cannot be more than' +
          ' the amount in column J of Part 2)',
          num: '370',
          disabled: true, cellClass: 'alignRight'
        },
        {
          header: 'R <br> Carryback to previous years <br> (total of amounts in columns V, W, and X of Part 4)',
          num: '375',
          disabled: true, cellClass: 'alignRight'
        },
        {
          header: 'S <br>Closing balance<br>(N + O + P – Q – R)',
          num: '380',
          tn: '380',
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          disabled: true, cellClass: 'alignRight'
        }
      ]
    },
    "401": {
      type: 'table', num: '401', showNumbering: true, fixedRows: true,
      columns: [
        {
          header: 'T <br> Country in which foreign business income was earned',
          num: '900',
          tn: '900',
          width: '69px',
          type: 'dropdown', options: countryOptions
        },
        {
          header: 'U <br> Unused foreign business income tax credit <br>(Part 3, (P - Q))',
          num: '402',
          disabled: true, cellClass: 'alignRight'
        },
        {
          header: 'V <br> Carryback to 1st previous tax year *', num: '403', tn: '901',
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}, cellClass: 'alignRight'
        },
        {
          header: 'W <br> Carryback to 2nd previous tax year *', num: '404', tn: '902',
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}, cellClass: 'alignRight'
        },
        {
          header: 'X <br> Carryback to 3rd previous tax year *', num: '405', tn: '903',
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}, cellClass: 'alignRight'
        }
      ]
    },
    "501": {
      type: 'table', num: '501', fixedRows: true, hasTotals: true,
      columns: [
        {header: 'A <br> Province', type: 'none', disabled: true},
        {header: 'B <br> Income from logging under Regulation 700', num: '502', cellClass: 'alignRight'},
        {header: 'C <br> Amount B x 6 2/3&#x25;', num: '503', disabled: true, cellClass: 'alignRight'},
        {header: 'D <br> Logging tax paid on income in column B', num: '504', cellClass: 'alignRight'},
        {header: 'E <br> Amount D x 2/3', num: '505', disabled: true, cellClass: 'alignRight'},
        {
          header: 'F <br> Lesser of amounts <br> C or E', num: '506', total: true, totalNum: '514',
          input2Style: 'doubleUnderline', totalIndicator: 'G', disabled: true
        }
      ],
      cells: [
        {
          0: {label: 'British Columbia'},
          1: {num: '500', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}, tn: '500'},
          2: {num: '507'},
          3: {num: '510', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}, tn: '510'},
          4: {num: '508'},
          5: {num: '509'}
        },
        {
          0: {label: 'Quebec'},
          1: {num: '520', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}, tn: '520'},
          2: {num: '511'},
          3: {num: '530', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}, tn: '530'},
          4: {num: '512'},
          5: {num: '513'}
        }
      ]
    }
  }
})();
