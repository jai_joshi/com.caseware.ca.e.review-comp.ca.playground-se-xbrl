(function() {

  wpw.tax.global.formData.t2s21 = {
      formInfo: {
        abbreviation: 't2s21',
        title: 'Federal Foreign Income Tax Credits and Federal Logging Tax Credit',
        schedule: 'Schedule 21',
        code: 'Code 1002',
        formFooterNum: 'T2 SCH 21 E (14)',
        headerImage: 'canada-federal',
        showCorpInfo: true,
        description: [
          {
            type: 'list',
            items: [
              'Corporations resident in Canada at any time in the year and authorized foreign banks can use this' +
              ' schedule to claim a federal foreign non-business income tax credit, a federal foreign business income ' +
              'tax credit, or a provincial or territorial foreign non-business income tax credit.',
              'Calculate the foreign income tax credits for each country separately. Attach another schedule if the' +
              ' corporation is claiming credits for more than five countries.',
              'Calculate the provincial/territorial foreign non-business income tax credits for each country and' +
              ' province or territory separately.',
              'Unless otherwise noted, all legislative references are to the <i>Income Tax Act</i> and the' +
              ' <i> Income Tax Regulations</i>.'
            ]
          }
        ],
        category: 'Federal Tax Forms'
      },
      sections: [
        {
         hideFieldset: true,
          rows: [
            {
              type: 'infoField',
              num: '099',
              label: 'Is the corporation an authorized foreign bank?',
              inputType: 'radio',
              width: '30%',
              init: '2'
            }
          ]
        },
        {
        header: 'Part 1 - Federal foreign non-business income tax credit',
          rows: [
            {
              type: 'table', num: '101'
            },
            {labelClass: 'fullLength'},
            {
              type: 'table', num: '102'
            },
            {labelClass: 'fullLength'},
            {
              label: 'Enter the total deductible federal foreign non-business income tax credit, or a lesser amount,' +
              ' on line 632 of the T2 return.', labelClass: 'fullLength tabbed'
            },
            {labelClass: 'fullLength'},
            {
              label: '* Exclude income that is exempt from tax in Canada under an income tax treaty, dividends' +
              ' received from foreign affiliates, and tax-exempt income as defined in subsection 126(7). Net foreign' +
              ' non-business income is the excess of qualifying income over qualifying losses, which are determined' +
              ' according to subsection 126(9).', labelClass: 'fullLength tabbed'
            },
            {labelClass: 'fullLength'},
            {
              label: '** Exclude taxes paid to a foreign government on income that is exempt from tax in Canada under' +
              ' an income tax treaty; foreign taxes paid on dividends received from foreign affiliates; and any' +
              ' foreign taxes that may reasonably be regarded as relating to an amount that any other person or' +
              ' partnership has received, or is entitled to receive, from that government.',
              labelClass: 'fullLength tabbed'
            },
            {
              label: 'Exclude taxes paid for property (other than capital property) from which the corporation is not' +
              ' expected to realize a profit.', labelClass: 'fullLength tabbed'
            },
            {
              label: 'Exclude taxes paid that are in excess of the limit and paid for dividends and interest on a' +
              ' share or debt obligation held for one year or less.', labelClass: 'fullLength tabbed'
            },
            {
              label: 'Exclude taxes attributable to amounts received or receivable for eligible loans.',
              labelClass: 'fullLength tabbed'
            }
          ]
        },
        {
        header: 'Part 2 - Federal foreign business income tax credit',
          rows: [
            {
              type: 'table',
              num: '201'
            },
            {labelClass: 'fullLength'},
            {
              type: 'table', num: '202'
            },
            {labelClass: 'fullLength'},
            {
              label: 'Enter the total deductible federal foreign business income tax credit, or a lesser amount,' +
              ' on line 636 of the T2 return.',
              labelClass: 'fullLength tabbed'
            },
            {labelClass: 'fullLength'},
            {
              label: '* Exclude income that is exempt from tax in Canada under an income tax treaty and tax-exempt' +
              ' income as defined in subsection 126(7). Net foreign business income is the excess of qualifying' +
              ' income over qualifying losses, which are determined according to subsection 126(9).',
              labelClass: 'fullLength tabbed'
            },
            {labelClass: 'fullLength'},
            {
              label: '** Exclude taxes paid to a foreign government on income that is exempt from tax in Canada ' +
              'under an income tax treaty. Also exclude any foreign taxes that may reasonably be regarded as relating' +
              ' to an amount that any other person or partnership has received or is entitled to receive from ' +
              'that government', labelClass: 'fullLength tabbed'
            },
            {
              label: 'Exclude taxes paid for property (other than capital property) from which the corporation is not' +
              ' expected to realize a profit.', labelClass: 'fullLength tabbed'
            },
            {
              label: 'Exclude taxes paid that are in excess of the limit and paid for dividends and interest on a' +
              ' share or debt obligation held for one year or less.', labelClass: 'fullLength tabbed'
            }
          ]
        },
        {
        header: 'Part 3 - Continuity of unused federal foreign business income tax credits',
          rows: [
            {
              type: 'table', num: '300'
            },
            {labelClass: 'fullLength'},
            {label: '*** An unused federal foreign business income tax credit expires as follows:'},
            {
              label: '• after 7 tax years if it was earned in a tax year ending before March 23, 2004; or',
              labelClass: 'tabbed2'
            },
            {
              label: '• after 10 tax years if it was earned in a tax year ending after March 22, 2004.',
              labelClass: 'tabbed2'
            },
            {labelClass: 'fullLength'},
            {
              type: 'table', num: '301'
            }
          ]
        },
        {
        header: 'Part 4 - Request for a federal foreign business income tax credit carryback',
          rows: [
            {
              type: 'table', num: '401'
            },
            {labelClass: 'fullLength'},
            {
              label: '* Total of carryback (amounts from columns V, W, and X) cannot be more than the unused foreign' +
              ' business income tax credit (amount from column U)', labelClass: 'fullLength tabbed'
            }
          ]
        },
        {
          'header': 'Part 5 - Federal logging tax credit',
          'rows': [
            {
              'type': 'table',
              'num': '501'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '6 2/3% of taxable income (or, for non-residents, 6 2/3% of taxable income earned in Canada)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '515'
                  }
                }
              ],
              'indicator': 'H'
            },
            {
              'label': '<b> Federal logging tax credit </b> - Lesser of amounts G and H',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '580',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '580'
                  }
                }
              ],
              'indicator': 'I'
            },
            {
              'label': 'Enter amount I or a lesser amount on line 640 of the T2 return.',
              'labelClass': 'tabbed fullLength'
            }
          ]
        },
        {
          'header': 'Part 6 - Adjusted net income',
          'rows': [
            {
              'label': 'Line references are from page 3 of the T2 return.'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'To be completed by all corporations other than an authorized foreign bank',
              'labelClass': 'fullLength bold'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Net income for income tax purposes (line 300) (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '599'
                  }
                },
                null
              ]
            },
            {
              'label': 'Deduct:',
              'labelClass': 'bold'
            },
            {
              'label': 'Net capital losses claimed under paragraph 111(1)(b) (line 332)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '601'
                  }
                },
                null
              ]
            },
            {
              'label': 'Taxable dividends deductible under sections 112 and 113',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '602'
                  }
                },
                null
              ]
            },
            {
              'label': 'Amount deductible under paragraph 110(1)(d.2) for prospector\'s and grubstaker\'s shares (line 350)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '603'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '604'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '605'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ]
            },
            {
              'label': 'Subtotal (if negative, enter "0")',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '598'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Add:</b> Amount added to taxable income for foreign tax deductions under section 110.5 (line 355)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '606'
                  }
                }
              ]
            },
            {
              'label': 'Total',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '607'
                  }
                }
              ],
              'indicator': 'A'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'To be completed by an authorized foreign bank only',
              'labelClass': 'bold'
            },
            {
              'label': 'Taxable income earned in Canada (line 360) (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '608'
                  }
                }
              ],
              'indicator': 'B'
            },
            {
              'label': 'Income from its Canadian banking business (line 300) (if negative, enter "0")',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '609'
                  }
                },
                null
              ]
            },
            {
              'label': '<b> Add: </b> Amount added to taxable income for foreign tax deductions under subparagraph 115(1)(a)(vii) (line 355)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '616'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '611'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '612'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'C'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Amount B or C, whichever is less',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '613'
                  }
                }
              ],
              'indicator': 'D'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Adjusted net income </b> (amount A or D, whichever is applicable)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '600',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '600'
                  }
                }
              ]
            }
          ]
        },
        {
          'header': 'Part 7 - Part I tax otherwise payable (foreign non-business income tax credit)',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Line references are from page 7 of the T2 return.'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Base amount of Part 1 tax (line 550)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '700'
                  }
                }
              ],
              'indicator': 'A'
            },
            {
              'label': 'Deduct:',
              'labelClass': 'bold tabbed'
            },
            {
              'label': 'Federal tax abatement (line 608)',
              'labelClass': '2',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '701'
                  }
                },
                null
              ]
            },
            {
              'label': 'Investment corporation deduction (line 620)',
              'labelClass': '2',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '702'
                  }
                },
                null
              ]
            },
            {
              'label': 'Additional deduction for credit unions (line 628)',
              'labelClass': '2',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '703'
                  }
                },
                null
              ]
            },
            {
              'label': 'General tax reduction (line 639)',
              'labelClass': '2',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '704'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '705'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '706'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'B'
            },
            {
              'label': 'Add:',
              'labelClass': 'bold tabbed'
            },
            {
              'label': 'Recapture of investment tax credit (line 602)',
              'labelClass': '2',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '707'
                  }
                },
                null
              ]
            },
            {
              'label': 'Refundable tax on Canadian-controlled private corporation\'s (CCPC)investment income (line 604)',
              'labelClass': '2',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '708'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '709'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '710'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'C'
            },
            {
              'label': '<b>Part I tax otherwise payable (foreign business income tax credit)</b> ' +
              '<br>(amount A <b>minus</b> amount B <b>plus</b> amount C)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '610',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '610'
                  }
                }
              ]
            }
          ]
        },
        {
          'header': 'Part 8 - Part I tax otherwise payable (foreign business income tax credit)',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Line references are from page 7 of the T2 return.'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Base amount of Part 1 tax (line 550)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '800'
                  }
                }
              ],
              'indicator': 'A'
            },
            {
              'label': 'Deduct:',
              'labelClass': 'bold tabbed'
            },
            {
              'label': 'Investment corporation deduction (line 620)',
              'labelClass': '2',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '801'
                  }
                },
                null
              ]
            },
            {
              'label': 'Additional deduction for credit union (line 628)',
              'labelClass': '2',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '802'
                  }
                },
                null
              ]
            },
            {
              'label': 'General tax reduction for CCPCs (line 638)',
              'labelClass': '2',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '803'
                  }
                },
                null
              ]
            },
            {
              'label': 'General tax reduction (line 639)',
              'labelClass': '2',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '804'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '805'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '806'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'B'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Add </b>: Recapture of investment tax credit (line 602)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '807'
                  }
                }
              ],
              'indicator': 'C'
            },
            {
              'label': 'Part I tax of otherwise payable (foreign business income tax credit)',
              'labelClass': 'bold tabbed fullLength'
            },
            {
              'label': '(amount A <b> minus </b> amount B <b> plus </b> amount C)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '620',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '620'
                  }
                }
              ]
            }
          ]
        }
        //{
        //header: 'Part 9 - Provincial or territorial foreign tax credit',
        //  rows: [
        //    {labelClass: 'fullLength'},
        //    // TODO: need to put into 'list' format - bullet points instead of dashes//
        //    {
        //      label: '- Use Part 9 to calculate your provincial or territorial foreign tax credit.',
        //      labelClass: 'fullLength'
        //    },
        //    {
        //      label: '- Complete a separate calculation for each province or territory for which you are claiming' +
        //      ' the credit. If you have foreign non-business income from more than one country, complete a separate' +
        //      ' calculation for each country and total these calculations to determine the credit for the applicable' +
        //      ' province or territory.',
        //      labelClass: 'fullLength'
        //    },
        //    {labelClass: 'fullLength'},
        //    {
        //      type: 'table', num: '900'
        //    },
        //    {labelClass: 'fullLength'},
        //    {
        //      label: 'Foreign non-business income tax available for provincial or territorial foreign tax credit:',
        //      labelClass: 'bold fullLength'
        //    },
        //    {
        //      label: 'Foreign non-business income tax paid for the year (amount C in Part 1)',
        //      input2: true,
        //      indicator: 'A',
        //      num: '904'
        //    },
        //    {label: 'Deduct:', labelClass: 'bold'},
        //    {
        //      label: 'Foreign non-business income tax paid, deducted from income under subsection 20(12) (amount' +
        //      ' from column D in Part 1)',
        //      labelClass: 'tabbed',
        //      input2: true,
        //      num: '905',
        //      indicator: 'B',
        //      input2Style: 'underline',
        //      labelWidth: '65%'
        //    },
        //    {
        //      label: 'Subtotal (amount A <b> minus </b> amount B)',
        //      input2: true,
        //      num: '906',
        //      indicator: 'C',
        //      input2Style: 'doubleUnderline',
        //      labelClass: 'text-right'
        //    },
        //    {
        //      label: 'Federal foreign non-business income tax credit deductible (amount from column I in Part 1)',
        //      labelClass: 'tabbed',
        //      num: '907', input2: true, input2Style: 'underline', indicator: 'D'
        //    },
        //    {
        //      label: 'Foreign non-business income tax credit available (amount C minus amount D)',
        //      input2: true,
        //      num: '908',
        //      input2Style: 'doubleUnderline',
        //      indicator: 'E'
        //    },
        //    {labelClass: 'fullLength'},
        //    {
        //      label: '<b> Note</b>: If amount E is zero, no provincial or territorial foreign tax credit may be' +
        //      ' claimed in respect of the foreign country',
        //      labelClass: 'fullLength'
        //    },
        //    {labelClass: 'fullLength'},
        //    {
        //      type: 'table', num: '909'
        //    },
        //    {labelClass: 'fulLLength'},
        //    {label: 'Limit on amount of foreign non-business income taxes that can be claimed:', labelClass: 'bold'},
        //    {labelClass: 'fullLength'},
        //    {
        //      label: 'Net foreign non-business income earned in the year (amount from column B in Part 1)',
        //      input1: true,
        //      indicator: 'G',
        //      num: '914'
        //    },
        //    {labelClass: 'fullLength'},
        //    {
        //      type: 'table', num: '915'
        //    },
        //    {labelClass: 'fullLength'},
        //    {
        //      type: 'table', num: '918'
        //    },
        //    {labelClass: 'fullLength'},
        //    {
        //      label: '<b> Provincial or territorial foreign tax credit </b> (lesser of amount F or amount I)',
        //      input2: true,
        //      num: '923',
        //      input2Style: 'doubleUnderline',
        //      indicator: 'J'
        //    },
        //    {
        //      label: 'Enter amount J on the corresponding line in Part 2 of Schedule 5, <i> Tax Calculation' +
        //      ' Supplementary - Corporations</i>. If you have more than one calculation of the credit for a' +
        //      ' province or territory, enter the total credits calculated on the corresponding line in Part 2 ' +
        //      'of Schedule 5.',
        //      labelClass: 'fullLength'
        //    },
        //    {labelClass: 'fullLength'},
        //    {
        //      label: 'For Ontario, if the corporation is not a life insurance corporation, also enter amount J on' +
        //      ' line 500 of Schedule 510, <i> Ontario Corporate Minimum Tax </i>.'
        //    },
        //    {labelClass: 'fullLength'},
        //    {
        //      label: '* Enter the amount allocated to the province or territory in column F from Part 1 of Schedule' +
        //      ' 5. For Nova Scotia and Newfoundland and Labrador, include their respective offshore areas. For ' +
        //      'Ontario, if the corporation\'s taxable income is nil, calculated the amount in column F as if the' +
        //      ' taxable income were $1000.', labelClass: 'fullLength tabbed'
        //    },
        //    {
        //      label: '** Exclude taxable income earned outside Canada. For Ontario, if the corporation\'s taxable' +
        //      ' income is nil, enter \'1,000\'.', labelClass: 'tabbed fullLength'
        //    },
        //    {
        //      label: '*** For all provinces and territories except Ontario, use the higher rate. If the rate has' +
        //      ' changed during the year, use the average rate on the number of days in the tax year before and after' +
        //      ' the change. For Ontario, use the basic rate of tax calculated in Part 1 on Schedule 500, Ontario' +
        //      ' Corporation Tax Calculations.',
        //      labelClass: 'fullLength tabbed'
        //    },
        //    {
        //      label: '**** Enter the amount from line 360 or line Z, whichever applies, from page 3 of the T2' +
        //      ' Corporation Income Tax Return. For Ontario, if the corporation\'s taxable income is nil,' +
        //      ' enter \'1,000\'.', labelClass: 'fullLength tabbed'
        //    }
        //  ]
        //}
      ]
    };
})();
