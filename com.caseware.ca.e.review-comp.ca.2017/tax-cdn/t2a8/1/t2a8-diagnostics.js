(function() {
  wpw.tax.create.diagnostics('t2a8', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    diagUtils.diagnostic('t2a8.100',
      {category: "TAX RULE",
        label: "The corporation has indicated that political contribution has been made by the corporation during the year in AT5, but line 002, 004, or 006 is blank.",
        severity: "warning"},
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[3].isNonZero())
              return tools.requireOne([row[0], row[1], row[2]], 'isNonZero');
            else return true;
          });
        });
  });
})();
