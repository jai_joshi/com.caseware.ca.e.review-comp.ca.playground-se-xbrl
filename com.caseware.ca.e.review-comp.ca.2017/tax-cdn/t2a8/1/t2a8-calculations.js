(function() {


  wpw.tax.create.calcBlocks('t2a8', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field, form) {
      var Total008=0;
      var FiscalBeg = field('CP.tax_start').get();
      var FiscalEnd = field('CP.tax_end').get();
      var Tot2014=0;
      var contributionyear=true; //check if contribution year is between 2003&2004
      field('100').getRows().forEach(function(row, rowIndex) {
        Total008+=field('100').cell(rowIndex,3).get();
        if(field('100').cell(rowIndex,2).get()!= undefined){//make sure it doesn't count blanks
          if(contributionyear){
            contributionyear=(field('100').cell(rowIndex,2).get().year==2004||field('100').cell(rowIndex,2).get().year==2003);
            if (field('100').cell(rowIndex,2).get().year==2004){
              Tot2014 +=field('100').cell(rowIndex,3).get();
            }
          }
        }
      })
      field('010').assign(Total008);
      field('015').assign(field('010').get()+field('012').get()+field('013').get());

      field('026').assign(Math.max(field('t2a.068').get()-field('t2a.070').get()-field('t2a.071').get()-field('t2a.072').get()));

      //===Section C
      if(contributionyear && FiscalBeg.year>2002 && FiscalEnd.year<2005){
        field('051').assign(Tot2014+field('013').get());
        field('052').assign(field('010').get()+field('012').get()+field('013').get());
        field('056').assign(Math.min(field('052').get(),150));
        field('058').assign(Math.max(field('051').get()-field('056').get(),0));
        field('060').assign(Math.min(field('058').get(),50));
        field('062').assign(Math.max(field('052').get()-field('056').get()-field('060').get(),0));
        field('064').assign(Math.min(field('062').get(),675));
        field('066').assign(Math.max(field('051').get()-field('056').get()-field('060').get()-field('064').get(),0));
        field('068').assign(Math.min(field('066').get(),225));
        field('070').assign(Math.max(field('052').get()-field('056').get()-field('060').get()-field('064').get()-field('068').get(),0));
        field('072').assign(Math.min(field('070').get(),900));
        field('074').assign(Math.max(field('051').get()-field('056').get()-field('060').get()-field('064').get()-field('068').get()-field('072').get(),0));
        field('076').assign(Math.min(field('074').get(),300));
        field('078').assign((field('056').get()+field('060').get())*.75+(field('064').get()+field('068').get())*.5+(field('072').get()+field('076').get())*.333);
      }else{
        //====Section A
        if(FiscalBeg.year>=2004){
          field('040').assign(field('010').get()+field('013').get());
          field('041').assign(Math.min(field('040').get(),200)*.75);
          field('043').assign(Math.min(Math.max(field('040').get()-200,0),900)*.5);
          field('045').assign(Math.max(field('040').get()-1100,0)*1/3);
          field('047').assign(field('041').get()+field('043').get()+field('045').get());
          field('049').assign(Math.min(field('047').get(),1000));
        }
        if(FiscalEnd.year<2004){
          field('014').assign(field('010').get()+field('012').get());
          field('016').assign(Math.min(field('014').get(),150)*.75);
          field('018').assign(Math.min(Math.max(field('014').get()-150,0),675)*.5);
          field('020').assign(Math.max(field('014').get()-825,0)*1/3);
          field('022').assign(field('016').get()+field('018').get()+field('020').get());
          field('024').assign(Math.min(field('022').get(),750));
        }
      }
      field('025').assign(field('049').get()+field('024').get()+field('078').get());
      field('030').assign(Math.min(field('025').get(),field('026').get()));
    })
  });
})();

