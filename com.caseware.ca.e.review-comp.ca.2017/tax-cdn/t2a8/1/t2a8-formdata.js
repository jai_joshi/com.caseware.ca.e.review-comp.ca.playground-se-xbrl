(function () {
  'use strict';
  wpw.tax.create.formData('t2a8', {
    formInfo: {
      abbreviation: 'AT5',
      title: 'ALBERTA POLITICAL CONTRIBUTIONS TAX CREDIT - AT1 SCHEDULE 8',
      schedule: 'Schedule 8',
      showCorpInfo: true,
      //headerImage: 'canada-alberta',
      formFooterNum: 'AT5 (Jul-12)',
      category: 'Alberta Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
            {
              label: "For corporations to apply for the Alberta Political Contributions Tax Credit under the Alberta Corporate Tax Act when contributions have been " +
              "made under the Election Finances and Contributions Disclosure Act to a political party, constituency association or candidate registered in " +
              "Alberta. Original receipts must be retained and, if requested, made available to Tax and Revenue Administration. If the corporation has " +
              "made qualifying political contributions to a registered party that has nominated a candidate or a registered candidate under the <b>Senatorial " +
              "Selection Act</b> in respect of an election under the Senatorial Selection Act, then see Guide for instructions on how to claim this credit.",
              labelClass: 'fullLength'
            },
            {
              label: "Report all monetary amounts in dollars; DO NOT include cents.",
              labelClass: 'fullLength'
            }
        ]
      },
      {
        'rows': [
          {
            'label': 'Political Contributions made under the Election Finances and Contributions Disclosure Act to a ' +
            'Registered Party, Registered Constituency Association or Registered Candidate',
            'labelClass': 'fullLength bold font-medium'
          },
          {
            'type': 'table',
            'num': '100'
          },
          {
            'label': 'Total qualifying political contributions made by the corporation (sum of amounts in column 008)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '010',
                },
                'padding': {
                  'type': 'tn',
                  'data': '010'
                }
              },
            ]
          },
          {
            'label': 'Alberta qualifying political contributions made in 2003 or earlier from a partnership (from federal T5013 box 37)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '012',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '012'
                }
              },
            ]
          },
          {
            'label': 'Alberta qualifying political contributions made in 2004 or later from a partnership (from federal T5013 box 37)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '013',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '013'
                }
              },
            ]
          },
          {
            'label': 'Total amount of contributions in taxation year: Line 010 + line 012 + line 013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '015',
                },
                'padding': {
                  'type': 'tn',
                  'data': '015'
                }
              },
            ]
          },
          {
            label:'To determine your maximum allowable credit, first complete either Section A, B or C (as applicable)<br>then complete lines 025, 026 and 030 below.',
            labelClass: 'fullLength bold'
          },
          {
            label:'Maximum Allowable Credit: ',
            labelClass: 'fullLength'
          },
          {
            'label': 'Section A Line 049 OR Section B Line 024 OR Section C Line 078',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '025',
                },
                'padding': {
                  'type': 'tn',
                  'data': '025'
                }
              }
            ]
          },
          {
            'label': 'From AT1 page 2, line 068 - (lines 070 + 071 + 072)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '026',
                },
                'padding': {
                  'type': 'tn',
                  'data': '026'
                }
              }
            ]
          },
          {
            'label': 'Alberta Political Contributions Tax Credit: lesser of amounts on lines 025 and 026 <br> <b>Enter this amount on AT1 page 2, line 074</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '030',
                },
                'padding': {
                  'type': 'tn',
                  'data': '030'
                }
              }
            ]
          }
        ]
      },
      //===SECTION A
      {
        'header': 'SECTION A - MAXIMUM ALLOWABLE CREDIT ',
        'rows': [
          {
            'label': 'Complete this section when <u>ALL political contributions</u> for the taxation year were made in <u>2004 or later</u>',
            'labelClass': 'fullLength bold indent'
          },
          {
            'label': 'Total qualifying Alberta political contributions <b>made on or after January 1, 2004</b> ' +
            '<br>(Line 010 + line 013 where date of donation is January 1, 2004 or later) ',
            'labelClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '040',
                  'disabled': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '040'
                }
              }
            ]
          },
          {
            'label': '75% of first $200 of amount on line 040',
            'labelClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '041',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '041'
                }
              },
              null
            ]
          },
          {
            'label': '50% of next $900 of amount on line 040',
            'layout': 'alignInput',
            'labelClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '043',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '043'
                },
              },
              null
            ]
          },
          {
            'label': '33 1/3% of amount on line 040 exceeding $1100',
            'layout': 'alignInput',
            'labelClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '045',
                  'disabled': true,
                },
                'padding': {
                  'type': 'tn',
                  'data': '045'
                }
              },
              null
            ]
          },
          {
            'label': 'Total',
            'layout': 'alignInput',
            'labelClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '047',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '047'
                },
              },
              null
            ]
          },
          {
            'label': '<b>Maximum Allowable Credit:</b> lesser of amount on line 047 and $1000',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '049',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '049'
                },
              }
            ]
          }
        ]
      },
      //=====SECTION B
      {
        'header': 'SECTION B - MAXIMUM ALLOWABLE CREDIT',
        'rows': [
          {
            'label': 'Complete this section when <u>ALL political contributions</u> for the taxation year were made in <u>2003 or earlier</u>',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Total qualifying Alberta political contributions <b>made prior to January 1, 2004 </b> ' +
            '<br>(Line 010 + line 012 where date of donation is before January 1, 2004)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '014',
                  'disabled': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '014'
                }
              }
            ]
          },
          {
            'label': '75% of first $150 of amount on line 014',
            'labelClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '016',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '016'
                }
              },
              null
            ]
          },
          {
            'label': '50% of next $675 of amount on line 014',
            'layout': 'alignInput',
            'labelClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '018',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '018'
                },
              },
              null
            ]
          },
          {
            'label': '33 1/3% of amount on line 014 exceeding $825',
            'layout': 'alignInput',
            'labelClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '020',
                  'disabled': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '020'
                }
              },
              null
            ]
          },
          {
            'label': 'Total',
            'layout': 'alignInput',
            'labelClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '022',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '022'
                },
              },
              null
            ]
          },
          {
            'label': '<b>Maximum Allowable Credit:</b> lesser of amount on line 022 and $750',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '024',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '024'
                },
              }
            ]
          }
        ]
      },
      //===Section C
      {
        'header': 'SECTION C - MAXIMUM ALLOWABLE CREDIT <br> FOR THE 2003 TO 2004 TRANSITIONAL PERIOD',
        'rows': [
          {
            'label': 'Complete this section only if the corporation\'s <u>taxation year begins in 2003 and ends in 2004 AND</u> ' +
            'if qualifying <u>contributions were made in BOTH 2003 and 2004.</u>',
            'labelClass': 'fullLength bold indent'
          },
          {
            'label': 'Amount of qualifying Alberta political contributions <b>made in 2004</b> ' +
            '(include only donations made in 2004 that were within the taxation year) ' +
            '<br>Page 1 applicable amount(s) from column 008 + line 013',
            'labelClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '051',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '051'
                }
              }
            ]
          },
          {
            'label': 'Total qualifying Alberta political contributions <b>made in the taxation year</b>' +
            '(include donations from both 2003 and 2004 that were within the taxation year) ' +
            '<br>Page 1 lines 010 + 012 + 013 ',
            'labelClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '052',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '052'
                }
              }
            ]
          },
          {
            'label': 'Lesser of amount at line 052 and $150.00',
            'labelClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '056',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '056'
                }
              }
            ]
          },
          {
            'label': 'Line 051 minus line 056 (<i>if negative, enter "0"</i>)',
            'labelClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '058',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '058'
                }
              },
              null
            ]
          },
          {
            'label': 'Lesser of amount at line 058 and $50.00',
            'labelClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '060',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '060'
                }
              }
            ]
          },
          {
            'label': 'Line 052 minus (lines 056 + 060) (<i>if negative, enter "0"</i>)',
            'layout': 'alignInput',
            'labelClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '062',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '062'
                },
              },
              null
            ]
          },
          {
            'label': 'Lesser of amount at line 062 and $675.00',
            'labelClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '064',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '064'
                }
              }
            ]
          },
          {
            'label': 'Line 051 minus (lines 056 + 060 + 064) (<i>if negative, enter "0"</i>)',
            'layout': 'alignInput',
            'labelClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '066',
                  'disabled': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '066'
                }
              },
              null
            ]
          },
          {
            'label': 'Lesser of amount at line 066 and $225.00',
            'labelClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '068',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '068'
                }
              }
            ]
          },
          {
            'label': 'Line 052 minus (lines 056 + 060 + 064 + 068) (<i>if negative, enter "0"</i>)',
            'layout': 'alignInput',
            'labelClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '070',
                  'disabled': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '070'
                }
              },
              null
            ]
          },
          {
            'label': 'Lesser of amount at line 070 and $900.00',
            'labelClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '072',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '072'
                }
              }
            ]
          },
          {
            'label': 'Line 051 minus (lines 056 + 060 + 064 + 068 + 072) (<i>if negative, enter "0"</i>)',
            'layout': 'alignInput',
            'labelClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '074',
                  'disabled': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '074'
                }
              },
              null
            ]
          },
          {
            'label': 'Lesser of amount at line 074 and $300.00',
            'labelClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '076',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '076'
                }
              }
            ]
          },

          {
            'label': '<b>Maximum Allowable Credit:</b><br>[(lines 056 + 060) X 75%] + [(lines 064 + 068) X 50%] + [(lines 072 + 076) X 33.3%]',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '078',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '078'
                },
              }
            ]
          }
        ]
      }
    ]
  });
})();
