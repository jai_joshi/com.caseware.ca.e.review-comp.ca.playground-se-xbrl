(function() {
  wpw.tax.create.tables('t2a8', {
    '100': {
      columns: [
        {
          header: 'Name of Party, Constituency Association or Candidate',
          type: 'custom',
          formField: true,
          cellClass: 'alignLeft',
          num:'002',
          tn:'002',
        },
        {
          header: 'Official Receipt Number',
          colClass: 'std-input-width',
          type: 'text',
          cellClass: 'alignLeft',
          num:'004',
          tn:'004',
          validate: {'or':[{'length':{'min':'1', 'max':'30'}}, {'check':'isEmpty'}]},
        },
        {
          header: 'Date of Donation (from receipt)',
          colClass: 'std-input-col-width',
          formField: true,
          type:'date',
          cellClass: 'alignCenter',
          num:'006',
          tn:'006'
        },
        {
          header: 'Donation Amount',
          colClass: 'std-input-col-padding-width',
          formField: true,
          cellClass: 'alignCenter',
          num:'008',
          tn:'008'
        }
      ]
    }
  })
})();
