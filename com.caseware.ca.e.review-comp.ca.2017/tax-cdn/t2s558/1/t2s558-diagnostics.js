(function() {
  wpw.tax.create.diagnostics('t2s558', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S558'), common.bnCheck('647')));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S558'), common.bnCheck('747')));

    diagUtils.diagnostic('5580005', common.prereq(common.check('t2s5.460', 'isNonZero'),
        common.requireFiled('T2S558')));

    diagUtils.diagnostic('5580010', common.prereq(common.requireFiled('T2S558'),
        common.check(['t2s5.460', '720'], 'isEmpty', true)));

    diagUtils.diagnostic('5580015', common.prereq(common.and(
        common.requireFiled('T2S558'),
        common.check('720', 'isNonZero')),
        common.check(['200', '210', '220', '230', '240'], 'isNonZero', true)));

    diagUtils.diagnostic('5580020', common.prereq(common.and(
        common.requireFiled('T2S558'),
        common.check('720', 'isNonZero')),
        common.check(['300', '310', '320', '330'], 'isNonZero', true)));

    diagUtils.diagnostic('5580025', common.prereq(common.and(
        common.requireFiled('T2S558'),
        common.check('720', 'isNonZero')),
        common.check('400', 'isNonZero')));

    diagUtils.diagnostic('558.645', common.prereq(common.and(
        common.requireFiled('T2S558'),
        common.check('645', 'isNonZero')),
        common.check(['646', '647'], 'isNonZero', true)));

    diagUtils.diagnostic('558.745', common.prereq(common.and(
        common.requireFiled('T2S558'),
        common.check(['745'], 'isNonZero')),
        common.check(['746', '747'], 'isNonZero', true)));

  });
})();
