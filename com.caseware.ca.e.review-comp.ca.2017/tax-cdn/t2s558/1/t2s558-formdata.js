(function() {

  wpw.tax.create.formData('t2s558', {
    formInfo: {
      abbreviation: 'T2S558',
      isRepeatForm: true,
      repeatFormData: {
        titleNum: '210'
      },
      title: 'Ontario Production Services Tax Credit',
      schedule: 'Schedule 558',
      headerImage: 'canada-federal',
      code: 'Code 1502',
      formFooterNum: 'T2 SCH 558 E (17)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule to claim an Ontario production services tax credit (OPSTC) ' +
              'under section 92 of the <i>Taxation Act, 2007</i> (Ontario). Complete a separate Schedule ' +
              '558 for each eligible production.'
            },
            {
              label: 'The OPSTC is a refundable tax credit that is equal to 25% of the qualifying production ' +
              'expenditure (QPE) incurred after June 30, 2009, and before April 24, 2015, and 21.5% for QPE' +
              ' incurred after April 23, 2015, by a qualifying corporation in a tax year for eligible productions.' +
              ' Transitional rules may allow QPEs incurred after April 23, 2015, and before August 1, 2016, ' +
              'to be eligible for the 25% rate (see Part 4). For expenditures incurred after June 30, 2009, ' +
              'only expenditures incurred after the final script stage to the end of the post production' +
              ' stage are eligible for the credit.'
            },
            {
              label: 'To claim the OPSTC, you must meet the eligibility requirements in Part 3.'
            },
            {
              label: 'If you claim, or have claimed the Ontario film and television tax credit for that same' +
              ' production for any tax year, the OPSTC you are entitled to is nil for that ' +
              'same eligible Ontario production.'
            },
            {
              label: 'Before claiming an OPSTC, a qualifying corporation must obtain a Certificate ' +
              'of Eligibility from the Ontario Media Development Corporation (OMDC) for each eligible ' +
              'production. Enter the certificate information for the production in Part 2.'
            },
            {
              label: 'The OPSTC is considered government assistance under paragraph 12(1)(x) of the ' +
              'federal <i>Income Tax Act</i> and must either be included in income in the tax year ' +
              'the credit is received or deducted from the capital cost of the property. The OPSTC is ' +
              'not considered assistance under subsection 33(4) of Ontario Regulation 37/09 to the ' +
              '<i>Taxation Act, 2007</i> (Ontario) for the purposes of calculating the credit itself.'
            },
            {
              label: 'To claim the OPSTC, include a completed copy of this schedule and a copy of the ' +
              'certificate of eligibility issued by the OMDC with your ' +
              '<i>T2 Corporation Income Tax Return</i> for the tax year.'
            }
          ]
        }
      ],
      category: 'Ontario Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Contact information',
        'rows': [
          {
            'type': 'table',
            'num': '100'
          }
        ]
      },
      {
        'header': 'Part 2 - Identifying the eligible production',
        'rows': [
          {
            'type': 'table',
            'num': '250'
          },
          {
            'type': 'table',
            'num': '260'
          }
        ]
      },
      {
        'header': 'Part 3 - Eligibility',
        'rows': [
          {
            'type': 'infoField',
            'tn': '300',
            'num': '300',
            'label': '1. Did the primary activities of the corporation in the tax year consist of carrying on a film or video production business or a film or video production services business, through a permanent establishment in Ontario?',
            'labelClass': 'tabbed',
            'labelWidth': '80%',
            'inputType': 'radio',
            'init': '2'
          },
          {
            'type': 'infoField',
            'tn': '310',
            'num': '310',
            'label': '2. Was the corporation exempt from tax for the tax year under Part III of the <i>Taxation Act, 2007</i> (Ontario) or Part I of the federal <i>Income Tax Act</i>?',
            'labelClass': 'tabbed',
            'labelWidth': '80%',
            'inputType': 'radio',
            'init': '1'
          },
          {
            'type': 'infoField',
            'tn': '320',
            'num': '320',
            'label': '3. Was the corporation, at any time in the tax year, controlled directly or indirectly, in any way, by one or more persons, all or part of whose taxable income was exempt from tax under Part I of the federal <i>Income Tax Act</i>?',
            'labelClass': 'tabbed',
            'labelWidth': '80%',
            'inputType': 'radio',
            'init': '1'
          },
          {
            'type': 'infoField',
            'tn': '330',
            'num': '330',
            'label': '4. Was the corporation, at any time in the tax year, a prescribed labour-sponsored venture capital corporation?',
            'labelClass': 'tabbed',
            'labelWidth': '80%',
            'inputType': 'radio',
            'init': '1'
          },
          {
            'type': 'infoField',
            'tn': '400',
            'num': '400',
            'label': '5. Have you claimed, at any time, an Ontario film and television tax credit for the production identified in Part 2?',
            'labelClass': 'tabbed',
            'labelWidth': '80%',
            'inputType': 'radio',
            'init': '1'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If you answered <b>no</b> to question 1; or <b>yes</b> to questions 2, 3, 4, or 5, you are <b>not eligible</b> for the OPSTC.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        forceBreakAfter: true,
        'header': 'Part 4 - Ontario production expenditure incurred before April 24, 2015',
        'spacing': 'R_tn2',
        'rows': [
          {
            'label': '<b>Transitional rules:</b><br>You may also complete this part to claim Ontario' +
            ' production expenditures after April 23, 2015, and before August 1, 2016, at the 25% ' +
            'rate if the corporation meets all the following criteria:',
            'labelClass': 'fullLength'
          },
          {
            'label': '• Before April 24, 2015, you have entered into at least one written agreement in respect of a qualifying production expenditure for the eligible production with a person that deals at arm\'s length with the corporation and any of the following criteria.',
            'labelClass': 'fullLength'
          },
          {
            'label': '- The agreement is in respect of services of a producer, a director, a key cast member, a production crew or a post-production crew.',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '- The agreement is in respect of a studio located in Ontario, or a location in Ontario',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '- The agreement demonstrates, in the opinion of the Minister of Tourism, Culture and Sport, that the corporation has made a significant commitment to production activities in Ontario',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '• Before August 1, 2015, you have applied to the Ontario Media Development Corporation under subsection (6) for a certificate in respect of the production.'
          },
          {
            'label': '• Principal photography or key animation for the production began before August 1, 2015.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible wage expenditure paid to Ontario-based individuals, for services rendered in Ontario, that is directly attributable to the eligible production',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '630',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '630'
                }
              }
            ]
          },
          {
            'label': 'Eligible service contract expenditure, for services rendered in Ontario, that is directly attributable to the eligible production and paid to:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario-based individuals',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '632',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '632'
                }
              },
              null
            ]
          },
          {
            'label': 'other taxable Canadian corporations (for their Ontario-based employees)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '634',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '634'
                }
              },
              null
            ]
          },
          {
            'label': 'other taxable Canadian corporations (solely owned by an Ontario-based individual)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '636',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '636'
                }
              },
              null
            ]
          },
          {
            'label': 'partnerships (for their Ontario-based members or employees)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '638',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '638'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of lines 632 to 638)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '639',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'input': {
                  'num': '640',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '640'
                }
              }
            ]
          },
          {
            'label': 'Eligible wage and service contract expenditures incurred by a parent corporation (a taxable Canadian corporation) and transferred to its wholly-owned subsidiary (the corporation) under a reimbursement agreement',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '645',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '645'
                }
              }
            ]
          },
          {
            'label': 'Complete lines 646 and 647 if there is an entry on line 645:',
            'labelClass': 'fullLength'
          },
          {
            'label': ' - Name of parent corporation',
            'labelClass': 'tabbed'
          },
          {
            'type': 'infoField',
            'num': '646',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '1',
                    'max': '175'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'tn': '646'
          },
          {
            'label': ' - Business Number of parent corporation',
            'labelClass': 'tabbed'
          },
          {
            'num': '647',
            'tn': '647',
            'type': 'infoField',
            'inputType': 'custom',
            'format': [
              '{N9}RC{N4}',
              'NR'
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible tangible property expenditure directly attributable to the eligible production<br>(see subsection 92(5.6) of the <i>Taxation Act, 2007</i> (Ontario))',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '650',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '650'
                }
              }
            ]
          },
          {
            'label': 'Ontario production expenditure for the tax year (total of lines 630, 640, 645 and 650)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '651',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '651'
                }
              }
            ]
          },
          {
            'label': 'Ontario production expenditure for all previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '652',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '652'
                }
              }
            ]
          },
          {
            'label': '<b>Ontario production expenditure</b> (lines 651 <b>plus</b> line 652)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '655',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '655'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 5 - Assistance attributable to the expenditure incurred before April 24, 2015',
        'spacing': 'R_tn2',
        'rows': [
          {
            'label': 'Assistance received for the Ontario production expenditure included' +
            ' on line 655 (include amounts received, entitled to be received, or reasonably expected' +
            ' to be received by the corporation or any other person or partnership)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '657',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '657'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Amounts repaid under a legal obligation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '658',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '658'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (line 657 <b>minus</b> line 658) (if negative, enter "0")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '659',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '660',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '660'
                }
              },
              null
            ]
          },
          {
            'label': 'Amounts considered to have been paid/received, as applicable, for the eligible production and included in line 655:'
          },
          {
            'label': 'Federal tax credits and assistance*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '662',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '662'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Ontario refundable tax credits and assistance**',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '664',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '664'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (line 662 <b>plus</b> line 664)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '666',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '665',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '665'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Net assistance</b> (line 660 <b>minus</b> line 665) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '669',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '670',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '670'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            type: 'description',
            lines: [
              {
                type: 'list', listType: 'asterisk',
                items: [
                  {
                    label: 'Include only the following federal tax credits and assistance:',
                    sublist: [
                      'Canadian film or video production tax credit',
                      'Investment tax credit',
                      'Investment tax credit of cooperative corporation',
                      'Assistance provided after June 30, 2009 and before April 1, 2010 ' +
                      'by the Canadian Television Fund and assistance provided after March 31, 2010 by the ' +
                      'Canada Media Fund under the License Fee Program referred to in subsection 1106(11)' +
                      ' of the Federal regulations'
                    ]
                  },
                  {
                    label: 'Include only the following Ontario refundable tax credits and assistance:',
                    sublist: [
                      'Ontario Production Services Tax Credit',
                      'Ontario Book Publishing Tax Credit',
                      'Ontario Computer Animation and Special Effects Tax Credit',
                      'Ontario Sound Recording Tax Credit',
                      'Payments from the 2015 Ontario Production Services and Computer Animation and' +
                      ' Special Effects Transitional Fund administered by the OMDC'
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 6 - Ontario production expenditure incurred after April 23, 2015',
        'spacing': 'R_tn2',
        'rows': [
          {
            'label': 'Eligible wage expenditure paid to Ontario-based individuals, for services rendered in Ontario, that is directly attributable to the eligible production',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '730',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '730'
                }
              }
            ]
          },
          {
            'label': 'Eligible service contract expenditure, for services rendered in Ontario, that is directly attributable to the eligible production and paid to: *',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario-based individuals',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '732',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '732'
                }
              },
              null
            ]
          },
          {
            'label': 'other taxable Canadian corporations (for their Ontario-based employees)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '734',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '734'
                }
              },
              null
            ]
          },
          {
            'label': 'other taxable Canadian corporations (solely owned by an Ontario-based individual)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '736',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '736'
                }
              },
              null
            ]
          },
          {
            'label': 'partnerships (for their Ontario-based members or employees)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '738',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '738'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of lines 732 to 738)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '739',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '740',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '740'
                }
              }
            ],
          },
          {
            'label': 'Eligible wage and service contract expenditures incurred by a parent corporation (a taxable Canadian corporation) and transferred to its wholly-owned subsidiary (the corporation) under a reimbursement agreement',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '745',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '745'
                }
              }
            ]
          },
          {
            'label': 'Complete lines 746 and 747 if there is an entry on line 745:',
            'labelClass': 'fullLength'
          },
          {
            'label': ' - Name of parent corporation',
            'labelClass': 'tabbed'
          },
          {
            'type': 'infoField',
            'num': '746',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '1',
                    'max': '175'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'tn': '746'
          },
          {
            'label': ' - Business Number of parent corporation',
            'labelClass': 'tabbed'
          },
          {
            'num': '747',
            'tn': '747',
            'type': 'infoField',
            'inputType': 'custom',
            'format': [
              '{N9}RC{N4}',
              'NR'
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible tangible property expenditure directly attributable to the eligible production**<br>(see subsection 92(5.6) of the <i>Taxation Act, 2007</i> (Ontario))',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '750',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '750'
                }
              }
            ]
          },
          {
            'label': 'Ontario production expenditure for the tax year (total of lines 730, 740, 745 and 750)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '751',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '751'
                }
              }
            ]
          },
          {
            'label': 'Ontario production expenditure for all previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '752',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '752'
                }
              }
            ]
          },
          {
            'label': '<b>Ontario production expenditure</b> (line 751 <b>plus</b> line 752)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '755',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '755'
                }
              }
            ]
          },
          {
            'label': '* See Subsection 92(15) of the <i>Taxation Act, 2007</i> (Ontario) for the restrictions to tax years starting after April 23, 2015.<br>** See Subsection 92(16) and 92(17) of the <i>Taxation Act, 2007</i> (Ontario) for the restrictions to tax years starting after April 23, 2015.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 7 - Assistance attributable to the expenditure incurred after April 23, 2015',
        'spacing': 'R_tn2',
        'rows': [
          {
            'label': 'Assistance received for the Ontario production expenditure included on line 755 ' +
            '(include amounts received, entitled to be received, or reasonably expected to be received by the ' +
            'corporation or any other person or partnership)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '757',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '757'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Amounts repaid under a legal obligation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '758',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '758'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (line 757 <b>minus</b> line 758) (if negative, enter "0")',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '759',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '760',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '760'
                }
              },
              null
            ]
          },
          {
            'label': 'Amounts deemed to have been paid/received, as applicable, for the eligible production and included in line 755:'
          },
          {
            'label': 'Federal tax credits and assistance*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '762',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '762'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Ontario refundable tax credits and assistance**',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '764',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '764'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (line 762 <b>plus</b> line 764)',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '766',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '765',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '765'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Net assistance</b> (line 760 <b>minus</b> line 765) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '769',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '770',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '770'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength',
            forceBreakAfter: true
          },
          {
            type: 'description',
            lines: [
              {
                type: 'list', listType: 'asterisk',
                items: [
                  {
                    label: 'Include only the following federal tax credits and assistance:',
                    sublist: [
                      'Canadian film or video production tax credit',
                      'Investment tax credit',
                      'Investment tax credit of cooperative corporation',
                      'Assistance provided after June 30, 2009 and before April 1, 2010 ' +
                      'by the Canadian Television Fund and assistance provided after March 31, 2010 by the ' +
                      'Canada Media Fund under the License Fee Program referred to in subsection 1106(11)' +
                      ' of the Federal regulations'
                    ]
                  },
                  {
                    label: 'Include only the following Ontario refundable tax credits and assistance:',
                    sublist: [
                      'Ontario Production Services Tax Credit',
                      'Ontario Book Publishing Tax Credit',
                      'Ontario Computer Animation and Special Effects Tax Credit',
                      'Ontario Sound Recording Tax Credit',
                      'Payments from the 2015 Ontario Production Services and Computer Animation and' +
                      ' Special Effects Transitional Fund administered by the OMDC'
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 8 - Qualifying production expenditure (QPE) incurred before April 24, 2015',
        'spacing': 'R_tn2',
        'rows': [
          {
            'label': 'Ontario production expenditure (line 655 in Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '671'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Net assistance (line 670 in Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '672'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'QPE from all previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '675',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '675'
                }
              },
              null
            ]
          },
          {
            'label': 'Eligible wage and service contract expenditures incurred by the parent corporation (a taxable Canadian corporation) and transferred to its wholly-owned subsidiary corporation under a reimbursement agreement',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '680',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '680'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of amount B and lines 675 to 680)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '681',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '685',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '685'
                }
              }
            ]
          },
          {
            'label': '<b>Qualifying production expenditure for the tax year</b> (amount A <b>minus</b> line 685)' +
            ' (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '695',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '695'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 9 - Qualifying production expenditure (QPE) incurred after April 23, 2015',
        'spacing': 'R_tn2',
        'rows': [
          {
            'label': 'Complete amount C to line 786 if you have incurred qualifying expenditures after April 23, 2015. If your tax year started after April 23, 2015, also complete from line 1 to line 787',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario production expenditure (line 755 in Part 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '771'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Net assistance (line 770 in Part 7)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '772'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'label': 'QPE from all previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '775',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '775'
                }
              },
              null
            ]
          },
          {
            'label': 'Eligible wage and service contract expenditures incurred by the parent corporation (a taxable Canadian corporation) and transferred to its wholly-owned subsidiary corporation under a reimbursement agreement',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '780',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '780'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of amount D, line 775 and line 780)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '781',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '785',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '785'
                }
              }
            ]
          },
          {
            'label': '<b>Qualifying production expenditure</b> (amount C <b>minus</b> line 785)' +
            ' (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '786',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '786'
                }
              }
            ]
          },
          {
            label: 'Calculation of the qualifying production expenditure limit:',
            labelClass: 'fullLength'
          },
          {
            'label': 'Eligible wage expenditure<br>(line 730 from Part 6 <b>plus</b> unclaimed expenditures from a previous tax year)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '971'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '1'
                }
              },
              null
            ]
          },
          {
            'label': 'Eligible service contract expenditures*<br>(line 740 from Part 6 <b>plus</b> unclaimed expenditures from a previous tax year)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '972'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '2'
                }
              },
              null
            ]
          },
          {
            'label': 'If you are a wholly-owned subsidiary, the eligible wage and service contract expenditures*' +
            ' incurred by your parent corporation (a taxable Canadian corporation) and that you have reimbursed under a' +
            ' reimbursement agreement (line 745 from Part 6 <b>plus</b> unclaimed expenditures from a previous tax year.)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '973'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '3'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of amounts 1 to 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '974'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '4'
                }
              },
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '960'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Net assistance (line 770 in Part 7)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '975'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '5'
                }
              },
              null
            ]
          },
          {
            'label':'If you are a parent corporation (a taxable Canadian corporation), ' +
            'the eligible wage and service contract expenditures incurred by you and reimbursed by ' +
            'your wholly-owned subsidiary corporation under a reimbursement agreement',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '976'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '6'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (amount 5 <b>plus</b> amount 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '977'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '978'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F'
                }
              }
            ]
          },
          {
            'label': '<b>Qualifying production expenditure limit</b> (amount E <b>minus</b> amount F)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '979',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '787',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '787'
                }
              }
            ]
          },
          {
            'label': '<b>Qualifying production expenditure for the tax year</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '795',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '795'
                }
              }
            ]
          },
          {
            'label': 'For tax years that start before April 24, 2015, enter the amount from line 786.<br>' +
            'For tax years that start after April 23, 2015, enter the lesser amount from line 786 or line 787.<br>' +
            '<br>* Amounts 2 and 3 are restricted to salary and wages paid to Ontario based individuals for services provided under contract.' +
            '<br>** Ontario labour expenditures must amount to at least 25% of the qualifying production expenditures.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 10 - Ontario production services tax credit',
        'spacing': 'R_mult_tn2',
        'rows': [
          {
            'type': 'table',
            'num': 'di_table_1'
          },
          {
            'type': 'table',
            'num': 'di_table_2'
          },
          {
            'label': '<b>Ontario production services tax credit</b> (Amount G <b>plus</b> Amount H)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '720',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '720'
                }
              }
            ]
          },
          {
            'label': 'Enter the amount from line 720 on line 460 of Schedule 5, <i>Tax Calculation Supplementary ' +
            '– Corporations</i>. If you are filing more than one Schedule 558, add the amounts on line 720' +
            ' from all of the schedules and enter the total on line 460 of Schedule 5.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            label: 'See the privacy notice on your return.',
            labelClass: 'absoluteAlignRight'
          }
        ]
      }
    ]
  });
})();
