(function() {

  wpw.tax.create.tables('t2s558', {
    'di_table_1': {
      'num': 'di_table_1',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'QPE for the tax year (line 695 in Part 8) incurred before April 24, 2015',
            'trailingDots': true
          },
          '1': {
            'tn': '715'
          },
          '2': {
            'num': '715',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '901',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '902',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'G'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_2': {
      'num': 'di_table_2',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'QPE for the tax year (line 795 in Part 9) incurred after April 23, 2015',
            'trailingDots': true
          },
          '1': {
            'tn': '716'
          },
          '2': {
            'num': '716',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '903',
            'decimals': '2',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '904',
            'decimals': '2',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'H'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    100: {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1],
      'columns': [
        {
          'width': '60%',
          cellClass: 'alignLeft'
        },
        {
          cellClass: 'alignLeft'
        }
      ],
      cells: [
        {
          0: {
            label: 'Name of contact person',
            num: '110', "validate": {"or":[{"length":{"min":"1","max":"60"}},{"check":"isEmpty"}]},
            tn: '110',
            type: 'text'
          },
          1: {
            label: 'Telephone number',
            num: '120',
            tn: '120',
            'telephoneNumber': true,
            type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
            validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
          }
        }
      ]
    },
    250: {
      infoTable: true,
      fixedRows: true,
      columns: [{}],
      cells: [
        {
          0: {
            label: 'Certificate of eligibility number',
            num: '200', "validate": {"or":[{"length":{"min":"8","max":"9"}},{"check":"isEmpty"}]},
            tn: '200',
            maxLength: '9',
            type: 'text'
          }
        },
        {
          0: {
            label: 'Production title',
            num: '210', "validate": {"or":[{"length":{"min":"1","max":"175"}},{"check":"isEmpty"}]},
            tn: '210',
            maxLength: '175',
            type: 'text'
          }
        }
      ]
    },
    260: {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          'type': 'none',
          cellClass: 'alignCenter',
          'width': '30%'
        },
        {
          'type': 'text'
        },
        {
          'type': 'none',
          'width': '32%',
          cellClass: 'alignCenter'
        },
        {
          'type': 'text'
        }
      ],
      cells: [
        {
          '0': {
            'label': 'Date principal photography began',
            'tn': '220'
          },
          '1': {
            'num': '220',
            'type': 'date'
          },
          '2': {
            type: 'none'
          },
          '3': {
            type: 'none'
          }
        },
        {
          '0': {
            'label': 'Estimated QPE',
            'tn': '230'
          },
          '1': {
            'num': '230', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            'maxLength': '13'
          },
          '2': {
            'label': 'Estimated OPSTC for the production',
            'tn': '240'
          },
          '3': {
            'num': '240', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            'maxLength': '13'
          }
        }
      ]
    },
    960: {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      cells: [
        {
          0: {
            label: 'Amount 4**'
          },
          2: {
            num: '961'
          },
          3: {
            label: 'x'
          },
          4: {
            num: '962'
          },
          5: {
            label: '='
          },
          6: {
            num: '963'
          },
          7: {
            label: 'E'
          }
        }
      ]
    }

  })
})();
