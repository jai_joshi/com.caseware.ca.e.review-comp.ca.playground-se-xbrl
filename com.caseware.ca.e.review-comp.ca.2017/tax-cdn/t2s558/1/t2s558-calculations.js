(function() {

  function enableFields(calcUtils, numArray) {
    numArray.forEach(function(fieldId) {
      calcUtils.field(fieldId).disabled(false)
    })
  }

  wpw.tax.create.calcBlocks('t2s558', function(calcUtils) {
    calcUtils.calc(function(calcUtils) {
      var num = '715';
      var num2 = '902';
      var midnum = '901';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '716';
      var num2 = '904';
      var midnum = '903';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('110').assign(field('CP.951').get() + ' ' + field('CP.950').get());
      field('110').source(field('CP.950'));
      field('120').assign(field('CP.959').get());
      field('120').source(field('CP.959'));
  });

    //Part 3 Eligibility
    calcUtils.calc(function(calcUtils, field, form) {
      if (field('300').get() == 2 ||
          field('310').get() == 1 ||
          field('320').get() == 1 ||
          field('330').get() == 1 ||
          field('400').get() == 1) {
        calcUtils.removeValue([715, 716, 720], true);
      }
      else (
          enableFields(calcUtils, ['715', '716', '720'])
      )
    });

    //Part 4
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.sumBucketValues('639', ['632', '634', '636', '638']);
      calcUtils.equals('640', '639');
      calcUtils.sumBucketValues('651', ['630', '640', '645', '650']);
      calcUtils.sumBucketValues('655', ['651', '652']);
    });

    //Part 5
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.subtract('659', '657', '658');
      field('660').assign(Math.max(field('659').get(), 0));
      calcUtils.sumBucketValues('666', ['662', '664']);
      calcUtils.equals('665', '666');
      calcUtils.subtract('669', '660', '665');
      field('670').assign(Math.max(field('669').get(), 0));
    });

    //Part 6
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.sumBucketValues('739', ['732', '734', '736', '738']);
      calcUtils.equals('740', '739');
      calcUtils.sumBucketValues('751', ['730', '740', '745', '750']);
      calcUtils.sumBucketValues('755', ['751', '752']);
    });

    //Part 7
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.subtract('759', '757', '758');
      field('760').assign(Math.max(field('759').get(), 0));
      calcUtils.sumBucketValues('766', ['762', '764']);
      calcUtils.equals('765', '766');
      calcUtils.subtract('769', '760', '765');
      field('770').assign(Math.max(field('769').get(), 0));
    });

    //Part 8
    calcUtils.calc(function(calcUtils, field, form) {
      field('671').assign(field('655').get());
      field('672').assign(field('670').get());
      calcUtils.sumBucketValues('681', ['672', '675', '680']);
      field('685').assign(field('681').get());
      field('695').assign(Math.max(field('671').get() - field('685').get(), 0));
    });

    //Part 9
    calcUtils.calc(function(calcUtils, field, form) {
      field('962').assign(field('ratesOn.740').get());
      field('962').source(field('ratesOn.740'));
      field('771').assign(field('755').get());
      field('772').assign(field('770').get());
      calcUtils.sumBucketValues('781', ['772', '775', '780']);
      field('785').assign(field('781').get());
      field('786').assign(Math.max(field('771').get() - field('785').get(), 0));

      calcUtils.sumBucketValues('974', ['971', '972', '973']);
      field('961').assign(field('974').get());
      field('963').assign(field('961').get() * field('962').get());
      field('975').assign(field('770').get());
      calcUtils.sumBucketValues('977', ['975', '976']);
      field('978').assign(field('977').get());
      calcUtils.subtract('979', '963', '978');
      field('787').assign(field('979').get());

      if ((calcUtils.dateCompare.lessThan(field('cp.tax_start').get(), wpw.tax.date(2015, 4, 23)))) {
        field('795').assign(field('786').get())
      }
      else {
        field('795').assign(Math.min(field('786').get(), field('787').get()))
      }
    });

    //Part 10
    calcUtils.calc(function(calcUtils, field, form) {
      field('901').assign(field('ratesOn.741').get());
      field('901').source(field('ratesOn.741'));
      field('903').assign(field('ratesOn.742').get());
      field('903').source(field('ratesOn.742'));

      field('715').assign(field('695').get());
      field('716').assign(field('795').get());
      field('902').assign(field('715').get() * field('901').get() / 100);
      field('904').assign(field('716').get() * field('903').get() / 100);

      field('720').assign(field('902').get() + field('904').get())
    });


  });
})();
