(function() {
  wpw.tax.create.diagnostics('t2s304', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3040001', common.prereq(common.check(['t2s5.507'], 'isNonZero'),
        common.requireFiled('T2S304')));

    diagUtils.diagnostic('3040002', common.prereq(common.and(
        common.requireFiled('T2S304'),
        common.check(['120'], 'isNonZero')),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            return row[1].require('isNonZero');
          });
        }));

    diagUtils.diagnostic('3040003', common.prereq(common.and(
        common.requireFiled('T2S304'),
        common.check(['901', '902', '903'], 'isNonZero')),
        common.check('120', 'isNonZero')));

    diagUtils.diagnostic('3040004', common.prereq(common.requireFiled('T2S304'),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[1].isNonZero())
              return row[0].require('isFilled');
            else return true;
          });
        }));

    //CRA has separate error codes for corresponding fields. (3040004, 3040005)
    diagUtils.diagnostic('3040005', common.prereq(common.requireFiled('T2S304'),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[0].isFilled())
              return row[1].require('isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('304.123', common.prereq(common.requireFiled('T2S304'),
        function(tools) {
          return tools.field('123').require(function() {
            return this.get() < 50000;
          })
        }));

    //TODO: Add this diagnostic after "history of taxation years" functionality is merged
    // diagUtils.diagnostic('3040006', common.prereq(function(tools) {
    //   return tools.checkMethod(tools.list(['901', '902', '903']), 'isNonZero');
    // }, function(tools) {
    //   return wpw.tax.utilities.dateCompare.greaterThan('priorTaxYearEnd', {year: 2005, month: 12, day: 31});
    // }));

  });
})();

