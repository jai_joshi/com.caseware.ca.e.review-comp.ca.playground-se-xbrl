(function() {

  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  wpw.tax.create.calcBlocks('t2s304', function(calcUtils) {

    //Part 1
    calcUtils.calc(function(calcUtils, field) {
      field('150').assign(field('100').total(1).get());
    });

    //calcs years for all table
    calcUtils.calc(function(calcUtils, field, form) {
      var tableArrays = [300, 400, 1000];
      var summaryTable = field('tyh.200');
      tableArrays.forEach(function(tableNum) {
        field(tableNum).getRows().forEach(function(row, rowIndex) {
          if (tableNum == 300) {
            var year = Math.abs(rowIndex - 19);
            row[1].assign(summaryTable.cell(year, 6).get());
          }
          else if (tableNum == 400) {
            row[1].assign(summaryTable.cell(rowIndex + 13, 6).get())
          }
          else {
            row[1].assign(summaryTable.cell(rowIndex + 12, 6).get())
          }
        });
      });
    });

    //Part 4
    calcUtils.calc(function(calcUtils, field) {
      var summaryTable = field('1000');
      var limitOnCredit = field('122').get();
      // historical data chart
      summaryTable.getRows().forEach(function(row, rowIndex) {
        if (rowIndex == 0) {
          // to avoid the first row being calculated to total
          row[7].assign(0);
          row[9].assign(0);
          row[11].assign(0);
          row[13].assign(0);
        } else {
          row[9].assign(Math.max(row[3].get() + row[5].get() + row[7].get(), 0));
          row[11].assign(returnAmountApplied(limitOnCredit, row[9].get()));
          row[13].assign(Math.max(row[9].get() - row[11].get(), 0));
          limitOnCredit -= row[11].get()
        }
      });
      summaryTable.cell(8, 5).assign(field('125').get());

      field('400').getRows().forEach(function(row, rowIndex) {
        var rIndex = rowIndex + 1;
        row[3].assign(summaryTable.cell(rIndex, 13).get());
      });
    });

    //Part 2
    calcUtils.calc(function(calcUtils, field) {
      field('102').assign(field('1000').total(3).get());
      field('104').assign(field('1000').cell(0, 3).get());
      calcUtils.subtract('105', '102', '104');
      calcUtils.equals('106', '105');

      calcUtils.equals('120', '150');
      calcUtils.equals('121', '120');
      calcUtils.sumBucketValues('122', '106', '121');

      field('123').assign(Math.min(field('1000').total(11).get() - field('904').get(), field('t2s307.240').get(), 50000));
      field('124').assign(field('300').total(5).get());
      calcUtils.sumBucketValues('125', '123', '124');
      calcUtils.equals('126', '125');
      calcUtils.subtract('200', '122', '126');
    });
  });
})();
