(function() {

  wpw.tax.create.tables('t2s304', {
    //Part 1
    100: {
      showNumbering: true,
      hasTotals: true,
      columns: [
        {
          header: 'Tax Credit receipt number<br>',
          num: '100', "validate": {"or": [{"length": {"min": "9", "max": "9"}}, {"check": "isEmpty"}]},
          tn: '100',
          type: 'text'
        },
        {
          header: 'Newfoundland and Labrador resort property investment <br>tax credit amount',
          colClass: 'std-input-width',
          num: '103', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          tn: '103',
          total: true,
          totalNum: '150',
          totalIndicator: 'A',
          totalMessage: 'Total credit earned in the current tax year'
        }
      ]
    },
    //Part 3
    300: {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {
          'width': '190px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          header: 'Tax year in which to apply the credit',
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          width: '190px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'total': true,
          'totalNum': '904',
          'totalMessage': 'Total (enter on line F in Part 2)'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        }],
      'cells': [
        {
          '0': {
            'label': '1st previous tax year '
          },
          '3': {
            'label': 'Amount to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '901'
          },
          '5': {
            'num': '901',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '2nd previous tax year '
          },
          '3': {
            'label': 'Amount to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '902'
          },
          '5': {
            'num': '902',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '3rd previous tax year '
          },
          '3': {
            'label': 'Amount to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '903'
          },
          '5': {
            'num': '903',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }
      ]
    },
    //Part 4
    400: {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      'columns': [
        {
          width: '190px',
          type: 'none'
        },
        {
          header: 'Year of origin (earliest year first)',
          colClass: 'std-input-width',
          type: 'date',
          disabled: true
        },
        {

          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          header: 'Credit available for carryforward',
          colClass: 'std-input-width',
          total: true,
          totalNum: '350',
          totalMessage: 'Total (equal to line 200 in Part 2)'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      'cells': [
        {0: {label: '7th previous tax year'}},
        {0: {label: '6th previous tax year'}},
        {0: {label: '5th previous tax year'}},
        {0: {label: '4th previous tax year'}},
        {0: {label: '3rd previous tax year'}},
        {0: {label: '2nd previous tax year'}},
        {0: {label: '1st previous tax year'}},
        {0: {label: 'Current tax year'}}
      ]
    },
    //History
    1000: {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          type: 'date',
          header: '<b>Year of origin</b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          totalNum: '501',
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          totalNum: '502',
          totalMessage: ' ',
          header: '<b>Current year contribution</b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          totalNum: '503',
          totalMessage: ' ',
          header: '<b>Transfer</b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          totalNum: '504',
          totalMessage: ' ',
          header: '<b>Amount available to apply</b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          disabled: true,
          totalNum: '505',
          totalMessage: ' ',
          header: '<b>Applied<b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          disabled: true,
          totalNum: '506',
          totalMessage: ' ',
          header: '<b>Balance to carry forward</b>'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      cells: [
        {
          4: {
            label: '*'
          },
          5: {
            type: 'none'
          },
          7: {
            type: 'none'
          },
          9: {
            type: 'none'
          },
          11: {
            type: 'none'
          },
          13: {
            type: 'none',
            label: 'N/A'
          }
        },
        {
          5: {
            type: 'none'
          },
          14: {
            label: '**'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          3: {
            type: 'none'
          },
          7: {
            type: 'none'
          }
        }
      ]
    }
  })
})();
