(function() {

  wpw.tax.create.formData('t2s304', {
    formInfo: {
      abbreviation: 'T2S304',
      title: 'Newfoundland and Labrador Resort Property Investment Tax credit',
      schedule: 'Schedule 304',
      headerImage: 'canada-federal',
      code: 'Code 0701',
      formFooterNum: 'T2 SCH 304 E',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'You can claim a Newfoundland and Labrador resort property investment tax ' +
              'credit if you are a <b>qualifying investor</b> that made an <b>eligible investment</b> in a ' +
              '<b>qualifying resort development property unit</b> in Newfoundland and Labrador after ' +
              'June 13, 2007, but not more than five years after the unit was first made available ' +
              'for sale. (The terms in bold are defined in Section 2 of the ' +
              '<i>Resort Property Investment Tax Credit Regulations</i>.)'
            },
            {
              label: 'Use this schedule to: ',
              sublist: ['claim the credit to reduce Newfoundland and Labrador income tax payable*' +
              ' in the current tax year;',
                'calculate the credit you have available to carry forward; or',
                'request a carryback.']
            },
            {
              label: 'An unused credit earned in the current tax year is not refundable. ' +
              'The unused credit can be carried forward for seven tax years and carried back ' +
              'three tax years. However, you cannot carry the credit back to tax years that ' +
              'end before January 1, 2006.'
            },
            {
              label: 'The credit is equal to the least of the following amounts: ',
              sublist: ['Newfoundland and Labrador income tax payable*;',
                'the amount indicated on the tax credit receipts issued during the current year,' +
                ' less amounts used in previous years, plus amounts carried forward or back from another year; or',
                '$50,000 (this is the maximum credit you can claim in a tax year, ' +
                'including any amounts carried back or carried forward).']
            },
            {
              label: '*Newfoundland and Labrador income tax payable is defined as the amount payable ' +
              'before the deduction for the small business tax holiday and the refundable credits. ' +
              '(see paragraph 2(l) of the Regulations)'
            }
          ]
        }
      ],
      category: 'Newfoundland and Labrador Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Total credit earned in the current tax year',
        'rows': [
          {
            'type': 'table',
            'num': '100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If you need more space, attach additional schedules.'
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of credit available for carryforward',
        'rows': [
          {
            'label': 'Credit at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '102'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Deduct:</b> Credit expired after seven tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit at the beginning of the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '106',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Add: ',
            'labelClass': 'bold'
          },
          {
            'label': 'Current-year credit earned (enter amount A)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '120',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              },
              {
                'input': {
                  'num': '121',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Total credit available (<b>add</b> lines B and C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '122'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Deduct: ',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit claimed in the current year (the least of amount D, income tax payable*, and $50,000)<br> (enter this amount on line 507 in Part 2 of Schedule 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '123'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'label': 'Credit carried back to previous tax years (complete Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '124'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F'
                }
              }
            ]
          },
          {
            'label': 'Line E <b>plus</b> line F',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '125'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '126'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': '<b>Closing balance</b> (line D <b>minus</b> line G)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 3 - Request for carryback of credit',
        'rows': [
          {
            'label': 'Complete this part to request a carryback of a current-year credit earned to a tax year that ends <b>after December 31, 2005</b>. The maximum amount of credit that you can apply to a prior year is the least of:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- the portion of your current-year credit earned that exceeds your Newfoundland and Labrador income tax payable* and $50,000, whichever is less;',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '- the income tax payable* in the prior year; and',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '- $50,000 minus any amounts previously applied in the prior year.',
            'labelClass': 'tabbed fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '300'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '*Newfoundland and Labrador income tax payable is defined as the amount payable before the deduction for the small business tax holiday and the refundable credits. (see paragraph 2(l) of the Regulations)',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 4 - Credit available for carryforward by year of origin',
        'rows': [
          {
            'label': 'You can complete this part to show all the credits from previous tax years available for carryforward, by year of origin. This will help you determine the amount of credit that could expire in following years.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '400'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The amount available from the 7th previous tax year will expire after this tax year. When you file your return for the next year, you will enter the expired amount on line 104 of Schedule 304 for that year.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for Newfoundland and Labrador Resort Property Investment Tax Credit',
        'rows': [
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  });
})();
