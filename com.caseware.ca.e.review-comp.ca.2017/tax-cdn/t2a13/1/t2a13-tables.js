(function() {
  var assetCode = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.assetCode, 'value');
  var canadaProvinces = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.canadaProvinces, 'EN');

  wpw.tax.create.tables('t2a13', {
    '100': {
      'showNumbering': true,
      'fixedRows': true,
      'keepButtonsSpace': true,
      'linkedRepeatForm': 't2s8w',
      'columns': [
        {
          'header': '1<br>Class number',
          'tn': '001',
          'disabled': true,
          colClass: 'small-input-width',
          'linkedFieldId': '101',
          decimals: 1
        },
        {
          'header': '<br>Description',
          colClass: 'std-input-col-width',
          'disabled': true,
          'linkedFieldId': '103',
          'type': 'text'
        },
        {
          'header': '8<br>Reduced undepreciated capital cost (column 6 minus column 7)',
          'disabled': true,
          'linkedFieldId': '210-A',
          filters: 'number'
        },
        {
          'header': '9<br>CCA rate %',
          colClass: 'small-input-width',
          'tn': '013',
          'disabled': true,
          'linkedFieldId': '212-A',
          num: '013'
        },
        {
          'header': '10<br>Recapture of capital cost allowance',
          'tn': '015',
          'total': true,
          'totalTn': '023',
          'totalNum': '023',
          'totalMessage': 'Totals : ',
          'disabled': true,
          'linkedFieldId': '213-A',
          num: '015'
        },
        {
          'header': '11<br>Terminal loss',
          'tn': '017',
          'total': true,
          'totalNum': '025',
          'totalTn': '025',
          'disabled': true,
          'linkedFieldId': '215-A',
          num: '017'
        },
        {
          'header': '12***<br>Capital cost allowance (column 8 X column 9 or a lesser amount)',
          'tn': '019',
          'total': true,
          'totalNum': '027',
          'disabled': true,
          'totalTn': '027',
          'linkedFieldId': '217-A',
          num: '019'
        },
        {
          'header': '13<br>Undepreciated capital cost at the end of the year (cols 6 + 10 - 11 - 12)',
          'tn': '021',
          'disabled': true,
          'linkedFieldId': '220-A',
          'total': true,
          num: '021'
        }
      ],
      'startTable': '090',
      'hasTotals': true
    },
    '110': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {
          'type': 'none',
          colClass: 'std-input-col-padding-width-2'
        },
        {
          'type': 'none',
          cellClass: 'alignLeft'
        }
      ],
      'cells': [
        {
          '1': {
            'label': 'Carry forward the amounts from lines 023, 025 and 027 to Schedule 12 lines 006, 008 and 004, respectively.',
            labelClass: 'bold'
          }
        }
      ]
    },
    '090': {
      'infoTable': false,
      'showNumbering': true,
      hasTotals: true,
      'repeats': [100],
      'keepButtonsSpace': true,
      fixedRows: true,
      'linkedRepeatForm': 't2s8w',
      'maxLoop': 100000,
      'columns': [
        {
          'header': '1<br>Class number',
          'tn': '001',
          num: '001',
          'disabled': true,
          colClass: 'small-input-width',
          'linkedFieldId': '101',
          decimals: 1
        },
        {
          'header': '<br>Description',
          colClass: 'std-input-col-width',
          'disabled': true,
          'linkedFieldId': '103',
          'type': 'text'
        },
        {
          'header': '2<br>Undepreciated capital cost at the beginning of the year' +
          ' (must equal the closing balance from last year\'s CCA schedule)',
          'tn': '003',
          'disabled': true,
          'linkedFieldId': '201-A',
          num: '003',
          filters: 'number',
          'total': true
        },
        {
          'header': '3*<br>Cost of acquisitions during the year <br>(new property must be available for use)',
          'tn': '005',
          'disabled': true,
          'linkedFieldId': '203-1-A',
          num: '005',
          filters: 'number',
          'total': true
        },
        {
          'header': '4<br>Net adjustments (show negative amounts in brackets)',
          'tn': '007',
          'disabled': true,
          'linkedFieldId': '205-A',
          num: '007',
          filters: 'number',
          'total': true
        },
        {
          'header': '5<br>Proceeds of dispositions during the year (amount not to exceed the capital cost)',
          'tn': '009',
          'disabled': true,
          'linkedFieldId': '207-A',
          num: '009',
          filters: 'number',
          'total': true
        },
        {
          'header': '6<br>Undepreciated capital cost (column 2 plus column 3 plus or minus column 4 minus column 5)',
          'disabled': true,
          'linkedFieldId': '208-A',
          filters: 'number',
          'total': true
        },
        {
          'header': '7**<br>50% rule (1/2 of the amount, if any, by which the net cost of acquisitions exceeds column 5)',
          'tn': '011',
          'disabled': true,
          'linkedFieldId': '211-A',
          num: '011',
          filters: 'number',
          'total': true

        }
      ]
    }
  })
})();
