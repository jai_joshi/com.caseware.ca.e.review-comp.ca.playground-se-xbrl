(function() {
  wpw.tax.create.formData('t2a13',{
    formInfo: {
      abbreviation: 'AT13',
      title: 'Alberta Capital Cost Allowance (CCA)',
      schedule: 'Schedule 13',
      // headerImage: 'canada-alberta',
      neededRepeatForms: ['t2s8w'],
      showCorpInfo: true,
      formFooterNum: 'AT13 (Jul-12)',
      category: 'Alberta Tax Forms',
      dynamicWidth: true,
      hideProtectedB: true
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            labelClass: 'fullLength'
          },
          {
            label: 'This schedule is <u>required</u> if the opening UCC or the CCA claimed for Alberta' +
            ' purposes for any class of assets differs from that for federal purposes.',
            labelClass: 'bold'
          },
          {
            label: 'Report all monetary amounts in dollars; DO NOT include cents.'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            type: 'table',
            num: '090'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table', num: '100'
          },
          {
            'type': 'table', 'num': '110'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {
            label: '*  Include any property acquired in previous years that has now become available for use. ' +
            'This property would have been previously excluded from column 3. List separately any acquisitions ' +
            'that are not subject to the 50% rule',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: '** The net cost of acquisitions is the cost of acquisitions plus or minus certain ' +
            'adjustments from column 4.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: '*** If the taxation year is shorter than 365 days, prorate the CCA claim.',
            labelClass: 'fullLength'
          }
        ]
      }
    ]
  });
})();
