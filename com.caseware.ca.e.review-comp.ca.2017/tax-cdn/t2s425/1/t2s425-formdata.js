(function() {
  'use strict';

  wpw.tax.global.formData.t2s425 = {
    formInfo: {
      abbreviation: 't2s425',
      title: 'British Columbia (BC) Scientific Research and Experimental Development Tax Credit',
      //TODO: DO NOT DELETE THIS LINE: code
      schedule: 'Schedule 425',
      //code: '1501',
      category: 'British Columbia Forms',
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'To claim this tax credit, attach this form to the<b> top of the ' +
              'corporation\'s T2 Corporation Income Tax Return</b> for the year. This form must be ' +
              'filed within 18 months of the end of the tax year in which the corporation\'s ' +
              'scientific research and experimental development (SR&ED) qualified BC expenditures ' +
              'are incurred.<b> Late filed tax credit forms will not be processed</b>. Please refer to ' +
              'Part 6 of the<i>Income Tax Act</i>(British Columbia). '
            },
            {
              label: 'This form is to be used by corporations that have made a<b> SR&ED qualified ' +
              'BC expenditure</b>. A corporation\'s <b>SR&ED qualified BC expenditure</b> for a tax ' +
              'year is the total of its <b>BC qualified expenditures</b> incurred in that year ' +
              'and its<b> eligible repayment</b> for that tax year. A corporation that is a member ' +
              'of a partnership, other than a specified member as defined in subsection 248(1) ' +
              'of the federal <i>Income Tax Act</i>, can use this form to claim its proportionate share ' +
              'of the partnership\'s British Columbia SR&ED tax credit earned on<b> BC qualified ' +
              'expenditures</b> incurred after February 20, 2007. A specified member includes ' +
              'any limited partner. The<b> credit amount from a partnership </b>is reported at line ' +
              'a of Part 5 and must not be included in Schedule A.'
            },
            {
              label: '<b>Qualified expenditure</b> has the same meaning as subsection 127(9) of ' +
              'the federal Act, except government assistance does not include a British Columbia ' +
              'SR&ED tax credit or an investment tax credit under subsection 127(5) or (6) of the federal Act. '
            },
            {
              label: '<b>BC qualified expenditures </b>include qualified expenditures made after ' +
              'August 31, 1999 (February 20, 2007 for partnerships) and before September 1, 2017 ' +
              'in respect of SR&ED carried on in British Columbia. The expenditure must have ' +
              'been incurred at a time when the corporation or partnership had a permanent ' +
              'establishment in British Columbia. Expenditures incurred by an individual or ' +
              'trust do not qualify. BC qualified expenditures do not include expenditures ' +
              'incurred in the course of earning income if any of the income is exempt income, ' +
              'as defined in section 248(1) of the federal Act, or is exempt from tax under ' +
              'Part 1 of the federal Act'
            },
            {
              label: 'A corporation\'s <b>eligible repayment</b> is the total of all amounts of ' +
              'designated assistance repaid in the tax year by the corporation or deemed repaid ' +
              'under subsection 127(10.8) of the federal Act, to the extent each amount is ' +
              'a repayment of designated assistance that reduced an amount of a BC qualified ' +
              'expenditure in the tax year or a previous tax year. '
            },
            {
              label: 'The <b>refundable</b> tax credit may only be claimed by a<b> Canadian-controlled private ' +
              'corporation (CCPC)</b>. Any refundable credit to which the corporation is entitled will ' +
              'be offset against other income taxes payable (federal and provincial) and a refund ' +
              'cheque may be issued for any remainder. The refundable credit is limited to 10% ' +
              'of the lesser of the corporation\'s SR&ED qualified BC expenditure for the tax ' +
              'year and its expenditure limit as defined in subsection 127(10.2) of the ' +
              'federal Act. If a CCPC\'s expenditure exceeds the corporation\'s expenditure ' +
              'limit, the corporation may be able to claim a non-refundable credit.'
            },
            {
              label: 'Other corporations, corporations claiming a credit amount from a ' +
              'partnership and CCPCs whose expenditure exceeds their expenditure limit, may claim ' +
              'a <b>non-refundable </b>tax credit to the extent of their British Columbia tax otherwise ' +
              'payable. Any remaining non-refundable tax credit, if not deductible in the year ' +
              'earned, may be used to offset British Columbia tax otherwise payable in any of the ' +
              'three previous years and ten subsequent years. '
            }
          ]
        }
      ]
    },
    sections: [
      {
        'header': 'Freedom of Information and Protection of Privacy Act (FOIPPA)',
        'rows': [
          {
            'label': 'The personal information on this form is collected for the purpose of administering the<i>Income Tax Act</i>(British Columbia) under the authority of section 26(a) of the<i> FOIPPA</i>. Questions about the collection or use of this information can be directed to the Manager, Intergovernmental Relations, PO Box 9444 Stn Prov Govt, Victoria BC V8W 9W8. (Telephone: Victoria at<b> 250-387-3332</b> or toll-free at<b> 1-877-387-3332</b> and ask to be re-directed). Email: ITBTaxQuestions@gov.bc.ca',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 1 - Corporate Information',
        'rows': [
          {
            'type': 'table',
            'num': '1600'
          },
          {
            'type': 'table',
            'num': '1650'
          },
          {
            'type': 'table',
            'num': '1700'
          }
        ]
      },
      {
        'header': 'Part 2 - Eligibility',
        'rows': [
          {
            'label': 'Was the corporation exempt from taxation under section 27 of the <i>Income Tax Act</i> (British Columbia) or Part I of the federal Act?',
            'type': 'infoField',
            'num': '201',
            'inputType': 'radio',
            'labelClass': 'fullLength',
            'labelWidth': '80%'
          },
          {
            'label': 'Was the corporation at any time in the year controlled directly or indirectly in any manner whatever by one or more persons, all or part of whose income was exempt from taxation under section 27 of the <i>Income Tax Act</i> (British Columbia) or Part I of the federal Act? ',
            'labelClass': 'fullLength',
            'num': '202',
            'type': 'infoField',
            'inputType': 'radio',
            'labelWidth': '80%',
            'init': '2'
          },
          {
            'label': 'Was the corporation at any time in the year:'
          },
          {
            'label': 'a) a small business venture capital corporation registered under section 3 of the Small Business Venture Capital Act?',
            'labelClass': 'fullLength',
            'num': '203',
            'type': 'infoField',
            'inputType': 'radio',
            'labelWidth': '80%',
            'init': '2'
          },
          {
            'label': 'b) an employee venture capital corporation registered under section 8 of the Employee Investment Act?',
            'labelClass': 'fullLength',
            'num': '204',
            'type': 'infoField',
            'inputType': 'radio',
            'labelWidth': '80%',
            'init': '2'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the answer to any of the above questions is "yes", the corporation is not eligible for a tax credit.',
            'labelClass': 'bold fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - SR&ED Qualified BC Expenditure',
        'rows': [
          {
            'label': 'To determine the amount of total current and capital BC qualified expenditures, complete Schedule A.',
            'labelClass': 'bold fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total current BC qualified expenditures in the tax year ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '350',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '350'
                }
              }
            ]
          },
          {
            'label': 'Total capital BC qualified expenditures in the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '360',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '360'
                }
              }
            ]
          },
          {
            'label': 'Eligible repayment made in the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '370',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '370'
                }
              }
            ]
          },
          {
            'label': '<b>SR&ED qualified BC expenditure for the year (add </b>lines 350 to 370)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '380',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '380'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 4 - Refundable tax credit for Canadian-controlled private corporations',
        'rows': [
          {
            'label': 'Corporation\'s expenditure limit as defined in subsection 127(10.2) of the federal Act',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '351'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Corporation\'s SR&ED qualified BC expenditure for the year (from line 380 in Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '352'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Enter the lesser of amount A and amount B',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '353'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Applicable rate',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '354',
                  'filters': 'decimals 2'
                }
              }
            ],
            'indicator': '%D'
          },
          {
            'label': 'Refundable tax credit available (amount C <b>multiplied</b> by percentage D)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '355'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': '<b>Refundable tax credit claim </b>(enter an amount up to amount E on line 674 of Schedule 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '356'
                }
              }
            ],
            'indicator': 'F'
          }
        ]
      },
      {
        'header': 'Part 5 - Non-refundable tax credit for the year',
        'rows': [
          {
            'label': 'Corporation\'s SR&ED qualified BC expenditure for the year (from line 380 in Part 3) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '357'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Applicable rate',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '358',
                  'filters': 'decimals 2'
                }
              }
            ],
            'indicator': '%H'
          },
          {
            'label': 'Tax credit earned in the year (<b>multiply </b>amount G by percentage H)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '359'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Tax credit allocated from a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '500'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Recaptured BC SR&ED tax credit allocated to the corporation from a partnership of which the corporation is a member',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '361'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': '<b>Subtotal</b> (amount a <b>minus</b> amount b)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '362'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '363'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': '(if amount b exceeds amount a enter <b>nil</b> at line J and transfer the calculated amount to Part 10 of this form)',
            'labelClass': 'tabbed'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Refundable tax credit claimed (from amount F in Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '364'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'Tax credit renounced for the current tax year (from amount Q in Part 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '365'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': '<b>Subtotal </b>(amount c <b>plus</b> amount d)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '366'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '367'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': 'Annual non-refundable tax credit for the year (amount I<b> plus</b> amount J <b>minus</b> amount K)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '368'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Tax credit balance at the beginning of the tax year (from line 520 in Part 7)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '369'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': 'Tax credit transferred on amalgamation or windup of a subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '371'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'label': '<b>Subtotal</b> (amount e<b> plus</b> amount f)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '372'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '373'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'label': 'Total non-refundable tax credit available for deduction (Amount L<b> plus</b> amount M)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '374'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'label': 'British Columbia income tax otherwise payable',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '375'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'label': 'British Columbia tax payable from line 240 on Schedule 5 less BC foreign tax credit, BC logging tax credit, BC political contribution tax credit, and BC small business venture capital tax credit.',
            'labelClass': 'tabbed'
          },
          {
            'label': '<b>Non-refundable tax credit claim </b>(enter this amount on line 659 of Schedule 5) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '376'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'label': 'Claim the amount that is the lesser of amount N and amount O or <b>nil</b> if the corporation has made a deduction for the BC two-year tax holiday for new small businesses',
            'labelClass': 'tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Complete Part 8 </b>if the amount claimed at line P includes amounts carried forward from preceding years at line e. The non-refundable tax credit may be used to offset British Columbia tax payable for a three-year carry back (if not deductible in the year earned) and a ten-year carry forward. '
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Complete Part 9 </b>to carry back any non-refundable tax credit if not deductible in the current year earned'
          }
        ]
      },
      {
        'header': 'Part 6 - Renunciation of tax credit',
        'rows': [
          {
            'label': 'The corporation hereby renounces, under subsection 100(1) of the <i>Income Tax Act</i> (British Columbia), all entitlement to the British Columbia Scientific Research and Experimental Development tax credit as follows:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount of non-refundable tax credit the corporation elects to renounce',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '377'
                }
              }
            ],
            'indicator': 'Q'
          },
          {
            'label': 'The amount renounced cannot exceed the total credit earned in the year less the refundable credit claimed in the year (amount I<b> minus</b> amount C from Part 5).',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 7 - Summary of tax credits available for carryforward or carryback',
        'rows': [
          {
            'label': 'Tax credit balance at the end of the previous year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '378'
                }
              }
            ],
            'indicator': 'R'
          },
          {
            'label': '<b>Deduct</b>: Tax credit expired after 10 years .',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '504',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '504'
                }
              }
            ]
          },
          {
            'label': 'Tax credit at the beginning of the year (amount R <b>minus</b> line 504)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '520',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '520'
                }
              }
            ]
          },
          {
            'label': 'Add',
            'labelClass': 'bold'
          },
          {
            'label': 'Tax credit transferred on amalgamation or windup (from amount f in Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '530',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '530'
                }
              },
              null
            ]
          },
          {
            'label': 'Tax credit allocated from a partnership (from amount J in Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '535',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '535'
                }
              },
              null
            ]
          },
          {
            'label': 'Tax credit earned in the current year (from amount I in Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '540',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '540'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Subtotal </b>(line 530 <b>plus</b> line 540)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '541'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '542'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'S'
          },
          {
            'label': 'Tax credit available (line 520 <b>plus</b> amount S)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '543'
                }
              }
            ],
            'indicator': 'T'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Tax credit renounced (from amount Q in Part 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '580',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '580'
                }
              },
              null
            ]
          },
          {
            'label': 'Refundable tax credit claimed (from amount F in Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '610',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '610'
                }
              },
              null
            ]
          },
          {
            'label': 'Non-refundable tax credit claimed (from amount P in Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '560',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '560'
                }
              },
              null
            ]
          },
          {
            'label': 'Non-refundable tax credit carried back to previous years (from amount X in Part 9)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '561'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'g'
                }
              }
            ]
          },
          {
            'label': '<b>Subtotal (Add</b> line 580, line 610, line 560, and amount g)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '562'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '563'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'U'
          },
          {
            'label': '<b>Tax credit balance at the end of the year</b> (amount T <b>minus</b> amount U)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '620',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '620'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 8 - Request for application of tax credit from previous years',
        'rows': [
          {
            'label': 'The corporation hereby requests the following tax credit to be applied to the current year tax payable.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'label': '<b>Total to be applied (add</b> amounts h to q)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1070'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1071'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'W'
          }
        ]
      },
      {
        'header': 'Part 9 - Request for carryback of tax credit',
        'rows': [
          {
            'label': 'The corporation hereby requests a carryback of the tax credit to be applied as follows:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1100'
          },
          {
            'label': '<b>Total carried back (add </b>lines 911 to 913)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '1170'
                }
              }
            ],
            'indicator': 'X'
          }
        ]
      },
      {
        'header': 'Part 10 - Calculating the recapture of tax credits',
        'rows': [
          {
            'label': 'You will have a recapture of tax credit in a year when all of the following conditions are met:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- you acquired a particular property and the cost of the property was a BC qualified expenditure in a tax year;',
            'labelClass': 'tabbed2 fullLength'
          },
          {
            'label': '- the cost of the particular property was included in computing your tax credit; and',
            'labelClass': 'tabbed2'
          },
          {
            'label': '- you disposed of the particular property or converted it to commercial use after March 31, 2000 (after February 20, 2007, in the case of partnerships), and within 10 tax years of the acquisition. This condition is also met if you disposed of or converted to commercial use a property which incorporates the property previously referred to. If, however, you sell the property to a non-arm\'s length purchaser who continues to use all the property, or substantially all for British Columbia SR&ED, the recapture does not apply.',
            'labelClass': 'tabbed2 fullLength'
          },
          {
            'label': 'You will also have a recapture if you have an excess of recaptured BC SR&ED tax credit allocated from a partnership transferred from Part 5 (amount b <b>minus</b> amount a, if greater than "0"). Enter the transferred amount in the calculation below as a positive number at line EE.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'You will report a recapture on the T2 return for the year in which you disposed of the property or converted it to commercial use.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'If you have more than one disposition, please complete the columns for each disposition for which a recapture applies, using the calculation format below.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1200'
          },
          {
            'label': '<b>Subtotal (add </b>all amounts from column CC)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1301'
                }
              }
            ],
            'indicator': 'DD'
          },
          {
            'label': 'Corporate partner\'s share of the excess of the BC SR&ED tax credit (from Part 5, amount b<b> minus </b>amount a, if greater than "0")*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1302'
                }
              }
            ],
            'indicator': 'EE'
          },
          {
            'label': '<b>Total recapture of BC SR&ED tax credit</b> (enter this amount on line 241 Schedule 5) (amount DD<b> plus</b> EE)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1303'
                }
              }
            ],
            'indicator': 'FF'
          },
          {
            'label': '*  As a member of a partnership, a corporation will report its appropriate portion of the British Columbia SR&ED tax credit of the partnership after the BC SR&ED tax credit has been reduced by the amount of any recapture. If this amount is a positive amount, enter the amount on line J in Part 5. However, if the partnership does not have sufficient BC SR&ED tax credit otherwise available to offset the recapture, then the amount by which reductions to the BC SR&ED tax credit exceed additions (the excess) will be determined and added to the corporation\'s tax otherwise payable.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Schedule A - Current and capital BC qualified expenditure',
        'rows': [
          {
            'type': 'table',
            'num': '1400'
          }
        ]
      },
      {
        header: 'Historical data and calculation for British Columbia Scientific Research and Experimental Development Tax Credit ',
        rows: [
          {
            type: 'table',
            num: '2000'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  };
})();
