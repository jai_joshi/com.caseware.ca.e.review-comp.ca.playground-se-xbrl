(function() {
  function returnPart8Rows(rowObjArr) {
    var tableRows = [];

    rowObjArr.forEach(function(rowObj) {
      tableRows.push({
        "0": {
          "label": "Year of origin"
        },
        "1": {
          "num": rowObj.num[0]
        },
        "3": {
          "label": "Amount to be applied",
          "labelClass": "center"
        },
        "5": {
          "num": rowObj.num[1]
        },
        "6": {
          "label": rowObj.indicator
        }
      })
    });

    return tableRows
  }

  wpw.tax.global.tableCalculations.t2s425 = {
    "1000": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          colClass: 'std-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          "type": "date"
        },
        {
          "type": "none"
        },
        {
          colClass: 'std-input-col-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      cells: returnPart8Rows([
        {num: ['1010', '1050'], indicator: 'h'},
        {num: ['1011', '1051'], indicator: 'i'},
        {num: ['1012', '1052'], indicator: 'j'},
        {num: ['1013', '1053'], indicator: 'k'},
        {num: ['1014', '1054'], indicator: 'l'},
        {num: ['1015', '1055'], indicator: 'm'},
        {num: ['1016', '1056'], indicator: 'n'},
        {num: ['1017', '1057'], indicator: 'o'},
        {num: ['1018', '1058'], indicator: 'p'},
        {num: ['1019', '1059'], indicator: 'q'}
      ])
    },
    "1100": {
      type: 'table', fixedRows: true, num: '1100', infoTable: true,
      columns: [
        {width: '220px', type: 'none', cellClass: 'alignCenter'},
        {colClass: 'std-input-width', type: 'date'},
        {type: 'none'},
        {colClass: 'std-input-width', type: 'none', cellClass: 'alignCenter'},
        {colClass: 'std-padding-width', type: 'none', cellClass: 'alignCenter'},
        {colClass: 'std-input-width'},
        {type: 'none', colClass: 'std-padding-width'}
      ],
      cells: [
        {
          0: {label: '1st previous tax year-ending on'},
          1: {num: '1110'},
          3: {label: 'Credit to be applied', labelClass: 'center'},
          4: {tn: '911'},
          5: {num: '911', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          6: {type: 'none'}
        },
        {
          0: {label: '2nd previous tax year-ending on '},
          1: {num: '1111'},
          3: {label: 'Credit to be applied', labelClass: 'center'},
          4: {tn: '912'},
          5: {num: '912', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          6: {type: 'none'}
        },
        {
          0: {label: '3rd previous tax year-ending on'},
          1: {num: '1112'},
          3: {label: 'Credit to be applied', labelClass: 'center'},
          4: {tn: '913'},
          5: {num: '913', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          6: {type: 'none'}
        }
      ]
    },
    "1200": {
      hasTotals: true,
      "showNumbering": true,
      "maxLoop": 100000,
      "superHeaders": [
        {
          "header": "Calculation - if you meet all of the above conditions ",
          "colspan": "3"
        }],
      "columns": [
        {
          "header": "<b>AA</b><br>Amount of tax credit in respect of the particular property you acquired",
          "num": "700",
          "tn": "700",
          type: 'text'
        },
        {
          "header": "<b>BB</b><br>Amount calculated at the tax credit rate at the time the property was acquired on either the proceeds of disposition (if sold in an arm's length transaction) or, in any other case, the fair market value of the property",
          "num": "710",
          "tn": "710",
          "width": "40%",
          type: 'text'
        },
        {
          "header": "<b>CC</b><br>Lesser of amount AA and amount BB",
          "num": "720",
          "total": true,
          "totalNum": "721",
          "disabled": true
        }]
    },
    "1400": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          formField: true
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          formField: true
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        }],
      "cells": [
        {
          "2": {
            "label": "Current",
            "labelClass": "center bold",
            "type": "none"
          },
          "5": {
            "label": "Capital",
            "labelClass": "center bold",
            "type": "none"
          }
        },
        {
          "0": {
            "label": "<b>Total current and capital expenditures for SR&ED</b> <br> (from lines 557 and 558 of Form T661)"
          },
          "2": {
            "num": "1410"
          },
          "3": {
            "label": "a"
          },
          "5": {
            "num": "1430"
          },
          "6": {
            "label": "A"
          }
        },
        {
          "0": {
            "label": "Add:",
            "labelClass": "bold"
          },
          "2": {
            "type": "none"
          },
          "5": {
            "type": "none"
          }
        },
        {
          "0": {
            "label": "Qualified expenditures you transferred (from lines 544 and 546 of Form T661)",
            "cellClass": "tabbed"
          },
          "2": {
            "num": "1411"
          },
          "3": {
            "label": "b"
          },
          "5": {
            "num": "1431"
          },
          "6": {
            "label": "B"
          }
        },
        {
          "0": {
            "label": "Government and non-government assistance(from lines 513, 514, 515, 516, 517 and 518 of Form T661)",
            "labelClass": "tabbed"
          },
          "2": {
            "num": "1412"
          },
          "3": {
            "label": "c"
          },
          "5": {
            "num": "1432"
          },
          "6": {
            "label": "C"
          }
        },
        {
          "0": {
            "label": "<b>Subtotals (add </b>amounts a to c and enter the sum in d) (<b>add</b> amounts A to C and enter the sum in D)"
          },
          "2": {
            "num": "1413",
            "inputClass": "doubleUnderline"
          },
          "3": {
            "label": "d"
          },
          "5": {
            "num": "1433",
            "inputClass": "doubleUnderline"
          },
          "6": {
            "label": "D"
          }
        },
        {
          "0": {
            "label": "Deduct:",
            "labelClass": "bold"
          },
          "2": {
            "type": "none"
          },
          "5": {
            "type": "none"
          }
        },
        {
          "0": {
            "label": "<b>Qualified expenditures transferred to you </b>(from lines 508 and 510 of Form T661)",
            'labelCellClass': 'indent'
          },
          "2": {
            "num": "1414"
          },
          "3": {
            "label": "e"
          },
          "5": {
            "num": "1434"
          },
          "6": {
            "label": "E"
          }
        },
        {
          "0": {
            "label": "Government assistance (defined in section 97 of the <i>Income Tax Act </i>(British Columbia).",
            'labelCellClass': 'indent'
          },
          "2": {
            "num": "1415"
          },
          "3": {
            "label": "f"
          },
          "5": {
            "num": "1435"
          },
          "6": {
            "label": "F"
          }
        },
        {
          "0": {
            "label": "Expenditures incurred in the tax year for SR&ED carried on outside British Columbia",
            "labelClass": "tabbed"
          },
          "2": {
            "num": "1416"
          },
          "3": {
            "label": "g"
          },
          "5": {
            "num": "1436"
          },
          "6": {
            "label": "G"
          }
        },
        {
          "0": {
            "label": "Expenditures incurred at a time when the corporation had no permanent establishment in British Columbia",
            "labelClass": "tabbed"
          },
          "2": {
            "num": "1417"
          },
          "3": {
            "label": "h"
          },
          "5": {
            "num": "1437"
          },
          "6": {
            "label": "H"
          }
        },
        {
          "0": {
            "label": "Current expenditures for SR&ED contract paid or payable to, or for the benefit of a person or partnership that is not a taxable supplier in respect of the expenditure ",
            "labelClass": "tabbed"
          },
          "2": {
            "num": "1418"
          },
          "3": {
            "label": "i"
          },
          "5": {
            "num": "1438"
          },
          "6": {
            "label": "I"
          }
        },
        {
          "0": {
            "label": "Expenditures for third-party payments made to entities outside British Columbia ",
            "labelClass": "tabbed"
          },
          "2": {
            "num": "1419"
          },
          "3": {
            "label": "j"
          },
          "5": {
            "num": "1439"
          },
          "6": {
            "label": "J"
          }
        },
        {
          "0": {
            "label": "Prescribed proxy amount on directly engaged salary and wages where the SR&ED was carried on outside British Columbia",
            "labelClass": "tabbed"
          },
          "2": {
            "num": "1420"
          },
          "3": {
            "label": "k"
          },
          "5": {
            "num": "1440"
          },
          "6": {
            "label": "K"
          }
        },
        {
          "0": {
            "label": "Prescribed proxy amount on directly engaged salary and wages at a time when the corporation had no permanent establishment in British Columbia ",
            "labelClass": "tabbed"
          },
          "2": {
            "num": "1421",
            "inputClass": "underline"
          },
          "3": {
            "label": "l"
          },
          "5": {
            "num": "1441",
            "inputClass": "underline"
          },
          "6": {
            "label": "L"
          }
        },
        {
          "0": {
            "label": "<b>Subtotal</b> (<b?add </b>amounts e to l and enter the sum in m)<br>" +
            "(<b>add </b>amounts E to L and enter the sum in M)"
          },
          "2": {
            "num": "1422",
            "inputClass": "underline"
          },
          "3": {
            "label": "m"
          },
          "5": {
            "num": "1442",
            "inputClass": "underline"
          },
          "6": {
            "label": "M"
          }
        },
        {
          "0": {
            "label": "<b>BC qualified expenditures. </b>In Part 3 of Form T666:<br>Report on line 350 the amount n (amount d <b>minus</b> amount m),<br>and report on line 360 the amount N (amount D<b> minus</b> amount M)"
          },
          "2": {
            "num": "1423",
            "inputClass": "doubleUnderline"
          },
          "3": {
            "label": "n"
          },
          "5": {
            "num": "1443",
            "inputClass": "doubleUnderline"
          },
          "6": {
            "label": "N"
          }
        }]
    },
    "1600": {
      "infoTable": true,
      "fixedRows": true,
      "columns": [{
        cellClass: 'alignLeft',
        formField: true
      },
        {
          cellClass: 'alignLeft',
          formField: true,
          type: 'text'
        }],
      "cells": [{
        "0": {
          "label": "<br>Corporation's name",
          "num": "1601",
          "labelClass": "bold",
          type: 'text',
          textAlign: 'left'
        },
        "1": {
          "label": "Partnership's filer identification number<br>(If you are claiming an amount from a partnership)",
          "num": "1602",
          "rf": true,
          "labelClass": "bold"
        }
      }]
    },
    "1650": {
      "infoTable": true,
      "fixedRows": true,
      "dividers": [1,
        2],
      "columns": [
        {
          type: 'custom',
          format: ['{N9}RC{N4}'],
          cellClass: 'alignCenter',
          formField: true
        },
        {
          cellClass: 'alignCenter',
          "type": "date",
          formField: true
        },
        {
          cellClass: 'alignCenter',
          "type": "date",
          formField: true
        }],
      "cells": [{
        "0": {
          "num": "1651",
          "label": "Business number",
          "maxLength": "8",
          "labelClass": "bold"
        },
        "1": {
          "num": "1652",
          "label": "Tax year start",
          "labelClass": "bold"
        },
        "2": {
          "num": "1653",
          "label": "Tax year end",
          "labelClass": "bold"
        }
      }]
    },
    "1700": {
      "infoTable": true,
      "fixedRows": true,
      "dividers": [1,
        2],
      "columns": [{
        cellClass: 'alignLeft',
        formField: true
      },
        {
          cellClass: 'alignLeft',
          formField: true
        },
        {
          cellClass: 'alignLeft',
          formField: true
        }
      ],
      "cells": [{
        "0": {
          "num": "1654",
          "label": "Contact person's name",
          "maxLength": "8",
          "labelClass": "bold",
          type: 'text'
        },
        "1": {
          "num": "1655",
          "label": "Telephone number",
          "labelClass": "bold",
          'telephoneNumber': true, type: 'text'
        },
        "2": {
          "num": "1656",
          "rf": true,
          "label": "Fax number",
          "labelClass": "bold",
          'telephoneNumber': true, type: 'text'
        }
      }]
    },
    '2000': {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {colClass: 'std-padding-width', type: 'none'},
        {
          colClass: 'std-input-width',
          type: 'date',
          disabled: true,
          header: '<b>Year of origin</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '2001',
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '2002',
          totalMessage: ' ',
          header: '<b>Current year contribution</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '2003',
          totalMessage: ' ',
          header: '<b>Transfers</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', totalNum: '2004',
          header: '<b>Amount available to apply</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '2005',
          header: '<b>Applied<b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '2006',
          header: '<b>Balance to carry forward</b>',
          formField: true
        },
        {colClass: 'std-padding-width', type: 'none'}
      ],
      cells: [
        {
          '4': {label: '*'},
          '5': {type: 'none'},
          '7': {type: 'none'},
          '9': {type: 'none'},
          '11': {type: 'none'},
          '13': {type: 'none', label: 'N/A'}
        },
        {
          '5': {type: 'none'},
          '14': {label: '**'}
        },
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {
          '3': {type: 'none'},
          '7': {type: 'none'}
        }
      ]
    }

  }
})();
