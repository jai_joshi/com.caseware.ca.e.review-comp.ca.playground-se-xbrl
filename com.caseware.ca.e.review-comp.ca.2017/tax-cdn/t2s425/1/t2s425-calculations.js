(function() {
  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }
  function runHistoricalTableCalc(calcUtils, tableObj, carryBackAmount, limitOnCreditAmount) {
    var tableNum = tableObj.tableNum;
    var openingBalanceCindex = tableObj.openingBalanceCindex;
    var currentYearCindex = tableObj.currentYearCindex;
    var transferCindex = tableObj.transferCindex;
    var availableCindex = tableObj.availableCindex;
    var appliedCindex = tableObj.appliedCindex;
    var endingBalanceCindex = tableObj.endingBalanceCindex;

    var summaryTable = calcUtils.field(tableNum);
    var numRows = summaryTable.size().rows;

    summaryTable.getRows().forEach(function(row, rowIndex) {
      // if (rowIndex == numRows - 1) {
      //   if (carryBackAmount > 0) {
      //     if (carryBackAmount > row[availableCindex].get()) {
      //       summaryTable.cell(11, 11).assign(row[availableCindex].get());
      //     }
      //     else {
      //       summaryTable.cell(11, 11).assign(carryBackAmount);
      //     }
      //     row[availableCindex].assign(row[openingBalanceCindex].get() + row[currentYearCindex].get() + row[transferCindex].get());
      //     row[endingBalanceCindex].assign(Math.max(row[availableCindex].get() - row[appliedCindex].get(), 0))
      //   }
      //   else {
      //     summaryTable.cell(11, 11).assign(0)
      //   }
      // }
      // else {
        if (rowIndex == 0) {
          // to avoid the first row being calculated to total
          row[currentYearCindex].assign(0);
          row[appliedCindex].assign(0);
          row[transferCindex].assign(0);
          row[availableCindex].assign(0);
          row[endingBalanceCindex].assign(0);
        } else {
          row[appliedCindex].assign(returnAmountApplied(limitOnCreditAmount, row[availableCindex].get()));
          row[availableCindex].assign(row[openingBalanceCindex].get() + row[currentYearCindex].get() + row[transferCindex].get());
          row[endingBalanceCindex].assign(Math.max(row[availableCindex].get() - row[appliedCindex].get(), 0));
          limitOnCreditAmount -= row[appliedCindex].get();
        }
      // }
    });
  }


  wpw.tax.create.calcBlocks('t2s425', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //to get year from tax year history
      var tableArray = [1000, 1100, 2000];
      var taxationYearTable = field('tyh.200');
      tableArray.forEach(function(num) {
        field(num).getRows().forEach(function(row, rowIndex) {
          if (num == 2000) {
            row[1].assign(taxationYearTable.getRow(rowIndex + 9)[6].get())
          }
          else {
            var dateCol = Math.abs(rowIndex - 19);
            row[1].assign(taxationYearTable.cell(dateCol, 6).get());
          }
        })
      })
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //update from rate table
      calcUtils.getGlobalValue('354', 'ratesBc', '488');
      calcUtils.getGlobalValue('358', 'ratesBc', '488');

      //Part 1 calcs
      calcUtils.getGlobalValue('1601', 'CP', '002');
      calcUtils.getGlobalValue('1651', 'CP', 'bn');
      calcUtils.getGlobalValue('1652', 'CP', 'tax_start');
      calcUtils.getGlobalValue('1653', 'CP', 'tax_end');
      if (field('CP.957').get() == 1){
        calcUtils.getGlobalValue('1654', 'CP', '958');
        calcUtils.getGlobalValue('1655', 'CP', '959');
        calcUtils.getGlobalValue('1656', 'CP', '619');
      }
      else {
        field('1654').assign(field('CP.951').get() + ' ' + field('CP.950').get());
        field('1654').source(field('CP.951'));
        calcUtils.getGlobalValue('1655', 'CP', '956');
        calcUtils.getGlobalValue('1656', 'CP', '639')
      }

    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 2 Eligibility
      field('201').assign((field('CP.2270').get() == 1) && (field('CP.085').get() == 1) ? 1 : 2);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 3 calcs
      calcUtils.equals('350', '1423');
      calcUtils.equals('360', '1443');
      field('provinces').assign(field('CP.prov_residence').get());
      var isBCJurisdiction = (field('provinces').getKey('BC') == true);
      if (isBCJurisdiction) {
        calcUtils.sumBucketValues('380', ['350', '360', '370']);
      } else {
        field('380').assign(0)
      }

    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 4 calcs
      calcUtils.getGlobalValue('351', 'T2S31', '410');
      calcUtils.equals('352', '380');
      calcUtils.min('353', ['351', '352']);
      calcUtils.multiply('355', ['353', '354'], 1 / 100);
      //todo: Bucky to update calcs for optimization calcs later
      field('356').assign(field('355').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 5 calcs
      calcUtils.equals('357', '380');
      calcUtils.multiply('359', ['357', '358'], 1 / 100);
      field('362').assign(field('500').get() - field('361').get());
      field('363').assign(Math.max(0, field('362').get()));
      calcUtils.equals('364', '356');
      calcUtils.equals('365', '377');//change from 376 into 377
      calcUtils.sumBucketValues('366', ['364', '365']);
      calcUtils.equals('367', '366');
      field('368').assign(field('359').get() + field('363').get() - field('367').get());
      calcUtils.equals('369', '520');
      field('371').assign(field('2000').get().totals[7]);
      calcUtils.sumBucketValues('372', ['369', '371']);
      calcUtils.equals('373', '372');
      calcUtils.sumBucketValues('374', ['368', '373']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var line240 = form('T2S5').field('240').get();
      var line650 = form('T2S5').field('650').get();
      var line651 = form('T2S5').field('651').get();
      var line653 = form('T2S5').field('653').get();
      var line656 = form('T2S5').field('656').get();

      field('375').assign(line240 - line650 - line651 - line653 - line656);
      calcUtils.min('376', ['375', '374']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 7 calcs
      field('378').assign(field('2000').get().totals[3]);
      field('504').assign(field('2000').cell(0,3).get());
      calcUtils.subtract('520', '378', '504');
      calcUtils.equals('530', '371');
      calcUtils.equals('535', '363');
      calcUtils.equals('540', '359');
      calcUtils.sumBucketValues('541', ['530', '535', '540']);
      calcUtils.equals('542', '541');
      calcUtils.sumBucketValues('543', ['520', '542']);
      calcUtils.equals('580', '377');
      calcUtils.equals('610', '356');
      calcUtils.equals('560', '375');
      calcUtils.equals('561', '1170');
      calcUtils.sumBucketValues('562', ['580', '610', '560', '561']);
      calcUtils.equals('563', '562');
      calcUtils.subtract('620', '543', '563');
    });

    calcUtils.calc(function(calcUtils, field, form) {

      //Part 8 calcs
      calcUtils.sumBucketValues('1070', ['1050', '1051', '1052', '1053', '1054', '1055', '1056', '1057', '1058', '1059']);
      calcUtils.equals('1071', '1070');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 9 calcs
      calcUtils.sumBucketValues('1170', ['911', '912', '913']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 10 calcs
      //table 1200 calcs
      field('1200').getRows().forEach(function(row) {
        var colAA = row[0].get();
        var colBB = row[1].get();
        var min = 0;
        if (isNaN(colAA)) {
          min = colBB || 0;
        } else if (isNaN(colBB)) {
          min = colAA;
        } else {
          min = Math.min(colAA, colBB);
        }
        row[2].assign(min);
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.equals('1301', '721');
      field('1302').assign(Math.max(0, field('361').get() - field('500').get()));
      calcUtils.sumBucketValues('1303', ['1301', '1302']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var formT661 = form('t661');
      field('1410').assign(formT661.field('557').get());
      field('1430').assign(formT661.field('558').get());

      field('1412').assign(
          formT661.field('513').get() +
          formT661.field('515').get() +
          formT661.field('517').get()
      );
      field('1412').source(formT661.field('513'));

      field('1432').assign(
          formT661.field('514').get() +
          formT661.field('516').get() +
          formT661.field('518').get()
      );
      field('1432').source(formT661.field('514'));

      field('1413').assign(Math.min(field('1410').get() + field('1411').get() + field('1412').get(), field('T661.511').get()));
      field('1433').assign(Math.min(field('1430').get() + field('1431').get() + field('1432').get(), field('T661.512').get()));

      calcUtils.sumBucketValues('1422', ['1414', '1415', '1416', '1417', '1418', '1419', '1420', '1421']);
      calcUtils.sumBucketValues('1442', ['1434', '1435', '1436', '1437', '1438', '1439', '1440', '1441']);
      calcUtils.subtract('1423', '1413', '1422');
      calcUtils.subtract('1443', '1433', '1442');
    });
    calcUtils.calc(function (calcUtils, field, form) {
      //summary chart
      var summaryTable = field('2000');
      var carryBackAmount = field('1170').get();
      var limitOnCredit = field('562').get();

      field('1000').getRows().forEach(function (row, rowIndex) {
        var amountCol = Math.abs(rowIndex - 10);
        row[5].assign(summaryTable.cell(amountCol, 11).get())
      });

      runHistoricalTableCalc(
        calcUtils,
        {
          tableNum: '2000',
          openingBalanceCindex: 3,
          currentYearCindex: 5,
          transferCindex: 7,
          availableCindex: 9,
          appliedCindex: 11,
          endingBalanceCindex: 13
        },
        carryBackAmount,
        limitOnCredit
      );
      summaryTable.cell(11, 5).assign(field('367').get())
    })

  });
})();
