(function() {
  wpw.tax.create.diagnostics('t2s425', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('4250001', common.prereq(common.check(['t2s5.659'], 'isNonZero'), common.requireFiled('T2S425')));

    diagUtils.diagnostic('4250002', common.prereq(common.check(['t2s5.674'], 'isNonZero'), common.requireFiled('T2S425')));

    diagUtils.diagnostic('4250004', common.prereq(common.and(
        common.requireFiled('T2S425'),
        common.check(['t2s5.241'], 'isNonZero')),
        function(tools) {
          var table = tools.field('1200');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireAll([row[0], row[1]], 'isNonZero')
          });
        }));
  });
})();
