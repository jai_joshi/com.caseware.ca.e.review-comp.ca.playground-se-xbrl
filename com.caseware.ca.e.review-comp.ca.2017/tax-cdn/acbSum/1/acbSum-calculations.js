(function () {

  var t1135AcbFieldIdArr = [113, 112, 111];

  var shareBondMappingArr = [
    {
      value: '1',
      option: 'Shares',
      acbTableNum: '300',
      dstTableNum: '1100',
      acbCIndices: [0, 9, 10, 11],
      acbSumCIndices: [2, 3, 4, 5],
      commonInfo: [
        {acbSumCIndex: 0, acbFieldId: '101'},//corp name
        {acbSumCIndex: 1, acbFieldId: '102'}//share class
      ],
      t1135CIndices: [6, 7, 8]
    },
    {
      value: '3',
      option: 'Bonds',
      acbTableNum: '510',
      dstTableNum: '1300',
      acbCIndices: [0, 11, 12, 13],
      acbSumCIndices: [1, 2, 3, 4],
      commonInfo: [{acbSumCIndex: 0, acbFieldId: '501'}],//bond issuer
      t1135CIndices: [5, 6, 7]
    }
  ];

  wpw.tax.create.calcBlocks('acbSum', function (calcUtils) {
    //get data from linked repeated forms
    calcUtils.calc(function (calcUtils, field, form) {
      var taxStart = field('CP.tax_start').get();
      var taxEnd = field('CP.tax_end').get();
      var assetTypeMappingArr = [
        {value: '1', tableNum: '100'},
        {value: '2', tableNum: '200'},
        {value: '3', tableNum: '300'},
        {value: '4', tableNum: '400'},
        {value: '5', tableNum: '500'},
        {value: '6', tableNum: '600'}
      ];

      //Add dependencies on all repeat forms
      calcUtils.allRepeatForms('acbTracker').forEach(function (form) {
        form.field('1200');
        form.field('1400');
        form.field('1500');
        form.field('1600');
      });

      //populate table summary
      assetTypeMappingArr.forEach(function (dataObj) {
        //Add dependencies on all repeat forms
        calcUtils.allRepeatForms('acbTracker').forEach(function (form) {
          form.field(dataObj.tableNum);
        });
        calcUtils.filterLinkedTable('acbTracker', dataObj.tableNum, ['100'], function (x) {
          return (x === dataObj.value);
        });
      });

      //populate table current year data for assets other than share and bond
      var assetTableMappingArr = [
        {
          tableNum: '1200',
          assetType: 2
        },
        {
          tableNum: '1400',
          assetType: 4
        },
        {
          tableNum: '1500',
          assetType: 5
        },
        {
          tableNum: '1600',
          assetType: 6
        }
      ];

      for (var i = 0; i < assetTableMappingArr.length; i++) {
        calcUtils.filterLinkedTable('acbTracker', assetTableMappingArr[i].tableNum, ['100', '413'], function (assetType, dispositionDate) {
          return (assetType === assetTableMappingArr[i].assetType && calcUtils.dateCompare.between(dispositionDate, taxStart, taxEnd));
        });
      }

      //get address cell for real estate table
      var table1200 = field('1200');
      var tableLength = field('1200').size().rows;
      for (var i = 0; i < tableLength; i++) {
        var linkedFormId = 'acbtracker-' + table1200.getRowInfo(i).repeatId;
        var linkedAddressValue = {
          address1: form(linkedFormId).field('401').get(),
          address2: form(linkedFormId).field('402').get(),
          areaCode: form(linkedFormId).field('406').get(),
          city: form(linkedFormId).field('404').get(),
          country: form(linkedFormId).field('405').get(),
          province: form(linkedFormId).field('403').get()
        };
        table1200.cell(i, 0).assign(linkedAddressValue);
      }
    });

    //get current year transaction data for share and bond
    calcUtils.calc(function (calcUtils, field, form) {
      var taxStart = field('CP.tax_start').get();
      var taxEnd = field('CP.tax_end').get();

      shareBondMappingArr.forEach(function (assetTypeObj) {
        var acbTrackerFormIds = calcUtils.allRepeatForms('acbtracker').filter(function (form) {
          return form.field('100').get() === assetTypeObj.value;
        });

        var dstTable = field(assetTypeObj.dstTableNum);
        acbTrackerFormIds.forEach(function (form) {
          var acbTable = form.field(assetTypeObj.acbTableNum);
          calcUtils.filterTableRow(dstTable, function (table, rowIndex) {
            // We return false to delete this row.
            return calcUtils.checkLinkExists(table, rowIndex, table.getRowLinks(rowIndex).filter(function (link) {
              return link.type === 'src';
            }));
          });

          calcUtils.fillAndLinkTable(dstTable, acbTable, function (acbTable, rowIndex) {
            return (calcUtils.dateCompare.between(acbTable.cell(rowIndex, 0).get(), taxStart, taxEnd) && acbTable.cell(rowIndex, 1).get() == '2')
          });
        });

        calcUtils.forEachLinkedRow(dstTable, function (dstTable, dstRowInd, linkedTable, linkedRowInd) {
          var mainRow = dstTable.getRow(dstRowInd);
          var linkRow = linkedTable.getRow(linkedRowInd);
          var linkedForm = calcUtils.form(linkedTable.fieldObj.formId);
          //update current year transaction value
          assetTypeObj.acbSumCIndices.forEach(function (cIndex, index) {
            //for face value of bonds
            if (assetTypeObj.value == '3' && index == 1) {
              mainRow[cIndex].assign(
                  linkRow[assetTypeObj.acbCIndices[index]].get() *
                  linkRow[assetTypeObj.acbCIndices[index] - 1].get()
              );
              mainRow[cIndex].source(linkRow[assetTypeObj.acbCIndices[index]]);
            } else {
              mainRow[cIndex].assign(linkRow[assetTypeObj.acbCIndices[index]].get());
              mainRow[cIndex].source(linkRow[assetTypeObj.acbCIndices[index]]);
            }
          });
          //update common info values
          assetTypeObj.commonInfo.forEach(function (dataObj) {
            mainRow[dataObj.acbSumCIndex].assign(linkedForm.field(dataObj.acbFieldId).get());
            mainRow[dataObj.acbSumCIndex].source(linkedForm.field(dataObj.acbFieldId));
          });
          //update t1135 cols value
          assetTypeObj.t1135CIndices.forEach(function (colIndex, index) {
            mainRow[colIndex].assign(linkedForm.field(t1135AcbFieldIdArr[index]).get());
            mainRow[colIndex].source(linkedForm.field(t1135AcbFieldIdArr[index]));
          });
        });
        // The above functions do not guarantee the order of the rows when they get added, so we
        // have to rearrange the rows to ensure the order is correct.
        // var linkRowIdOrder = [];
        // acbTrackerFormIds.forEach(function(linkedForm) {
        //   var acbTable = linkedForm.field(assetTypeObj.acbTableNum);
        //   linkRowIdOrder = linkRowIdOrder.concat(acbTable.getRowIds());
        // });
        // calcUtils.sortTableByLinks(dstTable, linkRowIdOrder);
      });

    });
  });
})();
