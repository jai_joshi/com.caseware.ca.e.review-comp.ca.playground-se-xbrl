(function() {

  var addOther = wpw.utilities.clone(wpw.tax.codes.countryCodes);
  addOther['OTH'] = {value: 'OTH Other'};
  var countryCodesAddOther = new wpw.tax.actions.codeToDropdownOptions(addOther, 'value', 'value');

  var t1135CategoryOptions = [
    {
      value: '1',
      option: 'Funds held outside Canada'
    },
    {
      value: '2',
      option: 'Shares of non-resident corporations (other than foreign affiliates)'
    },
    {
      value: '3',
      option: 'Indebtedness owed by non-resident'
    },
    {
      value: '4',
      option: 'Interests in non-resident trusts'
    },
    {
      value: '5',
      option: 'Real property outside Canada (other than personal use and real estate used in an active business)'
    },
    {
      value: '6',
      option: 'Other property outside Canada'
    }
  ];

  function getAcbTableSummary(assetType, linkedFieldIdArr) {
    if (angular.isUndefined(linkedFieldIdArr))
      linkedFieldIdArr = [];

    var descriptionCols = [];
    var valueCols = [];
    switch (assetType) {
      case 'Share':
        descriptionCols = ['Name of corporation', 'Class', 'Type of shares'];
        valueCols = ['Date of last transaction', 'Total number of shares', 'Average ACB per share', 'Total cost of shares'];
        break;
      case 'Bond':
        descriptionCols = ['Name of bond issuer'];
        valueCols = ['Date of last transaction', 'Total value of property held', 'Total acquisition cost of property held', 'Average ACB for every $100'];
        break;
      case 'Other':
        descriptionCols = ['Description'];
        valueCols = ['Acquisition date', 'ACB of the assets'];
        break;
    }

    var tableCols = [];
    var count = 0;

    descriptionCols.forEach(function(colHeading) {
      var headingObj = {
        header: colHeading,
        type: 'text',
        linkedFieldId: linkedFieldIdArr[count]
      };
      tableCols.push(headingObj);
      count++;
    });

    valueCols.forEach(function(colHeading, colIndex) {
      var colType;

      if (colIndex == 0)
        colType = 'date';

      tableCols.push(
          {
            header: colHeading,
            type: colType,
            linkedFieldId: linkedFieldIdArr[count],
            colClass: 'std-input-width'
          }
      );
      count++;
    });

    return {
      initRows: 0,
      hasTotals: true,
      fixedRows: true,
      columns: tableCols
    }
  }

  function getAcbTableCurrentYear(assetType, linkedFieldIdArr) {
    if (angular.isUndefined(linkedFieldIdArr))
      linkedFieldIdArr = [];

    var descriptionCols = [];
    var valueCols = [];
    var t1135Cols = [
      {header: 'Transfer to T1135', type: 'singleCheckbox'},
      {header: 'T1135 Category', type: 'dropdown', options: t1135CategoryOptions},
      {header: 'T1135 Country', type: 'dropdown', options: countryCodesAddOther}
    ];

    switch (assetType) {
      case 'Share':
        descriptionCols = ['Name of corporation', 'Class'];
        valueCols = ['Date of disposition', 'Number of Shares', 'Proceeds of disposition', 'Adjusted cost base'];
        break;
      case 'RealEstate':
        descriptionCols = ['Address'];
        valueCols = ['Date of acquisition', 'Date of disposition', 'Proceeds of disposition', 'Adjusted cost base', 'Outlays and expenses from disposition'];
        break;
      case 'Bond':
        descriptionCols = ['Name of bond issuer'];
        valueCols = ['Date of disposition', 'Face value of bonds', 'Proceeds of disposition', 'Adjusted cost base'];
        break;
      case 'Other':
        descriptionCols = ['Description'];
        valueCols = ['Date of acquisition', 'Date of disposition', 'Proceeds of disposition', 'Adjusted cost base', 'Outlays and expenses from disposition'];
        break;
    }

    var tableCols = [];
    var count = 0;

    descriptionCols.forEach(function(colHeading) {
      var headingObj = {};

      if (assetType === 'RealEstate') {
        headingObj = {
          header: colHeading,
          type: 'address',
          colClass: 'std-address-width'
        }
      } else if (assetType === 'Share' || assetType === 'Bond') {
        headingObj = {
          header: colHeading,
          type: 'text'
        }
      } else {
        headingObj = {
          header: colHeading,
          type: 'text',
          linkedFieldId: linkedFieldIdArr[count]
        };
        count++;
      }
      tableCols.push(headingObj);
    });

    valueCols.forEach(function(colHeading, colIndex) {
      var colType;
      if (assetType === 'Share' || assetType === 'Bond') {
        if (colIndex == 0)
          colType = 'date';
      } else {
        if (colIndex == 0 || colIndex == 1)
          colType = 'date';
      }

      tableCols.push(
          {
            header: colHeading,
            type: colType,
            linkedFieldId: linkedFieldIdArr[count],
            colClass: 'std-input-width'
          }
      );
      count++;
    });
    //add t1135 cols
    var isOtherAsset = (assetType !== 'Share' && assetType !== 'Bond');

    t1135Cols.forEach(function(colObj) {
      tableCols.push(
          {
            header: colObj.header,
            type: colObj.type,
            options: colObj.options,
            linkedFieldId: isOtherAsset ? linkedFieldIdArr[count] : '',
            colClass: 'std-input-width'
          });
      count++;
    });

    return {
      initRows: 0,
      hasTotals: true,
      fixedRows: true,
      columns: tableCols
    }
  }

  wpw.tax.create.tables('acbSum', {
    '100': getAcbTableSummary('Share', ['101', '102', '103', '1051', '1052', '1053', '1054']),
    '200': getAcbTableSummary('Other', ['407', '411', '425']),
    '300': getAcbTableSummary('Bond', ['501', '1061', '1062', '1063', '1064']),
    '400': getAcbTableSummary('Other', ['407', '411', '425']),
    '500': getAcbTableSummary('Other', ['407', '411', '425']),
    '600': getAcbTableSummary('Other', ['407', '411', '425']),
    '1100': getAcbTableCurrentYear('Share'),
    '1200': getAcbTableCurrentYear('RealEstate', ['411', '413', '414', '425', '415', '113', '112', '111']),
    '1300': getAcbTableCurrentYear('Bond'),
    '1400': getAcbTableCurrentYear('Other', ['407', '411', '413', '414', '425', '415', '113', '112', '111']),
    '1500': getAcbTableCurrentYear('Other', ['407', '411', '413', '414', '425', '415', '113', '112', '111']),
    '1600': getAcbTableCurrentYear('Other', ['407', '411', '413', '414', '425', '415', '113', '112', '111'])
  });
})();
