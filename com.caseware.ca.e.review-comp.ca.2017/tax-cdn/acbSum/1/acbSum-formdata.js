(function () {
  'use strict';

  wpw.tax.create.formData('acbSum', {
    formInfo: {
      abbreviation: 'acbSum',
      title: 'Capital Property Tracker',
      headerImage: 'cw',
      dynamicFormWidth: true,
      category: 'Workcharts'
    },
    sections: [
      {
        header: 'Capital Properties Summary',
        rows: [
          {label: 'Shares', labelClass: 'bold'},
          {type: 'table', num: '100'},
          {labelClass: 'fullLength'},
          {label: 'Real estate', labelClass: 'bold'},
          {type: 'table', num: '200'},
          {labelClass: 'fullLength'},
          {label: 'Bonds', labelClass: 'bold'},
          {type: 'table', num: '300'},
          {labelClass: 'fullLength'},
          {label: 'Other properties', labelClass: 'bold'},
          {type: 'table', num: '400'},
          {labelClass: 'fullLength'},
          {label: 'Personal-use property', labelClass: 'bold'},
          {type: 'table', num: '500'},
          {labelClass: 'fullLength'},
          {label: 'Listed personal property', labelClass: 'bold'},
          {type: 'table', num: '600'},
          {labelClass: 'fullLength'}
        ]
      },
      {
        header: 'Capital Properties Current Year Disposition Summary',
        rows: [
          {label: 'Shares', labelClass: 'bold'},
          {type: 'table', num: '1100'},
          {labelClass: 'fullLength'},
          {label: 'Real estate', labelClass: 'bold'},
          {type: 'table', num: '1200'},
          {labelClass: 'fullLength'},
          {label: 'Bonds', labelClass: 'bold'},
          {type: 'table', num: '1300'},
          {labelClass: 'fullLength'},
          {label: 'Other properties', labelClass: 'bold'},
          {type: 'table', num: '1400'},
          {labelClass: 'fullLength'},
          {label: 'Personal-use property', labelClass: 'bold'},
          {type: 'table', num: '1500'},
          {labelClass: 'fullLength'},
          {label: 'Listed personal property', labelClass: 'bold'},
          {type: 'table', num: '1600'},
          {labelClass: 'fullLength'}
        ]
      }
    ]
  });
})();
