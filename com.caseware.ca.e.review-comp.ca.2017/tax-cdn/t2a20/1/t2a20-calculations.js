(function() {
  wpw.tax.create.calcBlocks('t2a20', function(calcUtils) {
    calcUtils.calc(function (calcUtils, field) {
      //area A
      calcUtils.getGlobalValue('002', 'T2S2', '238-A');
      calcUtils.getGlobalValue('004', 'T2S2', '239-A');
      field('006').assign(Math.max(field('002').get() - field('004').get(), 0));
      calcUtils.getGlobalValue('008', 'T2S2', '250-A');
      calcUtils.getGlobalValue('010', 'T2S2', '210');
      field('012').assign(field('008').get() + field('010').get());
      calcUtils.getGlobalValue('013', 'T2S2', '255-A');
      field('014').assign(Math.max(field('006').get() + field('012').get() - field('013').get(), 0 ));
      field('016').assign(Math.min(field('014').get(), field('048').get(), field('T2A12.054').get()));
      field('018').assign(Math.max(field('014').get() - field('016').get(), 0));
    });
    calcUtils.calc(function (calcUtils, field) {
      //area B
      field('030').assign(field('T2A12.054').get() * field('ratesAb.001').get() / 100);
      field('032').assign(field('T2S2.225').get());
      field('034').assign(field('T2S2.227').get());
      field('036').assign(field('T2S2.230').get());
      field('038').assign(field('T2S2.231').get());
      field('040').assign(field('T2S2.232').get());
      field('042').assign(field('038').get() + field('040').get());
      field('044').assign(Math.min(field('036').get(), field('040').get()));
      field('046').assign((field('032').get() + field('034').get() + field('044').get())* field('ratesAb.002').get() /100);
      field('048').assign(field('030').get() + field('046').get());
    });
    calcUtils.calc(function (calcUtils, field) {
      //area C
      field('062').assign(field('T2S2.6051').get().totals[12] + field('T2S2.6061').get().totals[12]);
      calcUtils.getGlobalValue('064', 'T2S2', '439-A');
      field('066').assign(field('062').get() -  field('064').get());
      field('072').assign(field('068').get() + field('070').get());
      calcUtils.getGlobalValue('073', 'T2S2', '455-A');
      field('074').assign(field('066').get() + field('072').get() - field('073').get());
      calcUtils.getGlobalValue('076', 'T2S2', '460-A');
      field('078').assign(Math.max(field('074').get() - field('076').get(), 0))
    });
    calcUtils.calc(function (calcUtils, field) {
      //to get year for table summary
      var taxYearSummaryTable = field('tyh.200');
        field('100').getRows().forEach(function(row, rowIndex) {
          row[1].assign(taxYearSummaryTable.getRow(rowIndex + 16)[6].get());
          row[3].assign(field('T2S2.6011').getRow(rowIndex + 2)[12].get());
          row[7].assign(field('T2S2.6031').getRow(rowIndex + 2)[12].get());
          row[9].assign(field('T2S2.6051').getRow(rowIndex + 7)[12].get());
          row[11].assign(field('T2S2.6061').getRow(rowIndex + 2)[12].get());
        })
    })

  });
})();
