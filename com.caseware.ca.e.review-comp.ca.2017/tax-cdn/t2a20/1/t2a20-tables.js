(function() {
  wpw.tax.create.tables('t2a20', {
    '100':{
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {
          type: 'none'
        },
        {
          type: 'date',
          colClass: 'std-input-width',
          header: 'Year of origin <br>YYYY/MM/DD',
          tn: '090',
          formField:true
        },
        {
          type: 'none',
          colClass: 'std-spacing-width'
        },
        {
          colClass: 'std-input-width',
          header: 'Charitable donations available for carryforward',
          tn: '092',
          formField:true,
          total: true,
          totalMessage: 'Totals:'
        },
        {
          type: 'none',
          colClass: 'std-spacing-width'
        },
        {
          colClass: 'std-input-width',
          header: 'Gifts to Canada, a province, or territory available for carryforward',
          tn: '094',
          total: true,
          totalMessage: ' ',
          formField:true
        },
        {
          type: 'none',
          colClass: 'std-spacing-width'
        },
        {
          colClass: 'std-input-width',
          header: 'Gifts of certified cultural property available for carryforward',
          tn: '096',
          total: true,
          totalMessage: ' ',
          formField:true
        },
        {
          type: 'none',
          colClass: 'std-spacing-width'
        },
        {
          colClass: 'std-input-width',
          header: 'Gifts of certified ecological sensitive land available for carryforward',
          tn: '098',
          total: true,
          totalMessage: ' ',
          formField:true
        },
        {
          type: 'none',
          colClass: 'std-spacing-width'
        },
        {
          colClass: 'std-input-width',
          header: 'Additional deduction for gifts of medicine available for carryforward',
          tn: '100',
          total: true,
          totalMessage: ' ',
          formField:true
        },
        {
          type: 'none'
        }
      ],
      cells: [
        {},
        {},
        {},
        {},
        {}
      ]
    }
  })
})();
