(function() {
  wpw.tax.create.formData('t2a20',{
    formInfo: {
      abbreviation: 'AT20',
      title: 'Alberta Charitable Donations and Gifts',
      schedule: 'Schedule 20',
      // headerImage: 'canada-alberta',
      showCorpInfo: true,
      formFooterNum: 'AT20 (Jan-15)',
      category: 'Alberta Tax Forms',
      dynamicWidth: true,
      hideProtectedB: true
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            labelClass: 'fullLength'
          },
          {
            label: '<b>This schedule is required if the opening balance or the claim for Alberta purposes' +
            ' differs from that for federal purposes</b>' +
            '<br>Report all monetary amounts in dollars; DO NOT include cents.' +
            '<br><b>If the corporation is reporting nil net income or a loss for the year, donations cannot be claimed</b>'
          }
        ]
      },
      {
        header: 'AREA A - CHARITABLE DONATIONS',
        rows: [
          {
            'label': 'Charitable donations at the end of the preceding taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '002',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '002'
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Deduct: donations expired after five taxation years',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '004'
                },
                'input': {
                  'num': '004',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ]
          },
          {
            'label': 'Charitable donations at the beginning of the taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '006',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '006'
                }
              }
            ]
          },
          {
            label: 'Add:'
          },
          {
            'label': 'Donations transferred on amalgamation or wind-up of subsidiary',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '008',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '008'
                }
              },
              null
            ]
          },
          {
            'label': 'Total current year charitable donations made',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '010',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '010'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal: Lines 008 + 010',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '012',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '012'
                }
              }
            ]
          },
          {
            'label': 'Deduct: Adjustment for an acquisition of control (for donations made after March 22, 2004)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '013',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '013'
                }
              }
            ]
          },
          {
            'label': 'Total donations available: lines 006 + 012 - 013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '014',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '014'
                }
              }
            ]
          },
          {
            label: 'Amount applied against taxable income',
            labelClass: 'bold'
          },
          {
            label: 'Not exceeding the lesser of:'
          },
          {
            'label': 'total donations available (line 014) and maximum deduction calculation (line 048),<br>' +
            ' not exceeding the Alberta net income for tax purposes (schedule 12, line 054)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '016',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '016'
                }
              }
            ]
          },
          {
            label: 'Carry forward the amount at line 016 to Schedule 12, line 056',
            labelClass: 'bold'
          },
          {
            'label': 'Charitable donations closing balance: lines 014 - 016',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '018',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '018'
                }
              }
            ]
          }
        ]
      },
      {
        header: 'AREA B - Maximum deduction calculation for donations for taxation years starting after 1996',
        rows: [
          {
            'label': 'Alberta net income for tax purposes*: Schedule 12, line 054 X 75%',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '030',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '030'
                }
              }
            ]
          },
          {
            'label': 'Taxable capital gains arising in respect of gifts of capital property',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '032',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '032'
                }
              }
            ]
          },
          {
            'label': 'Taxable capital gain in respect of deemed gifts of non-qualifying securities per ITA subsection 40(1.01)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '034',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '034'
                }
              }
            ]
          },
          {
            'label': 'The amount of the recapture of capital cost allowance in respect of charitable gifts',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '036',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '036'
                }
              }
            ]
          },
          {
            'label': 'Proceeds of dispositions less outlays and expenses',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '038',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '038'
                }
              },
              null
            ]
          },
          {
            'label': 'The capital cost',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '040',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '040'
                }
              },
              null
            ]
          },
          {
            'label': 'The lesser of amounts on lines 038 and 040',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '042',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '042'
                }
              }
            ]
          },
          {
            'label': 'The lesser of amounts on lines 036 and 042',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '044',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '044'
                }
              }
            ]
          },
          {
            'label': 'Calculate: (lines 032 + 034 + 044) X 25%',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '046',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '046'
                }
              }
            ]
          },
          {
            'label': 'Maximum deduction allowable: lines 030 + 046',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '048',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '048'
                }
              }
            ]
          },
          {
            label: '<i>* For credit unions this amount is before the deduction of payments pursuant' +
            ' to allocations in proportion to borrowing and bonus interest.</i>'
          }
        ]
      },
      {
        header: 'GIFTS to Canada or a province, gifts of certified cultural property and gifts of certified ' +
        'ecologically sensitive land <br>(Report the combined totals for all three categories of gifts)',
        rows: [
          {
            'label': 'Gifts balance at the end of the preceding taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '062',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '062'
                }
              }
            ]
          },
          {
            'label': 'Deduct: gifts expired after five taxation years, or after ten taxation years for ' +
            'gifts of certified ecological sensitive land made after February 10, 2014',
            'layout': 'alignInput',
            labelCellClass: 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '064',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '064'
                }
              }
            ]
          },
          {
            'label': 'Gifts balance at the beginning of the taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '066',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '066'
                }
              }
            ]
          },
          {
            label: 'Add:'
          },
          {
            'label': 'Gifts transferred on amalgamation or wind-up of a subsidiary',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '068',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '068'
                }
              },
              null
            ]
          },
          {
            'label': 'Total current year gifts',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '070',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '070'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal: Lines 068 + 070',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '072',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '072'
                }
              }
            ]
          },
          {
            'label': 'Deduct: Adjustment for an acquisition of control (for donations made after March 22, 2004)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '073',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '073'
                }
              }
            ]
          },
          {
            'label': 'Total gifts available: lines 066 + 072 - 073',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '074',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '074'
                }
              }
            ]
          },
          {
            'label': 'Deduct: Amount applied against taxable income',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '076',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '076'
                }
              }
            ]
          },
          {
            label: 'Carry forward the amount at line 076 to Schedule 12, line 058',
            labelClass: 'bold'
          },
          {
            'label': 'Gifts closing balance: lines 074 - 076',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '078',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '078'
                }
              }
            ]
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            label: '<br>If the corporation elects to differ its Alberta claim for additional' +
            ' deduction for gifts of medicine (line 660 on Federal T2 Schedule 2), please enter' +
            ' the Alberta amount on Schedule 12, line 40, the federal amount on Schedule 12, line 41' +
            ' and provide the explanation of the difference on Alberta Schedule 12, line 48.',
            labelClass: 'bold'
          }
        ]
      },
      {
        header: 'Amount available for carryforward by year of origin',
        rows: [
          {
            label: 'You can complete this part to show all the donations and gifts from previous ' +
            'years available for carryforward by year of origin. <br>' +
            'This will help you determine the amount that could expire in the following years.'
          },
          {
            labelClass: 'fullLength'
          },
          {
            type: 'table',
            num: '100'
          }
        ]
      }
    ]
  });
})();
