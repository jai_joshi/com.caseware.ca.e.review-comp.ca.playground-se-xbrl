(function() {
  wpw.tax.global.tableCalculations.t2s504 = {
    '500': {
      'fixedRows': true,
      'infoTable': true,
      'width': 'calc((100% - 105px))',
      'columns': [
        {
          'width': '414px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '30px',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          disabled: true
        },
        {
          'width': '30px',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          disabled: true
        },
        {
          'width': '290px',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Ontario taxable income *',
            cellClass: 'singleUnderline alignCenter'
          },
          '2': {
            'num': '501',
            cellClass: 'singleUnderline',
            decimals: 2
          },
          '3': {
            'label': ' = ',
            labelClass: 'center'
          },
          '4': {
            'num': '503',
            decimals: 5
          },
          '5':{
            label: '%'
          }
        },
        {
          '0': {
            'label': 'Taxable income **',
            'labelClass': 'center'
          },
          '2': {
            'num': '502'
          },
          '4': {type: 'none'}
        }
      ]
    },
    '600': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          'type': 'none',
          colClass: 'half-col-width'
        },
        {
          colClass: 'std-input-width',
          formField:true
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          'alignText': 'right'
        },
        {
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          'type': 'none'
        },
        {
          colClass: 'half-col-width',
          formField:true
        },
        {
          colClass: 'std-input-width',
          'type': 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'half-col-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Amount h'
          },
          '1': {
            'num': '133'
          },
          '2': {
            'label': ' x ',
            'labelClass': 'center'
          },
          '3': {
            'label': 'Ontario basic rate of tax *',
            'labelClass': 'center'
          },
          '5': {
            'num': '134',
            decimals: 1,
            'labelClass': 'center',
            filters: 'decimals 2'
          },
          '6': {
            'label': '%    x OAF **',
            'labelClass': 'center'
          },
          '7': {
            'num': '135',
            decimals: 5,
            formField:true
          },
          '8': {
            'label': ' = '
          },
          '9': {
            tn: '130'
          },
          '10': {
            'num': '130', 'validate': {'or': [{'matches': '^[.\\d]{1,13}$'}, {'check': 'isEmpty'}]},
            formField:true
          },
          '11': {
            label: 'i'
          }
        }]
    },
    '700': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          colClass: 'std-input-col-width'
        },
        {
          colClass: 'std-input-width',
          formField:true
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          'type': 'none'
        },
        {
          colClass: 'small-input-width',
          formField:true
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter',
          formField:true
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '1': {
            label: 'Amount i'
          },
          '2': {
            'num': '136'
          },
          '3': {
            'label': ' x '
          },
          '4': {
            'label': 'Number of days in the tax year before April 24, 2015',
            cellClass: 'singleUnderline alignCenter'
          },
          '6': {
            'num': '137',
            cellClass: 'singleUnderline'
          },
          '7': {
            'label': ' = '
          },
          '8': {
            'num': '139'
          },
          '9': {
            'label': 'E',
            type: 'none'
          }
        },
        {
          '1': {
            'type': 'none'
          },
          '2': {
            type: 'none'
          },
          '4': {
            'label': 'Number of days  in the tax year',
            'labelClass': 'center'
          },
          '6': {
            'num': '138'
          },
          '7': {
            'Type': 'none'
          },
          '8': {
            type: 'none'
          },
          '9': {
            'type': 'none'
          }
        }
      ]
    },
    '800': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-col-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          'type': 'none'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '1': {
            label: 'Amount D'
          },
          '2': {
            'num': '140',
            formField: true
          },
          '3': {
            'label': ' x '
          },
          '4': {
            'label': 'Number of days in the tax year before April 24, 2015',
            cellClass: 'singleUnderline'
          },
          '6': {
            'num': '141',
            cellClass: 'singleUnderline',
            formField: true
          },
          '7': {
            'label': ' = '
          },
          '8': {
            'num': '143',
            formField: true
          },
          '9': {
            'label': 'j',
            type: 'none'
          }
        },
        {
          '1': {
            'type': 'none'
          },
          '2': {
            type: 'none'
          },
          '4': {
            'label': 'Number of days  in the tax year',
            'labelClass': 'center'
          },
          '6': {
            'num': '142',
            formField: true
          },
          '7': {
            'Type': 'none'
          },
          '8': {
            type: 'none'
          },
          '9': {
            'type': 'none'
          }
        }
      ]
    },
    '900': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          'width': '80px',
          'type': 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'half-col-width'
        },
        {
          'width': '20px',
          'type': 'none'
        },
        {
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          'type': 'none'
        },
        {
          colClass: 'small-input-width'
        },
        {
          'width': '20px',
          'type': 'none'
        },
        {
          'width': '130px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          'width': '100px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'half-col-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Amount K'
          },
          '1': {
            'num': '154'
          },
          '2': {
            'label': ' x '
          },
          '3': {
            'label': 'Number of days in the tax year before April 24, 2015',
            cellClass: 'singleUnderline alignCenter'
          },
          '5': {
            'num': '155',
            cellClass: 'singleUnderline'
          },
          '6': {
            label: 'x'
          },
          '7': {
            'label': 'Ontario basic rate of tax (from Part 1 of Schedule 500)'
          },
          '8': {
            'num': '157',
            decimals: 1
          },
          '9': {
            'label': '% x OAF **'
          },
          '10': {
            'num': '158',
            decimals: 5
          },
          '11': {
            'label': ' = '
          },
          '12': {
            'num': '159'
          },
          '13': {
            'label': 'L'
          }
        },
        {
          '1': {
            type: 'none'
          },
          '3': {
            'label': 'Number of days in the tax year'
          },
          '5': {num: '156'},
          '8': {
            type: 'none'
          },
          '10': {
            type: 'none'
          },
          '12': {
            type: 'none'
          }
        }
      ]
    },
    '1001': {
      'fixedRows': true,
      'infoTable': true,
      hasTotals: true,
      'columns': [
        {
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true,
          header: 'Year of origin'
        },
        {
          colClass: 'std-spacing-width',
          'type': 'none'
        },
        {
          'total': true,
          'totalNum': '1002',
          'totalMessage': 'Totals : ',
          header: 'Opening balance'
        },
        {
          colClass: 'std-spacing-width',
          'type': 'none'
        },
        {
          'total': true,
          'totalNum': '1003',
          'totalMessage': ' ',
          header: 'Transfers'
        },
        {
          'type': 'none',
          colClass: 'std-spacing-width'
        },
        {
          'disabled': true,
          'total': true,
          'totalNum': '1004',
          'totalMessage': ' ',
          header: 'Applied'
        },
        {
          colClass: 'std-spacing-width',
          'type': 'none'
        },
        {
          'disabled': true,
          'total': true,
          'totalNum': '1005',
          'totalMessage': ' ',
          header: 'Balance to carry forward'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '4': {
            'type': 'none',
            label: 'N/A'
          },
          '6': {
            'type': 'none',
            label: 'N/A'
          },
          '8': {
            'type': 'none',
            label: 'N/A'
          }
        },
        {},
        {},
        {},
        {},
        {
          '2': {
            cellClass: 'singleUnderline'
          },
          '4': {
            cellClass: 'singleUnderline'
          },
          '6': {
            cellClass: 'singleUnderline'
          },
          '8': {
            cellClass: 'singleUnderline'
          }
        }
      ]
    }
  }
})();