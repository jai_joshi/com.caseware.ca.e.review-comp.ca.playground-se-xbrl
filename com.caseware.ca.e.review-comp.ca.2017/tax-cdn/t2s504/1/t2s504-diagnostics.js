(function() {
  wpw.tax.create.diagnostics('t2s504', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('5040001', common.prereq(common.check(['t2s5.274'], 'isNonZero'), common.requireFiled('T2S504')));

    diagUtils.diagnostic('5040002', common.prereq(common.and(
        common.requireFiled('T2S504'),
        common.check(['t2s5.274'], 'isNonZero')),
        common.check('100', 'isNonZero')));

    diagUtils.diagnostic('5040003', common.prereq(common.check(['t2s5.404'], 'isNonZero'), common.requireFiled('T2S504')));

    diagUtils.diagnostic('5040004', common.prereq(common.and(
        common.requireFiled('T2S504'),
        common.check(['t2s5.404'], 'isNonZero')),
        common.check(['105', '115', '120', '130'], 'isNonZero', true)));

    diagUtils.diagnostic('5040005', common.prereq(common.and(
        common.requireFiled('T2S504'),
        common.check(['t2s504.130'], 'isNonZero')),
        common.check(['105'], 'isNonZero')));

  });
})();
