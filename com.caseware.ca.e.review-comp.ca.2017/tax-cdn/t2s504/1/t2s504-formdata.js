(function() {
  'use strict';

  wpw.tax.global.formData.t2s504 = {
    'formInfo': {
      'abbreviation': 'T2S504',
      'title': 'Ontario Resource Tax Credit and Ontario Additional Tax re Crown Royalties',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      //'subTitle': '(2015 and later tax years)',
      'schedule': 'Schedule 504',
      code: 'Code 1502',
      formFooterNum: 'T2 SCH 504 E (17)',
      'showCorpInfo': true,
      headerImage: 'canada-federal',
      'description': [
        {
          'type': 'list',
          'items': [
            'Use this schedule to calculate an Ontario resource tax credit, an Ontario additional tax re Crown ' +
            'royalties under sections 37 and 36 of the <i>Taxation Act, 2007</i> (Ontario) as well as to ' +
            'claim your unused resource tax credit. <br> <b>Note</b>: For tax years starting after April 22, 2015, ' +
            'the Ontario resource tax credit and the Ontario additional tax re Crown royalties are eliminated. ' +
            'The credit and the tax are calculated on a pro-rated basis for tax years that include April 23, 2015. ' +
            'Effective that date, Ontario will provide a deduction for royalties and mining taxes paid.',
            'The Ontario basic rate of tax referred to in Parts 3 and 4 is calculated in Part 1 of Schedule 500,' +
            '<i> Ontario Corporation Tax Calculation</i>.',
            'File this schedule with your <i>T2 Corporation Income Tax Return</i>.'
          ]
        }
      ],
      category: 'Ontario Forms'
    },
    'sections': [
      {
        'header': 'Part 1 - Notional resource allowance and adjusted Crown royalties',
        'rows': [
          {
            'label': '<b>Notional resource allowance</b> for the tax year as determined in subsection 7(3) of Ontario Regulation 37/09 under the <i>Taxation Act, 2007</i> (Ontario)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            label: '<b>Adjusted Crown royalties</b> for the tax year. Complete Part 5',
            labelClass: 'fullLength'
          },
          {
            'label': 'Amount T from Part 5',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '100',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              }
            ],
            'indicator': 'B'
          }
        ]
      },
      {
        'header': 'Part 2 - Ontario allocation factor (OAF)',
        'rows': [
          {
            'label': 'If the provincial or territorial jurisdiction entered on line 750 of the T2 return is "Ontario," enter "1" on line C.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the provincial or territorial jurisdiction entered on line 750 of the T2 return is "multiple," complete the following calculation and enter the result on line C:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '500'
          },
          {
            'label': 'Ontario allocation factor',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '106',
                  'decimals': 5
                }
              }
            ],
            'indicator': '% C'
          },
          {
            'label': '* Enter the taxable income allocated to Ontario from column F in Part 1 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>. If taxable income is nil, calculate the amount from column F as if taxable income were $1,000.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** Enter taxable income from line 360 or amount Z from the T2 return, whichever applies. If taxable income is nil, enter $1,000.',
            'labelClass': 'fulLLength'
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Summary of Ontario resource tax credit',
        'rows': [
          {
            'type': 'table',
            'num': '1001'
          }
        ]
      },
      {
        'header': 'Part 3 - Ontario resource tax credit',
        'rows': [
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Complete Part 4 to calculate the Ontario additional tax re Crown royalties (included ' +
            'in amount e). You can carry forward unexpired unused Ontario resource tax credits in the first five ' +
            'tax years beginning after April 23, 2015.',
            'labelClass': 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Ontario resource tax credit balance at the end of the previous tax year. Enter the amount ' +
            'from line 200 of Schedule 504 from the previous tax year, if applicable',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '115',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '115'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'Ontario resource tax credit balance transferred on an amalgamation or the windup of a' +
            ' subsidiary if subsection 87(1) or 88(1) of the federal <i>Income Tax Act</i> applies to the ' +
            'amalgamation or wind-up',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '120',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount a <b>plus</b> amount b)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '121'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'Credit expired (after the first five tax years beginning after April 23, 2015)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '125',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '125'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Ontario resource tax credit balance at the beginning of the tax year (amount c <b>minus</b> amount d)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '126'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '127'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Ontario corporate income tax payable before tax credits (amount C6 from Part 2 of Schedule 5' +
            ' <i>Tax Calculation Supplementary - Corporations)</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '128'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For tax years that begin before April 24, 2015',
            'labelClass': 'center fullLength bold'
          },
          {
            'label': 'Current-year credit earned, before the transitional calculation:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Notional resource allowance for the tax year (amount A from Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '129'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'label': 'Adjusted Crown royalties for the tax year (amount B from Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '131'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'g'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount f <b>minus</b> amount g) (if negative, enter "0")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '132'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'h'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            'type': 'table',
            'num': '600'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: 'Current-year credit:',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '700'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '800'
          },
          {
            'label': 'Total Ontario resource tax credit available for the current year(amount E <b>plus</b> amount j)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '145'
                }
              }
            ],
            indicator: 'k'
          },
          {
            'label' :'<b>Ontario resource tax credit</b> claimed in the current year (amount e or amount k,' +
            ' whichever is less)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '146'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Enter amount F on line 404  in Part 2 of Schedule 5 ' +
            '<i>Tax Calculation Supplementary - Corporations</i>',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Balance at the end of the tax year (amount D <b>plus</b> amount E <b>minus</b> amount F)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '147'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For tax years that begin after April 23, 2015',
            'labelClass': 'center fullLength bold'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Ontario resource tax credit</b> claimed in the current year (amount D or amount e, ' +
            'whichever is less)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '148'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            label: 'Enter amount H on line 404 in Part 2 of Schedule 5 <i>Tax Calculation Supplementary ' +
            '- Corporations</i>.',
            labelClass: 'fullLength'
          },
          {
            'label': 'Balance at the end of the tax year (amount D <b>minus</b> amount H)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '149'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Ontario resource tax credit balance at the end of the tax year (amount G or amount I, whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            labelClass: 'fullLength'
          },
          {
            label: '* Enter the rate used in the calculation of amount L from Part 4.',
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength',
            label: '** Enter amount C from Part 2.'
          },
          {
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Part 4 - Ontario additional tax re Crown royalties',
        'rows': [
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Adjusted Crown royalties for the tax year (amount B from Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '150'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'l'
                }
              }
            ]
          },
          {
            'label': 'Notional resource allowance for the tax year (amount A from Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '151'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'm'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount l <b>minus</b> amount m) (if negative, enter "0")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '152'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '153'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': 'Ontario additional tax re Crown royalties:',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '900'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter amount L on line 274 in Part 2 of Schedule 5 ' +
            '<i>Tax Calculation Supplementary Corporations</i>.',
            'labelClass': 'fulLLength'
          },
          {
            'label': '<b>Note</b>: For tax years starting after April 22, 2015, the Ontario additional tax re Crown royalties is eliminated.',
            'labelClass': 'fullLength'
          },
          {
            'label': '* Enter amount C from Part 2.',
            'labelClass': 'fullLength'
          },
          {
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 5 - Adjusted Crown royalties for the tax year',
        'rows': [
          {
            'label': 'Add the following amounts as defined under subsection 36(2) of the <i>Taxation Act, 2007</i> (Ontario):',
            'labelClass': 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Provincial or territorial tax on income from mining operations, excluding amounts ' +
            'prescribed in subsection 108.1(2) of Ontario Regulation 183, as it applies under subsection 11.0.1(2)' +
            ' of the <i>Corporations Tax Act</i> (Ontario)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '160'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'label': 'A Crown charge *, paid or payable to the Crown **, or receivable by the Crown under ' +
            'subsections 11.0.1(3) and (5) of the <i>Corporations Tax Act</i> (Ontario)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '161'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'label': 'Adjustment to income where an operator, at any time in a tax year, disposes of (or acquires) ' +
            'production from a Canadian natural accumulation of petroleum or natural gas, an oil or gas well,' +
            ' or a mineral resource at less than or more than the fair market value under subsection 26(4.1) of the' +
            '<i> Corporations Tax Act</i> (Ontario)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '162'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'label': 'Payments made under contract to reimburse Crown charges * under paragraph 1 of ' +
            'subsection 26(7) of the <i>Corporations Tax Act</i> (Ontario)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '163'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'label': 'The corporation\'s share of amounts M and N incurred by a partnership of which the corporation ' +
            'is a majority interest partner under subsection 31(1.2) of the <i>Corporations Tax Act</i>(Ontario)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '164'
                }
              }
            ],
            'indicator': 'Q'
          },
          {
            'label': 'Subtotal (total of amounts M to Q)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '165'
                }
              }
            ],
            'indicator': 'R'
          },
          {
            'label': 'Amounts received under contract as reimbursements of Crown charges * under paragraph' +
            ' 2 of subsection 26(7) of the <i>Corporations Tax Act</i> (Ontario)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '166'
                }
              }
            ],
            'indicator': 'S'
          },
          {
            'label': '<b>Adjusted Crown royalties for the tax year</b> (amount R <b>minus</b> amount S)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '167'
                }
              }
            ],
            'indicator': 'T'
          },
          {
            'label': 'Enter amount T to amount B from Part 1.',
            'labelClass': 'fullLength'
          },
          {
            'label': '* "Crown charge" is an amount to which the Crown is entitled and that is, or can reasonably ' +
            'be considered to be a royalty, tax, lease rental, or bonus relating to the acquisition, development,' +
            ' or ownership of a Canadian resource property, or relating to the production in Canada from certain' +
            ' resource properties. Crown charge does not include a municipal or school tax or amounts prescribed' +
            ' in section 108.2 of Ontario Regulation 183 under the <i>Corporations Tax Act</i> (Ontario).',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '** "Crown" refers to Her Majesty in right of Canada or of a province, an agent of Her ' +
            'Majesty in right of Canada or of a province, or a corporation, commission, or association that is' +
            ' controlled by Her Majesty in right of Canada or of a province, or by an agent of Her Majesty ' +
            'in right of Canada or of a province.',
            'labelClass': 'tabbed fullLength'
          }
        ]
      }
    ]
  };
})();