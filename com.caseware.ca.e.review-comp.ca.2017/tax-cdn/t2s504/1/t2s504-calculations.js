(function() {

  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  wpw.tax.create.calcBlocks('t2s504', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //Part 1
      /// TODO: calcUtils.getGlobalValue('105', 't2s51', ''); when schedule 51 is updated////
      calcUtils.equals('100', '167');

      if (field('T2J.750').get() == 'MJ') {
        field('501').assign(field('T2S5.998').cell(7, 6).get());
        field('501').source(field('T2S5.998').cell(7, 6));
        if (field('T2J.300').get() <= 0) {
          field('502').assign(1000)
        }
        else {
          calcUtils.getGlobalValue('502', 'T2J', '371');
        }
        field('503').assign(
            field('501').get() /
            field('502').get()
        );
        field('106').assign(field('503').get());
      }
      else if (field('T2J.750').get() == 'ON') {
        field('106').assign(1);
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //for workchart year
      var summaryTable = field('tyh.200');
      field('1001').getRows().forEach(function(row, rowIndex) {
        row[0].assign(summaryTable.getRow(rowIndex + 15)[6].get())
      });

      //Part 3
      var tableSummary = field('1001');
      field('115').assign(tableSummary.total(2).get());
      field('120').assign(tableSummary.total(4).get());
      field('125').assign(tableSummary.cell(0, 2).get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.sumBucketValues('121', ['115', '120']);
      calcUtils.subtract('126', '121', '125');
      calcUtils.equals('127', '126');
      calcUtils.getGlobalValue('128', 'T2S5', '299');

      var apr23 = wpw.tax.date(2015, 4, 23);
      var numberOfDays = wpw.tax.actions.calculateDaysDifference(field('CP.tax_start').get(), apr23);
      if (numberOfDays > field('CP.Days_Fiscal_Period').get()) {
        numberOfDays = field('CP.Days_Fiscal_Period').get();
      }

      var taxStartDate = field('CP.tax_start').get();
      if (taxStartDate) {
        if (calcUtils.dateCompare.lessThan(taxStartDate, apr23)) {
          calcUtils.equals('129', '105');
          calcUtils.equals('131', '100');
          calcUtils.subtract('132', '129', '131');
          calcUtils.equals('133', '132');
          calcUtils.getGlobalValue('134', 't2s500', '175');
          calcUtils.equals('135', '106');
          calcUtils.multiply('130', ['133', '135'], 0.115);
          calcUtils.equals('136', '130');
          field('137').assign(numberOfDays);
          calcUtils.getGlobalValue('138', 'cp', 'Days_Fiscal_Period');
          calcUtils.divide('139', '137', '138', '136');
          calcUtils.equals('140', '127');
          field('141').assign(numberOfDays);
          calcUtils.equals('142', '138');
          calcUtils.divide('143', '141', '142', '140');
          calcUtils.sumBucketValues('145', ['139', '143']);
          calcUtils.min('146', ['145', '128']);
          field('147').assign(field('127').get() + field('139').get() - field('146').get());
          calcUtils.removeValue([148, 149], true);
        }
        else {
          calcUtils.min('148', ['127', '128']);
          calcUtils.subtract('149', '127', '148');
          var beforeApril24 = ['129', '131', '132', '133', '134', '135', '130', '136', '137', '139', '140', '141', '143', '145', '146', '147'];
          calcUtils.removeValue(beforeApril24, true)
        }
      }
      field('155').assign(numberOfDays);//part4 calcs
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var tableSummary = field('1001');
      //for 200
      calcUtils.getApplicableValue('200', ['147', '149']);

      //Calculate amount applied and balance carry forward
      var limitOnCredit = field('148').get();
      for (var i = 1; i < tableSummary.getRows().length; i++) {
        var row = tableSummary.getRow(i);
        var availableAmount = row[2].get() + row[4].get();
        row[6].assign(returnAmountApplied(limitOnCredit, availableAmount));
        row[8].assign(availableAmount - row[6].get());
        limitOnCredit -= row[6].get()
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 4
      calcUtils.equals('150', '100');
      calcUtils.equals('151', '105');
      calcUtils.subtract('152', '150', '151');
      calcUtils.equals('153', '152');
      calcUtils.equals('154', '153');
      calcUtils.getGlobalValue('157', 't2s500', '175');
      calcUtils.equals('158', '106');
      calcUtils.getGlobalValue('156', 'cp', 'Days_Fiscal_Period');
      calcUtils.divide('159', '155', '156', ['154', '158'], 0.115);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 6
      //todo: getGlobalValue from Form Royalties
      // calcUtils.getGlobalValue('161', '', '');
      calcUtils.sumBucketValues('165', ['160', '161', '162', '163', '164']);
      calcUtils.subtract('167', '165', '166');
    });

  });
})();
