(function() {

  wpw.tax.create.calcBlocks('ratesNb', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      //S366
      field('1009').cell(0, 2).assign(400000);
      field('1009').cell(0, 4).assign(13);
      field('1009').cell(0, 6).assign(3);

      field('1009').cell(1, 2).assign(425000);
      field('1009').cell(1, 4).assign(13);
      field('1009').cell(1, 6).assign(2.5);

      field('1009').cell(2, 2).assign(450000);
      field('1009').cell(2, 4).assign(13);
      field('1009').cell(2, 6).assign(2);

      field('1009').cell(3, 2).assign(475000);
      field('1009').cell(3, 4).assign(13);
      field('1009').cell(3, 6).assign(1.5);

      field('1009').cell(4, 2).assign(400000);
      field('1009').cell(4, 4).assign(13);
      field('1009').cell(4, 6).assign(5);

      field('1009').cell(5, 2).assign(500000);
      field('1009').cell(5, 4).assign(13);
      field('1009').cell(5, 6).assign(5);

      field('1009').cell(6, 2).assign(500000);
      field('1009').cell(6, 4).assign(12);
      field('1009').cell(6, 6).assign(3);

      field('1009').cell(7, 2).assign(500000);
      field('1009').cell(7, 4).assign(11);
      field('1009').cell(7, 6).assign(3);

      field('1009').cell(8, 2).assign(500000);
      field('1009').cell(8, 4).assign(10);
      field('1009').cell(8, 6).assign(3);

      field('1009').cell(9, 2).assign(500000);
      field('1009').cell(9, 4).assign(10);
      field('1009').cell(9, 6).assign(4.5);

      field('1009').cell(10, 2).assign(500000);
      field('1009').cell(10, 4).assign(12);
      field('1009').cell(10, 6).assign(4.5);

      field('1009').cell(11, 2).assign(500000);
      field('1009').cell(11, 4).assign(12);
      field('1009').cell(11, 6).assign(4);

      field('1009').cell(12, 2).assign(500000);
      field('1009').cell(12, 4).assign(14);
      field('1009').cell(12, 6).assign(3.5);

      field('1009').cell(13, 2).assign(500000);
      field('1009').cell(13, 4).assign(14);
      field('1009').cell(13, 6).assign(3);

      var date2015 = wpw.tax.date(2015, 1, 1);
      var date2016 = wpw.tax.date(2016, 4, 1);
      var date2015Comparisons = calcUtils.compareDateAndFiscalPeriod(date2015);
      var date2016Comparisons = calcUtils.compareDateAndFiscalPeriod(date2016);

      //days in tax year
      field('700').assign(date2015Comparisons.daysAfterDate - date2016Comparisons.daysAfterDate);
      field('800').assign(date2016Comparisons.daysAfterDate);
      //pro-rate
      field('900').assign(field('902').get() * field('700').get() / 366 + field('905').get() * field('800').get() / 366)
    });
  });
})();
