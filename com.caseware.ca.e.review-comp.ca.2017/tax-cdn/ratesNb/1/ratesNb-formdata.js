(function() {
  wpw.tax.create.formData('ratesNb', {
    formInfo: {
      abbreviation: 'ratesNb',
      title: 'New Brunswick Table of Rates and Values',
      showCorpInfo: true,
      category: 'Rates Tables'
    },
    sections: [
      {
        header: 'Schedule 366 – New Brunswick Corporation Tax Calculation',
        rows: [
          {
            type: 'table', num: '1009'
          }
        ]
      },
      {
        header: 'S21W - Provincial or Territorial Foreign Income Tax Credits and Federal Logging Tax Credit',
        rows: [
          {
            'label': 'Number of days in the tax year after January 1, 2015 and before April 1, 2016',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '700'
                }
              }
            ]
          },
          {
            'label': 'Number of days in the tax year after March 31, 2016',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '800'
                }
              }
            ]
          },
          {
            'label': 'Prorated provincial or territorial tax rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '900',
                  'decimals': 4
                }
              }
            ],
            'indicator': '%'
          }
        ]
      }
    ]
  });
})();
