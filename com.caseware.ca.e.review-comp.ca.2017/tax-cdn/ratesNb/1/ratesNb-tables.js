(function() {

  wpw.tax.global.tableCalculations.ratesNb = {
    1009: {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          header: 'Taxation year',
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none'
        },
        {
          header: 'limit',
          colClass: 'std-input-width',

        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Higher rate',
          colClass: 'std-input-width',

        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Lower rate',
          colClass: 'std-input-width',

        }
      ],
      cells: [
        {
          0: {
            label: '2003-01-01'
          },
          2: {},
          4: {

            decimals: 1
          },
          6: {

            decimals: 1
          }
        },
        {
          0: {
            label: '2004-07-01'
          },
          2: {},
          4: {

            decimals: 1
          },
          6: {

            decimals: 1
          }
        },
        {
          0: {
            label: '2005-07-01'
          },
          2: {},
          4: {},
          6: {

            decimals: 1
          }
        },
        {
          0: {
            label: '2006-07-01'
          },
          2: {},
          4: {},
          6: {

            decimals: 1
          }
        },
        {
          0: {
            label: '2007-01-01'
          },
          2: {},
          4: {},
          6: {

            decimals: 1
          }
        },
        {
          0: {
            label: '2009-01-01'
          },
          2: {},
          4: {

            decimals: 1
          },
          6: {

            decimals: 1
          }
        },
        {
          0: {
            label: '2009-07-01'
          },
          2: {},
          4: {

            decimals: 1
          },
          6: {

            decimals: 1
          }
        },
        {
          0: {
            label: '2010-07-01'
          },
          2: {},
          4: {

            decimals: 1
          },
          6: {

            decimals: 1
          }
        },
        {
          0: {
            label: '2011-07-01'
          },
          2: {},
          4: {

            decimals: 1
          },
          6: {

            decimals: 1
          }
        },
        {
          0: {
            label: '2012-01-01'
          },
          2: {},
          4: {

            decimals: 1
          },
          6: {

            decimals: 1
          }
        },
        {
          0: {
            label: '2013-07-01'
          },
          2: {},
          4: {

            decimals: 1
          },
          6: {

            decimals: 1
          }
        },
        {
          0: {
            label: '2015-01-01'
          },
          2: {
            num: '901'
          },
          4: {
            num: '902',
            decimals: 1
          },
          6: {
            num: '903',
            decimals: 1
          }
        },
        {
          0: {
            label: '2016-04-01'
          },
          2: {
            num: '904'
          },
          4: {
            num: '905',
            decimals: 1
          },
          6: {
            num: '906',
            decimals: 1
          }
        },
        {
          0: {
            label: '2017-04-01'
          },
          2: {
            num: '907'
          },
          4: {
            num: '908',
            decimals: 1
          },
          6: {
            num: '909',
            decimals: 1
          }
        }
      ]
    }
  }
})();
