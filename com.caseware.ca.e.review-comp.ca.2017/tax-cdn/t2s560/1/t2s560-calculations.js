(function() {
  wpw.tax.create.calcBlocks('t2s560', function(calcUtils) {
    calcUtils.calc(function(calcUtils) {
      var num = '1043';
      var num2 = '608';
      var midnum = '608Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '1047';
      var num2 = '610';
      var midnum = '610Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '1050';
      var num2 = '613';
      var midnum = '613Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '1052';
      var num2 = '616';
      var midnum = '616Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '1053';
      var num2 = '617';
      var midnum = '617Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 1 calcs
      field('110').assign(field('T2J.951').get() + ' ' + field('T2J.950').get());
      field('120').assign(field('T2J.956').get());
    });

    calcUtils.calc(function(calcUtils, field) {
      //rate from rateOn
      calcUtils.getGlobalValue('1011', 'ratesOn', '753');
      calcUtils.getGlobalValue('608Mult', 'ratesOn', '756');
      calcUtils.getGlobalValue('605Mult', 'ratesOn', '757');
      calcUtils.getGlobalValue('609Mult', 'ratesOn', '758');
      calcUtils.getGlobalValue('610Mult', 'ratesOn', '759');
      calcUtils.getGlobalValue('611Mult', 'ratesOn', '760');
      calcUtils.getGlobalValue('612Mult', 'ratesOn', '761');
      calcUtils.getGlobalValue('613Mult', 'ratesOn', '762');
      calcUtils.getGlobalValue('616Mult', 'ratesOn', '763');
      calcUtils.getGlobalValue('617Mult', 'ratesOn', '764');

      var isEligible = (field('300').get() === '1' && field('310').get() === '2' && field('320').get() === '2' && field('330').get() === '2');
      var isSpecifiedProduct = (field('400').get() === '1');
      var isQsc = (field('403').get() === '1');

      field('1061').assign(field('410').get() + field('415').get() + field('420').get());
      field('425').assign(field('1061').get() * 50 / 100);
      field('1002').assign(field('405').get() + field('411').get() + field('416').get() + field('421').get() + field('425').get());
      field('1003').assign(field('430').get() + field('435').get());
      field('1004').assign(field('1003').get());
      field('1005').assign(field('440').get() + field('445').get());
      field('1006').assign(field('1005').get());
      field('1007').assign(Math.max(field('1004').get() - field('1006').get(), 0));
      field('1008').assign(field('1007').get());
      field('450').assign(field('1002').get() + field('1008').get());
      field('1009').assign(Math.max(field('455').get() - field('460').get(), 0));
      field('1010').assign(field('1009').get());
      field('465').assign(Math.max(field('450').get() - field('1010').get(), 0));

      //part 5
      field('1012').assign(Math.min(field('500').get() + field('505').get(), field('1011').get()));
      field('1013').assign(field('1012').get());
      field('1014').assign(field('1013').get() == 0 ? 0 :
          Math.max(field('1011').get() - field('1013').get(), 0)
      );
      field('510').assign(field('1014').get());
      field('1015').assign(field('515').get() + field('520').get());
      field('525').assign(field('1015').get());
      field('1016').assign(Math.max(field('530').get() - field('535').get(), 0));
      field('1017').assign(field('1016').get());
      field('1018').assign(field('540').get() + field('545').get());
      field('1019').assign(field('1018').get());
      field('1020').assign(field('550').get() + field('555').get());
      field('1021').assign(field('1020').get());
      field('1022').assign(field('1017').get() + field('1019').get() + field('1021').get());
      field('560').assign(field('1022').get());
      field('565').assign(Math.max(field('525').get() - field('560').get(), 0));

      if (isSpecifiedProduct) {
        //do not complete part 5
        field('570').assign(0);
      } else {
        //complete part 5
        field('570').assign(Math.min(field('510').get(), field('565').get()));
      }

      if (field('570').get() == '0') {
        calcUtils.removeValue(['575', '576', '577', '578'], true)
      }

      var isEligibleFor931 = (field('1062').get() === '1');
      //part 6
      field('1023').assign(field('710').get() + field('715').get() + field('720').get());
      field('1024').assign(field('1023').get());
      field('1025').assign(field('730').get() + field('735').get());
      field('1026').assign(field('1025').get());
      field('750').assign(field('705').get() + field('1024').get() + field('1026').get());
      field('1027').assign(field('740').get() + field('745').get());
      field('1028').assign(field('1027').get());
      field('1029').assign(Math.max(field('755').get() - field('760').get(), 0));
      field('1030').assign(field('1029').get());

      if (isEligibleFor931) {
        field('775').assign(Math.max(field('750').get() - field('1028').get() - field('1030').get(), 0));
      }
      else {
        field('775').assign(0);
      }

      var isEligibleFor932 = (field('1063').get() === '1');
      //part 7
      field('1031').assign(field('810').get() + field('815').get() + field('820').get());
      field('1032').assign(field('1031').get());
      field('830').assign(field('805').get() + field('1032').get());
      field('1033').assign(Math.max(field('835').get() - field('840').get(), 0));
      field('1034').assign(field('1033').get());

      if (isEligibleFor932) {
        field('875').assign(Math.max(field('830').get() - field('1034').get(), 0));
      }
      else {
        field('875').assign(0);
      }

      if (isSpecifiedProduct) {
        //part 8
        field('1041').assign(field('471').get());
        field('1042').assign(field('472').get());
        field('1043').assign(field('473').get());
        field('1047').assign(0);
        field('608').assign(field('1043').get() * field('608Mult').get() / 100);
      } else {
        field('1041').assign(0);
        field('1042').assign(0);
        field('1043').assign(0);
        field('1044').assign(0);
        if (isQsc) {
          //part 8
          field('1047').assign(field('473').get() + field('578').get());
        } else {
          field('1047').assign(0);
        }

        field('610').assign(
            field('403').get() == 2 ? 0 : field('1047').get() * field('610Mult').get() / 100);
        field('1050').assign(field('473').get() + field('578').get());
        field('613').assign(field('1050').get() * field('613Mult').get() / 100);

        calcUtils.getApplicableValue('1051', ['610', '613']);
      }
      field('1052').assign(field('775').get());
      field('1053').assign(field('875').get());
      field('616').assign(field('1052').get() * field('616Mult').get() / 100);
      field('617').assign(field('1053').get() * field('617Mult').get() / 100);

      if (isEligible) {
        calcUtils.getApplicableValue('620', ['608', '1051', '616', '617'])
      }
      else {
        field('620').assign(0);
      }
    });

  });
})();
