(function() {
  wpw.tax.create.diagnostics('t2s560', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('5600005', common.prereq(common.check('t2s5.462', 'isNonZero'),
        common.requireFiled('T2S560')));

    diagUtils.diagnostic('5600010', common.prereq(common.requireFiled('T2S560'),
        common.check(['t2s5.462', '620'], 'isEmpty', true)));

    diagUtils.diagnostic('5600015', common.prereq(common.and(
        common.requireFiled('T2S560'),
        common.check('620', 'isNonZero')),
        common.check(['200', '210', '220'], 'isNonZero', true)));

    diagUtils.diagnostic('5600020', common.prereq(common.and(
        common.requireFiled('T2S560'),
        common.check('620', 'isNonZero')),
        common.check(['300', '310', '320', '330'], 'isNonZero', true)));

    diagUtils.diagnostic('5600022', common.prereq(common.and(
        common.requireFiled('T2S560'),
        common.check(['470', '471', '472', '473', '575', '576', '577', '578'], 'isNonZero')),
        common.check(['400', '403'], 'isNonZero', true)));

    diagUtils.diagnostic('560.510', common.prereq(common.requireFiled('T2S560'),
        function(tools) {
          return tools.field('510').require(function() {
            return this.get() <= 100000;
          })
        }));
  });
})();
