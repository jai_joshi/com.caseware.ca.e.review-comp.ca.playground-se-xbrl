(function() {

  wpw.tax.create.formData('t2s560', {
    formInfo: {
      abbreviation: 't2s560',
      title: 'Ontario Interactive Digital Media Tax Credit',
      schedule: 'Schedule 560',
      isRepeatForm: true,
      repeatFormData: {
        titleNum: '301'
      },
      headerImage: 'canada-federal',
      showCorpInfo: true,
      code: 'Code 1601',
      formFooterNum: 'T2 SCH 560 E (17)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule to claim an Ontario interactive digital media tax credit (OIDMTC) ' +
              'under sections 93, 93.1, and 93.2 of the Taxation Act, 2007 (Ontario). Complete a separate Schedule ' +
              '560 for each eligible product or eligible digital game.'
            },
            {
              label: 'The OIDMTC is a refundable tax credit based on qualifying expenditures incurred by a ' +
              'qualifying corporation for an eligible product or eligible digital game'
            },
            {
              label: 'Under section 93, the applicable rates for eligible products after March 26, 2009 are:',
              sublist: [
                '1. for specified products, qualifying corporations including qualifying small corporations (QSCs):<br>' +
                '– 35% of qualifying expenditures incurred.<br>',
                '2. for non-specified products, qualifying corporations including QSCs: <br>' +
                '– 40% of qualifying expenditures incurred.'
              ]
            },
            {
              label: 'Under subsection 93.1, the applicable rate for an eligible digital game of a qualifying digital' +
              ' game corporation (QDGC) is 35% of its qualifying labour expenditure incurred after March 26, 2009.'
            },
            {
              label: 'Under subsection 93.2, the applicable rate for an eligible digital game of a specialized' +
              ' digital game corporation (SDGC) is 35% of its qualifying labour expenditure incurred after' +
              ' March 26, 2009.'
            },
            {
              label: 'You can claim an OIDMTC under section 93 for the tax year in which the product is completed. ' +
              'Complete parts 1, 2, 3, 4, 5, and 8 as applicable'
            },
            {
              label: 'You can claim an OIDMTC under section 93.1 for the tax year if you were a QDGC and ' +
              'eligible expenditures were incurred for an eligible digital game. Complete parts 1, 2, 3, 6, and 8. ' +
              'If you make a claim under section 93.1 for an eligible product for any tax year, you cannot make a ' +
              'claim under section 93 for that product.'
            },
            {
              label: 'You can claim an OIDMTC under section 93.2 for the tax year if you were a SDGC and ' +
              'eligible expenditures were incurred for an eligible digital game. Complete parts 1, 2, 3, 7, ' +
              'and 8. In the year of product completion, you can make a claim for an OIMDTC under section 93 ' +
              'or section 93.2, but not under both sections, unless the transition rule under subsection 93(2.6) applies.',
              labelClass: 'fullLength'
            },
            {
              label: 'To be eligible for the OIDMTC, you must be a Canadian corporation and meet the ' +
              'eligibility requirements in Part 3',
              labelClass: 'fullLength'
            },
            {
              label: 'For expenditures incurred after April 23, 2015, the credit focuses on products that' +
              ' educate children under the age of 12 and on entertainment products. Certain products, such as ' +
              'search engines, real estate databases, or news and public affairs products are excluded. ' +
              'The rules that exclude promotional products have been strengthened. Products that were started' +
              ' before April 24, 2015, and that are no longer eligible for the credit are still eligible' +
              ' for relief for expenditures incurred before April 24, 2015.'
            },
            {
              label: 'Before claiming an OIDMTC, you must obtain a certificate of eligibility from ' +
              'the Ontario Media Development Corporation (OMDC). Only one certificate of eligibility' +
              ' will be issued for all of the eligible products or eligible digital games.' +
              ' Enter the certificate information for this product in Part 2 of this schedule.',
              sublist: [
                  'applications for certification of eligible products must be made to the OMDC on or before' +
                  ' the later of: <br>' +
                  'a the day that is 18 months after the end of the tax year of the corporation in which' +
                  ' the development of the eligible product is completed; and <br> ' +
                  'b the day that is six months after November 14, 2016.'
              ]
            },
            {
              label: 'To claim the OIDMTC, file this schedule and the certificate of eligibility with your' +
              '<i> T2 Corporation Income Tax Return</i> for the tax year. '
            }
          ]
        }
      ],
      category: 'Ontario Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Contact information',
        'rows': [
          {
            'type': 'table',
            'num': '105'
          }
        ]
      },
      {
        'header': 'Part 2 - Identifying the eligible product or eligible digital game',
        'rows': [
          {
            'label': 'Certificate of eligibility number'
          },
          {
            'type': 'infoField',
            'tn': '200',
            'num': '200',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '8',
                    'max': '9'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          {
            'label': 'Product title'
          },
          {
            'type': 'infoField',
            'tn': '210',
            'num': '210',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '1',
                    'max': '175'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          {
            'label': 'Estimated OIDMTC for the eligible product or eligible digital game'
          },
          {
            'type': 'infoField',
            'num': '220',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'tn': '220'
          }
        ]
      },
      {
        'header': 'Part 3 - Eligibility',
        'fieldAlignRight': true,
        'rows': [
          {
            'label': '1) Did the corporation develop, start developing, or complete development of an eligible' +
            ' product or an eligible digital game at a permanent establishment in Ontario operated by the corporation?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '300',
            'tn': '300',
            'init': '2'
          },
          {
            'label': '2) Was the corporation exempt from tax for the tax year under Part III of the Taxation Act, 2007 (Ontario)?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '310',
            'tn': '310',
            'init': '1'
          },
          {
            'label': '3) Was the corporation, at any time in the tax year, controlled directly or indirectly, in any manner, by one or more corporations, all or part of whose taxable income was exempt from tax under section 57 of the Corporations Tax Act (Ontario) or Part III of the Taxation Act, 2007 (Ontario)?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '320',
            'tn': '320',
            'init': '1'
          },
          {
            'label': '4) Was the corporation, at any time in the tax year, a prescribed labour-sponsored venture capital corporation?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '330',
            'tn': '330',
            'init': '1'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If you answered <b>no</b> to question 1 or <b>yes</b> to questions 2, 3, or 4,' +
            ' then you are <b>not eligible</b> for the OIDMTC.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 4 - Eligible labour expenditures',
        'rows': [
          {
            'label': 'If you are claiming an Ontario interactive digital media tax credit under section 93 of the Taxation Act, 2007 (Ontario), complete this part and Part 5, if applicable.',
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Note:</b> If you make a claim under section 93.1 for any tax year, you cannot make a claim under section 93. If you make a claim under section 93.2, refer to subsections 93(2.5) and (2.6).',
            'labelClass': 'fullLength'
          },
          {
            label: '<b>Transition Rules</b>: For products that were started before April 24, 2015 and that ' +
            'are no longer eligible under the new requirements, your claim is limited to expenditures incurred ' +
            'before April 24, 2015.',
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Is the product a specified product, under subsection 93(15) of the <i>Taxation Act, 2007</i> (Ontario)?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '400',
            'tn': '400',
            'init': '2'
          },
          {
            'label': 'If you answered <b>yes</b>, do not complete Part 5.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Is the corporation a QSC under subsection 93(14) of the <i>Taxation Act, 2007</i> (Ontario)?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '403',
            'tn': '403',
            'init': '2'
          },
          {
            label: 'If you answered <b>no</b>, enter "0" on line 610 in Part 8.',
            'labelClass': 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Ontario labour expenditures for an eligible product is the total of the following amounts:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Qualifying wage amount paid to employees for the eligible product in the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '405',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '405'
                }
              }
            ]
          },
          {
            'label': 'Qualifying remuneration amount incurred for the eligible product in the tax year and paid to:',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '– individuals',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '411',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '411'
                }
              }
            ]
          },
          {
            'label': '– taxable Canadian corporations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '416',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '416'
                }
              }
            ]
          },
          {
            'label': '– eligible partnerships',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '421',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '421'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of line 405 to line 421)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1002'
                }
              }
            ],
            'indicator': '4A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario labour expenditures incurred for the eligible product *:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- by the qualifying corporation in a previous tax year',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '430',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '430'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': '- by a qualifying predecessor corporation before disposition, merger, or windup',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '435',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '435'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 430 <b>plus</b> line 435) ',
            'labelClass': 'text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1003'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1004'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '4B'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible labour expenditures for the eligible product included in determining an OIDMTC claimed in a previous tax year, or claimed under section 93.2 for this year:'
          },
          {
            'label': '- by the qualifying corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '440',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '440'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': '- by a qualifying predecessor corporation',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '445',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '445'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 440 plus line 445)',
            'labelClass': 'text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1005'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1006'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '4C'
          },
          {
            'label': 'Subtotal (amount 4B minus amount 4C) (if negative, enter "0")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1007'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1008'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '4D'
          },
          {
            'label': 'Total Ontario labour expenditures before government assistance (amount 4A <b>plus</b> amount 4D)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '450',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '450'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Government assistance for the Ontario labour expenditures for the eligible product:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amounts the corporation or any other person or partnership has received, is entitled to receive, or may reasonably expect to receive',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '455',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '455'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Repayment of government assistance',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '460',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '460'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': 'Net government assistance (line 455 <b>minus</b> line 460) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1009'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1010'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '4E'
          },
          {
            'label': 'Total eligible labour expenditures for the eligible product (amount H minus amount I) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '465',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '465'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': '<b>Total eligible labour expenditures for the eligible product</b> (line 450 <b>minus</b>' +
            ' amount 4E)(if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '473',
                  'underline': 'double',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '473'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': '* Include expenditures that were incurred in the 37-month period ending at the end of the month' +
            ' in which development of the eligible product was completed.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 5 – Eligible marketing and distribution expenditures for an eligible product (other than a specified product)',
        'spacing': 'R_tn2',
        'rows': [
          {
            'label': 'Maximum eligible marketing and distribution expenditures for the eligible product',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1011',
                  'format': {
                    'prefix': '$'
                  }
                }
              },
              null
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Eligible marketing and distribution expenditures incurred for the eligible product that were already included in determining an OIDMTC for a previous tax year:'
          },
          {
            'label': '- by the qualifying corporation',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '500',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '500'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': '- by a qualifying predecessor corporation before disposition, merger, or windup',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '505',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '505'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 500 <b>plus</b> line 505) (cannot exceed the maximum)',
            'labelClass': 'text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1012'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1013'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '5A'
          },
          {
            'label': 'Balance of maximum eligible marketing and distribution expenditures <br>' +
            '($100,000 <b>minus</b> amount 5A)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1014',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '510',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '510'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label' : 'Marketing and distribution expenditures incurred in the month in which development of' +
            ' the eligible product was completed, plus those incurred in the 24 months before, or 12 months after,' +
            ' the month in which development of the eligible product was completed:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- by the qualifying corporation in the tax year or in a previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '515',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '515'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': '- by a qualifying predecessor corporation before disposition, merger, or windup',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '520',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '520'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 515 <b>plus</b> line 520)',
            'labelClass': 'text-right',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1015',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '525',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '525'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Government assistance for the marketing and distribution expenditures on line 525:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amounts the corporation or any other person or partnership has received, is entitled to receive, or may reasonably expect to receive',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '530',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '530'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': 'Repayment of government assistance',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '535',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '535'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': 'Net government assistance (line 530 <b>minus</b> line 535)<br>(if negative, enter "0") .',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1016'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1017'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '5B'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Eligible marketing and distribution expenditures incurred for the eligible product already included in determining an OIDMTC claimed in a previous tax year:'
          },
          {
            'label': '- by the qualifying corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '540',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '540'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': '- by a qualifying predecessor corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '545',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '545'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 540 <b>plus</b> line 545)',
            'labelClass': 'text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1018'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1019'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '5C'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Total of all marketing and distribution expenditures on line 525 that are ' +
            'Ontario labour expenditures that were incurred:',
            labelClass: 'fullLength'
          },
          {
            'label': '- by the qualifying corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '550',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '550'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': '- by a qualifying predecessor corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '555',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '555'
                }
              },
              {
                'padding': {
                  'type': 'text'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 550 <b>plus</b> line 555)',
            'labelClass': 'text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1020'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1021'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            indicator: '5D'
          },
          {
            'label': 'Subtotal (total of amounts 5B, 5C, and 5D)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1022',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '560',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '560'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 525 <b>minus</b> line 560) (if negative, enter "0") ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '565',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '565'
                }
              }
            ]
          },
          {
            'label' : '<b>Eligible marketing and distribution expenditures</b> for the tax year' +
            ' (lesser of line 510 and line 565)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '578',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '578'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 6 - Qualifying labour expenditures for an eligible digital game for a qualifying digital game corporation',
        'rows': [
          {
            'label': 'If you are a qualifying digital game corporation (QDGC) claiming an Ontario interactive digital media tax credit (OIDMTC) under section 93.1 of the Taxation Act, 2007 (Ontario), complete this part.',
            'labelClass': 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': '<b>Note:</b> If you make a claim under section 93(2.3) for any previous tax year, you cannot make a claim under section 93.1. If you claim a credit under section 93.2 for the year, you can also make a claim under section 93.1 for that year if the transitional rules in subsection 93.1(5) apply',
            'labelClass': 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label' : 'To be eligible to claim an OIDMTC under section 93.1, a QDGC must incur at least ' +
            '1 million dollars of qualifying labour expenditures within any period of 36 months' +
            ' that ends in the tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': 'Is the corporation a QDGS which incur at least $1 million of qualifying labour expenditures within any period of 36 months that ends in the tax year?',
            'num': '1062',
            'init': '2'
          },
          {
            'label': 'Ontario labour expenditures for the eligible digital game* is the total of the following amounts:<br> (include only Ontario labour expenditures incurred after March 26, 2009)',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Qualifying wage amount paid to employees for the eligible digital game in the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '705',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '705'
                }
              }
            ]
          },
          {
            'label': 'Qualifying remuneration amount incurred for the eligible digital game in the tax year and paid to:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- individuals',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '710',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '710'
                }
              },
              null
            ]
          },
          {
            'label': '- taxable Canadian corporations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '715',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '715'
                }
              },
              null
            ]
          },
          {
            'label': '- eligible partnerships',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '720',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '720'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of lines 710, 715, and 720)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1023'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1024'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Ontario labour expenditures incurred for the eligible digital game*:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- by the QDGC in a previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '730',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '730'
                }
              },
              null
            ]
          },
          {
            'label': '- by a qualifying predecessor corporation before merger or windup',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '735',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '735'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 730 <b>plus</b> line 735)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1025'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1026'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Ontario labour expenditures for the eligible digital game (total of line 705, amount 6A, and 6B)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '750',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '750'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Eligible labour expenditures for the eligible digital game included in determining an OIDMTC under section 93.2',
            'labelClass': 'fullLength'
          },
          {
            'label': '- in the current year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '740',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '740'
                }
              },
              null
            ]
          },
          {
            'label': '- in the previous year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '745',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '745'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 740 <b>plus</b> line 745)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1027'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1028'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Government assistance for the Ontario labour expenditures for the eligible digital game:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amounts the corporation or any other person or partnership has received, is entitled to receive, or may reasonably expect to receive',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '755',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '755'
                }
              },
              null
            ]
          },
          {
            'label': 'Repayment of government assistance',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '760',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '760'
                }
              },
                null
            ]
          },
          {
            'label': 'Net government assistance (line 755 <b>minus</b> line 760) (if negative, enter "0").',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1029'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1030'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '<b>Total qualifying labour expenditures for the eligible digital game for a QDGC</b><br>' +
            '(line 750 <b>minus</b> amount 6C <b>minus</b> amount 6D) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '775',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '775'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': '* For an eligible digital game, include Ontario labour expenditures that were incurred in the 36-month period selected by the QDGC that ends in the tax year',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 7 - Qualifying labour expenditures for an eligible digital game for a specialized digital game corporation',
        'rows': [
          {
            'label': 'If you are a specialized digital game corporation (SDGC) claiming an Ontario interactive ' +
            'digital media tax credit (OIDMTC) under section 93.2 of the <i>Taxation Act, 2007</i>' +
            ' (Ontario), complete this part.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Note:</b> If you make a claim under section 93.2 for the year, you can also make a claim under section 93 for that year if the transitional rules in subsection 93(2.6) are met. You can also make a claim under 93.1 for that year if the transitional rules in subsection 93.1(5) are met.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'To be eligible to claim an OIDMTC under section 93.2, a SDGC must incur at least 1' +
            ' million dollars of Ontario labour expenditures for eligible digital games in the tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': 'Is the corporation a QDGS which incur at least $1 million of Ontario labour expenditures for eligible digital games in the tax year ?',
            'num': '1063',
            'init': '2'
          },
          {
            'label': 'Ontario labour expenditures for the eligible digital game is the total of the following amounts:<br>(include only Ontario labour expenditures incurred after March 26, 2009)',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Qualifying wage amount paid to employees for the eligible digital game in the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '805',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '805'
                }
              },
                null
            ]
          },
          {
            'label': 'Qualifying remuneration amount incurred for the eligible digital game in the tax year and paid to:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- individuals',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '810',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '810'
                }
              },
              null
            ]
          },
          {
            'label': '- taxable Canadian corporations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '815',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '815'
                }
              },
              null
            ]
          },
          {
            'label': '- eligible partnerships',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '820',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '820'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of lines 810, 815, and 820)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1031'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1032'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '7A'
          },
          {
            'label': 'Ontario labour expenditures for the eligible digital game before government assistance (Line 805 <b>plus</b> amount 7A)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '830',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '830'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Government assistance for the Ontario labour expenditures for the eligible digital game activities:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amounts the corporation or any other person or partnership has received, is entitled to receive, or may reasonably expect to receive',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '835',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '835'
                }
              },
              null
            ]
          },
          {
            'label': 'Repayment of government assistance',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '840',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '840'
                }
              },
              null
            ]
          },
          {
            'label': 'Net government assistance (line 835 <b>minus</b> line 840) (if negative, enter "0") ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1033'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1034'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '7B'
          },
          {
            'label': '<b>Total qualifying labour expenditures for the eligible digital game for a SDGC</b>' +
            ' (line 830 <b>minus</b> amount 7B)<br> (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '875',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '875'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 8 – Ontario Interactive Digital Media Tax Credit',
        'rows': [
          {
            label: 'If you answered <b>yes</b> to the question at line 400 in Part 4, calculate your credit for' +
            ' the specified product at line 608.',
            labelClass: 'fullLength'
          },
          {
            label: 'Ontario interactive digital media tax credit for a specified product',
            labelClass: 'fullLength'
          },
          {
            'type': 'table',
            'num': 'di_table_3'
          },
          {
            'label': 'If you answered <b>no</b> to the question at line 400, calculate your credit for the' +
            ' eligible product that is not a specified product at line 610 or 613.',
            labelClass: 'fullLength'
          },
          {
            'label': 'For a qualifying small corporation (QSC).',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': 'di_table_6'
          },
          {
            label: 'For a qualifying corporation other than a QSC.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': 'di_table_9'
          },
          {
            'label': 'Ontario interactive digital media tax credit for an eligible product that is not a specified ' +
            'product <br>(line 610 or 613, whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1051'
                }
              }
            ],
            'indicator': '8A'
          },
          {
            'label': 'Qualifying digital game corporation credit for an eligible digital game (section 93.1)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': 'di_table_10'
          },
          {
            'label': 'Specialized digital game corporation credit for an eligible digital game (section 93.2)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': 'di_table_11'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Ontario interactive digital media tax credit</b>' +
            ' (line 608, amount 8A, line 616, or line 617 whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '620',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '620'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter the amount from line 620 on line 462 of Schedule 5, <i>Tax Calculation Supplementary' +
            ' – Corporations</i>. If you are filing more than one Schedule 560, add the amount from line 620 from' +
            ' all the schedules and enter the total amount on line 462 of Schedule 5.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            label: 'See the privacy statement on your return.',
            labelClass: 'italic absoluteAlignRight',
          }
        ]
      }
    ]
  });
})();
