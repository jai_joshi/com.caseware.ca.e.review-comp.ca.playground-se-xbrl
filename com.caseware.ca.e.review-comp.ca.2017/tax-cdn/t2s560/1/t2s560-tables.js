(function() {
  function getTableColumns() {
    return [
      {
        'type': 'none'
      },
      {
        'colClass': 'std-padding-width',
        'type': 'none'
      },
      {
        'colClass': 'std-input-width'
      },
      {
        'colClass': 'qtr-col-width',
        'textAlign': 'center',
        'type': 'none'
      },
      {
        'colClass': 'half-col-width',
        'textAlign': 'center',
        'disabled': true
      },
      {
        'colClass': 'qtr-col-width',
        'textAlign': 'center',
        'type': 'none'
      },
      {
        'colClass': 'std-padding-width',
        'type': 'none'
      },
      {
        'colClass': 'std-input-width'
      }
    ]
  }

  wpw.tax.create.tables('t2s560', {
    'di_table_3': {
      'num': 'di_table_3',
      'columns': getTableColumns(),
      'cells': [
        {
          '0': {
            'label': 'Amount from line 473 in Part 4',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '1043',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '608Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '608'
          },
          '7': {
            'num': '608',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_6': {
      'columns': getTableColumns(),
      'cells': [
        {
          '0': {
            'label': 'Line 473 in Part 4 <b>plus</b> line 578 in Part 5',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '1047',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '610Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '610'
          },
          '7': {
            'num': '610',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'PP'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_9': {
      'columns': getTableColumns(),
      'cells': [
        {
          '0': {
            'label': 'Line 473 in Part 4 <b>plus</b> line 578 in Part 5',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '1050',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '613Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '613'
          },
          '7': {
            'num': '613',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_10': {
      'columns': getTableColumns(),
      'cells': [
        {
          '0': {
            'label': 'Amount from line 775 in Part 6',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '1052',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '616Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '616'
          },
          '7': {
            'num': '616',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' singleUnderline'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_11': {
      'columns': getTableColumns(),
      'cells': [
        {
          '0': {
            'label': 'Amount from line 875 in Part 7',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '1053',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '617Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '617'
          },
          '7': {
            'num': '617',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    '105': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1],
      'columns': [
        {
          'width': '60%',
          cellClass: 'alignLeft'
        },
        {
          cellClass: 'alignLeft'
        }
      ],
      'cells': [
        {
          '0': {
            label: 'Name of contact person',
            num: '110', "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
            type: 'text',
            tn: '110'
          },
          '1': {
            label: 'Telephone number including area code',
            num: '120',
            'tn': '120',
            'telephoneNumber': true,
            type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
            validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
          }
        }
      ]
    }
  })
})();
