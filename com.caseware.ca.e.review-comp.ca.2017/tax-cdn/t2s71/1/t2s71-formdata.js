(function() {
  'use strict';

  wpw.tax.global.formData.t2s71 = {
    'formInfo': {
      'abbreviation': 't2s71',
      isRepeatForm: true,
      repeatFormData: {
        titleNum: '101'
      },
      'title': 'Income Inclusion for Corporations that are Members of Single-Tier Partnerships',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      //'subTitle': ' (2011 and later tax years)',
      'schedule': 'Schedule 71',
      'showCorpInfo': true,
      category: 'Federal Tax Forms',
      formFooterNum: 'T2 SCH 71 E (15)',
      headerImage: 'canada-federal',
      'description': [
        {text: ''},
        {
          'type': 'list',
          'items': [
            {
              label: 'If the corporation is a member of a <b>single-tier partnership</b> that has a fiscal ' +
              'period-end that differs from the corporation\'s tax year-end, use this schedule to determine ' +
              'the corporation\'s income inclusion in respect of the partnership for the tax year under sections 34.2 and 34.3 of the <i>Income Tax Act</i>. Complete a separate schedule for each single-tier partnership.'
            },
            {
              label: 'Complete Part 3 of this schedule to calculate the <b>adjusted stub period accrual</b> (ASPA) in respect of a single-tier partnership if:',
              sublist: [
                'the corporation has a <b>significant interest</b> in the partnership at the end of the last fiscal period of the partnership that ends in the tax year;',
                'another fiscal period of the partnership starts in the tax year and ends after the tax year of the corporation;',
                'at the end of the tax year, the corporation is entitled to a share of an income, loss, taxable capital gain, or allowable capital loss of the partnership for the fiscal period that ends after the end of the tax year; and',
                'the corporation is <b>not</b> a professional corporation.'
              ]
            },
            {
              label: '<b>Significant interest</b> means that the corporation, or the corporation together with one or ' +
              'more persons or partnerships related to or affiliated with the ' +
              'corporation, is entitled to <b>more than 10%</b> of the income or loss of the partnership, or the ' +
              'assets (net of liabilities) of the partnership if it were to cease to exist.'
            },
            {
              label: 'The ASPA for the corporation\'s first tax year that ends<b> after March 22, 2011</b>, may' +
              ' be eligible as<b> qualifying transitional income</b> (QTI), which is calculated in Part 5 of ' +
              'this schedule. '
            },
            {
              label: 'A corporation can be eligible for <b>transitional relief</b> if it has QTI for the partnership. ' +
              'Transitional relief lets the corporation claim a <b>reserve</b> under ' +
              'subsection 34.2(11). The reserve is calculated in Part 7 of this schedule.'
            },
            {
              label: 'Complete Part 2 of this schedule to calculate the <b>eligible alignment income</b> for the ' +
              'purpose of QTI ' +
              'for the tax year. A corporation may be subject to an additional income inclusion if the members of the ' +
              'partnership have made a valid single-tier alignment election under subsections 249.1(8) and 249.1(10). ' +
              'An alignment election is not permitted where any member of the partnership is a professional corporation. ' +
              'The additional income that arises for an eligible fiscal period as a result of a valid election may ' +
              'qualify as eligible alignment income for the purposes of QTI. <b>A corporation is not required to have a ' +
              'significant interest in a partnership in order to have eligible alignment income.</b>'
            },
            {
              label: 'Generally, amounts included or claimed under subsections 34.2(2), 34.2(3), 34.2(4), 34.2(11), ' +
              'and 34.2(12) are deemed to have the <b>same character</b> and be in the <b>same proportions</b> as the ' +
              'partnership ' +
              'income to which they relate. For example, if a corporation receives $100,000 of partnership income' +
              ' for the partnership\'s fiscal period ending in its tax year, and that income is made up of $40,000 ' +
              'of active business income, $30,000 of income from property, and $30,000 as a taxable capital gain, ' +
              'the corporation\'s ASPA in respect of the partnership would be 40% active business income, 30% property ' +
              'income, and 30% taxable capital gains.'
            },
            {
              label: 'Section 34.2 does not apply when calculating, for a tax year of a foreign affiliate of a ' +
              'corporation resident in Canada, the affiliate\'s foreign accrual property income for the corporation ' +
              'and, generally, the affiliate\'s exempt surplus or exempt deficit, hybrid surplus or hybrid deficit, ' +
              'and taxable surplus or taxable deficit, for the corporation. See subsection 34.2(8).'
            },
            {
              label: 'Section 249.1 defines <b>fiscal period</b> and sets out conditions to align the partnership\'s ' +
              'fiscal period.'
            },
            {
              label: 'All legislative references are to the <i>Income Tax Act</i> and<i> Income Tax Regulations</i>. ' +
              'This schedule does not replace the Act and its regulations.'
            },
            {
              label: 'This schedule is a worksheet only. You do not have to file it with your <i>T2 Corporation ' +
              'Income Tax Return</i>.'
            },
            {
              label: 'Report on Schedule 73, <i>Income Inclusion Summary for Corporations that are Members of ' +
              'Partnerships</i>, the amounts calculated on this schedule. File Schedule 73 with the corporation\'s ' +
              'T2 return. If the corporation reported previous-year amounts of stub period accrual, alignment income, ' +
              'or transitional reserve on Schedule 1, complete a Schedule 73 for the previous year, and file ' +
              'it separately.'
            }
          ]
        }
      ]
    },
    'sections': [
      {
        'header': 'Part 1 – Partnership information',
        'rows': [
          {'label': 'Partnership\'s name'},
          {
            'type': 'infoField',
            'inputType': 'textArea',
            'labelClass': 'fullLength',
            'width': '70%',
            'num': '101'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Partnership\'s account number',
            'type': 'infoField',
            'inputType': 'custom',
            'format': [
              '{N9}RZ{N4}',
              'NR'
            ],
            'num': '102'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': 'Did the partnership elect to change its fiscal period-end?',
            'labelWidth': '821px',
            'num': '103',
            init: '2'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Tax year in which the corporation was first eligible for Qualified Transitional Income (QTI) in respect of the partnership:',
            'labelWidth': '821px',
            'num': '108',
            'type': 'infoField',
            'inputType': 'dropdown',
            'options': [
              {
                'value': '2011',
                'option': '2011'
              },
              {
                'value': '2012',
                'option': '2012'
              },
              {
                'value': '2013',
                'option': '2013'
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 2 – Eligible alignment income',
        'rows': [
          {
            'label': 'Complete Part 2 only once, for the year in which eligible alignment income arises. Do not complete it for other years. Calculate the eligible alignment income for the eligible fiscal period (note 1) if the following conditions apply:',
            'labelClass': 'fullLength'
          },
          {
            'label': '–a valid single-tier fiscal period alignment election has been made under subsections 249.1(8) and 249.1(10); and',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '–the eligible fiscal period is preceded by another fiscal period of the partnership that ends in the corporation\'s first tax year ending after March 22, 2011,  and the corporation is a member of the partnership at the end of that preceding fiscal period.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'If the eligible fiscal period is the first fiscal period of the partnership that ends in the corporation\'s first tax year ending after March 22, 2011, the eligible alignment income is nil.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s share of income of the partnership for the eligible fiscal period (note 1) (other than dividends for which a deduction is available under section 112 or 113)',
            'labelWidth': '440px'
          },
          {
            'type': 'table',
            'num': '221'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s share of loss of the partnership for the eligible fiscal period (note 1)',
            'labelWidth': '440px'
          },
          {
            'type': 'table',
            'num': '225'
          },
          {
            'label': 'Subtotal (amount a <b>minus</b> amount b) ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '202'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '203'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Corporation\'s share of taxable capital gain of the partnership for the eligible fiscal period (note 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '204'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of allowable capital loss of the partnership for the eligible fiscal period (note 1)<br>(cannot be more than amount c).',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '205'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount c <b>minus</b> amount d)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '206'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '207'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Subtotal (amount A <b>plus</b> amount B) ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '208'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Deduct',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Amounts deductible by the corporation (sections 66.1, 66.2, 66.21, and 66.4) for resource expenses deemed by subsection 66(18) to be incurred at the end of the eligible fiscal period (note 1) of the partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '209'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': '<b>Eligible alignment income</b> for the tax year (amount C <b>minus</b> amount D) (if negative, enter "0").',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '210'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Enter amount E in column 8 of Schedule 73.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 1. The eligible fiscal period is the first aligned fiscal period of the partnership that ends in the first tax year of the corporation ending after March 22, 2011.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Allocation of amount E (carried to Schedule 73)',
        'rows': [
          {
            'label': 'Business income included in amount E',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '211',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Property income included in amount E',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '212',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in amount E',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '213',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in amount E',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '214',
                  'init': '0'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Amounts from tax year in which the eligible fiscal period ended',
        'rows': [
          {
            'label': 'Do not complete this section if the eligible fiscal period of the partnership ends in the current tax year.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible alignment income (amount E) for the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '215',
                  'init': '0'
                }
              },
              null
            ]
          },
          {
            'label': 'Business income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '216',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Property income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '217',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '218',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '219',
                  'init': '0'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 3 – Adjusted stub period accrual (ASPA)',
        'rows': [
          {
            'label': 'A corporation\'s ASPA in respect of a partnership gives an estimate of the income that the corporation is deferring during a stub period as a result of its membership in a partnership that has a fiscal period that differs from the corporation\'s tax year. Where the last fiscal period of the partnership begins  in the corporation\'s tax year and ends after that tax year, the stub period is normally the period from the start of that fiscal period to the end of the corporation\'s tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the corporation becomes a member of a partnership during a fiscal period of the partnership, see Part 4.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s share of income of the partnership for the fiscal period(s) (note 2) that end(s) in the tax year of the corporation (other than dividends for which a deduction is available under section 112 or 113)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '321'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s share of loss of the partnership for the fiscal period(s) (note 2) that end(s) in the tax year of the corporation',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '325'
          },
          {
            'label': 'Subtotal (amount e <b>minus</b> amount f) ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '303'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '304'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Corporation\'s share of taxable capital gain of the partnership for the fiscal period(s) (note 2) that end(s) in the tax year of the corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '305'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'g'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of allowable capital loss of the partnership for the fiscal period(s) (note 2) that end(s) in the tax year of the corporation (cannot be more than amount g)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '306'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'h'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount g<b> minus</b> amount h)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '307'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '308'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Subtotal (amount F <b>plus</b> amount G)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '335'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'infoTable': true,
            'num': '309'
          },
          {
            'label': 'Stub period accrual (amount<b> H multiplied by </b>i)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '313'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Enter amount I in column 1 of Schedule 73 (if negative, enter "0").',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 2. The corporation can have more than one fiscal period of the partnership that ends in its tax year. Enter the income or loss allocated to the corporation for all of the fiscal periods.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 3. The number of days could be more than 365 if there is more than one fiscal period of the partnership that ends in the corporation\'s tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Deduct',
            'labelClass': 'fullLength bold'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Designated qualified resource expenses [subsections 66.1(6), 66.2(5), 66.21(1), and 66.4(5)] (note 4) for the stub period',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '314'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'j'
                }
              }
            ]
          },
          {
            'label': 'Enter amount j in column 2 of Schedule 73.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Discretionary amount designated by the corporation (note 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '315'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'k'
                }
              }
            ]
          },
          {
            'label': 'Enter amount k in column 3 of Schedule 73.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal (amount j <b>plus</b> amount k)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '316'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '317'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': '<b>ASPA</b> for the tax year (amount I <b>minus</b> amount J) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '318'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': 'Note 4. Subsection 34.2(6) gives the designated amount for qualified resource expenses. Once filed, the designation cannot be amended or revoked. The corporation can designate an amount as its qualified resource expense for the stub period in respect of a partnership to the extent the corporation gets in writing from the partnership, before the corporation\'s filing due date for the tax year for which the ASPA is being calculated, information identifying the relevant expenses. The relevant expenses are those identified by the partnership as being the corporation\'s qualified resource expenses incurred by the partnership, determined as if those expenses had been incurred by the partnership in its last fiscal period that ended in the tax year (that is, based on the corporation\'s share for the last fiscal period, and not at the end of the tax year). The amount designated cannot be more than the maximum amount that would be deductible by the corporation for the identified resource expenses under sections 66.1, 66.2, 66.21, and 66.4 in calculating its income if the partnership\'s fiscal period ended at the end of the corporation\'s tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 5. The corporation can designate an amount (other than an amount included on line j) on its T2 Corporation Income Tax Return filed on or before corporation\'s filing due date. Once filed, the designation cannot be amended or revoked. The corporation may have to include in its income an <b>income shortfall adjustment</b> to account for under-reported income when the corporation has made a discretionary designation to reduce the ASPA inclusion for a previous tax year. For the calculation of the income shortfall adjustment, see Part 8.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Allocation of amount K (carried to Schedule 73)',
        'rows': [
          {
            'label': 'Business income included in amount K',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '326',
                  'disabled': true,
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Property income included in amount K',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '327',
                  'disabled': true,
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in amount K',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '328',
                  'disabled': true,
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in amount K',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '329',
                  'disabled': true,
                  'init': '0'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Amounts from previous tax year',
        'rows': [
          {
            'label': 'ASPA (amount K) for the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '330',
                  'init': '0'
                }
              },
              null
            ]
          },
          {
            'label': 'Business income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '331',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Property income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '332',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '333',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '334',
                  'init': '0'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 4 – Income inclusion for a new corporate member of a partnership',
        'rows': [
          {
            'label': 'If the corporation becomes a member of a partnership during a fiscal period of the partnership (the <b>particular period</b>) that starts in the corporation\'s tax year and ends after the tax year, but on or before the filing due date for that year, and the corporation has a significant interest in the partnership at the end of the particular period, the corporation may include in calculating its income for the tax year in respect of the partnership the lesser of:',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount, if any, designated by the corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '401'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': 'Use 0 as designated amount?',
            'indicatorColumn': true,
            'input2': true,
            'num': '401A',
            'type': 'infoField',
            'width': '30px',
            'inputType': 'singleCheckbox',
            'labelCellClass': 'alignRight'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'and',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s income from the partnership for the particular period <br>(other than dividends for which a deduction is available under section 112 or 113)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '402'
          },
          {
            'type': 'table',
            'num': '404'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '406'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'l'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '419'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal (amount l <b>multiplied</b> by m)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '423'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Income inclusion for a new corporate member of a partnership</b> for the tax year (amount L or M, whichever is less) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '428'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'label': 'Enter amount N in column 6 of Schedule 73.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Allocation of amount N (carried to Schedule 73)',
        'rows': [
          {
            'label': 'Has an amount been designated by the corporation in amount L and carried to amount N?',
            'labelClass': 'fullLength',
            'num': '499',
            'type': 'infoField',
            'inputType': 'radio'
          },
          {
            'label': 'Business income included in amount N',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '429'
                }
              }
            ]
          },
          {
            'label': 'Property income included in amount N',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '430'
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in amount N',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '431'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in amount N',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '432'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Amounts from previous tax year',
        'rows': [
          {
            'label': 'Income inclusion for a new corporate member of a partnership (amount N) for the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '434'
                }
              },
              null
            ]
          },
          {
            'label': 'Business income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '435'
                }
              }
            ]
          },
          {
            'label': 'Property income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '436'
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '437'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '438'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 5 – Qualifying transitional income (QTI)',
        'rows': [
          {
            'label': 'In order for a corporation to have QTI, the corporation must have been a member of the partnership on March 22, 2011. Complete this part only once, for the year in which the QTI arises. Do not complete it for other years. The QTI amount is used to calculate the transitional reserve in Part 7 of this schedule. The QTI is made up of the corporation\'s eligible alignment income and ASPA for the corporation\'s first tax year that ends after March 22, 2011. <b>A corporation that is eligible for transitional relief may first have QTI for a particular partnership in its 2011 or 2012 tax year</b>. Subsection 34.2(15) requires that for the purpose of QTI, the income or loss of a partnership for a fiscal period has to be calculated as if:',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '• the partnership had deducted, for the fiscal period, the maximum amount deductible for any expense, reserve, allowance, or other amount;',
            'labelClass': 'fullLength'
          },
          {
            'label': '• the Act were read without reference to the flexible inventory adjustment rule in paragraph 28(1)(b); and',
            'labelClass': 'fullLength'
          },
          {
            'label': '• the partnership made a work-in-progress election under paragraph 34(a).',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible alignment income for the tax year (amount E from Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '501'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'label': 'ASPA for the tax year (amount K from Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '502'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'label': '<b>Qualifying transitional income</b> (amount O <b>plus</b> amount P) (note 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '503'
                }
              }
            ],
            'indicator': 'Q'
          },
          {
            'label': 'Enter amount Q in column 9 of Schedule 73.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 6. If a corporation has QTI (or would have had QTI if the partnership had ASPA), a one-time adjustment is calculated for the corporation\'s QTI  in a later year. See Part 6.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Qualifying transitional income (QTI) - Previous taxation year',
        'rows': [
          {
            'label': 'Qualifying transitional income (QTI) - Previous taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '504'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 6 – Adjusted amount of qualifying transitional income',
        'rows': [
          {
            'label': 'Calculate an adjustment of the ASPA included in the qualified transitional income (QTI). The QTI adjustment occurs only once and in the <b>particular tax year</b> (note 7). Once the adjustment to a corporation\'s QTI in respect of a partnership is made, that QTI is the corporation\'s QTI in respect of the partnership for the particular tax year and each later tax year for calculating the transitional reserve in Part 7.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s share of income of the partnership for the particular period (note 8) (other than dividends for which a deduction is available under section 112 or 113)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '600'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '604'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'n'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of loss of the partnership for the particular period (note 8)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'infoTable': true,
            'maxLoop': 100000,
            'num': '605',
            'hasTotals': true
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '609'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'o'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount n <b>minus</b> amount o)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '610'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '611'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'R'
          },
          {
            'label': 'Corporation\'s share of taxable capital gain of the partnership for the particular period (note 8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '612'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'p'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of allowable capital loss of the partnership for the particular period (note 8)<br>(cannot be more than amount p)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '613'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'q'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount p <b>minus</b> amount q)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '614'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '615'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'S'
          },
          {
            'label': 'Subtotal (amount R <b>plus</b> amount S)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '616'
                }
              }
            ],
            'indicator': 'T'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'infoTable': true,
            'num': '617'
          },
          {
            'label': 'Subtotal (amount T <b>multiplied</b> by r)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '621'
                }
              }
            ],
            'indicator': 'U'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Designated qualified resource expenses [subsections 66.1(6), 66.2(5), 66.21(1), and 66.4(5)] for the particular period (note 8) of the partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '622'
                }
              }
            ],
            'indicator': 'V'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Adjusted amount of stub period accrual included in QTI (amount U <b>minus</b> amount V) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '623'
                }
              }
            ],
            'indicator': 'W'
          },
          {
            'label': 'Eligible alignment income for the previous tax year (amount E from Part 2 of previous year\'s Schedule 71)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '624'
                }
              }
            ],
            'indicator': 'X'
          },
          {
            'label': '<b>Adjusted amount of qualifying transitional income</b> (amount W <b>plus</b> amount X)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '625'
                }
              }
            ],
            'indicator': 'Y'
          },
          {
            'label': 'Enter amount Y in column 10 of Schedule 73.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 7. The <b>particular tax year</b> is the first tax year:',
            'labelClass': 'fullLength'
          },
          {
            'label': '• that is after the tax year in which the corporation has (or would have, if the partnership had income) an ASPA that is (or would be) included in its QTI in respect of the partnership; and',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '• in which ends the fiscal period of the partnership that began in the tax year in which QTI in respect of the partnership was initially determined.',
            'labelClass': 'tabbed fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the corporation\'s current tax year meets these conditions, the adjustment of QTI applies, even if the recalculation of the QTI results in no adjustment to the amount of QTI.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note',
            'labelClass': 'fullLength bold tabbed'
          },
          {
            'label': 'Although the <b>particular tax year</b> is normally the tax year right after the tax year in which a corporation\'s QTI in respect of a particular partnership is first determined, this is not necessarily the case. For example, the corporation can have a short tax year between the end of the tax year in which the fiscal period of the partnership began and the beginning of the tax year in which that fiscal period ends.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 8. The <b>particular period</b> of the partnership is its fiscal period that starts in the corporation’s first tax year for which the QTI was initially calculated and ends in the corporation’s tax year (the <b>particular tax year</b>).',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 9. In which the QTI arose (or would have, if the partnership had income).',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Allocation of amount W (carried to Schedule 73)',
        'rows': [
          {
            'label': 'Business income included in amount W',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '626',
                  'disabled': true
                }
              }
            ]
          },
          {
            'label': 'Property income included in amount W',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '627',
                  'disabled': true
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in amount W',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '628',
                  'disabled': true
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in amount W',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '629',
                  'disabled': true
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Adjusted amount of qualifying transitional income',
        'rows': [
          {
            'label': 'If adjustment took place in a prior year, provide the QTI for the year of adjustment',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '636'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 7 – Transitional reserve',
        'rows': [
          {
            'label': 'Generally, a corporation is eligible for transitional relief if it is a partner of the partnership for which it had qualifying transitional income (QTI).',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subsections 34.2(13) and 34.2(18) set out circumstances under which the corporation may not claim, as a reserve, an amount for a partnership under subsection 34.2(11).',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subsection 34.2(14) sets out conditions in which a reserve can continue to be claimed by a corporation that has disposed of its interest in a partnership.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'A corporation that has QTI that first arises in its 2011 or 2012 tax year for a single-tier partnership may be eligible to claim a transitional reserve under subsection 34.2(11). The transitional reserve may allow a corporation to bring the QTI into its income over a period of up to five calendar years (note 10) that follows the tax year in which the QTI arose. The amount of transitional reserve in a particular tax year cannot be more than the least of amount AA, amount BB, and amount CC (for the first year in which QTI arose, this amount cannot be more than the least of amount AA and CC):',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'infoTable': true,
            'num': '700'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Previous year\'s reserve, if an amount was claimable (amount EE from Part 7 of previous year\'s Schedule 71)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '705'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 's'
                }
              }
            ]
          },
          {
            'label': 'Adjusted amount of QTI, if applicable (amount Y from Part 6)',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '706'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '1'
                }
              },
              null
            ]
          },
          {
            'label': 'QTI (note 13)',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '707'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '2'
                }
              },
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount by which QTI is increased, if it is the year in which the one- time QTI adjustment occurs. For other years, enter "0" (amount 1 <b>minus</b> amount 2) (if negative, enter "0")',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '708'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '709'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 't'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount s <b>plus</b> amount t)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '710'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '711'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'BB'
          },
          {
            'label': 'Corporation\'s income for the particular tax year (before claiming the reserve and amounts under sections 61.3 and 61.4) less dividends deductible under section 112 or 113 that are received after December 20, 2012',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '713'
                }
              }
            ],
            'indicator': 'CC'
          },
          {
            'label': 'If an amount was claimable as a transitional reserve for the previous year, enter amount AA, BB, or CC, whichever is less.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'If no amount was claimable as a transitional reserve for the previous year, enter amount AA, or CC, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '714'
                }
              }
            ],
            'indicator': 'DD'
          },
          {
            'label': '<b>Transitional reserve</b> (cannot be more than amount DD)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '715'
                }
              }
            ],
            'indicator': 'EE'
          },
          {
            'label': 'Enter amount EE in column 11 of Schedule 73',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 10. This applies for all tax years of the corporation that end in that calendar year.',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': 'Note 11. Amount Q in Part 5 of Schedule 71 for the corporation’s first tax year that ends after March 22, 2011 or, if the QTI has been adjusted in a particular tax year, amount Y in Part 6 of Schedule 71 for that year and each later tax year.',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': 'Note 12. The specified percentage of the corporation for the tax year for a partnership that can be claimed as a reserve is,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'labelClass': 'fulllength'
          },
          {
            'label': 'if the first tax year for which the corporation has QTI ends in 2011 and the tax year ends in:',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• 2011, 100%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2012, 85%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2013, 65%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2014, 45%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2015, 25%, and',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2016, 0%;',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'if the first tax year for which the corporation has QTI ends in 2012 and the tax year ends in:',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• 2012, 100%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2013, 85%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2014, 65%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2015, 45%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2016, 25%, and',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2017, 0%.',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 13. Amount Q in Part 5 of Schedule 71 for the corporation\'s first tax year that ends after March 22, 2011.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Allocation of amount EE (carried to Schedule 73)',
        'rows': [
          {
            'label': 'Business income included in Line EE',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '726'
                }
              }
            ]
          },
          {
            'label': 'Property income included in Line EE',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '727'
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in Line EE',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '728'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in Line EE',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '729'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Tracking the transitional reserve',
        'rows': [
          {
            'label': 'Table showing the deductible portion of the Transitional Reverse',
            'labelClass': 'bold fullLength'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'infoTable': true,
            'num': '720'
          }
        ]
      },
      {
        'header': 'Amounts from previous tax year',
        'rows': [
          {
            'label': 'Transitional reserve (amount EE) for the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '780',
                  'init': '0'
                }
              },
              null
            ]
          },
          {
            'label': 'Business income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '781',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Property income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '782',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Other income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '783',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '784',
                  'init': '0'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 8 – Income shortfall adjustment',
        'rows': [
          {
            'label': 'Complete this part only if the corporation designated a discretionary amount on line k in Part 3 of the base year\'s Schedule 71 for any qualifying partnership the corporation is a member of. Section 34.3 may require a corporate partner of a partnership for a tax year to include in its income an income shortfall adjustment to account for under-reported income.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Where the corporation has QTI, the corporation has to include in its income an income shortfall adjustment only for tax years that are after the first tax year of the corporation to which the adjustment of QTI applied</b> (note 14) <b>for any qualifying partnership.</b>',
            'labelClass': 'fullLength bold'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The <b>base year</b> is the preceding tax year of the corporation in which began the fiscal period of the qualifying partnership that ends in the corporation tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the corporate partner is a member of more than one <b>qualifying partnership</b>, the corporation can, in determining its income inclusion under section 34.3 for a tax year, offset an over-reported ASPA in respect of a qualifying partnership against an under-reported ASPA of another qualifying partnership.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'A <b>qualifying partnership</b> is a partnership that has a fiscal period that began in the preceding tax year and ended in the tax year, and in respect of which the corporation had to calculate an ASPA for the preceding tax year in which the fiscal period of the partnership began.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The <b>actual stub period accrual</b> is the recalculation of the ASPA in respect of a fiscal period of a partnership based on the pro-rated part of actual partnership income allocated to the corporation for the last fiscal period of the partnership that began in the base year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s share of income of the <b>qualifying partnership</b> for the last fiscal period that began in the <b>base year</b> (other than dividends for which a deduction was available under section 112 or 113)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'infoTable': true,
            'maxLoop': 100000,
            'num': '800',
            'hasTotals': true
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '804'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'u'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of loss of the <b>qualifying partnership</b> for the last fiscal period that began in the <b>base year</b>',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '805'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '808'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'v'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount u <b>minus</b> amount v)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '809'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '810'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'FF'
          },
          {
            'label': 'Corporation\'s share of taxable capital gain (note 15) of the <b>qualifying partnership</b> for the last fiscal period that began in the <b>base year</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '811'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'w'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of allowable capital loss (note 15) of the <b>qualifying partnership</b> for the last fiscal period that began in the <b>base year</b> (cannot be more than amount w)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '812'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'x'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount w <b>minus</b> amount x)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '813'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '814'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'GG'
          },
          {
            'label': 'Subtotal (amount FF <b>plus</b> amount GG)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '815'
                }
              }
            ],
            'indicator': 'HH'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'infoTable': true,
            'num': '816'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal (amount HH <b>multiplied</b> by y)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '820'
                }
              }
            ],
            'indicator': 'II'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount of the qualified resource expense [subsections 66.1(6), 66.2(5), 66.21(1), and 66.4(5)] in respect of the qualifying partnership that was designated by the corporate partner for the base year (amount j from Part 3 of the base year\'s Schedule 71)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '821'
                }
              }
            ],
            'indicator': 'JJ'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Actual stub period accrual</b> in respect of the qualifying partnership (amount II <b>minus</b> amount JJ)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '822'
                }
              }
            ],
            'indicator': 'KK'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 14. When the conditions in subsection 34.2(16) are met in a <b>particular tax year</b> (see definition in Part 6), the adjustment of QTI applies, even if the recalculation of the QTI results in no adjustment to the amount of QTI.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 15. In calculating the actual stub period accrual to determine the income inclusion, the corporation can offset all or part of the allowable capital losses from one qualifying partnership against all or part of the taxable capital gains from another qualifying partnership.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'ASPA from the base year (amount K from Part 3 of Schedule 71 for the base year)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '823'
                }
              }
            ],
            'indicator': 'LL'
          },
          {
            'label': 'Discretionary amount designated from the base year (amount k from Part 3 of Schedule 71 for the base year)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '824'
                }
              }
            ],
            'indicator': 'MM'
          },
          {
            'label': 'Subtotal (amount LL <b>plus</b> amount MM) (if negative, enter "0")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '825'
                }
              }
            ],
            'indicator': 'NN'
          },
          {
            'label': 'Base amount (amount KK or NN, whichever is less)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '826'
                }
              }
            ],
            'indicator': 'OO'
          },
          {
            'label': 'Subtotal (amount OO <b>minus</b> amount LL) (this amount can be positive or negative)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '827'
                }
              }
            ],
            'indicator': 'PP'
          },
          {
            'labelCLass': 'fullLength'
          },
          {
            'label': 'Number of days in the period that starts on the day after the day on which the <b>base year</b> ends, and ends on the day on which the tax year ends',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            indicator: 'z',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '828'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount PP <b>multiplied</b> by z)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '829'
                }
              }
            ],
            'indicator': 'QQ'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Average daily rate of interest determined by reference to the prescribed rate of interest under paragraph 4301(a) of the Income Tax Regulations for the period referred to at line z',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            indicator: 'aa',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '830',
                  'decimals': 5
                }
              }
            ]
          },
          {
            'label': '<b>Income shortfall adjustment</b> (amount QQ <b>multiplied by</b> aa)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '831'
                }
              }
            ],
            'indicator': 'RR'
          },
          {
            'label': 'Enter amount RR in column 14 of Schedule 73.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Base amount OO <b>multiplied by</b> z',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            indicator: 'bb',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '832'
                }
              }
            ]
          },
          {
            'label': 'Amount bb <b>multiplied by</b> aa',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            indicator: 'cc',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '833'
                }
              }
            ]
          },
          {
            'type': 'table',
            'fixedRows': true,
            'infoTable': true,
            'num': '835'
          },
          {
            'label': 'Enter amount SS in column 15 of Schedule 73.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  };
})();
