(function() {

  function allocate(field, outArr, incTbl, lossTbl, netTcg, deduct, total, factor) {
    var gainLoss; //store the gain or loss by item
    var netGainLoss = 0; //store the net gain or loss
    var adjust = 0; //store losses that aren't applied against same type gains
    for (var i = 0; i < 3; i++) {
      gainLoss = field(incTbl).cell(i, 3).get() - field(lossTbl).cell(i, 3).get();
      netGainLoss += gainLoss;
      if (gainLoss >= 0) {
        field(outArr[i]).assign(gainLoss);
      }
      else {
        adjust -= gainLoss;
        field(outArr[i]).assign(0);
      }
      if (angular.isDefined(factor)) {
        //factor is applied by the date calculations, reduces all amounts
        field(outArr[i]).assign(field(outArr[i]).get() * field(factor).get());
      }
    }
    if (adjust > 0) {
      //when there's a loss that can't be applied against the other types
      //i.e. other loss with only business income, this has to be reconciled using gains available
      allocReduce(field, outArr, adjust, netGainLoss, false);
    }
    //assign and factor netTcg separately
    field(outArr[3]).assign(field(netTcg).get());
    if (angular.isDefined(factor)) {
      field(outArr[3]).assign(field(outArr[3]).get() * field(factor).get());
    }

    if (deduct && total) {
      //when there's a deduction applied against the total, all types should be reduced to reconcile
      allocReduce(field, outArr, field(deduct).get(), field(total).get(), true);
    }
    allocRound(field, outArr, total);
  }

  function allocReduce(field, outArr, deduct, total, calcAll) {
    var div;
    var max = 4;
    total += deduct;
    if (!calcAll) {
      max = 3;
    }
    for (var i = 0; i < max; i++) {
      if (total == 0) {
        //this should never happen - prevent divide by zero
        div = deduct;
      }
      else {
        //factor to reduce by to reconcile to correct total amount
        div = deduct / total;
      }
      field(outArr[i]).assign(field(outArr[i]).get() * (1 - div));
    }
  }

  function allocRound(field, outArr, totalId) {
    var totalOut = 0;
    var maxVal = 0;
    var maxId;
    outArr.forEach(function(fieldId) {
      totalOut += field(fieldId).get();
      if (Math.max(maxVal, field(fieldId).get()) != maxVal) {
        maxVal = field(fieldId).get();
        maxId = fieldId;
      }
    });
    if (field(totalId).get() != totalOut && maxId) {
      field(maxId).assign(maxVal - (totalOut - field(totalId).get()));
    }
  }

  function getSum(field, fieldArr) {
    var sum = 0;
    fieldArr.forEach(function(fieldId) {
      if (fieldId.charAt(0) == '-' && angular.isDefined(field(fieldId.substring(1)).get())) {
        sum -= field(fieldId.substring(1)).get();
      }
      else if (angular.isDefined(field(fieldId).get())) {
        sum += field(fieldId).get();
      }
    });
    return sum;
  }

  wpw.tax.create.calcBlocks('t2s71', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      if (angular.isDefined(field('104').get())) {
        field('105').assign(calcUtils.addToDate(field('104').get(), 1, 0, -1));
        field('105').disabled(false);
      }
      if (angular.isDefined(field('106').get())) {
        field('107').assign(calcUtils.addToDate(field('106').get(), 1, 0, -1));
        field('107').disabled(false);
      }
    });
    calcUtils.calc(function(calcUtils, field) {
      //part2
      calcUtils.subtract('202', '222', '228');
      field('203').fieldAssign(field('202'));
      if (field('205').get() > field('204').get()) {
        field('205').fieldAssign(field('204'));
        field('205').disabled(false);
      }
      calcUtils.subtract('206', '204', '205');
      field('207').fieldAssign(field('206'));
      calcUtils.sumBucketValues('208', ['203', '207']);
      calcUtils.subtract('210', '208', '209');
      //section after part 2
      allocate(field, ['211', '212', '213', '214'], '221', '225', '206', '209', '210');
    });
    calcUtils.calc(function(calcUtils, field) {
      //part3
      calcUtils.subtract('303', '322', '336');
      field('304').fieldAssign(field('303'));
      if (field('306').get() > field('305').get()) {
        field('306').fieldAssign(field('305'));
        field('306').disabled(false);
      }
      calcUtils.subtract('307', '305', '306');
      field('308').fieldAssign(field('307'));
      field('310').assign(Math.max(0, wpw.tax.actions.calculateDaysDifference(field('106').get(), field('t2j.061').get())));
      calcUtils.sumBucketValues('335', ['304', '308']);
      calcUtils.divide('311', '310', '312');
      calcUtils.multiply('313', ['335', '311']);
      calcUtils.sumBucketValues('316', ['314', '315']);
      field('317').fieldAssign(field('316'));
      calcUtils.subtract('318', '313', '317');
      //section after part 3
      allocate(field, ['326', '327', '328', '329'], '321', '325', '307', '316', '318', '311');
    });
    calcUtils.calc(function(calcUtils, field) {
      //part4
      field('406').assign(Math.max(0, field('403').get() - field('405').get()));
      calcUtils.divide('421', '420', '422');
      calcUtils.multiply('423', ['406', '421']);
      if (field('401').get() == 0) {
        field('401A').disabled(false);
      }
      else {
        field('401A').assign(false);
      }

      if (field('401A').get()) {
        field('401').assign(0);
      }
      else {
        field('401').disabled(false);
      }
      ///

      if(!field('401A').get() &&(field('401').get() == 0 || field('423').get() <= field('401').get())){
          field('428').assign(field('423').get());
          field('499').assign(2);
      }else{
          field('428').assign(field('401').get());
          field('499').assign(1);
      }
      if (field('499').get() == 2) {
        field('498').assign(Math.max(0,
          field('402').cell(3, 3).get() - field('404').cell(3, 3).get()));
        allocate(field, ['429', '430', '431', '432'], '402', '404', '498', null, '428', '421');
      }
      else {
        field('429').disabled(false);
        field('430').disabled(false);
        field('431').disabled(false);
        field('432').disabled(false);
        if (!calcUtils.hasChanged('423') && calcUtils.hasChanged('499')) {
          calcUtils.removeValue(['429', '430', '431', '432'], false);
        }
      }
    });
    calcUtils.calc(function(calcUtils, field) {
      //part5
      //tax years ending in 2011-2013 are unsupported, so we assign 0
      field('501').assign(0);
      field('502').assign(0);
      field('503').assign(field('502').get() + field('501').get());
      //part6
    });
    calcUtils.calc(function(calcUtils, field) {
      field('604').fieldAssign(field('603'));
      field('609').fieldAssign(field('608'));
      calcUtils.subtract('610', '604', '609');
      field('611').fieldAssign(field('610'));
      if (field('613').get() > field('612').get()) {
        field('613').fieldAssign(field('612'));
        field('613').disabled(false);
      }
      calcUtils.subtract('614', '612', '613');
      field('615').fieldAssign(field('614'));
      calcUtils.sumBucketValues('616', ['611', '615']);
      calcUtils.divide('619', '618', '620');
      calcUtils.multiply('621', ['616', '619']);
      calcUtils.subtract('623', '621', '622');
      calcUtils.sumBucketValues('625', ['624', '623']);
      // section after part 6
      if (field('623')) {
        allocate(field, ['626', '627', '628', '629'], '600', '605', '614', '622', '623', '619');
      }
    });
    calcUtils.calc(function(calcUtils, field) {
      //post-part7
      //todo: num 1001 rf from prior year
      field('1050').assign(field('t2j.061').getKey('year'));
      field('1005').fieldAssign(field('625'));
      if (!angular.isDefined(field('1001').get()) || !angular.isDefined(field('1005').get()) || !angular.isDefined(field('1010').get()) || !angular.isDefined(field('1015').get()) || !angular.isDefined(field('1020').get()) || !angular.isDefined(field('1025').get())) {

      }

      if (field('108').get() == '2012' || field('108').get() == '2011') {
        //Year column
        field('1004').assign(Number(field('108').get()));
        field('1008').assign(Number(field('108').get()) + 1);
        field('1013').assign(Number(field('108').get()) + 2);
        field('1018').assign(Number(field('108').get()) + 3);
        field('1023').assign(Number(field('108').get()) + 4);
        field('1028').assign(Number(field('108').get()) + 5);
        // Specified percentage column
        calcUtils.getGlobalValue('1002', 'ratesFed', '840');
        calcUtils.getGlobalValue('1006', 'ratesFed', '841');
        calcUtils.getGlobalValue('1011', 'ratesFed', '842');
        calcUtils.getGlobalValue('1016', 'ratesFed', '843');
        calcUtils.getGlobalValue('1021', 'ratesFed', '844');
        calcUtils.getGlobalValue('1026', 'ratesFed', '845');
        // Reserve column
        calcUtils.multiply('1003', ['1001', '1002'], [1 / 100]);
        calcUtils.multiply('1007', ['1005', '1006'], [1 / 100]);
        calcUtils.multiply('1012', ['1010', '1011'], [1 / 100]);
        calcUtils.multiply('1017', ['1015', '1016'], [1 / 100]);
        calcUtils.multiply('1022', ['1020', '1021'], [1 / 100]);
        calcUtils.multiply('1027', ['1025', '1026'], [1 / 100]);
      }
      else {
        calcUtils.removeValue(['1001', '1002', '1003', '1004', '1005', '1006', '1007', '1008', '1009',
          '1011', '1012', '1013', '1014', '1016', '1017', '1018', '1019', '1021', '1022', '1023', '1024',
          '1026', '1027', '1028'], true, true);
      }
    });
    calcUtils.calc(function(calcUtils, field) {
      //part7
      if (angular.isDefined(field('1050').get()) && field('1050').get() != '') {
        var row = 0;
        var assigned = false;
        while (row < 6 && !assigned) {
          if (field('720').cell(row, 3).get() != 0) {
            field('701').fieldAssign(field('720').cell(row, 3));
            field('702').fieldAssign(field('720').cell(row, 5));
            assigned = true;
          }
          row++;
        }
      }
      calcUtils.multiply('704', ['701', '702'], (1 / 100));
      field('705').fieldAssign(field('780'));
      field('706').fieldAssign(field('625'));
      field('707').fieldAssign(field('504'));
      calcUtils.subtract('708', '706', '707');
      field('709').fieldAssign(field('708'));
      calcUtils.sumBucketValues('710', ['705', '709']);
      field('711').fieldAssign(field('710'));
      var num713 = 0;
      // field('713').assign(Math.max(0, getSum(field, ['t2j.371', //TODO: if condition may be necessary here to deal with pt4, check for line W
      //   '834', '780', '-434', '428', '-330', '318']))); //TODO subtract s3 dividends
      var num714 = Math.min(field('704').get(), field('711').get(), field('713').get());
      if (Math.min(field('704').get(), field('711').get(), field('713').get()) >= 0) {
        if (num714 == field('704').get()) {
          field('714').fieldAssign(field('704'));
        }
        else if (num714 == field('711').get()) {
          field('714').fieldAssign(field('711'));
        }
        else {
          field('714').fieldAssign(field('713'));
        }
      }
      else {
        field('714').assign(0);
      }
      field('715').fieldAssign(field('714'));
    });
    calcUtils.calc(function(calcUtils, field) {
      //part8
      field('804').fieldAssign(field('803'));
      field('808').fieldAssign(field('807'));
      calcUtils.subtract('809', '804', '808');
      field('810').fieldAssign(field('809'));
      calcUtils.subtract('813', '811', '812');
      field('814').fieldAssign(field('813'));
      calcUtils.sumBucketValues('815', ['810', '814']);
      calcUtils.divide('818', '817', '819');
      calcUtils.multiply('820', ['815', '818']);
      calcUtils.subtract('822', '820', '821');
      calcUtils.sumBucketValues('825', ['823', '824']);
      field('826').assign(field('825').get() < field('822').get() ? field('825').get() : field('822').get());
      field('827').assign(field('826').get() - field('823').get());
      calcUtils.multiply('829', ['827', '828']);
      calcUtils.multiply('831', ['829', '830']);
      calcUtils.multiply('832', ['826', '828']);
      calcUtils.multiply('833', ['832', '830']);
      calcUtils.getGlobalValue('836', 'ratesFed', '846');
      calcUtils.multiply('834', ['833', '836'], 1 / 100);
    });
    calcUtils.calc(function(calcUtils, field) {
      //Store values for s73 in fields
      field('tn270').assign(getSum(field, ['326', '-331', '429', '-435', '781', '-726']));
      field('tn275').assign(getSum(field, ['329', '432', '784']));
      field('tn280').assign(getSum(field, ['327', '-332', '430', '-436', '782', '-727']));
      field('tn285').assign(getSum(field, ['334', '438', '729']));
      field('tn290').assign(getSum(field, ['328', '-333', '431', '-437', '783', '-728']));
    });
  });
})();
