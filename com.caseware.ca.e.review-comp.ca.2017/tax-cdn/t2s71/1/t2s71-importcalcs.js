wpw.tax.create.importCalcs('T2S71', function(importTools) {
  function getImportObj(taxPrepIdsObj) {
    //get and sort by repeatId and key of taxPrepIdsObj
    var output = {};
    Object.keys(taxPrepIdsObj).forEach(function(key) {
      importTools.intercept(taxPrepIdsObj[key], function(importObj) {
        if (!output[importObj.repeatIndex]) {
          output[importObj.repeatIndex] = {};
        }
        output[importObj.repeatIndex][key] = importObj;
      });
    });
    return output;
  }

  var s71Obj = {
    info: getImportObj({
      qtiYear: 'REVSP[1].Ttwrev70'
    }),
    part4: getImportObj({
      busLineM: 'REVSP[2].Ttwrev149',
      propLineM: 'REVSP[2].Ttwrev150',
      tcgLineM: 'REVSP[2].Ttwrev151',
      otherLineM: 'REVSP[2].Ttwrev266',
      busLineL: 'REVSP[2].Ttwrev153',
      propLineL: 'REVSP[2].Ttwrev154',
      tcgLineL: 'REVSP[2].Ttwrev155',
      otherLineL: 'REVSP[2].Ttwrev267'
    })
  };

  importTools.after(function() {
    var s71;
    var rfId = 0;

    function getPropIfExists(obj, properties) {
      //recursively return either the object if some multilayer property exists, or an empty string
      //i.e. if foo[1][2][3], return it; if not, return ''
      properties = wpw.tax.utilities.ensureArray(properties, false);
      return properties.length < 1 ? (obj || '') : getPropIfExists((obj || '')[properties[0]], properties.splice(1));
    }

    while (rfId < importTools.allRepeatForms('T2S71').length) {
      s71 = importTools.form('T2S71', rfId);

      //QTI Year
      if (getPropIfExists(s71Obj, ['info', rfId, 'qtiYear', 'value'])) {
        var qtiYear = '201' + s71Obj['info'][rfId]['qtiYear']['value'].toString();
        s71.setValue('108', qtiYear.toString());
      }

      //Part 4
        var lineL = s71.getValue('401') || 0;
        var lineM = s71.getValue('423') || 0;
        var lineN = s71.getValue('428') || 0;

        if ((lineL > 0 && lineM > 0) && ((lineL < lineM) || (lineL == lineN))) {
          s71.setValue('499', '1');
          s71.setValue('429', getPropIfExists(s71Obj, ['part4', rfId, 'busLineL', 'value']) || 0);
          s71.setValue('430', getPropIfExists(s71Obj, ['part4', rfId, 'propLineL', 'value']) || 0);
          s71.setValue('432', getPropIfExists(s71Obj, ['part4', rfId, 'tcgLineL', 'value']) || 0);
          s71.setValue('431', getPropIfExists(s71Obj, ['part4', rfId, 'otherLineL', 'value']) || 0);
        }
        else {
          s71.setValue('499', '2');
          s71.setValue('429', getPropIfExists(s71Obj, ['part4', rfId, 'busLineM', 'value']) || 0);
          s71.setValue('430', getPropIfExists(s71Obj, ['part4', rfId, 'propLineM', 'value']) || 0);
          s71.setValue('432', getPropIfExists(s71Obj, ['part4', rfId, 'tcgLineM', 'value']) || 0);
          s71.setValue('431', getPropIfExists(s71Obj, ['part4', rfId, 'otherLineM', 'value']) || 0);
        }
      rfId++;
    }
  });
});