(function() {
  wpw.tax.create.diagnostics('t2s71', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S71'), common.bnCheck('102')));

  });
})();
