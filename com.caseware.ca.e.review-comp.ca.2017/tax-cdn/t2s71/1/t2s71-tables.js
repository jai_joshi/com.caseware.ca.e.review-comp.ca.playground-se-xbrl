(function() {

  function getIncomeLossCols(totalNum, indicator) {
    return [
      {
        "type": "none"
      },
      {
        "type": "none",
        colClass: 'std-input-col-width'
      },
      {
        "type": "none",
        colClass: 'std-input-width'
      },
      {
        "total": true,
        "totalNum": totalNum,
        colClass: 'std-input-width',
        filters: 'prepend $',
        totalIndicator: indicator
      },
      {
        colClass: 'std-padding-width',
        "type": "none"
      },
      {
        colClass: 'std-input-col-width',
        "type": "none"
      }
    ]
  }

  wpw.tax.global.tableCalculations.t2s71 = {
    "1000": {
      fixedRows: true,
      infoTable: true,
      num: '1000',
      columns: [
        {
          type: 'none'
        },
        {
          type: 'none',
          colClass: 'std-input-col-width'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          header: '<b>Fiscal period-start</b>',
          type: 'date',
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          header: '<b>Fiscal period-end</b>',
          type: 'date',
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        }
      ],
      cells: [
        {
          0: {label: 'If <b>yes</b>, give the dates of both the old and current fiscal periods of the partnership.'},
          1: {label: 'Old fiscal period'},
          3: {num: '104'},
          5: {num: '105'}
        },
        {
          0: {label: 'If <b>no</b>, give the start and end dates of the current fiscal period of the partnership.'},
          1: {label: 'Current fiscal period'},
          3: {num: '106'},
          5: {num: '107'}
        }
      ]
    },
    "221": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "columns": getIncomeLossCols('222', 'a'),
      'cells': [
        {
          '1': {'label': 'Income from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Income from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other income'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "225": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "columns": getIncomeLossCols('228', 'b'),
      'cells': [
        {
          '1': {'label': 'Loss from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Loss from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other losses'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "309": {

      "fixedRows": true,
      "infoTable": true,
      "num": "309",
      "columns": [
        {
          "width": "47%",
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [{
        "0": {
          "label": "Number of days that are in both the corporation's tax year and the fiscal period of the " +
          "partnership that starts in the corporation's tax year and ends after the tax year (the <b>stub period</b>)",
          cellClass: 'alignCenter singleUnderline'
        },
        "2": {
          "num": "310"
        },
        "3": {
          "label": " = ",
          cellClass: 'alignCenter'
        },
        "4": {
          "num": "311",
          decimals: 5
        },
        "5": {
          "label": "i",
          "labelClass": "fullLength text-left"
        }
      },
        {
          "0": {
            "label": "Number of days in the fiscal period(s) of the partnership that end(s) in the corporation's tax " +
            "year (note 3)",
            "labelClass": "fullLength center"
          },
          "2": {
            "num": "312"
          },
          "4": {
            type: 'none'
          }
        }]
    },
    "321": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "columns": getIncomeLossCols('322', 'e'),
      'cells': [
        {
          '1': {'label': 'Income from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Income from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other income'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "325": {

      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "325",
      "columns": getIncomeLossCols('336', 'f'),
      'cells': [
        {
          '1': {'label': 'Loss from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Loss from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other losses'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "402": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "columns": getIncomeLossCols('403'),
      'cells': [
        {
          '1': {'label': 'Income from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Income from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other income'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Taxable capital gain'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "404": {

      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "404",
      "columns": getIncomeLossCols('405'),
      'cells': [
        {
          '1': {'label': 'Loss from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Loss from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other losses'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Allowable capital loss'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    // combined with table 402 --> comp #1
    // "407": {
    //
    //   "infoTable": true,
    //   hasTotals: true,
    //   "maxLoop": 100000,
    //   "num": "407",
    //   "columns": [
    //     {
    //       "type": "none",
    //       "width": "10%"
    //     },
    //     {
    //       "header": "Type loss for eligible fiscal period",
    //       "type": "dropdown",
    //       "num": "408",
    //       "width": "35%",
    //       "options": [
    //         {
    //           "value": "1",
    //           "option": "1. Loss from business"
    //         },
    //         {
    //           "value": "2",
    //           "option": "2. Loss from property"
    //         },
    //         {
    //           "value": "3",
    //           "option": "3. Other eligible Loss"
    //         }
    //       ]
    //     },
    //     {
    //       "type": "none",
    //       "width": "10%"
    //     },
    //     {
    //       "header": "Amount",
    //       "num": "409",
    //       "total": true,
    //       "totalNum": "410",
    //       "width": "13%",
    filters: 'prepend $',
    //       "validator": "positive"
    //     },
    //     {
    //       "type": "none"
    //     }
    //   ]
    // },
    "419": {
      "fixedRows": true,
      "infoTable": true,
      "num": "419",
      "columns": [
        {
          "type": "none"
        },
        {
          colClass: 'std-padding-width', type: 'none'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width', type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width', type: 'none'
        },
        {
          colClass: 'std-input-col-width', type: 'none'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Number of days that are in both the corporation's tax year and the particular period",
            cellClass: 'alignCenter singleUnderline'
          },
          "2": {
            "num": "420",
            cellClass: 'singleUnderline'
          },
          "3": {
            "label": " = ",
            cellClass: 'alignCenter'
          },
          "4": {
            "num": "421",
            decimals: 5
          },
          "5": {
            "label": "m"
          }
        },
        {
          "0": {
            "label": "Number of days in the particular period",
            "labelClass": "fullLength center"
          },
          "2": {
            "num": "422"
          },
          "4": {
            type: 'none'
          }
        }
      ]
    },
    "600": {

      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "600",
      "columns": getIncomeLossCols('603'),
      'cells': [
        {
          '1': {'label': 'Income from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Income from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other income'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "605": {

      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "605",
      "columns": getIncomeLossCols('608'),
      'cells': [
        {
          '1': {'label': 'Loss from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Loss from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other losses'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "617": {

      "fixedRows": true,
      "infoTable": true,
      "num": "617",
      "columns": [
        {
          "width": "47%",
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [{
        "0": {
          "label": "Number of days that are in both the corporation's tax year (note 9) and the particular period of the partnership (note 8)",
          cellClass: 'alignCenter singleUnderline'
        },
        "2": {
          "num": "618",
          cellClass: 'singleUnderline'
        },
        "3": {
          "label": " = ",
          cellClass: 'alignCenter'
        },
        "4": {
          "num": "619",
          decimals: 5
        },
        "5": {
          "label": "r"
        }
      },
        {
          "0": {
            "label": "Number of days in the particular period of the partnership (note 8)",
            "labelClass": "fullLength center"
          },
          "2": {
            "num": "620"
          },
          "4": {
            type: 'none'
          }
        }]
    },
    "700": {

      "fixedRows": true,
      "infoTable": true,
      "num": "700",
      "columns": [
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-input-width',
          "type": "none",
          "textAlign": "right"
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'qtr-col-width',
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "left"
        }
      ],
      "cells": [{
        "1": {
          "label": "QTI (note 11)"
        },
        "2": {
          "num": "701",
          disabled: true,
          init: 0
        },
        "3": {
          "label": "× specified percentage (note 12)"
        },
        "4": {
          "num": "702",
          disabled: true,
          init: 0,
          "decimals": 2
        },
        "5": {
          "label": "% =",
          "labelClass": "bold"
        },
        "6": {
          "num": "703",
          "disabled": "true"
        },
        "7": {
          "num": "704"
        },
        "8": {
          "label": "AA"
        }
      }]
    },
    "800": {

      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "800",
      "columns": getIncomeLossCols('803'),
      'cells': [
        {
          '1': {'label': 'Income from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Income from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other income'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "805": {

      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "805",
      "columns": getIncomeLossCols('807'),
      'cells': [
        {
          '1': {'label': 'Loss from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Loss from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other loss'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "816": {

      "fixedRows": true,
      "infoTable": true,
      "num": "816",
      "columns": [
        {
          "width": "47%",
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [{
        "0": {
          "label": "Number of days that are in both the <b>base year</b> and the fiscal period of the partnership",
          cellClass: 'alignCenter singleUnderline'
        },
        "2": {
          "num": "817", cellClass: 'singleUnderline'
        },
        "3": {
          "label": " = ",
          cellClass: 'alignCenter'
        },
        "4": {
          "num": "818",
          decimals: 5
        },
        "5": {
          "label": "y",
          "labelClass": "fullLength text-left"
        }
      },
        {
          "0": {
            "label": "Number of days in the fiscal period of the partnership",
            "labelClass": "fullLength center"
          },
          "2": {
            "num": "819",
            "init": "0"
          },
          "4": {
            type: 'none'
          }
        }]
    },
    "835": {

      "fixedRows": true,
      "infoTable": true,
      "num": "835",
      "columns": [
        {
          "type": "none",
          colClass: 'std-input-col-width',
          "textAlign": "left"
        },
        {
          colClass: 'small-input-width'
        },
        {
          "type": "none"
        },
        {
          "type": "none"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        }],
      "cells": [{
        "0": {
          "label": "<b>Threshold amount</b> (",
          "labelClass": "right fullLength"
        },
        "1": {
          "num": "836",
          "decimals": 2
        },
        "2": {
          "label": '% of amount cc) (if negative, enter "0")',
          cellClass: 'alignLeft'
        },
        "4": {
          "num": "834"
        },
        "5": {
          "label": "SS"
        }
      }]
    },
    "720": {
      "fixedRows": true,
      "num": "720",
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          "alignText": "center"
        },
        {
          "header": 'Year',
          colClass: 'std-input-width',
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          "header": 'QTI'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "alignText": "center"
        },
        {
          "header": 'Specified percentage'
        },
        {
          colClass: 'qtr-col-width',
          "type": "none",
          "alignText": "center"
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "header": "Deductible portion"
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        }
      ],
      "cells": [
        {
          "0": {"label": "Year the reserve was created"},
          "1": {"num": "1004"},
          "3": {"num": "1001", 'init': 0},
          "4": {
            "label": "x",
            "labelClass": "center"
          },
          "5": {"num": "1002"},
          "6": {"label": "% ="},
          "8": {"num": "1003"},
          "9": {"label": ""}
        },
        {
          "0": {"label": "1st subsequent year"},
          "1": {"num": "1008"},
          "3": {"num": "1005", 'init': 0},
          "4": {
            "label": "x",
            "labelClass": "center"
          },
          "5": {"num": "1006"},
          "6": {"label": "% ="},
          "8": {"num": "1007"},
          "9": {"label": ""}
        },
        {
          "0": {"label": "2nd subsequent year"},
          "1": {"num": "1013"},
          "3": {"num": "1010", 'init': 0},
          "4": {
            "label": "x",
            "labelClass": "center"
          },
          "5": {"num": "1011"},
          "6": {"label": "% ="},
          "8": {"num": "1012"},
          "9": {"label": ""}
        },
        {
          "0": {"label": "3rd subsequent year"},
          "1": {"num": "1018"},
          "3": {"num": "1015", 'init': 0},
          "4": {
            "label": "x",
            "labelClass": "center"
          },
          "5": {"num": "1016"},
          "6": {"label": "% ="},
          "8": {"num": "1017"},
          "9": {"label": ""}
        },
        {
          "0": {"label": "4th subsequent year"},
          "1": {"num": "1023"},
          "3": {"num": "1020", 'init': 0},
          "4": {
            "label": "x",
            "labelClass": "center"
          },
          "5": {"num": "1021"},
          "6": {"label": "% ="},
          "8": {"num": "1022"},
          "9": {"label": ""}
        },
        {
          "0": {"label": "5th subsequent year"},
          "1": {"num": "1028"},
          "3": {"num": "1025", 'init': 0},
          "4": {
            "label": "x",
            "labelClass": "center"
          },
          "5": {"num": "1026"},
          "6": {"label": "% ="},
          "8": {"num": "1027"},
          "9": {"label": ""}
        },
        {
          "0": {'label': '&nbsp'},
          "1": {'type': 'none'},
          "3": {'type': 'none'},
          "4": {'type': 'none'},
          "5": {'type': 'none'},
          "6": {'type': 'none'},
          "8": {'type': 'none'},
          "9": {'type': 'none'}
        },
        {
          "0": {"label": "Corporation's current tax year"},
          "1": {"num": "1050"},
          "3": {'type': 'none'},
          "4": {'type': 'none'},
          "5": {'type': 'none'},
          "6": {'type': 'none'},
          "8": {'type': 'none'},
          "9": {'type': 'none'}
        }
      ]
    }
  }
})();