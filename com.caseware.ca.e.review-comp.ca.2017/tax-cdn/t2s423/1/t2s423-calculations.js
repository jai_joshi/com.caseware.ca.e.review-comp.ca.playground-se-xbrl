(function() {
  function workChartCalc(calcUtils, tableNum) {
    var field = calcUtils.field;

    field(tableNum).getRows().forEach(function(row) {
      row[6].assign(row[5].get() == 0 ? 0 : row[3].get() * (row[4].get() / row[5].get()))
    });
  }

  function episodicProductionCalc(calcUtils, fieldId, workChart1, workChart2) {
    var field = calcUtils.field;
    field(fieldId).assign(field(workChart1).total(6).get() + field(workChart2).total(6).get())
  }

  function applicableValue(calcUtils, num1, num2) {
    var field = calcUtils.field;
    var value = [];
    if (field(num1).get() > 0) {
      value = field(num1).get()
    }
    else if (field(num1).get() == undefined && field(num2).get() == undefined) {
      value = field(num1).get() + field(num2).get()
    }
    else {
      value = field(num2).get()
    }
    return value;
  }

  function enableFields(calcUtils, numArray) {
    numArray.forEach(function(fieldId) {
      calcUtils.field(fieldId).disabled(false)
    })
  }

  function calcRates(field, storeNum, numeratorNum, denominatorNum) {
    var result = 0;
    if (field(denominatorNum).get() !== 0) {
      result = field(numeratorNum).get() / field(denominatorNum).get()
    }
    field(storeNum).assign(result)
  }

  wpw.tax.create.calcBlocks('t2s423', function(calcUtils) {
    calcUtils.calc(function(calcUtils) {
      var num = '600';
      var num2 = '601';
      var midnum = '600Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '700';
      var num2 = '701';
      var midnum = '700Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '900';
      var num2 = '901';
      var midnum = '900Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '710';
      var num2 = '711';
      var midnum = '910Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils, field) {
      //update rates
      field('600Mult').assign(33);
      field('700Mult').assign(28);
      field('900Mult').assign(17.5);
      field('910Mult').assign(16);
    });
    calcUtils.calc(function(calcUtils, field) {
      //eligibility
      if (field('220').get() == 1 ||
          field('222').get() == 1 ||
          field('230').get() == 1 ||
          field('235').get() == 1 ||
          field('240').get() == 1 ||
          field('245').get() == 1 ||
          field('250').get() == 1) {
        field('251').assign(2);
        calcUtils.removeValue([405, 406, 815, 835, 420, 425, 430, 432], true);
      }
      else {
        field('251').assign(1);
        enableFields(calcUtils, [405, 406, 815, 835, 420, 425, 430, 432])
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 1 calcs
      field('151').assign(field('T2J.951').get() + ' ' + field('T2J.950').get());
      field('153').assign(field('T2J.956').get());
    });

    calcUtils.calc(function(calcUtils, field) {
      //part 4
      calcUtils.sumBucketValues('437', ['420', '425', '430', '432']);
      field('438').assign(field('437').get());
      calcUtils.sumBucketValues('490', ['405', '438', '435']);
      calcUtils.sumBucketValues('505', ['490', '499']);
      calcUtils.sumBucketValues('527', ['520', '525', '530']);
      field('528').assign(field('527').get());
      field('610').assign(Math.max(field('505').get() - field('528').get(), 0));
    });
    calcUtils.calc(function(calcUtils, field) {
      //part 5
      calcUtils.sumBucketValues('434', ['421', '426', '431', '433']);
      field('4341').assign(field('434').get());
      calcUtils.sumBucketValues('491', ['406', '4341', '436']);
      calcUtils.sumBucketValues('506', ['491', '492']);
      calcUtils.sumBucketValues('532', ['521', '526', '531']);
      field('533').assign(field('532').get());
      field('592').assign(Math.max(field('506').get() - field('533').get(), 0));
    });
    calcUtils.calc(function(calcUtils, field) {
      //part 6
      var line1 = calcUtils.dateCompare.lessThan(field('302').get(), wpw.tax.date(2016, 10, 1));
      field('600').assign(line1 ? field('610').get() : 0);
      field('700').assign(!line1 ? field('610').get() : 0);
      calcUtils.getApplicableValue('800', ['601', '701']);
    });
    calcUtils.calc(function(calcUtils, field) {
      //part 7
      episodicProductionCalc(calcUtils, '815', '950', '960');
      field('816').assign(field('610').get());
      calcRates(field, '821', '820', '825');
      calcRates(field, '823', '822', '827');
      var applicableACBLE = applicableValue(calcUtils, '821', '823');
      field('828').assign(field('816').get() * applicableACBLE);
      field('829').assign(field('828').get());
      var applicableRegional = applicableValue(calcUtils, '815', '829');
      field('830').assign(applicableRegional * 0.06);
    });
    calcUtils.calc(function(calcUtils, field) {
      //part 8
      episodicProductionCalc(calcUtils, '835', '970', '980');
      field('836').assign(field('610').get());
      calcRates(field, '8391', '839', '841');
      calcRates(field, '8401', '840', '842');
      var qualifiedExpense = applicableValue(calcUtils, '8391', '8401');
      field('843').assign(field('836').get() * qualifiedExpense);
      field('824').assign(field('843').get());
      var distanceLocationCredit = applicableValue(calcUtils, '835', '824');
      field('845').assign(distanceLocationCredit * 0.06)
    });
    calcUtils.calc(function(calcUtils, field) {
      //part 9
      var daveTaxCredit = calcUtils.dateCompare.lessThan(field('302').get(), wpw.tax.date(2016, 10, 1));
      field('900').assign(daveTaxCredit ? field('592').get() : 0);
      field('710').assign(!daveTaxCredit ? field('592').get() : 0);
      calcUtils.getApplicableValue('805', ['901', '711']);
    });
    calcUtils.calc(function(calcUtils, field, form) {
      //part 10
      field('846').assign(field('800').get());
      field('847').assign(field('830').get());
      field('848').assign(field('845').get());
      field('849').assign(field('805').get());
      calcUtils.sumBucketValues('850', ['846', '847', '848', '849'])
    });
    calcUtils.calc(function(calcUtils, field, form) {
      //worksheet
      workChartCalc(calcUtils, '950');
      workChartCalc(calcUtils, '960');
      workChartCalc(calcUtils, '970');
      workChartCalc(calcUtils, '980')
    });
  });
})();
