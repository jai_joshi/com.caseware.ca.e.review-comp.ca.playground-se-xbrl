(function() {

  function getPart7Table(tableObj) {
    return {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {type: 'none'},
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {colClass: 'std-input-width'},
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {colClass: 'std-input-width'},
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      cells: [
        {
          '0': {
            label: tableObj.label[0],
            cellClass: 'alignCenter singleUnderline'
          },
          '2': {
            tn: tableObj.tn[0]
          },
          '3': {
            num: tableObj.num[0],
            // "validate": {"or": [{"matches": "^-?[.\\d]{1,3}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline',
            formField: true
          },
          '4': {
            label: '='
          },
          '5': {
            num: tableObj.num[1],
            decimals: 5
          },
          '6': {
            label: tableObj.indicator
          }
        },
        {
          '0': {
            label: tableObj.label[1],
            labelClass: 'center'
          },
          '2': {
            tn: tableObj.tn[1]
          },
          '3': {
            num: tableObj.num[2],
            formField: true
            // , "validate": {"or": [{"matches": "^-?[.\\d]{1,3}$"}, {"check": "isEmpty"}]}
          },
          '5': {
            type: 'none'
          }
        }
      ]
    }
  }

  wpw.tax.create.tables('t2s423', {
    'di_table_1': {
      'num': 'di_table_1',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': '<b>Production services tax credit</b>: amount H from Part 4 (for productions that started principal photography prior to October 1, 2016)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '600'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '600Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '601',
          },
          '8': {
            'label': '1'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_2': {
      'num': 'di_table_2',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': '<b>Production services tax credit</b>: amount H from Part 4 (for productions that started principal photography  after September 30, 2016)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '700'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '700Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '701'
          },
          '8': {
            'label': '2'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_3': {
      'num': 'di_table_3',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': '<b>DAVE tax credit:</b>  amount P from Part 5 <br>(for productions that started principal photography prior to October 1, 2016) ',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '900',
            'decimals': 1
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '900Mult',
            'init': undefined,
            decimals: 1
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '901',
            'decimals': 1
          },
          '8': {
            'label': '1'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_4': {
      'num': 'di_table_4',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': '<b>DAVE tax credit:</b>amount P from Part 5 <br>(for productions that started principal photography after September 30, 2016)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '710'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '910Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '711'
          },
          '8': {
            'label': '2'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    '105': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1],
      'columns': [
        {
          'width': '60%',
          cellClass: 'alignLeft'
        },
        {
          cellClass: 'alignLeft'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Name of person to contact for more information',
            'num': '151', "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
            'tn': '151',
            type: 'text'
          },
          '1': {
            'label': 'Telephone number including area code',
            'num': '153',
            'tn': '153',
            type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
            validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
          }
        }
      ]
    },
    '106': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          type: 'custom',
          format: ['{N5}']
        }
      ],
      cells: [
        {
          '0': {
            tn: '303'
          },
          '1': {
            label: 'PS'
          },
          '2': {
            num: '303', "validate": {"or": [{"matches": "^-?[.\\d]{5,5}$"}, {"check": "isEmpty"}]}
          }
        }
      ]
    },
    '810': getPart7Table({
      label: ['Number of principal photography days* <br> outside the designated Vancouver area', 'Total number of days*'],
      num: ['820', '821', '825'],
      tn: ['820', '825'],
      indicator: 'p'
    }),
    '860': getPart7Table({
      label: ['ABCLE incurred outside the designated <br> Vancouver area for the tax year', 'ABCLE for the tax year'],
      num: ['822', '823', '827'],
      tn: ['822', '827'],
      indicator: 'q'
    }),

    '870': getPart7Table({
      label: ['Number of principal photography days* in a distant location', 'Total number days*'],
      num: ['839', '8391', '841'],
      tn: ['839', '841'],
      indicator: 's'
    }),

    '880': getPart7Table({
      label: ['ABCLE in a distant location for the tax year', 'ABCLE for the tax year'],
      num: ['840', '8401', '842'],
      tn: ['840', '842'],
      indicator: 't'
    }),

    '950': {
      hasTotals: true,
      columns: [
        {
          header: 'Episode number',
          width: '80px',
          type: 'text'
        },
        {
          header: 'Episode title',
          type: 'text',
          width: '300px'
        },
        {
          header: 'Principal photography start date',
          type: 'date',
          width: '153px'
        },
        {
          header: 'AQBCLE',
          width: '100px'
        },
        {
          header: 'RD',
          width: '70px'
        },
        {
          header: 'TD',
          width: '70px'
        },
        {
          header: 'Prorated AQBCLE <br>(AQBCLE x RD/TD)',
          total: true,
          totalMessage: 'Total prorated AQBCLE <br>(enter on line 815 in Part 7)',
          totalIndicator: '1'
        }
      ]
    },
    '960': {
      hasTotals: true,
      columns: [
        {
          header: 'Episode number',
          width: '80px',
          type: 'text'
        },
        {
          header: 'Episode title',
          type: 'text',
          width: '300px'
        },
        {
          header: 'Key animation start date',
          type: 'date',
          width: '153px'
        },
        {
          header: 'AQBCLE',
          width: '100px'
        },
        {
          header: 'RLE',
          width: '70px'
        },
        {
          header: 'TLE',
          width: '70px'
        },
        {
          header: 'Prorated AQBCLE <br>(AQBCLE x RLE/TLE)',
          total: true,
          totalMessage: 'Total prorated AQBCLE <br> (enter on line 815 in Part 7)',
          totalIndicator: '2'
        }
      ]
    },
    '970': {
      hasTotals: true,
      columns: [
        {
          header: 'Episode number',
          width: '80px',
          type: 'text'
        },
        {
          header: 'Episode title',
          type: 'text',
          width: '300px'
        },
        {
          header: 'Principal photography start date ',
          type: 'date',
          width: '153px'
        },
        {
          header: 'AQBCLE',
          width: '100px'
        },
        {
          header: 'DLD',
          width: '70px'
        },
        {
          header: 'TD',
          width: '70px'
        },
        {
          header: 'Prorated AQBCLE <br>(AQBCLE x DLD/TD)',
          total: true,
          totalMessage: 'Total prorated AQBCLE <br> (enter on line 835 in Part 8)',
          totalIndicator: '3'
        }
      ]
    },
    '980': {
      hasTotals: true,
      columns: [
        {
          header: 'Episode number',
          width: '80px',
          type: 'text'
        },
        {
          header: 'Episode title',
          type: 'text',
          width: '300px'
        },
        {
          header: 'Key animation start date',
          type: 'date',
          width: '153px'
        },
        {
          header: 'AQBCLE',
          width: '100px'
        },
        {
          header: 'DLLE',
          width: '70px'
        },
        {
          header: 'TLE',
          width: '70px'
        },
        {
          header: 'Prorated AQBCLE <br>(AQBCLE x DLLE/TLE)',
          total: true,
          totalMessage: 'Total prorated AQBCLE <br> (enter on line 835 in Part 8)',
          totalIndicator: '4'
        }
      ]
    }

  })
})();
