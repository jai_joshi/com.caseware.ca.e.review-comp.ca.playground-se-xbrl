(function() {

  wpw.tax.create.formData('t2s423', {
    'formInfo': {
      abbreviation: 't2s423',
      'title': 'British Columbia Production Services Tax Credit',
      schedule: 'Schedule 423',
      isRepeatForm: true,
      repeatFormData: {
        titleNum: '301'
      },
      'code': 'Code 1502',
      headerImage: 'canada-federal',
      'showCorpInfo': true,
      agencyUseOnlyBox: {
        text: 'Code number ',
        textAbove: '',
        tn: '423',
        agencyBoxClass: 'cra-box-small',
        headerClass: 'header-margin',
        textClass: 'text-margin',
        tnPosition: 'none'
      },
      formFooterNum: 'T1197 E (16)',
      'description': [
        {
          type: 'list',
          items: [
            {
              label: 'Use this form to claim the following credits under the <i>Income Tax Act</i> (British Columbia):',
              sublist: [
                'production services tax credit (section 82.1), complete parts 1, 2, 3, 4, 6, and 10;',
                'regional production services tax credit (section 82.2), complete Part 7;',
                'distant location production services tax credit (section 82.21), complete Part 8; and',
                'digital animation, visual effects and post production services tax credit (section 82.3),' +
                ' complete parts 5 and 9.'
              ]
            },
            {
              label: 'To claim any of the above credits, include the following with the <i>T2 Corporation Income' +
              ' Tax Return:</i>',
              sublist: [
                'accreditation certificate (or a copy); and',
                'a completed copy of this form for each accredited production. We consider each episode in a series' +
                ' to be a production.However, we will accept one form for episodes in ' +
                'a series that are accredited productions.'
              ]
            }
          ]
        }
      ],
      category: 'British Columbia Forms'
    },
    'sections': [
      {
        hideFieldset: true,
        'rows': [
          {labelClass: 'fullLength'},
          {
            label: '<i>Freedom of Information and Protection of Privacy Act (FOIPPA)</i>',
            labelClass: 'fullLength bold'
          },
          {
            'label': 'The personal information on this form is collected for the purpose of administering the' +
            ' <i>Income Tax Act</i> (British Columbia) under the authority of paragraph 26(a) of the FOIPPA. Questions ' +
            'about the collection or use of this information can be directed to the Manager, Intergovernmental ' +
            'Relations, PO Box 9444 Stn Prov Govt, Victoria BC V8W 9W8. (Telephone: Victoria at <b>250-387-3332</b> or' +
            ' toll-free at <b>1-877-387-3332</b> and ask to be re-directed). <br>Email: ITBTaxQuestions@gov.bc.ca',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'}
        ]
      },
      {
        'header': 'Part 1 - Contact information',
        'rows': [
          {
            'type': 'table',
            'num': '105'
          }
        ]
      },
      {
        'header': 'Part 2 - Identifying the film or video production',
        'rows': [
          {
            'type': 'splitTable',
            'fieldAlignRight': true,
            'side1': [
              {
                'label': 'Title of production'
              },
              {
                'type': 'infoField',
                'num': '301',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '175'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '301'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Title of production (from accreditation certificate if different than line 301)',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'num': '304',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '175'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '304'
              }
            ],
            'side2': [
              {
                'label': 'Date principal photography began',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'inputType': 'date',
                'num': '302',
                'tn': '302',
                'labelWidth': '65%'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Accreditation certificate number',
                'labelClass': 'fullLength'
              },
              {
                type: 'table',
                num: '106'
              }
            ]
          },
          {'type': 'horizontalLine'}
        ]
      },
      {
        'header': 'Part 3 - Eligibility',
        'fieldAlignRight': true,
        'rows': [
          {
            'label': 'Was the corporation at any time in the tax year controlled directly or indirectly in any way by one or more persons, all or part of whose taxable income was exempt from tax under section 27 of the <i>Income Tax Act</i> (British Columbia) or Part I of the federal <i>Income Tax Act</i>?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '220',
            'tn': '220',
            'init': '1'
          },
          {
            'label': 'Was all or part of the corporation\'s taxable income at any time in the tax year exempt from tax under section 27 of the <i>Income Tax Act</i> (British Columbia) or Part I of the federal Act?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '222',
            'tn': '222',
            'init': '2'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Was the corporation at any time in the tax year:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'a) a prescribed labour-sponsored venture capital corporation for the purposes of section 127.4 of the federal Act?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '230',
            'tn': '230',
            'labelClass': 'tabbed2',
            'init': '2'
          },
          {
            'label': 'b) a small business venture capital corporation registered under section 3 of the<i> Small Business Venture Capital Act</i>?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '235',
            'tn': '235',
            'labelClass': 'tabbed2',
            'init': '2'
          },
          {
            'label': 'c) a corporation that has an employee share ownership plan registered under section 2 of the<i> Employee Investment Act</i>?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '240',
            'tn': '240',
            'labelClass': 'tabbed2',
            'init': '2'
          },
          {
            'label': 'd) registered as an employee venture capital corporation under section 8 of the<i> Employee Investment Act</i>?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '245',
            'tn': '245',
            'labelClass': 'tabbed2',
            'init': '2'
          },
          {
            'label': 'Has the corporation claimed a British Columbia film and television tax credit for this production?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '250',
            'tn': '250',
            'init': '2'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If you answered <b>yes</b> to any of the above questions, <b>you are not eligible</b> for a British Columbia production services tax credit.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'label': 'Is the corporation eligible for the British production services tax credit',
            'inputType': 'radio',
            'num': '251'
          }
        ]
      },
      {
        'header': 'Part 4 - Accredited qualified BC labour expenditure',
        'rows': [
          {
            'label': 'Accredited BC labour expenditure for the tax year includes amounts:',
            'labelClass': 'fullLength'
          },
          {
            'label': '• incurred after the end of final script stage to the end of post-production;',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• incurred in the tax year or the previous tax year and that was not part of the claimant\'s accredited BC labour expenditure for the previous tax year;',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• paid during the tax year or within 60 days of the end of the tax year;',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• that are directly attributable to the production; and',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• that are for services provided by BC-based individuals and rendered in British Columbia.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'A BC-based individual is defined as an individual who was resident in British Columbia on December 31 of the year before the end of the tax year for which the corporation claims this credit. ',
            'labelClass': 'fullLength'
          },
          {
            'label': 'An accredited BC labour expenditure does not include amounts paid that are included in a British Columbia interactive digital media tax credit claim.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Accredited BC labour expenditure for the tax year</b> is the total of:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Salary or wages paid that are directly attributable to the production',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '405',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '405'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Remuneration directly attributable to the production paid to:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- BC-based individuals',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '420',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '420'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': '- taxable Canadian corporations (owned only by a BC-based individual)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '425',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '425'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': '- other taxable Canadian corporations (for their BC-based employees)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '430',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '430'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': '- partnerships carrying on business in Canada (for their BC-based members or employees)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '432',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '432'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts a to d)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '437'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '438'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Labour expenditure that would have qualified as a BC labour expenditure transferred under a reimbursement agreement by the corporation, a wholly owned subsidiary, to the parent corporation that is a taxable Canadian corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '435',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '435'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Accredited BC labour expenditure for the current tax year (total of amounts A to C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '490',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '490'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Accredited BC labour expenditure for the previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '499'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Accredited BC labour expenditure for the current and previous tax years (amount D <b>plus</b> amount E)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '505',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '505'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'All government and non-government assistance that the corporation has not repaid and can be reasonably considered to be attributable to an accredited BC labour expenditure',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '520',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '520'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': 'All accredited qualified BC labour expenditures claimed in previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '525',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '525'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'label': 'Accredited BC labour expenditure transferred under a reimbursement agreement by the parent corporation, that is a taxable Canadian corporation, to the corporation, a wholly owned subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '530',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '530'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'g'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts e to g)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '527'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '528'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': '<b> Accredited qualified BC labour expenditure for the tax year</b> (amount F <b>minus</b> amount G)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '610',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '610'
                }
              }
            ],
            'indicator': 'H'
          }
        ]
      },
      {
        'header': 'Part 5 - Accredited qualified BC labour expenditure directly attributable to digital animation, visual effects and post-production activities (DAVE) ',
        'rows': [
          {
            'label': 'DAVE activities include prescribed digital animation, or visual effects activities. If principal photography began after February 28, 2015, <br> DAVE activities are expanded to include prescribed digital post-production activities. ',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Accredited BC labour expenditure directly attributable to DAVE activities for the tax year</b> is the total of:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Salary or wages paid that are directly attributable to the production\'s DAVE activities',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '406',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '406'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Remuneration directly attributable to the production\'s DAVE activities paid to:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- BC-based individuals',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '421',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '421'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'h'
                }
              }
            ]
          },
          {
            'label': '- taxable Canadian corporations (owned only by a BC-based individual)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '426',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '426'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'i'
                }
              }
            ]
          },
          {
            'label': '- other taxable Canadian corporations (for their BC-based employees)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '431',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '431'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'j'
                }
              }
            ]
          },
          {
            'label': '- partnerships carrying on business in Canada (for their BC-based members or employees)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '433',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '433'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'k'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts h to k)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '434'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '4341'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Labour expenditure that would have qualified as a DAVE BC labour expenditure transferred under a reimbursement agreement by the corporation, a wholly owned subsidiary, to the parent corporation that is a taxable Canadian corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '436',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '436'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': 'Accredited DAVE BC labour expenditure for the current tax year (total of amounts I to K) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '491',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '491'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': 'Accredited DAVE BC labour expenditure for the previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '492'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'label': 'Accredited DAVE BC labour expenditure for the current and previous tax years (amount L <b>plus</b> amount M)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '506',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '506'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'All government and non-government assistance that the corporation has not repaid and can be reasonably considered to be attributable to accredited DAVE BC labour expenditure',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '521',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '521'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'l'
                }
              }
            ]
          },
          {
            'label': 'All accredited DAVE qualified BC labour expenditure claimed in previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '526',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '526'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'm'
                }
              }
            ]
          },
          {
            'label': 'Accredited DAVE BC labour expenditure transferred under a reimbursement agreement by the parent corporation, that is a taxable Canadian corporation, to the corporation, a wholly owned subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '531',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '531'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'n'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts l to n)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '532'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '533'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'label': '<b> Accredited qualified BC labour expenditure directly attributable to DAVE activities for the tax year</b> <br> (amount N <b>minus</b> amount O)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '592',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '592'
                }
              }
            ],
            'indicator': 'P'
          }
        ]
      },
      {
        'header': 'Part 6 - Production services tax credit',
        'rows': [
          {
            'label': 'The basic production services tax credit rate is 28% for productions that start principal photography after September 30, 2016. For episodic productions that start principal photography of the first eligible episode before October 1, 2016, the basic production services tax credit rate will remain at 33% for all other eligible episodes in that cycle.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Use line 1, if principal photography for the production or first eligible episode of a series started prior to October 1, 2016.<br> Use line 2, if principal photography for the production or first eligible episode of a series started after September 30, 2016',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': 'di_table_1'
          },
          {
            'type': 'table',
            'num': 'di_table_2'
          },
          {
            'label': '<b>Production services tax credit</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '800',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '800'
                }
              }
            ],
            'indicator': 'Q'
          },
          {
            'label': '(Use either line 1 or line 2 depending on the production\'s principal photography start date, but not both) <br> (Enter amount Q at line Y in part 10)',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 7 - Regional production services tax credit',
        'rows': [
          {
            'label': 'To be eligible for a regional production services tax credit, principal photography of the production or of each episode for an episodic production must be done in British Columbia outside of the designated Vancouver area for a minimum of five days and must be more than 50% of the total number of principal photography days in British Columbia. For animated productions that start key animation after June 26, 2015, there is no minimum number or percentage of principal photography days required. Claim the regional production services tax credit on the accredited qualified BC labour expenditure (AQBCLE) for the tax year prorated by the accredited BC labour expenditure (ABCLE) incurred in B.C. outside the designated Vancouver area over the accredited BC labour expenditure for the tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Episodic production</b> (complete Worksheet 1, and/or Worksheet 2, line 815 and line 830)',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Accredited qualified BC labour expenditure for regional credit <br> (amount from Worksheet 1, line 1, <b>plus</b> amount from Worksheet 2, line 2 if applicable)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '815',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '815'
                }
              }
            ],
            'indicator': 'R'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Other production</b>'
          },
          {
            'label': 'Accredited qualified BC labour expenditure for the tax year<br> (amount H from Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '816'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'o'
                }
              }
            ]
          },
          {
            'label': 'For animated productions that start key animation before June 27, 2015, and for live action productions:',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '810'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For animated productions that start key animation after June 26, 2015:',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '860'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Prorated accredited qualified BC labour expenditure <br> (amount o <b>multiplied by</b> amount p or amount q whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '828'
                }
              },
              {
                'input': {
                  'num': '829'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'S'
          },
          {
            'label': '<b>Regional production services tax credit</b> (amount R or amount S <b>multiplied by</b> 6%) <br>(Enter amount T at line Z in Part 10)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '830',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '830'
                }
              }
            ],
            'indicator': 'T'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Principal photography days of the eligible production or qualifying episodes in British Columbia',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 8 - Distant location production services tax credit',
        'rows': [
          {
            'label': 'To be eligible for a distant location production services tax credit, principal photography of the production or of each episode for an episodic production must be done in British Columbia in a distant location for a minimum of one day. For animated productions that start key animation after June 26, 2015, there is no minimum number or percentage of principal photography days required. Claim the distant location production services tax credit on the accredited qualified BC labour expenditure (AQBCLE) for the tax year prorated by the accredited BC labour expenditure (ABCLE) incurred in a distant location over the accredited BC labour expenditure for the tax year. ',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Episodic production</b> (complete Worksheet 3, and/or worksheet 4, line 835 and line 845)',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Accredited qualified BC labour expenditure for distant location credit <br> (amount from Worksheet 3, line 3 plus amount from Worksheet 4, line 4 if applicable)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '835',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '835'
                }
              }
            ],
            'indicator': 'U'
          },
          {
            'label': '<b>Other production</b>'
          },
          {
            'label': 'Accredited qualified BC labour expenditure for the tax year <br> (amount H from Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '836'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'r'
                }
              }
            ]
          },
          {
            'label': 'For animated productions that start key animation before June 27, 2015, and for live action productions:',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '870'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For animated productions that start key animation after June 26, 2015:',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '880'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Prorated accredited qualified BC labour expenditure <br> (amount r <b>multiplied by</b> amount s or amount t whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '843'
                }
              },
              {
                'input': {
                  'num': '824'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'V'
          },
          {
            'label': '<b>Distant location production services tax credit</b> (amount U or amount V <b>multiplied by</b> 6%) <br>(Enter amount W at line AA in Part 10)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '845',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '845'
                }
              }
            ],
            'indicator': 'W'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Principal photography days of the eligible production or qualifying episodes in British Columbia',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 9 - Digital animation, visual effects and post-production services tax credit',
        'forceBreakAfter': true,
        'rows': [
          {
            'label': 'The DAVE production services tax credit rate is 16% for productions that start principal photography after September 30, 2016. For episodic productions that start principal photography of the first eligible episode before October 1, 2016, the DAVE production services tax credit rate will remain at 17.5% for all other eligible episodes in that cycle',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Use line 1, if principal photography for the production or first eligible episode of a series started prior to October 1, 2016<br> Use line 2, if principal photography for the production or first eligible episode of a series started after September 30, 2016',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': 'di_table_3'
          },
          {
            'type': 'table',
            'num': 'di_table_4'
          },
          {
            'label': '<b>Digital animation, visual effects and post-production services tax credit</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '805',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '805'
                }
              }
            ],
            'indicator': 'X'
          },
          {
            'label': '(Use either line 1 or line 2 depending on the production\'s principal photography start date, but not both) <br> (Enter amount X at line BB in Part 10)',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 10 - British Columbia production services tax credit',
        'rows': [
          {
            'label': 'Production services tax credit (amount Q from Part 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '846'
                }
              }
            ],
            'indicator': 'Y'
          },
          {
            'label': 'Regional production services tax credit (amount T from Part 7)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '847'
                }
              }
            ],
            'indicator': 'Z'
          },
          {
            'label': 'Distant location production services tax credit (amount W from Part 8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '848'
                }
              }
            ],
            'indicator': 'AA'
          },
          {
            'label': 'Digital animation, visual effects, and post-production services tax credit (amount X from Part 9)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '849'
                }
              }
            ],
            'indicator': 'BB'
          },
          {
            'label': '<b>British Columbia production services tax credit</b> (total of amounts Y to BB)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '850',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ],
            'indicator': 'CC'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter amount CC on line 672 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>. If you are filing more than one of these forms, add all CC amounts from all of the forms and enter the total on line 672 of Schedule 5',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'BC Production Services Tax Credit Work Chart',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Worksheet 1: Regional production services tax credit for episodic productions',
            'labelClass': 'fullLength titleFont center'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>AQBCLE</b> - Total accredited qualified BC labour expenditure for that episode** <br><br> <b>RD</b> - Total number of principal photography days in British Columbia outside the designated Vancouver area for that episode <br><br><b>TD</b> - Total number of principal photography days in British Columbia for that episode',
            'labelClass': 'fullLength tabbed'
          },
          {
            'type': 'table',
            'num': '950'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '** Use this worksheet for all live action episodic productions and only animated episodic productions that start key animation prior to June 27, 2015. Use worksheet 2 for animated episodic productions that start key animation after June 26, 2015',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Worksheet 2: Regional production services tax credit for animated episodic productions that start key animation after June 26, 2015',
            'labelClass': 'fullLength titleFont center'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>AQBCLE</b> – Total accredited qualified BC labour expenditure for that episode** <br><br> <b>RLE</b> – Accredited BC labour expenditure for the tax year in respect of services rendered in BC outside the designated Vancouver area for that episode <br><br><b>TLE</b> – Accredited BC labour expenditure for the tax year for that episode',
            'labelClass': 'fullLength tabbed'
          },
          {
            'type': 'table',
            'num': '960'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '** Use this worksheet for animated episodic productions that start key animation after June 26, 2015',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Worksheet 3: Distant location production services tax credit for episodic productions',
            'labelClass': 'fullLength titleFont center'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>AQBCLE</b> - Total accredited qualified BC labour expenditure for that episode** <br><br> <b>DLD</b> - Total number of principal photography days in British Columbia done in a distant location for that episode <br><br><b>TD</b> - Total number of principal photography days in British Columbia for that episode',
            'labelClass': 'fullLength tabbed'
          },
          {
            'type': 'table',
            'num': '970'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '** Use this worksheet for all live action episodic productions and only animated episodic productions that start key animation prior to June 27, 2015. <br>    Use worksheet 4 for animated episodic productions that start key animation after June 26, 2015',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Worksheet 4: Distant location production services tax credit for animated episodic productions that start key animation after June 26, 2015',
            'labelClass': 'fullLength titleFont center'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>AQBCLE</b> - Total accredited qualified BC labour expenditure for that episode** <br><br> <b>DLLE</b> - Accredited BC labour expenditure for the tax year in respect of services rendered in a distant location for that episode <br><br><b>TLE</b> - Accredited BC labour expenditure for the tax year for that episode',
            'labelClass': 'fullLength tabbed'
          },
          {
            'type': 'table',
            'num': '980'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '** Use this worksheet for all animated episodic productions that start key animation after June 26, 2015',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        hideFieldset: true,
        rows:[
          {
            label: '<i>See the privacy notice on your return</i>.',
            labelClass: 'absoluteAlignRight'
          }
        ]
      }
    ]
  });
})();
