(function() {
  wpw.tax.create.diagnostics('t2s423', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('4230001', common.prereq(common.check(['t2s5.672'], 'isNonZero'),
        common.requireFiled('T2S423')));

    diagUtils.diagnostic('4230002', common.prereq(common.requireFiled('T2S423'),
        common.check(['151', '153'], 'isFilled', true)));

    diagUtils.diagnostic('4230003', common.prereq(common.requireFiled('T2S423'),
        common.check(['220', '222', '230', '235', '240', '245', '250'], 'isFilled', true)));

    diagUtils.diagnostic('4230004', common.prereq(common.requireFiled('T2S423'),
        common.check(['301', '302', '303'], 'isFilled', true)));

    diagUtils.diagnostic('4230005', common.prereq(common.and(
        common.requireFiled('T2S423'),
        common.check('800', 'isNonZero')),
        common.and(
            common.check('505', 'isNonZero'),
            common.check(['405', '420', '425', '430', '435'], 'isNonZero'))));

    diagUtils.diagnostic('4230010', common.prereq(common.requireFiled('T2S423'),
        common.check(['t2s5.672', '850'], 'isZero', true)));

    diagUtils.diagnostic('423.490', common.prereq(common.and(
        common.requireFiled('T2S423'),
        common.check('490', 'isNonZero')),
        common.check(['405', '420', '425', '430', '432', '435'], 'isNonZero')));

    diagUtils.diagnostic('423.491', common.prereq(common.and(
        common.requireFiled('T2S423'),
        common.check('491', 'isNonZero')),
        common.check(['406', '421', '426', '431', '433', '436'], 'isNonZero')));

    diagUtils.diagnostic('423.800', common.prereq(common.and(
        common.requireFiled('T2S423'),
        common.check('800', 'isNonZero')),
        common.check('610', 'isNonZero')));

  });
})();
