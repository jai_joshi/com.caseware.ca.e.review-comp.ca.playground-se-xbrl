(function() {
  wpw.tax.create.diagnostics('t2s460', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('4600001', common.prereq(common.check(['t2s5.705'], 'isNonZero'),
        common.requireFiled('T2S460')));

    diagUtils.diagnostic('4600010', common.prereq(common.requireFiled('T2S460'), common.generateChecks('050', ['001'])));

    diagUtils.diagnostic('4600011', common.prereq(common.requireFiled('T2S460'), common.generateChecks('051', ['002'])));

    diagUtils.diagnostic('4600011', common.prereq(common.requireFiled('T2S460'), common.generateChecks('052', ['003'])));

  });
})();
