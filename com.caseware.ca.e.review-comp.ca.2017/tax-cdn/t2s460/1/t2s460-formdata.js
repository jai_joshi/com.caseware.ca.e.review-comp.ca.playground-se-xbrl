(function() {

  wpw.tax.create.formData('t2s460', {
    formInfo: {
      abbreviation: 't2s460',
      title: 'Northwest Territories Investment Tax Credit',
      schedule: 'Schedule 460',
      headerImage: 'canada-federal',
      showCorpInfo: true,
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if you are a corporation with a permanent establishment in the ' +
              'Northwest Territories and have made investments eligible for the investment tax credit ' +
              'under the <i>Risk Capital Investment Tax Credits Act</i>.'
            },
            {
              label: 'The credit is available for investments made before March 1, 2008. ' +
              'You cannot claim the credit for investments made during the 2004 calendar year.'
            },
            {
              label: 'You can carry unused credits forward to the seven following tax years or back to ' +
              'the three previous tax years as long as the corporation maintained a permanent establishment ' +
              'in the Northwest Territories at any time in the year in which the credit arose.'
            },
            {
              label: 'You can claim a maximum credit of $30,000 in a tax year, less any amount ' +
              'claimed as a tax credit under section 127.4 of the federal <i>Income Tax Act</i>.'
            },
            {
              label: 'File a completed copy of this schedule with your T2 Corporation Income Tax Return.'
            }
          ]
        }
      ],
      category: 'Northwest Territories Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Total tax credit earned (for investments made before March 1, 2008)',
        'rows': [
          {
            'type': 'table',
            'num': '100'
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of total credit available and credit available for carry-forward',
        'rows': [
          {
            'label': 'Credit at end of preceding taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1001'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Deduct:</b> Credit expired after seven taxation years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit at beginning of taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1002',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Add',
            'labelClass': 'bold'
          },
          {
            'label': 'Current year credit earned (enter amount A)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '120',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              }
            ]
          },
          {
            'label': 'Total credit available',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1007'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Deduct: ',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit claimed in the current year against Northwest Territories tax payable (enter on line 705 in Part 2 of Schedule 5, Tax Calculation Supplementary - Corporations)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit carried back to preceding taxation year(s) (complete Part 3) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1008'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1009'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1010'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Closing balance (amount B minus amount D)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 3 - Request for carryback of credit',
        'rows': [
          {
            'type': 'table',
            'num': '1000'
          }
        ]
      },
      {
        'header': 'Part 4 - Analysis of credit available for carryforward by year of origin',
        'rows': [
          {
            'type': 'table',
            'num': '2000'
          }
        ]
      },
      {
        'header': 'Historical Data and Calculation for PEI Corporate Investment Tax Credit',
        'rows': [
          {
            'type': 'table',
            'num': '3000'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  });
})();
