(function() {
  wpw.tax.create.diagnostics('t2s13', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0130001', common.prereq(common.check(['t2j.213'], 'isChecked'), common.requireFiled('T2S13')));

    diagUtils.diagnostic('0130005', common.prereq(common.and(
        common.requireFiled('T2S13'),
        common.check(['t2s6.880'], 'isNonZero')),
        function(tools) {
          var row = tools.field('100').getRow(0);
          return (tools.requireOne(row[0], 'isNonZero') &&
          tools.requireOne([row[1], row[2]], 'isNonZero'));
        }));

    diagUtils.diagnostic('0130010', common.prereq(common.and(
        common.requireFiled('T2S13'),
        common.check(['t2s6.885'], 'isNonZero')),
        function(tools) {
          var row = tools.field('100').getRow(0);
          return tools.requireAll([row[0], row[5]], 'isNonZero');
        }));

    diagUtils.diagnostic('0130015', common.prereq(common.and(
        common.requireFiled('T2S13'),
        common.check(['t2s1.125'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list([
                '110', '115', '130', '135', '150', '155', '190', '195', '210', '215', '230', '235']),
              'isNonZero');
        }));

    diagUtils.diagnostic('0130020', common.prereq(common.and(
        common.requireFiled('T2S13'),
        common.check(['t2s1.413'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['120', '140', '160', '200', '220', '240']), 'isNonZero');
        }));

    diagUtils.diagnostic('0130025', {
      label: 'For each entry in columns 013002, 013003, or 013004, a corresponding entry is required in column 013001.'
    }, common.prereq(common.requireFiled('T2S13'),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[1], row[2], row[5]], 'isNonZero'))
              return row[0].require('isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0130025', {
      label: 'For each entry in column 013001, a corresponding entry is required in columns 013002, 013003, ' +
      'or 013004.'
    }, common.prereq(common.requireFiled('T2S13'),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[0].isNonZero())
              return tools.requireOne([row[1], row[2], row[5]], 'isNonZero');
            else return true;
          });
        }));
  });
})();
