wpw.tax.create.importCalcs('T2S13', function(importTools) {

  function getImportObj(taxPrepIds) {
    var output = {numDefined: 0, objects: {}};
    var intercepted;
    taxPrepIds = wpw.tax.utilities.ensureArray(taxPrepIds);
    taxPrepIds.forEach(function(taxPrepId, index) {
      intercepted = false;
      importTools.intercept(taxPrepId, function(importObj) {
        output['objects'][index] = importObj;
        output['numDefined']++;
        intercepted = true;
      });
      if (!intercepted) {
        output['objects'][index] = undefined;
      }
    });
    return output;
  }

  var taxPrepIds = [
    // [Transfer to s13w, Begin balance, Transfer, Add, Deduct]
    ['FDRES.Ttwres168', 'FDRES.Ttwres12', 'FDRES.Ttwres144', 'FDRES.Ttwres24', 'FDRES.Ttwres36'],
    ['FDRES.Ttwres169', 'FDRES.Ttwres13', 'FDRES.Ttwres145', 'FDRES.Ttwres25', 'FDRES.Ttwres37'],
    ['FDRES.Ttwres170', 'FDRES.Ttwres14', 'FDRES.Ttwres146', 'FDRES.Ttwres26', 'FDRES.Ttwres38'],
    ['FDRES.Ttwres172', 'FDRES.Ttwres16', 'FDRES.Ttwres148', 'FDRES.Ttwres28', 'FDRES.Ttwres40'],
    ['FDRES.Ttwres173', 'FDRES.Ttwres17', 'FDRES.Ttwres149', 'FDRES.Ttwres29', 'FDRES.Ttwres41'],
    ['FDRES.Ttwres220', 'FDRES.Ttwres222', 'FDRES.Ttwres223', 'FDRES.Ttwres226', 'FDRES.Ttwres227'],
    ['FDRES.Ttwres221', 'FDRES.Ttwres224', 'FDRES.Ttwres225', 'FDRES.Ttwres228', 'FDRES.Ttwres229'],
    ['FDRES.Ttwres174', 'FDRES.Ttwres60', 'FDRES.Ttwres150', 'FDRES.Ttwres30', 'FDRES.Ttwres42']
  ];

  var s13Rows = [
    getImportObj(taxPrepIds[0]),
    getImportObj(taxPrepIds[1]),
    getImportObj(taxPrepIds[2]),
    getImportObj(taxPrepIds[3]),
    getImportObj(taxPrepIds[4]),
    getImportObj(taxPrepIds[5]),
    getImportObj(taxPrepIds[6]),
    getImportObj(taxPrepIds[7])
  ];

  importTools.after(function() {
    var row;
    var s13;

    var rfId = 0;
    var repeatRow = false;

    for (var i = 0; i < s13Rows.length; i++) {
      row = s13Rows[i];
      if (row['numDefined'] != 0) {
        if (rfId != 0) {
          importTools.addRepeat('T2S13W');
        }

        s13 = importTools.form('T2S13W', rfId);
        rfId++;

        s13.setValue('103', (i + 1).toString());

        //On Financial Statements?
        if (angular.isDefined(row['objects'][0]) && row['objects'][0].value == 'Y') {
          if (repeatRow) {
            s13.setValue('131', '1');
            repeatRow = false;
          }
          else {
            repeatRow = true;
            i--;
          }
        }
        else {
          s13.setValue('131', '2');
        }

        //opening balance
        if (angular.isDefined(row['objects'][1])) {
          s13.setValue('110', row['objects'][1].value);
        }

        //transfer
        if (angular.isDefined(row['objects'][2])) {
          s13.setValue('111', row['objects'][2].value);
        }

        //add
        if (angular.isDefined(row['objects'][3])) {
          s13.setValue('120', '1', 3, 0);
          s13.setValue('120', row['objects'][3].value, 4, 0);
        }

        //deduct
        if (angular.isDefined(row['objects'][4])) {
          if (!angular.isDefined(row['objects'][3])) {
            s13.setValue('120', '2', 3, 0);
            s13.setValue('120', row['objects'][4].value, 4, 0);
          }
          else {
            s13.setValue('120', '2', 3, 1);
            s13.setValue('120', row['objects'][4].value, 4, 1);
          }
        }
      }
    }
  });
});