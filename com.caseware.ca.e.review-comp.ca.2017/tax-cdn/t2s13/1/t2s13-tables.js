(function() {

  function getTableSummaryColumns() {
    return [
      {
        'header': 'Description',
        'width': '395px',
        'disabled': true,
        type: 'none'
      },
      {
        'header': 'Balance at the beginning of the year <br> $',
        'disabled': true,
        total: true
      },
      {
        'header': 'Transfer on an amalgamation or the wind-up of a subsidiary <br> $',
        'disabled': true,
        total: true
      },
      {
        'header': 'Net Adjustments made during the year',
        'disabled': true,
        total: true
      },
      {
        'header': 'Balance at the end of the year <br> $',
        'disabled': true,
        total: true
      }
    ]
  }

  function getTableSummaryRows() {
    return [
      {'0': {label: 'Reserve for doubtful debts'}},
      {'0': {label: 'Reserve for undelivered goods and services not rendered'}},
      {'0': {label: 'Reserve for prepaid rent'}},
      {'0': {label: 'Reserve for returnable containers'}},
      {'0': {label: 'Reserve for unpaid amounts'}},
      {'0': {label: 'Reserve for corporate insurance policy'}},
      {'0': {label: 'Reserve for banking'}},
      {'0': {label: 'Other tax reserves'}}
    ]
  }

  wpw.tax.global.tableCalculations.t2s13 = {
    "100": {
      hasTotals: true,
      "showNumbering": true,
      "columns": [
        {
          "header": "Description of property",
          "width": "30%",
          "num": "001",
          "tn": "001",
           "validate": {"or":[{"length":{"min":"1","max":"175"}},{"check":"isEmpty"}]},
          type: 'text'
        },
        {
          "header": "Balance at the beginning of the year <br> $",
          "total": true,
          "num": "002","validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          "tn": "002",
          "totalNum": "008",
          totalMessage: 'Totals:',
          "totalTn": "008",
          "maxLength": "13"
        },
        {
          "header": "Transfer on an amalgamation or the wind-up of a subsidiary <br> $",
          "num": "003",
          "tn": "003",
          "total": true,
          "totalNum": "009",
          "totalTn": "009",
          "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        },
        {
          "header": "Adjustments made during the year <br> <br> Positive amount(s)",
          "num": "101",
          "total": true
        },
        {
          "header": "Adjustments made during the year <br> <br> Negative amount(s)",
          "num": "102",
          "total": true
        },
        {
          "header": "Balance at the end of the year <br> $",
          "num": "004",
          "tn": "004",
          "total": true,
          "totalNum": "010",
          "totalTn": "010",
         "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          "disabled": true
        }
      ]
    },
    "201": {
      hasTotals: true,
      "fixedRows": true,
      "columns": [
        {
          "header": "Description",
          "width": "25%",
          "type": "none"
        },
        {
          "header": "Balance at the beginning of the year <br> $",
          "total": true,
          "totalNum": "270",
          "totalTn": "270",
          totalMessage: 'Totals:',
          "maxLength": "13",
          "num": "109"
        },
        {
          "header": "Transfer on an amalgamaution or the wind-up of a subsidiary <br> $",
          "total": true,
          "totalNum": "275",
          "totalTn": "275",
          "maxLength": "13",
          "num": "114"
        },
        {
          "header": "Net Adjustments made during the year",
          "num": "202",
          "total": true
        },
        {
          "header": "Balance at the end of the year <br> $",
          "total": true,
          "totalNum": "280",
          "totalTn": "280",
          "maxLength": "13",
          "num": "119"
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Reserve for doubtful debts"
          },
          "1": {
            "num": "110", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "110",
            "rf": true
          },
          "2": {
            "num": "115", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "115"
          },
          "3": {
            "num": "310"
          },
          "4": {
            "num": "120", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "120"
          }
        },
        {
          "0": {
            "label": "Reserve for undelivered goods and services not rendered",
            "labelClass": "fullLength"
          },
          "1": {
            "num": "130", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "130",
            "rf": true
          },
          "2": {
            "num": "135", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "135"
          },
          "3": {
            "num": "330"
          },
          "4": {
            "num": "140", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "140"
          }
        },
        {
          "0": {
            "label": "Reserve for prepaid rent",
            "labelClass": "fullLength"
          },
          "1": {
            "num": "150", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "150",
            "rf": true
          },
          "2": {
            "num": "155", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "155"
          },
          "3": {
            "num": "350"
          },
          "4": {
            "num": "160", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "160"
          }
        },
        {
          "0": {
            "label": "Reserve for returnable containers",
            "labelClass": "fullLength"
          },
          "1": {
            "num": "190", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "190",
            "rf": true
          },
          "2": {
            "num": "195", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "195"
          },
          "3": {
            "num": "390"
          },
          "4": {
            "num": "200", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "200"
          }
        },
        {
          "0": {
            "label": "Reserve for unpaid amounts",
            "labelClass": "fullLength"
          },
          "1": {
            "num": "210", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "210",
            "rf": true
          },
          "2": {
            "num": "215", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "215"
          },
          "3": {
            "num": "410"
          },
          "4": {
            "num": "220", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "220"
          }
        },
        {
          "0": {
            "label": "Reserve for corporate insurance policy"
          },
          "1": {
            "num": "221",
            "rf": true
          },
          "2": {
            "num": "222"
          },
          "3": {
            "num": "421"
          },
          "4": {
            "num": "223"
          }
        },
        {
          "0": {
            "label": "Reserve for banking"
          },
          "1": {
            "num": "224",
            "rf": true
          },
          "2": {
            "num": "225"
          },
          "3": {
            "num": "424"
          },
          "4": {
            "num": "226"
          }
        },
        {
          "0": {
            "label": "Other tax reserves",
            "labelClass": "fullLength"
          },
          "1": {
            "num": "230", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "230",
            "rf": true
          },
          "2": {
            "num": "235", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "235"
          },
          "3": {
            "num": "430"
          },
          "4": {
            "num": "240", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "240"
          }
        }
      ]
    },
    '998': {//always hidden table
      'fixedRows': true,
      hasTotals: true,
      'columns': getTableSummaryColumns(),
      'cells': getTableSummaryRows()
    }
  }
})();
