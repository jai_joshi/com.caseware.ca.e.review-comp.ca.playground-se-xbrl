(function() {
  //USE THIS FUNCTION to return reserve amount per table then per type
  function paramSort(paramArr, a, b, rowNumbers) {
    var _arguments = arguments;

    function cdSumFn(resultObj, paramObj) {
      for (var i = 4; i < _arguments.length; i++) {
        resultObj[_arguments[i]] += paramObj[_arguments[i]];
      }
      return resultObj;
    }

    // result [0: [], 1: []] a has values {1,2}
    var aSplitArr = splitArrByAttr(paramArr, a, 1);

    //result [0: [], ..., b-last-value: []} b has values {1..b-last-value}
    var bSplitArr1 = splitArrByAttr(aSplitArr[0], b, 1);
    var bSplitArr2 = splitArrByAttr(aSplitArr[1], b, 1);

    //Reduce right for each index in each bSplitArr
    for (var i = 0; i < rowNumbers; i++) {
      var initialObj1 = {};
      initialObj1[a] = 1;
      initialObj1[b] = i + 1;
      var initialObj2 = {};
      initialObj2[a] = 1;
      initialObj2[b] = i + 1;
      for (var j = 4; j < _arguments.length; j++) {
        initialObj1[_arguments[j]] = 0;
        initialObj2[_arguments[j]] = 0;
      }
      bSplitArr1[i] = bSplitArr1[i] ? bSplitArr1[i].reduce(cdSumFn, initialObj1) : initialObj1;
      bSplitArr2[i] = bSplitArr2[i] ? bSplitArr2[i].reduce(cdSumFn, initialObj2) : initialObj2;
    }

    return {1: bSplitArr1, 2: bSplitArr2};
  }

  /**
   * Takes an array of objects paramArr [{a,b,c,d}..] splits into an array of arrays
   * with each index being a distinct value of param attr
   * @param paramArr
   * @param attr
   * @param offset
   * @returns Array.<Array.<Object>>
   */
  function splitArrByAttr(paramArr, attr, offset) {
    var result = [];

    paramArr = paramArr || [];
    paramArr.forEach(function(paramObj, index) {
      if (!paramObj.hasOwnProperty(attr)) {
        console.error('No attr: %s in object %d', attr, index);
        return;
      }

      result[paramObj[attr] - offset] = result[paramObj[attr] - offset] || [];
      result[paramObj[attr] - offset].push(paramObj);
    });

    return result;
  }

  function tableSummaryCalc(calcUtils, dataArray, tableNum) {
    calcUtils.field(tableNum).getRows().forEach(function(row, rIndex) {
      var beginningBalance = parseInt(dataArray[rIndex].beginningBalance);
      var transferAmount = parseInt(dataArray[rIndex].transferFromAmal);
      var netAdj = parseInt(dataArray[rIndex].netAdjust);
      var sourceFormId = parseInt(dataArray[rIndex].sourceFormId);

      row[1].assign(beginningBalance);
      row[2].assign(transferAmount);
      row[3].assign(netAdj);
      row[4].assign(beginningBalance + transferAmount + netAdj);

      row.forEach(function(cell) {
        cell.source(sourceFormId)
      });

    })
  }

  wpw.tax.create.calcBlocks('t2s13', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //table 100 calcs
      field('100').getRows().forEach(function(row) {
        row[5].assign(
            row[1].get() +
            row[2].get() +
            row[3].get() -
            row[4].get()
        );
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //calcs for table summary 201 and 998 hidden
      var t2s13wLinkedData = [];
      var rowNumbers = field('201').size().rows;
      var linkedForms = calcUtils.allRepeatForms('t2s13w');
      linkedForms.forEach(function(form) {
        t2s13wLinkedData.push({
          isReportedOnFs: form.field('131').get(),
          reserveType: form.field('103').get(),
          beginningBalance: form.field('110').get(),
          transferFromAmal: form.field('111').get(),
          netAdjust: form.field('126').get(),
          sourceFormId: form.repeatFormId
        })
      });

      //extra params are needed to return correct param value for the result array
      var sortedData = paramSort(t2s13wLinkedData, 'isReportedOnFs', 'reserveType', rowNumbers,
          'beginningBalance', 'transferFromAmal', 'netAdjust', 'sourceFormId');
      //populate table value
      tableSummaryCalc(calcUtils, sortedData[1], '998');
      tableSummaryCalc(calcUtils, sortedData[2], '201');
    });
  });
})();

