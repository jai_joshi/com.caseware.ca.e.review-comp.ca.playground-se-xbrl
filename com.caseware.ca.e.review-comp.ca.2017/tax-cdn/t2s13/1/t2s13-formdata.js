(function() {
  'use strict';

  wpw.tax.global.formData.t2s13 = {
    'formInfo': {
      'abbreviation': 't2s13',
      'title': 'Continuity of Reserves',
      formFooterNum: 'T2 SCH 13 E (11)',
      //'subTitle': '(2011 and later years)',
      'schedule': 'Schedule 13',
      'code': 'Code 1101',
      headerImage: 'canada-federal',
      'showCorpInfo': true,
      'description': [
        {
          type: 'list',
          items: [
            'For use by corporations to provide a continuity of all reserves claimed which are allowed for' +
            ' tax purposes.',
            'File one completed copy of this schedule with the corporation\'s <i> T2 Corporation Income Tax' +
            ' Return</i>.',
            'For more information, see the <i> T2 Corporation Income Tax Guide</i>.'
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        header: 'Part 1 - Capital gains reserves',
        rows: [
          {labelClass: 'fullLength'},
          {
            type: 'table', num: '100'
          },
          {labelCLass: 'fullLength'},
          {
            label: 'The amount from line 008 <b>plus</b> the amount from line 009 should be entered on line 880 of ' +
            'Schedule 6, <i>Summary of Dispositions of Capital Property</i>.', labelClass: 'fullLength tabbed'
          },
          {
            label: 'The amount from line 010 should be entered on line 885 of Schedule 6.',
            labelClass: 'fullLength tabbed'
          }
        ]
      },
      {
        header: 'Part 2 - Other reserves',
        rows: [
          {labelClass: 'fullLength'},
          {type: 'table', num: '201'},
          {type: 'table', num: '998', showWhen: {fieldId: 'show998', compare: {is: true}}},//always hidden table, value needed for S1
          {labelClass: 'fullLength'},
          {
            label: 'The amount from line 270 <b> plus</b> the amount from line 275 should be entered on line 125' +
            ' of Schedule 1, <i> Net Income (Loss) for Income Tax Purposes</i>, as an addition. The amount from' +
            ' line 280 should be entered on line 413 of Schedule 1 as a deduction.', labelClass: 'fullLength tabbed'
          }
        ]
      }
    ]
  };
})();
