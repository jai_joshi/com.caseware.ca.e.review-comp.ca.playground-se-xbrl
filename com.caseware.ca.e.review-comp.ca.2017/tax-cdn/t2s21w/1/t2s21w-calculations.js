(function() {

  var canadaProvinces = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.canadaProvinces, 'EN');

  function getApplicableProvs(calcUtils) {
    var field = calcUtils.field;
    if (field('010').get() == '1') {
      return {
        'NL': true,
        'PE': true,
        'NS': true,
        'NB': true,
        'ON': true,
        'MB': true,
        'SK': true,
        'BC': true,
        'YT': true,
        'NT': true,
        'NU': true
      };
    }
    else {
      var s5Ref = {
        'NL': '003',
        'PE': '005',
        'NS': '007',
        'NB': '009',
        'ON': '013',
        'MB': '015',
        'SK': '017',
        'BC': '021',
        'YT': '023',
        'NT': '025',
        'NU': '026'
      };
      var jurisdiction = field('cp.750').get();
      var applicable = {};
      canadaProvinces.forEach(function(prov, index) {
        if (prov.value == 'AB' || prov.value == 'QC') {
          return;
        }
        if (index != 0) {
          if (jurisdiction == 'MJ') {
            applicable[prov.value] = field('t2s5.' + s5Ref[prov.value]).get();
            if (field('t2s5.' + s5Ref[prov.value]).get() == undefined) {
              applicable[prov.value] = false;
            }
          }
          else {
            applicable[prov.value] = jurisdiction == prov.value;
          }
        }
      })
    }
    return applicable;
  }

  function getRatesData(calcUtils) {
    var field = calcUtils.field;
    var ratesData = {};
    //in srcData: for each province, specify:
    //s5num: num on s5 for jurisdiction checks
    //s5row: s5 rowIndex for allocation amount
    //form: formId where tax rate is found
    //formNum: num of rate
    //todo: add rates to array if provincial rates change

    var provSrcData = [
      {prov: 'NL', s5num: '003', s5row: '0', form: 'ratesNl', formNum: '900'},
      {prov: 'PE', s5num: '005', s5row: '2', form: 'ratesPei', formNum: '705'},
      {prov: 'NS', s5num: '007', s5row: '3', form: 'ratesNs', formNum: '401'},
      {prov: 'NB', s5num: '009', s5row: '5', form: 'ratesNb', formNum: '900'},
      {prov: 'ON', s5num: '013', s5row: '7', form: 'ratesOn', formNum: '602'},
      {prov: 'MB', s5num: '015', s5row: '8', form: 'ratesMb', formNum: '905'},
      {prov: 'SK', s5num: '017', s5row: '9', form: 'ratesSk', formNum: '090'},
      {prov: 'BC', s5num: '021', s5row: '11', form: 'ratesBc', formNum: '1117'},
      {prov: 'YT', s5num: '023', s5row: '12', form: 'ratesYt', formNum: '113'},
      {prov: 'NT', s5num: '025', s5row: '13', form: 'ratesNt', formNum: '705'},
      {prov: 'NU', s5num: '026', s5row: '14', form: 'ratesNu', formNum: '705'}
    ];
    var provOffshoreData = [
      {prov: 'NL', s5num: '004', s5row: '1'},  //XO at s5row 1
      {prov: 'NS', s5num: '008', s5row: '4'}   //NO at s5row 4
    ];
    var federalData = [
      {item: 'allInc', form: 't2j', num: '371'},
      {item: 'cdnInc', form: 't2s5', num: '1031'}
    ];
    var jurisdiction = field('cp.750').get();
    //Create empty rates object
    canadaProvinces.forEach(function(prov, index) {
      if (prov.value == 'AB' || 'QC') {
        return;
      }
      if (index != 0) {
        ratesData[prov.value + 'Tax'] = 0;
        ratesData[prov.value + 'Inc'] = 0;
      }
    });
    //Get federal rates
    federalData.forEach(function(data) {
      ratesData[data.item] = field(data.form + '.' + data.num).get();
    });
    //Get provincial rates
    provSrcData.forEach(function(data) {
      ratesData[data.prov + 'Tax'] = field(data.form + '.' + data.formNum).get();
      ratesData[data.prov + 'Inc'] = field('t2s5.998').cell(data.s5row, 6).get();
      if (jurisdiction == data.prov) { //if not MJ we overwrite the zero values
        ratesData['cdnInc'] = ratesData['allInc'];
        ratesData[data.prov + 'Inc'] = ratesData['allInc'];
      }
    });
    //Add offshore amounts
    provOffshoreData.forEach(function(data) {
      ratesData[data.prov + 'Inc'] += field('t2s5.998').cell(data.s5row, 6).get();
    });
    return ratesData;
  }

  function getS5SpecialCase(calcUtils, s5row) {
    var field = calcUtils.field;
    var s5totalB = field('T2S5.129').get();
    var s5totalD = field('T2S5.169').get();
    var s5colC = field('T2S5.998').cell(s5row, 2).get();
    var s5colE = field('T2S5.998').cell(s5row, 4).get();
    if (s5totalB != 0) {
      s5colC = (s5colC * 1000) / s5totalB;
    }
    if (s5totalD != 0) {
      s5colE = (s5colE * 1000) / s5totalD;
    }
    if (s5totalD != 0 && s5totalB != 0) {
      return (s5colC + s5colE) / 2;
    }
    else {
      return (s5colC + s5colE);
    }
  }

  function table950Calcs(calcUtils, prov, rIndex, alloc) {
    var field = calcUtils.field;
    var table101Row = field('t2s21.101').getRow(rIndex);
    var table102Row = field('t2s21.102').getRow(rIndex);
    var table950Row = field('950' + prov.value).getRow(rIndex);
    if (!table101Row || !table102Row || !table950Row) {
      return;
    }
    var rowsGetStore = [
      table101Row[0].get(), //Part 1 Col A
      table101Row[2].get(), //Part 1 Col C
      table101Row[3].get(), //Part 1 Col D
      table102Row[3].get(), //Part 1 Col I
      table101Row[1].get()  //Part 1 Col B
    ];
    table950Row[0].assign(rowsGetStore[0]);
    table950Row[1].assign(rowsGetStore[1]);
    table950Row[2].assign(rowsGetStore[2]);
    table950Row[3].assign(rowsGetStore[3]);
    table950Row[4].assign(Math.max(0, (table950Row[1].get() - table950Row[2].get() - table950Row[3].get())));
    table950Row[5].assign(Math.round(Math.max(0, (table950Row[1].get() - table950Row[2].get() - table950Row[3].get()) * alloc['cdn'])));
    table950Row[6].assign(rowsGetStore[4]);
    table950Row[7].assign(Math.max(0, table950Row[6].get() * alloc['tax']));
    table950Row[8].assign(Math.round(Math.max(0, table950Row[6].get() * alloc['tax'] * alloc['all'])));
    table950Row[9].assign(Math.min(table950Row[5].get(), table950Row[8].get()));
  }

  function getDiv(num, denom) {
    if (denom != 0) {
      if (num / denom > 0) {
        return num / denom;
      }
    }
    return 0;
  }

  wpw.tax.create.calcBlocks('t2s21w', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      var show = getApplicableProvs(calcUtils);
      var rates = getRatesData(calcUtils);
      var jurisdiction = field('cp.750').get();
      var total;

      canadaProvinces.forEach(function(prov, index) {
        if (prov.value == 'AB' || prov.value == 'QC') {
          return;
        }
        if (index != 0) {
          //Applicability - Table 025
          field(prov.value + 'Applicable').assign(show[prov.value]);

          //Table 900
          if (prov.value == jurisdiction && prov.value == 'ON' && rates['allInc'] == 0) { //ON Special Case 1
            field('100ON').assign(1000);
            field('105ON').assign(1000);
            field('110ON').assign(1000);
          }
          else if (jurisdiction == 'MJ' && prov.value == 'ON' && rates['allInc'] == 0) { //ON Special Case 2
            field('100ON').assign(getS5SpecialCase(calcUtils, 7));
            field('105ON').assign(1000);
            field('110ON').assign(1000);
          }
          else {
            field('100' + prov.value).assign(rates[prov.value + 'Inc']);
            field('105' + prov.value).assign(rates['cdnInc']);
            field('110' + prov.value).assign(rates['allInc']);
          }
          field('115' + prov.value).assign(rates[prov.value + 'Tax']);

          var allocations = //separate ratio calc because tables have 2 decimal limit
              {
                'cdn': getDiv(field('100' + prov.value).get(), field('105' + prov.value).get()),
                'all': getDiv(field('100' + prov.value).get(), field('110' + prov.value).get()),
                'tax': field('115' + prov.value).get() / 100
              };

          field('120' + prov.value).assign(allocations['cdn']);
          field('125' + prov.value).assign(allocations['all']);

          //Table 950
          total = 0;
          for (var rIndex = 0; rIndex < field('t2s21.101').size().rows; rIndex++) {
            table950Calcs(calcUtils, prov, rIndex, allocations);
          }
        }
      });

      //todo:For Ontario, if the corporation is not a life insurance corporation, also enter amount J on
      // todo: line 500 of Schedule 510, Ontario Corporate Minimum Tax .
    });
    calcUtils.calc(function(calcUtils, field) {
      //Calc to remove extra blank row if imported from TaxPrep.
      //todo: make an import calc when implemented
      canadaProvinces.forEach(function(prov) {
        if (prov['value'] == 'AB' || prov['value'] == 'QC'|| prov['value'] == null) {
          return;
        }
        for (var row = 0; row < field('950' + prov['value']).size().rows; row++) {
          if (!field('T2S21.101').getRow(row)) {
            calcUtils.removeTableRow('T2S21W', '950' + prov['value'], row);
          }
        }
      });
    });
  });
})();
