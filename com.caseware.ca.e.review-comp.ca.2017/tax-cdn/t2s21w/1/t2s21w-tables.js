(function() {

  var countryAddressCodes = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.countryAddressCodes, 'value');
  var canadaProvinces = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.canadaProvinces, 'EN');
  var countryOptions = getDropdownOptions(countryAddressCodes);

  function getDropdownOptions(countryCodes) {
    var options = [{value: '', option: ''}];
    countryCodes.forEach(function(opt, index) {
      if(index == 0) {
        return;
      }
      options.push({value: opt.value, option: opt.value + ' : ' + opt.option});
    });
    return options;
  }

  function getTables() {
    var tableObj = {};
    //Table 025 contains fields for use with ngShow, is set to ngShow: false
    tableObj['025'] =
    {
      'infoTable': true,
      'fixedRows': true,
      'num': '025',
      'columns': getTable025Data(true),
      'cells': getTable025Data(false)
    };

    var table900Cols = getTable900Cols(); //Should only need to calculate once
    canadaProvinces.forEach(function(item, index) {
      if (item.value == 'AB' || item.value == 'QC') {
        return;
      }
      if (index != 0) {
        tableObj['900' + item.value] =
        {
          'infoTable': true,
          'fixedRows': true,
          'num': '900' + item.value,
          'columns': table900Cols,
          'cells': getTable900Cells(item)
        };
        tableObj['950' + item.value] =
        {
          'fixedRows': true,
          'hasTotals': true,
          'num': '950' + item.value,
          'columns': getTable950Cols(item)
        };
      }
    });
    return tableObj
  }

  function getTable025Data(cols) {
    var Arr = [];
    //Column Calcs
    if (cols) {
      Arr.push({'width': '20%', 'type': 'none'});
      canadaProvinces.forEach(function(item, index) {
        if (item.value == 'AB' || item.value == 'QC') {
          return;
        }
        if (index != 0) {
          Arr.push({colClass: 'small-input-width'});
        }
      });
      Arr.push({'width': '20%', 'type': 'none'})
    }
    //Row Calcs
    else {
      var row = {};
      var index = 1;
      row[0] = {'type': 'none'};
      canadaProvinces.forEach(function(item) {
        if (item.value == 'AB' || item.value == 'QC' || item.value == ' ') {
          return;
        }

        row[index] = {
          'label': item.value + '<br>',
          'type': 'singleCheckbox',
          'num': item.value + 'Applicable',
          'cannotOverride': true
        };
        index++;
      });
      row[index] = {'type': 'none'};
      Arr.push(row);
    }
    return Arr;
  }

  function getTable900Cols() {
    return [
      {
        'width': '60%',
        'type': 'none'
      },
      {
        'width': '335px',
        'type': 'none'
      },
      {
        'width': '20px',
        'type': 'none'
      },
      {
        'width': '150px',
        'formField': true
      },
      {
        'width': '10px',
        'type': 'none'
      }
    ]
  }

  function getTable950Cols(prov) {
    var table950ColData = [
      {header: '<br>Country', num: '195'},
      {header: 'A<br>Foreign NBIT paid for the year<br>(Part 1, Column C)', num: '200'},
      {
        header: 'B<br>Less: Foreign NBIT deducted under subs. 20(12)<br>(Part 1, Column D)',
        num: '205'
      },
      {
        header: 'C<br>Less: Federal foreign NBIT credit deductible<br>(Part 1, Column I)',
        num: '210'
      },
      {header: 'D<br>Foreign NBIT credit available <br>(A - B - C)', num: '215'},
      {header: 'E<br>Multiplied by: Canadian source taxable income ratio <br>(D x v)', num: '220'},
      {header: 'F<br>Net foreign non-business income earned in the year<br>(Part 1, Column B)', num: '225'},
      {header: 'G<br>Multiplied by: provincial or territorial tax rate <br>(F x iv)', num: '230'},
      {header: 'H<br>Multiplied by: Canadian and foreign source taxable income ratio (G x vi)', num: '235'},
      {header: 'I<br>Provincial or territorial foreign tax credit<br>(E or H, whichever is least)', num: '240'}
    ];
    var colData = [];
    var lastCol = table950ColData.length - 1;
    table950ColData.forEach(function(column, index) {
          if (index != lastCol && index != 0) { //Middle Columns
            colData.push(
                {
                  'header': column.header,
                  'width': '20%',
                  'num': column.num + prov.value,
                  cellClass: 'alignRight',
                  'formField': true,
                  'decimals': 0
                }
            )
          } else if (index == lastCol) { //Total Column
            colData.push(
                {
                  'header': column.header,
                  'width': '20%',
                  'totalNum': column.num + prov.value,
                  'totalMessage': prov.option + ' Foreign Tax Credit',
                  'formField': true,
                  'total': true
                }
            )
          } else { //First Column
            colData.push(
                {
                  'header': column.header,
                  'width': '69px',
                  'num': column.num + prov.value,
                  cellClass: 'alignRight',
                  'formField': true,
                  'type': 'dropdown',
                  'options': countryOptions
                }
            )
          }
        }
    );
    return colData;
  }

  function getTable900Cells(prov) {
    return [
      {
        '0': {},
        '1': {'label': 'Taxable Income earned in province or territory *'},
        '2': {'label': 'i'},
        '3': {'num': '100' + prov.value},
        '4': {}
      },
      {
        '0': {},
        '1': {'label': 'Canadian-source taxable income **'},
        '2': {'label': 'ii'},
        '3': {'num': '105' + prov.value},
        '4': {}
      },
      {
        '0': {},
        '1': {'label': 'Taxable income from all sources ****'},
        '2': {'label': 'iii'},
        '3': {'num': '110' + prov.value},
        '4': {}
      },
      {
        '0': {},
        '1': {'label': 'Provincial or territorial tax rate ***'},
        '2': {'label': 'iv'},
        '3': {'num': '115' + prov.value, 'decimals': '3', 'filters': 'decimals 2'},
        '4': {'label': '%'}
      },
      {
        '0': {},
        '1': {'label': 'Canadian source taxable income ratio (i / ii)'},
        '2': {'label': 'v'},
        '3': {'num': '120' + prov.value, 'decimals': '3', 'cannotOverride': true},
        '4': {}
      },
      {
        '0': {},
        '1': {'label': 'Canadian and foreign source taxable income ratio (i / iii)'},
        '2': {'label': 'vi'},
        '3': {'num': '125' + prov.value, 'decimals': '3', 'cannotOverride': true},
        '4': {}
      }
    ]
  }

  wpw.tax.global.tableCalculations.t2s21w = getTables()

})();
