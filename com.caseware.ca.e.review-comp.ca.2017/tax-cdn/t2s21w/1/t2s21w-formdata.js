(function() {
  'use strict';
  var canadaProvinces = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.canadaProvinces, 'EN');
  var foreign_countries = JSON.parse(JSON.stringify(wpw.tax.codes.countryAddressCodes));
  delete foreign_countries.CA;
  var foreignCountries = new wpw.tax.actions.codeToDropdownOptions(foreign_countries, 'value');

  var canadaProvinces = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.canadaProvinces, 'EN');
  var foreign_countries = JSON.parse(JSON.stringify(wpw.tax.codes.countryAddressCodes));
  delete foreign_countries.CA;
  var foreignCountries = new wpw.tax.actions.codeToDropdownOptions(foreign_countries, 'value');

  function getAllSections() {
    //Section data before provincial calculations
    var sections = [{

      hideFieldset: true,
      rows: [
        {
          type: 'infoField',
          inputType: 'radio',
          label: 'Show all provinces and territories?',
          num: '010',
          init: '2'
        },
        {
          type: 'table',
          num: '025',
          showWhen: 'Never'
        }
      ]
    }];
    //Provincial calculation sections
    canadaProvinces.forEach(function(section, index) {
      if (index != 0) {
        if (section.value == 'AB' || section.value == 'QC') {
          return;
        }
        if (section.value == 'NL' || section.value == 'NS') {
          section.option += ' and ' + section.option + ' Offshore'
        }
        sections.push(
            {
              header: 'Part 9 - Provincial or territorial foreign tax credit - ' + section.option,
              rows: [
                {
                  type: 'table',
                  num: '900' + section.value
                },
                {labelClass: 'fullLength'},
                {
                  type: 'table',
                  num: '950' + section.value
                },
                {labelClass: 'fullLength'},
                {
                  label: 'Enter amount I  on the corresponding line in Part 2 of Schedule 5, ' +
                  '<i>Tax Calculation Supplementary – Corporations</i>. If you have more than one calculation of the ' +
                  'credit for a province or territory, enter the total credits calculated on the corresponding' +
                  ' line in Part 2 of Schedule 5.'
                },
                {
                  label: 'For Ontario, if the corporation is not a life insurance corporation, also enter amount I on ' +
                  'line 550 of Schedule 510, <i>Ontario Corporate Minimum Tax</i>.'
                }
              ],
              showWhen: section.value + 'Applicable'
            }
        )
      }
    });
    //Section data after provincial calculations
    sections.push(
        {
          header: 'Notes',
          rows: [
            {
              label: '* Enter the amount allocated to the province or territory in column F from Part 1 of Schedule 5. ' +
              'For Nova Scotia and Newfoundland and Labrador, include their respective offshore areas. ' +
              'For Ontario, if the corporation\'s taxable income is nil, calculate the amount in column F ' +
              'as if the taxable income were $1,000.',
              labelClass: 'fullLength'
            },
            {
              label: '** Exclude taxable income earned outside Canada. For Ontario, if the corporation\'s taxable' +
              ' income is nil, enter \"1,000.\"',
              labelClass: 'fullLength'
            },
            {
              label: '*** For all provinces and territories except Ontario, use the higher tax rate. If the rate has' +
              ' changed during the tax year, use the average rate based on the number of days in the tax year before' +
              ' and after the change. For Ontario, use the basic rate of tax calculated in Part 1 of Schedule 500,' +
              ' Ontario Corporation Tax Calculation',
              labelClass: 'fullLength'
            },
            {
              label: '**** Enter the amount from line 360 or line Z, whichever applies, from page 3 of the' +
              ' T2 Corporation Income Tax Return. For Ontario, if the corporation\'s taxable income is nil, enter' +
              ' \"1,000.\"',
              labelClass: 'fullLength'
            }
          ]
        }
    );
    return sections;
  }

  wpw.tax.global.formData.t2s21w = {
    formInfo: {
      abbreviation: 't2s21w',
      //isRepeatForm: true,
      title: 'Provincial or Territorial Foreign Income Tax Credits Workchart',
      schedule: 'SCHEDULE 21 Workchart',
      showCorpInfo: true,
      description: [
        {
          type: 'list',
          items: [
            'Use Part 9 to calculate your provincial or territorial foreign tax credit. ',
              'Complete a separate calculation for each province or territory for which you are claiming the credit.' +
              ' If you have foreign non-business income from more than one country, complete a separate calculation ' +
              'for each country and total these calculations to determine the credit for the applicable' +
              ' province or territory',
            'Corporations resident in Canada at any time in the year and authorizing foreign banks can use this' +
            ' schedule to claim a federal foreign non-business income tax credit, a federal foreign business' +
            ' income tax credit, or a provincial or territorial foreign non-business income tax credit.',
            'Corporations can use this schedule to claim a federal logging tax credit.',

            'Unless otherwise noted, all legislative references are to the <i>Income Tax Act</i> and the' +
            ' <i> Income Tax Regulations</i>.'
          ]
        },
        {
          type: 'infoField',
          inputType: 'radio',
          label: 'Show All Sections?',
          num: '010',
          init: '2'
        }
      ],
      category: 'Workcharts'
    },
    sections: getAllSections()
  };
})();
