(function () {

  wpw.tax.create.diagnostics('t2j', function (paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    function mark(fields, params) {
      fields = wpw.tax.utilities.ensureArray(fields);

      return function (tools) {
        tools.checkMethod(tools.list(fields), function () {
          this.mark(params);
        });
        return true;
      }
    }

    diagUtils.diagnostic('BN', forEach.row('5000', forEach.bnCheckCol(2, true)));
    diagUtils.diagnostic('BN', common.bnCheck('001'));

    diagUtils.diagnostic('2000150', common.check('300', 'isNonZero'));

    diagUtils.diagnostic('2000801', common.prereq(common.check('800', 'isNonZero'),
      function (tools) {
        return tools.requireOne(tools.field('801'), 'isNonZero')
      }));

    diagUtils.diagnostic('2000920', function (tools) {
      var table = tools.field('5000');
      return tools.checkAll(table.getRows(), function (row) {
        if (tools.checkMethod([row[2], row[4], row[6]], 'isNonZero'))
          return tools.requireAll([row[2], row[4], row[6]], 'isNonZero');
        else return true;
      })
    });

    diagUtils.diagnostic('T2J.770', function (tools) {
      return tools.field('770').require(function () {
        return this.get() > 3000;
      })
    });

    //TODO: to run this diagnostic when jurisdiction is AB, QC
    // diagUtils.diagnostic('T2J.760', function(tools) {
    //   return tools.field('760').require(function() {
    //     return this.get() > 3000;
    //   })
    // });

    diagUtils.diagnostic('T2J.997', common.prereq(common.check('997', 'isYes'),
      function (tools) {
        tools.field('t2j.997').mark();
        return tools.field('l996.996').require('isFilled');
      }));

    diagUtils.diagnostic('T2J.259', common.prereq(function (tools) {
      return tools.checkMethod(tools.list(['cp.259', '259']), 'isChecked');
    }, function (tools) {
      return tools.requireAll(tools.list(['cp.259', '259']), 'isEmpty');
    }));

    diagUtils.diagnostic('T2J.271', common.prereq(function (tools) {
      return tools.checkMethod(tools.list(['cp.271', '271']), 'isChecked');
    }, function (tools) {
      return tools.requireAll(tools.list(['cp.271', '271']), 'isEmpty');
    }));

    diagUtils.diagnostic('T2J.refundCheck', common.prereq(common.check('893', 'isPositive'), common.check('894', 'isNonZero')));

    diagUtils.diagnostic('T2J.894', common.prereq(common.check('892', 'isPositive'), common.check('894', 'isZero')));

  });
})();
