(function() {

  var proRateCols = [
    {
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'std-spacing-width',
      "type": "none"
    },
    {
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-padding-width',
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'std-input-col-width',
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'std-spacing-width',
      "type": "none"
    },
    {
      colClass: 'small-input-width'
    },
    {
      colClass: 'std-padding-width',
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'std-padding-width'
    },
    {
      colClass: 'std-spacing-width', type: 'none'
    },
    {
      colClass: 'std-padding-width'
    },
    {
      colClass: 'std-padding-width',
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'std-padding-width'
    },
    {
      colClass: 'std-padding-width',
      cellClass: 'alignCenter', type: 'none'
    },
    {
      colClass: 'std-input-width'
    },
    {
      "type": "none",
      colClass: 'std-padding-width'
    },
    {
      "type": "none",
      colClass: 'std-input-col-width'
    }
  ];

  wpw.tax.global.tableCalculations.t2j = {
    "100": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          cellClass: 'alignLeft'
        },
        {
          "header": "Yes",
          colClass: 'half-col-width',
          "type": "singleCheckbox"
        },
        {
          "header": "Schedule",
          "type": "none",
          cellClass: 'alignCenter',
          colClass: 'half-col-width'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Is the corporation related to any other corporations?"
          },
          "1": {
            "tn": "150",
            "num": "150"
          },
          "2": {
            "label": "9"
          },
          "disable": true
        },
        {
          "0": {
            "label": "Is the corporation an associated CCPC?"
          },
          "1": {
            "tn": "160",
            "num": "160"
          },
          "2": {
            "label": "23"
          },
          "disable": true
        },
        {
          "0": {
            "label": "Is the corporation an associated CCPC that is claiming the expenditure limit?"
          },
          "1": {
            "tn": "161",
            "num": "161"
          },
          "2": {
            "label": "49"
          },
          "disable": true
        },
        {
          "0": {
            "label": "Does the corporation have any non-resident shareholders who own voting shares?"
          },
          "1": {
            "tn": "151",
            "num": "151"
          },
          "2": {
            "label": "19"
          }
        },
        {
          "0": {
            "label": "Has the corporation had any transactions, including section 85 transfers, with its shareholders, officers, or employees, other than transactions in the ordinary course of business? Exclude non-arm's length transactions with non-residents"
          },
          "1": {
            "tn": "162",
            "num": "162"
          },
          "2": {
            "label": "11"
          }
        },
        {
          "0": {
            "label": "If you answered <b>yes</b> to the above question, and the transaction was between corporations not dealing at arm's length, were all or substantially all of the assets of the transferor disposed of to the transferee?"
          },
          "1": {
            "tn": "163",
            "num": "163"
          },
          "2": {
            "label": "44"
          }
        },
        {
          "0": {
            "label": "Has the corporation paid any royalties, management fees, or other similar payments to residents of Canada?"
          },
          "1": {
            "tn": "164",
            "num": "164"
          },
          "2": {
            "label": "14"
          }
        },
        {
          "0": {
            "label": "Is the corporation claiming a deduction for payments to a type of employee benefit plan?"
          },
          "1": {
            "tn": "165",
            "num": "165"
          },
          "2": {
            "label": "15"
          }
        },
        {
          "0": {
            "label": "Is the corporation claiming a loss or deduction from a tax shelter?"
          },
          "1": {
            "tn": "166",
            "num": "166"
          },
          "2": {
            "label": "T5004"
          }
        },
        {
          "0": {
            "label": "Is the corporation a member of a partnership for which a partnership account number has been assigned?"
          },
          "1": {
            "tn": "167",
            "num": "167"
          },
          "2": {
            "label": "T5013"
          }
        },
        {
          "0": {
            "label": "Did the corporation, a foreign affiliate controlled by the corporation, or any other corporation or trust that did not deal at arm's length with the corporation have a beneficial interest in a non-resident discretionary trust (without reference to section 94)?"
          },
          "1": {
            "tn": "168",
            "num": "168"
          },
          "2": {
            "label": "22"
          }
        },
        {
          "0": {
            "label": "Did the corporation own any shares in one or more foreign affiliates in the tax year?"
          },
          "1": {
            "tn": "169",
            "num": "169"
          },
          "2": {
            "label": "25"
          }
        },
        {
          "0": {
            "label": "Has the corporation made any payments to non-residents of Canada under subsections 202(1) and/or 105(1) of the <i>Income Tax Regulations?</i>"
          },
          "1": {
            "tn": "170",
            "num": "170"
          },
          "2": {
            "label": "29"
          }
        },
        {
          "0": {
            "label": "Did the corporation have a total amount over $1 million of reportable transactions with non-arm's length non-residents?"
          },
          "1": {
            "tn": "171",
            "num": "171"
          },
          "2": {
            "label": "T106"
          }
        },
        {
          "0": {
            "label": "For private corporations: Does the corporation have any shareholders who own 10% or more of the corporation's common and/or preferred shares?"
          },
          "1": {
            "tn": "173",
            "num": "173"
          },
          "2": {
            "label": "50"
          }
        },
        {
          "0": {
            "label": "Has the corporation made payments to, or received amounts from, a retirement compensation plan arrangement during the year?"
          },
          "1": {
            "tn": "172",
            "num": "172"
          },
          "2": {
            "label": "----"
          }
        },
        {
          "0": {
            "label": "Does the corporation earn income from one or more Internet webpages or websites?"
          },
          "1": {
            "tn": "180",
            "num": "180"
          },
          "2": {
            "label": "88"
          }
        },
        {
          "0": {
            "label": "Is the net income/loss shown on the financial statements different from the net income/loss for income tax purposes?"
          },
          "1": {
            "tn": "201",
            "num": "201"
          },
          "2": {
            "label": "1"
          }
        },
        {
          "0": {
            "label": "Has the corporation made any charitable donations; gifts of cultural or ecological property; or gifts of medicine?"
          },
          "1": {
            "tn": "202",
            "num": "202"
          },
          "2": {
            "label": "2"
          }
        },
        {
          "0": {
            "label": "Has the corporation received any dividends or paid any taxable dividends for purposes of the dividend refund?"
          },
          "1": {
            "tn": "203",
            "num": "203"
          },
          "2": {
            "label": "3"
          }
        },
        {
          "0": {
            "label": "Is the corporation claiming any type of losses?"
          },
          "1": {
            "tn": "204",
            "num": "204"
          },
          "2": {
            "label": "4"
          }
        },
        {
          "0": {
            "label": "Is the corporation claiming a provincial or territorial tax credit or does it have a permanent establishment in more than one jurisdiction?"
          },
          "1": {
            "tn": "205",
            "num": "205"
          },
          "2": {
            "label": "5"
          }
        },
        {
          "0": {
            "label": "Has the corporation realized any capital gains or incurred any capital losses during the tax year?"
          },
          "1": {
            "tn": "206",
            "num": "206"
          },
          "2": {
            "label": "6"
          }
        },
        {
          "0": {
            'label': 'i) Is the corporation claiming the small business deduction and reporting a) income or loss' +
            ' from property (other than dividends deductible on line 320 of the T2 return),' +
            ' b) income from a partnership, c) income from a foreign business, d) income from a personal services ' +
            'business, e) income referred to in clause 125(1)(a)(i)(C) or 125(1)(a)(i)(B), or f) business limit assigned' +
            ' under subsection 125(3.2); or <br> ii) does the corporation have aggregate investment income at line 440?'
          },
          "1": {
            "tn": "207",
            "num": "207"
          },
          "2": {
            "label": "7"
          }
        },
        {
          "0": {
            "label": "Does the corporation have any property that is eligible for capital cost allowance?"
          },
          "1": {
            "tn": "208",
            "num": "208"
          },
          "2": {
            "label": "8"
          }
        },
        {
          "0": {
            "label": "Does the corporation have any property that is eligible capital property?"
          },
          "1": {
            "tn": "210",
            "num": "210"
          },
          "2": {
            "label": "10"
          }
        },
        {
          "0": {
            "label": "Does the corporation have any resource-related deductions?"
          },
          "1": {
            "tn": "212",
            "num": "212"
          },
          "2": {
            "label": "12"
          }
        },
        {
          "0": {
            "label": "Is the corporation claiming deductible reserves (other than transitional reserves under section 34.2)?"
          },
          "1": {
            "tn": "213",
            "num": "213"
          },
          "2": {
            "label": "13"
          }
        },
        {
          "0": {
            "label": "Is the corporation claiming a patronage dividend deduction?"
          },
          "1": {
            "tn": "216",
            "num": "216"
          },
          "2": {
            "label": "16"
          }
        },
        {
          "0": {
            "label": "Is the corporation a credit union claiming a deduction for allocations in proportion to borrowing or an additional deduction?"
          },
          "1": {
            "tn": "217",
            "num": "217"
          },
          "2": {
            "label": "17"
          }
        },
        {
          "0": {
            "label": "Is the corporation an investment corporation or a mutual fund corporation?"
          },
          "1": {
            "tn": "218",
            "num": "218"
          },
          "2": {
            "label": "18"
          }
        },
        {
          "0": {
            "label": "Is the corporation carrying on business in Canada as a non-resident corporation?"
          },
          "1": {
            "tn": "220",
            "num": "220"
          },
          "2": {
            "label": "20"
          }
        },
        {
          "0": {
            "label": "Is the corporation claiming any federal, provincial, or territorial foreign tax credits, or any federal logging tax credits?"
          },
          "1": {
            "tn": "221",
            "num": "221"
          },
          "2": {
            "label": "21"
          }
        },
        {
          "0": {
            "label": "Does the corporation have any Canadian manufacturing and processing profits?"
          },
          "1": {
            "tn": "227",
            "num": "227"
          },
          "2": {
            "label": "27"
          }
        },
        {
          "0": {
            "label": "Is the corporation claiming an investment tax credit?"
          },
          "1": {
            "tn": "231",
            "num": "231"
          },
          "2": {
            "label": "31"
          }
        },
        {
          "0": {
            "label": "Is the corporation claiming any scientific research and experimental development (SR&ED) expenditures?"
          },
          "1": {
            "tn": "232",
            "num": "232"
          },
          "2": {
            "label": "T661"
          }
        },
        {
          "0": {
            "label": "Is the total taxable capital employed in Canada of the corporation and its related corporations over $10,000,000?"
          },
          "1": {
            "tn": "233",
            "num": "233"
          },
          "2": {
            "label": "33/34/35"
          }
        },
        {
          "0": {
            "label": "Is the total taxable capital employed in Canada of the corporation and its associated corporations over $10,000,000?"
          },
          "1": {
            "tn": "234",
            "num": "234"
          },
          "2": {
            "label": "----"
          }
        },
        {
          "0": {
            "label": "Is the corporation claiming a surtax credit?"
          },
          "1": {
            "tn": "237",
            "num": "237"
          },
          "2": {
            "label": "37"
          }
        },
        {
          "0": {
            "label": "Is the corporation subject to gross Part VI tax on capital of financial institutions?"
          },
          "1": {
            "tn": "238",
            "num": "238"
          },
          "2": {
            "label": "38"
          }
        },
        {
          "0": {
            "label": "Is the corporation claiming a Part I tax credit?"
          },
          "1": {
            "tn": "242",
            "num": "242"
          },
          "2": {
            "label": "42"
          }
        },
        {
          "0": {
            "label": "Is the corporation subject to Part IV.1 tax on dividends received on taxable preferred shares or Part VI.1 tax on dividends paid?"
          },
          "1": {
            "tn": "243",
            "num": "243"
          },
          "2": {
            "label": "43"
          }
        },
        {
          "0": {
            "label": "Is the corporation agreeing to a transfer of the liability for Part VI.1 tax?"
          },
          "1": {
            "tn": "244",
            "num": "244"
          },
          "2": {
            "label": "45"
          }
        },
        {
          "0": {
            "label": "Is the corporation subject to Part II - Tobacco Manufacturers' surtax?"
          },
          "1": {
            "tn": "249",
            "num": "249"
          },
          "2": {
            "label": "46"
          }
        }]
    },
    "1011": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          cellClass: 'alignLeft'
        },
        {
          // "header": "Yes",
          colClass: 'half-col-width',
          "type": "singleCheckbox"
        },
        {
          // "header": "Schedule",
          "type": "none",
          cellClass: 'alignCenter',
          colClass: 'half-col-width'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "For financial institutions: Is the corporation a member of a related group of financial institutions with one or more members subject to gross Part VI tax?"
          },
          "1": {
            "tn": "250",
            "num": "250"
          },
          "2": {
            "label": "39"
          }
        },
        {
          "0": {
            "label": "Is the corporation claiming a Canadian film or video production tax credit refund?"
          },
          "1": {
            "tn": "253",
            "num": "253"
          },
          "2": {
            "label": "T1131"
          }
        },
        {
          "0": {
            "label": "Is the corporation claiming a film or video production services tax credit refund?"
          },
          "1": {
            "tn": "254",
            "num": "254"
          },
          "2": {
            "label": "T1177"
          }
        },
        {
          "0": {
            "label": "Is the corporation subject to Part XIII.1 tax? (Show your calculations on a sheet that you identify as Schedule 92.)"
          },
          "1": {
            "tn": "255",
            "num": "255"
          },
          "2": {
            "label": "92"
          }
        },
        {
          "0": {
            "label": "Did the corporation have any foreign affiliates in the tax year?"
          },
          "1": {
            "tn": "271",
            "num": "271"
          },
          "2": {
            "label": "T1134"
          }
        },
        {
          "0": {
            "label": "Did the corporation own or hold specified foreign property where the total cost amount of all" +
            " such property, at any time in the year, was more than CAN$100,000?"
          },
          "1": {
            "tn": "259",
            "num": "259"
          },
          "2": {
            "label": "T1135"
          }
        },
        {
          "0": {
            "label": "Did the corporation transfer or loan property to a non-resident trust?"
          },
          "1": {
            "tn": "260",
            "num": "260"
          },
          "2": {
            "label": "T1141"
          }
        },
        {
          "0": {
            "label": "Did the corporation receive a distribution from or was it indebted to a non-resident trust in the year?"
          },
          "1": {
            "tn": "261",
            "num": "261"
          },
          "2": {
            "label": "T1142"
          }
        },
        {
          "0": {
            "label": "Has the corporation entered into an agreement to allocate assistance for SR&ED carried out in Canada?"
          },
          "1": {
            "tn": "262",
            "num": "262"
          },
          "2": {
            "label": "T1145"
          }
        },
        {
          "0": {
            "label": "Has the corporation entered into an agreement to transfer qualified expenditures incurred in respect of SR&ED contracts?"
          },
          "1": {
            "tn": "263",
            "num": "263"
          },
          "2": {
            "label": "T1146"
          }
        },
        {
          "0": {
            "label": "Has the corporation entered into an agreement with other associated corporations for salary or wages of specified employees for SR&ED?"
          },
          "1": {
            "tn": "264",
            "num": "264"
          },
          "2": {
            "label": "T1174"
          }
        },
        {
          "0": {
            "label": "Did the corporation pay taxable dividends (other than capital gains dividends) in the tax year?"
          },
          "1": {
            "tn": "265",
            "num": "265"
          },
          "2": {
            "label": "55"
          }
        },
        {
          "0": {
            "label": "Has the corporation made an election under subsection 89(11) not to be a CCPC?"
          },
          "1": {
            "tn": "266",
            "num": "266"
          },
          "2": {
            "label": "T2002"
          }
        },
        {
          "0": {
            "label": "Has the corporation revoked any previous election made under subsection 89(11)?"
          },
          "1": {
            "tn": "267",
            "num": "267"
          },
          "2": {
            "label": "T2002"
          }
        },
        {
          "0": {
            "label": "Did the corporation (CCPC or deposit insurance corporation (DIC)) pay eligible dividends, or did its general rate income pool (GRIP) change in the tax year?"
          },
          "1": {
            "tn": "268",
            "num": "268"
          },
          "2": {
            "label": "53"
          }
        },
        {
          "0": {
            "label": "Did the corporation (other than a CCPC or DIC) pay eligible dividends, or did its low rate income pool (LRIP) change in the tax year?"
          },
          "1": {
            "tn": "269",
            "num": "269"
          },
          "2": {
            "label": "54"
          }
        }]
    },
    "401": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          colClass: 'std-input-width',
          "type": "none",
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width',
          "type": "none"
        },
        {
          "type": "none"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Amount C"
          },
          "1": {
            "num": "411",
            "saveNum": true,
            "disabled": true
          },
          "2": {
            "label": "x"
          },
          "3": {
            "tn": "415"
          },
          "4": {
            "label": "***"
          },
          "5": {
            cellClass: 'singleUnderline',
            "num": "415", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            "saveNum": true
          },
          "6": {
            "label": "D="
          },
          "8": {
            "num": "420",
            "saveNum": true,
            "disabled": true
          },
          "9": {
            "label": "E"
          }
        },
        {
          "1": {
            "type": "none"
          },
          "5": {
            "num": "469",
            "saveNum": true,
            "inputClass": "noBorder",
            "disabled": true
          },
          "8": {
            "type": "none"
          }
        }
      ]
    },
    "402": {
      "fixedRows": true,
      "infoTable": true,
      "verticalAlign": "middle",
      "columns": [
        {
          cellClass: 'alignLeft',
          "type": "none"
        },
        {
          colClass: 'small-input-width',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        }
      ],
      "cells": [{
        "0": {
          "label": "<b>General tax reduction for Canadian-controlled private corporations</b> – Amount I <b>multiplied</b> by "
        },
        "1": {
          "inputClass": "noBorder",
          "num": "488"
        },
        "2": {
          "label": "%="
        },
        "4": {
          "saveNum": true,
          "num": "443",
          "disabled": true
        },
        "5": {
          "label": "J"
        }
      }]
    },
    "403": {
      "fixedRows": true,
      "infoTable": true,
      "verticalAlign": "middle",
      "columns": [
        {
          cellClass: 'alignLeft',
          "type": "none"
        },
        {
          colClass: 'small-input-width',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        }
      ],
      "cells": [{
        "0": {
          "label": "<b>General tax reduction </b> – Amount Q <b>multiplied</b> by "
        },
        "1": {
          "inputClass": "noBorder",
          "num": "491"
        },
        "2": {
          "label": "%="
        },
        "4": {
          "saveNum": true,
          "num": "462",
          "disabled": true
        },
        "5": {
          "label": "R"
        }
      }]
    },
    "8000": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'small-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          cellClass: 'alignCenter',
          "type": "none"
        }
      ],
      "cells": [{
        "0": {
          "label": "Amount A, B, C, or H, whichever is the least"
        },
        "1": {
          "num": "1420"
        },
        "2": {
          "label": " x ",
          "labelClass": "center"
        },
        "3": {
          "label": "Number of days in the tax year before January 1, 2016",
          cellClass: 'singleUnderline alignCenter'
        },
        "5": {
          "num": "1421",
          cellClass: 'singleUnderline alignCenter',
          "disabled": true
        },
        "6": {
          "label": " x ",
          "labelClass": "center"
        },
        "7": {
          "num": "1423"
        },
        "8": {
          "label": " %= "
        },
        "9": {
          "num": "1424"
        },
        "10": {
          "label": "1"
        }
      },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "5": {
            "num": "1422",
            "disabled": true
          },
          "7": {
            "type": "none"
          },
          "9": {
            "type": "none"
          }
        },
        {
          "0": {
            "label": "Amount A, B, C, or H, whichever is the least"
          },
          "1": {
            "num": "1425"
          },
          "2": {
            "label": " x ",
            "labelClass": "center"
          },
          "3": {
            "label": "Number of days in the tax year after December 31, 2015",
            "labelClass": "center",
            cellClass: 'singleUnderline'
          },
          "5": {
            "num": "1426",
            cellClass: 'singleUnderline',
            "disabled": true
          },
          "6": {
            "label": " x ",
            "labelClass": "center"
          },
          "7": {
            "num": "1428",
            decimals: 1
          },
          "8": {
            "label": " %= "
          },
          "9": {
            "num": "1429"
          },
          "10": {
            "label": "2"
          }
        },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "Number of days in the tax year",

            "labelClass": "center"
          },
          "5": {
            "num": "1427",

            "disabled": true
          },
          "7": {
            "type": "none"
          },
          "9": {
            "type": "none"
          }
        },
        {
          "0": {
            "label": "Amount A, B, C, or H, whichever is the least"
          },
          "1": {
            "num": "1500"
          },
          "2": {
            "label": " x ",
            "labelClass": "center"
          },
          "3": {
            "label": "Number of days in the tax year before January 1, 2019",
            "labelClass": "center",
            cellClass: 'singleUnderline'
          },
          "5": {
            "num": "1501",
            cellClass: 'singleUnderline',
            "disabled": true
          },
          "6": {
            "label": " x ",
            "labelClass": "center"
          },
          "7": {
            "num": "1502",
            decimals: 1
          },
          "8": {
            "label": " %= "
          },
          "9": {
            "num": "1504"
          },
          "10": {
            "label": "3"
          }
        },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "Number of days in the tax year",

            "labelClass": "center"
          },
          "5": {
            "num": "1503",

            "disabled": true
          },
          "7": {
            "type": "none"
          },
          "9": {
            "type": "none"
          }
        }
        //comment out 2019 calcs for now
        // {
        //   "0": {
        //     "label": "Amount A, B, C, or H, whichever is the least"
        //   },
        //   "1": {
        //     "num": "1510"
        //   },
        //   "2": {
        //     "label": " x ",
        //     "labelClass": "center"
        //   },
        //   "3": {
        //     "label": "Number of days in the tax year after December 31, 2018",
        //     "labelClass": "center",
        //     cellClass: 'singleUnderline'
        //   },
        //   "5": {
        //     "num": "1506",
        //     cellClass: 'singleUnderline',
        //     "disabled": true
        //   },
        //   "6": {
        //     "label": " x ",
        //     "labelClass": "center"
        //   },
        //   "7": {
        //     "num": "1508",
        //     decimals: 1
        //   },
        //   "8": {
        //     "label": " %= "
        //   },
        //   "9": {
        //     "num": "1509"
        //   },
        //   "10": {
        //     "label": "4"
        //   }
        // },
        // {
        //   "1": {
        //     "type": "none"
        //   },
        //   "3": {
        //     "label": "Number of days in the tax year",
        //
        //     "labelClass": "center"
        //   },
        //   "5": {
        //     "num": "1507",
        //
        //     "disabled": true
        //   },
        //   "7": {
        //     "type": "none"
        //   },
        //   "9": {
        //     "type": "none"
        //   }
        // }
      ]
    },
    // "1300": {
    //   "type": "table",
    //   "fixedRows": true,
    //   "infoTable": true,
    //   "num": "1300",
    //   "columns": [{
    //     "width": "60%",
    //     "type": "none"
    //   },
    //     {
    //       "type": "none"
    //     }],
    //   "cells": [{
    //     "1": {
    //       "label": "If the result is positive, you have a <b>balance unpaid</b>. If the result is negative, you have an <b>overpayment</b>. Enter the amount on whichever line applies. Generally, we do not charge or refund a difference of $2 or less."
    //     }
    //   }]
    // },
    "T2_Principal_Products": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "header": "Principal product / service",
          "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
          cellClass: 'alignLeft',
          "width": "70%"
        },
        {
          "header": "Percentage", "validate": {"or": [{"length": {"min": "1", "max": "6"}}, {"check": "isEmpty"}]},
          "filters": "decimals 2",
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          cellClass: 'alignLeft'
        }
      ],
      "cells": [{
        "0": {
          "num": "284",
          "tn": "284",
          "disabled": true,
          type: 'text'
        },
        "1": {
          "num": "285",
          "tn": "285",
          "disabled": true,
          decimals: 3
        },
        "2": {
          "label": "%"
        }
      },
        {
          "0": {
            "num": "286",
            "tn": "286",
            "disabled": true,
            type: 'text'
          },
          "1": {
            "num": "287",
            "tn": "287",
            "disabled": true,
            decimals: 3
          },
          "2": {
            "label": "%"
          }
        },
        {
          "0": {
            "num": "288",
            "tn": "288",
            "disabled": true,
            type: 'text'
          },
          "1": {
            "num": "289",
            "tn": "289",
            "disabled": true,
            decimals: 3
          },
          "2": {
            "label": "%"
          }
        }]
    },
    "Direct_Deposit_Info": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          colClass: 'std-spacing-width',
          "type": "none"
        },
        {
          "header": "Branch Number",
          colClass: 'std-input-width',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          colClass: 'std-spacing-width',
          "type": "none"
        },
        {
          "header": "Institution number",
          colClass: 'std-input-col-width',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          colClass: 'std-spacing-width',
          "type": "none"
        },
        {
          "header": "Account Number",
          colClass: 'std-input-col-width',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          "type": "none"
        },
        {
          type: 'none',
          colClass: 'std-input-width'
        }
      ],
      "cells": [{
        "1": {
          num: '910', "validate": {"or": [{"matches": "^-?[.\\d]{5,5}$"}, {"check": "isEmpty"}]},
          "tn": "910",
          "cannotOverride": true
        },
        "3": {
          num: '914', "validate": {"or": [{"length": {"min": "3", "max": "4"}}, {"check": "isEmpty"}]},
          'type': 'text',
          "tn": "914",
          "cannotOverride": true
        },
        "5": {
          num: '918', "validate": {"or": [{"matches": "^-?[.\\d]{1,12}$"}, {"check": "isEmpty"}]},
          "tn": "918",
          "cannotOverride": true
        }
      }]
    },
    // "9000": {
    //   "type": "table",
    //   "fixedRows": true,
    //   "num": "9000",
    //   "infoTable": true,
    //   "columns": [
    //     {
    //       "width": "20px",
    //       "type": "none",
    //       "alignText": "right"
    //     },
    //     {
    //       "width": "70px",
    //       "formField": true
    //     },
    //     {
    //       "type": "none",
    //       width: '50px',
    //       "alignText": "left"
    //     },
    //     {
    //       width: '70px'
    //     },
    //     {
    //       width: '40px',
    //       type: 'none'
    //     },
    //     {
    //       "width": "300px",
    //       "type": "none",
    //       cellClass: 'alignCenter'
    //     },
    //     {
    //       colClass: 'std-input-width'
    //     },
    //     {
    //       width: '50px',
    //       type: 'none',
    //       alignText: 'left'
    //     },
    //     {
    //       colClass: 'std-input-width'
    //     },
    //     {
    //       type: 'none'
    //     }
    //   ],
    //   "cells": [
    //     {
    //       "0": {
    //         "label": "[(",
    //         labelClass: 'right'
    //       },
    //       "1": {
    //         "num": "9001",
    //         decimals: 4
    //       },
    //       "2": {
    //         "label": " ) + (",
    //         "labelClass": "center"
    //       },
    //       '3': {
    //         num: '9005'
    //       },
    //       '4': {
    //         label: '% x'
    //       },
    //       "5": {
    //         "label": "Number of days in the tax year after 2015",
    //         "labelClass": "center"
    //       },
    //       "6": {
    //         "num": "9002",
    //         cellClass: 'singleUnderline'
    //       },
    //       "7": {
    //         "label": ") % ] = "
    //       },
    //       '8': {
    //         num: '9003',
    //         decimals: 4
    //       }
    //     },
    //     {
    //       "1": {
    //         "type": "none"
    //       },
    //       '3': {
    //         type: 'none'
    //       },
    //       "5": {
    //         "label": "Number of days in the tax year",
    //         "labelClass": "center"
    //       },
    //       "6": {
    //         num: '9004'
    //       },
    //       "7": {
    //         type: 'none'
    //       },
    //       '8': {
    //         type: 'none'
    //       }
    //     }
    //   ]
    // },
    // "7000": {
    //   "type": "table",
    //   "fixedRows": true,
    //   "num": "7000",
    //   "infoTable": true,
    //   "columns": [
    //     {
    //       "width": "30px",
    //       "type": "none",
    //       "alignText": "right"
    //     },
    //     {
    //       "width": "70px",
    //       "formField": true
    //     },
    //     {
    //       "type": "none",
    //       width: '60px',
    //       "alignText": "left"
    //     },
    //     {
    //       width: '70px'
    //     },
    //     {
    //       width: '30px',
    //       type: 'none'
    //     },
    //     {
    //       "width": "300px",
    //       "type": "none",
    //       cellClass: 'alignCenter'
    //     },
    //     {
    //       "width": "80px"
    //     },
    //     {
    //       width: '50px',
    //       type: 'none',
    //       alignText: 'left'
    //     },
    //     {
    //       colClass: 'std-input-width'
    //     },
    //     {
    //       type: 'none'
    //     }
    //   ],
    //   "cells": [
    //     {
    //       "0": {
    //         "label": "[(",
    //         labelClass: 'right'
    //       },
    //       "1": {
    //         "num": "7001",
    //         decimals: 4
    //       },
    //       "2": {
    //         "label": " ) + (",
    //         "labelClass": "center"
    //       },
    //       '3': {
    //         num: '7002'
    //       },
    //       '4': {
    //         label: 'x'
    //       },
    //       "5": {
    //         "label": "Number of days in the tax year after 2015",
    //         "labelClass": "center"
    //       },
    //       "6": {
    //         "num": "7003",
    //         cellClass: 'singleUnderline'
    //       },
    //       "7": {
    //         "label": ") % ] = "
    //       },
    //       '8': {
    //         num: '7004',
    //         decimals: 5
    //       }
    //     },
    //     {
    //       "1": {
    //         "type": "none"
    //       },
    //       '3': {
    //         type: 'none'
    //       },
    //       "5": {
    //         "label": "Number of days in the tax year",
    //         "labelClass": "center"
    //       },
    //       "6": {
    //         num: '7005'
    //       },
    //       "7": {
    //         type: 'none'
    //       },
    //       '8': {
    //         type: 'none'
    //       }
    //     }
    //   ]
    // },
    // "1000": {
    //   "type": "table",
    //   "fixedRows": true,
    //   "num": "1000",
    //   "infoTable": true,
    //   "columns": [
    //     {
    //       "width": "30px",
    //       "type": "none",
    //       "alignText": "right"
    //     },
    //     {
    //       "width": "70px",
    //       "formField": true
    //     },
    //     {
    //       "type": "none",
    //       width: '60px',
    //       "alignText": "left"
    //     },
    //     {
    //       width: '70px'
    //     },
    //     {
    //       width: '30px',
    //       type: 'none'
    //     },
    //     {
    //       "width": "300px",
    //       "type": "none",
    //       cellClass: 'alignCenter'
    //     },
    //     {
    //       "width": "80px"
    //     },
    //     {
    //       width: '50px',
    //       type: 'none',
    //       alignText: 'left'
    //     },
    //     {
    //       colClass: 'std-input-width'
    //     },
    //     {
    //       type: 'none'
    //     }
    //   ],
    //   "cells": [
    //     {
    //       "0": {
    //         "label": "[(",
    //         labelClass: 'right'
    //       },
    //       "1": {
    //         "num": "1001",
    //         decimals: 2
    //       },
    //       "2": {
    //         "label": " ) - (",
    //         "labelClass": "center"
    //       },
    //       '3': {
    //         num: '1002',
    //         decimals: 2
    //       },
    //       '4': {
    //         label: 'x'
    //       },
    //       "5": {
    //         "label": "Number of days in the tax year after 2015",
    //         "labelClass": "center"
    //       },
    //       "6": {
    //         "num": "1003",
    //         cellClass: 'singleUnderline'
    //       },
    //       "7": {
    //         "label": ") % ] = "
    //       },
    //       '8': {
    //         num: '1004',
    //         decimals: 4
    //       }
    //     },
    //     {
    //       "1": {
    //         "type": "none"
    //       },
    //       '3': {
    //         type: 'none'
    //       },
    //       "5": {
    //         "label": "Number of days in the tax year",
    //         "labelClass": "center"
    //       },
    //       "6": {
    //         num: '1005'
    //       },
    //       "7": {
    //         type: 'none'
    //       },
    //       '8': {
    //         type: 'none'
    //       }
    //     }
    //   ]
    // },
    // "2000": {
    //   "type": "table",
    //   "fixedRows": true,
    //   "num": "2000",
    //   "infoTable": true,
    //   "columns": [
    //     {
    //       width: '70px'
    //     },
    //     {
    //       type: 'none',
    //       width: '10px'
    //     },
    //     {
    //       "width": "20px",
    //       "type": "none",
    //       "alignText": "right"
    //     },
    //     {
    //       "width": "70px",
    //       "formField": true
    //     },
    //     {
    //       "type": "none",
    //       width: '40px',
    //       "alignText": "left"
    //     },
    //     {
    //       width: '70px'
    //     },
    //     {
    //       width: '30px',
    //       type: 'none'
    //     },
    //     {
    //       "width": "300px",
    //       "type": "none",
    //       cellClass: 'alignCenter'
    //     },
    //     {
    //       "width": "80px"
    //     },
    //     {
    //       width: '50px',
    //       type: 'none',
    //       alignText: 'left'
    //     },
    //     {
    //       colClass: 'std-input-width'
    //     },
    //     {
    //       type: 'none'
    //     }
    //   ],
    //   "cells": [
    //     {
    //       '0': {
    //         num: '2006'
    //       },
    //       '1': {
    //         label: '÷'
    //       },
    //       "2": {
    //         "label": "[(",
    //         labelClass: 'right'
    //       },
    //       "3": {
    //         "num": "2001"
    //       },
    //       "4": {
    //         "label": " + ",
    //         "labelClass": "center"
    //       },
    //       '5': {
    //         num: '2002',
    //         decimals: 4
    //       },
    //       '6': {
    //         label: 'x'
    //       },
    //       "7": {
    //         "label": "Number of days in the tax year after 2015",
    //         "labelClass": "center"
    //       },
    //       "8": {
    //         "num": "2003",
    //         cellClass: 'singleUnderline'
    //       },
    //       "9": {
    //         "label": ")] = "
    //       },
    //       '10': {
    //         num: '2004',
    //         decimals: 4
    //       }
    //     },
    //     {
    //       '0': {
    //         type: 'none'
    //       },
    //       "3": {
    //         "type": "none"
    //       },
    //       '5': {
    //         type: 'none'
    //       },
    //       "7": {
    //         "label": "Number of days in the tax year",
    //         "labelClass": "center"
    //       },
    //       "8": {
    //         num: '2005'
    //       },
    //       "9": {
    //         type: 'none'
    //       },
    //       '10': {
    //         type: 'none'
    //       }
    //     }
    //   ]
    // },
    // "3000": {
    //   "type": "table",
    //   "fixedRows": true,
    //   "num": "3000",
    //   "infoTable": true,
    //   "columns": [
    //     {
    //       "width": "20px",
    //       "type": "none",
    //       "alignText": "right"
    //     },
    //     {
    //       "width": "70px",
    //       "formField": true
    //     },
    //     {
    //       "type": "none",
    //       width: '50px',
    //       "alignText": "left"
    //     },
    //     {
    //       width: '70px'
    //     },
    //     {
    //       width: '40px',
    //       type: 'none'
    //     },
    //     {
    //       "width": "300px",
    //       "type": "none",
    //       cellClass: 'alignCenter'
    //     },
    //     {
    //       colClass: 'std-input-width'
    //     },
    //     {
    //       width: '50px',
    //       type: 'none',
    //       alignText: 'left'
    //     },
    //     {
    //       colClass: 'std-input-width'
    //     },
    //     {
    //       type: 'none'
    //     }
    //   ],
    //   "cells": [
    //     {
    //       "0": {
    //         "label": "[(",
    //         labelClass: 'right'
    //       },
    //       "1": {
    //         "num": "3001", decimals: 4
    //       },
    //       "2": {
    //         "label": " ) + (",
    //         "labelClass": "center"
    //       },
    //       '3': {
    //         num: '3006'
    //       },
    //       '4': {
    //         label: 'x'
    //       },
    //       "5": {
    //         "label": "Number of days in the tax year after 2015",
    //         "labelClass": "center"
    //       },
    //       "6": {
    //         "num": "3002",
    //         cellClass: 'singleUnderline'
    //       },
    //       "7": {
    //         "label": ") % ] = "
    //       },
    //       '8': {
    //         num: '3003',
    //         decimals: 4
    //       }
    //     },
    //     {
    //       "1": {
    //         "type": "none"
    //       },
    //       '3': {
    //         type: 'none'
    //       },
    //       "5": {
    //         "label": "Number of days in the tax year",
    //         "labelClass": "center"
    //       },
    //       "6": {
    //         num: '3004'
    //       },
    //       "7": {
    //         type: 'none'
    //       },
    //       '8': {
    //         type: 'none'
    //       }
    //     }
    //   ]
    // },
    "PSBfraction": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-col-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-col-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          "type": "none",
          cellClass: 'alignCenter',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-col-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        }
      ],
      "cells": [{
        "0": {
          "label": "Taxable income from a personal services business"
        },
        "1": {
          "num": "555", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          "tn": "555"
        },
        "2": {
          "label": " x ",
          "labelClass": "center"
        },
        "3": {
          "label": "Number of days in the tax year after December 31, 2015",
          "labelClass": "center",
          cellClass: 'singleUnderline',
        },
        "5": {
          "num": "4002",
          cellClass: 'singleUnderline',
          "disabled": true
        },
        "6": {
          "label": " x ",
          "labelClass": "center"
        },
        "7": {
          "num": "4003"
        },
        "8": {
          "label": " %= "
        },
        "9": {
          "num": "560", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}, "tn": "560"
        },
        "10": {
          "label": "B"
        }
      },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "Total number of days in the taxation year",

            "labelClass": "center",
            "labelWidth": '110%'
          },
          "5": {
            "num": "4005",

            "disabled": true
          },
          "7": {
            "type": "none"
          },
          "9": {
            "type": "none"
          }
        }
      ]
    },
    "5000": {
      "infoTable": true,
      "hasTotals": true,
      "columns": [
        {
          "header": 'J<br>Name of corporation receiving the income and assigned amount',
          "textAlign": 'center',
          type: 'text'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "header": 'J<br>Business number of the corporation receiving the assigned amount',
          "tn": '490',
          "num": '490',
          "textAlign": 'center',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "header": 'K<br>Income paid under clause 125(1)(a)(i)(B) to the corporation identified in column J' + '3'.sup(),
          "tn": '500',
          "num": '500', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          "textAlign": 'center',
          "total": true,
          "totalNum": '510',
          "totalTn": '510'
        },
        {
          "type": "none",
          colClass: 'small-input-width'
        },
        {
          "header": 'L<br>Business limit assigned to corporation identified in column J' + '4'.sup(),
          "tn": '505',
          "num": '505', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          "textAlign": 'center',
          "total": true,
          "totalNum": '515',
          totalMessage: '',
          "totalTn": '515'
        }
      ]
    },
    "1101": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          colClass: 'small-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          "type": "none"
        },
        {
          colClass: 'qtr-col-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'qtr-col-width'
        },
        {
          colClass: 'std-spacing-width',
          "type": "none"
        },
        {
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Amount A"
          },
          "1": {
            "num": "1400"
          },
          "2": {
            "label": "x",
            "labelClass": "center"
          },
          "3": {
            "label": "Number of days in the tax year before January 1, 2016",
            cellClass: 'singleUnderline',
            "labelClass": "center"
          },
          "5": {
            "num": "1401",
            cellClass: 'singleUnderline'
          },
          "6": {
            "label": " x ",
            "labelClass": "center"
          },
          "7": {"num": "1480"},
          "9": {"num": '1480-1'},
          "10": {
            "label": "/"
          },
          "11": {
            "num": "1480-2"
          },
          "12": {
            "label": "%= "
          },
          "13": {
            "num": "441"
          },
          "14": {
            "label": "1"
          }
        },
        {
          "1": {"type": 'none'},
          "3": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "5": {"num": "1402"},
          "7": {"type": 'none'},
          "9": {"type": 'none'},
          "11": {"type": 'none'},
          "13": {"type": 'none'}
        },
        {
          "0": {
            "label": "Amount A"
          },
          "1": {
            "num": "1403"
          },
          "2": {
            "label": " x ",
            "labelClass": "center"
          },
          "3": {
            "label": "Number of days in the tax year after December 31, 2015",
            "labelClass": "center",
            cellClass: 'singleUnderline'
          },
          "5": {
            "num": "1404",
            cellClass: 'singleUnderline'
          },
          "6": {
            "label": " x ",
            "labelClass": "center"
          },
          "7": {"num": "1406"},
          "9": {"num": '1406-1'},
          "10": {
            "label": " / "
          },
          "11": {"num": '1406-2'},
          "12": {
            "label": " %= "
          },
          "13": {
            "num": "1407"
          },
          "14": {
            "label": "2"
          }
        },
        {
          "1": {"type": 'none'},
          "3": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "5": {"num": "1405"},
          "7": {"type": 'none'},
          "9": {"type": 'none'},
          "11": {"type": 'none'},
          "13": {"type": 'none'}
        }
      ]
    },
    "1102": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          colClass: 'small-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          "type": "none"
        },
        {
          colClass: 'qtr-col-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Amount C"
          },
          "1": {
            "num": "1408"
          },
          "2": {
            "label": " x ",
            "labelClass": "center"
          },
          "3": {
            "label": "Number of days in the tax year before January 1, 2016",
            "labelClass": "center",
            cellClass: 'singleUnderline'
          },
          "5": {
            "num": "1409",
            cellClass: 'singleUnderline'
          },
          "6": {
            "label": " x ",
            "labelClass": "center"
          },
          "7": {"num": "1481"},
          "9": {"num": '1481-1'},
          "10": {
            "label": " / "
          },
          "11": {
            "num": "1481-2"
          },
          "12": {
            "label": " %=  "
          },
          "13": {
            "num": "446"
          },
          "14": {
            "label": "3"
          }
        },
        {
          "1": {"type": 'none'},
          "3": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "5": {"num": "1410"},
          "7": {"type": 'none'},
          "9": {"type": 'none'},
          "11": {"type": 'none'},
          "13": {"type": 'none'}
        },
        {
          "0": {
            "label": "Amount C"
          },
          "1": {
            "num": "1411"
          },
          "2": {
            "label": " x ",
            "labelClass": "center"
          },
          "3": {
            "label": "Number of days in the tax year after December 31, 2015",
            "labelClass": "center",
            cellClass: 'singleUnderline'
          },
          "5": {
            "num": "1412",
            cellClass: 'singleUnderline'
          },
          "6": {
            "label": " x ",
            "labelClass": "center"
          },
          "7": {"num": "1415"},
          "9": {"type": 'none'},
          "11": {"type": 'none'},
          "12": {
            "label": " %= "
          },
          "13": {
            "num": "1414"
          },
          "14": {
            "label": "4"
          }
        },
        {
          "1": {"type": 'none'},
          "3": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "5": {"num": "1413"},
          "7": {"type": 'none'},
          "9": {"type": 'none'},
          "11": {"type": 'none'},
          "13": {"type": 'none'}
        }
      ]
    },
    "1103": {
      width: '855px',
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Number of days in the tax year before January 1, 2016",
            "labelClass": "center",
            cellClass: 'singleUnderline'
          },
          "2": {
            "num": "1470",
            cellClass: 'singleUnderline'
          },
          "3": {
            "label": " x ",
            "labelClass": "center"
          },
          "4": {
            "num": "2001"
          },
          "6": {
            "type": "none"
          },
          "8": {"type": 'none'},
          "9": {
            "label": "%="
          },
          "10": {
            "num": "1452",
            decimals: '5'
          },
          "11": {
            "label": "5"
          }
        },
        {
          "0": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "2": {"num": "1471"},
          "4": {"type": 'none'},
          "6": {"type": 'none'},
          "8": {"type": 'none'},
          "10": {"type": 'none'}
        },
        {
          "0": {
            "label": "Number of days in the tax year after December 31, 2015",
            "labelClass": "center",
            cellClass: 'singleUnderline'
          },
          "2": {
            "num": "1453",
            cellClass: 'singleUnderline'
          },
          "3": {
            "label": " x ",
            "labelClass": "center"
          },
          "4": {
            "num": "1455"
          },
          "6": {"num": '1455-1'},
          "7": {
            "label": " / "
          },
          "8": {
            "num": "1455-2"
          },
          "9": {
            "label": " %= "
          },
          "10": {
            "num": "1456",
            decimals: '5'
          },
          "11": {
            "label": "6"
          }
        },
        {
          "0": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "2": {
            "num": "1454"
          },
          "4": {"type": 'none'},
          "6": {"type": 'none'},
          "8": {"type": 'none'},
          "10": {"type": 'none'}
        }
      ]
    },
    "1104": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          cellClass: 'alignCenter',
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Amount G"
          },
          "1": {
            "num": "1457"
          },
          "2": {
            "label": " x ",
            "labelClass": "center"
          },
          "3": {
            "label": "100",
            "labelClass": "center",
            cellClass: 'singleUnderline'
          },
          "5": {
            "num": "2006",
            cellClass: 'singleUnderline'
          },
          "6": {
            "label": " = "
          },
          "7": {
            "num": "1460"
          },
          "8": {
            "label": "I"
          }
        },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "H",
            "labelClass": "center"
          },
          "5": {
            "num": "1459",
            decimals: '4'
          },
          "7": {
            "type": "none"
          }
        }
      ]
    },
    "1105": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Foreign business income tax credit from line 636 on page 8"
          },
          "2": {
            "num": "1431"
          },
          "3": {
            "label": " x ",
            "labelClass": "center"
          },
          "4": {
            "num": "1432"
          },
          "5": {
            "label": " = "
          },
          "6": {
            "num": "1433",
            cellClass: 'singleUnderline'
          },
          "7": {
            "label": "M"
          }
        }
      ]
    },
    "1106": {
      "fixedRows": true,
      "infoTable": true,
      "columns": proRateCols,
      "cells": [
        {
          "0": {
            "label": "Amount O"
          },
          "2": {
            "num": "1437"
          },
          "3": {
            "label": " x ",
            "labelClass": "center"
          },
          "4": {
            "label": "Number of days in the tax year before January 1, 2016",
            "labelClass": "center",
            cellClass: 'singleUnderline'
          },
          "6": {
            "num": "1438",
            cellClass: 'singleUnderline'
          },
          "7": {
            "label": " x ",
            "labelClass": "center"
          },
          "8": {"num": "1445"},
          "10": {"num": '1445-1'},
          "11": {
            "label": " / "
          },
          "12": {
            "num": "1445-2"
          },
          "13": {
            "label": " %= "
          },
          "14": {
            "num": "1440",
            decimals: '5'
          },
          "15": {
            "label": "7"
          }
        },
        {
          "2": {"type": 'none'},
          "4": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "6": {"num": "1439"},
          "8": {"type": 'none'},
          "10": {"type": 'none'},
          "12": {"type": 'none'},
          "14": {"type": 'none'}
        },
        {
          "0": {
            "label": "Amount O"
          },
          "2": {
            "num": "1441"
          },
          "3": {
            "label": " x ",
            "labelClass": "center"
          },
          "4": {
            "label": "Number of days in the tax year after December 31, 2015",
            "labelClass": "center",
            cellClass: 'singleUnderline'
          },
          "6": {
            "num": "1442",
            cellClass: 'singleUnderline'
          },
          "7": {
            "label": " x ",
            "labelClass": "center"
          },
          "8": {"num": "1446"},
          "10": {"num": "1446-1"},
          "11": {
            "label": " / "
          },
          "12": {
            "num": "1446-2"
          },
          "13": {
            "label": " %= "
          },
          "14": {
            "num": "1444",
            decimals: '5'
          },
          "15": {
            "label": "8"
          }
        },
        {
          "2": {"type": 'none'},
          "4": {
            "label": "Number of days in the tax year",

            "labelClass": "center"
          },
          "6": {"num": "1443"},
          "8": {"type": 'none'},
          "10": {"type": 'none'},
          "12": {"type": 'none'},
          "14": {"type": 'none'}
        }
      ]
    },
    "DividendRefund": {
      "fixedRows": true,
      "infoTable": true,
      "columns": proRateCols,
      "cells": [
        {
          "0": {
            "label": "Amount E"
          },
          "2": {
            "num": "2637"
          },
          "3": {
            "label": " x ",
            "labelClass": "center"
          },
          "4": {
            "label": "Number of days in the tax year before January 1, 2016",
            "labelClass": "center",
            cellClass: 'singleUnderline'
          },
          "6": {
            "num": "2438",
            cellClass: 'singleUnderline'
          },
          "7": {
            "label": " x ",
            "labelClass": "center"
          },
          "8": {"num": "2445"},
          "10": {"num": '2445-1'},
          "11": {
            "label": " / "
          },
          "12": {
            "num": "2445-2"
          },
          "13": {
            "label": " %= "
          },
          "14": {
            "num": "2440"
          },
          "15": {
            "label": "1"
          }
        },
        {
          "2": {"type": 'none'},
          "4": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "6": {"num": "2439"},
          "8": {"type": 'none'},
          "10": {"type": 'none'},
          "12": {"type": 'none'},
          "14": {"type": 'none'}
        },
        {
          "0": {
            "label": "Amount E"
          },
          "2": {
            "num": "3637"
          },
          "3": {
            "label": " x ",
            "labelClass": "center"
          },
          "4": {
            "label": "Number of days in the tax year before January 1, 2016",
            "labelClass": "center",
            cellClass: 'singleUnderline'
          },
          "6": {
            "num": "3438",
            cellClass: 'singleUnderline'
          },
          "7": {
            "label": " x ",
            "labelClass": "center"
          },
          "8": {"num": "3445"},
          "10": {"num": '3445-1'},
          "11": {
            "label": " / "
          },
          "12": {
            "num": "3445-2"
          },
          "13": {
            "label": " %= "
          },
          "14": {
            "num": "3440"
          },
          "15": {
            "label": "2"
          }
        },
        {
          "2": {"type": 'none'},
          "4": {
            "label": "Number of days in the tax year",

            "labelClass": "center"
          },
          "6": {"num": "3439"},
          "8": {"type": 'none'},
          "10": {"type": 'none'},
          "12": {"type": 'none'},
          "14": {"type": 'none'}
        }
      ]
    },
    "ForamountH": {
      "fixedRows": true,
      "infoTable": true,
      "columns": proRateCols,
      "cells": [
        {
          "0": {
            "label": "Amount D or G, whichever is less"
          },
          "2": {
            "num": "2437"
          },
          "3": {
            "label": " x ",
            "labelClass": "center"
          },
          "4": {
            "label": "Number of days in the tax year before January 1, 2016",
            "labelClass": "center",
            cellClass: 'singleUnderline'
          },
          "6": {
            "num": "2538",
            cellClass: 'singleUnderline'
          },
          "7": {
            "label": " x ",
            "labelClass": "center"
          },
          "8": {"num": "2545"},
          "10": {"num": '2545-1'},
          "11": {
            "label": " / "
          },
          "12": {
            "num": "2545-2"
          },
          "13": {
            "label": " %= "
          },
          "14": {
            "num": "2540"
          },
          "15": {
            "label": "1"
          }
        },
        {
          "2": {"type": 'none'},
          "4": {
            "label": "Number of days in the tax year",

            "labelClass": "center"
          },
          "6": {"num": "2539"},
          "8": {"type": 'none'},
          "10": {"type": 'none'},
          "12": {"type": 'none'},
          "14": {"type": 'none'}
        },
        {
          "0": {
            "label": "Amount D or G, whichever is less"
          },
          "2": {
            "num": "3437"
          },
          "3": {
            "label": " x ",
            "labelClass": "center"
          },
          "4": {
            "label": "Number of days in the tax year before January 1, 2016",
            "labelClass": "center",
            cellClass: 'singleUnderline'
          },
          "6": {
            "num": "3538",
            cellClass: 'singleUnderline'
          },
          "7": {
            "label": " x ",
            "labelClass": "center"
          },
          "8": {"num": "3545"},
          "10": {"num": '3545-1'},
          "11": {
            "label": " / "
          },
          "12": {
            "num": "3545-2"
          },
          "13": {
            "label": " %= "
          },
          "14": {
            "num": "3540"
          },
          "15": {
            "label": "2"
          }
        },
        {
          "2": {"type": 'none'},
          "4": {
            "label": "Number of days in the tax year",

            "labelClass": "center"
          },
          "6": {"num": "3539"},
          "8": {"type": 'none'},
          "10": {"type": 'none'},
          "12": {"type": 'none'},
          "14": {"type": 'none'}
        }
      ]
    },
    '1999': {
      'infoTable': true,
      'fixedRows': true,
      'dividers': [2, 4],
      'columns': [
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'text', disabled: true, cannotOverride: true},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'text', disabled: true, cannotOverride: true},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'text', disabled: true, cannotOverride: true}
      ],
      'cells': [
        {
          '0': {tn: '095'},
          '2': {tn: '096'},
          '4': {tn: '898'}
        }
      ]
    },
    '4000': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-input-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-input-width'}
      ],
      'cells': [
        {
          '0': {tn: '955'},
          '1': {num: '955', type: 'date', 'cannotOverride': true},
          '4': {type: 'text'},
          '6': {tn: '956'},
          '7': {
            num: '956', type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
            'cannotOverride': true
          }
        },
        {
          '1': {label: 'Date', cellClass: 'alignCenter'},
          '4': {label: 'Signature of the authorized signing officer of the corporation', cellClass: 'alignCenter'},
          '7': {label: 'Telephone Number', cellClass: 'alignCenter'}
        }
      ]
    },
    '4010': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none'}
      ],
      'cells': [
        {
          '0': {label: 'I,'},
          '1': {tn: '950'},
          '2': {num: '950', type: 'text', 'cannotOverride': true},
          '4': {tn: '951'},
          '5': {num: '951', type: 'text', 'cannotOverride': true},
          '7': {tn: '954'},
          '8': {num: '954', type: 'text', 'cannotOverride': true}
        },
        {
          '2': {label: 'Last name', cellClass: 'alignCenter'},
          '5': {label: 'First name', cellClass: 'alignCenter'},
          '8': {label: 'Position, office, or rank', cellClass: 'alignCenter'}
        }
      ]
    }
  }
})();
