(function() {
  'use strict';
  var foreign_countries = JSON.parse(JSON.stringify(wpw.tax.codes.countryAddressCodes));
  delete foreign_countries.CA;
  var foreignCountries = new wpw.tax.actions.codeToDropdownOptions(foreign_countries, 'value');

  var dateTimeConditionArr = [
    {
      switchIf: {
        formId: 'cp',
        fieldId: '063',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '066',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '071',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '072',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '076',
        value: '1'
      }
    }
  ];

  wpw.tax.global.formData.t2j = {
    'formInfo': {
      'abbreviation': 'T2J',
      'title': 'T2 Jacket',
      'code': 'Code 1601',
      hideHeaderBox: true,
      schedule: '200',
      printTitle: 'T2 Corporation Income Tax Return',
      printNum: '200',
      'headerImage': 'canada-federal',
      agencyUseOnlyBox: {
        tn: '055',
        headerClass: 'alignRight',
        textAbove: ' '
      },
      disclaimerShowWhen: {fieldId: 'cp.179', check: 'isYes'},
      disclaimerTextField: {formId: 'cp', fieldId: '183'},
      description: [
        {
          'text': 'This form serves as a federal, provincial, and territorial corporation income' +
          ' tax return, unless the corporation is located in Quebec or Alberta. If the corporation is' +
          ' located in one of these provinces, you have to file a separate provincial corporation return.'
        },
        {
          'text': 'All legislative references on this return are to the federal ' +
          '<i><i>Income Tax Act</i> and Income Tax Regulations</i>. This return ' +
          'may contain changes that had not yet become law at the time of publication.'
        },
        {
          'text': 'Send one completed copy of this return, including schedules and the ' +
          '<i>General Index of Financial Information</i> (GIFI), to your tax centre or tax services office. ' +
          'You have to file the return ' +
          'within six months after the end of the corporation\'s tax year.'
        },
        {
          'text': 'For more information see cra.gc.ca or Guide T4012, <i>T2 Corporation – Income Tax Guide</i>.'
        }
      ],
      'descriptionHighlighted': true,
      'highlightFieldsets': true,
      category: 'Federal Tax Forms',
      formFooterNum: 'T2 E (16)'

    },
    sections: [
      {
        forceBreakAfter: true,
        'header': 'Identification',
        'rows': [
          {
            'type': 'multiColumn',
            'dividers': [
              false
            ],
            'columns': [
              [
                {
                  'type': 'infoField',
                  'inputType': 'custom',
                  'format': [
                    '{N9}RC{N4}'
                  ],
                  'label': 'Business number (BN)',
                  'labelClass': 'bold',
                  'num': '001',
                  'tn': '001',
                  'disabled': true,
                  'cannotOverride': true
                }
              ],
              []
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'splitTable',
            'side1': [
              {
                'label': 'Corporation\'s name',
                'labelClass': 'bold'
              },
              {
                'type': 'infoField',
                'maxLength': '175',
                'minLength': '1',
                'num': '002',
                'tn': '002',
                'disabled': true,
                'cannotOverride': true,
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '175'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'width': '93%'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'labelClass': 'fullLength bold',
                'label': 'Address of head office'
              },
              {
                'type': 'infoField',
                'label': 'Has the address of head office changed since the last time we was notified?',
                'num': '010',
                'tn': '010',
                'inputType': 'radio',
                'disabled': true,
                'cannotOverride': true
              },
              {
                'label': '(If <b>yes</b>, complete lines 011 to 018.)',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'inputType': 'address',
                'add1Num': '011',
                'add2Num': '012',
                'provNum': '016',
                'cityNum': '015',
                'countryNum': '017',
                'postalCodeNum': '018',
                'add1Tn': '011',
                'add2Tn': '012',
                'cityTn': '015',
                'provTn': '016',
                'countryTn': '017',
                'postalTn': '018',
                'cannotOverride': true

              },
              {
                'labelClass': 'fullLength bold',
                'label': 'Mailing address (if different from head office address)'
              },
              {
                'type': 'infoField',
                'label': 'Has this address changed since the last time we were notified?',
                'num': '020',
                'tn': '020',
                'inputType': 'radio',
                'labelWidth': '70%',
                'disabled': true,
                'cannotOverride': true
              },
              {
                'label': '(If <b>yes</b>, complete lines 021 to 028.)',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'label': 'c/o',
                'labelAfterNum': true,
                'inputClass': 'std-input-width-2',
                'num': '021',
                'tn': '021',
                'labelClass': 'bold',
                'disabled': true,
                'cannotOverride': true,
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '30'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                }
              },
              {
                'type': 'infoField',
                'inputType': 'address',
                'add1Num': '022',
                'add2Num': '023',
                'provNum': '026',
                'cityNum': '025',
                'countryNum': '027',
                'postalCodeNum': '028',
                'add1Tn': '022',
                'add2Tn': '023',
                'cityTn': '025',
                'provTn': '026',
                'countryTn': '027',
                'postalTn': '028',
                'cannotOverride': true
              },
              {
                'labelClass': 'fullLength bold',
                'label': 'Location of books and records (if different from head office address)'
              },
              {
                'type': 'infoField',
                'label': 'Has the location of books and records changed since the last time we were notified?',
                'labelWidth': '70%',
                'num': '030',
                'tn': '030',
                'inputType': 'radio',
                'disabled': true,
                'cannotOverride': true
              },
              {
                'label': '(If <b>yes</b>, complete lines 031 to 038.)',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'inputType': 'address',
                'add1Num': '031',
                'add2Num': '032',
                'provNum': '036',
                'cityNum': '035',
                'countryNum': '037',
                'postalCodeNum': '038',
                'add1Tn': '031',
                'add2Tn': '032',
                'cityTn': '035',
                'provTn': '036',
                'countryTn': '037',
                'postalTn': '038',
                'cannotOverride': true
              },
              {
                'type': 'splitInputs',
                'inputType': 'radio',
                'label': '<b>Type of corporation at the end of the tax year</b> (tick one)',
                'divisions': 2,
                'disabled': true,
                'cannotOverride': true,
                'num': '040',
                'tn': '040',
                'other': {
                  'label': '5. Other corporation (specify below)',
                  'value': '5',
                  'alwaysShow': true,
                  'valueNum': '1067',
                  'disabled': true,
                  'cannotOverride': true
                },
                'items': [
                  {
                    'label': 'Canadian-controlled private corporation (CCPC)',
                    'value': '1'
                  },
                  {
                    'label': 'Other private corporation',
                    'value': '2'
                  },
                  {
                    'label': 'Public corporation',
                    'value': '3'
                  },
                  {
                    'label': 'Corporation controlled by a public corporation',
                    'value': '4'
                  }
                ]
              },
              {
                'type': 'infoField',
                'label': 'If the type of corporation changed during the tax year, provide the effective date of the change',
                'inputType': 'date',
                'num': '043',
                'tn': '043',
                'width': '40%',
                'labelWidth': '50%',
                'disabled': true,
                cannotOverride: true
              }
            ],
            'side2': [
              {
                'label': 'To which tax year does this return apply?',
                'labelClass': 'bold fullLength'
              },
              {
                'type': 'infoField',
                'inputType': 'dateRange',
                'label1': 'Tax year start',
                'num': '060',
                'disabled': true,
                'tn1': '060',
                'source1': 'cp-tax_start',
                'cannotOverride': true,
                'label2': 'Tax year-end',
                'num2': '061',
                'tn2': '061',
                'source2': 'cp-tax_end',
                'dateTimeShowWhen': {
                  'or': dateTimeConditionArr
                }
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'Has there been an acquisition of control resulting in the application of subsection 249(4) since the tax year start on line 060?',
                'labelClass': 'bold',
                'labelWidth': '70%',
                'num': '063',
                'tn': '063',
                'inputType': 'radio',
                'disabled': true, cannotOverride: true
              },
              {
                'type': 'infoField',
                'inputType': 'date',
                'label': 'If yes, provide the date control was acquired',
                'num': '065',
                'tn': '065',
                'width': '40%',
                'labelWidth': '50%',
                'disabled': true,
                'dateTimeShowWhen': {
                  'or': dateTimeConditionArr
                }, cannotOverride: true
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'Is the date on line 061 a deemed tax year-end according to subsection 249(3.1)?',
                'labelClass': 'bold',
                'labelWidth': '70%',
                'num': '066',
                'tn': '066',
                'inputType': 'radio',
                'disabled': true, cannotOverride: true
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'Is the corporation a professional corporation that is a member of a partnership?',
                'labelClass': 'bold',
                'labelWidth': '70%',
                'num': '067',
                'tn': '067',
                'inputType': 'radio',
                'disabled': true, cannotOverride: true
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Is this the first year of filing after:',
                'labelClass': 'fullLength bold'
              },
              {
                'type': 'infoField',
                'label': 'Incorporation? ',
                'labelClass': 'tabbed',
                'labelWidth': '70%',
                'num': '070',
                'tn': '070',
                'inputType': 'radio',
                'disabled': true,
                'cannotOverride': true
              },
              {
                'type': 'infoField',
                'label': 'Amalgamation?',
                'labelClass': 'tabbed',
                'labelWidth': '70%',
                'num': '071',
                'tn': '071',
                'inputType': 'radio',
                'disabled': true,
                'cannotOverride': true
              },
              {
                'label': 'If <b>yes</b>, complete lines 030 to 038 and attach Schedule 24.',
                'labelClass': 'fullLength'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'Has there been a wind-up of a subsidiary under section 88 during the current tax year? ',
                'labelClass': 'bold',
                'labelWidth': '70%',
                'num': '072',
                'inputType': 'radio',
                'tn': '072',
                'cannotOverride': true,
                'disabled': true
              },
              {
                'label': 'If <b>yes</b>, complete and attach Schedule 24.',
                'labelClass': 'fullLength'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'Is this the final tax year before amalgamation?',
                'labelClass': 'bold',
                'labelWidth': '70%',
                'num': '076',
                'inputType': 'radio',
                'tn': '076',
                'disabled': true,
                'cannotOverride': true
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'Is this the final return up to dissolution?',
                'labelClass': 'bold',
                'labelWidth': '70%',
                'num': '078',
                'inputType': 'radio',
                'tn': '078',
                'disabled': true, cannotOverride: true
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'If an election was made under section 261, state the functional currency used?',
                'labelClass': 'bold',
                'labelWidth': '50%',
                'num': '079',
                'width': '40%',
                'tn': '079',
                'disabled': true,
                'validate': {
                  'or': [
                    {
                      'matches': '^-?[.\\d]{2,2}$'
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                }
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Is the corporation a resident of Canada?',
                'labelClass': 'bold',
                'type': 'infoField',
                'num': '080',
                'tn': '080',
                'inputType': 'radio',
                'init': '1',
                'disabled': true,
                'cannotOverride': true
              },
              {
                'labelClass': 'fullLength',
                'label': 'If <b>no</b>, give the country of residence on line 081 and complete and attach Schedule 97.'
              },
              {
                'type': 'infoField',
                'inputType': 'dropdown',
                'textAlign': 'right',
                'num': '081',
                'tn': '081',
                'options': foreignCountries,
                'disabled': true,
                'cannotOverride': true
              },
              {
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'num': '082',
                'tn': '082',
                'inputType': 'radio',
                'label': 'Is the non-resident corporation claiming an exemption under an income tax treaty?',
                'labelClass': 'bold',
                'labelWidth': '70%',
                'disabled': true
              },
              {
                'label': 'If <b>yes</b>, complete and attach Schedule 91.',
                'labelClass': 'fullLength'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'splitInputs',
                'inputType': 'radio',
                'label': 'If the corporation is exempt from tax under section 149, tick one of the following boxes:',
                'divisions': 1,
                'disabled': true,
                'cannotOverride': true,
                'num': '085',
                'tn': '085',
                'items': [
                  {
                    'label': 'Exempt under paragraph 149(1)(e) or (l)',
                    'value': '1'
                  },
                  {
                    'label': 'Exempt under paragraph 149(1)(j)',
                    'value': '2'
                  },
                  {
                    'label': 'Exempt under paragraph 149(1)(t)',
                    'value': '3'
                  },
                  {
                    'label': 'Exempt under other Paragraphs of section 149',
                    'value': '4'
                  }
                ]
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Software approval code',
                'input2': true,
                'disabled': true,
                'num': '099',
                'tn': '099',
                'type': 'text',
                'showWhen': 'hideTn099And101',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '4',
                        'max': '4'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                }
              },
              {
                'label': 'Software version',
                'disabled': true,
                'cannotOverride': true,
                'input2': true,
                'num': '101',
                'tn': '101',
                'type': 'text',
                'showWhen': 'hideTn099And101',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '30'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                }
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'label': 'Do not use this area',
            'labelClass': 'fullLength titleFont center'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'table',
            'num': '1999'
          }
        ]
      },
      {
        'header': 'Attachments',
        'rows': [
          {
            'label': '<b>Financial statement information:</b> Use GIFI schedules 100, 125, and 141.',
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Schedules</b> - Answer the following questions. For each <b>yes</b> response, ' +
            '<b>attach</b> the schedule to the T2 return, unless otherwise instructed',
            labelClass: 'fullLength'
          },
          {
            'type': 'table',
            'num': '100',
            forceBreakAfter: true
          },
          {
            'type': 'table',
            'num': '1011'
          }
        ]
      },
      {
        'header': 'Additional Information',
        'rows': [
          {
            'type': 'infoField',
            'label': 'Did the corporation use the International Financial Reporting Standards (IFRS) when it prepared its financial statements?',
            'num': '270',
            'inputType': 'radio',
            'tn': '270',
            'labelWidth': '70%', cannotOverride: true
          },
          {
            'type': 'infoField',
            'label': 'Is the corporation inactive?',
            'num': '280',
            'inputType': 'radio',
            'tn': '280',
            'disabled': true,
            'labelWidth': '70%', cannotOverride: true
          },
          {
            'label': 'Specify the principal products mined, manufactured, sold, constructed, or services provided, giving the approximate percentage of the total revenue that each product or service represents.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Main activity to generate business activity',
            'type': 'infoField',
            'colClass': 'std-input-width',
            'num': '299', cannotOverride: true
          },
          {
            'type': 'table',
            'num': 'T2_Principal_Products'
          },
          {
            'type': 'infoField',
            'label': 'Did the corporation immigrate to Canada during the tax year?',
            'num': '291',
            'inputType': 'radio',
            'tn': '291',
            'disabled': true, cannotOverride: true
          },
          {
            'type': 'infoField',
            'label': 'Did the corporation emigrate from Canada during the tax year?',
            'num': '292',
            'inputType': 'radio',
            'tn': '292',
            'disabled': true,
            'labelWidth': '70%', cannotOverride: true
          },
          {
            'type': 'infoField',
            'label': 'Do you want to be considered as a quarterly instalment remitter if you are eligible?',
            'num': '293',
            'inputType': 'radio',
            'tn': '293',
            'labelWidth': '70%', cannotOverride: true
          },
          {
            'type': 'infoField',
            'label': 'If the corporation was eligible to remit instalments on a quarterly basis for part of the tax year, provide the date the corporation ceased to be eligible',
            'num': '294',
            'inputType': 'date',
            'labelWidth': '70%',
            'tn': '294', cannotOverride: true
          },
          {
            'type': 'infoField',
            'label': 'If the corporation\'s major business activity is construction, did you have any subcontractors during the tax year?',
            'num': '295',
            'inputType': 'radio',
            'tn': '295',
            'labelWidth': '70%', cannotOverride: true
          }
        ]
      },
      {
        forceBreakAfter: true,
        'header': 'Taxable income',
        'rows': [
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Net income or (loss) for income tax purposes from Schedule 1, financial statements, or GIFI',
            'indicator': 'A',
            'columns': [
              {
                'underline': 'single',
                'padding': {
                  'type': 'tn',
                  'data': '300'
                },
                'input': {
                  'num': '300',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': false
            },
            'label': 'Deduct:',
            'labelCellClass': 'bold'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Charitable donations from Schedule 2',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '311'
                },
                'input': {
                  'num': '311',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Cultural gifts from Schedule 2',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '313'
                },
                'input': {
                  'num': '313',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Ecological gifts from Schedule 2',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '314'
                },
                'input': {
                  'num': '314',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Gifts of medicine from Schedule 2',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '315'
                },
                'input': {
                  'num': '315',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Taxable dividends deductible under section 112 or 113, or subsection 138(6) from Schedule 3',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '320'
                },
                'input': {
                  'num': '320',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true
                }
              },
              null
            ], cannotOverride: true
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Part VI.1 tax deduction*',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '325'
                },
                'input': {
                  'num': '325',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Non-capital losses of previous tax years from Schedule 4',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '331'
                },
                'input': {
                  'num': '331',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true
                }
              },
              null
            ], cannotOverride: true
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Net capital losses of previous tax years from Schedule 4',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '332'
                },
                'input': {
                  'num': '332',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true
                }
              },
              null
            ], cannotOverride: true
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Restricted farm losses of previous tax years from Schedule 4',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '333'
                },
                'input': {
                  'num': '333',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true
                }
              },
              null
            ], cannotOverride: true
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Farm losses of previous tax years from Schedule 4',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '334'
                },
                'input': {
                  'num': '334',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              null
            ], cannotOverride: true
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Limited partnership losses of previous tax years from Schedule 4',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '335'
                },
                'input': {
                  'num': '335',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              null
            ], cannotOverride: true
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Taxable capital gains or taxable dividends allocated from a central credit union',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '340'
                },
                'input': {
                  'num': '340',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Prospector\'s and grubstaker\'s shares',
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'single',
                'padding': {
                  'type': 'tn',
                  'data': '350'
                },
                'input': {
                  'num': '350',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false
            },
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'indicator': 'B',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '351',
                  'disabled': true, cannotOverride: true
                }
              },
              {
                'underline': 'single',
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                },
                'input': {
                  'num': '372',
                  'disabled': true, cannotOverride: true
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false
            },
            'label': 'Subtotal (amount A <b>minus</b> amount B) (if negative, enter "0")',
            'labelCellClass': 'alignRight',
            'indicator': 'C',
            'columns': [
              {
                'input': {
                  'disabled': true,
                  'num': '352', cannotOverride: true
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': false
            },
            'label': 'Add:',
            'labelCellClass': 'bold'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Section 110.5 additions or subparagraph 115(1)(a)(vii) additions',
            'labelCellClass': 'indent',
            'indicator': 'D',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '355'
                },
                'input': {
                  'num': '355',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': '<b>Taxable income</b> (amount C <b>plus</b> amount D)',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '360'
                },
                'underline': 'double',
                'input': {
                  'num': '360',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }, cannotOverride: true
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Income exempt under paragraph 149(1)(t)',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '370'
                },
                'input': {
                  'num': '370',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': '<b>Taxable income</b> for a corporation with exempt income under paragraph 149(1)(t) (line 360 <b>minus</b> line 370)',
            'indicator': 'Z',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '371'
                },
                'underline': 'double',
                'input': {
                  'num': '371', cannotOverride: true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': false
            },
            'label': '* This amount is equal to 3.5 times the Part VI.1 tax payable at line 724 on page 9.'
          }
        ]
      },
      {
        forceBreakAfter: true,
        'header': 'Small business deduction',
        'spacing': 'R',
        'rows': [
          {
            'label': 'Canadian-controlled private corporations (CCPCs) throughout the tax year',
            'labelClass': 'fullLength bold'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Income from active business carried on in Canada from Schedule 7',
            'indicator': 'A',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '400'
                },
                'input': {
                  'num': '400',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Taxable income from line 360 on page 3, minus 100/28 of the amount on line 632* on page 8, ' +
            '<b>minus</b> 4 times the amount on line 636** on page 8, and <b>minus</b> any amount that, because of federal law, is exempt from Part I tax',
            'indicator': 'B',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '405'
                },
                'input': {
                  'num': '405',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Business limit (see notes 1 and 2 below)',
            'indicator': 'C',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '410'
                },
                'input': {
                  'num': '410',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ]
          },
          {
            'label': 'Notes:',
            'labelClass': 'bold'
          },
          {
            'label': '1. For CCPCs that are not associated, enter $500,000 on line 410. However, if the corporation\'s ' +
            'tax year is less than 51 weeks, prorate this amount by the number of days in the tax year <b>divided</b>' +
            ' by 365, and enter the result on line 410.',
            'labelClass': 'fullLength'
          },
          {
            'label': '2. For associated CCPCs, use Schedule 23 to calculate the amount to be entered on line 410.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Business limit reduction:',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '401'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Reduced business limit (amount C <b>minus</b> amount E) (if negative, enter \"0\").',
            'indicator': 'F',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '425'
                },
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true,
                  'num': '425'
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Business limit the CCPC assigns under subsection 125(3.2) (from line 515 below)',
            'indicator': 'G',
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '408',
                  'disabled': true
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Amount F <b>minus</b> amount G',
            'indicator': 'H',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '427'
                },
                'underline': 'double',
                'input': {
                  'num': '427',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true, cannotOverride: true
                }
              }
            ]
          },
          {
            'label': 'Small business deduction',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '8000'
          },
          {
            'layout': 'alignInput',
            labelCellClass: 'alignRight',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false
            },
            'label': 'Total of amounts 1, 2, and 3 (enter amount I on line J on page 8)',
            'indicator': 'I',
            'labelClass': 'alignRight',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '430'
                },
                'underline': 'double',
                'input': {
                  'num': '430',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ]
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'asterisk',
                'items': [
                  {
                    'label': 'Calculate the amount of foreign non-business income tax credit deductible on line 632 without reference to the refundable tax on the CCPC\'s investment income (line 604) and without reference to the corporate tax reductions under section 123.4'
                  },
                  {
                    'label': 'Calculate the amount of foreign business income tax credit deductible on line 636 without reference to the corporation tax reductions under section 123.4.'
                  },
                  {
                    'label': '<b>Large corporations</b>',
                    'sublist': [
                      'If the corporation is not associated with any corporations in both the current and previous tax years, the amount to be entered on line 415 is: (total taxable capital employed in Canada for the <b>prior</b> year <b>minus</b> $10,000,000) x 0.225%.',
                      'If the corporation is not associated with any corporations in the current tax year, but was associated in the previous tax year, the amount to be entered on line 415 is: (total taxable capital employed in Canada for the <b>current</b> year <b>minus</b> $10,000,000) x 0.225%.',
                      'For corporations associated in the current tax year, see Schedule 23 for the special rules that apply.'
                    ]
                  }
                ]
              }
            ]
          },
          {
            'label': 'Specified corporate income and assignment under subsection 125(3.2)',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Applicable to tax years that begin after March 21, 2016',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Except that, if the tax year of your corporation started before <b>and</b> ends on or after March 22, 2016 and in the tax year of a CCPC, you can make an assignment of business limit to that other CCPC if its tax year started after March 21, 2016.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '5000'
          },
          {
            'label': 'Notes:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': '3.  This amount is [as defined in subsection 125(7) <b>specified corporate income</b> (a)(i)]' +
            ' the total of all amounts each of which is income from an active business of the corporation for the year' +
            ' from the provision of services or property to a private corporation (directly or indirectly, ' +
            'in any manner whatever) if',
            'labelClass': 'fullLength'
          },
          {
            'label': '(A)at any time in the year, the corporation (or one of its shareholders) or a person who ' +
            'does not deal at arm\'s length with the corporation (or one of its shareholders) holds a direct or indirect' +
            ' interest in the private corporation, and',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '(B) it is not the case that all or substantially all of the corporation\'s income for the year ' +
            'from an active business is from the provision of services or property to',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '(I) persons (other than the private corporation) with which the corporation deals at arm\'s length, or',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '(II) partnerships with which the corporation deals at arm\'s length, other than a partnership in which a person that does not deal at arm\'s length with the corporation holds a direct or indirect interest.',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '4. The amount of the business limit you assign to a CCPC cannot be greater than the amount determined by the formula A – B, where A is the amount of ' +
            'income referred to in column K in respect of that CCPC and B is the portion of the amount described in A that is deductible by you in respect of the ' +
            'amount of income referred to in clauses 125(1)(a)(i)(A) or (B) for the year. The amount on line 515 cannot be greater than the amount on line 425.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'General tax reduction for Canadian-controlled private corporations',
        'rows': [
          {
            'label': 'Canadian-controlled private corporations throughout the tax year',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxable income from page 3 (line 360 or amount Z, whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '421'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Lesser of amounts B9 and H9 from Part 9 of Schedule 27',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '422', cannotOverride: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Amount K13 from Part 13 of Schedule 27',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '423', cannotOverride: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Personal services business income',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '432',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '432'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'label': 'Amount used to calculate the credit union deduction (amount F from Schedule 17)',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '424'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 400, 405, 410, or 427 / Amount H on page 4, whichever is the least',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '426'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F'
                }
              }
            ]
          },
          {
            'label': 'Aggregate investment income from line 440 on page 6*',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '407'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> amounts B to G)',
            'layout': 'alignInput',
            labelCellClass: 'alignRight',
            'layoutOptions': {
              'showDots': false,
              'indicatorColumn': true
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '428',
                  'disabled': true, cannotOverride: true
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '999',
                  'disabled': true, cannotOverride: true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': 'Amount A <b>minus</b> amount H (if negative, enter \"0\")',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '429',
                  'disabled': true, cannotOverride: true
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'type': 'table',
            'num': '402'
          },
          {
            'label': 'Enter amount J on line 638 on page 8.',
            'labelClass': 'fullLength'
          },
          {
            'label': '* Except for a corporation that is, throughout the year, a cooperative corporation (within the meaning assigned by subsection 136(2)) or a credit union.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        forceBreakAfter: true,
        'header': 'General tax reduction',
        'rows': [
          {
            'label': 'Do not complete this area if you are a Canadian-controlled private corporation, an investment corporation, a mortgage investment corporation, a mutual fund corporation, or any corporation with taxable income that is not subject to the corporation tax rate of 38%.',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxable income from page 3 (line 360 or amount Z, whichever applies).',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '444', cannotOverride: true
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': 'Lesser of amounts B9 and H9 from Part 9 of Schedule 27',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '447', cannotOverride: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'L'
                }
              }
            ]
          },
          {
            'label': 'Amount K13 from Part 13 of Schedule 27',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '448', cannotOverride: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'M'
                }
              }
            ]
          },
          {
            'label': 'Personal services business income',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '434',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '434'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'N'
                }
              }
            ]
          },
          {
            'label': 'Amount used to calculate the credit union deduction (amount F from Schedule 17)',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '435',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'N'
                }
              }
            ]
          },
          {
            'label': 'Subtotal(<b>add</b> amounts L to O)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '451',
                  'disabled': true, cannotOverride: true
                }
              },
              {
                'underline': 'double',
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                },
                'input': {
                  'num': '995',
                  'disabled': true, cannotOverride: true
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'label': 'Amount K<b> minus</b> amount P (if negative, enter \"0\")',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '452',
                  'disabled': true, cannotOverride: true
                }
              }
            ],
            'indicator': 'Q'
          },
          {
            'type': 'table',
            'num': '403'
          },
          {
            'label': 'Enter amount R on line 639 on page 8.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Refundable portion of Part I tax',
        'rows': [
          {
            'label': 'Canadian-controlled private corporations throughout the tax year',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Aggregate investment income from Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '440',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '440'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'A'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '1101'
          },
          {
            'label': 'Subtotal (amount 1 <b>plus</b> amount 2)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': false,
              'indicatorColumn': true
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1450',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'data': '1450'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1451',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Foreign investment income from Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '445',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '445'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '1102'
          },
          {
            'label': 'Subtotal (amount 3 <b>plus</b> amount 4)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': false,
              'indicatorColumn': true
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1416'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'label': 'Foreign non-business income tax credit from line 632 on page 8 <b>minus</b> amount D (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '1417'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Amount B <b>minus</b> amount E (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '466', cannotOverride: true
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Foreign non-business income tax credit from line 632 on page 8',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '992'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '1103'
          },
          {
            'label': 'Subtotal (amount 5 <b>plus</b> amount 6)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': false,
              'indicatorColumn': true
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1461',
                  'decimals': 4
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'H'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '1104'
          },
          {
            'label': 'Taxable income from line 360 on page 3',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '467', cannotOverride: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'J'
                }
              }
            ]
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Amount from line 400, 405, 410, or 427 on page 4, whichever is the least',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': '',
            'columns': [
              {
                'input': {
                  'num': '468', cannotOverride: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'K'
                }
              },
              null
            ]
          },
          {
            'label': 'Amount I',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '1430', cannotOverride: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'L'
                }
              },
              null
            ]
          },
          {
            'type': 'table',
            'num': '1105'
          },
          {
            'label': 'Subtotal (total of amounts K to M)',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': false,
              'indicatorColumn': true
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1434', cannotOverride: true
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1435', cannotOverride: true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'N'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount J <b>minus</b> amount N)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'input': {
                  'num': '1436', cannotOverride: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'O'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '1106'
          },
          {
            'label': 'Subtotal (amount 7 <b>plus</b> amount 8)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1447', cannotOverride: true
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1448', cannotOverride: true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'label': 'Part I tax payable <b>minus</b> investment tax credit refund (line 700 <b>minus</b> line 780 from page 9)',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '1449'
                }
              }
            ],
            'indicator': 'Q'
          },
          {
            'label': '<b>Refundable portion of Part I tax</b> - Amount F, P, or Q, whichever is the least',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '450',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }, cannotOverride: true
                },
                'padding': {
                  'type': 'tn',
                  'data': '450'
                }
              }
            ],
            'indicator': 'R'
          }
        ]
      },
      {
        forceBreakAfter: true,
        'header': 'Refundable dividend tax on hand',
        'rows': [
          {
            'label': 'Refundable dividend tax on hand at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '460',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '460'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Deduct:</b><br>Dividend refund for the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '465',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '465'
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '478',
                  'disabled': true, cannotOverride: true
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '899',
                  'disabled': true, cannotOverride: true
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': '<b>Add</b>'
          },
          {
            'label': 'Refundable portion of Part I tax from line 450 on page 6',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '479', cannotOverride: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Total Part IV tax payable from Schedule 3',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '481', cannotOverride: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Net refundable dividend tax on hand transferred from a predecessor corporation on amalgamation, or from a wound-up subsidiary corporation',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '480',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '480'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (add amounts B, C, and line 480)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '482',
                  'disabled': true, cannotOverride: true
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1898',
                  'disabled': true, cannotOverride: true
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': '<b>Refundable dividend tax on hand at the end of the tax year </b> - Amount A<b> plus </b>amount D',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '485',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '485'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Dividend refund',
        'rows': [
          {
            'label': 'Private and subject corporations at the time taxable dividends were paid in the tax year ',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Taxable dividends paid in the tax year from line 460 on page 3 of Schedule 3',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '500'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': 'DividendRefund'
          },
          {
            'label': 'Subtotal (amount 1 <b>plus</b> amount 2)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '501', cannotOverride: true
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '502', cannotOverride: true
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Refundable dividend tax on hand at the end of the tax year from line 485 above',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '504'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': '<b>Dividend refund</b> - Amount F or G, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1505', cannotOverride: true
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': 'Dividend refund from amount H on page 7',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        forceBreakAfter: true,
        'header': 'Part I tax',
        'rows': [
          {
            'label': 'Base amount Part I tax - Taxable income from page 3 (line 360 or amount Z, whichever applies) <b>multiplied</b> by 38%',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '550',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '550'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': '<b>Additional tax on personal services business income</b> (section 123.5)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': 'PSBfraction'
          },
          {
            'label': 'Recapture of investment tax credit from Schedule 31',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '602',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '602'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Calculation for the refundable tax on the Canadian-controlled private corporation\'s (CCPC) investment income',
            'labelClass': 'fullLength bold'
          },
          {
            'label': '(if it was a CCPC throughout the tax year)',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Aggregate investment income from line 440 on page 6',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '551',
                  'disabled': true, cannotOverride: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'label': 'Taxable income from line 360 on page 3',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '552',
                  'disabled': true, cannotOverride: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              },
              null
            ]
          },
          {
            'label': 'Deduct:',
            'labelClass': 'fullLength bold tabbed'
          },
          {
            'label': 'Amount from line 400, 405, 410, or 427 on page 4, whichever is the least',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '553',
                  'disabled': true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F'
                }
              },
              null
            ]
          },
          {
            'label': 'Net amount (amount E <b>minus</b> amount F)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': false,
              'indicatorColumn': true
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '554',
                  'disabled': true, cannotOverride: true
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '897',
                  'disabled': true, cannotOverride: true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': 'ForamountH'
          },
          {
            'label': 'Refundable tax on CCPC\'s investment income (amount 1 <b>plus</b> amount 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '604',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '604'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1604',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': 'Subtotal (<b>add</b> amounts A, B, C, and H)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': false,
              'indicatorColumn': true
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'input': {
                  'num': '605',
                  'disabled': true, cannotOverride: true
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Small business deduction from line 430 on page 4',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '607', cannotOverride: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'J'
                }
              }
            ]
          },
          {
            'label': 'Federal tax abatement',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '608',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '608'
                }
              },
              null
            ]
          },
          {
            'label': 'Manufacturing and processing profits deduction from Schedule 27',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '616',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '616'
                }
              },
              null
            ]
          },
          {
            'label': 'Investment corporation deduction',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '620',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '620'
                }
              },
              null
            ]
          },
          {
            'label': 'Taxed capital gains',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '624',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '624'
                }
              },
              null
            ]
          },
          {
            'label': 'Additional deduction - credit unions from Schedule 17',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '628',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '628'
                }
              },
              null
            ]
          },
          {
            'label': 'Federal foreign non-business income tax credit from Schedule 21',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '632',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '632'
                }
              },
              null
            ]
          },
          {
            'label': 'Federal foreign business income tax credit from Schedule 21',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '636',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '636'
                }
              },
              null
            ]
          },
          {
            'label': 'General tax reduction for CCPCs from amount J on page 5.',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '638',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '638'
                }
              },
              null
            ]
          },
          {
            'label': 'General tax reduction from amount R on page 5',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '639',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '639'
                }
              },
              null
            ]
          },
          {
            'label': 'Federal logging tax credit from Schedule 21',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '640',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '640'
                }
              },
              null
            ]
          },
          {
            'label': 'Eligible Canadian bank deduction under section 125.21',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '641',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '641'
                }
              },
              null
            ]
          },
          {
            'label': 'Federal qualifying environmental trust tax credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '648',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '648'
                }
              },
              null
            ]
          },
          {
            'label': 'Investment tax credit from Schedule 31',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '652',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '652'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': false,
              'indicatorColumn': true
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '653',
                  'disabled': true, cannotOverride: true
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '895',
                  'disabled': true, cannotOverride: true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': '<b>Part I tax payable</b> - Amount I <b>minus</b> amount K',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '654',
                  'disabled': true, cannotOverride: true
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': 'Enter amount L on line 700 on page 9.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Privacy statement',
        'headerClass': 'italic',
        'rows': [
          {
            'label': 'Personal information is collected under the <i>Income Tax Act</i> to administer tax, benefits,' +
            ' and related programs. It may also be used for any purpose related to the administration or enforcement' +
            ' of the Act such as audit, compliance and the payment of debts owed to the Crown. It may be shared or ' +
            'verified with other federal, provincial/territorial government institutions to the extent authorized by' +
            ' law. Failure to provide this information may result in interest payable, penalties or other actions. ' +
            'Under the <i>Privacy Act</i>, individuals have the right to access their personal information and request ' +
            'correction if there are errors or omissions.<br>' +
            'Refer to Info Source' +
            ' <a href="http://www.cra-arc.gc.ca/gncy/tp/nfsrc/nfsrc-eng.html">cra.gc.ca/gncy/tp/nfsrc/nfsrc-eng.html</a>, ' +
            'personal information bank CRA PPU 047.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        forceBreakAfter: true,
        'header': 'Summary of tax and credits',
        'rows': [
          {
            'label': 'Federal tax',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Part I tax payable from amount L on page 8',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '700',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }, cannotOverride: true
                },
                'padding': {
                  'type': 'tn',
                  'data': '700'
                }
              }
            ]
          },
          {
            'label': 'Part II surtax payable from Schedule 46',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '708',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '708'
                }
              }
            ]
          },
          {
            'label': 'Part III.1 tax payable from Schedule 55',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '710',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '710'
                }
              }
            ]
          },
          {
            'label': 'Part IV tax payable from Schedule 3',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '712',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '712'
                }
              }
            ]
          },
          {
            'label': 'Part IV.1 tax payable from Schedule 43',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '716',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '716'
                }
              }
            ]
          },
          {
            'label': 'Part VI tax payable from Schedule 38',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '720',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '720'
                }
              }
            ]
          },
          {
            'label': 'Part VI.1 tax payable from Schedule 43',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '724',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '724'
                }
              }
            ]
          },
          {
            'label': 'Part XIII.1 tax payable from Schedule 92',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '727',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '727'
                }
              }
            ]
          },
          {
            'label': 'Part XIV tax payable from Schedule 20',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '728',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '728'
                }
              }
            ]
          },
          {
            'label': 'Total federal tax',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '730',
                  'disabled': true, cannotOverride: true
                }
              }
            ]
          },
          {
            'label': 'Add Provincial or Territorial Tax:',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'infoField',
            'inputType': 'dropdown',
            'num': '750',
            'tn': '750',
            'label': 'Provincial or territorial jurisdiction',
            'labelClass': 'tabbed',
            'indicatorColumn': true,
            'options': [
              {
                'option': 'Alberta',
                'value': 'AB'
              },
              {
                'option': 'British Columbia',
                'value': 'BC'
              },
              {
                'option': 'Manitoba',
                'value': 'MB'
              },
              {
                'option': 'New Brunswick',
                'value': 'NB'
              },
              {
                'option': 'Newfoundland and Labrador',
                'value': 'NL'
              },
              {
                'option': 'Newfoundland and Labrador Offshore',
                'value': 'XO'
              },
              {
                'option': 'Nova Scotia',
                'value': 'NS'
              },
              {
                'option': 'Nova Scotia Offshore',
                'value': 'NO'
              },
              {
                'option': 'Northwest Territories',
                'value': 'NT'
              },
              {
                'option': 'Nunavut',
                'value': 'NU'
              },
              {
                'option': 'Ontario',
                'value': 'ON'
              },
              {
                'option': 'Prince Edward Island',
                'value': 'PE'
              },
              {
                'option': 'Quebec',
                'value': 'QC'
              },
              {
                'option': 'Saskatchewan',
                'value': 'SK'
              },
              {
                'option': 'Yukon Territories',
                'value': 'YT'
              },
              {
                'option': 'Multi-jurisdiction',
                'value': 'MJ'
              },
              {
                'option': 'Outside Canada',
                'value': 'OC'
              }
            ]
          },
          {
            'label': '(if more than one jurisdiction, enter \'Multi-jurisdiction\' and complete Schedule 5)',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'Net provincial or territorial tax payable (except Quebec and Alberta)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '760',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '760'
                }
              }
            ]
          },
          {
            'label': 'Total tax payable',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': false,
              'indicatorColumn': true
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'input': {
                  'num': '770',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }, cannotOverride: true
                },
                'padding': {
                  'type': 'tn',
                  'data': '770'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Deduct other credits:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Investment tax credit refund from Schedule 31',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '780',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '780'
                }
              },
              null
            ]
          },
          {
            'label': 'Dividend refund from amount U on page 7',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '784',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '784'
                }
              },
              null
            ]
          },
          {
            'label': 'Federal capital gains refund from Schedule 18',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '788',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '788'
                }
              },
              null
            ]
          },
          {
            'label': 'Federal qualifying environmental trust tax credit refund',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '792',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '792'
                }
              },
              null
            ]
          },
          {
            'label': 'Canadian film or video production tax credit refund (Form T1131)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '796',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '796'
                }
              },
              null
            ]
          },
          {
            'label': 'Film or video production services tax credit refund (Form T1177)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '797',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '797'
                }
              },
              null
            ]
          },
          {
            'label': 'Tax withheld at source',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '800',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '800'
                }
              },
              null
            ]
          },
          {
            'label': 'Total payments on which tax has been withheld',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '801',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '801'
                }
              },
              null,
              null,
              null
            ]
          },
          {
            'label': 'Provincial and territorial capital gains refund from Schedule 18',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '808',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '808'
                }
              },
              null
            ]
          },
          {
            'label': 'Provincial and territorial refundable tax credits from Schedule 5',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '812',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '812'
                }
              },
              null
            ]
          },
          {
            'label': 'Tax instalments paid',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '840',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '840'
                }
              },
              null
            ]
          },
          {
            'label': 'Total Credits ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': false,
              'indicatorColumn': true
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '890',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '890'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '889',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Balance (amount A<b> minus</b> amount B)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': false,
              'indicatorColumn': true
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'input': {
                  'num': '891',
                  'disabled': true
                }
              }
            ]
          },
          {
            'label': 'If the result is positive, you have a <b>balance unpaid</b>. If the result is negative, you have an <b>overpayment</b>. Enter the amount on whichever line applies. Generally, we do not charge or refund a difference of $2 or less.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'splitTable',
            'fieldAlignRight': true,
            'side1': [
              {
                'label': 'Balance unpaid',
                'labelClass': 'bold text-right',
                'layout': 'alignInput',
                'layoutOptions': {
                  'indicatorColumn': true,
                  'showDots': false,
                  'paddingRight': false
                },
                'labelCellClass': 'alignRight',
                'columns': [
                  {
                    'underline': 'double',
                    'input': {
                      'num': '892',
                      'disabled': true
                    }
                  },
                  null
                ]
              },
              {
                'label': 'For information on how to make your payment, go to <a href="http://www.cra-arc.gc.ca/payments/">cra.gc.ca/payments</a>.',
                'labelClass': 'fullLength'
              }
            ],
            'side2': [
              {
                'label': 'Overpayment',
                'labelClass': 'text-right bold',
                'layout': 'alignInput',
                'layoutOptions': {
                  'indicatorColumn': true,
                  'showDots': false,
                  'paddingRight': false
                },
                'labelCellClass': 'alignRight',
                'columns': [
                  {
                    'underline': 'double',
                    'input': {
                      'num': '893',
                      'disabled': true
                    }
                  },
                  null
                ]
              },
              {
                'type': 'infoField',
                'label': 'Refund code',
                'inputType': 'dropdown',
                'num': '894',
                'tn': '894',
                'labelWidth': '80px',
                'init': '0',
                'options': [
                  {
                    'option': ' ',
                    'value': '0'
                  },
                  {
                    'option': '1. Refund the overpayment',
                    'value': '1'
                  },
                  {
                    'option': '2. Transfer the overpayment to next year\'s instalment account',
                    'value': '2'
                  },
                  {
                    'option': '3. Apply the overpayment to another liability or to a different account',
                    'value': '3'
                  }
                ]
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'splitTable',
            'side1': [
              {
                label: 'Direct deposit request',
                labelClass: 'bold'
              },
              {
                'label': 'To have the corporation\'s refund deposited directly into the corporation\'s bank account at a financial institution in Canada, or to change banking information you already gave us, complete the information below:',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'label': 'How would you like to receive refund payments?',
                'inputType': 'dropdown',
                'num': '900',
                'cannotOverride': true,
                'disabled': true,
                'options': [
                  {
                    'option': 'Current Method of Payment',
                    'value': '0'
                  },
                  {
                    'option': 'Start Refunds via Direct Deposit',
                    'value': '1'
                  },
                  {
                    'option': 'Change Direct Deposit Banking Information',
                    'value': '2'
                  }
                ]
              }
            ],
            'side2': [
              {
                'type': 'infoField',
                'label': 'If the corporation is a Canadian-controlled private corporation throughout the tax year, does it qualify for the one-month extension of the date the balance of tax is due?',
                'inputType': 'radio',
                'num': '896',
                'tn': '896',
                'labelWidth': 'calc((100% - 157px))'
              },
              {
                'label': 'If this return was prepared by a tax preparer for a fee, provide their EFILE number',
                'type': 'infoField',
                'num': '920',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '5',
                        'max': '5'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '920',
                'labelWidth': '70%'
              }
            ]
          },
          {
            'type': 'table',
            'fixedRows': true,
            'infoTable': true,
            'num': 'Direct_Deposit_Info'
          }
        ]
      },
      {
        'header': 'Certification',
        'rows': [
          {
            'type': 'table',
            'num': '4010'
          },
          {
            'label': 'am an authorized signing officer of the corporation. I certify that I have examined this return, ' +
            'including accompanying schedules and statements, and that the information given on this return is, to the ' +
            'best of my knowledge, correct and complete. I also certify that the method of calculating income for ' +
            'this tax year is consistent with that of the previous tax year except as specifically disclosed in a' +
            ' statement attached to this return.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '4000'
          },
          {
            'type': 'infoField',
            'label': 'Is the contact person the same as the authorized signing officer? If no, complete the information below:',
            'num': '957',
            'inputType': 'radio',
            'width': '30%',
            'tn': '957',
            'cannotOverride': true
          },
          {
            'type': 'infoField',
            'label': 'Name of other authorized person',
            'num': '958',
            'width': '30%',
            'tn': '958',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '1',
                    'max': '60'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cannotOverride': true
          },
          {
            'label': 'Telephone number',
            'num': '959',
            type: 'infoField',
            inputType: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
            'width': '30%',
            'tn': '959',
            'cannotOverride': true,
            validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
          },
          {
            'type': 'infoField',
            'label': 'Extension',
            'labelClass': 'tabbed',
            'num': '617',
            'width': '10%',
            'cannotOverride': true
          }
        ]
      },
      {
        'header': 'Language of correspondence - Langue de correspondance',
        'rows': [
          {
            'type': 'infoField',
            'label': 'Indicate your language of correspondence by entering <b>1</b> for English or <b>2</b> for French.<br>Indiquez votre langue de correspondance en inscrivant <b>1</b> pour anglais ou <b>2</b> pour français.',
            'inputType': 'dropdown',
            'labelWidth': 'calc((100% - 157px))',
            'num': '990',
            'tn': '990',
            'cannotOverride': true,
            'disabled': true,
            'options': [
              {
                'option': " " ,
                'value': "0"
              },
              {
                'option': "1: English",
                'value': "1"
              },
              {
                'option': "2: French",
                'value': "2"
              }
            ]
          }
        ]
      }
    ]
  };
})();
