(function() {
  function checking(calcUtils, flagged, schedules, tns) {
    var allForms = wpw.tax.create.retrieveAll('formData');
    schedules.forEach(function(formId, index) {
      if (flagged.indexOf(formId) != -1) {
        calcUtils.field(tns[index]).assign(true);
      }
      else {
        calcUtils.field(tns[index]).assign(false);
      }
      calcUtils.field(tns[index]).disabled(true);
      //only allow jump to source if we support that form on our application
      if(allForms.hasOwnProperty(formId.toLowerCase())) {
        calcUtils.field(tns[index]).source(calcUtils.form(formId));
      }
    });
  }

  function setLimit(field, numArray) {
    var limit = field('CP.Days_Fiscal_Period').get();
    for (var i = 0; i < numArray.length; i++) {
      field(numArray[i]).assign(limit);
    }
  }

  /**
   * @param (check) date Object, the stated date from calculation
   * @param (situation) string, "before" and "after"
   * Calculate days difference for 2 cases with defined situation:
   * 1) Situation "before": check < CP.tax_start < CP.tax_end
   * 2) Situation "after": CP.tax_start < CP.tax_end < check
   * @return number of days in number,
   */
  function daysdiff(check, situation) {
    var begin;
    var end;
    var toJsDate = wpw.tax.utilities.toJsDate;
    var taxBegin = toJsDate(wpw.tax.field('CP.tax_start').get());
    var taxEnd = toJsDate(wpw.tax.field('CP.tax_end').get());

    check = toJsDate(check);
    if (situation == 'before') {
      begin = ((check - taxBegin) < 0 ? 0 : (check - taxBegin));
      end = ((check - taxEnd) < 0 ? 0 : (check - taxEnd));
    }
    else if (situation == 'after') {
      begin = ((taxBegin - check) < 0 ? 0 : (taxBegin - check));
      end = ((taxEnd - check) < 0 ? 0 : (taxEnd - check));
    }

    var diff = end + begin;
    var days = (diff / 86400000);
    return (Math.round(days) > wpw.tax.field('CP.Days_Fiscal_Period').get() ? wpw.tax.field('CP.Days_Fiscal_Period').get() : Math.round(days));
  }

  function runProrationCalcs(calcUtils, date, wholeNum, numerator, denominator, rate, result, isDateAfter) {
    var field = calcUtils.field;
    var calculateDaysDiff = wpw.tax.actions.calculateDaysDifference;
    var taxStartDate = field('CP.tax_start').get();
    var taxEndDate = field('CP.tax_end').get();
    var addToDate = wpw.tax.utilities.addToDate;
    var date2018 = wpw.tax.date(2018, 1, 1);

    calcUtils.getGlobalValue(denominator, 'CP', 'Days_Fiscal_Period');
    calcUtils.min(wholeNum, [400, 405, 410, 425, 427]);

    //days after 2018
    if (calcUtils.dateCompare.equal(wpw.tax.date(2018, 12, 31), date)) {
      field(numerator).assign(Math.max(calculateDaysDiff(date2018, taxStartDate), calculateDaysDiff(date2018, taxEndDate)))
    }
    else {
      if (isDateAfter == false) {
        field(numerator).assign(calculateDaysDiff(taxStartDate, date));
      }
      else {
        field(numerator).assign(
            calcUtils.dateCompare.greaterThan(taxEndDate, date2018) ?
               Math.max(calculateDaysDiff(date2018, taxEndDate), calculateDaysDiff(taxStartDate, addToDate(date2018, 0, 0, -1))) :
                calculateDaysDiff(addToDate(date, 0, 0, 1), taxEndDate))
      }
    }

    if (field(numerator).get() >= field(denominator).get()) {
      field(numerator).assign(field(denominator).get());
    }

    field(result).assign(denominator == 0 ? 0 :
        (field(wholeNum).get() * (field(numerator).get() / field(denominator).get()) * field(rate).get() / 100));
  }

  wpw.tax.create.calcBlocks('t2j', function(calcUtils) {
    var TISumArray = [311,
      // 312,
      313, 314, 315, 320, 325, 331, 332, 333, 334, 335, 340, 350];

    calcUtils.calc(function(calcUtils, field) {
      //Updates from CP to Identification section
      calcUtils.getGlobalValue('001', 'cp', 'bn');
      calcUtils.getGlobalValue('060', 'cp', 'tax_start');
      calcUtils.getGlobalValue('061', 'cp', 'tax_end');
      calcUtils.getGlobalValue('436', 'cp', 'Days_Fiscal_Period');
      calcUtils.getGlobalValue('442', 'cp', 'Days_Fiscal_Period');
      calcUtils.getGlobalValue('455', 'cp', 'Days_Fiscal_Period');
      calcUtils.getGlobalValue('459', 'cp', 'Days_Fiscal_Period');
      calcUtils.getGlobalValue('002', 'cp', '002');
      calcUtils.getGlobalValue('080', 'cp', '080'); // resident of Canada?
      calcUtils.getGlobalValue('081', 'cp', '081'); // if No then which country
      calcUtils.getGlobalValue('088', 'cp', '088');
      calcUtils.getGlobalValue('089', 'cp', '089');

      //HEAD OFFICE ADDRESS UPDATES
      calcUtils.getGlobalValue('010', 'cp', '010');
      var hasHOAddressChanged = (field('010').get() == 1);

      if (hasHOAddressChanged) {
        calcUtils.getGlobalValue('011', 'cp', '011');
        calcUtils.getGlobalValue('012', 'cp', '012');
        calcUtils.getGlobalValue('015', 'cp', '015');
        calcUtils.getGlobalValue('016', 'cp', '016');
        calcUtils.getGlobalValue('017', 'cp', '017');
        calcUtils.getGlobalValue('018', 'cp', '018');
      }
      else {
        calcUtils.removeValue(['011', '012', '015', '016', '017', '018'], true);
      }

      calcUtils.getGlobalValue('020', 'cp', '020');
      var hasMailingAddressChanged = field('020').get();
      if (hasMailingAddressChanged == '1') {
        calcUtils.getGlobalValue('021', 'cp', '021');
        calcUtils.getGlobalValue('022', 'cp', '022');
        calcUtils.getGlobalValue('023', 'cp', '023');
        calcUtils.getGlobalValue('025', 'cp', '025');
        calcUtils.getGlobalValue('026', 'cp', '026');
        calcUtils.getGlobalValue('027', 'cp', '027');
        calcUtils.getGlobalValue('028', 'cp', '028');
      }
      else {
        calcUtils.removeValue(['021', '022', '023', '025', '026', '027', '028'], true);
      }
      calcUtils.getGlobalValue('030', 'cp', '030');
      var hasBooksLocationChanged = (field('030').get() == 1);
      if (hasBooksLocationChanged) {
        calcUtils.getGlobalValue('031', 'cp', '031');
        calcUtils.getGlobalValue('032', 'cp', '032');
        calcUtils.getGlobalValue('035', 'cp', '035');
        calcUtils.getGlobalValue('036', 'cp', '036');
        calcUtils.getGlobalValue('037', 'cp', '037');
        calcUtils.getGlobalValue('038', 'cp', '038');
      }
      else {
        calcUtils.removeValue(['031', '032', '035', '036', '037', '038'], true);
      }
    });
    calcUtils.calc(function(calcUtils, field) {
      //MAILING ADDRESS UPDATES
      calcUtils.getGlobalValue('070', 'cp', '070'); //first year after incorp.
      calcUtils.getGlobalValue('071', 'cp', '071'); //first year after ammalgamation
      calcUtils.getGlobalValue('076', 'cp', '076'); //final year before ammalgamation
      calcUtils.getGlobalValue('078', 'cp', '078'); //final year before dissolution
      calcUtils.getGlobalValue('079', 'cp', '079'); //currency
      calcUtils.getGlobalValue('072', 'cp', '072'); //Has there been a wind-up of a subsidiary under section 88 during the current tax year?
      calcUtils.getGlobalValue('063', 'cp', '063'); //'Has there been an acquisition of control to which subsection 249(4) applies since the tax year start on line 060?'
      calcUtils.getGlobalValue('065', 'cp', '065');
      calcUtils.getGlobalValue('066', 'cp', '066');
      calcUtils.getGlobalValue('067', 'cp', '067'); //IS CORP A PROF CORP WHICH IS A PART OF A PARTNERSHIP
      field('040').assign(field('CP.Corp_Type').get());//Type of corporation
      field('040').source(field('CP.Corp_Type'));
      calcUtils.getGlobalValue('1067', 'cp', '117'); //Type of corporation if Type is 'Other'
      calcUtils.getGlobalValue('043', 'cp', '043');// If Corp_Type changed in the year update date.
      calcUtils.getGlobalValue('082', 'cp', '082');
      calcUtils.getGlobalValue('085', 'cp', '085'); // S149 type

      //online mail and software version update
      calcUtils.getGlobalValue('997', 'CP', '997');//amended return?

      if (field('997').get() == '1') {
        //use filing date of amended tax return instead of signing date
        calcUtils.getGlobalValue('955', 'l996', '100');
        //088 should be updated only on initial return
        field('088').assign()
      }
      else {
        calcUtils.getGlobalValue('088', 'cp', '088');
        calcUtils.getGlobalValue('955', 'CP', '955');
      }

      calcUtils.getGlobalValue('099', 'CP', '099');//software approval code
      calcUtils.getGlobalValue('101', 'CP', '101');//software version
    });

    calcUtils.calc(function(calcUtils, field) {
      //Attachment section
      var flagged = Object.keys(wpw.tax.utilities.flagger(calcUtils));
      var schedules = ['T2S9', 'T2S23', 'T2S49', 'T2S19', 'T2S11', 'T2S44', 'T2S14', 'T2S15', 'T5004', 'T5013', 'T2S22', 'T2S25',
        'T2S29', 'T106', 'T2S50', 'T2S1', 'T2S2',
        'T2S3', 'T2S4', 'T2S5', 'T2S6', 'T2S7', 'T2S8', 'T2S10', 'T2S12', 'T2S13', 'T2S16', 'T2S17', 'T2S18', 'T2S20',
        'T2S21', 'T2S27', 'T2S31', 'T661', 'T2S37', 'T2S38', 'T2S42', 'T2S43', 'T2S45', 'T2S46', 'T2S47', 'T2S48', 'T2S92',
        'T1134', 'T1135', 'T1141', 'T1142', 'T1145', 'T1146', 'T2S55', 'T2S53', 'T2S54'];
      var tns = ['150', '160', '161', '151', '162', '163', '164', '165', '166', '167', '168', '169',
        '170', '171', '173', '201', '202',
        '203', '204', '205', '206', '207', '208', '210', '212', '213', '216', '217', '218', '220',
        '221', '227', '231', '232', '237', '238', '242', '243', '244', '249', '253', '254', '255', '271', '259', '260', '261', '262', '263',
        '265', '268', '269'];

      checking(calcUtils, flagged, schedules, tns);
      // Update from CP to Additional information section.
      calcUtils.getGlobalValue('270', 'cp', '270'); //IFRS
      calcUtils.getGlobalValue('280', 'cp', '280'); // Inactive corp
    });

    calcUtils.calc(function(calcUtils, field) {
      //Principal prods & services only if [280] is false

      var isCorpInactive = (field('280').get() == 2);
      if (isCorpInactive) {
        calcUtils.getGlobalValue('299', 'cp', '299');
        calcUtils.getGlobalValue('284', 'cp', '284');
        calcUtils.getGlobalValue('285', 'cp', '285');
        calcUtils.getGlobalValue('286', 'cp', '286');
        calcUtils.getGlobalValue('287', 'cp', '287');
        calcUtils.getGlobalValue('288', 'cp', '288');
        calcUtils.getGlobalValue('289', 'cp', '289');
      }
      else {
        calcUtils.removeValue(['284', '285', '286', '287', '288', '289']);
      }

      calcUtils.getGlobalValue('291', 'cp', '291'); // immigrate?
      calcUtils.getGlobalValue('292', 'cp', '292'); // emmigrate?
      calcUtils.getGlobalValue('293', 'cp', '293');
      calcUtils.getGlobalValue('294', 'cp', '294');
      calcUtils.getGlobalValue('295', 'cp', '295');
    });
    calcUtils.calc(function(calcUtils, field) {
      // TAXABLE INCOME
      calcUtils.getGlobalValue('300', 'T2S1', '512'); // net income for tax purposes from S125
      calcUtils.getGlobalValue('311', 'T2S2', '260'); // Charitable donations from Line 260 of T2S2
      // calcUtils.getGlobalValue('312', 'T2S2', '360'); // Gifts to Canada, prov, etc. from Line 360 of T2S2
      calcUtils.getGlobalValue('313', 'T2S2', '460'); // Cultural gifts from Line 460 of T2S2
      calcUtils.getGlobalValue('314', 'T2S2', '560'); // Ecological gists from Line 560 of T2S2
      calcUtils.getGlobalValue('315', 'T2S2', '660'); // Gifts of medicine  from Line 660 of T2S2
      calcUtils.getGlobalValue('320', 'T2S3', '240');
      calcUtils.multiply('325', ['724'], 3.5);
      field('325').source(field('724'));
      calcUtils.getGlobalValue('331', 'T2S4', '130');
      field('332').assign(field('T2S4.225').get() * 1 / 2);
      field('332').source(field('T2S4.225'));
      //calcUtils.getGlobalValue('332', 'T2S4', '250');
      calcUtils.getGlobalValue('333', 'T2S4', '430');
      calcUtils.getGlobalValue('334', 'T2S4', '330');
      calcUtils.getGlobalValue('335', 'T2S4', '675');

      //Bucky: following num created for optimization calcUtils
      calcUtils.sumBucketValues('otherThanS2CreditTotal',
          ['320', '325', '331', '332', '333', '334', '335', '340', '350']);
      calcUtils.sumBucketValues('otherThanS4CreditTotal',
          ['320', '325', '311', '313', '314', '315', '332', '333', '335', '340', '350']);
      calcUtils.subtract('S2CreditLimit', '300', 'otherThanS2CreditTotal', false, true);
      calcUtils.subtract('S4CreditLimit', '300', 'otherThanS4CreditTotal', false, true);

    });
    calcUtils.calc(function(calcUtils, field) {

      calcUtils.sumBucketValues(351, TISumArray);
      calcUtils.equals(372, 351);
      calcUtils.subtract(352, 300, 372, false);
      calcUtils.sumBucketValues(360, [352, 355]);
      calcUtils.subtract(371, 360, 370);
    });
    calcUtils.calc(function(calcUtils, field) {
      // Small Business Deduction
      var isCCPC = (field('040').get() == 1);
      var isAssociatedCCPC = (field('T2J.160').get() == 1);
      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();
      var isCorpTypeChanged = field('CP.119').get() == 1;

      if (isCCPC) {
        if (isCorpTypeChanged) {
          calcUtils.removeValue(['400', '405', '410', '415', '421', '510', '515'], true);
        }
        else {
          calcUtils.getGlobalValue('400', 'T2S7', '545'); // Income from active business carried on in Canada from Schedule 7\
          //tn405val from s21 table
          // var colBTotal = field('t2s21.101').total(1).get();
          // var colFVal = field('t2s21.600').get();
          // var colGVal = field('t2s21.610').get() - field('604').get();
          // var colITotal = (colFVal == 0) ? 0 : colBTotal * colGVal / colFVal;

          // field('405').assign(Math.max((field('360').get() - 100 / 28 * colITotal - 4 * field('636').get()), 0));
          //Calculate Field 405
          var adjusted632=0;
          for (var i=0; i<calcUtils.form('T2S21').field(102).size().rows;i++){
            if(calcUtils.form('T2S21').field(102).cell(i,0).get()!=0){ //prevent division by 0
              adjusted632 += calcUtils.form('T2S21').field(101).cell(i,1).get()*(calcUtils.form('T2S21').field(102).cell(i,1).get() - calcUtils.form('T2S21').field('708').get())/calcUtils.form('T2S21').field(102).cell(i,0).get();
            }
          }
          if (adjusted632>0){
            adjusted632=Math.min(adjusted632,field('632').get());
          }else{
            adjusted632=field('632').get();
          };
          field('405').assign(Math.max((field('360').get() - 100 / 28 * adjusted632 - 4 * field('636').get()), 0));

          if (isAssociatedCCPC) {
            field('410').assign((daysFiscalPeriod < 357) ?
              (calcUtils.form('T2S23').field('085').cell(0, 5).get() * daysFiscalPeriod / 365) :
              calcUtils.form('T2S23').field('085').cell(0, 5).get());
            field('410').source(calcUtils.form('T2S23').field('085').cell(0, 5));
            //num 415 calcs
            field('415').assign(field('entityProfileSummary.101').cell(0, 5).get());
            field('415').source(field('entityProfileSummary.101').cell(0, 5));
          }
          else {
            field('410').assign((daysFiscalPeriod < 357) ? (500000 * daysFiscalPeriod / 365) : 500000);
            //num 415 calcs
            field('415').assign(field('entityProfileSummary.101').cell(0, 5).get());
            field('415').source(field('entityProfileSummary.101').cell(0, 5));
          }
        }
      }
      else {
        field('400').assign(0);
        field('405').assign(0);
        field('410').assign(0);
        field('415').assign(0);
      }
      calcUtils.equals('411', '410');
      calcUtils.getGlobalValue('469', 'ratesFed', '112');
      field('420').assign(field('469').get() == 0 ? 0 :
          (field('411').get() *
          field('415').get() /
          field('469').get())
      );
    });

    calcUtils.calc(function(calcUtils, field) {
      calcUtils.subtract(425, 410, 420, false);
      calcUtils.subtract(427, 425, 408, false);
      calcUtils.getGlobalValue('1423', 'ratesFed', '116');
      calcUtils.getGlobalValue('1428', 'ratesFed', '116-1');
      calcUtils.getGlobalValue('1502', 'ratesFed', '116-2');
      calcUtils.getGlobalValue('1508', 'ratesFed', '116-3');

    });
    calcUtils.calc(function(calcUtils, field) {
      var jan2016 = wpw.tax.date(2015, 12, 31);
      var jan2019 = wpw.tax.date(2018, 12, 31);
      runProrationCalcs(calcUtils, jan2016, '1420', '1421', '1422', '1423', '1424', false);
      runProrationCalcs(calcUtils, jan2016, '1425', '1426', '1427', '1428', '1429', true);
      runProrationCalcs(calcUtils, jan2019, '1500', '1501', '1503', '1502', '1504', false);
      calcUtils.sumBucketValues('430', ['1424', '1429', '1504', '1509']);
    });
    calcUtils.calc(function(calcUtils, field) {
      var isCCPC = (field('040').get() == 1);
      var isCorpTypeChanged = field('CP.119').get() == 1;

      //General tax reduction (controlled private corporations)
      if (isCCPC) {
        if (isCorpTypeChanged) {
          calcUtils.removeValue(['421', '422', '423', '432', '424', '426', '407'])
        }
        else {
          calcUtils.equals('421', '371');
          calcUtils.getGlobalValue('422', 'T2S27', '901');
          calcUtils.getGlobalValue('423', 'T2S27', '395');
          if (field('T2S7.520').get() < 0) {
            field('432').assign(0)
          }
          else {
            calcUtils.getGlobalValue('432', 'T2S7', '520');
          }
        }
      }
      else {
        field('421').assign(0);
        field('422').assign(0);

      }

      //calcUtils.getGlobalValue('424', 'T2S17', '');//ToDO:F S17 not avaiable
      calcUtils.min('426', ['400', '405', '410', '425', '427']);
      calcUtils.equals('407', '440');
      var GTRControlledSumArray = [422, 423, 432, 424, 426, 407];
      calcUtils.sumBucketValues(428, GTRControlledSumArray);
      calcUtils.equals('999', '428');
      field('429').assign(Math.max(field('421').get() - field('999').get(), 0));
      calcUtils.getGlobalValue('488', 'ratesFed', '128');
      calcUtils.multiply(443, [429, 488], (1 / 100));
    });
    calcUtils.calc(function(calcUtils, field) {
      var isCCPC = (field('040').get() == 1);
      //General tax reduction //
      //calcUtils below mean do not fill if the corp is ccpc.......
      var isCorpTypeChanged = field('CP.119').get() == 1;

      var generalTaxNumArr = ['444', '447', '448', '449', '434'];

      if (isCCPC) {
        if (isCorpTypeChanged) {
          if (field('360').get()) {
            if (field('371').get()) {
              calcUtils.equals('444', '371');
            }
            else {
              calcUtils.equals('444', '360');
            }
          }
        } else {
          calcUtils.removeValue(generalTaxNumArr, true);
        }
      }
      else {
        if (field('360').get()) {
          if (field('371').get()) {
            calcUtils.equals('444', '371');
          }
          else {
            calcUtils.equals('444', '360');
          }
        }
      }

      calcUtils.getGlobalValue('447', 'T2S27', '901');
      calcUtils.getGlobalValue('448', 'T2S27', '395');
      //calcUtils.getGlobalValue('434', 'T2S7', '520');//TODO: doublecheck???
      //calcUtils.getGlobalValue('449', 'T2S17', '');//ToDO:F S17 not avaiable
      var GTRSumArray = [447, 448, 434, 435];
      calcUtils.sumBucketValues(451, GTRSumArray);
      calcUtils.equals('995', '451');
      calcUtils.subtract(452, 444, 451);
      calcUtils.getGlobalValue('491', 'ratesFed', '128');
      calcUtils.multiply(462, [452, 491], (1 / 100));

      field('440').assign(isCCPC ? field('t2s7.092').get() : 0);
      field('440').source(field('t2s7.092'));
      //Amount A calculations
      calcUtils.equals('1400', '440');
      calcUtils.getGlobalValue('1480', 'ratesFed', '2057-0');
      calcUtils.getGlobalValue('1480-1', 'ratesFed', '2057-1');
      calcUtils.getGlobalValue('1480-2', 'ratesFed', '2057-2');
      calcUtils.getGlobalValue('1402', 'CP', 'Days_Fiscal_Period');
      field('1401').assign(daysdiff(wpw.tax.date(2016, 1, 1), 'before'));
      if (field('1402').get() == 0 || field('1480-2').get() == 0) {
        field('441').assign(0)
      } else {
        field('441').assign(
            field('1400').get() *
            field('1401').get() /
            field('1402').get() *
            (field('1480').get() + (field('1480-1').get() / field('1480-2').get())) / 100);
      }

      calcUtils.equals('1403', '440');
      calcUtils.getGlobalValue('1406', 'ratesFed', '2058-0');
      calcUtils.getGlobalValue('1406-1', 'ratesFed', '2058-1');
      calcUtils.getGlobalValue('1406-2', 'ratesFed', '2058-2');
      calcUtils.getGlobalValue('1405', 'CP', 'Days_Fiscal_Period');
      field('1404').assign(daysdiff(wpw.tax.date(2015, 12, 31), 'after'));
      field('1407').assign(field('1403').get() * field('1404').get() / field('1405').get() *
          (field('1406').get() + (field('1406-1').get() / field('1406-2').get())) / 100);

      calcUtils.sumBucketValues('1450', ['441', '1407']);
      calcUtils.equals('1451', '1450');

      field('445').assign(isCCPC ? field('t2s7.079').get() : 0);
      field('445').source(field('t2s7.079'));
      //Amount C calculations
      calcUtils.equals('1408', '445');
      calcUtils.getGlobalValue('1481', 'ratesFed', '2059-0');
      calcUtils.getGlobalValue('1481-1', 'ratesFed', '2059-1');
      calcUtils.getGlobalValue('1481-2', 'ratesFed', '2059-2');
      calcUtils.getGlobalValue('1410', 'CP', 'Days_Fiscal_Period');
      field('1409').assign(daysdiff(wpw.tax.date(2016, 1, 1), 'before'));
      field('446').assign(field('1408').get() * field('1409').get() / field('1410').get() *
          (field('1481').get() + (field('1481-1').get() / field('1481-2').get())) / 100);

      calcUtils.equals('1411', '445');
      calcUtils.getGlobalValue('1415', 'ratesFed', '2060');
      calcUtils.getGlobalValue('1413', 'CP', 'Days_Fiscal_Period');
      field('1412').assign(daysdiff(wpw.tax.date(2015, 12, 31), 'after'));
      field('1414').assign(field('1411').get() * field('1412').get() / field('1413').get() *
          (field('1415').get()) / 100);

      calcUtils.sumBucketValues('1416', ['446', '1414']);
      calcUtils.subtract('1417', '632', '1416');
      calcUtils.subtract('466', '1451', '1417');

      calcUtils.equals('992', '632');
      field('1470').assign(daysdiff(wpw.tax.date(2016, 1, 1), 'before'));
      calcUtils.getGlobalValue('1471', 'CP', 'Days_Fiscal_Period');
      calcUtils.getGlobalValue('2001', 'ratesFed', '2054');
      field('1452').assign(field('1470').get() * field('2001').get() / field('1471').get());

      field('1453').assign(daysdiff(wpw.tax.date(2015, 12, 31), 'after'));
      calcUtils.getGlobalValue('1454', 'CP', 'Days_Fiscal_Period');
      calcUtils.getGlobalValue('1455', 'ratesFed', '2052-0');
      calcUtils.getGlobalValue('1455-1', 'ratesFed', '2052-1');
      calcUtils.getGlobalValue('1455-2', 'ratesFed', '2052-2');
      field('1456').assign(field('1453').get() / field('1454').get() * (field('1455').get() +
          (field('1455-1').get() / field('1455-2').get())));
      calcUtils.sumBucketValues('1461', ['1452', '1456']);
      calcUtils.equals('1457', '992');
      calcUtils.getGlobalValue('2006', 'ratesFed', '2056');
      calcUtils.equals('1459', '1461');
      calcUtils.divide('1460', '2006', '1459', ['1457']);
      field('467').assign(isCCPC ? field('360').get() : 0);
      field('468').assign(Math.min(field('400').get(), field('405').get(), field('410').get(), field('427').get()));
      calcUtils.equals('1430', '1460');
      calcUtils.equals('1431', '636');
      calcUtils.getGlobalValue('1432', 'ratesFed', '134');
      calcUtils.multiply('1433', ['1431', '1432']);
      calcUtils.sumBucketValues('1434', ['468', '1430', '1433']);
      calcUtils.equals('1435', '1434');
      calcUtils.subtract('1436', '467', '1435');
      //Amount O calculations
      calcUtils.equals('1437', '1436');
      field('1438').assign(daysdiff(wpw.tax.date(2016, 1, 1), 'before'));
      calcUtils.getGlobalValue('1439', 'CP', 'Days_Fiscal_Period');
      calcUtils.getGlobalValue('1445', 'ratesFed', '2057-0');
      calcUtils.getGlobalValue('1445-1', 'ratesFed', '2057-1');
      calcUtils.getGlobalValue('1445-2', 'ratesFed', '2057-2');
      if (field('1439').get() == 0 || field('1445-2').get() == 0) {
        field('1440').assign(0)
      } else {
        field('1440').assign((field('1437').get() * field('1438').get() / field('1439').get()) *
            ((field('1445').get() + (field('1445-1').get() / field('1445-2').get())) / 100));
      }

      calcUtils.equals('1441', '1436');
      field('1442').assign(daysdiff(wpw.tax.date(2015, 12, 31), 'after'));
      calcUtils.getGlobalValue('1443', 'CP', 'Days_Fiscal_Period');
      calcUtils.getGlobalValue('1446', 'ratesFed', '2058-0');
      calcUtils.getGlobalValue('1446-1', 'ratesFed', '2058-1');
      calcUtils.getGlobalValue('1446-2', 'ratesFed', '2058-2');
      if (field('1443').get() == 0 || field('1446-2').get() == 0) {
        field('1444').assign(0)
      } else {
        field('1444').assign(
            (field('1441').get() *
            field('1442').get() /
            field('1443').get()) *
            ((field('1446').get() + (field('1446-1').get() / field('1446-2').get())) / 100));
      }

      calcUtils.sumBucketValues('1447', ['1440', '1444']);
      calcUtils.equals('1448', '1447');
      field('1449').assign(isCCPC ? Math.max(field('700').get() - field('780').get(), 0) : 0);
      field('450').assign(Math.min(field('466').get(), field('1448').get(), field('1449').get()));

    });
    calcUtils.calc(function(calcUtils, field) {
      setLimit(field, [7005, 1005, 2005, 9004, 3004]);
      //Refundable dividend tax on hand
      calcUtils.subtract(478, 460, 465);
      calcUtils.equals('899', '478');
      calcUtils.equals(479, 450);
      calcUtils.getGlobalValue('481', 'T2S3', '360');
      calcUtils.sumBucketValues(482, [479, 480, 481]);
      calcUtils.equals('1898', '482');
      calcUtils.sumBucketValues(485, [899, 1898]);
    });

    calcUtils.calc(function(calcUtils, field) {
      //Dividend Refund
      calcUtils.getGlobalValue('500', 't2s3', '460');
      calcUtils.equals('2637', '500');
      field('2438').assign(daysdiff(wpw.tax.date(2016, 1, 1), 'before'));
      calcUtils.getGlobalValue('2439', 'CP', 'Days_Fiscal_Period');
      calcUtils.getGlobalValue('2445', 'ratesFed', '135-0');
      calcUtils.getGlobalValue('2445-1', 'ratesFed', '135-1');
      calcUtils.getGlobalValue('2445-2', 'ratesFed', '135-2');
      if (field('2439').get() == 0 || field('2445-2').get() == 0) {
        field('2440').assign(0)
      } else {
        field('2440').assign((field('2637').get() * field('2438').get() / field('2439').get()) *
            ((field('2445').get() + (field('2445-1').get() / field('2445-2').get())) / 100));
      }
      calcUtils.equals('3637', '500');
      field('3438').assign(daysdiff(wpw.tax.date(2015, 12, 31), 'after'));
      calcUtils.getGlobalValue('3439', 'CP', 'Days_Fiscal_Period');
      calcUtils.getGlobalValue('3445', 'ratesFed', '136-0');
      calcUtils.getGlobalValue('3445-1', 'ratesFed', '136-1');
      calcUtils.getGlobalValue('3445-2', 'ratesFed', '136-2');
      if (field('3439').get() == 0 || field('3445-2').get() == 0) {
        field('3440').assign(0)
      } else {
        field('3440').assign((field('3637').get() * field('3438').get() / field('3439').get()) *
            ((field('3445').get() + (field('3445-1').get() / field('3445-2').get())) / 100));
      }
      calcUtils.sumBucketValues('501', ['2440', '3440']);
      calcUtils.equals('502', '501');

      calcUtils.equals('504', '485');
      field('1505').assign(Math.min(field('502').get(), field('504').get()));
    });

    calcUtils.calc(function(calcUtils, field) {
      var dateCompare = calcUtils.dateCompare;
      var taxStartDate = field('CP.tax_start').get();
      var taxEndDate = field('CP.tax_end').get();
      var after2015 = wpw.tax.date(2015, 12, 31);
      var before2016 = wpw.tax.date(2016, 1, 1);
      var isCCPC = (field('040').get() == 1);
      var daysDifferenceAfter2015 = dateCompare.greaterThan(taxEndDate, after2015);
      var daysDifferenceBefore2016 = dateCompare.lessThan(taxStartDate, before2016);
      //Part I tax
      calcUtils.equals('555', '432');
      calcUtils.multiply(550, [371], 38 / 100);
      // calcUtils.multiply(560, [555], 5 / 100);
      field('560').assign(
          field('555').get() *
          (field('4002').get() /
          field('4005').get()) *
          field('4003').get() / 100
      );
      calcUtils.getGlobalValue('602', 'T2S31', '1004');
      if (isCCPC) {
        calcUtils.equals(552, 360);
        calcUtils.equals(551, 440);
      }
      else {
        field('552').assign(0);
      }

      calcUtils.equals('408', '515');
      calcUtils.min(553, [400, 405, 410, 425, 427]);
      calcUtils.subtract(554, 552, 553);
      calcUtils.equals('897', '554');

      calcUtils.getGlobalValue('3001', 'ratesFed', '144');
      field('3006').assign(4);
      if (daysDifferenceAfter2015 || daysDifferenceBefore2016) {
        field('3002').assign(wpw.tax.actions.calculateDaysDifference(before2016, taxEndDate));
        if (field('3002').get() > field('3004').get()) {
          field('3002').assign(field('3004').get())
        }
      }
      field('3003').assign(
          field('3006').get() *
          field('3002').get() /
          field('3004').get() +
          field('3001').get()
      );
      field('2437').assign(Math.min(field('551').get(), field('551').get()));
      field('2538').assign(daysdiff(wpw.tax.date(2016, 1, 1), 'before'));
      calcUtils.getGlobalValue('2539', 'CP', 'Days_Fiscal_Period');
      calcUtils.getGlobalValue('2545', 'ratesFed', '2157-0');
      calcUtils.getGlobalValue('2545-1', 'ratesFed', '2157-1');
      calcUtils.getGlobalValue('2545-2', 'ratesFed', '2157-2');
      field('2540').assign(field('2437').get() * field('2538').get() / field('2539').get() *
          (field('2545').get() + (field('2545-1').get() / field('2545-2').get())) / 100);

      field('3437').assign(Math.min(field('551').get(), field('897').get()));
      field('3538').assign(daysdiff(wpw.tax.date(2015, 12, 31), 'after'));
      calcUtils.getGlobalValue('3539', 'CP', 'Days_Fiscal_Period');
      calcUtils.getGlobalValue('3545', 'ratesFed', '2158-0');
      calcUtils.getGlobalValue('3545-1', 'ratesFed', '2158-1');
      calcUtils.getGlobalValue('3545-2', 'ratesFed', '2158-2');
      field('3540').assign(field('3437').get() * field('3538').get() / field('3539').get() *
          (field('3545').get() + (field('3545-1').get() / field('3545-2').get())) / 100);

      calcUtils.sumBucketValues('604', ['2540', '3540']);
      calcUtils.equals('1604', '604');

      //Tax on personal services business income calc//
      calcUtils.getGlobalValue('4001', 'T2S7', '520');
      field('4002').assign(wpw.tax.actions.calculateDaysDifference(after2015, taxEndDate));
      calcUtils.getGlobalValue('4003', 'ratesFed', '2053');
      calcUtils.getGlobalValue('4005', 'CP', 'Days_Fiscal_Period');
      if (field('4002').get() >= field('4005').get()) {
        calcUtils.equals('4002', '4005', true);
      }
      else {
        field('4002').assign(wpw.tax.actions.calculateDaysDifference(before2016, taxEndDate));
      }

      calcUtils.sumBucketValues(605, [550, 560, 602, 1604]);
      calcUtils.equals(607, 430);
    });
    calcUtils.calc(function(calcUtils, field) {
      //Federal tax abatement  = 10% of DOMESTIC taxable income
      var federalTaxAbatementRate = calcUtils.form('ratesFed').field('142').get();
      var provinces = field('CP.prov_residence').get();
      var isOC = false;
      if (provinces) {
        isOC = (provinces['OC'] == true);
      }

      if (field('cp.750').get() == 'MJ') {
        //TODO: need to make sure S5 is completed so we can get the global value
        var outsideCanadaTaxableIncome = calcUtils.form('T2S5').field('1032').get();
        if (outsideCanadaTaxableIncome == 0 || outsideCanadaTaxableIncome == undefined) {
          field('608').assign(field('371').get() * (federalTaxAbatementRate / 100));
        }
        else {
          field('608').assign((field('371').get() - outsideCanadaTaxableIncome) * (federalTaxAbatementRate / 100));
        }
      }
      else {
        if (isOC) {
          field('608').assign(0);
        }
        else {
          field('608').assign(field('371').get() * (federalTaxAbatementRate / 100));
        }
      }
      calcUtils.getGlobalValue('616', 'T2S27', '366');
      calcUtils.getGlobalValue('632', 'T2S21', '180');
      calcUtils.getGlobalValue('636', 'T2S21', '280');

      //calcUtils.getGlobalValue('624', 'T2S18', '');//ToDO:S18 not avaiable
      //calcUtils.getGlobalValue('628', 'T2S17', '');//ToDO:S17 not avaiable
      //calcUtils.getGlobalValue('632', 'T2S31', '');//ToDO:line 180 s31 not avaiable
      //calcUtils.getGlobalValue('636', 'T2S21', '');//ToDO:line 280 S21 not avaiable
      field('638').assign(Math.max(field('443').get(), 0));
      calcUtils.equals(639, 462);
      calcUtils.getGlobalValue('640', 'T2S21', '580');
      calcUtils.getGlobalValue('652', 'T2S31', '1010');
    });
    calcUtils.calc(function(calcUtils, field) {
      var p1SumArray = [607, 608, 616, 620, 628, 632, 636, 638, 639, 640, 641, 648, 652];
      var p1SumArrayExceptNum652 = [607, 608, 616, 620, 628, 632, 636, 638, 639, 640, 641, 648];

      // created for optimization calcUtils for S31 until globalcalcUtils work
      calcUtils.sumBucketValues('S31CreditLimitTemp', p1SumArrayExceptNum652);
      field('S31CreditLimit').assign(field('605').get() - field('S31CreditLimitTemp').get());
      calcUtils.sumBucketValues(653, p1SumArray);
      calcUtils.equals('895', '653');
      calcUtils.subtract(654, 605, 895, false, p1SumArray);

      //Summary of Tax and Credits
      calcUtils.equals(700, 654);
      calcUtils.getGlobalValue('708', 'T2S46', '120');
      calcUtils.getGlobalValue('710', 'T2S55', '190');
      if (!field('710').get()) {
        calcUtils.getGlobalValue('710', 'T2S55', '290');
      }

      calcUtils.getGlobalValue('712', 'T2S3', '360');
      //calcUtils.getGlobalValue('716', 'T2S43', '');//ToDO: S43 not avaiable
      //calcUtils.getGlobalValue('720', 'T2S38', '');//ToDO: S38 not avaiable
      //calcUtils.getGlobalValue('724', 'T2S43', '');//ToDO: S43 not avaiable
      //calcUtils.getGlobalValue('727', 'T2S92', '');//ToDO: S92 not avaiable
      //calcUtils.getGlobalValue('728', 'T2S20', '');//ToDO: S20 not avaiable
      var summaryTopSumArray = [700, 708, 710, 712, 716, 720, 724, 727, 728];
      calcUtils.sumBucketValues(730, summaryTopSumArray);
    });
    calcUtils.calc(function(calcUtils, field) {
      var summaryTopSumArray = [700, 708, 710, 712, 716, 720, 724, 727, 728];
      //provincial or territorial tax payable or credits from S5
      var line255S5 = field('T2S5.255');
      if (line255S5.get() >= 0) {
        field('760').assign(line255S5.get());
        field('812').assign(0);
      }
      else {
        field('760').assign(0);
        field('812').assign(line255S5.get() * (-1));
      }
      field('760').source(line255S5);
      field('812').source(line255S5);

      calcUtils.sumBucketValues(770, [730, 760], summaryTopSumArray);
      calcUtils.getGlobalValue('780', 'T2S31', '610');
      calcUtils.equals('784', '1505');
      //calcUtils.getGlobalValue('788', 'T2S18', '');//ToDO: S18 not avaiable
      calcUtils.setRepeatSummaryValue('796', 'T2S47', '620', 'add');
      field('796').source(field('T2S47.620'));
      calcUtils.setRepeatSummaryValue('797', 'T2S48', '620', 'add');
      field('797').source(field('t2s48.620'));
      //calcUtils.getGlobalValue('808', 'T2S18', '');//ToDO: S18 not avaiable
    });
    calcUtils.calc(function(calcUtils, field) {
      var summaryBottomSumArray = [780, 784, 788, 792, 796, 797, 800, 808, 812, 840];
      calcUtils.sumBucketValues(890, summaryBottomSumArray);
      calcUtils.equals('889', '890');
      calcUtils.subtract(891, 770, 889, true);

      var value = calcUtils.getNumValue(891, true);
      if ((value >= 0)) {
        field('892').assign(value);
        field('893').assign(0);
      }
      else {
        field('892').assign(0);
        calcUtils.multiply('893', ['891'], (-1));
      }

      //provinces tn750
      calcUtils.getGlobalValue('750', 'CP', '750');

      //EFILE number
      calcUtils.getGlobalValue('920', 'CP', '920');

      //Direct Deposit INfo:
      calcUtils.getGlobalValue('900', 'CP', '900');
      calcUtils.getGlobalValue('910', 'CP', '910');
      if (!field('910').get()) {
        calcUtils.getGlobalValue('910', 'CP', '902');
      }
      calcUtils.getGlobalValue('914', 'CP', '914');
      if (!field('914').get()) {
        calcUtils.getGlobalValue('914', 'CP', '903');
      }
      calcUtils.getGlobalValue('918', 'CP', '918');
      if (!field('918').get()) {
        calcUtils.getGlobalValue('918', 'CP', '904');
      }

      //Certification
      calcUtils.getGlobalValue('950', 'CP', '950');
      calcUtils.getGlobalValue('951', 'CP', '951');
      calcUtils.getGlobalValue('954', 'CP', '954');
      calcUtils.getGlobalValue('955', 'CP', '955');
      calcUtils.getGlobalValue('956', 'CP', '956');
      calcUtils.getGlobalValue('637', 'CP', '637');
      calcUtils.getGlobalValue('957', 'CP', '957');
      if (field('957').get() == 2) {
        calcUtils.getGlobalValue('958', 'CP', '958');
        calcUtils.getGlobalValue('959', 'CP', '959');
        calcUtils.getGlobalValue('617', 'CP', '617');
      } else {
        calcUtils.removeValue(['958', '959', '617'], true);
      }

      //Language
      calcUtils.getGlobalValue('990', 'CP', '990');
      //tn099 and 101 should not display ion T2J but only in COR export
      field('hideTn099And101').assign(false)
    });
  });
})
();