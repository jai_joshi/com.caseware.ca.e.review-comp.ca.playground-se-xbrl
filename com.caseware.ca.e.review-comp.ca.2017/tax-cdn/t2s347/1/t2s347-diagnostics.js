(function() {
  wpw.tax.create.diagnostics('t2s347', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3470001', common.prereq(common.and(
        common.requireFiled('T2S347'),
        common.check(['t2s5.567'], 'isNonZero')),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne([row[0]], 'isNonZero') ||
                tools.requireOne(tools.list(['t2s5.838']), 'isNonZero');
          });
        }));

    diagUtils.diagnostic('3470005', common.prereq(common.requireFiled('T2S347'),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[0], tools.field('t2s5.838')], 'isNonZero') > 1)
              return tools.requireOne([row[0], tools.field('t2s5.838')], 'isEmpty');
            else return true;
          });
        }));

    diagUtils.diagnostic('3470010', common.prereq(common.requireFiled('T2S347'),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[0].isNonZero())
              return row[1].require('isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('3470015', common.prereq(common.requireFiled('T2S347'),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[1].isNonZero())
              return row[0].require('isNonZero');
            else return true;
          });
        }));

  });
})();
