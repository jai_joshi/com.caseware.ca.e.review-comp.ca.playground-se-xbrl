(function() {
  wpw.tax.global.tableCalculations.t2s347 = {
    "900": {
      hasTotals: true,
      "showNumbering": true,
      "columns": [{
        "header": "Certificate number",
        "num": "100","validate": {"or":[{"length":{"min":"9","max":"9"}},{"check":"isEmpty"}]}, 
        "tn": "100",
        "rf": true,
        type: 'text'
      },
        {
          "header": "Amount of Nova Scotia digital media tax credit",
          "num": "200",
          "tn": "200","validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          "total": true,
          "totalNum": "300",
          "totalTn": "300",
          "totalMessage": "Total amount of the Nova Scotia digital media tax credit"
        }]
    }
  }
})();