(function() {
  'use strict';

  wpw.tax.global.formData.t2s347 = {
      formInfo: {
        abbreviation: 'T2S347',
        title: 'Additional Certificate Numbers for the Nova Scotia Digital Media Tax Credit',
        //TODO: DO NOT DELETE THIS LINE: subtitle
        //subTitle: '(2008 and later tax years)',
        schedule: 'Schedule 347',
        showCorpInfo: true,
        headerImage: 'canada-federal',
        category: 'Nova Scotia Forms'
      },
      sections: [
        {
          hideFieldset: true,
          'rows': [
            {
              label: 'For use by a corporation that has more than one certificate number for the ' +
              'Nova Scotia digital media tax credit.',
              labelClass: 'fullLength tabbed2'
            },
            {labelClass: 'fullLength'},
            {
              'type': 'table',
              'num': '900'
            },
            {
              label: 'Enter this amount on line 567 in Part 2 of Schedule 5.',
              labelClass: 'fullLength tabbed'
            }
          ]
        }
      ]
    };
})();