(function() {
  wpw.tax.global.tableCalculations.mealsEntS = {
    '1000': {
      'executeAtEnd': true,
      'hasTotals': true,
      'linkedRepeatForm': 'mealsEnt',
      'maxLoop': 100000,
      'columns': [
        {
          'header': 'Description',
          'width': '40%',
          cellClass: 'alignCenter',
          'disabled': true,
          'canSort': true,
          'linkedFieldId': '101',
          'type': 'text'
        },
        {
          'header': '$',
          filters: 'prepend $',
          'disabled': true,
          'total': true,
          'totalNum': 'total',
          'canSort': true,
          'init': '0',
          'linkedFieldId': '1001'
        },
        {
          'header': '$ Add back<br>(All provinces except Quebec)',
          filters: 'prepend $',
          'disabled': true,
          'init': '0',
          'total': true,
          'totalNum': 'addBack',
          'canSort': true,
          'linkedFieldId': '1002'
        },
        {
          'header': '$ Add back<br>(Quebec)',
          'canSort': true,
          filters: 'prepend $',
          'disabled': true,
          'init': '0',
          'total': true,
          'totalNum': 'addBackQuebec',
          showWhen: {fieldId: 'CP.prov_residence', has: {QC: true}},
          'linkedFieldId': '1003'
        }
      ]
    }
  }
})();
