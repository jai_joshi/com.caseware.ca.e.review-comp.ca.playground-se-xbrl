(function() {

  wpw.tax.create.calcBlocks('mealsEntS', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('showQuebec', 'cp', 'prov_residence');
      calcUtils.field('gifiValue').assign(calcUtils.sumRepeatGifiValue('t2s125', 300, 8523));
      calcUtils.field('gifiValue').source(field('t2s125.8523'));
    });
  });
})();
