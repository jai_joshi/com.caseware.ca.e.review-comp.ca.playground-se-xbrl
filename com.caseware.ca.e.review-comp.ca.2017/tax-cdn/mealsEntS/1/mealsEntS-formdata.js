(function() {
  'use strict';

  wpw.tax.global.formData.mealsEntS = {
    formInfo: {
      abbreviation: 'mealsEntS',
      title: 'Meals and Entertainment Expenses - Summary',
      neededRepeatForms: ['mealsEnt'],
      headerImage: 'cw',
      category: 'Workcharts'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Meals and Entertainment amount (Code 8523) in GIFI 125 - Income Statement',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': 'gifiValue',
                  'disabled': true,
                  'cannotOverride': true
                }
              }
            ]
          }
        ]
      }
    ]
  };
})();
