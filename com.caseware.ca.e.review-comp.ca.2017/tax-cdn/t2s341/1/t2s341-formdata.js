(function() {
  'use strict';

  wpw.tax.global.formData.t2s341 = {
    formInfo: {
      abbreviation: 'T2S341',
      title: 'Nova Scotia Corporate Tax Reduction for New Small Businesses',
      schedule: 'Schedule 341',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 341 E (17)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'The Nova Scotia corporate tax reduction for new small businesses applies to the first ' +
              'three tax years of a qualifying Canadian-controlled private corporation (CCPC) incorporated ' +
              'in Nova Scotia. A CCPC not incorporated in Nova Scotia may also be eligible for the reduction, ' +
              'provided the head office of the corporation is located in Nova Scotia and at least 25% of ' +
              'the wages and salaries paid by the corporation in the tax year are paid to employees who are ' +
              'resident in Nova Scotia. The corporation must have at least two employees, one of whom must be ' +
              'full-time and not related to a specified shareholder of the corporation.'
            },
            {
              label: 'This reduction is only available to a corporation that qualifies for the federal small ' +
              'business deduction for the year and maintained a permanent establishment in Nova Scotia at any time during the year.'
            },
            {
              label: 'The tax reduction is based on the portion of an eligible corporation\'s ' +
              'taxable income earned in the year in Nova Scotia.'
            },
            {
              label: 'The Nova Scotia Department of Finance and Treasury Board administers the eligibility ' +
              'requirements. Corporations claiming the reduction have to apply annually to the Nova Scotia ' +
              'Minister of Finance and Treasury Board for a certificate of eligibility and must do so within three ' +
              'years of the tax year-end for which they are claiming the reduction.'
            },
            {
              label: 'The reduction is not available to a corporation:',
              sublist: [
                'if it, or any predecessor corporation, was associated with another corporation at any time since ' +
                'the date of incorporation. However, associated corporations may have their ineligibility ' +
                'waived by the Nova Scotia Minister of Finance and Treasury Board;',
                'that carries on the professional practice of an accountant, dentist, lawyer, medical doctor, veterinarian, or chiropractor;',
                'if it, or any predecessor corporation, was the beneficiary of a trust with an ineligible beneficiary at any time since the date of incorporation;',
                'if it, or any predecessor corporation, was a member of a partnership or a co-venturer in a joint ' +
                'venture with an ineligible partner or co-venturer at any time since the date of incorporation; or',
                'if, before incorporation, the same or substantially the same business was carried on as a sole ' +
                'proprietorship, partnership, or corporation, whether registered or not. However, if the sole ' +
                'proprietorship or partnership carried on the same or substantially the same business for 90 days ' +
                'or less before incorporation, the corporation can apply to the Minister for a certificate of eligibility.'
              ]
            },
            {
              label: 'This schedule is a worksheet only and is not required to be filed with your ' +
              '<i>T2 Corporation Income Tax Return</i>. Enter amount C from this schedule and the certificate ' +
              'number issued by the province on line 556 and line 834 respectively of Schedule 5, ' +
              '<i>Tax Calculation Supplementary – Corporations.</i>'
            }
          ]
        }
      ],
      category: 'Nova Scotia Forms'
    },
    sections: [
      {
        'hideFieldset': true,
        'rows': [
          {
            'label': 'Does corporation eligible for Nova Scotia Tax Reduction for New Small Businesses?',
            'labelWidth': '45%',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '999',
            'init': '2'
          }
        ]
      },
      {
        'header': 'Calculation of the Nova Scotia corporate tax reduction for new small businesses',
        'rows': [
          {
            'label': 'The least of the amounts on lines 400, 405, 410, or 427 of the small business deduction calculation on page 4 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            disabled: true,
            'columns': [
              {
                underline: 'double',
                'input': {
                  'num': '101'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'type': 'table',
            'num': '800'
          },
          {
            'label': 'Nova Scotia corporate tax reduction for new small businesses – 3% of amount B',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'double',
                'input': {
                  'num': '122'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Enter amount C on line 556 of Schedule 5.',
            'labelClass': 'fullLength'
          },
          {
            'label': '*If the corporation has a permanent establishment only in Nova Scotia or in the offshore area of Nova Scotia, enter the taxable income from line 360 of the T2 return. Otherwise, enter the total of the taxable incomes allocated to both jurisdictions in Nova Scotia (the province itself and the offshore area) from column F in Part 1 of Schedule 5.',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '**Includes the territories and the offshore jurisdictions for Nova Scotia and Newfoundland and Labrador.',
            'labelClass': 'tabbed fullLength'
          }
        ]
      }
    ]
  };
})();
