(function() {
  wpw.tax.create.calcBlocks('t2s341', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      var isEligible = (field('999').get() == '1');
      var val101 = Math.min(
          form('T2J').field('400').get(),
          form('T2J').field('405').get(),
          form('T2J').field('410').get(),
          form('T2J').field('425').get()
      );

      field('101').assign(isEligible ? val101 : 0);
      calcUtils.equals('400', '101');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var taxableIncomeAllocationNS = calcUtils.getTaxableIncomeAllocation('NS');
      var taxableIncomeAllocationNO = calcUtils.getTaxableIncomeAllocation('NO');

      var totalProvincialTI = taxableIncomeAllocationNS.provincialTI + taxableIncomeAllocationNO.provincialTI;

      field('401').assign(totalProvincialTI);
      field('402').assign(taxableIncomeAllocationNS.allProvincesTI);
      field('401').source(taxableIncomeAllocationNS.provincialTISourceField);
      field('402').source(taxableIncomeAllocationNS.allProvincesTISourceField);

      field('403').assign(
          field('400').get() *
          field('401').get() /
          field('402').get());

      field('122').assign(field('403').get() * 3 / 100);
    });

  });
})();
