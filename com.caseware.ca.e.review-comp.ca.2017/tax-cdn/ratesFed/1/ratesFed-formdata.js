(function() {

  wpw.tax.global.formData.ratesFed = {
    formInfo: {
      abbreviation: 'ratesFed',
      title: 'Rates Table',
      showCorpInfo: true,
      description: [
        {type: 'heading', text: 'Federal'}
      ],
      category: 'Rates Tables'
    },
    sections: [
      {
        'header': 'This version of CaseWare Review & Compilation - Canada supports fiscal periods',
        'rows': [
          {
            'label': 'Starting from',
            'num': '2030',
            'input2': true,
            'type': 'infoField',
            'inputType': 'date'
          },
          {
            'label': 'Ending by',
            'num': '2031',
            'input2': true,
            'type': 'infoField',
            'inputType': 'date'
          }
        ]
      },
      {
        'header': 'Jacket – Federal',
        'rows': [
          {
            'label': 'Taxable capital gain employed in Canada (line 233 and 234)',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Related corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '233'
                }
              }
            ]
          },
          {
            'label': 'Associated corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '234'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable income – Part VI.1 tax deduction (line 325)',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Multiplier for the taxation year before 2010',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100'
                }
              }
            ]
          },
          {
            'label': 'Multiplier for the taxation year after 2009, and before 2012',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '101',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'Multiplier for the taxation year after 2011',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '102',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Small business deduction (Schedule 23)',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year',
            'labelClass': 'fullLength'
          },
          {
            'label': '2003-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '105'
                }
              }
            ]
          },
          {
            'label': '2004-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '106'
                }
              }
            ]
          },
          {
            'label': '2005-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '107'
                }
              }
            ]
          },
          {
            'label': '2007-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '109'
                }
              }
            ]
          },
          {
            'label': '2009-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '111'
                }
              }
            ]
          },
          {
            'label': 'Business limit reduction',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '112'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Rate',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxation year',
            'labelClass': 'fullLength'
          },
          {
            'label': '2004-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '115'
                }
              }
            ]
          },
          {
            'label': '2008-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '116'
                }
              }
            ]
          },
          {
            'label': '2016-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '116-1',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': '2018-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '116-2'
                }
              }
            ]
          },
          {
            'label': '2019-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '116-3'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable income reduction – Federal foreign non-business income tax credit',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Federal foreign non-business income tax credit',
            'labelClass': 'fullLength bold tabbed'
          },
          {
            'label': 'Taxation year',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '2009-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '117',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': '2011-11-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '118',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Federal foreign non-business income tax credit',
            'labelClass': 'fullLength bold tabbed'
          },
          {
            'label': 'For a taxation year ending before January 1, 2010',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '119'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Accelerated tax reduction*',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Reduced business limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '120'
                }
              }
            ]
          },
          {
            'label': 'Manufacturing and processing profits deduction (Schedule 27)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '121',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '122'
                }
              }
            ]
          },
          {
            'label': '* For 2005 and later taxation years, the accelerated tax reduction is no longer applicable.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'General tax reduction',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year',
            'labelClass': 'fullLength'
          },
          {
            'label': '2004-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '123'
                }
              }
            ]
          },
          {
            'label': '2008-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '124',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': '2009-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '125'
                }
              }
            ]
          },
          {
            'label': '2010-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '126'
                }
              }
            ]
          },
          {
            'label': '2011-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '127',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': '2012-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '128'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Refundable portion of Part 1 tax',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Aggregate investment income',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Foreign investment income',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '131',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Foreign investment income - taxation year ending after 2015',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2051',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Foreign non-business income tax credit – taxation year ending before November 1, 2011',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '132',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Foreign non-business income tax credit – taxation year starting after October 31, 2011',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '133',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Foreign non-business income tax credit – taxation year ending after 2015',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2052',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For amount D',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Foreign investment income tax credit – taxation year ending before January 1, 2016 - integer',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2059-0',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Foreign investment income tax credit – taxation year ending before January 1, 2016 - numerator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2059-1',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Foreign investment income tax credit – taxation year ending before January 1, 2016 - denominator ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2059-2',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Foreign investment income tax credit – taxation year ending after December 31, 2015',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2060',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For amount H',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Foreign non-business income tax credit – taxation year ending before January 1, 2016',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2054',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Foreign non-business income tax credit – taxation year ending after December 31, 2015 - integer',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2052-0',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Foreign non-business income tax credit – taxation year ending after December 31, 2015 - numerator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2052-1',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Foreign non-business income tax credit – taxation year ending after December 31, 2015 - denominator ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2052-2',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Foreign business income tax credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '134'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For amount I',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2056'
                }
              }
            ]
          },
          {
            'label': 'For amount P',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Foreign business income tax credit – taxation year ending before January 1, 2016 - integer',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2057-0',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Foreign business income tax credit – taxation year ending before January 1, 2016 - numerator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2057-1',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Foreign business income tax credit – taxation year ending before January 1, 2016 - denominator ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2057-2',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Foreign business income tax credit – taxation year ending after December 31, 2015 - integer',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2058-0',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Foreign business income tax credit – taxation year ending after December 31, 2015 - numerator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2058-1',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Foreign business income tax credit – taxation year ending after December 31, 2015 - denominator ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2058-2',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Dividend refund',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Dividend refund - before January 1, 2016 - integer',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '135-0',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Dividend refund - before January 1, 2016 - numerator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '135-1',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Dividend refund - before January 1, 2016 - denominator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '135-20',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Dividend refund - after December 31, 2015 - integer',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '136-0',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Dividend refund - after December 31, 2015 - numerator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '136-1',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Dividend refund - after December 31, 2015 - denominator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '136-20',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Part I tax',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Basic rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '140'
                }
              }
            ]
          },
          {
            'label': 'Basic rate for a non-resident-owned investment corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '141'
                }
              }
            ]
          },
          {
            'label': 'General deduction of taxable income',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '142'
                }
              }
            ]
          },
          {
            'label': 'Rate for a mutual fund corporation or an investment corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '143'
                }
              }
            ]
          },
          {
            'label': 'Refundable tax on CCPC&#039;s investment income',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '144',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Refundable tax on CCPC&#039;s investment income – taxation year ending before January 1, 2016 - integer',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2157-0',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Refundable tax on CCPC&#039;s investment income – taxation year ending before January 1, 2016 - numerator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2157-1',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Refundable tax on CCPC&#039;s investment income – taxation year ending before January 1, 2016 - denominator ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2157-2',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Refundable tax on CCPC&#039;s investment income – taxation year ending after December 31, 2015 - integer',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2158-0',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Refundable tax on CCPC&#039;s investment income – taxation year ending after December 31, 2015 - numerator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2158-1',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Refundable tax on CCPC&#039;s investment income – taxation year ending after December 31, 2015 - denominator ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2158-2',
                  'decimals': 4
                }
              }
            ]
          },
          {
            'label': 'Income from a personal services business',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2053'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Balance unpaid (overpayment)',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Minimum amount assessed for collection or refund',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '145'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The small business deduction for a CCPC on the first $500,000 of ABI',
            'labelClass': 'bold fullLength'
          },
          {
            'label': '2015-12-31',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2040'
                }
              }
            ]
          },
          {
            'label': '2015-12-31',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2041',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': '2015-12-31',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2042'
                }
              }
            ]
          },
          {
            'label': '2015-12-31',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2043',
                  'decimals': 1
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 1 – Net income (loss) for income tax purposes',
        'rows': [
          {
            'label': 'Non-deductible meals and entertainment expenses at 50%',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '146'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Non-deductible expenses for food and beverages for long-haul truck drivers',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Non-deductible rate in 2008',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '147'
                }
              }
            ]
          },
          {
            'label': 'Non-deductible rate in 2009',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '148'
                }
              }
            ]
          },
          {
            'label': 'Non-deductible rate in 2010',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '149'
                }
              }
            ]
          },
          {
            'label': 'Non-deductible rate after 2010',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 2 – Charitable donations and gifts',
        'rows': [
          {
            'label': 'Maximum deduction',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Donations net income limit percentage',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '151'
                }
              }
            ]
          },
          {
            'label': 'Capital gain gifts rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '152'
                }
              }
            ]
          },
          {
            'label': 'Gifts of medicine rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '153'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 3 – Dividends received, taxable dividends paid, and Part IV tax calculation',
        'rows': [
          {
            'label': 'Part IV tax before 2015',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '155',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Part IV tax before 2015 - integer',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '156-0',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Part IV tax before 2015 - numerator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '156-1',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Part IV tax before 2015 - denominator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '156-2',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Part IV tax after 2016 - integer',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '157-0',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Part IV tax after 2016 - numerator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '157-1',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Part IV tax after 2016 - denominator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '157-2',
                  'decimals': 2
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 4 – Corporation loss continuity and application',
        'rows': [
          {
            'label': 'Restriction farm losses',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Basic amount',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '160'
                }
              }
            ]
          },
          {
            'label': 'Limit applicable to losses in excess of $15,000',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '161'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 5 – Tax calculation supplementary',
        'rows': [
          {
            'label': 'Newfoundland and Labrador',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Maximum resort property investment tax credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '170'
                }
              }
            ]
          },
          {
            'input1Header': 'Higher rate',
            'input2Header': 'Lower rate'
          },
          {
            'label': 'Newfoundland and Labrador',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year'
          },
          {
            'label': '2002-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '171'
                }
              },
              {
                'input': {
                  'num': '172'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2010-04-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '173'
                }
              },
              {
                'input': {
                  'num': '174'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Newfoundland and Labrador offshore',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year'
          },
          {
            'label': '2002-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '175'
                }
              },
              {
                'input': {
                  'num': '176'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2010-04-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '177'
                }
              },
              {
                'input': {
                  'num': '178'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Prince Edward Island',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year'
          },
          {
            'label': '2002-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '180',
                  'decimals': 1
                }
              },
              {
                'input': {
                  'num': '181',
                  'decimals': 1
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2005-04-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '182',
                  'decimals': 1
                }
              },
              {
                'input': {
                  'num': '183',
                  'decimals': 1
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2006-04-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '184',
                  'decimals': 1
                }
              },
              {
                'input': {
                  'num': '185',
                  'decimals': 1
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2007-04-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '186',
                  'decimals': 1
                }
              },
              {
                'input': {
                  'num': '187',
                  'decimals': 1
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2008-04-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '188',
                  'decimals': 1
                }
              },
              {
                'input': {
                  'num': '189',
                  'decimals': 1
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2009-04-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '190',
                  'decimals': 1
                }
              },
              {
                'input': {
                  'num': '191',
                  'decimals': 1
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2010-04-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '192'
                }
              },
              {
                'input': {
                  'num': '193'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Northwest Territories',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year'
          },
          {
            'label': '2002-07-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '194'
                }
              },
              {
                'input': {
                  'num': '195'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2004-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '196'
                }
              },
              {
                'input': {
                  'num': '197'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2006-07-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '198'
                }
              },
              {
                'input': {
                  'num': '199'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Nunavut',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year'
          },
          {
            'label': '2002-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200'
                }
              },
              {
                'input': {
                  'num': '201'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Political contributions tax credit – multiple provinces',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Bracket 1',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '210'
                }
              },
              {
                'input': {
                  'num': '211'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Bracket 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '212'
                }
              },
              {
                'input': {
                  'num': '213'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Bracket 3',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '214',
                  'decimals': 2
                }
              },
              {
                'input': {
                  'num': '215',
                  'decimals': 2
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Maximum allowable credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '216'
                }
              },
              {
                'input': {
                  'num': '217'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 6 – Summary of dispositions of capital property and Schedule 18 – Capital gains refund',
        'rows': [
          {
            'label': 'Inclusion rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '220'
                }
              }
            ]
          },
          {
            'label': 'Capital gains inclusion rate for gifts of certain capital property',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '221'
                }
              }
            ]
          },
          {
            'label': 'Personal-use property minimum proceeds of disposition and adjusted cost base',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '222'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 10 – Cumulative eligible capital deduction',
        'rows': [
          {
            'label': 'Eligible capital property rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '240',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Current year deduction rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '241'
                }
              }
            ]
          },
          {
            'label': 'Adjustment before July 1, 1988',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '242',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Adjustment after June 30, 1988',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '243',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Non-taxable portion of a non-arm\'s length transferor\'s gain',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '244',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Rate to calculate indicator B',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '245'
                }
              }
            ]
          },
          {
            'label': 'Rate to calculate indicator C',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '246'
                }
              }
            ]
          },
          {
            'label': 'Rate to calculate indicator J',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '247'
                }
              }
            ]
          },
          {
            'label': 'Rate to calculate indicator Q',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '248'
                }
              }
            ]
          },
          {
            'label': 'Rate to calculate indicator S - numerator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '249-0'
                }
              }
            ]
          },
          {
            'label': 'Rate to calculate indicator S - denominator',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '249-1'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 14 – Miscellaneous payments to residents',
        'rows': [
          {
            'label': 'Minimum amount of the payments',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '250'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 16 – Patronage dividend deduction',
        'rows': [
          {
            'label': 'Agricultural co-operative corporations',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Percentage of the income attributable to member-customers that limits the patronage dividend amount paid in the form of tax-deferred co-operative shares.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '260'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedules 27, 300, 381, 402, 404, 440 – Manufacturing and processing',
        'rows': [
          {
            'label': 'Profits deduction',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Manufacturing and processing profits deduction',
            'labelClass': 'fullLength bold tabbed'
          },
          {
            'label': 'Taxation year',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '2004-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '300'
                }
              }
            ]
          },
          {
            'label': '2008-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '301',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': '2009-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '302'
                }
              }
            ]
          },
          {
            'label': '2010-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '303'
                }
              }
            ]
          },
          {
            'label': '2011-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '304',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': '2012-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '305'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Generating electrical energy or producing steam deduction',
            'labelClass': 'fullLength bold tabbed'
          },
          {
            'label': 'Taxation year',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '2004-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '306'
                }
              }
            ]
          },
          {
            'label': '2008-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '307',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': '2009-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '308'
                }
              }
            ]
          },
          {
            'label': '2010-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '309'
                }
              }
            ]
          },
          {
            'label': '2011-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '310',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': '2012-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '311'
                }
              }
            ]
          },
          {
            'label': 'Maximum - Small manufacturing corporations',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '312'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Calculation of cost of capital',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Rate',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '313'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Calculation of cost manufacturing and processing capital',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Factor',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '314',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Calculation of cost manufacturing and processing labour',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Factor',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '315',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Newfoundland and Labrador – Schedule 300',
            'num': '319',
            'input2': true,
            'labelClass': 'fullLength bold tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Saskatchewan – Schedule 404',
            'labelClass': 'fullLength bold tabbed'
          },
          {
            'label': 'Income eligible for the manufacturing and processing profits tax reduction',
            'labelClass': 'fullLength bold tabbed'
          },
          {
            'label': 'Taxable income reduction – Federal foreign business income tax credit – factor for a taxation year ending before January 1, 2010',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '320'
                }
              }
            ]
          },
          {
            'label': 'Qualifying reduction rate',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '321'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Yukon – Schedule 440',
            'labelClass': 'fullLength bold tabbed'
          },
          {
            'label': 'Credit against income taxed at small business rate',
            'labelClass': 'tabbed'
          },
          {
            'label': 'Before January 1, 2005',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '323',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'After December 31, 2004',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '324',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'Credit against income taxed at general rate',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '325',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Investment tax credit',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Manitoba – Schedule 381',
            'labelClass': 'fullLength bold tabbed'
          },
          {
            'label': 'Tax credit rate',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '326'
                }
              }
            ]
          },
          {
            'label': 'Maximum refund rate',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '327'
                }
              }
            ]
          },
          {
            'label': 'Property acquired before January 1, 2008',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '328',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'Property acquired after December 31, 2007',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '329'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Saskatchewan – Schedule 402',
            'labelClass': 'fullLength bold tabbed'
          },
          {
            'label': 'Saskatchewan manufacturing and processing investment tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '330'
                }
              }
            ]
          },
          {
            'label': 'Saskatchewan refundable manufacturing and processing investment tax credit for qualified property acquired after April 6, 2006, and before October 28, 2006',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '331'
                }
              }
            ]
          },
          {
            'label': 'Saskatchewan refundable manufacturing and processing investment tax credit for qualified property acquired after October 27, 2006',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '332'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedules 31, 49 and 321 – Investment tax credit',
        'rows': [
          {
            'label': 'Investment tax credits on SR&ED expenditures',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Rate of investments for qualified property rate (Schedules 31 and 321)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '340'
                }
              }
            ]
          },
          {
            'label': 'Qualified resource property acquired primarily for use in Atlantic Canada and acquired'
          },
          {
            'label': '- after March 28, 2012, and before 2014',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '1102'
                }
              }
            ]
          },
          {
            'label': '- after 2013 and before 2016',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '1103'
                }
              }
            ]
          },
          {
            'label': '- after 2015*',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '1104'
                }
              }
            ]
          },
          {
            'label': 'Rate of qualified expenditures for a CCPC',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '341'
                }
              }
            ]
          },
          {
            'label': 'Rate for qualified expenditures for a CCPC exceeding the corporation\'s expenditure limit or rate of qualified expenditures incurred before 2014 for a corporation that is not a CCPC',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '342'
                }
              }
            ]
          },
          {
            'label': 'Rate for qualified expenditures for a CCPC exceeding the corporation\'s expenditure limit or rate of qualified expenditures incurred after 2013 for a corporation that is not a CCPC',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1343'
                }
              }
            ]
          },
          {
            'label': 'Rate for qualified expenditures incurred after 2013 for a corporation that is not a CCPC',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1105'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Expenditure limit for a CCPC throughout the current taxation year (Schedule 49)',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year',
            'input1Header': 'First limit',
            'input2Header': 'Second limit'
          },
          {
            'label': '2002-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '343'
                }
              },
              {
                'input': {
                  'num': '344'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2003-01-02',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '345'
                }
              },
              {
                'input': {
                  'num': '346'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2007-01-02',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '348'
                }
              },
              {
                'input': {
                  'num': '349'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2008-02-26',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '351'
                }
              },
              {
                'input': {
                  'num': '352'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2010-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '354'
                }
              },
              {
                'input': {
                  'num': '355'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable capital threshold used to calculate the SR&ED expenditure limit (Schedule 49)',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxable capital - Lower limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '357'
                }
              }
            ]
          },
          {
            'label': 'Taxable capital - Higher limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '358'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Pre-production mining expenditures',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Expenditures incurred',
            'labelClass': 'fullLength'
          },
          {
            'label': '2003-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '360'
                }
              }
            ]
          },
          {
            'label': '2004-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '361'
                }
              }
            ]
          },
          {
            'label': '2005-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '362'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Refund investment tax credit rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '363'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Apprenticeship job creation expenditures',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Tax credit rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '365'
                }
              }
            ]
          },
          {
            'label': 'Tax credit limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '366'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Child care space creation expenditures',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Tax credit rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '367'
                }
              }
            ]
          },
          {
            'label': 'Tax credit limit by new space created',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '368'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 32 (T661) – Claim for scientific research and experimental development (SR&ED) carried out in Canada',
        'rows': [
          {
            'label': 'Prescribed proxy rate',
            'num': '370',
            'input2': true,
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Prescribed proxy rate - in 2013 tax year',
            'num': '371',
            'input2': true,
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Prescribed proxy rate- after 2013 tax year',
            'num': '372',
            'input2': true,
            'labelClass': 'fullLength bold'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Maximum Canada Pension Plan pensionable earnings',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year',
            'labelClass': 'fullLength'
          },
          {
            'label': '2012-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '373'
                }
              }
            ]
          },
          {
            'label': '2013-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '374'
                }
              }
            ]
          },
          {
            'label': '2014-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '375'
                }
              }
            ]
          },
          {
            'label': '2015-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '376'
                }
              }
            ]
          },
          {
            'label': '2016-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '377'
                }
              }
            ]
          },
          {
            'label': '2017-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '378'
                }
              }
            ]
          },
          {
            'label': '2018-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '379'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedules 33, 34, 35 and 342 – Tax on large corporations or financial institutions or large insurance corporation',
        'rows': [
          {
            'label': 'Amount used for the business limit reduction',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Capital deduction',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '380'
                }
              }
            ]
          },
          {
            'label': 'Rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '381',
                  'decimals': 3
                }
              }
            ]
          },
          {
            'label': 'Threshold to determine if the corporation is a large corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '382'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Nova Scotia – Schedule 342',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Tax on large corporations after March 31, 2004'
          },
          {
            'label': 'Taxable capital less than $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '383',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'Taxable capital equal or over $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '384',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'Tax on large corporations after June 30, 2005'
          },
          {
            'label': 'Taxable capital less than $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '385',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Taxable capital equal or over $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '386',
                  'decimals': 3
                }
              }
            ]
          },
          {
            'label': 'Tax on large corporations after June 30, 2006'
          },
          {
            'label': 'Taxable capital less than $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '387',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'Taxable capital equal or over $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '388',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Tax on large corporations after June 30, 2007'
          },
          {
            'label': 'Taxable capital less than $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '389',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Taxable capital equal or over $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '390',
                  'decimals': 3
                }
              }
            ]
          },
          {
            'label': 'Tax on large corporations after June 30, 2008'
          },
          {
            'label': 'Taxable capital less than $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '391',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'Taxable capital equal or over $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '392',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'Tax on large corporations after June 30, 2009'
          },
          {
            'label': 'Taxable capital less than $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '393',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'Taxable capital equal or over $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '394',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Tax on large corporations after June 30, 2010'
          },
          {
            'label': 'Taxable capital less than $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '395',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'Taxable capital equal or over $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '396',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'Tax on large corporations after June 30, 2011'
          },
          {
            'label': 'Taxable capital less than $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '397',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'Taxable capital equal or over $10,000,000',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '398',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Rate for the energy eficiency tax credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '399'
                }
              }
            ]
          },
          {
            'label': 'Sch 33 - Rate for Small Business Deduction',
            'num': '322',
            'input2': true,
            'labelClass': 'fullLength',
            'decimals': 3
          },
          {
            'label': 'Sch 33 - Amount for Part 5',
            'num': '811',
            'input2': true,
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Schedules 47 (T1131), 48 (T1177) and 422 (T1196) – A film, video production or television',
        'rows': [
          {
            'label': 'A Canadian film or video production tax credit',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Production cost limit rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '420'
                }
              }
            ]
          },
          {
            'label': 'Canadian film or video production tax credit rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '421'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'A film or video production services tax credit',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Film or video production services tax credit rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '422'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'British Columbia film and television tax credit',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Production cost rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '423'
                }
              }
            ]
          },
          {
            'label': 'Production cost rate (for productions with principal photography that begin after February 28, 2010)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '424'
                }
              }
            ]
          },
          {
            'label': 'Basic tax credit rate for labour expenditures (for expenses incurred after December 31, 2004)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '425'
                }
              }
            ]
          },
          {
            'label': 'Additional basic tax credit rate for labour expenditures (for expenses incurred after December 31, 2007)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '426'
                }
              }
            ]
          },
          {
            'label': 'Regional tax credit rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '427',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'Distant location regional tax credit rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '428'
                }
              }
            ]
          },
          {
            'label': 'Training tax credit rate for labour expenditures paid',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '429'
                }
              }
            ]
          },
          {
            'label': 'Training tax credit rate for qualified labour expenditures',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '430'
                }
              }
            ]
          },
          {
            'label': 'Digital animation or visual effects basic tax credit rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '431'
                }
              }
            ]
          },
          {
            'label': 'Digital animation or visual effects additional basic tax credit rate (for productions with principal photography that begin after February 28, 2010)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '432',
                  'decimals': 1
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 53 – General rate income pool (GRIP) calculation',
        'rows': [
          {
            'label': 'Multiplication factor based on the deduction rate for eligible small business',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year'
          },
          {
            'label': '2006-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '450',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'General rate factor',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year'
          },
          {
            'label': '2006-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '451',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': '2010-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '452',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': '2011-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '453',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': '2012-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '454',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': '2015-01-01 and after',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '456',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Income inclusion rate in the calculation of the GRIP addition for 2006',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year'
          },
          {
            'label': '2006-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '455'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 54 – Low rate income pool (LRIP) calculation',
        'rows': [
          {
            'label': 'Income inclusion rate for the credit union deduction and the aggregate investment income',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year'
          },
          {
            'label': '2006-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '460'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Multiplying factor of the deduction for investment corporations (line 620 of the T2 return)',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year'
          },
          {
            'label': '2006-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '461'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Adjustment for the corporation that ceases to be a CCPC or DIC',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Rate for the calculation of net capital losses',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '462'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 55 – Part III.1 tax on excessive eligible dividend designations',
        'rows': [
          {
            'label': 'CCPC or DIC',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year'
          },
          {
            'label': '2006-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '470'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Other',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year'
          },
          {
            'label': '2006-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '471'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedules 71, 72, and 73 - Income Inclusion Summary for Corporations that are Members of Partnerships',
        'rows': [
          {
            'label': 'Deferral of Corporate Tax',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Eligible reserve rates for the qualifying transitional income',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Year the reserve was created',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '840'
                }
              }
            ]
          },
          {
            'label': 'First subsequent year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '841'
                }
              }
            ]
          },
          {
            'label': 'Second subsequent year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '842'
                }
              }
            ]
          },
          {
            'label': 'Third subsequent year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '843'
                }
              }
            ]
          },
          {
            'label': 'Fourth subsequent year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '844'
                }
              }
            ]
          },
          {
            'label': 'Fifth subsequent year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '845'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Rate for threshold amount',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '846'
                }
              }
            ]
          },
          {
            'label': 'Part 3 - Rate for additional amount',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '324-73',
                  'decimals': 1
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Instalments',
        'rows': [
          {
            'label': 'Income tax instalment threshold before 2008',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '820'
                }
              }
            ]
          },
          {
            'label': 'Income tax instalment threshold after 2007',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '821'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Quarterly instalments',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Maximum taxable income',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '822'
                }
              }
            ]
          },
          {
            'label': 'Maximum taxable capital',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '823'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Non-deductible automobile expenses for passenger vehicles',
        'rows': [
          {
            'label': 'Interest expenses',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Monthly interest expenses limit based on the acquisition date',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Year of acquisition',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'In 2000',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '830'
                }
              }
            ]
          },
          {
            'label': 'After 2000',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '831'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Leasing costs',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Monthly limit of leasing costs according to the date the lease began',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Year the lease began',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'In 2000',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '832'
                }
              }
            ]
          },
          {
            'label': 'After 2000',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '833'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Maximum eligible cost based on the year of acquisition',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Year of acquisition',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'In 2000',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '834'
                }
              }
            ]
          },
          {
            'label': 'After 2000',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '835'
                }
              }
            ]
          },
          {
            'label': 'Rate – Eligible cost',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '836'
                }
              }
            ]
          },
          {
            'label': 'Rate – Greater of lines 4 and 5',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '837'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Excess amount for the refundable interests',
            'labelClass': 'fullLength bold',
            'num': '838',
            'input2': true
          }
        ]
      },
      {
        'header': 'Internet filing',
        'rows': [
          {
            'label': 'Federal',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Mandatory electronic filing',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Gross income – threshold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '850'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Non-resident corporations',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Maximum amount of tax withheld',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '851'
                }
              }
            ]
          },
          {
            'label': 'Maximum amount of tax withheld if it is the first year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '852'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Other Section 20(1)(e) Deduction. S1, Line 703',
        'rows': [
          {
            'label': 'Rate for column 8:',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '2050'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Other information',
        'rows': [
          {
            'label': 'Deemed taxable income for the purpose of certain calculations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '860'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Provincial and territorial lower and higher tax rates not including Quebec and Alberta',
        'rows': [
          {
            'type': 'table',
            'num': '1020'
          }
        ]
      }
    ]
  };
})();
