(function() {
  wpw.tax.global.tableCalculations.ratesFed = {
    "1020": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          cellClass: 'alignLeft'
        },
        {
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true,
          "header": "Lower rate"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true,
          "header": "Higher rate"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        }],
      "cells": [{
        "0": {
          "label": "Newfoundland and Labrador"
        },
        "2": {
          "num": "1021"
        },
        "4": {
          "num": "1022"
        }
      },
        {
          "0": {
            "label": "Nova Scotia"
          },
          "2": {
            "num": "1023"
          },
          "4": {
            "num": "1024"
          }
        },
        {
          "0": {
            "label": "Prince Edward Island"
          },
          "2": {
            "num": "1025", decimals: 1
          },
          "4": {
            "num": "1026"
          }
        },
        {
          "0": {
            "label": "New Brunswick"
          },
          "2": {
            "num": "1027"
          },
          "4": {
            "num": "1028"
          }
        },
        {
          "0": {
            "label": "Ontario"
          },
          "2": {
            "num": "1029", decimals: 1
          },
          "4": {
            "num": "1030", decimals: 1
          }
        },
        {
          "0": {
            "label": "Manitoba"
          },
          "2": {
            "num": "1031"
          },
          "4": {
            "num": "1032"
          }
        },
        {
          "0": {
            "label": "Saskatchewan"
          },
          "2": {
            "num": "1033"
          },
          "4": {
            "num": "1034"
          }
        },
        {
          "0": {
            "label": "British Columbia"
          },
          "2": {
            "num": "1035", decimals: 1
          },
          "4": {
            "num": "1036"
          }
        },
        {
          "0": {
            "label": "Yukon"
          },
          "2": {
            "num": "1037"
          },
          "4": {
            "num": "1038"
          }
        },
        {
          "0": {
            "label": "Northwest Territories"
          },
          "2": {
            "num": "1039"
          },
          "4": {
            "num": "1040", decimals: 1
          }
        },
        {
          "0": {
            "label": "Nunavut"
          },
          "2": {
            "num": "1041"
          },
          "4": {
            "num": "1042"
          }
        }]
    }
  }
})();
