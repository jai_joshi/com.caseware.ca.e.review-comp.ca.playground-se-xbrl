(function() {

  wpw.tax.global.formData.t2s23 = {
    'formInfo': {
      'abbreviation': 'T2S23',
      'title': 'Agreement Among Associated Canadian- Controlled Private Corporation to Allocate the Business Limit',
      'schedule': 'Schedule 23',
      'code': 'Code 1001',
      'headerImage': 'canada-federal',
      hideHeaderBox: true,
      'showCorpInfo': true,
      formFooterNum: 'T2 SCH 23 E (15)',
      'description': [
        {
          'type': 'list',
          'items': [
            'For use by a Canadian-controlled private corporation (CCPC) to identify all associated corporations ' +
            'and to assign a percentage for each associated corporation. This percentage will be used to allocate ' +
            'the business limit for purposes of the small business deduction. Information from this schedule will ' +
            'also be used to determine the date the balance of tax is due and to calculate the reduction to the' +
            ' business limit.',
            'An associated CCPC that has more than one tax year ending in a calendar year, is required to file an ' +
            'agreement for each tax year ending in that calendar year.'
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    'sections': [
      {
        'header': '', 'hideFieldset': true,
        'rows': [
          {
            'type': 'table', 'num': '010'
          }
        ]
      },
      {
        'header': 'Allocating the business limit',
        'rows': [
          {
            type: 'infoField',
            inputType: 'date',
            label: 'Date filed (do not use this area)',
            tn: '025',
            num: '025',
            disabled: true,
            cannotOverride: true
          },
          {

            labelClass: 'fullLength'
          },
          {
            label: 'Enter the calendar year to which the agreement applies',
            num: '050', "validate": {"or": [{"length": {"min": "4", "max": "4"}}, {"check": "isEmpty"}]},
            tn: '050',
            type: 'infoField',
            inputType: 'fixedSizeBox', boxes: 4
          },
          {

            labelClass: 'fullLength'
          },
          {
            'label': 'Is this an amended agreement for the above calendar year that is intended to ' +
            'replace an agreement previously filed by any of the associated corporations listed below?',
            'labelClass': 'fullLength',
            'num': '075',
            'type': 'infoField',
            'inputType': 'radio',
            labelWidth: '85%',
            'tn': '075'
          },
          {
            'type': 'table', 'num': '085'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Business limit reduction under subsection 125(5.1) of the Act',
            labelClass: 'fullLength bold'
          },
          {
            label: 'The business limit reduction is calculated in the small business deduction area of the T2 return.' +
            ' One of the factors used in this calculation is the "large corporation amount" at line 415 of the T2' +
            ' return. The amount at line 415 is determined using the formula 0.225% x (D - $10,000,000). ' +
            'Details of this formula and variable D are in subsection 125(5.1) of the Act.',
            labelClass: 'fullLength'
          },
          {
            label: '* Each corporation will enter on line 410 of the T2 return, the amount allocated to it in column 6.' +
            ' However, if the corporation\'s tax year is less than 51 weeks, prorate the amount in column 6 by the' +
            ' number of days in the tax year divided by 365, and enter the result on line 410 of the T2 return.',
            labelClass: 'fullLength tabbed'
          },
          {
            labelClass: 'fullLength'
          },
          {
            label: 'Special rules for business limit',
            labelClass: 'fullLength bold'
          },
          {
            label: 'Special rules apply under subsection 125(5) if a CCPC has more than one tax year ending in the ' +
            'same calendar year and it is associated in more than one of those tax years with another CCPC that has' +
            ' a tax year ending in that calendar year. The business limit for the second or later tax year' +
            ' will be equal to the business limit determined for the first tax year ending in the calendar year or ' +
            'the business limit determined for the second or later tax year ending in the same calendar year, ' +
            'whichever is less.',
            labelClass: 'fullLength'
          }
          //{'label': 'Business limit reduction under subsection 125(5.1) of the ITA', labelClass: 'fullLength bold'},
          //{labelClass: 'fullLength'},
          //{
          //  'label': 'The business limit reduction is calculated in the small business deduction area of the' +
          //  ' T2 return. One of the factors used in this calculation is the \'Large corporation amount\' at ' +
          //  'line 415 of the T2 return. If the corporation is a member of an associated group ** of corporations' +
          //  ' in the current tax year, the amount at line 415 of the T2 return is equal to 0.225% x ' +
          //  '(A - $10,000,000) where, \'A\' is the total of taxable capital employed in Canada *** of each ' +
          //  'corporation in the associated group for its last tax year-ending in the preceding calendar year.',
          //  'labelClass': 'fullLength'
          //},
          //{
          //  'label': '* Each corporation will enter on line 410 of the T2 return, the amount allocated to it in' +
          //  ' column 6. However, if the corporation\'s tax year is less than 51 weeks, prorate the amount ' +
          //  'in column 6 by the number of days in the tax year divided by 365, and enter the result on line 410' +
          //  ' of the T2 return.',
          //  'labelClass': 'fullLength'
          //},
          //{
          //  'label': 'Special rules apply if a CCPC has more than one tax year-ending in a calendar year and ' +
          //  'is associated in more than one of those years with another CCPC that has a tax year-ending in the' +
          //  ' same calendar year. If the tax year straddles January 1, 2009, the business limit for the second' +
          //  ' (or subsequent) tax year(s) will be equal to the lesser of the business limit that would have been' +
          //  ' determined for the first tax year-ending in the calendar year, if $500,000 was used in allocating ' +
          //  'the amounts among associated corporations and the business limit determined for the second ' +
          //  '(or subsequent) tax year(s) ending in the same calendar year. Otherwise, the business limit for ' +
          //  'the second (or subsequent) tax year(s) will be equal to the lesser of the business limit determined' +
          //  ' for the first tax year-ending in the calendar year and the business limit determined for the second' +
          //  ' (or subsequent) tax year(s) ending in the same calendar year.',
          //  'labelClass': 'fullLength'
          //},
          //{
          //  'label': '** The associated group includes the corporation filing this schedule and each corporation ' +
          //  'that has an \'association code\' of 1 or 4 in column 3.',
          //  'labelClass': 'fullLength'
          //},
          //{
          //  'label': '*** \'Taxable capital employed in Canada\' has the mearning assigned by subsection 181.2(1)' +
          //  ' or 181.3(1) or section 181.4 of the ITA.',
          //  'labelClass': 'fullLength'
          //}
        ]
      }
    ]
  };
})();
