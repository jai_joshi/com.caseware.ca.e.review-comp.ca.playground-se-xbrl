(function() {

  wpw.tax.create.calcBlocks('t2s23', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field) {
      if (field('t2j.061').get())
        field('050').assign(field('t2j.061').getKey('year'));
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var table = field('085');
      var linkedTable = form('entityProfileSummary').field('085');
      table.getRows().forEach(function(row, rIndex) {
        row.forEach(function(cell, cIndex) {
          cell.assign(linkedTable.cell(rIndex, cIndex).get());
          cell.source(linkedTable.cell(rIndex, cIndex));
        })
      });
    });
  });
})();
