(function() {
  wpw.tax.global.tableCalculations.t2s23 = {
    '010': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          width: '10%',
          type: 'none'
        },
        {
          width: '90%',
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {
            label: '<b> Column 1: </b>'
          },
          '1': {
            label: 'Enter the legal name of each of the corporations in the associated group. Include non-CCPCs and' +
            ' CCPCs that have filed an election under subsection 256(2) of the <i>Income Tax Act</i> not to be' +
            ' associated for purposes of the small business deduction.'
          }
        },
        {
          '0': {
            label: '<b> Column 2: </b>'
          },
          '1': {
            label: 'Provide the business number for each corporation (if a corporation is not registered, enter "NR").'
          }
        },
        {
          '0': {
            label: '<b> Column 3: </b>'
          },
          '1': {
            label: 'Enter the association code from the list below that applies to each corporation: '
          }
        },
        {
          '0': {
            labelClass: 'fullLength'
          },
          '1': {
            label: '1- Associated for purposes of allocating the business limit (unless code 5 applies)'
          }
        },
        {
          '0': {
            labelClass: 'fullLength'
          },
          '1': {
            label: '2- CCPC that is a \'third corporation\' that has elected under subsection 256(2) not to be associated for purposes of the small business deduction'
          }
        },
        {
          '0': {
            labelClass: 'fullLength'
          },
          '1': {
            label: '3- Non-CCPC that is a \'third corporation\' as defined in subsection 256(2)'
          }
        },
        {
          '0': {
            labelClass: 'fullLength'
          },
          '1': {
            label: '4-Associated non-CCPC'
          }
        },
        {
          '0': {
            labelClass: 'fullLength'
          },
          '1': {
            label: '5 – Associated CCPC to which code 1 does not apply because of a subsection 256(2) election made by a "third corporation"'
          }
        },
        {
          '0': {
            label: '<b> Column 4: </b>'
          },
          '1': {
            label: 'Enter the business limit for the year of each corporation in the associated group.'
          }
        },
        {
          '0': {
            label: '<b> Column 5: </b>'
          },
          '1': {
            label: 'Assign a percentage to allocate the business limit to each corporation that has an association code 1 in column 3. The total of all percentages in column 5 cannot exceed 100%'
          }
        },
        {
          '0': {
            label: '<b> Column 6: </b>'
          },
          '1': {
            label: 'Enter the business limit allocated to each corporation by multiplying the amount in column 4 by the percentage in column 5. Add all business limits allocated in column 6 and enter the total at line A.'
          }
        },
        {
          '0': {
            labelClass: 'fullLength'
          },
          '1': {
            label: 'Ensure that the total at line A does not exceed $500,000.'
          }
        }
      ]
    },
    '085': {
      fixedRows: true,
      showNumbering: true,
      columns: [
        {
          header: '1- Names of associated corporations',
          num: '100',"validate": {"or":[{"length":{"min":"1","max":"175"}},{"check":"isEmpty"}]},
          tn: '100',
          disabled: true,
          type: 'text'
        },
        {
          header: '2- Business Number of Associated Corporations',
          num: '200',
          tn: '200',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR'],
          disabled: true
        },
        {
          header: '3- Association Code',
          num: '300',
          tn: '300',
          width: '10%',
          type: 'dropdown',
          options: [
            {
              value: '1',
              option: '1- Associated for purposes of allocating the business limit (unless code 5 applies)'
            },
            {
              value: '2',
              option: '2 – CCPC that is a "third corporation" that has elected under subsection 256(2) not to be ' +
              'associated for purposes of the small business deduction'
            },
            {
              value: '3',
              option: '3 – Non-CCPC that is a "third corporation" as defined in subsection 256(2)'
            },
            {
              value: '4',
              option: '4- Associated non-CCPC'
            },
            {
              value: '5',
              option: '5- Associated CCPC to which code 1 does not apply because of a subsection 256(2) election made by a \'third corporation\''
            }
          ]
        },
        {
          header: '4- Business limit for the year before the allocation $',
          width: '10%',
          num: '301',
          disabled: true
        },
        {
          header: '5- Percentage of the business limit %',
          num: '350',
          width: '10%',
          tn: '350',
          filters: 'decimals 2',
          disabled: true,
          total: true,
          totalNum: '1350',
          format: {
            suffix: '%'
          },
          decimals: 2,
          totalLimit: '100',
          "validate": {
            "and": [
              'percent',
              {
                "or": [
                  {"length": {"min": "1", "max": "6"}},
                  {"check": "isEmpty"}
                ]
              }
            ]
          },
        },
        {
          header: '6- Business limited allocated* $',
          num: '400',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          width: '13%',
          total: true,
          totalIndicator: 'A',
          tn: '400'
        }
      ],
      hasTotals: true
    }
  }
})();
