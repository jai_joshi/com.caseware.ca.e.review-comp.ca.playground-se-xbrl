(function() {
  wpw.tax.create.diagnostics('t2s23', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('0230002', function(tools) {
      var table = tools.mergeTables(tools.field('t2s9.050'), tools.field('t2s23.085'));
      return tools.checkAll(table.getRows(), function(row) {
        if (tools.field('t2j.040').get() == '1' &&
            (tools.field('t2j.430').isNonZero() || tools.field('t2j.896').isYes()) &&
            (tools.field('t2j.160').isYes() || (row[3] == '1' ||
            row[3] == '2' || row[3] == '3' &&
            typeof row[2] != 'undefined') ||
            tools.field('t2s31.400').isNonZero() || row[11].get() == '3'))
          return !!wpw.tax.utilities.flagger()['T2S23'];
        else return true;
      });
    });

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S23'), forEach.row('085', forEach.bnCheckCol(1, true))));

    diagUtils.diagnostic('0230150', common.prereq(common.requireFiled('T2S23'),
        function(tools) {
          return tools.checkAll(tools.field('085').getRows(), function(row) {
            if (tools.checkMethod([row[0], row[1], row[2]], 'isNonZero'))
              return tools.requireAll([row[0], row[1], row[2], row[5]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0230010', common.prereq(common.and(
        common.requireFiled('T2S23'),
        common.check(['t2j.061'], 'isFilled')),
        function(tools) {
          return tools.field('050').require(function() {
            return this.get() == tools.field('t2j.061').getKey('year')
          });
        }));

    diagUtils.diagnostic('0231012', common.prereq(common.requireFiled('T2S23'),
        function(tools) {
          var table = tools.field('085');
          var duplicate = false;
          var bn = {};
          for (var i = 0; i < table.size().rows; i++) {
            var rowBN = table.cell(i, 1).get();
            if (rowBN && bn[rowBN]) {
              table.cell(i, 1).mark();
              duplicate = true;
            }
            bn[rowBN] = true;
          }
          return !duplicate;
        }));

    diagUtils.diagnostic('percentTotal', common.prereq(common.requireFiled('T2S23'),
        function(tools) {
          var table = tools.field('085');
          return tools.checkAll(table.getRows(), function(row) {
            return row[4].require(function() {
              return tools.field('1350').get() == 100;
            })
          });
        }));

  });
})();

