var fs = require("fs");
// Get content from file
var contents = fs.readFileSync("gifimappingJson.json");
// Define to JSON type
var jsonContent = JSON.parse(contents);
//convert to wanted json format
var output = {};

jsonContent.forEach(function(mapObj) {
  //Current Year
  if (!output.hasOwnProperty(mapObj.taxprepCurrentYearID)) {
    output[mapObj.taxprepCurrentYearID] = {};
    output[mapObj.taxprepCurrentYearID].gifiCode = mapObj.gifiCode;
    output[mapObj.taxprepCurrentYearID].colIndex = 0;
  }

  //Prior Year
  if (!output.hasOwnProperty(mapObj.taxprepPriorYearID)) {
    output[mapObj.taxprepPriorYearID] = {};
    output[mapObj.taxprepPriorYearID].gifiCode = mapObj.gifiCode;
    output[mapObj.taxprepPriorYearID].colIndex = 1;
  }
});

//export the json to file
fs.writeFile('gifiOutput.json', JSON.stringify(output), function () {
  console.log("The file was saved!");
});
