(function() {
  wpw.tax.create.diagnostics('t2s302', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    //Error code 0050525 is defined in t2s5 diagnostics.

    //Error code 0050523 is defined in t2s5 diagnostics.

    diagUtils.diagnostic('3020540', common.prereq(common.requireFiled('T2S302'),
        function(tools) {
      var table = tools.field('900');
      return tools.checkAll(table.getRows(), function(row) {
        if (tools.checkMethod([row[0], row[1]], 'isNonZero'))
          return tools.requireAll([row[0], row[1]], 'isNonZero');
        else return true
      });
    }));

    diagUtils.diagnostic('302.300', common.prereq(common.requireFiled('T2S302'),
        function(tools) {
      return tools.field('300').require(function() {
        return this.get() <= '3000000';
      })
    }));

  });
})();
