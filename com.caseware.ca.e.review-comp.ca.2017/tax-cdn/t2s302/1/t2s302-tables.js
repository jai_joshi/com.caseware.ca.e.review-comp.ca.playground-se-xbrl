(function() {

  wpw.tax.create.tables('t2s302', {

    900: {
      showNumbering: true,
      hasTotals: true,
      columns: [
        {
          header: 'Certificate number<br><br>',
          tn: '100',"validate": {"or":[{"length":{"min":"9","max":"9"}},{"check":"isEmpty"}]}, 
          type: 'text'
        },
        {
          header: 'Amount of the Newfoundland and Labrador film and video industry tax credit<br>',
          colClass: 'std-input-width',
          tn: '200',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          total: true,
          totalNum: '300',
          totalTn: '300',
          totalMessage: 'Total amount of the Newfoundland and Labrador film and video industry tax credit'
        }
      ]
    }
  })
})();

