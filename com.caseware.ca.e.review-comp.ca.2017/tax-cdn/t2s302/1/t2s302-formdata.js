(function() {

  wpw.tax.create.formData('t2s302', {
    formInfo: {
      abbreviation: 'T2S302',
      title: 'Additional Certificate Numbers for the Film and Video Industry Tax Credit',
      schedule: 'Schedule 302',
      headerImage: 'canada-federal',
      category: 'Newfoundland and Labrador Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: 'For use by a corporation that has more than one certificate number for the Newfoundland' +
            ' and Labrador film and video industry tax credit.',
            labelClass: 'fullLength tabbed2'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '900'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Enter this amount on line 521 in Part 2 of Schedule 5.',
            labelClass: 'fullLength tabbed'
          }
        ]
      }
    ]
  });
})();
