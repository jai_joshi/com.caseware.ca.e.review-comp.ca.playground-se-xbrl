(function() {

  wpw.tax.create.calcBlocks('t2s302', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field) {
      field('300').assign(field('900').total(1).get());
    });

  });
})();
