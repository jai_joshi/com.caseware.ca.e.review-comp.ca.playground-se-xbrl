(function() {

  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  function runHistoricalTableCalc(calcUtils, tableObj, caryBackAmount, limitOnCreditAmount) {
    var tableNum = tableObj.tableNum;
    var openingBalanceCindex = tableObj.openingBalanceCindex;
    var currentYearCindex = tableObj.currentYearCindex;
    var transferCindex = tableObj.transferCindex;
    var appliedCindex = tableObj.appliedCindex;
    var endingBalanceCindex = tableObj.endingBalanceCindex;

    var summaryTable = calcUtils.field(tableNum);
    var numRows = summaryTable.size().rows;

    summaryTable.getRows().forEach(function(row, rowIndex) {
      if (rowIndex == numRows - 1) {
        if (caryBackAmount > 0) {
          summaryTable.cell(11, 8).assign(caryBackAmount);
          row[endingBalanceCindex].assign(Math.max(row[openingBalanceCindex].get() + row[currentYearCindex].get() +
              row[transferCindex].get() - row[appliedCindex].get(), 0))
        }
        else {
          summaryTable.cell(11, 8).assign(0)
        }
      }
      else {
        if (rowIndex == 0) {
          // to avoid the first row being calculated to total
          row[currentYearCindex].assign(0);
          row[appliedCindex].assign(0);
          row[transferCindex].assign(0);
          row[endingBalanceCindex].assign(0);
        }
        else {
          row[appliedCindex].assign(returnAmountApplied(limitOnCreditAmount, row[openingBalanceCindex].get()));
          row[endingBalanceCindex].assign(Math.max(row[openingBalanceCindex].get() + row[currentYearCindex].get()
              + row[transferCindex].get() - row[appliedCindex].get(), 0));
          limitOnCreditAmount -= row[appliedCindex].get();
        }
      }
    });
  }

  function runSumByKeyCal(calcUtils, date1, date2, fieldId) {
    var data = [];
    var field = calcUtils.field;
    var tableNum = '1000';
    var valColIndex = 3;
    var dateColIndex = 2;

    field(tableNum).getRows().forEach(function(row) {
      var amount = row[valColIndex].get();
      var acquisitionDate = calcUtils.dateCompare.betweenInclusive(row[dateColIndex].get(), date1, date2);
      data.push({'date': acquisitionDate, 'amount': amount})
    });
    field(fieldId).assign(calcUtils.sumByKey(data, 'date', true, 'amount'));
  }

  wpw.tax.create.calcBlocks('t2s402', function(calcUtils) {
    calcUtils.calc(function(calcUtils) {
      var num = '218';
      var num2 = '225';
      var midnum = '219';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '223';
      var num2 = '226';
      var midnum = '224';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });

    calcUtils.calc(function(calcUtils, field) {
      //part 1
      var amountBeforeDate = 0;
      var amountAfterDate = 0;
      field('1000').getRows().forEach(function(row) {
        var amount = row[3].get();
        var acqDate = row[2].get();
        var isAfterMarch2017 = calcUtils.dateCompare.greaterThanOrEqual(acqDate, wpw.tax.date(2017, 3, 23));
        if (isAfterMarch2017) {
          amountAfterDate += amount;
        } else {
          amountBeforeDate += amount;
        }
      });
      field('300').assign(field('1000').total(3).get());
      field('301').assign(amountBeforeDate);
      field('302').assign(amountAfterDate);
    });

    calcUtils.calc(function(calcUtils, field) {
      //part 2
      field('105').assign(field('304').get() - field('104').get());
      field('106').assign(field('105').get());
      field('143').assign(field('106').get() + field('110').get());
      field('162').assign(field('150').get() + field('160').get()+ field('161').get());
      field('163').assign(field('162').get());
      field('200').assign(field('143').get() - field('163').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 3
      calcUtils.getGlobalValue('219', 'ratesSk', '502');
      calcUtils.getGlobalValue('224', 'ratesSk', '503');

      calcUtils.equals('218', '301');
      calcUtils.equals('223', '302');

      field('250').assign(
          field('225').get() +
          field('226').get() +
          field('230').get() +
          field('240').get()
      );
    });

  });
})();
