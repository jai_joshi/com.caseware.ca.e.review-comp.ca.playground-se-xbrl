(function() {
  wpw.tax.create.formData('t2s402', {
    formInfo: {
      abbreviation: 'T2S402',
      title: 'Saskatchewan Manufacturing and Processing Investment Tax Credit',
      code: 'Code 1701',
      schedule: 'Schedule 402',
      formFooterNum: 'T2 SCH 402 E (17)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if you are a corporation (other than one that is exempt from tax ' +
              'under section 149 of the federal <i>Income Tax Act</i>) that has acquired qualified property in the ' +
              'current year for use in Saskatchewan, and you want to claim a Saskatchewan manufacturing ' +
              'and processing investment tax credit.'
            },
            {
              label: 'Use this schedule to show a credit allocated from a trust or a partnership.'
            },
            {
              label: 'Include the capital cost of the qualified property (see Part 1 below).'
            },
            {
              label: 'To claim this credit:',
              sublist: [
                'you must have acquired qualified property in the current year for use in Saskatchewan',
                'you must use the property primarily for manufacturing or processing goods for sale or lease.',
                'the property must not have been used, or acquired for use or lease, for any purpose before you acquired it.'
              ]
            },
            {
              label: '<b>Qualified property</b>, is defined in subsections 127(9), (11), and (11.1) of the federal Act. ' +
              'The federal definition of qualified property is altered for the Saskatchewan investment tax credit ' +
              'by including property eligible under paragraph (c) of Class 43.1 of Schedule II of the federal ' +
              '<i>Income Tax Regulations</i> Property that you lease (other than to a person exempt from tax under section ' +
              '149 of the federal act) may also qualify for the credit.'
            },
            {
              label: '<b>Manufacturing or processing</b> is defined in subsection 125.1(3) of the federal Act ' +
              'and includes qualified activities as defined by section 5202 of the federal ' +
              '<i>Income Tax Regulations</i>. The federal definition of <b>manufacturing or processing</b> is altered for ' +
              'the Saskatchewan investment tax credit by excluding paragraph (h).'
            },
            {
              label: 'The non-refundable credit may be renounced but must include all current year credits;' +
              ' partial renouncements are not permitted. The renouncement must be filed on or before the' +
              ' filing due date of the federal <i>T2 Corporation Income Tax Return</i>'
            },
            {
              label: 'File one completed copy of this schedule with your <i>T2 Corporation Income Tax Return</i> .'
            }
          ]
        }
      ],
      category: 'Saskatchewan Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Qualified property (acquired in current tax year) eligible for the credit',
        'rows': [
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Total capital cost</b> (total of column 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'double',
                'input': {
                  'num': '300'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Total capital cost of property acquired before March 23, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'double',
                'input': {
                  'num': '301'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Total capital cost of property acquired after March 22, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'double',
                'input': {
                  'num': '302'
                }
              }
            ],
            'indicator': 'C'
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of credit available for the year',
        'rows': [
          {
            'label': 'Credit at the end of the preceding tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '304'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'label': 'Credit expired*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'single',
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit at the beginning of the tax year (amount D <b>minus</b> line 104)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '106',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Credit transferred on an amalgamation or the wind-up of a subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              }
            ]
          },
          {
            'label': 'Total credit available (line 105 <b>plus</b> line 110)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '143'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Credit renounced',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '150'
                }
              }
            ]
          },
          {
            'label': 'Credit claimed in the current year (amount E <b>minus</b> line 150)' +
            ' (Enter line 160, on line 630 in Part 2 of Schedule 5** )',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              }
            ]
          },
          {
            'label': 'Credit carried back to preceding tax year(s)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                underline: 'single',
                'input': {
                  'num': '161'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '162'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '163'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Closing balance',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* The carry-forward period for a credit earned in the current tax year from qualified property ' +
            'acquired before April 7, 2006, and an unused credit earned in the 7 preceding tax years ending before ' +
            'April 7, 2006, has now been extended to 10 years. A credit earned in a tax year ending before April 7,' +
            ' 1999, had a carry-forward period of seven years and is now expired.',
            'labelClass': 'fullLength'
          },
          {
            'label': '**The carry-forward period ends for the 2017 tax year. There will be no further carryforward ' +
            'after the 2017 tax year. You have to claim or renounce any leftover carry-forward from the previous year.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 – Calculation of Saskatchewan refundable manufacturing and processing investment tax credit',
        'rows': [
          {
            'label': 'Qualified property acquired in the current tax year:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': 'di_table_1'
          },
          {
            'type': 'table',
            'num': 'di_table_2'
          },
          {
            'label': 'Credit allocated from a partnership ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '230',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '230'
                }
              }
            ]
          },
          {
            'label': 'Credit allocated from a trust',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '240',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '240'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': '<b>Saskatchewan refundable manufacturing and processing investment tax credit</b> (total of lines 225, 226, 230, and 240)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'double',
                'input': {
                  'num': '250'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Enter amount F on line 644 of Schedule 5, ,<i>Tax Calculation Supplementary – Corporations</i>.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  });
})();
