(function() {

  wpw.tax.create.diagnostics('t2s402', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('4020001', common.prereq(common.and(
        common.requireFiled('T2S402'),
        common.check(['t2s5.630'], 'isNonZero')),
        function(tools) {
          var table = tools.field('1000');
          return tools.checkAll(table.getRows(), function(row) {
            return (tools.requireAll([row[0], row[2], row[3]], 'isFilled') ||
            tools.requireOne(tools.list(['105', '110']), 'isNonZero'));
          });
        }));

    diagUtils.diagnostic('4020002', common.prereq(common.requireFiled('T2S402'),
        function(tools) {
          var table = tools.field('1000');
          return tools.checkAll(table.getRows(), function(row) {
            var columns = [row[0], row[2], row[3]];
            if (tools.checkMethod(columns, 'isFilled'))
              return tools.requireAll(columns, 'isFilled');
            else return true;
          });
        }));

    diagUtils.diagnostic('4020003', common.prereq(common.and(
        common.requireFiled('T2S402'),
        common.check(['t2s5.644'], 'isNonZero')),
        function(tools) {
          var table = tools.field('1000');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireAll([row[0], row[2], row[3]], 'isFilled') ||
                tools.requireOne(tools.list(['230', '240']), 'isNonZero');
          });
        }));

  });
})();
