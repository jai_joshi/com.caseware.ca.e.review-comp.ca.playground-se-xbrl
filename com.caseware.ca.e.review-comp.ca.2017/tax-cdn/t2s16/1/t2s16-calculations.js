(function() {
  function fillArray(array, start, end) {
    for (var i = start; i <= end; i = i + 1) {
      array.push(i)
    }
  }
  wpw.tax.create.calcBlocks('t2s16', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 1 calculations //
      calcUtils.sumBucketValues('109', ['103', '106']);
      calcUtils.sumBucketValues('111-0', ['110', '111']);
      field('902-0').assign(100);
      calcUtils.subtract('902', '902-0', '901');
      calcUtils.equals('905', '901');
      calcUtils.equals('906', '112');
      calcUtils.multiply('907', ['905', '906'], 1 / 100);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 2 Calculations //
      calcUtils.equals('909', '907');
      calcUtils.multiply('910', ['909'], 85 / 100);
      calcUtils.min('911', ['209', '910']);
      calcUtils.subtract('912', '109', '209');
      calcUtils.sumBucketValues('913', ['911', '912']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 3 calculations //
      // if elseif situation; not sure how to do calc for num: 914//
      calcUtils.equals('915', '907');
      calcUtils.equals('916', '104');
      calcUtils.sumBucketValues('917', ['915', '916']);
      calcUtils.min('918', ['914', '917']);
      calcUtils.equals('113', '918');
      calcUtils.sumBucketValues('919', ['114', '130']);
      calcUtils.equals('935', '907');
      calcUtils.subtract('936', '113', '104');
      calcUtils.subtract('937', '935', '936');
      calcUtils.equals('938', '937');
      calcUtils.min('115', ['919', '938']);
      calcUtils.sumBucketValues('116', ['113', '115']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 4 Calculations //
      calcUtils.equals('920', '919');
      calcUtils.equals('921', '115');
      calcUtils.subtract('922', '920', '921');
      calcUtils.subtract('923', '914', '113');
      calcUtils.sumBucketValues('117', ['922', '923']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 5 calculations //
      // if elseif situation again- active business income- for num: 924 //
      calcUtils.equals('946', '901');
      calcUtils.equals('947', '118');
      calcUtils.multiply('948', '946', '947');
      // TODO: if calcUtils for 948, if 118 == 112
      calcUtils.sumBucketValues('928', ['948', '120']);
      calcUtils.min('121', ['119', '928']);
      calcUtils.equals('929', '121');
      calcUtils.subtract('930', '121', '120');
      calcUtils.subtract('931', '929', '930');
      calcUtils.min('123', ['931', '122']);
      calcUtils.sumBucketValues('932', ['121', '123']);
      calcUtils.subtract('124', '118', '932');
    });
  });
})();
