(function() {
  'use strict';

  wpw.tax.global.formData.t2s16 = {
      formInfo: {
        abbreviation: 'T2S16',
        title: 'Patronage Dividend Deduction',

        //subTitle: '(2006 and later tax years)',
        schedule: 'Schedule 16',
        //code: 'Code 0602',
        headerImage: 'canada-federal',
        showCorpInfo: true,
        description: [
          {text: ''},
          {
            type: 'list',
            items: [
              'Use this form to claim a deduction from income for payments made to customers for allocations in' +
              ' proportion to patronage (patronage dividends) made within the year or within the following 12 months.',
              'Only co-operatives and credit unions are entitled to a deduction for patronage dividends paid ' +
              'to non-arm\'s length persons, except for prescribed payments.',
              'Do not include payments made to member customers in an agency relationship under contractual' +
              ' obligations.',
              'Generally, tax-deferred co-operative shares are particular types of shares that are issued after' +
              ' 2005 and before 2021 by an agricultural co-operative corporation to a person or partnership that ' +
              'is an eligible member of the agricultural co-operative corporation at the time of the issuance,' +
              ' under an allocation in proportion to patronage.',
              'File a completed schedule with the <i> T2 Corporation Income Tax Return </i> within six months of ' +
              'the end of the year',
              'File an amended Schedule 16 for payments that are deductible in the tax year, but made to customers ' +
              'after filing this schedule.',
              'For more information, see Interpretation Bulletin IT-362, <i> Patronage Dividends </i>.'
              //'This schedule may contain changes that had not yet become law at the time of publishing.'
            ]
          }
        ],
        category: 'Federal Tax Forms'
      },
      sections: [
        {
          'header': 'Part 1 - Details of patronage dividends paid and other information',
          'rows': [
            {
              'type': 'table',
              'num': '100'
            },
            {
              'labelClass': 'fulLLength'
            },
            {
              'label': 'Were any of the above patronage dividends paid by an agricultural co-operative corporation. in the form of tax-deferred co-operative shares?',
              'labelWidth': '84%',
              'type': 'infoField',
              'inputType': 'radio',
              'num': '150',
              'tn': '150',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total patronage dividends paid to all customers (total of amounts A and B)',
              'labelClass': '2',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '109',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '109'
                  }
                }
              ],
              'indicator': 'C'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '900'
            },
            {
              'label': 'Net income before patronage dividend deduction',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '112',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '112'
                  }
                }
              ],
              'indicator': 'D'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '904'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Note: </b> Keep all documents showing how the payment was made, the date(s) of payment, the date(s) of allocation, and the year(s) of the customer\'s patronage',
              'labelClass': 'tabbed2 fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Were the patronage dividends calculated at the same rate to all customers (member and non-members), except to allow for different types, classes, grades or qualities of products, or services?',
              'labelClass': 'tabbed2',
              'num': '200',
              'tn': '200',
              'type': 'infoField',
              'inputType': 'radio',
              'labelWidth': '84%'
            }
          ]
        },
        {
          'header': 'Part 2 - Calculation of agricultural co-operative corporations',
          'rows': [
            {
              'label': 'Complete this part if you answered <b> yes</b> to the question on line 150 above. If you answered <b> no</b>, leave it blank and go to Part 3.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Patronage dividends paid ion the form of tax-deferred co-operative shares',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '209',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '209'
                  }
                }
              ],
              'indicator': 'E'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '908'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Enter amount E or F, whichever is less',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '911'
                  }
                }
              ],
              'indicator': 'G'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Add: </b> Patronage dividends paid in the form of property other than tax-deferred co-operative shares (amount C <b>minus</b> amount E)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '912'
                  }
                }
              ],
              'indicator': 'H'
            },
            {
              'label': 'Amount used in the calculations of the patronage dividend deduction of an agricultural co-operative corporation (amount G <b>plus</b> amount H)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '913'
                  }
                }
              ],
              'indicator': 'I'
            }
          ]
        },
        {
          'header': 'Part 3 - Calculation of patronage dividend deduction',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '1. Deduction for current-year payments: '
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If you completed Part 2, enter amount I at line J. Otherwise, enter amount C from Part 1 at line J.',
              'labelClass': 'tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total patronage dividends paid to all customers',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '914'
                  }
                }
              ],
              'indicator': 'J'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If you answered <b> yes </b> to the question on line 200 in part 1, enter amount J at line K. Otherwise, calculate your patronage dividend deduction for current-year payments as follows:',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Income attributable to member customer business (amount c from Part 1)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '915'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'd'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Add: </b> Patronage dividends credited to non-member customers of the year'
            },
            {
              'label': '(amount from line 104 from Part 1)',
              'labelClass': '2',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '916'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'e'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total (amount d <b>plus</b> amount e)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '917'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'f'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Enter amount J or amount f, whichever is less (enter this amount at line K)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '918'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'g'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Patronage dividend deduction for current-year payments',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '113',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '113'
                  }
                }
              ],
              'indicator': 'K'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '2. Deduction for amounts carried forward:'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Balance of undeducted amounts carried forward from a previous year',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '114',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '114'
                  }
                }
              ],
              'indicator': 'L'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Add </b> : Undeducted amounts transferred on amalgamation or wind-up of a subsidiary',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '130',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '130'
                  }
                }
              ],
              'indicator': 'M'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total of lines 114 and 130',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '919'
                  }
                }
              ],
              'indicator': 'N'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Income attributable to member customers business (amount C from Part 1)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '935'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'h'
                  }
                }
              ]
            },
            {
              'label': '<b>Less</b>: Amount K <b>minus</b> amount from line 104',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '936'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'l'
                  }
                }
              ]
            },
            {
              'label': 'Total (amount h <b>minus</b> amount i)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '937'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '938'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'O'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Portion of amount carried forward that is deductible in the current year',
              'labelClass': 'tabbed'
            },
            {
              'label': '(amount N or amount O, whichever is less)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '115',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '115'
                  }
                }
              ],
              'indicator': 'Q'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '3. <b> Patronage dividend deduction </b> (amount K <b>plus</b> amount Q) .',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '116',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '116'
                  }
                }
              ],
              'indicator': 'R'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Enter amount from line R on line 416 of Schedule 1.',
              'labelClass': 'tabbed'
            }
          ]
        },
        {
          'header': 'Part 4 - Patronage dividends carried forward',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Amount from line N',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '920'
                  }
                }
              ],
              'indicator': 'S'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Deduct</b>: Portion of carryfoward deducted in the current tax year (amount from line Q)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '921'
                  }
                }
              ],
              'indicator': 'T'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Subtotal (amount S <b>minus</b> amount T)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '922'
                  }
                }
              ],
              'indicator': 'U'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Add: </b> Portion of current year payments eligible for carryforward (amount J <b>minus</b> amount K)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '923'
                  }
                }
              ],
              'indicator': 'V'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Balance of patronage dividends available for carryforward </b> (amount U <b>plus</b> amount V)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '117',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '117'
                  }
                }
              ],
              'indicator': 'W'
            },
            {
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 5 - Calculation of income from an active business carried on in Canada (ABI)',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Active business income (ABI) before patronage dividend deduction',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '118',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '118'
                  }
                }
              ],
              'indicator': 'X'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If amount X is equal to amount D in Part 1, enter the patronage dividend deduction from line R in Part 3',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '924'
                  }
                }
              ],
              'indicator': 'Y'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If the amount X is different from the amount D, complete the following:'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total patronage dividends (paid to all customers) attributable to the ABI (amount C from Part 1)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '119',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '119'
                  }
                }
              ],
              'indicator': 'Z'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'ABI attributable to member customer business:',
              'labelClass': 'bold'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '945'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Add: </b> Patronage dividends credited to non-member customers of the year and attributable to income earned from an active business (amount from line 104 in Part 1)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '120',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '120'
                  }
                }
              ],
              'indicator': 'BB'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total (amount AA plus amount BB)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '928'
                  }
                }
              ],
              'indicator': 'CC'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Patronage dividend deduction for current-year payments (enter amount Z or amount CC, whichever is less) .',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '121',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '121'
                  }
                }
              ],
              'indicator': 'DD'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Deduction for amounts carried forward: </b>'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'ABI attributable to member customer business (amount AA)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '929'
                  }
                }
              ],
              'indicator': 'EE'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b>Less:</b> Amount DD <b>minus</b> amount BB',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '930'
                  }
                }
              ],
              'indicator': 'FF'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total (amount EE <b>minus</b> amount FF)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '931'
                  }
                }
              ],
              'indicator': 'GG'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Portion of amount carried forward that relates to the active business carried on Canada',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '122',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '122'
                  }
                }
              ],
              'indicator': 'HH'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Amount carried forward that is deductible in the current year (enter amount GG or amount HH, whichever is less)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '123',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '123'
                  }
                }
              ],
              'indicator': 'II'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total of amounts DD and II',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '932'
                  }
                }
              ],
              'indicator': 'JJ'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Patronage dividend deduction (amount Y or amount JJ, whichever applies)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '933'
                  }
                }
              ],
              'indicator': 'KK'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Income from active business carried on in Canada </b> (amount X <b>minus</b> amount KK)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '124',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '124'
                  }
                }
              ],
              'indicator': 'LL'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Enter amount from line LL at line 400 of the T2 return.'
            }
          ]
        }
      ]
    };
})();
