(function() {
  wpw.tax.global.tableCalculations.t2s16 = {
    "100": {
      hasTotals: true,
      "fixedRows": true,
      "columns": [{
        "width": "40%",
        "type": "none"
      },
        {
          "total": true,
          "totalMessage": "Totals",
          "totalIndicator": "A",
          "totalNum": "103"
        },
        {
          "width": "35%",
          "total": true,
          "totalIndicator": "B",
          "totalNum": "106"
        }],
      "cells": [{
        "1": {
          "label": "Member customers",
          "type": "none"
        },
        "2": {
          "label": "Non-member customers",
          "type": "none"
        }
      },
        {
          "0": {
            "label": "Payments to customers of the year"
          },
          "1": {
            "num": "101", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "101"
          },
          "2": {
            "num": "104", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "104"
          }
        },
        {
          "0": {
            "label": "Payments to customers of a previous year"
          },
          "1": {
            "num": "102", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "102"
          },
          "2": {
            "num": "105", "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            "tn": "105"
          }
        }]
    },
    "900": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [{
        colClass: 'std-padding-width',
        "type": "none"
      },
        {
          "width": "165px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "185px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          "width": "5px",
          "type": "none"
        },
        {
          colClass: 'std-input-width'
        },
        {
          "width": "5px",
          "type": "none"
        },
        {
          "width": "50px",
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          "type": "none"
        }],
      "cells": [{
        "1": {
          "label": "Total business transacted:",
          "labeClass": "bold"
        },
        "2": {
          "label": " a) with member customers"
        },
        "3": {
          "tn": "110"
        },
        "5": {
          "num": "110"
, "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        },
        "7": {
          "num": "901",
          "filters": "decimals 2",
          decimals: 3
        },
        "8": {
          "label": "a"
        }
      },
        {
          "2": {
            "label": "b) with non-member customers"
          },
          "3": {
            "tn": "111"
          },
          "5": {
            "num": "111"
, "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          },
          "7": {
            "num": "902",
            "filters": "decimals 2",
            decimals: 3
          },
          "8": {
            "label": "b"
          }
        },
        {
          "2": {
            "label": "Total (line 110 <b>plus</b> line 111)"
          },
          "3": {},
          "5": {
            "num": "111-0",
            "inputClass": "doubleUnderline"
          },
          "7": {
            "num": "902-0",
            "filters": "decimals 2",
            "inputClass": "doubleUnderline",
            decimals: 3
          },
          "8": {}
        }]
    },
    "904": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [{
        colClass: 'std-padding-width',
        "type": "none"
      },
        {
          "width": "90px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "60px"
        },
        {
          "width": "20px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "20px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "150px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
            colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "none"
        },
        {
          "type": "none"
        }],
      "cells": [{
        "1": {
          "label": "Percentage a"
        },
        "2": {
          "num": "905",
          decimals: 3
        },
        "3": {
          "label": "%"
        },
        "4": {
          "label": "x"
        },
        "5": {
          "label": "Amount D"
        },
        "6": {
          "num": "906"
        },
        "7": {
          "label": "="
        },
        "8": {
          "num": "907",
          "inputClass": "doubleUnderline"
        },
        "9": {
          "label": "c"
        }
      }]
    },
    "908": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [{
        "width": "20px",
        "type": "none"
      },
        {
          "width": "90px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "width": "90px",
          cellClass: 'alignCenter',
          "type": "none"
        },
        {
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        }],
      "cells": [{
        "1": {
          "label": "Amount c"
        },
        "2": {
          "num": "909"
        },
        "3": {
          "label": " x  85%="
        },
        "5": {
          "num": "910",
          "inputClass": "doubleUnderline"
        },
        "6": {
          "label": "F"
        }
      }]
    },
    "945": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [{
        "width": "100px",
        "type": "none"
      },
        {
          "width": "100px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "30px"
        },
        {
          "width": "7px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "30px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "100px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "100px"
        },
        {
          "width": "10px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "100px"
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        }],
      "cells": [{
        "1": {
          "label": "Percentage a from Part 1"
        },
        "2": {
          "num": "946",
          decimals: 3
        },
        "3": {
          "label": "%"
        },
        "4": {
          "label": "x",
          "labelClass": "center"
        },
        "5": {
          "label": "amount X"
        },
        "6": {
          "num": "947"
        },
        "7": {
          "label": "="
        },
        "8": {
          "num": "948"
        },
        "9": {
          "label": "AA"
        }
      }]
    }
  }
})();
