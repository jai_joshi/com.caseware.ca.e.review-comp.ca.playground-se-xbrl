(function() {
  wpw.tax.create.diagnostics('t2s16', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0160001', common.prereq(common.check(['t2s1.416'], 'isNonZero'), common.requireFiled('T2S16')));

    diagUtils.diagnostic('0160005', common.prereq(common.and(
        common.requireFiled('T2S16'),
        function(tools) {
          return tools.checkMethod(tools.list(['t2s1.416', '109', '110', '111', '113']), 'isNonZero');
        }), function(tools) {
      return tools.requireOne(tools.list(['101', '102', '104', '105']), 'isNonZero');
    }));

    diagUtils.diagnostic('0160010', common.prereq(common.and(
        common.requireFiled('T2S16'),
        function(tools) {
          return tools.field('113').isNonZero() && tools.field('200').isNo();
        }), function(tools) {
      return tools.requireAll(tools.list(['110', '111']), 'isNonZero');
    }));

    diagUtils.diagnostic('0160015', common.prereq(common.and(
        common.requireFiled('T2S16'),
        function(tools) {
          return tools.field('121').isNonZero() && tools.field('200').isNo();
        }), function(tools) {
      return tools.requireAll(tools.list(['118', '119', '120']), 'isNonZero');
    }));

    diagUtils.diagnostic('0160020', common.prereq(common.and(
        common.requireFiled('T2S16'),
        function(tools) {
          return tools.checkMethod(tools.list(['101', '102', '104', '105']), 'isNonZero');
        }), function(tools) {
      return tools.field('200').require('isNonZero');
    }));

    diagUtils.diagnostic('0160021', common.prereq(common.and(
        common.requireFiled('T2S16'),
        common.check('150', 'isYes')),
        function(tools) {
          return tools.field('209').require('isNonZero');
        }));

    diagUtils.diagnostic('0160025', common.prereq(common.and(
        common.requireFiled('T2S16'),
        common.check('119', 'isPositive')),
        function(tools) {
          return tools.requireOne(tools.list(['110', '111']), 'isNonZero');
        }));
  });
})();

