(function () {
  wpw.tax.create.formData('t2a9s', {
    formInfo: {
      abbreviation: 't2a9s',
      title: 'Alberta Dispositions of Capital Property',
      formFooterNum: 'AT192 (Jan-13)',
      // headerImage: 'canada-alberta',
      showCorpInfo: true,
      category: 'Alberta Tax Forms'
    },
    sections: [
      {
        rows: [
          {
            labelClass: 'fullLength'
          },
          {
            label: 'For use by a corporation for a taxation year ending on or before March 31, 2012 in which the ' +
            'corporation is claiming the Alberta SR&ED tax credit. Schedule 9 Supplemental is to be filed with ' +
            'Schedule 9, "Alberta Scientific & Experimental Development (SR&ED) Tax Credit". If the corporation, ' +
            'in the immediately preceding taxation year, applied federal investment tax credits for SR&ED that ' +
            'originated in more than one taxation year, then a separate AT1 Schedule 9 Supplemental must be prepared ' +
            'for each year in which the federal investment tax credits originated.<b> Schedule 9 Supplemental must be ' +
            'received by Alberta Tax and Revenue Administration within 21 months of the taxation year end in ' +
            'which the Alberta SR&ED eligible expenditures were incurred</b>. For more information on completing ' +
            'Schedule 9 Supplemental, see the Guide to Claiming the Alberta SR&ED Tax Credit (the Guide). ',
            labelClass: 'fullLength'
          },
          {
            label: 'Report all monetary values in dollars; DO NOT include cents.'
          },
          {type: 'horizontalLine'},
          {
            label: 'This Schedule 9 Supplemental is prepared in respect of the federal investment tax credits originating in the taxation year\n' +
            'ending',
            type: 'infoField',
            inputType: 'date',
            num: '001'
          },
          {
            label: 'that were deducted from federal tax payable in the immediately preceding taxation year.',
            labelClass: 'fullLength'
          },
          {
            label: 'If this schedule is prepared in respect of federal investment tax credits originating in a taxation year other than the\n' +
            'immediately preceding taxation year, all instructions for the "prior" or "immediately preceding" taxation year, on lines 400\n' +
            'to 428, except line 424, should be read as if referring to the year in which the investment tax credits originated.',
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            label: 'Alberta Portion of Prior Year Federal Investment Tax Credit',
            labelClass: 'fullLength bold'
          },
          {
            "label": "Prior year eligible expenditures (prior year line 031 of Schedule 9) less prior year Alberta SR&ED tax credit (prior year line 110 of Schedule 9 - see the Guide)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "400"
                },
                "padding": {
                  "type": "tn",
                  "data": "400"
                }
              },
              null
            ]
          },
          {
            "label": "Prior year maximum expenditure limit (prior year line 108 of Schedule 9) less prior year Alberta SR&ED tax credit (prior year line 110 of Schedule 9 - see the Guide)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "402"
                },
                "padding": {
                  "type": "tn",
                  "data": "402"
                }
              },
              null
            ]
          },
          {
            "label": "Lesser of line 400 and 402",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "404"
                },
                "padding": {
                  "type": "tn",
                  "data": "404"
                }
              },
              null
            ]
          },
          {
            "label": "A X B / C",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "406"
                },
                "padding": {
                  "type": "tn",
                  "data": "406"
                }
              },
              null
            ]
          },
          {
            label: 'A = Prior year federal expenditure limit (prior year line 410 of federal schedule 31) ',
            labelClass: 'fullLength'
          },
          {
            label: 'B = Prior year eligible expenditures less prior year Alberta SR&ED tax credit (line 400 above) ',
            labelClass: 'fullLength'
          },
          {
            label: 'C = Prior year federal expenditures of the corporation (prior year line 559 of federal T661)',
            labelClass: 'fullLength'
          },
          {
            "label": "Lesser of line 404 and 406",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "408"
                },
                "padding": {
                  "type": "tn",
                  "data": "408"
                }
              },
              null
            ]
          },
          {
            "label": "Line 408 X 35%",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "410"
                },
                "padding": {
                  "type": "tn",
                  "data": "410"
                }
              }
            ]
          },
          {
            "label": "Prior year eligible expenditures less prior year Alberta SR&ED tax credit (line 400 above) less line 406",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "412"
                },
                "padding": {
                  "type": "tn",
                  "data": "412"
                }
              },
              null
            ]
          },
          {
            "label": "Greater of line 412 and zero",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "414"
                },
                "padding": {
                  "type": "tn",
                  "data": "414"
                }
              },
              null
            ]
          },
          {
            "label": "Prior year maximum expenditure limit less prior year Alberta SR&ED tax credit (line 402 above)less line 406",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "416"
                },
                "padding": {
                  "type": "tn",
                  "data": "416"
                }
              },
              null
            ]
          },
          {
            "label": "Greater of line 416 and zero",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "418"
                },
                "padding": {
                  "type": "tn",
                  "data": "418"
                }
              },
              null
            ]
          },
          {
            "label": "Lesser of line 414 and 418",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "420"
                },
                "padding": {
                  "type": "tn",
                  "data": "420"
                }
              }
            ]
          },
          {
            "label": "Line 420 X 20%",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "422"
                },
                "padding": {
                  "type": "tn",
                  "data": "422"
                }
              }
            ]
          },
          {
            "label": "Amount of federal SR&ED investment tax credits for the immediately preceding\n" +
            "taxation year deducted from federal tax payable\n" +
            "(line 435 of federal T661 - see the Guide)\n",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "424"
                },
                "padding": {
                  "type": "tn",
                  "data": "424"
                }
              }
            ]
          },
          {
            "label": "Amount of federal SR&ED investment tax credits for the immediately\n" +
            "preceding taxation year (prior year line 540 of federal schedule 31)\n",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "426"
                },
                "padding": {
                  "type": "tn",
                  "data": "426"
                }
              }
            ]
          },
          {
            "label": "(Line 410 + line 422) X (line 424 / line 426) X 10%",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "428"
                },
                "padding": {
                  "type": "tn",
                  "data": "428"
                }
              }
            ]
          },
          {
            label: 'Enter the amount from line 428 on page 2, line 116 of Schedule 9',
            labelClass: 'bold'
          }
        ]
      }
    ]
  });
})();
