(function() {

  wpw.tax.create.calcBlocks('t2a9s', function(calcUtils) {
    calcUtils.calc(function (calcUtils, field) {
      field('404').assign(Math.min(field('400').get(), field('402').get()));
      field('408').assign(Math.min(field('404').get(), field('406').get()));
      field('410').assign(field('408').get() * 35 /100);
      field('412').assign(field('400').get() - field('406').get());
      field('414').assign(Math.max(field('412').get(), 0));
      field('416').assign(field('402').get() - field('406').get());
      field('418').assign(Math.max(field('416').get(), 0));
      field('420').assign(Math.min(field('414').get(), field('418').get()));
      field('422').assign(field('420').get() * 20 /100);
      calcUtils.getGlobalValue('424', 'T661', '435');
      field('428').assign(
          (field('410').get() + field('422').get()) *
          (field('424').get() / field('426').get()) *
          10 /100
      );
    });
  });
})();
