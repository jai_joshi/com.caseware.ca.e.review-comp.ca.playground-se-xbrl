(function() {

  wpw.tax.global.formData.sec_20_1_e = {
      formInfo: {
        abbreviation: 'SEC_20_1_e',
        title: 'Section 20(1)(e) Deduction',
        showCorpInfo: true,
        dynamicFormWidth: true,
        headerImage: 'canada-federal',
        description: [
          {
            type: 'list',
            items: [
              {
                label: 'Use this workchart for section 20(1)(e) deduction and related election on subsection ' +
                '20(1)(e)(v)'
              },
              {
                label: 'Make sure to add back any of expenses, which have been deducted on financial statements, ' +
                'on line 216, "Financing fees deducted in books", and/or on line 235, ' +
                '"Share issue expense" to Schedule 1, if applicable'
              }
            ]
          }
        ],
        category: 'Workcharts'
      },
      sections: [
        {
          hideFieldset: true,
          rows: [
            {labelClass: 'fullLength'},
            {
              type: 'table', num: '1000'
            },
            {labelClass: 'fullLength'},
            {
              label: 'Column 7 Amount A Calculation:'
            },
            {
              type: 'table', num: '800'
            },
            {labelClass: 'fullLength'},
            {
              label: '* For conditions (ii), (ii.1) and (ii.2):',
              labelClass: 'fullLength'
            },
            {
              label: '<i>"Annual fees, etc": "...(other than a payment that is contingent or dependent on ' +
              'the use of, or production from, property or is computed by reference to revenue, profit, ' +
              'cash flow, commodity price or any other similar criterion or by reference to dividends' +
              ' paid or payable to shareholders of any class of shares of the capital stock of a ' +
              'corporation) as a standby charge, guarantee fee, registrar fee, transfer agent fee, ' +
              'filing fee, service fee or any similar fee, ..." </i>',
              labelClass: 'fullLength tabbed'
            },
            {
              label: 'excerpt from <i>Income Tax Act</i> (R.S.C., 1985, c. 1 (5th Supp.)), Section 20(1)(e.1)',
              labelClass: 'fullLength tabbed'
            }
          ]
        }
      ]
    };
})();
