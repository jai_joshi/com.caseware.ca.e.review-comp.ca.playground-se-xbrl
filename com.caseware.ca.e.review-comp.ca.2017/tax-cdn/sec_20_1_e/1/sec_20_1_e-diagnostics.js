(function() {
  wpw.tax.create.diagnostics('sec_20_1_e', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('sec20.beforeDate', common.prereq(
        function(tools) {
          var table = tools.field('1000');
          return tools.checkAll(table.getRows(), function(row) {
            return row[1].isNonZero();
        })},function(tools) {
          var table = tools.field('1000');
          return tools.checkAll(table.getRows(), function(row) {
            if(wpw.tax.utilities.dateCompare.lessThan(row[1].get(), tools.field('cp.tax_start').get())) {
              return tools.requireAll([row[1], row[5]], function() {
               return wpw.tax.utilities.dateCompare.greaterThanOrEqual(row[1].get(), tools.field('cp.tax_start').get()) ||
                    row[5].isNonZero();
              })
            } else return true;
          })
        }));

    diagUtils.diagnostic('sec20.afterDate', common.prereq(
        function(tools) {
          var table = tools.field('1000');
          return tools.checkAll(table.getRows(), function(row) {
            return row[1].isNonZero();
        })},function(tools) {
          var table = tools.field('1000');
          return tools.checkAll(table.getRows(), function(row) {
            if(wpw.tax.utilities.dateCompare.greaterThan(row[1].get(), tools.field('cp.tax_end').get())) {
              return row[1].require(function() {
               return wpw.tax.utilities.dateCompare.lessThanOrEqual(this.get(), tools.field('cp.tax_end').get());
              })
            } else return true;
          })
        }));

    diagUtils.diagnostic('sec20.withinDate', common.prereq(
        function(tools) {
          var table = tools.field('1000');
          return tools.checkAll(table.getRows(), function(row) {
            return row[1].isNonZero() && row[5].isNonZero();
        })},function(tools) {
          var table = tools.field('1000');
          return tools.checkAll(table.getRows(), function(row) {
            if(wpw.tax.utilities.dateCompare.between(row[1].get(), tools.field('cp.tax_start').get(), tools.field('cp.tax_end').get())) {
              return tools.requireAll([row[1], row[5]], function() {
                return !wpw.tax.utilities.dateCompare.between(row[1].get(), tools.field('cp.tax_start').get(), tools.field('cp.tax_end').get()) ||
                row[5].isZero();
              })
            } else return true;
          })
        }));

  });
})();
