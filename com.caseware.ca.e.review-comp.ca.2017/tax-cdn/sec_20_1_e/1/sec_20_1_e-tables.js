(function() {
  wpw.tax.global.tableCalculations.sec_20_1_e = {
    '800': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          'type': 'none',
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'type': 'none',
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-spacing-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          cellClass: 'alignCenter',
          'type': 'none'
        }
      ],
      'cells': [{
        '1': {
          'label': 'Amount A ='
        },
        '2': {num: '400'},
        '3': {
          'label': '  %  x ',
          'labelClass': 'center'
        },
        '4': {
          'label': 'Number of days in the tax year',
          cellClass: 'singleUnderline alignCenter'
        },
        '6': {
          cellClass: 'singleUnderline alignCenter',
          num: '401'
        },
        '7': {
          'label': ' = ',
          'labelClass': 'center'
        },
        '8': {num: '403', decimals: 2}
      },
        {
          '2': {
            'type': 'none'
          },
          '4': {
            'label': 'Number of days in a full calendar year',
            'labelClass': 'center'
          },
          '6': {
            num: '402'
          },
          '8': {type: 'none'}
        }]
    },
    '1000': {
      'showNumbering': true,
      hasTotals: true,
      'maxLoop': 100000,
      'columns': [
        {
          header: 'Subsection 20(1)(e)(v) applies in the current tax year',
          type: 'singleCheckbox',
          width: '60px'
        },
        {
          'header': '1<br><br>Date',
          width: '110px',
          'type': 'date'
        },
        {
          'header': '2<br><br>Description',
          'width': '150px',
          cellClass: 'alignCenter',
          type: 'text'
        },
        {
          'header': '3<br><br>Conditions',
          'type': 'n-dropdown',
          'width': '60px',
          cellClass: 'alignRight',
          'options': [
            {
              option: '20(1)(e)(i)',
              options: [
                {
                  'option': '(i): Subsection 20(1)(e)(i)',
                  'value': '1'
                }
              ]
            },
            {
              option: '20(1)(e)(ii)',
              options: [
                {
                  'option': '(ii): Subsection 20(1)(e)(ii)',
                  'value': '2'
                },
                {
                  'option': '(ii.1): Subsection 20(1)(e)(ii.1)',
                  'value': '3'
                },
                {
                  'option': '(ii.2): Subsection 20(1)(e)(ii.2)',
                  'value': '4'
                }
              ]
            }
          ]
        },
        {
          'header': '4<br><br>Expense Amount*',
          'width': '60px',
          cellClass: 'alignLeft',
          total: true,
          totalMessage: 'Totals'
        },
        {
          'header': '5<br><br>Amount deducted in previous taxation year',
          'width': '60px',
          cellClass: 'alignLeft',
          total: true
        },
        {
          'header': '6<br><br>Balance before deduction<br>(column 4 minus column 5)',
          'width': '60px',
          cellClass: 'alignLeft',
          total: true
        },
        {
          'header': '7<br><br>Calculated deduction for the year (column 7 multiply with amount A)',
          'width': '60px',
          cellClass: 'alignLeft',
          total: true
        },
        {
          'header': '8<br><br>Annual deduction<br>(lesser of column 6 and column 7)',
          'width': '60px',
          cellClass: 'alignLeft',
          total: true
        },
        {
          'header': '9<br><br>Ending balance for deduction<br>(column 6 minus column 8)',
          'width': '60px',
          cellClass: 'alignLeft',
          total: true
        }]
    }
  }
})();
