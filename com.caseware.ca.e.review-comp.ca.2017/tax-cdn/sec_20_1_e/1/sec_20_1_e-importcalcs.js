wpw.tax.create.importCalcs('sec_20_1_e', function(tools) {
  tools.intercept('DEDUC.SLIPA.TdeducF2', function(importObj) {
    if (importObj.value == 'Y') {
      tools.form('sec_20_1_e').setValue('1000', 2, 3, importObj.rowIndex);
      tools.form('sec_20_1_e').setValue('1000', true, 0, importObj.rowIndex)
    }
  })
});
