(function() {

  wpw.tax.create.calcBlocks('sec_20_1_e', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //subsection 20(1)(e)(v) application
      //Calculation for column 8
      calcUtils.getGlobalValue('400', 'ratesFed', '2050');
      calcUtils.getGlobalValue('401', 'CP', 'Days_Fiscal_Period');
      field('402').assign(365);
      field('403').assign(field('402').get() == 0 ? 0 :
          (field('400').get() *
          (field('401').get() /
          field('402').get())));

      var table = field('1000');
      table.getRows().forEach(function(row) {
        if (row[4].get() > row[5].get()) {
          row[6].assign(row[4].get() - row[5].get());
        }
        else {
          row[6].assign(0);
        }
        row[7].assign(row[4].get() * field('403').get() * (1 / 100));
        if (row[0].get() == '1' && (
            row[3].get() == '2' ||
            row[3].get() == '3' ||
            row[3].get() == '4')) {
          row[8].assign(row[6].get());
          row[9].assign(0);
        }
        else {
          row[8].assign(Math.min(row[6].get(), row[7].get()));
          row[9].assign(row[6].get() - row[8].get()
          );
        }
      });
    });
  });
})();
