(function() {
  wpw.tax.create.diagnostics('t2rentalworkchart', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    // CUSTOM DIAGNOSTICS
    // notice for the non-corp percentage feature in expense section
    diagUtils.diagnostic('rentalWC.nonCorpPercentage', function(tools) {
      return tools.field('8520').mark().isZero();
    });

    // notice for partnership calculation
    diagUtils.diagnostic('rentalWC.partnershipCalc', common.prereq(function(tools) {
      return tools.field('1021').get() == '2';
    }, function(tools) {
      return tools.field('1025').mark().isNonZero();
    }));

  });
})();
