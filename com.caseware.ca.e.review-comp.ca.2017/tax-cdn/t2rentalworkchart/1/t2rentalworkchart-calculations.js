(function() {

  function runTable600Calcs(calcUtils) {
    var table = calcUtils.field('600');
    var nonCorpPercent = calcUtils.field('8520').get();
    table.getRows().forEach(function(row) {
      if (nonCorpPercent !== 0) {
        row[4].disabled(true);
        row[4].assign(row[2].get() * nonCorpPercent / 100);
      } else {
        row[4].disabled(false); //remove disabled
      }

      row[6].assign(
          row[2].getNumber() -
          row[4].getNumber());

    });
  }

  wpw.tax.create.calcBlocks('t2rentalworkchart', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field, form) {
      var sequence = calcUtils.form().sequence;
      var isPartnerShip = (field('1021').get() == 2);
      var isCoOwner = (field('1021').get() == 3);

      var expenseArray = ['8690', '8710', '8810', '8860', '8871', '8960', '9060', '9180', '9200', '9220', '9281'];

      calcUtils.getGlobalValue('1008', 'cp', 'tax_start');
      calcUtils.getGlobalValue('1009', 'cp', 'tax_end');

      // Percentage restrictions
      field('1024').assign(Math.max(Math.min(field('1024').get(), 100), 0));
      field('1023').assign(Math.max(Math.min(field('1023').get(), 100), 0));

      // Ownership options
      // Partnership
      if (isPartnerShip) {
        calcUtils.removeValue(['1024'], true);
        calcUtils.removeValue(['801'], false);
        field(801).disabled(true);
        field('1024').assign(100);

        field(9945).disabled(true);
        field(9947).disabled(true);
        field(9948).disabled(true);
        field(9947).disabled(true);

        field(800).disabled(false);
        field(1022).disabled(false);
        field(1023).disabled(false);
        field(1025).disabled(false);
        field(9938).disabled(false);
        field(9974).disabled(false);
        field(9943).disabled(false);
      }
      // Co-ownership
      else if (isCoOwner) {
        field(1022).disabled(true);
        field(1025).disabled(true);
        field(9974).disabled(true);
        field(9943).disabled(true);

        field(1024).disabled(false);
        field(9945).disabled(false);
        field(801).disabled(false);
        field(9947).disabled(false);
        field(9948).disabled(false);

        calcUtils.removeValue(['1022', '1023', '9938'], true);
        field('800').assign();
        field('1023').assign(100);
      }
      // Full ownership
      else {
        field(1022).disabled(true);

        field(9947).disabled(false);
        field(9948).disabled(false);

        field(1025).disabled(true);
        field(9974).disabled(true);
        field(9943).disabled(true);
        calcUtils.removeValue(['1024'], true);
        calcUtils.removeValue(['801'], false);
        field(801).disabled(true);
        field(9945).disabled(true);
        field('1024').assign(100);
        calcUtils.removeValue(['1022', '1023', '9938'], true);
        field('800').assign();
        field('1023').assign(100);
      }

      calcUtils.equals('654', '653');
      calcUtils.equals('8231', '654');
      calcUtils.sumBucketValues('8299', ['8141', '8231']);

      // Table 2000 related calculation
      //todo: need to apply percentage filter to check for num8520
      //field('8520').assign(Math.max(Math.min(field('8520').get(), 100), 0));

      calcUtils.subtract('8521-2', '8521', '8521-1');
      if (isPartnerShip) {
        expenseArray.forEach(function(fieldId) {
          field(fieldId).disabled(true);
          field(fieldId + '-1').disabled(true);
        });
      }
      else {
        expenseArray.forEach(function(fieldId) {
          field(fieldId).disabled(false);
          field(fieldId + '-1').disabled(false);
          if ((field('8520').get() > 0)) {
            expenseArray.forEach(function(fieldId) {
              calcUtils.multiply(fieldId + '-1', ['8520', fieldId], [1 / 100]);
              calcUtils.subtract(fieldId + '-2', fieldId, fieldId + '-1');
            });

            runTable600Calcs(calcUtils)
          }
          else {
            expenseArray.forEach(function(fieldId) {
              field(fieldId + '-1').disabled(false);
              calcUtils.subtract(fieldId + '-2', fieldId, fieldId + '-1');
            });

            runTable600Calcs(calcUtils)
          }
        });
      }

      calcUtils.sumBucketValues('601', ['8521-2', '8690-2', '8710-2', '8810-2', '8860-2',
        '8871-2', '8960-2', '9060-2', '9180-2', '9200-2', '9220-2', '9281-2', '9000-2']);
      calcUtils.equals('9368', '601');
      calcUtils.subtract('9369', '8299', '9368', true);

      //todo: situation calculations for 9944
      if (isCoOwner) {
        calcUtils.multiply('9370', ['1024', '9369'], 1 / 100);
        calcUtils.subtract('9944', '9370', '9945', true);
      }
      else {
        calcUtils.equals('9944', '9369');
      }

      //update linked S8w forms value
      var ccaWorkchartLinkedFormId = calcUtils.allRepeatForms('t2s8w').filter(function(form) {
        return form.field('105').get() == '1';
      });

      if (ccaWorkchartLinkedFormId.length == 0) {
        calcUtils.sumBucketValues('9496', ['9944', '9947']);
        calcUtils.subtract('9950', '9496', '9948', true);
        calcUtils.subtract('9937', '9950', '9936', true);
      }
      else {
        var ccaLinkedData = ccaWorkchartLinkedFormId[0].field('998').get() || [];
        var rentalcalcUtilsData = ccaWorkchartLinkedFormId[0].field('999').get() || [];

        rentalcalcUtilsData.forEach(function(rentalData) {
          if (sequence == rentalData.rentalFormSequence) {
            //update recapture from all linked s8w
            field('9947').assign(rentalData.totalRecapture);
            //update terminal loss from all linked s8w
            field('9948').assign(rentalData.totalTerminalLoss);
            //update ccaClaimed from all linked s8w
            field('9936').assign(rentalData.totalCcaClaimed);
          }
        });

        calcUtils.sumBucketValues('9496', ['9944', '9947']);
        calcUtils.subtract('9950', '9496', '9948', true);

        //list all related ccaClass to the real estate property to the summary table
        var fn = function(field105, field106) {
          return (field105 == 1 && field106 == calcUtils.form().sequence);
        };
        calcUtils.filterLinkedTable('t2s8w', '090', ['105', '106'], fn);
        calcUtils.filterLinkedTable('t2s8w', '100', ['105', '106'], fn);
      }

      calcUtils.subtract('9937', '9950', '9936', true, true);
      //For line e

      if (isPartnerShip) {
        calcUtils.equals('9938', '1025');
      }
      else {
        calcUtils.removeValue([9938]);
        calcUtils.multiply('9938', ['9937', '1023'], 1 / 100);
      }

      if (isPartnerShip) {
        field('9946').assign(field('9938').get() + field('9974').get() - field('9943').get());
      }
      else {
        calcUtils.equals('9946', '9937');
      }

    });
  });
})();
