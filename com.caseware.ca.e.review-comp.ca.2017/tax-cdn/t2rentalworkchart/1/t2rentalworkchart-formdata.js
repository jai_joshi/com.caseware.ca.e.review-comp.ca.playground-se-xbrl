(function() {
  'use strict';

  wpw.tax.global.formData.t2rentalworkchart = {
    formInfo: {
      isRepeatForm: true,
      abbreviation: 't2rentalworkchart',
      title: 'Rental Income Workchart per Property',
      headerImage: 'cw',
      repeatFormData:{
        titleNum: '1001'
      },
      neededRepeatForms: ['T2S8W'],
      highlightFieldSets: true,
      category: 'Workcharts'
    },
    'sections': [
      {
        'header': 'Property Profile',
        'rows': [
          {
            'type': 'splitTable',
            'fieldAlignRight': true,
            'side1': [
              {
                'label': '<b>Identification</b>',
                'labelClass': 'fullLength'
              },
              {
                'label': 'Name of property',
                'labelClass': 'tabbed'
              },
              {
                'type': 'infoField',
                'inputType': 'textArea',
                'num': '1001'
              },
              {
                'label': 'Address of the property',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'inputType': 'address',
                'add1Num': '1002',
                'add2Num': '1003',
                'provNum': '1004',
                'cityNum': '1005',
                'countryNum': '1006',
                'postalCodeNum': '1007'
              },
              {
                'type': 'infoField',
                'inputType': 'radio',
                'label': 'Foreign property?',
                'num': '1019',
                'labelWidth': '70%',
                'init': '2'
              },
              {
                'type': 'infoField',
                'inputType': 'radio',
                'label': 'Was this the final year of operation?',
                'num': '1020',
                'labelWidth': '70%',
                'init': '2'
              }
            ],
            'side2': [
              {
                'label': 'To which tax year does this rental income applicable to?',
                'labelClass': 'fullLength bold centerText'
              },
              {
                'type': 'infoField',
                'inputType': 'dateRange',
                'label1': 'Tax year-start',
                'num': '1008',
                'label2': 'Tax year-end',
                'num2': '1009',
                'width2': '40%'
              },
              {
                'labelClass': 'fullLength'
              },
              {
                'label': '<b>Ownership of property</b>',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'indicatorColumn': true,
                'inputType': 'dropdown',
                'label': 'Is this property fully owned by the corporation ?',
                'num': '1021',
                'labelWidth': '65%',
                'init': '1',
                'showValues': 'before',
                'options': [
                  {
                    'value': '1',
                    'option': 'Fully owned'
                  },
                  {
                    'value': '2',
                    'option': 'Partnership'
                  },
                  {
                    'value': '3',
                    'option': 'Co-ownership'
                  }
                ]
              },
              {
                'labelClass': 'fullLength'
              },
              {
                'label': '<b>Partnership</b>',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'indicatorColumn': true,
                'label': 'Name of partnership',
                'num': '1022'
              },
              {
                'label': 'Corporation\'s percentage of partnership of the property',
                'layout': 'alignInput',
                'layoutOptions': {
                  'indicatorColumn': true,
                  'showDots': true,
                  'paddingRight': false
                },
                'columns': [
                  {
                    'input': {
                      'num': '1023',
                      'decimals': wpw.tax.utilities.workchartDecimal
                    }
                  }
                ],
                'indicator': '%'
              },
              {
                'label': '<b>T5013</b> Is there a T5013 slip for this property? <b>If yes,</b> Please enter the Amount of net income in the slip',
                'layout': 'alignInput',
                'layoutOptions': {
                  'indicatorColumn': true,
                  'showDots': true,
                  'paddingRight': false
                },
                'columns': [
                  {
                    'input': {
                      'num': '1025',
                      'decimals': wpw.tax.utilities.workchartDecimal
                    }
                  }
                ],
                'indicator': 'x'
              },
              {
                'labelClass': 'fullLength'
              },
              {
                'label': '<b>Other co-ownership</b>',
                'labelClass': 'fullLength'
              },
              {
                'label': 'Corporation\'s percentage of ownership of the property',
                'layout': 'alignInput',
                'layoutOptions': {
                  'indicatorColumn': true,
                  'showDots': true,
                  'paddingRight': false
                },
                'columns': [
                  {
                    'input': {
                      'decimals': wpw.tax.utilities.workchartDecimal,
                      'num': '1024'
                    }
                  }
                ],
                'indicator': '%'
              }
            ]
          }
        ]
      },
      {
        'header': 'Income',
        'rows': [
          {
            'label': 'Gross rents',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '8141'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '650'
          },
          {
            'label': '<b>Total other income :</b>',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '654'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '8231'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Gross rental income',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '8299'
                }
              }
            ],
            'indicator': 'a'
          }
        ]
      },
      {
        'header': 'Expenses',
        'rows': [
          {
            'label': 'Non-corporation portion percentage :',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '8520',
                  'decimals': 3
                }
              }
            ],
            'indicator': '%'
          },
          {
            'type': 'table',
            'num': '2000'
          },
          {
            'type': 'table',
            'num': '600'
          },
          {
            'label': '<b>Total Deductible Expenses, excluding CCA, Co-owners expenses, etc.</b>',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '601'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '9368'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'b'
          },
          {
            'label': 'Net income (loss) before adjustments (line a <b>minus</b> line b)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '9369'
                }
              }
            ],
            'indicator': 'y'
          },
          {
            'label': 'Co-owners - Corporate\'s share of amount y',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '9370'
                }
              }
            ],
            'indicator': 'c'
          },
          {
            'label': '<b>Minus</b> other expenses of the co-owner, please specify:',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': '801'
                }
              },
              null,
              {
                'input': {
                  'num': '9945'
                }
              }
            ]
          },
          {
            'label': 'Subtotal',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight bold',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '9944'
                }
              }
            ]
          },
          {
            'label': '<b>Plus</b> recaptured capital cost allowance (co-owners - corporate\'s amount)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '9947'
                }
              }
            ]
          },
          {
            'label': 'Subtotal',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight bold',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '9496'
                }
              }
            ]
          },
          {
            'label': '<b>Minus</b> terminal loss (co-owners - corporate\'s amount)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '9948'
                }
              }
            ]
          },
          {
            'label': 'Net income (net loss) before cost allowance',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight bold',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '9950'
                }
              }
            ]
          },
          {
            'label': '<b>Minus</b> capital cost allowance',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '9936'
                }
              }
            ]
          },
          {
            'label': '<b>Net income (loss)</b> -If you are a sole proprietor or a co-owner, this will be the amount z',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '9937'
                }
              }
            ],
            'indicator': 'd'
          },
          {
            'label': '<b>Partnerships</b> - Corporate\'s share of line d above or amount x',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '9938'
                }
              }
            ],
            'indicator': 'e'
          },
          {
            'label': '<b>Plus</b> GST/HST rebate for partners received in the year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '9974'
                }
              }
            ]
          },
          {
            'label': '<b>Minus</b> other expenses of the partner, plese specify:',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': 800
                }
              },
              null,
              {
                'input': {
                  'num': '9943'
                }
              }
            ]
          },
          {
            'label': '<b>Your net income (loss)</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '9946'
                }
              }
            ],
            'indicator': 'z'
          }
        ]
      },
      {
        'header': ' ',
        'hideFieldset': true,
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '090'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '100'
          }
        ]
      },
      {
        'header': 'Area F - Details of land additions and dispositions in the year',
        'rows': [
          {
            'label': 'Cost of all land additions in the year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '9923'
                }
              }
            ]
          },
          {
            'label': 'Proceeds from all land dispositions in the year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '9924'
                }
              }
            ]
          }
        ]
      }
    ]
  };
})();