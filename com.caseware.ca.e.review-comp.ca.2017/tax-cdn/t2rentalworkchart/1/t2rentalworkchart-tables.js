(function () {

  wpw.tax.global.tableCalculations.t2rentalworkchart = {
    "090": {
      "infoTable": false,
      "showNumbering": true,
      // "fixedRows": true,
      // "maxLoop": 100000,
      "columns": [
        {
          "header": "1<br>Class number",
          "tn": "200",
          num: '200',
          "disabled": true,
          colClass: 'small-input-width',
          "linkedFieldId": "101"
        },
        {
          "header": "<br>Description",
          colClass: 'std-input-col-width',
          "disabled": true,
          "linkedFieldId": "103",
          type: 'text'
        },
        {
          "header": "2<br>Undepreciated capital cost at the beginning of the year (amount from column 13 of last year's schedule 8)",
          "tn": "201",
          "disabled": true,
          "linkedFieldId": "201",
          num: '201'
        },
        {
          "header": "3<br>Cost of acquisitions during the year (new property must be available for use)<br>(see note 1 below)",
          "tn": "203",
          "disabled": true,
          "linkedFieldId": "203",
          num: '203'
        },
        {
          "header": "4<br>Adjustments and transfers (show amounts that will reduce the underpreciated capital cost in brackets)<br>(see note 2 below",
          "tn": "205",
          "disabled": true,
          "linkedFieldId": "205",
          num: '205'
        },
        {
          "header": "5<br>Proceeds of dispositions during the year (amount not to exceed the capital cost)",
          "tn": "207",
          "disabled": true,
          "linkedFieldId": "207",
          num: '207'
        },
        {
          "header": "6<br>Undepreciated capital cost (column 2 plus column 3 plus or minus column 4 minus column 5)",
          "disabled": true,
          "linkedFieldId": "208"
        },
        {
          "header": "7<br>50% rule (1/2 of the amount, if any, by which the net cost of acquisitions exceeds column 5)<br>(see note 3 below)",
          "tn": "211",
          "disabled": true,
          "linkedFieldId": "211",
          num: '211'
        }
      ]
    },

    "100": {
      "infoTable": false,
      "showNumbering": true,
      "keepButtonsSpace": true,
      "columns": [
        {
          "header": "1<br>Class number",
          "tn": "200",
          "disabled": true,
          colClass: 'small-input-width',
          "linkedFieldId": "101"
        },
        {
          "header": "<br>Description",
          colClass: 'std-input-col-width',
          "disabled": true,
          "linkedFieldId": "103",
          type: 'text'
        },
        {
          "header": "8<br>Reduced undepreciated capital cost (column 6 minus column 7)",
          "disabled": true,
          "linkedFieldId": "210"
        },
        {
          "header": "9<br>CCA rate % <br>(see note 4 below)",
          "tn": "212",
          colClass: 'small-input-width',
          "disabled": true,
          "linkedFieldId": "212",
          num: '212'
        },
        {
          "header": "10<br>Recapture of capital cost allowance (see note 5 below)",
          "tn": "213",
          "total": true,
          "totalNum": "213",
          "totalMessage": "Totals : ",
          "disabled": true,
          "linkedFieldId": "213",
          num: '213'
        },
        {
          "header": "11<br>Terminal loss",
          "tn": "215",
          "total": true,
          "totalNum": "215",
          "disabled": true,
          "linkedFieldId": "215",
          num: '215'
        },
        {
          "header": "12<br>Capital cost allowance (for declining balance method, column 8 multiplied by column 9, or a lower amount<br>(see note 6 below)",
          "tn": "217",
          "total": true,
          "totalNum": "217",
          "disabled": true,
          "linkedFieldId": "217",
          num: '217'
        },
        {
          "header": "13<br>Undepreciated capital cost at the end of the year (column 6 minus column 12)",
          "tn": "220",
          "disabled": true,
          "linkedFieldId": "220",
          num: '220'
        }
      ],
      "hasTotals": true
    },
    "600": {
      "infoTable": true,
      "maxLoop": 100000,
      "showNumbering": true,
      "width": "calc((100% - 105px))",
      "columns": [
        {
          "header": "<b>Description of other expenses</b>",
          "num": "602",
          "width": "240px",
          type: 'text'
        },
        {
          // "width": "20px",
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignRight',
          "hiddenTotal": true,
          "total": true,
          "totalNum": "9000"
        },
        {
          "width": "20px",
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignRight',
          "hiddenTotal": true,
          "total": true,
          "totalNum": "9000-1"
        },
        {
          "width": "20px",
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignRight',
          "hiddenTotal": true,
          "total": true,
          "totalNum": "9000-2",
          "disabled": true
        }
      ],
      "hasTotals": true
    },
    "650": {
      "infoTable": true,
      "maxLoop": 100000,
      "showNumbering": true,
      "width": "calc((100% - 105px))",
      "columns": [
        {
          "header": "<b>Description of other incomes</b>",
          "num": "651",
          "width": "240px",
          type: 'text'
        },
        {
          "type": "none"
        },
        {
          "header": "<b>Amount</b>",
          "num": "652",
          "total": true,
          "hiddenTotal": true,
          "totalNum": "653",
          colClass: 'std-input-width'
        }],
      "hasTotals": true
    },
    "2000": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none"
        },
        {
          "width": "20px",
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "header": "<b>Total expense</b>",
          cellClass: 'alignRight',
          "hiddenTotal": true,
          "totalNum": "9949"
        },
        {
          "width": "20px",
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "header": "<b>Non-corporate portion</b>",
          cellClass: 'alignRight',
          "hiddenTotal": true,
          "totalNum": "9949-1"
        },
        {
          "width": "20px",
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "header": "<b>Deductible expenses</b>",
          cellClass: 'alignRight',
          "hiddenTotal": true,
          "totalNum": "9949-2",
          "disabled": true
        },
        {
          "width": "156px",
          "type": "none"
        }],
      "cells": [{
        "0": {
          "label": "Advertising"
        },
        "2": {
          "num": "8521"
        },
        "4": {
          "num": "8521-1"
        },
        "6": {
          "num": "8521-2"
        }
      },
        {
          "0": {
            "label": "Insurance"
          },
          "2": {
            "num": "8690"
          },
          "4": {
            "num": "8690-1"
          },
          "6": {
            "num": "8690-2"
          }
        },
        {
          "0": {
            "label": "Interest"
          },
          "2": {
            "num": "8710"
          },
          "4": {
            "num": "8710-1"
          },
          "6": {
            "num": "8710-2"
          }
        },
        {
          "0": {
            "label": "Office Expenses"
          },
          "2": {
            "num": "8810"
          },
          "4": {
            "num": "8810-1"
          },
          "6": {
            "num": "8810-2"
          }
        },
        {
          "0": {
            "label": "Legal, accounting, and other professional fees"
          },
          "2": {
            "num": "8860"
          },
          "4": {
            "num": "8860-1"
          },
          "6": {
            "num": "8860-2"
          }
        },
        {
          "0": {
            "label": "Management and administration fees"
          },
          "2": {
            "num": "8871"
          },
          "4": {
            "num": "8871-1"
          },
          "6": {
            "num": "8871-2"
          }
        },
        {
          "0": {
            "label": "Maintenance and repairs"
          },
          "2": {
            "num": "8960"
          },
          "4": {
            "num": "8960-1"
          },
          "6": {
            "num": "8960-2"
          }
        },
        {
          "0": {
            "label": "Salaries, wages, and benefits (including employer's contributions)"
          },
          "2": {
            "num": "9060"
          },
          "4": {
            "num": "9060-1"
          },
          "6": {
            "num": "9060-2"
          }
        },
        {
          "0": {
            "label": "Property Taxes"
          },
          "2": {
            "num": "9180"
          },
          "4": {
            "num": "9180-1"
          },
          "6": {
            "num": "9180-2"
          }
        },
        {
          "0": {
            "label": "Travel"
          },
          "2": {
            "num": "9200"
          },
          "4": {
            "num": "9200-1"
          },
          "6": {
            "num": "9200-2"
          }
        },
        {
          "0": {
            "label": "Utilities"
          },
          "2": {
            "num": "9220"
          },
          "4": {
            "num": "9220-1"
          },
          "6": {
            "num": "9220-2"
          }
        },
        {
          "0": {
            "label": "Motor vehicle expenses (not including CCA)"
          },
          "2": {
            "num": "9281"
          },
          "4": {
            "num": "9281-1"
          },
          "6": {
            "num": "9281-2"
          }
        }],
      "hasTotals": true
    }
  }
})();
