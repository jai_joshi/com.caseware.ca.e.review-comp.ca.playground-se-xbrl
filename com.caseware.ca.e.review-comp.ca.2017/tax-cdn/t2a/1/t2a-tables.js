(function() {

  var proRateCols = [
    {
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'std-spacing-width',
      "type": "none"
    },
    {
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-padding-width',
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'std-input-col-width',
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'std-spacing-width',
      "type": "none"
    },
    {
      colClass: 'small-input-width'
    },
    {
      colClass: 'std-padding-width',
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'std-padding-width'
    },
    {
      colClass: 'std-spacing-width', type: 'none'
    },
    {
      colClass: 'std-padding-width'
    },
    {
      colClass: 'std-padding-width',
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'std-padding-width'
    },
    {
      colClass: 'std-padding-width',
      cellClass: 'alignCenter', type: 'none'
    },
    {
      colClass: 'std-input-width'
    },
    {
      "type": "none",
      colClass: 'std-padding-width'
    },
    {
      "type": "none",
      colClass: 'std-input-col-width'
    }
  ];

  wpw.tax.global.tableCalculations.t2a = {
    "BusinessNature": {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          header: 'Nature of Business',
          cellClass: 'alignCenter',
          type:'text'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          "header": "SIC Code",
          'type': 'selector',
          colClass: 'std-input-width',
          selectorOptions: {
            title: 'SCI Codes',
            items: wpw.tax.codes.sicCodes
          }
        }
      ],
      cells: [
        {
          '0':{
            num: '028a',
            'disabled': true,
            'cannotOverride': true
          },
          '1': {
            tn: '028'
          },
          '2':{
            num: '028'
          }
        }
      ]
    },
    "ABNumber": {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-col-padding-width',
          format: ['{N9}'],
          type: 'custom',
        },
      ],
      "cells": [
        {
          "1": {
            "tn": "034"
          },
          "2": {
            "num": "034",
            'cannotOverride': true
          },
        }
      ]
    },
    "BNumber": {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-col-padding-width',
          type: 'custom',
        },
      ],
      "cells": [
        {
          "1": {
            "tn": "035"
          },
          "2": {
            "num": "035",
            format: ['{N9}RC{N4}', 'NR'],
            'cannotOverride': true
          },
        }
      ]
    },
    "TaxBegDate": {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-col-width',
          type: 'date',
        },
      ],
      "cells": [
        {
          "1": {
            "tn": "036"
          },
          "2": {
            "num": "036",
            'cannotOverride': true
          },
        }
      ]
    },
    "TaxEndDate": {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-col-width',
          type: 'date',
        },
      ],
      "cells": [
        {
          "1": {
            "tn": "037"
          },
          "2": {
            "num": "037",
            'cannotOverride': true
          },
        }
      ]
    },
    'GR047': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-col-padding-width',
          type: 'number',
        },
      ],
      "cells": [
        {
          "1": {
            "tn": "047"
          },
          "2": {
            "num": "047",
            'disabled': true,
            'cannotOverride': true,
          },
        }
      ]
    },
    'TA048': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-col-padding-width',
          type: 'number',
        },
      ],
      "cells": [
        {
          "1": {
            "tn": "048"
          },
          "2": {
            "num": "048",
            'disabled': true,
            'cannotOverride': true,
          },
        }
      ]
    },
    "AmalDate": {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-col-width',
          type: 'date'
        },
        {
          type: 'none',
          colClass: 'small-input-width'
        },
      ],
      "cells": [
        {
          "1": {
            "tn": "052"
          },
          "2": {
            "num": "052",
          },
        }
      ]
    },
    "DissolutionDate": {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-col-width',
          type: 'date',
        },
        {
          type: 'none',
          colClass: 'small-input-width'
        },
      ],
      "cells": [
        {
          "1": {
            "tn": "053"
          },
          "2": {
            "num": "053",
          },
        }
      ]
    },
    '1010': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none'}
      ],
      'cells': [
        {
          '0': {label: 'I,'},
          '1': {tn: '097'},
          '2': {num: '097', type: 'text', 'cannotOverride': true},
          '4': {tn: '098'},
          '5': {num: '098', type: 'text', 'cannotOverride': true},
          '7': {tn: '099'},
          '8': {num: '099', type: 'text', 'cannotOverride': true}
        },
        {
          '2': {label: 'Print Surname', cellClass: 'alignCenter'},
          '5': {label: 'Print First Name', cellClass: 'alignCenter'},
          '8': {label: 'Position, office or rank', cellClass: 'alignCenter'}
        }
      ]
    },
    '1020': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-input-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-padding-width'}
      ],
      'cells': [
        {
          '3': {type: 'text'},
          '5': {num: '955', type: 'date', 'cannotOverride': true},
        },
        {
          '3': {label: 'Signature of the authorized signing officer of the corporation', cellClass: 'alignCenter'},
          '5': {label: 'Date', cellClass: 'alignCenter'}
        }
      ]
    },
  }
})();
