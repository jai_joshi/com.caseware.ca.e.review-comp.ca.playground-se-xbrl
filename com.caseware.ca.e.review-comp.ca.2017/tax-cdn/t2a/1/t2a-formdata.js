(function() {
  'use strict';
  var foreign_countries = JSON.parse(JSON.stringify(wpw.tax.codes.countryAddressCodes));
  delete foreign_countries.CA;
  var foreignCountries = new wpw.tax.actions.codeToDropdownOptions(foreign_countries, 'value');

  var dateTimeConditionArr = [
    {
      switchIf: {
        formId: 'cp',
        fieldId: '063',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '066',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '071',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '072',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '076',
        value: '1'
      }
    }
  ];

  wpw.tax.global.formData.t2a = {
    'formInfo': {
      'abbreviation': 'AT1',
      'title': 'Alberta Corporate Income Tax Return',
      hideHeaderBox: true,
      schedule: 'AT1',
      printTitle: 'ALBERTA CORPORATE INCOME TAX RETURN - AT1 FOR 2001 AND SUBSEQUENT TAXATION YEARS',
      'headerImage': 'canada-federal',
      agencyUseOnlyBox: {
        tn: '055',
        headerClass: 'alignRight',
        textAbove: ' ',
        pdfPaint: true
      },
      description: [
        {
          'text': 'For corporations with taxation years ending in 2001 and subsequent. If the corporation has '+
          'no taxable income to report both federally and for Alberta purposes, please refer to form '+
          'AT100 to determine if the corporation is exempt from filing. Otherwise, the AT1 and ' +
          'applicable schedules must be received by Tax and Revenue Administration (TRA) within 6 '+
          'months of the corporation\'s taxation year end. Mail, fax, courier or hand deliver to: TAX AND '+
          'REVENUE ADMINISTRATION, 9811 109 ST, EDMONTON AB T5K 2L5. Fax: 780-427-0348. '
        }
      ],
      'descriptionHighlighted': true,
      'highlightFieldsets': true,
      category: 'Alberta Tax Forms',
      formFooterNum: 'AT1 (Jun-15)'

    },
    sections: [
      {
        forceBreakAfter: true,
        'rows': [
          {
            'type': 'splitTable',
            'side1': [
              {
                'label': 'Legal Name of Corporation',
              },
              {
                'type': 'infoField',
                'maxLength': '175',
                'minLength': '1',
                'num': '010',
                'tn': '010',
                'disabled': true,
                'cannotOverride': true,
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '175'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'width': '93%'
              },
              {
                'label': 'Operating Name of Corporation',
              },
              {
                'type': 'infoField',
                'maxLength': '175',
                'minLength': '1',
                'num': '011',
                'tn': '011',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '175'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'width': '93%'
              },
              {
                'label': 'Mailing Address of Business',
              },
              {
                'type': 'infoField',
                'inputType': 'address',
                'add1Num': '012',
                'add2Num': '013',
                'provNum': '015',
                'cityNum': '014',
                'countryNum': '016',
                'postalCodeNum': '017',
                'add1Tn': '012',
                'add2Tn': '013',
                'cityTn': '014',
                'provTn': '015',
                'countryTn': '016',
                'postalTn': '017',
                'cannotOverride': true
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'If the assessment notice and assessment correspondence are to be sent to an address other than that above, provide that address:',
              },
              {
                'label': 'Name',
              },
              {
                'type': 'infoField',
                'maxLength': '175',
                'minLength': '1',
                'num': '018',
                'tn': '018',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '175'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'width': '93%'
              },
              {
                'type': 'infoField',
                'inputType': 'address',
                'add1Num': '019',
                'add2Num': '020',
                'provNum': '022',
                'cityNum': '021',
                'countryNum': '023',
                'postalCodeNum': '024',
                'add1Tn': '019',
                'add2Tn': '020',
                'cityTn': '021',
                'provTn': '022',
                'countryTn': '023',
                'postalTn': '024',
                'cannotOverride': true
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Name of the person to contact to discuss this return',
              },
              {
                'type': 'infoField',
                'maxLength': '175',
                'minLength': '1',
                'num': '025',
                'tn': '025',
                'disabled': true,
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '60'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'width': '93%'
              },
              {
                'type': 'infoField',
                'inputType': 'custom',
                'label': 'Telephone number:',
                'labelCellClass': 'indent-3',
                'layout': 'alignInput',
                'telephoneNumber': true,
                'layoutOptions': {
                  'indicatorColumn': true,
                  'showDots': false,
                  'rightPadding': false
                },
                'columns': [
                  {
                    'padding': {
                      'type': 'tn',
                      'data': '026'
                    },
                    'input': {
                      'type': 'custom',
                      'format': ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
                      'num': '026',
                      'validate': {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]},
                    }
                  }
                ]
              },
              {
                'type': 'infoField',
                'inputType': 'custom',
                'label': 'Fax number:',
                'labelCellClass': 'indent-3',
                'layout': 'alignInput',
                'layoutOptions': {
                  'indicatorColumn': true,
                  'showDots': false,
                  'rightPadding': false
                },
                'columns': [
                  {
                    'padding': {
                      'type': 'tn',
                      'data': '027'
                    },
                    'input': {
                      'num': '027',
                      type: 'custom',
                      'format': ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
                      'validate': {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
                    }
                  }
                ]
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'table',
                'num': 'BusinessNature'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'splitInputs',
                'inputType': 'radio',
                'label': 'Type of Corporation',
                'divisions': 1,
                'disabled': true,
                'cannotOverride': true,
                'num': '029',
                'tn': '029',
                'other': {
                  'label': '5. Other, specify:',
                  'value': '5',
                  'alwaysShow': true,
                  'valueNum': '1067',
                  'disabled': true,
                  'cannotOverride': true
                },
                'items': [
                  {
                    'label': 'Canadian-controlled private corporation throughout the year (excluding Alberta professional)',
                    'value': '1'
                  },
                  {
                    'label': 'Alberta Professional',
                    'value': '2'
                  },
                  {
                    'label': 'Other private',
                    'value': '3'
                  },
                  {
                    'label': 'Public',
                    'value': '4'
                  }
                ]
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'splitInputs',
                'inputType': 'radio',
                'label': 'Special Corporation Status (if applicable)',
                'divisions': 2,
                'num': '030',
                'disabled': true,
                'tn': '030',
                'items': [
                  {
                    'label': 'Investment Corporation',
                    'value': '1'
                  },
                  {
                    'label': 'Mutual Fund Corporation',
                    'value': '2'
                  },
                  {
                    'label': 'Co-operative',
                    'value': '3'
                  },
                  {
                    'label': 'Credit Union',
                    'value': '4'
                  },
                  {
                    'label': 'Corporations exempt under the federal ITA section 149',
                    'value': '5'
                  }
                ]
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'Has there been a wind-up of a subsidiary under federal Income Tax Act (ITA) section 88 during the current taxation year?',
                'labelWidth': '70%',
                'num': '031',
                'tn': '031',
                'inputType': 'radio',
                'disabled': true, cannotOverride: true,
                'layoutOptions': {
                  'showDots': false
                },
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'Is this the first year of filing after an amalgamation?',
                'labelWidth': '70%',
                'num': '032',
                'tn': '032',
                'inputType': 'radio',
                'disabled': true,
                cannotOverride: true,
                'layoutOptions': {
                  'showDots': false
                },
              },
            ],
            'side2': [
              {
                'label': 'Alberta Corporate Account Number (CAN)',
              },
              {
                'label': '(Enter the 9 or 10 digit account number)',
                'labelClass': 'font-small',
              },
              {
                'type': 'table',
                'num': 'ABNumber'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Federal Business Number (BN)',
              },
              {
                'type': 'table',
                'num': 'BNumber'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Taxation Year Beginning',
              },
              {
                'type': 'table',
                'num': 'TaxBegDate'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Taxation Year Ending',
              },
              {
                'type': 'table',
                'num': 'TaxEndDate'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'Has the taxation year end changed since the last return was filed?',
                'labelWidth': '70%',
                'num': '038',
                'tn': '038',
                'inputType': 'radio',
                'disabled': true, cannotOverride: true,
                'layoutOptions': {
                  'showDots': false
                },
              },
              {
                'type': 'splitInputs',
                'inputType': 'radio',
                'label': 'If "Yes", specify the reason:',
                'divisions': 1,
                'disabled': true,
                'cannotOverride': true,
                'num': '039',
                'tn': '039',
                'items': [
                  {
                    'label': 'Canada Revenue Agency (CRA) approved tax year end change',
                    'value': '1'
                  },
                  {
                    'label': 'Change in control',
                    'value': '2'
                  },
                  {
                    'label': 'Final return',
                    'value': '3'
                  }
                ]
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'splitInputs',
                'inputType': 'radio',
                'label': 'State the functional currency used, if other than Canadian:',
                'divisions': 2,
                'disabled': true,
                'cannotOverride': true,
                'num': '041',
                'tn': '041',
                'items': [
                  {
                    'label': 'United States of America',
                    'value': '1'
                  },
                  {
                    'label': 'United Kingdom',
                    'value': '2'
                  },
                  {
                    'label': 'European Monetary Union',
                    'value': '3'
                  },
                  {
                    'label': 'Australia',
                    'value': '4'
                  }
                ]
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'If field 041 is checked, provide average exchange rate for calculation: (functional currency converting to Canadian currency)',
                'layout': 'alignInput',
                'layoutOptions': {
                  'indicatorColumn': true,
                  'showDots': false,
                  'rightPadding': false
                },
                'columns': [
                  {
                    'input': {
                      'num': '043',
                      'disabled': true,
                      'cannotOverride': true,
                    },
                    'padding': {
                      'type': 'tn',
                      'data': '043',
                    }
                  }
                ]
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Gross Revenue (To nearest thousand)',
              },
              {
                'type': 'table',
                'num': 'GR047'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Total Assets (Book value per balance sheet, to nearest thousand)\n',
              },
              {
                'type': 'table',
                'num': 'TA048'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'Is this a final return?',
                'labelWidth': '70%',
                'num': '050',
                'tn': '050',
                'inputType': 'radio',
                'disabled': true,
                'layoutOptions': {
                  'showDots': false
                },
              },
              {
                'label': 'If "Yes", specify the reason:',
              },
              {
                'type': 'infoField',
                'inputType': 'dropdown',
                'num': '1001',
                showWhen: {fieldId: '050', compare: {is: '1'}},
                inputClass: 'std-input-col-width-2',
                'indicatorColumn': true,
                'options': [
                  {
                    'option': ' ',
                    'value': '0'
                  },
                  {
                    'option': '1. Amalgamation, specify date of amalgamation: ',
                    'value': '1'
                  },
                  {
                    'option': '2. Discontinuance of permanent establishment in Alberta',
                    'value': '2'
                  },
                  {
                    'option': '3. Bankruptcy',
                    'value': '3'
                  },
                  {
                    'option': '4. Wind-up into parent',
                    'value': '4'
                  },
                  {
                    'option': '5. Dissolution of corporation, specify date operations ceased:',
                    'value': '5'
                  }
                ]
              },
              {
                type:'table',
                num:'AmalDate',
                showWhen: {
                  and: [
                    {fieldId: '050', compare: {is: '1'}},
                    {fieldId: '1001', compare: {is: '1'}}
                  ]
                },
              },
              {
                type:'table',
                num:'DissolutionDate',
                showWhen: {
                  and: [
                    {fieldId: '050', compare: {is: '1'}},
                    {fieldId: '1001', compare: {is: '5'}}
                  ]
                },
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'Was there a transfer of property under federal ITA subsection 85(1), 85(2) or 97(2) that occurred after May 30, 2001, and during the taxation year being reported?',
                'labelWidth': '70%',
                'num': '054',
                'tn': '054',
                'inputType': 'radio',
                'layoutOptions': {
                  'showDots': false
                },
              }
            ]
          },
        ]
      },
      /////
      {
        forceBreakAfter: true,
        'rows': [
          {
            'label': '<b>Taxable Income:</b> The calculation of taxable income for federal purposes can differ from the calculation for '+
            'Alberta purposes if the corporation chooses to use different discretionary deduction amounts (e.g., different '+
            'application of losses, CCA, charitable donation, etc.).',
          },
          {
            'type': 'infoField',
            'label': 'Is the corporation reporting different taxable income for Alberta and federal purposes?',
            'labelClass': 'indent',
            'num': '060',
            'inputType': 'radio',
            'width': '30%',
            'tn': '060',
            'init': '2',
          },
          {
            'type': 'infoField',
            'label': 'Has the corporation elected to use any different discretionary amounts for the current year claim or do opening balances differ for federal and Alberta purposes?',
            'labelClass': 'indent',
            'num': '061',
            'inputType': 'radio',
            'width': '30%',
            'tn': '061',
            'init': '2',
            'cannotOverride': true
          },
          {
            'label': 'If line 060 and/or 061 is "Yes", then schedule 12 and supporting schedules MUST be completed to reconcile federal and Alberta taxable income.',
            'labelClass': 'font-small bold indent',
          },
          {
            'label': 'Alberta taxable income or (loss)',
            'labelClass': 'bold',
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'If both lines 060 and 061 are "No", then line 062 must equal federal T2, lines 360 - 370 '+
            'OR, if reporting a loss, enter the amount from federal Schedule 4 lines 110 + 310 '+
            'If either line 060 or 061 is "Yes", enter the amount from Schedule 12, lines 090 - 092',
            'labelClass': 'bold',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '062'
                },
                'input': {
                  'num': '062',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ]
          },
          {
            'label': '(If line 062 is negative, complete Schedule 10 to request a loss carry-back, if applicable)',
            'labelClass': 'font-small bold',
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Deduct: Royalty Tax Deduction (Schedule 5, line 021)',
            'labelClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '064'
                },
                'input': {
                  'num': '064',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Alberta Allocation Factor (Schedule 2, column I)',
            'labelClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '065'
                },
                'input': {
                  'num': '065',
                  decimals:6,
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Amount Taxable in Alberta (line 062 - line 064) X line 065 * (if negative, enter "0")<br>' +
            '(* if the corporation has permanent establishments only in Alberta, multiply by "1")',
            'labelClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '066'
                },
                'input': {
                  'num': '066',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Basic Alberta Tax Payable',
            'labelClass': 'bold',
            'columns': [
              {
                'padding': {
                  'data': '067',
                },
                'underline': 'double',
                'input': {
                  'num': '067',
                  'disabled': true,
                  decimals: 4
                }
              }
            ],
            indicator: '%',
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Total (line 066 * Basic Alberta tax rate)',
            'labelClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '068'
                },
                'input': {
                  'num': '068',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ]
          },



          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Alberta Small Business Deduction <br> Schedule 1, line 031',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '070'
                },
                'input': {
                  'num': '070',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Alberta Manufacturing and Processing Profits <br> Deduction Schedule 11, line 023',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '071'
                },
                'input': {
                  'num': '071',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Alberta Foreign Investment Income Tax Credit <br> Schedule 4, line 020',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '072'
                },
                'input': {
                  'num': '072',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Alberta Political Contributions Tax Credit <br>Schedule 8, line 030',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '074'
                },
                'input': {
                  'num': '074',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Alberta Other Tax Deductions and Credits <br>Schedule 3, line 604',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '76a'
                },
                'input': {
                  'num': '76a',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true
                }
              },
              null
            ], cannotOverride: true
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Other Deductions: (specify and attach the appropriate schedules)',
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'type': 'text',
                  'num': '76bdesc',
                }
              },
              null,
              {
                'padding': {
                  'type': 'tn',
                  'data': '76b'
                },
                'input': {
                  'num': '76b',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false
            },
            'label': 'Total (lines 76a + 76b)',
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '076'
                },
                'input': {
                  'num': '076',
                  'disabled': true
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false
            },
            'label': '<b>Total</b> (lines 070 + 071 + 072 + 074 + 076)',
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '079'
                },
                'input': {
                  'disabled': true,
                  'num': '079', cannotOverride: true
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': '<b>Alberta Tax Payable</b> (lines 068 - line 079)',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '080'
                },
                'underline': 'double',
                'input': {
                  'num': '080',
                  'disabled': true
                }
              }
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Alberta Scientific Research & Experimental Development Tax Credit, Schedule 9, line 120',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '081'
                },
                'input': {
                  'num': '081',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true
                }
              },
              null
            ]
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Instalments, other payments and ARTC instalments credited to income tax account for this taxation year (see Guide)',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '082'
                },
                'input': {
                  'num': '082',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                }
              },
              null
            ],
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Alberta Royalty Tax Credit<br>Schedule 6, line 010',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '083'
                },
                'input': {
                  'num': '083',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                }
              },
              null
            ],
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Alberta Royalty Tax Credit Instalments claimed/processed <br>Schedule 6, line 012',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '084'
                },
                'input': {
                  'num': '084',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'disabled': true
                }
              },
              null
            ],
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Alberta Capital Gains Refund (available only to mutual fund corporations and public investment corporations, see Guide)',
            'labelCellClass': 'indent',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '086'
                },
                'input': {
                  'num': '086',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              null
            ],
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            'label': 'Other Credits: (specify and attach the appropriate schedule(s))',
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'type': 'text',
                  'num': '087a',
                }
              },
              null,
              {
                'padding': {
                  'type': 'tn',
                  'data': '087'
                },
                'input': {
                  'num': '087',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                }
              },
              null
            ],
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false
            },
            'label': '<b>Total</b> (lines 081 + 082 + 083 - 084 + 086 + 087)',
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '088'
                },
                'input': {
                  'disabled': true,
                  'num': '088', cannotOverride: true
                }
              }
            ]
          },
          {
            label:'<b>Balance Unpaid (Overpayment)</b> (line 080 - line 088) '
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            label: '(An assessed balance, including interest and penalty charges, of less than $20.00 ' +
            'will be neither charged nor refunded. See Guide.)',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '090'
                },
                'input': {
                  'num': '090',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
            ], cannotOverride: true
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true
            },
            label: '<b>If line 090 is a balance due</b> (i.e. positive amount), indicate the amount enclosed with '+
            'the return. <b>Make cheque payable to Government of Alberta</b>',
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '091'
                },
                'input': {
                  'num': '091',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
            ], cannotOverride: true
          },
          {
            'type': 'infoField',
            label: '<b>If line 090 is an overpayment</b> (i.e. negative amount), indicate the desired disposition: <br>'+
            'Refund = 1; Apply to payments for the next taxation year = 2',
            'inputType': 'dropdown',
            'num': '092',
            'tn': '092',
            'labelWidth': '60px',
            'init': '0',
            'options': [
              {
                'option': ' ',
                'value': '0'
              },
              {
                'option': '1. Refund',
                'value': '1'
              },
              {
                'option': '2. Apply to payments for the next taxation year',
                'value': '2'
              }
            ]
          },
          {
            'label': 'If you would like your Notice of Assessment provided to you by fax rather than by mail, please specify your complete fax number',
            'layout': 'alignInput',
            'type': 'infoField',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'padding': {
                  'type': 'tn',
                  'data': '093'
                },
                'input': {
                  'num': '093',
                  'type': 'custom',
                  validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]},
                  'format': ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}']
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Certification',
        'rows': [
          {
            'type': 'table',
            'num': '1010'
          },
          {
            'label': 'am an authorized signing officer of the corporation. I certify that this return, including accompanying schedules and statements, has been examined by me ' +
            'and is a true, correct and complete return. I further certify that the method of computing income for this taxation year is consistent with that of the previous ' +
            'taxation year except as specifically disclosed in a statement to this return.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1020'
          }
        ]
      }
    ]
  };
})();
