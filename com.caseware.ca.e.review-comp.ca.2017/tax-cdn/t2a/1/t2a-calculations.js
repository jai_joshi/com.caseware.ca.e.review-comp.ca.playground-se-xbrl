(function () {
  function checking(calcUtils, flagged, schedules, tns) {
    var allForms = wpw.tax.create.retrieveAll('formData');

  }

  function sumField(field, fieldArr) {
    var total = 0;
    for (var i in fieldArr) {
      total += field(fieldArr[i]).get();
    }
    return total;
  }

  wpw.tax.create.calcBlocks('t2a', function (calcUtils) {

    calcUtils.calc(function (calcUtils, field, form) {
      calcUtils.getGlobalValue('010', 'cp', '002');

      calcUtils.getGlobalValue('012', 'cp', '022');
      calcUtils.getGlobalValue('013', 'cp', '023');
      calcUtils.getGlobalValue('014', 'cp', '025');
      calcUtils.getGlobalValue('015', 'cp', '026');
      calcUtils.getGlobalValue('016', 'cp', '027');
      calcUtils.getGlobalValue('017', 'cp', '028');
      field('025').assign(field('CP.951').get() + ' ' + field('CP.950').get());
      field('025').source(field('CP.950'));
      calcUtils.getGlobalValue('026', 'cp', '956');
      calcUtils.getGlobalValue('027', 'cp', '639');
      var sciCodes = wpw.tax.codes.sicCodes;
      var sciField = field('028').get();
      for (var category in sciCodes) {
        if (sciCodes[category][sciField]) {
          field('028a').assign(sciCodes[category][sciField].replace(/([0-9]*)\s\-/, ""));
        }
      }
      field('029').assign(field('CP.Corp_Type').get()); //Type of Corporation
      field('029').source(field('CP.Corp_Type'));
      // set field 030 to specific corporation
      field('030').assign(0);
      if (field('CP.122').get() == '1') {
        field('030').assign('4');
      }
      if (['10', '11'].indexOf(field('CP.122').get()) > 0) {
        field('030').assign('1');
      }
      if (['7', '12'].indexOf(field('CP.122').get()) > 0) {
        field('030').assign('2');
      }
      if (field('CP.122').get() == '6') {
        field('030').assign('3');
      }
      if (field('CP.2270').get() == '1') {
        field('030').assign('5');
      }


      calcUtils.getGlobalValue('031', 'cp', '072'); //Has there been a wind-up of a subsidiary under section 88 during the current
      calcUtils.getGlobalValue('032', 'cp', '071'); //first year after ammalgamation
      calcUtils.getGlobalValue('034', 'cp', '135'); //Alberta Corporate Account Number (CAN)
      calcUtils.getGlobalValue('035', 'cp', 'bn'); //BN
      calcUtils.getGlobalValue('036', 'cp', 'tax_start'); //Tax year
      calcUtils.getGlobalValue('037', 'cp', 'tax_end');

      calcUtils.getGlobalValue('038', 'cp', '140');// Tax year change
      if (field('038').get() == 1) {
        calcUtils.getGlobalValue('039', 'cp', '141');
      } else {
        field('039').assign(0);
        field('039').source(field('CP.140'));
      }

      var currency = field('cp.079').get(); // currency
      switch (currency) {
        case 'USD':
          field('041').assign(1);
          break;
        case 'GBP':
          field('041').assign(2);
          break;
        case 'EUR':
          field('041').assign(3);
          break;
        case 'AUD':
          field('041').assign(4);
          break;
        default:
          field('041').assign(0);
      }

      field('041').source(field('cp.079'));
      if (field('041').get() != 0) {
        field('043').assign(field('191').get());
      } else {
        field('043').assign(0);
      }
      field('043').source(field('cp.191'));

      field('047').assign(calcUtils.sumRepeatGifiValue('T2S125', null, '8299', 0));//Gross Revenue
      field('047').source(field('T2S125.8299'));
      field('048').assign(field('T2S100.2599').get()[0]);//Total Asset
      field('048').source(field('T2S100.2599'));
      //Is this the final year?
      field('050').assign(2);
      if (field('cp.076').get() == 1 || field('cp.078').get() == 1) {
        field('050').assign(1);
        if (field('cp.076').get() == 1) {
          field('1001').assign('1');
          calcUtils.getGlobalValue('052', 'cp', '109');
        }
        if (field('cp.078').get() == 1) {
          field('1001').assign('5');
          calcUtils.getGlobalValue('053', 'cp', '111');
        }
      }
      field('053').disabled(true);
      field('052').disabled(true);

      if (field('050').get() == '2') {
        field('1001').assign('0');
      }
      field('1001').disabled(false);

    });
    calcUtils.calc(function (calcUtils, field) {
      ///SECOND BOX CALC
      var atFlagger = {};
      if (wpw.tax.flaggers.at1.flaggedForms) {
        atFlagger = wpw.tax.flaggers.at1.flaggedForms;
      }
      calcUtils.getGlobalValue('060', 't2a12', '100');
      if (!field('060').get()) {
        field('060').assign(2);
      }
      if (field('060').get() == 1 || field('061').get() == 1) {
        field('062').assign(field('t2a12.090').get() - field('t2a12.092').get());
        field('062').source(field('t2s12.090'));
      } else {
        if ((field('t2s4.110').get() + field('t2s4.310').get()) * (-1) == 0) {
          field('062').assign(field('t2j.360').get() - field('t2j.370').get())
          field('062').source(field('t2j.360'));
        } else {
          field('062').assign((field('t2s4.110').get() + field('t2s4.310').get()) * (-1))
          field('062').source(field('t2s4.110'));
        }
      }
      if ('T2A5' in atFlagger) {
        calcUtils.getGlobalValue('064', 't2a5', '021');
      } else {
        field('064').assign(0);
      }
      if (field('cp.750').get() == 'AB') {
        field('065').assign(1.000);
      } else {
        var AreaBSum = 0;
        for (var i = 0; i < field('t2a2.2000').size().rows; i++) {
          AreaBSum += field('t2a2.2000').cell(i, 5).get();
        }
        field('065').assign(field('t2a2.009').get() + AreaBSum + field('t2a2.109').get())
      }
      field('066').assign((field('062').get() - field('064').get()) * field('065').get())

      //== Calculation of Alberta tax rate
      var startDate = wpw.tax.field('cp.tax_start').get();
      var endDate = wpw.tax.field('cp.tax_end').get();
      var Sdate = new Date(startDate.year, startDate.month - 1, startDate.day);
      var numberOfDays = 0;
      var rate = 0;
      var FiscalLength = Math.round((new Date(endDate.year, endDate.month - 1, endDate.day) - Sdate) / 86400000 + 1);
      var range1 = new Date(2015, 5, 30);
      if (Sdate <= range1) {
        numberOfDays += Math.round((range1 - Sdate) / 86400000 + 1);
        rate = numberOfDays / FiscalLength * field('ratesAb.025').get();
      }
      numberOfDays = FiscalLength - numberOfDays;
      rate += numberOfDays / FiscalLength * field('ratesAb.026').get();
      field('067').assign(rate);
      //
      if (field('030').get() == 5) {
        field('068').assign(0);
      } else {
        field('068').assign(field('066').get() * field('067').get() / 100);
      }
      if ('T2A1' in atFlagger) {
        calcUtils.getGlobalValue('070', 't2a1', '031');
      } else {
        field('070').assign(0);
      }
      if ('T2A11' in atFlagger) {
        calcUtils.getGlobalValue('071', 't2a11', '023');
      } else {
        field('071').assign(0);
      }
      if ('T2A4' in atFlagger) {
        calcUtils.getGlobalValue('072', 't2a4', '020');
      } else {
        field('072').assign(0);
      }
      if ('T2A8' in atFlagger) {
        calcUtils.getGlobalValue('074', 't2a8', '030');
      } else {
        field('074').assign(0);
      }
      calcUtils.getGlobalValue('76a', 't2a3', '604');
      field('076').assign(field('76a').get() + field('76b').get());
      field('079').assign(sumField(field, ['070', '071', '072', '074', '076']));
      field('080').assign(field('068').get() - field('079').get());
      if ('T2A9' in atFlagger) {
        calcUtils.getGlobalValue('081', 't2a9', '120');
      } else {
        field('081').assign(0);
      }
      field('088').assign(sumField(field, ['081', '082', '083', '086', '087']) - field('084').get());
      field('090').assign(field('080').get() - field('088').get());
      field('091').assign(Math.max(field('090').get(), 0));

      //todo need to ask Alberta Notice of Assessment question to alberta CP to calculate field 093
      calcUtils.getGlobalValue('097', 'cp', '950');
      calcUtils.getGlobalValue('098', 'cp', '951');
      calcUtils.getGlobalValue('099', 'cp', '954');
      calcUtils.getGlobalValue('955', 'cp', '955');
    });


  });
})
();