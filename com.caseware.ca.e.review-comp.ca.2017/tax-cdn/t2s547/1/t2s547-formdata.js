(function () {
  'use strict';

  wpw.tax.global.formData.t2s547 = {
    'formInfo': {
      'abbreviation': 't2s547',
      isRepeatForm: true,
      repeatFormData: {
        titleNum: '700'
      },
      'title': 'Director/Officer Information',

      //'subTitle': '(2009 and later tax years)',
      'schedule': 'Schedule 547',
      //'code': 'Code 0902',
      'headerImage': 'canada-federal',
      'showCorpInfo': true,
      'description': [],
      category: 'Ontario Forms'
    },
    'sections': [
      {
        'header': 'Part 7 - Director/Officer Information',
        'rows': [
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'items': [
                  '<b>Director</b>: If the individual named in this part is a director (or must be reported ceased' +
                  ' as a director), complete lines 700 to 797',
                  '<b>Officer</b>: If the individual named in this part is one of the corporation\'s five most' +
                  ' senior officers (or must be reported ceased in an officer position), complete lines 700 to 790 ' +
                  'and the applicable lines from 801 to 912.',
                  '<b>Director and officer</b>: If the individual named in this part is a director and one of ' +
                  'the corporation\'s five most senior officers (or must be reported ceased in these position(s)),' +
                  ' complete lines 700 to 797 and the applicable lines from 801 to 912.',
                  'The corporation is required to show information on the MGS public record for all its directors and ' +
                  'a maximum of five of its most senior officers. If the MGS public record shows more than five' +
                  ' officer positions, report cease dates for all except the corporation\'s five most senior officer ' +
                  'positions.',
                  'To report changes to the name of a director/officer, or changes to both the address and the date ' +
                  'elected/appointed of a director/officer, enter the director/officer information exactly as shown' +
                  ' incorrectly on the public record, with a cease date, and then photocopy and complete only Part 7 ' +
                  'with the correct director/officer information'
                ]
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Please photocopy this page and complete Part 7 only for each additional individual for whom director/officer information changes are being reported.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Full name and address for service</b> (P.O. box not acceptable as stand-alone address). The name entered in lines 700 to 710 must be exactly as shown on the MGS public record.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '102'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '715'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '745'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '759'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Director',
            'labelClass': 'bold fullLength'
          },
          {
            'type': 'table',
            'num': '792'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Officer information',
            'labelClass': 'bold fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '101'
          }
        ]
      },
      {
        'hideFieldset': true,
        'rows': [
          {
            'label': 'Once you have completed this page, complete the certification in Part 4 of schedule 546',
            'labelClass': 'fullLength bold center'
          }
        ]
      }
    ]
  };
})();
