(function() {

  var usaStates = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.usaStates, 'EN');
  var canadaProvinces = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.canadaProvinces, 'EN');
  var countries = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.countryAddressCodes, 'value');

  var tnWidth = '26px';
  var dateWidth = '145px';

  wpw.tax.global.tableCalculations.t2s547 = {
    "102": {
      'type': 'table', 'num': '102', 'infoTable': true, 'fixedRows': true, 'dividers': [1, 2],
      'columns': [
        {cellClass: 'alignCenter', maxLength: '35'},
        {cellClass: 'alignCenter', maxLength: '20'},
        {cellClass: 'alignCenter', maxLength: '20'}
      ],
      'cells': [
        {
          '0': {'num': '700', "validate": {"or":[{"length":{"min":"1","max":"35"}},{"check":"isEmpty"}]}, 'label': 'Last Name', 'tn': '700', type: 'text'},
          '1': {'num': '705', "validate": {"or":[{"length":{"min":"1","max":"20"}},{"check":"isEmpty"}]}, 'label': 'First Name', 'tn': '705', type: 'text'},
          '2': {'num': '710', "validate": {"or":[{"length":{"min":"1","max":"20"}},{"check":"isEmpty"}]}, 'label': 'Middle Name(s)', 'tn': '710', type: 'text'}
        }
      ]
    },
    "715": {
      'type': 'table', 'num': '715', 'infoTable': true, 'fixedRows': true, 'dividers': [1, 2],
      'columns': [
        {cellClass: 'alignCenter', maxLength: '8'},
        {'width': '75%', cellClass: 'alignCenter', maxLength: '29'},
        {cellClass: 'alignCenter', maxLength: '10'}
      ],
      'cells': [
        {
          '0': {'num': '720', "validate": {"or":[{"length":{"min":"1","max":"8"}},{"check":"isEmpty"}]}, 'label': 'Street number', 'tn': '720', type: 'text'},
          '1': {
            'num': '730', "validate": {"or":[{"length":{"min":"1","max":"29"}},{"check":"isEmpty"}]}, 'label': 'Street name/Rural route/Lot and Concession number',
            'tn': '730', type: 'text'
          },
          '2': {'num': '740', "validate": {"or":[{"length":{"min":"1","max":"10"}},{"check":"isEmpty"}]}, 'label': 'Suite number', 'tn': '740', type: 'text'}
        }
      ]
    },
    "745": {
      'type': 'table', 'infoTable': true, 'fixedRows': true, 'num': '745',
      'columns': [
        {cellClass: 'alignCenter', maxLength: '42'}
      ],
      'cells': [
        {
          '0': {
            'label': 'Additional address information if applicable (line 730 must be completed first)',
            'num': '750', "validate": {"or":[{"length":{"min":"1","max":"42"}},{"check":"isEmpty"}]},
            'tn': '750',
            maxLength: '42',
            type: 'text'
          }
        }
      ]
    },
    "759": {
      'type': 'table', 'num': '759', 'infoTable': true, 'fixedRows': true, 'dividers': [1, 2, 3],
      'columns': [
        {cellClass: 'alignCenter', maxLength: '30'},
        {
          cellClass: 'alignCenter', isProvinceField: {countryNum: '780'}
        },
        {
          cellClass: 'alignCenter', type: 'dropdown', options: countries, init: 'CA',
          isCountry: {provinceNum: '770', postalNum: '790'}
        },
        {
          'width': '13%', cellClass: 'alignCenter', maxLength: '10',
          isPostalField: {countryNum: '780'}
        }
      ],
      'cells': [
        {
          '0': {
            'num': '760', "validate": {"or":[{"length":{"min":"1","max":"30"}},{"check":"isEmpty"}]},
            'label': 'Municipality (e.g. city, town)',
            'tn': '760',
            type: 'text',
            maxLength: '30'
          },
          '1': {'num': '770', "validate": {"or":[{"length":{"min":"2","max":"2"}},{"check":"isEmpty"}]}, label: 'Province', 'tn': '770', type: 'text'},
          '2': {'num': '780', "validate": {"or":[{"length":{"min":"2","max":"2"}},{"check":"isEmpty"}]}, 'label': 'Country', 'tn': '780'},
          '3': {'num': '790', "validate": {"or":[{"length":{"min":"1","max":"10"}},{"check":"isEmpty"}]}, 'label': 'Postal code', 'tn': '790', type: 'text'}
        }
      ]
    },
    "792": {
      'type': 'table', 'infoTable': true, 'fixedRows': true, 'num': '792',
      'columns': [
        {'type': 'radio', 'width': '50%'},
        {'type': 'date', cellClass: 'alignCenter', 'header': 'Date elected/appointed'},
        {'type': 'date', cellClass: 'alignCenter', 'header': 'Date ceased, if applicable'}
      ],
      'cells': [
        {
          '0': {
            'label': 'Is this director a resident Canadian?' + '<br>' +
            ' (applies to directors of corporations with share capital only)',
            'num': '795',
            'tn': '795'
          }, '1': {'num': '796', 'tn': '796'}, '2': {'num': '797', 'tn': '797'}
        }
      ]
    },
    "101": {
      type: 'table', num: '101', infoTable: true, fixedRows: true,
      columns: [
        { type: 'none'},
        { type: 'none', colClass: 'std-padding-width'},
        {header: 'Date appointed', type: 'date', colClass: 'std-input-width'},
        { type: 'none', colClass: 'std-padding-width'},
        { type: 'none', colClass: 'std-padding-width'},
        {header: 'Date Ceased, if applicable', type: 'date', colClass: 'std-input-width'},
        { type: 'none', width: '315px'}
      ],
      cells: [
        {
          0: {label: 'President'},
          1: {tn: '801'},
          2: {num: '801'},
          3: {},
          4: {tn: '802'},
          5: {num: '802'}
        },
        {
          0: {label: 'Secretary'},
          1: {tn: '806'},
          2: {num: '806'},
          3: {},
          4: {tn: '807'},
          5: {num: '807'}
        },
        {
          0: {label: 'Treasurer'},
          1: {tn: '811'},
          2: {num: '811'},
          3: {},
          4: {tn: '812'},
          5: {num: '812'}
        },
        {
          0: {label: 'General Manager'},
          1: {tn: '816'},
          2: {num: '816'},
          3: {},
          4: {tn: '817'},
          5: {num: '817'}
        },
        {
          0: {label: 'Chair'},
          1: {tn: '821'},
          2: {num: '821'},
          3: {},
          4: {tn: '822'},
          5: {num: '822'}
        },
        {
          0: {label: 'Chairperson'},
          1: {tn: '826'},
          2: {num: '826'},
          3: {},
          4: {tn: '827'},
          5: {num: '827'}
        },
        {
          0: {label: 'Chairman'},
          1: {tn: '831'},
          2: {num: '831'},
          3: {},
          4: {tn: '832'},
          5: {num: '832'}
        },
        {
          0: {label: 'Chairwoman'},
          1: {tn: '836'},
          2: {num: '836'},
          3: {},
          4: {tn: '837'},
          5: {num: '837'}
        },
        {
          0: {label: 'Vice-Chair'},
          1: {tn: '841'},
          2: {num: '841'},
          3: {},
          4: {tn: '842'},
          5: {num: '842'}
        },
        {
          0: {label: 'Vice-President'},
          1: {tn: '846'},
          2: {num: '846'},
          3: {},
          4: {tn: '847'},
          5: {num: '847'}
        },
        {
          0: {label: 'Assistant Secretary'},
          1: {tn: '851'},
          2: {num: '851'},
          3: {},
          4: {tn: '852'},
          5: {num: '852'}
        },
        {
          0: {label: 'Assistant Treasurer'},
          1: {tn: '856'},
          2: {num: '856'},
          3: {},
          4: {tn: '857'},
          5: {num: '857'}
        },
        {
          0: {label: 'Chief Manager'},
          1: {tn: '861'},
          2: {num: '861'},
          4: {tn: '862'},
          5: {num: '862'}
        },
        {
          0: {label: 'Executive Director'},
          1: {tn: '866'},
          2: {num: '866'},
          4: {tn: '867'},
          5: {num: '867'}
        },
        {
          0: {label: 'Managing Director'},
          1: {tn: '871'},
          2: {num: '871'},
          4: {tn: '872'},
          5: {num: '872'}
        },
        {
          0: {label: 'Chief Executive Officer'},
          1: {tn: '876'},
          2: {num: '876'},
          4: {tn: '877'},
          5: {num: '877'}
        },
        {
          0: {label: 'Chief Financial Officer'},
          1: {tn: '881'},
          2: {num: '881'},
          4: {tn: '882'},
          5: {num: '882'}
        },
        {
          0: {label: 'Chief Information Officer'},
          1: {tn: '886'},
          2: {num: '886'},
          4: {tn: '887'},
          5: {num: '887'}
        },
        {
          0: {label: 'Chief Operating Officer'},
          1: {tn: '891'},
          2: {num: '891'},
          4: {tn: '892'},
          5: {num: '892'}
        },
        {
          0: {label: 'Chief Administrative Officer'},
          1: {tn: '896'},
          2: {num: '896'},
          4: {tn: '897'},
          5: {num: '897'}
        },
        {
          0: {label: 'Comptroller'},
          1: {tn: '901'},
          2: {num: '901'},
          4: {tn: '902'},
          5: {num: '902'}
        },
        {
          0: {label: 'Authorized Signing Officer'},
          1: {tn: '906'},
          2: {num: '906'},
          4: {tn: '907'},
          5: {num: '907'}
        },
        {
          0: {label: 'Other (untitled)'},
          1: {tn: '911'},
          2: {num: '911'},
          4: {tn: '912'},
          5: {num: '912'}
        }
      ]
    }
  }
})();
