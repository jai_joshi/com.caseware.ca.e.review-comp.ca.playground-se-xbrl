(function() {
  wpw.tax.create.diagnostics('t2s547', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var officerInfoFieldElected = ['801', '806', '811', '816', '821', '826', '831',
          '836', '841', '846', '851', '856', '861', '866', '871', '876', '881',
          '886', '891', '896', '901', '906', '911'],
        officerInfoFieldCeased = ['802', '807', '812', '817', '822', '827', '832', '837',
          '842', '847', '852', '857', '862', '867', '872', '877', '882', '887', '892',
          '897', '902', '907', '912'],
        addressFields = ['700', '705', '720', '730', '760', '780'];

    diagUtils.diagnostic('5470001', common.prereq(common.requireFiled('T2S547'), common.and(
        common.requireFiled('T2S546'),
        function(tools) {
          return tools.field('t2s546.300').get() == 2;
        }
    )));

    diagUtils.diagnostic('5470002', common.prereq(common.requireFiled('T2S547'),
        function(tools) {
          if (tools.field('780').get() == 'CA' || tools.field('780').get() == 'US') {
            return tools.requireAll(tools.list(addressFields.concat('770', '790')), 'isNonZero') &&
                (tools.requireAll(tools.list(['795', '796']), 'isFilled') ||
                    tools.requireOne(tools.list(officerInfoFieldElected.concat(officerInfoFieldCeased)), 'isFilled'))
          }
          else {
            return tools.requireAll(tools.list(addressFields), 'isNonZero') &&
                (tools.requireAll(tools.list(['795', '796']), 'isFilled') ||
                    tools.requireOne(tools.list(officerInfoFieldElected.concat(officerInfoFieldCeased)), 'isFilled'))
          }
        }));

    diagUtils.diagnostic('5470003', common.prereq(common.and(
        common.requireFiled('T2S547'),
        common.check(['797'], 'isNonZero')),
        common.check('796', 'isNonZero')));

    diagUtils.diagnostic('5470004', common.prereq(common.and(
        common.requireFiled('T2S547'),
        common.check(['796', '797'], 'isNonZero')),
        common.check('795', 'isNonZero')));

    diagUtils.diagnostic('5470005', common.prereq(common.and(
        common.requireFiled('T2S547'),
        function(tools) {
          return (tools.field('795').get() == 1 || tools.field('795').get() == 2);
        }), common.check(['796', '797'], 'isNonZero')));

    var checkDateCeased = function(fieldNum) {
      diagUtils.diagnostic('5470006', common.prereq(common.and(
          common.requireFiled('T2S547'),
          common.check(fieldNum, 'isFilled')),
          common.check(fieldNum - 1, 'isFilled')));
    };

    for (var field in officerInfoFieldCeased) {
      checkDateCeased(officerInfoFieldCeased[field]);
    }

  });
})();
