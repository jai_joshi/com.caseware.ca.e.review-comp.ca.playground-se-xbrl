(function() {

  wpw.tax.create.formData('t2s48', {
    formInfo: {
      abbreviation: 'T2S48',
      title: 'Film or Video Production Services Tax Credit',
      schedule: 'Schedule 48',
      code: 'Code 1101',
      formFooterNum: 'T1177 E (11)',
      isRepeatForm: true,
      headerImage: 'canada-federal',
      repeatFormData: {
        titleNum: '301'
      },
      agencyUseOnlyBox: {
        text: 'Code number',
        height: '80px',
        width: '190px',
        tn: '048',
        textAfterTn: true,
        tnPosition: 'centre',
        textAbove: ' '
      },
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this form to claim a tax credit for qualified Canadian labour expenditures ' +
              'of an eligible production corporation. The corporation must have incurred the expenditures ' +
              'for a production that the Minister of Canadian Heritage certified as an accredited production.'
            },
            {
              label: 'To claim this credit, include the following with your <i>T2 Corporation Income Tax Return</i> for the tax year:',
              sublist: ['the accredited film or video production certificate (or a copy) issued by the Canadian Audio-Visual Certification Office (CAVCO); and',
                'a completed copy of this form for each accredited production. We consider each episode in a series ' +
                'to be a production. However, we will accept one form for episodes in a series that are accredited productions.']
            },
            {
              label: 'For information on claiming this tax credit, go to <b>www.cra.gc.ca/filmservices</b> ' +
              'or see Guide RC4385, <i>Film or Video Production Services Tax Credit – Guide to Form T1177</i> .'
            }
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        header: 'Part 1 – Contact information (please print)',
        rows: [
          {
            type: 'table', num: '100'
          }
        ]
      },
      {
        header: 'Part 2 - Identifying the film or video production',
        rows: [
          {
            type: 'splitTable',
            fieldAlignRight: true,
            side1: [
              {
                label: 'Title of production'
              },
              {
                type: 'infoField',
                num: '301', "validate": {"or":[{"length":{"min":"1","max":"175"}},{"check":"isEmpty"}]},
                tn: '301'
              },
              {type: 'horizontalLine'},
              {
                label: 'CAVCO reference number (for a certificate issued before April 1, 2010)',
                labelClass: 'fullLength'
              },
              {
                type: 'table',
                num: '106'
              },
              {type: 'horizontalLine'},
              {
                label: 'For a series of episodes, enter range of CAVCO certificate numbers<br>' +
                'that were issued before April 1, 2010',
                labelClass: 'fullLength'
              }
            ],
            side2: [
              {
                type: 'infoField',
                label: 'Date principal photography began',
                inputType: 'date',
                num: '302',
                tn: '302',
                labelWidth: '65%'
              },
              {type: 'horizontalLine'},
              {
                type: 'infoField',
                label: 'CAVCO certificate number',
                num: '304',
                tn: '304',
                inputType: 'custom',
                format: ['{N6}', '{N9}']
              },
              {type: 'horizontalLine'},
              {
                type: 'infoField',
                label: 'From',
                num: '305', "validate": {"or":[{"length":{"min":"7","max":"7"}},{"check":"isEmpty"}]},
                tn: '305',
                inputType: 'custom',
                format: ['AC{N6}']
              },
              {
                type: 'infoField',
                label: 'To',
                num: '306', "validate": {"or":[{"length":{"min":"7","max":"7"}},{"check":"isEmpty"}]},
                tn: '306',
                inputType: 'custom',
                format: ['AC{N6}']
              }
            ]
          }
        ]
      },
      {
        header: 'Part 3 – Eligibility',
        rows: [
          {
            type: 'infoField',
            tn: '330',
            num: '330',
            label: '1. Were the activities of the corporation in Canada primarily the carrying on of ' +
            'a film or video production business or a film or video production services business through ' +
            'a permanent establishment in Canada?',
            labelClass: 'tabbed',
            labelWidth: '80%',
            inputType: 'radio',
            init: '2'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            tn: '335',
            num: '335',
            label: '2. Was all or part of the corporation\'s taxable income exempt from Part I tax at any time in the tax year?',
            labelClass: 'tabbed',
            labelWidth: '80%',
            inputType: 'radio',
            init: '2'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            tn: '340',
            num: '340',
            label: '3. Was the corporation at any time in the tax year controlled directly or indirectly in any ' +
            'manner whatever by one or more persons, all or part of whose taxable income was exempt from Part I tax?',
            labelClass: 'tabbed',
            labelWidth: '80%',
            inputType: 'radio',
            init: '2'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            tn: '345',
            num: '345',
            label: '4. Was the corporation at any time in the tax year a prescribed labour-sponsored venture capital corporation?',
            labelClass: 'tabbed',
            labelWidth: '80%',
            inputType: 'radio',
            init: '2'
          },
          {labelClass: 'fullLength'},
          {
            label: 'If you answered <b>no</b> to question 1 or <b>yes</b> to any other question, you ' +
            'are <b>not eligible</b> for the film or video production services tax credit.',
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            label: 'Is the corporation eligible for the Canadian film or video production services tax credit?',
            inputType: 'radio',
            num: '346'
          }
        ]
      },
      {
        header: 'Part 4 – Qualified Canadian labour expenditure',
        rows: [
          {
            'label': '<b>Canadian labour expenditure for the tax year</b> is the total of:'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Salary or wages paid for services rendered in Canada and directly attributable to the production',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '601',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '601'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Remuneration for services rendered in Canada directly attributable to the production and paid to:'
          },
          {
            'label': '- individuals resident in Canada',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '603',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '603'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': '- other taxable Canadian corporations (for their employees who are resident in Canada)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '605',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '605'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': '- taxable Canadian corporations (solely owned by an individual resident in Canada)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '606',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '606'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': '- partnerships carrying on business in Canada (for their members or employees who are resident in Canada)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '607',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '607'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': 'Canadian labour expenditure transferred under a reimbursement agreement by the corporation, a wholly owned subsidiary, to the parent corporation that is a taxable Canadian corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '609',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '609'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'label': 'Canadian labour expenditure for the tax year (total of amounts a to f)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '608'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '610'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Canadian labour expenditures for all previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '611',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '611'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Total Canadian labour expenditures (amount A <b>plus</b> amount B)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '621'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': '<b>Deduct:</b>'
          },
          {
            'label': 'Total government and non-government assistance that the corporation has not repaid',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '612',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '612'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'g'
                }
              }
            ]
          },
          {
            'label': 'Qualified Canadian labour expenditures for all previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '613',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '613'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'h'
                }
              }
            ]
          },
          {
            'label': 'Canadian labour expenditure transferred under a reimbursement agreement by the parent corporation, that is a taxable Canadian corporation, to the corporation, a wholly owned subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '615',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '615'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'i'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts g to i)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '616'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '617'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': '<b>Qualified Canadian labour expenditure</b> (amount C <b>minus</b> amount D)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '618',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '618'
                }
              }
            ],
            'indicator': 'E'
          }
        ]
      },
      {
        header: 'Part 5 – Film or video production services tax credit',
        rows: [
          {
            type: 'table',
            num: '500'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Enter amount F on line 797 of your <i>T2 Corporation Income Tax Return</i>. ' +
            'If you are filing more than one Form T1177, add amount F from all the forms and enter the ' +
            'total on line 797 of your T2 return.', labelClass: 'fullLength'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            label: '<i>Privacy</i> Act, Personal Information Bank number CRA PPU 047',
            labelClass: 'absoluteAlignRight'
          }

        ]
      }
    ]
  });
})();
