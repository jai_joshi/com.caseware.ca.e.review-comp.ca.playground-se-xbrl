(function() {

  wpw.tax.create.calcBlocks('t2s48', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //part 1 calcs
      field('100').cell(0, 0).assign(field('T2J.951').get() + ' ' + field('T2J.950').get());
      field('100').cell(0, 1).assign(field('T2J.956').get());
      //eligibility
      if (field('330').get() == 1 &&
          field('335').get() == 2 &&
          field('340').get() == 2 &&
          field('345').get() == 2) {
        field('346').assign(1)
      }
      else {
        field('346').assign(2)
      }
    });

    //Part 4
    calcUtils.calc(function(calcUtils, field, form) {
      var isNotEligible = field('346').get() == 2

      calcUtils.sumBucketValues('608', ['601', '603', '605', '606', '607', '609']);
      calcUtils.equals('610', '608');
      calcUtils.sumBucketValues('621', ['610', '611']);
      calcUtils.sumBucketValues('616', ['612', '613', '615']);
      calcUtils.equals('617', '616');

      if (isNotEligible) {
        calcUtils.removeValue([618, 620], true);
      }
      else {
        field('100').cell(0, 0).assign(field('T2J.951').get() + ' ' + field('T2J.950').get());
        field('100').cell(0, 1).assign(field('T2J.956').get());
        calcUtils.subtract('618', '621', '617')
      }
    });

    //Part 5
    calcUtils.calc(function(calcUtils, field, form) {
      field('619').assign(field('ratesFed.422').get());
      field('619').source(field('ratesFed.422'));
      calcUtils.multiply('620', ['618', '619'], (1 / 100));
    });

  });
})();
