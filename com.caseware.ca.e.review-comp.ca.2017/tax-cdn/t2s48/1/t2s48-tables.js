(function() {

  wpw.tax.create.tables('t2s48', {
    '100': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1],
      'columns': [
        {
          'width': '60%',
          cellClass: 'alignLeft'
        },
        {
          cellClass: 'alignLeft'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Name of person to contact for more information',
            'num': '151', "validate": {"or":[{"length":{"min":"1","max":"60"}},{"check":"isEmpty"}]},
            'tn': '151',
            type: 'text'
          },
          '1': {
            'label': 'Telephone number including area code',
            'num': '153',
            'tn': '153',
            type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
            validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
          }
        }
      ]
    },
    500: {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '50%',
          type: 'none'
        },
        {
          width: '40px'
        },
        {
          width: '230px',
          type: 'none'
        },
        {
          width: '120px',
          cellClass: 'alignRight'
        },
        {
          width: '15px',
          type: 'none'
        },
        {
          width: '8px',
          type: 'none'
        }
      ],
      cells: [
        {
          0: {
            label: '<b>Film or video production services tax credit</b> (amount E in Part 4 <b>multiplied by</b>'
          },
          1: {
            num: '619'
          },
          2: {
            label: '%)'
          },
          3: {
            tn: '620',
            num: '620',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          },
          4: {
            label: 'F'
          }
        }
      ]
    },
    '106': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          type: 'custom',
          format: ['{N5}']
        }
      ],
      cells: [
        {
          '0': {
            tn: '303'
          },
          '1': {
            label: 'TC'
          },
          '3': {
            num: '303', "validate": {"or": [{"matches": "^-?[.\\d]{5,5}$"}, {"check": "isEmpty"}]}
          }
        }
      ]
    }

  })
})();
