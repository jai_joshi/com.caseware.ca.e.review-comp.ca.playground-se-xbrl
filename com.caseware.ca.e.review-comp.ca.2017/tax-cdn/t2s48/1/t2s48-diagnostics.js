(function() {
  wpw.tax.create.diagnostics('t2s48', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0480001', common.prereq(common.check(['t2j.797'], 'isNonZero'), common.requireFiled('T2S48')));

    diagUtils.diagnostic('0480002', common.prereq(common.requireFiled('T2S48'),
        function(tools) {
          return tools.requireAll(tools.list(['151', '153']), 'isFilled');
        }));

    diagUtils.diagnostic('0480003', common.prereq(common.requireFiled('T2S48'),
        function(tools) {
          return tools.requireAll(tools.list(['301', '302']), 'isNonZero');
        }));

    diagUtils.diagnostic('0480005', common.prereq(common.requireFiled('T2S48'),
        function(tools) {
          return tools.field('304').require('isNonZero') || tools.requireAll(tools.list(['305', '306']), 'isNonZero');
        }));

    diagUtils.diagnostic('0480006', common.prereq(common.and(
        common.requireFiled('T2S48'),
        common.check(['620'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['601', '603', '605', '606', '607', '609', '611']), 'isNonZero');
        }));

    diagUtils.diagnostic('0480010', common.prereq(common.requireFiled('T2S48'),
        function(tools) {
          return tools.requireAll(tools.list(['t2j.797', '620']), 'isZero');
        }));

    diagUtils.diagnostic('0480014', common.prereq(common.and(
        common.requireFiled('T2S48'),
        common.check(['620'], 'isNonZero')),
        function(tools) {
          return tools.requireAll(tools.list(['330', '335', '340', '345']), 'isNonZero');
        }));

    diagUtils.diagnostic('048.304', common.prereq(common.and(
        common.requireFiled('T2S48'),
        common.check(['304', '305'], 'isNonZero', true)),
        function(tools) {
          return tools.field('304').require('isNonZero');
        }));

    diagUtils.diagnostic('048.305', common.prereq(common.and(
        common.requireFiled('T2S48'),
        common.check(['304'], 'isZero')),
        function(tools) {
          return tools.field('305').require('isNonZero');
        }));

    diagUtils.diagnostic('048.306', common.prereq(common.and(
        common.requireFiled('T2S48'),
        common.check(['305'], 'isZero')),
        function(tools) {
          return tools.field('306').require('isNonZero');
        }));

  });
})();

