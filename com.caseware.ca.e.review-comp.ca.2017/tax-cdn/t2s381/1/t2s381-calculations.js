(function() {

  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  wpw.tax.create.calcBlocks('t2s381', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      field('950').assign(field('ratesMb.149').get());
      field('950').source(field('ratesMb.149'));
      //part1
      field('108').assign(field('103').get());
      field('213').assign(field('501').get());
      field('104').assign(field('500').cell(0, 3).get());
      field('105').assign(field('213').get() - field('104').get());
      field('106').assign(field('105').get());
      field('110').assign(field('503').get());
      field('119').assign(field('108').get());

      field('120').assign(field('119').get() * field('950').get() / 100);
    });
    calcUtils.calc(function(calcUtils, field) {
      field('951').assign(field('ratesMb.150').get());
      field('951').source(field('ratesMb.150'));
      field('952').assign(field('ratesMb.151').get());
      field('952').source(field('ratesMb.151'));
      //part2
      calcUtils.sumBucketValues('141', ['120', '130', '140']);
      calcUtils.subtract('151', '141', '150');
      field('152').assign(field('151').get());
      calcUtils.sumBucketValues('153', ['106', '110', '152']);
      var limit = Math.min(field('T2S383.301').get(), field('153').get());
      field('160').assign(Math.min(field('153').get() - field('904').get(), limit));
      field('251').assign(field('153').get());
      field('252').assign(field('T2S383.301').get());
      calcUtils.subtract('253', '251', '252');

      field('143').assign(field('142').get() * field('951').get() / 100);
      field('146').assign(field('145').get() * field('952').get() / 100);
      field('123').assign(field('143').get() + field('146').get());
      field('124').assign(field('152').get());
      field('125').assign(Math.min(field('253').get(), field('123').get(), field('124').get()));
      field('126').assign(field('904').get());
      calcUtils.sumBucketValues('127', ['160', '125', '126']);
      field('128').assign(field('127').get());
      calcUtils.subtract('200', '153', '128');
    });
    calcUtils.calc(function(calcUtils, field) {
      //part3
      //to get year for table summary
      var tableArray = [300, 400, 500];
      var taxationYearTable = field('tyh.200');
      tableArray.forEach(function(num) {
        field(num).getRows().forEach(function(row, rowIndex) {
          if (num == 300) {
            var dateCol = Math.abs(rowIndex - 19);
            row[1].assign(taxationYearTable.cell(dateCol, 6).get());
          }
          else {
            row[1].assign(taxationYearTable.getRow(rowIndex + 10)[6].get())
          }
        })
      });
    });
    calcUtils.calc(function(calcUtils, field) {
      var summaryTable = field('500');
      //part4
      field('400').getRows().forEach(function(row, rowIndex) {
        row[3].assign(summaryTable.getRow(rowIndex)[13].get());
      });
    });
    calcUtils.calc(function(calcUtils, field) {
      //historical data table
      var summaryTable = field('500');
      var limitOnCredit = field('128').get();
      summaryTable.getRows().forEach(function(row, rowIndex) {
        if (rowIndex == 0) {
          // to avoid the first row being calculated to total
          row[7].assign(0);
          row[9].assign(0);
          row[11].assign(0);
          row[13].assign(0);
        } else {
          row[9].assign(row[3].get() + row[5].get() + row[7].get());
          row[11].assign(returnAmountApplied(limitOnCredit, row[9].get()));
          row[13].assign(Math.max(row[9].get() - row[11].get(), 0));
          limitOnCredit -= row[11].get();
        }
      });
      summaryTable.cell(10, 5).assign(field('151').get());
    });

  })
})();
