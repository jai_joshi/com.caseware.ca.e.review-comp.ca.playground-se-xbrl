(function() {
  wpw.tax.create.diagnostics('t2s381', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3810001', common.prereq(common.and(
        common.check('t2s5.605', 'isNonZero'),
        common.requireFiled('T2S381')),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireAll([row[0], row[2], row[3]], 'isNonZero') ||
                tools.requireOne(tools.list(['105', '110', '130', '140']), 'isNonZero');
          });
        }));

    diagUtils.diagnostic('3810002', common.prereq(common.requireFiled('T2S381'),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[0], row[2], row[3]], 'isNonZero'))
              return tools.requireAll([row[0], row[2], row[3]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('3810003', common.prereq(common.and(
        common.requireFiled('T2S381'),
        common.check('t2s5.621', 'isNonZero')),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireAll([row[0], row[2], row[3]], 'isNonZero') ||
                tools.requireOne(tools.list(['130', '140']), 'isNonZero');
          });
        }));

  });
})();
