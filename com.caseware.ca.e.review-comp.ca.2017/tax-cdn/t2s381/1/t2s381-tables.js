(function() {

  wpw.tax.create.tables('t2s381', {
    '100': {
      hasTotals: true,
      columns: [
        {
          'header': 'CCA Class',
          type: 'selector',
          tn: '101',
          selectorOptions: {
            title: 'CCA Classes',
            items: wpw.tax.codes.ccaClasses,
            hideKeys: true
          }
        },
        {
          header: 'Description of qualified property'
        },
        {
          type: 'date',
          tn: '102',
          header: 'Acquisition date*',
          width: '155px'
        },
        {
          header: 'Capital cost**',
          tn: '103',
          colClass: 'std-input-width',
          total: true,
          totalIndicator: 'A',
          totalMessage: ' ',
          totalNum: '103'
        }
      ]
    },
    '210': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      cells: [
        {
          0: {
            label: 'Amount A from Part 1'
          },
          1: {
            num: '119'
          },
          2: {
            label: 'x'
          },
          3: {
            num: '950'
          },
          4: {
            label: '%='
          },
          5: {
            tn: '120'
          },
          6: {
            num: '120',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        }
      ]
    },
    '250': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      cells: [
        {
          '0': {
            label: 'Amount E'
          },
          '1': {
            num: '251'
          },
          '2': {
            label: '-'
          },
          '3': {
            label: 'Manitoba tax otherwise payable',
            labelClass: 'text-right'
          },
          '4': {
            num: '252'
          },
          '5': {
            label: '='
          },
          '6': {
            num: '253'
          },
          '7': {
            label: '1'
          }
        }
      ]
    },
    '260': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      cells: [
        {
          0: {
            label: 'Credit included in amount a that is earned before July 1, 2013'
          },
          1: {
            tn: '142'
          },
          2: {
            num: '142'
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          3: {
            label: 'x'
          },
          4: {
            num: '951'
          },
          5: {
            label: '%='
          },
          6: {
            num: '143'
          },
          7: {
            label: '2A'
          }
        },
        {
          0: {
            label: 'Credit included in amount a that is earned after June 30, 2013'
          },
          1: {
            tn: '145'
          },
          2: {
            num: '145',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          3: {
            label: 'x'
          },
          4: {
            num: '952'
          },
          5: {
            label: '%='
          },
          6: {
            num: '146'
          },
          7: {
            label: '2B'
          }
        }
      ]
    },
    '300': {
      'fixedRows': true,
      hasTotals: true,
      'infoTable': true,
      'columns': [
        {
          'width': '190px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true,
          header: 'Tax year in which to apply the credit'
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'formField': true,
          'total': true,
          'totalNum': '904',
          'totalMessage': 'Total (enter on line e in Part 2)',
          header: 'Credit to be applied'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        }],
      'cells': [
        {
          '0': {
            'label': '1st preceding taxation year'
          },
          '4': {
            'tn': '901'
          },
          '5': {
            'num': '901', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '2nd preceding taxation year'
          },
          '4': {
            'tn': '902'
          },
          '5': {
            'num': '902'
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '3rd preceding taxation year'
          },
          '4': {
            'tn': '903'
          },
          '5': {
            'num': '903', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }]
    },
    '400': {
      'fixedRows': true,
      hasTotals: true,
      'infoTable': true,
      'columns': [
        {
          type: 'none'
        },
        {
          header: 'Year of origin',
          'width': '190px',
          'type': 'date',
          'disabled': true
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Credit available for carryforward',
          colClass: 'std-input-width',
          total: true,
          totalNum: '401',
          totalMessage: 'Total (equal to line 200 in Part 2)'
        },
        {
          type: 'none'
        }
      ],
      'cells': [
        {'0': {label: '10th previous tax year ending on'}},
        {'0': {label: '9th previous tax year ending on'}},
        {'0': {label: '8th previous tax year ending on'}},
        {'0': {label: '7th previous tax year ending on'}},
        {'0': {label: '6th previous tax year ending on'}},
        {'0': {label: '5th previous tax year ending on'}},
        {'0': {label: '4th previous tax year ending on'}},
        {'0': {label: '3rd previous tax year ending on'}},
        {'0': {label: '2nd previous tax year ending on'}},
        {'0': {label: '1st previous tax year ending on'}},
        {'0': {label: 'Current tax year ending on'}}
      ]
    },
    '500': {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {type: 'none'},
        {
          colClass: 'std-input-width',
          type: 'date',
          disabled: true,
          header: '<b>Year of origin</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',

          total: true,
          totalNum: '501',
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',
          total: true,
          totalNum: '502',

          totalMessage: ' ',
          header: '<b>Current year credit</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width',
          total: true,
          totalNum: '503',

          totalMessage: ' ',
          header: '<b>Transfer amount</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width', total: true, totalMessage: ' ', totalNum: '504',
          header: '<b>Amount available to apply</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width', total: true, totalMessage: ' ', disabled: true, totalNum: '505',
          header: '<b>Applied<b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          colClass: 'std-input-width', total: true, totalMessage: ' ', disabled: true, totalNum: '506',
          header: '<b>Balance to carry forward</b>'
        },
        {type: 'none'}
      ],
      cells: [
        {
          '4': {label: '*'},
          '5': {type: 'none'},
          '9': {type: 'none'},
          '7': {type: 'none'},
          '11': {type: 'none'},
          '13': {type: 'none', label: 'N/A'}
        },
        {
          '5': {type: 'none'},
          '14': {label: '**'}
        },
        {
          '5': {type: 'none'}
        },
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {
          '3': {type: 'none'},
          '7': {type: 'none'}
        }
      ]
    }
  })
})();
