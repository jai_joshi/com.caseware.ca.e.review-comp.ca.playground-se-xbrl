(function() {
  wpw.tax.create.formData('t2s381', {
    formInfo: {
      abbreviation: 'T2S381',
      title: 'Manitoba Manufacturing Investment Tax Credit',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      //subTitle: '(2014 and later tax years)',
      schedule: 'Schedule 381',
      code: '1401',
      formFooterNum: 'T2 SCH 381 E (14)',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'If you are a corporation that has acquired qualified property before <b>2018</b>, or that ' +
              'has acquired equipment used to produce or conserve energy, mainly to manufacture or process goods' +
              ' for sale or lease, or you are a corporation that has unused Manitoba manufacturing ' +
              'investment tax credit at the end of the previous year, use this schedule to claim ' +
              'a 10% tax credit against Manitoba corporation income tax payable.'
            },
            {
              label: 'The tax credit will be first applied to reduce the Manitoba corporation income tax payable' +
              ' and any remaining amount earned in this tax year may be refunded. The maximum refundable' +
              ' part of this credit is 70% of earned credits for qualified property acquired after ' +
              'December 31, 2007 and 80% of earned credits for qualified property acquired after June 30, 2013. ' +
              'Any unused investment tax credit can be carried forward up to 10 years or carried' +
              ' back to any of the 3 previous years'
            },
            {
              label: 'A corporation may renounce its entitlement to all or part of its manufacturing investment ' +
              'tax credit earned in the current tax year. If the renunciation is filed by the filing due date ' +
              'for the year, the corporation is deemed to never have received,' +
              ' been entitled to receive or had a reasonable expectation of receiving the amount. ' +
              'If the renunciation is filed within a 365-day period immediately following the filing-due date,' +
              ' the corporation is deemed to never have received, been entitled to receive or had ' +
              'a reasonable expectation of receiving the amount for all purposes except for paragraph 37(1)(d) ' +
              'and subsections 127(18) to (20) of the federal <i>Income Tax Act</i>.'
            },
            {
              label: 'Manufacturing or processing is defined in subsection 125.1(3) of the federal tax act ' +
              'and includes qualified activities as defined in section 5202 of the federal <i>Income Tax Regulations</i>'
            },
            {
              label: 'Qualified property is <b>new</b> and <b>used</b> prescribed buildings, machinery, and equipment ' +
              'used by the corporation in Manitoba mainly to manufacture or process goods for sale or lease. ' +
              'Qualified property includes <b>Class 43.1</b> or <b>43.2</b> property that the corporation ' +
              'acquired after <b>April 22, 2003</b>, and is used by the corporation in Manitoba to produce energy, ' +
              'or to conserve or reduce the need to acquire energy. Qualified property includes property leased ' +
              'for the same purposes, to a lessee who is not exempt from tax under section 149' +
              ' of the federal <i>Income Tax Act</i>.'
            },
            {
              label: 'Property acquired has to be "available for use" by the corporation, as determined under' +
              ' subsections 13(27) and 13(28) of the federal <i>Income Tax Act</i>, <b>not including</b> the time just ' +
              'before the disposition of the property by the corporation as per paragraphs 13(27)(c) and 13(28)(d).'
            },
            {
              label: 'The Manitoba manufacturing investment tax credit (MMITC) is considered government ' +
              'assistance under paragraph 12(1)(x) of the federal <i>Income Tax Act</i> and must be included in ' +
              'income in the tax year the credit is received. The MMITC is not considered government assistance ' +
              'under section 7.2 of the Manitoba <i>Income Tax Act</i> for the purposes of calculating the credit itself.'
            },
            {
              label: 'To claim this credit, you must file this schedule no later than <b>one year</b> after the ' +
              'filing due date for your <i>T2 Corporation Income Tax Return</i> for the tax year  in which ' +
              'the property was acquired. File a completed copy of this schedule with your ' +
              '<i>T2 Corporation Income Tax Return</i>'
            }
          ]
        }
      ],
      category: 'Manitoba Forms'
    },
    'sections': [
      {
        'header': 'Part 1 - Qualified property (acquired in this tax year) eligible for the credit',
        'rows': [
          {
            'type': 'table',
            'num': '100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Total capital cost of qualified property acquired by the corporation in the current year</b> (total of column 103)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '108',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '108'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* The acquisition date is the date that the property became available for use. ',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength',
            'label': '** When you calculate the capital cost of qualified property, deduct the amount of any government or non-government assistance. '
          }
        ]
      },
      {
        'header': 'Part 2 - Total credit available for the year and credit available for carryforward',
        'rows': [
          {
            'label': 'Unused credit at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '213'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Deduct</b>: Credit expired*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              },
              null
            ]
          },
          {
            'label': 'Unused credit at the beginning of this tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '106',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': '<b>Add:</b>'
          },
          {
            'label': 'Credit transferred on an amalgamation or the wind-up of a subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit earned in the current year:',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '210'
          },
          {
            'label': 'Credit allocated from a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit allocated from a trust',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '140',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '140'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of lines 120, 130, and 140)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '141'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': '<b>Deduct</b>: Credit renounced',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '150'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': '<b>Total credit earned in the current year</b> (amount a <b>minus</b> amount b) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '151'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '152'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': '<b>Total credit available for the current tax year</b> (total of amounts B, C, and D)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '153'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': '<b>Deduct</b>'
          },
          {
            'label': '<b>Non-refundable credit claimed in the current tax year**</b><br>(enter on line 605 of Schedule 5) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'Refundable credit:',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'num': '250'
          },
          {
            'type': 'table',
            'num': '260'
          },
          {
            'label': 'Subtotal (amount 2A plus amount 2B)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '123'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '2'
                }
              }
            ]
          },
          {
            'label': 'Amount D',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '124'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '3'
                }
              }
            ]
          },
          {
            'label': '<b>Refundable credit claimed in the current year</b> (amount 1, amount 2 or amount 3, whichever is less) (enter on line 621 of Schedule 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '125',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '125'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': '<b>Credit carried back to previous tax year</b>(s) (complete Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '126'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts c, d, and e)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '127'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '128'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': '<b>Closing balance for carryforward</b> (amount E <b>minus</b> amount F)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* An unused credit expires after <b>10</b> years if it relates to qualified property acquired in a tax year after <b>2003</b>',
            'labelClass': 'fullLength'
          },
          {
            'label': '** The non-refundable credit claimed in the current tax year cannot exceed the Manitoba tax otherwise payable or amount E, whichever is less.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Request for carryback of credit',
        'rows': [
          {
            'label': 'Complete this part to ask for a carryback of a current-year credit earned'
          },
          {
            'type': 'table',
            'num': '300'
          }
        ]
      },
      {
        'header': 'Part 4 - Analysis of credit available for carryforward by year of origin',
        'rows': [
          {
            'label': 'You can complete this part to show all the credits from previous tax years available for carryforward, by year of origin. This will help you determine the amount of credit that could expire in future years',
            'labelClass': 'fullLength'
          },
          {
            'label': 'The carry-forward period for tax years ending after 2003 is <b>10</b> years',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '400'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for Manitoba Manufacturing Investment Tax Credit',
        'rows': [
          {
            'type': 'table',
            'num': '500'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  });
})();
