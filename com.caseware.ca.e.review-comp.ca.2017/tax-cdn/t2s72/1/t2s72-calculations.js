(function() {

  function allocate(field, outArr, incTbl, lossTbl, netTcg, deduct, total, factor) {
    var gainLoss; //store the gain or loss by item
    var netGainLoss = 0; //store the net gain or loss
    var adjust = 0; //store losses that aren't applied against same type gains
    for (var i = 0; i < 3; i++) {
      gainLoss = field(incTbl).cell(i, 3).get() - field(lossTbl).cell(i, 3).get();
      netGainLoss += gainLoss;
      if (gainLoss >= 0) {
        field(outArr[i]).assign(gainLoss);
      }
      else {
        adjust -= gainLoss;
        field(outArr[i]).assign(0);
      }
      if (angular.isDefined(factor)) {
        //factor is applied by the date calculations, reduces all amounts
        field(outArr[i]).assign(field(outArr[i]).get() * field(factor).get());
      }
    }
    if (adjust > 0) {
      //when there's a loss that can't be applied against the other types
      //i.e. other loss with only business income, this has to be reconciled using gains available
      allocReduce(field, outArr, adjust, netGainLoss, false);
    }
    //assign and factor netTcg separately
    field(outArr[3]).assign(field(netTcg).get());
    if (angular.isDefined(factor)) {
      field(outArr[3]).assign(field(outArr[3]).get() * field(factor).get());
    }
    if (deduct && total) {
      //when there's a deduction applied against the total, all types should be reduced to reconcile
      allocReduce(field, outArr, field(deduct).get(), field(total).get(), true);
    }
    allocRound(field, outArr, total);
  }

  function allocReduce(field, outArr, deduct, total, calcAll) {
    var div;
    var max = 4;
    total += deduct;
    if (!calcAll) {
      max = 3;
    }
    for (var i = 0; i < max; i++) {
      if (total == 0) {
        //this should never happen - prevent divide by zero
        div = deduct;
      }
      else {
        //factor to reduce by to reconcile to correct total amount
        div = deduct / total;
      }
      field(outArr[i]).assign(field(outArr[i]).get() * (1 - div));
    }
  }

  function allocRound(field, outArr, totalId) {
    var totalOut = 0;
    var maxVal = 0;
    var maxId;
    outArr.forEach(function(fieldId) {
      totalOut += field(fieldId).get();
      if (Math.max(maxVal, field(fieldId).get()) != maxVal) {
        maxVal = field(fieldId).get();
        maxId = fieldId;
      }
    });
    if (field(totalId).get() != totalOut && maxId) {
      field(maxId).assign(maxVal - (totalOut - field(totalId).get()));
    }
  }
  function getSum(field, fieldArr) {
    var sum = 0;
    fieldArr.forEach(function(fieldId) {
      if (fieldId.charAt(0) == '-' && angular.isDefined(field(fieldId.substring(1)).get())) {
        sum -= field(fieldId.substring(1)).get();
      }
      else if (angular.isDefined(field(fieldId).get())) {
        sum += field(fieldId).get();
      }
    });
    return sum;
  }

  wpw.tax.create.calcBlocks('t2s72', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      //part 1
      if (angular.isDefined(field('104').get())) {
        field('105').assign(calcUtils.addToDate(field('104').get(), 1, 0, -1));
        field('105').disabled(false);
      }
      if (angular.isDefined(field('106').get())) {
        field('107').assign(calcUtils.addToDate(field('106').get(), 1, 0, -1));
        field('107').disabled(false);
      }
    });
    calcUtils.calc(function(calcUtils, field) {
      //part2
      field('205').fieldAssign(field('204'));
      field('210').fieldAssign(field('209'));
      calcUtils.subtract('211', '205', '210');
      field('212').fieldAssign(field('211'));
      calcUtils.subtract('215', '213', '214');
      field('216').fieldAssign(field('215'));
      calcUtils.sumBucketValues('217', ['212', '216']);
      calcUtils.subtract('219', '217', '218');
      field('223').fieldAssign(field('213'));
      allocate(field, ['220', '221', '222', '223'], '201', '206', '215', '218', '219');
    });
    calcUtils.calc(function(calcUtils, field) {
      //part3 (1)
      field('319').assign(Math.max(0, wpw.tax.actions.calculateDaysDifference(field('106').get(), field('T2J.061').get())));
      field('305').fieldAssign(field('304'));
      field('310').fieldAssign(field('309'));
      field('311').assign(Math.max(0, field('305').get() - field('309').get()));
      field('312').fieldAssign(field('311'));
      field('315').assign(Math.max(0, field('313').get() - field('314').get()));
      field('316').fieldAssign(field('315'));
      field('317').assign(Math.max(0, field('312').get() + field('316').get()));
      if (field('321').get() != 0) {
        field('320').assign(Math.max(0, field('319').get() / field('321').get()));
      }
      else {
        field('320').assign(0);
      }
      field('322').assign(Math.max(0, field('317').get() * field('320').get()));
    });
    calcUtils.calc(function(calcUtils, field) {
      //part3 (2)
      field('343').assign(Math.max(0, wpw.tax.actions.calculateDaysDifference(field('106').get(), field('T2J.061').get())));
      field('327').fieldAssign(field('326'));
      field('332').fieldAssign(field('331'));
      field('333').assign(Math.max(0, field('327').get() - field('332').get()));
      field('334').fieldAssign(field('333'));
      field('337').assign(Math.max(0, field('335').get() - field('336').get()));
      field('338').fieldAssign(field('337'));
      field('339').assign(Math.max(0, field('334').get() + field('338').get()));
      field('340').fieldAssign(field('219'));
      field('341').assign(Math.max(0, field('339').get() - field('340').get()));
      if (field('345').get() != 0) {
        field('344').assign(Math.max(0, field('343').get() / field('345').get()));
      }
      else {
        field('344').assign(0);
      }
      field('346').assign(Math.max(0, field('341').get() * field('344').get()));
    });
    calcUtils.calc(function(calcUtils, field) {
      //part3 (3)
      field('365').assign(Math.max(0, wpw.tax.actions.calculateDaysDifference(field('106').get(), field('T2J.061').get())));
      field('351').fieldAssign(field('350'));
      field('356').fieldAssign(field('355'));
      field('357').assign(Math.max(0, field('351').get() - field('356').get()));
      field('358').fieldAssign(field('357'));
      field('361').assign(Math.max(0, field('359').get() - field('360').get()));
      field('362').fieldAssign(field('361'));
      field('363').assign(Math.max(0, field('358').get() + field('362').get()));
      if (field('367').get() != 0) {
        field('366').assign(Math.max(0, field('365').get() / field('367').get()));
      }
      else {
        field('366').assign(0);
      }
      field('368').assign(Math.max(0, field('363').get() * field('366').get()));
    });
    calcUtils.calc(function(calcUtils, field) {
      var aspaTables = ['322', '346', '368'];
      calcUtils.calcOnFieldChange(field('322'));
      calcUtils.calcOnFieldChange(field('346'));
      calcUtils.calcOnFieldChange(field('368'));
      if (calcUtils.hasChanged('322') ||
          calcUtils.hasChanged('346') ||
          calcUtils.hasChanged('368')) {
        if (field('322').get() != 0) {
          field('399').assign('1');
        }
        else if (field('346').get() != 0) {
          field('399').assign('2');
        }
        else if (field('368').get() != 0) {
          field('399').assign('3');
        }
        field('399').disabled(false);
      }
      if (field('399').get()) {
        field('369').assign(field(aspaTables[field('399').get() - 1]).get());
      }
      else {
        field('369').assign(0);
      }
      field('372').assign(field('370').get() + field('371').get());
      field('373').fieldAssign(field('372'));
      field('374').assign(Math.max(0, field('369').get() - field('373').get()));
    });
    calcUtils.calc(function(calcUtils, field) {
      switch (field('399').get()) {
        case '1':
          allocate(field, ['380', '381', '382', '383'], '301', '306', '315', '372', '374', '320');
          break;
        case '2':
          allocate(field, ['380', '381', '382', '383'], '323', '328', '337', '372', '374', '344');
          break;
        case '3':
          allocate(field, ['380', '381', '382', '383'], '347', '352', '361', '372', '374', '366');
          break;
        default:
          field('380').assign(0);
          field('381').assign(0);
          field('382').assign(0);
          field('383').assign(0);
      }
    });
    calcUtils.calc(function(calcUtils, field) {
      //part4
      field('414').assign(Math.max(0, field('413P').get() - field('413L').get()));
      field('431').assign(field('414').get() * field('452').get());
      if (field('453').get() && field('453').get() != 0) {
        field('452').assign(field('451').get() / field('453').get());
      }
      else {
        field('452').assign(0);
      }
      field('432').fieldAssign(field('431'));
      if (field('401').get() == 0) {
        field('401A').disabled(false);
      }
      else {
        field('401A').assign(false);
      }

      if (field('401A').get()) {
        field('401').assign(0);
      }
      else {
        field('401').disabled(false);
      }
      if (!field('401A').get() && field('401').get() == 0) {
        field('437').fieldAssign(field('432'), field('432').get() >= 0 ? field('432').get() : 0);
        field('499').assign(2);
      }
      else {
        if (field('432').get() < field('401').get()) {
          field('437').fieldAssign(field('432'), field('432').get() > 0 ? field('432').get() : 0);
          field('499').assign(2);
        }
        else {
          field('437').fieldAssign(field('401'), field('401').get() > 0 ? field('401').get() : 0);
          field('499').assign(1);
        }
      }
      if (field('499').get() == 2) {
        if (!calcUtils.hasChanged('401')) {
          field('498').assign(Math.max(0,
              field('410P').cell(3, 3).get() - field('410L').cell(3, 3).get()));
          allocate(field, ['438', '439', '440', '441'], '410P', '410L', '498', null, '437', '452');
        }
      }
      else {
        field('438').disabled(false);
        field('439').disabled(false);
        field('440').disabled(false);
        field('441').disabled(false);
        if (!calcUtils.hasChanged('431') && calcUtils.hasChanged('499')) {
          calcUtils.removeValue(['438', '439', '440', '441'], false);
        }
      }
    });
    calcUtils.calc(function(calcUtils, field) {
      field('501').assign(0);
      field('502').assign(0);
      calcUtils.sumBucketValues('503', ['501', '502']);
    });
    calcUtils.calc(function(calcUtils, field) {
      //part6
      field('604').fieldAssign(field('603'));
      field('609').fieldAssign(field('643'));
      field('610').assign(Math.max(0, field('604').get() - field('609').get()));
      field('611').fieldAssign(field('610'));
      field('614').assign(Math.max(0, field('612').get() - field('613').get()));
      field('615').fieldAssign(field('614'));
      field('616').assign(field('611').get() + field('615').get());
      field('617').assign(0);
      field('618').assign(Math.max(0, field('616').get() - field('617').get()));
      if (field('622').get() && field('622').get() != 0) {
        field('621').assign(field('620').get() / field('622').get());
      }
      else {
        field('621').assign(0);
      }
      field('623').assign(field('618').get() * field('621').get());
      field('625').assign(Math.max(0, field('623').get() - field('624').get()));
      field('627').assign(field('625').get() + field('626').get());
      allocate(field, ['628', '629', '630', '631'], '600', '641', '614', '624', '625', '621');
    });
    calcUtils.calc(function(calcUtils, field) {
      //part7
      if (angular.isDefined(field('1050').get()) && field('1050').get() != '') {
        var row = 0;
        var assigned = false;
        while (row < 6 && !assigned) {
          if (field('720').cell(row, 3).get() != 0) {
            field('701').fieldAssign(field('720').cell(row, 3));
            field('702').fieldAssign(field('720').cell(row, 5));
            assigned = true;
          }
          row++;
        }
      }
      calcUtils.multiply('704', ['701', '702'], (1 / 100));
      field('705').fieldAssign(field('780'));
      field('706').fieldAssign(field('625'));
      field('707').fieldAssign(field('503'));
      calcUtils.subtract('708', '706', '707');
      field('709').fieldAssign(field('708'));
      calcUtils.sumBucketValues('710', ['705', '709']);
      field('711').fieldAssign(field('710'));
      // field('712').assign(Math.max(0, getSum(field, ['t2j.371', //TODO: if condition may be necessary here to deal with pt4, check for line W
      //   '835', '780', '-443', '437', '-385', '374']))); //todo check for dividends and implement this calc
      var num713 = Math.min(field('704').get(), field('711').get(), field('712').get());
      if (Math.min(field('704').get(), field('711').get(), field('712').get()) >= 0) {
        if (num713 == field('704').get()) {
          field('713').fieldAssign(field('704'));
        }
        else if (num713 == field('711').get()) {
          field('713').fieldAssign(field('711'));
        }
        else {
          field('713').fieldAssign(field('712'));
        }
      }
      else {
        field('713').assign(0);
      }
      field('714').fieldAssign(field('713'));
    });
    calcUtils.calc(function(calcUtils, field) {
      //todo: num 1001 rf from prior year

      field('1050').assign(field('t2j.061').getKey('year'));
      field('1005').fieldAssign(field('625'));
      if (!angular.isDefined(field('1001').get()) || !angular.isDefined(field('1005').get()) || !angular.isDefined(field('1010').get()) || !angular.isDefined(field('1015').get()) || !angular.isDefined(field('1020').get()) || !angular.isDefined(field('1025').get())) {

      }

      if (field('108').get() == '2013' || field('108').get() == '2012' || field('108').get() == '2011') {
        //Year column
        field('1004').assign(Number(field('108').get()));
        field('1008').assign(Number(field('108').get()) + 1);
        field('1013').assign(Number(field('108').get()) + 2);
        field('1018').assign(Number(field('108').get()) + 3);
        field('1023').assign(Number(field('108').get()) + 4);
        field('1028').assign(Number(field('108').get()) + 5);
        if (field('108').get() == '2013') {
          // Specified percentage column
          calcUtils.getGlobalValue('1002','ratesFed','841');
          calcUtils.getGlobalValue('1006','ratesFed','842');
          calcUtils.getGlobalValue('1011','ratesFed','843');
          calcUtils.getGlobalValue('1016','ratesFed','844');
          calcUtils.getGlobalValue('1021','ratesFed','845');
          calcUtils.getGlobalValue('1026','ratesFed','845');
        }
        else {
          // Specified percentage column
          calcUtils.getGlobalValue('1002','ratesFed','840');
          calcUtils.getGlobalValue('1006','ratesFed','841');
          calcUtils.getGlobalValue('1011','ratesFed','842');
          calcUtils.getGlobalValue('1016','ratesFed','843');
          calcUtils.getGlobalValue('1021','ratesFed','844');
          calcUtils.getGlobalValue('1026','ratesFed','845');
        }
        // Reserve column
        calcUtils.multiply('1003', ['1001', '1002'], [1 / 100]);
        calcUtils.multiply('1007', ['1005', '1006'], [1 / 100]);
        calcUtils.multiply('1012', ['1010', '1011'], [1 / 100]);
        calcUtils.multiply('1017', ['1015', '1016'], [1 / 100]);
        calcUtils.multiply('1022', ['1020', '1021'], [1 / 100]);
        calcUtils.multiply('1027', ['1025', '1026'], [1 / 100]);
      }
      else {
        calcUtils.removeValue(['1001', '1002', '1003', '1004', '1005', '1006', '1007', '1008', '1009',
          '1011', '1012', '1013', '1014', '1016', '1017', '1018', '1019', '1021', '1022', '1023', '1024',
          '1026', '1027', '1028'], true, true);
      }
    });
    calcUtils.calc(function(calcUtils, field) {
      //part8
      field('804').fieldAssign(field('803'));
      field('809').fieldAssign(field('843'));
      calcUtils.subtract('810', '804', '809');
      field('811').fieldAssign(field('810'));
      calcUtils.subtract('814', '812', '813');
      field('815').fieldAssign(field('814'));
      calcUtils.sumBucketValues('816', ['811', '815']);
      calcUtils.divide('892', '891', '893');
      calcUtils.multiply('821', ['816', '892']);
      calcUtils.subtract('823', '821', '822');
      if ((field('824').get() + field('825').get()) <= '0')
        calcUtils.assignValue('826', 0);
      else
        calcUtils.sumBucketValues('826', ['824', '825']);
      if ((field('823').get() - field('826').get()) <= '0')
        field('827').fieldAssign(field('823'));
      else
        field('827').fieldAssign(field('826'));
      calcUtils.subtract('828', '827', '824', true);
      calcUtils.multiply('830', ['828', '829']);
      calcUtils.multiply('832', ['830', '831']);
      calcUtils.multiply('833', ['827', '829']);
      calcUtils.multiply('834', ['831', '833']);
      calcUtils.getGlobalValue('837','ratesFed','846');
      calcUtils.multiply('835', ['834', '837']);
    });

    calcUtils.calc(function(calcUtils, field) {
      //Store values for s73 in fields
      field('tn270').assign(getSum(field, ['380', '-386', '438', '-444', '781', '-726']));
      field('tn275').assign(getSum(field, ['383', '441', '784']));
      field('tn280').assign(getSum(field, ['381', '-387', '439', '-445', '782', '-727']));
      field('tn285').assign(getSum(field, ['389', '447', '729']));
      field('tn290').assign(getSum(field, ['382', '-388', '440', '-446', '783', '-728']));
    });
  });
})();
