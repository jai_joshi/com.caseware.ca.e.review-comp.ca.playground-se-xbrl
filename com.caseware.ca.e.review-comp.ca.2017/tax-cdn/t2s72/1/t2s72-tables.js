(function() {

  function getIncomeLossCols(totalNum, indicator) {
    return [
      {
        "type": "none"
      },
      {
        "type": "none",
        colClass: 'std-input-col-width'
      },
      {
        "type": "none",
        colClass: 'std-input-width'
      },
      {
        "total": true,
        "totalNum": totalNum,
        colClass: 'std-input-width',
        filters: 'prepend $',
        totalIndicator: indicator
      },
      {
        colClass: 'std-padding-width',
        "type": "none"
      },
      {
        colClass: 'std-input-col-width',
        "type": "none"
      }
    ]
  }

  wpw.tax.global.tableCalculations.t2s72 = {
    "720": {
      "fixedRows": true,
      "num": "720",
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          "alignText": "center"
        },
        {
          "header": 'Year',
          colClass: 'std-input-width',
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          "header": 'QTI'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "alignText": "center"
        },
        {
          "header": 'Specified percentage'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "alignText": "center"
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "header": "Deductible portion"
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        }
      ],
      "cells": [
        {
          "0": {"label": "Year the reserve was created"},
          "1": {"num": "1004"},
          "3": {"num": "1001", 'init': 0},
          "4": {
            "label": "x",
            "labelClass": "center"
          },
          "5": {"num": "1002"},
          "6": {"label": "% ="},
          "8": {"num": "1003"},
          "9": {"label": ""}
        },
        {
          "0": {"label": "1st subsequent year"},
          "1": {"num": "1008"},
          "3": {"num": "1005", 'init': 0},
          "4": {
            "label": "x",
            "labelClass": "center"
          },
          "5": {"num": "1006"},
          "6": {"label": "% ="},
          "8": {"num": "1007"},
          "9": {"label": ""}
        },
        {
          "0": {"label": "2nd subsequent year"},
          "1": {"num": "1013"},
          "3": {"num": "1010", 'init': 0},
          "4": {
            "label": "x",
            "labelClass": "center"
          },
          "5": {"num": "1011"},
          "6": {"label": "% ="},
          "8": {"num": "1012"},
          "9": {"label": ""}
        },
        {
          "0": {"label": "3rd subsequent year"},
          "1": {"num": "1018"},
          "3": {"num": "1015", 'init': 0},
          "4": {
            "label": "x",
            "labelClass": "center"
          },
          "5": {"num": "1016"},
          "6": {"label": "% ="},
          "8": {"num": "1017"},
          "9": {"label": ""}
        },
        {
          "0": {"label": "4th subsequent year"},
          "1": {"num": "1023"},
          "3": {"num": "1020", 'init': 0},
          "4": {
            "label": "x",
            "labelClass": "center"
          },
          "5": {"num": "1021"},
          "6": {"label": "% ="},
          "8": {"num": "1022"},
          "9": {"label": ""}
        },
        {
          "0": {"label": "5th subsequent year"},
          "1": {"num": "1028"},
          "3": {"num": "1025", 'init': 0},
          "4": {
            "label": "x",
            "labelClass": "center"
          },
          "5": {"num": "1026"},
          "6": {"label": "% ="},
          "8": {"num": "1027"},
          "9": {"label": ""}
        },
        {
          "0": {'label': '&nbsp'},
          "1": {'type': 'none'},
          "3": {'type': 'none'},
          "4": {'type': 'none'},
          "5": {'type': 'none'},
          "6": {'type': 'none'},
          "8": {'type': 'none'},
          "9": {'type': 'none'}
        },
        {
          "0": {"label": "Corporation's current tax year"},
          "1": {"num": "1050"},
          "3": {'type': 'none'},
          "4": {'type': 'none'},
          "5": {'type': 'none'},
          "6": {'type': 'none'},
          "8": {'type': 'none'},
          "9": {'type': 'none'}
        }
      ]
    },
    "1000": {
      fixedRows: true,
      infoTable: true,
      num: '1000',
      columns: [
        {
          type: 'none'
        },
        {
          type: 'none',
          colClass: 'std-input-col-width'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          header: '<b>Fiscal period-start</b>',
          type: 'date',
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          header: '<b>Fiscal period-end</b>',
          type: 'date',
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        }
      ],
      cells: [
        {
          0: {label: 'If <b>yes</b>, give the dates of both the old and current fiscal periods of the partnership.'},
          1: {label: 'Old fiscal period'},
          3: {num: '104'},
          5: {num: '105'}
        },
        {
          0: {label: 'If <b>no</b>, give the start and end dates of the current fiscal period of the partnership.'},
          1: {label: 'Current fiscal period (note 1)'},
          3: {num: '106'},
          5: {num: '107'}
        }
      ]
    },
    "201": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "201",
      "columns": getIncomeLossCols('204'),
      'cells': [
        {
          '1': {'label': 'Income from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Income from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other income'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "206": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "206",
      "columns": getIncomeLossCols('209'),
      'cells': [
        {
          '1': {'label': 'Loss from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Loss from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other losses'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "301": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "301",
      "columns": getIncomeLossCols('304'),
      'cells': [
        {
          '1': {'label': 'Income from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Income from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other income'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "306": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "306",
      "columns": getIncomeLossCols('309'),
      'cells': [
        {
          '1': {'label': 'Loss from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Loss from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other losses'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "318": {
      "fixedRows": true,
      "infoTable": true,
      "num": "318",
      "columns": [
        {
          "width": "47%",
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Number of days that are in both the corporation's tax year and the fiscal period of the " +
            "partnership that starts in the corporation's tax year and ends after the tax year (the <b>stub" +
            " period</b>)",
            cellClass: 'alignCenter singleUnderline'
          },
          "2": {
            "num": "319",
            "init": "0"
          },
          "3": {
            "label": " = ",
            cellClass: 'alignCenter'
          },
          "4": {
            "num": "320",
            decimals: 5
          },
          "5": {
            "label": "i",
            "labelClass": "fullLength text-left"
          }
        },
        {
          "0": {
            "label": "Number of days in the <b>first</b> fiscal period of the partnership that ends in " +
            "the corporation's tax year",
            "labelClass": "fullLength center"
          },
          "2": {
            "num": "321",
            "init": "0"
          },
          "4": {
            type: 'none'
          }
        }
      ]
    },
    "323": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "323",
      "columns": getIncomeLossCols('326'),
      'cells': [
        {
          '1': {'label': 'Income from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Income from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other income'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "328": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "328",
      "columns": getIncomeLossCols('331'),
      'cells': [
        {
          '1': {'label': 'Loss from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Loss from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other losses'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "342": {
      "fixedRows": true,
      "infoTable": true,
      "num": "342",
      "columns": [
        {
          "width": "47%",
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Number of days that are in both the corporation's tax year and the fiscal period of the " +
            "partnership that starts in the corporation's tax year and ends after the tax year (the <b>stub period</b>)",
            cellClass: 'alignCenter singleUnderline'
          },
          "2": {
            "num": "343",
            "init": "0"
          },
          "3": {
            "label": " = ",
            cellClass: 'alignCenter'
          },
          "4": {
            "num": "344",
            decimals: 5
          },
          "5": {
            "label": "n",
            "labelClass": "fullLength text-left"
          }
        },
        {
          "0": {
            "label": "Number of days that are in the eligible fiscal period (note 4) of the partnership that ends " +
            "in the corporation's tax year",
            "labelClass": "fullLength center"
          },
          "2": {
            "num": "345",
            "init": "0"
          },
          "4": {
            type: 'none'
          }
        }
      ]
    },
    "347": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "347",
      "columns": getIncomeLossCols('350'),
      'cells': [
        {
          '1': {'label': 'Income from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Income from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other income'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "352": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "352",
      "columns": getIncomeLossCols('355'),
      'cells': [
        {
          '1': {'label': 'Loss from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Loss from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other losses'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "364": {
      "fixedRows": true,
      "infoTable": true,
      "num": "364",
      "columns": [
        {
          "width": "47%",
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [{
        "0": {
          "label": "Number of days that are in both the corporation's tax year and the fiscal period of the " +
          "partnership that starts in the corporation's tax year and ends after the tax year (the <b>stub period</b>)",
          cellClass: 'alignCenter singleUnderline'
        },
        "2": {
          "num": "365",
          "init": "0"
        },
        "3": {
          "label": " = ",
          cellClass: 'alignCenter'
        },
        "4": {
          "num": "366",
          decimals: 5
        },
        "5": {
          "label": "s",
          "labelClass": "fullLength text-left"
        }
      },
        {
          "0": {
            "label": "Number of days in the fiscal period(s) of the partnership that end(s) in the corporation's " +
            "tax year (note 6)",
            "labelClass": "fullLength center"
          },
          "2": {
            "num": "367",
            "init": "0"
          },
          "4": {
            type: 'none'
          }
        }]
    },
    "410P": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "410P",
      "columns": getIncomeLossCols('413P'),
      'cells': [
        {
          '1': {'label': 'Income from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Income from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other losses'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Taxable capital gain'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "410L": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "410L",
      "columns": getIncomeLossCols('413L'),
      'cells': [
        {
          '1': {'label': 'Loss from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Loss from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other losses'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Allowable capital loss'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "450": {
      "fixedRows": true,
      "infoTable": true,
      "num": "450",
      "columns": [
        {
          "width": "47%",
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Number of days that are in both the corporation's tax year and the particular period",
            cellClass: 'alignCenter singleUnderline'
          },
          "2": {
            "num": "451",
            "init": "0"
          },
          "3": {
            "label": " = ",
            cellClass: 'alignCenter'
          },
          "4": {
            "num": "452",
            decimals: 5
          },
          "5": {
            "label": "w",
            "labelClass": "fullLength text-left"
          }
        },
        {
          "0": {
            "label": "Number of days in the particular period",
            "labelClass": "fullLength center"
          },
          "2": {
            "num": "453",
            "init": "0"
          },
          "4": {
            type: 'none'
          }
        }
      ]
    },
    "600": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "600",
      "columns": getIncomeLossCols('603'),
      'cells': [
        {
          '1': {'label': 'Income from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Income from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other income'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "619": {
      "fixedRows": true,
      "infoTable": true,
      "num": "619",
      "columns": [
        {
          "width": "47%",
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Number of days that are in both the corporation's tax year (note 12) " +
            "and the particular period of the partnership (note 11)",
            cellClass: 'alignCenter singleUnderline'
          },
          "2": {
            "num": "620",
            "init": "0"
          },
          "3": {
            "label": " = ",
            cellClass: 'alignCenter'
          },
          "4": {
            "num": "621",
            decimals: 5
          },
          "5": {
            "label": "bb",
            "labelClass": "fullLength text-left"
          }
        },
        {
          "0": {
            "label": "Number of days in the particular period of the partnership (note 11)",
            "labelClass": "fullLength center"
          },
          "2": {
            "num": "622",
            "init": "0"
          },
          "4": {
            type: 'none'
          }
        }
      ]
    },
    "641": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "641",
      "columns": getIncomeLossCols('643'),
      'cells': [
        {
          '1': {'label': 'Loss from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Loss from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other losses'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "700": {
      "fixedRows": true,
      "infoTable": true,
      "num": "700",
      "columns": [
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-input-width',
          "type": "none",
          "textAlign": "right"
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "left"
        }
      ],
      "cells": [
        {
          "1": {
            "label": "QTI (note 14)"
          },
          "2": {
            "num": "701",
            disabled: true,
            init: 0
          },
          "3": {
            "label": "× specified percentage (note 15)"
          },
          "4": {
            "num": "702",
            disabled: true,
            init: 0,
            "decimals": 2
          },
          "5": {
            "label": "% =",
            "labelClass": "bold"
          },
          "6": {
            "num": "703"
          },
          "7": {
            "num": "704"
          },
          "8": {
            "label": "MM"
          }
        }
      ]
    },
    "800": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "800",
      "columns": getIncomeLossCols('803')
      ,
      'cells': [
        {
          '1': {'label': 'Income from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Income from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other income'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "836": {
      "fixedRows": true,
      "infoTable": true,
      "num": "836",
      "columns": [
        {
          "type": "none",
          colClass: 'std-input-col-width',
          "textAlign": "left"
        },
        {
          colClass: 'small-input-width'
        },
        {
          "type": "none"
        },
        {
          "type": "none"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        }],
      "cells": [
        {
          "0": {
            "label": "<b>Threshold amount</b> (",
            "labelClass": "right fullLength"
          },
          "1": {
            "num": "837",
            "decimals": 2
          },
          "2": {
            "label": "% of amount mm) (if negative, enter \"0\")",
            "labelClass": "left fullLength"
          },
          "4": {
            "num": "835"
          },
          "5": {
            "label": "EEE"
          }
        }
      ]
    },
    "840": {
      "infoTable": true,
      'fixedRows': true,
      'hasTotals': true,
      "num": "840",
      "columns": getIncomeLossCols('843'),
      'cells': [
        {
          '1': {'label': 'Loss from business'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Loss from property'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        },
        {
          '1': {'label': 'Other loss'},
          '3': {'validate': {'or': [{'check': 'isZero'}, {'check': 'isPositive'}]}, 'init': 0}
        }
      ]
    },
    "890": {
      "fixedRows": true,
      "infoTable": true,
      "num": "890",
      "columns": [
        {
          "width": "47%",
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "textAlign": "center"
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Number of days that are in both the <b>base year</b> and the fiscal period of the partnership",
            cellClass: 'alignCenter singleUnderline'
          },
          "2": {
            "num": "891",
            "init": "0"
          },
          "3": {
            "label": " = ",
            cellClass: 'alignCenter'
          },
          "4": {
            "num": "892",
            decimals: 5
          },
          "5": {
            "label": "ii",
            "labelClass": "fullLength text-left"
          }
        },
        {
          "0": {
            "label": "Number of days in the fiscal period of the partnership",
            "labelClass": "fullLength center"
          },
          "2": {
            "num": "893",
            "init": "0"
          },
          "4": {
            type: 'none'
          }
        }
      ]
    }
  }
})();
