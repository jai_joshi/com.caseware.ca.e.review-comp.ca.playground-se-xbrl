wpw.tax.create.importCalcs('T2S72', function(importTools) {
  function getImportObj(taxPrepIdsObj) {
    //get and sort by repeatId and key of taxPrepIdsObj
    var output = {};
    Object.keys(taxPrepIdsObj).forEach(function(key) {
      importTools.intercept(taxPrepIdsObj[key], function(importObj) {
        if (!output[importObj.repeatIndex]) {
          output[importObj.repeatIndex] = {};
        }
        output[importObj.repeatIndex][key] = importObj;
      });
    });
    return output;
  }

  var s72Obj = {
    info: getImportObj({
      qtiYear: 'RSPMT[2].Ttwpmt125'
    }),
    part4: getImportObj({
      busLineX: 'RSPMT[3].Ttwpmt162',
      propLineX: 'RSPMT[3].Ttwpmt163',
      tcgLineX: 'RSPMT[3].Ttwpmt164',
      otherLineX: 'RSPMT[3].Ttwpmt231',
      busLineW: 'RSPMT[3].Ttwpmt165',
      propLineW: 'RSPMT[3].Ttwpmt166',
      tcgLineW: 'RSPMT[3].Ttwpmt167',
      otherLineW: 'RSPMT[3].Ttwpmt232'
    })
  };

  importTools.after(function() {
    var s72;
    var rfId = 0;

    function getPropIfExists(obj, properties) {
      //recursively return either the object if some multilayer property exists, or an empty string
      //i.e. if foo[1][2][3], return it; if not, return ''
      properties = wpw.tax.utilities.ensureArray(properties, false);
      return properties.length < 1 ? (obj || '') : getPropIfExists((obj || '')[properties[0]], properties.splice(1));
    }

    while (rfId < importTools.allRepeatForms('T2S72').length) {
      s72 = importTools.form('T2S72', rfId);

      //QTI Year
      if (getPropIfExists(s72Obj, ['info', rfId, 'qtiYear', 'value'])) {
        var qtiYear = '201' + s72Obj['info'][rfId]['qtiYear']['value'].toString();
        s72.setValue('108', qtiYear.toString());
      }

      //Part 4
      var lineW = s72.getValue('401') || 0;
      var lineX = s72.getValue('431') || 0;
      var lineY = s72.getValue('437') || 0;

      if ((lineW > 0 && lineX > 0) && ((lineW < lineX) || (lineW == lineY))) {
        s72.setValue('499', '1');
        s72.setValue('438', getPropIfExists(s72Obj, ['part4', rfId, 'busLineW', 'value']) || 0);
        s72.setValue('439', getPropIfExists(s72Obj, ['part4', rfId, 'propLineW', 'value']) || 0);
        s72.setValue('441', getPropIfExists(s72Obj, ['part4', rfId, 'tcgLineW', 'value']) || 0);
        s72.setValue('440', getPropIfExists(s72Obj, ['part4', rfId, 'otherLineW', 'value']) || 0);
      }
      else {
        s72.setValue('499', '2');
        s72.setValue('438', getPropIfExists(s72Obj, ['part4', rfId, 'busLineX', 'value']) || 0);
        s72.setValue('439', getPropIfExists(s72Obj, ['part4', rfId, 'propLineX', 'value']) || 0);
        s72.setValue('441', getPropIfExists(s72Obj, ['part4', rfId, 'tcgLineX', 'value']) || 0);
        s72.setValue('440', getPropIfExists(s72Obj, ['part4', rfId, 'otherLineX', 'value']) || 0);
      }
      rfId++;
    }
  });
});