(function() {
  wpw.tax.create.diagnostics('t2s72', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S72'), common.bnCheck('102')));

  });
})();
