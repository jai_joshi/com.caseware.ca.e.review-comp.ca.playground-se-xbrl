(function() {
  'use strict';

  wpw.tax.global.formData.t2s72 = {
    'formInfo': {
      'abbreviation': 't2s72',
      isRepeatForm: true,
      repeatFormData: {
        titleNum: '101'
      },
      'title': 'Income Inclusion for Corporations that are Members of Multi-Tier Partnerships',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      //'subTitle': ' (2011 and later tax years)',
      'schedule': 'Schedule 72',
      'showCorpInfo': true,
      formFooterNum: 'T2 SCH 72 E (15)',
      category: 'Federal Tax Forms',
      headerImage: 'canada-federal',
      'description': [
        {
          'type': 'list',
          'items': [
            {
              label: 'If the corporation is a member of a <b>multi-tier</b> partnership that has a fiscal ' +
              'period-end that differs from the corporation tax year-end, or if the corporation is a ' +
              'member of a partnership subject to a <b>multi-tier alignment</b> under subsection 249.1(9) or ' +
              '249.1(11) of the <i>Income Tax Act</i>, use this schedule to determine the corporation\'s ' +
              'income inclusion in respect of the partnership for the tax year under sections 34.2 ' +
              'and 34.3. A corporation that is a member of a partnership that changed its structure ' +
              'from multi-tier to single-tier should continue calculating its income inclusion under ' +
              'sections 34.2 and 34.3 using Schedule 72. Complete a separate schedule for each ' +
              'partnership.'
            },
            {
              label: '<b>Multi-tier alignment</b>, in respect of a partnership, means the alignment under ' +
              'subsection 249.1(9) or 249.1(11) of the fiscal period of the partnership and the ' +
              'fiscal period of one or more other partnerships.'
            },
            {
              label: 'Complete Part 3 of this schedule to calculate the <b>adjusted stub period accrual</b>' +
              ' (ASPA) in respect of multi-tier partnerships if:',
              sublist: [
                'the corporation has a <b>significant interest</b> in the partnership at the end of ' +
                'the last fiscal period of the partnership that ends in the tax year;',
                'another fiscal period of the partnership starts in the tax year and ends after ' +
                'the tax year of the corporation,',
                'at the end of the tax year, the corporation is entitled to a share of an income, ' +
                'loss, taxable capital gain, or allowable capital loss of the partnership for the ' +
                'fiscal period that ends after the end of the tax year, and',
                'the corporation is <b>not</b> a professional corporation.'
              ]
            },
            {
              label: 'If the corporation is a member of a partnership subject to a multi-tier ' +
              'alignment, the ASPA inclusion does not apply to the corporation in respect of the ' +
              'partnership for tax years before the tax year that includes the end of the first ' +
              'aligned fiscal period of the partnership under the multi-tier alignment.'
            },
            {
              label: '<b>Significant interest</b> means that the corporation, or the corporation ' +
              'together with one or more persons or partnerships related to or affiliated with the ' +
              'corporation, is entitled to <b>more than 10%</b> of the income or loss of the partnership, ' +
              'or the assets (net of liabilities) of the partnership if it were to cease to exist.'
            },
            {
              label: 'The ASPA that may be eligible as <b>qualifying transitional income</b> (QTI)' +
              ' is the ASPA for the corporation\'s tax year during which ends the fiscal period of' +
              ' the partnership that is aligned with the fiscal period of one or more other' +
              ' partnerships under the multi-tier alignment, or, for any other case, for the' +
              ' corporation\'s first tax year that ends <b>after March 22, 2011</b>. The QTI is' +
              ' calculated in Part 5 of this schedule.'
            },
            {
              label: 'A corporation can be eligible for <b>transitional relief</b> if it has QTI ' +
              'for the partnership. Transitional relief lets the corporation claim a <b>reserve</b> under ' +
              'subsection 34.2(11). The reserve is calculated in Part 7 of this schedule.'
            },
            {
              label: 'Complete Part 2 of this schedule to calculate the <b>eligible alignment income</b> ' +
              'for the purpose of QTI for the tax year. A corporation may be subject to an additional ' +
              'income inclusion if the members of the partnership, or of a partnership described in ' +
              'relation to the partnership by any of subparagraphs 249.1(1)(c)(ii) to 249.1(1)(c)(iv), ' +
              'have elected or are deemed to have elected to change the fiscal period of the partnership ' +
              '(multi-tier fiscal period alignment). The additional income that arises for the first ' +
              'aligned fiscal period of the partnership that ends in the tax year of the corporation ' +
              'may qualify  as eligible alignment income for the purpose of QTI. To have eligible ' +
              'alignment income, the corporation must be a member of the partnership at the end of ' +
              'the eligible fiscal period. <b>A corporation does not need to have a significant interest ' +
              'in a partnership in order to have eligible alignment income</b>.'
            },
            {
              label: 'Generally, amounts included or claimed under subsections 34.2(2), 34.2(3), 34.2(4), ' +
              '34.2(11), and 34.2(12) are deemed to have the <b>same character</b> and be in the ' +
              '<b>same proportions</b> as the partnership income to which they relate. For example, if a corporation ' +
              'receives $100,000 of partnership income for the partnership\'s fiscal period ending in its tax ' +
              'year, and that income is made up of $40,000 of active business income, $30,000 of ' +
              'income from property, and $30,000 as a taxable capital gain, the corporation\'s ASPA for ' +
              'the partnership would be 40% active business income, 30% property income, and 30% ' +
              'taxable capital gains.'
            },
            {
              label: 'Section 34.2 does not apply when calculating, for a tax year of a foreign affiliate ' +
              'of a corporation resident in Canada, the affiliate\'s foreign accrual property income for ' +
              'the corporation and, generally, the affiliate\'s exempt surplus or exempt deficit, hybrid ' +
              'surplus or hybrid deficit, and taxable surplus or taxable deficit,  for the corporation. ' +
              'See subsection 34.2(8).'
            },
            {
              label: 'Section 249.1 defines <b>fiscal period</b> and sets out conditions to align the partnership\'s fiscal period.'
            },
            {
              label: 'All legislative references are to the <i>Income Tax Act</i> and <i>Income Tax ' +
              'Regulations</i>. This schedule does not replace the Act and its regulations.'
            },
            {
              label: 'This schedule is a worksheet only. You do not have to file it with your <i>T2 ' +
              'Corporation Income Tax Return</i>.'
            },
            {
              label: 'Report on Schedule 73, <i>Income Inclusion Summary for Corporations that Are Members of ' +
              'Partnerships</i>, the amounts calculated on this schedule. File Schedule 73 with the corporation\'s ' +
              'T2 return. If the corporation reported previous-year amounts of stub period accrual, alignment ' +
              'income, or transitional reserve on Schedule 1, complete a Schedule 73 for the previous year, ' +
              'and file it separately.'
            }
          ]
        }
      ]
    },
    'sections': [
      {
        'header': 'Part 1 – Partnership information',
        'rows': [
          {'label': 'Partnership\'s name'},
          {
            'type': 'infoField',
            'inputType': 'textArea',
            'labelClass': 'fullLength',
            'width': '70%',
            'num': '101'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Partnership\'s account number',
            'type': 'infoField',
            'inputType': 'custom',
            'format': [
              '{N9}RZ{N4}',
              'NR'
            ],
            'num': '102'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': 'Did the partnership elect to change its fiscal period-end? (note 1)',
            'labelWidth': '80%',
            'num': '103',
            init: '2'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The first tax year for which the corporation has QTI ends in',
            'labelWidth': '80%',
            'num': '108',
            'type': 'infoField',
            'inputType': 'dropdown',
            'options': [
              {
                'value': '2011',
                'option': '2011'
              },
              {
                'value': '2012',
                'option': '2012'
              },
              {
                'value': '2013',
                'option': '2013'
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 1. For tiered partnership structures for which no valid multi-tier alignment election was made, subsection 249.1(11) deems that a valid multi-tier alignment election has been made to end the fiscal period on December 31, 2011.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 – Eligible alignment income',
        'rows': [
          {
            'label': 'Complete Part 2 only once, for the year in which eligible alignment income arises. <b>Do not</b> complete it for other years. Calculate the eligible alignment income for the tax year if the partnership made a multi-tier fiscal period alignment election under subsection 249.1(9) to end the fiscal period of the partnership on a particular day that is before March 22, 2012, or if a deemed valid multi-tier alignment election resulted under subsection 249.1(11). Subsection 249.1(10) of the Act sets out conditions to meet before a multi-tier alignment election is valid.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s share of income of the partnership for the eligible fiscal period (note 2) (other than dividends for which a deduction is available under section 112 or 113 and any amount that would be included in calculating the income of the corporation if there was no multi-tier alignment)',
            'labelWidth': '440px'
          },
          {
            'type': 'table',
            'num': '201'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '205'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of loss of a partnership for the eligible fiscal period (note 2)',
            'labelWidth': '440px'
          },
          {
            'type': 'table',
            'num': '206'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '210'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount a <b>minus</b> amount b)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '211'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '212'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s share of taxable capital gain of the partnership for the eligible fiscal period (note 2) (other than any amount that would be included in calculating the income of the corporation if there was no multi-tier alignment)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '213'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of allowable capital loss of a partnership for the eligible fiscal period (note 2) (cannot be more than amount c).',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '214'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount c<b> minus</b> amount d)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '215'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '216'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Subtotal (amount A<b> plus</b> amount B)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '217'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Amounts deductible by the corporation (sections 66.1, 66.2, 66.21, and 66.4) for resource expenses deemed by subsection 66(18) to be incurred at the end of the eligible fiscal period (note 2) of the partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '218'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': '<b>Eligible alignment income</b> for the tax year (amount C minus amount D) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '219'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Enter amount E in column 8 of Schedule 73.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 2. The eligible fiscal period is the first aligned fiscal period of the partnership that ends in the tax year of the corporation. Page',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Allocation of amount E (carried to Schedule 73)',
        'rows': [
          {
            'label': 'Business income included in amount E',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '220',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Property income included in amount E',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '221',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in amount E',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '222',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in amount E',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '223',
                  'init': '0'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Amounts from tax year in which the eligible fiscal period ended',
        'rows': [
          {
            'label': 'Do not complete this section if the eligible fiscal period of the partnership ends in the current tax year of the corporation.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible alignment income (amount E) for the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '225',
                  'init': '0'
                }
              },
              null
            ]
          },
          {
            'label': 'Business income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '226',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Property income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '227',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '228',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '229',
                  'init': '0'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 3 – Adjusted stub period accrual (ASPA)',
        'rows': [
          {
            'label': 'A corporation\'s ASPA for a partnership gives an estimate of the income that the corporation is deferring as a result of its membership in a partnership that has a fiscal period that differs from the corporation\'s tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If a fiscal period of the partnership ends in the corporation\'s tax year and the year is the first tax year in which the fiscal period of the partnership is aligned with the fiscal period of one or more other partnerships under a multi-tier alignment, complete this part as follows:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'items': [
                  'if a fiscal period of the partnership ends in the corporation\'s tax year and <b>before</b> the first fiscal period of the partnership that is aligned under the multi-tier alignment (the eligible fiscal period), complete lines e to I [subparagraph (b)(i) of the definition of "adjusted stub period accrual" in  subsection 34.2(1)].',
                  'if the first fiscal period of the partnership that is aligned under the multi-tier alignment (the eligible fiscal period) is the <b>first</b> fiscal period of the partnership that ends in the corporation\'s tax year, complete lines j to O [subparagraph (b)(ii) of the definition of "adjusted stub period accrual" in subsection 34.2(1)].'
                ]
              }
            ]
          },
          {
            'label': 'If <b>none</b> of these situations above apply, complete lines o to S [paragraph (a) of the definition of "adjusted stub period accrual" in subsection 34.2(1)].',
            'labelClass': 'fullLength tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the corporation becomes a member of a partnership during a fiscal period of the partnership, see Part 4.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'label': 'Complete this section if a fiscal period of the partnership ends in the corporation\'s tax year and before the eligible fiscal period (note 3) ends [subparagraph (b)(i) of the definition of "adjusted stub period accrual" in subsection 34.2(1)]',
            'labelClass': 'fullLength bold center'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s share of income of the partnership for the <b>first</b> fiscal period that ends in the corporation\'s tax year (other than dividends for which a deduction is available under section 112 or 113)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '301'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '305'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of loss of the partnership for the <b>first</b> fiscal period that ends in the corporation\'s tax year',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'infoTable': true,
            'maxLoop': 100000,
            'num': '306',
            'hasTotals': true
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '310'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount e <b>minus</b> amount f) ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '311'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '312'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Corporation\'s share of taxable capital gain of the partnership for the <b>first</b> fiscal period that ends in the corporation\'s tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '313'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'g'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of allowable capital loss of the partnership for the <b>first</b> fiscal period that ends in the corporation\'s tax year (cannot be more than amount g)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '314'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'h'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount g <b>minus</b> amount h) ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '315'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '316'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Subtotal (amount F <b>plus</b> amount G) ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '317'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '318'
          },
          {
            'label': 'Stub period accrual (amount H <b>multiplied</b> by i) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '322'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Enter amount I in column 1 of Schedule 73.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'To calculate the corporation\'s ASPA for the tax year, complete line T to line V on page 5.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 3. The eligible <b>fiscal period</b> is the first aligned fiscal period of the partnership that ends in the tax year of the corporation.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'label': 'Complete this section if the eligible fiscal period (note 4) is the first fiscal period of the partnership that ends in the corporation’s tax year [subparagraph (b)(ii) of the definition of "adjusted stub period accrual" in subsection 34.2(1)]',
            'labelClass': 'fullLength bold center'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s share of income of the partnership for the eligible fiscal period (note 4)<br>(other than dividends for which a deduction is available under section 112 or 113)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '323'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '327'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'j'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of loss of the partnership for the eligible fiscal period (note 4)',
            'labelClass': 'fulLLength'
          },
          {
            'type': 'table',
            'maxLoop': 100000,
            'num': '328'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '332'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'k'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount j <b>minus</b> amount k) ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '333'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '334'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': 'Corporation\'s share of taxable capital gain of the partnership for the eligible fiscal period (note 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '335'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'l'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of allowable capital loss of the partnership for the eligible fiscal period (note 4) (cannot be more than amount l)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '336'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'm'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount l <b>minus</b> amount m) ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '337'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '338'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': 'Subtotal (amount J <b>plus</b> amount K) ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '339'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': 'Eligible alignment income (amount E from Part 2 ).',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '340'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'label': 'Subtotal (amount L <b>minus</b> amount M) ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '341'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'type': 'table',
            'num': '342'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Stub period accrual (amount N <b>multiplied</b> by n) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '346'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'label': 'Enter amount O in column 1 of Schedule 73.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'To calculate the corporation\'s ASPA for the tax year, complete line T to line V on page 5.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 4. The eligible <b>fiscal period</b> is the first aligned fiscal period of the partnership that ends in the tax year of the corporation.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'label': 'Complete this section if subparagraphs (b)(i) and (b)(ii) of the definition of "adjusted stub period accrual" in subsection 34.2(1) do not apply[paragraph (a) of the definition of "adjusted stub period accrual" in subsection 34.2(1)]',
            'labelClass': 'fullLength bold center'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s share of income of the partnership for the fiscal period(s) (note 5) that end(s) in the tax yearof the corporation (other than dividends for which a deduction is available under section 112 or 113)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'maxLoop': 100000,
            'num': '347'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '351'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'o'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of loss of the partnership for the fiscal period(s) (note 5) that end(s) in the taxyear of the corporation',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'maxLoop': 100000,
            'num': '352'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '356'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'p'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount o <b>minus</b> amount p)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '357'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '358'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'label': 'Corporation\'s share of taxable capital gain of the partnership for the fiscal period(s) (note 5) that end(s) in the tax year of the corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '359'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'q'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of allowable capital loss of the partnership for the fiscal period(s) (note 5) thatend(s) in the tax year of the corporation (cannot be more than amount q)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '360'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'r'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount q <b>minus</b> amount r) ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '361'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '362'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'Q'
          },
          {
            'label': 'Subtotal (amount P <b>plus</b> amount Q)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '363'
                }
              }
            ],
            'indicator': 'R'
          },
          {
            'type': 'table',
            'num': '364'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Stub period accrual (amount R <b>multiplied</b> by s) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '368'
                }
              }
            ],
            'indicator': 'S'
          },
          {
            'label': 'Enter amount S in column 1 of Schedule 73.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'To calculate the corporation\'s ASPA for the tax year, complete line T to line V on page 5.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 5. The corporation can have more than one fiscal period of the partnership that ends in its tax year. Enter the income or loss allocated to thecorporation for all of the fiscal periods.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 6. The number of days could be more than 365 if there is more than one fiscal period of the partnership that ends in the corporation\'s tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount I, O, or S, whichever applies',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '369'
                }
              }
            ],
            'indicator': 'T'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Designated qualified resource expenses [subsections 66.1(6), 66.2(5), 66.21(1), and 66.4(5)] (note 7) for the stub period. Enter amount t in column 2 of Schedule 73',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '370'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 't'
                }
              }
            ]
          },
          {
            'label': 'Discretionary amount designated by the corporation (note 8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '371'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'u'
                }
              }
            ]
          },
          {
            'label': 'Enter amount u in column 3 of Schedule 73.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal (amount t <b>plus</b> amount u)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '372'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '373'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'U'
          },
          {
            'label': '<b>ASPA</b> for the tax year (amount T <b>minus</b> amount U) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '374'
                }
              }
            ],
            'indicator': 'V'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 7. Subsection 34.2(6) gives the designated amount for qualified resource expenses. Once filed, the designation cannot be amended or revoked. The corporation can designate an amount as its qualified resource expense for the stub period in respect of a partnership to the extent the corporation gets in writing from the partnership, before the corporation\'s filing due date for the tax year for which the ASPA is being calculated, information identifying the relevant expenses. The relevant expenses are those identified by the partnership as being the corporation\'s qualified resource expenses incurred by the partnership, determined as if those expenses had been incurred by the partnership in its last fiscal period that ended in the tax year (that is, based on the corporation\'s share for the last fiscal period, and not at the end of the tax year). The amount designated cannot be more than the maximum amount that would be deductible by the corporation for the identified resource expenses under sections 66.1, 66.2, 66.21, and 66.4 in calculating its income if the partnership\'s fiscal period ended at the end of the corporation\'s tax year.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 8.The corporation can designate an amount (other than an amount included on line t) on its <i>T2 Corporation Income Tax Return</i> filed on or before the corporation\'s filing due date. Once filed, the designation cannot be amended or revoked. The corporation may have to include in its income an <b>income shortfall adjustment</b> to account for under-reported income when the corporation has made a discretionary designation to reduce the ASPA inclusion for a previous tax year. For the calculation of the income shortfall adjustment, see Part 8.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Allocation of amount V (carried to Schedule 73)',
        'rows': [
          {
            'type': 'splitInputs',
            'inputType': 'radio',
            'showValues': 'false',
            'rf': true,
            'init': '1',
            'label': 'Which is the amount that applies for amount T and carries to amount V?',
            'divisions': 1,
            'num': '399',
            'items': [
              {
                'label': 'Amount I',
                'value': '1'
              },
              {
                'label': 'Amount O',
                'value': '2'
              },
              {
                'label': 'Amount S',
                'value': '3'
              }
            ]
          },
          {
            'label': 'Business income included in amount V',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '380',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Property income included in amount V',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '381',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in amount V',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '382',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in amount V',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '383',
                  'init': '0'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'From prior year',
        'rows': [
          {
            'label': 'Adjusted stub period accrual from prior year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '385',
                  'init': '0'
                }
              },
              null
            ]
          },
          {
            'label': 'Business income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '386',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Property income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '387',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '388',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '389',
                  'init': '0'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 4 – Income inclusion for a new corporate member of a partnership',
        'rows': [
          {
            'label': 'If the corporation becomes a member of a partnership during a fiscal period of the partnership (the <b>particular period</b>) that starts in the corporation\'s tax year and ends after the tax year, but on or before the filing due date for that year, and the corporation has a significant interest in the partnership at the end of the particular period, the corporation may include in calculating the income for the tax year in respect of the partnership the lesser of:',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount, if any, designated by the corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '401'
                }
              }
            ],
            'indicator': 'W'
          },
          {
            'label': 'Use 0 as designated amount?',
            'input2': true,
            'num': '401A',
            'type': 'infoField',
            'width': '30px',
            'inputType': 'singleCheckbox',
            'labelCellClass': 'alignRight'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'and',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Corporation\'s income from the partnership for the particular period (other than dividends for which a deduction is available under section 112 or 113)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '410P'
          },
          {
            'type': 'table',
            'num': '410L'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '414'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'v'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '450'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal (amount v <b>multiplied</b> by w)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '431'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '432'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'X'
          },
          {
            'label': '<b>Income inclusion for a new corporate member of a partnership</b> for the tax year (amount W or X, whichever is less) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '437'
                }
              }
            ],
            'indicator': 'Y'
          },
          {
            'label': 'Enter amount Y in column 6 of Schedule 73.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Allocation of amount Y (carried to Schedule 73)',
        'rows': [
          {
            'label': 'Has an amount been designated by the corporation in amount W and carried to amount Y?',
            'labelClass': 'fullLength',
            'num': '499',
            'type': 'infoField',
            'inputType': 'radio'
          },
          {
            'label': 'Business income included in amount Y',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '438'
                }
              }
            ]
          },
          {
            'label': 'Property income included in amount Y',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '439'
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in amount Y',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '440'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in amount Y',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '441'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Amounts from previous tax year',
        'rows': [
          {
            'label': 'Income inclusion for a new corporate member of a partnership (amount Y) for the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '443'
                }
              },
              null
            ]
          },
          {
            'label': 'Business income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '444'
                }
              }
            ]
          },
          {
            'label': 'Property income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '445'
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '446'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '447'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 5 – Qualifying transitional income (QTI)',
        'rows': [
          {
            'label': 'In order for a corporation to have QTI, the corporation must have been a member of the partnership on March 22, 2011.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Complete this Part only once, for the year in which the QTI arises. <b>Do not</b> complete it for other years.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The QTI amount is used to calculate the transitional reserve in Part 7 of this schedule. The QTI is made up of the corporation\'s eligible alignment income and ASPA for the corporation\'s tax year during which ends the fiscal period of the partnership that is aligned with the fiscal period of one or more other partnerships under the multi-tier alignment, or, for any other case, for the first tax year that ends after March 22, 2011.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'A corporation that is eligible for transitional relief may first have QTI in respect of a particular partnership in its 2011, 2012, or 2013 tax year.',
            'labelClass': 'fullLength bold'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subsection 34.2(15) requires that for the purpose of QTI, the income or loss of a partnership for a fiscal period has to be calculated as if:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'items': [
                  {
                    'label': 'the partnership had deducted, for the period, the maximum amount deductible for any expense, reserve, allowance, or other amount;'
                  },
                  {
                    'label': 'the Act were read without reference to the flexible inventory adjustment rule in paragraph 28(1)(b); and'
                  },
                  {
                    'label': 'the partnership made a work-in-progress election under paragraph 34(a).'
                  }
                ]
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible alignment income for the tax year (amount E from Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '501'
                }
              }
            ],
            'indicator': 'Z'
          },
          {
            'label': 'ASPA for the tax year (amount V from Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '502'
                }
              }
            ],
            'indicator': 'AA'
          },
          {
            'label': '<b>Qualifying transitional income</b> (amount Z <b>plus</b> amount AA) (note 9)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '503'
                }
              }
            ],
            'indicator': 'BB'
          },
          {
            'label': 'Enter amount BB in column 9 of Schedule 73.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 9. If a corporation has QTI (or would have had QTI if the partnership had ASPA income), a one-time adjustment is calculated for the corporation\'s QTI in a later year. See Part 6.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Qualifying transitional income (QTI) - Previous taxation year',
        'rows': [
          {
            'label': 'Qualifying transitional income (QTI) - Previous taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '504'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 6 – Adjusted amount of qualifying transitional income',
        'rows': [
          {
            'label': 'Calculate an adjustment of the ASPA included in the qualified transitional income (QTI). The QTI adjustment occurs only once and in the particular tax year (note 10). Once the adjustment to a corporation\'s QTI in respect of a partnership is made, that QTI is the corporation\'s QTI in respect of the partnership for the particular tax year and each later tax year for calculating the transitional reserve in Part 7.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s share of income of the partnership for the particular period (note 11) (other thandividends for which a deduction is available under section 112 or 113)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'maxLoop': 100000,
            'num': '600'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '604'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'x'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of loss of the partnership for the particular period (note 11)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'maxLoop': 100000,
            'num': '641'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '609'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'y'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount x <b>minus</b> amount y)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '610'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '611'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'CC'
          },
          {
            'label': 'Corporation\'s share of taxable capital gain of the partnership for the particular period (note 11)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '612'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'z'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of allowable capital loss of the partnership for the particular period (note 11) (cannot be more than amount z)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '613'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'aa'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount z <b>minus</b> amount aa)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '614'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '615'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'DD'
          },
          {
            'label': 'Subtotal (amount CC <b>plus</b> amount DD)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '616'
                }
              }
            ],
            'indicator': 'EE'
          },
          {
            'labelClass': 'fulLLength'
          },
          {
            'label': 'Enter "0". Amount FF is "nil" due to the amendment of paragraph 34.2(17)(b)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '617'
                }
              }
            ],
            'indicator': 'FF'
          },
          {
            'label': 'Subtotal (amount EE )',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '618'
                }
              }
            ],
            'indicator': 'GG'
          },
          {
            'type': 'table',
            'num': '619'
          },
          {
            'label': 'Subtotal (amount GG <b>multiplied</b> by amount bb)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '623'
                }
              }
            ],
            'indicator': 'HH'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Designated qualified resource expenses [subsections 66.1(6), 66.2(5), 66.21(1), and 66.4(5)] for the particular period (note 11) of the partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '624'
                }
              }
            ],
            'indicator': 'II'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Adjusted amount of stub period accrual included in QTI</b> (amount HH <b>minus</b> amount II) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '625'
                }
              }
            ],
            'indicator': 'JJ'
          },
          {
            'label': '<b>Eligible alignment income for the previous tax year</b> (amount E from Part 2 of previous year\'s Schedule 72)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '626'
                }
              }
            ],
            'indicator': 'KK'
          },
          {
            'label': '<b>Adjusted amount of qualifying transitional income</b> (amount JJ <b>plus</b> amount KK)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '627'
                }
              }
            ],
            'indicator': 'LL'
          },
          {
            'label': 'Enter amount LL in column 10 of Schedule 73.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 10. The <b>particular tax year</b> is the first tax year:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'items': [
                  'that is after the tax year in which the corporation has (or would have, if the partnership had income) an ASPA that is (or would be) included in its QTI  in respect of the partnership; and',
                  'in which ends the fiscal period of the partnership that began in the tax year in which QTI in respect of the partnership was initially determined.'
                ]
              }
            ]
          },
          {
            'label': 'If the corporation\'s current tax year meets these conditions, the adjustment of QTI applies, even if the recalculation of the QTI results in no adjustment to the amount of QTI.',
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Note</b> <br>Although the <b>particular tax year</b> is normally the tax year right after the tax year in which a corporation\'s QTI in respect of a particular partnership is first determined, this is not necessarily the case. For example, the corporation can have a short tax year between the end of the tax year in  which the fiscal period of the partnership began and the beginning of the tax year in which that fiscal period ends.',
            'labelClass': 'tabbed fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 11. The <b>particular period</b> of the partnership is its fiscal period that starts in the corporation\'s first tax year for which the QTI was initially calculated and ends in the corporation\'s tax year (the <b>particular tax year</b>).',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 12. In which the QTI arose (or would have, if the partnership had income).',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Allocation of amount JJ (carried to Schedule 73)',
        'rows': [
          {
            'label': 'Business income included in amount JJ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '628'
                }
              }
            ]
          },
          {
            'label': 'Property income included in amount JJ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '629'
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in amount JJ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '630'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in amount JJ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '631'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Adjusted amount of qualifying transitional income',
        'rows': [
          {
            'label': 'If adjustment took place in a prior year, provide the QTI for the year of adjustment',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '640'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 7 – Transitional reserve',
        'rows': [
          {
            'label': 'Generally, a corporation is eligible for transitional relief if it is a partner of the partnership for which it had qualifying transitional income (QTI).',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subsections 34.2(13) and 34.2(18) set out circumstances under which the corporation may not claim, as a reserve, an amount in respect of a partnership under subsection 34.2(11).',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subsection 34.2(14) sets out conditions in which a reserve may continue to be claimed by a corporation that has disposed of its interest in a partnership.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'A corporation that has QTI that first arises in its 2011, 2012, or 2013 tax year in respect of a partnership may be eligible to claim a transitional reserve under subsection 34.2(11). Generally, the transitional reserve may allow a corporation to bring the QTI into its income over a period of up to five calendar years (note 13) that follows the tax year in which the QTI arose. The amount of transitional reserve claimed in a particular tax year cannot be more than the least of amounts MM, NN, and OO (for the first year in which QTI arose, this amount cannot be more than the least of amounts MM and OO):',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '700'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Previous tax year\'s reserve, <b>if</b> an amount was claimable (amount QQ from Part 7 of previous year\'s Schedule 72)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '705'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'cc'
                }
              }
            ]
          },
          {
            'label': 'Adjusted amount of QTI, if applicable (amount LL from Part 6 of Schedule 72 for the particular tax year)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': '',
            'columns': [
              {
                'input': {
                  'num': '706'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '1'
                }
              },
              null
            ]
          },
          {
            'label': 'QTI (note 16)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': '',
            'columns': [
              {
                'input': {
                  'num': '707'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '2'
                }
              },
              null
            ]
          },
          {
            'label': 'Amount by which QTI is increased, if it is the year in which the one-time QTI adjustment occurs. For other years, enter "0". (amount 1 <b>minus</b> amount 2) (if negative, enter "0")',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': '',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '708'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '709'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'dd'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount cc <b>plus</b> amount dd)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '710'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '711'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'NN'
          },
          {
            'label': 'Corporation\'s income for the particular tax year (before claiming the reserve and amounts under sections 61.3 and 61.4) less dividends deductible under section 112 or 113 that are received after December 20, 2012',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '712'
                }
              }
            ],
            'indicator': 'OO'
          },
          {
            'label': 'If an amount was claimable as a transitional reserve for the previous year, amount MM, NN, or OO, whichever is less. If no amount was claimable as a transitional reserve for the previous year, amount MM, or OO, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '713'
                }
              }
            ],
            'indicator': 'PP'
          },
          {
            'label': '<b>Transitional reserve</b> (cannot be more than amount PP)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '714'
                }
              }
            ],
            'indicator': 'QQ'
          },
          {
            'label': 'Enter amount QQ in column 11 of Schedule 73.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 13. This applies for all tax years of the corporation that end in that calendar year.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 14. Amount BB in Part 5 of Schedule 72 for the corporation\'s tax year during which ends the fiscal period of the partnership that is aligned with the fiscal period of one or more other partnerships under the multi-tier alignment or, if the QTI has been adjusted in a particular tax year, amount LL in Part 6 of Schedule 72 for that year and each later tax year.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 15. The specified percentage of the corporation for the tax year in respect of a partnership that can be claimed as a reserve is,',
            'labelClass': 'fullLength'
          },
          {
            'label': 'if the first tax year for which the corporation has QTI ends in 2011 and the tax year ends in: ',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• 2011, 100%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2012, 85%, ',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2013, 65%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2014, 45%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2015, 25%, and ',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2016, 0%;',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '',
            'labelClass': 'fullLength'
          },
          {
            'label': 'if the first tax year for which the corporation has QTI ends in 2012 and the tax year ends in:',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• 2012, 100%, ',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2013, 85%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2014, 65%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2015, 45%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2016, 25%, and',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2017, 0%.',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '',
            'labelClass': 'fullLength'
          },
          {
            'label': 'if the first tax year for which the corporation has QTI ends in 2013 and the tax year ends in:',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• 2013, 85%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2014, 65%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2015, 45%,',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2016, 25%, and',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• 2017, 0%.',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '',
            'labelClass': 'fullLength'
          },
          {
            'label': 'The first tax year for which the corporation has QTI can end in 2013 only if a multi-tier alignment takes place and the corporation\'s first tax year that includes the aligned fiscal period ends in 2013.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 16. Amount BB in Part 5 of Schedule 72 for the corporation\'s tax year during which ends the fiscal period of the partnership that is aligned with the fiscal period of one or more other partnerships under the multi-tier alignment.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Allocation of amount QQ (carried to Schedule 73)',
        'rows': [
          {
            'label': 'Business income included in amount QQ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '726'
                }
              }
            ]
          },
          {
            'label': 'Property income included in amount QQ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '727'
                }
              }
            ]
          },
          {
            'label': 'Other eligible income included in amount QQ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '728'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in amount QQ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '729'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Tracking the transitional reserve',
        'rows': [
          {
            'label': 'Table showing the deductible portion of the Transitional Reverse',
            'labelClass': 'bold fullLength'
          },
          {
            'type': 'table',
            'num': '720'
          }
        ]
      },
      {
        'header': 'Amounts from previous tax year',
        'rows': [
          {
            'label': 'Transitional reserve (amount QQ) for the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '780',
                  'init': '0'
                }
              },
              null
            ]
          },
          {
            'label': 'Business income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '781',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Property income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '782',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Other income included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '783',
                  'init': '0'
                }
              }
            ]
          },
          {
            'label': 'Net taxable capital gain included in carried forward amount',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '784',
                  'init': '0'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 8 – Income shortfall adjustment',
        'rows': [
          {
            'label': 'Complete this part only if the corporation designated a discretionary amount on line u in Part 3 of the <b>base year</b>\'s Schedule 72 for which paragraph (a) of the definition of "adjusted stub period accrual" in subsection 34.2(1) applies for any <b>qualifying partnership</b> the corporation is a member of. Section 34.3 may require a corporate partner of a partnership for a tax year to include in its income an <b>income shortfall adjustment</b> to account for under-reported income.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoBox',
            'width': '100%',
            'info': '<b>Where the corporation has QTI, the corporation has to include in its income an income' +
            ' shortfall adjustment only for tax years that are after the first tax year of the corporation to' +
            ' which the adjustment of QTI applied (note 17) for any qualifying partnership.</b>'
          },
          {
            'label': 'The <b>base year</b> is the preceding tax year of the corporation in which began the fiscal period of the qualifying partnership that ends in the corporation tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the corporate partner is a member of more than one <b>qualifying partnership</b>, the corporation can, in determining its income inclusion under section 34.3 for a tax year, offset an over-reported ASPA in respect of a qualifying partnership against an under-reported ASPA of another qualifying partnership.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'A <b>qualifying partnership</b> is a partnership that has a fiscal period that began in the previous tax year and ended in the tax year, and in respect of which the corporation had to calculate an ASPA for the previous tax year in which the fiscal period of the partnership began.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The <b>actual stub period accrual</b> is the recalculation of the ASPA in respect of a fiscal period of a partnership based on the pro-rated part of actual partnership income allocated to the corporation for the last fiscal period of the partnership that began in the base year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s share of income of the <b>qualifying partnership</b> for the last fiscal period that began in the <b>base year</b> (other than dividends for which a deduction was available under section 112 or 113)',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'maxLoop': 100000,
            'num': '800'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '804'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'ee'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of loss of the <b>qualifying partnership</b> for the last fiscal period that began in the <b>base year</b>',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'maxLoop': 100000,
            'num': '840'
          },
          {
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '809'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'ff'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount ee <b>minus</b> amount ff)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '810'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '811'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'RR'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporation\'s share of taxable capital gain (note 18) of the <b>qualifying partnership</b> for the last fiscal period that began in the <b>base year</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '812'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'gg'
                }
              }
            ]
          },
          {
            'label': 'Corporation\'s share of allowable capital loss (note 18) of the <b>qualifying partnership</b> for the last fiscal period that began in the <b>base year</b> (cannot be more than amount gg)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '813'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'hh'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount gg <b>minus</b> amount hh)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '814'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '815'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'SS'
          },
          {
            'label': 'Subtotal (amount RR <b>plus</b> amount SS)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '816'
                }
              }
            ],
            'indicator': 'TT'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '890'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal (amount TT <b>multiplied</b> by ii)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '821'
                }
              }
            ],
            'indicator': 'UU'
          },
          {
            'label': 'Amount of the qualified resource expense [subsections 66.1(6), 66.2(5), 66.21(1), and 66.4(5)] in respect of the <b>qualifying partnership</b> that was designated by the corporate partner for the <b>base year</b> (amount t from Part 3 of the base year\'s Schedule 72)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '822'
                }
              }
            ],
            'indicator': 'VV'
          },
          {
            'label': '<b>Actual stub period accrual</b> in respect of the qualifying partnership (amount UU <b>minus</b> amount VV)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '823'
                }
              }
            ],
            'indicator': 'WW'
          },
          {
            'label': 'Note 17. When the conditions in subsection 34.2(16) are met in a <b>particular tax year</b> (see definition in Part 6), the adjustment of QTI applies, even if the recalculation of the QTI results in no adjustment to the amount of QTI.<br><br>Note 18. In calculating the actual stub period accrual to determine the income inclusion, the corporation can offset all or part of the allowable capital losses from one qualifying partnership against all or part of the taxable capital gains from another qualifying partnership.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'ASPA from the base year (amount V from Part 3 of Schedule 72 for the base year)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '824'
                }
              }
            ],
            'indicator': 'XX'
          },
          {
            'label': 'Discretionary amount designated from the base year (amount u from Part 3 of Schedule 72 for the base year)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '825'
                }
              }
            ],
            'indicator': 'YY'
          },
          {
            'label': 'Subtotal (amount XX <b>plus</b> amount YY) (if negative, enter "0")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '826'
                }
              }
            ],
            'indicator': 'ZZ'
          },
          {
            'label': 'Base amount (amount WW or ZZ, whichever is less)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '827'
                }
              }
            ],
            'indicator': 'AAA'
          },
          {
            'label': 'Subtotal (amount AAA <b>minus</b> amount XX) (this amount can be positive or negative)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '828'
                }
              }
            ],
            'indicator': 'BBB'
          },
          {
            'label': 'Number of days in the period that starts after the day on which the <b>base year</b> ends, and ends on the day on which the tax year ends',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '829'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'jj'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount BBB <b>multiplied</b> by jj)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },

            'columns': [
              {
                'input': {
                  'num': '830'
                }
              }
            ],
            'indicator': 'CCC'
          },
          {
            'label': 'Enter the average daily rate of interest determined by reference to the prescribed rate ' +
            'of interest under paragraph 4301(a) of the <i>Income Tax Regulations</i> for the period referred ' +
            'to at line jj',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '831',
                  'decimals': 5
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'kk'
                }
              }
            ]
          },
          {
            'label': 'Income shortfall adjustment (amount CCC <b>multiplied</b> by kk)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '832'
                }
              }
            ],
            'indicator': 'DDD'
          },
          {
            'label': 'Enter amount DDD in column 14 of Schedule 73.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Base amount AAA <b>multiplied</b> by jj',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '833'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'll'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount ll <b>multiplied</b> by kk',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '834'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'mm'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '836'
          },
          {
            'label': 'Enter amount EEE in column 15 of Schedule 73',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  };
})();
