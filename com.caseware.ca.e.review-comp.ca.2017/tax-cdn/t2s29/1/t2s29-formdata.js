(function() {
  'use strict';

  wpw.tax.global.formData.t2s29 = {
      'formInfo': {
        'abbreviation': 'T2S29',
        'title': 'Payments to Non-Residents',
        //TODO: DO NOT DELETE THIS LINE: subtitle
        //'subTitle': '(1998 and later taxation years)',
        'schedule': 'Schedule 29',
        formFooterNum: 'T2 SCH 29 (99)',
        'showCorpInfo': true,
        headerImage: 'canada-federal',
        'description': [
          {
            'type': 'list',
            'items': [
              'A corporation that makes payments or credits amounts to non-residents under subsections 202(1) ' +
              'and 105(1) of the <i>Income Tax Regulations</i> has to file the applicable information',
              'The corporation has to complete the information below for all amounts paid or credited to ' +
              'non-residents that are listed in Note 1. If the total amount paid or credited is less than $100, ' +
              'you do not have to complete the information for that payee.'
            ]
          }
        ],
        category: 'Federal Tax Forms'
      },
      'sections': [
        {
          'hideFieldset': true,
          'rows': [
            {
              'type': 'table', 'num': '050'
            },
            {labelClass: 'fullLength'},
            {
              'type': 'table', 'num': '1000'
            }
          ]
        }
      ]
    };
})();
