(function() {
  wpw.tax.global.tableCalculations.t2s29 = {
    "050": {
      "showNumbering": true,
      "columns": [{
        "header": "Name (list each payee separately)",
        "num": "100",
        "width": "35%",
        "tn": "100",
        "validate": {"or":[{"length":{"min":"1","max":"75"}},{"check":"isEmpty"}]}, 
        type: 'text'
      },
        {
          "header": "Address",
          "num": "200",
          "width": "45%",
          "tn": "200",
          "type": "address"
        },
        {
          "header": "Payment code (see note 1)",
          "num": "300",
          "width": "10%",
          "tn": "300",
          "maxLength": "2",
          "type": "dropdown",
          "options": [{
            "value": "1",
            "option": "1- Royalties"
          },
            {
              "value": "2",
              "option": "2- Rent"
            },
            {
              "value": "3",
              "option": "3- Management fees/commissions"
            },
            {
              "value": "4",
              "option": "4- Technical assistance fees"
            },
            {
              "value": "5",
              "option": "5- Research and development fees"
            },
            {
              "value": "6",
              "option": "6- Interest"
            },
            {
              "value": "7",
              "option": "7- Dividends"
            },
            {
              "value": "8",
              "option": "8- Film payments*"
            },
            {
              "value": "9",
              "option": "9- Other services"
            }]
        },
        {
          "header": "Amount $",
          "num": "400",
          "width": "15%",
          "tn": "400",
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        }]
    },
    1000: {
      infoTable: true,
      fixedRows: true,
      columns: [
        {type: 'none', cellClass: 'alignLeft'},
        {type: 'none', cellClass: 'alignLeft'},
        {type: 'none', cellClass: 'alignLeft', colClass: 'std-input-width'},
        {type: 'none', cellClass: 'alignLeft'}
      ],
      cells: [
        {
          0: {label: 'Note 1: Enter the applicable payment code in column 300:'},
          1: {label: '1 – Royalties'},
          2: {label: '6 – Interest'}
        },
        {
          1: {label: '2 – Rents'},
          2: {label: '7 – Dividends'}
        },
        {
          1: {label: '3 – Management fees/commissions'},
          2: {label: '8 – Film payments:'},
          3: {label: '– a motion picture film, or'}
        },
        {
          1: {label: '4 – Technical assistance fees'},
          3: {label: '– a film or video tape for use in connection with television'}
        },
        {
          1: {label: '5 – Research and development fees'},
          2: {label: ' 9 – Other services'}
        }
      ]
    }
  }
})();
