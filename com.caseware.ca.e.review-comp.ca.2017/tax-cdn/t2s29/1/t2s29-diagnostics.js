(function() {
  wpw.tax.create.diagnostics('t2s29', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0290001', common.prereq(
        common.check(['t2j.170'], 'isChecked'),
        function(tools) {
          var table = tools.field('050');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne([row[0], row[1], row[2], row[3]], 'isNonZero');
          });
        }));

    diagUtils.diagnostic('0290002', common.prereq(common.requireFiled('T2S29'),
        function(tools) {
          var table = tools.field('050');
          return tools.checkAll(table.getRows(), function(row) {
            var columns = [row[0], row[1], row[2], row[3]];
            if (tools.checkMethod(columns, 'isNonZero'))
              return tools.requireAll(columns, 'isNonZero');
            else return true;
          });
        }));
  });
})();
