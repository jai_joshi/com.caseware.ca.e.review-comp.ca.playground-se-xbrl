(function() {

  wpw.tax.create.calcBlocks('l996s', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field, form) {
      // load options
      var historyField = form('l996').field('history');
      var historyData = historyField.get();
      var historyOptions = [];

      angular.forEach(historyData, function(data, key) {
        historyOptions.push({
          value: key,
          option: wpw.tax.utilities.toJsDate(wpw.tax.utilities.convertToDate(+key)).toString()
        })
      });
      field('100').config('options', historyOptions);

      //when option changes
      var key = field('100').get();
      var data = historyField.getKey(key);
      if (!angular.isUndefined(data)) {
        field('101').assign(data[101]);
        field('996').assign(data[996]);
      } else {
        calcUtils.removeValue(['101', '996'], true);
      }
    });
  });
})();
