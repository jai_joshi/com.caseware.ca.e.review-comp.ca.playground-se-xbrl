(function() {

  wpw.tax.global.formData.l996s = {
    formInfo: {
      abbreviation: 'L996S',
      title: 'Line 996 Summary - Amended Tax Return - Description of Changes',
      category: 'Workcharts'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            type: 'infoField',
            label: 'Please select date which amended tax return filed',
            num: '100',
            inputType: 'dropdown',
            labelWidth: '40%',
            width: '30%'
          },
          {
            type: 'infoField',
            label: 'Short summary of changes',
            num: '101',
            labelWidth: '40%',
            width: '30%',
            disabled: true
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            labelAfterNum: true,
            label: 'Detailed description of changes',
            labelWidth: '90%',
            inputType: 'none'
          },
          {
            type: 'infoField',
            inputType: 'textArea',
            maxLength: 500,
            lineLength: 78,
            tn: '996',
            num: '996',
            disabled: true,
            "validate": {"or":[{"length":{"min":"1","max":"78"}},{"check":"isEmpty"}]}
          }
        ]
      }
    ]
  };
})();
