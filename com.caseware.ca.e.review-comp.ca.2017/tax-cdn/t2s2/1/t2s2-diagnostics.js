(function() {

  function generateChecks(source, requireFilled) {
    return function(tools) {
      if (tools.field(source).isNonZero()) {
        var fields = tools.list(requireFilled);
        return tools.requireOne(fields, 'isNonZero');
      }
      return true;
    }
  }
  wpw.tax.create.diagnostics('t2s2', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0020001', common.prereq(common.check('t2j.202', 'isNonZero'), common.requireFiled('t2s2')));

    diagUtils.diagnostic('0020002', common.prereq(common.requireFiled('T2S2'), generateChecks('t2j.311', ['210', '240', '250'])));

    diagUtils.diagnostic('0020004', common.prereq(common.requireFiled('T2S2'), generateChecks('t2j.313', ['410', '440', '450'])));

    diagUtils.diagnostic('0020005', common.prereq(common.requireFiled('T2S2'), generateChecks('t2j.314', ['510', '520', '540', '550'])));

    diagUtils.diagnostic('0020006', common.prereq(common.requireFiled('T2S2'), generateChecks('t2j.315', ['610', '640', '650'])));
  });
})();
