(function() {

  var expandableColumns = [
    {type: 'none'},
    {
      colClass: 'std-padding-width',
      'type': 'none'
    },
    {
      colClass: 'std-input-width',
      header: 'Federal'
    },
    {
      colClass: 'std-padding-width',
      'type': 'none'
    },
    {
      colClass: 'std-input-width',
      header: 'Quebec',
      showWhen: {fieldId: 'provinces', has: {QC: true}}
    },
    {
      colClass: 'std-padding-width',
      'type': 'none',
      showWhen: {fieldId: 'provinces', has: {QC: true}}
    },
    {
      colClass: 'std-input-width',
      header: 'Alberta',
      showWhen: {fieldId: 'provinces', has: {AB: true}}
    },
    {
      colClass: 'std-padding-width',
      'type': 'none',
      showWhen: {fieldId: 'provinces', has: {QC: true}}
    }
  ];

  function getHistoricalTableColumns(totalNum1, totalNum2) {
    return [
      {
        colClass: 'std-input-width',
        type: 'date',
        header: '<b>Year of origin</b>'
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        type: 'none',
        total: true,
        totalNum: totalNum1,
        totalMessage: 'Total : ',
        disabled: true,
        header: '<b>Current year contribution</b>'
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        colClass: 'std-input-col-width',
        type: 'none',
        total: true,
        totalMessage: ' ',
        disabled: true,
        header: '<b>Amount transferred from wind-up</b>'
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        type: 'none',
        total: true,
        totalMessage: ' ',
        header: '<b>Adjustment</b>'
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        colClass: 'std-input-col-width',
        total: true, totalMessage: ' ',
        header: '<b>Amount available to apply</b>'
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        total: true, totalNum: totalNum2, totalMessage: ' ', disabled: true,
        header: '<b>Applied<b>'
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        total: true, totalMessage: ' ', disabled: true,
        header: '<b>Balance to carry forward</b>'
      },
      {type: 'none', colClass: 'std-padding-width'}
    ]
  }

  var historicalTable5YearsCells = [
    {
      9: {label: '*'},
      10: {type: 'none'},
      12: {type: 'none'}
    },
    {
      13: {label: '**'}
    },
    {},
    {},
    {},
    {},
    {
      2: {type: ''},
      4: {type: ''},
      6: {type: ''}
    }
  ];

  var historicalTable10YearsCells = [
    {
      9: {label: '*'},
      10: {type: 'none'},
      12: {type: 'none'}
    },
    {
      13: {label: '**'}
    },
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {
      2: {type: ''},
      4: {type: ''},
      6: {type: ''}
    }
  ];

  var historicalTable20YearsCells = [
    {
      9: {label: '*'},
      10: {type: 'none'},
      12: {type: 'none'}
    },
    {
      13: {label: '**'}
    },
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {
      2: {type: ''},
      4: {type: ''},
      6: {type: ''}
    }
  ];

  var part6Columns = expandableColumns.slice();
  part6Columns.splice(0, 1,
      {width: '20px', type: 'none', cellClass: 'alignCenter'},
      {colClass: 'std-input-width'},
      {width: '30px', type: 'none', cellClass: 'alignCenter'},
      {colClass: 'std-input-width'},
      {type: 'none', cellClass: 'alignCenter'}
  );

  wpw.tax.global.tableCalculations.t2s2 = {
    '100': {
      type: 'table', num: '100', infoTable: true, fixedRows: true,
      columns: expandableColumns,
      cells: [
        {
          0: {'label': 'Charitable donations at the end of the previous tax year'},
          2: {num: '238'},
          3: {label: 'A'},
          4: {num: '238-Q'},
          6: {num: '238-A'}
        },
        {
          0: {'label': '<b>Deduct:</b>'},
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {'label': 'Charitable donations expired after five tax years'},
          1: {tn: '239'},
          2: {
            num: '239',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          4: {num: '239-Q', cellClass: 'singleUnderline'},
          6: {num: '239-A', cellClass: 'singleUnderline'}
        },
        {
          0: {'label': 'Charitable donations at the beginning of the current tax year'},
          1: {tn: '240'},
          2: {num: '240', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          3: {label: 'B'},
          4: {num: '240-Q'},
          6: {num: '240-A'}
        },
        {
          0: {'label': 'Add:', labelClass: 'fullLength bold'},
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            'label': 'Charitable donations transferred on an amalgamation or the wind-up of a subsidiary',
            'labelClass': 'tabbed'
          },
          1: {tn: '250'},
          2: {num: '250', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          4: {num: '250-Q'},
          6: {num: '250-A'}
        },
        {
          0: {
            'label': 'Total charitable donations made in the current year <br>' +
            '(include this amount on line 112 of Schedule 1)',
            'labelClass': 'tabbed'
          },
          1: {tn: '210'},
          2: {
            num: '210',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          },
          4: {num: '210-Q', cellClass: 'singleUnderline'},
          6: {num: '210-A', cellClass: 'singleUnderline'}
        },
        {
          0: {'label': 'Subtotal (line 250 <b>plus</b> line 210)', 'labelClass': 'fullLength text-right'},
          2: {num: '251'},
          3: {label: 'C'},
          4: {num: '251-Q'},
          6: {num: '251-A'}
        },
        {
          0: {'label': 'Subtotal (amount B <b>plus</b> amount C)', 'labelClass': 'fullLength text-right'},
          2: {num: '252', labelClass: 'doubleUnderline'},
          3: {label: 'D'},
          4: {num: '252-Q', labelClass: 'doubleUnderline'},
          6: {num: '252-A', labelClass: 'doubleUnderline'}
        },
        {
          0: {'label': '<b>Deduct:</b>'},
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {'label': 'Adjustment for an acquisition of control'},
          1: {tn: '255'},
          2: {
            num: '255',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          },
          4: {num: '255-Q', cellClass: 'singleUnderline'},
          6: {num: '255-A', cellClass: 'singleUnderline'}
        },
        {
          0: {'label': 'Total charitable donations available (amount D <b>minus</b> amount on line 255)'},
          2: {num: '256'},
          3: {label: 'E'},
          4: {num: '256-Q'},
          6: {num: '256-A'}
        },
        {
          0: {'label': '<b>Deduct:</b>'},
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            'label': 'Amount applied in the current year against taxable income' +
            '(cannot be more than amount O in Part 2)<br>(enter this amount on line 311 of the T2 return)',
            labelClass: 'tabbed'
          },
          1: {tn: '260'},
          2: {
            num: '260',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          4: {num: '260-Q', cellClass: 'singleUnderline'},
          6: {num: '260-A', cellClass: 'singleUnderline'}
        },
        {
          0: {'label': 'Charitable donations closing balance (amount E <b>minus</b> amount on line 260)'},
          1: {tn: '280'},
          2: {
            num: '280',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'doubleUnderline'
          },
          4: {num: '280-Q', cellClass: 'doubleUnderline'},
          6: {num: '280-A', cellClass: 'doubleUnderline'}
        },
        {
          0: {
            'label': 'The amount of qualifying donations for the Ontario community food program donation tax credit for farmers included in the amount on line 260 (for donations made after December 31, 2013)'
          },
          1: {tn: '262'},
          2: {
            num: '262',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'doubleUnderline'
          },
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            'label': 'Ontario community food program donation tax credit for farmers<br> ' +
            '(amount on line 262 <b> multiplied </b> by 25%)'
          },
          2: {num: '262-1', cellClass: 'doubleUnderline'},
          3: {label: '1'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: 'Enter amount 1 on line 420 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>. ' +
            'The maximum amount you can claim in the<br> current year is whichever is less: the Ontario income tax otherwise' +
            ' payable or amount 1. For more information, see section 103.1.2 of the <i>Taxation Act, 2007</i> (Ontario).',
            labelClass: 'fullLength'
          },
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            'label': 'The amount of qualifying donations for the Nova Scotia food bank tax credit for farmers included in the amount on line 260 (for donations made after December 31, 2015)'
          },
          1: {tn: '263'},
          2: {
            num: '263',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            labelClass: 'doubleUnderline'
          },
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            'label': 'Nova Scotia food bank tax credit for farmers<br> ' +
            '(amount on line 263 <b>multiplied</b> by 25%)'
          },
          2: {num: '263-1', cellClass: 'doubleUnderline'},
          3: {label: '2'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: 'Enter amount 2 on line 570 of Schedule 5, <i>Tax Calculation Supplementary – Corporations</i>. ' +
            'The maximum amount you can claim in the <br>current year is whichever is less: the Nova Scotia income tax otherwise payable or amount 2. For more information, see section 50A of<br> the Nova Scotia Income Tax Act.'
          },
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: 'The amount of qualifying gifts for the British Columbia farmers\' food donation tax credit included<br>  ' +
            'in the amount on line 260 (for donations made after February 16, 2016 and before <br>January 1, 2019).'
          },
          1: {tn: '265'},
          2: {
            num: '265',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            labelClass: 'doubleUnderline'
          },
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            'label': 'British Columbia farmers\' food donation tax credit<br>' +
            '(amount on line 265 <b>multiplied</b> by 25%)'
          },
          2: {num: '265-1', labelClass: 'doubleUnderline'},
          3: {label: '3'},
          4: {type: 'none'},
          6: {type: 'none'}
        }
      ]
    },
    '400': {
      type: 'table', num: '400', infoTable: true, fixedRows: true,
      columns: expandableColumns,
      cells: [
        {
          0: {'label': 'Gifts of certified cultural property at the end of the previous tax year'},
          2: {num: '438'},
          3: {label: 'A'},
          4: {num: '438-Q'},
          6: {num: '438-A'}
        },
        {
          0: {'label': 'Deduct:', labelClass: 'fullLength bold'},
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {'label': 'Gifts of certified cultural property expired after five tax years'},
          1: {tn: '439'},
          2: {
            num: '439',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          },
          4: {num: '439-Q', cellClass: 'singleUnderline'},
          6: {num: '439-A', cellClass: 'singleUnderline'}
        },
        {
          0: {'label': 'Gifts of certified cultural property at the beginning of the current tax year'},
          1: {tn: '440'},
          2: {num: '440', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          3: {label: 'B'},
          4: {num: '440-Q'},
          6: {num: '440-A'}
        },
        {
          0: {'label': 'Add:', labelClass: 'fullLength bold'},
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            'label': 'Gifts of certified cultural property transferred on an amalgamation or the wind-up of a subsidiary',
            'labelClass': 'tabbed'
          },
          1: {tn: '450'},
          2: {num: '450', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          4: {num: '450-Q'},
          6: {num: '450-A'}
        },
        {
          0: {
            'label': 'Total gifts of certified cultural property in the current year <br> ' +
            '(include this amount on line 112 of Schedule 1)',
            'labelClass': 'tabbed'
          },
          1: {tn: '410'},
          2: {
            num: '410',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          },
          4: {num: '410-Q', cellClass: 'singleUnderline'},
          6: {num: '410-A', cellClass: 'singleUnderline'}
        },
        {
          0: {'label': 'Subtotal (line 450 <b>plus</b> line 410)', 'labelClass': 'fullLength text-right'},
          2: {num: '411', cellClass: 'singleUnderline'},
          3: {label: 'C'},
          4: {num: '411-Q', cellClass: 'singleUnderline'},
          6: {num: '411-A', cellClass: 'singleUnderline'}
        },
        {
          0: {'label': 'Subtotal (amount B <b>plus</b> amount C)', 'labelClass': 'fullLength text-right'},
          2: {num: '413', labelClass: 'doubleUnderline'},
          3: {label: 'D'},
          4: {num: '413-Q', labelClass: 'doubleUnderline'},
          6: {num: '413-A', labelClass: 'doubleUnderline'}
        },
        {
          0: {'label': 'Deduct:', labelClass: 'fullLength bold'},
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            'label': 'Adjustment for an acquisition of control',
            labelClass: 'tabbed'
          },
          1: {tn: '455'},
          2: {num: '455', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          4: {num: '455-Q'},
          6: {num: '455-A'}
        },
        {
          0: {
            'label': 'Amount applied in the current year against taxable income <br> (enter this amount on line 313 ' +
            'of the T2 return)'
          },
          1: {tn: '460'},
          2: {
            num: '460',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          },
          4: {num: '460-Q', cellClass: 'singleUnderline'},
          6: {num: '460-A', cellClass: 'singleUnderline'}
        },
        {
          0: {'label': 'Subtotal (line 455 <b>plus</b> line 460)', 'labelClass': 'fullLength text-right'},
          2: {num: '461', cellClass: 'singleUnderline'},
          3: {label: 'E'},
          4: {num: '461-Q', cellClass: 'singleUnderline'},
          6: {num: '461-A', cellClass: 'singleUnderline'}
        },
        {
          0: {
            'label': 'Gifts of certified cultural property closing balance ' +
            '(amount D <b>minus</b> amount E) .'
          },
          1: {tn: '480'},
          2: {
            num: '480',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'doubleUnderline'
          },
          4: {num: '480-Q', cellClass: 'doubleUnderline'},
          6: {num: '480-A', cellClass: 'doubleUnderline'}
        }
      ]
    },
    '500': {
      type: 'table', num: '500', infoTable: true, fixedRows: true,
      columns: expandableColumns,
      cells: [
        {
          0: {'label': 'Gifts of certified ecologically sensitive land at the end of the previous tax year'},
          2: {num: '538'},
          3: {label: 'F'},
          4: {num: '538-Q'},
          6: {num: '538-A'}
        },
        {
          0: {'label': 'Deduct:', labelClass: 'fullLength bold'},
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            'label': 'Gifts of certified ecologically sensitive land expired after 5 tax years, <br>' +
            'or after 10 tax years for gifts made after February 10, 2014'
          },
          1: {tn: '539'},
          2: {
            num: '539',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          4: {num: '539-Q', cellClass: 'singleUnderline'},
          6: {num: '539-A', cellClass: 'singleUnderline'}
        },
        {
          0: {
            'label': 'Gifts of certified ecologically sensitive land at the beginning of the current tax year'
          },
          1: {tn: '540'},
          2: {num: '540', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          3: {label: 'G'},
          4: {num: '540-Q'},
          6: {num: '540-A'}
        },
        {
          0: {'label': 'Add:', labelClass: 'fullLength bold'},
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        // {
        //   0: {
        //     'label': 'Gifts of certified ecologically sensitive land transferred on an amalgamation ' +
        //     'or the windup of a subsidiary before February 11, 2014',
        //     'labelClass': 'tabbed'
        //   },
        //   2: {num: '548'},
        //   4: {num: '548-Q'},
        //   6: {num: '548-A'}
        // },
        // {
        //   0: {
        //     'label': 'Gifts of certified ecologically sensitive land transferred on an amalgamation ' +
        //     'or the windup of a subsidiary after February 11, 2014',
        //     'labelClass': 'tabbed'
        //   },
        //   2: {num: '549'},
        //   4: {num: '549-Q'},
        //   6: {num: '549-A'}
        // },
        {
          0: {
            'label': 'Gifts of certified ecologically sensitive land transferred on an amalgamation or the wind-up of a subsidiary',
            'labelClass': 'tabbed'
          },
          1: {tn: '550'},
          2: {num: '550', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          4: {num: '550-Q'},
          6: {num: '550-A'}
        },
        {
          0: {
            'label': 'Total current-year gifts of certified ecologically sensitive land made before' +
            'February 11, 2014 (include this amount on line 112 of Schedule 1)',
            'labelClass': 'tabbed'
          },
          1: {tn: '510'},
          2: {num: '510', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          4: {type: 'none'},
          6: {num: '510-A'}
        },
        {
          0: {
            'label': 'Total current-year gifts of certified ecologically sensitive land made after' +
            'February 10, 2014 (include this amount on line 112 of Schedule 1)',
            'labelClass': 'tabbed'
          },
          1: {tn: '520'},
          2: {
            num: '520',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          },
          4: {type: 'none', cellClass: 'singleUnderline'},
          6: {num: '520-A', cellClass: 'singleUnderline'}
        },
        {
          0: {
            'label': 'Subtotal (<b>add</b> lines 550, 510, and 520)',
            'labelClass': 'fullLength text-right'
          },
          2: {num: '511', cellClass: 'singleUnderline'},
          3: {label: 'H'},
          4: {num: '511-Q', cellClass: 'singleUnderline'},
          6: {num: '511-A', cellClass: 'singleUnderline'}
        },
        {
          0: {'label': 'Subtotal (amount G <b>plus</b> amount H)', 'labelClass': 'fullLength text-right'},
          2: {num: '513', labelClass: 'doubleUnderline'},
          3: {label: 'I'},
          4: {num: '513-Q', labelClass: 'doubleUnderline'},
          6: {num: '513-A', labelClass: 'doubleUnderline'}
        },
        {
          0: {'label': 'Deduct:', labelClass: 'fullLength bold'},
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            'label': 'Adjustment for an acquisition of control',
            labelClass: 'tabbed'
          },
          1: {tn: '555'},
          2: {num: '555', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          4: {num: '555-Q'},
          6: {num: '555-A'}
        },
        {
          0: {
            'label': 'Amount applied in the current year against taxable income<br> ' +
            '(enter this amount on line 314 of the T2 return)',
            labelClass: 'tabbed'
          },
          1: {tn: '560'},
          2: {
            num: '560',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          },
          4: {num: '560-Q', cellClass: 'singleUnderline'},
          6: {num: '560-A', cellClass: 'singleUnderline'}
        },
        {
          0: {'label': 'Subtotal (line 555 <b>plus</b> line 560)', 'labelClass': 'fullLength text-right'},
          2: {num: '561', cellClass: 'singleUnderline'},
          3: {label: 'J'},
          4: {num: '561-Q', cellClass: 'singleUnderline'},
          6: {num: '561-A', cellClass: 'singleUnderline'}
        },
        {
          0: {
            'label': 'Gifts of certified ecologically sensitive land closing balance (amount I <b>minus</b> amount J)'
          },
          1: {tn: '580'},
          2: {
            num: '580',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'doubleUnderline'
          },
          4: {num: '580-Q', cellClass: 'doubleUnderline'},
          6: {num: '580-A', cellClass: 'doubleUnderline'}
        }
      ]
    },
    '599': {
      type: 'table', num: '599', infoTable: true, fixedRows: true,
      columns: expandableColumns,
      cells: [
        {
          0: {'label': 'Additional deduction for gifts of medicine at the end of the previous tax year'},
          2: {num: '638'},
          3: {label: 'K'},
          4: {num: '638-Q'},
          6: {num: '638-A'}
        },
        {
          0: {'label': 'Deduct:', labelClass: 'fullLength bold'},
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            'label': 'Additional deduction for gifts of medicine expired ' +
            'after five tax years'
          },
          1: {tn: '639'},
          2: {
            num: '639',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          },
          4: {num: '639-Q', cellClass: 'singleUnderline'},
          6: {num: '639-A', cellClass: 'singleUnderline'}
        },
        {
          0: {'label': 'Additional deduction for gifts of medicine at the beginning of the current tax year'},
          1: {tn: '640'},
          2: {num: '640', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          3: {label: 'L'},
          4: {num: '640-Q'},
          6: {num: '640-A'}
        },
        {
          0: {'label': 'Add:', labelClass: 'fullLength bold'},
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            'label': 'Additional deduction for gifts of medicine transferred on an amalgamation or the ' +
            'wind-up of a subsidiary',
            'labelClass': 'tabbed'
          },
          1: {tn: '650'},
          2: {num: '650', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          4: {num: '650-Q'},
          6: {num: '650-A'}
        },
        {
          0: {
            'label': '<br> Additional deduction for gifts of medicine for the current year: <br>',
            'labelClass': 'tabbed'
          },
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            'label': 'Proceeds of disposition',
            'labelClass': 'tabbed'
          },
          1: {tn: '602'},
          2: {num: '602', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          3: {label: '1'},
          4: {num: '602-Q'},
          5: {label: '1'},
          6: {num: '602-A'},
          7: {label: '1'}
        },
        {
          0: {
            'label': 'Cost of gifts of medicine',
            'labelClass': 'tabbed'
          },
          1: {tn: '601'},
          2: {
            num: '601',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          },
          3: {label: '2'},
          4: {num: '601-Q', cellClass: 'singleUnderline'},
          5: {label: '2'},
          6: {num: '601-A', cellClass: 'singleUnderline'},
          7: {label: '2'}
        },
        {
          0: {
            'label': 'Subtotal (line 1 <b>minus</b> line 2)',
            'labelClass': 'fullLength text-right'
          },
          2: {num: '603'},
          3: {label: '3'},
          4: {num: '603-Q'},
          5: {label: '3'},
          6: {num: '603-A'},
          7: {label: '3'}
        },
        {
          0: {
            'label': 'Line 3 <b>multiplied</b> by 50%',
            'labelClass': 'tabbed'
          },
          2: {num: '604'},
          3: {label: '4'},
          4: {num: '604-Q'},
          5: {label: '4'},
          6: {num: '604-A'},
          7: {label: '4'}
        },
        {
          0: {
            'label': 'Eligible amount of gifts',
            'labelClass': 'tabbed'
          },
          1: {tn: '600'},
          2: {num: '600', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          3: {label: '5'},
          4: {num: '600-Q'},
          5: {label: '5'},
          6: {num: '600-A'},
          7: {label: '5'}
        }
      ]
    },
    '900': {
      type: 'table', fixedRows: true, num: '900', infoTable: true,
      columns: part6Columns,
      cells: [
        {
          0: {label: ' a ', labelClass: 'center'},
          1: {num: '901'},
          2: {label: ' x b ', labelClass: 'center', cellClass: 'singleUnderline'},
          3: {num: '902'},
          4: {label: ' = Additional deduction for gifts of medicine for the current year ', labelClass: 'center'},
          5: {tn: '610'},
          6: {
            num: '610',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          },
          8: {type: 'none'},
          10: {type: 'none'}
        },
        {
          1: {type: 'none'},
          2: {label: ' c ', labelClass: 'center'},
          3: {num: '903'},
          4: {type: 'none'},
          6: {type: 'none'},
          8: {type: 'none'},
          10: {type: 'none'}
        },
        {
          showWhen: {fieldId: 'provinces', has: {QC: true}},
          0: {label: ' Quebec ', labelClass: 'bold underline'},
          1: {type: 'none'},
          3: {type: 'none'},
          6: {type: 'none'},
          8: {type: 'none'},
          10: {type: 'none'}
        },
        {
          showWhen: {fieldId: 'provinces', has: {QC: true}},
          0: {label: ' a ', labelClass: 'center'},
          1: {num: '901-Q'},
          2: {label: ' x b ', labelClass: 'center', cellClass: 'singleUnderline'},
          3: {num: '902-Q'},
          4: {label: ' = Additional deduction for gifts ', labelClass: 'center'},
          6: {type: 'none'},
          8: {num: '610-Q', cellClass: 'singleUnderline'},
          10: {type: 'none'}
        },
        {
          showWhen: {fieldId: 'provinces', has: {QC: true}},
          1: {type: 'none'},
          2: {label: ' c ', labelClass: 'center'},
          3: {num: '903-Q'},
          4: {label: ' of medicine for the current year  ', labelClass: 'center'},
          6: {type: 'none'},
          8: {type: 'none'},
          10: {type: 'none'}
        },
        {
          showWhen: {fieldId: 'provinces', has: {AB: true}},
          0: {label: ' Alberta ', labelClass: 'bold underline'},
          1: {type: 'none'},
          3: {type: 'none'},
          6: {type: 'none'},
          8: {type: 'none'},
          10: {type: 'none'}
        },
        {
          showWhen: {fieldId: 'provinces', has: {AB: true}},
          0: {label: ' a ', labelClass: 'center'},
          1: {num: '901-A'},
          2: {label: ' x b ', labelClass: 'center', cellClass: 'singleUnderline'},
          3: {num: '902-A'},
          4: {label: ' = Additional deduction for gifts ', labelClass: 'center'},
          6: {type: 'none'},
          8: {type: 'none'},
          10: {num: '610-A', cellClass: 'singleUnderline'}
        },
        {
          showWhen: {fieldId: 'provinces', has: {AB: true}},
          1: {type: 'none'},
          2: {label: ' c ', labelClass: 'center'},
          3: {num: '903-A'},
          4: {label: ' of medicine for the current year  ', labelClass: 'center'},
          6: {type: 'none'},
          8: {type: 'none'},
          10: {type: 'none'}
        }
      ]
    },
    '950': {
      type: 'table', num: '950', infoTable: true, fixedRows: true,
      columns: expandableColumns,
      cells: [
        {
          0: {
            'label': 'where: <br>' +
            '<b>a</b> is the <b>lesser</b> of line 2 and line 4 <br>' +
            '<b>b</b>' + ' is the eligible amount of gifts (line 600)' + '<br>' +
            '<b>c</b>' + ' is the proceeds of disposition (line 602)' + '<br>',
            'labelClass': 'tabbed'
          },
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            'label': 'Subtotal (line 650 <b>plus</b> line 610)',
            'labelClass': 'fullLength text-right'
          },
          2: {num: '611'},
          3: {label: 'M'},
          4: {num: '611-Q'},
          6: {num: '611-A'}
        },
        {
          0: {
            'label': 'Subtotal (amount L <b>plus</b> amount M)',
            'labelClass': 'fullLength text-right'
          },
          2: {num: '613', labelClass: 'doubleUnderline'},
          3: {label: 'N'},
          4: {num: '613-Q', labelClass: 'doubleUnderline'},
          6: {num: '613-A', labelClass: 'doubleUnderline'}
        },
        {
          0: {'label': 'Deduct:', labelClass: 'fullLength bold'},
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {
            label: 'Adjustment for an acquisition of control',
            labelClass: 'tabbed'
          },
          1: {tn: '655'},
          2: {num: '655', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          4: {num: '655-Q'},
          6: {num: '655-A'}
        },
        {
          0: {
            label: 'Amount applied in the current year against taxable income' + '<br>' +
            '(enter this amount on line 315 of the T2 return)',
            labelClass: 'tabbed'
          },
          1: {tn: '660'},
          2: {
            num: '660',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          },
          4: {num: '660-Q', cellClass: 'singleUnderline'},
          6: {num: '660-A', cellClass: 'singleUnderline'}
        },
        {
          0: {
            label: 'Subtotal (line 655 ' + '<b>plus</b> ' + 'line 660) ',
            labelClass: 'fullLength text-right'
          },
          2: {num: '661', cellClass: 'singleUnderline'},
          3: {label: 'O'},
          4: {num: '661-Q', cellClass: 'singleUnderline'},
          6: {num: '661-A', cellClass: 'singleUnderline'}
        },
        {
          0: {
            label: 'Additional deduction for gifts of medicine closing balance ' +
            '(amount N <b>minus</b> amount O)',
            labelClass: 'tabbed'
          },
          1: {tn: '680'},
          2: {
            num: '680',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'doubleUnderline'
          },
          4: {num: '680-Q', labelClass: 'doubleUnderline'},
          6: {num: '680-A', labelClass: 'doubleUnderline'}
        }
      ]
    },
    '5011': {
      fixedRows: true, num: '5011', infoTable: true, hasTotals: true, avoidRunOnCellsLoad: true,
      columns: getHistoricalTableColumns('5012', '5013'),
      cells: historicalTable5YearsCells
    },
    '5031': {
      type: 'table', fixedRows: true, num: '5031', infoTable: true, hasTotals: true, avoidRunOnCellsLoad: true,
      columns: getHistoricalTableColumns('5032', '5033'),
      cells: historicalTable5YearsCells
    },
    '5041': {
      type: 'table', fixedRows: true, num: '5041', infoTable: true, hasTotals: true, avoidRunOnCellsLoad: true,
      columns: getHistoricalTableColumns('5042', '5043'),
      cells: historicalTable5YearsCells
    },
    '5051': {
      type: 'table', fixedRows: true, num: '5051', infoTable: true, hasTotals: true, avoidRunOnCellsLoad: true,
      columns: getHistoricalTableColumns('5052', '5053'),
      cells: historicalTable10YearsCells
    },
    '5061': {
      type: 'table', fixedRows: true, num: '5061', infoTable: true, hasTotals: true, avoidRunOnCellsLoad: true,
      columns: getHistoricalTableColumns('5062', '5063'),
      cells: historicalTable5YearsCells
    },
    '6011': {
      type: 'table', fixedRows: true, num: '6011', infoTable: true, hasTotals: true, avoidRunOnCellsLoad: true,
      showWhen: {fieldId: 'provinces', has: {AB: true}},
      columns: getHistoricalTableColumns('6012', '6013'),
      cells: historicalTable5YearsCells
    },
    '6031': {
      type: 'table', fixedRows: true, num: '6031', infoTable: true, hasTotals: true, avoidRunOnCellsLoad: true,
      showWhen: {fieldId: 'provinces', has: {AB: true}},
      columns: getHistoricalTableColumns('6032', '6033'),
      cells: historicalTable5YearsCells
    },
    '6041': {
      type: 'table', fixedRows: true, num: '6041', infoTable: true, hasTotals: true, avoidRunOnCellsLoad: true,
      showWhen: {fieldId: 'provinces', has: {AB: true}},
      columns: getHistoricalTableColumns('6042', '6043'),
      cells: historicalTable5YearsCells
    },
    '6051': {
      type: 'table', fixedRows: true, num: '6051', infoTable: true, hasTotals: true, avoidRunOnCellsLoad: true,
      showWhen: {fieldId: 'provinces', has: {AB: true}},
      columns: getHistoricalTableColumns('6052', '6053'),
      cells: historicalTable10YearsCells
    },
    '6061': {
      type: 'table', fixedRows: true, num: '6061', infoTable: true, hasTotals: true, avoidRunOnCellsLoad: true,
      showWhen: {fieldId: 'provinces', has: {AB: true}},
      columns: getHistoricalTableColumns('6062', '6063'),
      cells: historicalTable5YearsCells
    },
    '7011': {
      type: 'table', fixedRows: true, num: '7011', infoTable: true, hasTotals: true, avoidRunOnCellsLoad: true,
      showWhen: {fieldId: 'provinces', has: {QC: true}},
      columns: getHistoricalTableColumns('7012', '7013'),
      cells: historicalTable20YearsCells
    },
    '7031': {
      type: 'table', fixedRows: true, num: '7031', infoTable: true, hasTotals: true, avoidRunOnCellsLoad: true,
      showWhen: {fieldId: 'provinces', has: {QC: true}},
      columns: getHistoricalTableColumns('7032', '7033'),
      cells: historicalTable20YearsCells
    },
    '7041': {
      type: 'table', fixedRows: true, num: '7041', infoTable: true, hasTotals: true, avoidRunOnCellsLoad: true,
      showWhen: {fieldId: 'provinces', has: {QC: true}},
      columns: getHistoricalTableColumns('7042', '7043'),
      cells: historicalTable20YearsCells
    },
    '7061': {
      type: 'table', fixedRows: true, num: '7061', infoTable: true, hasTotals: true, avoidRunOnCellsLoad: true,
      showWhen: {fieldId: 'provinces', has: {QC: true}},
      columns: getHistoricalTableColumns('7062', '7063'),
      cells: historicalTable20YearsCells
    },
    "1000": {
      "hasTotals": true,
      "superHeaders": [
        {"colspan": "3"},
        {
          "header": "Gifts of certified ecologically sensitive land available for carryforward",
          "divider": true,
          "colspan": "2"
        }
      ],
      "columns": [
        {
          "header": 'Year of origin<br>MM-DD-YYYY',
          cellClass: 'alignCenter',
          type: 'date',
          colClass: 'std-input-width'
        },
        {
          "header": 'Charitable donations available for carryforward',
          cellClass: 'alignCenter',
          "total": true,
          "totalNum": '1001',
          totalMessage: 'Totals'
        },
        {
          header: 'Gifts of certified cultural property available for carryforward',
          total: true,
          totalNum: '1005'
        },
        {
          "header": 'Gifts of certified ecologically sensitive land available for carryforward, made before February 11, 2014',
          cellClass: 'alignCenter',
          "total": true,
          "totalNum": '1002'
        },
        {
          "header": 'Gifts of certified ecologically sensitive land available for carryforward, made after February 10, 2014',
          cellClass: 'alignCenter',
          "total": true,
          "totalNum": '1003'
        },
        {
          "header": 'Additional deduction for gifts of medicine available for carryforward',
          cellClass: 'alignCenter',
          "total": true,
          "totalNum": '1004'
        }

      ]
    }
  }
})();
