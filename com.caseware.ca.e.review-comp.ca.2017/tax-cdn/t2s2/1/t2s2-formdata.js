(function() {
  'use strict';

  wpw.tax.global.formData.t2s2 = {
    'formInfo': {
      'abbreviation': 'T2S2',
      'title': 'Charitable Donations and Gifts',
      //TODO: DO NOT DELETE THESE 2 LINE: subtitle and code
      //'subTitle': '(2014 and later tax years)',
      'schedule': 'Schedule 2',
      'code': 'Code 1602',
      'showCorpInfo': true,
      formFooterNum: 'T2 SCH 2 E (17)',
      'description': [
        {
          type: 'list',
          items: [
            {
              label: 'For use by corporations to claim any of the following:',
              sublist: ['the eligible amount of charitable donations to qualified donees;',
                'the Ontario, Nova Scotia, and British Columbia food donation tax credits for farmers;',
                'the eligible amount of gifts of certified cultural property;',
                'the eligible amount of gifts of certified ecologically sensitive land; or',
                'the additional deduction for gifts of medicine']
            },
            {
              label: 'All legislative references are to the federal' + '<i>' + ' <i>Income Tax Act</i>' + '</i>' +
              ', unless otherwise specified'
            },
            {
              label: 'The eligible amount of a gift is the amount by which the fair market value of the' +
              ' gifted property exceeds the amount of an advantage, if any, for the gift.'
            },
            {
              label: 'The donations and gifts are eligible for a 5-year carryforward except for gifts of certified ecologically sensitive land made after February 10, 2014, which are eligible for a 10-year carryforward. Please note that the provincial food donation tax credits must be applied in the current tax year.'
            },
            {
              label: 'Use this schedule to show a transfer of unused amounts from previous years following an amalgamation or the wind-up of a subsidiary as described under subsections 87(1) and 88(1).'
            },
            {
              label: 'Subsection 110.1(1.2) provides as follows:',
              sublist: ['Where a particular corporation has undergone an acquisition of control, for tax years' +
              ' that end on or after the acquisition of control, no corporation can claim a' +
              ' deduction for a gift made by the particular corporation to a qualified ' +
              'donee before the acquisition of control.', 'If a particular corporation makes ' +
              'a gift to a qualified donee pursuant to an arrangement under which both the gift ' +
              'and the acquisition of control is expected, no corporation can claim a deduction ' +
              'for the gift unless the person acquiring control of the particular corporation ' +
              'is the qualified donee.']
            },
            {
              label: 'An eligible medical gift to a qualifying organization for activities outside of Canada ' +
              'may be eligible for an additional deduction. Calculate the additional deduction in Part 5.'
            },
            {
              label: 'File one completed copy of this schedule with your' + '<i>' +
              ' T2 Corporation Income Tax Return.' + '</i>'
            },
            {label: 'For more information, see the ' + '<i>' + 'T2 Corporation - Income Tax Guide.' + '</i>'}
          ]
        }
      ],
      'descriptionHighlighted': true,
      'highlightFieldsets': true,
      category: 'Federal Tax Forms'
    },
    'sections': [
      {
        'header': 'Historical Data and calculation for Charitable donations',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength boldUnderline center',
            'label': 'Federal'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'table',
            'num': '5011'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Expired: ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '5014'
                }
              }
            ]
          },
          {
            'label': '** Expiring if not use: ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '5015'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength boldUnderline center',
            'label': 'Alberta',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'type': 'table',
            'num': '6011',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'label': '* Expired: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '6014'
                }
              }
            ]
          },
          {
            'label': '** Expiring if not use: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '6015'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength boldUnderline center',
            'label': 'Quebec',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'type': 'table',
            'num': '7011',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'label': '* Expired: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '7014'
                }
              }
            ]
          },
          {
            'label': '** Expiring if not use: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '7015'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          }
        ]
      },
      {
        'header': 'Part 1 - Charitable donations',
        'rows': [
          {
            'type': 'table',
            'num': '100'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Enter amount 3 on line 683 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>' +
            '. The maximum amount you can claim in the' +
            '<br> current year is whichever is less: the British Columbia income ' +
            'tax otherwise payable or amount 3. For more information, see<br> section 20.1 of the British Columbia' +
            '<i> Income Tax Act</i>.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - Maximum allowable deduction for charitable donations',
        'rows': [
          {
            'label': 'Net income for tax purposes* <b>multiplied</b> by 75%',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '224'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Taxable capital gains arising in respect of gifts of capital property included in Part 1**',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '225',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '225'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G'
                }
              }
            ]
          },
          {
            'label': 'Taxable capital gain in respect of a disposition of a non-qualifying security<br> under subsection 40(1.01)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '227',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '227'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'H'
                }
              }
            ]
          },
          {
            'label': 'The amount of the recapture of capital cost <br>allowance in respect of charitable donations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '230',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '230'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Proceeds of disposition, <b>less</b><br>outlays and expenses**',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '231'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'I'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Capital cost**',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '232'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'J'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Amount I or J, whichever is less',
            'labelClass': ' ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '235',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '235'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Amount on line 230 or 235, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '236'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'K'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> amounts G, H, and K)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '237'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'L'
                }
              }
            ]
          },
          {
            'label': 'Amount L <b>multiplied</b> by 25%',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '241'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'label': 'Subtotal (amount F <b>plus</b> amount M)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '242'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'label': '<b>Maximum allowable deduction for charitable donations</b> (enter amount E from Part 1, amount N, or net income for tax purposes, whichever is less)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '243'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '*  For credit unions, subsection 137(2) states that this amount is before the deduction of payments pursuant to allocations in proportion to borrowing and bonus interest.',
            'labelClass': 'fullLength'
          },
          {
            'label': '**  This amount must be prorated by the following calculation: eligible amount of the gift <b> divided by </b> the proceeds of disposition of the gift.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for Gifts of certified cultural property',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength boldUnderline center',
            'label': 'Federal'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'table',
            'num': '5031'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Expired: ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '5034'
                }
              }
            ]
          },
          {
            'label': '** Expiring if not use: ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '5035'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength boldUnderline center',
            'label': 'Alberta',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'type': 'table',
            'num': '6031',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'label': '* Expired: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '6034'
                }
              }
            ]
          },
          {
            'label': '** Expiring if not use: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '6035'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength boldUnderline center',
            'label': 'Quebec',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'type': 'table',
            'num': '7031',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'label': '* Expired: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '7034'
                }
              }
            ]
          },
          {
            'label': '** Expiring if not use: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '7035'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          }
        ]
      },
      {
        'header': 'Part 3 - Gifts of certified cultural property',
        'rows': [
          {
            'type': 'table',
            'num': '400'
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for Gifts of certified ecologically sensitive land',
        'rows': [
          {
            'label': 'Gifts of certified ecologically sensitive land made before February 11, 2014',
            'labelClass': 'fullLength boldUnderline'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength boldUnderline center',
            'label': 'Federal'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'table',
            'num': '5041'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Expired: ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '5044'
                }
              }
            ]
          },
          {
            'label': '** Expiring if not use: ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '5045'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength boldUnderline center',
            'label': 'Alberta',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'type': 'table',
            'num': '6041',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'label': '* Expired: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '6044'
                }
              }
            ]
          },
          {
            'label': '** Expiring if not use: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '6045'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Gifts of certified ecologically sensitive land made after February 10, 2014',
            'labelClass': 'fullLength boldUnderline'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength boldUnderline center',
            'label': 'Federal'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'table',
            'num': '5051'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Expired: ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '15044'
                }
              }
            ]
          },
          {
            'label': '** Expiring if not use: ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '15045'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength boldUnderline center',
            'label': 'Alberta',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'type': 'table',
            'num': '6051',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'label': '* Expired: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '6054'
                }
              }
            ]
          },
          {
            'label': '** Expiring if not use: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '6055'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'label': 'Gifts of certified ecologically sensitive land made after March 23, 2006',
            'labelClass': 'fullLength boldUnderline',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength boldUnderline center',
            'label': 'Quebec',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'type': 'table',
            'num': '7041',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'label': '* Expired: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '7044'
                }
              }
            ]
          },
          {
            'label': '** Expiring if not use: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '7045'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          }
        ]
      },
      {
        'header': 'Part 4 - Gifts of certified ecologically sensitive land',
        'rows': [
          {
            'type': 'table',
            'num': '500'
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for Additional deduction for gifts of medicine',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength boldUnderline center',
            'label': 'Federal'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'table',
            'num': '5061'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Expired: ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '5064'
                }
              }
            ]
          },
          {
            'label': '** Expiring if not use: ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '5065'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength boldUnderline center',
            'label': 'Alberta',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'type': 'table',
            'num': '6061',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'label': '* Expired: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '6064'
                }
              }
            ]
          },
          {
            'label': '** Expiring if not use: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '6065'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'AB': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength boldUnderline center',
            'label': 'Quebec',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'type': 'table',
            'num': '7061',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'label': '* Expired: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '7064'
                }
              }
            ]
          },
          {
            'label': '** Expiring if not use: ',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            },
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '7065'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          },
          {
            'labelClass': 'fullLength',
            'showWhen': {
              'fieldId': 'provinces',
              'has': {
                'QC': true
              }
            }
          }
        ]
      },
      {
        'header': 'Part 5 -Additional deduction for gifts of medicine',
        'rows': [
          {
            'type': 'table',
            'num': '599'
          },
          {
            'type': 'table',
            'num': '900'
          },
          {
            'type': 'table',
            'num': '950'
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Part 6 - Amount available for carryforward by year of origin',
        'rows': [
          {
            'label': 'You can complete this part to show all the donations and gifts from previous years available for carryforward by year of origin. <br>This will help you determine the amount that could expire in following years.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1000'
          }
        ]
      }
    ]
  };
})();
