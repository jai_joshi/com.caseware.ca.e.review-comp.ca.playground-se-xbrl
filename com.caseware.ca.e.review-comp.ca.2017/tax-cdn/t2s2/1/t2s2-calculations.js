(function() {

  function part1Calc(calcUtils, suffix) {
    var num210 = '210' + suffix;
    var num238 = '238' + suffix;
    var num239 = '239' + suffix;
    var num240 = '240' + suffix;
    var num250 = '250' + suffix;
    var num251 = '251' + suffix;
    var num252 = '252' + suffix;
    var num255 = '255' + suffix;
    var num256 = '256' + suffix;
    var num260 = '260' + suffix;
    var num280 = '280' + suffix;
    calcUtils.subtract(num240, num238, num239);
    calcUtils.sumBucketValues(num251, [num250, num210]);
    calcUtils.sumBucketValues(num252, [num240, num251]);
    calcUtils.subtract(num256, num252, num255);
    calcUtils.subtract(num280, num256, num260);
  }

  function part4Calc(calcUtils, suffix) {
    var num410 = '410' + suffix;
    var num411 = '411' + suffix;
    var num413 = '413' + suffix;
    var num438 = '438' + suffix;
    var num439 = '439' + suffix;
    var num440 = '440' + suffix;
    var num450 = '450' + suffix;
    var num455 = '455' + suffix;
    var num460 = '460' + suffix;
    var num461 = '461' + suffix;
    var num480 = '480' + suffix;
    calcUtils.subtract(num440, num438, num439);
    calcUtils.sumBucketValues(num411, [num450, num410]);
    calcUtils.sumBucketValues(num413, [num440, num411]);
    calcUtils.sumBucketValues(num461, [num455, num460]);
    calcUtils.subtract(num480, num413, num461);
  }

  function part5Calc(calcUtils, suffix) {

    var num511 = '511' + suffix;
    var num513 = '513' + suffix;
    var num538 = '538' + suffix;
    var num539 = '539' + suffix;
    var num540 = '540' + suffix;
    var num555 = '555' + suffix;
    var num560 = '560' + suffix;
    var num561 = '561' + suffix;
    var num580 = '580' + suffix;
    calcUtils.subtract(num540, num538, num539);
    calcUtils.sumBucketValues(num513, [num540, num511]);
    calcUtils.sumBucketValues(num561, [num555, num560]);
    calcUtils.subtract(num580, num513, num561);
  }

  function part6Calc(calcUtils, suffix) {
    var num600 = '600' + suffix;
    var num601 = '601' + suffix;
    var num602 = '602' + suffix;
    var num603 = '603' + suffix;
    var num604 = '604' + suffix;
    var num610 = '610' + suffix;
    var num611 = '611' + suffix;
    var num613 = '613' + suffix;
    var num638 = '638' + suffix;
    var num639 = '639' + suffix;
    var num640 = '640' + suffix;
    var num650 = '650' + suffix;
    var num655 = '655' + suffix;
    var num660 = '660' + suffix;
    var num661 = '661' + suffix;
    var num680 = '680' + suffix;
    var num901 = '901' + suffix;
    var num902 = '902' + suffix;
    var num903 = '903' + suffix;

    calcUtils.subtract(num640, num638, num639);
    calcUtils.subtract(num603, num602, num601);
    calcUtils.multiply(num604, [num603], 0.50);
    calcUtils.min(num901, [num601, num604]);
    calcUtils.equals(num902, num600);
    calcUtils.equals(num903, num602);
    calcUtils.divide(num610, num902, num903, num901);
    calcUtils.sumBucketValues(num611, [num650, num610]);
    calcUtils.sumBucketValues(num613, [num640, num611]);
    calcUtils.sumBucketValues(num661, [num655, num660]);
    calcUtils.subtract(num680, num613, num661);
  }

  //federal calcUtils
  //array.length = 4
  var tableArraysF = [5011, 5031, 5041, 5051, 5061];
  var expiredAmount = [5014, 5034, 5044, 5054, 5064, 15044];
  var expiringAmount = [5015, 5035, 5045, 5055, 5065, 15045];
  var totalBeginningAmount = [238, 438, 538, 538, 638];//double 538 cuz its a total
  var expiredAmountPart = [239, 439, 539, 539, 639];//double 539 cuz its a total
  var adjustmentAmount = [255, 455, 555, 555, 655];//double 555 cuz its a total
  var appliedAmount = [260, 460, 560, 560, 660];//double 560 cuz its a total
  //array.length = 4 cuz part 6 is not applicable
  var amountFromAmalPart = [250, 450, 548, 549];
  var currentYearAmountPart = [210, 410, 510, 520];
  var donationWorkchartTotal = [401, 403, 404, 405];

  //alberta calcUtils
  //array.length = 4
  var tableArraysA = [6011, 6031, 6041, 6051, 6061];
  var expiredAmountA = [6014, 6034, 6044, 6054, 6064];
  var expiringAmountA = [6015, 6035, 6045, 6055, 6065];
  var totalBeginningAmountA = ['238-A', '438-A', '538-A', '538-A', '638-A'];
  var expiredAmountPartA = ['239-A', '439-A', '539-A', '539-A', '639-A'];
  var adjustmentAmountA = ['255-A', '455-A', '555-A', '555-A', '655-A'];
  var appliedAmountA = ['260-A', '460-A', '560-A', '560-A', '660-A'];

  //Quebec calcUtils
  //array.length = 4
  var tableArraysQ = [7011, 7031, 7041, 7061];
  var expiredAmountQ = [7014, 7034, 7044, 7064];
  var expiringAmountQ = [7015, 7035, 7045, 7065];
  var totalBeginningAmountQ = ['238-Q', '438-Q', '538-Q', '638-Q'];
  var expiredAmountPartQ = ['239-Q', '439-Q', '539-Q', '639-Q'];
  var adjustmentAmountQ = ['255-Q', '455-Q', '555-Q', '655-Q'];
  var appliedAmountQ = ['260-Q', '460-Q', '560-Q', '660-Q'];

  function amountAppliedcalcUtils(tableRow, totalTax) {
    var applied = 0;
    var original = tableRow[8].get();
    if (original == 0) {
      applied = original;
    }
    else {
      if (totalTax > original) {
        applied = original;
      }
      else {
        applied = totalTax;
      }
    }
    return applied;
  }

  //calcUtils for Federal workchart & Alberta
  function tableAppliedCalcs(field, tableArrays) {
    var totalTax = field('T2J.300').get();
    var part1Limit = Math.min(field('256').get(), field('242').get(), field('taxTotal').get());
    var rowIndex = 1;
    var tableIndex = 0;

    //calcs for current year amount
    executeCalcsForCurrentYearAdd(field, tableArrays);
    //calcUtils for applied amount
    while (totalTax >= 0) {
      var tableRow = field(tableArrays[tableIndex]).getRow(rowIndex);
      var colApplied = tableRow[10];
      //first row of each table
      if (rowIndex == 1) {
        //calc applied amount
        colApplied.assign(amountAppliedcalcUtils(tableRow, totalTax));
        //check credit applied limit for part 1 (table 0)
        if (tableIndex == 0) {
          if (colApplied.get() >= part1Limit) {
            colApplied.assign(part1Limit);
          }
          part1Limit = part1Limit - colApplied.get();
        }
        totalTax = totalTax - colApplied.get();

        //go right until go thru all table, move to next row of table 0
        tableIndex++;
        if (tableIndex > (tableArrays.length - 1)) {
          rowIndex = 2;
          tableIndex = 0;
        }
      }
      else {
        //check all row in table 0 after the first row of all table been checked
        if (tableIndex == 0) {
          //calc applied amount
          colApplied.assign(amountAppliedcalcUtils(tableRow, totalTax));
          if (tableIndex == 0) {
            //check part1 limit
            if (colApplied.get() >= part1Limit) {
              colApplied.assign(part1Limit);
            }
            part1Limit = part1Limit - colApplied.get();
          }
          //calcUtils remained
          totalTax = totalTax - colApplied.get();
          //go down until check all row on table 0
          rowIndex++;
          if (rowIndex > (field(tableArrays[0]).size().rows - 1)) {
            rowIndex = 2;
            tableIndex = 1;
          }
        }
        else {
          if (rowIndex >= 2 && rowIndex <= 6) {
            //calc applied amount
            colApplied.assign(amountAppliedcalcUtils(tableRow, totalTax));
            totalTax = totalTax - colApplied.get();
            //if not table 0 then go right
            tableIndex++;
            if (tableIndex > (tableArrays.length - 1)) {
              rowIndex++;
              // if reaches the last row of the table 1, start checking table 3 cuz table 3 has more rows
              tableIndex = (rowIndex == field(tableArrays[0]).size().rows) ? 3 : 1;
            }
          }
          else {
            if (tableIndex == 3) {
              //calc applied amount
              colApplied.assign(amountAppliedcalcUtils(tableRow, totalTax));
              totalTax = totalTax - colApplied.get();
              //go down until check all row on table 3(only table 3 have 11 rows other only have 6)
              rowIndex++;
              if (rowIndex > (field(tableArrays[3]).size().rows - 1)) {
                break;
              }
            }
            else {
              break;
            }
          }
        }
      }
    }

    //calcUtils remained amount for all table...only table 3 have 11row other table only have 6
    executeCalcsOnRemainedCells(field, tableArrays)
  }

  //calcUtils for Quebec workchart only 4 table is same row length
  function tableAppliedCalcsQuebec(field, tableArrays) {
    var totalTax = field('taxTotal').get();
    var part1Limit = Math.min(field('256').get(), field('242').get(), field('taxTotal').get());
    var rowIndex = 1;
    var tableIndex = 0;
    //calcs for current year amount
    executeCalcsForCurrentYearAdd(field, tableArrays, true);
    //calcUtils for applied amount
    while (totalTax >= 0) {
      var tableRow = field(tableArrays[tableIndex]).getRow(rowIndex);
      var colApplied = tableRow[10];
      //first row of each table
      if (rowIndex == 1) {
        //calc applied amount
        colApplied.assign(amountAppliedcalcUtils(tableRow, totalTax));
        if (tableIndex == 0) {
          //check part1 limit
          if (colApplied.get() >= part1Limit) {
            colApplied.assign(part1Limit);
          }
          part1Limit = part1Limit - colApplied.get();
        }
        totalTax = totalTax - colApplied.get();
        //go right
        tableIndex++;
        if (tableIndex > (tableArrays.length - 1)) {
          rowIndex = 2;
          tableIndex = 0;
        }
      }
      else {
        //check all row in table 0 after the first row of all table been checked
        if (tableIndex == 0) {
          //calc applied amount
          colApplied.assign(amountAppliedcalcUtils(tableRow, totalTax));
          if (tableIndex == 0) {
            //checkpart1limit
            if (colApplied.get() >= part1Limit) {
              colApplied.assign(part1Limit);
            }
            part1Limit = part1Limit - colApplied.get();
          }
          //calcUtils remained
          totalTax = totalTax - colApplied.get();
          //go down until check all row on table 0
          rowIndex++;
          if (rowIndex > (field(tableArrays[tableIndex]).size().rows - 1)) {
            rowIndex = 2;
            tableIndex = 1;
          }
        }
        else {
          if (rowIndex >= 2 && rowIndex <= (field(tableArrays[tableIndex]).size().rows - 1)) {
            colApplied.assign(amountAppliedcalcUtils(tableRow, totalTax));
            totalTax = totalTax - colApplied.get();
            tableIndex++;
            if (tableIndex > (tableArrays.length - 1)) {
              rowIndex++;
              tableIndex = 1;
              if (rowIndex == field(tableArrays[tableIndex]).size().rows) {
                break;
              }
            }
          }
          else {
            break;
          }
        }
      }
    }
    //calcUtils remained amount for all table
    executeCalcsOnRemainedCells(field, tableArrays);
  }

  function executeCalcsForCurrentYearAdd(field, tableArrays, isQuebec) {
    tableArrays.forEach(function(tableNum, tableIndex) {
      //to get last row
      var tableRow;
      if (isQuebec) {
        tableRow = field(tableNum).getRow(field(tableNum).size().rows - 1);
      }
      else {
        tableRow = field(tableNum).getRow(6);
        if (tableIndex == 3) {
          tableRow = field(tableNum).getRow(11);
        }
      }
      // get value from donation wc to current year contribution cells
      tableRow[2].assign(field('T2S2S.' + donationWorkchartTotal[tableIndex]).get());
      tableRow[4].assign(field('T2S2AS.' + donationWorkchartTotal[tableIndex]).get());
      //calcUtils amount available for current year
      tableRow[8].assign(tableRow[2].get() + tableRow[4].get() - tableRow[6].get())
    });
  }

  function executeCalcsOnRemainedCells(field, tableArrays) {
    //calcUtils remained amount for all table
    tableArrays.forEach(function(tableNum, tableIndex) {
      field(tableNum).getRows().forEach(function(row, rIndex) {
        if (rIndex == 0)
          return;
        row[12].assign(row[8].get() - row[10].get());
      })
    });
  }

  function assignDateCol(field, tableArrays, historicalTable10Years) {
    tableArrays.forEach(function(num) {
      field(num).getRows().forEach(function(row, rowIndex) {
        var taxationYearTable = field('tyh.200');
        if (num == historicalTable10Years) {
          row[0].assign(taxationYearTable.getRow(rowIndex + 9)[6].get())
        }
        else {
          row[0].assign(taxationYearTable.getRow(rowIndex + 14)[6].get())
        }
      })
    })
  }

  wpw.tax.create.calcBlocks('t2s2', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //to get year for table summary
      assignDateCol(field, tableArraysF, 5051)
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('taxTotal').assign(field('T2J.300').get());
      //Federal
      tableArraysF.forEach(function(tableNum, tableIndex) {
        //update current year contribution from historical table to Part calcUtils
        //last table does not need to get updated
        if (tableIndex < tableArraysF.length - 2) {
          //update amount from amal to the part calcUtils
          field(amountFromAmalPart[tableIndex].toString()).assign(field(tableNum).total(4).get());
          //update amount from current year to the part calcUtils
          field(currentYearAmountPart[tableIndex].toString()).assign(field(tableNum).total(2).get());
        }

        //to update expired and expiring amount at the end of each table then assign it to Part calcUtils
        //update Amount at the beginning of current year (total of all amount EXCEPT last row-current year contribution
        //update applied amount to the part calcUtils
        //update adjustment amount from the table to part calcUtils
        //federal
        field(expiredAmount[tableIndex].toString()).assign(field(tableNum).cell(0, 8).get());
        field(expiringAmount[tableIndex].toString()).assign(field(tableNum).cell(1, 12).get());

        var totalExpiredValue = 0;
        var totalBeginningValue = 0;
        var totalAdjustmentValue = 0;

        if (tableIndex == 2 || tableIndex == 3) {
          totalExpiredValue = field(expiredAmount[2]).get() + field(expiredAmount[3]).get();
          totalBeginningValue =
              field(tableArraysF[2]).total(8).get() +
              field(tableArraysF[3]).total(8).get() -
              field(tableArraysF[2]).cell(6, 8).get() -
              field(tableArraysF[3]).cell(11, 8).get();
          totalAdjustmentValue =
              field(tableArraysF[2]).cell(6, 6).get() +
              field(tableArraysF[3]).cell(11, 6).get();
        }
        else {
          totalExpiredValue = field(expiredAmount[tableIndex]).get();
          totalBeginningValue =
              field(tableNum).total(8).get() -
              field(tableNum).cell(6, 8).get();
          totalAdjustmentValue = field(tableNum).cell(6, 6).get();
          field(appliedAmount[tableIndex]).assign(field(tableNum).total(10).get());
        }

        field(expiredAmountPart[tableIndex]).assign(totalExpiredValue);
        field(totalBeginningAmount[tableIndex]).assign(totalBeginningValue);
        field(adjustmentAmount[tableIndex]).assign(totalAdjustmentValue);
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      /*Part 2*/
      calcUtils.multiply('224', ['taxTotal'], 0.75);
      calcUtils.min('235', ['231', '232']);
      calcUtils.min('236', ['230', '235']);
      calcUtils.sumBucketValues('237', ['225', '227', '236']);
      calcUtils.multiply('241', ['237'], 0.25);
      calcUtils.sumBucketValues('242', ['224', '241']);
      calcUtils.min('243', ['256', '242', 'taxTotal']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      /* Part 1*/
      tableAppliedCalcs(field, tableArraysF);
      part1Calc(calcUtils, '');
      calcUtils.multiply('262-1', ['262'], 0.25);
      calcUtils.multiply('263-1', ['263'], 0.25);
      calcUtils.multiply('265-1', ['265'], 0.25);
    });
    calcUtils.calc(function(calcUtils, field, form) {
      //*Part 4*now is part 3
      part4Calc(calcUtils, '');
    });
    calcUtils.calc(function(calcUtils, field, form) {
      /*Part 5(now 4)*/
      calcUtils.sumBucketValues('550', ['548', '549']);
      calcUtils.sumBucketValues('511', ['550', '510', '520']);
      part5Calc(calcUtils, '');
      /*Part 6(now 5)*/
      part6Calc(calcUtils, '');
    });

    //Alberta
    calcUtils.calc(function(calcUtils, field, form) {
      field('provinces').assign(field('CP.prov_residence').get());
      var isAlbertaJurisdiction = (field('provinces').getKey('AB') == true);
      if (isAlbertaJurisdiction) {
        //to get year for table summary
        tableAppliedCalcs(field, tableArraysA);
        assignDateCol(field, tableArraysA, 6051);
        tableArraysA.forEach(function(tableNum, tableIndex) {
          field(expiredAmountA[tableIndex]).assign(field(tableNum).cell(0, 8).get());
          field(expiringAmountA[tableIndex]).assign(field(tableNum).cell(1, 12).get());

          var totalExpiredValueA = 0;
          var totalBeginningValueA = 0;
          var totalAdjustmentValueA = 0;

          if (tableIndex == 2 || tableIndex == 3) {
            totalExpiredValueA = field(expiredAmountA[2]).get() + field(expiredAmountA[3]).get();
            totalBeginningValueA =
                field(tableArraysA[2]).total(8).get() +
                field(tableArraysA[3]).total(8).get() -
                field(tableArraysA[2]).cell(6, 8).get() -
                field(tableArraysA[3]).cell(11, 8).get();

            totalAdjustmentValueA =
                field(tableArraysA[2]).cell(6, 6).get() +
                field(tableArraysA[3]).cell(11, 6).get();

            field(appliedAmountA[tableIndex]).assign(
                field(tableArraysA[2]).total(10).get() +
                field(tableArraysA[3]).total(10).get()
            )
          }
          else {
            totalExpiredValueA = field(expiredAmountA[tableIndex]).get();
            totalBeginningValueA =
                field(tableNum).total(8).get() -
                field(tableNum).cell(6, 8).get();
            totalAdjustmentValueA = field(tableNum).cell(6, 6).get();
            field(appliedAmountA[tableIndex]).assign(field(tableNum).total(10).get());
          }
          field(expiredAmountPartA[tableIndex]).assign(totalExpiredValueA);
          field(totalBeginningAmountA[tableIndex]).assign(totalBeginningValueA);
          field(adjustmentAmountA[tableIndex]).assign(totalAdjustmentValueA);
        });
        part1Calc(calcUtils, '-A');
        part4Calc(calcUtils, '-A');
        part5Calc(calcUtils, '-A');
        part6Calc(calcUtils, '-A');
      }
    });

    //Quebec
    calcUtils.calc(function(calcUtils, field, form) {
      field('provinces').assign(field('CP.prov_residence').get());
      var isQuebecJurisdiction = (field('provinces').getKey('QC') == true);
      if (isQuebecJurisdiction) {
        //to get year for table summary
        tableAppliedCalcsQuebec(field, tableArraysQ);
        tableArraysQ.forEach(function(tableNum) {
          var tableField = field(tableNum);
          var taxationYearTable = field('tyh.200');
          var dateCol = taxationYearTable.cell(0, 6).get();
          var taxEnd = field('CP.tax_end').get();
          tableField.cell(0, 0).assign(!dateCol ? calcUtils.addToDate(taxEnd, -21, 0, 0) : calcUtils.addToDate(dateCol, -1, 0, 0));
          for (var i = 1; i < tableField.size().rows; i++) {
            tableField.cell(i, 0).assign(taxationYearTable.getRow(i - 1)[6].get());
          }
        });
        tableArraysQ.forEach(function(tableNum, tableIndex) {
          field(expiredAmountQ[tableIndex]).assign(field(tableNum).cell(0, 8).get());
          field(expiringAmountQ[tableIndex]).assign(field(tableNum).cell(1, 12).get());
          field(appliedAmountQ[tableIndex]).assign(field(tableNum).total(10).get());
          field(expiredAmountPartQ[tableIndex]).assign(field(expiredAmountQ[tableIndex]).get());
          field(totalBeginningAmountQ[tableIndex]).assign(
              field(tableNum).total(8) -
              field(tableNum).cell(6, 8).get()
          );
          field(adjustmentAmountQ[tableIndex]).assign(field(tableNum).cell(6, 6).get());
        });
        part1Calc(calcUtils, '-Q');
        part4Calc(calcUtils, '-Q');
        part5Calc(calcUtils, '-Q');
        part6Calc(calcUtils, '-Q');
      }
    });
  });
})
();
