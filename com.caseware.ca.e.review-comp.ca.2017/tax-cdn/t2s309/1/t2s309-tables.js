(function() {

  wpw.tax.create.tables('t2s309', {

    900: {
      showNumbering: true,
      hasTotals: true,
      columns: [
        {
          header: 'Certificate number<br><br>',
          tn: '100',"validate": {"or":[{"length":{"min":"1","max":"20"}},{"check":"isEmpty"}]}, 
          type: 'text'
        },
        {
          header: 'Amount of the Newfoundland and Labrador interactive <br>digital media tax credit<br>',
          colClass: 'std-input-width',
          tn: '200',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          total: true,
          totalNum: '300',
          totalTn: '300',
          totalMessage: 'Total amount of the Newfoundland and Labrador interactive digital media tax credit'
        }
      ]
    }
  })
})();

