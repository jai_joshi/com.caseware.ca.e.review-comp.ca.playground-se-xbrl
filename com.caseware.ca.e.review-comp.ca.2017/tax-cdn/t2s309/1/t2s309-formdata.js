(function() {

  wpw.tax.create.formData('t2s309', {
    formInfo: {
      abbreviation: 'T2S309',
      title: 'Additional Certificate Numbers for the Newfoundland and Labrador Interactive Digital Media Tax Credit',
      schedule: 'Schedule 309',
      headerImage: 'canada-federal',
      code: 'Code 1502',
      category: 'Newfoundland and Labrador Forms',
      formFooterNum: 'T2 SCH 309 E (17)'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: 'For use by a corporation that has more than one certificate number for the' +
            ' Newfoundland and Labrador interactive digital media tax credit. File this schedule and ' +
            'the certificates with your T2 Corporation Income Tax Return.',
            labelClass: 'fullLength tabbed2'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '900'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Enter this amount on line 522 in Part 2 of Schedule 5, ' +
            '<i>Tax Calculation Supplementary - Corporations</i>.',
            labelClass: 'fullLength tabbed'
          }
        ]
      }
    ],
    category: 'Newfoundland and Labrador Forms'
  });
})();
