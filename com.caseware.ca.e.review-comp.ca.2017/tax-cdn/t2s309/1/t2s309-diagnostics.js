(function() {
  wpw.tax.create.diagnostics('t2s309', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3090001', common.prereq(common.and(
        common.requireFiled('T2S309'),
        common.check(['t2s5.522'], 'isNonZero')),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne([row[0], tools.field('t2s5.840')], 'isNonZero');
          });
        }));

    diagUtils.diagnostic('3090005', common.prereq(common.requireFiled('T2S309'),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[0], tools.field('t2s5.840')], 'isNonZero') > 1)
              return tools.requireOne([row[0], tools.field('t2s5.840')], 'isEmpty');
            else return true;
          });
        }));

    diagUtils.diagnostic('3090010', common.prereq(common.requireFiled('T2S309'),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[0].isNonZero())
              return row[1].require('isNonZero');
            else return true
          });
        }));

    diagUtils.diagnostic('3090015', common.prereq(common.requireFiled('T2S309'),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[1].isNonZero())
              return row[0].require('isNonZero');
            else return true
          });
        }));

  });
})();
