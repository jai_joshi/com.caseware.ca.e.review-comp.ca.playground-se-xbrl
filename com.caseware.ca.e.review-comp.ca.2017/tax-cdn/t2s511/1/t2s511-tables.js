(function() {
  wpw.tax.global.tableCalculations.t2s511 = {
    "100": {
      fixedRows: true,
      hasTotals: true,
      "showNumbering": true,
      "infoTable": false,
      "maxLoop": 100000,
      "columns": [
        {
          "header": "Names of associated corporations",
          "tn": "200",
          "maxLength": 175,
          "width": "30%",
          type: 'text'
        },
        {
          "header": "Business number <br> (Canadian corporation only) <br> (see Note 1))",
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR'],
          "tn": "300"
        },
        {
          "header": "Total assets * <br> (see Note 2)",
          "tn": "400",
          "total": true,
          "totalNum": "450",
          "totalTn": "450",
          filters: 'prepend $',
          "maxLength": 13,
          "filters": ""
        },
        {
          "header": "Total revenue ** <br> (see Note 2)",
          "tn": "500",
          "total": true,
          "totalNum": "550",
          "totalTn": "550",
          filters: 'prepend $',
          "maxLength": 13
        }
      ]
    }
  }
})();
