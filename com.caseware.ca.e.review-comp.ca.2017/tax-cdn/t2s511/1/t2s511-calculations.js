(function() {

  wpw.tax.create.calcBlocks('t2s511', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      var table = field('100');
      var linkedTable = form('entityProfileSummary').field('511');
      table.getRows().forEach(function(row, rIndex) {
        row.forEach(function(cell, cIndex) {
          var linkedCell = linkedTable.cell(rIndex,cIndex);
          cell.assign(linkedCell.get());
          cell.source(linkedCell);
        });
      });
    });
  });
})();
