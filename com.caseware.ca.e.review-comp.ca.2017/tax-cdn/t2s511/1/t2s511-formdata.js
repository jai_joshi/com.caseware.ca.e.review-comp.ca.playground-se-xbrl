(function() {
  'use strict';

  wpw.tax.global.formData.t2s511 = {
    'formInfo': {
      'abbreviation': 'T2S511',
      'title': 'Ontario Corporate Minimum Tax - Total Assets And Revenue For Associated Corporations',
      //'subTitle': '(2009 and later tax years)',
      'schedule': 'Schedule 511',
      'showCorpInfo': true,
      code: 'Code 0901',
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 511',
      'description': [
        {text: ''},
        {
          'type': 'list',
          'items': [//display things need to improve
            'For use by corporations to report the total assets and total revenue of all the Canadian' +
            ' or foreign corporations with which the filing corporation was associated at any time' +
            ' during the tax year. These amounts are required to determine if the filing corporation' +
            ' is subject to corporate minimum tax. ',
            'Total assets and total revenue include the associated corporation\'s share of any' +
            ' partnership(s)/joint venture(s) total assets and total revenue.',
            'Attach additional schedules if more space is required',
            'File this schedule with the ' + '<i>' + ' T2 Corporation Income Tax Return ' + '</i>' + '']
        }
      ],
      category: 'Ontario Forms'
    },
    'sections': [
      {
        hideFieldset: true,
        rows: [
          {
            type: 'table',
            num: '100'
          },
          {labelClass: 'fullLength'},//the text display need to be improved
          {
            label: 'Enter the total assets from line 450 on line 116 in Part 1 of Schedule 510, ' +
            '<i>' + ' Ontario Corporate Minimum Tax.',
            labelClass: 'fullLength tabbed'
          },
          {
            label: 'Enter the total revenue from line 550 on line 146 in Part 1 of Schedule 510.',
            labelClass: 'fullLength tabbed'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Note 1: Enter "NR" if a corporation is not registered.',
            labelClass: 'fullLength tabbed'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Note 2: If the associated corporation does not have a tax year that ends in the filing' +
            ' corporation\'s current tax year but was associated with the filing corporation in the previous' +
            ' tax year of the filing corporation, enter the total revenue and total assets from the tax year' +
            ' of the associated corporation that ends in the previous tax year of the filing corporation.',
            labelClass: 'fullLength tabbed'
          },
          {labelClass: 'fullLength'},
          {
            type: 'description',
            lines: [
              {
                type: 'list', listType: 'asterisk', labelClass: 'bold',
                items: [
                  {
                    label: 'Rules for total assets',
                    sublist: [
                      'Report total assets in accordance with generally accepted accounting principles,' +
                      ' adjusted so that consolidation and equity methods are not used.',
                      'Include the associated corporation\'s share of the total assets of partnership(s)' +
                      ' and joint venture(s) but exclude the recorded asset(s) for the investment in' +
                      ' partnerships and joint ventures.',
                      'Exclude unrealized gains and losses on assets that are included in net income for' +
                      ' accounting purposes but not in income for corporate income tax purposes.'
                    ]
                  },
                  {
                    label: 'Rules for total revenue',
                    sublist: [
                      'Report total revenue in accordance with generally accepted accounting principles,' +
                      ' adjusted so that consolidation and equity methods are not used.',
                      'If the associated corporation has 2 or more tax years ending in the filing corporation\'s' +
                      ' tax year, multiply the sum of the total revenue for each of those tax years by 365' +
                      ' and divide by the total number of days in all of those tax years.',
                      'If the associated corporation\'s tax year is less than 51 weeks and is the only tax year' +
                      ' of the associated corporation that ends in the filing corporation\'s tax year, multiply' +
                      ' the associated corporation\'s total revenue by 365 and divide by the number of days in' +
                      ' the associated corporation\'s tax year.',
                      'Include the associated corporation\'s share of the total revenue of partnerships' +
                      ' and joint ventures.',
                      'If the partnership or joint venture has 2 or more fiscal periods ending in the associated' +
                      ' corporation\'s tax year, multiply the sum of the total revenue for each of the fiscal' +
                      ' periods by 365 and divide by the total number of days in all the fiscal periods.'
                    ]

                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  };
})();
