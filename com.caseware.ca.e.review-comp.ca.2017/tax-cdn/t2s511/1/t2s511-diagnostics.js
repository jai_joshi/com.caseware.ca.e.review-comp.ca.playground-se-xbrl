(function() {
  wpw.tax.create.diagnostics('t2s511', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S511'), forEach.row('100', forEach.bnCheckCol(1, true))));

    diagUtils.diagnostic('5110001', common.prereq(common.check(['t2s510.116', 't2s510.146'], 'isNonZero'),
        common.requireFiled('T2S511')));

    diagUtils.diagnostic('5110002', common.prereq(common.requireFiled('T2S511'),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[0], row[1], row[2], row[3]], 'isNonZero')) {
              return tools.requireAll([row[0], row[1]], 'isNonZero') && tools.requireOne([row[2], row[3]], 'isNonZero');
            }
            else return true;
          });
        }));

    diagUtils.diagnostic('5110003', common.prereq(common.and(
        common.requireFiled('T2S510'),
        common.check('cp.226', 'isYes')),
        common.requireFiled('T2S511')));
  });
})();
