(function() {
  wpw.tax.create.diagnostics('t2s31', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0310001', common.prereq(common.and(
        common.requireFiled('T2S31'),
        function(tools) {
          return tools.field('t2j.231').isChecked() ||
              tools.checkMethod(tools.list(['t2j.652', 't2j.780']), 'isNonZero');
        }), function(tools) {
      return tools.requireOne(tools.list([
        '220', '230', '235', '240', '242', '250', '520',
        '530', '540', '550', '850', '860', '880', '625',
        '630', '640', '635', '655', '775', '777', '780', '782']), 'isNonZero');
    }));

    diagUtils.diagnostic('0310002', common.prereq(common.and(
        common.requireFiled('T2S31'),
        function(tools) {
          return tools.checkMethod(tools.list(['240', '242']), 'isNonZero');
        }), function(tools) {
      var table = tools.field('099');
      return tools.checkAll(table.getRows(), function(row) {
        return tools.requireAll([row[0], row[2], row[3], row[4], row[5]], 'isNonZero');
      });
    }));

    diagUtils.diagnostic('0310003', common.prereq(common.and(
        common.requireFiled('T2S31'),
        function(tools) {
          return tools.field('540').isNonZero();
        }), function(tools) {
      return tools.requireOne(tools.list(['350', '360', '370']), 'isNonZero');
    }));

    diagUtils.diagnostic('0310005', common.prereq(common.and(
        common.requireFiled('T2S31'),
        function(tools) {
          return tools.field('t2j.780').isNonZero();
        }), function(tools) {
      return tools.requireOne(tools.list(['235', '240', '242', '250', '540', '550']), 'isNonZero');
    }));

    diagUtils.diagnostic('0310006', common.prereq(common.and(
        common.requireFiled('T2S31'),
        function(tools) {
          return tools.checkMethod(tools.list(['901', '902', '903']), 'isNonZero');
        }), function(tools) {
      return tools.requireOne(tools.list(['235', '240', '242', '250']), 'isNonZero');
    }));

    diagUtils.diagnostic('0310007', common.prereq(common.and(
        common.requireFiled('T2S31'),
        function(tools) {
          return tools.checkMethod(tools.list(['911', '912', '913']), 'isNonZero');
        }), function(tools) {
      return tools.requireOne(tools.list(['540', '550']), 'isNonZero');
    }));

    diagUtils.diagnostic('0310008', common.prereq(common.and(
        common.requireFiled('T2S31'),
        function(tools) {
          return tools.checkMethod(tools.list(['310', '610', 't2j.780']), 'isNonZero');
        }), function(tools) {
      return tools.requireOne(tools.list(['101']), 'isNonZero');
    }));

    diagUtils.diagnostic('0310010', common.prereq(common.and(
        common.requireFiled('T2S31'),
        function(tools) {
          return tools.field('t2j.602').isNonZero();
        }), function(tools) {
      return tools.requireOne(tools.list(['700', '710', '720', '730', '740', '750', '760']), 'isNonZero') ||
          tools.requireOne(tools.list(['792', '795', '797', '799']), 'isNonZero');
    }));

    diagUtils.diagnostic('0310012', common.prereq(common.and(
        common.requireFiled('T2S31'),
        function(tools) {
          return tools.checkMethod(tools.list(['810', '811', '812', '813', '820', '821', '826']), 'isNonZero')
        }), function(tools) {
      return tools.requireOne(tools.list(['800']), 'isNonZero') &&
          tools.requireOne(tools.list(['805', '806', '807']), 'isNonZero');
    }));

    diagUtils.diagnostic('0310013', common.prereq(common.requireFiled('T2S31'),
        function(tools) {
          var table = tools.field('1400');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[1].isNonZero())
              return row[0].require('isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0310014', common.prereq(common.and(
        common.requireFiled('T2S31'),
        common.check(['870', '872', '874', '876', '880', '921', '922', '923'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['810', '811', '812', '813', '820', '821', '826', '835']), 'isNonZero');
        }));

    diagUtils.diagnostic('0310015', common.prereq(common.requireFiled('T2S31'),
        function(tools) {
          var table = tools.field('1500');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[2].isNonZero())
              return tools.requireAll([row[0], row[1]], 'isFilled');
            else return true;
          });
        }));

    diagUtils.diagnostic('0310016', common.prereq(common.and(
        common.requireFiled('T2S31'),
        function(tools) {
          return tools.field('640').isNonZero();
        }), function(tools) {
      var table = tools.field('1500');
      return tools.checkAll(table.getRows(), function(row) {
        return tools.requireOne(row[2], 'isNonZero');
      });
    }));

    diagUtils.diagnostic('0310017', common.prereq(common.and(
        common.requireFiled('T2S31'),
        function(tools) {
          return tools.checkMethod(tools.list(['931', '932', '933']), 'isNonZero');
        }), function(tools) {
      return tools.requireOne(tools.list(['635', '640', '655']), 'isNonZero');
    }));

    diagUtils.diagnostic('0310018', common.prereq(common.requireFiled('T2S31'),
        function(tools) {
          var table = tools.field('1664');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[3].isNonZero())
              return tools.requireAll([row[0], row[1], row[2]], 'isFilled');
            else return true;
          });
        }));

    diagUtils.diagnostic('031.102', common.prereq(common.and(
        common.requireFiled('T2S31'),
        function(tools) {
          var endDate = tools.field('cp.tax_end').get();
          return endDate.year >= 2010 && tools.field('102').isYes();
        }), function(tools) {
      return tools.field('103').require('isNonZero');
    }));

    diagUtils.diagnostic('031.350', common.prereq(common.and(
        common.requireFiled('T2S31'),
        common.check(['350'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['103', 't661.380', 't661.500', 't661.502', 't661.508']), 'isNonZero');
        }));

    diagUtils.diagnostic('031.650', common.prereq(common.and(
        common.requireFiled('T2S31'),
        common.check(['101'], 'isYes')),
        function(tools) {
          return tools.field('650').require('isNonZero');
        }));

    diagUtils.diagnostic('031.830', common.prereq(common.and(
        common.requireFiled('T2S31'),
        common.check(['830'], 'isNonZero')),
        function(tools) {
          var table = tools.field('1400');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne(tools.list(['810', '811', '812', '813', '820', '821', row[1]]), 'isNonZero');
          });
        }));

    diagUtils.diagnostic('031.360', common.prereq(common.and(
        common.requireFiled('T2S31'),
        common.check(['360'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['t661.390', 't661.504', 't661.510']), 'isNonZero');
        }));

    diagUtils.diagnostic('031.370', common.prereq(common.and(
        common.requireFiled('T2S31'),
        common.check(['370'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['t661.560']), 'isNonZero');
        }));

    diagUtils.diagnostic('031.420', common.prereq(common.and(
        common.requireFiled('T2S31'),
        common.check(['420'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['t661.300', 't661.305', 't661.310', 't661.320', 't661.340',
            't661.345', 't661.350', 't661.355', 't661.360', 't661.370', 't661.500', 't661.502', 't661.508']), 'isNonZero');
        }));

    diagUtils.diagnostic('031.430', common.prereq(common.and(
        common.requireFiled('T2S31'),
        common.check(['430'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['t661.300', 't661.305', 't661.310', 't661.320', 't661.340',
            't661.345', 't661.350', 't661.355', 't661.360', 't661.370', 't661.500', 't661.502', 't661.508']), 'isNonZero');
        }));

    diagUtils.diagnostic('031.440', common.prereq(common.and(
        common.requireFiled('T2S31'),
        common.check(['440'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['t661.390', 't661.504', 't661.510']), 'isNonZero');
        }));

    diagUtils.diagnostic('031.450', common.prereq(common.and(
        common.requireFiled('T2S31'),
        common.check(['450'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['t661.390', 't661.504', 't661.510']), 'isNonZero');
        }));

    diagUtils.diagnostic('031.460', common.prereq(common.and(
        common.requireFiled('T2S31'),
        common.check(['460'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['t661.560']), 'isNonZero');
        }));

    diagUtils.diagnostic('031.480', common.prereq(common.and(
        common.requireFiled('T2S31'),
        common.check(['480'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['t661.560']), 'isNonZero');
        }));

    diagUtils.diagnostic('031.490', common.prereq(common.and(
        common.requireFiled('T2S31'),
        common.check(['490'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['t661.560']), 'isNonZero');
        }));

    diagUtils.diagnostic('031.trainingTaxCredit', common.prereq(common.and(
        common.requireFiled('T2S31'),
        common.check(['t2s552.500'], 'isNonZero')),
        function(tools) {
          return tools.field('1500').cell(0, 2).require('isNonZero');
        }));

    diagUtils.diagnostic('031.611', common.prereq(common.and(
        common.requireFiled('T2S31'),
        function(tools) {
          return tools.field('1500').cell(0,0).isFilled() ||
              tools.field('1500').cell(0,1).isFilled() ||
              tools.field('1500').cell(0,2).isNonZero();
        }), common.check('611', 'isYes')));
  });
})();

