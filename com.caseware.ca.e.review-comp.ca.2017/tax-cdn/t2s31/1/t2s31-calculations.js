(function() {

  var creditAvailable = ['882', '658', '786', '253', '553'];
  var creditApplied = ['885', '660', '785', '260', '560'];
  var creditCarryBack = ['886', '661', '787', '260', '561', '261', '580'];

  function amountAppliedCalcs(creditLimit, available) {
    var applied = 0;
    if (available <= 0) {
      applied = 0;
    }
    else {
      if (creditLimit > available) {
        applied = available;
      }
      else {
        applied = creditLimit;
      }
    }
    return applied;
  }

  function optimizationCreditCalcs(calcUtils) {
    var creditLimit = calcUtils.getGlobalValue(false, 'T2J', 'S31CreditLimit');
    var index = 0;
    var available = 0;
    while (creditLimit >= 0 && index < creditAvailable.length) {
      if (index == 0 || index == 1 || index == 2) {
        available = calcUtils.field(creditAvailable[index]).get() - calcUtils.field(creditCarryBack[index]).get()
      }
      else {
        available = calcUtils.field(creditAvailable[index]).get() -
            calcUtils.field(creditCarryBack[index]).get() - calcUtils.field(creditCarryBack[(index + 2)]).get();
      }
      calcUtils.field(creditApplied[index]).assign(amountAppliedCalcs(creditLimit, available));
      creditLimit = creditLimit - calcUtils.field(creditApplied[index]).get();
      index++;
    }
  }

  wpw.tax.create.calcBlocks('t2s31', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {

      //to get year for table summary
      var tableArray = [1200, 1300, 1450, 1550, 1600];
      var count = 2;
      tableArray.forEach(function(tableNum) {
        field(tableNum).getRows().forEach(function(row, rowIndex) {
          var linkedTable = field('tyh.200');
          var linkedTableRowLength = linkedTable.size().rows;//return 21
          row[1].assign(linkedTable.cell(linkedTableRowLength - count, 2).get());
          count++;
        });
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 1
      field('051').assign(10);
      field('052').assign(10);
      field('053').assign(5);
      field('054').assign(0);
      field('055').assign(35);
      field('056').assign(20);
      field('057').assign(15);
      field('058').assign(10);
      field('059').assign(10);
      field('060').assign(5);
      field('061').assign(0);
      field('062').assign(10);
      field('063').assign(7);
      field('064').assign(4);
      field('065').assign(0);
      field('066').assign(10);
      field('067').assign(25);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 5
      var table099 = field('099');
      var num236 = 0;
      var num241 = 0;

      table099.getRows().forEach(function(row) {
        var typeOfTransaction = row[1].get();
        var amountOfTransaction = row[5].get();

        if (typeOfTransaction == 1 || typeOfTransaction == 2) {
          num236 += amountOfTransaction;
        }
        else if (typeOfTransaction == 3) {
          num241 += amountOfTransaction;
        }
      });
      field('236').assign(num236);
      field('241').assign(num241);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.sumBucketValues('216', ['210', '215']);
      calcUtils.equals('217', '216');
      calcUtils.subtract('220', '209', '217');
      calcUtils.getGlobalValue('237', 'ratesFed', '1102');
      calcUtils.getGlobalValue('238', 'ratesFed', '1103');
      calcUtils.sumBucketValues('251', ['230', '235', '240', '242', '250']);
      calcUtils.equals('252', '251');
      calcUtils.sumBucketValues('253', ['220', '252']);
      optimizationCreditCalcs(calcUtils);
      calcUtils.equals('261', '904');
      field('281').assign(field('260').get() + field('261').get() + field('280').get());
      calcUtils.equals('282', '281');
      calcUtils.subtract('283', '253', '282');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 7
      var isQualified = field('101').get() == 1;
      if (isQualified) {
        calcUtils.sumBucketValues('905', ['240', '242', '250'], true);
        calcUtils.equals('906', '283', true);
        field('907').assign(Math.min(field('905').get(), field('906').get()) * (40 / 100))
      }
      else {
        calcUtils.removeValue([905, 906, 907], true);
      }

      calcUtils.equals('310', '907');
      calcUtils.subtract('320', '283', '310');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 8
      field('350').assign(field('t661.557').get() + field('103').get());
      field('350').source(field('T661.557'));
      field('360').assign(field('t661.558').get());
      field('360').source(field('t661.558'));
      field('370').assign(field('t661.560').get());
      field('370').source(field('t661.560'));
      calcUtils.sumBucketValues('380', ['350', '360', '370']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 10
      field('401').assign(8000000);
      calcUtils.getGlobalValue('1403', 'ratesFed', '340');
      var isQualified = field('101').get() == 1;
      var isCCPC = field('CP.Corp_Type').get() == 1;
      if (isCCPC) {
        field('402').assign(Math.max(field('390').get(), 500000));
        field('405').assign(Math.max((field('401').get() - field('404').get()), 0))
      }
      else {
        field('402').assign(field('390').get() > 0 ? Math.max(field('390').get(), 500000) : 0);
        field('405').assign(field('390').get() > 0 ? Math.max((field('401').get() - field('404').get()), 0) : 0);
      }
      calcUtils.multiply('404', ['402', '1403']);
      field('406').assign(40000000 - field('398').get());
      calcUtils.multiply('407', ['406'], (1 / 40000000));

      var s23TableLength = field('t2s23.085').size().rows;
      var isAssociatedCorp = (s23TableLength > 1);
      field('408').assign(!isAssociatedCorp ? field('405').get() * field('407').get() : 0);
      field('400').assign(isAssociatedCorp ? field('t2s49.085').cell(0, 4).get() : 0);
      field('400').source(field('t2s49.085').cell(0, 4));

      var daysFiscalPeriod = field('cp.Days_Fiscal_Period').get();
      calcUtils.getApplicableValue('412', ['408', '400']);
      field('413').assign(daysFiscalPeriod);
      field('415').assign(365);
      field('414').assign((daysFiscalPeriod < 357) ? (field('412').get() * field('413').get() / field('415').get()) : 0);

      // if (isQualified) {
      calcUtils.getApplicableValue('410', ['408', '400', '414']);
      // } else {
      //   field('410').assign(0)
      // } //comment out for CRA approval
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 11 Rate
      var generalSREDRate = 0;
      calcUtils.getGlobalValue('taxStart', 'CP', 'tax_start');
      var date01Jan2013 = wpw.tax.date(2013, 1, 1);
      var date01Jan2014 = wpw.tax.date(2014, 1, 1);

      if (wpw.tax.actions.calculateDaysDifference(date01Jan2013, field('taxStart').get()) <= 0) {
        generalSREDRate = 20
      }
      else if (wpw.tax.actions.calculateDaysDifference(field('taxStart').get(), date01Jan2014) <= 0) {
        generalSREDRate = 15
      }
      else {
        generalSREDRate = 20 -
            (wpw.tax.actions.calculateDaysDifference(date01Jan2013, field('taxStart').get())
            / 365 * (20 - 15))
      }

      field('431').assign(generalSREDRate);
      field('451').assign(generalSREDRate);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 11
      var isCCPC = (field('CP.Corp_Type').get() == 1);
      if (isCCPC) {
        calcUtils.min('420', ['350', '410']);
      }
      else {
        field('422').assign(0);
      }

      calcUtils.getGlobalValue('421', 'ratesFed', '341');
      calcUtils.subtract('430', '350', '410');

      calcUtils.equals('1430', '430');
      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();
      field('1434').assign(daysFiscalPeriod);
      calcUtils.getGlobalValue('taxStartDate', 'cp', 'tax_start', function(taxStartDate) {
        var before2014 = wpw.tax.date(2014, 1, 1);
        var comp = wpw.tax.actions.calculateDaysDifference(taxStartDate, before2014);
        if (comp >= field('1432').get()) {
          field('1431').assign(field('1432').get());
        }
        else {
          field('1431').assign(comp);
        }
      });
      calcUtils.getGlobalValue('1432', 'ratesFed', '342');
      calcUtils.divide('1433', '1430', '1434', ['1431', '1432'], [1 / 100]);

      calcUtils.equals('1435', '430');
      field('1439').assign(daysFiscalPeriod);
      calcUtils.getGlobalValue('taxStartDate', 'cp', 'tax_start', function(taxStartDate) {
        var after2013 = wpw.tax.date(2013, 1, 1);
        var comp = wpw.tax.actions.calculateDaysDifference(after2013, taxStartDate);
        if (comp >= field('1439').get()) {
          field('1436').assign(field('1439').get());
        }
        else {
          field('1436').assign(comp);
        }
      });
      calcUtils.getGlobalValue('1437', 'ratesFed', '1343');
      calcUtils.divide('1438', '1435', '1439', ['1436', '1437'], [1 / 100]);

      calcUtils.sumBucketValues('431', ['1433', '1438']);
      calcUtils.equals('432', '431');
      calcUtils.subtract('435', '410', '350');

      calcUtils.subtract('433', '410', '350');
      calcUtils.min('440', ['360', '433']);
      calcUtils.getGlobalValue('441', 'ratesFed', '341');
      field('442').assign(field('cp.Corp_Type').get() != '1' ? 0 : field('440').get() * field('441').get() / 100);
      calcUtils.subtract('450', '360', '435');

      calcUtils.equals('1460', '450');
      field('1454').assign(daysFiscalPeriod);
      calcUtils.getGlobalValue('taxStartDate', 'cp', 'tax_start', function(taxStartDate) {
        var before2014 = wpw.tax.date(2014, 1, 1);
        var comp = wpw.tax.actions.calculateDaysDifference(taxStartDate, before2014);
        if (comp >= field('1454').get()) {
          field('1451').assign(field('1454').get());
        }
        else {
          field('1451').assign(comp);
        }

      });
      calcUtils.getGlobalValue('1452', 'ratesFed', '342');
      calcUtils.divide('1453', '1460', '1454', ['1451', '1452'], [1 / 100]);

      calcUtils.equals('1455', '450');
      field('1459').assign(daysFiscalPeriod);

      var taxStartDate = calcUtils.form('CP').field('tax_start').get();
      var after2013 = wpw.tax.date(2013, 1, 1);
      var comp = wpw.tax.actions.calculateDaysDifference(after2013, taxStartDate);
      if (comp >= field('1459').get()) {
        field('1456').assign(field('1459').get());
      }
      else {
        field('1456').assign(comp);
      }

      calcUtils.getGlobalValue('1457', 'ratesFed', '1343');
      calcUtils.divide('1458', '1455', '1459', ['1456', '1457'], [1 / 100]);

      calcUtils.subtract('451', '1453', '1458');
      calcUtils.equals('452', '451');
      calcUtils.equals('453', '370');
      calcUtils.getGlobalValue('461', 'ratesFed', '341');
      calcUtils.getGlobalValue('481', 'ratesFed', '342');
      calcUtils.getGlobalValue('491', 'ratesFed', '1343');
      calcUtils.sumBucketValues('483', ['462', '482', '492']);
      calcUtils.equals('484', '483');
      calcUtils.sumBucketValues('485', ['422', '432', '442', '452', '484']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 12
      calcUtils.sumBucketValues('516', ['510', '515']);
      calcUtils.equals('517', '516');
      calcUtils.subtract('520', '509', '517');
      calcUtils.equals('540', '485');
      calcUtils.sumBucketValues('551', ['530', '540', '550']);
      calcUtils.equals('552', '551');
      calcUtils.sumBucketValues('553', ['520', '552']);
      optimizationCreditCalcs(calcUtils);
      calcUtils.equals('561', '914');
      calcUtils.sumBucketValues('581', ['560', '561', '580']);
      calcUtils.equals('582', '581');
      calcUtils.subtract('583', '553', '582');
      calcUtils.getApplicableValue('610', ['1657', '665']);
      calcUtils.subtract('620', '583', '610');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 14
      var isQualified = field('101').get() == 1;
      if (isQualified) {
        field('651').assign(field('540').get() + field('550').get() - field('484').get());
        calcUtils.min('652', ['651', '583']);
        if (field('650').get() == 1) {
          calcUtils.removeValue([653, 654], true);
          calcUtils.multiply('1655', ['652'], (40 / 100), true);
        }
        else {
          calcUtils.min('653', ['652', '422'], true);
          calcUtils.subtract('654', '652', '653');
          calcUtils.multiply('1655', ['654'], (40 / 100));
        }

        calcUtils.equals('1656', '653');
        calcUtils.sumBucketValues('1657', ['1655', '1656']);
      }
      else {
        calcUtils.removeValue([651, 652, 653, 654, 1655, 1656, 1657], true);
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 15
      var isQualified = field('101').get() == 1;
      if (isQualified) {
        calcUtils.removeValue([1658, 1659, 1661, 1662, 1663, 664, 665], true);
      }
      else {
        //Part 15
        calcUtils.equals('1658', '583');
        calcUtils.min('1659', ['1658', '422']);
        calcUtils.subtract('1661', '1658', '1659');
        calcUtils.min('1662', ['1661', '442']);
        calcUtils.multiply('1663', ['1662'], (40 / 100));
        calcUtils.equals('664', '1659');
        calcUtils.sumBucketValues('665', ['1663', '664']);
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 16 table 699
      field('699').getRows().forEach(function(row) {
        row[2].assign(Math.min(row[0].get(), row[1].get()));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //table719
      field('719').getRows().forEach(function(row, rowIndex) {
        row[3].assign(row[0].get() * row[1].get() * (1 / 100) - row[2].get());
        row[5].assign(Math.min(row[3].get(), row[4].get()));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 17
      calcUtils.equals('761', '711');
      calcUtils.equals('762', '751');
      calcUtils.equals('763', '760');
      calcUtils.sumBucketValues('764', ['761', '762', '763']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 18
      calcUtils.equals('827', '826');
      calcUtils.equals('828', '827');
      calcUtils.sumBucketValues('830', ['810', '811', '812', '813', '820', '821', '828']);
      calcUtils.subtract('833', '830', '832');
      calcUtils.sumBucketValues('836', ['833', '835']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 19
      calcUtils.sumBucketValues('846', ['841', '845']);
      calcUtils.equals('847', '846');
      calcUtils.subtract('850', '839', '847');
      field('861').assign(10);
      field('862').assign(5);
      field('863').assign(7);
      field('864').assign(4);
      calcUtils.sumBucketValues('880', ['871', '873', '875', '877']);
      calcUtils.equals('881', '880');
      calcUtils.sumBucketValues('882', ['850', '860', '881']);
      calcUtils.equals('886', '924');
      optimizationCreditCalcs(calcUtils);
      calcUtils.sumBucketValues('887', ['885', '886']);
      calcUtils.equals('888', '887');
      calcUtils.subtract('890', '882', '888');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 21
      field('1500').getRows().forEach(function(row) {
        row[3].assign(row[2].get() * (10 / 100));
        row[4].assign(Math.min(row[3].get(), 2000));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 22
      calcUtils.sumBucketValues('616', ['612', '615']);
      calcUtils.equals('617', '616');
      calcUtils.subtract('625', '613', '617');

      field('640').assign(field('1500').get().totals[4]);
      calcUtils.sumBucketValues('656', ['630', '635', '640', '655']);
      calcUtils.equals('657', '656');
      calcUtils.sumBucketValues('658', ['625', '657']);
      optimizationCreditCalcs(calcUtils);
      calcUtils.equals('661', '934');
      calcUtils.sumBucketValues('662', ['660', '661']);
      calcUtils.equals('663', '662');
      calcUtils.subtract('690', '658', '663');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 24
      calcUtils.sumBucketValues('706', ['715', '705']);
      calcUtils.subtract('726', '706', '725');
      calcUtils.sumBucketValues('745', ['726', '735']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 25
      calcUtils.equals('752', '745');
      calcUtils.getGlobalValue('753', 'ratesFed', '367');
      calcUtils.getGlobalValue('756', 'ratesFed', '368');
      calcUtils.min('758', ['754', '757']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 26
      calcUtils.sumBucketValues('771', ['765', '770']);
      calcUtils.equals('772', '771');
      calcUtils.subtract('775', '1764', '772');
      calcUtils.equals('780', '758');
      calcUtils.sumBucketValues('783', ['777', '780', '782']);
      calcUtils.equals('784', '783');
      calcUtils.sumBucketValues('786', ['775', '784']);
      calcUtils.equals('787', '944');
      optimizationCreditCalcs(calcUtils);
      calcUtils.sumBucketValues('788', ['785', '787']);
      calcUtils.equals('789', '788');
      calcUtils.subtract('790', '786', '789');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 28
      calcUtils.min('798', ['795', '797']);
      calcUtils.sumBucketValues('1001', ['792', '798', '799']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 29
      calcUtils.equals('1002', '764');
      calcUtils.equals('1003', '1001');
      calcUtils.sumBucketValues('1004', ['1002', '1003']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 30
      calcUtils.equals('1005', '260');
      calcUtils.equals('1006', '560');
      calcUtils.equals('1007', '885');
      calcUtils.equals('1008', '660');
      calcUtils.equals('1009', '785');
      calcUtils.sumBucketValues('1010', ['1005', '1006', '1007', '1008', '1009']);
    });

    calcUtils.calc(function(calcUtils) {
      var num = 236;
      var num2 = 240;
      var midnum = 237;
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = 241;
      var num2 = 242;
      var midnum = 238;
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = 402;
      var num2 = 404;
      var midnum = 1403;
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get();
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = 420;
      var num2 = 422;
      var midnum = 421;
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = 440;
      var num2 = 442;
      var midnum = 441;
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = 460;
      var num2 = 462;
      var midnum = 461;
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = 480;
      var num2 = 482;
      var midnum = 481;
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = 490;
      var num2 = 492;
      var midnum = 491;
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = 870;
      var num2 = 871;
      var midnum = 861;
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = 872;
      var num2 = 873;
      var midnum = 862;
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = 874;
      var num2 = 875;
      var midnum = 863;
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = 876;
      var num2 = 877;
      var midnum = 864;
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = 752;
      var num2 = 754;
      var midnum = 753;
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = 755;
      var num2 = 757;
      var midnum = 756;
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get();
      calcUtils.field(num2).assign(product);
    });
  });
})();
