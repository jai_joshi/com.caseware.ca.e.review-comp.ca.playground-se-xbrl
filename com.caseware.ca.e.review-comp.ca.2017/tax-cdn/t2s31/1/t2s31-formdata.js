(function() {
  var labelWidth = '70%';
  //TODO: num 403 cannot be passed in values, and num 1403 has replaced 403

  wpw.tax.global.formData.t2s31 = {
    'formInfo': {
      'abbreviation': 'T2S31',
      'title': 'Investment Tax Credit - Corporations',
      code: 'Code 1401',
      'schedule': 'Schedule 31',
      'headerImage': 'canada-federal',
      formFooterNum: 'T2 SCH 31 E (16)',
      category: 'Federal Tax Forms'
    },
    'sections': [
      {
        'header': 'General information',
        'rows': [
          {
            label: '• Use this schedule:',
            labelClass: 'fullLength'
          },
          {
            label: '– to calculate an investment tax credit (ITC) earned during the tax year;',
            labelClass: 'tabbed fullLength'
          },
          {
            label: '– to claim a deduction against Part I tax payable;',
            labelClass: 'tabbed fullLength'
          },
          {
            label: '– to claim a refund of credit earned during the current tax year;',
            labelClass: 'tabbed fullLength'
          },
          {
            label: '– to claim a carryforward of credit from previous tax years;',
            labelClass: 'tabbed fullLength'
          },
          {
            label: '– to transfer a credit following an amalgamation or wind-up of a subsidiary, as described ' +
            'under subsections 87(1) and 88(1);',
            labelClass: 'tabbed fullLength'
          },
          {
            label: '– to request a credit carryback to one or more previous years; or',
            labelClass: 'tabbed fullLength'
          },
          {
            label: '– if you are subject to a recapture of ITC.',
            labelClass: 'tabbed fullLength'
          },
          {
            label: '• Unless otherwise noted, all legislative references are to the <i>Income Tax Act</i> and the ' +
            '<i>Income Tax Regulations</i>.',
            labelClass: 'fullLength'
          },
          {
            label: '• The ITC is eligible for a three-year carryback (if not deductible in the year earned). ' +
            'It is also eligible for a twenty-year carryforward.',
            labelClass: 'fullLength'
          },
          // {
          //   label: 'All legislative references are to the <i>Income Tax Act</i> and the <i>Income Tax Regulations</i>.',
          //   labelClass: 'fullLength'
          // },
          {
            label: '• Investments or expenditures, described in subsection 127(9) and Regulation Part XLVI, that earn an ITC are:',
            labelClass: 'fullLength'
          },
          {
            label: '– qualified property and qualified resource property (Parts 4 to 7 of this schedule);',
            labelClass: 'tabbed fullLength'
          },
          {
            label: '– expenditures that are part of the scientific research and experimental development (SR&ED) ' +
            'qualified expenditure pool (Parts 8 to 17). File Form T661, <i>Scientific Research and Experimental Development (SR&ED) Expenditures Claim</i>;',
            labelClass: 'tabbed fullLength'
          },
          {
            label: '– pre-production mining expenditures (Parts 18 to 20);',
            labelClass: 'tabbed fullLength'
          },
          {
            label: '– apprenticeship job creation expenditures (Parts 21 to 23); and',
            labelClass: 'tabbed fullLength'
          },
          {
            label: '– child care spaces expenditures (Parts 24 to 28).',
            labelClass: 'tabbed fullLength'
          },
          {
            label: '• Include a completed copy of this schedule with the <i>T2 Corporation Income Tax Return</i>. ' +
            'If you need more space, attach additional schedules.',
            labelClass: 'fullLength'
          },
          {
            label: '• For more information on ITCs, see "Investment Tax Credit" in Guide T4012, ' +
            '<i>T2 Corporation – Income Tax Guide</i>, Information Circular IC78-4, ' +
            '<i>Investment Tax Credit Rates</i>, and its related Special Release.',
            labelClass: 'fullLength'
          },
          {
            label: '• For more information on SR&ED, see T4088, <i>Guide to Form T661 – Scientific Research and ' +
            'Experimental Development (SR&ED) Expenditures Claim</i>. Also see the <i>Eligibility of Work for SR&ED ' +
            'Investment Tax Credits Policy</i> at ' +
            '<b>cra.gc.ca/txcrdt/sred-rsde/clmng/lgbltywrkfrsrdnvstmnttxcrdts-eng.html</b>.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        forceBreakAfter: true,
        'header': 'Detailed information',
        'rows': [
          {
            label: '• For the purpose of this schedule, <b>investment</b> means the capital cost of the property ' +
            '(excluding amounts added by an election under section 21), determined without reference to ' +
            'subsections 13(7.1) and 13(7.4), minus the amount of any government or non-government assistance ' +
            'that the corporation has received, is entitled to receive, or can reasonably be expected to ' +
            'receive for that property when it files the income tax return for the year in which the ' +
            'property was acquired.',
            labelClass: 'fullLength'
          },
          {
            label: '• An ITC deducted or refunded in a tax year for a depreciable property, other than a ' +
            'depreciable property deductible under paragraph 37(1)(b), reduces both the capital cost of ' +
            'that property and the undepreciated capital cost of that class in the next tax year. An ITC ' +
            'for SR&ED deducted or refunded in a tax year will reduce the balance in the pool of deductible ' +
            'SR&ED expenditures and the adjusted cost base (ACB) of an interest in a partnership in the ' +
            'next tax year. An ITC from pre-production mining expenditures deducted in a tax year reduces ' +
            'the balance in the pool of deductible cumulative Canadian exploration expenses in the next tax year.',
            labelClass: 'fullLength'
          },
          {
            label: '• Property acquired has to be <b>available for use</b> before a claim for an ITC ' +
            'can be made. See subsections 127(11.2) and 248(19) for more information.',
            labelClass: 'fullLength'
          },
          {
            label: '• Expenditures for SR&ED and capital costs for a property qualifying for an ITC must be ' +
            'identified by the claimant on Form T661 and Schedule 31 no later than 12 months after the ' +
            'claimant\'s income tax return is due for the tax year in which it incurred the expenditures or ' +
            'capital costs.',
            labelClass: 'fullLength'
          },
          {
            label: '• Expenditures for pre-production mining, apprenticeship, or child care space for an ITC ' +
            'must be identified by the claimant on Schedule 31 no later than 12 months after the ' +
            'claimant\'s income tax return is due for the tax year in which it incurred the expenditures ' +
            'or capital costs.',
            labelClass: 'fullLength'
          },
          {
            label: '• Partnership allocations – Subsection 127(8) provides for the allocation of the amount ' +
            'that may reasonably be considered to be a partner\'s share of the ITCs of the partnership ' +
            'at the end of the fiscal period of the partnership. An allocation of ITCs is generally ' +
            'considered to be the partner\'s reasonable share of the ITCs if it is made in the same ' +
            'proportion in which the partners have agreed to share any income or loss and if section ' +
            '103 is not applicable for the agreement to share any income or loss. Special rules apply ' +
            'to specified and limited partners. For more information, see Guide T4068, ' +
            '<i>Guide for the Partnership Information Return</i>.',
            labelClass: 'fullLength'
          },
          {
            label: '• For SR&ED expenditures, the expression <b>in Canada</b> includes the "exclusive economic zone"' +
            ' (as defined in the Oceans Act to generally consist of an area that is within 200 nautical ' +
            'miles from the Canadian coastline), including the airspace, seabed and subsoil for that zone.',
            labelClass: 'fullLength'
          },
          {
            label: '• For the purpose of this schedule, the expression <b>Atlantic Canada</b> includes the ' +
            'Gaspé Peninsula and the provinces of Newfoundland and Labrador, Prince Edward Island, Nova Scotia,' +
            ' and New Brunswick, as well as their respective offshore regions (prescribed in Regulation 4609).',
            labelClass: 'fullLength'
          },
          {
            label: '• For the purpose of this schedule, <b>qualified property</b> means property in Atlantic ' +
            'Canada that is used primarily for manufacturing and processing, farming or fishing, logging, ' +
            'storing grain, or harvesting peat. Property in Atlantic Canada that is used primarily for oil ' +
            'and gas, and mining activities is considered qualified property only if acquired by the ' +
            'taxpayer <b>before</b> March 29, 2012. Qualified property includes new buildings and new machinery ' +
            'and equipment (prescribed in Regulation 4600), and if acquired by the taxpayer <b>after</b> ' +
            'March 28, 2012, new energy generation and conservation property (prescribed in ' +
            'Regulation 4600). Qualified property can also be used primarily to produce or process ' +
            'electrical energy or steam in a prescribed area (as described in Regulation 4610). See ' +
            'the definition of <b>qualified property</b> in subsection 127(9) for more information.',
            labelClass: 'fullLength'
          },
          {
            label: '• For the purpose of this schedule, <b>qualified resource property</b> means property in ' +
            'Atlantic Canada that is used primarily for oil and gas, and mining activities, if acquired by ' +
            'the taxpayer <b>after</b> March 28, 2012, and <b>before</b> January 1, 2016. Qualified resource ' +
            'property includes new buildings and new  machinery and equipment (prescribed in Regulation ' +
            '4600). See the definition of <b>qualified resource property</b> in subsection 127(9) ' +
            'for more information.',
            labelClass: 'fullLength'
          },
          {
            label: '• For the purpose of this schedule, <b>pre-production mining exploration expenditures</b>' +
            ' are pre-production mining expenditures incurred <b>after</b> March 28, 2012, by the ' +
            'taxpayer to determine the existence, location, extent, or quality of certain mineral ' +
            'resources in Canada, excluding expenses incurred in the exploration of an oil or gas well. ' +
            'See subparagraph (a)(i) of the definition of <b>pre-production mining expenditure</b>' +
            ' in subsection 127(9) for more information.',
            labelClass: 'fullLength'
          },
          {
            label: '• For the purpose of this schedule, <b>pre-production mining development expenditures</b>' +
            ' are pre-production mining expenditures incurred <b>after</b> March 28, 2012, by the taxpayer to bring ' +
            'a new mineral resource mine in Canada into production, excluding expenses in the development of ' +
            'a bituminous sands deposit or an oil shale deposit. See subparagraph (a)(ii) of the definition ' +
            'of <b>pre-production mining expenditure</b> in subsection 127(9) for more information.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 1 - Investments, expenditures, and percentages',
        'rows': [
          {
            'type': 'table',
            'fixedRows': true,
            'num': '050'
          },
          {
            'label': 'Qualified property acquired primarily for use in Atlantic Canada',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '051'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Qualified resource property acquired primarily for use in Atlantic Canada and acquired:',
            'labelWidth': 'labelWidth'
          },
          {
            'label': '- after March 28, 2012, and before 2014',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '052'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '- after 2013 and before 2016',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '053'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '- after 2015*',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '054'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Expenditures',
            'labelClass': 'bold',
            'labelWidth': 'labelWidth'
          },
          {
            'label': 'If you are a Canadian-controlled private corporation (CCPC), this percentage may apply to the portion that you claim of the SR&ED qualified expenditure pool that does not exceed your expenditure limit (see Part 10 on page 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '055'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '<b>Note</b>: If your current year\'s qualified expenditures are more than your expenditure limit (see Part 10 on page 5), the excess is eligible for an ITC calculated at the 20% rate**.',
            'labelWidth': labelWidth,
            'labelClass': 'tabbed'
          },
          {
            'label': 'If you are a corporation that is not a CCPC and have incurred qualified expenditures for SR&ED in any area in Canada:',
            'labelWidth': 'labelWidth'
          },
          {
            'label': '- before 2014**',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '056'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '- after 2013**',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '057'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'If you are a taxable Canadian corporation that incurred pre-production mining expenditures before March 29, 2012',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '058'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'If you are a taxable Canadian corporation that incurred pre-production mining exploration expenditures:',
            'labelWidth': 'labelWidth'
          },
          {
            'label': '- after March 28, 2012, and before 2013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '059'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '- in 2013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '060'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '- after 2013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '061'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'If you are a taxable Canadian corporation that incurred pre-production mining development expenditures***:',
            'labelWidth': 'labelWidth'
          },
          {
            'label': '- after March 28, 2012, and before 2014',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '062'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '- in 2014',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '063'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '- in 2015',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '064'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '- after 2015',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '065'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If you paid salary and wages to apprentices in the first 24 months of their apprenticeship contract for employment',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '066'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'If you incurred eligible expenditures after March 18, 2007, for the creation of licensed child care spaces for the children of your employees and, potentially, for other children',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '067'
                }
              }
            ],
            'indicator': '%'
          },
          {labelClass: 'fullLength'},
          {
            label: '* A transitional relief rate of 10% may apply to property acquired after 2013 and before 2017,' +
            ' if the property is acquired under a written agreement entered into before March 29, 2012, or the property' +
            ' is acquired as part of a phase of a project where the construction or the engineering and design work ' +
            'for the construction started before March 29, 2012. See paragraph (a.1) of the definition of specified' +
            ' percentage in subsection 127(9) for more information.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** The reduction of the rate from 20% to 15% applies to 2014 and later tax years, except that, for 2014 tax years that start before 2014, the reduction is pro-rated based on the number of days in the tax year that are after 2013.',
            'labelClass': 'fullLength'
          },
          {
            'label': '*** A transitional relief rate may apply to expenditures incurred after 2013 and before 2016,' +
            ' if the expenditure is incurred under a written agreement entered into before March 29, 2012, or the ' +
            'expenditure is incurred as part of the development of a new mine where the construction or the ' +
            'engineering and design work for the construction of the new mine started before March 29, 2012. See' +
            ' subparagraphs (k)(ii) and (iii) of the definition of <b>specified percentage</b>' +
            ' in subsection 127(9) for more information. ',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Part 2 - Determination of a qualifying corporation',
        'rows': [
          {
            label: 'Is the corporation a qualifying corporation?',
            type: 'infoField',
            inputType: 'radio',
            labelWidth: labelWidth,
            num: '101',
            tn: '101',
            init: '2'
          },
          {labelClass: 'fullLength'},
          {
            label: 'For the purpose of a refundable ITC, a <b>qualifying corporation</b> is defined under subsection' +
            ' 127.1(2). The corporation has to be a CCPC and its taxable income (before any loss carrybacks) for its' +
            ' previous tax year cannot be more than its <b>qualifying income limit</b> for the particular tax year. ' +
            'If the corporation is associated with any other corporations during the tax year, the total of the taxable' +
            ' incomes of the corporation and the associated corporations (before any loss carrybacks), for their last' +
            ' tax year ending in the previous calendar year, cannot be more than their qualifying income ' +
            'limit for the particular tax year.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: '<b>Note</b>: A CCPC considered associated with another corporation under subsection 256(1)' +
            ' will be considered not associated for the calculation of a refundable ITC if:',
            labelClass: 'fullLength'
          },
          {
            label: '• one corporation is associated with another corporation solely because one or more ' +
            'persons own shares of the capital stock of both corporations; and',
            labelClass: 'fullLength tabbed'
          },
          {
            label: '• one of the corporations has at least one shareholder who is not common to both corporations.',
            labelClass: 'fullLength tabbed'
          },
          {labelClass: 'fullLength'},
          {
            label: 'If you are a <b>qualifying</b> corporation, you will earn a <b>100%</b> refund on your ' +
            'share of any ITCs earned at the 35% rate on qualified <b>current</b> expenditures for SR&ED, ' +
            'up to the allocated expenditure limit. The 100% refund does not apply to qualified <b>capital</b>' +
            ' expenditures eligible for the 35% credit rate. They are only eligible for the <b>40%</b> refund*.',
            labelClass: 'fullLength'
          },
          {
            label: 'Some CCPCs that are <b>not qualifying</b> corporations may also earn a <b>100%</b> refund on ' +
            'their share of any ITCs earned at the 35% rate on qualified <b>current</b> expenditures for SR&ED, ' +
            'up to the allocated expenditure limit. The expenditure limit can be determined in Part 10 on ' +
            'page 5. The 100% refund does not apply to qualified <b>capital</b> expenditures eligible for the 35% ' +
            'credit rate. They are only eligible for the <b>40%</b> refund*.',
            labelClass: 'fullLength'
          },
          {
            label: 'The 100% refund will not be available to a corporation that is an <b>excluded corporation</b>' +
            ' as defined under subsection 127.1(2). A corporation is an excluded corporation if, at any time ' +
            'during the year, it is a corporation that is either controlled by (directly or indirectly, in any ' +
            'manner whatever) or is related to:',
            labelClass: 'fullLength'
          },
          {
            label: 'a) one or more persons exempt from Part I tax under section 149;',
            labelClass: 'fullLength tabbed'
          },
          {
            label: 'b) Her Majesty in right of a province, a Canadian municipality, or any other public authority; or',
            labelClass: 'fullLength tabbed'
          },
          {
            label: 'c) any combination of persons referred to in a) or b) above.',
            labelClass: 'fullLength tabbed'
          },
          {labelClass: 'fullLength'},
          {
            label: '* Capital expenditures incurred after December 31, 2013, including lease payments for property ' +
            'that would have been a capital expenditure if purchased directly, are <b>not</b> qualified SR&ED ' +
            'expenditures and are <b>not</b> eligible for an ITC on SR&ED expenditures.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Corporations in the farming industry',
        'rows': [
          {
            label: 'Complete this area if the corporation is making SR&ED contributions.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Is the corporation claiming a contribution in the current year to an agricultural ' +
            'organization whose goal is to finance SR&ED work (for example, check-off dues)?',
            type: 'infoField',
            inputType: 'radio',
            labelWidth: labelWidth,
            num: '102',
            tn: '102',
            canClear: true,
            'indicatorColumn': true
          },
          {
            label: 'If <b>yes</b>, complete Schedule 125, <i>Income Statement Information</i>, to identify the ' +
            'type of farming industry the corporation is involved in.',
            labelClass: 'fullLength'
          },
          {
            'label': 'Contributions to agricultural organizations for SR&ED* <br> (Enter this amount on line 350 of Part 8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '103',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '103'
                }
              }
            ]
          },
          {labelClass: 'fullLength'},
          {
            label: '* Enter only contributions not already included on Form T661.',
            labelClass: 'fullLength '
          },
          {
            label: 'Include 80% of the contributions made <b>after</b> 2012. For contributions made <b>before</b> ' +
            '2013, include all of the contributions.',
            labelClass: 'fullLength'

          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {
            label: 'Qualified Property and Qualified Resource Property',
            labelClass: 'fullLength titleFont center'
          }
        ]
      },
      {
        'header': 'Part 4 – Eligible investments for qualified property and qualified resource property ' +
        'from the current tax year',
        'rows': [
          {
            type: 'table',
            num: '099'
          }
          //{
          //  label: '* CCA: capital cost allowance'
          //},
          //{
          //  label: '** Type of Investment:'
          //},
          //{
          //  label: '- Type 1: Qualified property',
          //  labelClass: 'tabbed'
          //},
          //{
          //  label: '- Type 2: Qualified resource property acquired after March 28, 2012, and before January 1, 2014',
          //  labelClass: 'tabbed'
          //},
          //{
          //  label: '- Type 3: Qualified resource property acquired after December 31, 2013, and before January 1, 2016',
          //  labelClass: 'tabbed'
          //}
        ]
      },
      {
        'header': 'Part 5 - Current-year credit and account balances - ITC from investments in qualified property and qualified resource property',
        'rows': [
          {
            'label': 'ITC at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '209'
                }
              }
            ],
            'indicator': 'B1'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit deemed as a remittance of co-op corporations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '210',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '210'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit expired',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '215',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '215'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 210 <b>plus</b> line 215)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '216'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '217'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'C1'
          },
          {
            'label': 'ITC at the beginning of the tax year (amount B1 <b>minus</b> amount C1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '220',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '220'
                }
              }
            ]
          },
          {
            'label': 'Add',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit transferred on amalgamation or wind-up of subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '230',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '230'
                }
              },
              null
            ]
          },
          {
            'label': 'ITC from repayment of assistance',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '235',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '235'
                }
              },
              null
            ]
          },
          {
            'type': 'table',
            'num': 'di_table_1'
          },
          {
            'type': 'table',
            'num': 'di_table_2'
          },
          {
            'label': 'Credit allocated from a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '250',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '250'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of lines 230 to 250)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '251'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '252'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D1'
          },
          {
            'label': 'Total credit available (line 220 <b>plus</b> amount D1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '253'
                }
              }
            ],
            'indicator': 'E1'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit deducted from Part I tax (enter this amount at line D8 in Part 30)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '260',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '260'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit carried back to the previous year(s) (from amount H1 in Part 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '261'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'Credit transferred to offset Part VII tax liability',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '280',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '280'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of line 260, amount a, and line 280)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '281'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '282'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'F1'
          },
          {
            'label': 'Credit balance before refund (amount E1 <b>minus</b> amount F1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '283'
                }
              }
            ],
            'indicator': 'G1'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Refund of credit claimed on investments from qualified property and qualified resource property (from Part 7)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '310',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '310'
                }
              }
            ]
          },
          {
            'label': '<b>ITC closing balance of investments from qualified property and qualified resource property</b><br>(amount G1 <b>minus</b> line 310)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '320',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '320'
                }
              }
            ]
          },
          {
            'label': '* Include investments acquired after 2013 and before 2017 that are eligible for transitional relief.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 6 - Request for carryback of credit from investments in qualified property and qualified resource property',
        'rows': [
          {
            'type': 'table',
            'num': '1200'
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Part 7 - Refund of ITC for qualifying corporations on investments from qualified property and qualified resource property',
        'rows': [
          {
            'label': 'Current-year ITCs (total of lines 240, 242, and 250 in Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '905'
                }
              }
            ],
            'indicator': 'I1'
          },
          {
            'label': 'Credit balance before refund (from amount G1 in Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '906'
                }
              }
            ],
            'indicator': 'J1'
          },
          {
            'label': '<b>Refund</b> (40% of amount I1 or J1, whichever is less)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '907'
                }
              }
            ],
            'indicator': 'K1'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Enter amount K1 or a lesser amount on line 310 in Part 5 (also enter it on line 780 of the T2 ' +
            'return if you don\'t claim an SR&ED ITC refund).',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'hideFieldset': true,
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'SR&ED',
            'labelClass': 'fullLength titleFont center'
          }
        ]
      },
      {
        'header': 'Part 8 - Qualified SR&ED expenditures',
        'rows': [
          {
            'label': 'Current expenditures (line 557 on Form T661 <b>plus</b> line 103 in Part 3)*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '350',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '350'
                }
              }
            ]
          },
          {
            'label': 'Capital expenditures incurred <b>before</b> 2014 (from line 558 on Form T661)**',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '360',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '360'
                }
              }
            ]
          },
          {
            'label': 'Repayments made in the year (from line 560 on Form T661)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '370',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '370'
                }
              }
            ]
          },
          {
            'label': '<b>Qualified SR&ED expenditures</b> (total of lines 350 to 370)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '380',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '380'
                }
              }
            ]
          },
          {labelClass: 'fullLength'},
          {
            'label': '* If you are claiming only contributions made to agricultural organizations for SR&ED, line 350 should equal line 103 in Part 3. Do not file Form T661.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '** Capital expenditures incurred after December 31, 2013, are not qualified SR&ED expenditures. Capital cost allowance can be claimed for depreciable property acquired for use in SR&ED after 2013.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'header': 'Part 9 - Components of the SR&ED expenditure limit calculation',
        'rows': [
          {
            'label': 'Part 9 only applies if you are a CCPC.',
            'labelClass': 'fullLength bold'
          },
          {labelClass: 'fullLength'},
          {
            'label': '<b>Note:</b> A CCPC considered associated with another corporation under subsection 256(1) ' +
            'will be considered not associated for the calculation of an SR&ED expenditure limit if:',
            'labelClass': 'fullLength'
          },
          {
            'label': '• one corporation is associated with another corporation solely because one or more persons own shares of the capital stock of the corporation; and',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• one of the corporations has at least one shareholder who is not common to both corporations.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'Is the corporation associated with another CCPC for the purpose of calculating the SR&ED expenditure limit?',
            'type': 'infoField',
            'inputType': 'radio',
            'labelWidth': '84%',
            'num': '385',
            'tn': '385',
            'canClear': true,
          },
          {
            label: 'If you answered <b>no</b> to the question on line 385 above or if you are not associated with any' +
            ' other corporations, complete lines 390 and 398 . ',
            labelClass: 'fullLength tabbed'
          },
          {
            label: 'If you answered <b>yes</b>, the amounts for associated corporations will be determined ' +
            'on Schedule 49.',
            labelClass: 'fullLength tabbed'
          },
          {
            'label': 'Enter your taxable income for the previous tax year* (prior to any loss carrybacks applied)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '390',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '390'
                }
              }
            ]
          },
          {
            'label': 'Enter your taxable capital employed in Canada for the previous tax year minus $10 million. <br>If this amount is nil or negative, enter "0". If this amount is over $40 million, enter $40 million',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '398',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '398'
                }
              }
            ]
          },
          {labelClass: 'fullLength'},
          {
            'label': '* If the tax year referred to on line 390 is less than 51 weeks, <b>multiply</b> the taxable income by the following result: 365 <b>divided</b> by the number of days in that tax year.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'header': 'Part 10 - SR&ED expenditure limit for a CCPC',
        'rows': [
          {
            'label': 'For a stand-alone (not associated) corporation:',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                padding: {type: 'text', data: '$'},
                'input': {
                  'num': '401'
                }
              }
            ]
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'num': 'di_table_3'
          },
          {
            'label': 'Excess ($8,000,000 <b>minus</b> amount A2; if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '405'
                }
              }
            ],
            'indicator': 'B2'
          },
          {
            'label': '$40,000,000 <b>minus</b> line 398 in Part 9',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '406'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'Amount a <b>divided</b> by $40,000,000',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '407',
                  'decimals': 5
                }
              }
            ],
            'indicator': 'C2'
          },
          {
            'label': '<b>Expenditure limit for the stand-alone corporation</b> (amount B2 <b>multiplied</b> by amount C2)*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '408'
                }
              }
            ],
            'indicator': 'D2'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'For an associated corporation:',
            'labelClass': 'bold'
          },
          {
            'label': 'If associated, the allocation of the SR&ED expenditure limit, as provided on Schedule 49*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '400',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '400'
                }
              }
            ],
            'indicator': 'E2'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'If your tax year is less than 51 weeks, calculate the amount of the expenditure limit as follows:',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '411'
          },
          {
            'label': '<b>Your SR&ED expenditure limit for the year</b> (enter the amount from amount D2, E2, or F2, whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '410',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '410'
                }
              }
            ]
          },
          {
            'label': '* Amount D2 or E2 cannot be more than $3,000,000.'
          }
        ]
      },
      {
        'header': 'Part 11 - Investment tax credits on SR&ED expenditures',
        'rows': [
          {
            'type': 'table',
            'num': 'di_table_4'
          },
          {
            'label': 'Line 350 <b>minus</b> line 410 (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '430',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '430'
                }
              },
              null
            ]
          },
          {
            'type': 'table',
            'num': '434'
          },
          {
            'label': 'Subtotal (amount b <b>plus</b> amount c)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '431'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '432'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'H2'
          },
          {
            'label': 'Line 410 <b>minus</b> line 350 (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '435'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': 'di_table_5'
          },
          {
            'label': 'Line 360 <b>minus</b> amount d above (if negative, enter "0") ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '450',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '450'
                }
              },
              null
            ]
          },
          {
            'type': 'table',
            'num': '454'
          },
          {
            'label': 'Subtotal (amount e <b>plus</b> amount f)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '451'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '452'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'J2'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'If a corporation makes a repayment of any government or non-government assistance, or contract payments that reduced theamount of qualified expenditures for ITC purposes, the amount of the repayment is eligible for a credit.',
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Repayments</b> (amount from line 370 in Part 8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '453'
                }
              },
              null,
              null
            ]
          },
          {
            'type': 'table',
            'num': 'di_table_6'
          },
          {
            'type': 'table',
            'num': 'di_table_7'
          },
          {
            'type': 'table',
            'num': 'di_table_8'
          },
          {
            'label': 'Subtotal (<b>add</b> amounts g to i)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '483'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '484'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'K2'
          },
          {
            'label': '<b>Current-year SR&ED ITC</b> (total of amounts G2 to K2; enter on line 540 in Part 12)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '485'
                }
              }
            ],
            'indicator': 'L2'
          },
          {labelClass: 'fullLength'},
          {
            'label': '* For corporations that are not CCPCs, enter "0" for amounts G2 and I2.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** For tax years that end after 2013, the general SR&ED ITC rate is reduced from 20% to 15%, ' +
            'except that, for 2014 tax years that start before 2014, the reduction is pro-rated based on the number of' +
            ' days in the tax year that are after 2013. For tax years that have a start date after 2013, multiply the ' +
            'amount by 15%.',
            'labelClass': 'fullLength'
          },
          {
            'label': '***  If you are reporting a repayment for a tax year which included two calendar years with ' +
            'different rates (such as a 2014 tax year that started in 2013), the amount of repayment is allocated' +
            ' between the two ITC rates as follows:',
            'labelClass': 'fullLength'
          },
          {
            'label': '– For the first part of the tax year, enter on the line next to the applicable ITC rate, ' +
            'the result of the following calculation: The full repayment amount <b>multiplied</b> by the number of days' +
            ' in the tax year which were in the first calendar year, <b>divided</b> by the total number of days in the tax year.',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': 'For the last part of the tax year which is in the second calendar year, enter on the line next ' +
            'to the applicable ITC rate, the difference between the first part calculated above and the full repayment amount.',
            'labelClass': 'tabbed fullLength'
          }
        ]
      },
      {
        'header': 'Part 12 - Current-year credit and account balances - ITC from SR&ED expenditures',
        'rows': [
          {
            'label': 'ITC at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '509'
                }
              }
            ],
            'indicator': 'M2'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit deemed as a remittance of co-op corporations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '510',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '510'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit expired',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '515',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '515'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 510 <b>plus</b> line 515)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '516'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '517'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'N2'
          },
          {
            'label': 'ITC at the beginning of the tax year (amount M2 <b>minus</b> amount N2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '520',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '520'
                }
              }
            ]
          },
          {
            'label': 'Add',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit transferred on amalgamation or wind-up of subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '530',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '530'
                }
              },
              null
            ]
          },
          {
            'label': 'Total current-year credit (from amount L2 in Part 11)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '540',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '540'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit allocated from a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '550',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '550'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of lines 530 to 550)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '551'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '552'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'O2'
          },
          {
            'label': 'Total credit available (line 520 <b>plus</b> amount O2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '553'
                }
              }
            ],
            'indicator': 'P2'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit deducted from Part I tax (enter this amount at line E8 in Part 30)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '560',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '560'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit carried back to the previous year(s) (from amount S2 in Part 13)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '561'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'j'
                }
              }
            ]
          },
          {
            'label': 'Credit transferred to offset Part VII tax liability',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '580',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '580'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of line 560, amount j, and line 580)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '581'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '582'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'Q2'
          },
          {
            'label': 'Credit balance before refund (amount P2 <b>minus</b> amount Q2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '583'
                }
              }
            ],
            'indicator': 'R2'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Refund of credit claimed on SR&ED expenditures (from Part 14 or 15, whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '610',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '610'
                }
              }
            ]
          },
          {
            'label': 'ITC closing balance on SR&ED (amount R2 <b>minus</b> line 610)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '620',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '620'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 13 - Request for carryback of credit from SR&ED expenditures',
        'rows': [
          {
            'type': 'table',
            'num': '1300'
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Part 14 - Refund of ITC for qualifying corporations - SR&ED',
        'rows': [
          {
            'label': 'Complete this part only if you are a qualifying corporation as determined on line 101 in Part 2.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Is the corporation an excluded corporation as defined under subsection 127.1(2)?',
            'type': 'infoField',
            'inputType': 'radio',
            'labelWidth': '84%',
            'num': '650',
            'tn': '650',
            'init': '2'
          },
          {
            'label': 'Current-year ITC (lines 540 <b>plus</b> 550 in Part 12 <b>minus</b> amount K2 in Part 11)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '651'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'k'
                }
              }
            ]
          },
          {
            'label': 'Refundable credits (amount k or amount R2 in Part 12, whichever is less)*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '652'
                }
              }
            ],
            'indicator': 'T2'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Amount T2 or amount G2 in Part 11, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '653'
                }
              }
            ],
            'indicator': 'U2'
          },
          {
            'label': 'Net amount (amount T2 <b>minus</b> amount U2; if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '654'
                }
              }
            ],
            'indicator': 'V2'
          },
          {
            'label': 'Amount V2 <b>multiplied</b> by 40%',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1655'
                }
              }
            ],
            'indicator': 'W2'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Amount U2.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1656'
                }
              }
            ],
            'indicator': 'X2'
          },
          {
            'label': '<b>Refund of ITC</b> (amount W2 <b>plus</b> amount X2 - enter this, or a lesser amount, on line 610 in Part 12)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1657'
                }
              }
            ],
            'indicator': 'Y2'
          },
          {
            'label': 'Enter the total of line 310 in Part 5 and line 610 in Part 12 on line 780 of the T2 return.',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            'label': '* If you are also an excluded corporation, as defined in subsection 127.1(2), this amount must be multiplied by 40%. Claim this, or a lesser amount, as your refund of ITC for amount Y2.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'header': 'Part 15 - Refund of ITC for CCPCs that are not qualifying or excluded corporations - SR&ED',
        'rows': [
          {
            'label': 'Complete this box only if you are a CCPC that is not a qualifying or excluded corporation as determined on line 101 in Part 2.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit balance before refund (from amount R2 in Part 12)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1658'
                }
              }
            ],
            'indicator': 'Z2'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Amount Z2 or amount G2 in Part 11, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1659'
                }
              }
            ],
            'indicator': 'AA2'
          },
          {
            'label': 'Net amount (amount Z2 <b>minus</b> amount AA2; if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1661'
                }
              }
            ],
            'indicator': 'BB2'
          },
          {
            'label': 'Amount BB2 or amount I2 in Part 11, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1662'
                }
              }
            ],
            'indicator': 'CC2'
          },
          {
            'label': 'Amount CC2 <b>multiplied</b> by 40%',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1663'
                }
              }
            ],
            'indicator': 'DD2'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Amount AA2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '664'
                }
              }
            ],
            'indicator': 'EE2'
          },
          {
            'label': '<b>Refund of ITC</b> (amount DD2 <b>plus</b> amount EE2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '665'
                }
              }
            ],
            'indicator': 'FF2'
          },
          {
            'label': 'Enter FF2, or a lesser amount, on line 610 in Part 12 and also on line 780 of the T2 return.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'hideFieldset': true,
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Recapture - SR&ED',
            'labelClass': 'fullLength titleFont center'
          }
        ]
      },
      {
        'header': 'Part 16 - Recapture of ITC for corporations and partnerships – SR&ED',
        'rows': [
          {
            'label': 'You will have a recapture of ITC in a year when <b>all</b> of the following conditions are met:',
            'labelClass': 'fullLength'
          },
          {
            'label': '• you acquired a particular property in the current year or in any of the 20 previous tax' +
            ' years, and the credit was earned in a tax year ending after 1997 and did not expire before 2008;',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• you claimed the cost of the property as a qualified expenditure for SR&ED on Form T661;',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• the cost of the property was included in calculating your ITC or was the subject of an' +
            ' agreement made under subsection 127(13) to transfer qualified expenditures; and',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• you disposed of the property or converted it to commercial use after February 23, 1998.' +
            ' This condition is also met if you disposed of or converted to commercial use a property that incorporates ' +
            'the particular property previously referred to.',
            'labelClass': 'fullLength tabbed'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Note:',
            'labelClass': 'bold tabbed'
          },
          {
            'label': 'The recapture <b>does not apply</b> if you disposed of the property to a non-arm\'s-length purchaser who intended to use it all or substantially all for SR&ED. When the non-arm\'s-length purchaser later sells or converts the property to commercial use, the recapture rules will apply to the purchaser based on the historical ITC rate of the original user.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'You will report a recapture on the T2 return for the year in which you disposed of the property or converted it to commercial use. In the following tax year, add the amount of the ITC recapture to the SR&ED expenditure pool.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'If you have more than one disposition for calculations 1 and 2, complete the columns for each disposition for which a recapture applies, using the calculation formats below.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'type': 'table',
            'num': '699'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '719'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoBox',
            'header': 'Calculation 3',
            'info': 'As a member of the partnership, you will report your share of the SR&ED ITC of the partnership after the SR&ED ITC has been reduced by the amount of the recapture. If this amount is a positive amount, you will report it on line 550 in Part 12 on page 7. However, if the partnership does not have enough ITC otherwise available to offset the recapture, then the amount by which reductions to ITC exceed additions (the excess) will be determined and reported on line 760.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporate partner\'s share of the excess of SR&ED ITC (amount to be reported on line E3 in Part 17)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '760',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '760'
                }
              }
            ]
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Part 17 - Total recapture of SR&ED investment tax credit',
        'rows': [
          {
            'label': 'Recaptured ITC from calculation 1, amount A3 in Part 16',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '761'
                }
              }
            ],
            'indicator': 'C3'
          },
          {
            'label': 'Recaptured ITC from calculation 2, amount B3 in Part 16',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '762'
                }
              }
            ],
            'indicator': 'D3'
          },
          {
            'label': 'Recaptured ITC from calculation 3, line 760 in Part 16',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '763'
                }
              }
            ],
            'indicator': 'E3'
          },
          {
            'label': '<b>Total recapture of SR&ED investment tax credit</b> (total of amounts C3 to E3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '764'
                }
              }
            ],
            'indicator': 'F3'
          },
          {
            'label': 'Enter amount F3 on line A8 in Part 29.'
          }
        ]
      },
      {
        'hideFieldset': true,
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Pre-Production Mining',
            'labelClass': 'fullLength titleFont center'
          }
        ]
      },
      {
        'header': 'Part 18 - Pre-production mining expenditures',
        'rows': [
          {
            'label': 'Exploration information',
            'labelClass': 'fullLength bold center'
          },
          {
            'label': 'A mineral resource that qualifies for the credit means a mineral deposit from which the principal mineral to be extracted is diamond, a base or precious metal deposit, or a mineral deposit from which the principal mineral to be extracted is an industrial mineral that, when refined, results in a base or precious metal.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'In column 800, list all minerals for which pre-production mining expenditures have taken place in the tax year.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'For each of the minerals reported in column 800, identify each project (in column 805), mineral title (in column 806), and mining division (in column 807) where title is registered. If there is no mineral title, identify only the project and mining division.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1350'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1340'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Pre-production mining expenditures* ',
            'labelClass': 'fullLength bold center'
          },
          {
            'label': 'Exploration:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Pre-production mining expenditures that you incurred in the tax year (<b>before</b> January 1,' +
            ' 2014) for the purpose of determining the existence, location, extent, or quality of a mineral' +
            ' resource in Canada:',
            labelClass: 'fullLength'
          },
          {
            'label': 'Prospecting',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '810',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '810'
                }
              }
            ]
          },
          {
            'label': 'Geological, geophysical, or geochemical surveys',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '811',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '811'
                }
              }
            ]
          },
          {
            'label': 'Drilling by rotary, diamond, percussion, or other methods',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '812',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '812'
                }
              }
            ]
          },
          {
            'label': 'Trenching, digging test pits, and preliminary sampling',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '813',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '813'
                }
              }
            ]
          },
          {
            'label': 'Development:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Pre-production mining expenditures incurred in the tax year for bringing a new mine in a mineral resource in Canada into production in reasonable commercial quantities and incurred before the new mine comes into production in such quantities:'
          },
          {
            'label': 'Clearing, removing overburden, and stripping',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '820',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '820'
                }
              }
            ]
          },
          {
            'label': 'Sinking a mine shaft, constructing an adit, or other underground entry',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '821',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '821'
                }
              }
            ]
          },
          {
            'label': 'Other pre-production mining expenditures incurred in the tax year:',
            'labelClass': 'fullLength tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1400'
          },
          {
            'label': 'Total of column 826',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '827'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '828'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'A4'
          },
          {
            'label': 'Total pre-production mining expenditures (total of lines 810 to 821 and amount A4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '830',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '830'
                }
              }
            ]
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Total of all assistance (grants, subsidies, rebates, and forgivable loans) or reimbursements that the corporation has received or is entitled to receive in respect of the amounts referred to on line 830 above',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '832',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '832'
                }
              }
            ]
          },
          {
            'label': 'Excess (line 830 <b>minus</b> line 832) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '833'
                }
              }
            ],
            'indicator': 'B4'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Repayments of government and non-government assistance',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '835',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '835'
                }
              }
            ]
          },
          {
            'label': '<b>Pre-production mining expenditures</b> (amount B4 <b>plus</b> line 835)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '836'
                }
              }
            ],
            'indicator': 'C4'
          },
          {
            'label': '* A pre-production mining expenditure is defined under subsection 127(9).'
          }
        ]
      },
      {
        'header': 'Part 19 - Current-year credit and account balances - ITC from pre-production mining expenditures',
        'spacing': 'R_mult_tn2',
        'rows': [
          {
            'label': 'ITC at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '839'
                }
              }
            ],
            'indicator': 'D4'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit deemed as a remittance of co-op corporations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '841',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '841'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit expired',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '845',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '845'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 841 <b>plus</b> line 845)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '846'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '847'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'E4'
          },
          {
            'label': 'ITC at the beginning of the tax year (amount D4 <b>minus</b> amount E4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '850',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '850'
                }
              }
            ]
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit transferred on amalgamation or wind-up of subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '860',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '860'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': 'di_table_9'
          },
          {
            'type': 'table',
            'num': 'di_table_10'
          },
          {
            'type': 'table',
            'num': 'di_table_11'
          },
          {
            'type': 'table',
            'num': 'di_table_12'
          },
          {
            'label': 'Current year credit (total of amounts a to d)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '880',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '880'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '881',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'F4'
          },
          {
            'label': 'Total credit available (total of lines 850, 860, and amount F4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '882'
                }
              }
            ],
            'indicator': 'G4'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit deducted from Part I tax (enter this amount at line F8 in Part 30)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '885',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '885'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit carried back to the previous year(s) (from amount I4 in Part 20)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '886'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 885 <b>plus</b> amount e)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '887'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '888'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'H4'
          },
          {
            'label': '<b>ITC closing balance from pre-production mining expenditures</b> (amount G4 <b>minus</b> amount H4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '890',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '890'
                }
              }
            ]
          },
          {
            'label': '* Also include pre-production mining development expenditures incurred before 2014 and pre-production mining development expenditures incurred after 2013 and before 2016 that are eligible for transitional relief.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** Also include pre-production mining development expenditures incurred in 2015 if the expense ' +
            'is described in subparagraph (a)(ii) of the definition <b>pre-production mining expenditure</b> in subsection ' +
            '127(9) of the Act because of paragraph (g.4) of the definition <b>Canadian exploration expense</b> in ' +
            'subsection 66.1(6) of the Act.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 20 - Request for carryback of credit from pre-production mining expenditures',
        'rows': [
          {
            'type': 'table',
            'num': '1450'
          }
        ]
      },
      {
        'hideFieldset': true,
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Apprenticeship Job Creation',
            'labelClass': 'fullLength titleFont center'
          }
        ]
      },
      {
        'header': 'Part 21 - Total current-year credit - ITC from apprenticeship job creation expenditures',
        'rows': [
          {
            label: 'If you are a related person as defined under subsection 251(2), has it been agreed in writing that' +
            ' you are the only employer who will be claiming the apprenticeship job creation tax credit ' +
            'for this tax year for each apprentice whose contract number (or social insurance number (SIN) or name)' +
            ' appears below? (If not, you cannot claim the tax credit.) ',
            'type': 'infoField',
            'inputType': 'radio',
            'labelWidth': '84%',
            'num': '611',
            'tn': '611',
            'canClear': true
          },
          {
            labelClass: 'fullLength'
          },
          {
            label: 'For each apprentice in their first 24 months of the apprenticeship, enter the apprenticeship ' +
            'contract number registered with Canada, or a province or territory, under an apprenticeship program' +
            ' designed to certify or license individuals in the trade. For the province, the trade must be a ' +
            'Red Seal trade. If there is no contract number, enter the SIN or the name of the eligible apprentice.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1500'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Other than qualified expenditure incurred, and net of any other government or non-government assistance received or to be received.' +
            ' <b>Eligible salary and wages, and qualified expenditures</b> are defined under subsection 127(9).',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 22 - Current-year credit and account balances - ITC from apprenticeship job creation expenditures',
        'rows': [
          {
            'label': 'ITC at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '613'
                }
              }
            ],
            'indicator': 'B5'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit deemed as a remittance of co-op corporations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '612',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '612'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit expired after 20 tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '615',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '615'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 612 <b>plus</b> line 615)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '616'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '617'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'C5'
          },
          {
            'label': 'ITC at the beginning of the tax year (amount B5 <b>minus</b> amount C5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '625',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '625'
                }
              }
            ]
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit transferred on amalgamation or wind-up of subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '630',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '630'
                }
              },
              null
            ]
          },
          {
            'label': 'ITC from repayment of assistance',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '635',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '635'
                }
              },
              null
            ]
          },
          {
            'label': 'Total current-year credit (from amount A5 in Part 21)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '640',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '640'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit allocated from a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '655',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '655'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of lines 630 to 655)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '656'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '657'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D5'
          },
          {
            'label': 'Total credit available (line 625 <b>plus</b> amount D5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '658'
                }
              }
            ],
            'indicator': 'E5'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit deducted from Part I tax (enter this amount at line G8 in Part 30)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '660',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '660'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit carried back to the previous year(s) (from amount G5 in Part 23)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '661'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 660 <b>plus</b> amount a)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '662'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '663'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'F5'
          },
          {
            'label': '<b>ITC closing balance from apprenticeship job creation expenditures</b> (amount E5 <b>minus</b> amount F5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '690',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '690'
                }
              }
            ]
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Part 23 - Request for carryback of credit from apprenticeship job creation expenditures',
        'rows': [
          {
            'type': 'table',
            'num': '1550'
          }
        ]
      },
      {
        'hideFieldset': true,
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Child Care Spaces',
            'labelClass': 'fullLength titleFont center'
          }
        ]
      },
      {
        'header': 'Part 24 - Eligible child care spaces expenditures',
        'rows': [
          {
            label: 'Enter the eligible expenditures that you incurred to create licensed child care spaces for the ' +
            'children of the employees and, potentially, for other children. You cannot be carrying on a child care ' +
            'services business. The eligible expenditures include:',
            'labelClass': 'fullLength'
          },
          {
            'label': '• the cost of depreciable property (other than specified property); and',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• the specified child care start-up expenditures;',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'Properties should be acquired and expenditures should be incurred only to create new child care' +
            ' spaces at a licensed child care facility.',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            'type': 'table',
            'num': '1664'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Specified child care start-up expenditures from the current tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '705',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '705'
                }
              }
            ]
          },
          {
            'label': 'Total gross eligible expenditures for child care spaces (line 715 <b>plus</b> line 705)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '706'
                }
              }
            ],
            'indicator': 'A6'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Total of all assistance (including grants, subsidies, rebates, and forgivable loans) or reimbursements that the corporation has received or is entitled to receive in respect of the amounts referred to in amount A6',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '725',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '725'
                }
              }
            ]
          },
          {
            'label': 'Excess (amount A6 <b>minus</b> line 725) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '726'
                }
              }
            ],
            'indicator': 'B6'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Repayments by the corporation of government and non-government assistance',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '735',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '735'
                }
              }
            ]
          },
          {
            'label': '<b>Total eligible expenditures for child care spaces</b> (amount B6 <b>plus</b> line 735)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '745',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '745'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 25 - Current-year credit - ITC from child care spaces expenditures',
        'spacing': 'R_mult_tn2',
        'rows': [
          {
            'label': 'The credit is equal to 25% of eligible child care spaces expenditures incurred to a maximum of $10,000 per child care space created in a licensed child care facility.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': 'di_table_13'
          },
          {
            'type': 'table',
            'num': 'di_table_14'
          },
          {
            'label': '<b>ITC from child care spaces expenditures</b> (amount C6 or D6, whichever is less)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '758'
                }
              }
            ],
            'indicator': 'E6'
          }
        ]
      },
      {
        'header': 'Part 26 - Current-year credit and account balances - ITC from child care spaces expenditures',
        'rows': [
          {
            'label': 'ITC at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1764'
                }
              }
            ],
            'indicator': 'F6'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit deemed as a remittance of co-op corporations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '765',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '765'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit expired after 20 tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '770',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '770'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 765 <b>plus</b> line 770)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '771'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '772'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'G6'
          },
          {
            'label': 'ITC at the beginning of the tax year (amount F6 <b>minus</b> amount G6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '775',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '775'
                }
              }
            ]
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit transferred on amalgamation or wind-up of subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '777',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '777'
                }
              },
              null
            ]
          },
          {
            'label': 'Total current-year credit (from amount E6 in Part 25)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '780',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '780'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit allocated from a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '782',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '782'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of lines 777 to 782)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '783'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '784'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'H6'
          },
          {
            'label': 'Total credit available (line 775 <b>plus</b> amount H6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '786'
                }
              }
            ],
            'indicator': 'I6'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit deducted from Part I tax (enter this amount at line H8 in Part 30) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '785',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '785'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit carried back to the previous year(s) (from amount K6 in Part 27)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '787'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 785 <b>plus</b> amount a)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '788'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '789'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'J6'
          },
          {
            'label': '<b>ITC closing balance from child care spaces expenditures</b> (amount I6 <b>minus</b> amount J6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '790',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '790'
                }
              }
            ]
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Part 27 - Request for carryback of credit from child care space expenditures',
        'rows': [
          {
            'type': 'table',
            'num': '1600'
          }
        ]
      },
      {
        'hideFieldset': true,
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Recapture - Child Care Spaces',
            'labelClass': 'fullLength titleFont center'
          }
        ]
      },
      {
        'header': 'Part 28 -  Recapture of ITC for corporations and partnerships – Child care spaces',
        'rows': [
          {
            'label': 'The ITC will be recovered against the taxpayer\'s tax otherwise payable under Part I of the Act if, at any time within 60 months of the day on which the taxpayer acquired the property:',
            'labelClass': 'fullLength'
          },
          {
            'label': '• the new child care space is no longer available; or',
            'labelClass': 'fullLength'
          },
          {
            'label': '• property that was an eligible expenditure for the child care space is:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- disposed of or leased to a lessee; or',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '- converted to another use.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'If the property disposed of is a child care space, the amount that can reasonably be considered to have been included in the original ITC (paragraph 127(27.12)(a))',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '792',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '792'
                }
              }
            ]
          },
          {
            'label': 'In the case of eligible expenditures (paragraph 127(27.12)(b)), the lesser of:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'The amount that can reasonably be considered to have been included in the original ITC',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '795',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '795'
                }
              },
              null
            ]
          },
          {
            'label': '25% of either the proceeds of disposition (if sold in an arm\'s length transaction) or the fair market value (in any other case) of the property',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '797',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '797'
                }
              },
              null
            ]
          },
          {
            'label': 'Amount from line 795 or line 797, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '798'
                }
              }
            ],
            'indicator': 'A7'
          },
          {
            'type': 'infoBox',
            'width': '80%',
            'header': 'Partnerships',
            'info': 'As a member of the partnership, you will report your share of the child care spaces ITC of the partnership after the child care spaces ITC has been reduced by the amount of the recapture. If this amount is a positive amount, you will report it on line 782 in  Part 26 on page 15. However, if the partnership does not have enough ITC otherwise available to offset the recapture, then the amount by which reductions to ITC exceed additions (the excess) will be determined and reported on line 799 below.'
          },
          {
            'label': 'Corporate partner\'s share of the excess of ITC',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '799',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '799'
                }
              }
            ]
          },
          {
            'label': '<b>Total recapture of child care spaces investment tax credit</b> (total of line 792, amount A7, and line 799)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1001'
                }
              }
            ],
            'indicator': 'B7'
          },
          {
            'label': 'Enter amount B7 on line B8 in Part 29.'
          }
        ]
      },
      {
        'hideFieldset': true,
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Summary of Investment Tax Credits',
            'labelClass': 'fullLength titleFont center'
          }
        ]
      },
      {
        'header': 'Part 29 - Total recapture of investment tax credit',
        'rows': [
          {
            'label': 'Recaptured SR&ED ITC (from amount F3 in Part 17)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1002'
                }
              }
            ],
            'indicator': 'A8'
          },
          {
            'label': 'Recaptured child care spaces ITC (from amount B7 in Part 28)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1003'
                }
              }
            ],
            'indicator': 'B8'
          },
          {
            'label': '<b>Total recapture of investment tax credit</b> (amount A8 <b>plus</b> amount B8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1004'
                }
              }
            ],
            'indicator': 'C8'
          },
          {
            'label': 'Enter amount C8 on line 602 of the T2 return.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 30 - Total ITC deducted from Part I tax',
        'rows': [
          {
            'label': 'ITC from investments in qualified property deducted from Part I tax (from line 260 in Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1005'
                }
              }
            ],
            'indicator': 'D8'
          },
          {
            'label': 'ITC from SR&ED expenditures deducted from Part I tax (from line 560 in Part 12)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1006'
                }
              }
            ],
            'indicator': 'E8'
          },
          {
            'label': 'ITC from pre-production mining expenditures deducted from Part I tax (from line 885 in Part 19)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1007'
                }
              }
            ],
            'indicator': 'F8'
          },
          {
            'label': 'ITC from apprenticeship job creation expenditures deducted from Part I tax (from line 660 in Part 22)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1008'
                }
              }
            ],
            'indicator': 'G8'
          },
          {
            'label': 'ITC from child care space expenditures deducted from Part I tax (from line 785 in Part 26)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1009'
                }
              }
            ],
            'indicator': 'H8'
          },
          {
            'label': '<b>Total ITC deducted from Part I tax</b> (total of amounts D8 to H8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1010'
                }
              }
            ],
            'indicator': 'I8'
          },
          {
            'label': 'Enter amount I8 on line 652 of the T2 return.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  };
})();
