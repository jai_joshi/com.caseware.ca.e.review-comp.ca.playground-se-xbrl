(function() {

  function getCarryBackCols(totalNum, totalMessage, totalIndicator) {
    return [
      {
        colClass: 'std-input-col-padding-width',
        "type": "none",
        cellClass: 'alignCenter'
      },
      {
        colClass: 'std-input-width',
        "type": "date",
        "disabled": true
      },
      {
        "type": "none"
      },
      {
        colClass: 'std-input-col-width',
        "type": "none",
        cellClass: 'alignRight'
      },
      {
        colClass: 'std-padding-width',
        "type": "none",
        cellClass: 'alignCenter'
      },
      {
        colClass: 'std-input-width',
        "formField": true,
        "total": true,
        "totalNum": totalNum,
        "totalMessage": totalMessage,
        "totalIndicator": totalIndicator
      },
      {
        "type": "none",
        colClass: 'std-padding-width'
      }
    ]
  }

  wpw.tax.global.tableCalculations.t2s31 = {
    'di_table_1': {
      'num': 'di_table_1',
      'type': 'table',
      'columns': [
        {
          'trailingDots': true,
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Qualified property; and qualified resource property acquired after March 28, 2012, and before January 1, 2014*<br>(applicable part from amount A1 in Part 4)'
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '236',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '237',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '240'
          },
          '7': {
            'num': '240',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_2': {
      'num': 'di_table_2',
      'type': 'table',
      'columns': [
        {
          'trailingDots': true,
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Qualified resource property acquired after December 31, 2013, and before January 1, 2016<br>(applicable part from amount A1 in Part 4)'
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '241',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '238',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '242'
          },
          '7': {
            'num': '242',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_3': {
      'num': 'di_table_3',
      'type': 'table',
      'columns': [
        {
          'trailingDots': true,
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Taxable income for the previous tax year (from line 390 in Part 9) or $500,000, whichever is more'
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '402'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '1403',
            'init': '10'
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '404',
            'cellClass': ' singleUnderline'
          },
          '8': {
            'label': 'A2'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_4': {
      'num': 'di_table_4',
      'type': 'table',
      'columns': [
        {
          'trailingDots': true,
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Current expenditures (from line 350 in Part 8) or the expenditure limit (from line 410 in Part 10), whichever is less*'
          },
          '1': {
            'tn': '420'
          },
          '2': {
            'num': '420',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '421',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '422',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'G2'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_5': {
      'num': 'di_table_5',
      'type': 'table',
      'columns': [
        {
          'trailingDots': true,
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Capital expenditures (from line 360 in Part 8) or amount b above, whichever is less*'
          },
          '1': {
            'tn': '440'
          },
          '2': {
            'num': '440',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '441',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '442',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'I2'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_6': {
      'num': 'di_table_6',
      'type': 'table',
      'columns': [
        {
          'trailingDots': true,
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'The ITC on the repayment (the credit) is calculated using the ITC rate that you used to determine your ITC'
          },
          '1': {
            'tn': '460'
          },
          '2': {
            'num': '460',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '461',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '462',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'g'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_7': {
      'num': 'di_table_7',
      'type': 'table',
      'columns': [
        {
          'trailingDots': true,
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'when your qualified expenditures for ITC purposes were reduced because of the government or non'
          },
          '1': {
            'tn': '480'
          },
          '2': {
            'num': '480',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '481',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '482',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'h'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_8': {
      'num': 'di_table_8',
      'type': 'table',
      'columns': [
        {
          'trailingDots': true,
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'nongovernment assistance, or contract payments. Enter the amount of the repayment on the line that corresponds to the appropriate rate. ***'
          },
          '1': {
            'tn': '490'
          },
          '2': {
            'num': '490',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '491',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '492',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' singleUnderline'
          },
          '8': {
            'label': 'i'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_9': {
      'num': 'di_table_9',
      'type': 'table',
      'columns': [
        {
          'trailingDots': true,
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Pre-production mining expenditures* incurred before January 1, 2013 (applicable part from amount C4 in Part 18)'
          },
          '1': {
            'tn': '870'
          },
          '2': {
            'num': '870',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '861',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '871',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'a'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_10': {
      'num': 'di_table_10',
      'type': 'table',
      'columns': [
        {
          'trailingDots': true,
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Pre-production mining exploration expenditures** incurred in 2013 (applicable part from amount C4 in Part 18)'
          },
          '1': {
            'tn': '872'
          },
          '2': {
            'num': '872',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '862',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '873',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'b'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_11': {
      'num': 'di_table_11',
      'type': 'table',
      'columns': [
        {
          'trailingDots': true,
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Pre-production mining development expenditures incurred in 2014 (applicable part from amount C4 in Part 18)'
          },
          '1': {
            'tn': '874'
          },
          '2': {
            'num': '874',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '863',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '875',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'c'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_12': {
      'num': 'di_table_12',
      'type': 'table',
      'columns': [
        {
          'trailingDots': true,
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Pre-production mining development expenditures incurred in 2015 (applicable part from amount C4 in Part 18)'
          },
          '1': {
            'tn': '876'
          },
          '2': {
            'num': '876',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '864',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '877',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' singleUnderline'
          },
          '8': {
            'label': 'd'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_13': {
      'num': 'di_table_13',
      'type': 'table',
      'columns': [
        {
          'trailingDots': true,
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Eligible expenditures (from line 745 in Part 24)'
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '752'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '753',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '754',
            'cellClass': ' doubleUnderline'
          },
          '8': {
            'label': 'C6'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_14': {
      'num': 'di_table_14',
      'type': 'table',
      'columns': [
        {
          'trailingDots': true,
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Number of child care spaces'
          },
          '1': {
            'tn': '755'
          },
          '2': {
            'num': '755',
            'validate': {
              'or': [
                {
                  'matches': '^-?[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '756',
            'init': undefined
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '757',
            'validate': {
              'or': [
                {
                  'matches': '^-?[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' doubleUnderline'
          },
          '8': {
            'label': 'D6'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    "411": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "width": "115px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          "width": "30px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "200px",
          "type": "none",
          'textAlign': 'center'
        },
        {
          "width": "10px",
          "type": "none"
        },
        {
          "width": "100px",
          "formField": true
        },
        {
          "width": "20px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        }],
      "cells": [
        {
          "0": {
            "label": "Amount D2 or E2"
          },
          "1": {
            "num": "412",
            disabled: true
          },
          "2": {
            "label": " X ",
            "labelClass": "center"
          },
          "3": {
            "label": "Number of days in the tax year",
            labelClass: 'center',
            cellClass: 'singleUnderline'
          },
          "5": {
            "num": "413",
            cellClass: 'singleUnderline',
            "disabled": true
          },
          "6": {
            "label": " = "
          },
          "8": {
            "num": "414",
            "disabled": true
          },
          "9": {
            "label": "F2"
          }
        },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "365",
            "labelClass": "center"
          },
          "5": {
            "num": "415",
            "disabled": true
          },
          "8": {
            "type": "none"
          }
        }]
    },
    "699": {
      "infoTable": false,
      hasTotals: true,
      "showNumbering": true,
      "maxLoop": 100000,
      "superHeaders": [
        {
          "header": "Calculation 1 - If you meet all of the above conditions",
          "colspan": "3"
        }],
      "columns": [
        {
          "header": "Amount of ITC you originally calculated for the property you acquired, or the original user's ITC where you acquired the property from a non-arm's length party, as described in the note above",
          cellClass: 'alignCenter', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          "tn": "700"
        },
        {
          "header": "Amount calculated using ITC rate at the date of acquisition (or the original user's date of acquisition) on either the proceeds of disposition (if sold in an arm's length transaction) or the fair market value of the property (in any other case)",
          cellClass: 'alignCenter', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          "tn": "710"
        },
        {
          "header": "Amount from column 700 or 710, whichever is less",
          cellClass: 'alignCenter', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          "total": true,
          "totalNum": "711",
          "totalMessage": "Subtotal (enter amount A3 on line C3 in Part 17)",
          "totalIndicator": "A3",
          "disabled": true
        }]
    },
    "719": {
      "infoTable": false,
      hasTotals: true,
      "showNumbering": true,
      "maxLoop": 100000,
      "superHeaders": [
        {
          "header": "Calculation 2 - Only if you transferred all or a part of the qualified expenditure to another person under an agreement described in subsection 127(13); otherwise, enter nil on line B3.",
          "colspan": "6"
        }
      ],
      "columns": [
        {
          "header": "<b>A</b><br>Rate that the transferee used in determining its ITC for qualified expenditures under a subsection 127(13) agreement",
          cellClass: 'alignCenter',
          "tn": "720", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          "filters": "decimals 2",
          decimals: 3,
          colClass: 'std-small-input'
        },
        {
          "header": "<b>B</b><br>Proceeds of disposition of the property if you dispose of it to an arm's length person; or, in any other case, enter the fair market value of the property at conversion or disposition",
          cellClass: 'alignCenter',
          "tn": "730", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
        },
        {
          "header": "<b>C</b><br>Amount, if any, already provided for in Calculation 1 (This allows for the situation where only part of the cost of a property is transferred under a subsection 127(13) agreement.)",
          cellClass: 'alignCenter',
          "tn": "740", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
        },
        {
          "header": "<b>D</b><br>Amount determined by the formula<br>(A x B) – C",
          cellClass: 'alignCenter',
          "disabled": true
        },
        {
          "header": "<b>E</b><br>ITC earned by the transferee for the qualified expenditures that were transferred",
          cellClass: 'alignCenter',
          "tn": "750", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
        },
        {
          "header": "<b>F</b><br>Amount from column D or E,<br>whichever is less",
          cellClass: 'alignCenter',
          "total": true,
          "totalNum": "751",
          "totalMessage": "Subtotal (total of column F)<br>(enter amount B3 on line D3 in Part 17)",
          "totalIndicator": "B3",
          "disabled": true
        }
      ]
    },
    "1200": {
      "fixedRows": true,
      hasTotals: true,
      "infoTable": true,
      "columns": getCarryBackCols('904', 'Total of lines 901 to 903<br> (enter amount H1 on line a in Part 5)', 'H1'),
      "cells": [
        {
          "0": {
            "label": "1st preceding taxation year"
          },
          "1": {
            "num": "1201"
          },
          "3": {
            "label": " Credit to be applied ",
            "labelClass": "center"
          },
          "4": {
            "tn": "901"
          },
          "5": {
            "num": "901", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          "0": {
            "label": "2nd preceding taxation year"
          },
          "1": {
            "num": "1202"
          },
          "3": {
            "label": " Credit to be applied ",
            "labelClass": "center"
          },
          "4": {
            "tn": "902"
          },
          "5": {
            "num": "902", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          "0": {
            "label": "3rd preceding taxation year"
          },
          "1": {
            "num": "1203"
          },
          "3": {
            "label": " Credit to be applied ",
            "labelClass": "center"
          },
          "4": {
            "tn": "903"
          },
          "5": {
            "num": "903", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }]
    },
    "1300": {
      "fixedRows": true,
      hasTotals: true,
      "infoTable": true,
      "columns": getCarryBackCols('914', 'Total of lines 911 to 913<br>(enter amount S2 at line j in Part 12)', 'S2'),
      "cells": [
        {
          "0": {
            "label": "1st preceding taxation year"
          },
          "1": {
            "num": "1301"
          },
          "3": {
            "label": " Credit to be applied ",
            "labelClass": "center"
          },
          "4": {
            "tn": "911"
          },
          "5": {
            "num": "911", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          "0": {
            "label": "2nd preceding taxation year"
          },
          "1": {
            "num": "1302"
          },
          "3": {
            "label": " Credit to be applied ",
            "labelClass": "center"
          },
          "4": {
            "tn": "912"
          },
          "5": {
            "num": "912", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          "0": {
            "label": "3rd preceding taxation year"
          },
          "1": {
            "num": "1303"
          },
          "3": {
            "label": " Credit to be applied ",
            "labelClass": "center"
          },
          "4": {
            "tn": "913"
          },
          "5": {
            "num": "913", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }]
    },
    "1340": {
      hasTotals: true,
      "showNumbering": true,
      "maxLoop": 100000,
      "columns": [
        {
          "header": "Mineral title",
          cellClass: 'alignLeft',
          "num": "806", "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
          "tn": "806",
          type: 'text'
        },
        {
          "header": "Mining division",
          cellClass: 'alignLeft',
          "num": "807", "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
          "tn": "807",
          type: 'text'
        }
      ],
      "startTable": "1350"
    },
    "1350": {
      "showNumbering": true,
      hasTotals: true,
      "maxLoop": 100000,
      "repeats": ["1340"],
      "columns": [
        {
          "header": "List of minerals",
          cellClass: 'alignLeft',
          "num": "800", "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
          "tn": "800",
          type: 'text'
        },
        {
          "header": "Project name",
          cellClass: 'alignLeft',
          "num": "805", "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
          "tn": "805",
          type: 'text'
        }
      ]
    },
    "1400": {
      "infoTable": false,
      "maxLoop": 100000,
      hasTotals: true,
      "showNumbering": true,
      paddingRight: 'std-input-col-padding-width',
      "columns": [
        {
          "header": "Description",
          "tn": "825", "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
          type: 'text'
        },
        {
          "header": "Amount",
          "tn": "826", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          "hiddenTotal": true,
          "total": true,
          "totalNum": "826",
          colClass: 'std-input-width'
        }]
    },
    "1450": {
      "fixedRows": true,
      hasTotals: true,
      "infoTable": true,
      "columns": getCarryBackCols('924', 'Total of lines 921 to 923<br>(enter amount I4 on line e in Part 19)', 'I4'),
      "cells": [
        {
          "0": {
            "label": "1st preceding taxation year"
          },
          "1": {
            "num": "1921"
          },
          "3": {
            "label": " Credit to be applied ",
            "labelClass": "center"
          },
          "4": {
            "tn": "921"
          },
          "5": {
            "num": "921", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          "0": {
            "label": "2nd preceding taxation year"
          },
          "1": {
            "num": "1922"
          },
          "3": {
            "label": " Credit to be applied ",
            "labelClass": "center"
          },
          "4": {
            "tn": "922"
          },
          "5": {
            "num": "922", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          "0": {
            "label": "3rd preceding taxation year"
          },
          "1": {
            "num": "1923"
          },
          "3": {
            "label": " Credit to be applied ",
            "labelClass": "center"
          },
          "4": {
            "tn": "923"
          },
          "5": {
            "num": "923", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }]
    },
    "1500": {
      "infoTable": false,
      "maxLoop": 100000,
      hasTotals: true,
      "showNumbering": true,
      "columns": [
        {
          "header": "<b>A</b><br>Contract number<br>(SIN or name of apprentice)",
          cellClass: 'alignCenter',
          "tn": "601", "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
          type: 'text'
        },
        {
          "header": "<b>B</b><br>Name of eligible trade",
          cellClass: 'alignCenter',
          "tn": "602", "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
          type: 'text'
        },
        {
          "header": "<b>C</b><br>Eligible salary and wages*",
          cellClass: 'alignCenter',
          "tn": "603", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          "header": "<b>D</b><br>Column C x 10%",
          cellClass: 'alignCenter',
          "tn": "604", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          "disabled": true
        },
        {
          "header": "<b>E</b><br>Lesser of column D or $2000",
          cellClass: 'alignCenter',
          "tn": "605", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          "total": true,
          "totalMessage": "Total current-year credit (total of column E)<br>(enter amount A5 on line 640 in Part 22)",
          "totalIndicator": "A5",
          "disabled": true
        }]
    },
    "1550": {
      "fixedRows": true,
      hasTotals: true,
      "infoTable": true,
      "columns": getCarryBackCols('934', 'Total of lines 931 to 933<br>(enter amount G5 on line a in Part 22)', 'G5'),
      "cells": [
        {
          "0": {
            "label": "1st preceding taxation year"
          },
          "1": {
            "num": "1551"
          },
          "3": {
            "label": " Credit to be applied ",
            "labelClass": "center"
          },
          "4": {
            "tn": "931"
          },
          "5": {
            "num": "931", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          "0": {
            "label": "2nd preceding taxation year"
          },
          "1": {
            "num": "1552"
          },
          "3": {
            "label": " Credit to be applied ",
            "labelClass": "center"
          },
          "4": {
            "tn": "932"
          },
          "5": {
            "num": "932", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          "0": {
            "label": "3rd preceding taxation year"
          },
          "1": {
            "num": "1553"
          },
          "3": {
            "label": " Credit to be applied ",
            "labelClass": "center"
          },
          "4": {
            "tn": "933"
          },
          "5": {
            "num": "933", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }]
    },
    "1600": {
      "fixedRows": true,
      "infoTable": true,
      hasTotals: true,
      "columns": getCarryBackCols('944', 'Total of lines 941 to 943<br>(enter amount K6 on line a in Part 26)', 'K6'),
      "cells": [
        {
          "0": {
            "label": "1st preceding taxation year"
          },
          "1": {
            "num": "1601"
          },
          "3": {
            "label": " Credit to be applied ",
            "labelClass": "center"
          },
          "4": {
            "tn": "941"
          },
          "5": {
            "num": "941", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          "0": {
            "label": "2nd preceding taxation year"
          },
          "1": {
            "num": "1602"
          },
          "3": {
            "label": " Credit to be applied ",
            "labelClass": "center"
          },
          "4": {
            "tn": "942"
          },
          "5": {
            "num": "942", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          "0": {
            "label": "3rd preceding taxation year"
          },
          "1": {
            "num": "1603"
          },
          "3": {
            "label": " Credit to be applied ",
            "labelClass": "center"
          },
          "4": {
            "tn": "943"
          },
          "5": {
            "num": "943", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }]
    },
    "1664": {
      "infoTable": false,
      "showNumbering": true,
      hasTotals: true,
      "maxLoop": 100000,
      "superHeaders": [
        {
          "header": "Cost of depreciable property from the current tax year",
          "colspan": "4"
        }],
      "columns": [
        {
          "header": "Capital cost allowance class number",
          "tn": "665",
          cellClass: 'alignCenter',
          "type": "dropdown",
          "options": [{
            "option": "Class 01 : (4%) Building",
            "value": "1"
          },
            {
              "option": "Class 02 : (6%) Electrical generating equipment, manufacturing and distributing equip. plan, acquired before 1988",
              "value": "2"
            },
            {
              "option": "Class 03 : (5%) Building before 1988, alterations after 1987",
              "value": "3"
            },
            {
              "option": "Class 04 : (6%) Railway system, tramway or trolley",
              "value": "4"
            },
            {
              "option": "Class 05 : (10%) Property that is chemical pulp mill, or ground mill pulp. integrated mill, loading and unloading wharves, railway tracks acquired before 1962",
              "value": "5"
            },
            {
              "option": "Class 06 : (10%) Building made of frame, log, stucco, greenhouses",
              "value": "6"
            },
            {
              "option": "Class 07 : (15%) Property that is a canoe, scow, vessel, marine railway, rail suspension device, railway car, pumping or compression equipment",
              "value": "7"
            },
            {
              "option": "Class 08 : (20%) Certain property, furniture, appliances, tools costing $500 or more, photocopiers, electronic, communications equipment",
              "value": "8"
            },
            {
              "option": "Class 09 : (25%) Property acquired before may 26, 1976 that is an electrical generating equipment, radar equipment, radio transmission/receiving, portable electrical generating equipment",
              "value": "9"
            },
            {
              "option": "Class 10 : (30%) Computer hardware, system software, motor vehicles",
              "value": "10"
            },
            {
              "option": "Class 11 : (100%) Property that is an electrical advertising sign, poster panel, bulletin board",
              "value": "11"
            },
            {
              "option": "Class 13 : (straight-line) Leasehold interest, interest in minerals, petroleum",
              "value": "13"
            },
            {
              "option": "Class 14 : (straight-line) Patents, franchises, concessions, or licences for a limited period",
              "value": "14"
            },
            {
              "option": "Class 15 : (straight-line) Property would otherwise be included in another class. timber resource property",
              "value": "15"
            },
            {
              "option": "Class 16 : (40%) Property acquired before May 26, 1976 aircraft, furniture/fittings related to aircraft, spare part, taxicab - motor vehicle for purpose of leasing or renting, coin op'd video game, truck or tractor for freight",
              "value": "16"
            },
            {
              "option": "Class 17 : (8%) Property such as telephone systems, telegraph acquired before May 26,1976 or property after Feb 27,2000 electrical generating equipment, production and distribution equipment, telephone, telegraph or data communication switching, roads like airport runway, sidewalk, parking area",
              "value": "17"
            },
            {
              "option": "Class 18 : (60%) Property that is a motion picture film acquired before May 26,1976 besides TV commercial or a certified feature film",
              "value": "18"
            },
            {
              "option": "Class 19 : (20%) Property acquired after June 13, 1963 but before Jan 1, 1967, gas well, logging, mining, construction",
              "value": "19"
            },
            {
              "option": "Class 20 : (20%) Property that would be included, building, extension to a building, alteration of property",
              "value": "20"
            },
            {
              "option": "Class 21 : (50%) Property acquired after Dec 5, 1963 but before April 1, 1967; manufacturing and processing",
              "value": "21"
            },
            {
              "option": "Class 22 : (50%) Property acquired after Mar 516, 1964 but before 1988 or 1990; power operated movable equipment designed for purpose of excavating, moving, placing or compacting earth",
              "value": "22"
            },
            {
              "option": "Class 23 : (100%) Property that is a leasehold interest, Expo 86, building or other structure ",
              "value": "23"
            },
            {
              "option": "Class 24 : (50%) Property acquired after April 26, 1965 but before 1971; for the purpose of preventing, reducing or eliminating pollution of coastal, inland or boundary waters of Canada",
              "value": "24"
            },
            {
              "option": "Class 25 : (100%) Property that was acquired before Oct 23, 1968; or after Oct 22, 1968 and before 1974",
              "value": "25"
            },
            {
              "option": "Class 26 : (5%) Property that is a catalyst, deuterium, enriched water",
              "value": "26"
            },
            {
              "option": "Class 27 : (50%) Property acquired before 1999 for the purpose of reducing air pollution",
              "value": "27"
            },
            {
              "option": "Class 28 : (30%) Property situated in Canada acquired by taxpayer before 1988 or before 1990; mills, mines",
              "value": "28"
            },
            {
              "option": "Class 29 : (50%) Eligible machinery and equipment after March 18, 2007 and before 2016",
              "value": "29"
            },
            {
              "option": "Class 30 : (40%) Unmanned telecommunication spacecraft before 1988 or 1990",
              "value": "30"
            },
            {
              "option": "Class 31 : (5%) Property that is a multiple-unit residential building in Canada, time period after Nov.18, 1974 and before 1982",
              "value": "31"
            },
            {
              "option": "Class 32 : (10%) Property that is a multiple-unit residential building in Canada, time period after Nov.18, 1974 and before 1978",
              "value": "32"
            },
            {
              "option": "Class 33 : (15%) Property that is a timber resource property",
              "value": "33"
            },
            {
              "option": "Class 34 : (50%) Electrical generating equipment, steam generating equipment, by-product of industrial process",
              "value": "34"
            },
            {
              "option": "Class 35 : (7%) Railway car after May 25, 1976",
              "value": "35"
            },
            {
              "option": "Class 37 : (15%) Property related to amusement parks, land improvements, building, structures and equipment bridges, fences, etc...",
              "value": "37"
            },
            {
              "option": "Class 38 : (30%) Property not included in class 22",
              "value": "38"
            },
            {
              "option": "Class 39 : (25%) Property acquired after 1987 but before Feb 26,1992",
              "value": "39"
            },
            {
              "option": "Class 40 : (30%) Property acquired after 1987 and before 1990 that is a powered industrial lift truck",
              "value": "40"
            },
            {
              "option": "Class 41 : (25%) Property related to mining, vessels to find petroleum, natural gas or mineral resources, oil/gas wells",
              "value": "41"
            },
            {
              "option": "Class 42 : (12%) Fibre-optic cables, telephone or data communication equipment that is a wire or cable after Feb 22,2005",
              "value": "42"
            },
            {
              "option": "Class 43 : (30%) Mining structure, machinery or equipment acquired after Feb 25, 1992",
              "value": "43"
            },
            {
              "option": "Class 44 : (25%) Patents, right to use patented information",
              "value": "44"
            },
            {
              "option": "Class 45 : (45%) Computer hardware",
              "value": "45"
            },
            {
              "option": "Class 46 : (30%) Data equipment and system software, acquired after March 22,2004",
              "value": "46"
            },
            {
              "option": "Class 47 : (8%) Property that is transmission or distribution equipment, equipment for purpose of producing oxygen or nitrogen, breakwater, dock, jetty, building",
              "value": "47"
            },
            {
              "option": "Class 48 : (15%) Property bought after Feb 22, 2005 that is a combustion turbine, burners, compressors that generate electrical energy",
              "value": "48"
            },
            {
              "option": "Class 49 : (8%) Property that is a pipeline, control, and monitoring devices, valves and ancillary equipment",
              "value": "49"
            },
            {
              "option": "Class 50 : (55%) Computer hardware and system software acquired after March 18, 2007",
              "value": "50"
            },
            {
              "option": "Class 51 : (6%) Property acquired after Mar 18, 2007 that is a pipeline, ancillary equipment",
              "value": "51"
            },
            {
              "option": "Class 52 : (100%) Computer hardware and system software acquired after Jan 27, 2009 and before Feb, 2011",
              "value": "52"
            },
            {
              "option": "Class 1.2 : (10%) M&P Building after March 18, 2007",
              "value": "1.2"
            },
            {
              "option": "Class 1.3 : (6%) Other non-residential building after march 18, 2007",
              "value": "1.3"
            },
            {
              "option": "Class 10.1 : (30%) Passenger vehicles value > $30,000",
              "value": "10.1"
            },
            {
              "option": "Class 12.1 : (100%) Die, jig, pattern, motion pictures, video tape, etc...",
              "value": "12.1"
            },
            {
              "option": "Class 12.2 : (100%) All items that are not included in 12.1",
              "value": "12.2"
            },
            {
              "option": "Class 43.1 : (30%) Electrical generating equipment, heat recovery equipment",
              "value": "43.1"
            },
            {
              "option": "Class 43.2 : (50%) Property acquired after Feb 22,2005 and before 2020",
              "value": "43.2"
            }],
          "width": "109px"
        },
        {
          "header": "Description of investment",
          cellClass: 'alignCenter',
          "tn": "675", "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
          type: 'text'
        },
        {
          "header": "Date available for use",
          cellClass: 'alignCenter',
          "tn": "685",
          "type": "date",
          "width": "145px"
        },
        {
          "header": "Amount of investment",
          cellClass: 'alignCenter',
          "total": true,
          "tn": "695", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          "totalNum": "715",
          "totalMessage": "Total cost of depreciable property from the current tax year (total of column 695)",
          "totalTn": "715",
          colClass: 'std-input-width'
        }]
    },
    "050": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "width": "100px",
          "type": "none",
          "alignText": "left",
          "header": "<b>Investments</b>"
        },
        {
          "type": "none"
        },
        {
          "width": "150px",
          "header": "<b>Specified percentage</b>",
          "alignText": "left",
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        }]
    },
    "099": {
      "infoTable": false,
      "showNumbering": true,
      hasTotals: true,
      "maxLoop": 100000,
      "columns": [
        {
          "header": "Capital cost allowance class number",
          "tn": "105",
          num: '105',
          cellClass: 'alignCenter',
          "type": "dropdown",
          "options": [
            {
              "option": "Class 01 : (4%) Building",
              "value": "1"
            },
            {
              "option": "Class 02 : (6%) Electrical generating equipment, manufacturing and distributing equip. plan, acquired before 1988",
              "value": "2"
            },
            {
              "option": "Class 03 : (5%) Building before 1988, alterations after 1987",
              "value": "3"
            },
            {
              "option": "Class 04 : (6%) Railway system, tramway or trolley",
              "value": "4"
            },
            {
              "option": "Class 05 : (10%) Property that is chemical pulp mill, or ground mill pulp. integrated mill, loading and unloading wharves, railway tracks acquired before 1962",
              "value": "5"
            },
            {
              "option": "Class 06 : (10%) Building made of frame, log, stucco, greenhouses",
              "value": "6"
            },
            {
              "option": "Class 07 : (15%) Property that is a canoe, scow, vessel, marine railway, rail suspension device, railway car, pumping or compression equipment",
              "value": "7"
            },
            {
              "option": "Class 08 : (20%) Certain property, furniture, appliances, tools costing $500 or more, photocopiers, electronic, communications equipment",
              "value": "8"
            },
            {
              "option": "Class 09 : (25%) Property acquired before may 26, 1976 that is an electrical generating equipment, radar equipment, radio transmission/receiving, portable electrical generating equipment",
              "value": "9"
            },
            {
              "option": "Class 10 : (30%) Computer hardware, system software, motor vehicles",
              "value": "10"
            },
            {
              "option": "Class 11 : (100%) Property that is an electrical advertising sign, poster panel, bulletin board",
              "value": "11"
            },
            {
              "option": "Class 13 : (straight-line) Leasehold interest, interest in minerals, petroleum",
              "value": "13"
            },
            {
              "option": "Class 14 : (straight-line) Patents, franchises, concessions, or licences for a limited period",
              "value": "14"
            },
            {
              "option": "Class 15 : (straight-line) Property would otherwise be included in another class. timber resource property",
              "value": "15"
            },
            {
              "option": "Class 16 : (40%) Property acquired before May 26, 1976 aircraft, furniture/fittings related to aircraft, spare part, taxicab - motor vehicle for purpose of leasing or renting, coin op'd video game, truck or tractor for freight",
              "value": "16"
            },
            {
              "option": "Class 17 : (8%) Property such as telephone systems, telegraph acquired before May 26,1976 or property after Feb 27,2000 electrical generating equipment, production and distribution equipment, telephone, telegraph or data communication switching, roads like airport runway, sidewalk, parking area",
              "value": "17"
            },
            {
              "option": "Class 18 : (60%) Property that is a motion picture film acquired before May 26,1976 besides TV commercial or a certified feature film",
              "value": "18"
            },
            {
              "option": "Class 19 : (20%) Property acquired after June 13, 1963 but before Jan 1, 1967, gas well, logging, mining, construction",
              "value": "19"
            },
            {
              "option": "Class 20 : (20%) Property that would be included, building, extension to a building, alteration of property",
              "value": "20"
            },
            {
              "option": "Class 21 : (50%) Property acquired after Dec 5, 1963 but before April 1, 1967; manufacturing and processing",
              "value": "21"
            },
            {
              "option": "Class 22 : (50%) Property acquired after Mar 516, 1964 but before 1988 or 1990; power operated movable equipment designed for purpose of excavating, moving, placing or compacting earth",
              "value": "22"
            },
            {
              "option": "Class 23 : (100%) Property that is a leasehold interest, Expo 86, building or other structure ",
              "value": "23"
            },
            {
              "option": "Class 24 : (50%) Property acquired after April 26, 1965 but before 1971; for the purpose of preventing, reducing or eliminating pollution of coastal, inland or boundary waters of Canada",
              "value": "24"
            },
            {
              "option": "Class 25 : (100%) Property that was acquired before Oct 23, 1968; or after Oct 22, 1968 and before 1974",
              "value": "25"
            },
            {
              "option": "Class 26 : (5%) Property that is a catalyst, deuterium, enriched water",
              "value": "26"
            },
            {
              "option": "Class 27 : (50%) Property acquired before 1999 for the purpose of reducing air pollution",
              "value": "27"
            },
            {
              "option": "Class 28 : (30%) Property situated in Canada acquired by taxpayer before 1988 or before 1990; mills, mines",
              "value": "28"
            },
            {
              "option": "Class 29 : (50%) Eligible machinery and equipment after March 18, 2007 and before 2016",
              "value": "29"
            },
            {
              "option": "Class 30 : (40%) Unmanned telecommunication spacecraft before 1988 or 1990",
              "value": "30"
            },
            {
              "option": "Class 31 : (5%) Property that is a multiple-unit residential building in Canada, time period after Nov.18, 1974 and before 1982",
              "value": "31"
            },
            {
              "option": "Class 32 : (10%) Property that is a multiple-unit residential building in Canada, time period after Nov.18, 1974 and before 1978",
              "value": "32"
            },
            {
              "option": "Class 33 : (15%) Property that is a timber resource property",
              "value": "33"
            },
            {
              "option": "Class 34 : (50%) Electrical generating equipment, steam generating equipment, by-product of industrial process",
              "value": "34"
            },
            {
              "option": "Class 35 : (7%) Railway car after May 25, 1976",
              "value": "35"
            },
            {
              "option": "Class 37 : (15%) Property related to amusement parks, land improvements, building, structures and equipment bridges, fences, etc...",
              "value": "37"
            },
            {
              "option": "Class 38 : (30%) Property not included in class 22",
              "value": "38"
            },
            {
              "option": "Class 39 : (25%) Property acquired after 1987 but before Feb 26,1992",
              "value": "39"
            },
            {
              "option": "Class 40 : (30%) Property acquired after 1987 and before 1990 that is a powered industrial lift truck",
              "value": "40"
            },
            {
              "option": "Class 41 : (25%) Property related to mining, vessels to find petroleum, natural gas or mineral resources, oil/gas wells",
              "value": "41"
            },
            {
              "option": "Class 42 : (12%) Fibre-optic cables, telephone or data communication equipment that is a wire or cable after Feb 22,2005",
              "value": "42"
            },
            {
              "option": "Class 43 : (30%) Mining structure, machinery or equipment acquired after Feb 25, 1992",
              "value": "43"
            },
            {
              "option": "Class 44 : (25%) Patents, right to use patented information",
              "value": "44"
            },
            {
              "option": "Class 45 : (45%) Computer hardware",
              "value": "45"
            },
            {
              "option": "Class 46 : (30%) Data equipment and system software, acquired after March 22,2004",
              "value": "46"
            },
            {
              "option": "Class 47 : (8%) Property that is transmission or distribution equipment, equipment for purpose of producing oxygen or nitrogen, breakwater, dock, jetty, building",
              "value": "47"
            },
            {
              "option": "Class 48 : (15%) Property bought after Feb 22, 2005 that is a combustion turbine, burners, compressors that generate electrical energy",
              "value": "48"
            },
            {
              "option": "Class 49 : (8%) Property that is a pipeline, control, and monitoring devices, valves and ancillary equipment",
              "value": "49"
            },
            {
              "option": "Class 50 : (55%) Computer hardware and system software acquired after March 18, 2007",
              "value": "50"
            },
            {
              "option": "Class 51 : (6%) Property acquired after Mar 18, 2007 that is a pipeline, ancillary equipment",
              "value": "51"
            },
            {
              "option": "Class 52 : (100%) Computer hardware and system software acquired after Jan 27, 2009 and before Feb, 2011",
              "value": "52"
            },
            {
              "option": "Class 1.2 : (10%) M&P Building after March 18, 2007",
              "value": "1.2"
            },
            {
              "option": "Class 1.3 : (6%) Other non-residential building after march 18, 2007",
              "value": "1.3"
            },
            {
              "option": "Class 10.1 : (30%) Passenger vehicles value > $30,000",
              "value": "10.1"
            },
            {
              "option": "Class 12.1 : (100%) Die, jig, pattern, motion pictures, video tape, etc...",
              "value": "12.1"
            },
            {
              "option": "Class 12.2 : (100%) All items that are not included in 12.1",
              "value": "12.2"
            },
            {
              "option": "Class 43.1 : (30%) Electrical generating equipment, heat recovery equipment",
              "value": "43.1"
            },
            {
              "option": "Class 43.2 : (50%) Property acquired after Feb 22,2005 and before 2020",
              "value": "43.2"
            }],
          "width": "109px"
        },
        {
          "header": "Type of investment",
          cellClass: 'alignCenter',
          "type": "dropdown",
          "options": [
            {
              "option": "Code 1a : Qualified property",
              "value": "1"
            },
            {
              "option": "Code 1b : Qualified resource property acquired after March 28, 2012, and before January 01, 2014",
              "value": "2"
            },
            {
              "option": "Code 1c : Qualified resource property acquired after December 31, 2013, and before January 01, 2016",
              "value": "3"
            }
          ],
          "width": "109px"
        },
        {
          "header": "Description of investment",
          "tn": "110", "validate": {"or": [{"length": {"min": "1", "max": "30"}}, {"check": "isEmpty"}]},
          num: '110',
          type: 'text'
        },
        {
          "header": "Date available for use",
          "tn": "115",
          "width": "145px",
          "type": "date",
          num: '115'
        },
        {
          "header": "Location used in Atlantic Canada<br>(province)",
          "tn": "120",
          num: '120',
          cellClass: 'alignCenter',
          "type": "dropdown",
          "options": [
            {
              "option": "Alberta",
              "value": "AB"
            },
            {
              "option": "British Columbia",
              "value": "BC"
            },
            {
              "option": "Manitoba",
              "value": "MB"
            },
            {
              "option": "New Brunswick",
              "value": "NB"
            },
            {
              "option": "Newfoundland and Labrador",
              "value": "NL"
            },
            {
              "option": "Nunavut",
              "value": "NU"
            },
            {
              "option": "Nova Scotia",
              "value": "NS"
            },
            {
              "option": "Northwest Territories",
              "value": "NT"
            },
            {
              "option": "Ontario",
              "value": "ON"
            },
            {
              "option": "Prince Edward Island",
              "value": "PE"
            },
            {
              "option": "Quebec",
              "value": "QC"
            },
            {
              "option": "Saskatchewan",
              "value": "SK"
            },
            {
              "option": "Yukon",
              "value": "YT"
            }],
          "width": "109px"
        },
        {
          "header": "Amount of investment",
          "tn": "125", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          num: '125',
          colClass: 'std-input-width',
          "total": true,
          "totalIndicator": "A1",
          "totalMessage": "Total of investments for qualified property and qualified resource property"
        }
      ]
    },
    "434": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          colClass: 'std-input-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          "type": "none"
        },
        {
          colClass: 'std-spacing-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width',
          "formField": true
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width',
          "formField": true
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Amount from line 430"
          },
          "1": {
            "num": "1430"
          },
          "2": {
            "label": " x ",
            "labelClass": "center"
          },
          "3": {
            "label": "Number of days in the tax year before 2014",
            cellClass: 'singleUnderline',
            labelClass: 'center'
          },
          "5": {
            "num": "1431",
            cellClass: 'singleUnderline',
            "disabled": true
          },
          "6": {
            "label": " x ",
            "labelClass": "center"
          },
          "7": {
            "num": "1432"
          },
          "8": {
            "label": " %= "
          },
          "9": {
            "num": "1433"
          },
          "10": {
            "label": "b"
          }
        },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "5": {
            "num": "1434",
            "disabled": true
          },
          "7": {
            "type": "none"
          },
          "9": {
            "type": "none"
          }
        },
        {
          "0": {
            "label": "Amount from line 430**"
          },
          "1": {
            "num": "1435"
          },
          "2": {
            "label": " x ",
            "labelClass": "center"
          },
          "3": {
            "label": "Number of days in the tax year after 2013",
            cellClass: 'singleUnderline',
            labelClass: 'center'
          },
          "5": {
            "num": "1436",
            cellClass: 'singleUnderline',
            "disabled": true
          },
          "6": {
            "label": " x ",
            "labelClass": "center"
          },
          "7": {
            "num": "1437"
          },
          "8": {
            "label": " %= "
          },
          "9": {
            "num": "1438"
          },
          "10": {
            "label": "c"
          }
        },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "5": {
            "num": "1439",
            "disabled": true
          },
          "7": {
            "type": "none"
          },
          "9": {
            "type": "none"
          }
        }
      ]
    },
    "454": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          colClass: 'std-input-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          "type": "none"
        },
        {
          colClass: 'std-spacing-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width',
          "formField": true
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'small-input-width',
          "formField": true
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        },
        {
          "type": "none",
          colClass: 'std-input-col-width'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Amount from line 450"
          },
          "1": {
            "num": "1460"
          },
          "2": {
            "label": "x",
            "labelClass": "center"
          },
          "3": {
            "label": "Number of days in the tax year before 2014",
            cellClass: 'singleUnderline',
            "labelClass": "center"
          },
          "5": {
            "num": "1451",
            cellClass: 'singleUnderline',
            "disabled": true
          },
          "6": {
            "label": " x ",
            "labelClass": "center"
          },
          "7": {
            "num": "1452"
          },
          "8": {
            "label": " %= "
          },
          "9": {
            "num": "1453"
          },
          "10": {
            "label": "e"
          }
        },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "5": {
            "num": "1454",
            "disabled": true
          },
          "7": {
            "type": "none"
          },
          "9": {
            "type": "none"
          }
        },
        {
          "0": {
            "label": "Amount from line 450**"
          },
          "1": {
            "num": "1455"
          },
          "2": {
            "label": " x ",
            "labelClass": "center"
          },
          "3": {
            "label": "Number of days in the tax year after 2013",
            cellClass: 'singleUnderline',
            "labelClass": "center"
          },
          "5": {
            "num": "1456",
            cellClass: 'singleUnderline',
            "disabled": true
          },
          "6": {
            "label": " x ",
            "labelClass": "center"
          },
          "7": {
            "num": "1457"
          },
          "8": {
            "label": " %= "
          },
          "9": {
            "num": "1458"
          },
          "10": {
            "label": "f"
          }
        },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "5": {
            "num": "1459",
            "disabled": true
          },
          "7": {
            "type": "none"
          },
          "9": {
            "type": "none"
          }
        }
      ]
    }
  }
})();
