(function() {
  wpw.tax.create.formData('rc59x', {
    formInfo: {
      abbreviation: 'RC59X',
      title: 'Cancel Business Consent or Delegated Authority',
      printNum: 'RC59X',
      headerImage: 'canada-federal',
      category: 'Client Communication'
    },
    sections: [
      {
        rows: [
          {
            type: 'multiColumn',
            dividers: [false],
            columns: [
              [
                {
                  type: 'infoField',
                  inputType: 'none',
                  label: 'Representatives',
                  labelClass: 'titleFont bold'
                },
                {
                  type: 'infoField',
                  inputType: 'none',
                  label: '<b>Cancel your access faster</b> online using "Represent a Client." Go to ' +
                  '<b>cra.gc.ca/loginservices</b> ' +
                  'and log in. On the "Welcome" page, select "Review and update", then your "RepID", ' +
                  '"Group ID", or "Business number." Open the "Manage clients" tab, then select ' +
                  '"Authorization request" and follow the instructions.',
                  labelClass: 'fullLength'
                }
              ],
              [
                {
                  type: 'infoField',
                  inputType: 'none',
                  label: 'Business owners',
                  labelClass: 'titleFont bold'
                },
                {
                  type: 'infoField',
                  inputType: 'none',
                  label: '<b>Cancel your representative\'s access faster</b> online using "My Business ' +
                  'Account." Go to <b>cra.gc.ca/loginservices</b> and log in. On the "Welcome" page, ' +
                  'select "Manage", then "Representatives" and follow the instructions.',
                  labelClass: 'fullLength'
                }
              ]
            ]
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {
            label: '<b>Use this form to</b> cancel consent for an existing representative or delegated authority.',
            labelClass: 'fullLength'
          },
          {
            label: '<b>Do not use</b> this form <b>if both </b>of the following apply:',
            labelClass: 'fullLength'
          },
          {
            label: '• you are a selected listed financial institution (SLFI) for goods and ' +
            'services tax/harmonized sales tax (GST/HST) purposes, or Quebec sales tax (QST) purposes, or both; <b>and</b>',
            labelClass: 'fullLength tabbed'
          },
          {
            label: '• you have a GST/HST (RT) program account that includes QST information.',
            labelClass: 'fullLength tabbed'
          },
          {
            label: 'Instead, use Form RC7259, Business Consent for Certain Selected Listed Financial ' +
            'Institutions. For more information, including the definition of an SLFI for GST/HST <b>and</b> QST ' +
            'purposes, go to <b>cra.gc.ca/slfi.</b>',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'Part 1 - Business information',
        rows: [
          {
            label: 'Enter the business name and the business number (BN) as registered with the CRA.',
            labelClass: 'fullLength'
          },
          {
            type: 'table', num: '100'
          }
        ]
      },
      {
        header: 'Part 2 - Cancel one or more authorizations',
        rows: [
          {
            label: 'Tick only one option to cancel authorization of a representative or delegated authority or both. ' +
            'If you tick B or D, fill in the RepID, GroupID, or BN.',
            labelClass: 'fullLength'
          },
          {
            type: 'table', num: '360'
          },
          {
            type: 'table', num: '400'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table', num: '403'
          },
          {
            type: 'table', num: '405'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table', num: '410'
          }
        ]
      },
      {
        header: 'Part 3 - Certification',
        rows: [
          {
            label: 'You must sign and date this form. The CRA must receive this form within six months of the date ' +
            'it was signed or it will not be processed. This form must only be signed by an individual with proper ' +
            'authority for the business (see the choices below). An authorized representative cannot sign this ' +
            'form unless they have delegated authority. If the name of the individual signing this form does ' +
            'not exactly match CRA records, this form will not be processed. Forms that cannot be processed, ' +
            'for any reason, will be returned to the business. To avoid processing delays and before you sign ' +
            'this form, you must make sure that the CRA has complete and valid information on file for ' +
            'your business. We may contact you to confirm the information you have given.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {label: 'The individual signing this form is (tick one box only):'},
          {
            type: 'splitInputs',
            inputType: 'checkbox',
            divisions: '4',
            num: '500',
            showValues: 'false',
            items: [
              {label: 'an owner', value: '1'},
              {
                label: 'an officer of a non-profit organization',
                value: '2'
              },
              {label: 'a partner of a partnership', value: '3'},
              {label: 'a trustee', value: '4'},
              {label: 'a corporate director', value: '5'},
              {
                label: 'an individual with delegated authority',
                value: '6'
              },
              {
                label: 'a corporate officer',
                value: '7'
              }
            ]
          },
          {labelClass: 'fullLength'},
          {
            type: 'table', num: '510'
          },
          {labelClass: 'fullLength'},
          {label: 'I certify that the information given on this form is correct and complete.'},
          {
            type: 'table', num: '520'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Once completed, send this form to your tax centre. For more information, go to <b>cra.gc.ca/taxcentre.</b>',
            labelClass: 'fullLength'
          },
          {
            label: 'Our goal is to process RC59 forms within 15 business days from when we get them.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            type: 'infoField', inputType: 'template',
            content: 'mod/com.caseware.ca.tax.abstract/directives/templateInjector/rc59xadd.htm'
          }
        ]
      }
    ]
  })
})();
