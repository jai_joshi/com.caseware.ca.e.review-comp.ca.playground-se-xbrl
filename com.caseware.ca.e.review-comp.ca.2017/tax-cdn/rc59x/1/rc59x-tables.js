(function() {
  var programIdentifier = [
    {
      option: 'RC',
      value: '1'
    },
    {
      option: 'RT',
      value: '2'
    },
    {
      option: 'RP',
      value: '3'
    },
    {
      option: 'RR',
      value: '4'
    },
    {
      option: 'RM',
      value: '5'
    },
    {
      option: 'RE',
      value: '6'
    },
    {
      option: 'RN',
      value: '7'
    },
    {
      option: 'RD',
      value: '8'
    },
    {
      option: 'RG',
      value: '9'
    },
    {
      option: 'SL',
      value: '10'
    },
    {
      option: 'RZ',
      value: '11'
    }
  ];

  wpw.tax.create.tables('rc59x', {
    '100': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          cellClass: 'alignCenter',
          type: 'none'
        },
        {
          width: '65%',
          type: 'text'
        },
        {
          type: 'custom',
          format: ['{N9}RC{N4}']
        }
      ],
      cells: [
        {
          '0': {
            label: 'Business name'
          },
          '1': {
            num: '101'
          },
          '2': {
            label: 'Business number',
            num: '102'
          }
        }
      ]
    },
    '360': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none',
          width: '60px'
        },
        {
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {
            type: 'singleCheckbox',
            num: '450'
          },
          '1': {
            label: '<b>A.</b> Cancel <b>all</b> authorizations for <b>all</b> accounts.'
          }
        },
        {
          '0': {
            type: 'singleCheckbox',
            num: '451'
          },
          '1': {
            label: '<b>B.</b> Cancel <b>all</b> authorizations, only for the individual, group, or firm identified' +
            ' below.'
          }
        },
        {
          '0': {
            type: 'singleCheckbox',
            num: '452'
          },
          '1': {
            label: '<b>C.</b> Cancel <b>all</b> authorizations, only for the following program account'
          }
        }
      ]
    },
    '400': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none',
          width: '60px'
        },
        {
          type: 'none',
          width: '120px'
        },
        {
          type: 'none',
          width: '50px'
        },
        {
          type: 'none',
          width: '120px'
        },
        {
          width: '80px',
          type: 'custom',
          format: ['{N4}']
        },
        {
          type: 'none'
        }],
      cells: [
        {
          1: {label: 'Program identifier:', width: '120px'},
          2: {num: '401', type: 'dropdown', options: programIdentifier},
          3: {label: 'Reference number:'},
          4: {num: '402'}
        }
      ]
    },
    '403': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none',
          width: '60px'
        },
        {
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {
            type: 'singleCheckbox',
            num: '4031'
          },
          '1': {
            label: '<b>D.</b> Cancel authorization for the individual, group, or film identified below for the' +
            ' following program account:'
          }
        }
      ]
    },
    '405': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none',
          width: '60px'
        },
        {
          type: 'none',
          width: '120px'
        },
        {
          width: '50px'
        },
        {
          type: 'none',
          width: '120px'
        },
        {
          width: '80px',
          type: 'custom',
          format: ['{N4}']
        },
        {
          width: '50%',
          type: 'none'
        }
      ],
      cells: [
        {
          1: {label: 'Program identifier:'},
          2: {num: '406', type: 'dropdown', options: programIdentifier},
          3: {label: 'Reference number:'},
          4: {num: '407'}
        }
      ]
    },
    '410': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none',
          width: '10%',
          cellClass: 'alignCenter'
        },
        {
          width: '20%'
        },
        {
          cellClass: 'alignCenter',
          type: 'none'
        },
        {
          width: '55%'
        }],
      cells: [
        {
          '0': {
            label: 'RepID:'
          },
          '1': {
            num: '411',
            type: 'custom',
            format: ['{N7}']
          },
          '2': {
            label: 'Name of individual:'
          },
          '3': {
            num: '412'
          }
        },
        {
          '0': {
            label: 'or',
            'labelClass': 'bold'
          },
          '1': {
            'type': 'none'
          },
          '2': {
            'type': 'none'
          },
          '3': {
            'type': 'none'
          }
        },
        {
          '0': {
            label: 'GroupID:'
          },
          '1': {
            num: '413',
            type: 'custom',
            format: ['G{N5}']
          },
          '2': {
            label: 'Name of group:'
          },
          '3': {
            num: '414'
          }
        },
        {
          '0': {
            label: 'or',
            'labelClass': 'bold'
          },
          '1': {
            'type': 'none'
          },
          '2': {
            'type': 'none'
          },
          '3': {
            'type': 'none'
          }
        },
        {
          '0': {
            label: 'BN:'
          },
          '1': {
            num: '415',
            type: 'custom',
            format: ['{N9}RC{N4}']
          },
          '2': {
            label: 'Name of firm: '
          },
          '3': {
            num: '416'
          }
        }]
    },
    '510': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '50%',
          cellClass: 'alignLeft'
        },
        {
          cellClass: 'alignLeft'
        }
      ],
      cells: [
        {
          '0': {
            label: 'First name:',
            num: '501'
          },
          '1': {
            label: 'Last name:',
            num: '502'
          }
        },
        {
          '0': {
            label: 'Title:',
            num: '503'
          },
          '1': {
            label: 'Telephone number:',
            num: '504',
            'telephoneNumber': true, type: 'text'
          }
        }
      ]
    },
    '520': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          width: '10%'
        },
        {},
        {
          width: '150px',
          type: 'none'
        },
        {
          width: '200px'
        }
      ],
      cells: [
        {
          '0': {
            label: 'Signature:'
          },
          '2': {
            label: 'Date (MM-DD-YYYY):'
          },
          '3': {
            type: 'date',
            num: '550'
          }
        }
      ]
    }
  });
})();
