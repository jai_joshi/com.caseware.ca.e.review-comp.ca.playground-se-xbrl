(function() {
  wpw.tax.create.diagnostics('rc59x', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var diagObject = {errorCode: 'RC59.oneCheckboxOnlyPart4', fields: ['450', '451', '452', '4031']};

    function checkMultipleCheckboxes(diagObject) {
      diagUtils.diagnostic(diagObject.errorCode, common.prereq(function(tools) {
        return tools.checkMethod(tools.list(diagObject.fields), 'isChecked') > 1;
      }, function(tools) {
        var test = 0;
        tools.checkAll(tools.list(diagObject.fields), function(field) {
          field.isChecked() ? test++ : null;
        });
        return tools.requireAll(tools.list(diagObject.fields), function() {
          return test < 2;
        });
      }));
    }

    checkMultipleCheckboxes(diagObject);

  });
})();
