(function() {

  wpw.tax.create.calcBlocks('rc59x', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //part 1
      calcUtils.getGlobalValue('101', 'CP', '002');
      calcUtils.getGlobalValue('102', 'CP', 'bn');

      //part 5
      calcUtils.getGlobalValue('501', 'CP', '951');
      calcUtils.getGlobalValue('502', 'CP', '950');
      calcUtils.getGlobalValue('503', 'CP', '954');
      calcUtils.getGlobalValue('504', 'CP', '956');
      calcUtils.getGlobalValue('550', 'CP', '955');
    })
  })
})();
