(function() {
  wpw.tax.create.diagnostics('t2s442', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('4420001', common.prereq(common.and(
        common.requireFiled('T2S442'),
        common.check('t2s5.698', 'isNonZero')),
        common.check(['103', '130', '140', '145'], 'isNonZero')));

  });
})();

