(function() {
  var tnWidth = '25px';
  var percentWidth = '50px';
  var formFieldWidth = '107px';

  wpw.tax.create.tables('t2s442', {
    '1220': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '470px',
          type: 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width',

        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          width: percentWidth,

        },
        {
          type: 'none',
          width: '40px'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',

        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {
            label: 'Expenditures from line 103 above'
          },
          '1': {num: '1004'},
          '2': {label: 'x'},
          '3': {num: '1003'},
          '4': {label: '%='},
          '5': {tn: '120'},
          '6': {num: '120', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}},
          '7': {label: 'a'}
        },
        {
          '0': {label: 'Expenditures of line 103 above paid or payable to the Yukon College'},
          '1': {num: '1006'},
          '2': {label: 'x'},
          '3': {num: '1005'},
          '4': {label: '%='},
          '5': {tn: '121'},
          '6': {num: '121', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}},
          '7': {label: 'b'}
        }
      ]
    },
    '1230': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '200px',
          type: 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width'

        },
        {
          type: 'none',
          colClass: 'std-input-col-padding-width '
        },
        {
          width: percentWidth
        },
        {
          type: 'none',
          width: '40px'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'

        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {label: 'Repayments*'},
          '1': {num: '1231', formField: true},
          '2': {label: 'x applicable rate** '},
          '3': {num: '1232'},
          '4': {label: '%='},
          '5': {tn: '145'},
          '6': {num: '145', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}},
          '7': {label: 'e'}
        }
      ]
    }
  })
})();
