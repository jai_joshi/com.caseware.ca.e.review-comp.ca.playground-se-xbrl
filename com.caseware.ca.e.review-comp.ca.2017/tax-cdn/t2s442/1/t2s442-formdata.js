(function() {

  wpw.tax.create.formData('t2s442', {
    formInfo: {
      abbreviation: 't2s442',
      title: 'Yukon Research and development tax credit',
      schedule: 'Schedule 442',
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'For use by corporations with a permanent establishment in the Yukon at any time in the year, ' +
              'which have made eligible expenditures for scientific research and experimental development ' +
              'carried out in the territory, and which want to: ',
              sublist: [
                'calculate a Yukon research and development tax credit; ',
                'claim the credit to reduce Yukon income tax otherwise payable in the current year; or',
                'claim a refund of the remaining credit.'
              ]
            },
            {
              label: 'If all or part of a corporation\'s income is exempt from tax under subsection 149(1) of ' +
              'the federal <i>Income Tax Act</i>, or a corporation is controlled by one or more persons, all or part of ' +
              'whose income is exempt from tax under subsection 149(1) of the federal Act then the corporation ' +
              'is not eligible to claim the Yukon research and development tax credit'
            },
            {
              label: 'An eligible expenditure is one that meets the definition of a qualified expenditure ' +
              'under subsection 127(9) of the federal <i>Income Tax Act</i>'
            },
            {
              label: 'Eligible expenditures must be identified on this schedule and filed no later than ' +
              '12 months after the T2 Corporation Income Tax Return is due for the tax year in which the ' +
              'expenditures were incurred.'
            },
            {
              label: 'Credits earned in the year are applied to reduce Yukon income tax otherwise payable ' +
              'for the year. Any remaining balance will be refunded. '
            },
            {label: 'Use this schedule to show a credit allocated from a trust or a partnership.'},
            {
              label: 'Use this schedule to calculate and claim the Yukon research and development ' +
              'tax credit on repayments of government or non-government assistance or on a contract ' +
              'payment made after December 31, 2010, and in the tax year, that reduced an eligible expenditure.'
            },
            {label: ' Include a completed copy of this schedule with your T2 Corporation Income Tax Return.'}
          ]
        }
      ],
      category: 'Yukon Forms'
    },
    sections: [
      {
        'header': 'Access to Information and Protection of <i>Privacy Act</i> (Yukon)',
        'rows': [
          {
            'label': 'The personal information requested on this form is collected under the authority of and used for the purpose of administering the <i>Income Tax Act</i> (Yukon). Questions about the collection or use of this information can be directed to the Yukon Department of Finance at 867-667-5343, Box 2703, Whitehorse YT Y1A 2C6.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 1 - Total eligible expenditures for research and development incurred in the current tax year',
        'rows': [
          {
            'label': 'Total eligible expenditures for research and development incurred in the current tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '103',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '103'
                }
              }
            ],
            'indicator': 'A'
          }
        ]
      },
      {
        'header': 'Part 2 - Total current year refundable credit',
        'rows': [
          {
            'label': 'Current year refundable credit earned:'
          },
          {
            'type': 'table',
            'num': '1220'
          },
          {
            'label': 'Subtotal (amount a plus amount b)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1011'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1012'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit allocated from a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'Credit allocated from a trust',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '140',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '140'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Is the repayment is for an eligible expenditure paid to the Yukon College?',
            'type': 'infoField',
            'inputType': 'radio',
            'labelWidth': '50%',
            'num': '1229',
            'init': '2'
          },
          {
            'type': 'table',
            'num': '1230'
          },
          {
            'label': 'Subtotal (total of amounts c, d, and e)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1031'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1032'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Total current year refundable credit (amount B plus amount C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': '(enter amount D on line 698 of Schedule 5, Tax Calculation Supplementary - Corporations)'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Repayments must relate to a repayment made after December 31, 2010, by the corporation in the tax year, but not in any other tax year.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Repayments are the sum of the following:'
          },
          {
            'label': '- a repayment made in the tax year of government or non-government assistance or a contract payment that reduced an eligible expenditure other than for first term or second term shared-use-equipment; and',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '- a repayment made in the tax year of government or non-government assistance or a contract payment that reduced an eligible expenditure for first term or second term shared-use-equipment, multiplied by 1/4.',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '** If the repayment is for an eligible expenditure paid to the Yukon College, use 20%. Otherwise use 15%.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  });
})();
