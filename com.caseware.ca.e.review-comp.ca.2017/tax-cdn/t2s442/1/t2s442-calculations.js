(function() {

  wpw.tax.create.calcBlocks('t2s442', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      //from rate table
      calcUtils.getGlobalValue('1003', 'ratesYt', '120');
      calcUtils.getGlobalValue('1005', 'ratesYt', '121');

      field('1004').assign(field('103').get());
      field('120').assign(field('1004').get() * field('1003').get() / 100);
      field('121').assign(field('1006').get() * field('1005').get() / 100);
      field('1011').assign(field('120').get() + field('121').get());
      field('1012').assign(field('1011').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      if (field('1229').get() == '2') {
        calcUtils.getGlobalValue('1232', 'ratesYt', '122');
      } else {
        calcUtils.getGlobalValue('1232', 'ratesYt', '123');
      }

      field('145').assign(field('1231').get() * field('1232').get() / 100);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('1031').assign(
          field('130').get() +
          field('140').get() +
          field('145').get());
      field('1032').assign(field('1031').get());

      field('160').assign(field('1012').get() + field('1032').get());
    });

  });
})
();
