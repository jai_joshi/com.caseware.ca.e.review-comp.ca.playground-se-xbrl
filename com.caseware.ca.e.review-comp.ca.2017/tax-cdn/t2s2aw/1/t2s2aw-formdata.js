(function() {
  'use strict';
  wpw.tax.global.formData.t2s2aw = {
      formInfo: {
        abbreviation: 'T2S2AW',
        title: 'Donations Through Amalgamation/Wind-up Workchart',
        headerImage: 'cw',
        isRepeatForm: true,
        dynamicFormWidth: true,
        repeatFormData: {
          linkedRepeatTable: 'T2S2AS.100',
          titleNum: '101'
        },
        category: 'Workcharts'
      },
      sections: [
        {
        header: 'Detailed Donations Workchart',
          rows: [
            // {
            //   label: 'Update this form from:', num: '102',
            //   type: 'infoField', inputType: 'dropdown', init: '1',
            //   showValues: 'before',
            //   options: [
            //     {value: '1', option: 'No update, enter Data Directly'},
            //     {
            //       value: '2',
            //       option: 'Update from GL in Simple Engagement (the Default if there is SE with a GL)'
            //     },
            //     {value: '3', option: 'From GL in linked CW WP file through TaxSync'},
            //     {
            //       value: '4',
            //       option: 'From a CSV file (from Excel; Google Sheets; QuickBooks; Sage (Simply Accounting) '
            //     }
            //   ]
            // },
            // {labelClass: 'fullLength'},
            {type: 'infoField', label: 'GL A/C # (if applicable)',  inputClass: 'std-input-col-width-3', num: '100'},
            {type: 'infoField', label: 'Description of GL A/C', inputClass: 'std-input-col-width-3', num: '101'},
            {
              label: 'Type of Donations in this Workchart:', num: '103',
              type: 'infoField', inputType: 'dropdown', init: '1',
              showValues: 'before',
              inputClass: 'std-input-col-width-3',
              options: [
                {value: '1', option: 'Charitable Donations (Part 1 of Sch. 2)'},
                //{value: '2', option: 'Gifts to Canada, a province or a Territory (Part 3 of Sch. 2)'},
                {value: '2', option: 'Gifts of certified cultural property (Part 3 of Sch. 2)'},
                {
                  value: '3a', option: 'Gifts of certified ecologically sensitive land made ' +
                'before February 11, 2014(Part 4 of Sch. 2)'
                },
                {
                  value: '3b', option: 'Gifts of certified ecologically sensitive land made ' +
                'after February 10, 2014(Part 4 of Sch. 2)'
                },
                {value: '4', option: 'This form has a mixture of different types of Donations'}
              ]
            },
            {labelClass: 'fullLength'},
            {
              type: 'table', num: '200'
            },
            {
              label: 'SUMMARY:',
              labelClass: 'bold text-right'
            },
            {
              'label': '1. Charitable Donations (Part 1 of Sch. 2)',
              'labelCellClass': 'alignRight',
              'showWhen': {
                'or': [
                  {
                    'fieldId': '103',
                    'compare': {
                      'is': 1
                    }
                  },
                  {
                    'fieldId': '103',
                    'compare': {
                      'is': 4
                    }
                  }
                ]
              },
              'layout': 'alignInput',
              'layoutOptions': {
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '300',
                    'disabled': true
                  }
                }
              ]
            },
            {
              'label': '2. Gifts of certified cultural property (Part 4 of Sch. 2)',
              'labelCellClass': 'alignRight',
              'showWhen': {
                'or': [
                  {
                    'fieldId': '103',
                    'compare': {
                      'is': 2
                    }
                  },
                  {
                    'fieldId': '103',
                    'compare': {
                      'is': 4
                    }
                  }
                ]
              },
              'layout': 'alignInput',
              'layoutOptions': {
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '302',
                    'disabled': true
                  }
                }
              ]
            },
            {
              'label': '3a. Gifts of certified ecologically sensitive land made before February 11, 2014(Part 5 of Sch. 2)',
              'labelCellClass': 'alignRight',
              'showWhen': {
                'or': [
                  {
                    'fieldId': '103',
                    'compare': {
                      'is': '3a'
                    }
                  },
                  {
                    'fieldId': '103',
                    'compare': {
                      'is': 4
                    }
                  }
                ]
              },
              'layout': 'alignInput',
              'layoutOptions': {
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '303',
                    'disabled': true
                  }
                }
              ]
            },
            {
              'label': '3b. Gifts of certified ecologically sensitive land made after February 10, 2014(Part 5 of Sch. 2)',
              'labelCellClass': 'alignRight',
              'showWhen': {
                'or': [
                  {
                    'fieldId': '103',
                    'compare': {
                      'is': '3b'
                    }
                  },
                  {
                    'fieldId': '103',
                    'compare': {
                      'is': 4
                    }
                  }
                ]
              },
              'layout': 'alignInput',
              'layoutOptions': {
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '304',
                    'disabled': true
                  }
                }
              ]
            }
          ]
        },
        {
        header: 'Total Summary of transferred data using TaxBridge',
          rows: [
            {
              type: 'table', num: '500'
            }
          ]
        }
      ]
    };
})();
