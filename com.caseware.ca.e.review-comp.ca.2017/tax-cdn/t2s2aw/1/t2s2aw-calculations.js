(function() {

  wpw.tax.create.calcBlocks('t2s2aw', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      var selectedType = field('103').get();
      var multiType = selectedType == '4';

      var data = [];
      var table = field('200');
      // use getCol instead of getRows to reduce number of cells that need to be converted into Field objects
      table.getCol(4).forEach(function(cell, rowIndex) {
        var donationType;
        if (multiType) {
          donationType = cell.get();
          cell.disabled(false);
        } else {
          donationType = selectedType;
          cell.assign(donationType);
        }
        var donationAmount = table.cell(rowIndex, 5).get();
        data.push({'type': donationType, 'amount': donationAmount});
        field('300').assign(calcUtils.sumByKey(data, 'type', '1', 'amount'));
        field('302').assign(calcUtils.sumByKey(data, 'type', '2', 'amount'));
        field('303').assign(calcUtils.sumByKey(data, 'type', '3a', 'amount'));
        field('304').assign(calcUtils.sumByKey(data, 'type', '3b', 'amount'));
      });
    });
  });
})();
