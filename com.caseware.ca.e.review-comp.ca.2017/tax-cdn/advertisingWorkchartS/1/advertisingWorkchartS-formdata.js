(function() {
  'use strict';
  wpw.tax.global.formData.advertisingWorkchartS = {
      formInfo: {
        abbreviation: 'advertisingWorkchartS',
        title: 'Foreign Advertising Expenses Workchart - Summary',
        neededRepeatForms: ['advertisingWorkchart'],
        headerImage: 'cw',
        category: 'Workcharts'
      },
      sections: [
        {
          hideFieldset: true,
          rows: [
            {labelClass: 'fullLength'},
            {labelClass: 'fullLength'},
            {
              type: 'table', num: '1000'
            }
          ]
        }
      ]
    };
})();
