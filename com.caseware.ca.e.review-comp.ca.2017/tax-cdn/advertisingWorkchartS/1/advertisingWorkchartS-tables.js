(function() {
  wpw.tax.global.tableCalculations.advertisingWorkchartS = {
    "1000": {
      hasTotals: true,
      "linkedRepeatForm": "advertisingWorkchart",
      "columns": [{
        "header": "Description",
        cellClass: 'alignCenter',
        "disabled": true,
        "linkedFieldId": "101"
      },
        {
          "header": "Not deductible for tax purposes: ",
          "total": true,
          "totalNum": "101",
          "canSort": true,
          "init": "0",
          colClass: 'std-input-width',
          "linkedFieldId": "300"
        },
        {
          "header": "Deductible for tax purposes: ",
          colClass: 'std-input-width',
          "total": true,
          "totalNum": "102",
          "canSort": true,
          "init": "0",
          "linkedFieldId": "301"
        }]
    }
  }
})();
