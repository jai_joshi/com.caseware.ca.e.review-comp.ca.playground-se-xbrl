(function() {
  wpw.tax.create.calcBlocks('t661s', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //to allow only 1 option
      if (field('208').get()) {
        field('210').disabled(true)
      }
      else {
        if (calcUtils.hasChanged(field('208'))) {
          field('210').disabled(false)
        }
      }
      if (field('210').get()) {
        field('208').disabled(true)
      }
      else {
        if (calcUtils.hasChanged(field('210'))) {
          field('208').disabled(false)
        }
      }
    });
    calcUtils.calc(function(calcUtils, field) {
      //section C table
      field('1006').getRows().forEach(function(row) {
        if (field('267').get() == 2) {
          row[0].assign();
          row[1].assign();
          row[0].disabled(true);
          row[1].disabled(true);
        }
        else {
          row[0].disabled(false);
          row[1].disabled(false);
        }
      });
      if (field('281').get()) {
        field('282').disabled(false);
      }
      else {
        field('282').disabled(true);
        field('282').assign();
      }
    });
    calcUtils.calc(function(calcUtils, field, form) {

    })
  });
})();
