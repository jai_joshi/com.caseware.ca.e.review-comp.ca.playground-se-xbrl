(function() {

  wpw.tax.create.formData('t661s', {
    formInfo: {
      title: 'T661 - Part 2 – Project information',
      schedule: 'Schedule 60',
      abbreviation: 't661s',
      showCorpInfo: true,
      hideHeaderBox: true,
      num: 't661s',
      isRepeatForm: true,
      repeatFormData: {
        titleNum: '200'
      },
      category: 'Federal Tax Forms',
      formFooterNum: 'T661 E (15)'
    },
    'sections': [
      {
        hideFieldset: true,
        rows: [
          {
            type: 'table',
            num: '2000'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            label: 'Section A – Project identification',
            labelClass: 'fullLength titleFont center'
          },
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {
            label: 'Project title (and identification code if applicable)'
          },
          {
            type: 'infoField',
            inputType: 'textArea',
            tn: '200',
            num: '200',
            maxLength: 175
          },
          {type: 'horizontalLine'},
          {type: 'table', num: '201'},
          {type: 'horizontalLine'},
          {label: 'Project claim history', labelClass: 'bold'},
          {
            type: 'table',
            num: '1009'
          },
          {type: 'horizontalLine'},
          {
            label: 'Was any of the work done jointly or in collaboration with other businesses?',
            type: 'infoField',
            inputType: 'radio',
            labelWidth: '80%',
            num: '218',
            tn: '218'
          },
          {type: 'horizontalLine'},
          {label: 'If you answered yes to line 218, complete lines 220 and 221', labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          {type: 'table', num: '1002'},
          {labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {
            label: 'Section B – Project descriptions ',
            labelClass: 'fullLength titleFont center'
          },
          {labelClass: 'fullLength'},
          {
            label: 'What scientific or technological uncertainties did you attempt to overcome? (Maximum 350 words)'
          },
          {
            type: 'infoField',
            inputType: 'cTextArea',
            tn: '242',
            num: '242',
            maxLength: 3900,
            lineLength: 78
          },
          {labelClass: 'fullLength'},
          {
            label: 'What work did you perform in the tax year to overcome the scientific or ' +
            'technological uncertainties described in line 242?' +
            '(Summarize the systematic investigation or search) (Maximum 700 words)'
          },
          {
            type: 'infoField',
            inputType: 'cTextArea',
            tn: '244',
            num: '244',
            maxLength: 7800,
            lineLength: 78
          },
          {labelClass: 'fullLength'},
          {
            label: 'What scientific or technological advancements did you achieve or attempt' +
            ' to achieve as a result of the work described in line 244? (Maximum 350 words)'
          },
          {
            type: 'infoField',
            inputType: 'cTextArea',
            tn: '246',
            num: '246',
            maxLength: 3900,
            lineLength: 78
          },
          {labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {
            label: 'Section C – Additional project information',
            labelClass: 'fullLength titleFont center'
          },
          {label: 'Who prepared the responses for Section B?', labelClass: 'bold fullLength'},
          {type: 'table', num: '1003'},
          {type: 'table', num: '1004'},
          {type: 'horizontalLine'},
          {labelClass: 'fullLength', forceBreakAfter: true},
          {
            label: 'List the key individuals directly involved in the project and indicate ' +
            'their qualifications/experience.', labelClass: 'bold fullLength'
          },
          {labelClass: 'fullLength'},
          {type: 'table', num: '1005'},
          {labelClass: 'fullLength'},
          {
            label: 'Are you claiming any salary or wages for SR&ED performed outside Canada?',
            type: 'infoField',
            inputType: 'radio',
            labelWidth: '80%',
            num: '265',
            tn: '265'
          },
          {
            label: 'Are you claiming expenditures for SR&ED carried out on behalf of another party? .',
            type: 'infoField',
            inputType: 'radio',
            labelWidth: '80%',
            num: '266',
            tn: '266'
          },
          {
            label: 'Are you claiming expenditures for SR&ED performed by people other than your employees?',
            type: 'infoField',
            inputType: 'radio',
            labelWidth: '80%',
            num: '267',
            tn: '267'
          },
          {label: 'If you answered <b>yes</b> to line 267, complete lines 268 and 269', labelClass: 'fullLength'},
          {type: 'table', num: '1006'},
          {labelClass: 'fullLength'},
          {
            label: 'What evidence do you have to support your claim? (Check any that apply)',
            labelClass: 'fullLength'
          },
          {
            label: 'You do not need to submit these items with the claim. However, you are required ' +
            'to retain them in the event of a review', labelClass: 'fullLength'
          },
          {type: 'table', num: '1007'},
          {type: 'table', num: '1008'},
          {type: 'horizontalLine'},
          {label: 'T661 - Part 6 - Project cost in the year', labelClass: 'bold'},
          {
            'label': 'Salary or wages in the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '752'
                }
              }
            ]
          },
          {
            'label': 'Cost of materials in the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '754'
                }
              }
            ]
          },
          {
            'label': 'Contract expenditures for SR&ED performed on your behalf in the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '756'
                }
              }
            ]
          }
        ]
      }
    ]
  });
})();
