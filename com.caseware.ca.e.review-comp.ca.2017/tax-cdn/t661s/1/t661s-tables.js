(function() {

  function getLastPartCols(isLastRow) {
    var tableCols = [
      {type: 'none', colClass: 'std-padding-width'},
      {type: 'none', colClass: 'std-spacing-width'},
      {type: 'singleCheckbox', colClass: 'std-padding-width'},
      {type: 'none', colClass: 'std-spacing-width'},
      {type: 'none', colClass: 'std-input-col-width-2'},
      {type: 'none', colClass: 'std-spacing-width'},
      {type: 'none', colClass: 'std-padding-width'},
      {type: 'none', colClass: 'std-spacing-width'},
      {type: 'singleCheckbox', colClass: 'std-padding-width'},
      {type: 'none', colClass: 'std-spacing-width'},
      {type: 'none', colClass: 'std-input-col-width-2'}
    ];

    if (!isLastRow) {
      tableCols.push({
        type: 'none'
      })
    } else {
      tableCols.push(
          {type: 'none', colClass: 'std-spacing-width'},
          {type: 'none', colClass: 'std-padding-width'},
          {type: 'none', colClass: 'std-spacing-width'},
          {type: 'text'},
          {type: 'none', colClass: 'std-padding-width'}
      )
    }

    return tableCols;
  }

  wpw.tax.create.tables('t661s', {
    '2000':{
      fixedRows: true,
      infoTable: true,
      columns:[
        {
          type: 'none'
        },
        {
          type: 'none',
          cellClass: 'alignRight',
          colClass: 'std-input-col-width-2'
        }
      ],
      cells: [
        {
          0: {
            label: 'Complete a separate Part 2 for each project claimed this year.',
            labelClass: 'bold'
          },
          1:{
            label: 'CRA internal form identifier 060 <br> Code 1501'
          }
        }
      ]
    },
    '201': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          cellClass: 'alignLeft',
          label: 'Project start date',
          tn: '202',
          type: 'date'
        },
        {type: 'none', colClass: 'std-spacing-width'},
        {
          cellClass: 'alignLeft',
          label: 'Completion or expected completion date',
          tn: '204',
          type: 'date'
        },
        {type: 'none', colClass: 'std-spacing-width'},
        {
          cellClass: 'alignLeft',
          label: 'Field of science or technology code',
          tn: '206',
          type: 'selector',
          selectorOptions: {
            title: 'Field of Science or Technology',
            items: wpw.tax.codes.scienceTechCodes
          }
        }
      ],
      cells: [
        {
          0: {num: '202'},
          2: {num: '204'},
          4: {num: '206'}
        }
      ]
    },
    '1002': {
      'maxLoop': 100000,
      'showNumbering': true,
      'columns': [
        {
          'header': 'Names of the businesses',
          'tn': '220',
          type: 'text'
        },
        {
          'header': 'Business Number',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR'],
          tn: '221'
        }
      ]
    },
    '1003': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'singleCheckbox', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'none', colClass: 'std-input-col-width-2'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'text'}
      ],
      cells: [
        {
          0: {tn: '253'},
          2: {num: '253'},
          4: {label: 'Employee directly involved in the project'},
          6: {tn: '254'},
          8: {label: 'Name', num: '254'}
        },
        {
          0: {tn: '255'},
          2: {num: '255'},
          4: {label: 'Other employee of the company'},
          6: {tn: '256'},
          8: {label: 'Name', num: '256'}
        }
      ]
    },
    '1004': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'singleCheckbox', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'none', colClass: 'std-input-col-width-2'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'text', colClass: 'std-input-col-width-2'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'text'}
      ],
      cells: [
        {
          0: {tn: '257'},
          2: {num: '257'},
          4: {label: 'External consultant'},
          6: {tn: '258'},
          8: {label: 'Name', num: '258'},
          10: {tn: '259'},
          12: {label: 'Firm', num: '259'}
        }
      ]
    },
    '1005': {
      'maxLoop': 100000,
      'showNumbering': true,
      'columns': [
        {
          'header': 'Names',
          'tn': '260',
          type: 'text'
        },
        {
          'header': 'Qualifications/experience and position title',
          'tn': '261',
          type: 'text'
        }
      ]
    },
    '1006': {
      'maxLoop': 100000,
      'showNumbering': true,
      'columns': [
        {
          'header': 'Names of individuals or companies',
          'tn': '268',
          type: 'text'
        },
        {
          'header': 'Business Number',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR'],
          tn: '269'
        }
      ]
    },
    '1007': {
      fixedRows: true,
      infoTable: true,
      columns: getLastPartCols(),
      cells: [
        {
          0: {tn: '270'},
          2: {num: '270'},
          4: {label: 'Project planning documents'},
          6: {tn: '276'},
          8: {num: '276'},
          10: {label: 'Progress reports, minutes of project meetings'}
        },
        {
          0: {tn: '271'},
          2: {num: '271'},
          4: {label: 'Records of resources allocated to the project, time sheets'},
          6: {tn: '277'},
          8: {num: '277'},
          10: {label: 'Test protocols, test data, analysis of test results, conclusions'}
        },
        {
          0: {tn: '272'},
          2: {num: '272'},
          4: {label: 'Design of experiments'},
          6: {tn: '278'},
          8: {num: '278'},
          10: {label: 'Photographs and videos'}
        },
        {
          0: {tn: '273'},
          2: {num: '273'},
          4: {label: 'Project records, laboratory notebooks'},
          6: {tn: '279'},
          8: {num: '279'},
          10: {label: 'Samples, prototypes, scrap or other artefacts'}
        },
        {
          0: {tn: '274'},
          2: {num: '274'},
          4: {label: 'Design, system architecture and source code'},
          6: {tn: '280'},
          8: {num: '280'},
          10: {label: 'Contracts'}
        }
      ]
    },
    '1008': {
      fixedRows: true,
      infoTable: true,
      columns: getLastPartCols(true),
      cells: [
        {
          0: {tn: '275'},
          2: {num: '275'},
          4: {label: 'Records of trial runs'},
          6: {tn: '281'},
          8: {num: '281'},
          10: {label: 'Others.     Specify:'},
          12: {tn: '282'},
          14: {num: '282'}
        }
      ]
    },
    '1009': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'none', colClass: 'std-padding-width'},
        {type: 'singleCheckbox', colClass: 'std-padding-width'},
        {type: 'none', colClass: 'std-spacing-width'},
        {type: 'none'}
      ],
      cells: [
        {
          0: {tn: '208'},
          1: {num: '208'},
          3: {
            label: 'Continuation of a previously claimed project'
          }
        },
        {
          0: {tn: '210'},
          1: {num: '210'},
          3: {
            label: 'First claim for the project'
          }
        }
      ]
    }
  })
})();
