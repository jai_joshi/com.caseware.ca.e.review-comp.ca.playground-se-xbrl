(function() {

  wpw.tax.create.diagnostics('t661s', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T661s'), forEach.row('1002', forEach.bnCheckCol(1, true))));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T661s'), forEach.row('1006', forEach.bnCheckCol(1, true))));

    diagUtils.diagnostic('0600001', common.prereq(common.and(
      common.requireFiled('T661'),
      common.check(['200', '202', '204', '218', '265', '266', '267'], 'isFilled', true)),
        //check table for field 260
      function(tools) {
        var tableA = tools.field('1005');
        return tools.checkAll(tableA.getRows(), function(row) {
          return row[0].require('isFilled')
        });
      }
  ));

    diagUtils.diagnostic('0600003', common.prereq(common.requireFiled('T661s'), common.check(['208', '210'], 'isChecked')));

    diagUtils.diagnostic('0600004', common.prereq(common.and(
        common.requireFiled('T661'),
        common.check('218', 'isYes')),
        function(tools) {
          return tools.field('1002').cell(0, 0).require('isFilled');
        }));

    diagUtils.diagnostic('0600009', common.prereq(common.requireFiled('T661s'),
        common.check(['242', '244', '246'], 'isNonZero', true)));

    diagUtils.diagnostic('0600010', common.prereq(common.requireFiled('T661s'),
        common.check(['253', '255', '257'], 'isChecked')));

    diagUtils.diagnostic('0600011', common.prereq(common.and(
        common.requireFiled('T661'),
        common.check('253', 'isChecked')),
        common.check('254', 'isFilled')));

    diagUtils.diagnostic('0600012', common.prereq(common.and(
        common.requireFiled('T661'),
        common.check('255', 'isChecked')),
        common.check('256', 'isFilled')));

    diagUtils.diagnostic('0600013', common.prereq(common.and(
        common.requireFiled('T661'),
        common.check('257', 'isChecked')),
        common.check('258', 'isFilled')));

    diagUtils.diagnostic('0600014', common.prereq(common.and(
        common.requireFiled('T661'),
        common.check('267', 'isYes')),
        function(tools) {
          var table = tools.field('1006');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireAll([row[0], row[1]], 'isFilled');
          });
        }));

    diagUtils.diagnostic('0600015', common.prereq(common.requireFiled('T661s'),
        function(tools) {
          var table1 = tools.field('1007');
          var table2 = tools.field('1008');
          return tools.requireOne([table1.cell(0, 2), table1.cell(1, 2), table1.cell(2, 2), table1.cell(3, 2), table1.cell(4, 2), table2.cell(0, 2),
            table1.cell(0, 8), table1.cell(1, 8), table1.cell(2, 8), table1.cell(3, 8), table1.cell(4, 8), table2.cell(0, 8)], 'isChecked');
        }));

    diagUtils.diagnostic('0600016', common.prereq(common.and(
        common.requireFiled('T661'),
        function(tools) {
          return tools.field('1008').cell(0, 8).isChecked();
        }), function(tools) {
      return tools.field('1008').cell(0, 14).require('isFilled');
    }));

    diagUtils.diagnostic('0600018', common.prereq(common.requireFiled('T661s'), common.check('206', 'isFilled')));

  });
})();
