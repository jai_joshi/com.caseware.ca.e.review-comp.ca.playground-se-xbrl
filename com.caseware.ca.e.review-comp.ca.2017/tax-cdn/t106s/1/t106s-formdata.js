(function() {

  wpw.tax.global.formData.t106s = {
    formInfo: {
      isRepeatForm: true,
      abbreviation: 't106s',
      title: 'Information Return of Non-Arm\'s Length Transactions with Non-Residents-Slips',
      printTitle: 'T106 Slip',
      subtitle: 't106s Summary Form',
      repeatFormData:{
        titleNum: '1000'
      },
      highlightFieldSets: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            'Refer to the instruction sheet before you complete this form.',
            'Check the applicable boxes and complete the areas that apply'
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            'type': 'infoField',
            'inputType': 'dropdown',
            'labelWidth': '70%',
            'num': '100',
            'options': [
              {
                'option': '',
                'value': ''
              },
              {
                'option': 'New',
                'value': '1'
              },
              {
                'option': 'Unmodified',
                'value': '2'
              },
              {
                'option': 'Amended',
                'value': '3'
              },
              {
                'option': 'Delete (Close)',
                'value': '4'
              }
            ],
            'label': 'What type of slip is this?'
          },
          {
            'type': 'infoField',
            'inputType': 'dropdown',
            'showWhen': {
              'fieldId': '100',
              'compare': {
                'is': '4'
              }
            },
            'num': '101',
            'options': [
              {
                'option': '',
                'value': ''
              },
              {
                'option': 'Duplicate',
                'value': '1'
              },
              {
                'option': 'Filed in Error',
                'value': '2'
              },
              {
                'option': 'Other',
                'value': '3'
              }
            ],
            'label': 'What is the reason for deleting this supplement?'
          },
          {
            'type': 'infoField',
            'labelWidth': '70%',
            'width': '16%',
            'num': '102',
            'label': 'Sequence Number:'
          }
        ]
      },
      {
        header: 'Part I – Reporting person/partnership identification',
        rows: [
          {
            type: 'table',
            num: '2100'
          },
          {
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            inputType: 'dateRange',
            label: 'For what taxation year are you filing these T106 form?',
            labelWidth: '85%',
            'label1': 'Tax year-start', num: '110', disabled: true,
            source1: 'cp-tax_start',
            'label2': 'Tax year-end', 'num2': '111', source2: 'cp-tax_end'
          }
        ]
      },
      {
        header: 'Part II – Non-resident information',
        rows: [
          {
            type: 'infoField',
            inputClass: 'std-input-col-padding-width-2',
            label: '1. Name of the non-resident',
            num: '1000'
          },
          {type: 'horizontalLine'},
          {
            label: '2. Address of the non-resident and country of residence' +
            ' (see Instructions for information on country codes)'
          },
          {
            type: 'table',
            num: '1100'
          },
          {type: 'horizontalLine'},
          {
            type: 'splitTable', fieldAlignRight: true,
            side1: [
              {
                label: '3. Type of relationship'
              },
              {
                type: 'table',
                num: '1150'
              },
              {labelClass: 'fullLength'},
              {
                type: 'infoField',
                inputType: 'dropdown',
                inputClass: 'std-input-width',
                label: 'Select one: ',
                num: '1150-1',
                'options': [
                  {
                    'option': '',
                    'value': ''
                  },
                  {
                    'option': '1',
                    'value': '1'
                  },
                  {
                    'option': '2',
                    'value': '2'
                  },
                  {
                    'option': '3',
                    'value': '3'
                  }
                ]
              }
            ],
            side2: [
              {
                label: ''
              },
              {
                type: 'infoField',
                inputType: 'radio',
                canClear: true,
                label: 'If "1", is the non-resident in a country with which Canada does not have a tax treaty?' +
                '<b> If yes, see message below:</b>',
                labelWidth: '75%',
                num: '1062-3'
              },
              {
                label: ''
              },
              {
                showWhen: {fieldId: '1062-3', compare: {is: '1'}},
                label: 'Please send <b>all related financial statements</b> by paper copy to the following address: <br>' +
                '<br>' +
                'Winnipeg Taxation Centre<br>' +
                'Data Assessment & Evaluation Programs<br>' +
                'Validation & Verification Section<br>' +
                'Foreign Reporting Returns<br>' +
                '66 Stapon Road<br>' +
                'Winnipeg MB  R3C 3M2'
              }
            ]
          },
          {type: 'horizontalLine'},
          {
            label: '4. State the main business activities for the transactions reported' +
            'in Part III by entering the appropriate NAICS code(s). See instructions for NAICS codes.',
            labelClass: 'fullLength'
          },
          {
            type: 'table',
            num: '1130'
          },
          {type: 'horizontalLine'},
          {
            label: '5. State the main countries for the transactions reported in Part III' +
            'by entering the appropriate country code(s) – see Instructions.',
            labelClass: 'fullLength'
          },
          {
            type: 'table',
            num: '1250'
          },
          {type: 'horizontalLine'},
          {
            type: 'splitTable', fieldAlignRight: true,
            side1: [
              {
                type: 'infoField',
                inputType: 'radio',
                canClear: true,
                label: '6. Have you prepared or obtained contemporaneous documentation as described' +
                'in subsection 247(4) of the <i>Income Tax Act</i> for the tax year/fiscal period' +
                'with respect to the non-resident?',
                labelWidth: '75%',
                num: '1062-6'
              }
            ],
            side2: [
              {
                type: 'infoField',
                inputType: 'radio',
                canClear: true,
                label: '7. Have any of the transfer pricing methodologies (TPM)' +
                'changed since the previous reporting period with respect to the non-resident?',
                labelWidth: '75%',
                num: '1062-7'
              }
            ]
          }
        ]
      },
      {
        header: 'Part III – Transactions between reporting person/partnership and non-resident',
        rows: [
          {
            label: 'Enter in the appropriate box the monetary consideration' +
            '(to the nearest Canadian dollar/ functional currency unit if applicable)' +
            'derived or incurred for the following transactions with the non-resident.' +
            'Enter the appropriate transfer pricing methodology (TPM) codes from the list in the Instructions.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '1300',
            hastTotals: true
          },
          {
            type: 'table',
            num: '1350'
          },
          {
            labelClass: 'fullLength'
          },
          {
            type: 'table',
            num: '1400',
            hasTotals: true
          },
          {
            type: 'table',
            num: '1500'
          },
          {
            type: 'table',
            num: '1600'
          },
          {
            type: 'table',
            num: '1630'
          },
          {
            type: 'table',
            num: '1650'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '1700'
          },
          {
            type: 'table',
            num: '1750'
          },
          {
            type: 'table',
            num: '1760'
          }
        ]
      },
      {
        header: 'Part IV – Loans, advances, investments and similar amounts',
        rows: [
          {
            type: 'table',
            num: '1800'
          }

        ]
      },
      {
        header: 'Part V – Derivatives',
        rows: [
          {
            type: 'table',
            num: '1900'
          },
          {
            type: 'table',
            num: '1950'
          },
          {
            type: 'table',
            num: '1960'
          }

        ]
      },
      {
        header: 'Part VI – Current accounts',
        rows: [
          {
            type: 'table',
            num: '2000'
          }
        ]
      }
    ]
  }
})
();