(function() {
  wpw.tax.create.diagnostics('t106s', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var mandatoryCheck = ['100', '1000', '1009', '1010', '1150-1', '1131', '1135', '1062-6', '1062-7'];
    var mandatoryFields = [
      {'num': '100', errorCode: '106s.100Check'}, {'num': '1000', errorCode: '106s.1000Check'}, {'num': '1009', errorCode: '106s.1009Check'},
      {'num': '1010', errorCode: '106s.1010Check'}, {'num': '1150-1', errorCode: '106s.1150-1Check'}, {'num': '1131', errorCode: '106s.1131Check'},
      {'num': '1135', errorCode: '106s.1135Check'}, {'num': '1062-6', errorCode: '106s.1062-6Check'}, {'num': '1062-7', errorCode: '106s.1062-7Check'}];
    var part3TableTPMCheck = ['1301', '1351', '1403', '1404', '1405', '1406', '1502', '1503', '1504', '1505', '1632', '1651', '1751'];
    var descriptionChecks = [{numField: '1351-1', descField: '1351'},{numField: '1651-1', descField: '1651'},{numField: '1751-1', descField: '1751'}];

    diagUtils.diagnostic('BN', common.prereq(common.check('1002', 'isFilled'), common.bnCheck('1002')));

    diagUtils.diagnostic('106s.taxTreatyCheck', common.prereq(common.check('1150-1', 'isYes'), common.check('1062-3', 'isNonZero')));

    diagUtils.diagnostic('106s.deleteReasonCheck', common.prereq(common.check('100', function() {return this.get() == 4;}), common.check('101', 'isFilled')));

    // diagUtils.diagnostic('106s.requiredToFile', common.prereq(common.check('t106.102', 'isNonZero'), common.check('100', 'isNonZero')));

    for (var table in part3TableTPMCheck) {
      tpmCheck(part3TableTPMCheck[table]);
    }

    for (var field in mandatoryFields) {
      requiredFieldCheck(mandatoryFields[field]);
    }

    for (var check in descriptionChecks) {
      descriptionCheck(descriptionChecks[check]);
    }

    function tpmCheck(table) {
      diagUtils.diagnostic('106s.tpmCheck', common.prereq(common.check('100', 'isNonZero'),
          function(tools) {
            if (tools.checkMethod(tools.list([table + '-1', table + '-3']), 'isNonZero') > 1)
              return tools.requireAll(tools.list([table + '-2', table + '-4']), 'isNonZero');
            else if (tools.field(table + '-1').isNonZero())
              return tools.field(table + '-2').require('isNonZero');
            else if(tools.field(table + '-3').isNonZero())
              return tools.field(table + '-4').require('isNonZero');
            else return true;
          }));
    }

    function requiredFieldCheck(field) {
      diagUtils.diagnostic(field.errorCode, common.prereq(function(tools) {
        return tools.checkMethod(tools.list(mandatoryCheck), 'isNonZero')
      }, common.check(field.num, 'isNonZero')));
    }

    function descriptionCheck(fieldObj) {
      diagUtils.diagnostic('106s.descriptionCheck', common.prereq(common.check(fieldObj.numField, 'isNonZero'), common.check(fieldObj.descField, 'isFilled')));
    }

  });
})();
