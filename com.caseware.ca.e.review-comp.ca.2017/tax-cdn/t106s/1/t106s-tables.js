(function() {
  var addCanada = wpw.utilities.clone(wpw.tax.codes.countryCodes);
  addCanada['CAN'] = {value: 'CAN Canada'};
  var countryCodesAddCanada = new wpw.tax.actions.codeToDropdownOptions(addCanada, 'value', 'value');
  var tpm = [
    {
      'value': ' ',
      'option': ' '
    },
    {
      'value': '01',
      'option': '01 Comparable Uncontrolled Price'
    },
    {
      'value': '02',
      option: '02 Cost-Plus'
    },
    {
      'value': '03',
      option: '03 Resale'
    },
    {
      'value': '04',
      option: '04 Profit Split'
    },
    {
      'value': '05',
      option: '05 Transactional Net Margin'
    },
    {
      'value': '06',
      option: '06 Qualifying Cost Contribution Arrangement'
    },
    {
      'value': '07',
      option: '07 Other'
    }
  ];

  wpw.tax.global.tableCalculations.t106s = {
    '2100': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '80px'
        },
        {},
        {
          width: '80px'
        },
        {}
      ],
      cells: [
        {
          '0': {
            label: 'Corporation',
            type: 'singleCheckbox',
            num: '1001',
            disabled: true
          },
          '1': {
            type: 'custom',
            format: ['{N9}RC{N4}', 'NR'],
            label: 'Business Number (BN)',
            num: '1002',
            disabled: true
          },
          '2': {
            label: 'Trust',
            type: 'singleCheckbox',
            num: '1003',
            disabled: true
          },
          '3': {
            label: 'Trust account number',
            type: 'custom',
            format: ['T{N8}', 'NA'],
            disabled: true,
            num: '1004'
          }
        },
        {
          '0': {
            label: 'Partnership',
            type: 'singleCheckbox',
            num: '1005',
            disabled: true
          },
          '1': {
            label: 'Partnership identification number',
            type: 'custom',
            format: ['{N9}RZ{N4}', 'NR'],
            disabled: true,
            num: '1006'
          },
          '2': {
            label: 'Individual',
            type: 'singleCheckbox',
            num: '1007',
            disabled: true
          },
          '3': {
            label: 'Social Insurance Number',
            num: '1008',

            disabled: true
          }
        }
      ]
    },
    '1100': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          colClass: 'std-input-col-padding-width-3',
          type: 'text'
        },
        {
          type: 'none'
        },
        {
          type: 'dropdown',
          options: countryCodesAddCanada,
          colClass: 'std-input-width'
        }
      ],
      'cells': [
        {
          '0': {
            num: '1009'
          },
          '1': {
            label: 'Country code',
            cellClass: 'alignRight'
          },
          '2': {
            num: '1010'
          }
        }
      ]
    },
    '1150': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          type: 'none',
          width: '10px'
        },
        {
          type: 'none',
          width: '10px',
          label: '1.'
        },
        {
          type: 'none',
          label: 'Non-resident is controlled<br>' +
          'by reporting person/partnership'
        },
        {
          type: 'none',
          width: '10px',
          label: '2.'
        },
        {
          type: 'none',
          label: 'Non-resident controls<br>' +
          'reporting person/partnership'
        },
        {
          type: 'none',
          width: '10px',
          label: '3.'
        },
        {
          type: 'none',
          label: 'Other',
          colClass: 'std-padding-width'
        }
      ]
    },
    '1130': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'header': '',
          'type': 'none',
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          "header": "NAICS code",
          "type": "selector",
          selectorOptions: {
            title: 'NAICS Codes',
            items: wpw.tax.codes.naicsCodes
          }
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          "header": "NAICS code",
          "type": "selector",
          selectorOptions: {
            title: 'NAICS Codes',
            items: wpw.tax.codes.naicsCodes
          }
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          "header": "NAICS code",
          "type": "selector",
          selectorOptions: {
            title: 'NAICS Codes',
            items: wpw.tax.codes.naicsCodes
          }
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          "header": "NAICS code",
          "type": "selector",
          selectorOptions: {
            title: 'NAICS Codes',
            items: wpw.tax.codes.naicsCodes
          }
        }
      ],
      'cells': [
        {
          '0': {
            label: 'NAICS Code(s):'
          },
          '2': {
            num: '1131',
            cellClass: 'alignLeft'
          },
          '4': {
            num: '1132',
            cellClass: 'alignLeft'
          },
          '6': {
            num: '1133',
            cellClass: 'alignLeft'
          },
          '8': {
            num: '1134',
            cellClass: 'alignLeft'
          }
        }
      ]
    },
    '1250': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'type': 'none',
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          type: 'dropdown',
          options: countryCodesAddCanada
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          type: 'dropdown',
          options: countryCodesAddCanada
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          type: 'dropdown',
          options: countryCodesAddCanada
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          type: 'dropdown',
          options: countryCodesAddCanada
        }
      ],
      'cells': [
        {
          '0': {
            label: 'Country Code(s):'
          },
          '2': {
            num: '1135'
          },
          '4': {
            num: '1136'
          },
          '6': {
            num: '1137'
          },
          '8': {
            num: '1138'
          }
        }
      ]
    },
    '1300': {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          'header': '<br>' + '<b>Tangible Property</b>'
        },
        {
          colClass: 'std-input-width',
          "header": "<b>Sold<br>" +
          "to non-resident</b>",
          'totalMessage': ''
        },
        {
          "width": "55px",
          "header": '<b>TPM</b>',
          "type": 'dropdown',
          options: tpm
        },
        {
          colClass: 'std-input-width',
          "header": "<b>Purchased from non-resident</b>",
          "hiddenTotal": true,
          'totalMessage': ''
        },
        {
          "width": "55px",
          'header': '<b>TPM</b>',
          "type": "dropdown",
          options: tpm
        }
      ],
      "cells": [{
        "0": {
          "label": "Stock in trade/raw materials"
        },
        "1": {
          "num": "1301-1"
        },
        "2": {
          "num": "1301-2"
        },
        "3": {
          "num": "1301-3"
        },
        "4": {
          "num": "1301-4"
        }
      }
      ],
      "hasTotals": true
    },
    '1350': {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none"
        },
        {},
        {
          colClass: 'std-input-width'
        },
        {
          "width": "55px",
          "type": 'dropdown',
          options: tpm
        },
        {
          colClass: 'std-input-width'
        },
        {
          "width": "55px",
          "type": "dropdown",
          options: tpm
        }
      ],
      "cells": [{
        "0": {
          "label": "Other (specify):"
        },
        '1': {
          "num": "1351",
          type: 'text'
        },
        "2": {
          "num": "1351-1"
        },
        "3": {
          "num": "1351-2"
        },
        "4": {
          "num": "1351-3"
        },
        "5": {
          "num": "1351-4"
        }
      }
      ]
    },
    '1400': {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          'header': '<b>Rents, Royalties and Intangible Property</b>'
        },
        {
          colClass: 'std-input-width',
          "header": "<b>Revenue from non-resident</b>"
        },
        {
          "width": "55px",
          "header": '<b>TPM</b>',
          "type": 'dropdown',
          options: tpm
        },
        {
          colClass: 'std-input-width',
          "header": "<b>Expenditure to non-resident</b>"
        },
        {
          "width": "55px",
          'header': '<b>TPM</b>',
          "type": "dropdown",
          options: tpm
        }
      ],
      "cells": [{
        "0": {
          "label": "Rents"
        },
        "1": {
          "num": "1403-1"
        },
        "2": {
          "num": "1403-2"
        },
        "3": {
          "num": "1403-3"
        },
        "4": {
          "num": "1403-4"
        }
      },
        {
          "0": {
            "label": "Royalties (e.g., for the use of patents, trademarks, secret formulas, know-how)"
          },
          "1": {
            "num": "1404-1"
          },
          "2": {
            "num": "1404-2"
          },
          "3": {
            "num": "1404-3"
          },
          "4": {
            "num": "1404-4"
          }
        },
        {
          "0": {
            "label": "License or franchise fees"
          },
          "1": {
            "num": "1405-1"
          },
          "2": {
            "num": "1405-2"
          },
          "3": {
            "num": "1405-3"
          },
          "4": {
            "num": "1405-4"
          }
        },
        {
          "0": {
            "label": "Intangible property or rights (acquired or disposed of)"
          },
          "1": {
            "num": "1406-1"
          },
          "2": {
            "num": "1406-2"
          },
          "3": {
            "num": "1406-3"
          },
          "4": {
            "num": "1406-4"
          }
        }
      ]
    },
    '1500': {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          'header': '<b>Services</b>'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "width": "55px",
          "type": 'dropdown',
          options: tpm
        },
        {
          colClass: 'std-input-width',
          "hiddenTotal": true,
          "total": true
        },
        {
          "width": "55px",
          "type": "dropdown",
          options: tpm
        }
      ],
      "cells": [{
        "0": {
          "label": "Management, financial, administrative, marketing, training, etc"
        },
        "1": {
          "num": "1502-1"
        },
        "2": {
          "num": "1502-2"
        },
        "3": {
          "num": "1502-3"
        },
        "4": {
          "num": "1502-4"
        }
      },
        {
          "0": {
            "label": "Engineering, technical, construction, etc"
          },
          "1": {
            "num": "1503-1"
          },
          "2": {
            "num": "1503-2"
          },
          "3": {
            "num": "1503-3"
          },
          "4": {
            "num": "1503-4"
          }
        },
        {
          "0": {
            "label": "Research and development"
          },
          "1": {
            "num": "1504-1"
          },
          "2": {
            "num": "1504-2"
          },
          "3": {
            "num": "1504-3"
          },
          "4": {
            "num": "1504-4"
          }
        },
        {
          "0": {
            "label": "Commissions"
          },
          "1": {
            "num": "1505-1"
          },
          "2": {
            "num": "1505-2"
          },
          "3": {
            "num": "1505-3"
          },
          "4": {
            "num": "1505-4"
          }
        }
      ]
    },
    '1600': {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          'header': '<b>Financial</b>'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "width": "55px",
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "width": "55px",
          "type": "none"
        }
      ],
      "cells": [{
        "0": {
          "label": "Interest"
        },
        "1": {
          "num": "1602-1"
        },
        "2": {
          "num": "1602-2"
        },
        "3": {
          "num": "1602-3"
        },
        "4": {
          "num": "1602-4"
        }
      },
        {
          "0": {
            "label": "Dividends (e.g., common stock, preferred stock, deemed dividends)"
          },
          "1": {
            "num": "1603-1"
          },
          "2": {
            "num": "1603-2"
          },
          "3": {
            "num": "1603-3"
          },
          "4": {
            "num": "1603-4"
          }
        },
        {
          "0": {
            "label": "Sale of financial property (including factoring, securitizations and securities)"
          },
          "1": {
            "num": "1604-1"
          },
          "2": {
            "num": "1604-2"
          },
          "3": {
            "num": "1604-3"
          },
          "4": {
            "num": "1604-4"
          }
        },
        {
          "0": {
            "label": "Lease payments"
          },
          "1": {
            "num": "1605-1"
          },
          "2": {
            "num": "1605-2"
          },
          "3": {
            "num": "1605-3"
          },
          "4": {
            "num": "1605-4"
          }
        },
        {
          "0": {
            "label": "Securities Lending (fees and compensation payments)"
          },
          "1": {
            "num": "1606-1"
          },
          "2": {
            "num": "1606-2"
          },
          "3": {
            "num": "1606-3"
          },
          "4": {
            "num": "1606-4"
          }
        }
      ]
    },
    '1630': {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none"
        },
        {
          colClass: 'std-input-width'
        },
        {
          "width": "55px",
          "type": 'dropdown',
          options: tpm
        },
        {
          colClass: 'std-input-width'
        },
        {
          "width": "55px",
          "type": "dropdown",
          options: tpm
        }
      ],
      "cells": [{
        "0": {
          "label": "Insurance"
        },
        "1": {
          "num": "1632-1"
        },
        "2": {
          "num": "1632-2"
        },
        "3": {
          "num": "1632-3"
        },
        "4": {
          "num": "1632-4"
        }
      }
      ]
    },
    '1650': {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none"
        },
        {},
        {
          colClass: 'std-input-width'
        },
        {
          "width": "55px",
          "type": 'dropdown',
          options: tpm
        },
        {
          colClass: 'std-input-width'
        },
        {
          "width": "55px",
          "type": "dropdown",
          options: tpm
        }
      ],
      "cells": [{
        "0": {
          "label": "Other (excluding derivatives – see Part V):"
        },
        "1": {
          "num": "1651",
          type: 'text'
        },
        "2": {
          "num": "1651-1"
        },
        "3": {
          "num": "1651-2"
        },
        "4": {
          "num": "1651-3"
        },
        "5": {
          "num": "1651-4"
        }
      }
      ]
    },
    '1700': {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          'header': '<b>Other</b>'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "width": "55px",
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "width": "55px",
          "type": "none"
        }
      ],
      "cells": [{
        "0": {
          "label": "Reimbursement of expense"
        },
        "1": {
          "num": "1702-1"
        },
        "2": {
          "num": "1702-2"
        },
        "3": {
          "num": "1702-3"
        },
        "4": {
          "num": "1702-4"
        }
      }
      ]
    },
    '1750': {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none"
        },
        {},
        {
          colClass: 'std-input-width'
        },
        {
          "width": "55px",
          "type": 'dropdown',
          options: tpm
        },
        {
          colClass: 'std-input-width'
        },
        {
          "width": "55px",
          "type": "dropdown",
          options: tpm
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Other:"
          },
          "1": {
            "num": "1751",
            type: 'text'
          },
          "2": {
            "num": "1751-1"
          },
          "3": {
            "num": "1751-2"
          },
          "4": {
            "num": "1751-3"
          },
          "5": {
            "num": "1751-4"
          }
        }
      ]
    },
    '1760': {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          type: 'none'
        },
        {
          type: 'none',
          width: '50px',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-input-width'
        },
        {
          width: '50px',
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "width": '50px',
          'type': 'none'
        }
      ],
      'cells': [
        {
          "0": {
            "label": "Please enter the total of all entries made in each column of Part III"
          },
          "1": {
            type: 'none',
            label: '<b>A=</b>'
          },
          "2": {
            "num": "1752"
          },

          "3": {
            type: 'none',
            label: '<b>B=</b>'
          },
          "4": {
            "num": "1752-2"
          },
          "5": {
            "num": "1752-3"
          }
        }
      ]

    },
    "1800": {
      "fixedRows": true,
      "infoTable": true,
      columns: [
        {
          type: 'none'
        },
        {
          header: '<b>Beginning balance</b>',
          colClass: 'std-input-width',
          formField: true
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          header: '<b>Increase </b>',
          colClass: 'std-input-width',
          formField: true
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          header: '<b>Decrease</b>',
          colClass: 'std-input-width',
          formField: true
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          header: '<b>Ending balance</b>',
          colClass: 'std-input-width',
          disabled: true
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Amounts owed by reporting person/partnership"
          },
          "1": {
            "num": "1801"
          },
          "2": {
            "label": " + "
          },
          "3": {
            'num': '1802'
          },
          "4": {
            'label': '-'
          },
          "5": {
            "num": "1803"
          },
          "6": {
            "label": " = "
          },
          "7": {
            'num': '1804'
          }
        },
        {
          "0": {
            "label": "Amounts owed to reporting person/partnership"
          },
          "1": {
            "num": "1805"
          },
          "2": {
            "label": " + "
          },
          "3": {
            'num': '1806'
          },
          "4": {
            'label': '-'
          },
          "5": {
            "num": "1807"
          },
          "6": {
            "label": " = "
          },
          "7": {
            'num': '1808'
          }
        },
        {
          "0": {
            "label": "Investment in non-resident (ACB)"
          },
          "1": {
            "num": "1809"
          },
          "2": {
            "label": " + "
          },
          "3": {
            'num': '1810'
          },
          "4": {
            'label': '-'
          },
          "5": {
            "num": "1811"
          },
          "6": {
            "label": " = "
          },
          "7": {
            'num': '1812'
          }
        },
        {
          "0": {
            "label": "Please enter the total of all entries made in each column of Part IV"
          },
          "1": {
            "num": "1813",
            'type': 'none'
          },
          "2": {
            "label": " <b>C=</b> "
          },
          "3": {
            'num': '1820'
          },
          "4": {
            'label': '<b>D=</b>'
          },
          "5": {
            "num": "1830"
          },
          "6": {
            'type': 'none'
          },
          "7": {
            'type': 'none'
          }
        }
      ]
    },
    '1900': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none'
        },
        {
          num: '1929-0'
        },
        {
          header: '<b>Number of<br> contracts </b>',
          colClass: 'std-input-width',
          formField: true
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          header: '<b>Notional<br> amount </b>',
          colClass: 'std-input-width',
          formField: true
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          header: '<b>Revenue from <br> non-resident </b>',
          colClass: 'std-input-width',
          formField: true
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          header: '<b>Expenditure to<br>non-resident </b>',
          colClass: 'std-input-width',
          formField: true
        }],
      cells: [
        {
          '0': {
            label: '101 Interest Rate Contracts'
          },
          '1': {
            type: 'none'
          },
          '2': {
            colClass: 'std-input-width',
            num: '1901'
          },
          '3': {
            label: '$',
            width: '30px'
          },
          '4': {
            colClass: 'std-input-width',
            num: '1902'
          },
          '5': {
            label: '$',
            width: '30px'
          },
          '6': {
            colClass: 'std-input-width',
            num: '1903'
          },
          '7': {
            label: '$',
            width: '30px'
          },
          '8': {
            colClass: 'std-input-width',
            num: '1904'
          }
        },
        {
          '0': {
            label: '102 Foreign Exchange Contracts'
          },
          '1': {
            type: 'none'
          },
          '2': {
            colClass: 'std-input-width',
            num: '1905'
          },
          '3': {
            label: '$',
            width: '30px'
          },
          '4': {
            colClass: 'std-input-width',
            num: '1906'
          },
          '5': {
            label: '$',
            width: '30px'
          },
          '6': {
            colClass: 'std-input-width',
            num: '1907'
          },
          '7': {
            label: '$',
            width: '30px'
          },
          '8': {
            colClass: 'std-input-width',
            num: '1908'
          }
        },
        {
          '0': {
            label: '103 Credit Contracts'
          },
          '1': {
            type: 'none'
          },
          '2': {
            colClass: 'std-input-width',
            num: '1909'
          },
          '3': {
            label: '$',
            width: '30px'
          },
          '4': {
            colClass: 'std-input-width',
            num: '1910'
          },
          '5': {
            label: '$',
            width: '30px'
          },
          '6': {
            colClass: 'std-input-width',
            num: '1911'
          },
          '7': {
            label: '$',
            width: '30px'
          },
          '8': {
            colClass: 'std-input-width',
            num: '1912'
          }
        },
        {
          '0': {
            label: '104 Equity Contracts'
          },
          '1': {
            type: 'none'
          },
          '2': {
            colClass: 'std-input-width',
            num: '1913'
          },
          '3': {
            label: '$',
            width: '30px'
          },
          '4': {
            colClass: 'std-input-width',
            num: '1914'
          },
          '5': {
            label: '$',
            width: '30px'
          },
          '6': {
            colClass: 'std-input-width',
            num: '1915'
          },
          '7': {
            label: '$',
            width: '30px'
          },
          '8': {
            colClass: 'std-input-width',
            num: '1916'
          }
        },
        {
          '0': {
            label: '105 Commodity Contracts'
          },
          '1': {
            type: 'none'
          },
          '2': {
            colClass: 'std-input-width',
            num: '1917'
          },
          '3': {
            label: '$',
            width: '30px'
          },
          '4': {
            colClass: 'std-input-width',
            num: '1918'
          },
          '5': {
            label: '$',
            width: '30px'
          },
          '6': {
            colClass: 'std-input-width',
            num: '1919'
          },
          '7': {
            label: '$',
            width: '30px'
          },
          '8': {
            colClass: 'std-input-width',
            num: '1920'
          }
        },
        {
          '0': {
            label: '106 Index Contracts'
          },
          '1': {
            type: 'none'
          },
          '2': {
            colClass: 'std-input-width',
            num: '1921'
          },
          '3': {
            label: '$',
            width: '30px'
          },
          '4': {
            colClass: 'std-input-width',
            num: '1922'
          },
          '5': {
            label: '$',
            width: '30px'
          },
          '6': {
            colClass: 'std-input-width',
            num: '1923'
          },
          '7': {
            label: '$',
            width: '30px'
          },
          '8': {
            colClass: 'std-input-width',
            num: '1924'
          }
        },
        {
          '0': {
            label: '107 Fees (including commissions)'
          },
          '1': {
            type: 'none'
          },
          '2': {
            colClass: 'std-input-width',
            num: '1925'
          },
          '3': {
            label: '$',
            width: '30px'
          },
          '4': {
            colClass: 'std-input-width',
            num: '1926'
          },
          '5': {
            label: '$',
            width: '30px'
          },
          '6': {
            colClass: 'std-input-width',
            num: '1927'
          },
          '7': {
            label: '$',
            width: '30px'
          },
          '8': {
            colClass: 'std-input-width',
            num: '1928'
          }
        },
        {
          '0': {
            label: '108 Other payments / receipts (specify)'
          },
          '1': {
            type: 'text',
            num: '1929'
          },
          '2': {
            colClass: 'std-input-width',
            num: '1930'
          },
          '3': {
            label: '$',
            width: '30px'
          },
          '4': {
            colClass: 'std-input-width',
            num: '1931'
          },
          '5': {
            label: '$',
            width: '30px'
          },
          '6': {
            colClass: 'std-input-width',
            num: '1932'
          },
          '7': {
            label: '$',
            width: '30px'
          },
          '8': {
            colClass: 'std-input-width',
            num: '1933'
          }
        }
      ]
    },
    '1950': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none'
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          colClass: 'std-input-width'
        }],
      cells: [{
        '0': {
          label: 'Please enter the total of all entries made in each column of Part V'
        },
        '1': {
          label: '<b>E=</b>'
        },
        '2': {
          num: '1951'
        },
        '3': {
          label: '<b>F=</b>'
        },
        '4': {
          colClass: 'std-input-width',
          num: '1952'
        },
        '5': {
          label: '<b>G=</b>'
        },
        '6': {
          num: '1953'
        },
        '7': {
          label: '<b>H=</b>'
        },
        '8': {
          num: '1954'
        }
      }
      ]
    },
    '1960': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none'
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          colClass: 'std-input-width',
          type: 'none'
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          colClass: 'std-input-width',
          type: 'none'
        }],
      cells: [
        {
          '0': {
            label: 'Please enter in box I the total of all entries made in boxes A, B, C, D, G and H'
          },
          '1': {
            label: '<b>I=</b>'
          },
          '2': {
            colClass: 'std-input-width',
            num: '1961'
          }
        }
      ]
    },
    '2000': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none'
        },
        {
          header: '<b>Beginning balance </b>',
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          header: '<b>Increase</b>',
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          header: '<b>Decrease</b>',
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          header: '<b>Ending balance</b>',
          colClass: 'std-input-width',
          disabled: true
        }],
      cells: [
        {
          '0': {
            label: 'Amount of accounts payable'
          },
          '1': {
            num: '2001'
          },
          '2': {
            label: '+ $',
            width: '30px'
          },
          '3': {
            num: '2002'
          },
          '4': {
            label: '- $',
            width: '30px'
          },
          '5': {
            num: '2003'
          },
          '6': {
            label: '= $',
            width: '30px'
          },
          '7': {
            num: '2004'
          }
        },
        {
          '0': {
            label: 'Amount of accounts receivable'
          },
          '1': {
            num: '2005'
          },
          '2': {
            label: '+ $',
            width: '30px'
          },
          '3': {
            num: '2006'
          },
          '4': {
            label: '- $',
            width: '30px'
          },
          '5': {
            num: '2007'
          },
          '6': {
            label: '= $',
            width: '30px'
          },
          '7': {
            num: '2008'
          }
        }
      ]
    }
  }
})();
