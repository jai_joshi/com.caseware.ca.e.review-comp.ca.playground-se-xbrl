(function() {

  wpw.tax.create.calcBlocks('t106s', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {

      function removeOtherIdentification(calcUtils, valueArrayNum, checkboxArrayNum) {
        for (var i = 0; i < valueArrayNum.length; i++) {
          field(valueArrayNum[i]).assign(undefined);
          field(checkboxArrayNum[i]).assign(false);
        }
      }

      var soldToNonRes = ['1301-1', '1351-1', '1403-1', '1404-1', '1405-1', '1406-1', '1502-1', '1503-1', '1504-1', '1505-1', '1602-1', '1603-1', '1604-1',
        '1605-1', '1606-1', '1632-1', '1651-1', '1702-1', '1751-1'];

      var purchaseFromNonRes = ['1301-3', '1351-3', '1403-3', '1404-3', '1405-3', '1406-3', '1502-3', '1503-3', '1504-3', '1505-3',
        '1602-3', '1603-3', '1604-3', '1605-3', '1606-3', '1632-3', '1651-3', '1702-3', '1751-3'];

      var increase = ['1802', '1806', '1810'];

      var decrease = ['1803', '1807', '1811'];

      var numberOfContracts = ['1901', '1905', '1909', '1913', '1917', '1921', '1925', '1930'];

      var nationalAmount = ['1902', '1906', '1910', '1914', '1918', '1922', '1926', '1931'];

      var revenueFromNonRes = ['1903', '1907', '1911', '1915', '1919', '1923', '1927', '1932'];

      var expenditureToNonRes = ['1904', '1908', '1912', '1916', '1920', '1924', '1928', '1933'];

      var totalEntries = ['1752', '1752-2', '1820', '1830', '1953', '1954'];

      var identificationType = calcUtils.form('T106').field('103').get();

      //corporation//
      if (identificationType == 1) {
        field('1001').assign(true);
        calcUtils.getGlobalValue('1002', 'CP', 'bn');
        removeOtherIdentification(calcUtils, [1006, 1004, 1008], [1003, 1005, 1007]);
      }
/*      //partnership//
      else if (identificationType == 2) {
        field('1005').assign(true);
        calcUtils.getGlobalValue('1006', 'T106', '1012');
        removeOtherIdentification(calcUtils, [1002, 1004, 1008], [1001, 1003, 1007]);
      }
      // trust//
      else if (identificationType == 3) {
        field('1003').assign(true);
        calcUtils.getGlobalValue('1004', 'T106', '1010');
        removeOtherIdentification(calcUtils, [1002, 1006, 1008], [1001, 1005, 1007]);
      }
      //individual//
      else if (identificationType == 4) {
        field('1007').assign(true);
        calcUtils.getGlobalValue('1008', 'T106', '1011');
        removeOtherIdentification(calcUtils, [1002, 1004, 1006], [1001, 1003, 1005]);
      }*/

      calcUtils.getGlobalValue('110', 'CP', 'tax_start');
      calcUtils.getGlobalValue('111', 'CP', 'tax_end');

      calcUtils.sumBucketValues('1752', soldToNonRes);
      calcUtils.sumBucketValues('1752-2', purchaseFromNonRes);

      //Part IV
      field('1800').getRows().forEach(function(row) {
        row[7].assign(row[1].get() + row[3].get() - row[5].get());
      });
      calcUtils.sumBucketValues('1820', increase);
      calcUtils.sumBucketValues('1830', decrease);

      calcUtils.sumBucketValues('1951', numberOfContracts);
      calcUtils.sumBucketValues('1952', nationalAmount);
      calcUtils.sumBucketValues('1953', revenueFromNonRes);
      calcUtils.sumBucketValues('1954', expenditureToNonRes);
      calcUtils.sumBucketValues('1961', totalEntries);
    });

    calcUtils.calc(function(calcUtils, field, form) {

      field('102').assign(calcUtils.form().sequence);
      //part VI
      field('2000').getRows().forEach(function(row) {
        row[7].assign(row[1].get() + row[3].get() - row[5].get());
      });

    });
  });
})();
