(function() {
  function getHistoricalTableColumns() {
    return [
      {
        type: 'none'
      },
      {
        colClass: 'std-input-width',
        type: 'date',
        disabled: true,
        rf: true,
        header: '<b>Year of origin</b>'
      },
      {colClass: 'std-padding-width', type: 'none'},
      {
        colClass: 'std-input-width',
        type: 'none',

        total: true,
        rf: true,
        totalMessage: 'Total : ',
        header: '<b>Balance at the beginning of the tax year</b>'
      },
      {colClass: 'std-padding-width', type: 'none'},
      {
        colClass: 'std-input-width',
        type: 'none',
        rf: true,

        total: true,
        totalMessage: ' ',
        header: '<b>Transfer on amalgamation or wind up</b>'
      },
      {colClass: 'std-padding-width', type: 'none'},
      {
        colClass: 'std-input-width',  total: true,
        totalMessage: ' ', rf: true,
        header: '<b>Applied to Part VI<b>'
      },
      {colClass: 'std-padding-width', type: 'none'},
      {
        colClass: 'std-input-width',  total: true,
        totalMessage: ' ', rf: true,
        header: '<b>Balance at the end of year</b>'
      },
      {type: 'none', colClass: 'std-padding-width'},
      {
        type: 'none'
      }
    ]
  }

  var historicalTable7YearsCells = [
    {
      3: {type: ''},
      4: {label: '*'},
      5: {type: 'none'},
      7: {type: 'none'},
      9: {type: 'none'}
    },
    {
      3: {type: ''},
      5: {type: ''},
      7: {type: ''},
      9: {type: ''},
      10: {label: '**'}
    },
    {
      3: {type: ''},
      5: {type: ''},
      7: {type: ''},
      9: {type: ''}
    },
    {
      3: {type: ''},
      5: {type: ''},
      7: {type: ''}
    },
    {
      3: {type: ''},
      5: {type: ''},
      7: {type: ''}
    },
    {
      3: {type: ''},
      5: {type: ''},
      7: {type: ''}
    },
    {
      3: {type: ''},
      5: {type: ''},
      7: {type: ''}
    },
    {
      3: {type: ''},
      5: {type: ''},
      7: {type: ''}
    },
    {
      3: {type: ''},
      5: {type: ''},
      7: {type: ''}
    }

  ];

  wpw.tax.global.tableCalculations.t2s37 = {
    '5011': {
      fixedRows: true, num: '5011', infoTable: true, hasTotals: true,
      columns: getHistoricalTableColumns(),
      cells: historicalTable7YearsCells
    }
  }
})();
