(function() {
  'use strict';

  var countryAddressCodes = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.countryAddressCodes, 'value');

  wpw.tax.global.formData.t2s37 = {
    formInfo: {
      abbreviation: 'T2s37',
      title: 'Calculation of Unused Surtax Credit',
      //subTitle: '(2010 and later years)',
      schedule: 'Schedule 37',
      code: 'Code 1001',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            'Use this schedule to calculate a corporation\'s closing balance of unused surtax credit.',
            'Any unused surtax credit can be carried forward seven years. Unused surtax credits must be applied' +
            ' in order of the oldest first',
            'Refer to subsection 181.1(7) of the <i>Income Tax Act</i> when calculating the amount deductible for' +
            ' a corporation\'s unused surtax credits where control of the corporation has been acquired between the ' +
            'year in which the credits arose and the year in which you want to claim them.'
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        header: 'Calculation of closing balance of unused surtax credit',
        rows: [
          {
            'label': 'Unused surtax credit at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Deduct:</b> Unused surtax credit expired after seven tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '115',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '115'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Unused surtax credit at the beginning of the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '120'
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Add:</b> Unused surtax credit transferred on an amalgamation or the windup of a subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '220'
                },
                'padding': {
                  'type': 'tn',
                  'data': '220'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '230'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Deduct: </b> Amount of unused surtax credit carried forward from previous years and applied to reduce Part VI tax<br> payable in the current year (amount from line 885 of Schedule 38)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '420'
                },
                'padding': {
                  'type': 'tn',
                  'data': '420'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Closing balance of unused surtax credit</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '820'
                },
                'padding': {
                  'type': 'tn',
                  'data': '820'
                }
              }
            ]
          }
        ]
      },
      {
        header:'Historical Data and calculation for Unused surtax credit',
        rows: [
          {labelClass: 'fullLength'},
          {
            type: 'table',num: '5011'
          },
          {labelClass: 'fullLength'},
          {
            'label': '* Expired (will be posted to line 115)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '5014'
                }
              },
              null
            ]
          },
          {
            'label': '** Expiring if not use: ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '5015'
                }
              },
              null
            ]
          },
          {
            label: '<b>NOTE: Total applied to Part VI needs to be the same as line 420</b>'
          }
        ]

      }
    ]
  };
})();