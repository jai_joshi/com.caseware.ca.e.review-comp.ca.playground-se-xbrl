(function() {
  var columnWidth = '107px';

  wpw.tax.global.tableCalculations.t2s50 = {
    '050': {
      maxLoop: 10,
      linkedExternalTable: [{formId: 'safeIncomeWorkchart', fieldId: '1003'}],
      'showNumbering': true,
      hasTotals: true,
      'superHeaders': [
        {
          'divider': true
        },
        {
          'header': 'Provide only one number per shareholder',
          'divider': true,
          'colspan': '3'
        }
      ],
      'columns': [
        {
          'header': 'Name of shareholder',
          'num': '100',
          'tn': '100',
          type: 'text',
          cellClass: 'alignLeft'
        },
        {
          'header': 'Business Number',
          'num': '200',
          'tn': '200',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR'],
          cellClass: 'alignLeft'
        },
        {
          'header': 'Social insurance number',
          'num': '300',
          'tn': '300',
          type: 'custom',
          format: ['{N9}', 'NA'],
          cellClass: 'alignLeft',
          validate: {
            or: [
              {check: 'isEmpty'},
              {compare: {equal: 'NA'}},
              {
                and: [
                  {compare: {between: [100000000, 999999999]}},
                  {check: 'mod10'}
                ]
              }
            ]
          }
        },
        {
          'header': 'Trust number',
          'num': '350',
          'tn': '350',
          type: 'custom',
          format: ['T{N8}', 'NA']
        },
        {
          'header': 'Percentage common shares',
          'tn': '400',
          colClass: 'std-input-width',
          'filters': 'decimals 2',
          decimals: 3,
          "validate": {
            "and": [
              'percent',
              {
                "or": [
                  {"length": {"min": "1", "max": "6"}},
                  {"check": "isEmpty"}
                ]
              }
            ]
          },
          total: true,
          totalNum: '1001'
        },
        {
          'header': 'Percentage preferred shares',
          'tn': '500',
          colClass: 'std-input-width',
          'num': '500',
          'filters': 'decimals 2',
          decimals: 3,
          "validate": {
            "and": [
              'percent',
              {
                "or": [
                  {"length": {"min": "1", "max": "6"}},
                  {"check": "isEmpty"}
                ]
              }
            ]
          },
          total: true,
          totalNum: '1002'
        }
      ]
    }
  }
})();
