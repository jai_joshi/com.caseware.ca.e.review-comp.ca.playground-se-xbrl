(function() {
  wpw.tax.create.diagnostics('t2s50', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S50'), forEach.row('050', forEach.bnCheckCol(1, true))));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S50'), forEach.row('050', forEach.bnCheckCol(1, true))));

    diagUtils.diagnostic('0500001', common.prereq(
        common.check(['t2j.173'], 'isChecked'),
        common.requireFiled('T2S50')));

    diagUtils.diagnostic('0500002',
        {
          label: 'For each shareholder listed at line 050100, a percentage of ' +
          'common shares (050400) and/or percentage of preferred shares (050500) are required.',
          severity: 'required',
          category: 'EFILE'
        }, common.prereq(common.requireFiled('T2S50'),
            function(tools) {
              var table = tools.field('050');
              return tools.checkAll(table.getRows(), function(row) {
                if (row[0].isNonZero())
                  return tools.requireOne([row[4], row[5]], 'isNonZero');
                else return true;
              });
            }));

    diagUtils.diagnostic('0500002',
        {
          label: 'For each Business number provided in column 050200, a name and percentage ' +
          'of common shares and/or preferred shares are required.',
          severity: 'required',
          category: 'EFILE'
        }, common.prereq(common.requireFiled('T2S50'),
            function(tools) {
              var table = tools.field('050');
              return tools.checkAll(table.getRows(), function(row) {
                if (row[1].isNonZero())
                  return tools.requireOne([row[0]], 'isNonZero') &&
                      tools.requireOne([row[4], row[5]], 'isNonZero');
                else return true;
              });
            }));

    diagUtils.diagnostic('0500002',
        {
          label: 'For each social insurance number provided in column 050300, a name and ' +
          'percentage of common shares and/or preferred shares are required.',
          severity: 'required',
          category: 'EFILE'
        }, common.prereq(common.requireFiled('T2S50'),
            function(tools) {
              var table = tools.field('050');
              return tools.checkAll(table.getRows(), function(row) {
                if (row[2].isNonZero())
                  return tools.requireOne([row[0]], 'isNonZero') &&
                      tools.requireOne([row[4], row[5]], 'isNonZero');
                else return true;
              });
            }));

    diagUtils.diagnostic('0500002',
        {
          label: 'For each trust number provided in column 050350, a name and percentage of common ' +
          'shares and/or preferred shares are required.',
          severity: 'required',
          category: 'EFILE'
        }, common.prereq(common.requireFiled('T2S50'),
            function(tools) {
              var table = tools.field('050');
              return tools.checkAll(table.getRows(), function(row) {
                if (row[3].isNonZero())
                  return tools.requireOne([row[0]], 'isNonZero') &&
                      tools.requireOne([row[4], row[5]], 'isNonZero');
                else return true;
              });
            }));

    diagUtils.diagnostic('0500002',
        {
          label: 'For each percentage of common shares provided in column 050400, a name of ' +
          'a shareholder is required in column 050100.',
          severity: 'required',
          category: 'EFILE'
        }, common.prereq(common.requireFiled('T2S50'),
            function(tools) {
              var table = tools.field('050');
              return tools.checkAll(table.getRows(), function(row) {
                if (row[4].isNonZero())
                  return tools.requireOne([row[0]], 'isNonZero');
                else return true;
              });
            }));

    diagUtils.diagnostic('0500002',
        {
          label: 'For each percentage of preferred shares provided in column 050500, a name ' +
          'of a shareholder is required in column 050100.',
          severity: 'required',
          category: 'EFILE'
        }, common.prereq(common.requireFiled('T2S50'),
            function(tools) {
              var table = tools.field('050');
              return tools.checkAll(table.getRows(), function(row) {
                if (row[5].isNonZero())
                  return tools.requireOne([row[0]], 'isNonZero');
                else return true;
              });
            }));

    diagUtils.diagnostic('0501001', common.prereq(common.requireFiled('T2S50'),
        function(tools) {
          var column = tools.field('050').getCol('4');
          return tools.checkAll(column, function(cell) {
            return cell.require(function() {
              return this.get() ? this.get() <= 100 : true;
            })
          });
        }));

    diagUtils.diagnostic('0501002', common.prereq(common.and(
        common.requireFiled('T2S50'),
        function(tools) {
          return tools.field('050').cell(0, 4).isFilled();
        }),
        function(tools) {
          return tools.field('1001').require(function() {
            return this.get() <= 100;
          })
        }));

    diagUtils.diagnostic('0501003', common.prereq(common.requireFiled('T2S50'),
        function(tools) {
          var column = tools.field('050').getCol('5');
          return tools.checkAll(column, function(cell) {
            return cell.require(function() {
              return this.get() ? this.get() <= 100 : true;
            })
          });
        }));

    diagUtils.diagnostic('0501004', common.prereq(common.and(
        common.requireFiled('T2S50'),
        function(tools) {
          return tools.field('050').cell(0, 5).isFilled();
        }),
        function(tools) {
          return tools.field('1002').require(function() {
            return this.get() <= 100;
          })
        }));

    diagUtils.diagnostic('050.200', common.prereq(common.requireFiled('T2S50'),
        function(tools) {
          var table = tools.field('050');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[1], row[2], row[3]], 'isNonZero') > 1)
              return tools.requireAll([row[1], row[2], row[3]], function() {
                return tools.checkMethod([row[1], row[2], row[3]], 'isZero') > 1;
              });
            else return true;
          });
        }));
  });
})();
