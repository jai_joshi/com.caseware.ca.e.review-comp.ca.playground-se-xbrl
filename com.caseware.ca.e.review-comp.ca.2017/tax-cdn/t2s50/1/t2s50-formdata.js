(function() {
  'use strict';

  wpw.tax.global.formData.t2s50 = {
    formInfo: {
      abbreviation: 'T2S50',
      title: 'Shareholder Information',
      //TODO: DO NOT DELETE THIS LINE: code
      schedule: 'Schedule 50',
      code: 'Code 0601',
      showCorpInfo: true,
      formFooterNum: 'T2 SCH 50 (06)',
      hideProtectedB: true,
      headerImage: 'canada-federal',
      description: [
        {
          text: 'All private corporations must complete this schedule for any shareholder who holds 10% or more of the' +
          ' corporation\'s common and/or preferred shares. '
        },
        {text: ' '}
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        header: ' ', hideFieldset: true,
        rows: [
          {
            type: 'table', num: '050'
          },
          {labelClass: 'fullLength'},
          {
            label: '<i><b>Business Number</i></b>: if a corporation is not registered, enter \'NR\'',
            labelClass: 'fullLength'
          },
          {
            label: '<i><b>Social Insurance Number</i></b>: If an individual does not have a social insurance number, enter \'NR\'',
            labelClass: 'fullLength'
          },
          {
            label: '<i><b>Trust Number</i></b>: If a trust does not have a trust number, enter \'NA\'',
            labelClass: 'fullLength'
          }
        ]
      }
    ]
  };
})();
