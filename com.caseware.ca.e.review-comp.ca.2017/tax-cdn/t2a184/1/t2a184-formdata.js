(function () {
  wpw.tax.create.formData('t2a184', {
    formInfo: {
      abbreviation: 't2a184',
      title: 'Waiver In Respect Of Time Limit For Assessment, Reassessment Or Determination',
      formFooterNum: 'AT184 (Jul-12)',
      // headerImage: 'canada-alberta',
      showCorpInfo: true,
      category: 'Alberta Tax Forms',
      showDots: false
    },
    sections: [
      {
        rows: [
          {
            label: 'Name of Corporation (please print)',
            type: 'infoField',
            inputClass: 'std-input-col-width-2',
            num: '001'
          },
          {
            type: 'infoField',
            inputType: 'custom',
            format: ['{N10}'],
            label: 'Alberta Corporate Account Number',
            num: '002'
          },
          {
            label: '(enter your 9 or 10 digit account number)'
          },
          {
            'type': 'infoField',
            'inputType': 'address',
            'add1Num': '011',
            'add2Num': '012',
            'provNum': '016',
            'cityNum': '015',
            'countryNum': '017',
            'postalCodeNum': '018'
          },
          {
            label: 'Waiver for the taxation year:',
            labelClass: 'center',
            type: 'infoField',
            inputType: 'date'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            label: '&#9679; For use by a corporation to waive the time limit, under section 43(1) of the Alberta ' +
            'Corporate Tax Act, within which the President of Treasury Board and Minister of Finance may assess, ' +
            'reassess or make additional assessments of tax, interest or penalties or determine refundable tax credits.',
            labelClass: 'fullLength'
          },
          {
            label: '&#9679; One completed copy of this waiver is to be filed',
            labelClass: 'fullLength'
          },
          {
            label: 'a) One completed copy of this waiver is to be filed',
            labelClass: 'fullLength indent-3'
          },
          {
            label: 'b) for a corporation other than a Canadian-controlled private corporation, within 4 years of the date of mailing\n' +
            'of an original notice of assessment or notification that no tax is payable for the taxation year if the notice\n' +
            'was mailed on or after June 25, 1988.',
            labelClass: 'fullLength indent-3'
          },
          {
            label: '&#9679; In order for the waiver to be valid, the matters in respect of which the time limit is ' +
            'waived must be specified. The waiver must be signed by an officer of the corporation who has authority to bind the corporation.',
            labelClass: 'fullLength'
          },
          {
            label: '&#9679; The waiver is to be forwarded to TAX AND REVENUE ADMINISTRATION, 9811 109 ST, EDMONTON AB T5K 2L5.\n' +
            'Fax 780-422-5284.',
            labelClass: 'fullLength'
          },
          {
            label: '&#9679; This waiver remains in force until six months after the date of filing of a Notice' +
            ' of Revocation of Waiver (form AT2156) in prescribed form.',
            labelClass: 'fullLength'
          },
          {
            label: 'WAIVER',
            labelClass: 'center bold titleFont'
          },
          {
            label: 'The time limit referred to in section 43(1) of the Alberta Corporate Tax Act, within which the President of Treasury Board and\n' +
            'Minister of Finance may reassess or make additional assessments or assess tax, interest or penalties and determine the\n' +
            'entitlement to and amount of refundable tax credits, is hereby waived for the taxation year indicated above in respect of:',
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            label: 'Authorized Signing Officer'
          }
        ]
      },
      {
        hideFieldSet: true,
        rows: [
          {
            'type': 'multiColumn',
            'dividers': [
              false
            ],
            'columns': [
              [
                {
                  type: 'infoField',
                  label: 'Signature of Authorized Signing Officer'
                }
              ],
              [
                {
                  type: 'infoField',
                  label: 'Position or Office:',
                  num: '202'
                }
              ],
              [
                {
                  type: 'infoField',
                  label: 'Date:',
                  inputType: 'date',
                  num: '203'
                }
              ]
            ]
          }
        ]
      }
    ]
  });
})();
