(function() {

  var addOther = wpw.utilities.clone(wpw.tax.codes.countryCodes);
  addOther['OTH'] = {value: 'OTH Other'};
  var countryCodesAddOther = new wpw.tax.actions.codeToDropdownOptions(addOther, 'value', 'value');

  var t1135Options = [
    {
      value: '1',
      option: 'Funds held outside canada'
    },
    {
      value: '2',
      option: 'Shares of non-resident corporations (other than foreign affiliates)'
    },
    {
      value: '3',
      option: 'Indebtedness owed by non-resident'
    },
    {
      value: '4',
      option: 'Interests in non-resident trusts'
    },
    {
      value: '5',
      option: 'Real property outside Canada (other than personal use and real estate used in an active business)'
    },
    {
      value: '6',
      option: 'Other property outside Canada'
    }
  ];
  var shareTransactionOptions = [
    {value: '0', option: ''},
    {value: '1', option: 'Purchase'},
    {value: '2', option: 'Disposition'},
    {value: '3', option: 'Split'},
    {value: '4', option: 'Reverse stock split'},
    {value: '5', option: 'Return of capital'},
    {value: '6', option: 'Amalgamation/Dissolution'},
    {value: '7', option: 'Acquisition of control'},
    {value: '8', option: 'Other'}
  ];

  wpw.tax.global.tableCalculations.acbTracker = {
    '200': {
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: 'Date of last transaction',
          type: 'date'
        },
        {
          header: 'Total number of shares',
          decimals: 4
        },
        {
          header: 'Average ACB per share',
          decimals: 6
        },
        {
          header: 'Total cost of shares',
          decimals: 4
        }
      ],
      cells: [
        {
          0: {num:'1051'},
          1: {num:'1052'},
          2: {num:'1053'},
          3: {num:'1054'}
        }
      ]
    },
    '201': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-col-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none'
        }
      ],
      cells: [
        {
          1: {label: 'Foreign country'},
          2: {
            num: '111',
            type: 'dropdown',
            options: countryCodesAddOther
          }
        },
        {
          1: {label: 'T1135 category (if applicable)'},
          2: {
            num: '112',
            type: 'dropdown',
            options: t1135Options
          }
        },
        {
          1: {label: 'Transfer to T1135 ?'},
          2: {
            num: '113',
            type: 'singleCheckbox'
          },
          showWhen: {
            or: [
              {fieldId: '100', compare: {is: 1}},
              {fieldId: '100', compare: {is: 2}},
              {fieldId: '100', compare: {is: 3}},
              {fieldId: '100', compare: {is: 4}}
            ]
          }
        }
      ]
    },
    '300': {
      showNumbering: true,
      scrollx: true,
      columns: [
        {
          header: 'Transaction date',
          type: 'date',
          colClass: 'std-input-width'
        },
        {
          header: 'Transaction type',
          type: 'dropdown',
          colClass: 'std-input-width',
          options: shareTransactionOptions,
          init: '1'
        },
        {
          header: 'Number of shares acquired',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Acquisition cost',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Adjustment',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Exchange rate',
          init: '1.0',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Number of shares held',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Total cost of shares',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Average ACB per share',
          colClass: 'std-input-width',
          decimals: 6
        },
        {
          header: 'Number of shares disposed of',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Proceeds of disposition',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'ACB of shares disposed of',
          colClass: 'std-input-width',
          decimals: 4
        }
      ]
    },
    '410': {
      infoTable: false,
      fixedRows: true,
      columns: [
        {
          header: '<b>Acquisition date</b>',
          type: 'date',
          colClass: 'std-input-width'
        },
        {
          header: '<b>Acquisition cost</b>'
        },
        {
          header: '<b>Disposition date</b>',
          type: 'date',
          colClass: 'std-input-width'
        },
        {
          header: '<b>Proceeds of disposition</b>'
        },
        {
          header: '<b>Outlays and expenses from disposition</b>'
        }
      ],
      cells: [
        {
          '0': {num: '411'},
          '1': {num: '412'},
          '2': {num: '413'},
          '3': {num: '414'},
          '4': {num: '415'}
        }
      ]
    },
    '420': {
      infoTable: false,
      hasTotals: true,
      showNumbering: true,
      width: 'calc((100% - 105px))',
      columns: [
        {
          header: '<b>Date</b>',
          colClass: 'std-input-width',
          type: 'date'
        },
        {
          header: '<b>Adjustment Description</b>',
          type: 'text'
        },
        {
          header: '<b>Amount</b>',
          total: true,
          colClass: 'std-input-width'
        }
      ]
    },
    '505': {
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: 'Date of last transaction',
          type: 'date'
        },
        {
          header: 'Total value of property held'
        },
        {
          header: 'Total acquisition cost of property held'
        },
        {
          header: 'Average ACB for every $100'
        }
      ],
      cells: [
        {
          0: {num:'1061'},
          1: {num:'1062'},
          2: {num:'1063'},
          3: {num:'1064'}
        }
      ]
    },
    '510': {
      showNumbering: true,
      scrollx: true,
      columns: [
        {
          header: 'Transaction date',
          type: 'date',
          colClass: 'std-input-width'
        },
        {
          header: 'Transaction type',
          type: 'dropdown',
          colClass: 'std-input-width',
          init: '1',
          options: [
            {value: '1', option: 'Purchase'},
            {value: '2', option: 'Disposition'},
            {value: '3', option: 'Other'}
          ]
        },
        {
          header: 'Number acquired',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Face value',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Acquisition cost per unit',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Adjustment',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Exchange rate',
          colClass: 'std-input-width',
          init: '1.0',
          decimals: 4
        },
        {
          header: 'Total value of property held',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Total acquisition cost of property held',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Average ACB for every $100',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Number disposed of',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Face value per asset disposed of',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'Proceeds of disposition',
          colClass: 'std-input-width',
          decimals: 4
        },
        {
          header: 'ACB of bonds disposed of',
          colClass: 'std-input-width',
          decimals: 6
        }
      ]
    }
  }
})();
