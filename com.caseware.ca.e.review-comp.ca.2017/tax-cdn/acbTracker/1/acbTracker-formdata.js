(function() {

  wpw.tax.create.formData('acbTracker', {
    formInfo: {
      abbreviation: 'acbTracker',
      isRepeatForm: true,
      repeatFormData: {
        titleNum: 'repeatName'
      },
      title: 'ACB Tracker - re. Schedule 4, 6 and T1135',
      showCorpInfo: true,
      dynamicFormWidth: true,
      category: 'Workcharts'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            type: 'infoField',
            label: 'Type of Asset',
            num: '100',
            init: '1',
            inputType: 'dropdown',
            options: [
              {value: '1', option: 'Shares'},
              {value: '2', option: 'Real estate'},
              {value: '3', option: 'Bonds'},
              {value: '4', option: 'Other properties'},
              {value: '5', option: 'Personal-use property'},
              {value: '6', option: 'Listed personal property'}
            ]
          },
          {labelClass: 'fullLength'},
          {
            label: 'Foreign',
            num: '109',
            type: 'infoField',
            inputType: 'singleCheckbox'
          },
          {
            type: 'table',
            num: '201',
            showWhen: {fieldId: '109', compare: {is: true}}
          }
        ]
      },
      {
        header: 'Asset Profile',
        showWhen: {fieldId: '100', compare: {is: 1}},
        rows: [
          {
            label: 'Name of corporation'
          },
          {
            type: 'infoField',
            num: '101',
            width: 'calc(62% - 81px)'
          },
          {labelClass: 'fullLength'},
          {label: 'Description/ Type of shares:', labelClass: 'bold'},
          {
            type: 'infoField',
            label: 'Class :',
            num: '102',
            width: '20%'
          },
          {
            type: 'infoField',
            label: 'Type of shares',
            num: '103',
            width: '20%'
          },
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {label: 'Summary', labelClass: 'bold'},
          {
            type: 'table', num: '200'
          },
          {labelClass: 'fullLength'},
          {
            type: 'splitTable', fieldAlignRight: true,
            side1: [
              {
                type: 'infoField',
                label: 'Maximum FMV during the year',
                num: '210'
              }
            ],
            side2: [
              {
                type: 'infoField',
                label: 'FMV at year-end',
                num: '211'
              }
            ]
          },
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {
            type: 'table', num: '300'
          },
          {labelClass: 'fullLength'}
        ]
      },
      {
        header: 'Asset Profile',
        showWhen: {
          or: [
            {fieldId: '100', compare: {is: 2}},
            {fieldId: '100', compare: {is: 4}},
            {fieldId: '100', compare: {is: 5}},
            {fieldId: '100', compare: {is: 6}}
          ]
        },
        rows: [
          {
            'label': 'Description'
          },
          {
            'type': 'infoField',
            'inputType': 'textArea',
            'num': '407'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            label: 'Municipal address of the property',
            inputType: 'none'
          },
          {
            'type': 'infoField',
            'inputType': 'address',
            'add1Num': '401',
            'add2Num': '402',
            'provNum': '403',
            'cityNum': '404',
            'countryNum': '405',
            'postalCodeNum': '406'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '410'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '420'
          },
          {
            'label': '<b>Adjusted cost base of the asset :</b>',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '425'
                }
              }
            ],
            'indicator': '$'
          }
        ]
      },
      {
        header: 'Asset Profile',
        showWhen: {fieldId: '100', compare: {is: 3}},
        rows: [
          {
            label: 'Name of bond issuer'
          },
          {
            type: 'infoField',
            num: '501',
            width: 'calc(62% - 81px)'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            label: 'Maturity date',
            num: '502',
            width: '20%',
            inputType: 'date'
          },
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {label: 'Summary', labelClass: 'bold'},
          {
            type: 'table', num: '505'
          },
          {labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {
            type: 'table', num: '510'
          },
          {labelClass: 'fullLength'}
        ]
      }
    ]
  });
})();
