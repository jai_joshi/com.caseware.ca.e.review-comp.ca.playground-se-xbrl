(function() {

  /**
   *This function make specific row disabled and remove vals of specific cols
   * @param row
   * @param cIndexs
   * @param cIndexsClearVals
   */
  function applyDisabledToCells(row, cIndexs, cIndexsClearVals) {
    //enable all cells
    row.forEach(function(cell) {
      cell.disabled(false);
    });
    //disable all needed cells
    cIndexs.forEach(function(colIndex) {
      row[colIndex].disabled(true);
      row[colIndex].config('cannotOverride', true);
    });
    //remove value from cells which applicable;
    cIndexsClearVals = cIndexsClearVals || [];
    cIndexsClearVals.forEach(function(colIndex) {
      row[colIndex].assign('');
      row[colIndex].config('cannotOverride', true);
    });
  }

  /**
   *check for earliest date and get the rowIndex of that row in order
   * create an json with properties: {rowIndex: dateValue}
   * @param dataObject
   * @returns {Array}
   */
  function getSortedData(dataObject) {
    var sortedData = [];
    for (var data in dataObject) {
      sortedData.push([data, dataObject[data]])
    }

    return sortedData.filter(function(a) {
      return angular.isDefined(a[1]);
    }).sort(function(a, b) {
      return wpw.tax.utilities.milliseconds(a[1]) - wpw.tax.utilities.milliseconds(b[1]);
    });
  }

  /**
   *update last transaction to summary table
   * @param field
   * @param sortedArray
   * @param tableRows
   * @param assetType
   */
  function updateSummaryTable(field, sortedArray, tableRows, assetType) {
    var summaryTableRowIndex = sortedArray[sortedArray.length - 1][0];
    var summaryTableRow = tableRows[summaryTableRowIndex];

    if (summaryTableRowIndex >= 0) {
      if (assetType === 'shares') {
        var table200Row = field('200').getRow(0);
        table200Row[0].assign(summaryTableRow[0].get());
        table200Row[1].assign(summaryTableRow[6].get());
        table200Row[2].assign(summaryTableRow[8].get());
        table200Row[3].assign(summaryTableRow[7].get());
      }
      else {
        var table505Row = field('505').getRow(0);
        table505Row[0].assign(summaryTableRow[0].get());
        table505Row[1].assign(summaryTableRow[7].get());
        table505Row[2].assign(summaryTableRow[8].get());
        table505Row[3].assign(summaryTableRow[9].get());
      }
    }
  }

  function shareAndBondCalcs(calcUtils, field) {
    var taxStart = field('CP.tax_start').get();
    var taxEnd = field('CP.tax_end').get();
    var dataObject = {};
    var currentYearDispositionRowIndex = [];

    var isShare = (field('100').get() === '1');
    var isBond = (field('100').get() === '3');

    if (isShare || isBond) {
      var tableNum;

      if (isShare) {
        tableNum = '300'
      }
      else if (isBond) {
        tableNum = '510'
      }

      var table = field(tableNum);
      var tableRows = table.getRows();
      tableRows.forEach(function(row, rIndex) {
        var transactionType = row[1].get();
        //depend on each transaction type, cells need to be disabled
        if (transactionType) {
          if (isShare) {
            if (transactionType == 1 || transactionType == 6) {
              applyDisabledToCells(row, [6, 7, 8, 9, 10, 11], [9, 10, 11])
            }
            else if (transactionType == 2 || transactionType == 7) {
              applyDisabledToCells(row, [2, 3, 4, 5, 6, 7, 8, 11], [2, 3, 4, 5])
            }
            else if (transactionType == 3 || transactionType == 4 || transactionType == 5) {
              applyDisabledToCells(row, [2, 3, 7, 8, 9, 10, 11], [2, 3, 9, 10, 11]);
            }
            else {
              applyDisabledToCells(row, [8]);
            }
          } else if (isBond) {
            if (transactionType == 1) {
              applyDisabledToCells(row, [7, 8, 9, 10, 11, 12, 13], [10, 11, 12, 13])
            }
            else if (transactionType == 2) {
              applyDisabledToCells(row, [2, 3, 4, 5, 6, 7, 8, 9, 13], [2, 3, 4, 5, 6])
            }
            else if (transactionType == 3) {
              applyDisabledToCells(row, [9]);
            }
          }
        }
        //get date data by transaction date
        var transDate = row[0].get();
        dataObject[rIndex] = transDate;
        //check if there is disposition in current fiscal year period then push in the rowIndex to be used in S6
        var isWithinFiscalYear = (calcUtils.dateCompare.between(transDate, taxStart, taxEnd));
        if (isWithinFiscalYear && transactionType == '2') {
          currentYearDispositionRowIndex.push(rIndex)
        }

        if (isShare) {
          field('sharesTransRIndex').assign(currentYearDispositionRowIndex);
        } else if (isBond) {
          field('bondTransRIndex').assign(currentYearDispositionRowIndex);
        }
      });
      //sort data by transaction date
      var sortedData = getSortedData(dataObject);
      //from there we will know what order the calcs should go
      var numberOfSharesHeld = 0;
      var totalCostOfShares = 0;
      var averageAcbShares = 0;

      var totalValueOfProperty = 0;
      var totalAcquisitionCost = 0;
      var averageAcb = 0;
      var adjCost;
      var exRate;

      //calculate based on sorted order
      sortedData.forEach(function(data) {
        var summaryRow = tableRows[data[0]];
        var transType = summaryRow[1].get();

        if (isShare) {
          var numberOfShareAcquired = summaryRow[2].get();
          var acqCost = summaryRow[3].get();
          adjCost = summaryRow[4].get();
          exRate = summaryRow[5].get();
          var numberOfShareDisposed = summaryRow[9].get();

          //for purchased && amalgamation
          if (transType == 1 || transType == 6) {
            summaryRow[6].assign(numberOfShareAcquired + numberOfSharesHeld);
            summaryRow[7].assign((acqCost + adjCost) * exRate + totalCostOfShares);
            summaryRow[8].assign(summaryRow[6].get() == 0 ? 0 : summaryRow[7].get() / summaryRow[6].get());
          }
          //for disposition && acquisition
          else if (transType == 2 || transType == 7) {
            summaryRow[11].assign(averageAcbShares * numberOfShareDisposed);
            summaryRow[6].assign(numberOfSharesHeld - numberOfShareDisposed);
            summaryRow[7].assign(totalCostOfShares - summaryRow[11].get());
            summaryRow[8].assign(summaryRow[6].get() == 0 ? 0 : summaryRow[7].get() / summaryRow[6].get());
          }
          //for split && Reverse stock split
          else if (transType == 3 || transType == 4) {
            summaryRow[7].assign(adjCost * exRate + totalCostOfShares);
            summaryRow[8].assign(summaryRow[6].get() == 0 ? 0 : summaryRow[7].get() / summaryRow[6].get());
          }
          //for Return of capital
          else if (transType == 5) {
            summaryRow[6].assign(numberOfSharesHeld);
            summaryRow[7].assign(adjCost * exRate + totalCostOfShares);
          }
          //for other
          else {
            summaryRow[8].assign(summaryRow[6].get() == 0 ? 0 : summaryRow[7].get() / summaryRow[6].get());
          }
          numberOfSharesHeld = summaryRow[6].get();
          totalCostOfShares = summaryRow[7].get();
          averageAcbShares = summaryRow[8].get();
        }
        else if (isBond) {
          var numberAcq = summaryRow[2].get();
          var faceValue = summaryRow[3].get();
          var acqCostPerUnit = summaryRow[4].get();
          adjCost = summaryRow[5].get();
          exRate = summaryRow[6].get();
          var numberDis = summaryRow[10].get();
          var faceValueDis = summaryRow[11].get();

          //for purchased
          if (transType == 1) {
            summaryRow[7].assign(numberAcq * faceValue + totalValueOfProperty);
            summaryRow[8].assign((numberAcq * acqCostPerUnit + adjCost) * exRate + totalAcquisitionCost);
            summaryRow[9].assign(summaryRow[7].get() == 0 ? 0 : summaryRow[8].get() / summaryRow[7].get() * 100);
          }
          //for disposition
          else if (transType == 2) {
            summaryRow[7].assign(totalValueOfProperty);
            summaryRow[8].assign(totalValueOfProperty * averageAcb / 100);
            summaryRow[9].assign(averageAcb);
            if (numberDis == 0 || faceValueDis == 0) {
              summaryRow[13].assign(0);
            }
            else {
              summaryRow[13].assign(numberDis * averageAcb / 100 * faceValueDis);
            }
            summaryRow[7].assign(totalValueOfProperty - (numberDis * faceValueDis));
            summaryRow[8].assign(summaryRow[7].get() * averageAcb / 100);
          }
          //for other
          else {
            summaryRow[9].assign(summaryRow[7].get() == 0 ? 0 : summaryRow[8].get() / summaryRow[7].get() * 100);
          }
          totalValueOfProperty = summaryRow[7].get();
          totalAcquisitionCost = summaryRow[8].get();
          averageAcb = summaryRow[9].get();
        }
      });
      //update to summary table
      if (sortedData.length > 0) {
        if (isShare) {
          updateSummaryTable(field, sortedData, tableRows, 'shares');
        }
        else if (isBond) {
          updateSummaryTable(field, sortedData, tableRows, 'bonds');
        }
      }
    }
  }

  wpw.tax.create.calcBlocks('acbTracker', function(calcUtils) {
    //calcs for repeated form heading
    calcUtils.calc(function(calcUtils, field, form) {
      var assetNameCategories = {
        '0': {name: '-- Asset Type Not Selected --', desc: 'undefined'},
        '1': {name: 'Shares', desc: '101'},
        '2': {name: 'Real Estate', desc: '407'},
        '3': {name: 'Bonds', desc: '501'},
        '4': {name: 'Other', desc: '407'},
        '5': {name: 'PUP', desc: '407'},
        '6': {name: 'LPP', desc: '407'},
        '7': {name: 'ABILs', desc: '407'}
      };

      var repeatFormName = assetNameCategories[field('100').get()].name;
      var repeatFormDesc = field(assetNameCategories[field('100').get()].desc).get();
      if (repeatFormDesc != '' && angular.isDefined(repeatFormDesc)) {
        repeatFormName = repeatFormName + ' - ' + repeatFormDesc;
      }

      field('repeatName').assign(repeatFormName);
    });

    // calcs for real estate
    calcUtils.calc(function(calcUtils, field, form) {
      field('425').assign(
          field('420').total(2).get() +
          field('410').cell(0, 1).get());
    });
    // calcs for shares
    // calcs for bonds
    //todo: section 54 of ITA error if the purchase date and disposition date is within 30 days or less(diagnostic)
    calcUtils.calc(function(calcUtils, field, form) {
      shareAndBondCalcs(calcUtils, field)
    });
  });
})();
