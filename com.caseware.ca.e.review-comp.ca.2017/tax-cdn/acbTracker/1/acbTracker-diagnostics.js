(function() {
  wpw.tax.create.diagnostics('acbTracker', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    function getSortedDates(tableRows) {
      var dates = [];
      for (var i = 0; i < tableRows.length; i++) {
        dates.push([i, tableRows[i][0].get()])
      }
      dates.sort(function(a, b) {
        return wpw.tax.utilities.milliseconds(a[1]) - wpw.tax.utilities.milliseconds(b[1]);
      });
      return dates;
    }

    diagUtils.diagnostic('ACB.30Day', function(tools) {
      var table = tools.field('100').get() == '1' ? tools.field('300') : tools.field('510');
      var dates = getSortedDates(table.getRows());
      return tools.checkAll(dates, function(date, index) {
        var nextDate = dates[index + 1];
        if (nextDate && wpw.tax.actions.calculateDaysDifference(date[1], nextDate[1]) <= 31 &&
            table.cell(date[0], 1).get() == '2' &&
            table.cell(nextDate[0], 1).get() == '1' &&
            (table.fieldPath == '300' ?
                table.cell(date[0], 11).get() > table.cell(date[0], 10).get() :
                table.cell(date[0], 13).get() > table.cell(date[0], 12).get())) {
          table.cell(date[0], 0).mark();
          table.cell(nextDate[0], 0).mark();
          return false;
        } else return true;
      })
    });

  });
})();
