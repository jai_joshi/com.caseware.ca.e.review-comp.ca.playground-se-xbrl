(function() {
  wpw.tax.create.diagnostics('t2s380', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3800001', common.prereq(common.check(['t2s5.606', 't2s5.613'], 'isNonZero'), common.requireFiled('T2S380')));

    diagUtils.diagnostic('3800003', common.prereq(common.and(
        common.check(['901', '902', '903'], 'isNonZero'),
        common.requireFiled('T2S380')),
        common.check(['121', '123', '124', '126', '130', '140'], 'isNonZero')));

    diagUtils.diagnostic('380.119', common.prereq(common.requireFiled('T2S380'),
        function(tools) {
          return tools.field('119').require(function() {
            return this.get() <= (tools.field('106').get() + tools.field('107').get());
          })
        }));

  });
})();
