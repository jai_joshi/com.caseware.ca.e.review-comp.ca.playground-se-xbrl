(function() {
  var getTableColumns = [
    {
      'type': 'none',
      cellClass: 'alignLeft'
    },
    {
      colClass: 'std-input-width',
      formField: true
    },
    {
      colClass: 'std-padding-width',
      'type': 'none'
    },
    {
      colClass: 'small-input-width',
      decimals: 1
    },
    {
      colClass: 'std-padding-width',
      'type': 'none'
    },
    {
      colClass: 'std-padding-width',
      'type': 'none'
    },
    {
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-padding-width',
      'type': 'none'
    },
    {
      colClass: 'std-input-width',
      'type': 'none'
    },
    {
      colClass: 'std-padding-width',
      'type': 'none'
    }
  ];
  function getTableCells(label1, tnNumber1, fieldId1, label2, tnNumber2, fieldId2) {
    return [
      {
        0: {
          'label': label1
        },
        2: {
          'label': 'x'
        },
        4: {
          label: '%='
        },
        5:{
          tn: tnNumber1
        },
        6:{
          num: fieldId1,
          formField: true
        }
      },
      {
        0: {
          'label': label2
        },
        2: {
          'label': 'x'
        },
        4: {
          label: '%='
        },
        5:{
          tn: tnNumber2
        },
        6:{
          num: fieldId2,
          formField: true
        }
      }
    ]
  }
  wpw.tax.create.tables('t2s380', {
    '1000': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          'type': 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width',
          formField: true
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'qtr-col-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          formField: true
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          0:{
            label: 'Repayments** for assistance received before April 12, 2017'
          },
          2:{
            label: 'x '
          },
          4:{
            label: '% ='
          },
          6:{
            tn: '123'
          },
          7:{
            num: '123'
          }
        },
        {
          0:{
            label: 'Repayments** for assistance received after April 11, 2017'
          },
          2:{
            label: 'x '
          },
          4:{
            label: '% ='
          },
          6:{
            tn: '124'
          },
          7:{
            num: '124'
          }
        }
      ]
    },
    '1100': {
      'fixedRows': true,
      'infoTable': true,
      'columns': getTableColumns,
      'cells': getTableCells('Line 106 in Part 1', '121', '121', 'Line 107 in Part 1', '126', '126')
    },
    '1200': {
      'fixedRows': true,
      hasTotals: true,
      'infoTable': true,
      'columns': [
        {
          'width': '190px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-col-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'formField': true,
          'total': true,
          'totalNum': '904',
          'totalMessage': 'Total credit to be applied (enter at amount F in Part 2)'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        }],
      'cells': [
        {
          '0': {
            'label': '1st previous tax year '
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '901'
          },
          '5': {
            'num': '901',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '2nd previous tax year '
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '902'
          },
          '5': {
            'num': '902'
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '3rd previous tax year '
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '903'
          },
          '5': {
            'num': '903', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }]
    },
    '1300': {
      'fixedRows': true,
      hasTotals: true,
      'infoTable': true,
      'columns': [
        {
          type: 'none'
        },
        {
          header: 'Year of origin <br>(earliest year first)',
          'colClass': 'std-input-col-width',
          'type': 'date',
          'disabled': true
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Credit available',
          colClass: 'std-input-width',
          total: true,
          totalNum: '1301',
          totalMessage: ' '
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      'cells': [
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {}
      ]
    },
    '1400': {
      'fixedRows': true,
      'infoTable': true,
      hasTotals: true,
      'columns': [
        {
          type: 'none'
        },
        {
          header: 'Year of origin<br> (earliest year first)',
          'colClass': 'std-input-col-width',
          'type': 'date',
          'disabled': true
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Credit available',
          colClass: 'std-input-width',
          totalNum: '1401',
          total: true,
          totalMessage: ' '
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      'cells': [
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {}
      ]
    },
    '1500': {
      'fixedRows': true,
      'infoTable': true,
      'columns': getTableColumns,
      'cells': getTableCells('Line 108 in Part 1', '205', '205', 'Line 116 in Part 1', '215', '215')
    },
    '1550': {
      'fixedRows': true,
      'infoTable': true,
      'columns': getTableColumns,
      'cells': getTableCells('Line 109 in Part 1', '206', '206', 'Line 117 in Part 1', '216', '216')
    },
    '1600': {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {colClass: 'std-padding-width', type: 'none'},
        {
          colClass: 'std-input-width',
          type: 'date',
          disabled: true,
          header: '<b>Year of origin</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '1601',
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '502',
          totalMessage: ' ',
          header: '<b>Credit available for current year</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '503',
          totalMessage: ' ',
          header: '<b>Transfer amount</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', totalNum: '504',
          header: '<b>Amount available to apply</b>',
          formField: true,
          disabled: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '505',
          header: '<b>Applied<b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '506',
          header: '<b>Balance to carry forward</b>',
          formField: true

        },
        {colClass: 'std-padding-width', type: 'none'}
      ],
      cells: [
        {
          '4': {label: '*'},
          '5': {type: 'none'},
          '7': {type: 'none'},
          '9': {type: 'none'},
          '11': {type: 'none'},
          '13': {type: 'none', label: '     N/A'}
        },
        {
          '5': {type: 'none'},
          '14': {label: '**'}
        },
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {
          '3': {type: 'none'},
          '7': {type: 'none'}
        }
      ]
    }
  })
})();
