(function() {

  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  function runHistoricalTableCalc(calcUtils, tableObj, carryBackAmount, limitOnCreditAmount) {
    var tableNum = tableObj.tableNum;
    var openingBalanceCindex = tableObj.openingBalanceCindex;
    var currentYearCindex = tableObj.currentYearCindex;
    var transferCindex = tableObj.transferCindex;
    var availableCindex = tableObj.availableCindex;
    var appliedCindex = tableObj.appliedCindex;
    var endingBalanceCindex = tableObj.endingBalanceCindex;

    var summaryTable = calcUtils.field(tableNum);
    var numRows = summaryTable.size().rows;

    summaryTable.getRows().forEach(function(row, rowIndex) {
      // if (rowIndex == numRows - 1) {
      //   if (carryBackAmount > 0) {
      //     summaryTable.cell(21, 11).assign(carryBackAmount);
      //     row[endingBalanceCindex].assign(Math.max(row[availableCindex].get() - row[appliedCindex].get(), 0))
      //   }
      //   else {
      //     summaryTable.cell(21, 11).assign(0)
      //   }
      // }
      // else {
        if (rowIndex == 0) {
          // to avoid the first row being calculated to total
          row[currentYearCindex].assign(0);
          row[appliedCindex].assign(0);
          row[transferCindex].assign(0);
          row[availableCindex].assign(0);
          row[endingBalanceCindex].assign(0);
        } else {
          row[appliedCindex].assign(returnAmountApplied(limitOnCreditAmount, row[availableCindex].get()));
          row[availableCindex].assign(row[openingBalanceCindex].get() + row[currentYearCindex].get() + row[transferCindex].get());
          row[endingBalanceCindex].assign(Math.max(row[availableCindex].get() - row[appliedCindex].get(), 0));
          limitOnCreditAmount -= row[appliedCindex].get();
        }
      // }
    });
  }

  function runTableMultiplication(calcUtils, tableNum, result1, result2) {
    var field = calcUtils.field;
    field(result1).assign(
        field(tableNum).cell(0, 1).get() * field(tableNum).cell(0, 3).get() / 100
    );
    field(result2).assign(
        field(tableNum).cell(1, 1).get() * field(tableNum).cell(1, 3).get() / 100
    )
  }

  function getPart5Values(calcUtils, tableNum, sourceField1, sourceField2) {
    var field = calcUtils.field;
    field(tableNum).cell(0, 1).assign(field(sourceField1).get());
    field(tableNum).cell(0, 1).source(field(sourceField1));
    field(tableNum).cell(1, 1).assign(field(sourceField2).get());
    field(tableNum).cell(1, 1).source(field(sourceField2));
  }

  function getRatesMb(calcUtils, tableNum, sourceField1, sourceField2) {
    var field = calcUtils.field;
    field(tableNum).cell(0, 3).assign(field(sourceField1).get());
    field(tableNum).cell(0, 3).source(field(sourceField1));
    field(tableNum).cell(1, 3).assign(field(sourceField2).get());
    field(tableNum).cell(1, 3).source(field(sourceField2));
  }

  wpw.tax.create.calcBlocks('t2s380', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //update rates from ratesMb
      getRatesMb(calcUtils, '1000', 'ratesMb.145', 'ratesMb.1451');
      getRatesMb(calcUtils, '1100', 'ratesMb.145', 'ratesMb.1451');
      getRatesMb(calcUtils, '1500', 'ratesMb.146', 'ratesMb.147');
      getRatesMb(calcUtils, '1550', 'ratesMb.148', 'ratesMb.1481');

      //part1 calcs
      calcUtils.sumBucketValues('106', ['108', '116']);
      calcUtils.sumBucketValues('107', ['109', '117']);
      calcUtils.sumBucketValues('118', ['108', '116', '109', '117']);
      calcUtils.equals('119', '118');
    });

    calcUtils.calc(function(calcUtils, field) {
      //part2
      calcUtils.getGlobalValue('120', 'T2S380', '1601');
      field('104').assign(field('1600').cell(0, 3).get());
      calcUtils.subtract('105', '120', '104');
      calcUtils.equals('1051', '105');
      field('110').assign(field('1600').total(7).get());
      field('1100').cell(0, 1).assign(field('106').get());
      field('1100').cell(1, 1).assign(field('107').get());
      runTableMultiplication(calcUtils, '1000', '123', '124');
      runTableMultiplication(calcUtils, '1100', '121', '126');
      calcUtils.sumBucketValues('141', ['121', '130', '140', '126']);
      field('151').assign(field('141').get() - field('150').get());
      field('152').assign(field('151').get());
      calcUtils.sumBucketValues('153', ['1051', '110', '123', '124', '152']);
      field('160').assign(Math.min(Math.max(field('153').get() - field('181').get(), 0), field('T2S383.301').get()));
      field('180').assign(Math.max(field('248').get(), 0));
      field('181').assign(field('904').get());
      calcUtils.sumBucketValues('182', ['160', '180', '181']);
      field('183').assign(field('182').get());
      field('200').assign(Math.max(field('153').get() - field('183').get(), 0));
    });
    calcUtils.calc(function(calcUtils, field) {
      //part3
      //to get year for table summary
      var tableArray = [1200, 1300, 1400];
      var taxationYearTable = field('tyh.200');
      tableArray.forEach(function(num) {
        field(num).getRows().forEach(function(row, rowIndex) {
          if (num == 1200) {
            var dateCol = Math.abs(rowIndex - 19);
            row[1].assign(taxationYearTable.cell(dateCol, 6).get());
          }
          else if (num == 1300) {
            row[1].assign(taxationYearTable.getRow(rowIndex)[6].get())
          }
          else {
            row[1].assign(taxationYearTable.cell(rowIndex + 10, 6).get())
          }
        })
      });
      //table1600 has 21 years while tyh supports 20 years back
      var table1600 = field('1600');
      var dateCol = taxationYearTable.cell(0, 6).get();
      var taxEnd = field('CP.tax_end').get();
      //check if 20th years value defined on tyh, if not take cp.tax_end-21
      table1600.cell(0, 1).assign(
          !dateCol ? (!taxEnd ? null : calcUtils.addToDate(taxEnd, -21, 0, 0)) : calcUtils.addToDate(dateCol, -1, 0, 0));
      for (var i = 1; i < table1600.size().rows; i++) {
        table1600.cell(i, 1).assign(taxationYearTable.getRow(i - 1)[6].get());
      }
    });
    calcUtils.calc(function(calcUtils, field, form) {
      //part4
      var summaryTable = field('1600');
      field('1300').getRows().forEach(function(row, rowIndex) {
        var table1300SummaryTablerIndex = rowIndex + 1;
        row[3].assign(summaryTable.getRow(table1300SummaryTablerIndex)[13].get());
      });
      field('1400').getRows().forEach(function(row, rowIndex) {
        var table1400SummaryTablerIndex = rowIndex + 11;
        row[3].assign(summaryTable.getRow(table1400SummaryTablerIndex)[13].get());
      });
      field('400').assign(field('1301').get() + field('1401').get());
    });
    calcUtils.calc(function(calcUtils, field) {
      //part 5
      getPart5Values(calcUtils, '1500', '108', '116');
      getPart5Values(calcUtils, '1550', '109', '117');
      runTableMultiplication(calcUtils, '1500', '205', '215');
      runTableMultiplication(calcUtils, '1550', '206', '216');
      calcUtils.sumBucketValues('241', ['205', '215', '230', '240', '206', '216']);
      calcUtils.equals('242', '241');
      field('243').assign(field('153').get());
      calcUtils.getGlobalValue('244', 'T2S5', '230');
      field('245').assign(Math.max(field('243').get() - field('244').get(), 0));
      field('246').assign(field('245').get());
      field('247').assign(field('152').get());
      field('248').assign(Math.min(field('242').get(), field('246').get(), field('247').get()));
      field('180').assign(field('248').get());

      //historical data table
      var summaryTable = field('1600');
      var carryBackAmount = field('1200').get().totals[5];
      var limitOnCreditAmount = field('183').get();
      runHistoricalTableCalc(
          calcUtils,
          {
            tableNum: '1600',
            openingBalanceCindex: 3,
            currentYearCindex: 5,
            transferCindex: 7,
            availableCindex: 9,
            appliedCindex: 11,
            endingBalanceCindex: 13
          },
          carryBackAmount,
          limitOnCreditAmount
      );
      summaryTable.cell(21, 5).assign(field('123').get() + field('124').get() + field('141').get());
    });
  });
})();
