(function() {
  wpw.tax.create.formData('t2s380', {
    'formInfo': {
      'abbreviation': 't2s380',
      'title': 'Manitoba Research and Development Tax Credit',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      //'subTitle': ' (2015 and later tax years)',
      'schedule': 'Schedule 380',
      'showCorpInfo': true,
      code: 'Code 1701',
      formFooterNum: 'T2 SCH 380 E (17)',
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if you are a corporation with a permanent establishment in Manitoba that has' +
              ' made eligible expenditures for scientific research and experimental development carried out ' +
              'in the province under section 7.3 of the <i>Income Tax Act</i> (Manitoba), and you want to:',
              labelClass: 'fullLength',
              sublist: [
                'calculate a Manitoba research and development (R&D) tax credit for the current tax year;',
                'calculate the refundable component of the Manitoba R&D tax credit;',
                'claim the credit to reduce Manitoba income tax otherwise payable in the current tax year;',
                'carry back the current-year credit to reduce Manitoba income tax otherwise payable in any of the' +
                ' three previous tax years;',
                'carry forward the current-year credit to reduce Manitoba income tax otherwise payable in any ' +
                'of the 20 following tax years after 2005; or',
                'renounce the current-year credit in whole or in part. If the renunciation is filed by the ' +
                'filing due date for the year, the corporation is deemed never to have received, been entitled to ' +
                'receive or had a reasonable expectation of receiving the amount. If the renunciation is filed within' +
                ' the 365-day period immediately following the filing-due date, the corporation is deemed never ' +
                'to have received, been entitled to receive or had a reasonable expectation of receiving ' +
                'the amount for all purposes ' +
                '<b>except</b> for paragraph 37(1)(d) and subsections 127(18) to (20) of the federal ' +
                '<i>Income Tax Act</i>.',
                'to claim a credit transfer after an amalgamation or the windup of a subsidiary, as described in' +
                ' subsections 87(1) and 88(1) of the federal <i>Income Tax Act</i>;',
                'to claim the share of a credit allocated to you as a member of a partnership or as the beneficiary ' +
                'of a trust.'
              ]
            },
            {
              label: 'The Manitoba refundable research and development tax credit earned under subsection' +
              ' 7.3(2.3) of the <i>Income Tax Act</i> (Manitoba), is the least of:',
              labelClass: 'fullLength',
              sublist: [
                'the amount by which the R&D tax credit available at the end of the current tax ' +
                'year exceeds the total of Manitoba income tax otherwise payable in the current tax year and the amount ' +
                'renounced under subsection 7.3(7) for the year;',
                'the total of the following amounts in the current tax year:',
                '15% of the corporation\'s eligible expenditures incurred in the current tax year after' +
                ' April 11, 2017 and under a R&D contract with a university, college, or other' +
                ' post-secondary educational institution in Manitoba or with a person approved for this' +
                ' purpose by the Minister of Growth, Enterprise and Trade. For a' +
                ' list of educational institutions potentially eligible to participate in the SR&ED refundable' +
                ' Manitoba R&D tax credit program, go to ' +
                '<b>gov.mb.ca/finance/business/pubs/eligible_institutions.pdf;</b>'.link('http://www.http://gov.mb.ca/finance/business/pubs/eligible_institutions.pdf'),
                '7.5% of the eligible expenditures not under a R&D contract with a qualifying institution incurred after April 11, 2017;',
                '20% of expenditures under a R&D contract with a qualifying institution incurred before April 12, 2017;',
                '10% of eligible expenditures not under a R&D contract with a qualifying institution before April 12, 2017; and',
                'the refundable credit allocated to the corporation that is a member of a partnership or a beneficiary under a trust.',
                'the total credit earned in the current tax year.'
              ]
            },
            {
              label: 'An eligible expenditure must meet the definition of a <b>qualified expenditure</b> ' +
              'in subsection 127(9), but without reference to paragraph (d) of the federal <i>Income Tax Act</i>. ' +
              'The capital cost of a qualified expenditure is determined without reference to subsection 13(7.1) ' +
              'of the Act. Only qualified expenditures incurred in Manitoba are eligible for the ' +
              'Manitoba R&D tax credit.',
              labelClass: 'fullLength'
            },
            {
              label: 'This schedule must be filed no later than 12 months after the T2 Corporation ' +
              'Income Tax Return is due for the tax year that the expenditure was incurred in.',
              labelClass: 'fullLength'
            },
            {
              label: 'File this schedule with your T2 return.'
            }
          ]
        }
      ],
      category: 'Manitoba Forms'
    },
    'sections': [
      {
        'header': 'Part 1 - Summary of total eligible expenditures incurred in the current tax year',
        'rows': [
          {
            'label': 'Total eligible expenditures incurred in the current tax year before April 12, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '106',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '106'
                }
              }
            ]
          },
          {
            'label': 'Total eligible expenditures incurred in the current tax year after April 11, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '107',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '107'
                }
              }
            ]
          },
          {
            'label': 'Eligible expenditures under a R&D contract incurred in the current tax year before April 12, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '108',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '108'
                }
              },
              null
            ]
          },
          {
            'label': 'Eligible expenditures not under a R&D contract incurred in the current tax year before' +
            '<br> April 12, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '116',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '116'
                }
              },
              null
            ]
          },
          {
            'label': 'Eligible expenditures under a R&D contract incurred in the current tax year after April 11, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '109',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '109'
                }
              },
              null
            ]
          },
          {
            'label': 'Eligible expenditures not under a R&D contract incurred in the current tax year after April 11, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '117',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '117'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Summary of total eligible expenditures incurred in the current tax year</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '118'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '119'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            label: '(<b>add</b> lines 108, 116, 109, and 117)<br>' +
            'Amount A cannot exceed the total of line 106 <b>plus</b> 107.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        forceBreakAfter: true,
        'header': 'Part 2 - Total credit available and credit available for carryforward',
        'rows': [
          {
            'label': 'Credit at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '120'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Credit expired*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit at the beginning of the tax year (amount B <b>minus</b> line 104)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '1051',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Credit transferred on an amalgamation or the windup of a subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Credit earned in the current year:',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'num': '1100'
          },
          {
            'label': 'Credit allocated to the corporation that is a member of a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit allocated to the corporation that is a beneficiary under a trust',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '140',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '140'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> lines 121,126, 130, and 140)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '141'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit renounced',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '150'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Total credit earned in the current year</b> (amount C <b>minus</b> amount 150)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '151'
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '152'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Total credit available for the current year (<b>add</b> lines 105, 110, 123, 124, and amount D)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '153'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Non-refundable credit claimed in the current tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'single',
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              },
              null
            ]
          },
          {
            'label': '(cannot exceed the lesser of amount E and Manitoba tax otherwise payable) <br>' +
            'Enter on line 606 of Schedule 5, <i>Tax Calculation Supplementary – Corporations</i>',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Refundable credit claimed in the current year (amount M from Part 5) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '180',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '180'
                }
              },
              null
            ]
          },
          {
            label: 'Enter on line 613 of Schedule 5.'
          },
          {
            'label': 'Credit carried back to the previous tax years (complete Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'single',
                'input': {
                  'num': '181'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> lines 160, 180, and amount F)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '182'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '183'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': '<b>Closing balance</b> - Total credit available for carryforward (amount E <b>minus</b> amount G)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ]
          },
          {
            'label': '* An unused credit expires after 20 tax years if it was earned in a tax year ending after 2005.'
          },
          {
            'label': '** Repayments must relate to a repayment made by the corporation in the tax year and' +
            ' not in any other tax year. Repayments are the sum of the following:'
          },
          {
            'label': '– a repayment made in the tax year of government or non-government assistance or a contract ' +
            'payment that reduced an eligible expenditure other than for first term or second term ' +
            'shared-use equipment; and',
            'labelClass': 'tabbed'
          },
          {
            'label': '– a repayment made in the tax year of government or non-government assistance or a ' +
            'contract payment that reduced an eligible expenditure for first term or second term ' +
            'shared-use equipment, <b>multiplied</b> by 1/4.',
            'labelClass': 'tabbed'
          }
        ]
      },
      {
        'header': 'Part 3 - Request for carryback of credit',
        'rows': [
          {
            'type': 'table',
            'num': '1200'
          }
        ]
      },
      {
        'header': 'Part 4 - Analysis of credit available for carryforward by year of origin',
        'rows': [
          {
            'label': 'For tax years ending after 2005, the carryforward period is 20 years.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'splitTable',
            'side1': [
              {
                'type': 'table',
                'num': '1300'
              }
            ],
            'side2': [
              {
                'type': 'table',
                'num': '1400'
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Total credit available for carryforward</b> (equals line 200 in Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '400'
                }
              }
            ]
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Part 5 - Refundable Manitoba R&D tax credit',
        'rows': [
          {
            'label': 'Current-year refundable credit earned:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1500'
          },
          {
            type: 'table',
            num: '1550'
          },
          {
            'label': 'Refundable credit allocated to the corporation that is a member of a partnership*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '230',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '230'
                }
              },
              null
            ]
          },
          {
            'label': 'Refundable credit allocated to the corporation that is a beneficiary under a trust*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '240',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '240'
                }
              },
              null
            ]
          },
          {
            'label': 'Current-year refundable credit earned (<b>add</b> lines 205, 215, 206, 216, 230, and 240)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '241'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '242'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': 'Total credit available for the current year (amount E from Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '243'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'I'
                }
              }
            ]
          },
          {
            'label': 'Manitoba tax otherwise payable before refundable tax credits, excluding the' +
            ' non-refundable R&D tax credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '244'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'J'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount I <b>minus</b> amount J)',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '245'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '246'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': 'Total credit earned in the current year (amount D from Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '247'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': '<b>Refundable Manitoba R&D tax credit</b> (amount H, K, or L, whichever is the least) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '248'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            label: 'Enter on line 180 in Part 2.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Enter the part of the credit from line 130 and 140 in Part 2 that is refundable and ' +
            'relates to eligible expenditures incurred in the tax year. This is the refundable credit that' +
            ' is calculated based on eligible expenditures allocated to a corporation that is a member' +
            ' of a partnership or a beneficiary under a trust.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for Manitoba R&E credits',
        'rows': [
          {
            label: 'For the non-refundable tax credits, the carry­back period is 3 years and' +
            ' the carry­forward period is 20 years for tax years ended after 2005.',
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'type': 'table',
            'num': '1600'
          },
          {
            'label': '* Expired',
            labelClass: 'fullLength'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  });
})();
