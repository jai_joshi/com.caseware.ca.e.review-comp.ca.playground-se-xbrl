(function() {

  function getTablecolD(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          'type': 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          formField: true
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          formField: true
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          formField: true
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '2': {num: labelsObj.num[0]},
          '3': {
            'label': 'x',
            cellClass: 'alignCenter'
          },
          '4': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '5': {
            'label': '='
          },
          '6': {
            num: labelsObj.num[2]
          },
          '7': {
            label: labelsObj.indicator || ''
          }
        },
        {
          '2': {
            'type': 'none'
          },
          '4': {
            num: labelsObj.num[3]
          },
          '6': {
            'type': 'none'
          }
        }
      ]
    }
  }

  function getTableTaxInc(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {type: 'none', colClass: 'std-padding-width'},
        {
          colClass: 'std-input-width',
          formField: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {

          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          formField: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          formField: true
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '2': {num: labelsObj.num[0]},
          '3': {label: 'x'},
          '4': {
            label: labelsObj.label[1],
            labelClass: 'center',
            cellClass: 'singleUnderline'
          },
          '6': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '7': {label: '='},
          '8': {num: labelsObj.num[2]},
          '9': {label: labelsObj.indicator || ''}
        },
        {
          '2': {type: 'none'},
          '4': {
            label: labelsObj.label[2],
            labelClass: 'center fullLength'
          },
          '6': {num: labelsObj.num[3]},
          '8': {type: 'none'}
        }
      ]
    }
  }

  wpw.tax.create.tables('t2s383', {
    '1000': getTablecolD({
      label: ['Amount from line 427 of the T2 return'],
      indicator: 'D1',
      num: ['103', '104', '106', '105']
    }),
    '1001': getTablecolD({
      label: ['Amount from line 427 of the T2 return'],
      indicator: 'D2',
      num: ['153', '154', '156', '155']
    }),
    '2000': getTableTaxInc({
      label: ['Amount G1', 'Taxable income for Manitoba * ', 'Taxable income for all provinces ***'],
      indicator: 'H1',
      num: ['113', '114', '115', '116']
    }),
    '2001': getTableTaxInc({
      label: ['Amount G2', 'Taxable income for Manitoba * ', 'Taxable income for all provinces ***'],
      indicator: 'H2',
      num: ['163', '164', '165', '166']
    }),
    '3000': {
      hasTotals: true,
      fixedRows: true,
      paddingRight: 'std-input-col-padding-width-2',
      'showNumbering': true,
      'columns': [
        {
          'header': 'L1 <br> Amounts from column F1 in Part 3 of Schedule 7',
          disabled: true
        },
        {
          'header': 'M1 <br> Amounts from column H1 in Part 3 of Schedule 7 <b>multiplied</b> by 425,000/ 500,000',
          disabled: true
        },
        {
          'header': 'N1 <br> Column L1 <b>minus</b> column M1 (if negative, enter "0")',
          'total': true,
          'totalNum': '3500',
          totalMessage: 'Totals',
          totalIndicator: 'P1',
          disabled: true
        },
        {
          'header': 'O1 <br> Lesser of columns L1and M1(if column L1 is negative,enter "0")',
          'total': true,
          'totalNum': '3501',
          totalIndicator: 'Q1',
          disabled: true
        }
      ]
    },
    '3001': {
      hasTotals: true,
      fixedRows: true,
      paddingRight: 'std-input-col-padding-width-2',
      'showNumbering': true,
      'columns': [
        {
          'header': 'L2 <br> Amounts from column F1 in Part 3 of Schedule 7',
          disabled: true
        },
        {
          'header': 'M2 <br> Amounts from column H1 in Part 3 of Schedule 7 <b>multiplied</b> by 425,000/ 500,000',
          disabled: true
        },
        {
          'header': 'N2 <br> Column L2 <b>minus</b> column M2 (if negative, enter "0")',
          'total': true,
          'totalNum': '3502',
          totalMessage: 'Totals',
          totalIndicator: 'P2',
          disabled: true
        },
        {
          'header': 'O2 <br> Lesser of columns L2 and M2(if column L2 is negative,enter "0")',
          'total': true,
          'totalNum': '3503',
          totalIndicator: 'Q2',
          disabled: true
        }]
    },
    '4000': getTableTaxInc({
      label: ['Amount I1', 'Number of days in the tax year before January 1, 2016', 'Number of days in the tax year'],
      indicator: '1',
      num: ['4001', '4002', '4003', '4004']
    }),
    '4010': getTableTaxInc({
      label: ['Amount I2', 'Number of days in the tax year after December 31, 2015', 'Number of days in the tax year'],
      indicator: '2',
      num: ['4200', '4201', '4202', '4203']
    })
  })

})
();
