(function() {

  function runSch7TableCalcs(field, tableNum) {
    var s7Table2000 = field('T2S7.2000');
    field(tableNum).getRows().forEach(function(row, rIndex) {
      row[0].assign(s7Table2000.cell(rIndex, 1).get());
      row[1].assign(s7Table2000.cell(rIndex, 5).get() * (field('ratesMb.904').get() / field('ratesMb.128').get()));
      row[2].assign(Math.max(row[0].get() - row[1].get(), 0));
      row[3].assign(row[0].get() < 0 ? 0 :
          Math.min(row[0].get(), row[1].get())
      );
    });
  }

  wpw.tax.create.calcBlocks('t2s383', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      //rates & globalValue update
      field('104').assign(field('ratesMb.1009').cell(0, 2).get());
      field('104').source(field('ratesMb.1009').cell(0, 2));
      field('105').assign(field('ratesMb.128').get());
      field('105').source(field('ratesMb.128'));
      field('154').assign(field('ratesMb.1009').cell(1, 2).get());
      field('154').source(field('ratesMb.1009').cell(1, 2));
      field('155').assign(field('ratesMb.128').get());
      field('155').source(field('ratesMb.128'));
      //update ratesMb values
      calcUtils.getGlobalValue('102', 'T2J', '405');
      calcUtils.getGlobalValue('103', 'T2J', '427');

      calcUtils.getGlobalValue('152', 'T2J', '405');
      calcUtils.getGlobalValue('153', 'T2J', '427');

      //part2 should be eligible when a corp is a member of a partnership
      var isPartnershipMember = (field('CP.067').get() == 1 || field('T2S7.350').get() > 0);

      //check for partnership member
      field('101').assign(isPartnershipMember ? field('213').get() : field('T2J.400').get());
      field('151').assign(isPartnershipMember ? field('263').get() : field('T2J.400').get());

      // check for jurisdiction
      var taxableIncomeAllocation = calcUtils.getTaxableIncomeAllocation('MB');
      field('100').assign(taxableIncomeAllocation.provincialTI);
      field('116').assign(taxableIncomeAllocation.allProvincesTI);

      field('100').source(taxableIncomeAllocation.provincialTISourceField);
      field('116').source(taxableIncomeAllocation.allProvincesTISourceField);

      field('114').assign(field('100').get());
      field('150').assign(field('100').get());
      field('164').assign(field('100').get());

      field('166').assign(field('116').get());

      //period before jan 1,2016
      field('106').assign(field('105').get() == 0 ? 0 : field('103').get() * field('104').get() / field('105').get());
      field('107').assign(Math.min(field('101').get(), field('102').get(), field('106').get()));
      field('109').assign(field('107').get());
      field('110').assign(Math.max(field('108').get() - field('109').get(), 0));
      field('111').assign(field('110').get());
      field('112').assign(field('107').get() + field('111').get());
      field('113').assign(field('112').get());
      calcUtils.getGlobalValue('116', 'T2J', '371');
      field('115').assign(field('116').get() == 0 ? 0 : field('113').get() * field('114').get() / field('116').get());
      //period after jan 1,2016
      field('156').assign(field('155').get() == 0 ? 0 : field('153').get() * field('154').get() / field('155').get());
      field('157').assign(Math.min(field('151').get(), field('152').get(), field('156').get()));
      field('159').assign(field('157').get());
      field('160').assign(Math.max(field('158').get() - field('159').get(), 0));
      field('161').assign(field('160').get());
      field('162').assign(field('157').get() + field('161').get());
      field('163').assign(field('162').get());
      calcUtils.getGlobalValue('166', 'T2J', '371');
      field('165').assign(field('166').get() == 0 ? 0 : field('163').get() * field('164').get() / field('166').get());

      //check for fiscal period
      var janDate = wpw.tax.date(2016, 1, 1);
      var dateComparisons = calcUtils.compareDateAndFiscalPeriod(janDate);
      var isStartBeforeDate = dateComparisons.daysBeforeDate > 0;
      var isEndAfterDate = dateComparisons.daysAfterDate > 0;

      field('120').assign(isStartBeforeDate ? (field('100').get() - field('115').get()) : 0);
      field('167').assign(isEndAfterDate ? (field('150').get() - field('165').get()) : 0);

    });
    calcUtils.calc(function(calcUtils, field) {
      //part 2
      var isPartnershipMember = (field('CP.067').get() == 1 || field('T2S7.350').get() > 0);
      var janDate = wpw.tax.date(2016, 1, 1);
      var dateComparisons = calcUtils.compareDateAndFiscalPeriod(janDate);
      var endBeforeDate = dateComparisons.isStartBeforeDate;
      var startAfterDate = dateComparisons.isEndAfterDate;
      if (isPartnershipMember && endBeforeDate) {
        calcUtils.getGlobalValue('200', 'T2S7', '525');
        calcUtils.getGlobalValue('201', 'T2S7', '530');
        calcUtils.getGlobalValue('220', 'T2S7', '540');
        calcUtils.getGlobalValue('221', 'T2S7', '541');
        calcUtils.getGlobalValue('222', 'T2S7', '543');
        field('202').assign(Math.max(field('200').get() + field('201').get() + field('220').get() +
            field('221').get() - field('222').get(), 0));
        calcUtils.equals('203', '202');
        calcUtils.getGlobalValue('204', 'T2S7', '409');
        runSch7TableCalcs(field, '3000');
        calcUtils.getGlobalValue('205', 'T2S7', '370');
        calcUtils.getGlobalValue('206', 'T2S7', '380');
        field('207').assign(field('205').get() + field('206').get());
        field('208').assign(Math.min(field('3500').get(), field('3501').get()));
        calcUtils.sumBucketValues('209', ['208', '3501']);
        calcUtils.equals('210', '209');
        calcUtils.subtract('211', '204', '210');
        calcUtils.equals('212', '211');
        field('213').assign(Math.max(field('203').get() - field('212').get(), 0));
      }
      else if (isPartnershipMember && startAfterDate) {
        calcUtils.getGlobalValue('250', 'T2S7', '525');
        calcUtils.getGlobalValue('251', 'T2S7', '530');
        calcUtils.getGlobalValue('270', 'T2S7', '540');
        calcUtils.getGlobalValue('271', 'T2S7', '541');
        calcUtils.getGlobalValue('272', 'T2S7', '543');
        field('252').assign(Math.max(field('250').get() + field('251').get() + field('270').get() +
            field('271').get() - field('272').get(), 0));
        calcUtils.equals('253', '252');
        calcUtils.getGlobalValue('254', 'T2S7', '409');
        runSch7TableCalcs(field, '3001');
        calcUtils.getGlobalValue('255', 'T2S7', '370');
        calcUtils.getGlobalValue('256', 'T2S7', '380');
        field('257').assign(field('255').get() + field('256').get());
        field('258').assign(Math.min(field('3502').get(), field('257').get()));
        calcUtils.sumBucketValues('259', ['258', '3503']);
        calcUtils.equals('260', '259');
        calcUtils.subtract('261', '254', '260');
        calcUtils.equals('262', '261');
        field('263').assign(Math.max(field('253').get() - field('262').get(), 0));
      }
      else {
        calcUtils.removeValue([200, 201, 220, 221, 222, 205, 206, 250, 251, 270, 271, 272, 254, 255, 256], true)
      }

    });
    calcUtils.calc(function(calcUtils, field) {
      //part3 calcs
      var janDate = wpw.tax.date(2016, 1, 1);
      var dateComparisons = calcUtils.compareDateAndFiscalPeriod(janDate);
      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();

      field('4001').assign(field('120').get());
      field('4004').assign(daysFiscalPeriod);
      field('4200').assign(field('167').get());
      field('4203').assign(daysFiscalPeriod);

      field('4002').assign(dateComparisons.daysBeforeDate > daysFiscalPeriod ? daysFiscalPeriod : dateComparisons.daysBeforeDate);
      field('4201').assign(dateComparisons.daysAfterDate > daysFiscalPeriod ? daysFiscalPeriod : dateComparisons.daysAfterDate);

      field('4003').assign(field('4004').get() == 0 ? 0 : field('4001').get() * field('4002').get() / field('4004').get());
      field('4202').assign(field('4203').get() == 0 ? 0 : field('4200').get() * field('4201').get() / field('4203').get());
      field('300').assign(field('4003').get() + field('4202').get());
      field('301').assign(field('300').get() * 0.12);
    });
  });
})();
