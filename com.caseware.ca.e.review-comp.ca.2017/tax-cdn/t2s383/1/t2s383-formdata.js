(function () {
  'use strict';
  wpw.tax.create.formData('t2s383', {
    formInfo: {
      abbreviation: 'T2S383',
      title: 'Manitoba Corporation Tax Calculation',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      //subTitle: '(2016 and later tax years)',
      schedule: 'Schedule 383',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 383 E (12/2017)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if your corporation had a permanent establishment' +
              ' (under section 400 of the federal <i>Income Tax Regulations</i>) in Manitoba and had taxable' +
              ' income earned in the year in Manitoba.'
            },
            {
              label: 'This schedule is a worksheet only and is not required to be filed with your ' +
              '<i>T2 Corporation Income Tax Return</i>.'
            }
          ]
        }
      ],
      category: 'Manitoba Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Income subject to Manitoba lower and higher tax rates',
        'rows': [
          {
            'label': 'Period before January 1, 2016',
            'labelClass': 'bold center fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If there are days in the tax year before January 1, 2016, calculate the income subject to Manitoba lower and higher tax rates as follows:',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Taxable income for Manitoba *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100'
                }
              }
            ],
            'indicator': 'A1'
          },
          {
            'label': 'Income eligible for the Manitoba lower tax rate:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Amount from line 400 of the T2 return**',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '101'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B1'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 405 of the T2 return',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '102'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C1'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'label': 'Amount B1, C1, or D1, whichever is the least',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '107'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E1'
                }
              }
            ]
          },
          {
            'label': 'For credit unions only:'
          },
          {
            'label': 'Amount from line D of Schedule 17, <i>Credit Union Deductions</i>',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '108'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '1'
                }
              },
              null
            ]
          },
          {
            'label': 'Amount E1 above',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '109'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '2'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (amount 1 minus amount 2, if negative, enter "0")',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '110'
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '111'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F1'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> amounts E1 and F1) ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '112'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G1'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '2000'
          },
          {
            'label': '<b>Note</b>: amount H1 cannot exceed amount A1'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Income subject to Manitoba higher tax rate</b> (amount A1 <b>minus</b> amount H1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '120'
                }
              }
            ],
            'indicator': 'I1'
          },
          {
            'label': 'Enter amount I1 on the applicable line in Part 3',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fulLLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Period after December 31, 2015',
            'labelClass': 'bold center fullLength'
          },
          {
            'labelClass': 'fulLLength'
          },
          {
            'label': 'For days in the tax year after December 31, 2015, calculate the income subject to Manitoba lower and higher tax rates as follows:',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fulLLength'
          },
          {
            'label': 'Taxable income for Manitoba *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150'
                }
              }
            ],
            'indicator': 'A2'
          },
          {
            'label': 'Income eligible for the Manitoba lower tax rate:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Amount from line 400 of the T2 return**',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '151'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B2'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 405 of the T2 return',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '152'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C2'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '1001'
          },
          {
            'label': 'Amount B2, C2, or D2, whichever is the least',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '157'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E2'
                }
              }
            ]
          },
          {
            'label': 'For credit unions only:'
          },
          {
            'label': 'Amount from line D of Schedule 17, <i>Credit Union Deductions</i>',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '158'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '1'
                }
              },
              null
            ]
          },
          {
            'label': 'Amount E2 above',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '159'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '2'
                }
              },
              null
            ]
          },
          {
            'label': '(amount 1 <b>minus</b> amount 2) (if negative, enter "0")',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '160'
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '161'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F2'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> amounts E2 and F2) ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '162'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G2'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '2001'
          },
          {
            'label': '<b>Note</b>: amount H2 cannot exceed amount A2'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Income subject to Manitoba higher tax rate</b> (amount A2 <b>minus</b> amount H2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '167'
                }
              }
            ],
            'indicator': 'I2'
          },
          {
            'label': 'Enter amount I2 on the applicable line in Part 3',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fulLLength'
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'hideFieldset': true,
        'rows': [
          {
            'labelClass': 'fulLLength'
          },
          {
            'label': '* If the corporation has a permanent establishment only in Manitoba, enter the taxable income from line 360 of the T2 return. Otherwise, enter the taxable income allocated to Manitoba from column F in Part 1 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '** If the corporation is a member of a partnership, complete Part 2 to calculate income from active business.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '*** Includes the territories and the offshore jurisdictions for Nova Scotia and Newfoundland and Labrador.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'labelClass': 'fulLLength'
          }
        ]
      },
      {
        'header': 'Part 2 - Income from active business when there is partnership income',
        forceBreakAfter: true,
        'rows': [
          {
            'label': 'Complete this part only if the corporation is a member of a partnership.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Period before January 1, 2016',
            'labelClass': 'bold fullLength center'
          },
          {
            'label': 'If there are days in the tax year before January 1, 2016, calculate the income from active business as follows:',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Amount U from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '200'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '1'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 530 from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '201'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '2'
                }
              }
            ]
          },
          {
            'label': 'Line 540 from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '220'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '3'
                }
              }
            ]
          },
          {
            'label': 'Amount W from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '221'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '4'
                }
              }
            ]
          },
          {
            'label': 'Amount Y from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '222'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '5'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount 1 <b>plus</b> amount 2, <b>plus</b> amount 3, <b>plus</b> amount 4, <b>minus</b> amount 5)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '202',
                  disabled: true
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '203',
                  disabled: true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'J1'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount M from Part 4 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '204'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'K1'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '3000'
          },
          {
            'labelClass': 'fulLLength'
          },
          {
            'label': 'Amount on line 370 from Part 3 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '205'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'R1'
                }
              }, null, null
            ]
          },
          {
            'label': 'Amount on line 380 from Part 3 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '206'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'S1'
                }
              }, null, null
            ]
          },
          {
            'label': 'Subtotal (amount R1 <b>plus</b> amount S1)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '207',
                  disabled: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'T1'
                }
              }, null, null
            ]
          },
          {
            'label': 'Enter amount P1 or amount T1, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '208',
                  disabled: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'U1'
                }
              }, null
            ]
          },
          {
            'label': 'Specified partnership income (amount Q1 <b>plus</b> amount U1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '209',
                  disabled: true
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '210',
                  disabled: true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'V1'
                }
              }
            ]
          },
          {
            'label': 'Partnership income not eligible for small business deduction (amount K1 <b>minus</b> amount V1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '211',
                  disabled: true
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '212',
                  disabled: true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'W1'
          },
          {
            'label': '<b>Income from active business</b> (amount J1 <b>minus</b> amount W1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '213',
                  disabled: true
                }
              }
            ],
            'indicator': 'X1'
          },
          {
            'label': 'Enter the amount from line X1 on line B1 in Part 1.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Period after December 31, 2015',
            'labelClass': 'bold fullLength center'
          },
          {
            'label': 'For days in the tax year after December 31, 2015, calculate the income from active business as follows:',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Amount U from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '250'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '1'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 530 from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '251'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '2'
                }
              }
            ]
          },
          {
            'label': 'Line 540 from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '270'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '3'
                }
              }
            ]
          },
          {
            'label': 'Amount W from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '271'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '4'
                }
              }
            ]
          },
          {
            'label': 'Amount Y from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '272'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '5'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount 1 <b>plus</b> amount 2, <b>plus</b> amount 3, <b>plus</b> amount 4, <b>minus</b> amount 5)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '252',
                  disabled: true
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '253',
                  disabled: true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'J2'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount M from Part 4 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '254'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'K2'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '3001'
          },
          {
            'labelClass': 'fulLLength'
          },
          {
            'label': 'Amount on line 370 from Part 3 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '255'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'R2'
                }
              }, null, null
            ]
          },
          {
            'label': 'Amount on line 380 from Part 3 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '256'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'S2'
                }
              }, null, null
            ]
          },
          {
            'label': 'Subtotal (amount R2 <b>plus</b> amount S2)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '257',
                  disabled: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'T2'
                }
              }, null, null
            ]
          },
          {
            'label': 'Enter amount P2 or amount T2, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '258',
                  disabled: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'U2'
                }
              }, null
            ]
          },
          {
            'label': 'Specified partnership income (amount Q2 <b>plus</b> amount U2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '259',
                  disabled: true
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '260',
                  disabled: true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'V2'
                }
              }
            ]
          },
          {
            'label': 'Partnership income not eligible for small business deduction (amount K2 <b>minus</b> amount V2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '261',
                  disabled: true
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '262',
                  disabled: true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'W2'
          },
          {
            'label': '<b>Income from active business</b> (amount J2 <b>minus</b> amount W2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '263',
                  disabled: true
                }
              }
            ],
            'indicator': 'X2'
          },
          {
            'label': 'Enter the amount from line X2 on line B2 in Part 1.'
          }
        ]
      },
      {
        'header': 'Part 3 - Manitoba tax before credits',
        'rows': [
          {
            'label': 'Manitoba tax at the lower rate is zero. Calculate the Manitoba tax at the higher rate only.',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            'type': 'table',
            'num': '4000'
          },
          {
            'type': 'table',
            'num': '4010'
          },
          {
            'label': 'Subtotal (amount 1 <b>plus</b> amount 2)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            indicator: '3',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '300'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Manitoba tax before credits</b> (amount 3 <b>multiplied</b> by 12%) *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '301'
                }
              }
            ],
            'indicator': 'Y'
          },
          {labelClass: 'fullLength'},
          {
            'label': '* If the corporation has a permanent establishment in more than one jurisdiction or is claiming a Manitoba tax credit, enter amount Y on line 230 of Schedule 5. Otherwise, enter it on line 760 of the T2 return. ',
            'labelClass': 'fullLength tabbed'
          }
        ]
      }
    ]
  })
})();
