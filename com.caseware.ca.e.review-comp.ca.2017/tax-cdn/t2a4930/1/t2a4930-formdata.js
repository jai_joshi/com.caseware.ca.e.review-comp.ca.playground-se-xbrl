(function () {
  wpw.tax.create.formData('t2a4930', {
    formInfo: {
      abbreviation: 't2a4930',
      title: 'Alberta Dispositions of Capital Property',
      formFooterNum: 'AT4930 (Aug-12)',
      // headerImage: 'canada-alberta',
      showCorpInfo: true,
      category: 'Alberta Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: 'This form authorizes Tax and Revenue Administration to release confidential taxpayer information to a designated third party representative in\n' +
            'matters pertaining to applicable legislation. <b>Note: This authorization is valid until the taxpayer or authorized signing person of the\n' +
            'taxpayer cancels it in writing. Please complete a separate form for each representative.</b> Send the completed form to TAX AND\n' +
            'REVENUE ADMINISTRATION, 9811 109 STREET, EDMONTON, AB T5K 2L5 or fax to 780-427-0348. If you have any questions, please\n' +
            'phone 780-427-3044. If calling long distance within Alberta, call 310-0000, then enter 780-427-3044. ',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        rows: [
          {
            label: 'The purpose of this form is:',
            type: 'infoField',
            inputType: 'dropdown',
            inputClass: 'std-input-col-width-2',
            options: [
              {option: 'To authorize a third party representative to receive taxpayer information', value: '1'},
              {option: 'To cancel a third party representative from receiving taxpayer information', value: '2'}
            ]
          }
        ]
      },
      {
        header: '1. Taxpayer Identification',
        rows: [
          {
            type: 'infoField',
            inputClass: 'std-input-col-width-2',
            label: 'Corporate Legal Name:',
            num: '101'
          },
          {
            type: 'infoField',
            inputClass: 'std-input-col-width-2',
            label: 'Alberta Corporate Account Number (CAN):',
            num: '102'
          },
          {
            type: 'infoField',
            inputClass: 'std-input-col-width-2',
            label: 'Alberta Business Identification Number (BIN)',
            num: '103'
          }
        ]
      },
      {
        header: '2. Authorized Third Party Identification',
        rows: [
          {
            type: 'infoField',
            inputClass: 'std-input-col-width-2',
            label: 'Authorized Individuals\' Name',
            num: '104'
          },
          {
            type: 'infoField',
            inputClass: 'std-input-col-width-2',
            label: 'and/or Name of the Firm:',
            num: '105'
          },
          {
            type: 'infoField',
            inputType: 'address',
            label: 'Address:',
            add1Num: '011',
            add2Num: '012',
            provNum: '016',
            cityNum: '015',
            countryNum: '017',
            postalCodeNum: '018'
          },
          {
            type: 'infoField',
            inputClass: 'std-input-col-width',
            label: 'Phone Number:',
            num: '106',
            validate: 'phone',
            telephoneNumber: true
          }
        ]
      },
      {
        header: '3. Details of Authorization',
        rows: [
          {
            'type': 'splitTable',
            'side1': [
              {
                label: 'Indicate the period for which authorization or cancellation applies:'
              },
              {type: 'table', num: '998'}
            ],
            'side2': [
              {type: 'table', num: '999'}
            ]
          }
        ]
      },
      {
        header: '4. Authorized Signature (authorized signing officer of the taxpayer)',
        rows: [
          {
            'type': 'multiColumn',
            'dividers': [
              false
            ],
            'columns': [
              [
                {
                  type: 'infoField',
                  inputClass: 'std-input-col-width',
                  label: 'Name',
                  num: '201'
                }
              ],
              [
                {
                  type: 'infoField',
                  label: 'Phone Number:',
                  num: '202',
                  validate: 'phone',
                  telephoneNumber: true
                }
              ],
              [
                {
                  type: 'infoField',
                  label: 'Date Signed',
                  num: '203',
                  inputType: 'date'
                }
              ]
            ]
          },
          {
            'type': 'multiColumn',
            'dividers': [
              false
            ],
            'columns': [
              [
                {
                  type: 'infoField',
                  label: 'Signature',
                  inputClass: 'std-input-col-width-2'
                }
              ],
              [
                {
                  type: 'infoField',
                  label: 'Postion, Office or Rank:',
                  num: '204'
                }
              ]
            ]
          }
        ]
      }
    ]
  });
})();
