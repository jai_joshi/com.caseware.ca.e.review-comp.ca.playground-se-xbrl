(function () {
  wpw.tax.create.tables('t2a4930', {
    '998': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'none', colClass: 'std-input-width'},
        {type: 'none', colClass: 'std-spacing-width'},
        {},
        {type: 'none', colClass: 'std-spacing-width'},
        {},
        {type: 'none', colClass: 'std-spacing-width'}
      ],
      cells: [
        {
          0: {label: 'All Years'},
          2: {type: 'singleCheckbox'},
          4: {type: 'none'}
        },
        {
          0: {label: 'OR', labelClass: 'bold'},
          2: {type: 'none'},
          4: {type: 'none'}
        },
        {
          0: {label: 'Specific Years'},
          2: {type: 'text'},
          4: {type: 'text'}
        },
        {
          2: {type: 'text'},
          4: {type: 'text'}
        },
        {
          0: {label: 'OR', labelClass: 'bold'},
          2: {type: 'none'},
          4: {type: 'none'}
        },
        {
          0: {label: 'Specific Years'},
          2: {type: 'text'},
          4: {type: 'none'}
        }
      ]
    },
    '999': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'singleCheckbox', colClass: 'small-input-width'},
        {type: 'none'}
      ],
      cells: [
        {1: {label: 'All Tax Programs'}},
        {
          0: {type: 'none'},
          1: {label: 'OR', labelClass: 'bold'}
        },
        {1: {label: 'Corporate Income Tax'}},
        {1: {label: 'Tourism Levy'}},
        {1: {label: 'International Fuel Tax Agreement (IFTA)'}},
        {1: {label: 'Tax Exempt Fuel Users (TEFU)'}},
        {1: {label: 'Prescribed Rebate Offroad Percentages (PROP)'}},
        {1: {label: 'Other (specify)'}},
        {
          0: {type: 'none'},
          1: {type: 'infoField'}
        }
      ]
    }
  })
})();
