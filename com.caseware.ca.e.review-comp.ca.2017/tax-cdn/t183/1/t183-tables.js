(function(){

  wpw.tax.global.tableCalculations.t183 = {
    1000: {
      type: 'table', fixedRows: true, infoTable: true, num: '1000',
      columns: [
        {
          header: 'Tax year-start date',
          type: 'date',
          cellClass: 'alignCenter',
          formField: true
        },
        {
          header: 'Tax year-end date',
          type: 'date',
          cellClass: 'alignCenter',
          formField: true
        }
      ],
      cells: [
        {
          0: {num: '102'},
          1: {num: '103'}
        }
      ]
    },
    1010:{
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          colClass: 'std-spacing-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          type: 'text',
          cannotOverride: true
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          type: 'text',
          cannotOverride: true
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          type: 'text',
          cannotOverride: true
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-spacing-width'
        }
      ],
      cells: [
        {
          1:{
            label: 'I,'
          },
          3:{
            num: '300'
          },
          5:{
            num: '301'
          },
          7:{
            num: '302'
          }
        },
        {
          3:{
            type: 'none',
            label: 'Last name',
            cellClass: 'alignCenter'
          },
          5:{
            type: 'none',
            label: 'First name',
            cellClass: 'alignCenter'
          },
          7:{
            type: 'none',
            label: 'Position, office, or rank',
            cellClass: 'alignCenter'
          }
        }
      ]
    },
    1015:{
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          colClass: 'std-spacing-width'
        },
        {
          type: 'date',
          cannotOverride: true
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          type: 'text',
          colClass: 'std-input-col-width-3'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          type: 'text',
          cannotOverride: true
        },
        {
          type: 'none',
          colClass: 'std-spacing-width'
        }
      ],
      cells: [
        {
          1:{
            num: '303'
          },
          3:{
            num: '304'
          },
          5:{
            num: '305'
          }
        },
        {
          1:{
            type: 'none',
            label: 'Date (yyyy/mm/dd) ',
            cellClass: 'alignCenter'
          },
          3:{
            type: 'none',
            label: 'Signature of an authorized signing officer of the corporation ',
            cellClass: 'alignCenter'
          },
          5:{
            type: 'none',
            label: 'Telephone number',
            cellClass: 'alignCenter'
          }
        }
      ]
    },
    1020:{
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          colClass: 'std-spacing-width'
        },
        {
          type: 'text'
        },
        {
          type: 'none',
          colClass: 'std-spacing-width'
        },
        {
          colClass: 'std-input-col-padding-width',
          type: 'text'
        },
        {
          type: 'none',
          colClass: 'std-spacing-width'
        }
      ],
      cells: [
        {
          1:{
            num: '400'
          },
          3:{
            num: '401'
          }
        },
        {
          1:{
            label: 'Name of person or firm ',
            cellClass: 'alignCenter',
            type: 'none'
          },
          3:{
            cellClass: 'alignCenter',
            label: 'Electronic filer number',
            type: 'none'
          }
        }
      ]
    }
  }
})();
