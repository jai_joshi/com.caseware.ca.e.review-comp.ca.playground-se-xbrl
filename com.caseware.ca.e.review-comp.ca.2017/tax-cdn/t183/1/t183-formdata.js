(function() {
  'use strict';

  wpw.tax.global.formData.t183 = {
      formInfo: {
        abbreviation: 'T183',
        title: 'Information Return for Corporations Filing Electronically',
        schedule: 'T183',
        formFooterNum: 'T183 CORP E (16) ',
  headerImage: 'canada-federal',
        description: [
          {
            type: 'list',
            items: [
              {
                label: 'You have to complete this return for every initial and amended <i>T2 Corporation ' +
                'Income Tax Return</i> electronically filed to the Canada Revenue Agency (CRA) on your behalf.',
                labelClass: 'fullLength'
              },
              {
                label: 'By completing Part 2 and signing Part 3, you acknowledge that, under the ' +
                '<i>Income Tax Act</i>, you have to keep all records used to prepare your corporation ' +
                'income tax return, and provide this information to us on request.',
                labelClass: 'fullLength'
              },
              {
                label: 'Part 4 must be completed by either you or the electronic transmitter of your ' +
                'corporation income tax return.',
                labelClass: 'fullLength'
              },
              {
                label: 'Give the signed original of this return to the transmitter and keep a ' +
                'copy in your own records for six years.',
                labelClass: 'fullLength'
              },
              {
                label: '<b>Do not submit</b> this form to the CRA unless we ask for it.',
                labelClass: 'fullLength'
              },
              {
                label: 'We are responsible for ensuring the confidentiality of your electronically filed ' +
                'tax information only after we have accepted it.',
                labelClass: 'fullLength'
              }
            ]
          }
        ],
        category: 'EFILE'
      },
      sections: [
        {
        header: 'Part 1 - Identification',
          rows: [
            {
              type: 'splitTable', fieldAlignRight: true,
              side1: [
                {
                  type: 'infoField',
                  label: 'Corporation\'s name',
                  inputType: 'none'
                },
                {
                  type: 'infoField',
                  inputType: 'textArea',
                  num: '100',
                  maxLength: 175
                },
                {labelClass: 'fullLength'},
                {
                  type: 'infoField',
                  inputType: 'custom',
                  format: ['{N9}RC{N4}', 'NR'],
                  label: 'Business Number',
                  inputClass: 'std-input-col-width-2',
                  num: '101'
                }
              ],
              side2: [
                {
                  type: 'table', num: '1000'
                },
                {labelClass: 'fullLength'},
                {labelClass: 'fullLength'},
                {
                  type: 'infoField',
                  inputType: 'radio',
                  label: 'Is this an amended return?',
                  labelWidth: '70%',
                  num: '104'
                }
              ]
            }
          ]
        },
        {
        header: 'Part 2 - Declaration',
          rows: [
            {
              'label': 'Enter the following amounts, if applicable, from your corporation income tax return for the tax year noted above:',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Net income (or loss) for income tax purposes from Schedule 1, financial statements, or GIFI (line 300)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '200'
                  }
                }
              ]
            },
            {
              'label': 'Part I tax payable (line 700)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '201'
                  }
                }
              ]
            },
            {
              'label': 'Part II surtax payable (line 708)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '202'
                  }
                }
              ]
            },
            {
              'label': 'Part III.1 tax payable (line 710)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '203'
                  }
                }
              ]
            },
            {
              'label': 'Part IV tax payable (line 712)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '204'
                  }
                }
              ]
            },
            {
              'label': 'Part IV.1 tax payable (line 716)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '205'
                  }
                }
              ]
            },
            {
              'label': 'Part VI tax payable (line 720)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '206'
                  }
                }
              ]
            },
            {
              'label': 'Part VI.1 tax payable (line 724)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '207'
                  }
                }
              ]
            },
            {
              'label': 'Part XIV tax payable (line 728)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '208'
                  }
                }
              ]
            },
            {
              'label': 'Net provincial and territorial tax payable (line 760)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '209'
                  }
                }
              ]
            }
          ]
        },
        {
        header: 'Part 3 - Certification and authorization',
          rows: [
            // {
            //   'type': 'infoBox',
            //   'header': 'Sign up for online mail',
            //   'info': 'Get your CRA mail electronically delivered in My Business Account at <b>cra.gc.ca/mybusinessaccount</b>'
            // },
            {
              label: '<img src="prod/com.caseware.ca.e.review-comp.ca.2017/images/T183Part3.png" alt="CRA Email Logo" width="100%" height="100%">',
              'labelClass': 'fullLength'
            },
            {
              label: 'Get your CRA mail electronically delivered in My Business Account at <b>cra.gc.ca/mybusinessaccount</b>'
            },
            {
              label: "I understand that by providing an email address, I am <b>registering</b> the corporation" +
              " for the 'Manage online mail' service. I understand and agree that all notices and other" +
              " correspondence eligible for electronic delivery will no longer be printed and mailed. " +
              "The CRA will notify the corporation at this email address when they are available in My Business" +
              " Account and requiring immediate attention. They will be presumed to have been " +
              "received on the date that the email is sent."
            },
            {
              type: 'infoField',
              inputClass: 'std-input-col-width-3', num: '210',
              label: '<b>Email address</b> for online mail (optional):'
            },
            {
              labelClass: 'fullLength'
            },
            {
              type: 'table',
              num: '1010'
            },
            {labelClass: 'fullLength'},
            {
              label: 'am an authorized signing officer of the corporation. I certify that I have ' +
              'examined the corporation T2 income tax return, including accompanying schedules and statements,' +
              ' and that the information given on the T2 return and this T183 Corp information return is, ' +
              'to the best of my knowledge, correct and complete. I also certify that the method of calculating ' +
              'income for this tax year is consistent with that of the previous tax year except as specifically ' +
              'disclosed in a statement attached to this return. ' +
              '<br><br>I authorize the transmitter identified in Part 4 ' +
              'to electronically file the corporation income tax return identified in Part 1. ' +
              'The transmitter can also modify the information originally filed in response to any errors ' +
              'Canada Revenue Agency identifies. This authorization expires when the Minister of' +
              ' National Revenue accepts the electronic return as filed.',
              'labelClass': 'fullLength'
            },
            {labelClass: 'fullLength'},
            {labelClass: 'fullLength'},
            {
              type: 'table',
              num: '1015'
            }
          ]
        },
        {
        header: 'Part 4 - Transmitter identification',
          rows: [
            {
              label: 'The following transmitter has electronically filed the tax return of the ' +
              'corporation identified in Part 1.',
              labelClass: 'fullLength'
            },
            {labelClass: 'fullLength'},
            {
              type: 'table',
              num: '1020'
            }
          ]
        },
        {
        header: 'Privacy statement',
          rows: [
            {
              label: 'Personal information is collected under the <i>Income Tax Act</i> to administer tax, benefits, ' +
              'and related programs. It may also be used for any purpose related to the administration ' +
              'or enforcement of the Act such as audit, compliance and the payment of debts owed to ' +
              'the Crown. It may be shared or verified with other federal, provincial/territorial ' +
              'government institutions to the extent authorized by law. Failure to provide this ' +
              'information may result in interest payable, penalties or other actions. Under ' +
              'the <i>Privacy Act</i>, individuals have the right to access their personal information ' +
              'and request correction if there are errors or omissions. Refer to Info Source ' +
              'cra.gc.ca/gncy/tp/nfsrc/nfsrc-eng.html,'.link('http://www.cra-arc.gc.ca/gncy/tp/nfsrc/nfsrc-eng.html') +
              ' personal information ' +
              'bank CRA PPU 047.',
              labelClass: 'fullLength'
            }
          ]
        }
      ]
    };
})();
