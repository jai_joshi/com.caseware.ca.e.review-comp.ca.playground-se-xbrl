(function() {
  wpw.tax.create.calcBlocks('t183', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //part 1
      calcUtils.getGlobalValue('100', 'CP', '002');
      calcUtils.getGlobalValue('101', 'CP', 'bn');
      calcUtils.getGlobalValue('102', 'CP', 'tax_start');
      calcUtils.getGlobalValue('103', 'CP', 'tax_end');
      calcUtils.getGlobalValue('104', 'CP', '997');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 2
      calcUtils.getGlobalValue('200', 'T2J', '300');
      calcUtils.getGlobalValue('201', 'T2J', '700');
      calcUtils.getGlobalValue('202', 'T2J', '708');
      calcUtils.getGlobalValue('203', 'T2J', '710');
      calcUtils.getGlobalValue('204', 'T2J', '712');
      calcUtils.getGlobalValue('205', 'T2J', '716');
      calcUtils.getGlobalValue('206', 'T2J', '720');
      calcUtils.getGlobalValue('207', 'T2J', '724');
      calcUtils.getGlobalValue('208', 'T2J', '728');
      calcUtils.getGlobalValue('209', 'T2J', '760');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 3
      calcUtils.getGlobalValue('210', 'CP', '089');
      calcUtils.getGlobalValue('300', 'CP', '950');
      calcUtils.getGlobalValue('301', 'CP', '951');
      calcUtils.getGlobalValue('302', 'CP', '954');
      calcUtils.getGlobalValue('303', 'CP', '955');
      calcUtils.getGlobalValue('305', 'CP', '956');

      //part 4
      //TODO: get values for 400 and 401
    });
  });
})();
