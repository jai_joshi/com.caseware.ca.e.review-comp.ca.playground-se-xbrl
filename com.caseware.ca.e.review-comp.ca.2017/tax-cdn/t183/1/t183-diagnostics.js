(function() {
  wpw.tax.create.diagnostics('t183', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T183'), common.bnCheck('101')));

  });
})();
