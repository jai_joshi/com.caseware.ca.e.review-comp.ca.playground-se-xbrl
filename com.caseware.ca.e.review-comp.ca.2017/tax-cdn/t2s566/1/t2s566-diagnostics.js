(function() {
  wpw.tax.create.diagnostics('t2s566', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S566'), forEach.row('550', forEach.bnCheckCol(1, true))));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S566'), forEach.row('650', forEach.bnCheckCol(1, true))));

    diagUtils.diagnostic('566.415', common.prereq(common.requireFiled('T2S566'),
        function(tools) {
          return tools.field('415').require(function() {
            return this.get() <= 3000000;
          })
        }));

    diagUtils.diagnostic('566.440', common.prereq(common.requireFiled('T2S566'),
        function(tools) {
          return tools.field('440').require(function() {
            return this.get() <= 3000000;
          })
        }));

    diagUtils.diagnostic('5660005', common.prereq(common.check('t2s5.468', 'isNonZero'),
        common.requireFiled('T2S566')));

    diagUtils.diagnostic('5660010', common.prereq(common.and(
        common.requireFiled('T2S566'),
        common.check(['710', '711', '712'], 'isNonZero')),
        common.check(['100', '105', '110', '115', '120'], 'isFilled', true)));

    diagUtils.diagnostic('5660015', common.prereq(common.and(
        common.requireFiled('T2S566'),
        common.check(['710', '711', '712'], 'isNonZero')),
        common.check(['215', '300', '305', '310', '315'], 'isNonZero')));

    diagUtils.diagnostic('5660025', common.prereq(common.and(
        common.requireFiled('T2S566'),
        common.and(
            common.check(['710', '711', '712'], 'isNonZero'),
            common.check('315', 'isNonZero'))),
        common.check('310', 'isNonZero')));

    diagUtils.diagnostic('5660030', common.prereq(common.and(
        common.requireFiled('T2S566'),
        function(tools) {
          return tools.checkMethod(tools.list(['710', '711', '712']), 'isNonZero') &&
              (tools.field('t2s31.385').isEmpty() || tools.field('t2s31.385').get() == '1') &&
              tools.field('cp.226').get() == '1';
        }), common.check('420', 'isNonZero')));

    diagUtils.diagnostic('5660040', common.prereq(common.and(
        common.requireFiled('T2S566'),
        function(tools) {
          return tools.checkMethod(tools.list(['710', '711', '712']), 'isNonZero') &&
              (tools.field('425').isNonZero() || tools.field('505').isNonZero()) &&
              (tools.field('t2s31.385').isEmpty() || tools.field('t2s31.385').get() == '1') &&
              tools.field('cp.226').get() == '1';
        }), function(tools) {
      var table = tools.field('550');
      return tools.checkAll(table.getRows(), function(row) {
        return tools.requireAll([row[0], row[1], row[2]], 'isFilled') &&
            row[1].require(function() {
              return this.get() == tools.field('cp.bn').get();
            });
      })
    }));

    diagUtils.diagnostic('5660050', common.prereq(common.and(
        common.requireFiled('T2S566'),
        function(tools) {
          return tools.checkMethod(tools.list(['710', '711', '712']), 'isNonZero') &&
              (tools.field('t2s31.385').isEmpty() || tools.field('t2s31.385').get() == '1') &&
              tools.field('cp.226').get() == '1';
        }), function(tools) {
      var table = tools.field('650');
      return tools.checkAll(table.getRows(), function(row) {
        return tools.requireAll([row[0], row[1], row[2]], 'isFilled') &&
            row[1].require(function() {
              return this.get() == tools.field('cp.bn').get();
            });
      })
    }));

    diagUtils.diagnostic('5660055', common.prereq(common.and(
        common.requireFiled('T2S566'),
        common.check('715', 'isYes')),
        common.check('720', 'isNonZero')));

  });
})();
