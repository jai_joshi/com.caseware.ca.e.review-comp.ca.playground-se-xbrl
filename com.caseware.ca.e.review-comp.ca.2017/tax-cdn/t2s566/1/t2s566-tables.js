(function() {

  var percentCols = [
    {
      type: 'none',
      cellClass: 'alignRight'
    },
    {
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-padding-width',
      type: 'none',
      labelClass: 'center'
    },
    {
      colClass: 'std-input-col-width',
      type: 'none',
      labelClass: 'center'
    },
    {
      colClass: 'std-spacing-width',
      type: 'none'
    },
    {
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-padding-width',
      type: 'none',
      labelClass: 'center'
    },
    {
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-padding-width',
      'type': 'none'
    }
  ];

  var part7Columns = [
    {
      type: 'none', labelClass: 'center'
    },
    {
      colClass: 'std-spacing-width',
      type: 'none'
    },
    {
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-padding-width',
      type: 'none'
    },
    {
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-padding-width',
      type: 'none',
      cellClass: 'alignLeft'
    },
    {
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-padding-width',
      type: 'none',
      cellClass: 'alignLeft'
    },
    {
      colClass: 'std-input-width',
      'type': 'none'
    },
    {
      colClass: 'std-padding-width',
      'type': 'none'
    }
  ];

  var part4Columns = [
    {
      type: 'none',
      cellClass: 'alignLeft'
    },
    {
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-input-col-padding-width',
      type: 'none',
      cellClass: 'alignRight'
    },
    {
      colClass: 'std-padding-width',
      type: 'none'
    },
    {
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-padding-width',
      type: 'none',
      cellClass: 'alignLeft'
    },
    {
      colClass: 'std-input-width',
      'type': 'none'
    },
    {
      colClass: 'std-padding-width',
      'type': 'none'
    }
  ];

  wpw.tax.create.tables('t2s566', {
    'di_table_1': {
      'num': 'di_table_1',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Designated repayments made in the year of government or non-government assistance <br>or contract payments relating to Ontario qualified expenditures for first term <br>or second term shared-use equipment acquired before 2014',
            'trailingDots': true
          },
          '1': {
            'tn': '310'
          },
          '2': {
            'num': '310',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '311',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '315'
          },
          '7': {
            'num': '315',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'b'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_2': {
      'num': 'di_table_2',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Subtotal (amount a <b>plus</b> amount b)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '331',
            'decimals': 1,
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' doubleUnderline'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '332',
            'decimals': 1
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': '320'
          },
          '7': {
            'num': '320',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' singleUnderline'
          },
          '8': {
            'label': 'C'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_3': {
      'num': 'di_table_3',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Amount c or $500,000, whichever is greater',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '401',
            'cellClass': ' doubleUnderline'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '402',
            'init': undefined
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '403',
            'cellClass': ' singleUnderline'
          },
          '8': {
            'label': 'E'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_4': {
      'num': 'di_table_4',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Amount f or $500,000, whichever is greater',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '421',
            'cellClass': ' doubleUnderline'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '422',
            'init': undefined
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '423',
            'cellClass': ' doubleUnderline'
          },
          '8': {
            'label': 'H'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_5': {
      'num': 'di_table_5',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Eligible repayments made in the year for tax years ending before June 1, 2016 (see note in Part 3)',
            'trailingDots': true
          },
          '1': {
            'tn': '709'
          },
          '2': {
            'num': '709',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '929',
            'decimals': 2
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '930',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': 'Y'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    451: {
      infoTable: true,
      fixedRows: true,
      columns: part4Columns,
      cells: [
        {
          0: {
            label: 'Specified capital amount for the corporation for the <br>previous tax year (line 500 in Part 5)'
          },
          1: {
            num: '406'
          },
          2: {
            label: '<b>minus</b> $25,000,000 ='
          },
          3: {
            tn: '405'
          },
          4: {
            num: '405', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          5: {
            label: 'd**'
          }
        }
      ]
    },
    453: {
      infoTable: true,
      fixedRows: true,
      columns: part4Columns,
      cells: [
        {
          0: {
            label: 'Specified capital amount of the corporation and of its associated corporations for ' +
            'their last tax year-ending in the previous calendar year (line 505 in Part 5)'
          },
          1: {
            num: '426'
          },
          2: {
            label: '<b>minus</b> $25,000,000 ='
          },
          3: {
            tn: '425'
          },
          4: {
            num: '425', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          5: {
            label: 'g**'
          }
        }
      ]
    },
    454: {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          width: '200px',
          type: 'none'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      cells: [
        {
          0: {
            label: 'Amount G or amount K, whichever applies'
          },
          1: {
            num: '441'
          },
          2: {
            label: 'x'
          },
          3: {
            label: 'Number of days in the tax year',
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          5: {
            num: '442', cellClass: 'singleUnderline'
          },
          6: {
            label: '='
          },
          7: {
            tn: '445'
          },
          8: {
            num: '445', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          9: {
            label: 'L'
          }
        },
        {
          1: {
            type: 'none'
          },
          3: {
            label: '365',
            labelClass: 'center'
          },
          5: {
            num: '443'
          },
          8: {
            type: 'none'
          }
        }
      ]
    },
    550: {
      fixedRows: true,
      showNumbering: true,
      hasTotals: true,
      columns: [
        {
          header: '1<br>Names of associated corporations<br>',
          tn: '510',
          type: 'text'
        },
        {
          header: '2<br>Business Number of associated corporations<br>(enter "NR" if a corporation is not registered)<br>',
          tn: '515',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          header: '3<br>Specified capital amount<br>',
          tn: '520',
          total: true,
          totalNum: '525',
          totalMessage: 'Total specified capital amount',
          totalIndicator: 'O'
        }
      ]
    },
    650: {
      fixedRows: true,
      showNumbering: true,
      hasTotals: true,
      columns: [
        {
          header: '1<br>Names of associated corporations<br>',
          tn: '600',
          type: 'text'
        },
        {
          header: '2<br>Business Number of associated corporations<br>(enter "NR" if a corporation is not registered)<br>',
          tn: '605',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          header: 'Business limit for the year (before the allocation) $',
          format: {prefix: '$'},
          colClass: 'std-input-width',
          disabled: true
        },
        {
          header: '3<br>Expenditure limit allocated * (allocate the amount of the expenditure limit from line 435 in Part 4 to each associated corporation)<br>',
          colClass: 'std-input-width',
          tn: '610',
          total: true,
          totalNum: '620',
          totalMessage: 'Total expenditure limit',
          totalIndicator: 'P'
        }
      ]
    },
    750: {
      infoTable: true,
      fixedRows: true,
      columns: part7Columns,
      cells: [
        {
          0: {
            label: 'Number of days in tax year before June 1, 2016',
            cellClass: 'singleUnderline'
          },
          2: {num: '901', cellClass: 'singleUnderline'},
          3: {label: 'x'},
          4: {
            num: '902'
          },
          5: {label: '%='},
          6: {num: '903', decimals: 5},
          7: {label: '%1'}
        },
        {
          0: {
            label: 'Number of days in the tax year'
          },
          2: {
            num: '904'
          },
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {label: 'Number of days in tax year after May 31, 2016', cellClass: 'singleUnderline'},
          2: {num: '905', cellClass: 'singleUnderline'},
          3: {label: 'x'},
          4: {num: '906'},
          5: {label: '%='},
          6: {num: '907', decimals: 5},
          7: {label: '%2'}
        },
        {
          0: {
            label: 'Number of days in the tax year'
          },
          2: {
            num: '908'
          },
          4: {type: 'none'},
          6: {type: 'none'}
        }
      ]
    },
    752: {
      infoTable: true,
      fixedRows: true,
      columns: percentCols,
      cells: [
        {
          0: {
            label: 'Amount n'
          },
          1: {
            num: '910'
          },
          2: {
            label: 'x'
          },
          3: {
            label: '0.1',
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          5: {
            num: '912',
            decimals: '5'
          },
          6: {
            label: '='
          },
          7: {
            num: '913'
          },
          8: {
            label: 'S'
          }
        },
        {
          1: {
            type: 'none'
          },
          3: {
            label: 'Percentage 3%',
            labelClass: 'center'
          },
          5: {
            num: '914',
            decimals: 5
          },
          7: {
            type: 'none'
          }
        }
      ]
    },
    753: {
      infoTable: true,
      fixedRows: true,
      columns: part7Columns,
      cells: [
        {
          0: {
            label: 'Number of days in tax year before June 1, 2016', cellClass: 'singleUnderline'
          },
          2: {
            num: '716',
            "validate": {"or": [{"matches": "^-?[.\\d]{1,3}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline',
            tn: '716'
          },
          3: {label: 'x'},
          4: {num: '916'},
          5: {label: '%='},
          6: {num: '917', decimals: 5},
          7: {label: '%4', labelClass: 'align-left'}
        },
        {
          0: {label: 'Number of days in the tax year'},
          2: {
            num: '718',
            "validate": {"or": [{"matches": "^-?[.\\d]{1,3}$"}, {"check": "isEmpty"}]},
            tn: '718'
          },
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {label: 'Number of days in tax year after May 31, 2016', cellClass: 'singleUnderline'},
          2: {
            num: '717',
            "validate": {"or": [{"matches": "^-?[.\\d]{1,3}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline',
            tn: '717'
          },
          3: {label: 'x'},
          4: {num: '920'},
          5: {label: '%='},
          6: {num: '921', decimals: 5},
          7: {label: '%5'}
        },
        {
          0: {label: 'Number of days in the tax year'},
          2: {num: '922'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {label: 'Subtotal (percentage 4 <b>plus</b> percentage 5)'},
          2: {type: 'none'},
          4: {type: 'none'},
          6: {
            'num': '923',
            'decimals': 5
          },
          7:{label: '%6'}
        }
      ]
    },
    755: {
      infoTable: true,
      fixedRows: true,
      columns: percentCols,
      cells: [
        {
          0: {
            label: 'Amount q'
          },
          1: {
            num: '924'
          },
          2: {
            label: 'x'
          },
          3: {
            label: 'Percentage 6%',
            cellClass: 'singleUnderline'
          },
          5: {
            num: '925',
            decimals: 5, cellClass: 'singleUnderline'
          },
          6: {
            label: '='
          },
          7: {
            num: '926'
          },
          8: {
            label: 'X'
          }
        },
        {
          1: {
            type: 'none'
          },
          3: {
            label: '8%'
          },
          5: {
            num: '927', decimals: 5
          },
          7: {
            type: 'none'
          }
        }
      ]
    },
    800: {
      maxLoop: 100000,
      repeats: ['850'],
      showNumbering: true,
      columns: [
        {
          header: '1<br>Name of corporation making the payment<br>',
          tn: '800',
          type: 'text'
        },
        {
          header: '2<br>Address of the corporation making the payment<br>',
          tn: '805',
          type: 'address'
        }
      ]
    },
    850: {
      keepButtonsSpace: true,
      showNumbering: true,
      fixedRows: true,
      columns: [
        {
          header: '3<br>Is this an arm\'s length transaction?<br>',
          tn: '810',
          type: 'radio'
        },
        {
          header: '4<br>Gross amount of specified contract payment received<br>',
          tn: '815'
        },
        {
          header: '5<br>Actual SR&ED expenditure relating to contract included in claim<br>',
          tn: '820'
        }
      ]
    }

  })
})();
