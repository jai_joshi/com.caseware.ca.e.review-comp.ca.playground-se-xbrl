(function() {

  wpw.tax.create.formData('t2s566', {
    formInfo: {
      abbreviation: 'T2S566',
      title: 'Ontario Innovation Tax Credit',
      schedule: 'Schedule 566',
      headerImage: 'canada-federal',
      code: 'Code 1601',
      formFooterNum: 'T2 SCH 566 E (17)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule to claim an Ontario innovation tax credit (OITC). A qualifying corporation must:',
              sublist: ['have had a permanent establishment in Ontario during the tax year;',
                'have carried on scientific research and experimental development (SR&ED) in Ontario during the tax year;',
                'be eligible to claim a federal investment tax credit under section 127 of the federal ' +
                '<i>Income Tax Act</i> for its qualified expenditures; and',
                'have filed Form T661, <i>Scientific Research and Experimental Development (SR&ED) ' +
                'Expenditures Claim</i>,and Schedule 31, <i>Investment Tax Credit – Corporations</i>,' +
                ' within 18 months of the tax year-end.']
            },
            {
              label: 'The OITC is a refundable tax credit based on the sum of the corporation\'s qualified ' +
              'expenditures incurred in Ontario and any eligible repayments. The refundable tax credit is:',
              sublist: ['10% for tax years that end before June 1, 2016;',
                '8% for tax years that start after May 31, 2016;',
                'prorated for tax years that ends on or after June 1, 2016, and includes May 31, 2016. ']
            },
            {
              label: 'If you made a repayment of any government or non-government assistance, or ' +
              'contract payments that reduced your SR&ED qualified expenditure pool for OITC purposes, ' +
              'the amount of the repayment is eligible for a credit to the extent that your SR&ED ' +
              'qualified expenditure pool for OITC purposes was reduced because of the government or ' +
              'non-government assistance, or contract payments.'
            },
            {
              label: 'The OITC is available to a maximum annual expenditure limit of $3 million. ' +
              'Associated corporations must share the annual expenditure limit.'
            },
            {
              label: 'Qualifying corporations are eligible to claim the full OITC with a qualified ' +
              'expenditure limit of $3 million where their specified capital amount or their federal ' +
              'taxable income for the previous tax year is not more than $25 million and $500,000, ' +
              'respectively. If one of these amounts is more than the respective threshold, the $3 ' +
              'million limit is progressively reduced.'
            },
            {
              label: 'A corporation can waive its eligibility for all or part of the OITC by completing Part 7 of this schedule.'
            },
            {
              label: '<b>Expenditure limit, qualified expenditure</b>, and <b>eligible repayments</b> are defined in ' +
              'subsections 96(3), 96(3.1), as well as 96(8) and 96(12) of the <i>Taxation Act, 2007</i> ' +
              '(Ontario), respectively.'
            },
            {
              label: 'File this schedule with your <i>T2 Corporation Income Tax Return</i>'
            }
          ]
        }
      ],
      category: 'Ontario Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Eligibility',
        'rows': [
          {
            'type': 'infoField',
            'tn': '100',
            'num': '100',
            'label': '1. Did the corporation have a permanent establishment in Ontario at any time during the tax year?',
            'labelClass': 'tabbed',
            'labelWidth': '80%',
            'inputType': 'radio',
            'init': false
          },
          {
            'type': 'infoField',
            'tn': '105',
            'num': '105',
            'label': '2. Was the corporation exempt from tax for the tax year under Part III of the <i>Taxation Act, 2007</i> (Ontario)?',
            'labelClass': 'tabbed',
            'labelWidth': '80%',
            'inputType': 'radio',
            'init': false
          },
          {
            'type': 'infoField',
            'tn': '110',
            'num': '110',
            'label': '3. Did the corporation carry on SR&ED in Ontario during the tax year?',
            'labelClass': 'tabbed',
            'labelWidth': '80%',
            'inputType': 'radio',
            'init': false
          },
          {
            'type': 'infoField',
            'tn': '115',
            'num': '115',
            'label': '4. Is the corporation eligible to claim an investment tax credit under section 127 of the federal <i>Income Tax Act</i> on qualified expenditures made in the tax year?',
            'labelClass': 'tabbed',
            'labelWidth': '80%',
            'inputType': 'radio',
            'init': false
          },
          {
            'type': 'infoField',
            'tn': '120',
            'num': '120',
            'label': '5. Did the corporation file Form T661 in the tax year?',
            'labelClass': 'tabbed',
            'labelWidth': '80%',
            'inputType': 'radio',
            'init': false
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If you answered <b>yes</b> to question 2 or <b>no</b> to question 1, 3, 4, or 5, you are <b>not eligible</b> for the Ontario innovation tax credit.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - SR&ED qualified expenditure pool',
        'rows': [
          {
            'label': '<b>SR&ED qualified expenditure pool</b> *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '215',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '215'
                }
              }
            ],
            'indicator': 'A'
          },
          {labelClass: 'fullLength'},
          {
            'label': '* The SR&ED expenditure pool is not reduced for amounts considered to be specified contract payments. See Part 8 of this schedule. Include only Ontario qualified expenditures of a current nature and shared-use equipment acquired before 2014. Capital expenditures incurred after 2013 no longer qualify.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Eligible repayments',
        'rows': [
          {
            'label': 'Designated repayments made in the year of government or non-government assistance or contract payments relating to Ontario qualified expenditures of a current nature',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '300',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '300'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Designated repayments made in the year of government or non-government assistance or contract payments relating to Ontario qualified expenditures of a capital nature incurred before 2014',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '305',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '305'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': 'di_table_1'
          },
          {
            'type': 'table',
            'num': 'di_table_2'
          },
          {
            'label': '<b>Eligible repayments</b> (amount B <b>plus</b> amount C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '325',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '325'
                }
              }
            ],
            'indicator': 'D'
          },
          {labelClass: 'fullLength'},
          {'label': 'Note:'},
          {
            'label': 'Repayments are reported in Part 7 as follows:'
          },
          {
            'label': '-  at amount j, line 703, or line 709 (as applicable) for qualified expenditures incurred in tax years ending before June 1, 2016;',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '- at line 704 for qualified expenditures incurred in tax years starting after May 31, 2016; and',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '-  at line 708 for qualifying expenditures incurred in a tax year that ends on or after June 1, 2016, and includes May 31, 2016.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'header': 'Part 4 - Expenditure limit',
        'rows': [
          {
            'label': '<b>For a stand-alone corporation:</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '350'
                }
              }
            ]
          },
          {
            'label': 'Taxable income for the previous tax year (before any loss carrybacks being applied) *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '400',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '400'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': 'di_table_3'
          },
          {
            'label': 'Excess ($8,000,000 <b>minus</b> amount E) **',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '404'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'type': 'table',
            'num': '451'
          },
          {
            'label': '$25,000,000 <b>minus</b> amount d',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '410',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '410'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e**'
                }
              }
            ]
          },
          {
            'label': '<b>Expenditure limit for the stand-alone corporation:</b> (amount F x amount e) / 25,000,000 = ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '415',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '415'
                }
              }
            ],
            'indicator': 'G***'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* If any of the tax years referred to at line 400 is less than 51 weeks, <b>multiply</b> the taxable income by 365 and <b>divide</b> by the number of days in the tax year.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** If the result is negative, enter "0".'
          },
          {
            'label': '*** Amount G cannot be more than $3,000,000.'
          },
          {labelClass: 'fullLength'},
          {
            'label': '<b>For associated corporations:</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '419'
                }
              }
            ]
          },
          {
            'label': 'Total of all taxable incomes of the corporation and of its associated corporations <br>' +
            '(before any loss carrybacks being applied) for their last tax year-ending in the previous calendar year*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '420',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '420'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': 'di_table_4'
          },
          {
            'label': 'Excess ($8,000,000 <b>minus</b> amount H) **',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '424'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'type': 'table',
            'num': '453'
          },
          {
            'label': '$25,000,000 <b>minus</b> amount g',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '430',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '430'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'h**'
                }
              }
            ]
          },
          {
            'label': '<b>Expenditure limit for associated corporations:</b> (amount I x amount h) / 25,000,000 = ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '435',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '435'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': 'Enter amount J on line P in Part 6.'
          },
          {
            'label': 'Expenditure limit for the corporation (amount allocated from column 3 in Part 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '440',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '440'
                }
              }
            ],
            'indicator': 'K***'
          },
          {labelClass: 'fullLength'},
          {
            'label': '<b>Expenditure limit where the tax year of the stand-alone or associated corporation is less than 51 weeks:</b>',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Calculate the amount of the expenditure limit as follows:'
          },
          {
            'type': 'table',
            'num': '454'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* If any of the tax years referred to at line 420 is less than 51 weeks, <b>multiply</b> the taxable income by 365 and <b>divide</b> by the number of days in the tax year.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** If the result is negative, enter "0".'
          },
          {
            'label': '*** Amount K cannot be more than $3,000,000.'
          }
        ]
      },
      {
        'header': 'Part 5 - Calculation of the specified capital amount',
        'rows': [
          {
            'label': '<b>For stand-alone corporations</b> (see notes below):'
          },
          {
            'label': '<b>Specified capital amount</b> for the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '500',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '500'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'label': '<b>For associated corporations</b> (see notes below and subsection 96(4.1) of <i>Taxation Act, 2007</i> (Ontario)):',
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Specified capital amount for the corporation and each of its associated corporations</b> for their last tax year-ending in the previous calendar year (complete the table below)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '505',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '505'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '550'
          },
          {
            'label': 'Enter on line 505 the total specified capital amount in column 3 (amount O).'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Notes for stand-alone corporations and associated corporations</b>'
          },
          {
            'label': '1. If the corporation is an insurance corporation or a credit union for a tax year, enter the amount of the corporation\'s taxable capital employed in Canada for the applicable tax year, from line 590, 690, or 790 of Schedule 35, <i>Taxable Capital Employed in Canada - Large Insurance Corporations</i>, or line 690 of Schedule 34, <i>Taxable Capital Employed in Canada - Financial Institutions</i>.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '2. If the corporation is a financial institution, as defined in subsection 96(18) of the <i>Taxation Act, 2007</i> (Ontario), for a tax year, enter the amount of the corporation\'s adjusted taxable paid-up capital for the applicable tax year. You can use Schedule 514, <i>Ontario Capital Tax on Financial Institutions</i> to calculate this amount.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '3. For all other corporations, enter the amount of the corporation\'s taxable capital for the applicable tax year. You can use Schedule 515, <i>, Ontario Capital Tax on Other Than Financial Institutions </i> to calculate this amount.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'header': 'Part 6 - Agreement among associated corporations to allocate the expenditure limit',
        'rows': [
          {
            'type': 'table',
            'num': '650'
          },
          {
            'label': 'Enter on line 440 in Part 4 the expenditure limit allocated to the corporation in column 3.',
            'labelClass': 'fullLength'
          },
          {
            'label': '* Special rules apply if the corporation has more than one tax year-ending in a calendar year and is associated in more than one of those years with another corporation that has a tax year-ending in the same calendar year. In this case, the expenditure limit of the corporation for the second (and later) tax year(s) will be equal to the expenditure limit allocated for the first tax year-ending in the calendar year.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 7 - Calculation of the Ontario innovation tax credit',
        'rows': [
          {
            'label': '<b>For tax years ending before June 1, 2016</b>'
          },
          {
            'label': 'SR&ED qualified expenditure pool (line 215 in Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '790'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'i'
                }
              }
            ]
          },
          {
            'label': 'Eligible repayments (line 325 in Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '791'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'j'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount i <b>plus</b> amount j)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '700',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '700'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'k'
                }
              }
            ]
          },
          {
            'label': 'Expenditure limit (line 415, 440, or 445, whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '792'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'l'
                }
              }
            ]
          },
          {
            'label': 'Amount k or amount l, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '705',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '705'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'm'
                }
              }
            ]
          },
          {
            'label': '<b>Ontario innovation tax credit for tax years ending before June 1, 2016:</b> (amount m x 10%)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '710',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '710'
                }
              }
            ],
            'indicator': 'Q'
          },
          {labelClass: 'fullLength'},
          {
            'label': '<b>For a tax year that ends on or after June 1, 2016, and includes May 31, 2016</b>'
          },
          {
            'label': 'SR&ED qualified expenditure pool (line 215 in Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '793'
                }
              }
            ],
            'indicator': 'R'
          },
          {
            'label': 'Eligible repayments made in the year for tax years ending before June 1, 2016 <br>(line 325 in Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '703',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '703'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'n'
                }
              }
            ]
          },
          {
            label: 'Repayment for a tax year (transition year) that ends on or after June 1, 2016, and includes May 31, 2016. Complete the proration calculation below.',
            labelClass: 'fullLength'
          },
          {
            'type': 'table',
            'num': '750'
          },
          {
            'label': 'Subtotal (percentage 1 <b>plus</b> percentage 2)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '909',
                  'decimals': 5
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '%3'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '752'
          },
          {
            'label': 'Subtotal (amount R <b>plus</b> amount S)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '701',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '701'
                }
              }
            ],
            'indicator': 'T'
          },
          {
            'label': 'Expenditure limit (line 415, 440, or 445, whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '795'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'o'
                }
              }
            ]
          },
          {
            'label': 'Lesser of amount T and o',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '706',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '706'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'p'
                }
              }
            ]
          },
          {
            'label': '<b>Ontario innovation tax credit for a tax year that ends on or after June 1, 2016 and includes May 31, 2016:</b><br> (amount p <b>multiplied by</b> percentage 3).',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '711',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '711'
                }
              }
            ],
            'indicator': 'U'
          },
          {labelClass: 'fullLength'},
          {
            'label': '<b>For tax years that start after May 31, 2016</b>'
          },
          {
            'label': 'SR&ED qualified expenditure pool (line 215 in Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '780'
                }
              }
            ],
            'indicator': 'V'
          },
          {
            'label': 'Eligible repayments made in the year for tax years starting after May 31, 2016 (see note in Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '704',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '704'
                }
              }
            ],
            'indicator': 'W'
          },
          {
            'label': 'Eligible repayments made for a tax year (the transition year) that ends on or after June 1, 2016,' +
            ' and includes May 31, 2016 (see note in Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '708',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '708'
                }
              }
            ],
            'indicator': 'q'
          },
          {
            label: 'Repayment made in the year for a tax year (transition year) that ends on or after June 1, 2016, ' +
            'and includes May 31, 2016.<br> Complete the proration calculation below.',
            labelClass: 'fullLength '
          },
          {
            'type': 'table',
            'num': '753'
          },
          {
            'type': 'table',
            'num': '755'
          },
          {
            'type': 'table',
            'num': 'di_table_5'
          },
          {
            'label': 'Subtotal (total of amounts V, W, X, and Y)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '702',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '702'
                }
              }
            ],
            'indicator': 'Z'
          },
          {
            'label': 'Expenditure limit (line 415, 440, or 445, whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '783'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'r'
                }
              }
            ]
          },
          {
            'label': 'Lesser of Z and r',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '707',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '707'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 's'
                }
              }
            ]
          },
          {
            'label': '<b>Ontario innovation tax credit for tax years starting after May 31, 2016</b> (amount s x 8%)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '712',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '712'
                }
              }
            ],
            'indicator': 'AA'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'tn': '715',
            'num': '715',
            'label': 'Are you waiving all or part of the OITC?',
            'labelClass': 'tabbed',
            'labelWidth': '80%',
            'inputType': 'radio',
            'init': 2
          },
          {
            'label': 'If you answered <b>yes</b> at line 715, enter the amount of the tax credit waived on line 720.'
          },
          {
            'label': 'If you answered <b>no</b> at line 715, enter "0" on line 720.'
          },
          {
            'label': 'Waiver of the tax credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '720',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '720'
                }
              }
            ],
            'indicator': 'BB'
          },
          {
            'label': 'Amount 710, 711, and 712, whichever applies',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '784'
                }
              }
            ]
          },
          {
            'label': '<b>Ontario innovation tax credit claimed</b> (amount 710, 711, and 712, whichever applies, <b>minus</b> BB)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '785'
                }
              }
            ],
            'indicator': 'CC'
          },
          {
            'label': 'Enter amount CC on line 468 on page 5 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>.'
          }
        ]
      },
      {
        'header': 'Part 8 - Specified contract payments',
        'rows': [
          {
            'label': '• Specified contract payments, as defined in subsection 96(11) of the <i>Taxation Act, 2007</i> (Ontario), are contract payments received for the performance of SR&ED carried on in Ontario by a payor corporation that does not have a permanent establishment in Ontario and is not entitled to claim the OITC.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• According to subsection 96(9) of the <i>Taxation Act, 2007</i> (Ontario), the recipient does not have to deduct the specified contract payment from its SR&ED qualified expenditure pool.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• Specified contract payments include all amounts that are received, receivable, or reasonably expected to be received by the corporation',
            'labelClass': 'fullLength tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Note</b>'
          },
          {
            'label': 'A corporation cannot claim SR&ED credits for contract payments received from another corporation that are not specified contract payments. <br>These payments, if eligible, would be claimed by the corporation making the payments.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Provide details of specified contract payments received for which the OITC is being claimed:'
          },
          {
            'type': 'table',
            'num': '800'
          },
          {
            'type': 'table',
            'num': '850'
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  });
})();
