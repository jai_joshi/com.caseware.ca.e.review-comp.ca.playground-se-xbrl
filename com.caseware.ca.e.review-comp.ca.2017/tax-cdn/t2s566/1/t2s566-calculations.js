(function() {

  wpw.tax.create.calcBlocks('t2s566', function(calcUtils) {
    calcUtils.calc(function(calcUtils) {
      var num = '310';
      var num2 = '315';
      var midnum = '311';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '331';
      var num2 = '320';
      var midnum = '332';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get();
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '401';
      var num2 = '403';
      var midnum = '402';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get();
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '421';
      var num2 = '423';
      var midnum = '422';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get();
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '709';
      var num2 = '930';
      var midnum = '929';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get();
      calcUtils.field(num2).assign(product);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //update from rate table
      calcUtils.getGlobalValue('311', 'ratesOn', '791');
      calcUtils.getGlobalValue('332', 'ratesOn', '792');
      calcUtils.getGlobalValue('350', 'ratesOn', '799');
      calcUtils.getGlobalValue('402', 'ratesOn', '794');
      calcUtils.getGlobalValue('419', 'ratesOn', '799');
      calcUtils.getGlobalValue('422', 'ratesOn', '794');
      calcUtils.getGlobalValue('902', 'ratesOn', '796');
      calcUtils.getGlobalValue('906', 'ratesOn', '801');
      calcUtils.getGlobalValue('916', 'ratesOn', '796');
      calcUtils.getGlobalValue('920', 'ratesOn', '801');
      calcUtils.getGlobalValue('927', 'ratesOn', '801');
      calcUtils.getGlobalValue('929', 'ratesOn', '802');

      //Part 2
      //TODO: line 215 (t2s508 not available now)

      //Part 3
      calcUtils.multiply('315', ['310', '311'], (1 / 100));
      field('331').assign(field('305').get() + field('315').get());
      calcUtils.multiply('320', ['331', '332']);
      field('325').assign(field('300').get() + field('320').get());

      //Part 4
      //TODO: line 400
      var s23TableLength = field('t2s23.085').size().rows;
      var isAssociatedCorp = (s23TableLength > 1);

      //part 4
      //for a stand-alone corp
      field('401').assign(Math.max(field('400').get(), 500000));
      calcUtils.multiply('403', ['401', '402']);
      field('404').assign(Math.max(field('350').get() - field('403').get(), 0));
      calcUtils.equals('406', '500');
      field('405').assign(Math.max(field('406').get() - 25000000, 0));
      field('410').assign(Math.max(25000000 - field('405').get(), 0));
      field('415').assign(!isAssociatedCorp ? (field('404').get() * field('410').get() / 25000000) : 0);

      //for associated corps
      field('421').assign(Math.max(field('420').get(), 500000));
      calcUtils.multiply('423', ['421', '422']);
      field('424').assign(Math.max(field('419').get() - field('423').get(), 0));
      field('426').assign(field('505').get());
      field('425').assign(Math.max(field('426').get() - 25000000, 0));
      // field('430').assign(Math.max(25000000 - field('425').get(), 0));//applies only when associated corporation. error for CRA test. comment out for now
      field('435').assign(isAssociatedCorp ? Math.min(field('424').get() * field('430').get() / 25000000, 3000000) : 0);

      calcUtils.getApplicableValue('441', ['415', '440']);
      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();
      field('442').assign(daysFiscalPeriod);
      field('443').assign(365);
      field('445').assign((daysFiscalPeriod < 357) ? (field('441').get() * field('442').get() / field('443').get()) : 0);

      //Part 5
      //TODO: line 500

      // var linkedTable = form('entityProfileSummary').field('566');
      // field('550').getRows().forEach(function(row, rIndex) {
      //   var linkedRow = linkedTable.getRow(rIndex);
      //   var part6Row = field('650').getRow(rIndex);
      //   //part 5 table
      //   row[0].assign(linkedRow[0].get());
      //   row[0].source(linkedRow[0]);
      //   row[1].assign(linkedRow[1].get());
      //   row[1].source(linkedRow[1]);
      //   row[2].assign(linkedRow[2].get());
      //   row[2].source(linkedRow[2]);
      //   //part 6 table
      //   part6Row[0].assign(linkedRow[0].get());
      //   part6Row[0].source(linkedRow[0]);
      //   part6Row[1].assign(linkedRow[1].get());
      //   part6Row[1].source(linkedRow[1]);
      //   part6Row[2].assign(linkedRow[3].get());
      //   part6Row[2].source(linkedRow[3]);
      //   part6Row[3].assign(linkedRow[4].get());
      //   part6Row[3].source(linkedRow[4]);
      // });
      //
      // field('505').assign(field('550').total(2).get()); //commented it for CRA approval for now

      //part 7
      calcUtils.equals('790', '215');
      calcUtils.equals('791', '325');

      var taxStart = field('CP.tax_start').get();
      var taxEnd = field('CP.tax_end').get();

      var juneDate = wpw.tax.date(2016, 6, 1);
      var mayDate = wpw.tax.date(2016, 5, 31);
      var taxDays = field('CP.Days_Fiscal_Period').get();

      var isBeforeJuneDate = calcUtils.dateCompare.lessThan(taxEnd, juneDate);

      field('700').assign(isBeforeJuneDate ? field('790').get() + field('791').get() : 0);
      field('705').assign(isBeforeJuneDate ? Math.min(field('700').get(), field('792').get()) : 0);
      field('710').assign(isBeforeJuneDate ? (field('705').get() / 10) : 0);

      calcUtils.getApplicableValue('792', ['415', '440', '445']);

      var isAfterJuneDate = calcUtils.dateCompare.lessThan(juneDate, taxEnd);
      var isIncludeMayDate = calcUtils.dateCompare.betweenInclusive(mayDate, taxStart, taxEnd);
      var isAfterAndIncludeDatePeriod = (isAfterJuneDate && isIncludeMayDate);

      calcUtils.equals('793', '215');
      calcUtils.equals('703', '325');

      var dateComparisons = calcUtils.compareDateAndFiscalPeriod(juneDate);

      field('901').assign(dateComparisons.daysBeforeDate);
      field('904').assign(taxDays);
      field('903').assign(field('901').get() / field('904').get() * field('902').get());

      field('905').assign(dateComparisons.daysAfterDate);
      field('908').assign(taxDays);
      field('907').assign(field('905').get() / field('908').get() * field('906').get());
      field('909').assign(field('903').get() + field('907').get());

      calcUtils.equals('910', '703');
      field('912').assign(0.1);
      calcUtils.equals('914', '909');
      field('913').assign(field('910').get() * field('912').get() / (field('914').get() / 100));
      field('701').assign(field('793').get() + field('913').get());
      calcUtils.getApplicableValue('795', ['415', '440', '445']);
      field('706').assign(Math.min(field('701').get(), field('795').get()));
      field('711').assign(isAfterAndIncludeDatePeriod ? (field('706').get() * field('909').get() / 100) : 0);

      var isStartAfterMayDate = calcUtils.dateCompare.lessThan(mayDate, taxStart);

      if (field('709').get() > 0) {
        field('716').assign(field('901').get());
        field('718').assign(taxDays);
        field('717').assign(field('905').get());
        field('922').assign(taxDays);
      }
      else {
        field('716').assign(0);
        field('718').assign(0);
        field('717').assign(0);
        field('922').assign(0);
      }
      field('780').assign(isStartAfterMayDate ? field('215').get() : 0);
      field('917').assign(field('718').get() == 0 ? 0 : (field('716').get() / field('718').get() * field('916').get()));

      field('921').assign(field('922').get() == 0 ? 0 : (field('717').get() / field('922').get() * field('920').get()));
      field('923').assign(field('917').get() + field('921').get());

      calcUtils.equals('924', '708');
      calcUtils.equals('925', '923');
      field('926').assign(field('924').get() * field('925').get() / field('927').get());
      field('930').assign(field('709').get() * field('929').get());
      calcUtils.sumBucketValues('702', ['780', '704', '926', '930']);
      calcUtils.getApplicableValue('783', ['415', '440', '445']);
      field('707').assign(Math.min(field('702').get(), field('783').get()));
      field('712').assign(isStartAfterMayDate ? (field('707').get() * field('920').get() / 100) : 0);

      if (field('715').get() == 2) {
        field('720').assign(0)
      } else {
        field('720').disabled(false)
      }
      calcUtils.getApplicableValue('784', ['710', '711', '712']);

      //Condition 100
      var jurisdiction = field('CP.750').get();
      field('100').assign((jurisdiction === 'ON' || (jurisdiction === 'MJ' && field('T2S5.013').get())) ? 1 : 2);

      //Condition 105
      field('105').assign((field('CP.2270').get() == 1) && (field('CP.085').get() == 3) ? 1 : 2);

      //Condition 115
      field('115').assign(!!field('T2S31.380').get() ? 1 : 2);

      //Condition 120
      field('120').assign(!!field('T2J.232').get() ? 1 : 2);

      //Eligibility
      var isEligible = !(field('100').get() == 2 || field('105').get() == 1 || field('CP.110').get() == 2 || field('115').get() == 2 || field('120').get() == 2);

      field('785').assign(isEligible ? (field('784').get() - field('720').get()) : 0);
    });


  });
})();
