(function() {
  wpw.tax.global.tableCalculations.t2s394 = {
    '150': {
      showNumbering: true,
      maxLoop: 100000,
      hasTotals: true,
      columns: [
        {
          header: 'A <br> Certificate number of eligible rental housing project',
          tn: '100',
          "validate": {"or": [{"length": {"min": "9", "max": "9"}}, {"check": "isEmpty"}]},
          type: 'text'
        },
        {
          header: 'B <br> Capital cost of eligible rental housing project*',
          tn: '110', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
        },
        {
          header: 'C <br> Number of residential units in eligible rental housing project',
          tn: '120',
          "validate": {"or": [{"length": {"min": "1", "max": "6"}}, {"check": "isEmpty"}]},
          type: 'text'
        },
        {
          header: 'D <br> Column B <b>multiplied</b> by 8%',
          tn: '130', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
        },
        {
          header: 'E <br> Column C <b>multiplied</b> by $12,000',
          tn: '140', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
        },
        {
          header: 'F <br> MRHCTC for each eligible rental housing project (column D or E, whichever is less)',
          tn: '150', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          total: true,
          totalNum: '151',
          totalMessage: ' '
        }
      ]
    },
    '200': {
      repeats: ['300'],
      maxLoop: 100000,
      showNumbering: true,
      columns: [
        {
          header: 'H <br> Certificate number of eligible rental housing project',
          tn: '200',
          "validate": {"or": [{"length": {"min": "9", "max": "9"}}, {"check": "isEmpty"}]},
          type: 'text'
        },
        {
          header: 'I <br> Capital cost of eligible rental housing project*',
          tn: '210', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
        },
        {
          header: 'J <br> Number of residential units in eligible rental housing project',
          tn: '220',
          "validate": {"or": [{"length": {"min": "1", "max": "6"}}, {"check": "isEmpty"}]}
        },
        {
          header: 'K <br> Number of months in the tax year that the project is an eligible rental housing project',
          tn: '225', "validate": {"or": [{"length": {"min": "1", "max": "2"}}, {"check": "isEmpty"}]}
        }
      ]
    },
    '300': {
      fixedRows: true,
      keepButtonsSpace: true,
      showNumbering: true,
      maxLoop: 100000,
      hasTotals: true,
      columns: [
        {
          header: 'L <br> Column I <b>multiplied</b> by 8%',
          tn: '230', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: 'M <br> Column J <b>multiplied</b> by $12,000',
          tn: '240', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: 'N <br> Lesser of column L and column M divided by 60'
        },
        {
          header: 'O <br> MRHCTC for each eligible rental housing project (column K <b>multiplied</b> by column N)',
          tn: '250', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          total: true,
          totalNum: '250',
          totalMessage: ' '
        }
      ]
    },
    '400': {
      'fixedRows': true,
      hasTotals: true,
      infoTable: true,
      'columns': [
        {
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '160px',
          'type': 'date',
          'disabled': true,
          header: '<b>Year of origin</b>'
        },
        {
          'type': 'none'
        },
        {
          'width': '160px',
          header: '<b>Credit available for carryforward</b>',
          'total': true,
          'totalNum': '401',
          'totalMessage': 'Total (equals line 295 in Part 2)',
          colClass: 'std-input-width'
        },
        {type: 'none'}
      ],
      'cells': [
        {
          '0': {
            'label': '10th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '9th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '8th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '7th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '6th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '5th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '4th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '3rd previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '2nd previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '1st previous tax year ending on'
          }
        },
        {
          '0': {
            label: 'Current tax year ending on'
          }
        }]
    },
    '500': {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {colClass: 'std-padding-width', type: 'none'},
        {
          colClass: 'std-input-width',
          type: 'date',
          disabled: true,
          header: '<b>Year of origin</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '501',
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '502',
          totalMessage: ' ',
          header: '<b>Current year contribution</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '503',
          totalMessage: ' ',
          header: '<b>Transfers</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', totalNum: '504',
          header: '<b>Amount available to apply</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '505',
          header: '<b>Applied<b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '506',
          header: '<b>Balance to carry forward</b>'
        },
        {colClass: 'std-padding-width', type: 'none'}
      ],
      cells: [
        {
          '4': {label: '*'},
          '5': {type: 'none'},
          '7': {type: 'none'},
          '9': {type: 'none'},
          '11': {type: 'none'},
          '13': {type: 'none', label: 'N/A'}
        },
        {
          '5': {type: 'none'},
          '14': {label: '**'}
        },
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {'5': {type: 'none'}},
        {
          '3': {type: 'none'},
          '7': {type: 'none'}
        }
      ]
    }

  }
})();
