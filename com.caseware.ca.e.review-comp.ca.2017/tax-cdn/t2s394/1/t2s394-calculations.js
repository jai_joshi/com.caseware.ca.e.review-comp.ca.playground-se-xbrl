(function() {

  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  wpw.tax.create.calcBlocks('t2s394', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {

      //to get year for table summary
      var taxYearSummaryTable = field('tyh.200');
      var tableArray = [400, 500];
      tableArray.forEach(function(num) {
        field(num).getRows().forEach(function(row, rowIndex) {
          row[1].assign(taxYearSummaryTable.getRow(rowIndex + 10)[6].get())
        })
      });
    });

    calcUtils.calc(function(calcUtils, field) {
      //part1 calc
      field('150').getRows().forEach(function(row) {
        row[3].assign(row[1].get() * 0.08);
        row[4].assign(row[2].get() * 12000);
        row[5].assign(Math.min(row[4].get(), row[3].get()))
      });
      field('190').assign(field('151').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part2
      field('200').getRows().forEach(function(row, rowIndex) {
        var table300 = field('300').getRow(rowIndex);
        table300[0].assign(row[1].get() * 0.08);
        table300[1].assign(row[2].get() * 12000);
        table300[2].assign((Math.min(table300[0].get(), table300[1].get())) / 60);
        table300[3].assign(row[3].get() * table300[2].get());
      });
      field('260').assign(field('250').get());
      field('261').assign(field('501').get());
      field('270').assign(field('500').cell(0, 3).get());
      field('275').assign(field('261').get() - field('270').get());
      field('276').assign(field('275').get());
      field('280').assign(field('503').get());
      calcUtils.sumBucketValues('281', ['260', '276', '280']);
      field('290').assign(Math.min(field('281').get(), field('T2S383.301').get()));
      field('295').assign(field('281').get() - field('290').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 3
      var summaryTable = field('500');
      field('400').getRows().forEach(function(row, rowIndex) {
        row[3].assign(summaryTable.getRow(rowIndex)[13].get())
      });

      // historical data chart
      var limitOnCredit = field('290').get();
      summaryTable.getRows().forEach(function(row, rowIndex) {
        if (rowIndex == 0) {
          // to avoid the first row being calculated to total
          row[7].assign(0);
          row[9].assign(0);
          row[11].assign(0);
          row[13].assign(0);
        } else {
          row[9].assign(Math.max(row[3].get() + row[5].get() + row[7].get(), 0));
          row[11].assign(returnAmountApplied(limitOnCredit, row[9].get()));
          row[13].assign(Math.max(row[9].get() - row[11].get(), 0));
          limitOnCredit -= row[11].get();
        }
      });
      summaryTable.cell(10, 5).assign(field('260').get())
    });

  });
})();
