(function() {
  'use strict';

  wpw.tax.global.formData.t2s394 = {
    formInfo: {
      abbreviation: 't2s394',
      title: 'Manitoba Rental Housing Construction Tax Credit ',
      //subTitle: '(2013 and later tax years)',
      schedule: 'Schedule 394',
      code: 'Code 1303',
      formFooterNum: 'T2 SCH 394 E (15)',
      headerImage: 'canada-federal',
      showCorpInfo: true,
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'You can use this schedule to claim a Manitoba rental housing construction tax credit' +
              ' (MRHCTC) under section 10.6 of the <i>Income Tax Act</i> (Manitoba).'
            },
            {
              label: ' A qualifying entity can claim a refundable MRHCTC for a tax year equal to the lesser of:',
              sublist: [
                '8% of the qualifying entity\'s capital cost of an eligible rental housing project ' +
                'that became available for use in the tax year; and',
                '$12,000 multiplied by the number of residential units in the rental housing project.'
              ]
            },
            {
              label: 'A qualifying corporation can claim a non-refundable MRHCTC for a tax year equal to the lesser of:',
              sublist: [
                '8% of the capital cost of the project <b>multiplied</b> by the number of months in ' +
                'the tax year that the project is an eligible rental housing project <b>divided</b> by 60; and',
                '$12,000 <b>multiplied</b> by the number of residential units in the project, <b>multiplied</b> ' +
                'by the number of months in the tax year that the project is an eligible rental housing project' +
                '<b> divided </b>by 60',
                'Any unused non-refundable MRHCTC can be carried forward up to 10 years.'
              ]
            },
            {
              label: 'You are a <b>qualifying entity</b> if:',
              sublist: [
                'you are a housing corporation described in paragraph 149(1)(i) of the federal <i>Income Tax Act</i>',
                'you are a non-profit organization described in paragraph 149(1)(l) of the federal <i>Income Tax Act</i>',
                'you are a limited dividend housing company described in paragraph 149(1)(n)' +
                ' of the federal <i>Income Tax Act</i>; or',
                'you are a not for profit housing cooperative as described in subsection 275(2) of' +
                '<i> The Cooperatives Act</i> (Manitoba).'
              ]
            },
            {
              label: 'You are a <b>qualifying corporation</b> if you are a taxable Canadian corporation with' +
              ' a permanent establishment in Manitoba that is <b>not</b> a qualifying entity ' +
              'for the purposes of claiming this credit.'
            },
            {
              label: 'An <b>eligible rental housing project</b> is a rental housing project defined under ' +
              'subsection 10.6(1) of the <i>Income Tax Act</i> (Manitoba) that becomes available for use before 2020, ' +
              'and has been certified by the Minister of Housing and Community Development for Manitoba.'
            },
            {
              label: 'For qualifying corporations, the non-refundable MRHCTC is considered government assistance' +
              ' under paragraph 12(1)(x) of the federal <i>Income Tax Act</i> and <b>must</b> be included in income' +
              ' in the tax year it is received. The credit is <b>not</b> considered government assistance ' +
              'under section 10.6 of the <i>Income Tax Act</i> (Manitoba) for calculating the credit itself.'
            },
            {
              label: 'To claim the credit, the property has to be <b>available for use</b> in the tax year,' +
              ' according to subsections 13(27) and 13(28) of the federal <i>Income Tax Act</i>, <b>not including</b>' +
              ' the time just before you dispose of it under paragraphs 13(27)(c) and 13(28)(d).'
            },
            {
              label: 'When calculating your MRHCTC for a tax year, do not include any amount you used in' +
              ' claiming any other tax credit under the <i>Income Tax Act</i> (Manitoba).'
            },
            {
              label: 'Include a completed copy of this schedule with your T2 Corporation <i>Income Tax Return</i>. ' +
              'Keep a copy of the certificate(s) to support your claim but do not include the certificate(s) with your T2 return.'
            }
          ]
        }
      ],
      category: 'Manitoba Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Manitoba refundable rental housing construction tax credit',
        'rows': [
          {
            'label': 'Complete this part only if you are a qualifying entity.'
          },
          {
            'type': 'table',
            'num': '150'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Manitoba refundable rental housing construction tax credit (total of column F) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '190',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,15}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '190'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Enter amount G on line 326 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>.'
          },
          {
            'label': '* When you calculate the capital cost of an eligible rental housing project, deduct the amount of any government assistance received or receivable. ',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 – Manitoba rental housing construction tax credit',
        'rows': [
          {
            label: 'Complete this part only if you are a qualifying corporation.',
            labelClass: 'fullLength'
          },
          {
            'type': 'table',
            'num': '200'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '300'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total credit earned in the current tax year (total of column O)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '260',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,15}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '260'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Add',
            'labelClass': 'bold'
          },
          {
            'label': 'Unused credit at the end of the previous tax year ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '261'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': '<b>Deduct</b>: Credit expired after 10 tax years',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '270',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,15}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '270'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': 'Unused credit at the beginning of this tax year (amount a <b>minus</b> amount b)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '275',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,15}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '275'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '276',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,15}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'Q'
          },
          {
            'label': 'Credit transferred on an amalgamation or the windup of a subsidiary',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '280',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,15}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '280'
                }
              }
            ],
            'indicator': 'R'
          },
          {
            'label': 'Total credit available for the current tax year (total of amounts P to R)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '281'
                }
              }
            ],
            'indicator': 'S'
          },
          {
            'label': 'Deduct: Manitoba rental housing construction tax credit claimed in the current year**',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '290',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,15}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '290'
                }
              }
            ],
            'indicator': 'T'
          },
          {
            'label': 'Enter amount T on line 602 of Schedule 5.'
          },
          {
            'label': '<b>Closing balance for carryforward</b> (amount S <b>minus</b> amount T)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '295',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,15}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '295'
                }
              }
            ],
            'indicator': 'U'
          },
          {
            'label': '* When you calculate the capital cost of an eligible rental housing project, deduct the amount of any government assistance received or receivable. <br> ** The credit claimed in the current tax year cannot exceed the Manitoba tax otherwise payable or amount S, whichever is less. ',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Analysis of credit available for carryforward by year of origin',
        'rows': [
          {
            'label': 'If you are a qualifying corporation, you can complete this part to show all the credits from previous tax years available for carryforward, by year of origin. This will help you determine the amount of credit that could expire in future years.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '400'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The amount available from the 10th previous year will expire after this tax year. When you file your return for the next year, you will enter the expired amount on line 270 of Schedule 394 for that year.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for MRHCTC',
        'rows': [
          {
            'type': 'table',
            'num': '500'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  };
})();
