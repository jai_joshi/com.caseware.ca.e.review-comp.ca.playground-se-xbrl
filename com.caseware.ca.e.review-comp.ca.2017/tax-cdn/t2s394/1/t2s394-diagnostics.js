(function() {
  wpw.tax.create.diagnostics('t2s394', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('3940001', common.prereq(common.check(['t2s5.326', 't2s5.602'], 'isNonZero'),
        common.requireFiled('T2S394')));

    diagUtils.diagnostic('3940002', common.prereq(common.requireFiled('T2S394'),
        function(tools) {
      var table = tools.field('150');
      return tools.checkAll(table.getRows(), function(row) {
        if (row[5].isNonZero())
          return tools.requireAll([row[0], row[1], row[2]], 'isNonZero');
        else return true;
      });
    }));

    diagUtils.diagnostic('3940003', common.prereq(common.requireFiled('T2S394'),
        function(tools) {
      var table = tools.mergeTables(tools.field('200'), tools.field('300'));
      return tools.checkAll(table.getRows(), function(row) {
        if (row[7].isNonZero())
          return tools.requireAll([row[0], row[1], row[2]], 'isNonZero');
        else return true;
      });
    }));

    diagUtils.diagnostic('3941006', common.prereq(common.requireFiled('T2S394'),
        function(tools) {
      var table = tools.field('150');
      return tools.checkAll(table.getRows(), function(row) {
        if (row[2].isNonZero())
          return row[2].require(function() {
            return this.get() > 4;
          });
        else return true;
      });
    }));

    diagUtils.diagnostic('3941020', common.prereq(common.requireFiled('T2S394'),
        function(tools) {
      var table = tools.field('200');
      return tools.checkAll(table.getRows(), function(row) {
        if (row[2].isNonZero())
          return row[2].require(function() {
            return this.get() > 4;
          });
        else return true;
      });
    }));
  });
})();

