(function() {
  wpw.tax.create.tables('t2s1263', {
    '101': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{}],
      'cells': [{
        '0': {
          'label': 'Name of the third party',
          'num': '701',"validate": {"or":[{"length":{"min":"1","max":"60"}},{"check":"isEmpty"}]},
          'tn': '701',
          type: 'text'
        }
      }]
    },
    '107': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        colClass: 'std-padding-width',
        'type': 'none'
      },
        {
          cellClass: 'alignLeft',
          'width': '30%'
        },
        {
          'type': 'none'
        }],
      'cells': [{
        '0': {
          'tn': '704'
        },
        '1': {
          'label': 'Total amount paid in the year',
          'type': 'none'
        }
      },
        {
          '0': {
            'label': '$'
          },
          '1': {
            'num': '704',"validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          }
        }]
    },
    '706': {
      'infoTable': true,
      'tn': '706',
      'showNumbering': true,
      'labelAfterNum': true,
      'columns': [{
        'header': 'Project title (and identification code if applicable)',
        cellClass: 'alignLeft',
        'tn': '706',"validate": {"or":[{"length":{"min":"1","max":"60"}},{"check":"isEmpty"}]},
        type: 'text'
      }]
    },
    '710':{
      infoTable: true,
      fixedRows: true,
      columns:[
        {
          colClass: 'std-padding-width', type: 'none'
        },
        {type: 'none'},
        {
          colClass: 'small-input-width',
          cellClass: 'alignRight',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        }
      ],
      cells: [
        {
          '0':{
            tn: '711'
          },
          '1':{
            label: 'Approved association'
          },
          '2':{
            label: 'Yes'
          },
          '3':{
            type: 'singleCheckbox',
            num: '711'
          }
        },
        {
          '0':{
            tn: '712'
          },
          '1':{
            label: 'Non-profit SR&ED corporation resident in Canada'
          },
          '2':{
            label: 'Yes'
          },
          '3':{
            type: 'singleCheckbox',
            num: '712'
          }
        },
        {
          '0':{
            tn: '714'
          },
          '1':{
            label: 'An approved university, college, research institute, or other similar institution'
          },
          '2':{
            label: 'Yes'
          },
          '3':{
            type: 'singleCheckbox',
            num: '714'
          }
        },
        {
          '0':{
            tn: '716'
          },
          '1':{
            label: 'Granting council'
          },
          '2':{
            label: 'Yes'
          },
          '3':{
            type: 'singleCheckbox',
            num: '716'
          }
        },
        {
          '0':{
            tn: '718'
          },
          '1':{
            label: 'Other corporation resident in Canada'
          },
          '2':{
            label: 'Yes'
          },
          '3':{
            type: 'singleCheckbox',
            num: '718'
          }
        },
        {
          '0':{
            tn: '721'
          },
          '1':{
            label: 'Are you dealing at arm\'s length with the recipient?'
          },
          '3':{
            type: 'radio',
            num: '721'
          }
        }
      ]
    },
    '730':{
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-padding-width', type: 'none'
        },
        {type: 'none'},
        {
          colClass: 'small-input-width',
          cellClass: 'alignRight',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        }
      ],
      cells: [
        {
          '0':{
            tn: '731'
          },
          '1':{
            label: 'Experimental development'
          },
          '2':{
            label: 'Yes'
          },
          '3':{
            type: 'singleCheckbox',
            num: '731'
          }
        },
        {
          '0':{
            tn: '732'
          },
          '1':{
            label: 'Applied research'
          },
          '2':{
            label: 'Yes'
          },
          '3':{
            type: 'singleCheckbox',
            num: '732'
          }
        },
        {
          '0':{
            tn: '734'
          },
          '1':{
            label: 'Basic Research'
          },
          '2':{
            label: 'Yes'
          },
          '3':{
            type: 'singleCheckbox',
            num: '734'
          }
        }
      ]
    },
    '1001':{
      fixedRows: true,
      infoTable: true,
      noSequenceNum: true,
      columns:[
        {
          type: 'address', tn: '702'
        }
      ]
    }

  })
})();
