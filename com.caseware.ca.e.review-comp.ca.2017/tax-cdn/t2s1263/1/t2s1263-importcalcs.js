wpw.tax.create.importCalcs('t2s1263', function(tools) {
  //tn711 to tn718 importCalc
  var typeOfEntity = {
    1: '711',
    2: '712',
    3: '714',
    4: '716',
    5: '718'
  };
  tools.intercept('FDRRDX.FED.Ttwrdx20', function(importObj) {
    var repeatIndex = importObj.repeatIndex;
    tools.form('t2s1263', repeatIndex).setValue(typeOfEntity[importObj.value], true)
  });

  //part2 description importCalc
  function initializeTextArea() {
    var textAreaContents = [];
    textAreaContents[0] = textAreaContents[0] || [];
    textAreaContents[1] = textAreaContents[1] || [];
    textAreaContents[2] = textAreaContents[2] || [];
    return textAreaContents;
  }

  function getTextArea(taxPrepId, rowIndex, textAreaContents) {
    tools.intercept(taxPrepId, function(importObj) {
      textAreaContents.maxRepeatIndex = textAreaContents.maxRepeatIndex || importObj.repeatIndex;
      textAreaContents.maxRepeatIndex = Math.max(textAreaContents.maxRepeatIndex, importObj.repeatIndex);
      textAreaContents[rowIndex][importObj.repeatIndex] = importObj.value;
    });
  }

  var ensureString = function(val) {
    if (!val) {
      return '';
    } else {
      return val;
    }
  };

  function runToolsAfter(fieldId, textAreaContents) {
    tools.after(function() {
      if (!textAreaContents || textAreaContents.maxRepeatIndex === null) {
        return;
      }
      for (var i = 0; i <= textAreaContents.maxRepeatIndex; i++) {
        var explanation = ensureString(textAreaContents[0][i]) + ' ' + ensureString(textAreaContents[1][i]) + ' ' + ensureString(textAreaContents[2][i]);
        tools.form('t2s1263', i).setValue(fieldId, explanation);
      }
    })
  }

  //tn736
  var text1 = initializeTextArea();
  getTextArea('FDRRDX.FED.Ttwrdx8', 0, text1);
  getTextArea('FDRRDX.FED.Ttwrdx9', 1, text1);
  getTextArea('FDRRDX.FED.Ttwrdx27', 2, text1);
  runToolsAfter('736', text1);
  //tn738
  var text2 = initializeTextArea();
  getTextArea('FDRRDX.FED.Ttwrdx11', 0, text2);
  getTextArea('FDRRDX.FED.Ttwrdx12', 1, text2);
  getTextArea('FDRRDX.FED.Ttwrdx29', 2, text2);
  runToolsAfter('738', text2);
  //tn740
  var text3 = initializeTextArea();
  getTextArea('FDRRDX.FED.Ttwrdx14', 0, text3);
  getTextArea('FDRRDX.FED.Ttwrdx15', 1, text3);
  getTextArea('FDRRDX.FED.Ttwrdx31', 2, text3);
  runToolsAfter('740', text3);

});
