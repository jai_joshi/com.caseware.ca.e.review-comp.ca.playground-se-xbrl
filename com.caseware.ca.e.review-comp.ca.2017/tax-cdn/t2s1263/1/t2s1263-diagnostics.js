(function() {
  wpw.tax.create.diagnostics('t2s1263', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0300001', common.prereq(common.and(
        common.requireFiled('T2S1263'),
        function(tools) {
          var table = tools.field('706');
          return tools.checkMethod(tools.list(['701', '1000', '1001', '1002', '1003', '1004', '1005', '704', '706', '711',
                '712', '714', '716', '718', '721', '731', '732', '734', '736', '738', '740']), 'isNonZero') || !!table.getRow(0)[0].get();
        }), common.check('t661.370', 'isNonZero')));

  });
})();
