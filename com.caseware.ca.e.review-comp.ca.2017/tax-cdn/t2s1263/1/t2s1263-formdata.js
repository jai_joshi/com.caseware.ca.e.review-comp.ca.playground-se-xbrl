(function() {
  wpw.tax.create.formData('t2s1263', {
    formInfo: {
      isRepeatForm: true,
      abbreviation: 'T2S1263',
      schedule: 'Schedule 30',
      formFooterNum: 'T1263 E (15)',
      hideHeaderBox: true,
      repeatFormData: {
        titleNum: '701'
      },
      title: 'Third-party payments for scientific research and experimental development',
      headerImage: 'canada-federal',
      code: 'Code 0801',
      highlightFieldSets: true,
      description: [
        {text: 'Complete this form for each third-party payment and attach it to Form T661'},
        {text: 'For more information on third-party payments:'},
        {
          type: 'list', items: [
          'See line 370 of Guide to Form T661, <i>Scientific Research and Experimental Development' +
          ' (SR&ED) Expenditures Claim</i>;',
          'Consult our Web site: ' + ' <b>www.cra.gc.ca/sred.</b>'.link('http://www.cra-arc.gc.ca/sred/')
        ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        header: 'Required Information',
        rows: [
          {
            type: 'table', num: '101'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '1001'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '107'
          },
          {labelClass: 'fullLength'}
        ]
      },

      {
        header: 'Identify the research project(s) performed by the third-party entity for the payment',
        rows: [
          {
            type: 'table', num: '706'
          }
        ]
      },
      {
        header: 'Check (✓) the appropriate box to indicate the type of entity',
        rows: [
          {
            type: 'table',
            num: '710'
          }
        ]
      },
      {
        header: '2. Nature of payment',
        rows: [
          {
            label: 'Check (✓) the appropriate box to indicate the type of entity:',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {label: 'The payment is for:'},
          {
            type: 'table',
            num: '730'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Briefly explain what the payment is for:'
          },
          {
            type: 'infoField',
            inputType: 'textArea',
            num: '736','validate': {'or':[{'length':{'min':'1','max':'175'}},{'check':'isEmpty'}]},
            tn: '736',
            labelAfterNum: true
          },
          {labelClass: 'fullLength'},
          {
            label: 'Briefly explain how the SR&ED is related to a business that you carry on',
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            inputType: 'textArea','validate': {'or':[{'length':{'min':'1','max':'175'}},{'check':'isEmpty'}]},
            tn: '738',
            num: '738',
            labelAfterNum: true
          },
          {labelClass: 'fullLength'},
          {
            label: 'Briefly explain how you are entitled to exploit the results of the SR&ED',
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            inputType: 'textArea',
            tn: '740', 'validate': {'or':[{'length':{'min':'1', 'max':'225'}}, {'check':'isEmpty'}]},
            num: '740',
            labelAfterNum: true
          }
        ]
      },
      {
        'hideFieldset': true,
        'rows': [
          {
            labelClass: 'fullLength'
          },
          {
            label: 'Personal information is collected pursuant to subsections 37(1), 37(11), and 162(5.1) ' +
            'of the <i>Income Tax Act</i> (the Act) and is used for verification of compliance, administration and ' +
            'enforcement of the Scientific Research and Experimental Development (SR&ED) program requirements. <br>' +
            'Information may also be used for the administration and enforcement of other provisions of the Act, ' +
            'including audit, enforcement action, collections, and appeals, and may be disclosed under ' +
            'information-sharing agreements in accordance with the Act. Incomplete or inaccurate information' +
            ' may result in assessment of monetary penalties and/or delays in processing SR&ED claims. <br>' +
            'The social insurance number is collected pursuant to section 237 of the Act and is used for' +
            ' identification purposes.<br>' + 'Information is described in personal information bank CRA PPU 441' +
            ' “Scientific Research and Experimental Development”, in the Canada Revenue Agency (CRA) chapter ' +
            'of Info Source. Personal information is protected under the <i>Privacy Act</i> and individuals ' +
            'have a right of access to, correction, and protection of their personal information.' +
            ' Further details regarding requests for personal information at the CRA' +
            ' and our Info Source chapter can be found at <br>' +
            '<b>http://www.cra.gc.ca/atip/.</b>'.link('http://www.cra-arc.gc.ca/atip/'),
            labelClass: 'fullLength'
          }
        ]
      }
    ]
  })
})();
