(function() {
  wpw.tax.create.diagnostics('t2s562', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('5620010', common.prereq(common.check('t2s5.464', 'isNonZero'), common.requireFiled('T2S562')));

    diagUtils.diagnostic('5620020', common.prereq(common.requireFiled('T2S562'),
        common.check(['800', 't2s5.464'], 'isZero', true)));

    diagUtils.diagnostic('5620030', common.prereq(common.and(
        common.requireFiled('T2S562'),
        common.check('800', 'isNonZero')),
        common.check(['200', '210', '220', '230', '240', '250'], 'isNonZero', true)));

    diagUtils.diagnostic('5620040', common.prereq(common.and(
        common.requireFiled('T2S562'),
        common.check('700', 'isNonZero')),
        common.check(['400', '410', '420', '430', '440', '450', '460', '470', '480', '490',
          '500', '590', '600', '610', '620', '630', '640', '650', '660', '670', '680', '690'], 'isNonZero')));

    diagUtils.diagnostic('5620050', common.prereq(common.and(
        common.requireFiled('T2S562'),
        common.check('705', 'isNonZero')),
        common.check(['510', '595', '605', '615', '625', '635', '645',
          '655', '665', '675', '685', '695'], 'isNonZero')));

    diagUtils.diagnostic('5620060', common.prereq(common.and(
        common.requireFiled('T2S562'),
        common.check('150', 'isYes')),
        common.check('160', 'isNonZero')));

    diagUtils.diagnostic('5620070', common.prereq(common.and(
        common.requireFiled('T2S562'),
        common.check('150', 'isYes')),
        common.check('170', 'isNonZero')));

    diagUtils.diagnostic('5620080', common.prereq(common.and(
        common.requireFiled('T2S562'),
        common.check('800', 'isNonZero')),
        common.check(['300', '310', '320', '330'], 'isNonZero', true)));

    diagUtils.diagnostic('562.400', common.prereq(common.and(
        common.requireFiled('T2S562'),
        common.check('999', 'isNonZero')),
        common.check(['400', '410', '420', '430', '440', '450', '460', '470', '480', '490'], 'isZero', true)));

  });
})();
