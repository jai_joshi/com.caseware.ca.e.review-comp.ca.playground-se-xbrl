(function() {
  'use strict';

  wpw.tax.global.formData.t2s562 = {
      'formInfo': {
        abbreviation: 't2s562',
        'title': 'Ontario Sound Recording Tax Credit',
        //'subTitle': '(2009 and later tax years)',
        'schedule': 'Schedule 562',
        'code': 'Code 1501',
        formFooterNum:'T2 SCH 562 E (15) ',
        isRepeatForm: true,
        repeatFormData:{
          titleNum: '210'
        },
        headerImage: 'canada-federal',
        'showCorpInfo': true,
        'description': [
          {text: ''},
          {
            type: 'list',
            items: [
              {
                label: 'Use this schedule to claim an Ontario sound recording tax credit (OSRTC) under section 94 of ' +
                'the <i> Taxation Act, 2007</i> (Ontario). Complete a separate Schedule 562 for each eligible ' +
                'Canadian sound recording.'
              },
              {
                label: 'For purposes of section 94 of the <i>Taxation Act</i>, 2007 (Ontario), subsection 36(4) of ' +
                '<i>Ontario Regulation 37/09</i> states the conditions for a corporation to be an eligible sound ' +
                'recording company: The corporation needs to be a Canadian-controlled corporation that carries ' +
                'on a sound recording business as its primary activity at a permanent establishment in Ontario' +
                ' during the year and throughout the 12-month period ending immediately before the tax year. ' +
                'Subsection 36(1) defines a sound recording business to mean a business in which the principal ' +
                'activities are managing musicians or vocalists or both, publishing music, producing sound recordings,' +
                ' marketing and distributing sound recordings or a combination of those activities, carried out ' +
                'under contract with musicians, vocalists or copyright holders.'
              },
              {
                label: 'The OSRTC is a refundable tax credit that is equal to 20% of the qualifying expenditures' +
                ' incurred during a tax year by an eligible sound recording company (ESRC). The qualifying expenditures' +
                ' must be incurred by the ESRC no later than 24 months from the date that the first qualifying' +
                ' expenditure was incurred for the eligible Canadian sound recording. An expenditure on account of ' +
                'touring costs incurred in connection with a concert or live performance is not a ' +
                'qualifying expenditure.'
              },
              {
                label: 'The OSRTC is eliminated for expenditures incurred after April 23, 2015. As a transitional ' +
                'measure, expenditures incurred after April 23, 2015 will only qualify for the credit if the eligible' +
                ' sound recording was started before April 23, 2015, the expenditure was incurred before ' +
                'May 1, 2016, and the corporation did not receive an amount from the Ontario Music Fund in ' +
                'respect of the expenditure. '
              },
              {
                label: ' The criteria for a corporation to be eligible for the OSRTC include the eligibility ' +
                'requirements in Part 3 of this schedule.'
              },
              {
                label: 'Before claiming an OSRTC, the ESRC must first complete and sign the Ontario Media Development' +
                ' Corporation (OMDC) application for an OSRTC, and send it to the OMDC along with a copy of the' +
                ' sound recording for which the request for the tax credit is being made. If the sound recording is ' +
                'eligible, the OMDC will issue a certificate certifying that the sound recording is an eligible ' +
                'Canadian sound recording. Enter the certificate information for this sound recording in Part 2 ' +
                'of this schedule.'
              },
              {
                label: 'To claim the OSRTC, include the following with the <i>T2 Corporation Income Tax Return</i>' +
                ' for the tax year:',
                sublist: [
                  'a completed copy of this schedule for each sound recording; and ',
                  'the original or certified copy of the certificate of eligibility issued by the OMDC for each' +
                  ' sound recording.'
                ]
              }
            ]
          }
        ],
        category: 'Ontario Forms'
      },
      'sections': [
        {
          'header': 'Part 1 - Contact information (please print)',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Corporation\'s name (from certificate of eligibility, if different from above)',
              'width': '95%'
            },
            {
              'type': 'infoField',
              'num': '100',
              'validate': {
                'or': [
                  {
                    'length': {
                      'min': '1',
                      'max': '175'
                    }
                  },
                  {
                    'check': 'isEmpty'
                  }
                ]
              },
              'tn': '100',
              'maxLength': '175'
            },
            {
              'type': 'horizontalLine'
            },
            {
              'type': 'table',
              'num': '105'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Is the claim filed for an OSRTC earned through a partnership? *',
              'type': 'infoField',
              'inputType': 'radio',
              'num': '150',
              'tn': '150',
              'labelWidth': '75%',
              'labelClass': 'tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '155'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '165'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* When a corporate member of a partnership is claiming an amount for qualifying expenditures' +
              ' incurred by a partnership, complete a Schedule 562 for the partnership as if the partnership ' +
              'were a corporation. Each corporate partner, other than a limited partner, should file a separate' +
              ' Schedule 562 to claim the partner\'s share of the partnership\'s OSRTC. The allocated amounts ' +
              'can never exceed the amount of the partnership\'s OSRTC.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 2 - Identifying the eligible Canadian sound recording',
          'rows': [
            {
              'type': 'table',
              'num': '195'
            },
            {
              'type': 'table',
              'num': '235'
            }
          ]
        },
        {
          'header': 'Part 3 - Eligibility',
          'fieldAlignRight': true,
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '1. Was the corporation a Canadian-controlled corporation throughout the tax year under sections 26 to 28 of the <i> Investment Canada Act </i>?',
              'type': 'infoField',
              'inputType': 'radio',
              'num': '300',
              'tn': '300',
              'canClear': true
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '2. Was less than 50% of the corporation\'s taxable income for the previous tax year earned outside Ontario?',
              'type': 'infoField',
              'inputType': 'radio',
              'num': '310',
              'tn': '310',
              'canClear': true
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '3. Did the corporation, for the tax year, carry on primarily a sound recording business primarily through a permanent establishment in Ontario?',
              'type': 'infoField',
              'inputType': 'radio',
              'num': '320',
              'tn': '320',
              'canClear': true
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '4. Was the corporation exempt from tax for the tax year under Part III of the <i> Taxation Act, 2007 </i> (Ontario)?',
              'type': 'infoField',
              'inputType': 'radio',
              'num': '330',
              'tn': '330',
              'canClear': true
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If you answered <b>no</b> to questions 1, 2, or 3, or <b>yes</b> to question 4, then you are <b>not eligible</b> for the OSRTC.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 4 - Qualifying expenditures incurred before May 1, 2016',
          'spacing': 'R_tn2',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '390'
            },
            {
              'label': 'Expenditures incurred in the production of the recording:',
              'labelClass': 'bold'
            },
            {
              'label': 'Artists\' royalties',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '400',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '400'
                  }
                },
                null
              ]
            },
            {
              'label': 'Musician session fees',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '410',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '410'
                  }
                },
                null
              ]
            },
            {
              'label': 'Graphics (includes artwork, photography, digital scanning, layout, and colour separations)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '420',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '420'
                  }
                },
                null
              ]
            },
            {
              'label': 'Producers\' fees',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '430',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '430'
                  }
                },
                null
              ]
            },
            {
              'label': 'Software',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '440',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '440'
                  }
                },
                null
              ]
            },
            {
              'label': 'Studio costs and supplies',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '450',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '450'
                  }
                },
                null
              ]
            },
            {
              'label': 'Digital scanning',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '460',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '460'
                  }
                },
                null
              ]
            },
            {
              'label': 'Programming and testing',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '470',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '470'
                  }
                },
                null
              ]
            },
            {
              'label': 'Engineers\' and technicians\' fees',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '480',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '480'
                  }
                },
                null
              ]
            },
            {
              'label': 'Beta testing',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '490',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '490'
                  }
                },
                null
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Subtotal (total of lines 400 to 490)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '999'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'A'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Expenditures incurred in the production of the qualifying music video(s) for the recording',
              'labelClass': 'bold',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '500',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '500'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '510',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '510'
                  }
                }
              ],
              'indicator': 'C'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Expenditures incurred for the direct marketing of the recording',
              'labelClass': 'bold'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Salaries and wages for employees whose primary function is public relations or marketing',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '590',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '590'
                  }
                },
                {
                  'input': {
                    'num': '595',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '595'
                  }
                }
              ]
            },
            {
              'label': 'Consultants\' fees',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '600',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '600'
                  }
                },
                {
                  'input': {
                    'num': '605',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '605'
                  }
                }
              ]
            },
            {
              'label': 'Advertising and promotional costs',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '610',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '610'
                  }
                },
                {
                  'input': {
                    'num': '615',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '615'
                  }
                }
              ]
            },
            {
              'label': 'Launch costs:'
            },
            {
              'label': '- rental cost for sound and light equipment and the facility expenses',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '620',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '620'
                  }
                },
                {
                  'input': {
                    'num': '625',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '625'
                  }
                }
              ]
            },
            {
              'label': '-food, beverage, and entertainment expenses (in accordance with section 67.1 of the federal <i>Income Tax Act</i>',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '630',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '630'
                  }
                },
                {
                  'input': {
                    'num': '635',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '635'
                  }
                }
              ]
            },
            {
              'label': '-event planning services',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '640',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '640'
                  }
                },
                {
                  'input': {
                    'num': '645',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '645'
                  }
                }
              ]
            },
            {
              'label': '-design, printing, and mailing of invitations',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '650',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '650'
                  }
                },
                {
                  'input': {
                    'num': '655',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '655'
                  }
                }
              ]
            },
            {
              'label': '-security',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '660',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '660'
                  }
                },
                {
                  'input': {
                    'num': '665',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '665'
                  }
                }
              ]
            },
            {
              'label': '-business location permits and licences',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '670',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '670'
                  }
                },
                {
                  'input': {
                    'num': '675',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '675'
                  }
                }
              ]
            },
            {
              'label': '-photography',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '680',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '680'
                  }
                },
                {
                  'input': {
                    'num': '685',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '685'
                  }
                }
              ]
            },
            {
              'label': '-promotional gifts and souvenirs',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '690',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '690'
                  }
                },
                {
                  'input': {
                    'num': '695',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '695'
                  }
                }
              ]
            },
            {
              'label': 'Subtotal (total of lines 590 to 690)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '998'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'D'
                  }
                }
              ]
            },
            {
              'label': 'Subtotal (total of lines 595 to 695)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '997'
                  }
                }
              ],
              'indicator': 'E'
            },
            {
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 4 - Qualifying expenditures (continued)',
          'spacing': 'R_mult_tn2',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total qualifying expenditures incurred primarily in Ontario (total of amounts A, B, and D)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '700',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '700'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'F'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total expenditures incurred outside Ontario (amount C <b> plus </b> amount E)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '705',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '705'
                  }
                }
              ],
              'indicator': 'G'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total qualifying expenditures outside Ontario:'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': 'di_table_1'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Repayment of government assistance, to the extent that the government assistance reduced the OSRTC in a previous tax year',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '715',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '715'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'I'
                  }
                },
                null
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Subtotal (amount H <b> plus </b> amount I)',
              'labelClass': ', text-right',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '994'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '993'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'J'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Total qualifying expenditures </b> (amount F <b> plus </b> amount J)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '720',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '720'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '992',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'K'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Deduct:',
              'labelClass': 'bold'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Government assistance attributed to qualifying expenditures in amount K (includes amounts received, entitled to be received, or reasonably expected to be received',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '730',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '730'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'L'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Qualifying expenditures on line 720 that were included in determining the available credit in a previous tax year.',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '740',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '740'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'M'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Subtotal (amount L <b> plus </b> amount M)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '991'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '990'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'N'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Total qualifying expenditures eligible for the OSRTC </b> (amount K <b> minus </b> amount N)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '760',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '760'
                  }
                }
              ],
              'indicator': 'O'
            },
            {
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 5 - Tax credit calculation',
          'spacing': 'R_mult_tn2_complete',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Ontario sound recording tax credit:',
              'labelCLass': 'bold'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': 'di_table_2'
            },
            {
              'label': 'or, if the corporation answered <b> yes </b> at line 150 (in Part 1), determine the partner\'s share of Amount P:',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '801'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Enter amount P or Q (as applicable) on line 464 of Schedule 5, <i>Tax Calculation' +
              ' Supplementary – Corporations</i>. If you are filing more than one Schedule 562, add the amounts from ' +
              'lines P or Q (as applicable) on all of the schedules and enter the total amount on line ' +
              '464 of Schedule 5.',
              'labelClass': 'fullLength'
            }
          ]
        }
      ]
    };
})();
