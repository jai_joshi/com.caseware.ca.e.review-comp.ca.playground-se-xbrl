(function() {
  wpw.tax.global.tableCalculations.t2s562 = {
    'di_table_1': {
      'num': 'di_table_1',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Amount G',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '996',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' singleUnderline'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '1996',
            'init': '50'
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '710'
          },
          '7': {
            'num': '710',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' singleUnderline'
          },
          '8': {
            'label': 'H'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_2': {
      'num': 'di_table_2',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Total qualifying expenditures eligible for the OSRTC (amount O in Part 4)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '889',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '1889',
            'init': '20'
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '800'
          },
          '7': {
            'num': '800',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' doubleUnderline'
          },
          '8': {
            'label': 'P'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    '105': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1],
      'columns': [{
        'width': '70%',
        cellClass: 'alignLeft'
      },
        {
          cellClass: 'alignCenter'
        }],
      'cells': [{
        '0': {
          'label': 'Name of person to contact for more information',
          'num': '110', "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
          'tn': '110',
          'labelClass': 'left',
          'maxLength': '60',
          type: 'text'
        },
        '1': {
          'label': 'Telephone number including area code',
          'num': '120', type: 'custom',
          format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
          validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]},
          'tn': '120',
          cellClass: 'alignCenter',
          'maxLength': '10'
        }
      }]
    },
    '155': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [{
        'width': '55%',
        'type': 'none'
      },
        {}],
      'cells': [{
        '0': {
          'label': 'If you answered <b> yes</b> to the question at line 150, what is the name of the partnership?'
        },
        '1': {
          'num': '160', "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]},
          'tn': '160',
          'maxLength': '175',
          cellClass: 'alignLeft',
          type: 'text'
        }
      }]
    },
    '165': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {
          'width': '75%',
          'type': 'none'
        },
        {
          format: {
            suffix: '%'
          },
          "validate": {
            "and": [
              'percent',
              {
                "or": [
                  {"length": {"min": "1", "max": "6"}},
                  {"check": "isEmpty"}
                ]
              }
            ]
          },
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Enter the percentage of the partnership\'s OSRTC allocated to the corporation',
            'labelClass': 'fullLength'
          },
          '1': {
            'num': '170', "validate": {"or": [{"length": {"min": "1", "max": "6"}}, {"check": "isEmpty"}]},
            'tn': '170',
            decimals: 3,
            'maxLength': '6',
            'filters': 'decimals 2',
            cellClass: 'alignLeft',
            type: 'text'
          }
        }
      ]
    },
    '195': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {colClass: 'std-padding-width', type: 'none'},
        {width: '250px', type: 'none'},
        {type: 'text'}
      ],
      'cells': [
        {
          '0': {
            'tn': '200'
          },
          '1': {
            'label': 'Certificate of eligibility number'
          },
          '2': {
            'num': '200', "validate": {"or": [{"length": {"min": "8", "max": "9"}}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'tn': '210'
          },
          '1': {
            'label': 'Title of sound recording'
          },
          '2': {
            'num': '210', "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'tn': '220'
          },
          '1': {
            'label': 'Name(s) of artist(s)'
          },
          '2': {
            'num': '220', "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'tn': '230'
          },
          '1': {
            'label': 'Name(s) of video(s)'
          },
          '2': {
            'num': '230', "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]}
          }
        }]
    },
    '235': {
      'infoTable': true,
      'fixedRows': true,
      'dividers': [2, 3, 5],
      'columns': [
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'type': 'date',
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'type': 'date',
          colClass: 'std-input-width'
        }
      ],
      'cells': [
        {
          '0': {
            'tn': '240'
          },
          '1': {
            'label': 'Date of first qualifying expenditure incurred for this sound recording'
          },
          '2': {
            'num': '240'
          },
          '3': {
            'tn': '250'
          },
          '4': {
            'label': 'Date of last qualifying expenditure that is included in this claim'
          },
          '5': {
            'num': '250'
          }
        }
      ]
    },
    '390': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [{
        'type': 'none',
        'width': '58%'
      },
        {
          'type': 'none',
          cellClass: 'alignCenter',
          'width': '165px'
        },
        {
          'type': 'none',
          cellClass: 'alignCenter',
          'width': '165px'
        }],
      'cells': [{
        '1': {
          'label': 'Incurred primarily in Ontario'
        },
        '2': {
          'label': 'Incurred outside Ontario'
        }
      }]
    },
    '801': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        'type': 'none',
        colClass: 'small-input-width',
        cellClass: 'alignCenter'
      },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '250px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }],
      'cells': [{
        '0': {
          'label': 'Amount P'
        },
        '1': {
          'num': '810'
        },
        '2': {
          'label': 'x'
        },
        '3': {
          'label': 'percentage on line 170 in Part 1'
        },
        '4': {
          'num': '820',
          decimals: 3
        },
        '5': {
          'label': '%='
        },
        '7': {
          'num': '830'
        },
        '8': {
          'label': 'G'
        }
      }]
    }
  }
})();
