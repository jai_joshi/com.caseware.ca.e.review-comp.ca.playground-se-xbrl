(function() {

  function fillArray(array, start, end) {
    for (var i = start; i <= end; i++) {
      array.push(i)
    }
  }

  wpw.tax.create.calcBlocks('t2s562', function(calcUtils) {
    calcUtils.calc(function(calcUtils) {
      var num = '996';
      var num2 = '710';
      var midnum = '1996';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '889';
      var num2 = '800';
      var midnum = '1889';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils, field, form) {
      // Part 1: contact information//
      field('110').assign(field('CP.951').get() + ' ' + field('CP.950').get());
      field('110').source(field('CP.950'));
      calcUtils.getGlobalValue('120', 'CP', '959');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 3 calculations //
      var sum999 = [];
      fillArray(sum999, 400, 490);
      calcUtils.sumBucketValues('999', sum999);
      calcUtils.sumBucketValues('998', ['590', '600', '610', '620', '630', '640', '650', '660', '670', '680', '690']);
      calcUtils.sumBucketValues('997', ['595', '605', '615', '625', '635', '645', '655', '665', '675', '685', '695']);
    });

    calcUtils.calc(function(calcUtils, field, form) {

      //Part 4 calculations //
      calcUtils.sumBucketValues('700', ['999', '500', '998']);
      calcUtils.sumBucketValues('705', ['510', '997']);
      field('996').assign(field('705').get());
      calcUtils.sumBucketValues('994', ['710', '715']);
      calcUtils.equals('993', '994');
      calcUtils.sumBucketValues('720', ['700', '993']);
      calcUtils.equals('992', '720');
      calcUtils.sumBucketValues('991', ['730', '740']);
      calcUtils.equals('990', '991');
      calcUtils.subtract('760', '992', '990');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 5 calculations//
      var percentage = 1 / 100;
      calcUtils.equals('889', '760');
      field('1889').assign(20);
      field('800').assign(
          field('889').get() *
          field('1889').get() *
          percentage
      );
      calcUtils.equals('820', '170');
      calcUtils.equals('810', '800');
      calcUtils.multiply('830', ['810', '820'], 1 / 100)
    });

  });
})();
