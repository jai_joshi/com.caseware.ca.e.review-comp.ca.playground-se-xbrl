(function () {

  var tableCells = getTableRows([
    {
      "label": 'Newfoundland and Labrador',
      "tn": '003',
      prov: 'NL'
    },
    {
      "label": 'Newfoundland and Labrador Offshore',
      "tn": '004',
      prov: 'XO'
    },
    {
      "label": 'Prince Edward Island',
      "tn": '005',
      prov: 'PE'
    },
    {
      "label": 'Nova Scotia',
      "tn": '007',
      prov: 'NS'
    },
    {
      "label": 'Nova Scotia Offshore',
      "tn": '008',
      prov: 'NO'
    },
    {
      "label": 'New Brunswick',
      "tn": '009',
      prov: 'NB'
    },
    {
      "label": 'Quebec',
      "tn": '011',
      prov: 'QC'
    },
    {
      "label": 'Ontario',
      "tn": '013',
      prov: 'ON'
    },
    {
      "label": 'Manitoba',
      "tn": '015',
      prov: 'MB'
    },
    {
      "label": 'Saskatchewan',
      "tn": '017',
      prov: 'SK'
    },
    {
      "label": 'Alberta',
      "tn": '019',
      prov: 'AB'
    },
    {
      "label": 'British Columbia',
      "tn": '021',
      prov: 'BC'
    },
    {
      "label": 'Yukon',
      "tn": '023',
      prov: 'YT'
    },
    {
      "label": 'Northwest Territories',
      "tn": '025',
      prov: 'NT'
    },
    {
      "label": 'Nunavut',
      "tn": '026',
      prov: 'NU'
    },
    {
      "label": 'Outside Canada',
      "tn": '027',
      prov: 'OC'
    }
  ]);

  function getTableColumn() {
    return [
      {
        header: 'Tick <b>yes</b> if the corporation had a permanent establishment in the ' +
        'jurisdiction during the tax year but there are no sales and wages paid in jurisdiction',
        "width": "20%",
        "type": "singleCheckbox"
      },
      {
        "width": "10%",
        "type": "singleCheckbox",
        "indicator": "A",
        disabled: true
      },
      {
        "header": "Total salaries and wages paid in jurisdiction",
        "indicator": "B",
        "total": true,
        "totalNum": "129",
        "totalTn": "129"
        // "totalIndicator": "G"
      },
      {
        "header": "(B x taxable income) ÷ G",
        "indicator": "C",
        "total": true,
        "totalNum": "1129",
        "disabled": true
      },
      {
        "header": "Gross revenue attributable to jurisdiction",
        "indicator": "D",
        "total": true,
        "totalNum": "169",
        "totalTn": "169"
        // "totalIndicator": "H"
      },
      {
        "header": "(D x taxable income) ÷ H",
        "indicator": "E",
        "total": true,
        "totalNum": "1169",
        "disabled": true
      },
      {
        "header": "Allocation of taxable income (C + E) x 1/2**<br>(where either G or H is nil, do not multiply by 1/2)",
        "indicator": "F",
        "total": true,
        "disabled": true,
        "totalNum": "997"
      }
    ]
  }

  function getTableRows(labelArray) {
    var tableRows = [];

    if (!angular.isArray(labelArray))
      labelArray = [labelArray];

    if (labelArray.length > 0) {
      for (var i = 0; i < labelArray.length; i++) {
        var rowDataObj = labelArray[i];
        var inputRow = {0: {label: rowDataObj.label, tn: rowDataObj.tn}};
        inputRow[1] = {num: rowDataObj.tn};
        inputRow[2] = {tn: (parseInt(rowDataObj.tn) + 100).toString(), num: (parseInt(rowDataObj.tn) + 100).toString()};
        inputRow[3] = {num: (parseInt(rowDataObj.tn) + 2100).toString()};
        inputRow[4] = {tn: (parseInt(rowDataObj.tn) + 140).toString(), num: (parseInt(rowDataObj.tn) + 140).toString()};
        inputRow[5] = {num: (parseInt(rowDataObj.tn) + 2300).toString()};
        inputRow[6] = {num: (parseInt(rowDataObj.tn) + 2500).toString()};
        inputRow.prov = rowDataObj.prov;
        tableRows.push(inputRow);
      }
    }
    return tableRows;
  }

  function stripTableColumns(colArray, keepHeaders) {
    colArray.forEach(function (col, index) {
      Object.keys(col).forEach(function (key) {
        if (key == 'total' || key == 'totalNum' || key == 'totalTn') {
          delete colArray[index][key];
        }
        else if (!keepHeaders && (key == 'header' || key == 'indicator' || key == 'type')) {
          if (key == 'type') {
            colArray[index][key] = 'none';
          }
          else {
            delete colArray[index][key];
          }
        }
      });
    });
    return colArray;
  }

  wpw.tax.global.tableCalculations.t2s5 = {
    "998": {
      "fixedRows": "true",
      "hasTotals": true,
      "dividers": [3],
      "superHeaders": [
        {
          "header": "Jurisdiction<br>Tick <b>yes</b> if the corporation had a permanent establishment in the jurisdiction during the tax year.*",
          "colspan": "2"
        }
      ],
      "columns": [
        {
          "header": "Tick <b>yes</b> if the corporation had a permanent establishment in the jurisdiction during the tax year but there are no sales and wages paid in jurisdiction",
          "width": "20%",
          "type": "singleCheckbox"
        },
        {
          "width": "10%",
          "type": "singleCheckbox",
          "indicator": "A",
          "disabled": true,
          cannotOverride: true
        },
        {
          "header": "Total salaries and wages paid in jurisdiction",
          "indicator": "B"
        },
        {
          "header": "(B x taxable income) ÷ G",
          "indicator": "C",
          "disabled": true
        },
        {
          "header": "Gross revenue attributable to jurisdiction",
          "indicator": "D"
        },
        {
          "header": "(D x taxable income) ÷ H",
          "indicator": "E",
          "disabled": true
        }, {
          "header": "Allocation of taxable income (C + E) x 1/2**<br>(where either G or H is nil, do not multiply by 1/2)",
          "indicator": "F",
          "disabled": true
        }
      ],
      "cells": [{
        "0": {"label": "Newfoundland and Labrador", "tn": "003"},
        "1": {"num": "003"},
        "2": {"tn": "103", "num": "103", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2103"},
        "4": {"tn": "143", "num": "143", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2303"},
        "6": {"num": "2503"},
        "prov": "NL"
      }, {
        "0": {"label": "Newfoundland and Labrador Offshore", "tn": "004"},
        "1": {"num": "004"},
        "2": {"tn": "104", "num": "104", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2104"},
        "4": {"tn": "144", "num": "144", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2304"},
        "6": {"num": "2504"},
        "prov": "XO"
      }, {
        "0": {"label": "Prince Edward Island", "tn": "005"},
        "1": {"num": "005"},
        "2": {"tn": "105", "num": "105", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2105"},
        "4": {"tn": "145", "num": "145", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2305"},
        "6": {"num": "2505"},
        "prov": "PE"
      }, {
        "0": {"label": "Nova Scotia", "tn": "007"},
        "1": {"num": "007"},
        "2": {"tn": "107", "num": "107", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2107"},
        "4": {"tn": "147", "num": "147", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2307"},
        "6": {"num": "2507"},
        "prov": "NS"
      }, {
        "0": {"label": "Nova Scotia Offshore", "tn": "008"},
        "1": {"num": "008"},
        "2": {"tn": "108", "num": "108", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2108"},
        "4": {"tn": "148", "num": "148", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2308"},
        "6": {"num": "2508"},
        "prov": "NO"
      }, {
        "0": {"label": "New Brunswick", "tn": "009"},
        "1": {"num": "009"},
        "2": {"tn": "109", "num": "109", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2109"},
        "4": {"tn": "149", "num": "149", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2309"},
        "6": {"num": "2509"},
        "prov": "NB"
      }, {
        "0": {"label": "Quebec", "tn": "011"},
        "1": {"num": "011"},
        "2": {"tn": "111", "num": "111", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2111"},
        "4": {"tn": "151", "num": "151", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2311"},
        "6": {"num": "2511"},
        "prov": "QC"
      }, {
        "0": {"label": "Ontario", "tn": "013"},
        "1": {"num": "013"},
        "2": {"tn": "113", "num": "113", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2113"},
        "4": {"tn": "153", "num": "153", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2313"},
        "6": {"num": "2513"},
        "prov": "ON"
      }, {
        "0": {"label": "Manitoba", "tn": "015"},
        "1": {"num": "015"},
        "2": {"tn": "115", "num": "115", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2115"},
        "4": {"tn": "155", "num": "155", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2315"},
        "6": {"num": "2515"},
        "prov": "MB"
      }, {
        "0": {"label": "Saskatchewan", "tn": "017"},
        "1": {"num": "017"},
        "2": {"tn": "117", "num": "117", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2117"},
        "4": {"tn": "157", "num": "157", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2317"},
        "6": {"num": "2517"},
        "prov": "SK"
      }, {
        "0": {"label": "Alberta", "tn": "019"},
        "1": {"num": "019"},
        "2": {"tn": "119", "num": "119", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2119"},
        "4": {"tn": "159", "num": "159", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2319"},
        "6": {"num": "2519"},
        "prov": "AB"
      }, {
        "0": {"label": "British Columbia", "tn": "021"},
        "1": {"num": "021"},
        "2": {"tn": "121", "num": "121", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2121"},
        "4": {"tn": "161", "num": "161", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2321"},
        "6": {"num": "2521"},
        "prov": "BC"
      }, {
        "0": {"label": "Yukon", "tn": "023"},
        "1": {"num": "023"},
        "2": {"tn": "123", "num": "123", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2123"},
        "4": {"tn": "163", "num": "163", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2323"},
        "6": {"num": "2523"},
        "prov": "YT"
      }, {
        "0": {"label": "Northwest Territories", "tn": "025"},
        "1": {"num": "025"},
        "2": {"tn": "125", "num": "125", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2125"},
        "4": {"tn": "165", "num": "165", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2325"},
        "6": {"num": "2525"},
        "prov": "NT"
      }, {
        "0": {"label": "Nunavut", "tn": "026"},
        "1": {"num": "026"},
        "2": {"tn": "126", "num": "126", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2126"},
        "4": {"tn": "166", "num": "166", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2326"},
        "6": {"num": "2526"},
        "prov": "NU"
      }, {
        "0": {"label": "Outside Canada", "tn": "027"},
        "1": {"num": "027"},
        "2": {"tn": "127", "num": "127", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "3": {"num": "2127"},
        "4": {"tn": "167", "num": "167", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
        "5": {"num": "2327"},
        "6": {"num": "2527"},
        "prov": "OC"
      }]
    },
    "999": {
      "fixedRows": "true",
      "columns": [{"header": "Regulation", "type": "none"}, {
        "header": "Type of corporation",
        "type": "none",
        "width": "30%"
      }, {
        "header": "Type of entry in column B",
        "type": "none",
        "width": "30%"
      }, {"header": "Type of entry in column D", "type": "none", "width": "30%"}],
      "cells": [{
        "0": {"label": "402"},
        "1": {"label": "Corporations not specified below"},
        "2": {"label": "Salaries and wages"},
        "3": {"label": "Gross revenue"}
      }, {
        "0": {"label": "403"},
        "1": {"label": "Insurance corporations"},
        "2": {"label": "No entry required"},
        "3": {"label": "Net premiums"}
      }, {
        "0": {"label": "404"},
        "1": {"label": "Banks"},
        "2": {"label": "Salaries and wages"},
        "3": {"label": "Amount of loans and deposits"}
      }, {
        "0": {"label": "405"},
        "1": {"label": "Trust and loan corporations"},
        "2": {"label": "No entry required"},
        "3": {"label": "Gross revenue"}
      }, {
        "0": {"label": "406(1) (Note 1)"},
        "1": {"label": "Railway corporations"},
        "2": {"label": "Equated track miles/kilometres"},
        "3": {"label": "Gross ton miles/kilometres"}
      }, {
        "0": {"label": "406(2) (Note 1)"},
        "1": {"label": "Railway corporations (Note 2)"},
        "2": {
          "label": "The method of allocation depends on the business line - see the proper regulation.",
          "span": 1
        },
        '3': {
          "label": "The method of allocation depends on the business line - see the proper regulation."
        }
      }, {
        "0": {"label": "407"},
        "1": {"label": "Airline corporations"},
        "2": {"label": "Capital cost of fixed assets (Note 3)"},
        "3": {"label": "Revenue plane miles/kilometres (Note 4)"}
      }, {
        "0": {"label": "408"},
        "1": {"label": "Grain elevator operators"},
        "2": {"label": "Salaries and wages"},
        "3": {"label": "Bushels of grain received"}
      }, {
        "0": {"label": "409"},
        "1": {"label": "Bus and truck operators"},
        "2": {"label": "Salaries and wages"},
        "3": {"label": "Miles/kilometres driven"}
      }, {
        "0": {"label": "410 (Note 5)"},
        "1": {"label": "Ship operators"},
        "2": {"label": "Salaries and wages (Note 6)"},
        "3": {"label": "Port-call-tonnage"}
      }, {
        "0": {"label": "411"},
        "1": {"label": "Pipeline operators"},
        "2": {"label": "Salaries and wages"},
        "3": {"label": "Miles/kilometres of pipeline"}
      }, {
        "0": {"label": "412"},
        "1": {"label": "Divided businesses"},
        "2": {
          "label": 'The method of allocation depends on the business line - see the proper regulation.'
        },
        "3": {
          "label": 'The method of allocation depends on the business line - see the proper regulation.'
        }
      }, {
        "0": {"label": "413"},
        "1": {"label": "Non-resident corporations"},
        "2": {
          "label": 'The method of allocation depends on the business line - see the proper regulation.',
          "span": 1
        },
        "3": {
          "label": 'The method of allocation depends on the business line - see the proper regulation.'
        }
      }]
    },
    "1998": {
      "fixedRows": "true",
      "infoTable": true,
      "columns": [{"width": "20%", "type": "none"}, {
        "width": "10%",
        "type": "none",
        "disabled": true
      }, {}, {"disabled": true}, {}, {"disabled": true}, {"disabled": true}],
      "cells": [{
        "2": {
          "num": "129",
          "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          "tn": "129"
        },
        "4": {"num": "169", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}, "tn": "169"},
        "6": {"num": "997"}
      }]
    }
  };
})();