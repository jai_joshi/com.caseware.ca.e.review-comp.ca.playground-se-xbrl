(function() {

  function getApplicableValue(form, num1, num2) {
    var value = 0;
    var num1Val = form.field(num1).get();
    var num2Val = form.field(num2).get();

    if(num2Val > 0) {
      value = num2Val
    } else {
      value = num1Val
    }

    return value;
  }

  function getDomesticTaxableIncome(field, tableNum, cIndex) {
    var total = 0;
    for (var i = 0; i < field(tableNum).getRows().length - 1; i++) {
      total += field(tableNum).cell(i, cIndex).get();
    }
    return total
  }

  function calcTotal(field, table, col) {
    var row = 0;
    var total = 0;
    while (row < field(table).size()['rows']) {
      if (!wpw.tax.utilities.isNullUndefined(field(table).cell(row, col).get()) &&
          field(table).cell(row, col).get() != 0) {
        total += field(table).cell(row, col).get();
      }
      row++;
    }
    return total
  }

  function isJurisdiction(calcUtils, provincialCodes) {
    var isJur = false;
    provincialCodes = wpw.tax.utilities.ensureArray(provincialCodes);
    provincialCodes.forEach(function(code) {
      if (calcUtils.isJurisdiction(code)) {
        isJur = true;
      }
    });
    return isJur;
  }

  function getValueFromProvincialForms(calcUtils, provincialCodes, fieldObjArr) {
    var field = calcUtils.field;
    var isJur = isJurisdiction(calcUtils, provincialCodes);
    fieldObjArr.forEach(function(fieldObj) {
      field(fieldObj[0]).assign(isJur ? field(fieldObj[1]).get() : 0);
      field(fieldObj[0]).source(field(fieldObj[1]));
    });
  }

  wpw.tax.create.calcBlocks('t2s5', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      if (calcUtils.hasChanged('100')) {
        var headerB = '';
        var headerD = '';
        if (calcUtils.field('100').get())
          switch (calcUtils.field('100').get().toString()) {
            case '402':
              headerB = 'salaries and wages';
              headerD = 'Gross revenue';
              break;
            case '403':
              headerB = '';
              //todo: no entry required - header??
              headerD = 'Net premiums';
              break;
            case '404':
              headerB = 'salaries and wages';
              headerD = 'Gross revenue';
              break;
            case '405':
              headerB = '';
              //todo: no entry required - header??
              headerD = 'Gross revenue';
              break;
            case '406(1)':
              headerB = 'Equated track miles/kilometres';
              headerD = 'Gross ton miles/kilometres';
              break;
            case '406(2)':
              headerB = 'salaries and wages';
              headerD = 'Gross revenue';
              //TODO: look at chart
              break;
            case '407':
              headerB = 'capital cost of fixed assets';
              headerD = 'Revenue plane miles/kilometres';
              break;
            case '408':
              headerB = 'salaries and wages';
              headerD = 'Bushels of grain received';
              break;
            case '409':
              headerB = 'salaries and wages';
              headerD = 'Miles/kilometres driven';
              break;
            case '410':
              headerB = 'salaries and wages';
              headerD = 'Port-call-tonnage';
              break;
            case '411':
              headerB = 'salaries and wages';
              headerD = 'Miles/kilometres of pipeline';
              break;
            case '412':
            case '413':
              headerB = 'salaries and wages';
              headerD = 'Gross revenue';
              //TODO: look at chart
              break;
            default:
              headerB = 'salaries and wages';
              headerD = 'Gross revenue';
              break;
          }

        var tableFormData = wpw.tax.create.retrieve('formData', 't2s5').sections[1].rows[2];
        var colBHeading = tableFormData.columns ? tableFormData.columns[1] : {};
        var colDHeading = tableFormData.columns ? tableFormData.columns[3] : {};

        if (headerB == '') {
          colBHeading.header = '<i>' + 'No entry required' + '</i>';
          colBHeading.type = 'none';
        }
        else {
          colBHeading.header = 'Total ' + headerB + ' paid in jurisdiction';
          if (colBHeading.type)
            delete colBHeading.type;
        }
        colDHeading.header = headerD + ' attributable to jurisdiction';
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 1 table

      var table998 = field('998');
      var taxableIncome = calcUtils.form('T2J').field('371').get();
      var provinces = {};
      var col6Values = [];

      //manual totals:
      field('129').assign(calcTotal(field, 998, 2));
      field('169').assign(calcTotal(field, 998, 4));

      table998.getRows().forEach(function(row, rIndex) {
        //ColC
        var colB = row[2].get();
        var colBTotal = field('129').get();
        row[3].assign(colB * taxableIncome / colBTotal);
        //ColE
        var colD = row[4].get();
        var colDTotal = field('169').get();
        row[5].assign(colD * taxableIncome / colDTotal);
        //ColF
        var colC = row[3].get();
        var colE = row[5].get();
        var colFValue = 0;
        if (colBTotal == 0 || colDTotal == 0) {
          colFValue = colC + colE;
        }
        else {
          colFValue = (colC + colE) / 2
        }
        col6Values.push(Math.round(colFValue));
        //to set provinces:
        if (colB > 0 || colD > 0) {
          row[1].assign(true);
          provinces[table998.getRowInfo(rIndex).prov] = true;
        }
        else if (colB == 0 && colD == 0) {
          row[1].assign(false);
          provinces[table998.getRowInfo(rIndex).prov] = false;
        }
        //to let user overridden provinces
        if (row[0].get() == true) {
          row[1].assign(true);
          provinces[table998.getRowInfo(rIndex).prov] = true;
        }
      });

      //to deal with round up issue that total of column F > taxable income (BUG 1763)
      var col6Total = 0;
      var lastRow;
      for (var row = 0; row < col6Values.length; row++) {
        if (col6Values[row]) {
          col6Total += col6Values[row];
          lastRow = row;
        }
      }
      if (col6Total != taxableIncome) {
        col6Values[lastRow] -= (col6Total - taxableIncome);
      }
      field('998').getRows().forEach(function(row, index) {
        row[6].assign(col6Values[index]);
      });

      //calculated totals:
      field('1998').cell(0, 3).assign(calcTotal(field, 998, 3));
      field('1998').cell(0, 5).assign(calcTotal(field, 998, 5));
      field('1998').cell(0, 6).assign(calcTotal(field, 998, 6));

      field('provinces').assign(provinces);

      field('1031').assign(getDomesticTaxableIncome(field, '998', 6));
      var nonDomesticTaxableIncome = field('998').cell(15, 6).get();
      field('1032').assign(nonDomesticTaxableIncome);
      calcUtils.sumBucketValues('1033', ['1031', '1032']);
    });

    //part II
    calcUtils.calc(function(calcUtils, field, form) {
      //NL calc
      getValueFromProvincialForms(calcUtils, ['NL', 'XO'],
          [
            ['200', 't2s307.240'],
            ['205', 't2s307.503'],
            ['501', 't2s21w.240NL'],
            ['503', 't2s300.115'],
            ['504', 't2s308.150'],
            ['505', 't2s303.160'],
            ['507', 't2s304.123'],
            ['518', 't2s305.150'],
            ['520', 't2s301.160'],
            ['521', 't2s302.300'],
            ['522', 't2s309.300']
          ]
      );

      calcUtils.sumBucketValues(206, [200, 205]);
      calcUtils.equals(207, 206);
      calcUtils.sumBucketValues(508, [500, 501, 503, 504, 505, 507]);
      calcUtils.equals('509', '508');
      calcUtils.subtract('510', '207', '509');
      calcUtils.sumBucketValues(519, [510, 518]);
      calcUtils.sumBucketValues(822, [520, 521, 522]);
      //Correction from unit test
      calcUtils.equals('823', '822');
      calcUtils.subtract('209', '519', '823', true);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //PEI calc
      getValueFromProvincialForms(calcUtils, 'PE',
          [
            ['210', 'T2S322.811'],
            ['528', 't2s21w.240PE'],
            ['530', 'T2S321.160']
          ]
      );
      calcUtils.sumBucketValues(531, [525, 528, 530]);
      calcUtils.equals('532', '531');
      calcUtils.subtract('214', '210', '532');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //NS calc
      var isOffshore = field('cp.750').get() == 'NO';
      var isMJandNO = field('cp.750').get() == 'MJ' && field('T2S5.008').get();
      //always get value no matter jurisdiction
      getValueFromProvincialForms(calcUtils, ['NO', 'NS'],
          [
            ['215', 'T2S346.534'],
            ['221', 'T2S340.803'],
            ['554', 't2s21w.240NS'],
            ['570', 'T2S2.263-1'],
            ['556', 'T2S341.122'],
            ['565', 'T2S345.300'],
            ['566', 'T2S340.190'],
            ['567', 'T2S347.300'],
            ['569', 'T2S348.300']
          ]
      );
      //any jurisdiction including NS offshore
      field('220').assign(isOffshore || isMJandNO ? field('T2S346.9003').get() : 0);
      field('220').source(field('T2S346.9003'));

      calcUtils.sumBucketValues(222, [215, 220, 221]);
      calcUtils.equals('223', '222');
      calcUtils.sumBucketValues(1899, [550, 554, 556, 570]);
      calcUtils.equals('1898', '1899');
      calcUtils.subtract('1897', '223', '1898');

      calcUtils.sumBucketValues(1896, [565, 566, 567, 568, 569]);
      calcUtils.equals('895', '1896');
      calcUtils.subtract('224', '1897', '895', true);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //NB calc
      getValueFromProvincialForms(calcUtils, 'NB',
          [
            ['225', 'T2S366.275'],
            ['573', 'T2S360.999'],
            ['576', 't2s21w.240NB'],
            ['578', 'T2S367.150'],
            ['597', 'T2S360.190']
          ]
      );

      field('595').assign(calcUtils.isJurisdiction('NB') ? field('T2S365.900').total(1).get() : 0);
      field('595').source(field('T2S365.900').total(1));

      calcUtils.sumBucketValues(574, [225, 573]);
      calcUtils.equals('599', '574');
      calcUtils.sumBucketValues(577, [575, 576, 578]);
      calcUtils.equals('1578', '577');
      calcUtils.subtract('579', '599', '1578');
      calcUtils.sumBucketValues(596, [595, 597]);
      calcUtils.equals('598', '596');
      calcUtils.subtract('229', '579', '598', true);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //ON calc
      getValueFromProvincialForms(calcUtils, 'ON',
          [
            ['274', 't2s504.159'],
            ['277', 'T2S508.816'],
            ['408', 't2s21w.240ON'],
            ['416', 'T2S508.3251'],
            ['420', 'T2S2.262-1'],
            ['468', 'T2S566.785'],
            ['470', 'T2S568.411'],
            ['270', 'T2S500.210'],
            ['402', 'T2S500.399'],
            ['406', 'T2S502.505'],
            ['410', 'T2S500.535'],
            ['415', 'T2S525.250'],
            ['418', 'T2S510.147'],
            ['278', 'T2S510.120'],
            ['406', 'T2S502.505']
          ]
      );

      if (calcUtils.isJurisdiction('ON')) {
        calcUtils.setRepeatSummaryValue('456', 'T2S554', '612', 'add');

        var totalTn458 = 0;
        var allFormsT2S556 = calcUtils.allRepeatForms('t2s556');
        allFormsT2S556.forEach(function(formId) {
          totalTn458 += formId.field('755').get() + formId.field('815').get() + formId.field('915').get();
        });
        field('458').assign(totalTn458);

        calcUtils.setRepeatSummaryValue('460', 'T2S558', '720', 'add');

        //tn462
        var totalTn462 = 0;
        var allFormsT2S560 = calcUtils.allRepeatForms('t2s560');
        allFormsT2S560.forEach(function(formId) {
          totalTn462 += formId.field('620').get();
        });
        field('462').assign(totalTn462);

        //tn466
        var totalOnPublishing = 0;
        wpw.tax.form.allRepeatForms('T2S564').forEach(function(formdata) {
          totalOnPublishing += getApplicableValue(formdata, '550', '930');
        });
        field('466').assign(totalOnPublishing);
        //tn452
        var totalEducation = 0;
        wpw.tax.form.allRepeatForms('T2S550').forEach(function(formdata) {
          totalEducation += getApplicableValue(formdata, '500', '630');
        });
        field('452').assign(totalEducation);
        //tn454
        var totalApprenticeship = 0;
        wpw.tax.form.allRepeatForms('t2s552').forEach(function(form) {
          totalApprenticeship += getApplicableValue(form, '500', '630');
        });

        field('454').assign(totalApprenticeship);
        // tn464
        var totalRecording = 0;
        wpw.tax.form.allRepeatForms('T2S562').forEach(function(form) {
          totalRecording += getApplicableValue(form, '800', '830');
        });
        field('464').assign(totalRecording);
      } else {
        calcUtils.removeValue(['456', '458', '460', '462', '466', '452', '454', '464'], true)
      }

      field('456').source(field('T2S554.612'));
      field('458').source(field('T2S556.755'));
      field('460').source(field('t2s558.720'));
      field('462').source(field('T2S560.620'));
      field('466').source(field('T2S564.550'));
      field('452').source(field('T2S550.500'));
      field('454').source(field('T2S552.630'));
      field('464').source(field('T2S562.800'));

      if (calcUtils.isJurisdiction('ON')) {
        if (field('T2S504.146').get() > 0) {
          field('404').assign(field('T2S504.146').get());
        }
        else {
          field('404').assign(field('T2S504.148').get());
        }
      } else {
        field('404').assign(0)
      }
      field('404').source(field('T2S504.146'));

      calcUtils.subtract('403', '270', '402');
      calcUtils.equals('405', '403');
      calcUtils.sumBucketValues(298, [274, 276, 277]);
      calcUtils.equals('297', '298');
      calcUtils.sumBucketValues(299, [405, 297]);

      calcUtils.sumBucketValues(499, [404, 406, 408, 410, 415]);
      calcUtils.equals('498', '499');
      calcUtils.subtract('497', '299', '498');
      calcUtils.subtract('496', '497', '416');
      field('495').assign(Math.abs(Math.max(field('496').get() - field('418').get() - field('420').get(), 0)));
      calcUtils.sumBucketValues(494, [278, 280]);
      calcUtils.equals('493', '494');
      calcUtils.sumBucketValues(492, [495, 493]);
      calcUtils.sumBucketValues(491, [450, 452, 454, 456, 458, 460, 462, 464, 466, 468, 470]);
      calcUtils.equals('490', '491');
      calcUtils.subtract('290', '492', '490', true);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //MB calc

      getValueFromProvincialForms(calcUtils, 'MB',
          [
            ['230', 'T2S383.301'],
            ['601', 't2s21w.240MB'],
            ['602', 'T2S394.290'],
            ['605', 'T2S381.160'],
            ['606', 'T2S380.160'],
            ['607', 't2s385.160'],
            ['608', 't2s387.123'],
            ['610', 't2s391.140'],
            ['609', 'T2S390.220'],
            ['612', 'T2S390.104'],
            ['613', 'T2S380.180'],
            ['615', 'T2S389.461'],
            ['621', 'T2S381.125'],
            ['623', 't2s385.161'],
            ['324', 't2s392.704'],
            ['326', 'T2S394.190'],
            ['325', 'T2S393.200']
          ]
      );

      if (calcUtils.isJurisdiction('MB')) {
        calcUtils.setRepeatSummaryValue('620', 'T2S388', '900', 'add');
      } else {
        field('620').assign(0);
      }
      field('620').source(field('T2S388.900'));

      calcUtils.sumBucketValues('489', [601, 602, 605, 606, 603, 607, 608, 609, 610]);
      calcUtils.equals('488', '489');
      calcUtils.subtract('487', '230', '488');
      //Correction from unit test
      calcUtils.sumBucketValues('399', [611, 612, 613, 614, 615, 619, 620, 621, 622, 623, 324, 325, 326, 327]);
      calcUtils.equals('398', '399');
      calcUtils.subtract('234', '487', '398', true);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //SK calc
      getValueFromProvincialForms(calcUtils, 'SK',
          [
            ['235', 'T2S411.306'],
            ['625', 't2s21w.240SK'],
            ['626', 'T2S404.200'],
            ['630', 'T2S402.160'],
            ['631', 'T2S403.160'],
            ['632', 'T2S400.180'],
            ['644', 'T2S402.250']
          ]
      );
      if (isJurisdiction(calcUtils, 'SK')) {
        field('645').assign(getApplicableValue(calcUtils.form('T2S403'), '215', '221'))
      }

      calcUtils.sumBucketValues(396, [624, 625, 626, 630, 631]);
      calcUtils.equals('395', '396');
      calcUtils.subtract('394', '235', '395');
      calcUtils.sumBucketValues(393, [641, 644, 645]);
      calcUtils.equals('392', '393');
      calcUtils.subtract('239', '394', '392', true);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //BC calc
      var isBCJur = calcUtils.isJurisdiction('BC');
      getValueFromProvincialForms(calcUtils, 'BC',
          [
            ['240', 'T2S427.811'],
            ['241', 'T2S425.1303'],
            ['650', 't2s21w.240BC'],
            ['659', 'T2S425.376'],
            ['673', 'T2S421.221'],
            ['674', 'T2S425.356'],
            ['679', 'T2S428.816'],
            ['680', 'T2S429.501'],
            ['681', 'T2S430.830']
          ]
      );

      if (isBCJur) {
        calcUtils.setRepeatSummaryValue('671', 'T2S422', '800', 'add');
        calcUtils.setRepeatSummaryValue('672', 'T2S423', '850', 'add');
      } else {
        field('671').assign(0);
        field('672').assign(0);
      }
      field('671').source(field('T2S422.800'));
      field('672').source(field('T2S423.850'));

      calcUtils.sumBucketValues(391, [240, 241]);
      calcUtils.equals('390', '391');
      var bcTaxOtherwisePayable =
          field('390').get() -
          field('650').get() -
          field('651').get() -
          field('653').get() -
          field('1660').get();

      field('683').assign(isBCJur ? Math.min(field('t2s2.265-1').get(), bcTaxOtherwisePayable) : 0);
      field('683').source(form('t2s2').field('265-1'));

      calcUtils.sumBucketValues(389, [650, 651, 653, 656, 683, 1660, 659]);
      calcUtils.equals('388', '389');
      calcUtils.subtract('387', '390', '388');
      calcUtils.multiply('665', ['886'], '0.9');
      calcUtils.sumBucketValues(386, [670, 671, 672, 673, 674, 665, 679, 680, 681]);
      calcUtils.equals('385', '386');
      calcUtils.subtract('244', '387', '385', true);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //YT calc
      getValueFromProvincialForms(calcUtils, 'YT',
          [
            ['245', 'T2S443.812'],
            ['676', 't2s21w.240YT'],
            ['677', 'T2S440.516'],
            ['698', 'T2S442.160']
          ]
      );

      calcUtils.sumBucketValues(699, [675, 676, 677]);
      calcUtils.equals('1000', '699');
      calcUtils.subtract('697', '245', '1000', false);
      calcUtils.subtract('249', '697', '698', true);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //NT calc
      getValueFromProvincialForms(calcUtils, 'NT',
          [
            ['250', 'T2S461.811'],
            ['701', 't2s21w.240NT'],
            ['705', 'T2S460.160']
          ]
      );

      calcUtils.sumBucketValues(696, [700, 701, 705]);
      calcUtils.equals('695', '696');
      calcUtils.subtract('254', '250', '695');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //NU calc
      getValueFromProvincialForms(calcUtils, 'NU',
          [
            ['260', 'T2S481.811'],
            ['730', 't2s21w.240NU']
          ]
      );

      calcUtils.sumBucketValues(694, [725, 730]);
      calcUtils.equals('693', '694');
      calcUtils.subtract('264', '260', '693', '740');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //total credit
      calcUtils.sumBucketValues(255, [209, 214, 224, 229, 290, 234, 239, 244, 249, 254, 264]);
    });
  });
})();
