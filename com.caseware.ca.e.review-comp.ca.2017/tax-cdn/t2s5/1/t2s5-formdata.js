(function() {
  'use strict';

  wpw.tax.global.formData.t2s5 = {
    'formInfo': {
      'title': 'Tax Calculation Supplementary – Corporations',
      abbreviation: 'T2S5',
      'schedule': 'Schedule 5 Code 1701',
      // 'code': 'Schedule 5 Code 1701',
      formFooterNum: 'T2 SCH 5 E (17)',
      'showCorpInfo': true,
      'description': [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if, during the tax year, your corporation:',
              sublist: [
                'had a permanent establishment in more than one jurisdiction<br>(corporations that have no' +
                ' taxable income should only complete columns A, B, and D in Part 1);', 'is claiming provincial or' +
                ' territorial tax credits or rebates (see Part 2); or', 'has to pay taxes, other than income tax, ' +
                'for Newfoundland and Labrador, or Ontario (see Part 2).'
              ]
            },
            {
              label: 'All legislative references to the <i>Income Tax Regulations</i>.'
            },
            {
              label: 'For more information, see the <i>T2 Corporation – Income Tax Guide</i>.'
            },
            {
              label: 'For the regulation number to be entered in field 100 of Part 1 on page 2, see the chart below.'
            }
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    'sections': [
      {
        'hideFieldset': true,
        forceBreakAfter: true,
        'rows': [
          {
            'type': 'table',
            'num': '999'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 1: Enter brackets when completing field 100 in Part 1 for this regulation.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 2: Operating an airline service, ships, hotels, or receiving substantial revenues from petroleum or natural gas royalties.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 3: Exclude aircraft.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 4: Exclude miles/kilometres flown over the territorial waters of Canada.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 5: In Part 1, instead of taxable income, use the excess of taxable income over allocable income for the calculation in column C and allocable income for the calculation in column E.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 6: Only where taxable income exceeds allocable income.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 1 - Allocation of taxable income',
        'forceBreak': true,
        'rows': [
          {'labelClass': 'fullLength'},
          {
            'label': 'Enter the regulation that applies (402 to 413) from page 1.',
            'num': '100',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '3',
                    'max': '6'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'tn': '100',
            'type': 'infoField',
            'inputType': 'dropdown',
            'labelEnd': true,
            'init': '402',
            'showValues': 'before',
            'options': [
              {
                'value': '402',
                'option': 'Corporations not specified below'
              },
              {
                'value': '403',
                'option': 'Insurance corporations'
              },
              {
                'value': '404',
                'option': 'Banks'
              },
              {
                'value': '405',
                'option': 'Trust and loan corporations'
              },
              {
                'value': '406(1)',
                'option': 'Railway corporations'
              },
              {
                'value': '402(2)',
                'option': 'Railway corporations (Note 2)'
              },
              {
                'value': '407',
                'option': 'Airline corporations'
              },
              {
                'value': '408',
                'option': 'Grain elevator operations'
              },
              {
                'value': '409',
                'option': 'Bus and truck operators'
              },
              {
                'value': '410',
                'option': 'Ship operators'
              },
              {
                'value': '411',
                'option': 'Pipeline operators'
              },
              {
                'value': '412',
                'option': 'Divided businesses'
              },
              {
                'value': '413',
                'option': 'Non-resident corporations'
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '998'
          },
          {
            'type': 'table',
            'num': '1998'
          },
          {'labelClass': 'fullLength'},
          {'labelClass': 'fullLength'},
          {
            'label': 'SUMMARY: ',
            'labelClass': 'bold text-right'
          },
          {
            'label': 'Taxable income earned in all provinces and territories including offshore jurisdictions',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1031'
                }
              }
            ]
          },
          {
            'label': 'Taxable income earned outside of Canada',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1032'
                }
              }
            ]
          },
          {
            'label': 'Total taxable income',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1033'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* "Permanent establishment" is defined in subsection 400(2).',
            'labelClass': 'fullLength'
          },
          {
            'label': '** For corporations other than those described under section 402, use the appropriate calculation described in the Regulations to allocate taxable income.',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {
            'label': 'Notes:',
            'labelClass': 'fullLength bold'
          },
          {
            'label': '1. After determining the allocation of taxable income, you have to calculate the corporation\'s provincial or territorial tax payable. For more information on how to calculate the tax for each province or territory, see the instructions for Schedule 5 in the <i>T2 Corporation - Income Tax Guide</i>.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '2. If the corporation has provincial or territorial tax payable, complete Part 2 on the following pages.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '3. If the corporation is a member of a partnership and the partnership had a permanent establishment in a jurisdiction, select the jurisdiction in Column A and include your proportionate share of the partnership\'s salaries and wages and gross revenue in columns B and D, respectively.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'hideFieldset': true,
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'label': 'Show all provinces and territories for Part 2?',
            'labelClass': 'bold center',
            'inputType': 'radio',
            'num': 'showAll',
            'init': '2'
          }
        ]
      },
      {
        'header': 'Part 2 - Provincial and territorial tax payable, tax credits, and rebates - Newfoundland and Labrador',
        'showWhen': {
          'or': [
            {
              'fieldId': 'provinces',
              'has': {
                'NL': true
              }
            },
            {
              'fieldId': 'provinces',
              'has': {
                'XO': true
              }
            },
            {
              'fieldId': 'showAll',
              'compare': {
                'is': '1'
              }
            },
            {
              'fieldId': 'CP.750',
              'compare': {
                'is': 'NL'
              }
            },
            {
              'fieldId': 'CP.750',
              'compare': {
                'is': 'XO'
              }
            }
          ]
        },
        'rows': [
          {
            'label': '<br><b>Newfoundland and Labrador tax before credits</b> (from Schedule 307)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              },
              null
            ]
          },
          {
            'label': 'Newfoundland and Labrador offshore tax (from Schedule 307)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '205',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '205'
                }
              },
              null
            ]
          },
          {
            'label': 'Gross Newfoundland and Labrador tax (line 200 <b>plus</b> line 205)',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '206',
                  'disabled': 'true'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '207',
                  'disabled': 'true',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '1A'
          },
          {
            'label': 'Newfoundland and Labrador political contribution tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '500',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '500'
                }
              },
              null
            ]
          },
          {
            'label': 'Contribution',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '891',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '891'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Newfoundland and Labrador foreign tax credit (from Schedule 21)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '501',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '501'
                }
              },
              null
            ]
          },
          {
            'label': 'Newfoundland and Labrador manufacturing and processing profits tax credit (from Schedule 300)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '503',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '503'
                }
              },
              null
            ]
          },
          {
            'label': 'Newfoundland and Labrador venture capital tax credit (from Schedule 308)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '504',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '504'
                }
              },
              null
            ]
          },
          {
            'label': 'Newfoundland and Labrador direct equity tax credit (from Schedule 303)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '505',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '505'
                }
              },
              null
            ]
          },
          {
            'label': 'Newfoundland and Labrador resort property investment tax credit (from Schedule 304)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '507',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '507'
                }
              },
              null
            ]
          },
          {
            'label': 'Newfoundland and Labrador non-refundable tax credits (total of lines 500 to 507)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '508'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '509',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '1B'
          },
          {
            'label': 'Subtotal (amount 1A <b>minus</b> amount 1B) (if negative, enter \"0\")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '510',
                  'cannotOverride': true
                }
              }
            ],
            'indicator': '1C'
          },
          {
            'label': 'Newfoundland and Labrador capital tax on financial institutions (from Schedule 305)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '518',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '518'
                }
              }
            ]
          },
          {
            'label': 'Total Newfoundland and Labrador tax payable before refundable tax credits (amount 1C <b>plus</b> line 518)<br>(if negative, enter \"0\")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '519',
                  'cannotOverride': true
                }
              }
            ],
            'indicator': '1D'
          },
          {
            'label': '',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Newfoundland and Labrador research and development tax credit (from Schedule 301)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '520',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '520'
                }
              },
              null
            ]
          },
          {
            'label': 'Newfoundland and Labrador film and video industry tax credit *',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '521',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '521'
                }
              },
              null
            ]
          },
          {
            'label': 'Certificate number',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '821',
                  'type': 'text',
                  'validate': {
                    'or': [
                      {
                        'length': {
                          'min': '9',
                          'max': '9'
                        }
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '821'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Newfoundland and Labrador interactive digital media tax credit*',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '522',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '522'
                }
              },
              null
            ]
          },
          {
            'label': 'Certificate number',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '840',
                  'type': 'text',
                  'validate': {
                    'or': [
                      {
                        'length': {
                          'min': '1',
                          'max': '20'
                        }
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '840'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Newfoundland and Labrador refundable tax credits (total of lines 520 to 522)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '822'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '823',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '1E'
          },
          {
            'label': '<b>Net Newfoundland and Labrador tax payable or refundable credit</b> (amount 1D <b>minus</b> amount 1E)<br>(if a credit, enter amount in brackets) Include this amount on line 255 on page 8',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '209',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '209'
                }
              }
            ],
          },
          {
            'label': '<br>* To claim the credit, file the certificate with your T2 return.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - Provincial and territorial tax payable, tax credits, and rebates - Prince Edward Island',
        'showWhen': {
          'or': [
            {
              'fieldId': 'provinces',
              'has': {
                'PE': true
              }
            },
            {
              'fieldId': 'showAll',
              'compare': {
                'is': '1'
              }
            },
            {
              'fieldId': 'CP.750',
              'compare': {
                'is': 'PE'
              }
            }
          ]
        },
        'rows': [
          {
            'label': '<br><b>Prince Edward Island tax before credits</b> (from Schedule 322)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '210',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '210'
                }
              }
            ],
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Prince Edward Island political contribution tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '525',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '525'
                }
              },
              null
            ]
          },
          {
            'label': 'Contribution',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '892',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '892'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Prince Edward Island foreign tax credit (from Schedule 21)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '528',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '528'
                }
              },
              null
            ]
          },
          {
            'label': 'Prince Edward Island corporate investment tax credit (from Schedule 321)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '530',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '530'
                }
              },
              null
            ]
          },
          {
            'label': 'Prince Edward Island non-refundable tax credits (total of lines 525 to 530)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '531'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '532',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '2A'
          },
          {
            'label': '<b>Net Prince Edward Island tax payable</b> (line 210 <b>minus</b> amount 2A) (if negative, enter \"0\")<br>Include this amount on line 255 on page 8.',
            'layout': 'alignInput',
            'underline': 'double',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '214',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '214'
                }
              }
            ],
          }
        ]
      },
      {
        'header': 'Part 2 - Provincial and territorial tax payable, tax credits, and rebates - Nova Scotia',
        forceBreak: true,
        'showWhen': {
          'or': [
            {
              'fieldId': 'provinces',
              'has': {
                'NS': true
              }
            },
            {
              'fieldId': 'provinces',
              'has': {
                'NO': true
              }
            },
            {
              'fieldId': 'showAll',
              'compare': {
                'is': '1'
              }
            },
            {
              'fieldId': 'CP.750',
              'compare': {
                'is': 'NS'
              }
            },
            {
              'fieldId': 'CP.750',
              'compare': {
                'is': 'NO'
              }
            }
          ]
        },
        'rows': [
          {
            'label': '<br><b>Nova Scotia tax before credits</b> (from Schedule 346)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '215',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '215'
                }
              },
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Nova Scotia offshore tax (from Schedule 346)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '220',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '220'
                }
              },
              null
            ]
          },
          {
            'label': 'Recapture of Nova Scotia research and development tax credit (from Schedule 340)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '221',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '221'
                }
              },
              null
            ]
          },
          {
            'label': 'Gross Nova Scotia tax (total of lines 215 to 221)',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '222'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '223',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '3A'
          },
          {
            'labelClass': 'fullLengths'
          },
          {
            'label': 'Nova Scotia political contribution tax credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '550',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '550'
                }
              },
              null
            ]
          },
          {
            'label': 'Contribution',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '893',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '893'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Nova Scotia foreign tax credit (from Schedule 21)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '554',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '554'
                }
              },
              null
            ]
          },
          {
            'label': 'Nova Scotia food bank tax credit for farmers (from Schedule 2)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '570',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '570'
                }
              },
              null
            ]
          },
          {
            'label': 'Nova Scotia corporate tax reduction for new small businesses * (from Schedule 341)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '556',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '556'
                }
              },
              null
            ]
          },
          {
            'label': 'Certificate number',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '834',
                  'require': {
                    'or': [
                      {
                        'length': {
                          'min': 4,
                          'max': 4
                        }
                      }
                    ]
                  },
                  'type': 'text'
                },
                'padding': {
                  'type': 'tn',
                  'data': '834'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Nova Scotia non-refundable tax credits (total of lines 550, 554, 570, and 556)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1899'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1898',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '3B'
          },
          {
            'label': 'Total Nova Scotia tax payable before refundable tax credits (amount 3A <b>minus</b> amount 3B) (if negative, enter \"0\")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1897',
                  'cannotOverride': true
                }
              }
            ],
            'indicator': '3C'
          },
          {
            'label': 'Nova Scotia film industry tax credit **',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '565',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '565'
                }
              },
              null
            ]
          },
          {
            'label': 'Certificate number',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '836',
                  'type': 'text',
                  'validate': {
                    'or': [
                      {
                        'length': {
                          'min': '9',
                          'max': '9'
                        }
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '836'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Nova Scotia research and development tax credit (from Schedule 340)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '566',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '566'
                }
              },
              null
            ]
          },
          {
            'label': 'Nova Scotia digital media tax credit **',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '567',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '567'
                }
              },
              null
            ]
          },
          {
            'label': 'Certificate number',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '838',
                  'type': 'text',
                  'validate': {
                    'or': [
                      {
                        'length': {
                          'min': '1',
                          'max': '20'
                        }
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '838'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Nova Scotia capital investment tax credit **',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '568',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '568'
                }
              },
              null
            ]
          },
          {
            'label': 'Certificate number',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '841',
                  'type': 'text',
                  'validate': {
                    'or': [
                      {
                        'length': {
                          'min': '1',
                          'max': '20'
                        }
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '841'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Nova Scotia digital animation tax credit**',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '569',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '569'
                }
              },
              null
            ]
          },
          {
            'label': 'Certificate number',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '839',
                  'type': 'text',
                  'validate': {
                    'or': [
                      {
                        'length': {
                          'min': '1',
                          'max': '20'
                        }
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '839'
                }
              },
              null,
              null
            ]
          },
          {
            'forceBreakAfter': true,
            'label': 'Nova Scotia refundable tax credits (total of lines 565 to 569)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1896'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '895',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '3D'
          },
          {
            'label': '<b>Net Nova Scotia tax payable or refundable credit</b> (amount 3C <b>minus</b> amount 3D) (if a credit, enter amount in brackets)<br>Include this amount on line 255 on page 8.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '224',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '224'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': '* The amount of Nova Scotia corporate tax reduction for new small businesses cannot be more than the gross Nova Scotia tax <b>minus</b> all other Nova Scotia tax credits (including the refundable credits).',
            'labelClass': 'fullLength'
          },
          {
            'label': '** To claim the credit, file the certificate with your T2 return.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - Provincial and territorial tax payable, tax credits, and rebates - New Brunswick',
        'showWhen': {
          'or': [
            {
              'fieldId': 'provinces',
              'has': {
                'NB': true
              }
            },
            {
              'fieldId': 'showAll',
              'compare': {
                'is': '1'
              }
            },
            {
              'fieldId': 'CP.750',
              'compare': {
                'is': 'NB'
              }
            }
          ]
        },
        'rows': [
          {
            'label': '<br><b>New Brunswick tax before credits</b> (from Schedule 366)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '225',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '225'
                }
              },
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Recapture of New Brunswick research and development tax credit (from Schedule 360)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '573',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '573'
                }
              },
              null
            ]
          },
          {
            'label': 'Gross New Brunswick tax (line 225 plus line 573)',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '574'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '599',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '4A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'New Brunswick political contribution tax credit (for contributions made before June 1, 2017).',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '575',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '575'
                }
              },
              null
            ]
          },
          {
            'label': 'Contribution',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '894',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '894'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'New Brunswick foreign tax credit (from Schedule 21)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '576',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '576'
                }
              },
              null
            ]
          },
          {
            'label': 'New Brunswick small business investor tax credit (from Schedule 367)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '578',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '578'
                }
              },
              null
            ]
          },
          {
            'label': 'New Brunswick non-refundable tax credits (total of lines 575 to 578)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '577'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1578',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '4B'
          },
          {
            'label': 'Total New Brunswick tax payable before refundable tax credits (amount 4A <b>minus</b> amount 4B) (if negative, enter \"0\")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '579',
                  'cannotOverride': true
                }
              }
            ],
            'indicator': '4C'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'New Brunswick film tax credit *',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '595',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '595'
                }
              },
              null
            ]
          },
          {
            'label': 'Certificate number',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '850',
                  'type': 'text',
                  'validate': {
                    'or': [
                      {
                        'length': {
                          'min': '9',
                          'max': '9'
                        }
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '850'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'New Brunswick research and development tax credit (from Schedule 360)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '597',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '597'
                }
              },
              null
            ]
          },
          {
            'label': 'New Brunswick refundable tax credits (line 595 plus line 597)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '596'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '598',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '4D'
          },
          {
            'label': '<b>Net New Brunswick tax payable or refundable credit</b> (amount 4C <b>minus</b> amount 4D)<br>(if a credit, enter amount in brackets) Include this amount on line 255 on page 8.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '229',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '229'
                }
              }
            ],
          },
          {
            'label': '<br>* To claim the credit, file the certificate with your T2 return.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - Provincial and territorial tax payable, tax credits, and rebates - Ontario',
        'showWhen': {
          'or': [
            {
              'fieldId': 'provinces',
              'has': {
                'ON': true
              }
            },
            {
              'fieldId': 'showAll',
              'compare': {
                'is': '1'
              }
            },
            {
              'fieldId': 'CP.750',
              'compare': {
                'is': 'ON'
              }
            }
          ]
        },
        'rows': [
          {
            'label': '<br><b>Ontario basic income tax</b> (from Schedule 500)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '270',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '270'
                }
              },
              null
            ]
          },
          {
            'label': '<br> Ontario small business deduction (from Schedule 500)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '402',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '402'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 270 <b>minus</b> line 402)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '403'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '405',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '5A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario additional tax re Crown royalties (from Schedule 504)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '274',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '274'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario transitional tax debits (from Schedule 506)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '276',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '276'
                }
              },
              null
            ]
          },
          {
            'label': 'Recapture of Ontario research and development tax credit (from Schedule 508)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '277',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '277'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (total of lines 274 to 277)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '298'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '297',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '5B'
          },
          {
            'label': 'Gross Ontario tax (amount 5A <b>plus</b> amount 5B)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '299',
                  'cannotOverride': true
                }
              }
            ],
            'indicator': '5C'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario resource tax credit (from Schedule 504)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '404',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '404'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario tax credit for manufacturing and processing (from Schedule 502)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '406',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '406'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario foreign tax credit (from Schedule 21)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '408',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '408'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario credit union tax reduction (from Schedule 500)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '410',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '410'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario political contributions tax credit (from Schedule 525)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '415',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '415'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario non-refundable tax credits (total of lines 404 to 415)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '499'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '498',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '5D'
          },
          {
            'label': 'Subtotal (amount 5C <b>minus</b> amount 5D) (if negative, enter \"0\")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '497',
                  'cannotOverride': true
                }
              }
            ],
            'indicator': '5E'
          },
          {
            'label': 'Ontario research and development tax credit (from Schedule 508)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '416',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '416'
                }
              }
            ]
          },
          {
            'label': 'Ontario corporate income tax payable before Ontario corporate minimum tax credit and Ontario community food program donation tax credit for farmers (amount 5E <b>minus</b> line 416) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '496',
                  'cannotOverride': true
                }
              }
            ],
            'indicator': '5F'
          },
          {
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Ontario corporate minimum tax credit (from Schedule 510)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '418',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '418'
                }
              }
            ]
          },
          {
            'label': 'Ontario community food program donation tax credit for farmers (from Schedule 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '420',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '420'
                }
              }
            ]
          },
          {
            'label': 'Ontario corporate income tax payable (amount 5F <b>minus</b> the total of lines 418 and 420) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '495',
                  'cannotOverride': true
                }
              }
            ],
            'indicator': '5G'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario corporate minimum tax (from Schedule 510)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '278',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '278'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario special additional tax on life insurance corporations (from Schedule 512)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '280',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '280'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (line 278 <b>plus</b> line 280)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '494'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '493',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '5H'
          },
          {
            'forceBreakAfter': true,
            'label': 'Total Ontario tax payable before refundable tax credits (amount 5G <b>plus</b> amount 5H)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '492'
                }
              }
            ],
            'indicator': '5I'
          },
          {
            'label': 'Ontario qualifying environmental trust tax credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '450',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '450'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario co-operative education tax credit (from Schedule 550)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '452',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '452'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario apprenticeship training tax credit (from Schedule 552)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '454',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '454'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario computer animation and special effects tax credit (from Schedule 554)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '456',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '456'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario film and television tax credit (from Schedule 556)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '458',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '458'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario production services tax credit (from Schedule 558)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '460',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '460'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario interactive digital media tax credit (from Schedule 560)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '462',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '462'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario sound recording tax credit (from Schedule 562)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '464',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '464'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario book publishing tax credit (from Schedule 564)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '466',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '466'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario innovation tax credit (from Schedule 566)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '468',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '468'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario business-research institute tax credit (from Schedule 568)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '470',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '470'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario refundable tax credits (total of lines 450 to 470)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '491'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '490',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '5J'
          },
          {
            'label': '<b>Net Ontario tax payable or refundable tax credit</b> (amount 5I <b>minus</b> amount 5J)<br>(if a credit, enter amount in brackets) Include this amount on line 255 on page 8.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '290',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '290'
                }
              }
            ],
          }
        ]
      },
      {
        'header': 'Part 2 - Provincial and territorial tax payable, tax credits, and rebates - Manitoba',
        'showWhen': {
          'or': [
            {
              'fieldId': 'provinces',
              'has': {
                'MB': true
              }
            },
            {
              'fieldId': 'showAll',
              'compare': {
                'is': '1'
              }
            },
            {
              'fieldId': 'CP.750',
              'compare': {
                'is': 'MB'
              }
            }
          ]
        },
        'rows': [
          {
            'label': '<b>Manitoba tax before credits</b> (from Schedule 383)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '230',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '230'
                }
              }
            ]
          },
          {
            'label': 'Manitoba foreign tax credit (from Schedule 21)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '601',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '601'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba rental housing construction tax credit (from Schedule 394)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '602',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '602'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba non-refundable manufacturing investment tax credit (from Schedule 381)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '605',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '605'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba non-refundable research and development tax credit (from Schedule 380)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '606',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '606'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba co-op education and apprenticeship tax credit (from Schedule 384)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '603',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '603'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba non-refundable odour-control tax credit (from Schedule 385)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '607',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '607'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba small business venture capital tax credit (from Schedule 387)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '608',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '608'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba non-refundable cooperative development tax credit (from Schedule 390)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '609',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '609'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba Neighbourhoods Alive! tax credit (from Schedule 391)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '610',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '610'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba non-refundable tax credits (total of lines 601 to 610)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '489'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '488',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '6A'
          },
          {
            'label': 'Total Manitoba tax payable before refundable tax credits (line 230 <b>minus</b> amount 6A) (if negative, enter \"0\")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '487',
                  'cannotOverride': true
                }
              }
            ],
            'indicator': '6B'
          },
          {
            'label': 'Manitoba cultural industries printing tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '611',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '611'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba refundable cooperative development tax credit (from Schedule 390)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '612',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '612'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba refundable research and development tax credit (from Schedule 380)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '613',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '613'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba interactive digital media tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '614',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '614'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba book publishing tax credit (from Schedule 389)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '615',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '615'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba green energy equipment tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '619',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '619'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba film and video production tax credit (from Schedule 388)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '620',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '620'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba refundable manufacturing investment tax credit (from Schedule 381)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '621',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '621'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba paid work experience tax credit * (from Schedule 384)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '622',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '622'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba refundable odour-control tax credit for agricultural corporations (from Schedule 385)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '623',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '623'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba data processing investment tax credits (from Schedule 392)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '324',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '324'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba nutrient management tax credit (from Schedule 393)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '325',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '325'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba refundable rental housing construction tax credit (from Schedule 394)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '326',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '326'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba community enterprise development tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '327',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '327'
                }
              },
              null
            ]
          },
          {
            'label': 'Manitoba refundable tax credits (total of lines 611 to 623 plus lines 324 to 327)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '399'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '398',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '6C'
          },
          {
            'label': '<b>Net Manitoba tax payable or refundable tax credit</b> (amount 6B <b>minus</b> amount 6C)<br>(if a credit, enter amount in brackets) Include this amount on line 255 on page 8.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '234',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '234'
                }
              }
            ]
          },
          {
            'label': '* The name of the credit changed from Manitoba co-op education and apprenticeship tax credit to Manitoba paid work experience tax credit as of September 1, 2015.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - Provincial and territorial tax payable, tax credits, and rebates - Saskatchewan',
        'showWhen': {
          'or': [
            {
              'fieldId': 'provinces',
              'has': {
                'SK': true
              }
            },
            {
              'fieldId': 'showAll',
              'compare': {
                'is': '1'
              }
            },
            {
              'fieldId': 'CP.750',
              'compare': {
                'is': 'SK'
              }
            }
          ]
        },
        'rows': [
          {
            'label': '<b>Saskatchewan tax before credits</b> (from Schedule 411)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '235',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '235'
                }
              }
            ]
          },
          {
            'label': 'Saskatchewan political contribution tax credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '624',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '624'
                }
              },
              null
            ]
          },
          {
            'label': 'Contribution',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '890',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '890'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Saskatchewan foreign tax credit (from Schedule 21)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '625',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '625'
                }
              },
              null
            ]
          },
          {
            'label': 'Saskatchewan manufacturing and processing profits tax reduction (from Schedule 404)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '626',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '626'
                }
              },
              null
            ]
          },
          {
            'label': 'Saskatchewan manufacturing and processing investment tax credit (from Schedule 402)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '630',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '630'
                }
              },
              null
            ]
          },
          {
            'label': 'Saskatchewan non-refundable research and development tax credit (from Schedule 403)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '631',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '631'
                }
              },
              null
            ]
          },
          {
            'label': 'Saskatchewan non-refundable tax credits (total of lines 624 to 631)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '396'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '395',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '7A'
          },
          {
            'label': 'Total Saskatchewan tax payable before refundable tax credits (line 235 minus <b>minus</b> amount 7A) (if negative, enter \"0\")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '394',
                  'cannotOverride': true
                }
              }
            ],
            'indicator': '7B'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Saskatchewan qualifying environmental trust tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '641',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '641'
                }
              },
              null
            ]
          },
          {
            'label': 'Saskatchewan refundable manufacturing and processing investment tax credit (from Schedule 402)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '644',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '644'
                }
              },
              null
            ]
          },
          {
            'label': 'Saskatchewan refundable research and development tax credit (from Schedule 403)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '645',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '645'
                }
              },
              null
            ]
          },
          {
            'label': 'Saskatchewan refundable tax credits (total of lines 641 to 645)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '393'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '392',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '7C'
          },
          {
            'forceBreakAfter': true,
            'label': '<b>Net Saskatchewan tax payable or refundable tax credit</b> (amount 7B <b>minus</b> amount 7C)<br>(if a credit, enter amount in brackets) Include this amount on line 255 on page 8.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '239',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '239'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 2 - Provincial and territorial tax payable, tax credits, and rebates - British Columbia',
        'showWhen': {
          'or': [
            {
              'fieldId': 'provinces',
              'has': {
                'BC': true
              }
            },
            {
              'fieldId': 'showAll',
              'compare': {
                'is': '1'
              }
            },
            {
              'fieldId': 'CP.750',
              'compare': {
                'is': 'BC'
              }
            }
          ]
        },
        'rows': [
          {
            'label': '<br><b>British Columbia tax before credits</b> (from Schedule 427)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '240',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '240'
                }
              },
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Recapture of British Columbia scientific research and experimental development (SR&ED) tax credit (from Form T666)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '241',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '241'
                }
              },
              null
            ]
          },
          {
            'label': 'Gross British Columbia tax (line 240 <b>plus</b> line 241)',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '391'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '390',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '8A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'British Columbia foreign tax credit (from Schedule 21)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '650',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '650'
                }
              },
              null
            ]
          },
          {
            'label': 'British Columbia logging tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '651',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '651'
                }
              },
              null
            ]
          },
          {
            'label': 'British Columbia political contribution tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '653',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '653'
                }
              },
              null
            ]
          },
          {
            'label': 'Contribution',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '896',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '896'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'British Columbia farmers\' food donation tax credit (from Schedule 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '683',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '683'
                }
              },
              null
            ]
          },
          {
            'label': 'British Columbia small business venture capital tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '656',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '656'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit at the end of previous tax year',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '880',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '880'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Current-year credit',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '881',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '881'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Certificate number (from SBVC 10)',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '882',
                  'type': 'text',
                  'validate': {
                    'or': [
                      {
                        'length': {
                          'min': '9',
                          'max': '9'
                        }
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '882'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'British Columbia SR&ED non-refundable tax credit (from Form T666)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '659',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '659'
                }
              },
              null
            ]
          },
          {
            'label': 'British Columbia political contribution - senate nominee elections tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '1660'
                }
              },
              null
            ]
          },
          {
            'label': 'British Columbia non-refundable tax credits (total of lines 650, 651, 653, 683, 656, and 659)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '389'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '388',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '8B'
          },
          {
            'label': '<br>Total British Columbia tax payable before refundable tax credits (amount 8A <b>minus</b> amount 8B)(if negative, enter \"0\")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '387',
                  'cannotOverride': true
                }
              }
            ],
            'indicator': '8C'
          },
          {
            'label': 'British Columbia qualifying environmental trust tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '670',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '670'
                }
              },
              null
            ]
          },
          {
            'label': 'British Columbia film and television tax credit (from Form T1196)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '671',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '671'
                }
              },
              null
            ]
          },
          {
            'label': 'British Columbia production services tax credit (from Form T1197)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '672',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '672'
                }
              },
              null
            ]
          },
          {
            'label': 'British Columbia mining exploration tax credit (from Schedule 421)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '673',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '673'
                }
              },
              null
            ]
          },
          {
            'label': 'British Columbia SR&ED refundable tax credit (from Form T666)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '674',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '674'
                }
              },
              null
            ]
          },
          {
            'label': 'British Columbia book publishing tax credit (amount on line 886 multiplied by 90%)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '665',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '665'
                }
              },
              null
            ]
          },
          {
            'label': 'Base amount of Publishing support contributions received in the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '886',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '886'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'British Columbia training tax credit (from Schedule 428)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '679',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '679'
                }
              },
              null
            ]
          },
          {
            'label': 'British Columbia interactive digital media tax credit (from Schedule 429)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '680',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '680'
                }
              },
              null
            ]
          },
          {
            'label': 'British Columbia shipbuilding and ship repair industry tax credit (from Schedule 430)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '681',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '681'
                }
              },
              null
            ]
          },
          {
            'label': 'British Columbia refundable tax credits (total of lines 670 to 681, <b>plus</b> line 665)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '386'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '385',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '8D'
          },
          {
            'label': '<b>Net British Columbia tax payable or refundable tax credit</b> (amount 8C <b>minus</b> amount 8D)<br>(if a credit, enter amount in brackets) Include this amount on line 255 on page 8.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '244',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '244'
                }
              }
            ],
          }
        ]
      },
      {
        'header': 'Part 2 - Provincial and territorial tax payable, tax credits, and rebates - Yukon',
        'showWhen': {
          'or': [
            {
              'fieldId': 'provinces',
              'has': {
                'YT': true
              }
            },
            {
              'fieldId': 'showAll',
              'compare': {
                'is': '1'
              }
            },
            {
              'fieldId': 'CP.750',
              'compare': {
                'is': 'YT'
              }
            }
          ]
        },
        'rows': [
          {
            'label': '<br><b>Yukon tax before credits</b> (from Schedule 443)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '245',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '245'
                }
              }
            ],
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Yukon political contribution tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '675',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '675'
                }
              },
              null
            ]
          },
          {
            'label': 'Contribution',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '897',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '897'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Yukon foreign tax credit (from Schedule 21)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '676',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '676'
                }
              },
              null
            ]
          },
          {
            'label': 'Yukon manufacturing and processing profits tax credit (from Schedule 440)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '677',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '677'
                }
              },
              null
            ]
          },
          {
            'label': 'Yukon non-refundable tax credits (total of lines 675 to 677)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '699'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1000',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '9A'
          },
          {
            'label': 'Total Yukon tax payable before refundable tax credits (line 245 <b>minus</b> amount 9A) (if negative, enter \"0\")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '697',
                  'cannotOverride': true
                }
              }
            ],
            'indicator': '9B'
          },
          {
            'label': '<br> Yukon research and development tax credit (from Schedule 442)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '698',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '698'
                }
              }
            ],
          },
          {
            'label': '<b>Net Yukon tax payable or refundable tax credit</b> (amount 9B <b>minus</b> line 698)<br>(if a credit, enter amount in brackets) Include this amount on line 255 on page 8.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '249',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '249'
                }
              }
            ],
          }
        ]
      },
      {
        'header': 'Part 2 - Provincial and territorial tax payable, tax credits, and rebates - Northwest Territories',
        'showWhen': {
          'or': [
            {
              'fieldId': 'provinces',
              'has': {
                'NT': true
              }
            },
            {
              'fieldId': 'showAll',
              'compare': {
                'is': '1'
              }
            },
            {
              'fieldId': 'CP.750',
              'compare': {
                'is': 'NT'
              }
            }
          ]
        },
        'rows': [
          {
            'label': '<b>Northwest Territories tax before credits</b> (from Schedule 461)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '250',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '250'
                }
              }
            ],
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Northwest Territories political contribution tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '700',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '700'
                }
              },
              null
            ]
          },
          {
            'label': 'Contribution',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '898',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '898'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Northwest Territories foreign tax credit (from Schedule 21)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '701',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '701'
                }
              },
              null
            ]
          },
          {
            'label': 'Northwest Territories investment tax credit (from Schedule 460)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '705',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '705'
                }
              },
              null
            ]
          },
          {
            'label': 'Northwest Territories non-refundable tax credits (line 700 <b>plus</b> line 701)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '696'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '695',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '10A'
          },
          {
            'forceBreakAfter': true,
            'label': '<b>Net Northwest Territories tax payable</b> (line 250 <b>minus</b> amount 10A) (if negative, enter \"0\")<br>Include this amount on line 255 below.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '254',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '254'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 2 - Provincial and territorial tax payable, tax credits, and rebates - Nunavut',
        'showWhen': {
          'or': [
            {
              'fieldId': 'provinces',
              'has': {
                'NU': true
              }
            },
            {
              'fieldId': 'showAll',
              'compare': {
                'is': '1'
              }
            },
            {
              'fieldId': 'CP.750',
              'compare': {
                'is': 'NU'
              }
            }
          ]
        },
        'rows': [
          {
            'label': '<b>Nunavut tax before credits</b> (from Schedule 481)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '260',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '260'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Nunavut political contribution tax credit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '725',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '725'
                }
              },
              null
            ]
          },
          {
            'label': 'Contribution',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '899',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '899'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Nunavut foreign tax credit (from Schedule 21)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '730',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '730'
                }
              },
              null
            ]
          },
          {
            'label': 'Nunavut non-refundable tax credits (line 725 <b>plus</b> line 730)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '694'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '693',
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '11A'
          },
          {
            'label': '<b>Net Nunavut tax payable</b> (line 260 <b>minus</b> amount 11A) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '264',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'cannotOverride': true
                },
                'padding': {
                  'type': 'tn',
                  'data': '264'
                }
              }
            ],
          },
          {
            'label': 'Include this amount on line 255 below.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Summary',
        'rows': [
          {
            'label': '<br><br>Enter the total net tax payable or refundable tax credits for all provinces and territories on line 255.',
            'labelClass': 'fullLength'
          },
          {
            'label': '<br><br><b>Net provincial and territorial tax payable or refundable tax credits</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '255',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '255'
                }
              }
            ]
          },
          {
            'label': '<br><br>If the amount on line 255 is positive, enter the net provincial and territorial tax payable on line 760 on page 9 of the T2 return.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the amount on line 255 is negative, enter the net provincial and territorial refundable tax credits on line 812 on page 9 of the T2 return.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  };
})();