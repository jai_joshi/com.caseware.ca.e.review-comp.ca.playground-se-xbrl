(function() {
  wpw.tax.create.diagnostics('t2s5', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    var depFields103To127 = [
      't2s5.103',
      't2s5.104',
      't2s5.105',
      't2s5.107',
      't2s5.108',
      't2s5.109',
      't2s5.111',
      't2s5.113',
      't2s5.115',
      't2s5.117',
      't2s5.119',
      't2s5.121',
      't2s5.123',
      't2s5.125',
      't2s5.126',
      't2s5.127'
    ];

    var depFields143To167 = [
      't2s5.143',
      't2s5.144',
      't2s5.145',
      't2s5.147',
      't2s5.148',
      't2s5.149',
      't2s5.151',
      't2s5.153',
      't2s5.155',
      't2s5.157',
      't2s5.159',
      't2s5.161',
      't2s5.163',
      't2s5.165',
      't2s5.166',
      't2s5.167'
    ];

    var depFields = depFields103To127.concat(depFields143To167);

    diagUtils.diagnostic('0050010', common.prereq(common.and(common.requireFiled('T2S5'), function(tools) {
      return tools.field('t2j.750').get() == 'MJ';
    }), function(tools) {
      return tools.requireOne(tools.list(depFields), 'isNonZero');
    }));

    diagUtils.diagnostic('0050020', common.prereq(common.and(common.requireFiled('T2S5'), function(tools) {
      return tools.field('t2j.750').get() == 'MJ';
    }), function(tools) {
      var bucketNum100 = tools.field('t2s5.100').get();
      var validInputs = [
        '402', '403', '404', '405', '406(1)', '406(2)',
        '407', '408', '409', '410', '411', '412', '413'
      ];
      return validInputs.indexOf(bucketNum100) != -1;
    }));

    diagUtils.diagnostic('0050021', common.prereq(common.and(common.requireFiled('T2S5'), function(tools) {
      return tools.field('t2s5.100').get() == '404';
    }), function(tools) {
      return tools.requireOne(tools.list(depFields103To127), 'isNonZero') &&
          tools.requireOne(tools.list(depFields143To167), 'isNonZero');
    }));

    diagUtils.diagnostic('0050022', common.prereq(common.and(common.requireFiled('T2S5'), function(tools) {
      return tools.field('t2s5.100').get() == '407';
    }), function(tools) {
      return tools.requireOne(tools.list(depFields103To127), 'isNonZero') &&
          tools.requireOne(tools.list(depFields143To167), 'isNonZero');
    }));

    diagUtils.diagnostic('0050025', common.prereq(
        common.and(
            common.requireFiled('T2S5'),
            function(tools) {
              return tools.checkMethod(tools.list(depFields), 'isNonZero');
            }),
        function(tools) {
          return tools.checkAll(tools.field('998').getRows(), function(row) {
            return !tools.checkMethod(tools.select(row, [2, 4]), 'isNonZero') || tools.requireOne([row[0], row[1]], 'isChecked');
          })
        }));

    diagUtils.diagnostic('0050030', common.prereq(common.and(common.requireFiled('T2S5'), common.check('t2j.812', 'isNonZero')),
        function(tools) {
          var requiredFields = [
            't2s5.520', 't2s5.521', 't2s5.565', 't2s5.566', 't2s5.567',
            't2s5.568', 't2s5.595', 't2s5.597', 't2s5.450', 't2s5.452',
            't2s5.454', 't2s5.456', 't2s5.458', 't2s5.460', 't2s5.462',
            't2s5.464', 't2s5.466', 't2s5.468', 't2s5.470', 't2s5.611',
            't2s5.612', 't2s5.613', 't2s5.614', 't2s5.615', 't2s5.619',
            't2s5.620', 't2s5.621', 't2s5.622', 't2s5.623', 't2s5.324',
            't2s5.325', 't2s5.326', 't2s5.327', 't2s5.641', 't2s5.643',
            't2s5.644', 't2s5.645', 't2s5.670', 't2s5.671', 't2s5.672',
            't2s5.673', 't2s5.674', 't2s5.665', 't2s5.679', 't2s5.680',
            't2s5.681', 't2s5.698', 't2s5.740'
          ];
          return tools.requireOne(tools.list(requiredFields), 'isNonZero');
        }));

    diagUtils.diagnostic('0050150', common.prereq(common.requireFiled('T2S5'), common.generateChecks('t2s5.500', ['891'])));

    diagUtils.diagnostic('0050150', common.prereq(common.requireFiled('T2S5'), common.generateChecks('t2s5.525', ['892'])));

    diagUtils.diagnostic('0050150', common.prereq(common.requireFiled('T2S5'), common.generateChecks('t2s5.550', ['893'])));

    diagUtils.diagnostic('0050150', common.prereq(common.requireFiled('T2S5'), common.generateChecks('t2s5.575', ['894'])));

    diagUtils.diagnostic('0050150', common.prereq(common.requireFiled('T2S5'), common.generateChecks('t2s5.624', ['890'])));

    diagUtils.diagnostic('0050150', common.prereq(common.requireFiled('T2S5'), common.generateChecks('t2s5.653', ['896'])));

    diagUtils.diagnostic('0050150', common.prereq(common.requireFiled('T2S5'), common.generateChecks('t2s5.675', ['897'])));

    diagUtils.diagnostic('0050150', common.prereq(common.requireFiled('T2S5'), common.generateChecks('t2s5.700', ['898'])));

    diagUtils.diagnostic('0050150', common.prereq(common.requireFiled('T2S5'), common.generateChecks('t2s5.725', ['899'])));

    diagUtils.diagnostic('0050500', common.prereq(common.requireFiled('T2S5'), common.generateChecks('t2s5.556', ['834'])));

    diagUtils.diagnostic('0050501', common.prereq(common.and(common.requireFiled('T2S5'), common.check('565', 'isNonZero')),
        function(tools) {
          var table = tools.field('t2s345.900');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne(tools.field('t2s5.836'), 'isNonZero') || tools.requireOne(row[0], 'isNonZero');
          })
        }));

    //Requires submission/filing of certificate
    diagUtils.diagnostic('0050503', common.prereq(common.requireFiled('T2S5'), common.check(['565'], 'isZero')));

    //Requires submission/filing of certificate
    diagUtils.diagnostic('0050523', common.prereq(common.requireFiled('T2S5'), common.check(['521'], 'isZero')));

    //Requires submission/filing of certificate
    diagUtils.diagnostic('0050532', common.prereq(common.requireFiled('T2S5'), common.check(['595'], 'isZero')));

    //todo 0050532 we dont have New Brunswick forms yet

    diagUtils.diagnostic('0050600', common.prereq(common.and(common.requireFiled('T2S5'), common.check('620', 'isNonZero')),
        function(tools) {
          return tools.form('t2s388').field('900').require('isNonZero');
        }));

    diagUtils.diagnostic('0050525', common.prereq(common.and(common.requireFiled('T2S5'), common.check(['521'], 'isNonZero')),
        function(tools) {
          var table = tools.field('t2s302.900');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne([row[0], tools.field('821')], 'isNonZero');
          });
        }));

    diagUtils.diagnostic('0050530', common.prereq(common.requireFiled('T2S5'), common.generateChecks('t2s5.595', ['850'])));

    diagUtils.diagnostic('0050580', common.prereq(common.requireFiled('T2S5'), common.generateChecks('t2s5.656', ['880', '881'])));

    diagUtils.diagnostic('0050590', common.prereq(common.requireFiled('T2S5'), common.generateChecks('t2s5.881', ['882'])));

    diagUtils.diagnostic('0050665', common.prereq(common.requireFiled('T2S5'), common.generateChecks('t2s5.665', ['886'])));

    diagUtils.diagnostic('005.840', common.prereq(common.requireFiled('T2S5'), common.generateChecks('t2s5.522', ['840'])));

    diagUtils.diagnostic('005.507', common.prereq(common.requireFiled('T2S5'), function(tools) {
      return tools.field('507').require(function() {
        return this.get() < 50000;
      })
    }));

    diagUtils.diagnostic('005.839', common.prereq(common.and(common.requireFiled('T2S5'), function(tools) {
      return tools.field('t2s5.569').isNonZero();
    }), function(tools) {
      return tools.requireOne(tools.field('t2s5.839'), 'isNonZero') ||
          tools.requireOne(tools.field('t2s348.300'), 'isNonZero');
    }));

    diagUtils.diagnostic('005.556', common.prereq(common.requireFiled('T2S5'), function(tools) {
      return !(tools.field('556').get() > (tools.field('223').get() - tools.field('550').get() -
          tools.field('554').get() - tools.field('565').get() - tools.field('566').get() - tools.field('567').get()
          - tools.field('568').get() - tools.field('569').get()));
    }));

    diagUtils.diagnostic('005.522', common.prereq(common.requireFiled('T2S5'),
        function(tools) {
      return tools.field('522').require('isZero');
    }));
  });
})();
