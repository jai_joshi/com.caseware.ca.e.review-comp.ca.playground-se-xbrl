(function () {
  wpw.tax.create.formData('t2a5', {
    formInfo: {
      abbreviation: 't2a5',
      title: ' Alberta Royalty Tax Deduction',
      schedule: 'Schedule 5',
      formFooterNum: 'AT176 (jul-12)',
      // headerImage: 'canada-alberta',
      showCorpInfo: true,
      category: 'Alberta Tax Forms',
      dynamicFormWidth: true
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: 'Complete this version of Schedule 5 if the corporation\'s <b>taxation year commences on or ' +
            'after December 4, 2003</b> and the corporation is eligible to claim an amount under the Royalty ' +
            'Tax Deduction program. See Special Notice Vol. 5 No. 17 clarifying the rules and the Guide for ' +
            'details on successoring rules. If the corporation\'s taxation year commences before December ' +
            '4, 2003, then use the AT176.9801 version of Schedule 5',
            labelClass: 'fullLength'
          },
          {
            label: '<b>Complete schedule 7 before completing this schedule.</b>' +
            ' Use additional forms if necessary. Report all monetary values in dollars;DO NOT include cents.',
            labelClass: 'fullLength'
          },
          {
            label: 'References to sections below are to those of the Alberta Corporate Tax Act.',
            labelClass: 'fullLength'
          },
          {
            'type': 'infoField',
            'label': 'Was there a change in control that created the immediately preceding taxation year end?',
            labelClass: 'bold',
            'num': '100',
            'tn': '100',
            'inputType': 'radio'
          },
          {
            label: 'If "Yes", then all properties/pools change status by one succession at the beginning of the current ' +
            'taxation year. i.e. the unsuccessored pool becomes a first successor, first becomes second and ' +
            'the existing second is no longer eligible for the Royalty Tax Deduction.'
          },
          {
            'type': 'infoField',
            'label': 'Does the corporation have any successored pools to report?',
            labelClass: 'bold',
            'num': '200',
            'tn': '200',
            'inputType': 'radio'
          },
          {
            label: 'If "Yes", complete AREA D on page 2 <b>before</b> completing page 1.'
          },
          {
            label: 'If " No", do <b>not</b> complete AREA D on page 2, only complete page 1.'
          }
        ]
      },
      {
        header: 'AREA A - CALCULATION OF UNSUCCESSORED POOL BALANCES',
        rows: [
          {
            "label": "Crown charges under section 20(6)(a) to (e) with reference to section 20(13) if applicable. <br>Enter the amount from Schedule 7, line 061",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "001"
                },
                "padding": {
                  "type": "tn",
                  "data": "001"
                }
              }
            ]
          },
          {
            "label": "Resource allowance claimed under section 20(6)(g).<br>If the resource allowance for Alberta purposes differs from that claimed for federal purposes,<br>enter the amount from Alberta Schedule 12 line 024.<br>Otherwise enter the amount from federal Schedule 1 line 346 (if negative, enter \"0\")",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "005"
                },
                "padding": {
                  "type": "tn",
                  "data": "005"
                }
              }
            ]
          },
          {
            "label": "Reimbursements received under the terms of a contract in respect of amounts included on line 001 under section 20(6)(f)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "007"
                },
                "padding": {
                  "type": "tn",
                  "data": "007"
                }
              }
            ]
          },
          {
            "label": "Change in the pool balance during the taxation year.<br>Line 001 minus (line 005 + line 007) (if negative, enter \"0\")",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "009"
                },
                "padding": {
                  "type": "tn",
                  "data": "009"
                }
              }
            ]
          },
          {
            "label": "Pool amount available for carry-forward at the end of the preceding taxation year.<br>(complete AREA C below and enter the amount from line 045)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "011"
                },
                "padding": {
                  "type": "tn",
                  "data": "011"
                }
              }
            ]
          },
          {
            "label": "Net amount available for unsuccessored pool (line 009 + line 011)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "013"
                },
                "padding": {
                  "type": "tn",
                  "data": "013"
                }
              }
            ]
          },
          {
            "label": "Taxable Income from AT1 page 2, line 062 (if negative, enter \"0\")",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "015"
                },
                "padding": {
                  "type": "tn",
                  "data": "015"
                }
              }
            ]
          },
          {
            "label": "Royalty tax deduction claim amount in respect of the pool for the year.<br>Cannot exceed the lesser of: Line 013 and (line 015 minus AREA D line 140)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "016"
                },
                "padding": {
                  "type": "tn",
                  "data": "016"
                }
              }
            ]
          },
          {
            "label": "Pool amount available to be carried forward:<br>Line 013 minus line 016",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "017"
                },
                "padding": {
                  "type": "tn",
                  "data": "017"
                }
              }
            ]
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            'type': 'infoField',
            'inputType': 'dropdown',
            'num': '026',
            'tn': '026',
            inputClass: 'std-input-col-width-2',
            'label': 'Was there a transfer of the resource pools during the year?',
            'indicatorColumn': true,
            'options': [
              {
                'option': ' Yes, due to disposition of all or substantially all of the Canadian resource properties\n' +
                'under section 20(8)',
                'value': '1'
              },
              {
                'option': ' Yes, due to a change in control or ceasing to be exempt from tax under section 20(14) ',
                'value': '2'
              },
              {
                'option': 'No OR transfers have occurred during the year but section 20(8) does not apply',
                'value': '3'
              }
            ]
          },
          {
            label: 'If the value at line 026 is 1 or 2, then enter the legal name of the corporation who acquired the resource pools:'
          },
          {
            type: 'infoField',
            tn: '027',
            num: '027'
          }
        ]
      },
      {
        header: 'AREA B - CALCULATION OF THE ROYALTY TAX DEDUCTION',
        rows: [
          {
            "label": "Royalty Tax Deduction for the year is the sum of amounts from all pools: <br>AREA A line 016 + AREA D line 140",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "021"
                },
                "padding": {
                  "type": "tn",
                  "data": "021"
                }
              }
            ]
          },
          {
            label: 'The amount at line 021 cannot exceed the amount of taxable income on line 015 of AREA A ',
            labelClass: 'bold'
          },
          {
            label: 'Enter the amount at line 021 on AT1 page 2, line 064',
            labelClass: 'bold italic'
          }
        ]
      },
      {
        header: 'AREA C - UNSUCCESSORED POOL AMOUNTS AVAILABLE FOR CARRY-FORWARD',
        rows: [
          {
            label: 'Includes unsuccessored pool amounts transferred due to amalgamation under section 20(10) or wind-up of a wholly-owned\n' +
            'subsidiary under section 20(11) during this taxation year:',
            labelClass: 'fullLength'
          },
          {
            type: 'table',
            num: '999'
          },
          {
            "label": "Corporation's unsuccessored pool amounts carried forward from the preceding taxation year",
            labelClass: 'bold',
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "043"
                },
                "padding": {
                  "type": "tn",
                  "data": "043"
                }
              }
            ]
          },
          {
            "label": "Total unsuccessored pool amounts available for carry-forward (line 041+ line 043)<br>Enter this amount in AREA A line 011 above",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "045"
                },
                "padding": {
                  "type": "tn",
                  "data": "045"
                }
              }
            ]
          }
        ]
      },
      {
        header: 'AREA D - CALCULATION OF SUCCESSORED POOL BALANCES',
        rows: [
          {
            label: 'One line is to be used to report the property or properties per date of event. List from the oldest date of event to the newest date of event. '
          },
          {
            label: 'Use additional forms if additional space is required.'
          },
          {type: 'horizontalLine'},
          {
            label: 'Second Successored Pool Information:',
            labelClass: 'bold'
          },
          {
            type: 'table', num: '998'
          },
          {type: 'horizontalLine'},
          {
            label: 'First Successored Pool Information:',
            labelClass: 'bold'
          },
          {
            type: 'table', num: '997'
          },
          {type: 'horizontalLine'},
          {
            label: 'Total Royalty Tax Deduction Claim Amounts for All Successored Pools:<br>Amount in box H + amount in box I',
            'labelCellClass': 'alignRight bold',
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": false
            },
            "columns": [
              {
                "input": {
                  "num": "140"
                },
                "padding": {
                  "type": "tn",
                  "data": "140"
                }
              }, null
            ]
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            label: '* An amount can be entered in column C or column D but not in both.<br>' +
            'If there was no change in control during the year, or if the pool was not transferred, ' +
            'this amount should equal the amount from last year\'s Column G.',
            labelClass: 'fullLength'
          },
          {
            label: '** The amount in column D should equal the amount from the vendor\'s/predecessor\'s/corporation\'s ' +
            'Schedule 5 column G and this pool must be moved up to the next\n' +
            'successor level.',
            labelClass: 'fullLength'
          },
          {
            label: 'Note that where there was a change in control of the corporation, for ' +
            'taxation years ending after December 4, <b>2002</b>, pools should have been moved up by one successor level. ',
            labelClass: 'fullLength'
          }
        ]
      }
    ]
  });
})();
