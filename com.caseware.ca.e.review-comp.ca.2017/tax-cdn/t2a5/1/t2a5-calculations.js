(function () {

  wpw.tax.create.calcBlocks('t2a5', function (calcUtils) {
    calcUtils.calc(function (calcUtils, field) {
      //Area A
      field('009').assign(Math.max(0, field('001').get() - (field('005').get() + field('007').get())));
      field('011').assign(field('045').get());
      field('013').assign(field('009').get() + field('011').get());
      field('015').assign(field('T2A.062').get());
      field('017').assign(field('013').get() - field('016').get());
    });

    calcUtils.calc(function (calcUtils, field) {
      if (field('026').get() == 3) {
        field('027').assign();
      } else {
        field('027').disabled(false)
      }
    });

    calcUtils.calc(function (calcUtils, field) {
      //area B
      field('021').assign(Math.min(field('016').get() + field('140').get(), field('015').get()));
      //area C
      field('045').assign(field('041').get() + field('043').get());
    });

    calcUtils.calc(function (calcUtils, field) {
      //area D
      field('998').getRows().forEach(function (row) {
        var cOrDVal = 0;
        if (row[2].get() > 0) {
          cOrDVal = row[2].get()
        } else {
          cOrDVal = row[3].get()
        }
        row[5].assign(Math.min(cOrDVal, row[4].get()));
        row[6].assign(cOrDVal - row[5].get())
      });

      field('997').getRows().forEach(function (row) {
        var cOrDVal = 0;
        if (row[2].get() > 0) {
          cOrDVal = row[2].get()
        } else {
          cOrDVal = row[3].get()
        }
        row[5].assign(Math.min(cOrDVal, row[4].get()));
        row[6].assign(cOrDVal - row[5].get())
      });

      field('140').assign(field('998').total(5).get() + field('997').total(5).get())
    });


  });
})();
