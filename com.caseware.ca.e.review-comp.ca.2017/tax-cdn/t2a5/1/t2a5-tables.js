(function () {

  wpw.tax.create.tables('t2a5', {
    '999': {
      hasTotals: true,
      columns: [
        {header: 'Predecessor\'s Name', tn: '031', type: 'text'},
        {
          header: 'Alberta Corporate<br>Account Number (CAN)<br>(enter the 9 or 10 digit number',
          tn: '033',
          type: 'text'
        },
        {header: 'Date of Event', tn: '035', type: 'date',
          colClass: 'std-input-width'},
        {
          header: 'Carry-Forward Amount Transferred',
          colClass: 'std-input-col-width',
          tn: '037', total: true, totalTn: '041', totalNum: '041',
          totalMessage: 'Total amount transferred:'
        }
      ]
    },
    '998': {
      hasTotals: true,
      columns: [
        {
          header: 'A<br>Name of:\n' +
          'Vendor, or\n' +
          'Predecessor, or\n' +
          'Corporation if change in control', tn: '101', type: 'text'
        },
        {
          header: 'B<br>Date of Event',
          tn: '103',
          type: 'date',
          colClass: 'std-input-width'
        },
        {
          header: 'C<br>Pool amount available\n' +
          'for carry-forward at the\n' +
          'end of the preceding year*',
          tn: '105',
          colClass: 'std-input-width'
        },
        {
          header: 'D<br>Acquisition of all or substantially\n' +
          'all Canadian resource properties\n' +
          'under section 20(8) or change\n' +
          'in control under section 20(14)**',
          tn: '107',
          colClass: 'std-input-width'
        },
        {
          header: 'E<br>Property Income under\n' +
          'section 20(1)(c)',
          tn: '109',
          colClass: 'std-input-width'
        },
        {
          header: 'F<br>Royalty Tax Deduction Claim\n' +
          'amount in respect of the pool\n' +
          'for the year cannot exceed\n' +
          'the lesser of (C or D) and E\n',
          tn: '111',
          total: true,
          totalIndicator: 'H',
          totalMessage: 'Subtotals of the Royalty Tax Deduction Claim Amounts and Carry-Forward Pool Amounts for Second Successored Pools:',
          colClass: 'std-input-col-width'
        },
        {
          header: 'Pool amount available\n' +
          'for carry-forward\n' +
          'before transfer\n' +
          '[(C or D) - F]\n',
          tn: '113',
          total: true,
          totalTn: '115',
          totalNum: '115',
          colClass: 'std-input-col-width'
        }
      ]
    },
    '997' : {
      hasTotals: true,
      columns: [
        {
          header: 'A<br>Name of:\n' +
          'Vendor, or\n' +
          'Predecessor, or\n' +
          'Corporation if change in control', tn: '121', type: 'text'
        },
        {
          header: 'B<br>Date of Event',
          tn: '123',
          type: 'date',
          colClass: 'std-input-width'
        },
        {
          header: 'C<br>Pool amount available\n' +
          'for carry-forward at the\n' +
          'end of the preceding year*',
          tn: '125',
          colClass: 'std-input-width'
        },
        {
          header: 'D<br>Acquisition of all or substantially\n' +
          'all Canadian resource properties\n' +
          'under section 20(8) or change\n' +
          'in control under section 20(14)**',
          tn: '127',
          colClass: 'std-input-width'
        },
        {
          header: 'E<br>Property Income under\n' +
          'section 20(1)(c)',
          tn: '129',
          colClass: 'std-input-width'
        },
        {
          header: 'F<br>Royalty Tax Deduction Claim\n' +
          'amount in respect of the pool\n' +
          'for the year cannot exceed\n' +
          'the lesser of (C or D) and E\n',
          tn: '131',
          total: true,
          totalIndicator: 'I',
          totalMessage: 'Subtotals of the Royalty Tax Deduction Claim Amounts and Carry-Forward Pool Amounts for First Successored Pools:',
          colClass: 'std-input-col-width'
        },
        {
          header: 'Pool amount available\n' +
          'for carry-forward\n' +
          'before transfer\n' +
          '[(C or D) - F]\n',
          tn: '133',
          total: true,
          totalTn: '135',
          totalNum: '135',
          colClass: 'std-input-col-width'
        }
      ]
    }
  })
})();
