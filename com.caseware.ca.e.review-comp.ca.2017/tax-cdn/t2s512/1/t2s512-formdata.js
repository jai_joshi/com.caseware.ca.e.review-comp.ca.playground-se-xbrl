(function() {

  var dateTimeConditionArr = [
    {
      switchIf: {
        formId: 'cp',
        fieldId: '063',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '066',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '071',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '072',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '076',
        value: '1'
      }
    }
  ];

  wpw.tax.global.formData.t2s512 = {
    'formInfo': {
      'abbreviation': 'T2S512',
      'title': 'Ontario Special Additional Tax on Life Insurance Corporations (SAT)',

      //'subTitle': '(2009 and later tax years)',
      'schedule': 'Schedule 512',
      //'code': 'Code 0901',,
      headerImage: 'canada-federal',
      'showCorpInfo': true,
      'description': [
        {
          type: 'list',
          items: [
            {
              label: 'Complete this schedule for a life insurance corporation carrying on business in Ontario at' +
              ' any time in the tax year. The Ontario special additional tax on life insurance corporations (SAT) ' +
              'is levied under section 63 of the <i>Taxation Act, 2007</i> (Ontario).'
            },
            {
              label: 'Subsection 181(3) provides the basis to determine the carrying value of a life insurance' +
              ' corporation\'s assets or any other amount under section 63 of the <i>Taxation Act, 2007</i> ' +
              '(Ontario) for its capital, taxable paid-up capital, and taxable capital employed in Canada.'
            },
            {label: 'Unless otherwise noted, all amounts are determined as of the end of the year.'},
            {
              label: 'Unless otherwise noted, references to parts, subsections, subparagraphs, clauses, and' +
              ' regulations are from the federal <i>Income Tax Act</i> and the <i>Income Tax Regulations</i>.'
            },
            {label: 'File this schedule with the <i>T2 Corporation Income Tax Return</i>.'}
          ]
        }
      ],
      category: 'Ontario Forms'
    },
    'sections': [
      {
        'header': 'Part 1 - Capital (resident life insurance corporation)',
        'rows': [
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Long-term debt',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              },
              null
            ]
          },
          {
            'label': 'Capital stock (for a corporation incorporated without share capital, its members\' contributions)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '112',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '112'
                }
              },
              null
            ]
          },
          {
            'label': 'Retained earnings',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '114',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '114'
                }
              },
              null
            ]
          },
          {
            'label': 'Contributed surplus',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '116',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '116'
                }
              },
              null
            ]
          },
          {
            'label': 'Any other surpluses',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '118',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '118'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '119'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '120'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Deferred tax debit balance or future tax assets',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              },
              null
            ]
          },
          {
            'label': 'Any deficit deducted in computing shareholders\' equity'
          },
          {
            'label': '(subsection 63(4) of the <i>Taxation Act, 2007</i> (Ontario))',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '132',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '132'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '133'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '134'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': '<b>Capital for the year </b> (amount A <b>minus</b> amount B) (if negaitve, enter \'0\')',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '150'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 2 - Taxable capital employed in Canada (resident life insurance corporation)',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Capital for the year (amount from line 150 in Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '250'
                }
              },
              null
            ]
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Capital of foreign insurance subsidiaries (amount BBB from page 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '162'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total reserve liabilities',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '164',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '164'
                }
              },
              null
            ]
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Total reserve liabilities of foreign subsidiaries (amount CCC from page 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '166',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '166'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '167'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Canadian reserve liabilities (Regulation 2400(1))',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '168',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '168'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Proportion of capital over reserve liabilities:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '100'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Amount determined under subparagraph 190.11(b)(ii)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '180',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '180'
                }
              }
            ]
          },
          {
            'label': '<b>Taxable capital employed in Canada (resident life insurance corporation)</b><br>(amount on line 170 <b>plus</b> the amount on line 180)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '190',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '190'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 3- Capital and taxable capital employed in Canada (non-resident life insurance corporation)',
        'rows': [
          {
            'label': 'The amount that is the greater of:'
          },
          {
            'label': ' a) The amount, if any, by which the corporation\'s surplus funds derived from operations as of the end of the year, exceeds the total of all amounts, each of which is:'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': ' i) an amount on which it was required or would have been required, but for subsection 219(5.2), to have paid tax under Part XIV for a previous tax year, less the part, if any, of the amount on which tax was payable, or would have been payable, described in subparagraph 219(4)(a)(i.1), or',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': ' ii) an amount on which it was required or would have been required, but for subsection 219(5.2), to have paid tax under subsection 219(5.1) for the year because of the transfer of an insurance business to which subsection 138(11.5) or (11.92) applied',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '210',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '210'
                }
              },
              null
            ]
          },
          {
            'label': 'b) Attributed surplus for the year (Regulation 2400(1))',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '214',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '214'
                }
              },
              null
            ]
          },
          {
            'label': 'Greater of the amounts on line 210 and line 214 ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '215'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '216'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Add:',
            'labelClass': 'bold'
          },
          {
            'label': 'Any other surpluses related to insurance businesses carried on in Canada',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '218',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '218'
                }
              },
              null
            ]
          },
          {
            'label': 'Long-term debt that may reasonably be regarded as related to insurance businesses carried on in Canada',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '222',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '222'
                }
              },
              null
            ]
          },
          {
            'label': 'Amount determined under subparagraph 190.11(b)(ii)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '226',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '226'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '121'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '122'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': '<b>Capital and taxable capital employed in Canada (non-resident life insurance corporation)</b> (amount G <b>plus</b> amount H)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '290',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '290'
                }
              }
            ],
            'indicator': 'I'
          }
        ]
      },
      {
        'header': 'Part 4 - Capital allowance',
        'rows': [
          {
            'type': 'infoField',
            'inputType': 'radio',
            'init': '2',
            'label': 'Is the corporation related at the end of the year to another life insurance corporation that carries on business in Canada? ',
            'tn': '310',
            'num': '310',
            'labelWidth': '85%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If you answered <b>yes</b> to the question at line 310, use Schedule 513,<i> Agreement Among Related Life Insurance Corporations (Ontario)</i>, to calculate the capital allowance to be entered on line 330.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If you have not filed Schedule 513 with your return because another life insurance corporation in the related group has filed it, complete the following information for that corporation: ',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Name of corporation',
            'tn': '315',
            'type': 'infoField'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'splitTable',
            'side1': [
              {
                'type': 'infoField',
                'inputType': 'custom',
                'format': [
                  '{N9}RC{N4}',
                  'NR'
                ],
                'label': 'Business Number',
                'tn': '320',
                'num': '320'
              }
            ],
            'side2': [
              {
                'type': 'infoField',
                'inputType': 'date',
                'label': 'Tax year-end',
                'tn': '325',
                'num': '325',
                'dateTimeShowWhen': {
                  'or': dateTimeConditionArr
                }
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If you answered <b>no</b> to the question at line 310, complete the following calculation: '
          },
          {
            'label': 'Basic amount',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '499'
                }
              }
            ],
            'indicator': '1'
          },
          {
            'label': '<b>Add</b> the following'
          },
          {
            'label': '0.50 x [(lesser of $50 million and line 190 or line 290) <b>minus</b> $10 million] (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '123',
                  'disabled': true
                }
              },
              null
            ]
          },
          {
            'label': '0.25 x [(lesser of $100 million and line 190 or line 290) <b>minus</b> $50 million] (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '124',
                  'disabled': true
                }
              },
              null
            ]
          },
          {
            'label': '0.50 x [(lesser of $300 million and line 190 or line 290) <b>minus</b> $200 million] (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '125',
                  'disabled': true
                }
              },
              null
            ]
          },
          {
            'label': '0.75 x [(line 190 or line 290) <b>minus</b> $300 million] (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '126',
                  'disabled': true
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '127'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '128'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '2'
          },
          {
            'label': 'Total (amount 1<b> plus</b> amount 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '498'
                }
              }
            ],
            'indicator': '3'
          },
          {
            'label': '<b>Capital allowance</b>(amount 3 or the amount from line 400 on Schedule 513, whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '330',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '330'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 5 - Taxable paid-up capital',
        'rows': [
          {
            'label': 'Taxable capital employed in Canada (amount from line 190 in Part 2 or line 290 in Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '129'
                }
              },
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Deduct:</b>'
          },
          {
            'label': 'Capital allowance (amount from line 330 in Part 4 )',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '131'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '140'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '141'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'type': 'table',
            'num': '200'
          },
          {
            'label': '<b>Taxable paid-up capital</b> (amount J <b>multiplied</b> by amount K)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '350',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '350'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* If the taxable income (amount from line 360 of the T2 return) is 0, calculate Ontario taxable income and taxable income earned in all provinces and territories as if taxable income were $1,000.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '** If the corporation has a permanent establishment only in Ontario, enter the amount from line 360 from page 3 of the T2 return. Otherwise, enter the taxable income allocated to Ontario from column F in Part 1 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '*** Includes the offshore jurisdictions for Nova Scotia, and Newfoundland and Labrador.'
          }
        ]
      },
      {
        'header': 'Part 6 - SAT ',
        'rows': [
          {
            'label': 'Taxable paid-up capital (amount from line 350 in Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '331'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'L'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Gross SAT:'
          },
          {
            'type': 'table',
            'num': '300'
          },
          {
            'label': 'Deduct',
            'labelClass': 'bold'
          },
          {
            'label': 'Ontario corporate income tax payable (amount from line G6 of Schedule 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '332'
                }
              },
              null
            ]
          },
          {
            'label': 'Ontario corporate minimum tax payable (amount from line 278 of Schedule 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '333'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '334'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '335'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'label': '<b>SAT payable</b> ** (amount M <b>minus</b> amount N) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '336'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Cannot be more than 365.'
          },
          {
            'label': '** The SAT payable for the tax year is added to the corporation\'s corporate minimum tax (CMT) credit carryforward. The CMT credit can be deducted to reduce Ontario corporate income tax payable. Complete Part 4 of Schedule 510, <i>Ontario Corporate Minimum Tax</i>, to report SAT payable for the tax year.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Work Chart',
        'rows': [
          {
            'label': 'Complete the following tables to determine the amounts to use in Part 2, on page 1, in calculating the taxable capital employed in Canada of a Canadian resident corporation that carried on a life insurance business in Ontario.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Table 1',
            'labelClass': 'tabbed2 bold'
          },
          {
            'type': 'table',
            'num': '600'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Table 2',
            'labelClass': 'tabbed2 bold'
          },
          {
            'type': 'table',
            'num': '700'
          },
          {
            'type': 'table',
            'num': '750'
          },
          {
            'label': '<b>Notes</b>'
          },
          {
            'label': '1) The amount in column 5 of Table 2, for each subsidiary, cannot be less than zero',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '2) The amounts in column 6 of Table 2 are those that would be reported by the foreign insurance subsidiary for that year if it had to report to the Office of the Superintendent of Financial Institutions.',
            'labelClass': 'tabbed fullLength'
          }
        ]
      }
    ]
  };
})();
