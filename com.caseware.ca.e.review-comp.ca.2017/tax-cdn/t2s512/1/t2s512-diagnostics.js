(function() {
  wpw.tax.create.diagnostics('t2s512', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S512'), common.bnCheck('320')));

    diagUtils.diagnostic('5120001', common.prereq(common.check(['t2s5.280'], 'isNonZero'), common.requireFiled('T2S512')));

    diagUtils.diagnostic('5120002', common.prereq(function(tools) {
      return (tools.field('CP.750').get() == 'ON' || tools.field('CP.750').get() == 'MJ') &&
          (tools.field('t2s5.113').isNonZero() || tools.field('t2s5.153').isNonZero() || tools.field('t2s5.013').isChecked()) &&
          (tools.field('cp.122').get() == '2' || tools.field('t2s97.300').get() == '10');
    }, common.requireFiled('T2S512')));

    diagUtils.diagnostic('5120003', common.prereq(common.and(
        common.requireFiled('T2S512'),
        function(tools) {
          return tools.field('cp.122').get() == '2' &&
              tools.field('t2s97.300').get() != '10';
        }), common.or(
        common.check(['110', '112', '114', '116', '118'], 'isNonZero', true),
        common.check(['164', '166', '168'], 'isNonZero', true))));

    diagUtils.diagnostic('5120004', common.prereq(common.and(
        common.requireFiled('T2S512'),
        function(tools) {
          return tools.field('t2s97.300').get() == '10'
        }), common.check(['210', '214', '218', '222', '226'], 'isNonZero', true)));

    diagUtils.diagnostic('5120005', common.prereq(common.and(
        common.requireFiled('T2S512'),
        common.and(
            common.check(['320', '325'], 'isEmpty', true),
            common.check('330', 'isPositive'))),
        common.check('310', 'isNonZero')));
  });
})();
