(function() {

  wpw.tax.create.calcBlocks('t2s512', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      // Part 1 Calculation//
      calcUtils.sumBucketValues('119', ['110', '112', '114', '116', '118']);
      calcUtils.equals('120', '119');
      calcUtils.sumBucketValues('133', ['130', '132']);
      calcUtils.equals('134', '133');
      calcUtils.subtract('150', '120', '134');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 2 Calculation//
      calcUtils.equals('250', '150');
      calcUtils.sumBucketValues('162', ['250', '160']);
      calcUtils.sumBucketValues('167', ['164', '166']);
      calcUtils.equals('169', '168');
      calcUtils.equals('171', '167');
      calcUtils.sumBucketValues('190', ['170', '180']);
      calcUtils.divide('173', '169', '171');
      calcUtils.equals('172', '162');
      calcUtils.multiply('170', ['173', '172']);
      calcUtils.equals('160', '1000');
      calcUtils.equals('166', '1002');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 3 Calculation//
      calcUtils.sumBucketValues('121', ['218', '222', '226']);
      calcUtils.equals('122', '121');
      calcUtils.sumBucketValues('290', ['216', '122']);
      calcUtils.max('215', ['210', '214']);
      calcUtils.equals('216', '215');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 4 Calculation//
      field('499').assign(10000000);

      calcUtils.getApplicableValue('129', ['190', '290']);
      var value190Or290 = field('129').get();
      field('123').assign(Math.max(0, (field('ratesOn.807').get() * (Math.min(value190Or290, field('ratesOn.808').get())) - field('ratesOn.809').get())));
      field('124').assign(Math.max(0, (field('ratesOn.810').get() * (Math.min(value190Or290, field('ratesOn.811').get())) - field('ratesOn.812').get())));
      field('125').assign(Math.max(0, (field('ratesOn.813').get() * (Math.min(value190Or290, field('ratesOn.814').get())) - field('ratesOn.815').get())));
      field('126').assign(Math.max(0, (field('ratesOn.816').get() * value190Or290 - field('ratesOn.817').get())));
      calcUtils.sumBucketValues('127', ['123', '124', '125', '126']);
      calcUtils.equals('128', '127');
      calcUtils.sumBucketValues('498', ['499', '128']);
      //schedule 513 not available
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 5 Calculation//
      var taxableIncomeAllocation = calcUtils.getTaxableIncomeAllocation('ON');
      field('397').assign(taxableIncomeAllocation.provincialTI);
      field('398').assign(taxableIncomeAllocation.allProvincesTI);
      field('397').source(taxableIncomeAllocation.provincialTISourceField);
      field('398').source(taxableIncomeAllocation.allProvincesTISourceField);
      calcUtils.equals('131', '330');
      calcUtils.sumBucketValues('140', ['129', '131']);
      calcUtils.equals('141', '140');
      calcUtils.divide('400', '397', '398');
      calcUtils.multiply('350', ['141', '400']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 6 Calculation//
      calcUtils.equals('331', '350');
      calcUtils.equals('399', '331');
      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();
      field('401').assign(daysFiscalPeriod);
      field('402').assign(365);
      field('460').assign((field('399').get() * 1.25 / 100) * field('401').get() / field('402').get());
      calcUtils.getGlobalValue('332', 't2s5', '495');
      calcUtils.getGlobalValue('333', 't2s5', '278');
      calcUtils.sumBucketValues('334', ['332', '333']);
      calcUtils.equals('335', '334');
      field('336').assign(Math.max(0, (field('460').get() - field('335').get())));
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Workchart
      field('600').getRows().forEach(function(row, rIndex) {
        var table700Row = field('700').getRow(rIndex);
        row[5].assign(
            row[1].get() +
            row[2].get() +
            row[3].get() +
            row[4].get()
        );
        row[8].assign(
            row[5].get() -
            row[6].get() +
            row[7].get()
        );
        table700Row[1].assign(row[8].get());
        table700Row[5].assign(Math.max(0,
            table700Row[1].get() - (table700Row[2].get() + table700Row[3].get() + table700Row[4].get()))
        );
      });
    });


  });
})();
