(function() {
  wpw.tax.global.tableCalculations.t2s512 = {
    '100': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          cellClass: 'alignCenter',
          type: 'none',
          width: '170px'
        },
        {
          cellClass: 'alignCenter',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          width: '150px'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          width: '20px'
        },
        {
          width: '130px'
        },
        {
          type: 'none'
        }
      ],
      cells: [
        {
          '1': {
            label: 'Amount E',
            labelClass: 'center fullLength'
          },
          '2': {
            num: '169'
          },
          '3': {
            label: ' x Amount C ',
            labelClass: 'center fullLength'
          },
          '4': {
            num: '172'
          },
          '5': {
            label: '=',
            labelClass: 'center'
          },
          '6': {
            tn: '170',
            num: '170', validate: {'or': [{'matches': '^[.\\d]{1,13}$'}, {'check': 'isEmpty'}]}
          },
          '7': {
            label: 'F'
          }
        },
        {
          '0': {
            type: 'none'
          },
          '1': {
            label: 'Amount D',
            labelClass: 'center fullLength',

          },
          '2': {
            num: '171',

          },
          '4': {
            num: '173',
            type: 'none'
          },
          '6': {
            type: 'none'
          }
        }
      ]
    },
    '200': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          width: '200px'
        },
        {
          width: '10px',
          type: 'none'
        },
        {
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          width: '15px',
          type: 'none'
        },
        {
          type: 'none',
          width: '150px'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          width: '20px'
        }
      ],
      cells: [
        {
          '0': {
            label: 'Ontario domestic factor *:'
          },
          '2': {
            label: 'Ontario taxable income **',
            labelClass: 'center fullLength'
          },
          '3': {
            num: '397',
            cellClass: 'alignCenter'
          },
          '4': {
            label: '='
          },
          '6': {
            num: '400'
          },
          '7': {
            label: 'K'
          }
        },
        {
          '2': {
            label: 'taxable income earned in all provinces and territories ***',
            labelClass: 'fullLength center',

          },
          '3': {
            num: '398',
            cellClass: 'alignCenter',

          },
          '6': {
            type: 'none'
          }
        }
      ]
    },
    '300': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          width: '70px',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',

        },
        {
          width: '20px',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          cellClass: 'alignCenter',
          width: '80px',
          type: 'none'
        },
        {
          width: '20px',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          width: '200px',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          width: '10px',
          type: 'none'
        },
        {
          width: '100px',

        },
        {
          width: '10px',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          width: '50px',

        },
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          width: '130px',

        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          width: '131px'
        }],
      cells: [{
        '0': {
          label: 'Amount L'
        },
        '1': {
          num: '399'
        },
        '2': {
          label: ' x ',
          labelClass: 'center'
        },
        '3': {
          num: '301',
          label: '1.25%'
        },
        '4': {
          label: ' x ',
          labelClass: 'center'
        },
        '5': {
          label: 'Number of days in the tax year*',
          labelClass: 'center'
        },
        '7': {
          num: '401',
          cellClass: 'singleUnderline'
        },
        '9': {
          type: 'none'
        },
        '10': {
          label: ' = '
        },
        '11': {
          num: '460', validate: {'or': [{'matches': '^[.\\d]{1,13}$'}, {'check': 'isEmpty'}]},
          tn: '460'
        },
        '12': {
          label: 'M'
        }
      },
        {
          '1': {
            type: 'none'
          },
          '5': {
            label: '365',
            labelClass: 'center',

          },
          '7': {
            num: '402',

          },
          '9': {
            type: 'none'
          },
          '11': {
            type: 'none'
          }
        }
      ]
    },
    '600': {
      repeats: ['700'],
      showNumbering: true,
      columns: [
        {
          header: '<br>1<br><br>Name of foreign <br> insurance subsidiary',
          width: '300px',
          type: 'text'
        },
        {
          header: '<br>2<br><br>Long-term<br>debt'
        },
        {
          header: '<br>3<br><br>Capital stock or<br> members\'<br> contributions'
        },
        {
          header: '<br>4<br><br>Retained earnings'
        },
        {
          header: '<br>5<br><br>Surpluses'
        },
        {
          header: '<br>6<br><br>Subtotal <br> 2 + 3 + 4 = 5'
        },
        {
          header: '<br>7<br><br>Deferred tax<br> debit balance or <br> future tax assets'
        },
        {
          header: '<br>8<br><br>Deficit deducted <br>in computing<br>shareholders\'<br>equity'
        },
        {
          header: '<br>9<br><br>Capital<br> 6 - (7+8)<br> Enter in column (2)<br>in Table 2 below'
        }
      ]
    },
    '700': {
      hasTotals: true,
      fixedRows: true,
      showNumbering: true,
      superHeaders: [
        {
          divider: false
        },
        {
          divider: true
        },
        {
          header: '3 Capital stock and long-term debt invested in the subsidiary per clause 63(6)(a) of the' +
          ' Taxation Act, 2007 (Ontario)',
          divider: true,
          colspan: '2'
        }
      ],
      columns: [
        {
          header: '<br>1<br><br> Name of foreign<br>insurance subsidiary',
          width: '300px',
          type: 'text'
        },
        {
          header: '<br>2<br><br>Capital of foreign insurance subsidiary per subsection 63(6) of' +
          'the <i>Taxation Act, 2007</i> (Ontario)<br>(from column 9 in Table 1)'
        },
        {
          header: '<br><br><br><br><br><br><br>Capital stock'
        },
        {
          header: '<br><br><br><br><br><br><br>Long-term debt'
        },
        {
          header: '<br>4<br><br>Any other surplus contributed into the subsidiary per clause 63(6)(b)' +
          'the <i>Taxation Act, 2007</i> (Ontario)'
        },
        {
          header: '<br>5<br><br>Amount<br> determined under subsection 63(6) of the <i>Taxation Act, 2007</i>' +
          ' (Ontario) (capital of foreign insurance subsidiaries) columns 2 - (3 + 4) (see Note 1)',
          totalNum: '1000',
          total: true,
          totalMessage: '<b>Totals</b>'
        },
        {
          header: '<br>6<br><br>Total reserve liabilities under subsection 63(7) of the Taxation Act, 2007 (Ontario)' +
          '<br>(see Note 2)',
          totalNum: '1002',
          total: true
        }
      ]
    },
    '750': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          width: '100px'
        },
        {
          type: 'none',
          width: '100px'
        },
        {
          type: 'none',
          width: '100px'
        },
        {
          type: 'none',
          width: '130px'
        },
        {
          type: 'none',
          width: '122px'
        },
        {
          type: 'none',
          width: '122px'
        }
      ],
      cells: [
        {
          '4': {
            label: '<b>BBB</b><br> (enter on line 160 in Part 2)',
            labelClass: 'center'
          },
          '5': {
            label: '<b>CCC</b><br>(enter on line 166 in Part 2)',
            labelClass: 'center'
          }
        }
      ]
    }

  }
})();
