(function() {

  function getTableTotal(calcUtils, tableNum, cIndex) {
    var total = 0;
    var table = calcUtils.field(tableNum);
    table.getRows().forEach(function(row) {
      total += row[cIndex].get();
    });
    return total
  }

  function setAmountClaimed(calcUtils, summaryRow) {
    var claimOption = calcUtils.field('101').get();
    //option: '1. Maximize all discretionary deductions, even creating or increasing taxable loss'
    if (claimOption == 1) {
      summaryRow[6].assign(summaryRow[2].get())
    }
    //option: '2. Apply discretionary deductions to get to zero taxable income'
    else if (claimOption == 2) {
      summaryRow[6].assign(summaryRow[4].get())
    }
    //option: '3. Do not claim any discretionary deductions'
    else {
      //remove all value
      summaryRow[6].assign(0);
      //let user manually input the value
      summaryRow[6].disabled(false)
    }
  }

  function setGlobalValueToCells(calcUtils, rowIndex, tableNum, summaryRow, fieldInfo) {
    if (fieldInfo.formId && fieldInfo.fieldId) {
      var tableCell = calcUtils.field(tableNum).cell(rowIndex, 2);
      //for value which is total of repeated forms fields
      if (fieldInfo.isSumRepeatedValue) {
        var sum = 0;
        calcUtils.allRepeatForms(fieldInfo.formId).forEach(function(form) {
          sum += form.field(fieldInfo.fieldId).get();
        });
        tableCell.assign(sum);
        // var repeatFormIds = wpw.tax.actions.getFilteredRepeatIds(fieldInfo.formId);
        // calcUtils.setRepeatSummaryValue(
        //   {
        //     fieldId: tableNum,
        //     rowIndex: rowIndex,
        //     colIndex: 2
        //   },
        //   repeatFormIds,
        //   fieldInfo.fieldId)
      }
      //for value which is not from repeated forms
      else {
        tableCell.assign(calcUtils.form(fieldInfo.formId).field(fieldInfo.fieldId).get());
      }
    }
  }

  function runTableCalcs(calcUtils, tableNum) {
    var table = calcUtils.field(tableNum);
    table.getRows().forEach(function(row, rowIndex) {
      var summaryRowInfo = table.getRowInfo(rowIndex);
      var availableAmountInfo = summaryRowInfo.availableAmount;
      var optimizedAmountInfo = summaryRowInfo.optimizedAmount;
      if (availableAmountInfo && optimizedAmountInfo) {
        setGlobalValueToCells(calcUtils, rowIndex, tableNum, row, availableAmountInfo);
        setGlobalValueToCells(calcUtils, rowIndex, tableNum, row, optimizedAmountInfo);
      }
      setAmountClaimed(calcUtils, row)
    });
  }

  wpw.tax.create.calcBlocks('optimizationWorkchart', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field, form) {
      //Net income (loss) for income tax purposes before discretionary deductions
      var otherDeductionLists = ['401', '402', '404', '407', '408', '409', '410', '411', '414', '416', '417'];
      var otherDeductionAmount = 0;
      otherDeductionLists.forEach(function(fieldId) {
        otherDeductionAmount += form('T2S1').field(fieldId).get();
      });

      field('301').assign(
          form('T2S1').field('502').get() -
          form('T2S1').field('499').get() -
          otherDeductionAmount
      );
    });

    calcUtils.calc(function(calcUtils, field, form) {
      runTableCalcs(calcUtils, '500');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Calculate Net income/loss for income tax purposes
      field('302').assign(getTableTotal(calcUtils, '500', 6));
      calcUtils.equals('303', '302');
      //net income/loss can be negative
      calcUtils.subtract('304', '301', '303', true);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      runTableCalcs(calcUtils, '510');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Calculate Taxable Income
      field('305').assign(getTableTotal(calcUtils, '510', 6));
      calcUtils.equals('306', '305');
      //taxable income is not allow to be negative
      calcUtils.subtract('307', '304', '306');
    });
  });
})();
