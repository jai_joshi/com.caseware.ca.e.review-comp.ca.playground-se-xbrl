(function() {
  'use strict';

  wpw.tax.global.formData.optimizationWorkchart = {
    formInfo: {
      abbreviation: 'optimizationwc',
      title: 'Optimization Workchart',
      showCorpInfo: true,
      headerImage: 'cw',
      category: 'Workcharts'
    },
    sections: [
      {
        header: 'Optimization option',
        rows: [
          {
            label: 'Please choose the optimization option you wish to use:',
            labelClass: 'bold',
            labelWidth: '40%',
            num: '101',
            type: 'infoField',
            inputType: 'dropdown',
            init: '1',
            options: [
              {
                option: '1. Maximize all discretionary deductions, even creating or increasing ' +
                'Net loss for income tax purposes',
                value: '1'
              },
              {option: '2. Apply discretionary deductions to get to zero taxable income', value: '2'},

              {option: '3. Do not claim any discretionary deductions', value: '3'}
            ]
          },
          {labelClass: 'fullLength'}
          //todo: To be implement later
  /*        {
            label: '- Please indicate the priority order of discretionary deductions you wish to claim:',
            labelClass: 'bold fullLength',
            showWhen: {
              or: [
                {fieldId: '101', compare: {is: '2'}},
                {fieldId: '101', compare: {is: '3'}}
              ]
            }
          },
           {
           label: '<b>Note:</b><br>' +
           '- If there is no option selected, CorpTax will claim credits which are expiring earliest ' +
           'first from all discretionary deductions<br>' +
           '- Otherwise, CorpTax will claim deductions in selected order',
           labelClass: 'tabbed fullLength',
           showWhen: {
           or: [
           {fieldId: '101', compare: {is: '2'}},
           {fieldId: '101', compare: {is: '3'}}
           ]
           }
           },
           {
           label: '1st .',
           labelClass: 'tabbed',
           labelWidth: '5%',
           type: 'infoField',
           inputType: 'dropdown',
           num: '151',
           allowClearingValue: true,
           options: populateCreditList(151),
           showWhen: {
           or: [
           {fieldId: '101', compare: {is: '2'}},
           {fieldId: '101', compare: {is: '3'}}
           ]
           }
           },
           {
           label: '2nd .',
           labelClass: 'tabbed',
           labelWidth: '5%',
           type: 'infoField',
           inputType: 'dropdown',
           num: '152',
           allowClearingValue: true,
           options: populateCreditList(152),
           showWhen: {
           or: [
           {fieldId: '101', compare: {is: '2'}},
           {fieldId: '101', compare: {is: '3'}}
           ]
           }
           },
           {
           label: '3rd .',
           labelClass: 'tabbed',
           labelWidth: '5%',
           type: 'infoField',
           inputType: 'dropdown',
           num: '153',
           allowClearingValue: true,
           options: populateCreditList(153),
           showWhen: {
           or: [
           {fieldId: '101', compare: {is: '2'}},
           {fieldId: '101', compare: {is: '3'}}
           ]
           }
           },
           {
           labelClass: 'fullLength',
           showWhen: {
           or: [
           {fieldId: '101', compare: {is: '2'}},
           {fieldId: '101', compare: {is: '3'}}
           ]
           }
           }*/
        ]
      },
      {
        header: 'Table of Discretionary Deductions',
        rows: [
          {
            'label': 'Net income (loss) for income tax purposes before discretionary deductions',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '301'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'table',
            'num': '500'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '302'
                }
              },
              {
                'input': {
                  'num': '303'
                },
                'padding': {
                  'type': 'text',
                  'data': ' = '
                }
              }
            ]
          },
          {
            'label': 'Net income (loss) for income tax purposes ',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '304'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '510'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '305'
                }
              },
              {
                'input': {
                  'num': '306'
                },
                'padding': {
                  'type': 'text',
                  'data': ' = '
                }
              }
            ]
          },
          {
            'label': 'Taxable income',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '307'
                }
              }
            ]
          }
        ]
      }
    ]
  };
})();
