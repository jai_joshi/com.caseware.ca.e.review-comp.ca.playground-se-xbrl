(function () {
  var tableWidth = 'calc((100% - 131px))';

  function getCalculationTableHeading(showHeader) {
    return [
      {
        "type": "none"
      },
      {
        colClass: 'std-padding-width',
        "type": "none"
      },
      {
        colClass: 'std-input-width',
        "header": showHeader ? "<b>Available amount to claim</b>" : '',
        cellClass: 'alignRight',
        "disabled": true,
        "decimals": 0
      },
      {
        colClass: 'std-padding-width',
        "type": "none"
      },
      {
        colClass: 'std-input-width',
        "header": showHeader ? "<b>Optimized amount calculated by CorpTax</b>" : '',
        cellClass: 'alignRight',
        "disabled": true,
        "decimals": 0
      },
      {
        colClass: 'std-padding-width',
        "type": "none"
      },
      {
        colClass: 'std-input-width',
        "header": showHeader ? "<b>Amount Claimed</b><br>" +
        "(To claim a different amount, override the calculated amount. Note: you will then have to " +
        "manually adjust the respective forms)" : '',
        cellClass: 'alignRight',
        "decimals": 2
      },
      {
        "type": "none",
        colClass: 'std-padding-width'
      }
    ]
  }

  function getSectionHeading(sectionHeading, isSubHeading) {
    return [
      {
        0: {label: sectionHeading, cellClass: isSubHeading ? 'bold' : 'bold singleUnderline'},
        2: {type: 'none'},
        4: {type: 'none'},
        6: {type: 'none'}
      }
    ]
  }

  function getCalculationTableRows(labelArray) {
    var tableRows = [];

    if (!angular.isArray(labelArray))
      labelArray = [labelArray];

    for (var i = 0; i < labelArray.length; i++) {
      var rowDataObj = labelArray[i];
      var inputRow = {0: {label: rowDataObj.label}};
      inputRow[1] = {tn: rowDataObj.tn};
      inputRow.availableAmount = rowDataObj.availableAmount;
      inputRow.optimizedAmount = rowDataObj.optimizedAmount;
      tableRows.push(inputRow);
    }
    return tableRows
  }

  wpw.tax.global.tableCalculations.optimizationWorkchart = {
    "500": {
      "fixedRows": true,
      "infoTable": true,
      "width": tableWidth,
      "columns": getCalculationTableHeading(true),
      cells: getSectionHeading('Discretionary deductions applied against Net income (loss)').concat
      (
        getSectionHeading('Schedule 8', true),
        getCalculationTableRows([
          {
            label: 'Capital cost allowance from Schedule 8',
            tn: '403',
            availableAmount: {formId: 'T2S8W', fieldId: '214', isSumRepeatedValue: true},
            optimizedAmount: {formId: 'T2S8W', fieldId: '217', isSumRepeatedValue: true}
          }
        ]),
        getSectionHeading('Schedule 10', true),
        getCalculationTableRows([
          {
            label: 'Cumulative eligible capital deduction from Schedule 10',
            tn: '405',
            availableAmount: {formId: 'T2S10', fieldId: '112'},
            optimizedAmount: {formId: 'T2S10', fieldId: '112'}
          }
        ]),
        getSectionHeading('Schedule 6', true),
        getCalculationTableRows([
          {
            label: 'Allowable business investment loss from Schedule 6',
            tn: '406',
            availableAmount: {formId: 'T2S6', fieldId: '715'},
            optimizedAmount: {formId: 'T2S6', fieldId: '715'}
          }
        ]),
        getSectionHeading('Schedule 13', true),
        getCalculationTableRows([
            {
              label: 'Other reserves on line 280 from Schedule 13',
              tn: '412',
              availableAmount: {formId: 'T2S13', fieldId: '280'},
              optimizedAmount: {formId: 'T2S13', fieldId: '280'}
            }
          ]
        )
      )
    },
    "510": {
      "fixedRows": true,
      "infoTable": true,
      "width": tableWidth,
      "columns": getCalculationTableHeading(),
      cells: getSectionHeading('Discretionary deductions applied against Taxable income').concat
      (
        getSectionHeading('Schedule 2', true),
        getCalculationTableRows([
          {
            label: 'Charitable donations from Schedule 2', tn: '311',
            availableAmount: {formId: 'T2S2', fieldId: '256'},
            optimizedAmount: {formId: 'T2S2', fieldId: '260'}
          },
          {
            label: 'Gifts to Canada, a province, or a territory from Schedule 2', tn: '312',
            availableAmount: {formId: '', fieldId: ''},
            optimizedAmount: {formId: '', fieldId: ''}
          },
          {
            label: 'Cultural gifts from Schedule 2 ', tn: '313',
            availableAmount: {formId: 'T2S2', fieldId: '413'},
            optimizedAmount: {formId: 'T2S2', fieldId: '460'}
          },
          {
            label: 'Ecological gifts from Schedule 2 ', tn: '314',
            availableAmount: {formId: 'T2S2', fieldId: '513'},
            optimizedAmount: {formId: 'T2S2', fieldId: '560'}
          },
          {
            label: 'Gifts of medicine from Schedule 2 ', tn: '315',
            availableAmount: {formId: 'T2S2', fieldId: '613'},
            optimizedAmount: {formId: 'T2S2', fieldId: '660'}
          }
        ]),
        getSectionHeading('Schedule 3', true),
        getCalculationTableRows([
          {
            label: 'Taxable dividends deductible under section 112 or 113, or subsection 138(6) from Schedule 3',
            tn: '320',
            availableAmount: {formId: 'T2S3', fieldId: '320'},
            optimizedAmount: {formId: 'T2S3', fieldId: '320'}
          }
        ]),
        getSectionHeading('Schedule 4', true),
        getCalculationTableRows([
          {
            label: 'Non-capital losses of previous tax years from Schedule 4 ', tn: '331',
            availableAmount: {formId: 'T2S4', fieldId: '893'},
            optimizedAmount: {formId: 'T2S4', fieldId: '130'}
          },
          {
            label: 'Net capital losses of previous tax years from Schedule 4 ', tn: '332',
            availableAmount: {formId: 'T2S4', fieldId: '880'},
            optimizedAmount: {formId: 'T2S4', fieldId: '225'}
          },
          {
            label: 'Restricted farm losses of previous tax years from Schedule 4 ', tn: '333',
            availableAmount: {formId: 'T2S4', fieldId: '855'},
            optimizedAmount: {formId: 'T2S4', fieldId: '430'}
          },
          {
            label: 'Farm losses of previous tax years from Schedule 4 ', tn: '334',
            availableAmount: {formId: 'T2S4', fieldId: '872'},
            optimizedAmount: {formId: 'T2S4', fieldId: '330'}
          },
          {
            label: 'Limited partnership losses of previous tax years from Schedule 4', tn: '335',
            availableAmount: {formId: 'T2S4', fieldId: '675'},
            optimizedAmount: {formId: 'T2S4', fieldId: '675'}
          }
        ]),
        getSectionHeading('Others', true),
        getCalculationTableRows([
          {
            label: 'Taxable capital gains or taxable dividends allocated from a central credit union', tn: '340',
            availableAmount: {formId: 'T2J', fieldId: '340'},
            optimizedAmount: {formId: 'T2J', fieldId: '340'}
          },
          {
            label: 'Prospector\'s and grubstaker\'s shares ', tn: '350',
            availableAmount: {formId: 'T2J', fieldId: '350'},
            optimizedAmount: {formId: 'T2J', fieldId: '350'}
          },
          {
            label: 'Part VI.1 tax deduction ', tn: '325',
            availableAmount: {formId: 'T2J', fieldId: '325'},
            optimizedAmount: {formId: 'T2J', fieldId: '325'}
          }
        ])
      )
    }
  }
})();
