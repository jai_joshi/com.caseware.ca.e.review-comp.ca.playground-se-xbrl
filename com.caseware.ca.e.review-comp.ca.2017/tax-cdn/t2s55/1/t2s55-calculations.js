(function() {

  wpw.tax.create.calcBlocks('t2s55', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      var isCCPC = (field('CP.120').get() == 1);
      var isDIC = field('CP.189').get() == 1;
      var clearArr = {
        Part1: ['099', '100', '160', '190'],
        Part2: ['199', '200', '290']
      };
      if (isCCPC || isDIC) {
        calcUtils.getGlobalValue('099', 'T2S3', '460');
        calcUtils.sumBucketValues('100', ['099', '098']);
        field('150').assign(field('T2S3.455').get() + field('T2S3.543').get().totals[4]);
        field('150').source(field('T2S3.455'));
        calcUtils.setRepeatSummaryValue('160', 'T2S53', '590');
        field('160').source(field('T2S53.590'));

        calcUtils.subtract('170', '150', '160');
        calcUtils.subtract('185', '170', '180');
        calcUtils.multiply('190', ['185'], 0.2);
        calcUtils.removeValue(clearArr['Part2'], true);
      }
      else {
        calcUtils.getGlobalValue('199', 'T2S3', '460');
        calcUtils.sumBucketValues('200', ['199', '198']);
        calcUtils.getGlobalValue('210', 'T2S54', '290');
        calcUtils.subtract('285', '210', '280');
        calcUtils.multiply('290', ['285'], 0.2);
        calcUtils.removeValue(clearArr['Part1'], true);
      }
    });
  });
})();
