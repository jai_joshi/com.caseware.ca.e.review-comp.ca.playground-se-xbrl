(function() {
  wpw.tax.create.diagnostics('t2s55', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0550010', common.prereq(common.and(
        common.requireFiled('T2S55'),
        common.check(['190'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['150']), 'isNonZero');
        }));

    diagUtils.diagnostic('0550020', common.prereq(common.check(['290'], 'isNonZero'), common.requireFiled('T2S54')));

    diagUtils.diagnostic('0550030', common.prereq(common.check(['160'], 'isNonZero'), common.requireFiled('T2S53')));

    diagUtils.diagnostic('0550070', common.prereq(common.check(['t2j.710'], 'isNonZero'),
        common.and(
            common.requireFiled('T2S55'),
            common.or(
                common.requireFiled('T2S53'),
                common.requireFiled('T2S54')))));

    diagUtils.diagnostic('055.200', common.prereq(common.requireFiled('T2S55'),
        function(tools) {
      var forms = wpw.tax.form.allRepeatForms('t2s54');
      return tools.checkAll(forms, function(form) {
        var table = form.field('205');
        return tools.checkAll(table.getRows(), function(row) {
          if (wpw.tax.utilities.dateCompare.between(row[0].get(), tools.field('cp.tax_start').get(), tools.field('cp.tax_end').get()) &&
              row[2].get() > 0 &&
              tools.field('200').isZero())
            return tools.requireOne(tools.list(['200']), 'isNonZero');
          else return true;
        });
      });
    }));

  });
})();
