(function() {
  'use strict';

  wpw.tax.global.formData.t2s55 = {
      'formInfo': {
        'abbreviation': 't2s55',
        'title': 'Part III.1 Tax on Excessive Eligible Dividend Designations',
        //TODO: DO NOT DELETE THIS LINE: subtitle
        //'subTitle': ' (2006 and later tax years)',
        'schedule': 'Schedule 55',
        'showCorpInfo': true,
        code: '0605',
        formFooterNum: 'T2 SCH55 E (15)',
        headerImage: 'canada-federal',
        'description': [
          {text: ''},
          {
            'type': 'list',
            'items': [
              {
                label: 'Every corporation resident in Canada that pays a taxable dividend (other than a capital ' +
                'gains dividend within the meaning assigned by subsection 130.1(4) or 131(1)) in the tax year must ' +
                'file this schedule.'
              },
              {
                label: 'Canadian-controlled private corporations (CCPC) and deposit insurance corporations (DIC) ' +
                'must complete Part 1 of this schedule. All other corporations must complete Part 2.'
              },
              {
                label: 'Every corporation that has paid an eligible dividend must also file Schedule 53, ' +
                '<i>General Rate Income Pool (GRIP) Calculation</i>, or Schedule 54, ' +
                '<i>Low Rate Income Pool (LRIP) Calculation</i>, whichever is applicable.'
              },
              {
                label: 'File the completed schedules with your T2 Corporation Income Tax Return no later than six' +
                ' months from the end of the tax year.'
              },
              {
                label: 'All legislative references are to the <i>Income Tax Act</i> and the ' +
                '<i>Income Tax Regulations</i>.'
              },
              {
                label: 'Subsection 89(1) defines the terms eligible dividend, excessive eligible dividend ' +
                'designation, general rate income pool (GRIP), and low rate income pool (LRIP).'
              },
              {
                label: 'The calculations in Part 1 and Part 2 do not apply if the excessive eligible dividend' +
                ' designation arises from the application of paragraph (c) of the definition of excessive eligible' +
                ' dividend designation in subsection 89(1). This paragraph applies when an eligible dividend is paid' +
                ' to artificially maintain or increase the GRIP or to artificially maintain or decrease the LRIP.'
              }
            ]
          }
        ],
        category: 'Federal Tax Forms'
      },
      'sections': [
        {
          'header': 'Part 1 - Canadian-controlled private corporations and deposit insurance corporations',
          'rows': [
            {
              'label': 'Taxable dividends paid in the tax year'
            },
            {
              'label': 'Taxable dividends paid in the tax year <b>not included</b> in Schedule 3',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '098'
                  }
                },
                null
              ]
            },
            {
              'label': 'Taxable dividends paid in the tax year <b>included</b> in Schedule 3',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '099',
                    'disabled': true
                  }
                },
                null
              ]
            },
            {
              'label': 'Total taxable dividends paid in the tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '100',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '100'
                  }
                },
                null
              ]
            },
            {
              'label': 'Total eligible dividends paid in the tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '150',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '150'
                  }
                }
              ],
              'indicator': 'A'
            },
            {
              'label': 'GRIP at the end of the tax year (line 590 on Schedule 53) (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '160',
                    'disabled': true,
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '160'
                  }
                }
              ],
              'indicator': 'B'
            },
            {
              'label': 'Excessive eligible dividend designation (line 150 <b>minus</b> line 160)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '170',
                    'disabled': true
                  }
                }
              ],
              'indicator': 'C'
            },
            {
              'label': 'Deduct:',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Excessive eligible dividend designations elected under subsection 185.1(2) to be treated as ordinary dividends*',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '180',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '180'
                  }
                }
              ],
              'indicator': 'D'
            },
            {
              'label': 'Subtotal (amount C <b>minus</b> amount D)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '185',
                    'disabled': true
                  }
                }
              ],
              'indicator': 'E'
            },
            {
              'label': '<b>Part III.1 tax on excessive eligible dividend designations - CCPC or DIC</b> (amount E<b> multiplied</b> by 20%)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '190',
                    'disabled': true,
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '190'
                  }
                }
              ],
              'indicator': 'F'
            },
            {
              'label': 'Enter the amount from line 190 on line 710 of the T2 return.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 2 - Other corporations',
          'rows': [
            {
              'label': 'Taxable dividends paid in the tax year'
            },
            {
              'label': 'Taxable dividends paid in the tax year <b>not included</b> in Schedule 3',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '198'
                  }
                },
                null
              ]
            },
            {
              'label': 'Taxable dividends paid in the tax year <b>included</b> in Schedule 3',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '199',
                    'disabled': true
                  }
                },
                null
              ]
            },
            {
              'label': 'Total taxable dividends paid in the tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '200',
                    'disabled': true,
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '200'
                  }
                },
                null
              ]
            },
            {
              'label': 'Total excessive eligible dividend designations in the tax year (amount from line A of Schedule 54)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '210',
                    'disabled': true
                  }
                }
              ],
              'indicator': 'G'
            },
            {
              'label': 'Deduct',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Excessive eligible dividend designations elected under subsection 185.1(2) to be treated as ordinary dividends*',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '280',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '280'
                  }
                }
              ],
              'indicator': 'H'
            },
            {
              'label': 'Subtotal (amount G <b>minus</b> amount H)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '285',
                    'disabled': true
                  }
                }
              ],
              'indicator': 'I'
            },
            {
              'label': '<b>Part III.1 tax on excessive eligible dividend designations - Other corporations</b> (amount I<b> multiplied</b> by 20%)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '290',
                    'disabled': true,
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '290'
                  }
                }
              ],
              'indicator': 'J'
            },
            {
              'label': 'Enter the amount from line 290 on line 710 of the T2 return.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'hideFieldset': true,
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* You can elect to treat all or part of your excessive eligible dividend designation as a separate taxable dividend in order to eliminate or reduce the Part III.1 tax otherwise payable. You must file the election on or before the day that is 90 days <b>after</b> the day the notice of assessment for Part III.1 tax was sent. We will accept an election before the assessment of the tax. For more information on how to make this election, go to <b>www.cra.gc.ca/eligibledividends</b>.',
              'labelClass': 'fullLength'
            }
          ]
        }
      ]
    };
})();
