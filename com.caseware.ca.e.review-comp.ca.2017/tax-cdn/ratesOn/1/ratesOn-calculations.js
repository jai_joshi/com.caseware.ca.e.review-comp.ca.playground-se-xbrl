(function() {
  wpw.tax.create.calcBlocks('ratesOn', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      field('316').assign(2);
      field('317').assign(1.5);
      field('318').assign(250000);
      field('492').assign(4.5);
      field('4921').assign(3.5);
      field('493').assign(1 / 4);
      field('494').assign(23.56);
      field('600').assign(14);
      field('601').assign(12);
      field('602').assign(11.5);
      field('603').assign(8.5);
      field('604').assign(7.5);
      field('605').assign(7);
      field('6051').assign(8);
      field('606').assign(4.25);
      field('607').assign(500000);
      field('610').assign(50);
      field('611').assign(50);
      field('612').assign(14);
      field('613').assign(14);
      field('614').assign(14);
      field('615').assign(10000);
      field('616').assign(1000);
      field('620').assign(4);
      field('621').assign(5000000);
      field('622').assign(10000000);
      // field('623').assign(2.7);
      field('624').assign(50000000);
      field('625').assign(100000000);
      field('626').assign(3);
      field('630').assign(1500000);
      field('631').assign(40000000);
      field('632').assign(1 / 3);
      field('635').assign(0.0045);
      field('636').assign(0.00675);
      field('637').assign(0.0054);
      field('638').assign(0.003);
      field('639').assign(0.0045);
      field('640').assign(0.0036);
      field('641').assign(0);
      field('642').assign(0);
      field('643').assign(0);
      field('650').assign(15000000);
      field('651').assign(0.00225);
      field('652').assign(0.00150);
      field('653').assign(0);
      field('654').assign(20);
      field('655').assign(50);
      field('656').assign(30);
      field('660').assign(18600);
      field('670').assign(15);
      field('671').assign(10);
      field('672').assign(5);
      field('673').assign(400000);
      field('674').assign(600000);
      field('675').assign(200000);
      field('676').assign(1000);
      field('677').assign(30);
      field('678').assign(25);
      field('679').assign(5);
      field('680').assign(400000);
      field('681').assign(600000);
      field('682').assign(200000);
      field('683').assign(3000);
      field('690').assign(35);
      field('691').assign(25);
      field('692').assign(10);
      field('693').assign(400000);
      field('694').assign(600000);
      field('695').assign(200000);
      field('696').assign(5000);
      field('697').assign(25);
      field('698').assign(35);
      field('699').assign(5);
      field('700').assign(400000);
      field('701').assign(600000);
      field('702').assign(200000);
      field('703').assign(10000);
      field('710').assign(50);
      field('711').assign(20);
      field('720').assign(240000);
      field('721').assign(30);
      field('722').assign(40);
      field('723').assign(35);
      field('724').assign(40);
      field('725').assign(10);
      field('726').assign(50000);
      field('727').assign(20000);
      field('728').assign(15000);
      field('729').assign(30);
      field('730').assign(35);
      field('731').assign(10);
      field('740').assign(4);
      field('741').assign(25);
      field('742').assign(21.5);
      field('750').assign(10000000);
      field('751').assign(20000000);
      field('752').assign(50);
      field('753').assign(100000);
      field('754').assign(20);
      field('755').assign(25);
      field('756').assign(35);
      field('757').assign(20);
      field('758').assign(30);
      field('759').assign(40);
      field('760').assign(20);
      field('761').assign(25);
      field('762').assign(40);
      field('763').assign(35);
      field('764').assign(35);
      field('770').assign(50);
      field('771').assign(20);
      field('780').assign(50);
      field('781').assign(50);
      field('782').assign(30);
      field('783').assign(30000);
      field('790').assign(40);
      field('791').assign(25);
      field('792').assign(0.4);
      field('793').assign(3000000);
      field('794').assign(10);
      field('795').assign(2500000);
      field('796').assign(10);
      field('801').assign(8);
      field('802').assign(1.25);
      field('797').assign(7000000);
      field('798').assign(400000);
      field('799').assign(8000000);
      field('800').assign(500000);
      field('805').assign(20);
      field('806').assign(20000000);
      field('807').assign(0.5);
      field('808').assign(50000000);
      field('809').assign(10000000);
      field('810').assign(0.25);
      field('811').assign(100000000);
      field('812').assign(50000000);
      field('813').assign(0.5);
      field('814').assign(300000000);
      field('815').assign(200000000);
      field('816').assign(0.75);
      field('817').assign(300000000);
      //T2s552 Part 3- 2015 year
      field('901').assign(45);
      field('902').assign(35);
      field('903').assign(10);
      field('904').assign(400000);
      field('905').assign(600000);
      field('906').assign(200000);

      field('908').assign(30);
      field('909').assign(25);
      field('910').assign(5);
      field('911').assign(400000);
      field('912').assign(600000);
      field('913').assign(200000);
    });
  });
})();
