(function() {
  wpw.tax.global.tableCalculations.ratesOn = {
    "1000": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [{
        colClass: 'std-input-width',
        "type": "none",
        cellClass: 'alignCenter'
      },
        {
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true
        }],
      "cells": [{
        "0": {
          "label": "1"
        },
        "2": {
          "num": "807",
          decimals: 1
        },
        "4": {
          "num": "808"
        },
        "6": {
          "num": "809"
        }
      },
        {
          "0": {
            "label": "2"
          },
          "2": {
            "num": "810",
           decimals: 2
          },
          "4": {
            "num": '811'
          },
          "6": {
            "num": "812"
          }
        },
        {
          "0": {
            "label": "3"
          },
          "2": {
            "num": "813",
            decimals: 1
          },
          "4": {
            "num": "814"
          },
          "6": {
            "num": "815"
          }
        },
        {
          "0": {
            "label": "4"
          },
          "2": {
            "num": "816",
            decimals: 2
          },
          "4": {
            "num": "817"
          },
          "6": {
            "num": "818",
            disabled: true
          }
        }
      ]
    }
  }
})();
