(function() {
  wpw.tax.create.formData('ratesOn', {
    formInfo: {
      abbreviation: 'ratesOn',
      title: 'Ontario Table of Rates and Values',
      showCorpInfo: true,
      category: 'Rates Tables'
    },
    sections: [
      {
        header: 'Schedule 500 - Ontario corporation tax calculation',
        'rows': [
          {
            'label': 'Basic income tax',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year',
            'input2Header': 'Rate'
          },
          {
            'label': '2008-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '600'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2010-07-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '601'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2011-07-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '602',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario small business deduction',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year',
            'input2Header': 'Rate'
          },
          {
            'label': '2008-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '603',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2010-07-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '604',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2011-07-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '605'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2018-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '6051'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Surtax',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year',
            'input2Header': 'Rate'
          },
          {
            'label': '2010-07-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '606',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario business limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '607'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 502 - Ontario Tax Credit for Manufacturing and Processing ',
        'rows': [
          {
            'label': 'Profits tax credit or reduction',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '2008-01-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '316'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '2011-07-01',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '317',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Adjusted business income threshold - Small manufacturing corporation',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '318'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 506 - Ontario transitional tax debits and credits',
        'rows': [
          {
            'label': 'Net capital losses rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '610'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate of reserves deducted under subparagraphs 40(1)(a)(iii) or 44(1)(e)(iii) of the <i>Income Tax Act</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '611'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Post-2008 SR&ED balance rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '612'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate of cumulative post-2008 SR&ED limit at the end of the year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '613'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate of federal SR&ED transitional balance at the end of the year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '614'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Maximum transitional balance at the end of the year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '615'
                }
              }
            ]
          },
          {
            'label': 'Maximum amount',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '616'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 508 - Ontario Research and Development Tax Credit ',
        'rows': [
          {
            'label': 'Tax credit rate'
          },
          {
            'label': 'before June 1, 2016',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '492',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'after May 31, 2016',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '4921',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Factor',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '493',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Rate for the total recapture',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '494',
                  'decimals': 2
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 510 - Ontario corporate minimum tax',
        'rows': [
          {
            'label': 'Rates and limits',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year-ending',
            'input1Header': 'Total assets limit',
            'input2Header': 'Total revenue limit'
          },
          {
            'label': 'Before 2010-07-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '621'
                }
              },
              {
                'input': {
                  'num': '622'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'After 2010-06-30',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '624'
                }
              },
              {
                'input': {
                  'num': '625'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Federal Part VI.1 tax factor',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '626'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 512 - Ontario Special Additional Tax on Life Insurance Corporations (SAT)',
        'rows': [
          {
            'label': 'Part 4 - Capital allowance'
          },
          {
            'label': 'Additions'
          },
          {
            'type': 'table',
            'num': '1000'
          }
        ]
      },
      {
        'header': 'Schedule 514 - Ontario capital tax on financial institution',
        'rows': [
          {
            'label': 'Capital deduction',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '630'
                }
              }
            ]
          },
          {
            'label': 'Basic capital amount',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '631'
                }
              }
            ]
          },
          {
            'label': 'Multiplication factor for Canadian tangible property',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '632',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Rate',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year',
            'input1Header': 'Additional capital tax rate for a corporation that is a deposit-taking institution',
            'input2Header': 'Additional capital tax rate for a corporation that is not a deposit-taking institution'
          },
          {
            'label': '2006-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '636',
                  'decimals': 5
                }
              },
              {
                'input': {
                  'num': '637',
                  'decimals': 5
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2010-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '639',
                  'decimals': 4
                }
              },
              {
                'input': {
                  'num': '640',
                  'decimals': 4
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2010-07-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '642'
                }
              },
              {
                'input': {
                  'num': '643'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 515 - Ontario capital tax on other than financial institutions',
        'rows': [
          {
            'label': 'Capital deduction',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '650'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Rate',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year',
            'input2Header': 'Capital tax rate'
          },
          {
            'label': '2006-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '651',
                  'decimals': 5
                }
              }
            ]
          },
          {
            'label': '2010-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '652',
                  'decimals': 5
                }
              }
            ]
          },
          {
            'label': '2010-07-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '653'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Capital tax credit for manufacturers',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Minimum proportion of the Ontario manufacturing labour cost on the total Ontario labour cost',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '654'
                }
              }
            ]
          },
          {
            'label': 'Maximum proportion of the Ontario manufacturing labour cost on the total Ontario labour cost',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '655'
                }
              }
            ]
          },
          {
            'label': 'Credit\'s denominator rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '656'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 525 - Ontario political contributions tax credit',
        'rows': [
          {
            'label': 'Annual limit $18,600 for 2009 to 2013 calendar years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '660'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 550 - Ontario co-operative education tax credit (CETC)',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Qualifying work placements before March 27, 2009',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Eligible percentage $400,000 or less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '670'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Eligible percentage $600,000 or greater',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '671'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Eligible percentage - Percentage difference',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '672'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Salaries and wages paid - Lower limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '673'
                }
              }
            ]
          },
          {
            'label': 'Salaries and wages paid - Higher limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '674'
                }
              }
            ]
          },
          {
            'label': 'Salaries and wages paid - Limit difference',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '675'
                }
              }
            ]
          },
          {
            'label': 'Eligible expenditures limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '676'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Qualifying work placements after March 26, 2009',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Eligible percentage $400,000 or less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '677'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Eligible percentage $600,000 or greater',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '678'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Eligible percentage - Percentage difference',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '679'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Salaries and wages paid - Lower limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '680'
                }
              }
            ]
          },
          {
            'label': 'Salaries and wages paid - Higher limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '681'
                }
              }
            ]
          },
          {
            'label': 'Salaries and wages paid - Limit difference',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '682'
                }
              }
            ]
          },
          {
            'label': 'Eligible expenditures limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '683'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 552 - Ontario apprenticeship training tax credit (ATTC)',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Qualifying apprenticeship after March 26, 2009 for an apprenticeship program that began before April 24, 2015',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Eligible percentage, line 300 $400,000 or less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '901'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Eligible percentage, line 300 $600,000 or greater',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '902'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Eligible percentage - Percentage difference',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '903'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Salaries and wages paid - Lower limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '904'
                }
              }
            ]
          },
          {
            'label': 'Salaries and wages paid - Higher limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '905'
                }
              }
            ]
          },
          {
            'label': 'Salaries and wages paid - Limit difference',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '906'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Qualifying apprenticeship after April 23, 2015',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Eligible percentage, line 300 $400,000 or less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '908'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Eligible percentage, line 300 $600,000 or greater',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '909'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Eligible percentage - Percentage difference',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '910'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Salaries and wages paid - Lower limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '911'
                }
              }
            ]
          },
          {
            'label': 'Salaries and wages paid - Higher limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '912'
                }
              }
            ]
          },
          {
            'label': 'Salaries and wages paid - Limit difference',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '913'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Qualifying apprenticeship before March 27, 2009',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Eligible percentage, line 300 $400,000 or less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '690'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Eligible percentage, line 300 $600,000 or greater',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '691'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Eligible percentage - Percentage difference',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '692'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Salaries and wages paid - Lower limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '693'
                }
              }
            ]
          },
          {
            'label': 'Salaries and wages paid - Higher limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '694'
                }
              }
            ]
          },
          {
            'label': 'Salaries and wages paid - Limit difference',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '695'
                }
              }
            ]
          },
          {
            'label': 'Limit before March 27, 2009',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '696'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Qualifying apprenticeship after March 26, 2009',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Eligible percentage, line 300 $400,000 or less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '697'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Eligible percentage, line 300 $600,000 or greater',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '698'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Eligible percentage - Percentage difference',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '699'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Salaries and wages paid - Lower limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '700'
                }
              }
            ]
          },
          {
            'label': 'Salaries and wages paid - Higher limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '701'
                }
              }
            ]
          },
          {
            'label': 'Salaries and wages paid - Limit difference',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '702'
                }
              }
            ]
          },
          {
            'label': 'Limit after March 26, 2009',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '703'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 554 - Ontario computer animation and special effects tax credit',
        'rows': [
          {
            'label': 'Qualifying remuneration rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '710'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Tax credit rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '711'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 556 - Ontario film and television tax credit',
        'rows': [
          {
            'label': 'First-time production',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Basic amount',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '720'
                }
              }
            ]
          },
          {
            'label': 'Pre-2008 qualifying labour expenditure rate 1',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '721'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Pre-2008 qualifying labour expenditure rate 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '722'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Post-2007 qualifying labour expenditure rate 1',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '723'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Post-2007 qualifying labour expenditure rate 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '724'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Additional regional Ontario production credit rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '725'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Small first-time production',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Maximum qualifying labour expenditure amount',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '726'
                }
              }
            ]
          },
          {
            'label': 'Maximum eligible credit for regional Ontario production',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '727'
                }
              }
            ]
          },
          {
            'label': 'Maximum eligible credit for other productions',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '728'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Other productions',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Pre-2008 qualifying labour expenditure rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '729'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Post-2007 qualifying labour expenditure rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '730'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Additional regional Ontario production credit rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '731'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 558 - Ontario production services tax credit',
        'rows': [
          {
            'label': 'Rate for Part 9 Amount 4',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '740'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for QPE for the tax year incurred before April 24, 2015',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '741'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for QPE for the tax year incurred after April 23, 2015',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '742',
                  'decimals': '1'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 560 - Ontario interactive digital media tax credit',
        'rows': [
          {
            'label': 'Total assets limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '750'
                }
              }
            ]
          },
          {
            'label': 'Total revenue limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '751'
                }
              }
            ]
          },
          {
            'label': 'Applicable rate for qualifying remuneration amount',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '752'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Maximum eligible marketing and distribution expenditures',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '753'
                }
              }
            ]
          },
          {
            'label': 'Rate for a qualifying corporation (including a QSC) - Specified product - Expenditures incurred after March 23, 2006 and before March 26, 2008',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '754'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for a qualifying corporation (including a QSC) - Specified product - Expenditures incurred after March 25, 2008 and before March 27, 2009',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '755'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for a qualifying corporation (including a QSC) - Specified product - Expenditures incurred after March 26, 2009',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '756'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for a qualifying corporation (for a QSC) - Non-specified product - Expenditures incurred before March 24, 2006',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '757'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for a qualifying corporation (for a QSC) - Non-specified product - Expenditures incurred after March 23, 2006 and before March 27, 2009',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '758'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for a qualifying corporation (for a QSC) - Non-specified product - Expenditures incurred after March 26, 2009',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '759'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for a qualifying corporation (other than a QSC) - Non-specified product - Expenditures incurred after March 23, 2006 and before March 26, 2008',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '760'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for a qualifying corporation (other than a QSC) - Non-specified product - Expenditures incurred after March 25, 2008 and before March 27, 2009',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '761'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for a qualifying corporation (other than a QSC) - Non-specified product - Expenditures incurred after March 26, 2009',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '762'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for a qualifying digital game corporation - Expenditures incurred after March 26, 2009',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '763'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate for a specialized digital game corporation - Expenditures incurred after March 26, 2009',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '764'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 562 - Ontario sound recording tax credit (OSRTC)',
        'rows': [
          {
            'label': 'Rate of qualified expenditures incurred outside Ontario',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '770'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Rate of qualified expenditures eligible for the OSRTC',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '771'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 564 - Ontario book publishing tax credit (OBPTC)',
        'rows': [
          {
            'label': 'Qualifying expenditures',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Promotional cost rate (such as binding, printing and assembly expenditures)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '780'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Promotional cost rate (such as advertising, sales salaries and promotional tours)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '781'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Tax credit',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'OBPTC qualifying expenditure rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '782'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Maximum credit available',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '783'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 566 - Ontario innovation tax credit',
        'rows': [
          {
            'label': 'Qualified expenditures of a capital nature rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '790'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Eligible repayments rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '791'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Designated repayments ratio',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '792',
                  'decimals': 1
                }
              }
            ]
          },
          {
            'label': 'Maximum amount that can be claimed',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '793'
                }
              }
            ]
          },
          {
            'label': 'Multiplier',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '794'
                }
              }
            ]
          },
          {
            'label': 'Specified capital amount',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '795'
                }
              }
            ]
          },
          {
            'label': 'Tax credit rate before June 1, 2016',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '796'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Tax credit rate after May 31, 2016',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '801'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'num 929',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '802',
                  'decimals': '2'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Expenditure limit',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Taxation year',
            'input1Header': 'First limit',
            'input2Header': 'Second limit'
          },
          {
            'label': '2009-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '797'
                }
              },
              {
                'input': {
                  'num': '798'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '2010-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '799'
                }
              },
              {
                'input': {
                  'num': '800'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 568 - Ontario business-research institute tax credit',
        'rows': [
          {
            'label': 'Ontario business-research institute tax credit rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '805'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Eligible expenditure limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '806'
                }
              }
            ]
          }
        ]
      }
    ]
})
})();
