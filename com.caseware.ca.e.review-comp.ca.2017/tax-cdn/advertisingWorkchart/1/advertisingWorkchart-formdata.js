(function() {
  'use strict';
  wpw.tax.global.formData.advertisingWorkchart = {
    formInfo: {
      abbreviation: 'advertisingWorkchart',
      title: 'Foreign Advertising Expenses Workchart - Detailed',
      headerImage: 'cw',
      dynamicFormWidth: true,
      isRepeatForm: true,
      repeatFormData: {
        linkedRepeatTable: 'advertisingWorkchartS.1000',
        titleNum: '101'
      },
      category: 'Workcharts'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          // {
          //   'label': 'Update this form from:',
          //   'num': '102',
          //   'type': 'infoField',
          //   'inputType': 'dropdown',
          //   'init': '1',
          //   'showValues': 'before',
          //   'options': [
          //     {
          //       'value': '1',
          //       'option': 'No update, enter Data Directly'
          //     },
          //     {
          //       'value': '2',
          //       'option': 'Update from GL in Simple Engagement (the Default if there is SE with a GL)'
          //     },
          //     {
          //       'value': '3',
          //       'option': 'From GL in linked CW WP file through TaxSync'
          //     },
          //     {
          //       'value': '4',
          //       'option': 'From a CSV file (from Excel; Google Sheets; QuickBooks; Sage (Simply Accounting) '
          //     }
          //   ]
          // },
          // {
          //   'labelClass': 'fullLength'
          // },
          {
            'type': 'infoField',
            'label': 'GL A/C # (if applicable)',
            inputClass: 'std-input-col-width-3',
            'num': '100'
          },
          {
            'type': 'infoField',
            'label': 'Description of GL A/C',
            inputClass: 'std-input-col-width-3',
            'num': '101'
          },
          {
            'label': 'Type of Foreign Advertising Expenses in this Workchart:',
            'num': '103',
            inputClass: 'std-input-col-width-3',
            'type': 'infoField',
            'inputType': 'dropdown',
            'init': '3',
            'showValues': 'before',
            'options': [
              {
                'option': 'Not deductible for tax purposes',
                'value': '1'
              },
              {
                'option': 'Deductible for tax purposes',
                'value': '2'
              },
              {
                'option': 'This form has a mixture of expenses',
                'value': '3'
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '200'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'SUMMARY:',
            'labelClass': 'bold text-right'
          },
          {
            'label': 'Not deductible for tax purposes',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': false
            },
            'columns': [
              {
                'input': {
                  'num': '300',
                  'showWhen': {
                    'or': [
                      {
                        'field': '103',
                        'compare': {
                          'is': 1
                        }
                      },
                      {
                        'field': '103',
                        'compare': {
                          'is': 3
                        }
                      }
                    ]
                  }
                }
              }
            ]
          },
          {
            'label': 'Deductible for tax purposes',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': false
            },
            'columns': [
              {
                'input': {
                  'num': '301',
                  'showWhen': {
                    'or': [
                      {
                        'fieldId': '103',
                        'compare': {
                          'is': 2
                        }
                      },
                      {
                        'fieldId': '103',
                        'compare': {
                          'is': 3
                        }
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      }
    ]
  };
})();
