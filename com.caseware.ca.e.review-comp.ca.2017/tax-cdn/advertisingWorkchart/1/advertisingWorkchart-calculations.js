(function() {

  wpw.tax.create.calcBlocks('advertisingWorkchart', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field) {
      var selectedType = field('103').get();
      var multiType = selectedType == '3';

      var data = [];
      var table = field('200');
      // use getCol instead of getRows to reduce number of cells that need to be converted into Field objects
      table.getCol(3).forEach(function(cell, rowIndex) {
        var expenseType;
        if (multiType) {
          expenseType = cell.get();
          cell.disabled(false);
        } else {
          expenseType = selectedType;
          cell.assign(expenseType);
        }
        var expenseAmount = table.cell(rowIndex, 4).get();
        data.push({'type': expenseType, 'amount': expenseAmount});
        field('300').assign(calcUtils.sumByKey(data, 'type', '1', 'amount'));
        field('301').assign(calcUtils.sumByKey(data, 'type', '2', 'amount'));
      });
    });
  });
})();
