wpw.tax.create.importCalcs('advertisingWorkchart', function(tools) {

  tools.intercept('HSADV.SLIPB.TtaadvB2', function(importObj) {
    if (importObj.value > 0) {
      tools.form('advertisingWorkchart').setValue('200', 2, 3, importObj.rowIndex);
    }
  });
});
