(function() {
  wpw.tax.global.tableCalculations.advertisingWorkchart = {
    "200": {
      hasTotals: true,
      "columns": [
        {
          "header": "Transaction #",
          colClass: 'std-input-col-padding-width ',
          "canSort": true,
          "num": "201"
        },
        {
          "header": "Date of Transaction",
          colClass: 'std-input-col-width',
          "type": "date",
          "canSort": true,
          "num": "202"
        },
        {
          "header": "Description of Foreign Advertising Expenses",
          "canSort": true,
          "num": "203",
          type: 'text'
        },
        {
          "header": "Type",
          "type": "dropdown",
          colClass: 'std-input-col-padding-width ',
          "showValues": "before",
          "init": "1",
          "canSort": true,
          "options": [
            {
              "option": "Not deductible for tax purposes",
              "value": "1"
            },
            {
              "option": "Deductible for tax purposes",
              "value": "2"
            }]
        },
        {
          "header": "Amount",
          colClass: 'std-input-col-width',
          filters: 'prepend $',
          "total": true,
          "canSort": true,
          "init": "0",
          "totalNum": "206",
          "num": "205"
        }]
    }
  }
})();
