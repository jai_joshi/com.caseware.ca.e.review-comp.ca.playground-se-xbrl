(function() {

  wpw.tax.create.calcBlocks('t2s569', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //Part 3 calcs
      calcUtils.equals('301', '300');
      calcUtils.equals('302', '150');
      if (field('135').get() == 1) {
        if (field('140').get() == 1) {
          field('303').assign(undefined);
        }
        else if (field('140').get() == 2) {
          field('303').assign((field('301').get() * field('302').get() * (1 / 100)));
        }
      }
      else if (field('135').get() == 2) {
        field('303').assign(undefined);
      }
      if (field('300').get() && field('303').get()) {
        field('306').assign(field('303').get());
      }
      else if (field('300').get() && !field('303').get()) {
        field('306').assign(field('300').get());
      }
      else {
        field('306').assign(undefined);
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.equals('307', '305');
      calcUtils.getGlobalValue('308', 'CP', 'Days_Fiscal_Period');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      if (field('200').get() == 2 ||
          field('205').get() == 2 ||
          field('210').get() == 2 ||
          field('215').get() == 2 ||
          field('220').get() == 2 ||
          field('225').get() == 2 ||
          field('230').get() == 2) {
        field('310').assign(undefined);
      }
      else {
        field('310').assign((field('306').get() / field('308').get()) * field('307').get());
      }
    });


  });
})();
