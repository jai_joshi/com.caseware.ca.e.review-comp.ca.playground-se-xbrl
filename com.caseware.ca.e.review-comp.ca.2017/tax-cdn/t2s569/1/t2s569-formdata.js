(function() {
  'use strict';

  wpw.tax.global.formData.t2s569 = {
      'formInfo': {
        'abbreviation': 'T2S569',
        'title': 'Ontario Business-Research Institute Tax Credit Contract information',
        //TODO: DO NOT DELETE THIS LINE: subtitle
        'schedule': 'Schedule 569',
        isRepeatForm: true,
        'showCorpInfo': true,
        code: 'Code 0902',
        formFooterNum: 'T2 SCH 569 E (10)',
        headerImage: 'canada-federal',
        'description': [
          {text: ''},
          {
            'type': 'list',
            'items': [
              {
                label: ' Use this schedule to support your claim for the Ontario business-research institute ' +
                'tax credit (OBRITC), which is made on Schedule 568,<i> Ontario Business-Research Institute ' +
                'Tax Credit</i>. Complete a separate Schedule 569 for each eligible contract.'
              },
              {
                label: 'The OBRITC is a 20% refundable tax credit based on qualified expenditures ' +
                'incurred in Ontario under an eligible contract with an eligible research institute ' +
                '(ERI). An ERI, for purposes of the OBRITC, is defined in subsection 97(27) of ' +
                'the<i> Taxation Act, 2007 </i>(Ontario).'
              },
              {
                label: 'A list of eligible research institutes and the applicable ERI codes for ' +
                'eligible contracts can be found on our web site. Go to <b>www.cra.gc.ca/ctao</b> and select ' +
                '"business-research institute tax credit".'
              },
              {
                label: 'The eligibility requirements in Part 2 of this schedule must be met ' +
                'for the qualifying corporation to claim an OBRITC for this contract.'
              },
              {
                label: ' Eligible contracts entered into before August 10, 2007 were subject to ' +
                'advanced ruling legislation. OBRITC claims relating to one of these contracts must ' +
                'have the corresponding Ontario Ministry of Revenue ruling reference number entered ' +
                'at line 130 in Part 1 of this schedule.'
              },
              {
                label: 'Corporations can only claim the OBRITC for the number of days in the tax year ' +
                'that the corporation<b> was not</b> connected to the ERI. Connected corporations, ' +
                'for the purposes of the OBRITC, are defined in subsection 97(4) of the' +
                '<i> Taxation Act, 2007</i> (Ontario).'
              },
              {
                label: 'Eligible contracts and qualified expenditures are defined in subsections ' +
                '97(6) and 97(8), respectively, of the<i> Taxation Act, 2007 </i>(Ontario).'
              },
              {
                label: 'According to subsections 97(16) and (19) of the <i>Taxation Act</i>, 2007 ' +
                '(Ontario), qualified expenditures must be reduced by contributions the corporation ' +
                'received, is entitled to receive or may reasonably expect to receive. Qualified ' +
                'expenditures include repayment of government assistance made by the corporation ' +
                'during the year. Contribution and government assistance are defined in ' +
                'subsection 97(27) of the<i> Taxation Act, 2007</i> (Ontario).'
              }
            ]
          }
        ],
        category: 'Ontario Forms'
      },
      'sections': [
        {
        header: 'Part 1 - Contract details',
          rows: [
            {
              'type': 'table',
              'num': '1000'
            },
            {
              'type': 'table',
              'num': '1050'
            },
            {
              'type': 'table',
              'num': '1100'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If the date on line 120 is before August 10, 2007, was the contract subject to an advanced ruling?',
              'type': 'infoField',
              'inputType': 'radio',
              'num': '125',
              'rf': true,
              'tn': '125',
              'canClear': true,
              'labelClass': 'fullLength',
              'indicatorColumn': true
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'For all contracts entered into before August 10, 2007, enter the Ontario Ministry of Revenue ruling reference number',
              'type': 'infoField',
              'indicatorColumn': true,
              'num': '130',
              'validate': {
                'or': [
                  {
                    'length': {
                      'min': '1',
                      'max': '6'
                    }
                  },
                  {
                    'check': 'isEmpty'
                  }
                ]
              },
              'rf': true,
              'tn': '130',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Is the claim filed for an OBRITC earned through a partnership?*',
              'type': 'infoField',
              'inputType': 'radio',
              'num': '135',
              'rf': true,
              'tn': '135',
              'canClear': true,
              'labelClass': 'fullLength',
              'labelWidth': '80%',
              'indicatorColumn': true
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If the answer on line 135 is<b> yes</b>, are you a specified member?',
              'type': 'infoField',
              'inputType': 'radio',
              'num': '140',
              'rf': true,
              'canClear': true,
              'tn': '140',
              'labelClass': 'fullLength',
              'indicatorColumn': true
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If the answer on line 135 is <b>yes</b>, what is the name of the partnership?',
              'type': 'infoField',
              'num': '145',
              'validate': {
                'or': [
                  {
                    'length': {
                      'min': '1',
                      'max': '175'
                    }
                  },
                  {
                    'check': 'isEmpty'
                  }
                ]
              },
              'rf': true,
              'tn': '145',
              'labelClass': 'fullLength',
            'indicatorColumn': true
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Enter the corporation\'s percentage share of the income or loss of the partnership\'s fiscal period ending in the corporation\'s tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              indicator: '%',
              'columns': [
                {
                  'input': {
                    'num': '150',
                    'decimals': 3,
                    'validate': {
                      'and': [
                        'percent',
                        {
                          'or': [
                            {
                              'length': {
                                'min': '1',
                                'max': '6'
                              }
                            },
                            {
                              'check': 'isEmpty'
                            }
                          ]
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '150'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* When a corporate member of a partnership is claiming an amount for qualified expenditures incurred during the tax year under the eligible contract by the partnership, complete Schedule 569 as if the partnership were a corporation. Each corporate member, other than a specified member, should file a Schedule 569 as if it, instead of the partnership, had entered into the contract with the ERI and can claim the corporation\'s share of the partnership\'s qualified expenditures. Specified members of a partnership cannot claim an OBRITC. A definition of "specified member" can be found in subsection 248(1) of the federal<i>Income Tax Act</i>',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
        header: 'Part 2 - Eligibility',
          rows: [
            {
              label: 'Contract:',
              labelClass: 'bold'
            },
            {
              label: '1. Did the corporation enter into a contract with an ERI?',
              type: 'infoField',
              inputType: 'radio',
              num: '200', rf: true,
              tn: '200',
              init: '2',
              labelClass: 'fullLength',
              labelWidth: '80%'
            },
            {
              label: '2. Do the terms of the contract state that the ERI agrees to perform, in ' +
              'Ontario, scientific research and experimental development (SR&ED) related to the ' +
              'business carried on in Canada by the corporation?',
              type: 'infoField',
              inputType: 'radio',
              num: '205', rf: true,
              tn: '205',
              init: '2',
              labelClass: 'fullLength',
              labelWidth: '80%'
            },
            {
              label: '3. Was the corporation entitled to exploit the results of the SR&ED ' +
              'carried out under the contract?',
              type: 'infoField',
              inputType: 'radio',
              num: '210', rf: true,
              tn: '210',
              init: '2',
              labelClass: 'fullLength',
              labelWidth: '80%'
            },
            {labelClass: 'fullLength'},
            {
              label: 'If you answered no to question 1, 2, or 3, the contract is<b> not</b> an <b>eligible</b> ' +
              'contract for the purposes of an OBRITC.',
              labelClass: 'fullLength'
            },
            {labelClass: 'fullLength'},
            {
              label: 'Expenditures:',
              labelClass: 'bold'
            },
            {
              label: '4. Were the expenditures made by a payment of money by the corporation to the ERI' +
              ' or by a prescribed payment?',
              type: 'infoField',
              inputType: 'radio',
              num: '215', rf: true,
              tn: '215',
              init: '2',
              labelClass: 'fullLength',
              labelWidth: '80%'
            },
            {
              label: '5. Were the expenditures incurred in respect of SR&ED carried on in Ontario by the ERI?',
              type: 'infoField',
              inputType: 'radio',
              num: '220', rf: true,
              tn: '220',
              init: '2',
              labelClass: 'fullLength',
              labelWidth: '80%'
            },
            {
              label: '6. Are the expenditures identified in subparagraph 37(1)(a)(i), (i.1) or (ii) ' +
              'of the federal<i>Income Tax Act</i>and would they also qualify as qualified expenditures, ' +
              'as defined in subsection 127(9) of the <i>federal Act</i>, other than prescribed types of ' +
              'expenditures and certain salaries or wages?',
              type: 'infoField',
              inputType: 'radio',
              num: '225', rf: true,
              tn: '225',
              init: '2',
              labelClass: 'fullLength',
              labelWidth: '80%'
            },
            {
              label: '7. Were the expenditures incurred by the corporation for purposes of SR&ED related ' +
              'to the business carried on in Canada by the corporation?',
              type: 'infoField',
              inputType: 'radio',
              num: '230', rf: true,
              tn: '230',
              init: '2',
              labelClass: 'fullLength',
              labelWidth: '80%'
            },
            {labelClass: 'fullLength'},
            {
              label: 'If you answered no to question 4, 5, 6, or 7, the expenditures are<b> not eligible </b> ' +
              'expenditures for the purposes of an OBRITC.',
              labelClass: 'fullLength'
            }
          ]
        },
        {
        header: 'Part 3 - Qualified expenditures for this contract for the tax year',
          rows: [
            {
              'label': 'Qualified expenditures incurred in the tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '300',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '300'
                  }
                }
              ]
            },
            {
              'label': 'If the corporation answered<b> yes</b> at line 135 in Part 1, and<b> no </b>at line 140 in Part 1, determine the partnership\'s share of qualified expenditures available to claim in the tax year:'
            },
            {
              'type': 'table',
              'num': '1140'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Number of days in this tax year that the corporation was<b> not</b> connected to the ERI identified on line 110 in Part 1',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '305',
                    'validate': {
                      'or': [
                        {
                          'matches': '^-?[.\\d]{1,3}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '305'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Qualified expenditures for this contract for the tax year:',
              'labelClass': 'bold'
            },
            {
              'type': 'table',
              'num': '1150'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Enter amount B on line 405 of<b> Schedule 568</b>,<i> Ontario Business-Research Institute Tax Credit.</i>'
            }
          ]
        }
      ]
    };
})();
