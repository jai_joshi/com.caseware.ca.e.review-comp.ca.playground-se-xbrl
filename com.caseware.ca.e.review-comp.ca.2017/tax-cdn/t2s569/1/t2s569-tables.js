(function() {
  var eriCodes = wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.eriCodes, 'value');

  wpw.tax.global.tableCalculations.t2s569 = {
    "1000": {
      "infoTable": true,
      "fixedRows": true,
      "dividers": [1],
      "columns": [{
        "width": "60%",
        cellClass: 'alignCenter'
      },
        {
          cellClass: 'alignCenter'
        }],
      "cells": [{
        "0": {
          "label": "Name of person to contact for more information",
          "num": "100", "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
          "rf": true,
          "tn": "100",
          "labelClass": "bold",
          type: 'text'
        },
        "1": {
          "label": "Telephone number including area code",
          "num": "105",
          "rf": true,
          "tn": "105",
          "labelClass": "bold",
          'telephoneNumber': true,
          type: 'custom',
          format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
          validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
        }
      }]
    },
    "1050": {
      "infoTable": true,
      "fixedRows": true,
      "columns": [{}],
      "cells": [{
        "0": {
          "label": "Name of the ERI on the contract",
          "num": "110", "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]},
          "rf": true,
          "tn": "110",
          "labelClass": "bold",
          type: 'text'
        }
      }]
    },
    "1100": {
      "infoTable": true,
      "fixedRows": true,
      "dividers": [1],
      "columns": [
        {
          cellClass: 'alignCenter'
        },
        {
          "type": "date",
          cellClass: 'alignCenter'
        }
      ],
      "cells": [{
        "0": {
          "num": "115", "validate": {"or": [{"matches": "^-?[.\\d]{3,3}$"}, {"check": "isEmpty"}]},
          "rf": true,
          "tn": "115",
          "label": "ERI code",
          "labelClass": "bold",
          type: 'dropdown',
          options: eriCodes
        },
        "1": {
          "num": "120",
          "rf": true,
          "tn": "120",
          "label": "Date of contract",
          "labelClass": "bold"
        }
      }]
    },
    "1140": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          colClass: 'std-input-width',
          "type": "none",
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Line 300"
          },
          "1": {
            "num": "301"
          },
          "2": {
            "label": " x percentage on line 150 in Part 1 "
          },
          "3": {
            "num": "302",
            decimals: 3
          },
          "4": {
            "label": "%="
          },
          "5": {
            "num": "303",
            cellClass: 'doubleUnderline'
          },
          "6": {
            "label": "A"
          }
        }
      ]
    },
    "1150": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        }],
      "cells": [
        {
          "0": {
            "label": "(Line 300 or amount A, whichever applies) x line 305",
            cellClass: 'singleUnderline'
          },
          "1": {
            "label": "=",
            "labelClass": "center"
          },
          "2": {
            "num": "306",
            cellClass: 'singleUnderline'
          },
          "3": {
            "label": " x ",
            "labelClass": "center"
          },
          "4": {
            "num": "307",
            cellClass: 'singleUnderline'
          },
          "5": {
            "label": " = ",
            "labelClass": "center"
          },
          "6": {
            "tn": "310",
            "labelClass": "center"
          },
          "7": {
            "num": "310", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'doubleUnderline'
          },
          "8": {
            "label": " B ",
            "labelClass": "center"
          }
        },
        {
          "0": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "2": {
            "num": "308"
          },
          "4": {
            "type": "none"
          },
          "7": {
            "type": "none"
          }
        }]
    }
  }
})();
