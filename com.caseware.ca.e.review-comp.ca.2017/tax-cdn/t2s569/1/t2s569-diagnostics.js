(function() {
  wpw.tax.create.diagnostics('t2s569', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('5690005', common.prereq(common.check(['t2s5.470', 't2s568.405'], 'isNonZero'), common.requireFiled('T2S569')));

    diagUtils.diagnostic('5690010', common.prereq(common.and(
        common.requireFiled('T2S569'),
        common.check(['300', '310'], 'isNonZero', true)),
        common.check(['110', '115', '120', '135'], 'isNonZero', true)));

    diagUtils.diagnostic('5690015', common.prereq(common.and(
        common.requireFiled('T2S569'),
        function(tools) {
          return tools.checkMethod(tools.list(['300', '310']), 'isNonZero') > 1 &&
              wpw.tax.utilities.dateCompare.lessThan(tools.field('120').get(), {day: 10, month: 8, year: 2007});
        }), common.check(['125', '130'], 'isNonZero', true)));

    diagUtils.diagnostic('5690020', common.prereq(common.and(
        common.requireFiled('T2S569'),
        common.and(
            common.check(['300', '310'], 'isNonZero', true),
            common.check('135', 'isYes'))),
        common.check(['140', '145', '150'], 'isNonZero', true)));

    diagUtils.diagnostic('5690025', common.prereq(common.and(
        common.requireFiled('T2S569'),
        common.check(['300', '310'], 'isNonZero', true)),
        common.check(['200', '205', '210', '215', '220', '225', '230'], 'isNonZero', true)));

    diagUtils.diagnostic('5690030', common.prereq(common.and(
        common.requireFiled('T2S569'),
        common.check('310', 'isNonZero')),
        common.check('300', 'isNonZero')));

    diagUtils.diagnostic('569.135', common.prereq(common.and(
        common.requireFiled('T2S569'),
        common.check('135', 'isYes')),
        common.check(['140', '145'], 'isNonZero', true)));

    diagUtils.diagnostic('569.305', function(tools) {
      return tools.field('305').require(function() {
        return this.get() <= tools.field('cp.Days_Fiscal_Period').get();
      })
    });

  });
})();
