(function() {
  'use strict';

  wpw.tax.global.formData.t2s100 = {
    formInfo: {
      abbreviation: 'T2S100',
      title: 'Balance Sheet Information',
      schedule: 'Schedule 100',
      code: 'Code 0801',
      category: 'GIFI',
      disclaimerShowWhen: {fieldId: 'cp.184', check: 'isYes'},
      disclaimerTextField: {formId: 'cp', fieldId: '183'},
      formFooterNum: 'T2 SCH 100 E (14)',
      description: [
        {
          'type': 'list',
          'items': [
            'Use this schedule to report the corporation\'s balance sheet information.',
            'For more information, see Guide RC4088, <i>General Index of Financial Information (GIFI)</i> and T4012,<i>T2 Corporation – Income Tax Guide.</i>',
            'If you need more space, attach additional schedules.'
          ]
        }
      ]
    },
    sections: [
      {
        forceBreakAfter: true,
        header: 'Assets',
        rows: [
          {labelClass: 'fullLength'},
          {type: 'gifiWorkchart', num: '100'},
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {type: 'gifiWorkchart', num: '101'},
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {type: 'gifiWorkchart', num: '102'},
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {type: 'gifiWorkchart', num: '103'},
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {type: 'gifiWorkchart', num: '104'},
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'}
        ]
      },
      {
        header: 'Liabilities',
        rows: [
          {labelClass: 'fullLength'},
          {
            type: 'gifiWorkchart', num: '200'
          },
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {
            type: 'gifiWorkchart', num: '201', header: 'Long-term Liabilities'
          },
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {
            type: 'gifiWorkchart', num: '202', header: 'Other Liabilities'
          }
        ]
      },
      {
        header: 'Equity',
        rows: [
          {labelClass: 'fullLength'},
          {
            type: 'gifiWorkchart', num: '300'
          },
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {label: 'Retained Earnings', labelClass: 'fullLength bold'},
          {type: 'gifiWorkchart', num: '400'},
          {labelClass: 'fullLength'},
          {
            label: '* The amount on line 3680 must equal the amount on line 9999' +
            ' of S125 or S140 without considering line 9998',
            labelClass: 'fullLength'
          }
        ]
      }
    ]
  };
})();
