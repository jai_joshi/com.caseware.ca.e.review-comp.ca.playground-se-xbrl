(function() {

  wpw.tax.create.calcBlocks('t2s100', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {

      for (var yearIndex = 0; yearIndex < 5; yearIndex++) {
        var value9999 = calcUtils.gifiValue('T2S140', '100', 9999, yearIndex);
        var value9998 = calcUtils.gifiValue('T2S140', '100', 9998, yearIndex);
        var value = parseFloat((value9999 ? value9999 : 0)) - parseFloat((value9998 ? value9998 : 0));
        field('400').cell(1, yearIndex + 2).assign(value);
        field('3680').setKey(yearIndex, value);
      }
    });
  });
})();
