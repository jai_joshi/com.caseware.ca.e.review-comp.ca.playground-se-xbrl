(function () {
  wpw.tax.global.tableCalculations.t2s100 = {
    "100": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      hasTotals: true,
      "gifiCodes": "currentAsset",
      "header": "Current Assets",
      "fixedGifiRows": [
        {code: "1000", description: "Cash and deposits"}
        // {code: '1061', description: 'Allowance for doubtful accounts', isCredit: true},
        // {code: '1063', description: 'Allowance for doubtful trade accounts and receivable', isCredit: true},
        // {
        //   code: '1065',
        //   description: 'Allowance for doubtful trade accounts receivable from related parties',
        //   isCredit: true
        // },
        // {code: '1070', description: 'Allowance for doubtful amounts contained in leases receivable', isCredit: true},
        // {code: '1072', description: 'Allowance for doubtful accounts receivable from employees', isCredit: true}
      ],
      "range": [1000, 1599],
      "totalCode": "1599",
      "totalDescription": "Total Current Assets",
      "numberOfYearsToShow": 5
    },
    "101": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "tangibleAsset",
      "header": "Tangible Capital Assets<br>(Note: Accumulated amortization must be entered as a negative value)",
      "fixedGifiRows": [
        {
          "code": "1600",
          "description": "Land"
        },
        {
          "code": "1684",
          "description": "Buildings under construction"
        },
        {
          "code": "1782",
          "description": "Machinery and equipment under construction"
        },
        {
          "code": "1920",
          "description": "Other capital assets under construction"
        }],
      "range": [1600, 2009],
      "totalCode": "2008",
      "totalDescription": "Total Tangible Capital Assets",
      "amortizationCode": "2009",
      "amortizationDescription": "Total Accumulated Amortization of Tangible Capital Assets",
      "numberOfYearsToShow": 5
    },
    "102": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "intangibleAsset",
      "header": "Intangible Capital Assets<br>(Note: Accumulated amortization must be entered as a negative value)",
      "range": [2010, 2179],
      "totalCode": "2178",
      "totalDescription": "Total Intangible Capital Assets",
      "amortizationCode": "2179",
      "amortizationDescription": "Total Accumulated Amortization of Intangible Capital Assets",
      "numberOfYearsToShow": 5
    },
    "103": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "longtermAsset",
      hasTotals: true,
      "header": "Long-term Assets",
      "range": [2180, 2589],
      // fixedGifiRows: [
      //   {code: '2425', description: 'Accumulated amortization of deferred charges', isCredit: true}
      // ],
      "totalCode": "2589",
      "totalDescription": "Total Long-Term Assets",
      "numberOfYearsToShow": 5
    },
    "104": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "otherAsset",
      hasTotals: true,
      "header": "Other Assets",
      "range": [2590, 2599],
      "totalCode": "2598",
      "hideTotalCode": true,
      "totalDescription": "Total Other Assets",
      "postTotalRows": [{
        "code": "2599",
        "description": "Total assets",
        "dependantCodes": [1599,
          2008,
          2009,
          2178,
          2179,
          2589,
          2598],
        "operation": "addition"
      }],
      "numberOfYearsToShow": 5
    },
    "200": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "currentLiability",
      hasTotals: true,
      "header": "Current Liabilities",
      "range": [2600, 3139],
      "totalCode": "3139",
      "totalDescription": "Total Current Liabilities",
      "numberOfYearsToShow": 5
    },
    "201": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "longtermLiability",
      hasTotals: true,
      "header": "Long-term Liabilities",
      "range": [3140, 3450],
      "totalCode": "3450",
      "totalDescription": "Total Long-Term Liabilities",
      "numberOfYearsToShow": 5
    },
    "202": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "otherLiability",
      hasTotals: true,
      "header": "Other Liabilities",
      "range": [3460, 3499],
      "totalCode": "3498",
      "hideTotalCode": true,
      "totalDescription": "Total Other Liabilities",
      "postTotalRows": [{
        "code": "3499",
        "description": "Total Liabilities",
        "dependantCodes": [3139,
          3450,
          3498],
        "operation": "addition"
      }],
      "numberOfYearsToShow": 5
    },
    "300": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "equity",
      "header": "Shareholder's Equity",
      hasTotals: true,
      "fixedGifiRows": [{
        "code": "3500",
        "description": "Common Shares"
      }],
      "range": [3500, 3620],
      "endFixedGifiRows": [{
        "code": "3600",
        "description": "Retained Earnings/Deficit",
        "dependantCodes": [3849],
        "operation": "addition"
      }],
      "totalCode": "3620",
      "totalDescription": "Total Equity",
      "postTotalRows": [{
        "code": "3640",
        "description": "Total Liabilities and Equity",
        "dependantCodes": [3499, 3620],
        "operation": "addition"
      }],
      "numberOfYearsToShow": 5
    },
    "400": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "re",
      hasTotals: true,
      fixedGifiRows: [
        {code: '3660', description: 'Retained Earnings / Deficit-Start'},
        {
          code: '3680',
          description: 'Net income / Loss*'
        }
        // {code: '3700', description: 'Dividends declared', isCredit:true},
        // {code: '3701', description: 'Cash dividends', isCredit:true},
        // {code: '3702', description: 'Patronage dividends', isCredit:true}
      ],
      "range": [3660, 3849],
      "totalCode": "3849",
      "totalDescription": "Retained Earnings/Deficit-End",
      "numberOfYearsToShow": 5
    }
  }
})();
