(function() {
  wpw.tax.create.diagnostics('t2s100', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var tableNums = ['100', '101', '102', '103', '104', '200', '201', '202', '300', '400'];
    var gifiCodes = ['1061', '1063', '1065', '1070', '1072', '1602', '1621', '1623', '1625', '1627', '1629', '1631',
      '1633', '1681', '1683', '1741', '1743', '1745', '1747', '1749', '1751', '1753', '1755', '1757', '1759', '1761',
      '1763', '1765', '1767', '1769', '1771', '1773', '1775', '1777', '1779', '1781', '1784', '1786', '1788', '1901',
      '1903', '1905', '1907', '1909', '1911', '1913', '1915', '1917', '1919', '1922', '2011', '2013', '2015', '2017',
      '2019', '2021', '2023', '2025', '2027', '2071', '2073', '2075', '2077'];
    var filledGIFI = [];

    function convertToSet(arr) {
      var set = {};
      for (var i = 0; i < arr.length; i++) {
        var item = arr[i];
        set[item] = set[item] + 1 || 1;
      }
      return set;
    }

    function checkDuplicates(tableNum) {
      diagUtils.diagnostic('100.duplicate', function(tools) {
        var table = tools.field(tableNum);
        var codeSet = convertToSet(wpw.tax.gifiService.getCurrentCodes(tools.form().valueFormId, tableNum));
        return tools.checkAll(table.getRows(), function(row) {
          var code = row[0] ? row[0].valueObj.label : null;
          if (!wpw.tax.utilities.isEmptyOrNull(code) && codeSet[code] > 1) {
            row[1].mark();
            return false;
          }
          return true;
        });
      });
    }

    function checkHasCode(tableNum) {
      diagUtils.diagnostic('gifi.noCode', function(tools) {
        var table = tools.field(tableNum);
        return tools.checkAll(table.getRows(), function(row) {
          if (tools.checkMethod(row.slice(2), 'isNonZero') && !row[0].valueObj.label) {
            row[1].mark();
            return false;
          }
          return true;
        });
      });
    }

    for (var table in tableNums) {
      checkDuplicates(tableNums[table]);
      checkHasCode(tableNums[table]);
    }

    diagUtils.diagnostic('1000001', function(tools) {
          return tools.requireAll(tools.list(['2599', '3499', '3620']), function() {
            return this.require('isFilled');
          })
        });//No highlight required

    diagUtils.diagnostic('1000002', common.prereq(common.and(
        common.requireFiled('T2S100'),
        common.check(['2599', '3499', '3620'], 'isFilled')),
        function(tools) {
          tools.field('2599').mark()
          return (tools.field('2599').getKey(0) || 0) ==
              (parseFloat(tools.field('3499').getKey(0) || 0) +
                  parseFloat(tools.field('3620').getKey(0) || 0))
        }));//No highlight required

    diagUtils.diagnostic('gifi.requiredPair', common.prereq(common.requireFiled('T2S100'),
        function(tools) {
          return tools.checkAll(gifiCodes, function(code) {
            if (tools.field(code).getKey(0) && tools.field(code).getKey(0) != 0) {
              if (code >= 1000 && code <= 1599) {
                var gifiCodeRows = wpw.tax.gifiService.getCurrentCodes('t2s100', '100', code);
                var relatedGifiCodeRows = wpw.tax.gifiService.getCurrentCodes('t2s100', '100', code - 1);
                if (gifiCodeRows) {
                  return tools.field('100').cell(gifiCodeRows[0], 2).require(function() {
                    return relatedGifiCodeRows ?
                        (this.isZero() || this.isNegative()) &&
                        this.get() <= tools.field('100').cell(relatedGifiCodeRows[0], 2).get() :
                        this.isZero();
                  })
                } else return true;
              }
              else if (code >= 1600 && code <= 2009) {
                gifiCodeRows = wpw.tax.gifiService.getCurrentCodes('t2s100', '101', code);
                relatedGifiCodeRows = wpw.tax.gifiService.getCurrentCodes('t2s100', '101', code - 1);
                if (gifiCodeRows) {
                  return tools.field('101').cell(gifiCodeRows[0], 2).require(function() {
                    return relatedGifiCodeRows ?
                        (this.isZero() || this.isNegative()) &&
                        this.get() <= tools.field('101').cell(relatedGifiCodeRows[0], 2).get() :
                        this.isZero();
                  })
                } else return true;
              }
              else if (code >= 2010 && code <= 2179) {
                gifiCodeRows = wpw.tax.gifiService.getCurrentCodes('t2s100', '102', code);
                relatedGifiCodeRows = wpw.tax.gifiService.getCurrentCodes('t2s100', '102', code - 1);
                if (gifiCodeRows) {
                  return tools.field('102').cell(gifiCodeRows[0], 2).require(function() {
                    return relatedGifiCodeRows ?
                        (this.isZero() || this.isNegative()) &&
                        this.get() <= tools.field('102').cell(relatedGifiCodeRows[0], 2).get() :
                        this.isZero();
                  })
                } else return true;
              }
            }
            else return true;
          });
        }));

    //Same error code as in t2s10W
    diagUtils.diagnostic('010W.1000', common.prereq(common.and(
        common.requireFiled('T2S100'),
        function(tools) {
          for (var key in tools.field('2018').get()) {
            if (tools.field('2018').getKey(key) > 0)
              return true;
          }
          return false;
        }), function(tools) {
      var gifiCodeRows = wpw.tax.gifiService.getCurrentCodes('t2s100', '102', '2018');
      var row2018 = tools.field('102').getRow(gifiCodeRows).splice(2, 7);
      return tools.requireAll(row2018, function() {
        return this.get() < 1 || this.get() == undefined;
      });
    }));

    //Checks if the current year value for code 3660 is equal to the previous year value for code 3849
    diagUtils.diagnostic('100.3660', common.prereq(common.and(
        common.requireFiled('T2S100'),
        function(tools) {
          return !angular.isUndefined(tools.field('3660').getKey(0)) && !angular.isUndefined(tools.field('3849').getKey(1));
        }), function(tools) {
          return tools.field('400').getRow(0)[2].require(function() {
            return this.get() == tools.field('3849').getKey(1);
          });
        }));

    diagUtils.diagnostic('100.foreignCheck', common.prereq(function(tools) {
      return tools.checkMethod(tools.list(['t2s100.1004', 't2s100.1005', 't2s100.1187', 't2s100.2301', 't2s100.2302']), function() {
        if(!wpw.tax.utilities.isEmptyOrNull(this.getKey(0))) {
          filledGIFI.push(this);
          return true;
        }
        return false;
      })
    }, function(tools) {
      return tools.checkAll(filledGIFI, function(field) {
        var tableNum = field.fieldObj.fieldId > 2300 ? '103' : '100';
          var gifiCodeRows = wpw.tax.gifiService.getCurrentCodes('t2s100', tableNum, field.fieldObj.fieldId);
        return tools.field(tableNum).cell(gifiCodeRows, 2).require('isZero') || tools.requireOne(tools.list(['t1135.2000', 't1135.2001']), 'isChecked');
      })
    }));

  });
})();
