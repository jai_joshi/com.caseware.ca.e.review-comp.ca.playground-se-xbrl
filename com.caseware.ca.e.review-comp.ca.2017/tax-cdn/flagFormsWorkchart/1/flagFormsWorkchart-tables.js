(function() {
  var columnPrintWidth = 'small-input-width';

  wpw.tax.global.tableCalculations.flagFormsWorkchart = {
    '600': {
      fixedRows: true,
      infoTable: true,
      scrollx: true,
      columns: [
        {
          header: '<b>List of Tax Forms</b>',
          width: '65%',
          type: 'text',
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          header: '<b>Type</b>',
          type: 'text',
          disabled: true,
          cannotOverride: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          header: '<b>Print Return</b>',
          colClass: columnPrintWidth,
          type: 'singleCheckbox'
        },
        {colClass: 'std-spacing-width', type: 'none'}
      ]
    }
  }
})();
