(function () {

  wpw.tax.create.calcBlocks('flagFormsWorkchart', function (calcUtils) {

    function addToCategoryToList(categories, category, item) {
      if (!categories[category]) {
        categories[category] = [];
      }
      categories[category].push(item);
    }

    function setPrintFlaggedForms(calcUtils) {
      var guidList = [];
      var tableCells = calcUtils.field('600').valueObj.value.cells;
      for (var cell in tableCells) {
        if (tableCells[cell][4].value) {
          guidList.push(tableCells[cell].formId);
        }
      }
      calcUtils.form('config').field('printForms').set(guidList)
    }

    calcUtils.onFormLoad('flagFormsWorkchart', function (calcUtils) {
      // Check to see if the table has already been loaded
      if (calcUtils.field('tableLoaded').get()) {
        return;
      }

      // We initialize the table rows
      var table = calcUtils.field('600');
      var forms = wpw.tax.create.retrieveAll('formData');
      //Remove T2J and add it manually so it's always first
      delete forms['t2j'];
      //Remove forms as they should not be printed or jump to
      delete forms['autoFillWorkchart'];
      delete forms['flagFormsWorkchart'];
      delete forms['integrationWorkchart'];
      delete forms['engagementProfile'];

      // remove all rows first
      var numRows = table.size().rows;
      for (var i = 0; i < numRows; i++) {
        calcUtils.removeTableRow('flagformsworkchart', '600');
      }

      var undefinedCategory = 'No Category';
      var categories = {};
      for (var formId in forms) {
        if (!forms.hasOwnProperty(formId)) {
          continue;
        }
        var category = forms[formId].formInfo.category;
        if (!category) {
          addToCategoryToList(categories, undefinedCategory, formId);
        } else {
          addToCategoryToList(categories, category, formId);
        }
      }

      var addCategoryToTable = function (categoryList, categoryName) {
        for (var i = 0; i < categoryList.length; i++) {
          var formId = categoryList[i];
          var formData = wpw.tax.create.retrieve('formData', formId);
          var formAbbreviation ='';
          if(formData.formInfo.abbreviation!= undefined){
            if (formData.formInfo.abbreviation.substring(0, 3)=="t2s"){
              formAbbreviation =formData.formInfo.abbreviation.substring(0, 3).toUpperCase()+formData.formInfo.abbreviation.substring(3);
            }else{
              formAbbreviation=formData.formInfo.abbreviation.substring(0, 1).toUpperCase()+formData.formInfo.abbreviation.substring(1);
            }
            if (formData.formInfo.abbreviation=="CP"){
              formAbbreviation ="TOC";
            }
          }
          addFormRow(formAbbreviation + ' - ' + formData.formInfo.title, formId, categoryName);
        }
      };

      var addFormRow = function (name, formId, category) {
        calcUtils.addTableRow('flagFormsWorkchart', '600');
        var rowIndex = table.size().rows - 1;
        table.cell(rowIndex, 0).assign(name);
        table.cell(rowIndex, 0).config('cannotOverride', true);
        table.cell(rowIndex, 0).source(calcUtils.form(formId));
        table.cell(rowIndex, 2).assign(category);
        table.setRowInfo(rowIndex, 'formId', formId);
      };

      //Manually add T2J and GIFI forms since they should always be first
      addFormRow('T2 Jacket', 't2j', 'Federal Tax Forms');
      addCategoryToTable(categories['GIFI'].sort(), 'GIFI');
      delete categories['GIFI'];

      // Sort each category and add it to the table
      angular.forEach(categories, function (categoryList, categoryName) {
        if (categoryName == 'Federal Tax Forms') {
          categoryList.sort(function (a, b) {
            if (a.indexOf('t2s') > -1) a = a.substr(3);
            else a = a.substr(1);

            if (b.indexOf('t2s') > -1) b = b.substr(3);
            else b = b.substr(1);

            return a - b;
          });
        }
        else categoryList.sort();

        addCategoryToTable(categoryList, categoryName);
      });
      // Set this to true to ensure this calc won't run again
      calcUtils.field('tableLoaded').assign(true);

      var flaggedForms = wpw.tax.utilities.flagger(calcUtils.accessor);
      var table = calcUtils.field('600');
      calcUtils.removeDependency(table);
      table.getRows().forEach(function (row, rIndex) {
        var formId = table.getRowInfo(rIndex).formId;
        if (formId) {
          if (formId.toUpperCase() in flaggedForms) {
            row[4].set(true);
          } else {
            row[4].set(false);
          }
        }

        setPrintFlaggedForms(calcUtils);
      });
    });
  });
})();
