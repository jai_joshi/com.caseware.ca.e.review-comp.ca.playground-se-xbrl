(function () {
  'use strict';

  wpw.tax.create.formData('flagFormsWorkchart', {
    formInfo: {
      title: 'Form Flagger Workchart',
      showCorpInfo: true,
      headerImage: 'cw',
      category: 'Workcharts'
    },
    sections: [
      {
        header: ' ',
        hideFieldset: true,
        rows: [
          {
            type: 'table', num: '600'
          }
        ]
      }
    ]
  });
})
();
