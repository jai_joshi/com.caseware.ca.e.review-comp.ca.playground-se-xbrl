(function() {

  wpw.tax.create.formData('t2s554', {
    'formInfo': {
      abbreviation: 't2s554',
      'title': 'Ontario Computer Animation and Special Effects Tax Credit ',
      //'subTitle': '(2015 and later tax years)',
      'schedule': 'Schedule 554',
      isRepeatForm: true,
      repeatFormData: {titleNum: '210'},
      'code': 'Code 1701',
      formFooterNum: 'T2 SCH 554 E (17)',
      headerImage: 'canada-federal',
      'showCorpInfo': true,
      'description': [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule to claim an Ontario computer animation and special effects (OCASE) tax credit' +
              ' under section 90 of the <i>Taxation Act</i>, 2007 (Ontario). Complete a separate Schedule 554 for each eligible production.'
            },
            {
              label: 'The OCASE tax credit is a refundable tax credit that is equal to 20% of the qualifying labour' +
              ' expenditures, incurred before April 24, 2015, and 18% of expenditures incurred after April 23, 2015,' +
              ' by a qualifying corporation in a tax year, that are directly attributable to eligible computer ' +
              'animation and special effects activities (ECASEA) for an eligible production. Transitional rules' +
              ' may allow qualifying labour expenditures incurred after April 23, 2015 and before August 1, 2016 to ' +
              'be eligible for the 20% rate, see Part 4.'
            },
            {
              label: 'For a production that only starts incurring eligible expenditures after April 23, 2015,' +
              ' the qualifying corporation must have also been issued a certificate of eligibility for either the Ontario Production ' +
              'Services Tax Credit (OPSTC) or the Ontario Film and Television Tax Credit (OFTTC) for the production ' +
              'to be an eligible production.'
            },
            {
              label: 'To be eligible for the OCASE tax credit, you must be a Canadian corporation and meet the eligibility requirements in Part 3.'
            },
            {
              label: 'Before claiming an OCASE tax credit, you must obtain a certificate of eligibility from the Ontario Media Development Corporation (OMDC). Only one '+
              'certificate of eligibility will be issued for all of the eligible productions of the qualifying corporation for the tax year. '+
              'Enter the certificate information for this production in Part 2 of this schedule.'
            },
            {
              label: 'To claim the OCASE tax credit, file this schedule and the certificate of eligibility with your T2 – <i>Corporation Income Tax Return</i> for the tax year.'
            }
          ]
        }
      ],
      category: 'Ontario Forms'
    },
    'sections': [
      {
        'header': 'Part 1 - Contact information',
        'rows': [
          {
            'type': 'table',
            'num': '105'
          }
        ]
      },
      {
        'header': 'Part 2 – Identifying the eligible production',
        'rows': [
          {
            'label': 'Certificate of eligibility number'
          },
          {
            'type': 'infoField',
            'tn': '200',
            'num': '200',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '8',
                    'max': '9'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          {
            'label': 'Production title'
          },
          {
            'type': 'infoField',
            'num': '210',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '1',
                    'max': '175'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'tn': '210'
          },
          {
            'label': 'Estimated OCASE tax credit for this production'
          },
          {
            'type': 'infoField',
            'num': '220',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'tn': '220',
            'width': '50px'
          }
        ]
      },
      {
        'header': 'Part 3 - Eligibility',
        'rows': [
          {
            'label': 'Is the corporation a Canadian corporation that for the tax year, performed ECASEA for the eligible production at a permanent establishment in Ontario?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '300',
            'tn': '300',
            'init': '2'
          },
          {
            'label': 'Was the corporation exempt from tax for the tax year under Part III of the <i>Taxation Act, 2007</i> (Ontario)?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '310',
            'tn': '310',
            'init': '1'
          },
          {
            'label': 'Was the corporation, at any time in the tax year, controlled directly or indirectly, in any manner, by one or more corporations, all or part of whose taxable income is exempt from tax under section 57 of the <i>Corporations Tax Act</i> (Ontario) or Part III of the <i>Taxation Act, 2007</i> (Ontario)?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '320',
            'tn': '320',
            'init': '1'
          },
          {
            'label': 'Was the corporation, at any time in the tax year, a prescribed labour-sponsored venture capital corporation?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '330',
            'tn': '330',
            'init': '1'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If you answered <b>no</b> to question 1, or yes to question 2, 3, or 4, then you are <b>not eligible</b> for the OCASE tax credit. ',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Part 4 - Ontario labour expenditures before April 24, 2015',
        'rows': [
          {
            'label': '<b>Transitional rules</b>'
          },
          {
            'label': 'The corporation may claim Ontario labour expenditures incurred after April 23, 2015, and before August 1, 2016, at the 20% rate if the corporation meets all the following criteria: ',
            'labelClass': 'fullLength'
          },
          {
            'label': '• before April 24, 2015, the corporation has entered into at least one written agreement in respect of a qualifying labour expenditure for the eligible production with a person that deals at arm\'s length with the corporation and any of the following criteria: ',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '- the agreement is in respect of digital animation or digital visual effects for use in the eligible production <br> - the agreement demonstrates, in the opinion of the Minister of Tourism, Culture and Sport, the corporation has made a significant commitment to production activities related to the eligible production in Ontario.',
            'labelClass': 'fullLength tabbed2'
          },
          {
            'label': '• before August 1, 2015, the corporation has notified the Ontario Media Development Corporation in writing of its intent to apply for a certificate in respect of the eligible production;',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• before August 1, 2016, the corporation has applied to the Ontario Media Development Corporation for a certificate in respect of the eligible production;',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• principal photography or key animation for the production commenced before August 1, 2015. ',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'If eligible, complete Parts 4 and 5 instead of Parts 6 and 7. ',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Qualifying wage amount incurred for salaries and wages paid to employees of the corporation and directly attributable to ECASEA performed in Ontario',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '400',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '400'
                }
              },
              null
            ]
          },
          {
            'labelClass': 'bold'
          },
          {
            'label': 'Qualifying remuneration directly attributable to ECASEA performed in Ontario paid to:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- other taxable, arm\'s length Canadian corporationssolely owned by an individual*',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '411',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '411'
                }
              },
              null
            ]
          },
          {
            'label': '- individuals',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '412',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '412'
                }
              },
              null
            ]
          },
          {
            'label': '- eligible partnerships',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '422',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '422'
                }
              },
              null
            ]
          },
          {
            'label': 'Amounts paid to a parent corporation for Ontario labour expenditures incurred by it in respect of the subsidiary wholly-owned corporation\'s ECASEA under a reimbursement agreement,  effective for a production, if principal photography commenced after March 22, 2007',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '440',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '440'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> lines 400, 411, 412, 422 and 440)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '441'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '442'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Complete lines 450 and 460 if there is an entry on line 440:',
            'labelClass': 'fullLength'
          },
          {
            'label': ' - Name of parent corporation',
            'labelClass': 'tabbed'
          },
          {
            'type': 'infoField',
            'num': '450',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '1',
                    'max': '175'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'tn': '450'
          },
          {
            'label': ' - Business number of parent corporation',
            'labelClass': 'tabbed'
          },
          {
            'tn': '460',
            'type': 'infoField',
            'inputType': 'custom',
            'num': '460',
            'format': [
              '{N9}RC{N4}',
              'NR'
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario labour expenditures that the corporation (parent) transferred under a reimbursement agreement to a subsidiary wholly-owned corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '470',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '470'
                }
              }
            ]
          },
          {
            'label': '<b>Total Ontario labour expenditures</b> (amount A <b>minus</b> line 470) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '480',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '480'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* These other corporations must meet the requirements of paragraph 4 of subsection 25(4) of <i>Regulation 37/09</i> of the <i>Taxation Act, 2007</i> (Ontario), subject to the restrictions stated in subsection 25(2.1)',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 5 - Assistance before April 24, 2015',
        'rows': [
          {
            'label': 'Assistance related to expenditures incurred for the eligible production (other than excluded government assistance) that the qualifying corporation, or other person, or partnership has received, is entitled to receive, or may reasonably expect to receive:',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount of assistance directly attributable to line 480 in Part 4',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '500',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '500'
                }
              }
            ]
          },
          {
            'label': 'Amount of assistance not directly attributable to line 480 in Part 4',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '510',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '510'
                }
              },
              null
            ]
          },
          {
            'label': 'Prescribed cost, as defined in subsection 24 (1) of <i>Regulation 37/09 of the Taxation Act, 2007</i> (Ontario)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '520',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '520'
                }
              },
              null
            ]
          },
          {
            'type': 'table',
            'num': '550'
          },
          {
            'label': '<b>Total assistance</b> (line 500 <b>plus</b> line 530)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '540',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '540'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 6 - Ontario labour expenditures after April 23, 2015',
        'rows': [
          {
            'label': 'Qualifying wage amount incurred for salaries and wages paid to employees of the corporation and directly attributable to ECASEA performed in Ontario',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '615',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '615'
                }
              },
              null
            ]
          },
          {
            'label': 'Qualifying remuneration directly attributable to ECASEA performed in Ontario paid to:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- other taxable, arm\'s length Canadian corporations solely owned by an individual*',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '616',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '616'
                }
              },
              null
            ]
          },
          {
            'label': '- individuals',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '618',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '618'
                }
              },
              null
            ]
          },
          {
            'label': '- eligible partnerships',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '622',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '622'
                }
              },
              null
            ]
          },
          {
            'label': 'Amounts paid to a parent corporation for Ontario labour expenditures incurred by it in respect of the subsidiary wholly-owned corporation\'s ECASEA under a reimbursement agreement,  effective for a production, if principal photography commenced after March 22, 2007',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '640',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '640'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (<b>add</b> lines 615, 616, 618, 622 and 640)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '641'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '642'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Complete lines 650 and 660 if there is an entry on line 640:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Name of parent corporation',
            'labelClass': 'tabbed'
          },
          {
            'type': 'infoField',
            'num': '650',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '1',
                    'max': '175'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'tn': '650'
          },
          {
            'label': 'Business number of parent corporation',
            'labelClass': 'tabbed'
          },
          {
            'tn': '660',
            'type': 'infoField',
            'inputType': 'custom',
            'num': '660',
            'format': [
              '{N9}RC{N4}',
              'NR'
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario labour expenditures that the corporation (parent) transferred under a reimbursement agreement to a subsidiary wholly-owned corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '670',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '670'
                }
              }
            ]
          },
          {
            'label': '<b>Total Ontario labour expenditures</b> (amount B <b>minus</b> line 670) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '680',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '680'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* These other corporations must meet the requirements of paragraph 4 of subsection 25(4) of<i> Regulation 37/09 of the Taxation Act</i>, 2007 (Ontario), subject to the restrictions stated in subsection 25(2.1).',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 7 - Assistance after April 23, 2015',
        'rows': [
          {
            'label': 'Assistance related to expenditures incurred for the eligible production (other than excluded government assistance) that the qualifying corporation, or other person, or partnership has received, is entitled to receive, or may reasonably expect to receive:',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount of assistance directly attributable to line 680 in Part 6',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '700',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '700'
                }
              }
            ]
          },
          {
            'label': 'Amount of assistance not directly attributable to line 680 in Part 6',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '710',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '710'
                }
              },
              null
            ]
          },
          {
            'label': 'Prescribed cost, as defined in subsection 24(1) of <i>Regulation 37/09 of the Taxation Act, 2007</i> (Ontario) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '720',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '720'
                }
              },
              null
            ]
          },
          {
            'type': 'table',
            'num': '750'
          },
          {
            'label': '<b>Total assistance</b> (line 700 <b>plus</b> line 730)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '740',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '740'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 8 - Tax credit calculation',
        'rows': [
          {
            'label': 'Qualifying labour expenditures before April 24, 2015 <br>(line 480 from Part 4 <b>minus</b> line 540 from Part 5) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '600',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '600'
                }
              },
              null
            ]
          },
          {
            'label': 'Qualifying labour expenditures after April 23, 2015 <br>(line 680 in Part 6 <b>minus</b> line 740 in Part 7) (if negative, enter "0") ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '601',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '601'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Ontario computer animation and special effects tax credit before April 24, 2015</b> (line 600 <b>multiplied</b> by 20%)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '610',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '610'
                }
              }
            ]
          },
          {
            'label': '<b>Ontario computer animation and special effects tax credit after April 23, 2015</b> (line 601 <b>multiplied</b> by 18%)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '611',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '611'
                }
              }
            ]
          },
          {
            'label': '<b>Total Ontario computer animation and special effects tax credit</b> (line 610 <b>plus</b> line 611)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '612',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '612'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter the amount from line 612 on line 456 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>. If you are filing more than one Schedule 554, add all the amounts from each line 612 and enter the total amount on line 456 of Schedule 5.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            label: 'See the privacy notice on your return.',
            labelClass: 'absoluteAlignRight'
          }
        ]
      }
    ]
  });
})();

