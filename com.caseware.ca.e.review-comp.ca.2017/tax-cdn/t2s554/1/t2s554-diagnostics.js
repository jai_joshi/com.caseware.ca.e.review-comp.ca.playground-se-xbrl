(function() {
  wpw.tax.create.diagnostics('t2s554', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S554'), common.bnCheck('460')));
    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S554'), common.bnCheck('660')));

    diagUtils.diagnostic('5540005', common.prereq(common.check('t2s5.456', 'isNonZero'), common.requireFiled('T2S554')));

    diagUtils.diagnostic('5540010', common.prereq(common.requireFiled('T2S554'),
        common.check(['t2s5.456', '610', '611'], 'isEmpty', true)));

    diagUtils.diagnostic('5540015', common.prereq(common.and(
        common.requireFiled('T2S554'),
        common.check(['610', '611'], 'isNonZero')),
        common.check(['200', '210', '220'], 'isNonZero', true)));

    diagUtils.diagnostic('5540020', common.prereq(common.and(
        common.requireFiled('T2S554'),
        common.check(['610', '611'], 'isNonZero')),
        common.check(['300', '310', '320', '330'], 'isNonZero', true)));

    diagUtils.diagnostic('554.440', common.prereq(common.and(
        common.requireFiled('T2S554'),
        common.check('440', 'isNonZero')),
        common.check(['450', '460'], 'isNonZero', true)));

    diagUtils.diagnostic('554.640', common.prereq(common.and(
        common.requireFiled('T2S554'),
        common.check('640', 'isNonZero')),
        common.check(['650', '660'], 'isNonZero', true)));

  });
})();
