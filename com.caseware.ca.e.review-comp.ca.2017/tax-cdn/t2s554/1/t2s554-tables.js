(function() {
  var assistanceTableColumn = [
    {type: 'none', cellClass: 'alignRight'},
    {colClass: 'std-input-width'},
    {
      type: 'none',
      colClass: 'std-padding-width'
    },
    {type: 'none'},
    {colClass: 'std-input-width'},
    {
      type: 'none',
      colClass: 'std-padding-width'
    },
    {
      type: 'none',
      colClass: 'std-padding-width'
    },
    {type: 'none'},
    {
      type: 'none',
      colClass: 'std-padding-width'
    },
    {colClass: 'std-input-width'
    },
    {
      type: 'none',
      colClass: 'std-padding-width'
    }
  ];

  wpw.tax.create.tables('t2s554', {
    '105': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1],
      'columns': [
        {
          'width': '60%',
          cellClass: 'alignLeft'
        },
        {
          cellClass: 'alignLeft'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Name of contact person',
            'num': '110', "validate": {"or":[{"length":{"min":"1","max":"60"}},{"check":"isEmpty"}]},
            'tn': '110',
            type: 'text'
          },
          '1': {
            'label': 'Telephone number',
            'num': '120', type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
            validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]},
            'tn': '120',
            'telephoneNumber': true
          }
        }
      ]
    },
    '550': {
      'fixedRows': true,
      'infoTable': true,
      'columns': assistanceTableColumn,
      cells: [
        {
          '0':{
            label: 'Line 510'
          },
          '1':{
            num: '527'
          },
          '2':{
            label: ' x '
          },
          '3':{
            label: 'Line 480 in Part 4',
            cellClass: 'alignCenter singleUnderline'
          },
          '4':{
            num: '528',
            cellClass: 'singleUnderline'
          },
          '5':{
            label: ' ='
          },
          '8':{
            tn: '530'
          },
          '9':{
            num: '530',
            formField: true,
            "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          }
        },
        {
          '1':{
            type: 'none'
          },
          '3':{
            label: 'line 520',
            labelClass: 'center'
          },
          '4':{
            num: '529'
          },
          '9':{
            type: 'none'
          }
        }
      ]
    },
    '750': {
      'fixedRows': true,
      'infoTable': true,
      'columns': assistanceTableColumn,
      cells: [
        {
          '0':{
            label: 'Line 710'
          },
          '1':{
            num: '727'
          },
          '2':{
            label: ' x '
          },
          '3':{
            label: 'Line 680 in Part 6',
            cellClass: 'alignCenter singleUnderline'
          },
          '4':{
            num: '728',
            cellClass: 'singleUnderline'
          },
          '5':{
            label: ' ='
          },
          '8':{
            tn: '730'
          },
          '9':{
            num: '730',
            formField: true,
            "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          }
        },
        {
          '1':{
            type: 'none'
          },
          '3':{
            label: 'Line 720',
            labelClass: 'center'
          },
          '4':{
            num: '729'
          },
          '9':{
            type: 'none'
          }
        }
      ]
    }
  })
})();
