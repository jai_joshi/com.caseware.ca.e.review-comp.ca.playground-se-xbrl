(function() {
  function enableFields(calcUtils, numArray) {
    numArray.forEach(function(fieldId) {
      calcUtils.field(fieldId).disabled(false)
    })
  }

  function updateParentCorpInfo(calcUtils, amountField, nameField, businessNumField) {
    var field = calcUtils.field;
    var array = [nameField, businessNumField];
    if (field(amountField).get() == 0) {
      calcUtils.removeValue(array, true)
    }
    else {
      enableFields(calcUtils, array)
    }
  }

  wpw.tax.create.calcBlocks('t2s554', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      // Part 1: contact information//
      field('110').assign(field('CP.951').get() + ' ' + field('CP.950').get());
      field('110').source(field('CP.950'));
      calcUtils.getGlobalValue('120', 'CP', '959');
    });

    calcUtils.calc(function(calcUtils, field) {
      var isEligible = (field('300').get() == '1' && (field('310').get() == '2' || field('320').get() == '2' ||
      field('330').get() == '2'));
      if (isEligible) {
        enableFields(calcUtils, ['200', '210', '220']);
        //part 4
        calcUtils.sumBucketValues('441', ['400', '411', '412', '422', '440']);
        field('442').assign(field('441').get());
        updateParentCorpInfo(calcUtils, '440', '450', '460');
        field('480').assign(Math.max(field('442').get() - field('470').get(), 0));
        //part5
        field('527').assign(field('510').get());
        field('528').assign(field('480').get());
        field('529').assign(field('520').get());
        field('530').assign((field('528').get() / field('529').get()) * field('527').get());
        field('540').assign(field('500').get() + field('530').get());
        //part 6
        calcUtils.sumBucketValues('641', ['615', '616', '618', '622', '640']);
        field('642').assign(field('641').get());
        updateParentCorpInfo(calcUtils, '640', '650', '660');
        field('680').assign(Math.max(field('642').get() - field('670').get(), 0));
        //part 7
        field('727').assign(field('710').get());
        field('728').assign(field('680').get());
        field('729').assign(field('720').get());
        field('730').assign((field('728').get() / field('729').get()) * field('727').get());
        field('740').assign(field('700').get() + field('730').get());
        //part8
        field('600').assign(Math.max(field('480').get() - field('540').get(), 0));
        field('601').assign(Math.max(field('680').get() - field('740').get(), 0));
        field('610').assign(field('600').get() * 0.2);
        field('611').assign(field('601').get() * 0.18);
        calcUtils.sumBucketValues('612', ['610', '611'])
      }
      else {
        calcUtils.removeValue(['200', '210', '220'], true)
      }
    });

  })
}());
