(function() {
  wpw.tax.create.diagnostics('t2s54', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0540010', common.prereq(common.requireFiled('T2S54'),
        function(tools) {
          var table = tools.mergeTables(tools.field('199'), tools.field('205'));
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[1], row[2], row[4], row[5], row[8], row[9]], 'isNonZero'))
              return tools.requireOne([row[0]], 'isFilled');
            else return true;
          });
        }));

  });
})();
