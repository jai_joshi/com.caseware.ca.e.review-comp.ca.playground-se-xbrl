(function() {
  function enableNums(field, numArray) {
    for (var i = 0; i < numArray.length; i++) {
      field(numArray[i]).disabled(false);
    }
  }

  wpw.tax.create.calcBlocks('t2s54', function(calcUtils) {
    calcUtils.calc(function(calcUtils) {
      var num = '145';
      var num2 = '150';
      var midnum = '146';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils) {
      var num = '155';
      var num2 = '160';
      var midnum = '156';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get();
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils, field, form) {
      var part1Num = [100, 120, 140, 145, 150, 155, 160, 190];

      //Eligibility questions
      //var isCCPC = field('CP.120').get() == '1';
      var isDIC = field('CP.189').get() == '1';
      var currentYearElection = field('CP.2662').get() == '1';
      var previousYearElection = field('CP.267').get() == true;
      var isAmalgamation = field('CP.071').get() == '1';
      var isWoundUp = field('CP.072').get() == '1';
      //1. Was the corporation a CCPC during its preceding taxation year?
      // if (isCCPC) {
      //   field('010').assign(1);
      //   field('010').source(field('CP.120'));
      // }
      // else {
      //   field('010').assign(2);
      //   field('010').source(field('CP.120'));
      // } comment out as there's no way to check type of business for previous year

      //2. Corporations that become a CCPC or a DIC ( we dont have the option to check it yet so comment out for now)
      // if (field('CP.119').get() == '1' && field('010').get() == '1') {
      //   calcUtils.assignValue('011', '1');
      // }
      // else {
      //   calcUtils.assignValue('011', '2');
      // }
      //3. Corporations that were formed as a result of an amalgamation?
      if (isAmalgamation) {
        field('012').assign(1);
        field('012').source(field('CP.071'));
      }
      else {
        field('012').assign(2);
      }
      //6.Has the corporation wound-up a subsidiary in the preceding taxation year?
      if (isWoundUp) {
        field('015').assign(1);
        field('015').source(field('CP.072'));
      }
      else {
        field('015').assign(2);
        field('015').source(field('CP.072'));
      }

      //part 1 //
      if (field('010').get() == 2 || isDIC || currentYearElection || previousYearElection) {
        enableNums(field, part1Num);
        //tn140 only enable when subsection 89(11) is elected//
        if (field('CP.2661').get() == '1' || field('CP.2665').get() == 1) {
          enableNums(field, '140')
        }
        else {
          calcUtils.removeValue('140', true)
        }
        calcUtils.getGlobalValue('146', 'ratesFed', '460');
        calcUtils.getGlobalValue('156', 'ratesFed', '461');
        field('190').assign(field('100').get() + field('150').get() + field('160').get());
        field('145').assign(field('120').get() + field('140').get());
      }
      else {
        calcUtils.removeValue(part1Num, true)
      }

      //part 2
      field('199').getRows().forEach(function(row, rowIndex) {

        /// -- Calculations for column 2 -- ///
        var sectionsToAdd = [
          {
            date: '400',
            numField: '433'
          }, {
            date: '500',
            numField: '535'
          }, {
            date: '600',
            numField: '635'
          }
        ];

        var retrieveSectionValue = function(rowDate, section) {
          if (calcUtils.dateCompare.equal(rowDate, calcUtils.field(section.date).get())) {
            return calcUtils.field(section.numField).get()
          }
          return 0;
        };
        var rowDate = row[0].get();

        // Initialize to the value of the previous row
        var total = rowIndex > 0 ? field('199').cell(rowIndex - 1, 2).get() : 0;
        for (var i = 0; i < sectionsToAdd.length; i++) {
          total += retrieveSectionValue(rowDate, sectionsToAdd[i]);
        }
        row[2].assign(total);

        /// -- Calculations for column 5 -- ///
        var repeatedTable = field('205');
        var calcTotalsBeforeRow = function(rowIndex) {
          var total = 0;
          for (var i = 0; i < rowIndex; i++) {
            total += repeatedTable.cell(i, 3).get();
          }
          return total;
        };
        row[5].assign(calcTotalsBeforeRow(rowIndex));

        /// --- Calculation for repeated table --- ///

        var repeatedTableRow = repeatedTable.getRow(rowIndex);
        repeatedTableRow[0].assign(row[0].get());
        row[3].assign(
            row[1].get() +
            row[2].get() +
            (field('190').get() ? field('190').get() : 0));
        repeatedTableRow[1].assign(
            row[3].get() -
            row[4].get() -
            row[5].get());
      });

      field('205').getRows().forEach(function(row) {
        var col260 = row[1].get();
        var col270 = row[2].get();
        var min = 0;
        if (isNaN(col260)) {
          min = col270 || 0;
        } else if (isNaN(col270)) {
          min = col260;
        } else {
          min = Math.min(col260, col270);
        }
        row[3].assign(min);
      });

      //part3
      calcUtils.equals('300', '190');

      var dividendReceivable = field('199').getCol(1).map(function(cell) {
        return cell.get();
      });
      field('301').assign(Math.max.apply(null, dividendReceivable));

      field('510').assign(field('301').get() + field('302').get());

      var deductible = field('199').getCol(2).map(function(cell) {
        return cell.get();
      });
      field('520').assign(Math.max.apply(null, deductible));

      calcUtils.sumBucketValues('330', ['510', '520']);

      var dividendPayable = field('199').getCol(4).map(function(cell) {
        return cell.get();
      });
      field('351').assign(Math.max.apply(null, dividendPayable));
      calcUtils.equals('340', '330');
      calcUtils.sumBucketValues('350', ['300', '340']);
      field('540').assign(field('351').get() + field('352').get());
      calcUtils.equals('370', '290');
      calcUtils.sumBucketValues('380', ['540', '370']);
      calcUtils.equals('390', '380');
      calcUtils.subtract('590', '350', '390');

      //part 3
      //calcs.equals('510', '210'); //TODO: Need to confirm EAS Updates with the newer version of Comp. #1
      //calcs.sumBucketValues('330', ['510', '520']);
      //calcs.equals('340', '330');
      //calcs.sumBucketValues('350', ['300', '340']);
      ////calcs.equals('540', '240'); //TODO: Need to confirm EAS Updates with the newer version of Comp. #1
      //calcs.equals('370', '290');
      //calcs.sumBucketValues('380', ['540', '370']);
      //calcs.equals('390', '380');
      //calcs.subtract('590', '350', '390');

      //part 4
      //calcs.otherCalc('403', function(){
      //  field('403').get() = field('410').get() + calcs.getGlobalValue(false, 'T2S4', '897')
      //});
      //calcs.otherCalc('404', function(){
      //  field('404').get() = field('411').get() + calcs.getGlobalValue(false, 'T2S4', '200')
      //});
      //calcs.otherCalc('405', function(){
      //  field('405').get() = field('412').get() + calcs.getGlobalValue(false, 'T2S4', '876')
      //});
      //calcs.otherCalc('406', function(){
      //  field('406').get() = field('413').get() + calcs.getGlobalValue(false, 'T2S4', '859')
      //});
      //calcs.otherCalc('407', function(){
      //  field('407').get() = field('414').get() + calcs.getGlobalValue(false, 'T2S4', '634-t')
      //});
      //calcs.sumBucketValues('408', ['403', '404', '405', '406', '407']);
      //calcs.equals('409', '408');
      //calcs.sumBucketValues('415', ['410', '411', '412', '413', '414']);
      //calcs.equals('416', '415');
      //calcs.subtract('417', '409', '416');
      //calcs.equals('418', '417');
      //calcs.sumBucketValues('419', ['401', '402', '418']);
      ////var num040cp = calcs.getGlobalValue(false, 'CP', '040');   //TODO: S89 is not available
      ////if (num040cp == '1' || num040cp == '2') {
      ////  calcs.assignValue('423', '0');
      ////}
      ////else {
      ////  calcs.getGlobalValue('423', 'S89', '');
      ////}
      //calcs.getGlobalValue('424', 'T2S53', '100');
      //calcs.subtract('427', '425', '426');
      //calcs.equals('428', '427');
      //calcs.subtract('429', '424', '428', true);
      //calcs.equals('430', '429');
      //calcs.sumBucketValues('431', ['420', '421', '422', '423', '430']);
      //calcs.equals('432', '431');
      //calcs.subtract('433', '419', '432');

      //part 4
      if (field('011').get() == '1') {
        for (var fieldId = 401; fieldId <= 433; fieldId++) {
          field(fieldId).disabled(false);
        }
        calcUtils.sumBucketValues('408', ['403', '404', '405', '406', '407']);
        calcUtils.equals('409', '408');
        calcUtils.sumBucketValues('415', ['410', '411', '412', '413', '414']);
        calcUtils.equals('416', '415');
        calcUtils.subtract('417', '409', '416');
        calcUtils.equals('418', '417');
        calcUtils.sumBucketValues('419', ['401', '402', '418']);
        if (field('CP.Corp_Type').get() == '2') {
          calcUtils.removeValue('423', true)
        }
        else (field('423').disabled(false));
        //var num040cp = calcs.getGlobalValue(false, 'CP', '040');   //TODO: S89 is not available
        //if (num040cp == '1' || num040cp == '2') {
        //  calcs.assignValue('423', '0');
        //}
        //else {
        //  calcs.getGlobalValue('423', 'S89', '');
        //}
        calcUtils.getGlobalValue('424', 'T2S53', '100');
        calcUtils.subtract('427', '425', '426');
        calcUtils.equals('428', '427');
        calcUtils.subtract('429', '424', '428', true);
        calcUtils.equals('430', '429');
        calcUtils.sumBucketValues('431', ['420', '421', '422', '423', '430']);
        calcUtils.equals('432', '431');
        calcUtils.subtract('433', '419', '432');
      }
      else {
        calcUtils.removeValue(['401', '402', '403', '404', '405', '406', '407', '408', '409', '410', '411', '412', '413',
          '414', '415', '416', '417', '418', '419', '420', '421', '422', '423', '424', '425', '426', '427',
          '428', '429', '430', '431', '432', '433'], true);
      }
      //part5
      var part5Num = [501, 502, 503, 504, 505, 506, 507, 508, 509, 310, 511, 512, 513, 514, 515, 516, 517, 518,
        519, 320, 521, 522, 523, 524, 525, 526, 527, 528, 529, 530, 531, 532, 533, 534, 535];
      if (field('013').get() == '1' || field('014').get() == '1') {
        part5Num.forEach(function(fieldId) {
          field(fieldId).disabled(false);
        });
        calcUtils.sumBucketValues('508', ['503', '504', '505', '506', '507']);
        calcUtils.equals('509', '508');
        calcUtils.sumBucketValues('515', ['310', '511', '512', '513', '514']);
        calcUtils.equals('516', '515');
        calcUtils.subtract('517', '509', '516');
        calcUtils.equals('518', '517');
        calcUtils.sumBucketValues('519', ['501', '502', '518']);
        calcUtils.subtract('527', '525', '526');
        calcUtils.equals('528', '527');
        calcUtils.subtract('529', '524', '528', true);
        calcUtils.equals('530', '529');
        calcUtils.sumBucketValues('531', ['320', '521', '522', '523', '530']);
        calcUtils.equals('532', '531');
        calcUtils.subtract('533', '519', '532');
        calcUtils.sumBucketValues('535', ['533', '534']);
      }
      else {
        calcUtils.removeValue(part5Num, true)
      }

      //part6
      var part6Num = [601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620,
        621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635];
      if (field('016').get() == '1' || field('017').get() == '1') {
        part6Num.forEach(function(fieldId) {
          field(fieldId).disabled(false);
        });
        calcUtils.sumBucketValues('608', ['603', '604', '605', '606', '607']);
        calcUtils.equals('609', '608');
        calcUtils.sumBucketValues('615', ['610', '611', '612', '613', '614']);
        calcUtils.equals('616', '615');
        calcUtils.subtract('617', '609', '616');
        calcUtils.equals('618', '617');
        calcUtils.sumBucketValues('619', ['601', '602', '618']);
        calcUtils.subtract('627', '625', '626');
        calcUtils.equals('628', '627');
        calcUtils.subtract('629', '624', '628', true);
        calcUtils.equals('630', '629');
        calcUtils.sumBucketValues('631', ['620', '621', '622', '623', '630']);
        calcUtils.equals('632', '631');
        calcUtils.subtract('633', '619', '632');
        calcUtils.sumBucketValues('635', ['633', '634']);
      }
      else {
        calcUtils.removeValue(part6Num, true)
      }

    });
  });
})();
