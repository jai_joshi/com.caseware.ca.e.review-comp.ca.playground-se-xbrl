(function() {
  wpw.tax.global.tableCalculations.t2s54 = {
    'di_table_1': {
      'num': 'di_table_1',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Subtotal (line 120 <b>plus</b> 140)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '145',
            'disabled': true,
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'filters': '',
            'cellClass': ' doubleUnderline'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '146',
            'init': '80'
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '150'
          },
          '7': {
            'num': '150',
            'disabled': true,
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'filters': ''
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_2': {
      'num': 'di_table_2',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Investment corporation deduction (line 620 of the T2 return of the previous tax year)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '155',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'filters': ''
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '156',
            'init': '4'
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': '160'
          },
          '7': {
            'num': '160',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'filters': '',
            'cellClass': ' singleUnderline'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    "199": {
      "showNumbering": true,
      hasTotals: true,
      "repeats": ["205"],
      "executeAtEnd": true,
      "columns": [
        {
          "header": "Date* <br>(yyyy/mm/dd)",
          "tn": "200",
          "type": "date",
          "width": "18%"
        },
        {
          "header": "Total dividends** receivable <br> in the year up to but not <br> including the date on <br> line 200 that are deductible <br> under section 112",
          "tn": "210",
          "filters": "",
          filters: 'prepend $',
          "totalNum": "210"
        },
        {
          "header": "Total adjustments for <br> amalgamations,<br>wind-ups, or on ceasing <br> to be a CCPC***",
          "tn": "220",
          "filters": "",
          "maxLength": "13",
          filters: 'prepend $',
          "totalNum": "220"
        },
        {
          "header": "Subtotal <br> (<b>add</b> lines 190, 210, <br> and 220) ",
          "tn": "230",
          "filters": "",
          "maxLength": "13",
          filters: 'prepend $',
          "disabled": true,
          "totalNum": "230"
        },
        {
          "header": "Total dividends****<br>payable in the year up to<br>but not including the date<br>on line 200",
          "tn": "240",
          "filters": "",
          "maxLength": "13",
          filters: 'prepend $',
          "totalNum": "240"
        },
        {
          "header": "Total of excessive<br> eligible dividend designations made before the date on line 200 ",
          "tn": "250",
          "filters": "",
          "maxLength": "13",
          filters: 'prepend $',
          "totalNum": "250"
        }]
    },
    "205": {
      hasTotals: true,
      "showNumbering": true,
      "fixedRows": true,
      "keepButtonsSpace": true,
      "executeAtEnd": true,
      "columns": [
        {
          "header": "Date* <br>(yyyy/mm/dd)",
          "tn": "200",
          "type": "date",
          colClass: 'std-input-width',
          "disabled": true
        },
        {
          "header": "LRIP as of the date on<br>line 200<br>(line 230 <b>minus</b> the total<br>of line 240 and line 250)",
          "tn": "260",
          "width": "10%",
          "filters": "",
          "maxLength": "13",
          filters: 'prepend $',
          "disabled": true
        },
        {
          "header": "Total eligible dividends<br>paid on the date on<br>line 200 ",
          "tn": "270",
          "width": "10%",
          "filters": "",
          "maxLength": "13",
          filters: 'prepend $',
        },
        {
          "header": "Excessive eligible<br>dividend designation<br>(lesser of lines 260<br>and 270)",
          "tn": "280",
          "disabled": true,
          "total": true,
          "totalMessage": "Total excessive eligible dividend designations in the tax year (total of all amounts in column 280)",
          "totalNum": "290",
          filters: 'prepend $',
          "maxLength": 13,
          "width": "10%",
          "filters": "",
          "totalIndicator": "A"
        }],
      "startTable": "199"
    }
  }
})();
