(function() {
  'use strict';

  wpw.tax.global.formData.t2s54 = {
      'formInfo': {
        'abbreviation': 't2s54',
        isRepeatForm: true,
        'title': 'Low Rate Income Pool (LRIP) Calculation',
        //TODO: DO NOT DELETE THIS LINE: subtitle
        //'subTitle': ' (2006 and later tax years)',
        'schedule': 'Schedule 54',
        'showCorpInfo': true,
        code: '1501',
        formFooterNum: 'T2 SCH54 E (15)',
        headerImage: 'canada-federal',
        'description': [
          {
            'type': 'list',
            'items': [
              {
                label: ' Use this schedule to calculate the balance of the low rate income pool (LRIP) at any time' +
                ' in the tax year if you are a corporation resident in Canada that is:',
                sublist: [
                  'a corporation <b>other</b> than a Canadian-controlled private corporation (CCPC) or a deposit ' +
                  'insurance corporation (DIC); or',
                  'a corporation that elected under subsection 89(11) not to be a CCPC.'
                ]
              },
              {
                label: 'When an eligible dividend was paid or there was a change in the LRIP balance in the tax year,' +
                ' file a completed copy of this schedule with your <i> T2 Corporation Income Tax Return </i>. Do not ' +
                'send your worksheets with your return, but keep them in your records in case we ask to see them later.'
              },
              {
                label: 'All legislative references are to the Income Tax Act and the <i>Income Tax Regulations</i>.'
              },
              {
                label: 'Subsection 89(1) defines the terms eligible dividend, excessive eligible dividend ' +
                'designation, general rate income pool, and low rate income pool'
              }
            ]
          }
        ],
        category: 'Federal Tax Forms'
      },
      'sections': [
        {
          'header': 'Eligibility Questions',
          'fieldAlignRight': true,
          'rows': [
            {
              'label': 'Change in the type of corporation',
              'labelClass': 'fullLength bold'
            },
            {
              'type': 'infoField',
              'num': '010',
              'label': '1. Was the corporation a CCPC during its preceding taxation year?',
              'labelCellClass': 'indent',
              'inputType': 'radio',
              'init': false
            },
            {
              'type': 'infoField',
              'num': '011',
              'label': '2. Corporations that ceased to be a CCPC or a DIC',
              'labelCellClass': 'indent',
              'inputType': 'radio',
              'canClear': true
            },
            {
              'label': 'If the answer to question 2 is yes, complete Part 4.',
              'labelClass': 'bold fullLength tabbed2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Amalgamation (first year of filling after amalgamation)',
              'labelClass': 'fullLength bold'
            },
            {
              'type': 'infoField',
              'num': '012',
              'label': '3. Corporations that were formed as a result of an amalgamation',
              'labelCellClass': 'indent',
              'inputType': 'radio',
              'init': false
            },
            {
              'label': 'If the answer to question 3 is yes, answer questions 4 and 5. If the answer is no, go to question 6.',
              'labelClass': 'fullLength bold tabbed2'
            },
            {
              'type': 'infoField',
              'num': '013',
              'label': '4. Was one or more of the predecessor corporations neither a CCPC nor a DIC',
              'labelCellClass': 'indent',
              'inputType': 'radio',
              'canClear': true
            },
            {
              'label': 'If the answer to question 4 is yes, complete Part 5.',
              'labelClass': 'fullLength bold tabbed2'
            },
            {
              'type': 'infoField',
              'num': '014',
              'label': '5. Was one or more of the predecessor corporation a CCPC or a DIC during the taxation year that ended immediately before amalgamation?',
              'labelCellClass': 'indent',
              'inputType': 'radio',
              'canClear': true
            },
            {
              'label': 'If the answer to question 5 is yes, complete Part 5.',
              'labelClass': 'fullLength tabbed2 bold'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Winding-up',
              'labelClass': 'fullLength bold'
            },
            {
              'type': 'infoField',
              'num': '015',
              'label': '6. Has the corporation wound-up a subsidiary in the preceding taxation year?',
              'labelCellClass': 'indent',
              'inputType': 'radio',
              'init': false
            },
            {
              'label': 'If the answer to question 6 is yes, answer questions 7 and 8. If the answer is no, go to Part 1',
              'labelClass': 'fullLength bold tabbed2'
            },
            {
              'type': 'infoField',
              'num': '016',
              'label': '7. Was the subsidiary neither a CCPC or a DIC during its last taxation year?',
              'labelCellClass': 'indent',
              'inputType': 'radio',
              'canClear': true
            },
            {
              'label': 'If the answer to question 7 is yes, complete Part 6.',
              'labelClass': 'fullLength tabbed2 bold'
            },
            {
              'type': 'infoField',
              'num': '017',
              'label': '8. Was the subsidiary a CCPC or a DIC during its last taxation year?',
              'labelCellClass': 'indent',
              'inputType': 'radio',
              'canClear': true
            },
            {
              'label': 'If the answer to question 8 is yes, complete Part 6.',
              'labelClass': 'fullLength tabbed2 bold'
            }
          ]
        },
        {
          'header': 'Part 1 - Low rate income pool (LRIP)',
          'spacing': 'R_mult_tn2_complete',
          'rows': [
            {
              'label': 'LRIP at the end of the immediately previous tax year (enter \'0\' for the first tax year-ending in 2006)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '100',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'filters': ''
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '100'
                  }
                }
              ]
            },
            {
              'label': 'Income for the credit union deduction (amount E in Part 3 of Schedule 17 of the previous year if the corporation was <b>not</b> a CCPC in the previous tax year, otherwise enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '120',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'filters': ''
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '120'
                  }
                },
                null
              ]
            },
            {
              'label': 'Aggregate investment income of a corporation that has elected under subsection 89(11) not to be a CCPC (line 440 of the T2 return of the previous tax year)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '140',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'filters': ''
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '140'
                  }
                },
                null
              ]
            },
            {
              'type': 'table',
              'num': 'di_table_1'
            },
            {
              'type': 'table',
              'num': 'di_table_2'
            },
            {
              'label': 'Subtotal (<b>add </b> lines 100, 150, and 160)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '190',
                    'disabled': true,
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'filters': ''
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '190'
                  }
                }
              ]
            }
          ]
        },
        {
          'header': 'Part 2 - LRIP and excessive eligible dividend designations during the tax year',
          'rows': [
            {
              'label': 'Complete this part if you paid an eligible dividend in the tax year.',
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '199'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '205',
              'validate': {
                'or': [
                  {
                    'matches': '^[.\\d]{1,13}$'
                  },
                  {
                    'check': 'isEmpty'
                  }
                ]
              }
            },
            {
              label: 'Enter this amount on line G of Schedule 55.'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'description',
              'lines': [
                {
                  'type': 'list',
                  'listType': 'asterisk',
                  'items': [
                    {
                      'label': ' Enter on line 200 each date where: ',
                      'sublist': [
                        'an eligible dividend was paid in the year; or',
                        'an adjustment was made as a result of an amalgamation or the wind-up of a subsidiary or on ceasing to be a CCPC (by an election or otherwise).'
                      ]
                    },
                    {
                      'label': 'Taxable dividends from a corporation resident in Canada (other than eligible dividends) '
                    },
                    {
                      'label': 'Complete the worksheets in Parts 4 to 6 separately for each predecessor, each subsidiary involved in the wind-up, and when the corporation ceases to be a CCPC or DIC. Add up the adjustments for this date and enter on line 220.'
                    },
                    {
                      'label': 'Includes taxable dividends (other than an eligible dividend, a capital gains dividend within the meaning assigned by subsection 130.1(4) or 131(1), or a dividend deductible under subsection 130.1(1))'
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          'header': 'Part 3 - LRIP closing balance',
          'rows': [
            {
              'label': 'Amount from line 190 in Part 1',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '300'
                  }
                }
              ],
              'indicator': 'B'
            },
            {
              'label': 'Cumulative total dividends* receivable in the tax year that are deductible under section 112',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': '',
              'columns': [
                {
                  'input': {
                    'num': '301'
                  }
                },
                null,
                null
              ]
            },
            {
              'label': 'Total dividends other than eligible dividends receivable in the year that are deductible under section 112 ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': '',
              'columns': [
                {
                  'input': {
                    'num': '302'
                  }
                },
                null,
                null
              ]
            },
            {
              'label': 'Total dividends receivable in the tax year that are deductible under section 112',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '510',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'filters': ''
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '510'
                  }
                },
                null
              ]
            },
            {
              'label': 'Total adjustments for amalgamations, wind-ups, or on ceasing to be a CCPC**',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '520',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'filters': ''
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '520'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal (line 510 <b>plus</b> line 520)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '330'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '340'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'C'
            },
            {
              'label': 'Subtotal (amount B <b>plus</b> amount C)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '350'
                  }
                }
              ],
              'indicator': 'D'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Cumulative total dividends**** payable in the tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': '',
              'columns': [
                {
                  'input': {
                    'num': '351'
                  }
                },
                null,
                null
              ]
            },
            {
              'label': 'Total dividends other than eligible dividends payable in the year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': '',
              'columns': [
                {
                  'input': {
                    'num': '352'
                  }
                },
                null,
                null
              ]
            },
            {
              'label': 'Total dividends*** payable in the tax year ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '540',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'filters': ''
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '540'
                  }
                },
                null
              ]
            },
            {
              'label': 'Total excessive eligible dividend designations in the tax year (amount A in Part 2)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '370'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'E'
                  }
                }
              ]
            },
            {
              'label': 'Subtotal (line 540 <b>plus</b> amount E)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '380'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '390'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'F'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'LRIP at the end of the tax year (amount D <b>minus</b> amount F) (if negative enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '590',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'filters': ''
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '590'
                  }
                }
              ]
            },
            {
              'type': 'description',
              'lines': [
                {
                  'type': 'list',
                  'listType': 'asterisk',
                  'items': [
                    {
                      'label': 'Taxable dividends from a corporation resident in Canada (other than eligible dividends) '
                    },
                    {
                      'label': 'Complete the worksheets in Parts 4 to 6 separately for each predecessor, each subsidiary involved in the wind-up, and when the corporation ceases to be a CCPC or DIC. '
                    },
                    {
                      'label': 'Includes taxable dividends (other than an eligible dividend, a capital gains dividend within the meaning assigned by subsection 130.1(4) or 131(1), or a dividend deductible under subsection 130.1(1))'
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          'header': 'Part 4 - Worksheet for adjustment when a corporation ceases to be a CCPC or DIC',
          'showWhen': {
            'fieldId': '011'
          },
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Date of adjustments',
              'labelWidth': '20%',
              'labelClass': 'bold',
              'type': 'infoField',
              'inputType': 'date',
              'num': '400'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Complete this part if the corporation is neither a CCPC nor a DIC in this tax year but was a CCPC or a DIC in the previous tax year. ',
              'labelClass': 'fullLength'
            },
            {
              'label': 'This adjustment to the LRIP can be made at any time in the tax year.',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Keep a copy of this calculation for your records in case we ask to see it later. ',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Cost amount to the corporation of all property immediately before the end of the previous tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '401'
                  }
                }
              ],
              'indicator': '1'
            },
            {
              'label': 'The corporation\'s cash on hand immediately before the end of the previous tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '402'
                  }
                }
              ],
              'indicator': '2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total of subsection 111(1) losses that would have been deductible in computing the corporation\'s taxable income for the previous tax year if the corporation had had unlimited income from each business carried on and each property held and had realized an unlimited amount of capital gains for the previous tax year:',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Non-capital losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '403'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '3'
                  }
                },
                null
              ]
            },
            {
              'label': 'Net capital losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '404'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '4'
                  }
                },
                null
              ]
            },
            {
              'label': 'Farm losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '405'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '5'
                  }
                },
                null
              ]
            },
            {
              'label': 'Restricted farm losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '406'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '6'
                  }
                },
                null
              ]
            },
            {
              'label': 'Limited partnership losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '407'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '7'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal (<b>add</b> amounts 3 to 7)',
              'labelClass': 'text-right ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'labelCellClass': 'alignRight',
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '408'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '409'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '8'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total of all amounts deducted under subsection 111(1) in computing the corporation\'s taxable income for the previous tax year:',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Non-capital losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '410'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '9'
                  }
                },
                null
              ]
            },
            {
              'label': 'Net capital losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '411'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '10'
                  }
                },
                null
              ]
            },
            {
              'label': 'Farm losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '412'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '11'
                  }
                },
                null
              ]
            },
            {
              'label': 'Restricted farm losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '413'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '12'
                  }
                },
                null
              ]
            },
            {
              'label': 'Limited partnership losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '414'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '13'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal (<b>add</b> amounts 9 to 13)',
              'labelClass': 'text-right ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'labelCellClass': 'alignRight',
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '415'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '416'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '14'
                  }
                }
              ]
            },
            {
              'label': 'Unused and unexpired losses at the end of the corporation\'s previous tax year',
              'labelClass': 'fullLength'
            },
            {
              'label': '(amount 8 <b>minus</b> amount 14) (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '417'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '418'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': '15'
            },
            {
              'label': 'Subtotal (<b>add</b> amounts 1, 2, and 15)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '419'
                  }
                }
              ],
              'indicator': '16'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'All of the corporation\'s debts and other obligations to pay that were outstanding immediately before the end of its previous tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '420'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '17'
                  }
                }
              ]
            },
            {
              'label': 'Paid up capital of all the corporation\'s issued and outstanding shares of capital stock immediately before the end of its previous tax year ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '421'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '18'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'All of the corporation\'s reserves deducted in its previous tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '422'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '19'
                  }
                }
              ]
            },
            {
              'label': 'The corporation\'s capital dividend account immediately before the end of its previous tax year if the corporation is <b>not</b> a private corporation in the current tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '423'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '20'
                  }
                }
              ]
            },
            {
              'label': 'The corporation\'s general rate income pool (GRIP) at the end of its previous tax year ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': '',
              'columns': [
                {
                  'input': {
                    'num': '424'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '21'
                  }
                }
              ]
            },
            {
              'label': 'Eligible dividends paid in the previous tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': '',
              'columns': [
                {
                  'input': {
                    'num': '425'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '22'
                  }
                },
                null,
                null
              ]
            },
            {
              'label': 'Excessive eligible dividend designations made in the previous tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': '',
              'columns': [
                {
                  'input': {
                    'num': '426'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '23'
                  }
                },
                null,
                null
              ]
            },
            {
              'label': 'Subtotal (amount 22 <b>minus</b> amount 23) <br>(if negative, enter "0")',
              'labelClass': 'text-right ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'labelCellClass': 'alignRight',
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '427'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '428'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '24'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal (amount 21 <b>minus</b> amount 24) (if negative, use brackets)',
              'labelClass': 'text-right ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'labelCellClass': 'alignRight',
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '429'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '430'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '25'
                  }
                }
              ]
            },
            {
              'label': 'Subtotal (<b>add</b> amounts 17, 18, 19, 20, 25) (if negative, use brackets)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '431'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '432'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': '26'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b>Adjustment for a corporation that ceases to be a CCPC or DIC</b> (amount 16 <b>minus</b> amount 26) (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '433'
                  }
                }
              ],
              'indicator': '27'
            }
          ]
        },
        {
          'header': 'Part 5 - Worksheet for adjustment when a corporation is formed as a result of an amalgamation',
          'showWhen': {
            'or': [
              {
                'fieldId': '013'
              },
              {
                'fieldId': '014'
              }
            ]
          },
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Date of adjustments',
              'labelWidth': '20%',
              'labelClass': 'bold',
              'type': 'infoField',
              'inputType': 'date',
              'num': '500'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Complete this part if the corporation was formed as a result of an amalgamation or merger of two or more corporations, one or more of which is a taxable Canadian corporation. Complete a separate worksheet for <b>each</b> predecessor.',
              'labelClass': 'fullLength'
            },
            {
              'label': 'This adjustment to the LRIP can be made at any time in the tax year',
              'labelClass': 'fulllength'
            },
            {
              'label': 'The last tax year was its tax year that ended immediately before the amalgamation. ',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Keep a copy of this calculation for your records, in case we ask to see it later. ',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'For a predecessor corporation that was a CCPC or a DIC in its tax year that ended immediately before the amalgamation ',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Cost amount to the predecessor of all property immediately before the end of its last tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '501'
                  }
                }
              ],
              'indicator': '1'
            },
            {
              'label': 'The predecessor\'s cash on hand immediately before the end of its last tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '502'
                  }
                }
              ],
              'indicator': '2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total of subsection 111(1) losses that would have been deductible in computing the predecessor\'s taxable income for its last',
              'labelClass': 'fullLength'
            },
            {
              'label': 'tax year if the predecessor had had unlimited income from each business carried on and each property held and had realized',
              'labelClass': 'fullLength'
            },
            {
              'label': 'an unlimited amount of capital gains for its last tax year: ',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Non-capital losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '503'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '3'
                  }
                },
                null
              ]
            },
            {
              'label': 'Net capital losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '504'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '4'
                  }
                },
                null
              ]
            },
            {
              'label': 'Farm losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '505'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '5'
                  }
                },
                null
              ]
            },
            {
              'label': 'Restricted farm losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '506'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '6'
                  }
                },
                null
              ]
            },
            {
              'label': 'Limited partnership losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '507'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '7'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal (<b>add</b> amounts 3 to 7)',
              'labelClass': 'text-right ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'labelCellClass': 'alignRight',
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '508'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '509'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '8'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total of all amounts deducted under subsection 111(1) in computing the predecessor\'s taxable income for its last tax year: ',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Non-capital losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '310'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '9'
                  }
                },
                null
              ]
            },
            {
              'label': 'Net capital losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '511'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '10'
                  }
                },
                null
              ]
            },
            {
              'label': 'Farm losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '512'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '11'
                  }
                },
                null
              ]
            },
            {
              'label': 'Restricted farm losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '513'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '12'
                  }
                },
                null
              ]
            },
            {
              'label': 'Limited partnership losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '514'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '13'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal (<b>add</b> amounts 9 to 13)',
              'labelClass': 'text-right ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'labelCellClass': 'alignRight',
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '515'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '516'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '14'
                  }
                }
              ]
            },
            {
              'label': 'Unused and unexpired losses at the end of the predecessor\'s last tax year',
              'labelClass': 'fullLength'
            },
            {
              'label': '(amount 8 <b>minus</b> amount 14) (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '517'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '518'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': '15'
            },
            {
              'label': 'Subtotal (<b>add</b> amounts 1, 2, and 15)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '519'
                  }
                }
              ],
              'indicator': '16'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'All of the predecessor\'s debts and other obligations to pay that were outstanding immediately before the end of its last tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '320'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '17'
                  }
                }
              ]
            },
            {
              'label': 'Paid up capital of all the predecessor\'s issued and outstanding shares of capital stock immediately before the end of its last tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '521'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '18'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'All of the predecessor\'s reserves deducted in its last tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '522'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '19'
                  }
                }
              ]
            },
            {
              'label': 'The predecessor\'s capital dividend account immediately before the end of its last tax' +
              ' year if the corporation is <b>not</b> a private corporation in its first tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '523'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '20'
                  }
                }
              ]
            },
            {
              'label': 'The predecessor\'s general rate income pool (GRIP) at the end of its last tax year (if negative, use brackets)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': '',
              'columns': [
                {
                  'input': {
                    'num': '524'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '21'
                  }
                }
              ]
            },
            {
              'label': 'Eligible dividends paid in the previous tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': '',
              'columns': [
                {
                  'input': {
                    'num': '525'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '22'
                  }
                },
                null,
                null
              ]
            },
            {
              'label': 'Excessive eligible dividend designations made in the previous tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': '',
              'columns': [
                {
                  'input': {
                    'num': '526'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '23'
                  }
                },
                null,
                null
              ]
            },
            {
              'label': 'Subtotal (amount 22 <b>minus</b> amount 23) (if negative, enter "0")',
              'labelClass': 'text-right ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'labelCellClass': 'alignRight',
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '527'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '528'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '24'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal (amount 21 <b>minus</b> amount 24) (if negative, use brackets)',
              'labelClass': 'text-right ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'labelCellClass': 'alignRight',
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '529'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '530'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '25'
                  }
                }
              ]
            },
            {
              'label': 'Subtotal (<b>add</b> amounts 17, 18, 19, 20, 25) (if negative, use brackets)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '531'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '532'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': '26'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Adjustment for a predecessor corporation that was a CCPC or a DIC in its last tax year' +
              ' (amount 16 <b>minus</b> amount 26) (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '533'
                  }
                }
              ],
              'indicator': '27'
            },
            {
              'label': 'For a predecessor corporation that was neither a CCPC nor a DIC in its tax year that ended immediately before the amalgamation ',
              'labelClass': 'fullLength bold'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'LRIP at the end of its last tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '534'
                  }
                }
              ],
              'indicator': '28'
            },
            {
              'label': '<b>Adjustment for a predecessor corporation involved in an amalgamation</b> (amount 27 <b>plus</b> amount 28)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '535'
                  }
                }
              ],
              'indicator': '29'
            },
            {
              'label': 'Calculate amount 29 for <b>each</b> predecessor.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 6 - Worksheet for adjustment when a corporation has wound-up a subsidiary',
          'showWhen': {
            'or': [
              {
                'fieldId': '017'
              },
              {
                'fieldId': '016'
              }
            ]
          },
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Date of adjustments',
              'labelWidth': '20%',
              'labelClass': 'bold',
              'type': 'infoField',
              'inputType': 'date',
              'num': '600'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Complete this part if the corporation is the parent corporation (parent) that' +
              ' is neither a CCPC nor a DIC in a tax year and has, in the year, received all or substantially' +
              ' all of the assets on dissolution or wind-up of a subsidiary. Complete a separate worksheet for ' +
              'each subsidiary involved in the wind-up',
              'labelClass': 'fullLength'
            },
            {
              'label': 'This adjustment to the parent\'s LRIP can be made at any time in the tax year that is at or after the end of the subsidiary\'s last tax year.',
              'labelClass': 'fullLength'
            },
            {
              'label': 'The last tax year for the subsidiary was its tax year during which its assets were distributed to the parent corporation on the wind-up.',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Keep a copy of this calculation for your records in case we ask to see it later. ',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'For a subsidiary that was a CCPC or a DIC in its last tax year',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Cost amount to the subsidiary of all property immediately before the end of its last tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '601'
                  }
                }
              ],
              'indicator': '1'
            },
            {
              'label': 'The subsidiary\'s cash on hand immediately before the end of its last tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '602'
                  }
                }
              ],
              'indicator': '2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total of subsection 111(1) losses that would have been deductible in computing ' +
              'the subsidiary\'s taxable income for its last tax year if the subsidiary had had unlimited income ' +
              'from each business carried on and each property held and had realized an unlimited ' +
              'amount of capital gains for its last tax year:',
              'labelClass': 'fullLength'
            },
            {
              'label': 'an unlimited amount of capital gains for its last tax year: ',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Non-capital losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '603'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '3'
                  }
                },
                null
              ]
            },
            {
              'label': 'Net capital losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '604'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '4'
                  }
                },
                null
              ]
            },
            {
              'label': 'Farm losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '605'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '5'
                  }
                },
                null
              ]
            },
            {
              'label': 'Restricted farm losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '606'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '6'
                  }
                },
                null
              ]
            },
            {
              'label': 'Limited partnership losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '607'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '7'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal (<b>add</b> amounts 3 to 7)',
              'labelClass': 'text-right ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'labelCellClass': 'alignRight',
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '608'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '609'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '8'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total of all amounts deducted under subsection 111(1) in computing the subsidiary\'s taxable income for the last tax year:',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Non-capital losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '610'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '9'
                  }
                },
                null
              ]
            },
            {
              'label': 'Net capital losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '611'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '10'
                  }
                },
                null
              ]
            },
            {
              'label': 'Farm losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '612'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '11'
                  }
                },
                null
              ]
            },
            {
              'label': 'Restricted farm losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '613'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '12'
                  }
                },
                null
              ]
            },
            {
              'label': 'Limited partnership losses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': 'indent',
              'columns': [
                {
                  'input': {
                    'num': '614'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '13'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal (<b>add</b> amounts 9 to 13)',
              'labelClass': 'text-right ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'labelCellClass': 'alignRight',
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '615'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '616'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '14'
                  }
                }
              ]
            },
            {
              'label': 'Unused and unexpired losses at the end of the subsidiary\'s last tax year',
              'labelClass': 'fullLength'
            },
            {
              'label': '(amount 8 <b>minus</b> amount 14) (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '617'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '618'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': '15'
            },
            {
              'label': 'Subtotal (<b>add</b> amounts 1, 2, and 15)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '619'
                  }
                }
              ],
              'indicator': '16'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'All of the subsidiary\'s debts and other obligations to pay that were outstanding immediately before the end of its last tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '620'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '17'
                  }
                }
              ]
            },
            {
              'label': 'Paid up capital of all the subsidiary\'s issued and outstanding shares of capital stock immediately before the end of its last tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '621'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '18'
                  }
                }
              ]
            },
            {
              'label': 'All of the subsidiary\'s reserves deducted in its last tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '622'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '19'
                  }
                }
              ]
            },
            {
              'label': 'The subsidiary\'s capital dividend account immediately before the end of its last' +
              ' tax year if the parent is <b>not</b> a private corporation in the tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '623'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '20'
                  }
                }
              ]
            },
            {
              'label': 'The subsidiary\'s general rate income pool (GRIP) at the end of its last tax year (if negative, use brackets)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': '',
              'columns': [
                {
                  'input': {
                    'num': '624'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '21'
                  }
                }
              ]
            },
            {
              'label': 'Eligible dividends paid in its last tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': '',
              'columns': [
                {
                  'input': {
                    'num': '625'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '22'
                  }
                },
                null,
                null
              ]
            },
            {
              'label': 'Excessive eligible dividend designations made in its last tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': '',
              'columns': [
                {
                  'input': {
                    'num': '626'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '23'
                  }
                },
                null,
                null
              ]
            },
            {
              'label': 'Subtotal (amount 22 <b>minus</b> amount 23) (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'labelCellClass': '',
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '627'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '628'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '24'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal (amounts 21 <b>minus</b> amounts 24) (if negative, use brackets)',
              'labelClass': 'text-right ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'labelCellClass': 'alignRight',
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '629'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '630'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '25'
                  }
                }
              ]
            },
            {
              'label': 'Subtotal (<b>add</b> amounts, 17, 18, 19, 20, 25) (if negative, use brackets)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '631'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '632'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': '26'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Adjustment for a subsidiary that was a CCPC or a DIC in its last tax year (amount 16 <b>minus</b> amount 26) (if negative, enter "0") .',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '633'
                  }
                }
              ],
              'indicator': '27'
            },
            {
              'label': 'For a subsidiary that was neither a CCPC nor a DIC in its last tax year',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'LRIP at the end of its last tax year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '634'
                  }
                }
              ],
              'indicator': '28'
            },
            {
              'label': '<b>Adjustment for a subsidiary involved in a wind-up</b> (amount 27 <b>plus</b> amount 28)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '635'
                  }
                }
              ],
              'indicator': '29'
            },
            {
              'label': 'Calculate amount 29 for <b>each</b> subsidiary.',
              'labelClass': 'fullLength'
            }
          ]
        }
      ]
    };
})();
