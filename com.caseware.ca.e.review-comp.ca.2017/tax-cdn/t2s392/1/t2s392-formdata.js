(function() {
  'use strict';
  wpw.tax.create.formData('t2s392', {
    formInfo: {
      abbreviation: 't2s392',
      title: 'Manitoba Data Processing Investment Tax Credits',
      //subTitle: '(2013 and later tax years)',
      schedule: 'Schedule 392',
      code: 'Code 1302',
      formFooterNum: 'T2 SCH 392 E (16)',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      category: 'Manitoba Forms'
    },
    sections: [
      {
        'header': 'Data processing centre investment tax credit for operator',
        'rows': [
          {
            'label': '• You can use this schedule to claim a Manitoba <b>data processing centre investment tax credit ' +
            'for operator</b> under subsection 7.19(1) of the <i>Income Tax Act</i> (Manitoba). This refundable credit ' +
            'for eligible data processing centre corporations is equal to 4% of the cost of data processing buildings' +
            ' acquired by purchase or lease, or constructed and 7% of the cost of data processing centre property ' +
            'acquired (by purchase or lease) in the current tax year and before July 1, 2013. The rates increase to' +
            ' 4.5% and 8%, respectively, for property acquired in the current tax year and after June 30, 2013.' +
            ' To claim the data processing centre investment tax credit for operator complete Parts 1 to 3 of ' +
            'this schedule. ',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '• You are an <b>eligible data processing centre</b> corporation if:'
          },
          {
            'label': ' – you are a taxable Canadian corporation with a permanent establishment in Manitoba;',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': ' – your principal activity in Manitoba is data processing; and ',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '– when you are affiliated (according to section 251.1 of the federal <i>Income Tax Act</i>) ' +
            'with one or more corporations with a permanent establishment in Manitoba, your principal activity in ' +
            'Manitoba and that of your affiliated corporations, on a combined basis, is data processing.',
            'labelClass': 'tabbed fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '• A building is a data processing building if it is a prescribed building as defined in subsection 4600(1) of the federal <i>Income Tax Regulations</i> and:',
            'labelClass': 'fullLength'
          },
          {
            'label': '– you acquired it (by purchase or lease) or constructed it after April 17, 2012, and before January 1, 2019;',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '– it was not used, or acquired for use or lease, for any purpose before you acquired it; and',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '– it is situated in Manitoba and is used, or will be used, for the purpose of data processing.',
            'labelClass': 'tabbed fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '• Property is data processing centre property if it is (or would be, if you owned it) defined as prescribed machinery and equipment in subsection 4600(2) of the federal  <i>Income Tax Regulations</i> for the purpose of the definition of <b>qualified property</b> in subsection 127(9) of the federal <i>Income Tax Act</i>or a property included in paragraph (o) of Class 12, in paragraph (c) of Class 17,or in Class 42 or 50 in Schedule II of the federal Regulations and:',
            'labelClass': 'fullLength'
          },
          {
            'label': '– you acquired it (by purchase or lease) or constructed it after April 17, 2012, and before January 1, 2019;',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '– it was never used for any purpose before you acquired it,unless you refurbished it when you acquired it;',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '– it has not been included in calculating any corporation\'s data processing property investment tax credit for any tax year; and',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '• it is situated in Manitoba and is used, or will be used, in connection with operating or maintaining a data processing building.',
            'labelClass': 'tabbed fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '• For data processing centre property, <b>refurbished</b> means that at least 50% of the capital cost of the property is attributable to unused components installed since the property was last used or acquired for use or lease.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '• A credit may be claimed on data processing buildings acquired or constructed, as well as data processing centre property acquired after 2018 if it was acquired as a replacement for qualified property. ',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '• When calculating the data processing centre investment tax credit for operator, an eligible data processing centre corporation cannot include amounts for a building or a property acquired by lease if an amount for that same building or property is included in the calculation of another corporation\'s data processing centre investment tax credit for building lessor.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Data processing property investment tax credit',
        'rows': [
          {
            'label': '• You can also use this schedule to claim a Manitoba data processing property investment tax credit under subsection 7.19(1.1) of the <i>Income Tax Act</i> (Manitoba). This refundable credit for eligible corporations equal to 8% of the cost of data processing property acquired by purchase or lease in the current tax year. To claim the data processing property investment tax credit complete Parts 4 and 5 of this schedule.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '• You are an <b>eligible corporation</b> if you are a taxable Canadian corporation with a permanent establishment in Manitoba.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '• Property is <b>data processing property</b> if it is (or would be, if you owned it)a property included in Class 46 or 50 in Schedule II of the federal <i>Income Tax Regulations</i> and:',
            labelClass: 'fullLength'
          },
          {
            'label': '– you acquired it (by purchase or lease) after April 16, 2013, and before January 1, 2019; ',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '– it was never used for any purpose before you acquired it; ',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '– it is not replacement property, as defined in subsection 7.19(2.1) of the <i>Income Tax Act</i> (Manitoba); ',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '– it has not been included in calculating any corporation\'s data processing centre investment tax credit for any tax year; and ',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '– it is situated in Manitoba and is used, or will be used by you exclusively, or nearly exclusively, for the purpose of data processing. ',
            'labelClass': 'tabbed fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: '• To be eligible for this credit, the amount an eligible corporation invests in purchasing or' +
            ' leasing data processing property for that tax year has to be at least $10 million.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'header': 'Data processing centre investment tax credit for building lessor',
        'rows': [
          {
            'label': '• You can also use this schedule to claim a Manitoba data processing centre investment tax credit for building lessor under subsection 7.19(1.0.1) of the <i>Income Tax Act</i> (Manitoba). The credit is a refundable credit for an eligible corporation that purchases or constructs a building and leases it to another eligible corporation for use by that other corporation as a data processing building throughout the term of the lease. The data processing centre investment tax credit for building lessor is calculated in Part 6 of this schedule.',
            'labelClass': 'fullLength'
          },
          {
            'label': '• To be eligible for this credit, the lessor must:'
          },
          {
            'label': '• lease the building to another eligible corporation that it deals with at arm\'s length;',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '• never have used the building for any purpose before having purchased or built it; and',
            'labelClass': 'tabbed fullLength'
          },
          {
            'label': '• have purchased or built the building after December 31, 2013 and before January 1, 2019.',
            'labelClass': 'tabbed fullLength'
          }
        ]
      },
      {
        'header': 'General information',
        'rows': [
          {
            'label': '• For these credits, data processing means the use of networked computers to centralize the storage, management, dissemination, or hosting of data or information and may include the use of one or more of the systems or equipment, listed under the definition in subsection 7.19(2) of the <i>Income Tax Act</i> (Manitoba), to support the networked computers.',
            'labelClass': 'fullLength'
          },
          {
            'label': '• To claim these credits, the property has to be <b>available for use</b> by you in the tax year, according to subsections 13(27) and 13(28) of the federal <i>Income Tax Act</i>, <b>not including</b> the time just before you dispose of it under paragraphs 13(27)(c) and 13(28)(d).',
            'labelClass': 'fullLength'
          },
          {
            'label': '• File your completed schedule with your <i>T2 Corporation Income Tax Return</i>. You have to file this schedule no later than <b>one year</b> after the filing due date of your T2 Return for the tax year in which you acquired the property to claim these credits.',
            'labelClass': 'fullLength'
          },
          {
            'label': '• These credits are considered government assistance under paragraph 12(1)(x) of the federal <i>Income Tax Act</i> and <b>must</b> be included in income in the tax year the credits are received. These credits are <b>not</b> considered government assistance under section 7.19 of the <i>Income Tax Act</i> (Manitoba) for calculating the credits themselves.',
            'labelClass': 'fullLength'
          },
          {
            'label': '• When calculating your Manitoba data processing investment tax credits for a tax year, do not include any amount you used in claiming a tax credit under any other section of the <i>Income Tax Act</i> (Manitoba).',
            'labelClass': 'fullLength'
          },
          {
            'label': '• A corporation that is a member of a partnership may claim its reasonable share of the credit. The corporation\'s portion is the amount that may reasonably be considered to be the corporation\'s share of the credit. However, a corporation may file, with its T2 return for the tax year, an irrevocable election under subsection 7.19(6.4) of the Act, made by the corporation with written consent of the partnership, to allocate the entire amount of the credit for that tax year to the corporation.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 1 - Data processing building amount for the calculation of the data processing centre investment tax credit for operator',
        'rows': [
          {
            'type': 'table',
            'num': '100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '150'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Total capital and leasing cost of data processing buildings acquired or constructed in the' +
            ' current tax year before July 1, 2013</b> (amount A <b>plus</b> amount B)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': '<b>Total capital and leasing cost of data processing buildings acquired or constructed  ' +
            'in the current tax year after June 30, 2013</b> (amount C <b>plus</b> amount D) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '111',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '111'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* The acquisition date is the date the property became available for use. <br>** When you calculate the capital cost or the leasing cost, <b>deduct</b> the amount of any government assistance received or receivable if it was not already considered in the calculation of the capital cost or the leasing cost. See paragraph 7.19(4)(c) of the <i>Income Tax Act</i> (Manitoba).',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - Data processing centre property amounts for the calculation of the data processing centre investment tax credit for operator',
        'rows': [
          {
            'type': 'table',
            'num': '200'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '250'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Total capital and leasing cost of data processing centre property acquired in the current tax ' +
            'year before July 1, 2013</b> (amount G <b>plus</b> amount H) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '210',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '210'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': '<b>Total capital and leasing cost of data processing centre property acquired  in the ' +
            'current tax year after June 30, 2013</b> (amount I <b>plus</b> amount J)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '211',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '211'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* The acquisition date is the date the property became available for use. <br>** When you calculate the capital cost or the leasing cost, <b>deduct</b> the amount of any government assistance received or receivable if it was not already considered in the calculation of the capital cost or the leasing cost. See paragraph 7.19(4)(c) of the <i>Income Tax Act</i> (Manitoba).',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Data processing centre investment tax credit for operator',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Credit earned for data processing buildings</b>'
          },
          {
            'type': 'table',
            'num': '320'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Credit earned for data processing centre property</b>'
          },
          {
            'type': 'table',
            'num': '330'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit allocated from a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '309',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '309'
                }
              }
            ],
            'indicator': 'Q'
          },
          {
            'label': '<b>Data processing centre investment tax credit for operator</b> (Add amounts M to Q)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '310',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '310'
                }
              }
            ],
            'indicator': 'R'
          }
        ]
      },
      {
        'header': 'Part 4 - Data processing property amounts for the calculation of the data processing property investment tax credit',
        'rows': [
          {
            'type': 'table',
            'num': '400'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total capital cost of data processing property acquired in the current tax year (total of column 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '408'
                }
              }
            ],
            'indicator': 'S'
          },
          {
            'label': 'Total leasing cost of data processing property acquired in the current tax year (total of column 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '409'
                }
              }
            ],
            'indicator': 'T'
          },
          {
            label: '<b>Total capital and leasing cost of data processing property acquired in the current tax year</b>' +
            '<br> (must be at least $10 million) (amount S <b>plus</b> amount T)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '410',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '410'
                }
              }
            ],
            'indicator': 'U'
          },
          {labelClass: 'fullLength'},
          {
            'label': '* The acquisition date is the date the property became available for use',
            'labelClass': 'fullLength'
          },
          {
            label: ' ** When you calculate the capital cost or the leasing cost, <b>deduct</b> the amount of any ' +
            'government assistance received or receivable if it was not already considered in the calculation of ' +
            'the capital cost or the leasing cost. See paragraph 7.19(4.1)(c) of the <i>Income Tax Act</i> (Manitoba).',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 5 - Data processing property investment tax credit',
        'rows': [
          {
            'type': 'table',
            'num': '550'
          },
          {
            'label': 'Credit allocated from a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '499',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '499'
                }
              }
            ],
            'indicator': 'W'
          },
          {
            'label': '<b>Data processing property investment tax credit</b> (amount V <b>plus</b> amount W)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '500',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '500'
                }
              }
            ],
            'indicator': 'X'
          }
        ]
      },
      {
        'header': 'Part 6 - Data processing centre investment tax credit for building lessor',
        'rows': [
          {
            'label': '<b>Data processing property</b>'
          },
          {
            'type': 'table',
            'num': '600'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '650'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* The acquisition date is the date the property became available for use.',
            'labelClass': 'fullLength'
          },
          {
            label: ' ** When you calculate the capital cost, <b>deduct</b> the amount of any government assistance' +
            ' received or receivable for the property if it was not already considered in the calculation of the ' +
            'capital cost. See paragraph 7.19(4.0.1)(b) of the <i>Income Tax Act</i> (Manitoba).',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Data processing building</b>'
          },
          {
            'type': 'table',
            'num': '700'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '750'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Lessor\'s data processing centre investment tax credit amount from data processing centre property (amount Y)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '631'
                }
              }
            ],
            'indicator': 'AA'
          },
          {
            'label': 'Lessor\'s data processing centre investment tax credit amount from data processing buildings (amount Z)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '632'
                }
              }
            ],
            'indicator': 'BB'
          },
          {
            'label': 'Credit allocated from a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '621',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '621'
                }
              }
            ],
            'indicator': 'CC'
          },
          {
            'label': '<b>Data processing centre investment tax credit for building lessor</b> (add amounts AA to CC)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '622',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '622'
                }
              }
            ],
            'indicator': 'DD'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': '* The acquisition date is the date the property became available for use.',
            'labelClass': 'fullLength'
          },
          {
            label: ' ** When you calculate the capital cost, <b>deduct</b> the amount of any government assistance' +
            ' received or receivable for the property if it was not already considered in the calculation of the' +
            ' capital cost. See paragraph 7.19(4.0.1)(b) of the <i>Income Tax Act</i> (Manitoba).',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 7 - Manitoba data processing investment tax credits',
        'rows': [
          {
            'label': 'Data processing centre investment tax credit for operator (amount R from Part 3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '701'
                }
              }
            ],
            'indicator': 'EE'
          },
          {
            'label': 'Data processing property investment tax credit (amount X from Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '702'
                }
              }
            ],
            'indicator': 'FF'
          },
          {
            'label': 'Data processing centre investment tax credit for building lessor (amount DD from Part 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '703'
                }
              }
            ],
            'indicator': 'GG'
          },
          {
            'label': '<b>Manitoba data processing investment tax credits</b> (add amounts EE to GG)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '704'
                }
              }
            ],
            'indicator': 'HH'
          },
          {
            'label': '(enter amount HH on line 324 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>)',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  });
})();


