(function() {
  wpw.tax.create.diagnostics('t2s392', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var capitalLeasingCols = [{errorCode: '3920002', tableNum: '100'}, {errorCode: '3920004', tableNum: '200'},
      {errorCode: '3920006', tableNum: '400'}];
    var ccaClassAndDateCols = [{errorCode: '3920003', tableNum: '100'}, {errorCode: '3920005', tableNum: '200'},
      {errorCode: '3920007', tableNum: '400'}, {errorCode: '3920008', tableNum: '600'},
      {errorCode: '3920009', tableNum: '700'}];

    var ccaClassAndDateCheck = function(diagObject) {
      diagUtils.diagnostic(diagObject.errorCode, common.prereq(common.requireFiled('T2S392'),
          function(tools) {
            var table = tools.field(diagObject.tableNum);
            return tools.checkAll(table.getRows(), function(row) {
              if (tools.field('t2s5.324').isNonZero() && ((diagObject.errorCode == '3920008' || diagObject.errorCode == '3920009') ?
                      row[3].isNonZero() : tools.checkMethod([row[3], row[4]], 'isNonZero') > 0))
                return tools.requireAll([row[0], row[2]], 'isFilled');
              else return true;
            });
          }));
    };

    var capitalLeasingColCheck = function(diagObject) {
      diagUtils.diagnostic(diagObject.errorCode, common.prereq(common.requireFiled('T2S392'),
          function(tools) {
            var table = tools.field(diagObject.tableNum);
            return tools.checkAll(table.getRows(), function(row) {
              if (tools.checkMethod([row[3], row[4]], 'isNonZero') > 1) {
                return tools.requireOne([row[3], row[4]], function() {
                  return this.isZero() || this.isEmpty();
                });
              } else return true;
            });
          }));
    };

    for (var i = 0; i < capitalLeasingCols.length; i++) {
      capitalLeasingColCheck(capitalLeasingCols[i]);
    }
    for (i = 0; i < ccaClassAndDateCols.length; i++) {
      ccaClassAndDateCheck(ccaClassAndDateCols[i]);
    }

    diagUtils.diagnostic('3920001', common.prereq(common.check('t2s5.324', 'isNonZero'), common.requireFiled('T2S392')));

    diagUtils.diagnostic('392.410', common.prereq(common.requireFiled('T2S392'),
        function(tools) {
          return tools.field('410').require(function() {
            return this.isZero() || this.get() > 10000000;
          });
        }));

  });
})();