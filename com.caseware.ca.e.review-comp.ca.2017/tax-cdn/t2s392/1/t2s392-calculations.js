(function() {

  function runSumByKeyCal(calcUtils, tableNum, valColIndex, dateColIndex, trueConditionNum, falseConditionNum) {
    var data = [];
    var field = calcUtils.field;
    field(tableNum).getRows().forEach(function(row) {
      var amount = row[valColIndex].get();
      var date = wpw.tax.date(2013, 7, 1);
      var acquisitionDate = calcUtils.dateCompare.lessThan(row[dateColIndex].get(), date);
      data.push({'date': acquisitionDate, 'amount': amount})
    });
    field(trueConditionNum).assign(calcUtils.sumByKey(data, 'date', true, 'amount'));
    field(falseConditionNum).assign(calcUtils.sumByKey(data, 'date', false, 'amount'));
  }

  wpw.tax.create.calcBlocks('t2s392', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field) {

      //Part 1
      runSumByKeyCal(calcUtils, '100', 3, 2, '105', '107');
      runSumByKeyCal(calcUtils, '100', 4, 2, '106', '108');
    });

    calcUtils.calc(function(calcUtils, field) {

      //Part 2
      runSumByKeyCal(calcUtils, '200', 3, 2, '205', '207');
      runSumByKeyCal(calcUtils, '200', 4, 2, '206', '208');

      calcUtils.sumBucketValues('210', ['205', '206']);
      calcUtils.sumBucketValues('211', ['207', '208']);
    });

    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('311', 'ratesMb', '140');
      calcUtils.getGlobalValue('312', 'ratesMb', '141');
      calcUtils.getGlobalValue('313', 'ratesMb', '142');
      calcUtils.getGlobalValue('314', 'ratesMb', '143');

      //Part 3

      field('300').assign(field('302').get() * field('311').get() / 100);
      field('301').assign(field('303').get() * field('312').get() / 100);
      field('305').assign(field('307').get() * field('313').get() / 100);
      field('306').assign(field('308').get() * field('314').get() / 100);
      calcUtils.sumBucketValues('310', ['300', '301', '305', '306', '309']);
    });

    calcUtils.calc(function(calcUtils, field) {

      //Part 4
      field('408').assign(field('405').get());
      field('409').assign(field('406').get());
      calcUtils.sumBucketValues('410', ['408', '409']);
    });

    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('501', 'ratesMb', '144');

      //Part 5
      field('497').assign(field('410').get());
      field('498').assign(field('497').get() * field('501').get() / 100);
      calcUtils.sumBucketValues('500', ['498', '499']);
    });

    calcUtils.calc(function(calcUtils, field) {

      //Part 6
      field('600').getRows().forEach(function(row, rowIndex) {
        var table650Row = field('650').getRow(rowIndex);
        table650Row[0].assign(row[3].get() * 0.0267);
        table650Row[1].assign(table650Row[0].get() * 3);
        table650Row[3].assign(Math.max(table650Row[1].get() - table650Row[2].get(), 0));
        table650Row[4].assign((Math.min(table650Row[0].get(), table650Row[3].get())));
      });
      field('631').assign(field('613').get());
      field('632').assign(field('630').get());
      calcUtils.sumBucketValues('622', ['631', '632', '621']);

      field('700').getRows().forEach(function(row, rowIndex) {
        var table750Row = field('750').getRow(rowIndex);
        table750Row[1].assign(row[3].get() * 0.045 / table750Row[0].get());
        table750Row[2].assign(row[3].get() * 0.045);
        table750Row[4].assign(Math.max(table750Row[2].get() - table750Row[3].get(), 0));
        table750Row[5].assign((Math.min(table750Row[1].get(), table750Row[4].get())));
      });

    });

    calcUtils.calc(function(calcUtils, field) {

      //Part 7
      field('701').assign(field('310').get());
      field('702').assign(field('500').get());
      field('703').assign(field('622').get());
      calcUtils.sumBucketValues('704', ['701', '702', '703']);
    });

  })
})();
