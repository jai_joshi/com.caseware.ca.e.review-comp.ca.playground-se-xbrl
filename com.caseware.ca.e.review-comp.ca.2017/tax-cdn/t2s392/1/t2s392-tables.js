(function() {

  var part3Cols = [
    {
      cellClass: 'alignCenter',
      type: 'none'
    },
    {
      colClass: 'std-input-width',
      type: 'none'
    },
    {
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-padding-width',
      type: 'none'
    },
    {
      colClass: 'small-input-width'
    },
    {
      colClass: 'std-padding-width',
      type: 'none'
    },
    {
      colClass: 'std-padding-width',
      type: 'none'
    },
    {
      colClass: 'std-input-width',
      cellClass: 'alignRight'
    },
    {
      colClass: 'std-padding-width',
      type: 'none'
    }
  ];

  wpw.tax.create.tables('t2s392', {
    //Part 1
    '100': {
      maxLoop: 100000,
      showNumbering: true,
      hasTotals: true,
      columns: [
        {
          'header': 'CCA Class',
          cellClass: 'alignCenter',
          type: 'selector',
          width: '105px',
          tn: '101',
          selectorOptions: {
            title: 'CCA Classes',
            items: wpw.tax.codes.ccaClasses,
            hideKeys: true
          }
        },
        {
          header: '2 <br>Description of data processing building acquired (by purchase or lease) ' +
          'or constructed in the current tax year',
          type: 'text'
        },
        {
          header: '3 <br>Acquisition date*',
          width: '150px',
          type: 'date',
          canSort: true,
          tn: '102'
        },
        {
          header: '4 <br>Capital cost**',
          width: '135px',
          total: true,
          totalNum: '109',
          tn: '103', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
        },
        {
          header: '5 <br>Leasing cost**',
          width: '135px',
          total: true,
          totalNum: '112',
          tn: '104', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
        }]
    },
    '150': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'none'},
        {
          type: 'none',
          width: '20px',
          cellClass: 'alignRight'
        },
        {colClass: 'std-input-width'},
        {colClass: 'std-padding-width', type: 'none'},
        {colClass: 'std-input-width'},
        {colClass: 'std-padding-width', type: 'none'}
      ],
      cells: [{
        0: {
          label: 'Totals for buildings acquired or constructed before July 1, 2013'
        },
        1:{
          label: 'A'
        },
        2: {
          num: '105'
        },
        3:{
          label: 'B'
        },
        4: {
          num: '106'
        }
      },
        {
          0: {
            label: 'Totals for buildings acquired or constructed after June 30, 2013'
          },
          1:{
            label: 'C'
          },
          2: {
            num: '107'
          },
          3:{
            label: 'D'
          },
          4: {
            num: '108'
          }
        }]
    },
    //Part 2
    '200': {
      maxLoop: 100000,
      showNumbering: true,
      hasTotals: true,
      columns: [
        {
          'header': 'CCA Class',
          cellClass: 'alignCenter',
          type: 'selector',
          width: '105px',
          tn: '201',
          selectorOptions: {
            title: 'CCA Classes',
            items: wpw.tax.codes.ccaClasses,
            hideKeys: true
          }
        },
        {
          header: '2 <br>Description of data processing centre property acquired (by purchase or lease) ' +
          'in the current tax year',
          type: 'text'
        },
        {
          header: '3 <br>Acquisition date*',
          width: '150px',
          type: 'date',
          canSort: true,
          tn: '202', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: '4 <br>Capital cost**',
          width: '135px',
          total: true,
          totalNum: '209',
          tn: '203', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: '5 <br>Leasing cost**',
          width: '135px',
          total: true,
          totalNum: '212',
          tn: '204', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        }]
    },
    '250': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {type: 'none'},
        {
          type: 'none',
          width: '20px',
          cellClass: 'alignRight'
        },
        {colClass: 'std-input-width'},
        {colClass: 'std-padding-width', type: 'none'},
        {colClass: 'std-input-width'},
        {colClass: 'std-padding-width', type: 'none'}
      ],
      cells: [{
        0: {
          label: 'Totals for property acquired before July 1, 2013'
        },
        1:{
          label: 'G'
        },
        2: {
          num: '205'
        },
        3:{
          label: 'H'
        },
        4: {
          num: '206'
        }
      },
        {
          0: {
            label: 'Totals for property acquired after June 30, 2013'
          },
          1:{
            label: 'I'
          },
          2: {
            num: '207'
          },
          3:{
            label: 'J'
          },
          4: {
            num: '208'
          }
        }]
    },
    //Part 3
    '320': {
      infoTable: true,
      fixedRows: true,
      columns: part3Cols,
      cells: [
        {
          0: {
            label: 'Credit earned in the current tax year before July 1, 2013'
          },
          1: {
            label: 'Amount E'
          },
          2: {
            num: '302'
          },
          3: {
            label: 'x'
          },
          4: {
            num: '311'
          },
          5: {
            label: '%='
          },
          6: {
            tn: '300'
          },
          7: {
            num: '300', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          8: {
            label: 'M'
          }
        },
        {
          0: {
            label: 'Credit earned in the current tax year after June 30, 2013'
          },
          1: {
            label: 'Amount F'
          },
          2: {
            num: '303'
          },
          3: {
            label: 'x'
          },
          4: {
            num: '312',
            decimals: '1'
          },
          5: {
            label: '%='
          },
          6: {
            tn: '301'
          },
          7: {
            num: '301', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          8: {
            label: 'N'
          }
        }]
    },
    '330': {
      infoTable: true,
      fixedRows: true,
      columns: part3Cols,
      cells: [
        {
          0: {
            label: 'Credit earned in the current tax year before July 1, 2013'
          },
          1: {
            label: 'Amount K'
          },
          2: {
            num: '307'
          },
          3: {
            label: 'x'
          },
          4: {
            num: '313'
          },
          5: {
            label: '%='
          },
          6: {
            tn: '305'
          },
          7: {
            num: '305',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          8: {
            label: 'O'
          }
        },
        {
          0: {
            label: 'Credit earned in the current tax year after June 30, 2013'
          },
          1: {
            label: 'Amount L'
          },
          2: {
            num: '308'
          },
          3: {
            label: 'x'
          },
          4: {
            num: '314'
          },
          5: {
            label: '%='
          },
          6: {
            tn: '306'
          },
          7: {
            num: '306',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          8: {
            label: 'P'
          }
        }]
    },
    //Part 4
    '400': {
      maxLoop: 100000,
      showNumbering: true,
      hasTotals: true,
      columns: [
        {
          'header': 'CCA Class',
          cellClass: 'alignCenter',
          type: 'selector',
          width: '105px',
          tn: '401',
          selectorOptions: {
            title: 'CCA Classes',
            items: wpw.tax.codes.ccaClasses,
            hideKeys: true
          }
        },
        {
          header: '2 <br>Description of data processing property acquired (by purchase or lease) ' +
          'in the current tax year',
          type: 'text'
        },
        {
          header: '3 <br>Acquisition date*',
          width: '150px',
          type: 'date',
          canSort: true,
          tn: '402'
        },
        {
          header: '4 <br>Capital cost**',
          width: '135px',
          total: true,
          totalNum: '405',
          tn: '403', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: '5 <br>Leasing cost**',
          width: '135px',
          total: true,
          totalNum: '406',
          tn: '404', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        }]
    },
    //Part 5
    '550': {
      infoTable: true,
      fixedRows: true,
      columns: part3Cols,
      cells: [
        {
          0: {
            label: 'Credit earned in the current tax year'
          },
          1: {
            label: 'Amount U'
          },
          2: {
            num: '497'
          },
          3: {
            label: 'x'
          },
          4: {
            num: '501'
          },
          5: {
            label: '%='
          },
          6: {
            tn: '498'
          },
          7: {
            num: '498'
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          8: {
            label: 'V'
          }
        }]
    },
    //Part 6
    '600': {
      maxLoop: 100000,
      showNumbering: true,
      repeats: ['650'],
      columns: [
        {
          'header': 'CCA Class',
          cellClass: 'alignCenter',
          type: 'selector',
          width: '105px',
          tn: '601',
          selectorOptions: {
            title: 'CCA Classes',
            items: wpw.tax.codes.ccaClasses,
            hideKeys: true
          }
        },
        {
          header: '2 <br>Description of data processing centre property leased to an eligible corporation' +
          ' in the current tax year',
          type: 'text'
        },
        {
          header: '3 <br>Acquisition date*',
          width: '150px',
          type: 'date',
          canSort: true,
          tn: '602', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: '4 <br>Capital cost**',
          width: '135px',
          tn: '603', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        }
      ]
    },
    '650': {
      showNumbering: true,
      maxLoop: 100000,
      fixedRows: true,
      keepButtonsSpace: true,
      hasTotals: true,
      columns: [
        {
          header: '5 <br>Column 4 <b>multiplied</b> by 2.67%',
          tn: '608', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: '6 <br>Column 5 <b>multiplied</b> by 3',
          tn: '609', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: '7 <br>Total of the tax credit claimed for the property in each previous year',
          tn: '610', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: '8 <br>Column 6 <b>minus</b> column 7',
          tn: '611', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: '9 <br><b>Lesser</b> of column 5 and column 8',
          tn: '612', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          total: true,
          totalNum: '613',
          totalIndicator: 'Y'
        }
      ]
    },
    '700': {
      maxLoop: 100000,
      showNumbering: true,
      repeats: ['750'],
      columns: [
        {
          'header': 'CCA Class',
          cellClass: 'alignCenter',
          type: 'selector',
          width: '105px',
          tn: '604',
          selectorOptions: {
            title: 'CCA Classes',
            items: wpw.tax.codes.ccaClasses,
            hideKeys: true
          }
        },
        {
          header: '2 <br>Description of data processing centre building leased to an eligible corporation' +
          ' in the current tax year',
          type: 'text'
        },
        {
          header: '3 <br>Acquisition date*',
          width: '150px',
          type: 'date',
          canSort: true,
          tn: '605', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: '4 <br>Capital cost**',
          width: '135px',
          tn: '606', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        }
      ]
    },
    '750': {
      showNumbering: true,
      maxLoop: 100000,
      hasTotals: true,
      fixedRows: true,
      keepButtonsSpace: true,
      columns: [
        {
          header: '5 <br>Term of the lease in years',
          tn: '614', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: '6 <br>4.5% <b>multiplied</b> by column 4 <b>divided</b> by column 5',
          tn: '615', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: '7 <br>Column 4 <b>multiplied</b> by 4.5%',
          tn: '617', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: '8 <br>Total of the tax credit claimed for the building in each previous year',
          tn: '618', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: '9 <br>Column 7 <b>minus</b> column 8',
          tn: '619', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
        },
        {
          header: '10 <br><b>Lesser</b> of column 6 and column 9',
          tn: '620', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          total: true,
          totalNum: '630',
          totalIndicator: 'Z'
        }
      ]
    }
  })
}());
