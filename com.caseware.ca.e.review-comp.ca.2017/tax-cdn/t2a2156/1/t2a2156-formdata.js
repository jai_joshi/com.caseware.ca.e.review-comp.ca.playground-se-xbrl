(function () {
  wpw.tax.create.formData('t2a2156', {
    formInfo: {
      abbreviation: 't2a2156',
      title: 'Notice Of Revocation Of Waiver',
      formFooterNum: 'AT2156 (Jul-12)',
      // headerImage: 'canada-alberta',
      showCorpInfo: true,
      category: 'Alberta Tax Forms',
      showDots: false
    },
    sections: [
      {
        rows: [
          {
            label: 'Name of Corporation (please print)',
            type: 'infoField',
            inputClass: 'std-input-col-width-2',
            num: '001'
          },
          {
            type: 'infoField',
            inputType: 'custom',
            format: ['{N10}'],
            label: 'Alberta Corporate Account Number',
            num: '002'
          },
          {
            label: '(enter your 9 or 10 digit account number)'
          },
          {
            'type': 'infoField',
            'inputType': 'address',
            'add1Num': '011',
            'add2Num': '012',
            'provNum': '016',
            'cityNum': '015',
            'countryNum': '017',
            'postalCodeNum': '018'
          },
          {
            label: 'Date on which the relevant waiver was signed',
            labelClass: 'center',
            type: 'infoField',
            inputType: 'date'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            label: '&#9679; For use by a taxpayer to revoke, pursuant to section 43(3) of the Alberta Corporate Tax Act, a waiver (form AT184) in\n' +
            'respect of the time limit for assessment, reassessment or determination.',
            labelClass: 'fullLength'
          },
          {
            label: '&#9679; A separate Notice of Revocation of Waiver must be completed for each waiver to be revoked and must be forwarded,\n' +
            'together with a copy of the relevant waiver, to TAX AND REVENUE ADMINISTRATION, 9811 109 ST, EDMONTON AB\n' +
            'T5K 2L5. Fax 780-422-5284.',
            labelClass: 'fullLength'
          },
          {
            label: '&#9679; The waiver will be revoked on the day that is six months after the date the Notice of Revocation is filed. Tax and\n' +
            'Revenue Administration will insert the date of filing on this Notice and return a copy to the corporation.',
            labelClass: 'fullLength'
          },
          {
            label: '&#9679; A Notice of Revocation of Waiver cannot be rescinded or cancelled once it has been filed.',
            labelClass: 'fullLength'
          },
          {
            label: '&#9679; The Notice of Revocation of Waiver must be signed by an officer of the corporation who has authority to bind the\n' +
            'corporation.',
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            labelClass: 'fullLength'
          }
        ]
      },
      {
        rows: [
          {
            type: 'infoField',
            label: 'I hereby request revocation of the attached waiver (form AT184) for the taxation year',
            num: '201'
          },
          {
            'type': 'multiColumn',
            'dividers': [
              false
            ],
            'columns': [
              [
                {
                  type: 'infoField'
                },
                {
                  type: 'infoField',
                  inputType: 'none',
                  label: 'Signature of Authorized Signing Officer',
                  labelClass: 'center'
                }
              ],
              [
                {
                  type: 'infoField',
                  num: '202'
                },
                {
                  type: 'infoField',
                  inputType: 'none',
                  label: 'Position or Office',
                  labelClass: 'center'
                }
              ],
              [
                {
                  type: 'infoField',
                  inputType: 'date',
                  num: '203'
                },
                {
                  type: 'infoField',
                  inputType: 'none',
                  label: 'Date',
                  labelClass: 'center'
                }
              ]
            ]
          }
        ]
      },
      {
        rows: [
          {
            label: 'FOR COMPLETION BY TAX AND REVENUE ADMINISTRATION',
            labelClass: 'center italic'
          },
          {
            label: 'Date on which the Notice of Revocation of Waiver was filed:',
            labelClass: 'center',
            type: 'infoField'
          }
        ]
      }
    ]
  });
})();
