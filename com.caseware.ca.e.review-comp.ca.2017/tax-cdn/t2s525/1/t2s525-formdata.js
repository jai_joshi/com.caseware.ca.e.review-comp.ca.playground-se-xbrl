(function() {
  'use strict';

  wpw.tax.global.formData.t2s525 = {
    formInfo: {
      abbreviation: 'T2S525',
      title: 'Ontario Political Contributions Tax Credit',
      //subTitle: '(2014 and later tax years)',
      schedule: 'Schedule 525',
      code: 'Code 1701',
      formFooterNum: 'T2 SCH 525 E (17)',
      headerImage: 'canada-federal',
      showCorpInfo: true,
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if you are a corporation and you want to:',
              sublist: [
                'calculate an Ontario political contributions tax credit (OPCTC) under section 53.2 of the ' +
                '<i>' + ' Taxation Act, 2007 ' + '</i>' + ' (Ontario)',
                'claim an OPCTC for eligible contributions made in the tax year and before January 1, 2017, or for ' +
                'unused eligible contributions carried forward from any of the previous 20 tax years to reduce Ontario' +
                ' corporate income tax payable'
              ]
            },
            {
              label: 'The OPCTC is a non-refundable tax credit that is calculated by <b>multiplying</b>' +
              ' the corporation\'s Ontario basic rate of tax (calculated in Part 1 of Schedule 500, ' +
              '<i>Ontario Corporation Tax Calculation</i>) by the eligible contributions made to a <b>registered ' +
              'candidate</b>, a <b>registered constituency association</b>, or a <b>registered party</b>.' +
              'These terms are defined in the <i>Election Finances Act</i> (Ontario).',
            },
            {
              label: 'File this schedule with your ' + '<i>' + ' T2 Corporation Income Tax Return ' + '</i>'
            }
          ]
        }
      ],
      category: 'Ontario Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Eligible contribution balance at the end of the tax year',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible contribution balance at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '099'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'A'
                }
              }
            ]
          },
          {
            'label': '(enter the amount from line 190 of Schedule 525 from the previous tax year, if applicable)'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Unused eligible contributions expired after 20 tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '100',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible contribution balance at the beginning of the tax year (amount A <b> minus </b> amount B)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible contributions for the current tax year (made before January 1, 2017)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '120',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible contribution balance available (amount C <b> plus </b> amount D)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '130'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '140'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible contributions used in the current tax year (amount M from Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '150'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Eligible contribution balance at the end of the tax year </b> (amount E <b> minus </b> amount F)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '190',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '190'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        forceBreakAfter: true,
        'header': 'Part 2 - Calculation of current year OPCTC',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible contribution balance available (amount E from Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '200'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'H'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '205'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Ontario corporate income tax payable before OPCTC, Ontario research and development tax credit, Ontario corporate minimum tax credit, and any Ontario refundable tax credit **',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '230'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'J'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Maximum allowable current year OPCTC (lesser of amounts I and J)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '240',
                  'decimals': 2
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'K'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'OPCTC claimed (cannot exceed amount K)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '250',
                  'decimals': 2
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': 'Enter amount L on line 415 of Schedule 5, <i> Tax Calculation Supplementary- Corporations </i>.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '260'
          },
          {
            'label': 'Enter at amount F in Part 1.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Enter the rate calculated in Part 1 of Schedule 500.'
          },
          {
            'label': '**Enter the result of amount C6 <b>minus</b> the total of amounts from lines 404 to 410 from Schedule 5.'
          }
        ]
      },
      {
        'header': 'Political contributions that can be carried forward over 20 years Summary',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1001'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Note that this political contribution expires only at the beginning of fiscal period',
            'labelClass': 'fullLength'
          },
          {
            'label': '** Note that this political contribution expires only at the beginning of the subsequent fiscal period.',
            'labelClass': 'fullLength'
          }
        ]
      },
    ]
  };
})();
