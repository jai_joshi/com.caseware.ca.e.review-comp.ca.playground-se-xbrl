(function() {
  wpw.tax.global.tableCalculations.t2s525 = {
    '205': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          'type': 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width'
        },
        {
          cellClass: 'alignCenter',
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-col-width',
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          colClass: 'std-spacing-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': '(Lesser of $19,950 and amount H)'
          },
          '1': {
            'num': '210'
          },
          '2': {
            'label': 'x'
          },
          '3': {
            'label': 'Ontario basic rate of tax *'
          },
          '4': {
            'num': '215',
            decimals: 1
          },
          '5': {
            'label': '%'
          },
          '7': {
            'label': '='
          },
          '8': {
            'num': '220',
            decimals: 2
          },
          '9': {
            'label': 'I',
            filters: 'number'
          }
        }]
    },
    '260': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-col-width',
          'type': 'none'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-spacing-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }],
      'cells': [
        {
          '0': {
            'label': 'Eligible contributions used: OPCTC claimed (amount L)'
          },
          '1': {
            'num': '265',
            decimals: 2
          },
          '2': {
            'label': '÷'
          },
          '3': {
            'label': 'Ontario basic rate of tax*'
          },
          '4': {
            'num': '270',
            decimals: 1
          },
          '5': {
            'label': '%'
          },
          '7': {
            'label': '='
          },
          '8': {
            cellClass: 'doubleUnderline',
            'num': '275'
          },
          '9': {
            'label': 'M'
          }
        }]
    },
    '1001': {
      'fixedRows': true,
      'infoTable': true,
      hasTotals: true,
      'columns': [
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Year of origin',
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          header: 'Opening balance',
          'total': true,
          'totalNum': '3198',
          'totalMessage': 'Totals : '
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          header: 'Current year contributions',
          'total': true,
          'totalNum': '3199',
          'totalMessage': ' ',
          'disabled': true
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        },
        {
          header: 'Applied',
          'disabled': true,
          'total': true,
          'totalMessage': ' ',
          'totalNum': '3299'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        },
        {
          header: 'Balance to carry forward',
          'disabled': true,
          'total': true,
          'totalMessage': ' ',
          'totalNum': '3350'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '4': {
            'label': '*'
          },
          '5': {
            'type': 'none'
          },
          '7': {
            'type': 'none'
          },
          '9': {
            'type': 'none',
            label: 'N/A'
          }
        },
        {
          '5': {
            'type': 'none'
          },
          '10': {
            'label': '**'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '5': {
            'type': 'none'
          }
        },
        {
          '3': {
            'type': 'none',
            disabled: true
          }
        }
      ]
    }
  }
})();
