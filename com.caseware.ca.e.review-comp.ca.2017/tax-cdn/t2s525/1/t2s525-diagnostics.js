(function() {
  wpw.tax.create.diagnostics('t2s525', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('5250005', common.prereq(common.check(['t2s5.415'], 'isNonZero'), common.requireFiled('T2S525')));
  });
})();
