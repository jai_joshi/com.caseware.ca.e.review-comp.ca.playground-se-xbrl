(function() {

  wpw.tax.create.calcBlocks('t2s525', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      field('1001').getRows().forEach(function(row, index) {
        if (index != 0) {
          row[1].assign(form('tyh').field('200').cell(index - 1, 6).get());
        }
        row[8].assign(
            row[2].get() +
            row[4].get() -
            row[6].get()
        );
      });
      if (field('1001').cell(1, 1).get()) {
        field('1001').cell(0, 1).assign(calcUtils.addToDate(field('1001').cell(1, 1).get(), 0, 0, -1));
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      /* Part 1 */
      calcUtils.equals('099', '3198');
      field('100').assign(field('1001').cell(0, 3).get())
      calcUtils.subtract('110', '099', '100');
      calcUtils.sumBucketValues('130', ['110', '120']);
      calcUtils.equals('140', '130');
      calcUtils.equals('150', '275');
      calcUtils.subtract('190', '140', '150');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      /* Part 2 */
      calcUtils.equals('200', '140');
      if (field('200').get() >= 19950) {
        field('210').assign(19950);
      }
      else {
        calcUtils.equals('210', '200');
      }

      calcUtils.getGlobalValue('215', 'T2S500', '175');
      calcUtils.multiply('220', ['210', '215'], 1 / 100);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var lineC6 = form('T2S5').field('299').get();
      var line404 = form('T2S5').field('404').get();
      var line406 = form('T2S5').field('406').get();
      var line408 = form('T2S5').field('408').get();
      var line410 = form('T2S5').field('410').get();
      // var line414 = form('T2S5').field('414').get();

      field('230').assign(lineC6 - line404 - line406 - line408 - line410);
      field('230').source(form('T2S5').field('299'));
      calcUtils.min('240', ['220', '230']);
      calcUtils.equals('250', '240');
      calcUtils.equals('265', '250');
      calcUtils.equals('270', '215');
      calcUtils.divide('275', '265', '270', false, 100);
    });
  });
})();
