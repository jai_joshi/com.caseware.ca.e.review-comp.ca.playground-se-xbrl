(function() {
  wpw.tax.create.diagnostics('t2s14', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0140001', common.prereq(common.check(['t2j.164'], 'isChecked'), common.requireFiled('T2S14')));

    diagUtils.diagnostic('0140002', {
      label: 'For each and every occurrence of the name of the recipient in column 014100, ' +
      'there must be a corresponding address in column 014200, and at least one entry at ' +
      'either column 014300, 014400, 014500, 014600, or 014700.'
    }, common.prereq(common.requireFiled('T2S14'),
        function(tools) {
          var table = tools.field('001');
          return tools.checkAll(table.getRows(), function(row) {
            var cols1 = [row[0], row[1]];
            var cols2 = [row[2], row[3], row[4], row[5], row[6]];
            if (tools.checkMethod(cols1, 'isFilled'))
              return tools.requireAll(cols1, 'isNonZero') && tools.requireOne(cols2, 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0140002', {
      label: 'For each and every occurrence at either column 014300, 014400, 014500, 014600, or 014700, ' +
      'there must be a name and address of recipient in columns 014100 and 014200.'
    }, common.prereq(common.requireFiled('T2S14'),
        function(tools) {
          var table = tools.field('001');
          return tools.checkAll(table.getRows(), function(row) {
            var cols1 = [row[0], row[1]];
            if (tools.checkMethod([row[2], row[3], row[4], row[5], row[6]], 'isNonZero'))
              return tools.requireAll(cols1, 'isNonZero');
            else return true;
          });
        }));
  });
})();

