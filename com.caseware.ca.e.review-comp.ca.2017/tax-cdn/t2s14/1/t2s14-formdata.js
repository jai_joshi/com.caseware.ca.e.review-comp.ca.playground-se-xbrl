(function() {
  'use strict';

  wpw.tax.global.formData.t2s14 = {
    formInfo: {
      abbreviation: 'T2S14',
      title: 'Miscellaneous Payments to Residents',
      dynamicFormWidth: true,
      schedule: 'Schedule 14',
      formFooterNum: 'T2 SCH 14 (99)',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            'This schedule must be completed by all corporations who made the following payments to residents of Canada:<br>' +
            'royalties for which the corporation has not filed a T5 slip; research and development fees; management fees;' +
            ' technical assistance fees; and similar payments.',
            'Please enter the name and address of the recipient and the amount of the payment in the applicable column.' +
            ' If several payments of the same type (i.e., management fees) were made to the same person, enter the total' +
            ' amount paid. If similar types of payments have been made, but do not fit into any of the categories,' +
            ' enter these amounts in the column entitled "Similar payments".'
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            type: 'table', num: '001'
          }
        ]
      }
    ]
  };
})();
