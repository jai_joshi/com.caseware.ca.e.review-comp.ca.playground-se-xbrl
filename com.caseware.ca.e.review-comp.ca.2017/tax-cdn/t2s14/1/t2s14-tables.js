(function() {
  wpw.tax.global.tableCalculations.t2s14 = {
    "001": {
      "showNumbering": true,
      "columns": [{
        "header": "Name of recipient<br>(please print)",
        "num": "100", "validate": {"or":[{"length":{"min":"1","max":"175"}},{"check":"isEmpty"}]},
        "tn": "100",
        "width": "22%",
        type: 'text'
      },
        {
          "header": "Address of recipient",
          "num": "200",
          "tn": "200",
          "width": "33%",
          "type": "address"
        },
        {
          "header": "Royalties",
          "num": "300",
          "tn": "300",
          "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        },
        {
          "header": "Research and development fees",
          "num": "400",
          "tn": "400",
          "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        },
        {
          "header": "Management fees",
          "num": "500",
          "tn": "500",
          "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        },
        {
          "header": "Technical assistance fees",
          "num": "600",
          "tn": "600",
          "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        },
        {
          "header": "Similar payments",
          "num": "700",
          "tn": "700",
          "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        }]
    }
  }
})();
