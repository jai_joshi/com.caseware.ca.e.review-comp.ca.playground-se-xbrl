(function() {

  function getTableTaxInc(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {type: 'none', colClass: 'std-padding-width'},
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {

          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '2': {num: labelsObj.num[0]},
          '3': {label: 'x'},
          '4': {
            label: labelsObj.label[1],
            labelClass: 'center',
            cellClass: 'singleUnderline'
          },
          '6': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '7': {label: '='},
          '8': {num: labelsObj.num[2]},
          '9': {label: labelsObj.indicator || ''}
        },
        {
          '2': {type: 'none'},
          '4': {
            label: labelsObj.label[2],
            labelClass: 'center fullLength'
          },
          '6': {num: labelsObj.num[3]},
          '8': {type: 'none'}
        }
      ]
    }
  }

  function getTableRate(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        }
      ],
      cells: [
        {
          '0': {
            label: labelsObj.label[0]
          },
          1: {
            num: labelsObj.num[0]
          },
          '2': {
            label: 'x'
          },
          '3': {
            label: labelsObj.label[1],
            cellClass: 'center singleUnderline'
          },
          5: {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '6': {
            label: '='
          },
          7: {
            num: labelsObj.num[2]
          },
          '8': {
            label: labelsObj.indicator
          }
        },
        {
          '1': {
            type: 'none'
          },
          '3': {
            label: labelsObj.label[2]
          },
          '5': {
            num: labelsObj.num[3]
          },
          '7': {
            type: 'none'
          }
        }
      ]
    }
  }

  function getSimpleTableRate(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignLeft'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {
            label: labelsObj.label[0]
          },
          '1': {
            label: 'x',
            labelClass: 'center'
          },
          2: {
            num: labelsObj.num[0]
          },
          '3': {
            label: '%='
          },
          4: {
            num: labelsObj.num[1]
          },
          '5': {
            label: labelsObj.indicator
          }
        }
      ]
    }
  }

  wpw.tax.global.tableCalculations.t2s346 = {
    '3000': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      cells: [
        {
          '0': {
            label: 'Amount from line 427 of the T2 return'
          },
          1: {
            num: '3001'
          },
          '2': {
            label: ' x '
          },
          3: {
            num: '3002',
            cellClass: 'singleUnderline'
          },
          '4': {
            label: '='
          },
          5: {
            num: '3003',
            cellClass: 'doubleUnderline'
          },
          '6': {
            label: 'D1'
          }
        },
        {
          '1': {
            type: 'none'
          },
          3: {
            num: '3004'
          },
          '5': {
            type: 'none'
          }
        }
      ]
    },
    '4000': getTableTaxInc({
      label: ['Amount E1', 'Taxable income for Nova Scotia *', 'Taxable income for all provinces ***'],
      indicator: 'F1',
      num: [4001, 4002, 4003, 4004]
    }),
    '5000': getTableTaxInc({
      label: ['Amount E2', 'Taxable income for Nova Scotia *', 'Taxable income for all provinces ***'],
      indicator: 'F2',
      num: [5001, 5002, 5003, 5004]
    }),
    '6000': {
      hasTotals: true,
      showNumbering: true,
      superHeaders: [
        {
          header: 'J1'
        },
        {
          header: 'K1'
        },
        {
          header: 'L1'
        },
        {
          header: 'M1'
        }],
      columns: [
        {
          header: 'Amounts from column F1 in Part 3 of Schedule 7'
        },
        {
          header: 'Amounts from column K1 in Part 3 of Schedule 7 multiplied by 350,000 / 500,000',
          num: '406'
        },
        {
          header: 'Column J1 minus column K1 (if negative, enter "0")',
          num: '407',
          total: true,
          totalNum: '409',
          totalIndicator: 'N1',
          totalMessage: 'Totals'
        },
        {
          header: 'Lesser of columns J1 and K1 (if column J1 is negative, enter "0")',
          num: '408',
          total: true,
          totalNum: '410',
          totalIndicator: 'O1'
        }]
    },
    '6100': {
      hasTotals: true,
      width: 'calc((100% - 159px))',
      showNumbering: true,
      superHeaders: [
        {
          header: 'J2'
        },
        {
          header: 'K2'
        },
        {
          header: 'L2'
        },
        {
          header: 'M2'
        }],
      columns: [
        {
          header: 'Amounts from column F1 in Part 3 of Schedule 7'
        },
        {
          header: 'Amounts from column K1 in Part 3 of Schedule 7'
        },
        {
          header: 'Column J2 minus column K2 (if negative, enter "0")',
          total: true,
          totalIndicator: 'N2',
          totalMessage: 'Totals',
          totalNum: '6111'
        },
        {
          header: 'Lesser of columns J1 and K1 (if column J1 is negative, enter "0")',
          total: true,
          totalIndicator: 'O2',
          totalNum: '6112'
        }]
    },

    '9000': getTableTaxInc({
      label: ['Amount CC', 'Taxable income for Nova Scotia offshore', 'Taxable income for Nova Scotia <b>plus</b> Taxable income for Nova Scotia offshore'],
      indicator: 'DD',
      num: [9001, 9002, 9003, 9004]
    }),
    '9010': getTableRate({
      label: ['Amount F1', 'Number of days in the tax year before January 1, 2017', 'Number of days in the tax year'],
      indicator: '1',
      num: [9011, 9012, 9013, 9014]
    }),
    '9050': getTableRate({
      label: ['Amount F2', 'Number of days in the tax year after December 31, 2016', 'Number of days in the tax year'],
      indicator: '2',
      num: [9051, 9052, 9053, 9054]
    }),
    '9020': getTableRate({
      label: ['Amount G1', 'Number of days in the tax year before January 1, 2017', 'Number of days in the tax year'],
      indicator: '4',
      num: [9021, 9022, 9023, 9024]
    }),
    '9060': getTableRate({
      label: ['Amount G2', 'Number of days in the tax year after December 31, 2016', 'Number of days in the tax year'],
      indicator: '5',
      num: [9061, 9062, 9063, 9064]
    }),
    '9030': getSimpleTableRate({
      label: ['Tax at the lower rate for Nova Scotia and its offshore area (amount 3 multiplied by '],
      indicator: 'AA',
      num: [9031, 9032]
    }),
    '9040': getSimpleTableRate({
      label: ['Tax at the higher rate for Nova Scotia and its offshore area (amount 6 multiplied by '],
      indicator: 'BB',
      num: [9041, 9042]
    })
  }
})();
