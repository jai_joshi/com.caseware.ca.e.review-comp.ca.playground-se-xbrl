(function() {

  function runTableRateCalcs(field, tableNum) {
    if (field(tableNum + 4).get() == 0) {
      field(tableNum + 3).assign(0);
    } else {
      field(tableNum + 3).assign(
          field(tableNum + 1).get() *
          field(tableNum + 2).get() /
          field(tableNum + 4).get()
      );
    }
  }

  function runTableTaxCalcs(field, tableNum) {
    if (field(tableNum + 4).get() == 0) {
      field(tableNum + 3).assign(0);
    } else {
      field(tableNum + 3).assign(
          field(tableNum + 1).get() *
          field(tableNum + 2).get() /
          field(tableNum + 4).get());
    }
  }

  function runSch7TableCalcs(field, tableNum, isK1) {
    var s7Table2000 = field('T2S7.2000');
    var s7Table3000 = field('T2S7.3000');
    field(tableNum).getRows().forEach(function(row, rIndex) {
      row[0].assign(s7Table2000.cell(rIndex, 1).get());
      if (isK1 == true) {
        row[1].assign(s7Table3000.cell(rIndex, 2).get() * field('ratesNs.306').get() / field('ratesNs.307').get());
      }
      else {
        row[1].assign(s7Table3000.cell(rIndex, 2).get());
      }
      row[2].assign(Math.max(row[0].get() - row[1].get(), 0));
      row[3].assign(row[0].get() < 0 ? 0 :
          Math.min(row[0].get(), row[1].get())
      );
    });
  }

  wpw.tax.create.calcBlocks('t2s346', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field) {
      // part 2
      var isPartnershipMember = (field('CP.067').get() == 1);
      var janDate = wpw.tax.date(2017, 1, 1);
      var dateComparisons = calcUtils.compareDateAndFiscalPeriod(janDate);
      var endBeforeDate = dateComparisons.isStartBeforeDate;
      var startAfterDate = dateComparisons.isEndAfterDate;
      if (isPartnershipMember && endBeforeDate) {
        calcUtils.getGlobalValue('400', 'T2S7', '525');
        calcUtils.getGlobalValue('401', 'T2S7', '530');
        calcUtils.getGlobalValue('402', 'T2S7', '540');
        calcUtils.getGlobalValue('403', 'T2S7', '541');
        calcUtils.getGlobalValue('408', 'T2S7', '543');
        field('406').assign(Math.max(field('400').get() + field('401').get() + field('402').get() +
            field('403').get() - field('408').get(), 0));
        calcUtils.equals('407', '406');
        calcUtils.getGlobalValue('404', 'T2S7', '409');
        runSch7TableCalcs(field, '6000', true);
        calcUtils.getGlobalValue('411', 'T2S7', '370');
        calcUtils.getGlobalValue('412', 'T2S7', '380');
        calcUtils.sumBucketValues('413', ['411', '412']);
        calcUtils.min('414', ['409', '413']);
        calcUtils.sumBucketValues('415', ['410', '414']);
        calcUtils.equals('416', '415');
        calcUtils.subtract('417', '404', '416');
        calcUtils.equals('418', '417');
        field('419').assign(Math.max(field('407').get() - field('418').get(), 0));
      }
      else if (isPartnershipMember && startAfterDate) {
        calcUtils.getGlobalValue('500', 'T2S7', '525');
        calcUtils.getGlobalValue('501', 'T2S7', '530');
        calcUtils.getGlobalValue('502', 'T2S7', '540');
        calcUtils.getGlobalValue('503', 'T2S7', '541');
        calcUtils.getGlobalValue('504', 'T2S7', '543');
        field('505').assign(Math.max(field('500').get() + field('501').get() + field('502').get() +
            field('503').get() - field('504').get(), 0));
        calcUtils.equals('505', '506');
        calcUtils.getGlobalValue('517', 'T2S7', '409');
        runSch7TableCalcs(field, '6100', false);
        calcUtils.getGlobalValue('518', 'T2S7', '370');
        calcUtils.getGlobalValue('509', 'T2S7', '380');
        calcUtils.sumBucketValues('510', ['518', '509']);
        calcUtils.min('511', ['6111', '6112']);
        calcUtils.sumBucketValues('512', ['6112', '511']);
        calcUtils.equals('513', '512');
        calcUtils.subtract('514', '517', '513');
        calcUtils.equals('515', '514');
        field('516').assign(Math.max(field('506').get() - field('515').get(), 0));
      }
      else {
        calcUtils.removeValue([400, 401, 402, 403, 408, 404, 407, 411, 412, 413, 500, 501, 502, 503, 504, 517, 518, 509, 505, 506, 510, 511, 512, 513, 514, 515, 516], true);
      }

    });

    calcUtils.calc(function(calcUtils, field) {

      field('3002').assign(field('ratesNs.302').get());
      field('3002').source(field('ratesNs.302'));
      field('3004').assign(field('ratesNs.303').get());
      field('3004').source(field('ratesNs.303'));

      field('9031').assign(field('ratesNs.400').get());
      field('9031').source(field('ratesNs.400'));

      field('9041').assign(field('ratesNs.401').get());
      field('9041').source(field('ratesNs.401'));

      var isPartnershipMember = (field('CP.067').get() == 1);
      var taxableIncomeAllocationNS = calcUtils.getTaxableIncomeAllocation('NS');
      var taxableIncomeAllocationNO = calcUtils.getTaxableIncomeAllocation('NO');
      var totalProvincialTI = taxableIncomeAllocationNS.provincialTI + taxableIncomeAllocationNO.provincialTI;
      var allProvincesTI = taxableIncomeAllocationNS.allProvincesTI;

      var janDate = wpw.tax.date(2017, 1, 1);
      var dateComparisons = calcUtils.compareDateAndFiscalPeriod(janDate);
      var endBeforeDate = (dateComparisons.daysAfterDate <= 0);
      var startAfterDate = (dateComparisons.daysBeforeDate <= 0);
      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();

      //period after Dec 31 2016
      if (startAfterDate) {
        calcUtils.removeValue([100, 101, 102, 3001, 3003, 107, 4001, 4002, 4003, 4004, 112], true);
        field('200').assign(totalProvincialTI);
        field('201').assign(isPartnershipMember ? field('516').get() : field('T2J.400').get());
        field('201').source(isPartnershipMember ? field('516') : field('T2J.400'));
        calcUtils.getGlobalValue('202', 't2j', '405');
        calcUtils.getGlobalValue('203', 't2j', '427');
        calcUtils.min('207', ['201', '202', '203']);
        field('5001').assign(field('207').get());
        field('5002').assign(totalProvincialTI);
        field('5002').source(taxableIncomeAllocationNS.provincialTISourceField);
        field('5004').assign(allProvincesTI);
        field('5004').source(taxableIncomeAllocationNS.allProvincesTISourceField);
        runTableTaxCalcs(field, 5000);
        field('212').assign(Math.max(0, field('200').get() - field('5003').get()));
      }
      //period before Jan 1 2017
      else if (endBeforeDate) {
        calcUtils.removeValue([200, 201, 202, 203, 207, 5001, 5002, 5003, 5004, 212], true);
        field('100').assign(totalProvincialTI);
        field('100').source(taxableIncomeAllocationNS.provincialTISourceField);
        field('101').assign(isPartnershipMember ? field('419').get() : field('t2j.400').get());
        field('101').source(field('t2j.400'));
        calcUtils.getGlobalValue('102', 't2j', '405');
        field('3001').assign(field('t2j.427').get());
        field('3001').source(field('t2j.427'));

        field('3003').assign(field('3004').get() == 0 ? 0 :
            field('3001').get() * field('3002').get() / field('3004').get());
        field('107').assign(Math.min(field('101').get(), field('102').get(), field('3003').get()));
        field('4001').assign(field('107').get());
        field('4002').assign(totalProvincialTI);
        field('4002').source(taxableIncomeAllocationNS.provincialTISourceField);
        field('4004').assign(allProvincesTI);
        field('4004').source(taxableIncomeAllocationNS.allProvincesTISourceField);
        runTableTaxCalcs(field, 4000);
        field('112').assign(Math.max(0, field('100').get() - field('4003').get()));
      }
      else {
        field('100').assign(totalProvincialTI);
        field('100').source(taxableIncomeAllocationNS.provincialTISourceField);
        field('101').assign(isPartnershipMember ? field('419').get() : field('t2j.400').get());
        field('101').source(field('t2j.400'));
        calcUtils.getGlobalValue('102', 't2j', '405');
        field('3001').assign(field('t2j.427').get());
        field('3001').source(field('t2j.427'));

        field('3003').assign(field('3004').get() == 0 ? 0 :
            field('3001').get() * field('3002').get() / field('3004').get());
        field('107').assign(Math.min(field('101').get(), field('102').get(), field('3003').get()));
        field('4001').assign(field('107').get());
        field('4002').assign(totalProvincialTI);
        field('4002').source(taxableIncomeAllocationNS.provincialTISourceField);
        field('4004').assign(allProvincesTI);
        field('4004').source(taxableIncomeAllocationNS.allProvincesTISourceField);
        runTableTaxCalcs(field, 4000);
        field('112').assign(Math.max(0, field('100').get() - field('4003').get()));

        field('200').assign(totalProvincialTI);
        calcUtils.getGlobalValue('201', 't2j', '400');
        calcUtils.getGlobalValue('202', 't2j', '405');
        calcUtils.getGlobalValue('203', 't2j', '427');
        calcUtils.min('207', ['201', '202', '203']);
        field('5001').assign(field('207').get());
        field('5002').assign(totalProvincialTI);
        field('5002').source(taxableIncomeAllocationNS.provincialTISourceField);
        field('5004').assign(allProvincesTI);
        field('5004').source(taxableIncomeAllocationNS.allProvincesTISourceField);
        runTableTaxCalcs(field, 5000);
        field('212').assign(Math.max(0, field('200').get() - field('5003').get()));
      }
      //part 3
      //lower rate
      field('9011').assign(field('4003').get());
      field('9012').assign(dateComparisons.daysBeforeDate);
      field('9014').assign(daysFiscalPeriod);
      runTableRateCalcs(field, 9010);

      field('9051').assign(field('5003').get());
      field('9052').assign(dateComparisons.daysAfterDate);
      field('9054').assign(daysFiscalPeriod);
      runTableRateCalcs(field, 9050);

      field('507').assign(field('9013').get() + field('9053').get());

      field('9032').assign(field('507').get() * field('9031').get() / 100);

      //higher rate
      field('9021').assign(field('112').get());
      field('9022').assign(dateComparisons.daysBeforeDate);
      field('9024').assign(daysFiscalPeriod);
      runTableRateCalcs(field, 9020);

      field('9061').assign(field('212').get());
      field('9062').assign(dateComparisons.daysAfterDate);
      field('9064').assign(daysFiscalPeriod);
      runTableRateCalcs(field, 9060);

      field('508').assign(field('9023').get() + field('9063').get());

      field('9042').assign(field('508').get() * field('9041').get() / 100);

      field('529').assign(field('9032').get() + field('9042').get());

      if ((taxableIncomeAllocationNS.provincialTI > 0) && (taxableIncomeAllocationNO.provincialTI > 0)) {
        field('9001').assign(field('529').get());
      } else {
        field('9001').assign(0);
      }

      field('9002').assign(taxableIncomeAllocationNO.provincialTI);
      field('9002').source(taxableIncomeAllocationNO.provincialTISourceField);
      field('9004').assign(totalProvincialTI);
      field('9004').source(taxableIncomeAllocationNS.provincialTISourceField);

      runTableTaxCalcs(field, 9000);
      calcUtils.subtract('534', '529', '9003');
    });

  });
})();
