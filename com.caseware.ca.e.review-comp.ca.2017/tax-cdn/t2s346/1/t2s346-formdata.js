(function() {
  'use strict';

  wpw.tax.global.formData.t2s346 = {
    formInfo: {
      abbreviation: 'T2S346',
      title: 'Nova Scotia Corporation Tax Calculation',
      schedule: 'Schedule 346',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 346 E (11/2017)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if your corporation had a permanent establishment (under ' +
              'section 400 of the federal <i>Income Tax Regulations</i>) in Nova Scotia, and had taxable ' +
              'income earned in the year in Nova Scotia and its offshore area.'
            },
            {
              label: 'This schedule is a worksheet only and is not required to be filed with your <i>T2 Corporation Income Tax Return.</i>'
            }
          ]
        }
      ],
      category: 'Nova Scotia Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Calculation of income subject to the lower and higher tax rate for Nova Scotia and its offshore area',
        'rows': [
          {
            'label': 'Period before January 1, 2017',
            'labelClass': 'bold center fullLength'
          },
          {
            label: 'If there are days in the tax year before January 1, 2017, calculate the income' +
            ' from active business as follows:',
            labelClass: 'fullLength'
          },
          {
            'label': 'Taxable income for Nova Scotia*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100'
                }
              }
            ],
            'indicator': 'A1'
          },
          {
            'label': 'Income eligible for the lower tax rate for Nova Scotia and its offshore area:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Amount from line 400 of the T2 return**',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '101'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B1'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 405 of the T2 return',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '102'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C1'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '3000'
          },
          {
            'label': 'Amount B1, C1, or D1, whichever is the least',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '107'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E1'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '4000'
          },
          {
            'label': '<b>Income subject to the higher tax rate for Nova Scotia and its offshore area</b> ' +
            '(amount A1 <b>minus</b> amount F1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '112'
                }
              }
            ],
            'indicator': 'G1'
          },
          {
            'label': 'Enter amount F1 and/or amount G1 on the applicable line(s) in Part 3.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'label': 'Period after December 31, 2016',
            'labelClass': 'bold center fullLength'
          },
          {
            label: 'If there are days in the tax year before January 1, 2017, calculate the income' +
            ' from active business as follows:',
            labelClass: 'fullLength'
          },
          {
            'label': 'Taxable income for Nova Scotia*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200'
                }
              }
            ],
            'indicator': 'A2'
          },
          {
            'label': 'Income eligible for the lower tax rate for Nova Scotia and its offshore area:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Amount from line 400 of the T2 return**',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '201'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B2'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 405 of the T2 return',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '202'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C2'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 427 of the T2 return',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '203'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D2'
                }
              }
            ]
          },
          {
            'label': 'Amount B2, C2, or D2, whichever is the least',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '207'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E2'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '5000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Income subject to the higher tax rate for Nova Scotia and its offshore area (amount A2 minus amount F2)',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '212'
                }
              }
            ],
            'indicator': 'G2'
          },
          {
            'label': 'Enter amount F2 and/or amount G2 on the applicable line(s) in Part 3.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fulLLength'
          },
          {
            'label': '* If the corporation has a permanent establishment only in Nova Scotia or in the offshore area of Nova Scotia, enter the taxable income from line 360 of the T2 return. Otherwise, enter the total of the taxable incomes allocated to both jurisdictions in Nova Scotia (the province itself and the offshore area) from column F in Part 1 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '** If the corporation is a member of a partnership, complete Part 2 to calculate income from active business.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '*** Includes the territories and the offshore jurisdictions for Nova Scotia and Newfoundland and Labrador.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of income from active business when there is partnership income',
        'rows': [
          {
            'label': 'Complete this part only if the corporation is a member or a designated member of a partnership.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Period before January 1, 2017',
            'labelClass': 'bold center fullLength'
          },
          {
            label: 'If there are days in the tax year before January 1, 2017, calculate the income' +
            ' from active business as follows:',
            labelClass: 'fullLength'
          },
          {
            'label': 'Amount U from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '400'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '1'
                }
              }
            ]
          },
          {
            'label': 'Line 530 from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '401'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '2'
                }
              }
            ]
          },
          {
            'label': 'Line 540 from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '402'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '3'
                }
              }
            ]
          },
          {
            'label': 'Amount W from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '403'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '4'
                }
              }
            ]
          },
          {
            'label': 'Amount Y from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '408'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '5'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount 1 <b>plus</b> amount 2, <b>plus</b> amount 3, <b>plus</b> amount 4, <b>minus</b> amount 5)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '406'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '407'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'H1'
          },
          {
            'label': 'Amount M from Part 4 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '404'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'I1'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '6000'
          },
          {
            'labelClass': 'fulLLength'
          },
          {
            'label': 'Amount on line 370 from Part 3 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '411'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'P1'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Amount on line 380 from Part 3 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '412'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'Q1'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (amount P1 <b>plus</b> amount Q1)',
            'labelClass': 'text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '413'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'R1'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Enter amount N1 or amount R1, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '414'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'S1'
                }
              },
              null
            ]
          },
          {
            'label': 'Specified partnership income (amount O1 <b>plus</b> amount S1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '415'
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '416'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'T1'
                }
              }
            ]
          },
          {
            'label': 'Partnership income not eligible for small business deduction (amount I1 <b>minus</b> amount T1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '417'
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '418'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'U1'
          },
          {
            'label': '<b>Income from active business</b> (amount H1 <b>minus</b> amount U1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '419'
                }
              }
            ],
            'indicator': 'V1'
          },
          {
            'label': 'Enter amount V1 on line B1.',
            'labelClass': 'fullLength'
          },
          {
            type: 'horizontalLine'
          },
          {
            label: 'Period after December 31, 2016',
            labelClass: 'bold center fullLength'
          },
          {
            label: 'For days in the tax year after December 31, 2016, calculate the income from active business as follows:',
            labelClass: 'fullLength'
          },
          {
            'label': 'Amount U from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '500'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '1'
                }
              }
            ]
          },
          {
            'label': 'Line 530 from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '501'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '2'
                }
              }
            ]
          },
          {
            'label': 'Line 540 from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '502'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '3'
                }
              }
            ]
          },
          {
            'label': 'Amount W from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '503'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '4'
                }
              }
            ]
          },
          {
            'label': 'Amount Y from Part 5 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '504'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '5'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount 1 <b>plus</b> amount 2, <b>plus</b> amount 3, <b>plus</b> amount 4, <b>minus</b> amount 5)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '505'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '506'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'H2'
          },
          {
            'label': 'Amount M from Part 4 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '517'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'I2'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '6100'
          },
          {
            'labelClass': 'fulLLength'
          },
          {
            'label': 'Amount on line 370 from Part 3 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '518'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'P2'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Amount on line 380 from Part 3 of Schedule 7',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '509'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'Q2'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (amount P2 <b>plus</b> amount Q2)',
            'labelClass': 'text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '510'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'R2'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Enter amount N2 or amount R2, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '511'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'S2'
                }
              },
              null
            ]
          },
          {
            'label': 'Specified partnership income (amount O2 <b>plus</b> amount S2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '512'
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '513'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'T2'
                }
              }
            ]
          },
          {
            'label': 'Partnership income not eligible for small business deduction (amount I2 <b>minus</b> amount T2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '514'
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '515'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'U2'
          },
          {
            'label': '<b>Income from active business</b> (amount H2 <b>minus</b> amount U2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '516'
                }
              }
            ],
            'indicator': 'V2'
          },
          {
            'label': 'Enter amount V2 at amount B2.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Calculation of Nova Scotia tax before credits and of Nova Scotia offshore tax',
        'rows': [
          {
            'label': 'Tax at the lower rate for Nova Scotia and its offshore area:',
            'labelClass': 'bold fullLength'
          },
          {
            'type': 'table',
            'num': '9010'
          },
          {
            'type': 'table',
            'num': '9050'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal (amount 1 <b>plus</b> amount 2)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '507'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '3'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '9030'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Tax at the higher rate for Nova Scotia and its offshore area:',
            'labelClass': 'bold fullLength'
          },
          {
            'type': 'table',
            'num': '9020'
          },
          {
            'type': 'table',
            'num': '9060'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal (amount 4 <b>plus</b> amount 5)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '508'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '6'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '9040'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': '<b>Tax for Nova Scotia and its offshore area</b> (amount AA <b>plus</b> amount BB)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '529'
                }
              }
            ],
            'indicator': 'CC'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Only one jurisdiction',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'If the taxable income is allocated only to Nova Scotia or to Nova Scotia offshore, and the corporation is not claiming a Nova Scotia tax credit, enter amount CC on line 760 of the T2 return. If the corporation is claiming a credit, enter amount CC on line 215 or 220 of Schedule 5, whichever applies.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Jurisdictions in both Nova Scotia and Nova Scotia offshore',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'If the corporation has taxable income allocated to both Nova Scotia and its offshore area, calculate the following:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '9000'
          },
          {
            'label': '<b>Nova Scotia offshore tax</b> - enter amount DD on line 220 of Schedule 5.',
            'labelClass': 'fulllength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Nova Scotia tax before credits</b> (amount CC <b>minus</b> amount DD)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '534'
                }
              }
            ],
            'indicator': 'EE'
          },
          {
            'label': 'Enter amount EE on line 215 of Schedule 5.',
            'labelClass': 'fulLlength'
          }
        ]
      }
    ]
  };
})();
