(function () {
  wpw.tax.create.tables('t2s388', {
    '100': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1],
      'columns': [
        {
          'width': '60%',
          cellClass: 'alignLeft'
        },
        {
          cellClass: 'alignLeft'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Name of person to contact for more information',
            'num': '151',
            type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
            'tn': '151'
          },
          '1': {
            'label': 'Telephone number including area code',
            'num': '153', type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
            'tn': '153',
            'telephoneNumber': true,
            validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
          }
        }
      ]
    },
    '200': {
      infoTable: true,
      fixedRows: true,
      dividers: [2],
      columns: [
        {
          type: 'none',
          width: '30px'
        },
        {
          type: 'text'
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          type: 'text'
        },
        {
          type: 'date',
          width: '160px'
        }
      ],
      cells: [
        {
          '0': {
            tn: '201'
          },
          '1': {
            label: 'Title of production',
            type: 'none'
          },
          '2': {
            tn: '202'
          },
          '3': {
            type: 'none',
            label: 'Date the production was completed (delivery/answer print)'
          },
          '4': {type: 'none'}
        },
        {
          '0': {},
          '1': {num: '201', "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]}},
          '2': {},
          '3': {type: 'none'},
          '4': {num: '202'}
        },
        {
          '0': {
            tn: '203'
          },
          '1': {
            label: 'Certificate number',
            type: 'none'
          },
          '2': {
            tn: '204'
          },
          '3': {
            type: 'none',
            label: 'Date the principal photography began'
          },
          '4': {type: 'none'}
        },
        {
          '0': {},
          '1': {num: '203', "validate": {"or": [{"length": {"min": "9", "max": "9"}}, {"check": "isEmpty"}]}},
          '2': {},
          '3': {type: 'none'},
          '4': {num: '204'}
        }
      ]
    },
    '400': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {type: 'none'},
        {type: 'none', width: '30px'},
        {type: 'date', width: '135px', 'formField': true},
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        }
      ],
      cells: [
        {
          '0': {
            label: 'Enter on line D the production commencement time, which is the <b>earlier</b> ' +
            'of a or b (enter dates): '
          },
          '1': {
            tn: '420'
          },
          '2': {num: '420'},
          '3': {label: 'D'}
        }
      ]
    },
    '680': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {type: 'none'},
        {type: 'none', width: '150px'},
        {colClass: 'std-input-width'},
        {width: '30px', type: 'none'},
        {width: '30px', type: 'none'},
        {
          width: '50px',
          "validate": {
            "and": [
              'percent',
              {
                "or": [
                  {"length": {"min": "1", "max": "6"}},
                  {"check": "isEmpty"}
                ]
              }
            ]
          }
        },
        {width: '50px', type: 'none'},
        {width: '30px', type: 'none'},
        {colClass: 'std-input-width'},
        {type: 'none', colClass: 'std-padding-width'}
      ],
      cells: [
        {
          '0': {
            label: '<b>Basic tax credit</b>'
          },
          '1': {
            label: 'amount q'
          },
          '2': {num: '654'},
          '3': {label: 'x'},
          '4': {tn: '655'},
          '5': {
            num: '655',
            "validate": {
              "or": [{
                "and": [{
                  "length": {
                    "min": "1",
                    "max": "6"
                  }
                }, {"check": "isPositive"}]
              }, {"check": "isEmpty"}]
            }
          },
          '6': {label: '% ** ='},
          '7': {tn: '660'},
          '8': {num: '660', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          '9': {label: 'J'}
        }
      ]
    },
    '700': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {type: 'none'},
        {type: 'none', width: '150px'},
        {colClass: 'std-input-width'},
        {width: '30px', type: 'none'},
        {width: '30px', type: 'none'},
        {
          width: '50px',
          "validate": {
            "and": [
              'percent',
              {
                "or": [
                  {"length": {"min": "1", "max": "6"}},
                  {"check": "isEmpty"}
                ]
              }
            ]
          }
        },
        {width: '50px', type: 'none'},
        {width: '30px', type: 'none'},
        {colClass: 'std-input-width'},
        {type: 'none', colClass: 'std-padding-width'}
      ],
      cells: [
        {
          '0': {
            label: 'Frequent filming bonus'
          },
          '1': {
            label: 'amount q from Part 6'
          },
          '2': {num: '701'},
          '3': {label: 'x'},
          '4': {tn: '705'},
          '5': {
            num: '705',
            "validate": {
              "or": [{
                "and": [{
                  "length": {
                    "min": "1",
                    "max": "6"
                  }
                }, {"check": "isPositive"}]
              }, {"check": "isEmpty"}]
            }
          },
          '6': {label: '% * ='},
          '8': {num: '706'},
          '9': {label: 'K'}
        }
      ]
    },
    '800': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {type: 'none'},
        {type: 'none', width: '150px'},
        {colClass: 'std-input-width'},
        {width: '30px', type: 'none'},
        {width: '30px', type: 'none'},
        {
          width: '50px',
          "validate": {
            "and": [
              'percent',
              {
                "or": [
                  {"length": {"min": "1", "max": "6"}},
                  {"check": "isEmpty"}
                ]
              }
            ]
          }
        },
        {width: '50px', type: 'none'},
        {width: '30px', type: 'none'},
        {colClass: 'std-input-width'},
        {type: 'none', colClass: 'std-padding-width'}
      ],
      cells: [
        {
          '0': {
            label: '<b>Rural bonus tax credit</b>'
          },
          '1': {
            label: 'amount q from Part 6'
          },
          '2': {num: '801'},
          '3': {label: 'x'},
          '4': {tn: '805'},
          '5': {
            num: '805',
            "validate": {
              "or": [{
                "and": [{
                  "length": {
                    "min": "1",
                    "max": "6"
                  }
                }, {"check": "isPositive"}]
              }, {"check": "isEmpty"}]
            }
          },
          '6': {label: '% * ='},
          '7': {tn: '820'},
          '8': {num: '820', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          '9': {label: 'N'}
        }
      ]
    },
    '910': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {type: 'none'},
        {type: 'none', width: '150px'},
        {colClass: 'std-input-width'},
        {width: '30px', type: 'none'},
        {width: '30px', type: 'none'},
        {
          width: '50px',
          "validate": {
            "and": [
              'percent',
              {
                "or": [
                  {"length": {"min": "1", "max": "6"}},
                  {"check": "isEmpty"}
                ]
              }
            ]
          }
        },
        {width: '50px', type: 'none'},
        {width: '30px', type: 'none'},
        {colClass: 'std-input-width'},
        {type: 'none', colClass: 'std-padding-width'}
      ],
      cells: [
        {
          '0': {
            label: '<b>Producer bonus tax credit*</b>'
          },
          '1': {
            label: 'amount q from Part 6'
          },
          '2': {num: '844'},
          '3': {label: 'x'},
          '4': {tn: '845'},
          '5': {
            num: '845',
            "validate": {
              "or": [{
                "and": [{
                  "length": {
                    "min": "1",
                    "max": "6"
                  }
                }, {"check": "isPositive"}]
              }, {"check": "isEmpty"}]
            }
          },
          '6': {label: '% ** ='},
          '7': {tn: '850'},
          '8': {num: '850', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}},
          '9': {label: 'O'}
        }
      ]
    },
    '890': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none'
        },
        {
          type: 'none', width: '100px'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none', width: '80px'
        },
        {
          type: 'none', width: '30px'
        },
        {
          colClass: 'std-input-width'
        },
        {type: 'none', colClass: 'std-padding-width'}
      ],
      cells: [
        {
          '0': {
            label: '<b>Cost of production tax credit</b>'
          },
          '1': {
            label: 'amount W '
          },
          '2': {num: '881'},
          '3': {
            label: 'x 30%='
          },
          '4': {
            tn: '875'
          },
          '5': {
            num: '875', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          '6': {
            label: 'X'
          }
        }
      ]
    }
  })
})();
