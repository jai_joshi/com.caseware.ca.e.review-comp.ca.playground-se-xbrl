(function() {
  wpw.tax.create.calcBlocks('t2s388', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //eligibility
      field('340').assign(field('330').get() == 0 ? 0 : field('331').get() / field('340').get());
      if (field('CP.750').get() == 'MB' || (field('CP.750').get() == 'MJ' && field('T2S5.015').get())) {
        field('314').assign(1)
      }
      else {
        field('314').assign(2)
      }
      //part 1 calcs
      field('151').assign(field('T2J.951').get() + ' ' + field('T2J.950').get());
      field('153').assign(field('T2J.956').get());
      //run calc only if credit is eligible
      var eligibilityQuestions = field('311').get() == 1 && field('312').get() == 1 && field('313').get() == 1 && field('314').get() == 1;
      var eligibilityPercentage = field('340').get() > 25;
      if (eligibilityPercentage || eligibilityQuestions) {
        //part 4
        field('421').assign(field('204').get());
        field('427').assign(calcUtils.addToDate(field('421').get(), -2, 0, 0));
        //b)is the latest of 425,426,427
        var greaterDate = [];
        var greatestDate = [];
        if (calcUtils.dateCompare.greaterThan(field('425').get() , field('426').get())) {
          greaterDate = field('425').get()
        }
        else {
          greaterDate = field('426').get()
        }
        if (calcUtils.dateCompare.greaterThan(greaterDate, field('427').get())) {
          greatestDate = greaterDate
        }
        else {
          greatestDate = field('427').get()
        }
        field('420').assign(calcUtils.dateCompare.lessThan(greatestDate, field('421').get()) ? greatestDate : field('421').get());

        //part 5
        calcUtils.sumBucketValues('514', ['510', '511', '512', '513']);
        field('515').assign(field('514').get());
        field('550').assign(field('505').get() + field('515').get() + field('520').get());
        field('581').assign(field('572').get() * field('580').get() / 100);
        field('590').assign(Math.min(field('581').get(), field('571').get()));
        //part6
        field('600').assign(field('550').get());
        field('601').assign(field('590').get());
        field('602').assign(field('600').get() + field('601').get());
        field('611').assign(field('602').get() - field('610').get());
        field('616').assign(field('611').get() + field('615').get());
        field('650').assign(field('616').get() - field('620').get());
        field('654').assign(field('650').get());
        field('660').assign(field('654').get() * field('655').get() / 100);
        //part7
        field('701').assign(field('650').get());
        field('706').assign(field('701').get() * field('705').get() / 100);
        field('730').assign(field('706').get() * field('710').get() / 100);
        //part8
        field('801').assign(field('650').get());
        field('820').assign(field('801').get() * field('805').get() / 100);
        //part9
        field('844').assign(field('650').get());
        field('850').assign(field('844').get() * field('845').get() / 100);
        //part10
        calcUtils.sumBucketValues('861', ['857', '858', '859', '860']);
        field('856').assign(field('861').get());
        calcUtils.sumBucketValues('880', ['855', '856', '862', '864', '865']);
        calcUtils.subtract('870', '880', '866');
        field('875').assign(field('881').get() * 0.3);
        //part11
        field('901').assign(field('660').get());
        field('902').assign(field('730').get());
        field('903').assign(field('820').get());
        field('904').assign(field('850').get());
        field('905').assign(field('901').get() + field('902').get() + field('903').get() + field('904').get());
        field('906').assign(field('875').get());

        var date = wpw.tax.date(2010, 4, 1);
        var isBeforeApr = calcUtils.dateCompare.lessThan(field('204').get(), date);
        if (isBeforeApr) {
          field('900').assign(field('905').get())
        }
        else {
          calcUtils.max('900', ['905', '906']);
        }
      }
      else if (calcUtils.hasChanged('311') || calcUtils.hasChanged('312') || calcUtils.hasChanged('313') || calcUtils.hasChanged('314')) {
        calcUtils.removeValue([320, 330, 331, 425, 426, 505, 510, 511, 512, 513, 520, 571, 572, 580, 610, 615,
          620, 6557, 705, 710, 805, 845, 855, 857, 858, 859, 860, 862, 864, 865, 866, 881]);
      }
    });
  });
})();
