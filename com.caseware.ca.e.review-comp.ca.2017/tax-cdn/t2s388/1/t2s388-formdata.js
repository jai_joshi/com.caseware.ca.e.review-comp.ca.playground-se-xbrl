(function() {
  'use strict';
  wpw.tax.create.formData('t2s388', {
    formInfo: {
      abbreviation: 't2s388',
      title: 'Manitoba Film and Video Production Tax Credit',
      //subTitle: '(2012 and later tax years)',
      schedule: 'Schedule 388',
      code: 'Code 1202',
      formFooterNum: 'T2 SCH 388 E (15)',
      headerImage: 'canada-federal',
      isRepeatForm: true,
      showCorpInfo: true,
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule to claim a Manitoba Film and Video Production Tax Credit under sections 7.5' +
              ' to 7.9 of the <i>Income Tax Act</i> (Manitoba). Complete a separate Schedule 388 for each eligible film'
            },
            {
              label: 'For an eligible film where principal photography begins after March 31, 2010,' +
              ' an eligible corporation may claim either:',
              sublist: [
                'the cost-of-salaries credit for the tax year; or',
                'the cost-of-production credit for the tax year'
              ]
            },
            {
              label: 'For an eligible film where principal photography begins before April 1, 2010, ' +
              'an eligible corporation may claim only the cost-of-salaries tax credit for the tax year.' +
              ' Do not complete Part 10.'
            },
            {
              label: 'The cost-of-salaries credit includes the basic tax credit, frequent filming bonus tax credit,' +
              ' the rural bonus tax credit and the producer bonus tax credit'
            },
            {
              label: 'The cost-of-production credit includes eligible salaries, eligible service contract ' +
              'expenditures, eligible parent-subsidiary amounts, eligible tangible property expenditures and ' +
              'eligible accommodation expenditures'
            },
            {
              label: 'The terms <b>"eligible individual", "eligible non-resident individual"</b> and ' +
              '<b>"eligible employee"</b> are defined in subsection 7.5(1) of the <i>Income Tax Act</i> (Manitoba)'
            },
            {
              label: 'To claim this credit, for each eligible film, include the following items with your ' +
              '<i>T2 Corporation Income Tax Return</i> for the tax year',
              sublist: [
                'a Certificate of Completion (if the production was completed in the tax year), <b>or</b> ' +
                'an Advance Certificate of Eligibility (if the production was not completed in the tax year) ' +
                'issued by Manitoba Film and Music;',
                'a completed copy of this form – you can complete one form for episodes in a series that are ' +
                'certified eligible films; and',
                'all the documents listed in Part 12 on page 5 of this form'
              ]
            }
          ]
        }
      ],
      category: 'Manitoba Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Contact information',
        'rows': [
          {
            'type': 'table',
            'num': '100'
          }
        ]
      },
      {
        'header': 'Part 2 - Identifying the film or video production',
        'rows': [
          {
            'type': 'table',
            'num': '200'
          }
        ]
      },
      {
        'header': 'Part 3 - Eligibility ',
        'rows': [
          {
            'label': '1. Is the corporation incorporated in Canada?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '311',
            'tn': '311',
            'labelWidth': '80%'
          },
          {
            'label': '2. Is the corporation a taxable Canadian corporation?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '312',
            'tn': '312',
            'labelWidth': '80%'
          },
          {
            'label': '3. Does the corporation primarily carry on the business of film or video production?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '313',
            'tn': '313',
            'labelWidth': '80%'
          },
          {
            'label': '4. Does the corporation have a permanent establishment in Manitoba?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '314',
            'tn': '314',
            'labelWidth': '80%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Portion of salaries and wages* paid to eligible employees and employees who are eligible non-resident individuals',
            'labelClass': 'fullLength bold'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total number of individuals who are employees of the production corporation in the tax year, whether or not they worked on this film',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '320',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,4}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '320'
                }
              }
            ]
          },
          {
            'label': 'All salaries and wages the corporation has paid to <b>employees</b> in the tax year. (Do <b>not</b> include amounts paid to individual contractors or corporations)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '330',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '330'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Salaries and wages paid to <b>eligible employees</b> (including <b>employees who are eligible non-resident individuals</b>) in the tax year for work performed on this film in Manitoba**. (Do <b>not</b> include amounts paid to individual contractors or corporations)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '331',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '331'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Portion of the total salaries and wages that are paid to <b>eligible employees</b> and <b>employees who are eligible non-resident individuals</b> (line B <b>divided by</b> line A)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '340',
                  'decimals': '3',
                  'validate': {
                    'and': [
                      'percent',
                      {
                        'or': [
                          {
                            'length': {
                              'min': '1',
                              'max': '6'
                            }
                          },
                          {
                            'check': 'isEmpty'
                          }
                        ]
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '340'
                }
              }
            ],
            'indicator': '%C'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Important</b> - If you answered <b>No</b> to any of questions 1 to 4, or if the percentage at line C is less than 25%, you are <b>not</b> eligible for the Manitoba film and video production tax credit.',
            'labelClass': 'fullLength'
          },
          {
            'label': '* Salary or wages for which a T4 has been issued by the eligible corporation. <br>** For a documentary, include the amount for work performed in Manitoba and any other locations.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 4 - The production commencement time ',
        'rows': [
          {
            'type': 'table',
            'num': '400'
          },
          {
            'type': 'infoField',
            'inputType': 'date',
            'label': 'a) the date principal photography began; and',
            'labelWidth': '80%',
            'tn': '421',
            'num': '421',
            'indicatorColumn': true
          },
          {
            'label': 'b) the <b>latest</b> of:'
          },
          {
            'type': 'infoField',
            'inputType': 'date',
            'label': 'i) the date the corporation or its parent first incurred development labour costs for the development of script material on which the production is based;',
            'labelClass': 'tabbed',
            'labelWidth': '80%',
            'tn': '425',
            'num': '425',
            'indicatorColumn': true
          },
          {
            'type': 'infoField',
            'inputType': 'date',
            'label': 'ii) the date the corporation or its parent first acquired a right for the story that is the basis of the final script; and',
            'labelClass': 'tabbed',
            'labelWidth': '80%',
            'tn': '426',
            'num': '426',
            'indicatorColumn': true
          },
          {
            'type': 'infoField',
            'inputType': 'date',
            'label': 'iii) the date that is two years before the date on which principal photography began',
            'labelClass': 'tabbed',
            'labelWidth': '80%',
            'tn': '427',
            'num': '427',
            'indicatorColumn': true,
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '1',
                    'max': '60'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          }
        ]
      },
      {
        'header': 'Part 5 - Eligible salaries',
        'rows': [
          {
            'label': 'Eligible salaries* for services rendered by eligible individuals',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Salary or wages (paid to eligible individuals)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '505',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '505'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': '<b>Add:</b>'
          },
          {
            'label': 'Remuneration paid to'
          },
          {
            'label': 'eligible individuals',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '510',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '510'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'taxable Canadian corporations (solely owned by an eligible individual)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '511',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '511'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': 'other taxable Canadian corporations (for their eligible employees)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '512',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '512'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'partnerships carrying on business in Canada (for their members who are eligible individuals or eligible employees)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '513',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '513'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts a to d)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '514'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '515'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': '<b>Add:</b>'
          },
          {
            'label': 'Labour expenditures incurred by a parent corporation and transferred to its wholly-owned subsidiary (the corporation) under a reimbursement agreement',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '520',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '520'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': '<b>Eligible salaries for services rendered by eligible individuals</b> (amount E <b>plus</b> amount F <b>plus</b> amount G)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '550',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '550'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'label': 'The following portion of Part 5 should be completed only if you are filing this claim for the tax year during which the production was completed and if you are attaching a Certificate of Completion to your claim',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Eligible salaries for services provided by eligible non-resident individuals</b> <br>(calculate these amounts separately for each year of the production and enter the totals for all years below)',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Eligible salaries the corporation would have paid if the services provided by eligible non-resident individuals were provided by eligible individuals (include salary or wages and remuneration, but do <b>not</b> include amounts for taxable benefits received by eligible non-resident individuals)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '571',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '571'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': 'Eligible salaries for services rendered by eligible individuals (from amount H)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '572'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'label': 'Enter the rate of the deemed labour cap that is specified on the Certificate of Completion',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '580',
                  'validate': {
                    'and': [
                      'percent',
                      {
                        'or': [
                          {
                            'length': {
                              'min': '1',
                              'max': '6'
                            }
                          },
                          {
                            'check': 'isEmpty'
                          }
                        ]
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '580'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '%g'
                }
              }
            ]
          },
          {
            'label': 'Maximum eligible salaries for eligible non-resident individuals (amount f <b>multiplied by</b> amount g)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '581'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'h'
                }
              }
            ]
          },
          {
            'label': 'Eligible salaries for services rendered by eligible non-resident individuals (enter the lesser of the amounts e and h)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '590',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '590'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Manitoba eligible salaries include amounts that are:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'a) reasonable in the circumstances; <br> b) directly attributable to the production of the eligible film or video; <br> c) incurred in the tax year, or the previous tax year, and paid no later than 60 days after the end of the tax year; <br> d) incurred and paid before January 1, 2020; and <br> e) for the stages of production from the <b>production commencement</b> time to the end of the post-production stage. Manitoba eligible salaries do not include amounts that are incurred in the immediately preceding year and paid within 60 days after the end of that preceding year ',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'header': 'Part 6 - Basic tax credit',
        'rows': [
          {
            'label': 'Eligible salaries for services rendered by eligible individuals (amount H from Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '600'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'i'
                }
              }
            ]
          },
          {
            'label': '<b>Add:</b>'
          },
          {
            'label': 'Eligible salaries for services rendered by eligible non-resident individuals (amount I from Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '601'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'j'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount i <b>plus</b> amount j)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '602'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'k'
                }
              }
            ]
          },
          {
            'label': '<b>Deduct:</b>'
          },
          {
            'label': 'Eligible salaries included in amount k that may be claimed by another corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '610',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '610'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'l'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount k <b>minus</b> amount l)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '611'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'm'
                }
              }
            ]
          },
          {
            'label': '<b>Add:</b>'
          },
          {
            'label': 'Amount of eligible salaries allocated to the corporation through a joint allocation agreement that was filed with the Minister of National Revenue',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '615',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '615'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'n'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount m <b>plus</b> amount n)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '616'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'o'
                }
              }
            ]
          },
          {
            'label': '<b>Deduct:</b>'
          },
          {
            'label': 'Total amount of government assistance* received or receivable by the corporation in connection with these eligible salaries',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '620',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '620'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'p'
                }
              }
            ]
          },
          {
            'label': 'Total eligible salaries (amount o minus amount p)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '650',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '650'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'q'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '680'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Government assistance as defined in subsection 7.5(1) of the <i>Income Tax Act</i> (Manitoba) <br> ** The rate of basic credit that is specified on the Advance Certificate of Eligibility or the Certificate of Completion.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 7 - Frequent filming bonus tax credit',
        'rows': [
          {
            'type': 'table',
            'num': '700'
          },
          {
            'label': 'Percentage of labour eligible for frequent filming bonus',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '710',
                  'validate': {
                    'and': [
                      'percent',
                      {
                        'or': [
                          {
                            'length': {
                              'min': '1',
                              'max': '6'
                            }
                          },
                          {
                            'check': 'isEmpty'
                          }
                        ]
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '710'
                }
              }
            ],
            'indicator': '%**L'
          },
          {
            'label': '<b>Frequent filming bonus tax credit</b> (amount K <b>multiplied by</b> amount L)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '730',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '730'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* The rate of frequent filming bonus that is specified on the Advance Certificate of Eligibility or the Certificate of Completion <br> ** The percentage of eligible hours that is specified on the Advance Certificate of Eligibility or the Certificate of Completion. In any other situation, enter "<b>100</b>"'
          }
        ]
      },
      {
        'header': 'Part 8 - Rural bonus tax credit',
        'rows': [
          {
            'type': 'table',
            'num': '800'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* The rate of rural bonus that is specified on the Advance Certificate of Eligibility or the Certificate of Completion',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 9 - Producer bonus tax credit',
        'rows': [
          {
            'type': 'table',
            'num': '910'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* If an eligible individual in Manitoba in the year in which the principal photography ends, or in the immediately preceding year, receives credit as a producer, co-producer or executive producer of that film <br><br> ** The rate of producer bonus that is specified on the Advance Certificate of Eligibility or the Certificate of Completion',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 10 - Cost of production tax credit',
        'rows': [
          {
            'label': 'If principal photography begins after March 31, 2010, complete this part to calculate the cost of production tax credit',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Salary and wages* paid to eligible individuals and eligible non-resident individuals that are directly attributable to the eligible Manitoba film ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '855',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '855'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'label': '<b>Add:</b><br> Eligible service contract expenditures directly attributable to the eligible film paid to:'
          },
          {
            'label': 'eligible individuals',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '857',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '857'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'r'
                }
              }
            ]
          },
          {
            'label': 'taxable Canadian corporations (solely owned by an eligible individual)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '858',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '858'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 's'
                }
              }
            ]
          },
          {
            'label': 'other taxable Canadian corporations (for their eligible employees)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '859',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '859'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 't'
                }
              }
            ]
          },
          {
            'label': 'partnerships (for their members who are eligible individuals or eligible employees)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '860',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '860'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'u'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts r to u)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '861'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '856'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'Q'
          },
          {
            'label': 'Eligible service contract expenditures incurred by the parent corporation and transferred to its wholly-owned subsidiary corporation under a reimbursement agreement',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '862',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '862'
                }
              }
            ],
            'indicator': 'R'
          },
          {
            'label': 'Eligible tangible property expenditures directly attributable to the eligible film',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '864',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '864'
                }
              }
            ],
            'indicator': 'S'
          },
          {
            'label': 'Eligible accommodation expenditures** directly attributable to the eligible film',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '865',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '865'
                }
              }
            ],
            'indicator': 'T'
          },
          {
            'label': 'Subtotal (total of amounts P to T)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '880'
                }
              }
            ],
            'indicator': 'U'
          },
          {
            'label': '<b>Deduct:</b>'
          },
          {
            'label': 'Government assistance included in amount U that the corporation has not repaid',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '866',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '866'
                }
              }
            ],
            'indicator': 'V'
          },
          {
            'label': 'Cost of production for the tax year (amount U <b>minus</b> amount V) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '870',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '870'
                }
              }
            ],
            'indicator': 'W'
          },
          {
            'type': 'table',
            'num': '890'
          },
          {
            'label': '* Manitoba eligible salaries include amounts that are:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'a) reasonable in the circumstances; <br>b) directly attributable to the production of the eligible film or video; <br> c) incurred in the tax year, or the previous tax year, and paid no later than 60 days after the end of the tax year; <br> d) incurred and paid before January 1, 2020; and<br>e) for the stages of production from the production commencement time to the end of the post-production stage. Manitoba eligible salaries do not include amounts that are incurred in the immediately preceding year and paid within 60 days after the end of that preceding year. <br>',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '** Accommodation expenditures for productions commencing after April 17, 2012, are eligible to a maximum of $300 per night per accommodation unit in Manitoba.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 11 - Manitoba film and video production tax credit',
        'rows': [
          {
            'label': 'Basic tax credit (amount J from Part 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '901'
                }
              }
            ],
            'indicator': 'Y'
          },
          {
            'label': 'Frequent filming bonus tax credit (amount M from Part 7)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '902'
                }
              }
            ],
            'indicator': 'Z'
          },
          {
            'label': 'Rural bonus tax credit (amount N from Part 8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '903'
                }
              }
            ],
            'indicator': 'AA'
          },
          {
            'label': 'Producer bonus tax credit (amount O from Part 9)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '904'
                }
              }
            ],
            'indicator': 'BB'
          },
          {
            'label': '<b>Cost of salaries tax credit</b> (total of amounts Y to BB) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '905'
                }
              }
            ],
            'indicator': 'CC'
          },
          {
            'label': '<b>Cost of production tax credit</b> (amount X from Part 10)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '906'
                }
              }
            ],
            'indicator': 'DD'
          },
          {
            'label': '<b>Manitoba film and video production tax credit*</b>(amount CC or amount DD, whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '900',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '900'
                }
              }
            ],
            'indicator': 'EE'
          },
          {
            'label': 'Enter amount EE on line 620 of Schedule 5, Tax <i>Calculation Supplementary - Corporations.</i> If you are filing more than one of these schedules, add the amounts from line EE of all the schedules, and enter the total on line 620 of Schedule 5',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* If principal photography begins before April 1, 2010, only the cost of salaries tax credit (amount CC) may be claimed at line 900',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 12 - Claim checklist',
        'rows': [
          {
            'label': 'To speed up the processing of your claim, make sure you include all the documents listed below with your <i>T2 Corporation Income Tax Return</i> for <b>each</b> film for which you are claiming the Manitoba film and video production tax credit.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '1. An Advance Certificate of Eligibility <b>or</b> a Certificate of Completion issued by Manitoba Film and Music',
            'labelWidth': '90%',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'num': '1200'
          },
          {
            'label': '2. A completed copy of this form (you can complete one form for episodes in a series that are certified eligible productions)',
            'labelWidth': '90%',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'num': '1201'
          },
          {
            'label': '3. A Report of Eligible Manitoba Labour Expenditures conforming to the format presented in Form B (1)*',
            'labelWidth': '90%',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'num': '1202'
          },
          {
            'label': '4. The final detailed cost report upon which the Report of Eligible Manitoba Labour Expenditures is based (indicating eligible Manitoba labour expenditures)',
            'labelWidth': '90%',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'num': '1203'
          },
          {
            'label': '5. The financial structure for the film or video for the tax year',
            'labelWidth': '90%',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'num': '1204'
          },
          {
            'label': '6. If applicable, any documentation that indicates a change in control to the corporation or its corporate structure, which has occurred after the date on which the Part A application has been submitted',
            'labelWidth': '90%',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'num': '1205'
          },
          {
            'label': '7. If applicable, the actual list of deemed labour (Form D, Part B*) signed by unions/guilds/Film Training Manitoba (it should also be on file with Manitoba Film and Music)',
            'labelWidth': '90%',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'num': '1206'
          },
          {
            'label': '8. If you are claiming eligible salaries for services provided by eligible non-resident individuals in Part 5 of this form, include a document showing a separate breakdown of eligible salaries by tax years for:',
            'labelClass': 'fullLength'
          },
          {
            'label': '(1) eligible individuals, and <br>(2) eligible non-resident individuals',
            'labelClass': 'tabbed'
          },
          {
            'labelClass': 'tabbed',
            'label': 'in addition to the actual list of deemed labour',
            'labelWidth': '90%',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'num': '1207'
          },
          {
            'label': '<b>Important</b> - Declarations of Manitoba Residency (Form A (1)*) for all individuals for which amounts are claimed on this application must be available upon request',
            'labelClass': 'fullLength'
          },
          {
            'label': '* For more information on the forms mentioned above, provided by Manitoba Film and Music, visit their website at <a href="http://mbfilmmusic.ca/en/">www.mbfilmmusic.ca</a>',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  });
})();
