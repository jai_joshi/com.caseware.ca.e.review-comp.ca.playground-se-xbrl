(function () {

  wpw.tax.create.calcBlocks('t2a4', function (calcUtils) {

    calcUtils.calc(function (calcUtils, field, form) {

      var linkedTable = form('t2s21').field('101');
      field('990').getRows().forEach(function (row, rIndex) {
        row[0].assign(linkedTable.cell(rIndex, 0).get());
        row[0].source(linkedTable.cell(rIndex, 0));
        row[1].assign(linkedTable.cell(rIndex, 1).get());
        row[1].source(linkedTable.cell(rIndex, 1));
        row[2].assign(field('t2a.065').get());
        row[2].source(field('t2a.065'));
        row[3].assign(row[1].get() * row[2].get() * field('t2a.067').get()/100);
        row[4].assign(linkedTable.cell(rIndex, 3).get());
        row[4].source(linkedTable.cell(rIndex, 3));
        row[5].assign(linkedTable.cell(rIndex, 2).get() - row[4].get());
        row[6].assign(form('t2s21').field('102').cell(rIndex, 3).get());
        row[6].source(form('t2s21').field('102').cell(rIndex, 3));
        row[7].assign((row[5].get() - row[6].get()) * row[2].get());
        row[8].assign(Math.min(row[3].get(), row[7].get()));
      });
     });

    calcUtils.calc(function (calcUtils, field, form) {
      field('018').assign(field('t2a.068').get() - (field('t2a.070').get() + field('t2a.071').get()));
      field('018').source(field('t2a.068'));
      field('020').assign(Math.min(field('014').get(), field('018').get()));
    });
  });
})();
