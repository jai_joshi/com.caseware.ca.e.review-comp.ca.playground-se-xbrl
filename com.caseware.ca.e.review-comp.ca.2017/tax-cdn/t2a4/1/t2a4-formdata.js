(function() {
  'use strict';

  wpw.tax.global.formData.t2a4 = {
    formInfo: {
      abbreviation: 't2a4',
      title: 'Alberta Foreign Investment Income Tax Credit',
      schedule: 'AT1 Schedule 4',
      headerImage: 'canada-Alberta',
      showCorpInfo: true,
      formFooterNum: 'AT201 (Jul-12)',
      category: 'Alberta Tax Forms',
      dynamicFormWidth: true
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: 'For corporations which have included in income any foreign investment income and which ' +
            'are entitled to a Federal Non-Business Foreign Tax Credit.',
            labelClass: 'fullLength'
          },
          {
            label: 'Report all monetary values in dollars; DO NOT include cents.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '990'
          },
          {
            label: 'From AT1, page 2: line 068 - (lines 070 + 071)',
            layout: 'alignInput',
            layoutOptions: {
              'indicatorColumn': true,
              'showDots': true
            },
            'columns': [
              {
                'input': {
                  'num': '018'
                },
                'padding': {
                  'type': 'tn',
                  'data': '018'
                }
              }
            ]
          },
          {
            label: 'Alberta Foreign Investment Income Tax Credit:<br>' +
            'Lesser of amounts on lines 014 and 018',
            layout: 'alignInput',
            layoutOptions: {
              'indicatorColumn': true,
              'showDots': true
            },
            'columns': [
              {
                'input': {
                  'num': '020'
                },
                'padding': {
                  'type': 'tn',
                  'data': '020'
                }
              }
            ]
          },
          {
            label: 'Enter this amount on AT1 page 2, line 072',
            labelClass: 'fullLength bold'
          },
          {labelClass: 'fullLength'},
          {
            label: '* If the corporation has permanent establishments in Alberta only, enter "1" in column C.',
            labelClass: 'fullLength'
          },
          {
            label: '** If the corporation\'s deduction from income under subsection 8(2.2) of the Alberta Corporate ' +
            'Tax Act (ACTA) is different from the deduction under subsection 20(12) of the Income Tax Act (ITA)' +
            ' for any country, then Alberta Schedule 12 is required to be\n' +
            'completed. The total of these amounts for each country for Alberta purposes is to be included' +
            ' in the amount at line 040 on\n' +
            'Alberta Schedule 12. See guide for more information.',
            labelClass: 'fullLength'
          }
        ]
      }
    ]
  };
})();