(function() {

  var countryAddressCodes = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.countryAddressCodes, 'value');
  var canadaProvinces = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.canadaProvinces, 'EN');
  var countryOptions = getDropdownOptions(countryAddressCodes);

  function getDropdownOptions(countryCodes) {
    var options = [{value: '', option: ''}];
    countryCodes.forEach(function(opt, index) {
      if(index == 0) {
        return;
      }
      options.push({value: opt.value, option: opt.value + ' : ' + opt.option});
    });
    return options;
  }

  wpw.tax.global.tableCalculations.t2a4 = {
    '990': {
      fixedRows: true,
      'showNumbering': true,
      'columns': [
        {
          'header': 'A<br>Country in which foreign non-business\n' +
          'income was\n' +
          'earned from\n' +
          'federal sch 21\n' +
          'line 100\n' +
          '(use the 2 digit\n' +
          'country code\n' +
          'specified in\n' +
          'the Guide)',
          'tn': '002',
          'type': 'dropdown',
          'options': countryOptions
        },
        {
          'header': 'B<br>Net foreign\n' +
          'investment\n' +
          'income from\n' +
          'federal schedule\n' +
          '21 line 110',
          tn: '004'
        },
        {
          'header': 'C<br>Alberta\n' +
          'allocation\n' +
          'factor\n' +
          'from AT1\n' +
          'Schedule 2:*'
        },
        {
          'header': 'D<br>B X C X (AT1 line 068 / AT1 line 066)'
        },
        {
          'header': 'ACTA 8(2.2)* * or ITA 20(12)'
        },
        {
          'header': 'E<br>Foreign investment income tax paid(federal sch 21 line 120) minus greater of amount ' +
          'deducted under ACTA 8(2.2)* * or ITA 20(12) (federal sch 21 line 130)',
          tn: '006'
        },
        {
          'header': 'F<br>Federal\n' +
          'non-business\n' +
          'foreign tax\n' +
          'credit from\n' +
          'federal schedule\n' +
          '21 line 180',
          tn: '008'
        },
        {
          'header': 'G<br>(E - F) X C'
        },
        {
          'header': 'H<br>Allowable Credit\n' +
          'Lesser of D or G',
          'tn': '012',
          'total': true,
          'totalNum': '014',
          totalTn: '014',
          'totalMessage': 'Total Allowable Credits: sum of amounts in column H'
        }
      ],
      'hasTotals': true
    }
  }
})();
