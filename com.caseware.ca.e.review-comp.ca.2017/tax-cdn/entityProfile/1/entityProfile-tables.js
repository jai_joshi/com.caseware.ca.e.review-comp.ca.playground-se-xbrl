(function() {
  wpw.tax.create.tables('entityProfile', {
    "405": {
      "infoTable": true,
      "maxLoop": 100000,
      "showNumbering": true,
      "columns": [
        {
          "header": "First name",
          colClass: 'std-input-col-width',
          type: 'text'
        },
        {
          "type": "none",
          colClass: 'std-spacing-width'
        },
        {
          "header": "Last name",
          colClass: 'std-input-col-width',
          type: 'text'
        },
        {
          "type": "none",
          colClass: 'std-spacing-width'
        },
        {
          "header": "SIN",
          colClass: 'std-input-col-width',
          type: 'text'
        },
        {
          "type": "none"
        }
      ]
    },
    "504": {
      "infoTable": true,
      "maxLoop": 100000,
      "showNumbering": true,
      "columns": [
        {
          "header": "First name",
          colClass: 'std-input-col-width',
          type: 'text'
        },
        {
          "type": "none",
          colClass: 'std-spacing-width'
        },
        {
          "header": "Last name",
          colClass: 'std-input-col-width',
          type: 'text'
        },
        {
          "type": "none",
          colClass: 'std-spacing-width'
        },
        {
          "header": "SIN",
          colClass: 'std-input-col-width',
          type: 'text'
        },
        {
          "type": "none"
        }
      ]
    }
  })
})();
