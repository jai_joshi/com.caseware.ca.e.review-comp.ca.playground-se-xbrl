(function () {


  wpw.tax.create.calcBlocks('entityProfile', function (calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      var entityNameCategories = {
        1: {name: 'Person', desc: '201'},
        2: {name: 'Corporation', desc: '301'},
        3: {name: 'Trust', desc: '401'},
        4: {name: 'Partnership', desc: '501'}
      };

      var repeatFormName = entityNameCategories[field('101').get()].name;
      var repeatFormDesc = field(entityNameCategories[field('101').get()].desc).get();
      if (repeatFormDesc != '' && angular.isDefined(repeatFormDesc)) {
        repeatFormName = repeatFormName + ' - ' + repeatFormDesc;
      }

      field('repeatName').assign(repeatFormName);
    });

  });
})();
