(function() {
  'use strict';

  wpw.tax.create.formData('entityProfile', {
    formInfo: {
      abbreviation: 'entityProfile',
      title: 'Group Entities Selection Workchart',
      headerImage: 'cw',
      isRepeatForm: true,
      repeatFormData: {
        titleNum: 'repeatName'
      },
      category: 'Workcharts'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: 'Type of entity :', num: '101',
            type: 'infoField',
            inputType: 'dropdown',
            init: '1',
            showValues: 'before',
            options: [
              {
                value: '1',
                option: 'Person'
              },
              {
                value: '2',
                option: 'Corporation'
              },
              {
                value: '3',
                option: 'Trust'
              },
              {
                value: '4',
                option: 'Partnership'
              }
            ]
          },
          {
            label: 'NOTE: User must enter SIN, Business Number, Partnership Number, Trust Number',
            labelClass: 'bold fullLength'
          }
        ]
      },
      {
        showWhen: {fieldId: '101', compare: {is: '1'}},
        rows: [
          {type: 'infoField', label: 'First name', width: '15%', num: '201'},
          {type: 'infoField', label: 'Middle name', width: '15%', num: '202'},
          {type: 'infoField', label: 'Last name', width: '15%', num: '203'},
          {type: 'infoField', label: 'Nick name (If any)', width: '15%', num: '204'},
          {
            type: 'infoField',
            label: 'Gender',
            width: '15%',
            num: '205',
            inputType: 'dropdown',
            init: '0',
            showValues: 'before',
            options: [
              {
                value: '0',
                option: 'N/A'
              },
              {
                value: '1',
                option: 'Male'
              },
              {
                value: '2',
                option: 'Female'
              }
            ]
          },
          {type: 'infoField', label: 'Date of birth', width: '15%', num: '206', inputType: 'date'},
          {
            type: 'infoField',
            label: 'SIN',
            width: '15%',
            num: '207',
            validate: {
              or: [
                {check: 'isEmpty'},
                {compare: {equal: 'NA'}},
                {
                  and: [
                    {compare: {between: [100000000, 999999999]}},
                    {check: 'mod10'}
                  ]
                }
              ]
            }
          },
          {
            type: 'infoField',
            label: 'Is this person a client?',
            width: '15%',
            num: '208',
            inputType: 'dropdown',
            init: '0',
            showValues: 'before',
            options: [
              {
                value: '0',
                option: 'N/A'
              },
              {
                value: '1',
                option: 'Yes'
              },
              {
                value: '2',
                option: 'No'
              }
            ]
          },
          {
            type: 'infoField',
            label: 'If this person is a client, what is the Customer Reference #?',
            width: '15%',
            num: '209'
          },
          {
            type: 'infoField',
            label: 'Marital Status',
            width: '15%',
            num: '210',
            inputType: 'dropdown',
            init: '0',
            showValues: 'before',
            options: [
              {
                value: '0',
                option: 'N/A'
              },
              {
                value: '1',
                option: 'Married'
              },
              {
                value: '2',
                option: 'Single'
              },
              {
                value: '3',
                option: 'Widower'
              },
              {
                value: '4',
                option: 'Common-law'
              }
            ]
          },
          {
            type: 'infoField',
            inputType: 'address',
            add1Num: '211',
            add2Num: '212',
            provNum: '213',
            cityNum: '214',
            countryNum: '215',
            postalCodeNum: '216'
          }
        ]
      },
      {
        showWhen: {fieldId: '101', compare: {is: '2'}},
        rows: [
          {type: 'infoField', label: 'Name of the Corporation', width: '15%', num: '301'},
          {type: 'infoField', label: 'BDA (doing business as) if Applicable', width: '15%', num: '302'},
          {
            type: 'infoField',
            label: 'Business Number (Federal)',
            inputType: 'custom',
            format: ['{N9}RC{N4}', 'NR'],
            num: '303'
          },
          {type: 'infoField', label: 'Fiscal Period: Start Date', width: '15%', num: '304', inputType: 'date'},
          {type: 'infoField', label: 'Fiscal Period: End Date', width: '15%', num: '305', inputType: 'date'},
          {
            type: 'infoField',
            label: 'Type of Corporation',
            inputType: 'dropdown',
            num: '306',
            init: '1',
            options: [
              {
                option: '1. Canadian Controlled Private Corporation - CCPC',
                value: '1'
              },
              {option: '2. Other private corporation', value: '2'},
              {option: '3. Public Corporation', value: '3'},
              {option: '4. Corporation controlled by a public corporation', value: '4'},
              {option: '5. Other', value: '5'}
            ]
          },
          {
            type: 'infoField',
            'inputType': 'dropdown',
            num: '307',
            label: 'Provincial or territorial jurisdiction',
            options: [
              {'option': 'Alberta', 'value': 'AB'},
              {'option': 'British Columbia', 'value': 'BC'},
              {'option': 'Manitoba', 'value': 'MB'},
              {'option': 'New Brunswick', 'value': 'NB'},
              {'option': 'Newfoundland and Labrador', 'value': 'NL'},
              {'option': 'Newfoundland and Labrador Offshore', 'value': 'XO'},
              {'option': 'Nova Scotia', 'value': 'NS'},
              {'option': 'Nova Scotia Offshore', 'value': 'NO'},
              {'option': 'Northwest Territories', 'value': 'NT'},
              {'option': 'Nunavut', 'value': 'NU'},
              {'option': 'Ontario', 'value': 'ON'},
              {'option': 'Prince Edward Island', 'value': 'PE'},
              {'option': 'Quebec', 'value': 'QC'},
              {'option': 'Saskatchewan', 'value': 'SK'},
              {'option': 'Yukon Territories', 'value': 'YT'},
              {'option': 'Multi-jurisdiction', 'value': 'MJ'},
              {'option': 'Outside Canada', 'value': 'OC'}
            ]
          }
        ]
      },
      {
        showWhen: {fieldId: '101', compare: {is: '3'}},
        rows: [
          {type: 'infoField', label: 'Name of Trust', width: '15%', num: '401'},
          {
            type: 'infoField',
            inputType: 'custom',
            format: ['T{N8}', 'NA'],
            label: 'Trust Number',
            width: '15%',
            num: '415'
          },
          {
            type: 'infoField',
            label: 'Type of Trust',
            width: '15%',
            num: '402',
            inputType: 'dropdown',
            init: '0',
            showValues: 'before',
            options: [
              {
                value: '0',
                option: 'N/A'
              },
              {
                value: '1',
                option: 'Discretionary trust'
              },
              {
                value: '2',
                option: 'Non-discretionary'
              },
              {
                value: '3',
                option: 'Reversionary trust'
              }
            ]
          },
          {type: 'infoField', label: 'Name of Trustee', width: '15%', num: '403'},
          {type: 'infoField', label: 'Name of Executor', width: '15%', num: '404'},
          {label: 'Name(s) of Beneficiaries:'},
          {type: 'table', num: '405'},
          {
            type: 'infoField',
            inputType: 'address',
            add1Num: '406',
            add2Num: '407',
            provNum: '408',
            cityNum: '409',
            countryNum: '410',
            postalCodeNum: '411'
          }
        ]
      },
      {
        showWhen: {fieldId: '101', compare: {is: '4'}},
        rows: [
          {type: 'infoField', label: 'Name of Partnership', width: '15%', num: '501'},
          {
            type: 'infoField',
            label: 'Partnership Number',
            inputType: 'custom',
            format: ['{N9}RZ{N4}', 'NR'],
            num: '511'
          },
          {
            type: 'infoField',
            label: 'Type of Partnership',
            width: '15%',
            num: '502',
            inputType: 'dropdown',
            init: '0',
            showValues: 'before',
            options: [
              {
                value: '0',
                option: 'N/A'
              },
              {
                value: '1',
                option: 'General Partnerships'
              },
              {
                value: '2',
                option: 'Limited Partnerships'
              },
              {
                value: '3',
                option: 'Limited Liability Partnerships'
              }
            ]
          },
          {type: 'infoField', label: 'Name of General Partner', width: '15%', num: '503'},
          {label: 'Name of all Limited Partners'},
          {type: 'table', num: '504'},
          {
            type: 'infoField',
            inputType: 'address',
            add1Num: '505',
            add2Num: '506',
            provNum: '507',
            cityNum: '508',
            countryNum: '509',
            postalCodeNum: '510'
          }
        ]
      }
    ]
  });
})();