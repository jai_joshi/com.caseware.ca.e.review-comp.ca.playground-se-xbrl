(function() {
  wpw.tax.create.diagnostics('entityProfile', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.bnCheck('303'));
    diagUtils.diagnostic('BN', common.bnCheck('415'));
    diagUtils.diagnostic('BN', common.bnCheck('511'));

  });
})();
