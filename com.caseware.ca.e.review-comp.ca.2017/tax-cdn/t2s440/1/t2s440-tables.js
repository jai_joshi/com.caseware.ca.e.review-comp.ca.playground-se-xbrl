(function() {

  function getTableTaxInc(labelsObj) {
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {type: 'none', colClass: 'std-input-width'},
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {

          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '2': {num: labelsObj.num[0]},
          '3': {label: 'x'},
          '4': {
            label: labelsObj.label[1],
            labelClass: 'center',
            cellClass: 'singleUnderline'
          },
          '6': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '7': {label: '='},
          '8': {num: labelsObj.num[2]},
          '9': {label: labelsObj.indicator || ''}
        },
        {
          '2': {type: 'none'},
          '4': {
            label: labelsObj.label[2],
            labelClass: 'center fullLength'
          },
          '6': {num: labelsObj.num[3]},
          '8': {type: 'none'}
        }
      ]
    }
  }

  function getTableRate(labelsObj) {
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'small-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '1': {num: labelsObj.num[0]},
          '2': {label: ' x ', labelClass: 'center'},
          '3': {num: labelsObj.num[1], decimals: labelsObj.decimals},
          '4': {label: ' %= '},
          '6': {num: labelsObj.num[2]},
          '7': {label: labelsObj.indicator}
        }]
    }
  }

  function getTableProRate(labelsObj) {
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'small-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '1': {num: labelsObj.num[0]},
          '2': {label: 'x', labelClass: 'center'},
          '3': {
            label: labelsObj.label[1],
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          '5': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '6': {label: 'x'},
          '7': {num: labelsObj.num[2], decimals: 2},
          '8': {label: '%='},
          '9': {num: labelsObj.num[3]},
          '10': {label: labelsObj.indicator}
        },
        {
          '1': {type: 'none'},
          '3': {
            label: labelsObj.label[2],
            labelClass: 'center'
          },
          '5': {num: labelsObj.num[4]},
          '7': {type: 'none'},
          '9': {type: 'none'}
        }
      ]
    }
  }

  wpw.tax.create.tables('t2s440', {
    '900': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'small-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-input-col-width'
        }
      ],
      cells: [
        {
          '0': {label: '3 . Foreign business income tax credit deductible at line 636** of the T2 return'},
          '1': {num: '901'},
          '2': {label: 'x', labelClass: 'center'},
          '3': {
            label: 'a***',
            labelClass: 'center'
          },
          '5': {num: '902'},
          '6': {label: '='},
          '7': {num: '903'},
          '8': {label: '3'}
        }
      ]
    },

    '950': getTableProRate({
      label: ['The lesser of amounts A and B', 'Taxable income for Yukon', 'Taxable income for all provinces **'],
      indicator: 'G',
      num: ['951', '952', '953', '954', '955']
    }),

    '1000': getTableProRate({
      label: ['The lesser of amounts C and F', 'Taxable income for Yukon', 'Taxable income for all provinces **'],
      indicator: 'H',
      num: ['1001', '1002', '1003', '1004', '1005']
    })

  })
})();
