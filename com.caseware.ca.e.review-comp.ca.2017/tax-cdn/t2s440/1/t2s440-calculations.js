(function() {

  wpw.tax.create.calcBlocks('t2s440', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('500', 'T2S27', '200');
      calcUtils.getGlobalValue('851', 'T2S443', '504');
      calcUtils.getGlobalValue('854', 'CP', 'Days_Fiscal_Period');
      field('509').assign(Math.max(field('500').get() - field('851').get(), 0));
      calcUtils.getGlobalValue('510', 'T2J', '360');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //amount 1 and 2 deduction
      //Applies only to corporations that were Canadian-controlled private corporations throughout the tax year
      var isCCPC = (field('T2J.040').get() == 1);
      if (isCCPC) {
        field('511').assign(field('851').get());
        calcUtils.getGlobalValue('512', 'T2J', '440');
      } else {
        field('511').assign(0);
        field('512').assign(0);
      }
      //amount 3
      calcUtils.getGlobalValue('901', 'T2J', '636');
      calcUtils.getGlobalValue('902', 'T2S27', '950');
      field('903').assign(field('901').get() * field('902').get());
      field('513').assign(field('511').get() + field('512').get() + field('903').get());
      field('514').assign(field('513').get());
      field('515').assign(field('510').get() - field('514').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var taxableIncomeAllocation = calcUtils.getTaxableIncomeAllocation('YT');
      field('952').assign(taxableIncomeAllocation.provincialTI);
      field('955').assign(taxableIncomeAllocation.allProvincesTI);

      field('952').source(taxableIncomeAllocation.provincialTISourceField);
      field('955').source(taxableIncomeAllocation.allProvincesTISourceField);

      field('1002').assign(field('952').get());
      field('1005').assign(field('955').get());

      //amount I and J
      field('951').assign(Math.min(field('500').get(), field('851').get()));
      field('1001').assign(Math.min(field('509').get(), field('515').get()));

    });

    calcUtils.calc(function(calcUtils, field, form) {
      //get value from rate table
      calcUtils.getGlobalValue('953', 'ratesYt', '101');
      calcUtils.getGlobalValue('1003', 'ratesYt', '102');
      field('954').assign(field('951').get() * field('952').get() / field('955').get() * field('953').get() / 100);
      field('1004').assign(field('1001').get() * field('1002').get() / field('1005').get() * field('1003').get() / 100);
      field('516').assign(field('954').get() + field('1004').get());
    });

  });
})();
