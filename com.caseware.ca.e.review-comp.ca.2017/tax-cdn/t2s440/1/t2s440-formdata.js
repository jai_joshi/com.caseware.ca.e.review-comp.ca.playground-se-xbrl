(function() {

  wpw.tax.create.formData('t2s440', {
    formInfo: {
      abbreviation: 't2s440',
      title: 'Yukon Manufacturing and processing profits tax credit',
      schedule: 'Schedule 440',
      formFooterNum: 'T2 SCH 440 E (17)',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if your corporation had a permanent establishment (as defined in section 400' +
              ' of the federal <i>Income Tax Regulations</i>) in Yukon at any time in the tax year, and had:',
              sublist: [
                  'taxable income earned in the tax year in Yukon; and',
                  'Canadian manufacturing and processing profits, as defined in subsection 125.1(3) of the federal' +
                  '<i> Income Tax Act</i>, earned in the tax year in Yukon.'
              ]
            },
            {
              label: 'This schedule is a worksheet only and is not required to be filed with your T2 <i>Corporation ' +
              'Income Tax Return</i>.'
            }
          ]
        }
      ],
      category: 'Yukon Forms'
    },
    sections: [
      {
        'header': 'Calculation of Yukon manufacturing and processing profits tax credit',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Canadian manufacturing and processing profits for the year from line 200 in Part 9 of Schedule 27',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '500'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Amount E from Schedule 443',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '851'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Subtotal (amount A minus amount B)',
            'labelClass': 'bold text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '509'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Taxable income from line 360 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '510'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Deduct the total of:',
            'labelClass': 'bold'
          },
          {
            'label': '1. Amount B*',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '511'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '1'
                }
              }
            ]
          },
          {
            'label': '2. Aggregate investment income from line 440 of the T2 return*',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '512'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '2'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '900'
          },
          {
            'label': 'Subtotal (add lines 1, 2, and 3)',
            'labelClass': 'bold text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '513'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '514'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Subtotal (amount D minus amount E)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '515'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'type': 'table',
            'num': '950'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Yukon manufacturing and processing profits tax credit</b> (amount G <b>plus</b> amount H)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '516'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Enter amount I on line 677 of Schedule 5.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Applies only to corporations that were Canadian-controlled private corporations throughout the tax year'
          },
          {
            'label': '** Calculate the amount of foreign business income tax credit without reference to the corporate tax reductions under section 123.4 of the federal <i>Income Tax Act</i>.'
          },
          {
            'label': '*** Use amount a from Part 9 of Schedule 27.'
          },
          {
            'label': '**** Includes the territories and the offshore jurisdictions for Nova Scotia and Newfoundland and Labrador.'
          }
        ]
      }
    ]
  });
})();
