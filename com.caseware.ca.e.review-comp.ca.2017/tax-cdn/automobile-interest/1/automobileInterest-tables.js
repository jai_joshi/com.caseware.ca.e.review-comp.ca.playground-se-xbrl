(function() {
  wpw.tax.global.tableCalculations.automobileInterest = {
    "100": {
      "fixedRows": true,
      "infoTable": false,
      "columns": [
          {
        "header": "Description",
        cellClass: 'alignLeft',
        type: 'text'
      },
        {
          "header": "Date financing began",
          "type": "date",
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          "header": "Date financing terminated",
          "type": "date",
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        }],
      "cells": [{
        "0": {
          "num": "105"
        },
        "1": {
          "num": "110"
        },
        "2": {
          "num": "115"
        }
      }]
    }
  }
})();
