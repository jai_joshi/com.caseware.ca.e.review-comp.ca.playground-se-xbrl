(function () {

  wpw.tax.create.calcBlocks('automobileInterest', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
    //Part 1 automobile interest
    calcUtils.getGlobalValue('310', 'CP','Days_Fiscal_Period');
    calcUtils.getGlobalValue('315', 'ratesFed', '831');
    field('316').assign(field('315').get() / 30);
    calcUtils.multiply('320', ['310', '316']);
    calcUtils.equals('321', '320');
    calcUtils.min('325', ['305', '321']);
    if (!angular.isDefined(field('110').get()) || !angular.isDefined(field('115').get())) {
      field('330').assign(0);
    }
    else {
      calcUtils.subtract('330', '305', '325');
    }
    calcUtils.sumBucketValues('340', ['330', '335']);
    })
  });
})();