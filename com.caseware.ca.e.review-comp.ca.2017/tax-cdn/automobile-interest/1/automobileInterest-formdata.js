(function() {
  'use strict';

  wpw.tax.global.formData.automobileInterest = {
      formInfo: {
        abbreviation: 'automobileInterest',
        title: 'Non-Deductible Automobile Interest and Other Expenses',
        headerImage: 'cw',
        isRepeatForm: true,
        repeatFormData:{
          linkedRepeatTable: 'automobileSummary.100',
          titleNum: '105'
        },
        category: 'Workcharts'
      },
      sections: [
        {
          hideFieldset: true,
          rows: [
            {
              'type': 'table',
              'num': '100'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Interest expenses paid or payable for the fiscal year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '305'
                  }
                }
              ],
              'indicator': 'A'
            },
            {
              'label': '# of days in the fiscal period in which interest was paid ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '310',
                    'filters': ''
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'a'
                  }
                }
              ]
            },
            {
              'label': 'Monthly limit',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '315'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'b'
                  }
                }
              ]
            },
            {
              'label': 'The daily amount allocated (amount b / 30 days)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '316'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'c'
                  }
                }
              ]
            },
            {
              'label': 'Subtotal (amount a multiply by amount c)',
              'labelClass': 'bold text-right',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '320'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '321'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'B'
            },
            {
              'label': 'Deductible interest expenses (lesser of A and B)',
              'labelClass': 'bold text-right',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '325'
                  }
                }
              ],
              'indicator': 'C'
            },
            {
              'label': 'Non-deductible interest expenses (line A - line C)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '330'
                  }
                }
              ]
            },
            {
              'label': 'Other non-deductible automobile expenses',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '335'
                  }
                }
              ]
            },
            {
              'label': 'Non-deductible interest expenses and other expenses',
              'labelClass': 'bold text-right',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '340'
                  }
                }
              ],
              'indicator': 'D'
            }
          ]
        }
      ]
    };
})();
