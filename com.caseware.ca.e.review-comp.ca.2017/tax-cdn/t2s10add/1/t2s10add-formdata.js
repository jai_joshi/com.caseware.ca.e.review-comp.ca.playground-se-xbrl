(function() {
  'use strict';

  wpw.tax.global.formData.t2s10add = {
      formInfo: {
        abbreviation: 'T2S10ADD',
        isRepeatForm: true,
        repeatFormData: {
          titleNum: '1024',
          linkedRepeatTable: 't2s10w.1002',
          nestedLayer: true
        },
        title: 'Eligible Capital Property Acquisition and Disposition History',
        neededRepeatForms: ['t2s10w'],
        schedule: 'SCHEDULE 10 ADDITIONAL',
        showCorpInfo: true,
        category: 'Workcharts'
      },
      sections: [
        {
        header: 'Capital Property Information',
          rows: [
            {labelClass: 'fullLength'},
            {
              type: 'infoField',
              num: '1023',
              inputType: 'dropdown',
              inputClass: 'std-input-col-width-3',
              options: [
                {option: 'Goodwill', value: '1'},
                {option: 'Customer lists', value: '2'},
                {option: 'Ledger accounts', value: '3'},
                {option: 'Trademarks', value: '4'},
                {option: 'Patents, franchises and licences with no limited period', value: '5'},
                {option: 'Expenses of incorporation, reorganization or amalgamation', value: '6'},
                {option: 'Milk and eggs quotas', value: '7'},
                {option: 'Initiation or admission fees', value: '8'},
                {option: 'Appraisal costs', value: '9'},
                {option: 'Legal and accounting fees', value: '10'},
                {option: 'Government rights', value: '11'},
                {option: 'Other', value: '12'}
              ],
              disabled: true,
              label: 'Type of eligible capital property'
            },
            {
              label: 'Business name:',
              type: 'infoField', inputClass: 'std-input-col-width-3',
              num: '1024',
              disabled: true
            },
            {type: 'horizontalLine'},
            {label: 'The Above is Updated From S10 Workchart', labelClass: 'fullLength bold center'},
            {type: 'horizontalLine'},
            // {
            //   label: 'Update this form from:', num: '1020',//TODO: need to create choose file button
            //   type: 'infoField', inputType: 'dropdown', init: '1',
            //   showValues: 'before',
            //   options: [
            //     {value: '1', option: 'No update, enter Data Directly'},
            //     {
            //       value: '2',
            //       option: 'From a CSV file (from Excel; Google Sheets; QuickBooks; Sage (Simply Accounting) '
            //     }
            //   ]
            // },
            {type: 'infoField', label: 'Sub-GL A/C # (if applicable)', inputClass: 'std-input-col-width-3', num: '1021'},
            {type: 'infoField', label: 'Description of GL A/C', inputClass: 'std-input-col-width-3', num: '1022'},
            {
              label: 'Description of the property:',
              type: 'infoField', inputClass: 'std-input-col-width-3',
              num: '1025'
            },
            {
              label: 'Location',
              type: 'infoField', inputClass: 'std-input-col-width-3',
              num: '1026'
            },
            {labelClass: 'fullLength'},
            {
              'type': 'table', 'num': '1004'
            },
            {labelClass: 'fullLength'},
            {labelClass: 'fullLength'},
            {
              'type': 'table', 'num': '1005'
            }
          ]
        }
      ]
    };
})();
