(function() {

  function updateFromSourceForm(calcUtils, sourceRepeatId) {
    var sourceFullFormID = 'T2S10W-' + sourceRepeatId;
    calcUtils.getGlobalValue('1023', sourceFullFormID, '1000');
    calcUtils.getGlobalValue('1024', sourceFullFormID, '1001');
  }

  wpw.tax.create.calcBlocks('t2s10add', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field, form) {
      if (field('sourceRepeatId').get()) {
        updateFromSourceForm(calcUtils, field('sourceRepeatId').get());
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //table 1004 calcs
      var table1004Row = field('1004').getRow(0);
      table1004Row[3].assign(table1004Row[1].get() - table1004Row[2].get());
      table1004Row[5].assign(table1004Row[3].get() + table1004Row[4].get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //table 1005 calcs
      var table1005Row = field('1005').getRow(0);
      table1005Row[3].assign(Math.max(0, table1005Row[1].get() - table1005Row[2].get()));
      table1005Row[5].assign(table1005Row[3].get() + table1005Row[4].get());
    });
  });
})();
