(function() {
  wpw.tax.global.tableCalculations.t2s10add = {
    '1004': {
      'fixedRows': true,
      'superHeaders': [{
        'header': 'Acquisition',
        'colspan': '6'
      }],
      'columns': [{
        colClass: 'std-input-width',
        'header': 'Acquisition date',
        'type': 'date'
      },
        {
          colClass: 'std-input-width',
          'header': 'Cost of acquisition',
          'formField': true
        },
        {
          colClass: 'std-input-width',
          'header': 'Government assistance',
          'formField': true
        },
        {
          colClass: 'std-input-width',
          'header': 'Cost before other adjustments',
          'formField': true,
          'disabled': true
        },
        {
          colClass: 'std-input-width',
          'header': 'Add: Other adjustments',
          'formField': true
        },
        {
          colClass: 'std-input-width',
          'header': 'Net Cost',
          'formField': true,
          'disabled': true
        }],
      'cells': [{
        '0': {
          'num': '1120'
        },
        '1': {
          'num': '1121'
        },
        '2': {
          'num': '1122'
        },
        '3': {
          'num': '1123'
        },
        '4': {
          'num': '1124'
        },
        '5': {
          'num': '1125'
        }
      }]
    },
    '1005': {
      'fixedRows': true,
      'superHeaders': [{
        'header': 'Disposition',
        'colspan': '6'
      }],
      'columns': [{
        colClass: 'std-input-width',
        'header': 'Disposition date',
        'type': 'date'
      },
        {
          colClass: 'std-input-width',
          'header': 'Proceeds of disposition',
          'formField': true
        },
        {
          colClass: 'std-input-width',
          'header': 'Outlays and expenses',
          'formField': true
        },
        {
          colClass: 'std-input-width',
          'header': 'Proceeds before other adjustments',
          'formField': true,
          'disabled': true
        },
        {
          colClass: 'std-input-width',
          'header': 'Add: Other adjustments',
          'formField': true
        },
        {
          colClass: 'std-input-width',
          'header': 'Net Proceeds',
          'formField': true,
          'disabled': true
        }],
      'cells': [{
        '0': {
          'num': '1131'
        },
        '1': {
          'num': '1126'
        },
        '2': {
          'num': '1127'
        },
        '3': {
          'num': '1128'
        },
        '4': {
          'num': '1129'
        },
        '5': {
          'num': '1130'
        }
      }]
    }
  }
})();
