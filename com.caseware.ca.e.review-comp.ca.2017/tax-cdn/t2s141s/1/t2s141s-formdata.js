(function() {
  'use strict';

  wpw.tax.global.formData.t2s141s = {
      formInfo: {
        abbreviation: 't2s141s',
        title: 'Notes Detail',
        schedule: 'Schedule 141',
        category: 'GIFI',
        disclaimerShowWhen: {fieldId: 'cp.184', check: 'isYes'},
        disclaimerTextField: {formId: 'cp', fieldId: '183'}
      },
      sections: [
        {
        header: 'Notes to the financial statements',
          rows: [
            {
              type: 'infoField',
              inputType: 'radio',
              num: '101',
              label: 'Attach this form to the Tax Return, when it contains useful information not ' +
              'included in any of the prescribed forms',
              labelWidth: '70%',
              init: '1'
            },
            {labelClass: 'fullLength'},
            {type: 'horizontalLine'},
            {
              type: 'infoField',
              inputType: 'radio',
              num: '100',
              label: 'Do you want to EFILE this form?',
              labelWidth: '70%',
              init: '1'
            },
            {label: 'Note', labelClass: 'bold'},
            {
              label: 'The CRA limits the size of the transmission to 4 million bytes. During transmission, ' +
              'if CorpTax advises you that you are exceeding this limit, please select "No" for ' +
              'this question. You must then submit this form by fax or mail.',
              labelClass: 'fullLength'
            },
            {labelClass: 'fullLength'},
            {type: 'horizontalLine'},
            {
              type: 'infoField',
              inputType: 'textArea',
              num: '102'
            }
          ]
        }
      ]
    };
})();
