(function() {

  wpw.tax.create.calcBlocks('t2s400', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('200', 'T2J', '360');
      calcUtils.getGlobalValue('201', 'T2S125', '3680');
      // calcUtils.getGlobalValue('202', 'T2S125', '');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.equals('099', '098');
      calcUtils.sumBucketValues('101', ['099', '100']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.equals('113', '101');
      calcUtils.sumBucketValues('114', ['112', '113']);
      // calcUtils.equals('115', '304');
      calcUtils.sumBucketValues('116', ['114', '115']);
      // calcUtils.equals('117', '309');
      calcUtils.multiply('118', ['116'], 0.25);
      calcUtils.subtract('123', '117', '118');
      calcUtils.sumBucketValues('146', ['123', '134', '135']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('300', 'ratesSk', '400');
      calcUtils.getGlobalValue('301', 'ratesSk', '401');
      calcUtils.getGlobalValue('302', 'ratesSk', '402');
      calcUtils.getGlobalValue('303', 'ratesSk', '403');
      calcUtils.getGlobalValue('304', 'ratesSk', '404');
      calcUtils.getGlobalValue('305', 'ratesSk', '410');
      calcUtils.getGlobalValue('306', 'ratesSk', '411');
      calcUtils.getGlobalValue('307', 'ratesSk', '412');
      calcUtils.getGlobalValue('308', 'ratesSk', '413');
      calcUtils.getGlobalValue('309', 'ratesSk', '414');

      calcUtils.equals('150', '101');
      calcUtils.equals('151', '115');
      calcUtils.sumBucketValues('152', ['150', '151']);
      if (field('123').get() >= 0) {
        calcUtils.multiply('153', ['123'], -1);
      }
      else {
        field('153').assign(Math.min(field('123').get() * -1, field('134').get() + field('135').get()));
      }
      calcUtils.sumBucketValues('156', ['152', '153']);
      field('500').assign(field('T2J.360').get() == 0 ? 0 : field('156').get() * field('T2S411.100').get() / field('T2J.360').get());
      field('501').assign(field('T2J.100').get() == 0 ? 0 : field('500').get() * field('T2S411.306').get() / field('T2J.100').get());
      calcUtils.sumBucketValues('167', ['501', '178', '179']);
      calcUtils.getGlobalValue('190', 'T2S411', '306');
      // calcUtils.getGlobalValue('195', 'T2S411', '306');
      calcUtils.subtract('192', '190', '195');
      field('180').assign(Math.min(field('167').get(), field('192').get()));
      calcUtils.subtract('193', '192', '180');
      calcUtils.subtract('191', '167', '180');

    });


  })
}());
