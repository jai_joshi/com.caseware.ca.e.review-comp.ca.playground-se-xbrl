(function() {

  wpw.tax.create.formData('t2s400', {
    formInfo: {
      abbreviation: 'T2S400',
      title: 'S400 - Saskatchewan Royalty Tax Rebate Calculation',
      schedule: 'Schedule 400',
      code: 'Code 0301',
      formFooterNum: 'T2 SCH 400 E (05)',
      description: [
        {
          type: 'list',
          items: [
            'For use by all corporations that maintained a permanent establishment in Saskatchewan in the taxation year and had <b>attributed Canadian royalties and taxes</b> as defined in subsection 2(1) of The <i>Saskatchewan Royalty Tax Rebate Regulations</i>.',
            'Complete two copies of this form. Attach one to each T2 return you file and send the other to the Department of Finance,<br>Government of Saskatchewan, 9th floor, 2350 Albert Street, Regina SK S4P 4A6.',
            'Sections and paragraphs referred to on this schedule are from the federal <i>Income Tax Act</i>, unless otherwise specified.',
            'The deduction of Crown royalties and mining taxes and elimination of the resource allowance are being phased in over a five-year transitional period. See <b>Chart</b> below.'
          ]
        }
      ],
      category: 'Saskatchewan Forms'
    },
    sections: [
      {
        'rows': [
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '*  At the end of the taxation year and before any deductions for the year'
          }
        ]
      },
      {
        'header': 'Attributed Canadian royalties and taxes calculation',
        'rows': [
          {
            'label': 'Attributed Canadian royalties and taxes as a result of transactions with Saskatchewan',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '098'
                }
              },
              {
                'input': {
                  'num': '099'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Attributed Canadian royalties and taxes as a result of transactions with territories or other provinces',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Attributed Canadian royalties and taxes per para. 2(1)(a) of The Saskatchewan Royalty Tax Rebate Regulations (amount A plus amount B) (see <b>Chart</b> below)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '101'
                },
                'padding': {
                  'type': 'tn',
                  'data': '101'
                }
              }
            ],
            'indicator': 'C'
          }
        ]
      },
      {
        'header': 'Depletion adjustment factor calculation (for 1976 and later taxation years)',
        'rows': [
          {
            'label': 'Resource profits per Part XII of the federal Income Tax Regulations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '112'
                },
                'padding': {
                  'type': 'tn',
                  'data': '112'
                }
              }
            ]
          },
          {
            'label': 'Less: Attributed Canadian royalties and taxes (amount C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '113'
                }
              }
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '114'
                }
              }
            ]
          },
          {
            'label': '<b>Add:</b> Resource allowance deducted under paragraph 20(1)(v.1) (see <b>Chart</b> below)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '115'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': '(if negative, enter "0") Total',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '116'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Depletion allowance deducted under section 65',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '117'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Amount E × 25%',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '118'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Depletion adjustment factor (amount F minus amount G: can be a positive or a negative amount)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '123'
                },
                'padding': {
                  'type': 'tn',
                  'data': '123'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': 'Saskatchewan depletion adjustment carryforward at the end of the preceding taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '134'
                },
                'padding': {
                  'type': 'tn',
                  'data': '134'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Saskatchewan depletion adjustment transferred on amalgamation or wind-up of subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '135'
                },
                'padding': {
                  'type': 'tn',
                  'data': '135'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': 'Saskatchewan depletion adjustment carryforward for the taxation year (add amounts H, I, and J ) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '146'
                },
                'padding': {
                  'type': 'tn',
                  'data': '146'
                }
              }
            ],
            'indicator': 'K'
          }
        ]
      },
      {
        'header': 'Chart',
        'rows': [
          {
            'type': 'table',
            'num': '3000'
          },
          {
            'label': 'Prorate these percentages using the number of days in each period  in your taxation year'
          }
        ]
      },
      {
        'header': 'Tax rebate calculation',
        'rows': [
          {
            'label': 'Attributed Canadian royalties and taxes (amount C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150'
                }
              }
            ]
          },
          {
            'label': '<b>Less:</b> Resource allowance deducted under paragraph 20(1)(v.1) (amount D)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '151'
                }
              }
            ]
          },
          {
            'label': 'Subtotal',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '152'
                }
              }
            ]
          },
          {
            'label': 'Depletion adjustment : If amount H is positive or "0," <b>subtract</b> amount H. <br>' +
            'If amount H is negative, <b>add</b> the lesser of: amount H x (- 1.0) and the total of amount I plus amount J',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '153'
                }
              }
            ]
          },
          {
            'label': 'Adjusted attributed Canadian royalties and taxes (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '156'
                },
                'padding': {
                  'type': 'tn',
                  'data': '156'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '4000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Add:</b> Royalty tax rebate transferred on amalgamation or wind-up of subsidiary **',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '178'
                },
                'padding': {
                  'type': 'tn',
                  'data': '178'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'label': '<b>Add:</b> Royalty tax rebate carryforward at the end of the preceding taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '179'
                },
                'padding': {
                  'type': 'tn',
                  'data': '179'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'label': 'Royalty tax credit (add amounts N, O, and P)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '167'
                },
                'padding': {
                  'type': 'tn',
                  'data': '167'
                }
              }
            ],
            'indicator': 'Q'
          },
          {
            'label': 'Saskatchewan tax payable before credits',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '190'
                }
              }
            ],
            'indicator': 'R'
          },
          {
            'label': '<b>Less:</b> Saskatchewan tax credits, if applicable',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '195'
                }
              }
            ],
            'indicator': 'S'
          },
          {
            'label': 'Saskatchewan tax payable before rebate (amount R minus amount S)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '192'
                }
              }
            ],
            'indicator': 'T'
          },
          {
            'label': '<b>Less:</b> Saskatchewan royalty tax rebate (lesser of amount Q and amount T): Enter on line 632 of Schedule 5',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '180'
                },
                'padding': {
                  'type': 'tn',
                  'data': '180'
                }
              }
            ],
            'indicator': 'U'
          },
          {
            'label': 'Saskatchewan tax payable after rebate (amount T minus amount U)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '193'
                }
              }
            ],
            'indicator': 'V'
          },
          {
            'label': 'Royalty tax rebate carryforward at the end of the taxation year (amount Q minus amount U) **',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '191'
                },
                'padding': {
                  'type': 'tn',
                  'data': '191'
                }
              }
            ],
            'indicator': 'W'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'asterisk',
                'items': [
                  {
                    'label': 'When a corporation has no taxable income for the taxation year, see paragraph 7(b) of The Saskatchewan Royalty Tax Rebate Regulations to determine the available credit for the taxation year.'
                  },
                  {
                    'label': 'See section 10 of The Saskatchewan Royalty Tax Rebate Regulations.'
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  });
})();
