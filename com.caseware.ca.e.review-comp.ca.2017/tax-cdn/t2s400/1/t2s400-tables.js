(function() {

  wpw.tax.global.tableCalculations.t2s400 = {
    '1000': {
      fixedRows: true,
      "infoTable": true,
      columns: [
        {type: 'none'},
        {
          header: 'Balance before current-year deduction',
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Current-year deduction',
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {label: 'Drilling and exploration expenses (ITAR 29)'},
          '1': {num: '400'},
          '3': {num: '401'}
        },
        {
          '0': {label: 'Canadian exploration and development expenses (section 66)'},
          '1': {num: '402'},
          '3': {num: '403'}
        },
        {
          '0': {label: 'Cumulative Canadian exploration expenses (section 66.1)'},
          '1': {num: '404'},
          '3': {num: '405'}
        },
        {
          '0': {label: 'Cumulative Canadian development expenses (section 66.2)'},
          '1': {num: '406'},
          '3': {num: '407'}
        }
      ]
    },
    '2000': {
      fixedRows: true,
      "infoTable": true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          type: 'none',
          colClass: 'std-padding-width',
          cellClass: 'alignCenter'
        },
        {
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          type: 'none',
          colClass: 'std-padding-width',
          cellClass: 'alignCenter'
        },
        {
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          type: 'none',
          colClass: 'std-padding-width',
          cellClass: 'alignCenter'
        },
        {
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          width: '5px'
        },
        {
          type: 'none',
          cellClass: 'alignCenter',
          width: '153px'
        },
        {
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {label: 'Taxable income from the T2 return'},
          '1': {label: '&#10148;'},
          '2': {num: '200'},
          '3': {label: 'Net earnings from the financial statements'},
          '4': {label: '&#10148;'},
          '5': {num: '201'},
          '6': {label: 'Earned depletion base *'},
          '7': {label: '&#10148;'},
          '8': {num: '202'}
        }
      ]
    },
    '3000': {
      fixedRows: true,
      "infoTable": true,
      columns: [
        {type: 'none'},
        {
          header: '2003',
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          width: '5px',
          type: 'none'
        },
        {
          header: '2004',
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          width: '5px',
          type: 'none'
        },
        {
          header: '2005',
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          width: '5px',
          type: 'none'
        },
        {
          header: '2006',
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          width: '5px',
          type: 'none'
        },
        {
          header: 'After 2006',
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {label: 'Percentage of Crown royalties and taxes on resource production prohibited from deduction, and percentage of resource allowance allowed as a deduction'},
          '1': {num: '300'},
          '3': {num: '301'},
          '5': {num: '302'},
          '7': {num: '303'},
          '9': {num: '304'}
        },
        {
          '0': {label: 'Deductible percentage of Crown royalties and mining taxes'},
          '1': {num: '305'},
          '3': {num: '306'},
          '5': {num: '307'},
          '7': {num: '308'},
          '9': {num: '309'}
        }
      ]
    },
    '4000': {
      fixedRows: true,
      "infoTable": true,
      columns: [
        {type: 'none'},
        {
          type: 'none',
          width: '100px'
        },
        {
          type: 'none',
          colClass: 'std-padding-width',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          width: '100px'
        },
        {colClass: 'std-input-width'},
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: 'Adjusted attributed Canadian royalties and taxes allocated to Saskatchewan'},
          '1': {label: '&#123; Amount L'},
          '2': {label: 'x'},
          '3': {label: 'Taxable income earned in Saskatchewan* &#125;', cellClass: 'singleUnderline'},
          '5': {num: '500'},
          '6': {label: 'M'}
        },
        {
          '3': {label: 'Taxable income'},
          '5': {type: 'none'}
        },
        {
          '0': {label: 'Available credit for the taxation year'},
          '1': {label: '&#123; Amount M'},
          '2': {label: 'x'},
          '3': {label: 'Saskatchewan tax payable before credits &#125;', cellClass: 'singleUnderline'},
          '5': {num: '501'},
          '6': {label: 'N'}
        },
        {
          '3': {label: 'Taxable income earned in Saskatchewan'},
          '5': {type: 'none'}
        }
      ]
    }
  };
})();
