(function() {
  function fillArray(array, start, end) {
    for (var i = start; i <= end; i = i + 1) {
      array.push(i)
    }
  }

  wpw.tax.create.calcBlocks('t2s27', function(calcUtils) {
    calcUtils.calc(function(calcUtils) {
      var num = 901;
      var num2 = 907;
      var midnum = 906;
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 1 calculations //


      if (field('T2S7.525').get() > 0) {
        calcUtils.getGlobalValue('100', 'T2S7', '525');//check with CRATest002Mar2017 and Comp003 taxPrep calcs
      } else {
        calcUtils.getGlobalValue('100', 'T2S7', '545');//check with CRATest002Mar2017 and Comp003 taxPrep calcs and create a loss on s125
      }

      calcUtils.sumBucketValues('110', ['100', '105']);
      if (field('110').get() > 200000){
        field('091').assign('2');
      }else {
          field('091').assign('1');
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 3 calculations //
      var eligibility = field('090').get() == 1 && field('091').get() == 1 && field('092').get() == 1 && field('093').get() == 1;
      if (eligibility) {
        field('120').assign(0)
      }
      else {
        calcUtils.getGlobalValue('120', 'T2S7', '525');
      }
      calcUtils.equals('301', '354');
      calcUtils.subtract('303', '301', '302');
      calcUtils.equals('304', '303');
      calcUtils.sumBucketValues('306', ['304', '305']);
      calcUtils.equals('125', '306');
      calcUtils.subtract('130', '120', '125');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 2 calculations // moving it below to part 3 as it gets value from part 3 calculate fields// applicable only
      calcUtils.equals('015', '130');
      calcUtils.equals('020', '150');
      calcUtils.equals('025', '170');
      calcUtils.equals('035', '140');
      calcUtils.equals('040', '160');
      field('030').assign((field('035').get() + field('040').get()) == 0 ? 0 :
          field('015').get() *
          (field('020').get() + field('025').get()) /
          (field('035').get() + field('040').get())
      );

    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 4 calculations //
      var sum314 = [];
      fillArray(sum314, 308, 313);
      calcUtils.sumBucketValues(314, sum314);
      calcUtils.multiply('315', ['314'], 1 / 10);
      calcUtils.sumBucketValues('140', ['315', '316', '317']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 5 calculations //
      calcUtils.equals('318', '140');
      calcUtils.multiply('line150', ['319'], 100 / 85);
      calcUtils.min('150', ['line150', '318']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 6 calculations //
      var sum325 = [];
      fillArray(sum325, 321, 324);
      calcUtils.sumBucketValues(325, sum325);
      calcUtils.equals('326', '325');
      calcUtils.subtract('327', '320', '326');
      calcUtils.sumBucketValues('329', ['327', '328']);
      var sum333 = [];
      fillArray(sum333, 330, 332);
      calcUtils.sumBucketValues('333', sum333);
      calcUtils.equals('334', '333');
      var sum339 = [];
      fillArray(sum339, 335, 338);
      calcUtils.sumBucketValues(339, sum339);
      calcUtils.equals('340', '339');
      calcUtils.subtract('341', '334', '340');
      calcUtils.sumBucketValues('343', ['341', '342']);
      calcUtils.sumBucketValues('160', ['329', '343']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 7 calculations //
      calcUtils.sumBucketValues('346', ['344', '345']);
      calcUtils.multiply('line170', ['346'], 100 / 75);
      calcUtils.min('170', ['line170', '160']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 8 Calculations //
      calcUtils.sumBucketValues('349', ['347', '348']);
      calcUtils.sumBucketValues('352', ['350', '351']);
      calcUtils.equals('353', '352');
      calcUtils.subtract('354', '349', '353');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var eligibility = field('090').get() == 1 && field('091').get() == 1 && field('092').get() == 1 && field('093').get() == 1;
      // Part 9 Calculations //

      if (eligibility) {
        field('200').assign(field('100').get())
      }
      else {
        field('200').assign(field('030').get())
      }
      var line400T2J = field('T2J.400').get();
      var line405T2J = field('T2J.405').get();
      var line410T2J = field('T2J.410').get();
      var line425T2J = field('T2J.425').get();
      var num305RateFed = field('ratesFed.305').get();
      field('355').assign(Math.min(line400T2J, line405T2J, line410T2J, line425T2J));
      calcUtils.subtract('356', '200', '355');
      calcUtils.equals('357', '356');
      calcUtils.getGlobalValue('358', 'T2J', '360');
      calcUtils.equals('359', '355');
      calcUtils.getGlobalValue('360', 'T2J', '440');
      calcUtils.getGlobalValue('904', 'T2J', '636');
      field('950').assign(
          (1 / (0.38 - num305RateFed / 100)));
      calcUtils.multiply('905', ['904', '950']);
      calcUtils.sumBucketValues('361', ['359', '360', '905']);
      calcUtils.equals('362', '361');
      calcUtils.subtract('363', '358', '362');
      calcUtils.min('901', ['357', '363']);
      calcUtils.getGlobalValue('906', 'ratesFed', '305');
      calcUtils.multiply('907', ['901', '906'], 1 / 100);
      calcUtils.equals('365', '417');
      calcUtils.sumBucketValues('366', ['907', '365']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 11 calculations //
      calcUtils.equals('375', '140');
      calcUtils.multiply('line205', ['376'], 100 / 85);
      calcUtils.min('205', ['line205', '375']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 10 calculations// moving it below to part 11 as it gets value from part 11 calculate fields
      calcUtils.equals('369', '130');
      calcUtils.equals('370', '205');
      calcUtils.equals('371', '206');
      calcUtils.equals('373', '140');
      calcUtils.equals('374', '160');
      field('372').assign((field('373').get() + field('374').get()) == 0 ? 0 :
          field('369').get() *
          (field('370').get() + field('371').get()) /
          (field('373').get() + field('374').get())
      );
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 12 calculations //
      calcUtils.sumBucketValues('380', ['378', '379']);
      calcUtils.multiply('line206', ['380'], 100 / 75);
      calcUtils.min('206', ['line206', '160']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var num305RateFed = field('ratesFed.305').get();
      // Part 13 calculations //
      calcUtils.equals('210', '372');
      calcUtils.equals('381', '355');
      calcUtils.subtract('382', '210', '381');
      calcUtils.equals('383', '382');
      calcUtils.getGlobalValue('384', 'T2J', '360');
      calcUtils.equals('385', '381');
      calcUtils.getGlobalValue('386', 'T2J', '440');
      calcUtils.getGlobalValue('387', 'T2J', '636');
      calcUtils.field('951').assign((1 / (0.38 - num305RateFed / 100)));
      calcUtils.multiply('388', ['904', '951']);
      calcUtils.sumBucketValues('389', ['385', '386', '388']);
      calcUtils.equals('390', '389');
      calcUtils.subtract('391', '384', '390');
      calcUtils.min('392', ['383', '391']);
      calcUtils.min('393', ['357', '363']);
      calcUtils.subtract('394', '392', '393');
      calcUtils.equals('395', '394');
      calcUtils.multiply('417', ['395'], 13 / 100);
    });
  });
})
();
