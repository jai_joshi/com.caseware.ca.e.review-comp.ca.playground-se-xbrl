(function() {
  wpw.tax.global.tableCalculations.t2s27 = {
    'di_table_1': {
      'num': 'di_table_1',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Lesser of amount B9 and amount H9',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '901'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '906',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '907'
          },
          '8': {
            'label': 'I9'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    '368': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
          {
        cellClass: 'alignCenter',
        'type': 'none'
      },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          'width': '20px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '150px',
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          'width': '150px',
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          'width': '5px',
          cellClass: 'alignLeft',
          'type': 'none'
        },
        {
          'width': '7px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '40px',
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        }],
      'cells': [{
        '0': {
          'label': 'ADJUBI'
        },
        '1': {
          'num': '369'
        },
        '2': {
          'label': 'x'
        },
        '3': {
          'label': '[ MCA (Part 11, line 205)',
          cellClass: 'alignCenter singleUnderline'
        },
        '4': {
          'num': '370',
          cellClass: 'singleUnderline'
        },
        '5': {
          'label': ' + MLA (Part 12, line 206)',
          cellClass: 'alignCenter singleUnderline'
        },
        '6': {
          'num': '371',
          cellClass: 'singleUnderline'
        },
        '7': {
          'label': '<b> ] </b>'
        },
        '8': {
          'label': ' <b> = </b>'
        },
        '9': {
          'label': '<b> (MPA) </b>'
        },
        '10': {
          'num': '372',
          'inputClass': 'doubleUnderline'
        }
      },
        {
          '0': {
            'label': '(Part 3, line 130)'
          },
          '1': {
            'type': 'none'
          },
          '3': {
            'label': '[ C (Part 4, line 140)'
          },
          '4': {
            'num': '373'
          },
          '5': {
            'label': '+ L (Part 6, line 160)'
          },
          '6': {
            'num': '374'
          },
          '7': {
            'label': '<b> ] </b>'
          },
          '8': {
            'type': 'none'
          },
          '9': {
            'type': 'none'
          },
          '10': {
            'type': 'none'
          }
        }]
    },
    '418': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        'width': '110px',
        'type': 'none'
      },
        {
          'width': '250px',
          'type': 'none'
        },
        {
          'width': '165px',
          'type': 'none'
        },
        {
          'width': '165px',
          'type': 'none'
        },
        {
          'type': 'none'
        }],
      'cells': [{
        '1': {
          'label': '<b> Term'
        },
        '3': {
          'label': '<b> <i> Income Tax Regulations </i> </b>'
        }
      },
        {},
        {
          '1': {
            'label': 'Gross cost'
          },
          '3': {
            'label': '5202 and 5204'
          }
        },
        {
          '1': {
            'label': 'Net resource income'
          },
          '3': {
            'label': '5203(3)'
          }
        },
        {
          '1': {
            'label': 'Qualified activities'
          },
          '3': {
            'label': '5202'
          }
        },
        {
          '1': {
            'label': 'Rental cost'
          },
          '3': {
            'label': '5202'
          }
        },
        {
          '1': {
            'label': 'Resource activities'
          },
          '3': {
            'label': '5203(2)'
          }
        },
        {
          '1': {
            'label': 'Salaries and wages'
          },
          '3': {
            'label': '5202'
          }
        }]
    },
    '1000': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        'width': '40px',
        'type': 'none'
      },
        {
          'type': 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width',
          formField: true
        },
        {
          'width': '55px',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }],
      'cells': [{
        '1': {
          'label': 'depreciable assets eligible for capital cost allowance under Schedule II of the Regulations'
        },
        '2': {
          'num': '308',
          filters: 'number'
        },
        '3': {
          'label': 'A4'
        },
        '4': {
          'type': 'none'
        }
      },
        {
          '1': {
            'label': 'timber limits and cutting rights (other than a timber resource property'
          },
          '2': {
            'num': '309',
            filters: 'number'
          },
          '3': {
            'label': 'B4'
          },
          '4': {
            'type': 'none'
          }
        },
        {
          '1': {
            'label': 'immovable wood assets (class 15)'
          },
          '2': {
            'num': '310',
            filters: 'number'
          },
          '3': {
            'label': 'C4'
          },
          '4': {
            'type': 'none'
          }
        },
        {
          '1': {
            'label': 'industrial mineral mines'
          },
          '2': {
            'num': '311',
            filters: 'number'
          },
          '3': {
            'label': 'D4'
          },
          '4': {
            'type': 'none'
          }
        },
        {
          '1': {
            'label': 'capital expenditures for scientific research and experimental development'
          },
          '2': {
            'num': '312',
            filters: 'number'
          },
          '3': {
            'label': 'E4'
          },
          '4': {
            'type': 'none'
          }
        },
        {
          '1': {
            'label': 'Part XVII property'
          },
          '2': {
            'num': '313',
            cellClass: 'singleUnderline',
            filters: 'number'
          },
          '3': {
            'label': 'F4'
          },
          '4': {
            'type': 'none'
          }
        },
        {
          '1': {
            'label': 'Subtotal (total of amounts A4 to F4)',
            'labelCellClass': 'alignRight'
          },
          '2': {
            'num': '314',
            cellClass: 'doubleUnderline',
            filters: 'number'
          },
          '3': {
            'label': 'x 10%='
          },
          '4': {
            'num': '315',
            filters: 'number'
          },
          '5': {
            'label': 'G4'
          }
        }]
    },
    '1001': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          'type': 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'small-input-width',
          'disabled': true
        },
        {
          colClass: 'small-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '1': {
            'label': 'Foreign business income tax credit deductible at line 636' + '(note 2)'.sup() + 'of the T2 return'
          },
          '3': {
            'num': '904'
          },
          '4': {
            'label': 'x '
          },
          '5': {
            'num': '950'
          },
          '6': {
            'label': ' (note3)'.sup() + ' ='
          },
          '7': {
            'num': '905'
          },
          '8': {
            'label': 'F9 '
          }
        }]
    },
    '1002': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          'type': 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'small-input-width',
          'disabled': true
        },
        {
          colClass: 'small-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [{
        '1': {
          'label': 'Foreign business income tax credit deductible at line 636 ' + '(note 2)'.sup() + ' of the T2 return'
        },
        '3': {
          'num': '387'
        },
        '4': {
          'label': 'x'
        },
        '5': {
          'num': '951'
        },
        '6': {
          'label': '(note 3)'.sup() + '='
        },
        '7': {
          'num': '388'
        },
        '8': {
          'label': 'F13'
        }
      }]
    },
    '010': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          'width': '20px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '150px',
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          'width': '150px',
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          'width': '5px',
          cellClass: 'alignLeft',
          'type': 'none'
        },
        {
          'width': '7px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '40px',
          cellClass: 'alignCenter',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        }],
      'cells': [
          {
        '0': {
          'label': 'ADJUBI'
        },
        '1': {
          'num': '015',
          disabled: true
        },
        '2': {
          'label': 'x'
        },
        '3': {
          'label': '[ MC (Part 5, line 150)',
          cellClass: 'singleUnderline'
        },
        '4': {
          'num': '020',
          disabled: true,
          cellClass: 'singleUnderline'
        },
        '5': {
          'label': ' + ML (Part 7, line 170)',
          cellClass: 'singleUnderline'
        },
        '6': {
          'num': '025',
          disabled: true,
          cellClass: 'singleUnderline'
        },
        '7': {
          'label': '<b> ] </b>'
        },
        '8': {
          'label': ' <b> = </b>'
        },
        '9': {
          'label': ' <b> (MP) </b>'
        },
        '10': {
          'num': '030',
          cellClass: 'doubleUnderline',
          disabled: true
        }
      },
        {
          '0': {
            'label': '(Part 3, line 130)'
          },
          '1': {
            'type': 'none'
          },
          '3': {
            'label': '[ C (Part 4, line 140)'
          },
          '4': {
            'num': '035',
            disabled: true
          },
          '5': {
            'label': '+ L (Part 6, line 160)'
          },
          '6': {
            'num': '040',
            disabled: true
          },
          '7': {
            'label': '<b> ] </b>'
          },
          '8': {
            'type': 'none'
          },
          '9': {
            'type': 'none'
          },
          '10': {
            'type': 'none'
          }
        }]
    }
  }
})();
