(function() {
  'use strict';

  wpw.tax.global.formData.t2s27 = {
    formInfo: {
      abbreviation: 'T2S27',
      title: 'Calculation of Canadian Manufacturing and Processing Profits Deduction',
      //subTitle: '(2010 and later tax years)',
      showCorpInfo: true,
      schedule: 'Schedule 27',
      code: 'Code 1601',
      formFooterNum: 'T2 SCH 27 E (17)',
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            'Small manufacturing corporations that meet requirements 1 through 4 in Part 1 of this schedule ' +
            'should complete Part 1 only. All other corporations should complete Part 2 except those that only' +
            ' generate electrical energy for sale or produce steam for sale. Corporations that generate electrical' +
            ' energy for sale or produce steam for sale must complete Part 10 on page 5.',
            'Income that is eligible for the small business deduction is not eligible for the manufacturing ' +
            'and processing profits deduction.',
            'All legislative references are to the <i>Income Tax Act</i> and the <i> Income Tax ' +
            'Regulations.</i>',
            //'See Interpretation Bullet IT-145R, <i> Canadian Manufacturing and Processing Profits - Reduced ' +
            //'Rate of Corporate Tax </i>, for more information.',
            'See page 7 for notes and definitions to help you complete this schedule.'
          ]
        }
      ],
      category: 'Federal Tax Forms'

    },
    sections: [
      {
        'header': 'Part 1 - Small manufacturing corporations',
        'rows': [
          {
            'label': 'See notes 1 to 7 on page 7.'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'To qualify as a small manufacturer, the corporation has to meet all of the following requirements:'
          },
          {
            'label': '1. its activities during the year were mainly manufacturing or processing in Canada of goods for sale or lease;',
            'type': 'infoField',
            'inputType': 'radio',
            'labelClass': 'fullLength',
            'labelWidth': '85%',
            'num': '090'
          },
          {
            'label': '2. the following calculation totals $200,000 or less: active business income <b> minus </b> active<br> business losses of the corporation for the year [this includes the corporation\'s share of<br> active business income and active business loss for the fiscal period of each partnership<br> of which the corporation was a member at any time in its year (see i) below]',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: '<b>Add:</b>'
          },
          {
            'label': 'the active business income for the tax year of each Canadian corporation with which the' +
            ' corporation was associated in the year (see ii) below))',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              }
            ]
          },
          {
            'label': 'Total (line 100 <b>plus</b> line 105)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              }
            ]
          },
          {
            'label': 'The corporation has met the second requirement to qualify as a small manufacturer',
            'type': 'infoField',
            'inputType': 'radio',
            'labelCellClass': 'indent',
            'labelWidth': '85%',
            'num': '091',
            'cannotOverride': true
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '3. it was not engaged at any time during the year in any activities as set out in subsections 5201 (c) to (c.3) of the Regulations.',
            'type': 'infoField',
            'inputType': 'radio',
            'labelClass': 'fullLength',
            'labelWidth': '85%',
            'num': '092'
          },
          {
            'label': '4. the corporation did not carry on any active business outside Canada at any time during the year.',
            'type': 'infoField',
            'inputType': 'radio',
            'labelClass': 'fullLength',
            'labelWidth': '85%',
            'num': '093'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> If the corporation meets requirements 1 through 4, its Canadian manufacturing and processing profits are equal to line 100 above. </b>',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Enter this amount on line 200 in Part 9 of this schedule. </b>',
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the corporation does not meet requirements 1 through 4,  complete Part 2 below.'
          },
          {labelClass: 'fullLength'},
          {
            'label': '<b> Partnerships and associated corporations </b>'
          },
          {
            'label': 'On a separate sheet of paper attached to this form, please give the following information (if it applies):',
            'labelClass': 'fullLength'
          },
          {
            label: 'i) for partnerships – give the name, partnership identification number, and total income or loss ' +
            'from each active business carried on by each partnership of which the corporation was a member at any time ' +
            'in its tax year; and',
            'labelClass': 'tabbed'
          },
          {
            label: 'ii) for associated corporations – give the name, Business Number, and total income from each active' +
            ' business for the tax year carried on by each Canadian corporation with which the corporation was associated ' +
            'in the year.',
            'labelClass': 'tabbed'
          }
        ]
      },
      {
        'header': 'Part 2 - Corporations that do not qualify as small manufacturers',
        'rows': [
          {
            'label': 'See note 6 on page 7.'
          },
          {labelClass: 'fullLength'},
          {
            'label': '<b> Canadian manufacturing and processing profits (MP) </b>'
          },
          {
            'type': 'table',
            'num': '010'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter amount MP at line 200 in Part 9 of this schedule.'
          }
        ]
      },
      {
        'header': 'Part 3 - Adjusted business income (ADJUBI)',
        'spacing': 'R_tn2',
        'rows': [
          {
            'label': 'See notes 1, 3, and 5 on page 7.'
          },
          {
            'label': 'Active business income <b> minus </b> active business losses of the corporation for the year [this includes<br> the corporation\'s share of active business income and active business loss for the fiscal period of<br> each partnership of which the corproation was a member at any time in its year (see i) in Part 1)]',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '120',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              }
            ]
          },
          {
            label: '<b>Deduct: </b>'
          },
          {
            'label': 'Net resource income (amount G8 from Part 8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '301'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'A3'
                }
              },
              null
            ]
          },
          {
            label: '<b>Minus: </b>'
          },
          {
            'label': 'Net  resource adjustment per subsection 5203(3.1) of the Regulations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '302'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B3'
                }
              },
              null
            ]
          },
          {
            'label': 'Excess (amount A3 <b>minus</b> amount B3, if negative, enter "0")',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '303'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '304'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C3'
                }
              }
            ]
          },
          {
            'label': 'Refund interest as defined in subsection 5203(4) of the Regulations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '305'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D3'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount C3 <b> plus </b> amount D3)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '306',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '125',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '125'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Adjusted business income (ADJUBI)</b> (line 120 <b>minus</b> line 125, if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              }
            ]
          },
          {
            'label': 'Enter this amount in Part 2, and Part 10 if applicable.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 4 - Cost of capital (C)',
        'rows': [
          {
            'label': 'See notes 3, 5, 8, and 12 on page 7.'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Gross cost of the following property that the corporation owned at the end of the year' +
            ' and used at any time during the year:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: 'Rental cost for the use of property, which would be included in G4 if it were owned by ' +
            'the corporation at the end of the year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '316'
                }
              }
            ],
            'indicator': 'H4'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: 'The corporation\'s share of the amounts that would be determined under amounts G4 and H4 ' +
            'for a partnership of which the corporation was a member at any time in the year, if one were ' +
            'to substitute "partnership" for "corporation" and "its fiscal period" for "the year"',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '317'
                }
              }
            ],
            'indicator': 'I4'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Cost of capital (C)</b> (<b>add</b> amounts G4, H4, and I4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '140',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '140'
                }
              }
            ]
          },
          {
            'label': 'Enter this amount in Parts 2 and 5, and Parts 10 and 11 if applicable.'
          }
        ]
      },
      {
        'header': 'Part 5 - Cost of manufacturing and processing capital (MC)',
        'rows': [
          {
            'label': 'See note 3, 5, and 12 on page 7.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Cost of capital (from line 140, Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '318'
                }
              }
            ],
            'indicator': 'A5'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The part of amount A5 that reflects the extent to which each property was used directly in ' +
            'qualified activities of the corporation during the year or in qualified activities of a partnership ' +
            'for the fiscal period of a partnership of which the corporation was a member at any time in the year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '319'
                }
              }
            ],
            'indicator': 'B5'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Cost of manufacturing and processing capital (MC) </b> <br> (amount B5 <b>multiplied</b> ' +
            'by 100 <b>divided</b> by 85, cannot be more than amount A5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '150'
                }
              }
            ]
          },
          {
            label: 'Enter this amount in Part 2.'
          }
        ]
      },
      {
        'header': 'Part 6 - Cost of labour (L)',
        'rows': [
          {
            'label': 'See notes 3, 5, 9, 10, and 12 on page 7.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Salaries and wages paid or payable to all employees for services performed during the year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '320'
                }
              }
            ],
            'indicator': 'A6'
          },
          {
            'label': 'Salaries and wages included in A6 that were:'
          },
          {
            'label': 'included in the gross cost of property (Part 4) other than property manufactured and leased during the year to other persons',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '321'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B6'
                }
              }
            ]
          },
          {
            'label': 'related to an active business carried on outside Canada',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '322'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C6'
                }
              }
            ]
          },
          {
            'label': 'related to activities engaged in for the purpose of earning Canadian resource profits as defined in section5202 of the Regulations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '323'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D6'
                }
              }
            ]
          },
          {
            'label': 'included in the corporation\'s Canadian or foreign exploration and development expenses',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '324'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E6'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts B6 to E6)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '325'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '326'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'F6'
          },
          {
            'label': 'Subtotal (amount A6 minus amount F6)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '327'
                }
              }
            ],
            'indicator': 'G6'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Salaries and wages referred to in amount A6, <b> minus </b> the deductions in amount F6 for the fiscal period of a partnership of which the corporation was a member at any time in its year - corporation\'s share (attach calculation)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '328'
                }
              }
            ],
            'indicator': 'H6'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal - salaries and wages (amount G6 <b>plus</b> amount H6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '329'
                }
              }
            ],
            'indicator': 'I6'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amounts paid or payable during the year to non-employees for performing functions relating to:'
          },
          {
            'label': 'management and administration',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '330'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'J6'
                }
              }
            ]
          },
          {
            'label': 'scientific research and experimental development',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '331'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'K6'
                }
              }
            ]
          },
          {
            'label': 'any service or function normally performed by employees of the corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '332'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'L6'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts J6 to L6)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '333'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '334'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'M6'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amounts included in amount M6 that were:'
          },
          {
            'label': 'included in the gross cost of property (Part 4), other than property manufactured by the corporation and leased during the year to other persons',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '335'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'N6'
                }
              }
            ]
          },
          {
            'label': 'related to an active business carried on outside Canada',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '336'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'O6'
                }
              }
            ]
          },
          {
            'label': 'related to activities engaged in for the purpose of earning Canadian resource profits as defined in section 5202 of the Regulations',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '337'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'P6'
                }
              }
            ]
          },
          {
            'label': 'included in the corporation\'s Canadian or foreign exploration and development expenses',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '338'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'Q6'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts N6 to Q6)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '339'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '340'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'R6'
          },
          {
            'label': 'Subtotal (amount M6 <b>minus</b> amount R6)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '341'
                }
              }
            ],
            'indicator': 'S6'
          },
          {
            'label': 'Amounts referred to in amount M6, <b> minus </b> the deductions in amount R6 for the fiscal ' +
            'period of a partnership of which the corporation was a member at any time in its year -' +
            ' corporation\'s share (attach calculation)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '342'
                }
              }
            ],
            'indicator': 'T6'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Subtotal - other payments (amount S6 <b>plus</b> amount T6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '343'
                }
              }
            ],
            'indicator': 'U6'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Cost of labour (L)</b> (amount I6 <b>plus</b> amount U6) - enter this amount in Part 2, and Part 10 if applicable',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 7 - Cost of manufacturing and processing labour (ML)',
        'rows': [
          {
            'label': 'See notes 3, 5, and 12 on page 7.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: 'Part of salaries and wages (included in amount I6, Part 6) that was paid or payable to employees' +
            ' for the time they were directly engaged in qualified activities of the corporation during the year or of ' +
            'the partnership during its fiscal period',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '344'
                }
              }
            ],
            'indicator': 'A7'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Part of other payments (included in amount U6, Part 6) that was paid or payable to ' +
            'non-employees for performing functions that would be directly related to qualified activities of ' +
            'the corporation during the year or of the partnership during its fiscal period, if they had been employees' +
            ' of the corporation or partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '345'
                }
              }
            ],
            'indicator': 'B7'
          },
          {
            'label': 'Total (amount A7 <b>plus</b> amount B7)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '346'
                }
              }
            ],
            'indicator': 'C7'
          },
          {
            'label': '<b> Cost of manufacturing and processing labour (ML)</b> <br>(amount C7 <b>multiplied</b>' +
            ' by 100 <b>divided</b> by 75, cannot be more than the amount on line 160 in Part 6)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '170',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '170'
                }
              }
            ]
          },
          {
            label: 'Enter this amount in Part 2.'
          }
        ]
      },
      {
        'header': 'Part 8 - Net resource income',
        'rows': [
          {
            'label': '<b> For corporations with resource activities </b>'
          },
          {
            'label': 'See notes 5, 11, and 12 on page 7.'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Resource profits as defined in section 1204 of the Regulations for the year of the corporation (including its share of resource profits as a member of a partnership under subsection 1206(3) of the Regulations)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '347'
                }
              }
            ],
            'indicator': 'A8'
          },
          {
            'label': 'Amounts included in income under section 59 (other than amounts that were included in calculating resource profits), including its share of such amounts as a member of a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '348'
                }
              }
            ],
            'indicator': 'B8'
          },
          {
            'label': 'Subtotal (amount A8 <b>plus</b> amount B8)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '349'
                }
              }
            ],
            'indicator': 'C8'
          },
          {
            'label': 'Amounts deducted by the corporation under section 65 (other than amounts that were deducted in calculating resource profits)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '350'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D8'
                }
              }
            ]
          },
          {
            'label': 'The corporation\'s income from the processing of foreign ore (see note 11 on page 7)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '351'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E8'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amounts D8 <b>plus</b> amount E8)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '352'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '353'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'F8'
          },
          {
            'label': '<b> Net resource income </b> (amount C8 <b>minus</b> amount F8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '354'
                }
              }
            ],
            'indicator': 'G8'
          },
          {
            label: 'Enter this amount at line A3 in Part 3.'
          }
        ]
      },
      {
        'header': 'Part 9 - Manufacturing and processing profits deduction',
        'rows': [
          {
            'label': '<b> For eligible corporations that have such profits </b>'
          },
          {
            'label': 'Canadian manufacturing and processing profits from Part 1 or Part 2, as applicable',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              },
              null
            ]
          },
          {
            'label': 'The least of the amounts on lines 400, 405, 410, and 427 of the T2 return' + '(note 1)'.sup(),
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '355'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'A9'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 200 <b>minus</b> amounts A9)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '356'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '357'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B9'
          },
          {
            'label': 'Taxable income from line 360 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '358'
                }
              }
            ],
            'indicator': 'C9'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The least of the amounts on lines 400, 405, 410, and 427 of the T2 return' + '(note 1)'.sup(),
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '359'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D9'
                }
              }
            ]
          },
          {
            'label': 'Aggregate investment income from line 440 of the T2 return' + '(note 1)'.sup(),
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '360'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E9'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '1001'
          },
          {
            'label': 'Subtotal (total of amounts D9 to F9)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '361'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '362'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'G9'
          },
          {
            'label': 'Subtotal (amount C9 <b>minus</b> amount G9)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '363'
                }
              }
            ],
            'indicator': 'H9'
          },
          {
            'type': 'table',
            'num': 'di_table_1'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Amount L13 from Part 13 if the corporation is also claiming a deduction for generating electrical energy for sale or producing steam for sale',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '365'
                }
              }
            ],
            'indicator': 'J9'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Manufacturing and processing profits deduction</b> (amount I9 <b>plus</b> amount J9)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '366'
                }
              }
            ],
            'indicator': 'K9'
          },
          {
            'label': 'Enter this amount at line 616 of the T2 return.'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Note 1: Applies only to corporations that were Canadian-controlled private corporations ' +
            'throughout the tax year.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 2: Calculate the amount of foreign business income tax credit without reference to the' +
            ' corporate tax reductions under section 123.4.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 3: General tax reduction for the tax year being 13% since January 1, 2012, ' +
            'the equation 1/(0.38 - X) becomes: 1/(0.38 - 0.13) = 1/(0.25) = 4',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'hideFieldset': true,
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Corporations that generate electrical energy for sale or produce steam for sale',
            labelClass: 'fullLength titleFont center'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'A corporation that only generates electrical energy for sale, or produces steam for sale, will need to complete Part 10. If the corporation has other eligible activities besides generating electrical energy or producing steam, it will need to complete Part 2 and Part 10.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Complete Part 10 using all manufacturing and processing profits, including generating electrical energy for sale or producing steam for sale',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Complete Part 2 using all eligible activities other than generating electrical energy for sale or producing steam for sale.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 10 – Corporations that generate electrical energy for sale or produce steam for sale',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Canadian manufacturing and processing profits (MPA)'
          },
          {
            'type': 'table',
            'num': '368'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter amount MPA at line 210 in Part 13 of this schedule.'
          }
        ]
      },
      {
        'header': 'Part 11 – Cost of all manufacturing and processing capital (MCA)' + '(note 1)'.sup(),
        'rows': [
          {
            'label': 'See notes 3, 5, and 12 on page 7.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Cost of capital (from line 140, Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '375'
                }
              }
            ],
            'indicator': 'A11'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The part of amount A11 that reflects the extent to which each property was used directly in qualified activities of the corporation during the year or qualified activities of a partnership during its fiscal period of which the corporation was a member at any time in the year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '376'
                }
              }
            ],
            'indicator': 'B11'
          },
          {
            'labelClass': 'fulLLength'
          },
          {
            'label': '<b>Cost of manufacturing and processing capital (MCA)</b> <br>' +
            '(amount B11 <b>multiplied</b> by 100 <b>divided</b> by 85, cannot be more than amount A11)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '205',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '205'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 1: Includes capital used directly in generating electrical energy for sale or producing ' +
            'steam for sale.'
          }
        ]
      },
      {
        'header': 'Part 12 – Cost of all manufacturing and processing labour (MLA)' + '(note 1)'.sup(),
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'See notes 3, 5, and 12 on page 7.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Part of salaries and wages (amount I6, Part 6) that was paid or payable to employees for the time they were directly engaged in qualified activities of the corporation during the year or of the partnership during its fiscal period',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '378'
                }
              }
            ],
            'indicator': 'A12'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Part of other payments (amount U6, Part 6) that was paid or payable to non-employees for performing functions that would be directly related to qualified activities of the corporation during the year or of the partnership during its fiscal period, if they had been employees of the corportion or partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '379'
                }
              }
            ],
            'indicator': 'B12'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total (amount A12 <b>plus</b> amount B12)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '380'
                }
              }
            ],
            'indicator': 'C12'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Cost of manufacturing and processing labour (MLA) </b> <br>(amount C12 <b>multiplied</b> by 100 <b>divided</b> by 75, cannot be more than amount 160 in part 6<br>Enter this amount in Part 10',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '206',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '206'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 1: Includes labour used directly in generating electrical energy for sale or' +
            ' producing steam for sale.'
          }
        ]
      },
      {
        'header': 'Part 13 - Manufacturing and processing profits deduction for generating electrical energy for sale or producing steam for sale',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> For eligible corporations that have profits from generating electrical energy for sale or producing steam for sale </b>'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Canadian manufacturing and processing profits from part 10',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '210',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '210'
                }
              },
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The least of the amounts on lines 400, 405, 410, and 427 of the T2 return' + '(note 1)'.sup(),
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '381'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'A13'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (Line 210 <b>minus</b> amount A13)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '382'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '383'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B13'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable income from line 360 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '384'
                }
              }
            ],
            'indicator': 'C13'
          },
          {
            'labelClass': 'fulLLength'
          },
          {
            'label': 'The least of the amounts on lines 400, 405, 410, and 427 of the T2 return',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '385'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D13'
                }
              }
            ]
          },
          {
            'label': 'Aggregate investment income from line 440 of the T2 return' + ' (note 1)'.sup(),
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '386'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E13'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '1002'
          },
          {
            'label': 'Subtotal (total of amounts D13 to F13)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '389'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '390'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'G13'
          },
          {
            'label': 'Subtotal(amount C13 <b>minus</b> amount G13)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '391'
                }
              }
            ],
            'indicator': 'H13'
          },
          {
            'label': 'Lesser of amount B13 and amount H13',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '392'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'I13'
                }
              }
            ]
          },
          {
            'label': 'Lesser of amount B9 and amount H9 from Part 9' + ' (note 4)'.sup(),
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '393'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'J13'
                }
              }
            ]
          },
          {
            'label': 'Subtotal(amount I13 <b>minus</b> amount J13)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '394'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '395'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'K13'
          },
          {
            'labelClass': 'fulLLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: '<b>Manufacturing and processing profits deduction for generating electrical energy for sale or ' +
            'producing steam for sale</b>' + ' (note 5)'.sup() + '(amount K13 <b>multiplied</b> by 13%)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '417'
                }
              }
            ],
            'indicator': 'L13'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 1: Applies only to corporations that were Canadian-controlled private corporations throughout' +
            ' the tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 2: Calculate the amount of foreign business income tax credit without reference to the ' +
            'corporate tax reductions under section 123.4.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 3: General tax reduction for the tax year being 13% since January 1, 2012, the equation' +
            ' 1/(0.38 - X) becomes: 1/(0.38 - 0.13) = 1/(0.25) = 4',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 4: Enter "0" if the corporation is only claiming a manufacturing and processing profits' +
            ' deduction for generating electrical energy for sale or producing steam for sale.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: 'Note 5: If the corporation is also claiming a manufacturing and processing profits deduction for ' +
            'other eligible activities, enter amount L13 on line J9 of Part 9. If the corporation is <b>only</b>' +
            ' claiming the deduction for generating electrical energy for sale or producing steam for sale, enter ' +
            'amount L13 on line 616 of the T2 return.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'hideFieldset': true,
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Notes and definitions to help you complete this schedule'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '1. <b> Active business</b>, in relation to any business carried on by a taxpayer resident in Canada, means any business other than a specified investment business or a personal services business.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: '2. <b>Active business income of an associated corporation</b> includes its share of the active ' +
            'business income for the fiscal period of a partnership of which it was a member at any time in the year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '3. <b> Fiscal period of a partnership </b> refers to the period coinciding with or ending in the tax year of the corporation',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '4. <b> Tax year of an associated corporation </b> means any tax year that coincides with or ends in the tax year of the corporation completing this schedule',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '5. <b> Year </b> means the tax year of the corporation completing this schedule.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '6. <b> Canadian manufacturing and processing profits </b> is defined in subsection 125.1(3). There is also a definition of <b> manufacturing or processing </b> which lists activities that are not included',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '7. The term <b> associated </b> has the meaning given by section 256. See the <i> T2 Corporation Income Tax Guide </i> for more information.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: '8. To determine the cost of capital, exclude from the gross cost or rental cost, ' +
            'the part that reflects the extent the property was used during the year by the corporation or the ' +
            'partnership (section 5204 of the Regulations):',
            'labelClass': 'fullLength'
          },
          {
            'label': '(a) in an active business carried on outside Canada;',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '(b) to earn Canadian investment income or foreign investment income as defined in subsection 129(4) (assuming that the subsection applies also to partnership);',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '(c) in activities that earn Canadian resource profits as defined in section 5202 of the Regulations; or ',
            'labelClass': 'fullLength tabbed'
          },
          {
            label: '(d) in activities referred to in items (a), (b), or (e) under the definition of ' +
            '"Canadian exploration and development expenses" in subsection 66(15), items (a) or (b) under' +
            ' the definition of "foreign exploration and development expenses" in subsection 66(15), items ' +
            '(a), (c), (f), or (i) under the definition of "Canadian exploration expense" in subsection 66.1(6), ' +
            'or items (a), (c), or (g) under the definition of "Canadian development expense" in subsection 66.2(5).',
            'labelClass': 'fullLength tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: '9. Canadian exploration and development expenses, foreign exploration and development expenses, ' +
            'Canadian exploration expense, and Canadian development expense are defined in subsection 66(15), ' +
            'subsection 66.1(6), and subsection 66.2(5) respectively. The corporation\'s <b>Canadian or foreign ' +
            'exploration and development expenses</b> include the share of these expenses incurred by a partnership ' +
            'only if the corporation was a member of that partnership at the end of the partnership\'s fiscal period.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '10. <b>Salaries and wages</b> paid or payable to employees or amounts paid or payable to ' +
            'non-employees by a partnership that are included in its Canadian or foreign exploration and development' +
            ' expenses will be excluded from the calculation of the partnership\'s "Cost of labour" only if these ' +
            'exploration and development expenses can be included in the corporate partner\'s exploration and' +
            ' development expenses (see note 9 above).',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: '11. Section 5203 of the Regulations, <b>Resource income</b>, provides that a corporation\'s income' +
            ' from the processing of foreign ore be excluded from its net resource income (NRI) for the year. ' +
            'Income from foreign ore processing is generally equal to the resource profits for the year less the sum of' +
            ' Canadian resource profits for the year and the earned depletion base at the beginning of ' +
            'the immediately following tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '12. The following terms are defined in the <i> Income Tax Regulations</i>: '
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '418'
          }
        ]
      }
    ]
  };
})();
