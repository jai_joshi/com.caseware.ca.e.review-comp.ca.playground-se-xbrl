(function() {
  wpw.tax.create.diagnostics('t2s27', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0270001', common.prereq(common.and(
        common.requireFiled('T2S27'),
        function(tools) {
          return tools.field('t2j.616').isNonZero() &&
              (tools.field('100').get() > 200000 ||
                  tools.field('105').get() > 200000 ||
                  tools.field('110').get() > 200000);
        }), function(tools) {
      return tools.requireOne(tools.list(['120']), 'isNonZero') &&
          (tools.requireAll(tools.list(['140', '150']), 'isNonZero') ||
              tools.requireAll(tools.list(['160', '170']), 'isNonZero') ||
              tools.requireAll(tools.list(['210']), 'isNonZero'))
    }));

    diagUtils.diagnostic('0270003', common.prereq(common.and(
        common.requireFiled('T2S27'),
        common.check(['t2j.616'], 'isNonZero')),
        function(tools) {
          return (tools.field('cp.226').get() == '1' ?
              tools.requireAll(tools.list(['100', '105']), 'isNotDefault') :
              tools.requireOne(tools.field('100'), 'isNonZero')) ||
              tools.requireOne(tools.list(['100', '105']), 'isNonZero') ||
              (tools.requireOne(tools.list(['120']), 'isNonZero') &&
                  (tools.requireAll(tools.list(['140', '150']), 'isNonZero') ||
                      tools.requireAll(tools.list(['160', '170']), 'isNonZero') ||
                      tools.requireAll(tools.list(['210']), 'isNonZero')))
        }));

    diagUtils.diagnostic('0270004', common.prereq(common.and(
        common.requireFiled('T2S27'),
        function(tools) {
          var table50Col3 = tools.field('t2s9.050').cell(0, 3).get();
          return tools.field('t2j.616').isPositive() &&
              ((tools.field('100').get() < 200001 && tools.field('100').get() > 0) ||
                  (tools.field('110').get() < 200001 && tools.field('110').get() > 0)) &&
              tools.field('t2j.160').isChecked() ||
              (table50Col3 == 1 ||
                  table50Col3 == 2 ||
                  table50Col3 == 3);
        }), function(tools) {
      return tools.field('105').require('isFilled');
    }));

    diagUtils.diagnostic('0270005', common.prereq(common.and(
        common.requireFiled('T2S27'),
        function(tools) {
          return tools.field('210').isNonZero() &&
              tools.field('cp.tax_end').getKey('year') > 2005
        }), function(tools) {
      return tools.requireOne(tools.list(['205', '206']), 'isNonZero');
    }));

    diagUtils.diagnostic('0270006', common.prereq(common.and(
        common.requireFiled('T2S27'),
        common.check(['t2s502.150', 't2s502.250'], 'isNonZero')),
        common.requireNotFiled('T2S27')));

    diagUtils.diagnostic('027.329', common.prereq(
        common.requireFiled('T2S27'),
        function(tools) {
          return tools.field('378').require(function() {
            return this.get() <= tools.field('329').get();
          })
        }));

    diagUtils.diagnostic('027.344', common.prereq(
        common.requireFiled('T2S27'),
        function(tools) {
          return tools.field('378').require(function() {
            return this.get() >= tools.field('344').get();
          })
        }));
  });
})();

