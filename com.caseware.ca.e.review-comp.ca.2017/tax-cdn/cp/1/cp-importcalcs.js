wpw.tax.create.importCalcs('CP', function(importTools) {
  function getImportObj(taxPrepIdsObj) {
    //get and sort by repeatId and key of taxPrepIdsObj
    var output = {};
    Object.keys(taxPrepIdsObj).forEach(function(key) {
      importTools.intercept(taxPrepIdsObj[key], function(importObj) {
        output[key] = importObj;
      });
    });
    return output;
  }

  //ON corp number
  importTools.intercept('IDENT.Ident331', function(importObj) {
    importTools.form('CP').setValue('146', importObj['value'].toString()); //this is to allow leading zeros
  });

  //Same contact as preparer?
  importTools.intercept('IDENT.Ident229', function(importObj) {
    if (importObj.value == 'N') {
      importTools.form('CP').setValue('300', 2)
    }
  });

  //remove spaces from phone format
  importTools.intercept('IDENT.Ident310', function(importObj) {
    importTools.form('CP').setValue('956', importObj['value'].replace(/\s/g, ""));
  });
  importTools.intercept('IDENT.Ident216', function(importObj) {
    importTools.form('CP').setValue('959', importObj['value'].replace(/\s/g, ""));
  });
  importTools.intercept('IDENT.Ident350', function(importObj) {
    importTools.form('CP').setValue('618', importObj['value'].replace(/\s/g, ""));
  });

  //remove spaces from Fax
  importTools.intercept('IDENT.Ident330', function(importObj) {
    importTools.form('CP').setValue('639', importObj['value'].replace(/\s/g, ""));
  });


  var cpObj = {
    address: {
      headOffice: getImportObj({
        'country': 'IDENT.Ident188',
        'postal': 'IDENT.Ident189',
        'zip': 'IDENT.Ident190'
      }),
      mailing: getImportObj({
        'preparer': 'IDENT.Ident193',
        'country': 'IDENT.Ident200',
        'postal': 'IDENT.Ident201',
        'zip': 'IDENT.Ident202'
      }),
      booksRecords: getImportObj({
        'preparer': 'IDENT.Ident336',
        'headOffice': 'IDENT.Ident337',
        'country': 'IDENT.Ident208',
        'postal': 'IDENT.Ident209',
        'zip': 'IDENT.Ident210'
      })
    }
  };

  function getCorpType(taxPrepId, value) {
    importTools.intercept(taxPrepId, function(importObj) {
      if (importObj.value == 'Y') {
        importTools.form('CP').setValue('147', value)
      }
    });
  }

  getCorpType('OHSPT.Ttwspt1', 1);
  getCorpType('OHSPT.Ttwspt2', 2);
  getCorpType('OHSPT.Ttwspt11', 3);
  getCorpType('OHSPT.Ttwspt12', 4);
  getCorpType('OHSPT.Ttwspt13', 6);
  getCorpType('OHSPT.Ttwspt6', 8);
  getCorpType('OHSPT.Ttwspt7', 9);
  getCorpType('OHSPT.Ttwspt10', 10);
  getCorpType('OHSPT.Ttwspt14', 11);
  getCorpType('OHSPT.Ttwspt15', 12);
  getCorpType('OHSPT.Ttwspt16', 13);
  getCorpType('OHSPT.Ttwspt17', 14);
  getCorpType('OHSPT.Ttwspt5', 15);
  getCorpType('OHSPT.Ttwspt18', 16);
  getCorpType('OHSPT.Ttwspt19', 17);
  getCorpType('OHSPT.Ttwspt20', 18);
  getCorpType('OHSPT.Ttwspt8', 19);
  getCorpType('OHSPT.Ttwspt9', 20);
  getCorpType('OHSPT.Ttwspt21', 21);
  getCorpType('OHSPT.Ttwspt23', 22);
  getCorpType('OHSPT.Ttwspt26', 99);

  function directDepositInfo(taxPrepValue, compTaxValue) {
    importTools.intercept('IDENT.Ident304', function(importObj) {
      if (importObj.value == taxPrepValue) {
        importTools.form('CP').setValue('147', compTaxValue)
      }
    });
  }
  directDepositInfo('Y', 1);
  directDepositInfo('N', 2);

  importTools.after(function() {
    var cp = importTools.form('CP');

    function getPropIfExists(obj, properties) {
      //recursively return either the object if some multilayer property exists, or an empty string
      //i.e. if foo[1][2][3], return it; if not, return ''
      properties = wpw.tax.utilities.ensureArray(properties, false);
      return properties.length < 1 ? (obj || '') : getPropIfExists((obj || '')[properties[0]], properties.splice(1));
    }

    //head office
    cp.setValue('017', getPropIfExists(cpObj['address'], ['headOffice', 'country', 'value']));
    if (getPropIfExists(cpObj['address'], ['headOffice', 'postal', 'value'])) {
      cp.setValue('018', getPropIfExists(cpObj['address'], ['headOffice', 'postal', 'value']).replace(/[\s]/, ''))
    }
    else {
      cp.setValue('018', getPropIfExists(cpObj['address'], ['headOffice', 'zip', 'value']).replace(/[\s]/, ''))
    }

    //mailing address
    if (getPropIfExists(cpObj['address'], ['mailing', 'preparer', 'value']) == 'Y') {
      cp.setValue('194', '2');
    }
    else {
      cp.setValue('027', getPropIfExists(cpObj['address'], ['mailing', 'country', 'value']));
      if (getPropIfExists(cpObj['address'], ['mailing', 'country', 'value']) == 'CA') {
        cp.setValue('028', getPropIfExists(cpObj['address'], ['mailing', 'postal', 'value']).replace(/[\s]/, ''))
      }
      else {
        cp.setValue('028', getPropIfExists(cpObj['address'], ['mailing', 'zip', 'value']).replace(/[\s]/, ''))
      }
    }

    //books and records
    if (getPropIfExists(cpObj['address'], ['booksRecords', 'headOffice', 'value']) == 'Y') {
      cp.setValue('329', '1');
    }
    else if (getPropIfExists(cpObj['address'], ['booksRecords', 'preparer', 'value']) == 'Y') {
      cp.setValue('329', '2');
    }
    else {
      cp.setValue('329', '3');
      cp.setValue('037', getPropIfExists(cpObj['address'], ['booksRecords', 'country', 'value']));
      if (getPropIfExists(cpObj['address'], ['booksRecords', 'country', 'value']) == 'CA') {
        cp.setValue('038', getPropIfExists(cpObj['address'], ['booksRecords', 'postal', 'value']).replace(/[\s]/, ''))
      }
      else {
        cp.setValue('038', getPropIfExists(cpObj['address'], ['booksRecords', 'zip', 'value']).replace(/[\s]/, ''))
      }
    }

  });
});
