(function () {
  'use strict';

  var currencyCodes = wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.currencyCodes, 'value');
  var titlesList = wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.titleList, 'value');
  var foreign_countries = JSON.parse(JSON.stringify(wpw.tax.codes.countryAddressCodes));
  delete foreign_countries.CA;
  var foreignCountries = wpw.tax.actions.codeToDropdownOptions(foreign_countries, 'value');

  var dateTimeConditionArr = [
    {
      switchIf: {
        formId: 'cp',
        fieldId: '063',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '066',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '071',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '072',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '076',
        value: '1'
      }
    }
  ];

  wpw.tax.create.formData('cp', {
    formInfo: {
      abbreviation: 'CP',
      title: 'Tax Optimiser Checklist',
      highlightFieldsets: true,
      headerImage: 'cw',
      category: 'Start Here',
      css: 'style-cdn.css',
      showDots: false
    },
    sections: [
      {
        rows: [
          {
            label: 'Note: This version of CaseWare Review and Compilation - Canada supports fiscal periods between',
            inputClass: 'std-input-col-width-2',
            // labelClass: 'bold font-medium font-orange',
            type: 'infoField',
            inputType: 'dateRange',
            num: '195',
            cannotOverride: true,
            num2: '196'
          }
        ]
      },
      {
        header: 'Entity Profile',
        headerClass: 'font-header',
        rows: [
          {
            header: 'General Information',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {
                type: 'infoField',
                inputType: 'custom',
                format: ['{N9}RC{N4}'],
                label: 'Business Number',
                tn: '001',
                inputClass: 'std-input-col-width-2',
                num: 'bn'
              },
              {
                type: 'infoField',
                inputType: 'none',
                label: 'Corporation\'s Name (Enter operating name in S125 if it is different from the corporation’s legal name)'
              },
              {
                type: 'infoField',
                inputType: 'textArea',
                tn: '002',
                num: '002', "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]},
                maxLength: 175,
                rf: true,
                tipGuidance: 'If legal name is too long, it shall be abbreviated when printed. Maximum input length is 175 characters'
              },
              {
                type: 'table',
                num: 'taxDateTable'
              },
              {
                labelClass: 'fullLength'
              },
              {
                type: 'infoField',
                label: 'Is the tax year a full year, including a floating year-end date?',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                num: '103',
                init: '2'
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Is this first year of filing after incorporation?',
                tn: '070',
                num: '070'
              },
              {
                type: 'infoField',
                inputType: 'date',
                inputClass: 'std-input-col-width-2',
                num: '105',
                label: 'What is the date of incorporation?',
                labelClass: 'tabbed'
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Is the corporation filing an amended tax return?',
                num: '997',
                tn: '997',
                init: '2'
              },
              // {
              //   type: 'table',
              //   num: 'amendedReturnTable'
              // },
              {
                //TODO: must create a form 996 for these type of return. Special rules for EFILE CIF and T2BCR. 'Expand' to Form 996 from this field which would be a calculated cell.
                type: 'infoField',
                inputType: 'link',
                label: 'If yes, provide reasons for amendment in Form Line 996',
                labelClass: 'fullLength ',
                note: 'Go to form line 996',
                formId: 'l996',
                showWhen: {fieldId: '997'}
              },
              {
                type: 'infoField',
                inputType: 'date',
                inputClass: 'std-input-col-width-2',
                label: 'Date amended return is filed',
                num: '301',
                showWhen: '997'
              }
            ]
          },
          {
            header: 'Type of Corporation',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {
                type: 'infoField',
                label: 'What is the corporation type at year’s end?',
                tn: '040',
                inputType: 'dropdown',
                num: 'Corp_Type',
                init: '1',
                inputClass: 'std-input-col-width-2',
                options: [
                  {
                    option: ' ',
                    value: null
                  },
                  {
                    option: '1. Canadian Controlled Private Corporation - CCPC',
                    value: '1'
                  },
                  {option: '2. Other Private Corporation', value: '2'},
                  {option: '3. Public Corporation', value: '3'},
                  {
                    option: '4. Corporation Controlled by a Public Corporation',
                    value: '4'
                  },
                  {option: '5. Other', value: '5'}
                  //{option: 'If an AB CCPC which was a Professional Corp.', value: '6'} todo: comment out for now
                  //will need it when implement 1.3
                ],
                rf: true
              },
              {
                type: 'infoField',
                label: 'If \'Other Corporation\', specify',
                labelWidth: '90%',
                width: '90%',
                num: '117',
                showWhen: {
                  fieldId: 'Corp_Type',
                  compare: {is: '5'}
                }
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Did the corporation type change during the year?',
                num: '119',
                init: '2'
              },
              {
                showWhen: {
                  fieldId: '119',
                  compare: {is: '1'}
                },
                type: 'infoField',
                label: 'If Yes, enter the date the corporation changed during the tax year:',
                inputType: 'date',
                inputClass: 'std-input-col-width-2',
                num: '043',
                tn: '043'
              },
              {
                type: 'infoField',
                label: 'If Yes, provide the corporation type at the end of last tax year',
                showWhen: {
                  fieldId: '119',
                  compare: {is: '1'}
                },
                inputType: 'dropdown',
                inputClass: 'std-input-col-width-2',
                num: '120',
                options: [
                  {
                    option: ' ',
                    value: null
                  },
                  {
                    option: 'Canadian Controlled Private Corporation - CCPC',
                    value: '1'
                  },
                  {option: 'Other private corporation', value: '2'},
                  {option: 'Public Corporation', value: '3'},
                  {
                    option: 'Corporation controlled by a public corporation',
                    value: '4'
                  },
                  {option: 'Other', value: '5'}
                  //{option: 'If an AB CCPC which was a Professional Corp.', value: '6'} //TODO: we will need this
                  //  option when we put in alberta forms. but comment out for now
                ]
              },
              {
                type: 'infoField',
                label: 'If \'Other Corporation\', specify',
                labelWidth: '90%',
                width: '90%',
                num: '121',
                showWhen: {
                  fieldId: '120',
                  compare: {is: '5'}
                }
              }
            ]
          },
          {
            header: 'Residency', fieldAlignRight: true,
            headerClass: 'font-subheader font-blue',
            subsection: [
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Is the corporation a resident of Canada?',
                tn: '080',
                num: '080',
                init: '1',
                rf: true
              },
              {
                type: 'infoField',
                inputType: 'dropdown',
                inputClass: 'std-input-col-width-2',
                textAlign: 'right',
                num: '081', "validate": {"or": [{"length": {"min": "2", "max": "2"}}, {"check": "isEmpty"}]},
                tn: '081',
                tipGuidance: 'Select country of residence. Complete and attach Schedule 97',
                showWhen: {
                  fieldId: '080',
                  compare: {is: '2'}
                },
                options: foreignCountries
              },
              {
                showWhen: {
                  fieldId: '080',
                  compare: {is: '2'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S97 - Additional Information On Non-Resident Corporations In Canada',
                formId: 'T2S97'
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'If non-resident corporation, is it claiming an exemption under an income tax treaty.' +
                ' If yes then include Schedule 91 with the return',
                tn: '082',
                num: '082',
                init: '2',
                showWhen: {
                  fieldId: '080',
                  compare: {is: '2'}
                }
              },
              {
                showWhen: {
                  and: [
                    {
                      fieldId: '082',
                      compare: {is: '2'}
                    },
                    {
                      fieldId: '080',
                      compare: {is: '2'}
                    }
                  ]
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S91 - Information Concerning Claims for Treaty-Based Exemptions',
                formId: 'T2S91'
              },
              {
                label: 'What is the corporation’s provincial jurisdiction?',
                num: '750',
                tn: '750',
                inputClass: 'std-input-col-width-2',
                type: 'infoField', inputType: 'dropdown',
                // init: 'ON',
                options: [
                  {option: ' ', value: null},
                  {option: 'Multiple Jurisdictions', value: 'MJ'},
                  {option: 'Alberta', value: 'AB'},
                  {option: 'British Columbia', value: 'BC'},
                  {option: 'Manitoba', value: 'MB'},
                  {option: 'New Brunswick', value: 'NB'},

                  {option: 'Newfoundland and Labrador', value: 'NL'},
                  {
                    option: 'Newfoundland and Labrador Offshore',
                    value: 'XO'
                  },
                  {option: 'Nova Scotia', value: 'NS'},
                  {option: 'Nova Scotia Offshore', value: 'NO'},

                  {option: 'Northwest Territories', value: 'NT'},
                  {option: 'Nunavut', value: 'NU'},
                  {option: 'Ontario', value: 'ON'},
                  {option: 'Prince Edward Island', value: 'PE'},

                  {option: 'Quebec', value: 'QC'},
                  {option: 'Saskatchewan', value: 'SK'},
                  {option: 'Yukon', value: 'YT'},
                  {option: 'Outside Canada', value: 'OC'}
                ]
              },
              {
                label: '(if Multiple Jurisdictions does apply, you MUST go to Schedule 5 to properly allocate ' +
                'amounts between jurisdictions)',
                labelClass: 'bold fullLength',
                showWhen: {
                  fieldId: '750',
                  compare: {is: 'MJ'}
                }
              },
              {
                type: 'infoField',
                inputType: 'link',
                note: 'Please click here to go to Schedule 5',
                formId: 't2s5',
                showWhen: {
                  fieldId: '750',
                  compare: {is: 'MJ'}
                }
              },
              {
                type: 'splitInputs',
                inputType: 'checkbox',
                divisions: '4',
                num: 'prov_residence',
                disabled: true,
                showValues: 'false',
                rf: true,
                showWhen: {
                  fieldId: '750',
                  compare: {is: 'MJ'}
                },
                items: [
                  {label: 'Alberta', value: 'AB'},
                  {label: 'British Columbia', value: 'BC'},
                  {label: 'Manitoba', value: 'MB'},
                  {label: 'New Brunswick', value: 'NB'},

                  {label: 'Newfoundland and Labrador', value: 'NL'},
                  {
                    label: 'Newfoundland and Labrador Offshore',
                    value: 'XO'
                  },
                  {label: 'Nova Scotia', value: 'NS'},
                  {label: 'Nova Scotia Offshore', value: 'NO'},

                  {label: 'Northwest Territories', value: 'NT'},
                  {label: 'Nunavut', value: 'NU'},
                  {label: 'Ontario', value: 'ON'},
                  {label: 'Prince Edward Island', value: 'PE'},

                  {label: 'Quebec', value: 'QC'},
                  {label: 'Saskatchewan', value: 'SK'},
                  {label: 'Yukon', value: 'YT'},
                  {label: 'Outside Canada', value: 'OC'}
                ]
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Did the corporation immigrate to Canada during the tax year?',
                tn: '291',
                num: '291',
                init: '2'
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Did the corporation emigrate from Canada during the tax year?',
                tn: '292',
                num: '292',
                init: '2'
              }
            ]
          },
          {
            header: 'Account numbers',
            headerClass: 'font-subheader font-blue',
            showWhen: {
              or: [
                {
                  fieldId: '750',
                  compare: {is: 'MJ'}
                },
                {
                  fieldId: '750',
                  compare: {is: 'BC'}
                },
                {
                  fieldId: '750',
                  compare: {is: 'MB'}
                },
                {
                  fieldId: '750',
                  compare: {is: 'NB'}
                },
                {
                  fieldId: '750',
                  compare: {is: 'NL'}
                },
                {
                  fieldId: '750',
                  compare: {is: 'XO'}
                },
                {
                  fieldId: '750',
                  compare: {is: 'NS'}
                },
                {
                  fieldId: '750',
                  compare: {is: 'NO'}
                },
                {
                  fieldId: '750',
                  compare: {is: 'NT'}
                },
                {
                  fieldId: '750',
                  compare: {is: 'NU'}
                },
                {
                  fieldId: '750',
                  compare: {is: 'PE'}
                },
                {
                  fieldId: '750',
                  compare: {is: 'QC'}
                },
                {
                  fieldId: '750',
                  compare: {is: 'SK'}
                },
                {
                  fieldId: '750',
                  compare: {is: 'OC'}
                }
              ]
            },
            subsection: [
              {
                type: 'infoField',
                label: 'British Columbia',
                inputType: 'text',
                rf: true,
                inputClass: 'std-input-col-width-2',
                num: 'bcAccountNum',
                showWhen: {
                  or: [
                    {fieldId: '750', compare: {is: 'BC'}},
                    {fieldId: 'T2S5.021'}
                  ]
                }
              },
              {
                type: 'infoField',
                label: 'Manitoba',
                inputType: 'text',
                rf: true,
                inputClass: 'std-input-col-width-2',
                num: 'mbAccountNum',
                showWhen: {
                  or: [
                    {fieldId: '750', compare: {is: 'MB'}},
                    {fieldId: 'T2S5.015'}
                  ]
                }
              },
              {
                type: 'infoField',
                label: 'Saskatchewan',
                inputType: 'text',
                rf: true,
                inputClass: 'std-input-col-width-2',
                num: 'skAccountNum',
                showWhen: {
                  or: [
                    {fieldId: '750', compare: {is: 'SK'}},
                    {fieldId: 'T2S5.017'}
                  ]
                }
              },
              {
                type: 'infoField',
                label: 'New Brunswick',
                inputType: 'text',
                rf: true,
                inputClass: 'std-input-col-width-2',
                num: 'nbAccountNum',
                showWhen: {
                  or: [
                    {fieldId: '750', compare: {is: 'NB'}},
                    {fieldId: 'T2S5.009'}
                  ]
                }
              },
              {
                type: 'infoField',
                label: 'Newfoundland and Labrador',
                inputType: 'text',
                rf: true,
                inputClass: 'std-input-col-width-2',
                num: 'nlAccountNum',
                showWhen: {
                  or: [
                    {fieldId: '750', compare: {is: 'NL'}},
                    {fieldId: 'T2S5.003'},
                    {fieldId: 'T2S5.004'},
                    {fieldId: '750', compare: {is: 'XO'}}
                  ]
                }
              },
              {
                type: 'infoField',
                label: 'Nova Scotia',
                inputType: 'text',
                rf: true,
                inputClass: 'std-input-col-width-2',
                num: 'nsAccountNum',
                showWhen: {
                  or: [
                    {fieldId: '750', compare: {is: 'NS'}},
                    {fieldId: '750', compare: {is: 'NO'}},
                    {fieldId: 'T2S5.007'},
                    {fieldId: 'T2S5.008'}
                  ]
                }
              },
              {
                type: 'infoField',
                label: 'Northwest Territories',
                inputType: 'text',
                rf: true,
                inputClass: 'std-input-col-width-2',
                num: 'ntAccountNum',
                showWhen: {
                  or: [
                    {fieldId: '750', compare: {is: 'NT'}},
                    {fieldId: 'T2S5.025'}
                  ]
                }
              },
              {
                type: 'infoField',
                label: 'Nunavut',
                inputType: 'text',
                rf: true,
                inputClass: 'std-input-col-width-2',
                num: 'nuAccountNum',
                showWhen: {
                  or: [
                    {fieldId: '750', compare: {is: 'NU'}},
                    {fieldId: 'T2S5.026'}
                  ]
                }
              },
              {
                type: 'infoField',
                label: 'Prince Edward Island',
                inputType: 'text',
                rf: true,
                inputClass: 'std-input-col-width-2',
                num: 'peAccountNum',
                showWhen: {
                  or: [
                    {fieldId: '750', compare: {is: 'PE'}},
                    {fieldId: 'T2S5.005'}
                  ]
                }
              },
              {
                type: 'infoField',
                label: 'Quebec',
                inputType: 'text',
                tipGuidance: 'We do not support Quebec return at this time. ' +
                'It is being implemented. Please check back later.',
                rf: true,
                inputClass: 'std-input-col-width-2',
                num: 'qcAccountNum',
                showWhen: {
                  or: [
                    {fieldId: '750', compare: {is: 'QC'}},
                    {fieldId: 'T2S5.011'}
                  ]
                }
              },
              {
                type: 'infoField',
                label: 'Yukon',
                inputType: 'text',
                labelWidth: '50%',
                width: '40%',
                rf: true,
                inputClass: 'std-input-col-width-2',
                num: 'ytAccountNum',
                showWhen: {
                  or: [
                    {fieldId: '750', compare: {is: 'YT'}},
                    {fieldId: 'T2S5.023'}
                  ]
                }
              }
            ]
          },
          {
            header: 'Ontario',
            headerClass: 'font-subheader font-blue',
            showWhen: {
              or: [
                {
                  fieldId: 'prov_residence', has: {ON: true}
                },
                {
                  fieldId: '750',
                  compare: {is: 'ON'}
                }
              ]
            },
            subsection: [
              {
                type: 'infoField',
                label: 'Ontario Corporation account number',
                inputType: 'text',
                num: '146',
                inputClass: 'std-input-col-width-2',
                rf: true,
                validate: {
                  "or": [
                    {"matches": "^-?[.\\d]{2,9}$"},
                    {"check": "isEmpty"}
                  ]
                }
              },
              {
                type: 'infoField',
                label: 'Select type of corporation in current year',
                num: '147',
                inputType: 'dropdown',
                inputClass: 'std-input-col-width-2',
                options: [
                  {option: ' ', value: null},
                  {option: '01 - Family farm corporation', value: '1'},
                  {option: '02 - Family fishing corporation', value: '2'},
                  {option: '03 - Mortgage investment corporation', value: '3'},
                  {option: '04 - Credit union', value: '4'},
                  {option: '06 - Bank', value: '6'},
                  {option: '08 - Financial institution prescribed by regulation only', value: '8'},
                  {option: '09 - Registered securities dealer', value: '9'},
                  {option: '10 - Farm feeder finance co-operative corporation ', value: '10'},
                  {option: '11 - Insurance corporation', value: '11'},
                  {option: '12 - Mutual insurance', value: '12'},
                  {option: '13 - Specialty mutual insurance', value: '13'},
                  {option: '14 - Mutual fund corporation', value: '14'},
                  {option: '15 - Bare trustee corporation', value: '15'},
                  {option: '16 - Professional corporation', value: '16'},
                  {option: '17 - Limited liability corporation', value: '17'},
                  {option: '18 - Gen. elec. energy/prod. of steam to gen. elec. for sale', value: '18'},
                  {option: '19 - Hydro successor, municipal electrical utility, or subsidiary of either', value: '19'},
                  {option: '20 - Producer/seller of steam not for generation of elec.', value: '20'},
                  {option: '21 - Mining corporation ', value: '21'},
                  {option: '22 - Non-resident corporation ', value: '22'},
                  {option: '99 - Other (if none of the previous descriptions apply)', value: '99'}
                  //TODO: enter full list from the CRA Efile Specs for Ontario
                ]
              },
              //TODO: Selection in pull-down as per CRA specs for Ontario forms.
              {
                type: 'infoField',
                label: 'Select type of corporation in prior year',
                width: '200px',
                num: '148',
                inputType: 'dropdown',
                inputClass: 'std-input-col-width-2',
                options: [
                  {option: ' ', value: null},
                  {option: '01 - Family farm corporation', value: '1'},
                  {option: '02 - Family fishing corporation', value: '2'},
                  {option: '03 - Mortgage investment corporation', value: '3'},
                  {option: '04 - Credit union', value: '4'},
                  {option: '06 - Bank', value: '6'},
                  {option: '08 - Financial institution prescribed by regulation only', value: '8'},
                  {option: '09 - Registered securities dealer', value: '9'},
                  {option: '10 - Farm feeder finance co-operative corporation ', value: '10'},
                  {option: '11 - Insurance corporation', value: '11'},
                  {option: '12 - Mutual insurance and paragraph 149(1)(m) of the federal Act', value: '12'},
                  {option: '13 - Specialty mutual insurance', value: '13'},
                  {option: '14 - Mutual fund corporation', value: '14'},
                  {option: '15 - Bare trustee corporation', value: '15'},
                  {option: '16 - Professional corporation (incorporated professional only)', value: '16'},
                  {option: '17 - Limited liability corporation', value: '17'},
                  {option: '18 -Gen. elec. energy/prod. of steam to gen. elec. for sale', value: '18'},
                  {option: '19 - Hydro successor, municipal electrical utility, or subsidiary of either', value: '19'},
                  {option: '20 - Producer/seller of steam not for generation of elec.', value: '20'},
                  {option: '21 - Mining corporation ', value: '21'},
                  {option: '22 - Non-resident corporation ', value: '22'},
                  {option: '99 - Other (if none of the previous descriptions apply)', value: '99'}
                  //TODO: enter full list from the CRA Efile Specs for Ontario - from T2S524, maintaining for coherence
                ]
              },
              {
                label: 'For more information on type of corporation, please refer to Schedule 524.',
                labelClass: 'fullLength'
              },
              {
                type: 'infoField',
                inputType: 'link',
                note: 'S524 - Ontario Specialty Types',
                formId: 'T2S524'
              },
              //TODO: Selection in pull-down as per CRA specs for Ontario forms. This should be updated from Roll Forward and from converted datafile.
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Is the corporation subject to the Ontario Business Corporations Act' +
                ' by being either incorporated, continued or amalgamated in Ontario. (Complete S546)',
                num: '149',
                init: '1'
              },
              {
                showWhen: {
                  fieldId: '149',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S546 - Corporations Information Act Annual Return for Ontario Corporations',
                formId: 'T2S546'
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: '<br>Is the corporation incorporated, continued or amalgamated outside Canada,' +
                ' but carrying on business in Ontario under the <i>Ontario Extra-Provincial Corporations Act</i>?' +
                ' (Complete S548)',
                labelWidth: '70%',
                num: '152',
                init: '2'
              },
              {
                showWhen: {
                  fieldId: '152',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S548 - Corporations Information Act Annual Return for Foreign Business Corporations',
                formId: 'T2S548'
              }
            ]
          },
          {
            header: 'Alberta',
            headerClass: 'font-subheader font-blue',
            showWhen: {
              or: [
                {
                  fieldId: 'prov_residence', has: {AB: true}
                },
                {
                  fieldId: '750',
                  compare: {is: 'AB'}
                }
              ]
            },
            fieldAlignRight: true,
            subsection: [
              {
                type: 'infoField',
                label: 'Alberta Corporation account number',
                inputClass: 'std-input-col-width-2',
                num: '135',
                tipGuidance: 'We do not support Alberta return at this time. ' +
                'It is being implemented. Please check back later.'
              },
              //TODO: edit checks for correct Alberta Account number.
              {
                type: 'infoField',
                label: 'What is the nature of business',
                inputClass: 'std-input-col-width-2',
                inputType: 'dropdown',
                num: '136',
                options: [
                  {option: ' ', value: null},
                  {option: 'Dairy farm', value: '0111'},
                  {option: 'Cattle farm', value: '0112'},
                  {option: 'Hog farm', value: '0113'},
                  {option: 'Poultry & Egg farm', value: '0114'}
                  //TODO: enter full list from the CRA Efile Specs for Alberta
                ]
              },
              {
                type: 'infoField',
                inputClass: 'std-input-col-width-2',
                num: '137'
              },
              //TODO: This should be a field which is filled out by the application based on the selection made field above
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Is the corporation filing an amended AT1?',
                num: '138',
                init: '2'
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'During the taxation year being reported, but after May 30, 2001,' +
                ' was there a transfer of property under Federal ITA, subsection 85(1), 85(2), or 97(2)?',
                num: '139',
                init: '2'
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Has there been a change in fiscal period since the last return was filed?',
                num: '140',
                init: '2'
              },
              {
                type: 'infoField',
                label: 'If yes, then select reason',
                inputType: 'dropdown',
                inputClass: 'std-input-col-width-2',
                showWhen: '140',
                num: '141',
                options: [
                  {
                    option: ' ',
                    value: null
                  },
                  {
                    option: 'Change in tax year approved by the CRA',
                    value: '1'
                  },
                  {
                    option: 'There has been a change in control',
                    value: '2'
                  },
                  {
                    option: 'Final return of the corporation',
                    value: '3'
                  }
                  //ODO: enter full list from the CRA Efile Specs for Alberta
                ]
              },
              //TODO: select from required choices available
              {
                type: 'infoField',
                label: 'If this is final return, select reason',
                inputType: 'dropdown',
                inputClass: 'std-input-col-width-2',
                showWhen: {
                  fieldId: '141',
                  compare: {is: '3'}
                },
                num: '142',
                options: [
                  {option: 'Amalgamation', value: '1'},
                  {
                    option: 'Permenant establishment discontinued in Alberta',
                    value: '2'
                  },
                  {option: 'Bankruptcy of the corporation', value: '3'},
                  {option: 'Wind-up into the parent corp', value: '4'},
                  {option: 'Dissolution of the corporation', value: '5'}
                  //TODO: ensure list complies exactly with CRA Efile Specs for Alberta
                ]
              },
              //TODO: select from the required choices available
              {
                type: 'infoField',
                inputClass: 'std-input-col-width-2',
                label: 'Dissolution date, if applicable',
                num: '143',
                inputType: 'date'
              },
              //TODO: Date field.
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Should the notice of Assessment be received by fax?',
                num: '144',
                init: '2'
              },
              {
                type: 'infoField',
                inputClass: 'std-input-col-width-2',
                label: 'If yes, provide fax #',
                num: '145'
              }
              //TODO: edit for telephone number; update fax number from 'Contact Person' below. If different, then give a diagnostic
            ]
          },
          {
            header: 'Foreign Reporting',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {type: 'table', num: 'foreignReportingTable'}
            ]
          },
          {
            header: 'Partnership',
            headerClass: 'font-subheader font-blue',
            subsection: [
              // {
              //   label: 'Membership in a partnership (Other than a professional corporation that has ' +
              //   'been mentioned on line 067 above)',
              //   labelClass: 'fullLength bold'
              // },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Is this a professional corporation that is a member of a partnership?',
                tn: '067',
                num: '067',
                tipGuidance: 'A professional corporation is a corporation that offers one or more of a ' +
                'specific set of professional services. Possible services include, but are not limited to, ' +
                'accountancy, law, medicine and social work. <br><br>For more information, please see the CRA\'s T2 Guide' +
                ' <a href="https://www.canada.ca/content/dam/cra-arc/formspubs/pub/t4012/t4012-17e.pdf"> here</a> '
                // init: '2'
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Is this a corporation that is a member of a partnership?',
                num: '2100',
                init: '2',
                rf: true
              },
              {
                showWhen: {
                  fieldId: '2100',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Are any of these partnerships a single-tier partnership which have a fiscal period ' +
                'different from that of the corporation?',
                num: '2210',
                init: '2'
              },
              {
                showWhen: {
                  fieldId: '2210',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Does the corporation have a significant interest* in any of these partnerships?',
                num: '222',
                init: '2'
              },
              {
                showWhen: {
                  fieldId: '222',
                  compare: {is: '1'}
                },
                label: 'If \'Yes\', then section 34.2 or 34.3 applies and you must complete a separate ' +
                'Schedule 71 for each such partnership.',
                labelClass: 'fullLength'
              },
              {
                showWhen: {
                  fieldId: '222',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S71 - Income Inclusion for Corporations that are Members of Single-Tier Partnerships',
                formId: 'T2S71'
              },
              {
                showWhen: {
                  fieldId: '2100',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Are any of these partnerships a multi-tier partnership which have a fiscal period' +
                ' different from that of the corporation?',
                num: '223',
                init: '2'
              },
              {
                showWhen: {
                  fieldId: '223',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Does the corporation have a significant interest* in any of these partnerships?',
                num: '224',
                init: '2'
              },
              {
                showWhen: {
                  fieldId: '224',
                  compare: {is: '1'}
                },
                label: 'If \'Yes\', then Section 34.2 and 34.3 applies and you must complete a separate ' +
                'Schedule 72 for each such partnership.',
                labelClass: 'fullLength'
              },
              {
                showWhen: {
                  fieldId: '224',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S72 - Income Inclusion for Corporations that are Members of Multi-Tier Partnerships',
                formId: 'T2S72'
              },
              {
                showWhen: {
                  or: [{
                    fieldId: '2210',
                    compare: {is: 1}
                  }, {
                    fieldId: '223',
                    compare: {is: 1}
                  }]
                },
                label: '* <b>Significant interest</b>' +
                ' means that the corporation, or the corporation together with one or more persons or partnerships' +
                ' related to or affiliated with the corporation, is entitled' +
                ' to more than 10% of the income or loss of the partnership,' +
                ' or the assets (net of liabilities) of the partnership if it were to cease to exist.',
                labelClass: 'fullLength'
              }
            ]
          },
          {
            header: 'Associated & Related Corporations',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {
                label: 'Is this corporation part of an associated or related group?',
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                num: '226',
                init: '2'
              },
              {
                label: 'Was the corporation associated with any corporation in the previous tax year' +
                ' (Business limit reduction - ITA 125(5.1))?',
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                num: '126',
                init: '2'
              },
              {
                label: 'Is the total taxable capital employed in Canada of the' +
                ' corporation and related corporations greater than $10,000,000?',
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                tn: '233',
                num: '233',
                init: '2',
                tipGuidance: 'It will apply even if Associated / Related Corporations does not apply'
              },
              {
                label: 'Is the total taxable capital employed in Canada of the corporation' +
                ' and associated corporations greater than $10,000,000?',
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                tn: '234',
                num: '234',
                init: '2',
                tipGuidance: 'It will apply even if Associated / Related Corporations does not apply'
              },
              {
                type: 'infoField',
                inputType: 'link',
                note: 'GED - Group Entities Data Workchart',
                formId: 'entityProfileSummary',
                showWhen: {
                  fieldId: '226',
                  compare: {is: '1'}
                }
              },
              {labelClass: 'fullLength'},
              {
                showWhen: {
                  fieldId: '226',
                  compare: {is: '1'}
                },
                label: 'Has the corporation entered into an agreement with other associated corporations' +
                ' for salary or wages of Specified Employees for SR&ED?  (Schedule T1174)',
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                tn: '264',
                num: '264',
                init: '2'
              },
              {
                showWhen: {
                  fieldId: '226',
                  compare: {is: '1'}
                },
                label: 'For financial institutions: Is the corporation a member of a related group ' +
                'of financial institutions with one or more subject to gross Part VI tax? (Schedule 39)',
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                tn: '250',
                num: '250',
                init: '2'
              },
              {
                "type": "infoField",
                "inputType": "seRadioButton",
                "inputClass": "std-input-col-width-2",
                "label": "Does the corporation wish to elect itself to not become an associated corporation?",
                "num": "1541",
                showWhen: {
                  fieldId: '226',
                  compare: {is: '1'}
                }
              },
              {
                showWhen: {
                  fieldId: '226',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S28 - Election Not To Be An Associated Corporation',
                formId: 'T2S28'
              }
            ]
          },
          {
            header: 'Changes in Tax Year',
            headerClass: 'font-subheader font-blue',
            fieldAlignRight: true,
            subsection: [
              {
                label: 'Does amalgamation, dissolution, or deemed year-end apply?',
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                num: '225',
                init: '2'
              },
              {
                showWhen: {
                  fieldId: '225',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Is this first year of filing after amalgamation?',
                tn: '071',
                num: '071',
                init: '2',
                tipGuidance: 'If there is a change of control (if any of those fields: 063, 066, 071, 072, 076 is Yes)' +
                '<br>please enter date and time in YYYY/MM/DD/HH/MM format in Lines 60 and 61 above (HH/MM = HOUR(s)MINUTE(s)',
              },
              {
                showWhen: {
                  fieldId: '071',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S24 - First Time Filer after Incorporation, Amalgamation or Winding-up of a Subsidiary into a Parent',
                formId: 'T2S24'
              },
              {
                showWhen: {
                  fieldId: '071',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S53 - General Rate Income Pool (GRIP) Calculation',
                formId: 'T2S53'
              },
              {
                type: 'infoField',
                label: 'Date of amalgamation',
                inputType: 'date',
                inputClass: 'std-input-col-width-2',
                num: '107',
                showWhen: {
                  or: [{
                    fieldId: '071',
                    compare: {is: 1}
                  }, {
                    fieldId: '225',
                    compare: {is: 1}
                  }]
                }
              },
              {
                showWhen: {
                  fieldId: '225',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Is this the final tax year before amalgamation?',
                tn: '076',
                num: '076',
                init: '2',
                tipGuidance: 'If there is a change of control (if any of those fields: 063, 066, 071, 072, 076 is Yes)' +
                '<br>please enter date and time in YYYY/MM/DD/HH/MM format in Lines 60 and 61 above (HH/MM = HOUR(s)MINUTE(s))'
              },
              {
                type: 'infoField',
                label: 'Date of amalgamation',
                labelWidth: '50%',
                inputType: 'date',
                inputClass: 'std-input-col-width-2',
                num: '109',
                showWhen: {
                  or: [{
                    fieldId: '076',
                    compare: {is: 1}
                  }, {
                    fieldId: '225',
                    compare: {is: 1}
                  }]
                }
              },
              {
                showWhen: {
                  fieldId: '225',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Is this the final return up to dissolution?',
                tn: '078',
                num: '078',
                init: '2'
              },
              {
                type: 'infoField',
                label: 'Date of dissolution',
                labelWidth: '50%',
                inputType: 'date',
                inputClass: 'std-input-col-width-2',
                num: '111',
                showWhen: {
                  or: [{
                    fieldId: '078',
                    compare: {is: 1}
                  }, {
                    fieldId: '225',
                    compare: {is: 1}
                  }]
                }
              },

              {
                showWhen: {
                  fieldId: '225',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Has there been a wind-up of a subsidiary under ' +
                'section 88 during the current tax year?',
                tn: '072',
                num: '072',
                init: '2',
                tipGuidance: 'If there is a change of control (if any of those fields: 063, 066, 071, 072, 076 is Yes)' +
                '<br>please enter date and time in YYYY/MM/DD/HH/MM format in Lines 60 and 61 above (HH/MM = HOUR(s)MINUTE(s))',
              },
              {
                showWhen: {
                  fieldId: '072',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S24 - First Time Filer after Incorporation, Amalgamation or Winding-up of a Subsidiary into a Parent',
                formId: 'T2S24'
              },
              {
                showWhen: {
                  fieldId: '225',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Has there been an acquisition of control to which subsection 249(4)' +
                ' applies since the start of the tax year on line 060?',
                tn: '063',
                num: '063',
                init: '2',
                tipGuidance: 'If there is a change of control (if any of those fields: 063, 066, 071, 072, 076 is Yes)' +
                '<br>please enter date and time in YYYY/MM/DD/HH/MM format in Lines 60 and 61 above (HH/MM = HOUR(s)MINUTE(s))'
              },
              {
                type: 'infoField',
                label: 'If yes, provide the date control was acquired',
                inputType: 'date',
                inputClass: 'std-input-col-width-2',
                tn: '065',
                num: '065',
                dateTimeShowWhen: {
                  or: dateTimeConditionArr
                },
                showWhen: {
                  or: [{
                    fieldId: '063',
                    compare: {is: 1}
                  }, {
                    fieldId: '225',
                    compare: {is: 1}
                  }]
                }
              },
              {
                showWhen: {
                  fieldId: '225',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Is the date on Line 061, a deemed year-end in according to subsection 249(3.1)?',
                tn: '066',
                num: '066',
                init: '2',
                tipGuidance: 'If there is a change of control (if any of those fields: 063, 066, 071, 072, 076 is Yes)' +
                '<br>please enter date and time in YYYY/MM/DD/HH/MM format in Lines 60 and 61 above (HH/MM = HOUR(s)MINUTE(s))'
              }
            ]
          }
        ]
      },
      {
        header: 'Document Optimiser',
        headerClass: 'font-header',
        rows: [
          {
            header: 'Filing Questions',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {type: 'table', num: 'commonQuestionsTable'}
            ]
          },
          {
            header: 'Scientific Research and Experimental Development (SR&ED)',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {type: 'table', num: 'sredTable'}
            ]
          },
          {
            header: 'Special Corporation Status',
            headerClass: 'font-subheader font-blue',
            subsection: [
              // {
              //   type: 'infoField',
              //   inputType: 'seRadioButton',
              //   inputClass: 'std-input-col-width-2',
              //   label: 'Is the corporation a credit union?',
              //   num: '1011',
              //   init: '2'
              // },
              {labelClass: 'fullLength'},
              {
                type: 'infoField',
                label: 'Select the corporation type:',
                num: '122',
                inputClass: 'std-input-col-width-2',
                inputType: 'dropdown',
                options: [
                  {option: ' ', value: null},
                  {option: 'Not Applicable', value: '13'},
                  {option: 'Credit union', value: '1'},
                  {option: 'Deposit insurance corporation', value: '2'},
                  {option: 'Bank', value: '3'},
                  {option: 'Authorized foreign bank', value: '4'},
                  {option: 'Life insurance corporation', value: '5'},
                  {
                    option: 'Agricultural corp which is a co-operative',
                    value: '6'
                  },
                  {option: 'Mutual fund corp', value: '7'},
                  {option: 'Life insurance corp', value: '8'},
                  {option: 'Deposit insurance corp', value: '9'},
                  {
                    option: 'Investment corp under sub. 130(3)',
                    value: '10'
                  },
                  {option: 'Mortgage investment corp', value: '11'},
                  {
                    option: 'Mutual fund corp under sub 131(8)',
                    value: '12'
                  }
                ],
                init: '13',
                rf: true,
                description: 'Special Corporation Status'
              }
            ]
          },
          {
            header: 'Subsection 89(11)',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {
                label: 'Has the corporation made an election under subsection 89(11) not to be a CCPC?',
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                num: '2661',
                init: '2'
              },
              {
                showWhen: {
                  fieldId: '2661',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S54 - Low Rate Income Pool (LRIP) Calculation',
                formId: 'T2S54'
              },
              {
                label: 'Has the election made in the current year?',
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                num: '2662',
                labelClass: 'tabbed',
                canClear: true,
                showWhen: {
                  fieldId: '2661',
                  compare: {is: '1'}
                }
              },
              {
                showWhen: {
                  fieldId: '2662',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S54 - Low Rate Income Pool (LRIP) Calculation',
                formId: 'T2S54'
              },
              {
                label: 'Has the election made in the previous year?',
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                num: '2663',
                labelClass: 'tabbed',
                canClear: true,
                showWhen: {
                  fieldId: '2661',
                  compare: {is: '1'}
                }
              },
              {
                label: 'If yes, has this election been revoked?',
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                num: '2664',
                labelClass: 'tabbed',
                canClear: true,
                showWhen: {
                  fieldId: '2663',
                  compare: {is: '1'}
                }
                //TODO: if 2662==1 during roll forward, update this field value to 1
              },
              {
                label: 'If yes, provide when it has been revoked',
                labelClass: 'tabbed2',
                type: 'infoField',
                inputType: 'dropdown',
                inputClass: 'std-input-col-width-2',
                options: [
                  {option: ' ', value: null},
                  {option: 'Current Year', value: '1'},
                  {option: 'Prior Year', value: '2'}
                ],
                num: '2665',
                init: '3',
                showWhen: {
                  fieldId: '2664',
                  compare: {is: '1'}
                }
              },
              {
                showWhen: {
                  fieldId: '2665',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S54 - Low Rate Income Pool (LRIP) Calculation',
                formId: 'T2S54'
              }
            ]
          },
          {
            header: 'Less Common Filing Questions',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {type: 'table', num: 'lessCommmonQuestionsTable'}
            ]
          },
          {
            header: 'Client Queries',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Do you want to use the client/inquiries requests features?',
                num: 'showQuery'
              },
              {
                type: 'infoField',
                showWhen: {fieldId: 'showQuery'},
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'RC59 - Business Consent on file for this Corporation',
                num: '1529'
              },
              {
                type: 'infoField',
                inputType: 'link',
                label: 'Please go to Q1 – RC59 to enter your RC59 information',
                labelClass: 'fullLength ',
                note: 'Go to RC59 query',
                docTitle: 'RC59 query',
                showWhen: {
                  and : [
                    {
                      fieldId: 'showQuery'
                    },
                    {
                      fieldId: '1529',
                      compare: {is: '2'}
                    }
                  ]
                }
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'T183 - Information Return for Corporations Filing Electronically',
                num: '1530',
                showWhen: {fieldId: 'showQuery'}
              },
              {
                label: '(Note - this form should only be sent to the Taxpayer after the return is complete ' +
                'and is ready for Efile to the CRA)',
                showWhen: {fieldId: 'showQuery'}
              },
              {
                type: 'infoField',
                inputType: 'link',
                label: 'Please go to Q2 – T183 to enter your T183 information',
                labelClass: 'fullLength ',
                note: 'Go to T183 query',
                docTitle: 'T183 query',
                showWhen: {
                  and : [
                    {fieldId: 'showQuery'},
                    {fieldId: '1530'}
                  ]
                }
              }
            ]
          }
        ]
      },
      {
        header: 'Addresses',
        headerClass: 'font-header',
        rows: [
          {
            header: 'Address of Head Office',
            headerClass: 'font-subheader font-blue bold',
            subsection: [
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Has the corporation’s head office address changed since the last time the CRA was notified?',
                tn: '010',
                num: '010',
                init: '2',
                rf: true
              },
              {
                type: 'infoField',
                inputType: 'address',
                add1Num: '011',
                add2Num: '012',
                provNum: '016',
                cityNum: '015',
                countryNum: '017',
                postalCodeNum: '018',
                add1Tn: '011',
                add2Tn: '012',
                cityTn: '015',
                provTn: '016',
                countryTn: '017',
                postalTn: '018',
                add1Rf: true,
                add2Rf: true,
                cityRf: true,
                countryRf: true,
                provRf: true,
                postalCodeRf: true
              }
            ]
          },
          {
            header: 'Mailing Address',
            headerClass: 'font-subheader font-blue bold',
            subsection: [
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Has the corporation’s mailing address changed since the last time the CRA was notified?',
                num: '020',
                init: '2',
                tn: '020',
                rf: true
              },
              //{type: 'infoField', inputType: 'seRadioButton',label: 'Use Tax Preparer's address', labelWidth: '70%', num: '206', init: '2'},
              // TODO: if True then update from TPP
              {
                type: 'infoField',
                label: 'Is the mailing address:',
                inputType: 'dropdown',
                num: '194',
                inputClass: 'std-input-col-width-2',
                textAlign: 'right',
                options: [
                  {option: ' ', value: null},
                  {option: 'Same as Head Office', value: '1'},
                  {option: 'Tax Preparers address', value: '2'},
                  {option: 'Other', value: '3'}
                ],
                init: '1',
                rf: true
              },
              // When Mailing Address is same as Head Office Address
              {
                type: 'infoField',
                label: 'Care of',
                num: '021', "validate": {"or": [{"length": {"min": "1", "max": "30"}}, {"check": "isEmpty"}]},
                tn: '021',
                inputClass: 'std-input-col-width-2',
                rf: true,
                showWhen: {
                  or: [
                    {
                      fieldId: '194',
                      compare: {is: '2'}
                    },
                    {
                      fieldId: '194',
                      compare: {is: '3'}
                    }
                  ]

                }
              },
              {
                type: 'infoField',
                inputType: 'address',
                add1Num: '022',
                add2Num: '023',
                provNum: '026',
                cityNum: '025',
                countryNum: '027',
                postalCodeNum: '028',
                add1Tn: '022',
                add2Tn: '023',
                cityTn: '025',
                provTn: '026',
                countryTn: '027',
                postalTn: '028',
                rf: true,
                showWhen: {
                  or: [
                    {
                      fieldId: '194',
                      compare: {is: '2'}
                    },
                    {
                      fieldId: '194',
                      compare: {is: '3'}
                    }
                  ]

                }
              }
            ]
          },
          {
            header: 'Location of Books & Records',
            headerClass: 'font-subheader font-blue bold',
            subsection: [
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Has the corporation’s books and records address changed since the last time the CRA was notified?',
                num: '030',
                init: '2',
                tn: '030',
                rf: true
              },
              {
                type: 'infoField',
                label: 'Is the books and records address:',
                inputType: 'dropdown',
                num: '329',
                inputClass: 'std-input-col-width-2',
                rf: true,
                textAlign: 'right',
                options: [
                  {option: ' ', value: null},
                  {option: 'Same as Head Office', value: '1'},
                  {option: 'Tax Preparers address', value: '2'},
                  {option: 'Other', value: '3'}
                ],
                init: '1'
              },
              {
                type: 'infoField',
                label: 'Care of',
                inputClass: 'std-input-col-width-2',
                num: '1165',
                rf: true,
                showWhen: {
                  or: [
                    {
                      fieldId: '329',
                      compare: {is: '2'}
                    },
                    {
                      fieldId: '329',
                      compare: {is: '3'}
                    }
                  ]
                }
              },
              {
                type: 'infoField',
                inputType: 'address',
                cannotOverride: true,
                add1Num: '031',
                add2Num: '032',
                provNum: '036',
                cityNum: '035',
                countryNum: '037',
                postalCodeNum: '038',
                add1Tn: '031',
                add2Tn: '032',
                cityTn: '035',
                provTn: '036',
                countryTn: '037',
                postalTn: '038',
                rf: true,
                showWhen: {
                  or: [
                    {
                      fieldId: '329',
                      compare: {is: '2'}
                    },
                    {
                      fieldId: '329',
                      compare: {is: '3'}
                    }
                  ]

                }
              }
            ]
          }
        ]
      },
      {
        header: 'Business Activity',
        headerClass: 'font-header',
        fieldAlignRight: true,
        rows: [
          {
            header: 'Additional Information',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                label: 'Is the corporation inactive?',
                inputClass: 'std-input-col-width-2',
                tn: '280',
                num: '280'
                // init: '2'
              },
              {
                label: 'What is the main activity to generate business activity? (Required for active business for Line 299 in EFILE.)',
                labelClass: 'fullLength tabbed'
              },
              //TODO : NAIC code is MANDATORY for Active corporations for Line 200299 in EFILE
              //TODO: automate selection of valid NAIC Code + Related product and service from CRA Efile specifications
              {
                type: 'table',
                num: 'Principal_Product_200299'
              },
              {
                type: 'table',
                num: 'Principal_Products'
              },
              {
                labelClass: 'fullLength'
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Did the corporation use the International Financial Reporting Standards (IFRS)' +
                ' when it prepared its financial statements?',
                init: '2',
                tn: '270',
                num: '270',
                rf: true
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Does the corporation want to be considered as a quarterly instalment remitter, pending eligibility?',
                tn: '293',
                num: '293',
                canClear: true
              },
              {
                type: 'infoField',
                label: 'If the corporation was eligible to remit instalments on a quarterly basis ' +
                'for part of the tax year, provide the date the corporation ceased to be eligible:',
                num: '294',
                inputClass: 'std-input-col-width-2',
                'inputType': 'date',
                'tn': '294'
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'If the corporation\'s major business activity is construction,' +
                ' did the corporation have any subcontractors during the tax year?',
                tn: '295',
                num: '295',
                canClear: true
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Is the corporation a deposit insurance corporation?',
                num: '189',
                canClear: true
              },
              {
                showWhen: {
                  fieldId: '189',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S53 - General Rate Income Pool (GRIP) Calculation',
                formId: 'T2S53'
              },
              {
                showWhen: {
                  fieldId: '189',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S54 - Low Rate Income Pool (LRIP) Calculation',
                formId: 'T2S54'
              },
              {
                showWhen: {
                  fieldId: '189',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S55 - Part III.1 Tax on Excessive Eligible Dividend Designations',
                formId: 'T2S55'
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Was the corporation a deposit insurance corporation in the previous year?',
                num: '190',
                canClear: true
              },
              {
                showWhen: {
                  fieldId: '190',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S53 - General Rate Income Pool (GRIP) Calculation',
                formId: 'T2S53'
              },
              {
                showWhen: {
                  fieldId: '190',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S54 - Low Rate Income Pool (LRIP) Calculation',
                formId: 'T2S53'
              },
              {
                showWhen: {
                  fieldId: '190',
                  compare: {is: '1'}
                },
                type: 'infoField',
                inputType: 'link',
                note: 'S55 - Part III.1 Tax on Excessive Eligible Dividend Designations',
                formId: 'T2S55'
              },
              {
                type: 'infoField',
                inputType: 'dropdown',
                options: currencyCodes,
                label: 'If an election was made under section 261, state the functional currency used:',
                inputClass: 'std-input-col-width-2',
                num: '079',
                tn: '079'
              },
              {
                type: 'infoField',
                label: 'Enter the average exchange rate for calculation (functional currency converting to Canadian currency):',
                inputClass: 'std-input-col-width-2',
                num: '191'
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'If the corporation is a cooperative or credit union, is it eligible for Small Business Deduction?',
                num: '186',
                canClear: true
              }
            ]
          },
          {
            header: 'Corporation Website',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {
                type: 'infoField',
                label: 'Enter the Internet webpage or website address:',
                num: '182',
                inputClass: 'std-input-col-width-3',
                rf: true
              },
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Did the corporation earn income from one or more internet webpages or websites?',
                num: '180',
                init: '2',
                tn: '180'
              }
            ]
          },
          {
            'header': 'Direct Deposit Information',
            headerClass: 'font-subheader font-blue',
            'subsection': [
              {
                label: 'To have the corporation\'s refund deposited directly into the corporation\'s bank account' +
                ' at a financial institution in Canada, or to change banking information you already gave us, ' +
                'complete the information below.',
                'labelClass': 'fullLength'
              },
              {
                type: 'infoField',
                label: 'How would you like to receive refund payments?',
                'inputType': 'dropdown',
                inputClass: 'std-input-col-width-2',
                num: '900',
                'options': [
                  {
                    option: ' ',
                    value: null
                  },
                  {
                    'option': 'Current Method of Payment',
                    'value': '0'
                  },
                  {
                    'option': 'Start Refunds via Direct Deposit',
                    'value': '1'
                  },
                  {
                    'option': 'Change Direct Deposit Banking Information',
                    'value': '2'
                  }
                ]
              },
              {
                label: '<b>Option 1. All amounts from all program accounts into one bank account.</b> ' +
                'Fill in this option ' +
                'if you want the direct deposit of all refunds and rebates from all program accounts, including ' +
                'the primary account and all division or branch accounts, to be deposited in one bank account.',
                labelClass: 'fullLength'
              },
              {
                type: 'table',
                num: 'Direct_Deposit_Info'
              },

              {
                label: '<b>OR<br> Option 2. Amounts from specific program accounts into specific bank accounts.</b> ' +
                'Fill in this option to have refunds or rebates for one or more specific program accounts deposited ' +
                'into a specific bank account',
                labelClass: 'fullLength'
              },
              {
                type: 'table',
                num: 'Direct_Deposit_Info_2'
              },

              {
                label: '<b>Other program accounts </b><br>For other program accounts, write the name and the two ' +
                'letters and last four digits of the program account in the spaces provided. For more ' +
                'information on which program accounts you can enter, read the information and instructions ' +
                'on page 2 of form RC366.',
                labelClass: 'fullLength'
              },
              {
                type: 'table',
                num: 'Direct_Deposit_Info_3'
              }
            ]
          }
        ]
      },
      {
        header: 'Contact Information',
        headerClass: 'font-header',
        fieldAlignRight: true,
        rows: [
          {
            header: 'Authorized Signing Officer',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {
                type: 'table',
                num: '800'
              },
              {
                type: 'table',
                num: '801'
              },

              {
                type: 'table',
                num: '176'
              },
              {
                type: 'infoField',
                label: 'Signing date for this return:',
                num: '955',
                inputType: 'date',
                inputClass: 'std-input-col-width-2',
                textAlign: 'right',
                tn: '955'
              }
            ]
          },
          {
            header: 'Contact Person',
            headerClass: 'font-subheader font-blue',
            fieldAlignRight: true,
            subsection: [
              {
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                label: 'Is the contact person the same as the authorized signing officer? ',
                tn: '957',
                num: '957',
                init: '1',
                rf: true
              },
              {
                type: 'infoField',
                label: 'Who is the contact person?',
                inputType: 'dropdown',
                inputClass: 'std-input-col-width-2',
                showWhen: {
                  fieldId: '957',
                  compare: {is: '2'}
                },
                num: '300', "validate": {"or": [{"matches": "^-?[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
                textAlign: 'right',
                options: [
                  {option: ' ', value: null},
                  {option: 'Same as Tax Preparer', value: '1'},
                  {option: 'Other', value: '2'}
                ],
                init: '2',
                rf: true
              },
              {
                type: 'table',
                num: 'Contact_1',
                showWhen: {
                  fieldId: '957',
                  compare: {is: '2'}
                }
              },
              {
                type: 'table',
                num: 'Contact_2',
                showWhen: {
                  fieldId: '957',
                  compare: {is: '2'}
                }
              },
              {
                type: 'table',
                num: 'Contact_4',
                showWhen: {
                  fieldId: '957',
                  compare: {is: '2'}
                }
              }
            ]
          }
        ]
      },
      {
        header: 'Other Filing Questions',
        headerClass: 'font-header',
        rows: [
          {
            header: 'Filing information',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {
                type: 'infoField',
                label: 'Language of correspondence',
                tn: '990',
                inputType: 'dropdown',
                num: '990',
                inputClass: 'std-input-col-width-2',
                init: '1',
                rf: true,
                options: [
                  {option: ' ', value: null},
                  {option: 'English', value: "1"},
                  {option: 'French', value: "2"}
                ]
              },
              {
                type: 'infoField',
                label: 'Has correspondence language been changed?',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                num: '128',
                init: '2'
              },
              {
                label: 'Was this return prepared by a tax preparer for a fee?',
                'labelCellClass': 'indent',
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                init: '1',
                num: '154'
              },
              {
                label: 'If yes, provide the tax preparer’s EFILE number:',
                'labelCellClass': 'indent',
                type: 'infoField',
                num: '920', "validate": {"or": [{"length": {"min": "5", "max": "5"}}, {"check": "isEmpty"}]},
                tn: '920',
                inputClass: 'std-input-col-width-2',
                maxLength: '20',
                showWhen: {
                  inputClass: 'std-input-col-width-2',
                  fieldId: '154',
                  compare: {is: '1'}
                }, rf: true
              }
            ]
          },
          {
            header: 'Disclaimer',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {
                labelClass: 'fullLength'
              },
              {
                label: 'Text of the Disclaimer'
              },
              {
                type: 'infoField',
                num: '183',
                inputType: 'textArea',
                init: 'Prepared without audit or review from information' +
                ' provided by the taxpayer, solely for income tax purposes.'
              },
              {
                label: 'Would you like the above disclaimer to',
                labelClass: 'fullLength'
              },
              {
                type: 'table',
                num: 'disclaimerTable'
              }
            ]
          },
          {
            header: 'Income Sprinkling',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {
                type: 'infoField',
                label: 'Does the corporation make distributions that could be considered "income sprinkling" under the ' +
                'proposed changes to section 120.4 of the ITA?',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                num: '1990',
                tipGuidance: 'This question is not part of the T2 return and has not been requested by or be ' +
                'transmitted to the CRA. You can use this to advise your clients accordingly'
                // init: '2'
              }
            ]
          },
          {
            header: 'Online Mail Registration',
            headerClass: 'font-subheader font-blue',
            subsection: [
              {
                label: 'Do you want to sign up for online mail for your T2 account?',
                type: 'infoField',
                inputType: 'seRadioButton',
                inputClass: 'std-input-col-width-2',
                num: '088',
                tn: '088'
              },
              {
                label: 'Enter an email address',
                showWhen: {
                  fieldId: '088',
                  compare: {is: '1'}
                }
              },
              {
                type: 'infoField',
                num: '089', "validate": {"or": [{"length": {"min": "1", "max": "75"}}, {"check": "isEmpty"}, "email"]},
                tn: '089',
                showWhen: {
                  fieldId: '088',
                  compare: {is: '1'}
                },
                tipGuidance: '<b>1.</b>Corporations that provide their email address need to be aware they must' +
                ' also register fully and sign into the My Business Account portal to view their output and ' +
                'to manage their online mail, <br> ' +
                '<b>2.</b>Correspondence will not be delivered electronically until Tax Payer has first registered for ' +
                'My Business Account<br>' +
                '<b>3.</b>Once you register for online mail for your T2 account, the Canada Revenue Agency (CRA) will send ' +
                'you an email when eligible notices, letters, or statements are available for viewing in' +
                ' My Business Account. The CRA will no longer send these eligible notices, letters, or statements' +
                ' to you through Canada Post'
              },
              {
                label: 'I understand that by providing an email address, I am registering for online ' +
                'mail. I have read and I accept the terms and conditions.',
                showWhen: {
                  fieldId: '088',
                  compare: {is: '1'}
                }
              },
              {
                type: 'infoField',
                inputType: 'link',
                note: 'CRA Online Mail Terms of Use',
                formId: 'onlineMail',
                showWhen: {
                  fieldId: '088',
                  compare: {is: '1'}
                }
              }
            ]
          }
        ]
      },
      {
        header: 'Software Information',
        headerClass: 'font-header',
        rows: [
          {
            header: 'Version information',
            headerClass: 'font-subsection font-blue',
            subsection: [
              {
                'label': 'Software Approval Code',
                'layout': 'alignInput',
                'layoutOptions': {
                  'showDots': true
                },
                'columns': [
                  {
                    'input': {
                      'num': '099',
                      'type': 'text',
                      'disabled': true,
                      'validate': {
                        'or': [
                          {
                            'length': {
                              'min': '4',
                              'max': '4'
                            }
                          },
                          {
                            'check': 'isEmpty'
                          }
                        ]
                      },
                      'cannotOverride': true
                    },
                    'padding': {
                      'type': 'tn',
                      'data': '099'
                    }
                  }
                ]
              },
              {
                'label': 'Software Version',
                'layout': 'alignInput',
                'layoutOptions': {
                  'showDots': true
                },
                'columns': [
                  {
                    'input': {
                      'num': '101',
                      'type': 'text',
                      'disabled': true,
                      'validate': {
                        'or': [
                          {
                            'length': {
                              'min': '1',
                              'max': '30'
                            }
                          },
                          {
                            'check': 'isEmpty'
                          }
                        ]
                      },
                      'cannotOverride': true
                    },
                    'padding': {
                      'type': 'tn',
                      'data': '101'
                    }
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  });
})
();