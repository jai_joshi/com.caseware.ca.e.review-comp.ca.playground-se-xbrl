(function () {

  var titlesList = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.titleList,
    'value');

  var checklistTableCol = [
    {
      type: 'none'
    },
    {
      type: 'link',
      colClass: 'half-col-width'
    },
    {
      type: 'seRadioButton',
      colClass: 'std-input-col-width-2'
    }
  ];

  var checkListTableColwithTn = [
    {
      type: 'none'
    },
    {
      type: 'link',
      colClass: 'half-col-width'
    },
    {
      type: 'none',
      colClass: 'std-padding-width'
    },
    {
      type: 'seRadioButton',
      colClass: 'std-input-col-width-2'
    }
  ];

  var dateTimeConditionArr = [
    {
      switchIf: {
        formId: 'cp',
        fieldId: '063',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '066',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '071',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '072',
        value: '1'
      }
    },
    {
      switchIf: {
        formId: 'cp',
        fieldId: '076',
        value: '1'
      }
    }
  ];

  wpw.tax.global.tableCalculations.cp = {
    'Principal_Product_200299': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          'header': 'NAICS code',
          'type': 'selector',
          colClass: 'std-input-width',
          selectorOptions: {
            title: 'NAICS Codes',
            items: wpw.tax.codes.naicsCodes
          }
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          header: 'NAICS Code description',
          cellClass: 'alignCenter'
        }
      ],
      cells: [
        {
          0: {
            num: '299',
            tn: '299',
            rf: true
          },
          2: {
            num: '344',
            rf: true,
            type: 'text'
          }
        }
      ]
    },
    'Principal_Products': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'text',
          header: 'Principal Product/service',
          cellClass: 'alignLeft',
          tipGuidance: 'Specify the principal products / services provided, and percentage of the total revenue that each product or service represents'
        },
        {
          header: 'Percentage',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          colClass: 'third-col-width'
        }
      ],
      cells: [
        {
          0: {
            num: '284', 'validate': {'or': [{'length': {'min': '1', 'max': '60'}}, {'check': 'isEmpty'}]},
            tn: '284',
            rf: true,
            type: 'text'
          },
          1: {
            num: '285', 'validate': {'or': [{'length': {'min': '1', 'max': '6'}}, {'check': 'isEmpty'}]},
            tn: '285',
            decimals: 3
          },
          2: {
            label: '%'
          }
        },
        {
          0: {
            num: '286', 'validate': {'or': [{'length': {'min': '1', 'max': '60'}}, {'check': 'isEmpty'}]},
            tn: '286',
            rf: true,
            type: 'text'
          },
          1: {
            num: '287', 'validate': {'or': [{'length': {'min': '1', 'max': '6'}}, {'check': 'isEmpty'}]},
            tn: '287',
            decimals: 3
          },
          2: {
            label: '%'
          }
        },
        {
          0: {
            num: '288', 'validate': {'or': [{'length': {'min': '1', 'max': '60'}}, {'check': 'isEmpty'}]},
            tn: '288',
            rf: true,
            type: 'text'
          },
          1: {
            num: '289', 'validate': {'or': [{'length': {'min': '1', 'max': '6'}}, {'check': 'isEmpty'}]},
            tn: '289',
            decimals: 3
          },
          2: {
            label: '%'
          }
        }
      ]
    },
    'Direct_Deposit_Info': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Branch Number',
          colClass: 'std-input-width',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Institution Number',
          colClass: 'std-input-width',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Account Number',
          colClass: 'std-input-col-width-2',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          type: 'none'
        }
      ],
      cells: [
        {
          1: {
            tn: '910'
          },
          2: {
            num: '910', 'validate': {'or': [{'matches': '^-?[.\\d]{5,5}$'}, {'check': 'isEmpty'}]}
          },
          4: {
            tn: '914'
          },
          5: {
            num: '914', 'validate': {'or': [{'length': {'min': '3', 'max': '4'}}, {'check': 'isEmpty'}]}
          },
          7: {
            tn: '918'
          },
          8: {
            num: '918', 'validate': {'or': [{'matches': '^-?[.\\d]{1,12}$'}, {'check': 'isEmpty'}]}
          }
        }
      ]
    },
    'Direct_Deposit_Info_2': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          cellClass: 'alignRight',
          colClass: 'std-input-col-width',
          type: 'none'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          cellClass: 'alignCenter',
          type: 'none'
        },
        {
          colClass: 'half-col-width',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Branch Number',
          colClass: 'half-col-width',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Institution Number',
          colClass: 'half-col-width',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Account Number',
          colClass: 'std-input-col-width-2',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          colClass: 'std-input-width',
          type: 'none'
        }],
      cells: [
        {
          0: {
            label: 'Corporation Income Tax Program Account(RC)',
            labelClass: 'bold'
          },
          2: {
            label: 'RC',
            labelClass: 'bold'
          },
          3: {
            num: '901'
          },
          5: {
            num: '902'
          },
          7: {
            num: '903'
          },
          9: {
            num: '904'
          }
        },
        {
          0: {
            label: 'GST/HST Program Account(RT)',
            labelClass: 'bold'
          },
          2: {
            label: 'RT',
            labelClass: 'bold'
          },
          3: {
            num: '905'
          },
          5: {
            num: '906'
          },
          7: {
            num: '907'
          },
          9: {
            num: '908'
          }
        },
        {
          0: {
            label: 'Payroll Deductions Program Account(RP)',
            labelClass: 'bold'
          },
          2: {
            label: 'RP',
            labelClass: 'bold'
          },
          3: {
            num: '915'
          },
          5: {
            num: '916'
          },
          7: {
            num: '917'
          },
          9: {
            num: '919'
          }
        }]
    },
    'Direct_Deposit_Info_3': {
      showNumbering: true,
      infoTable: true,
      columns: [
        {
          header: 'Name of the program account',
          cellClass: 'alignCenter',
          type: 'text'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          header: 'Two<br>Letters',
          colClass: 'small-input-width',
          cellClass: 'alignCenter',
          type: 'text'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          header: 'Four<br>Digits',
          colClass: 'small-input-width',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Branch Number',
          colClass: 'std-input-width',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Institution Number',
          colClass: 'std-input-width',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Account Number',
          colClass: 'std-input-col-width',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ]
    },
    '800': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          header: 'Title',
          width: '15%',
          type: 'dropdown',
          options: titlesList
        },
        {
          type: 'text',
          header: 'Last Name',
          tn: '950'
        },
        {
          type: 'text',
          header: 'First Name',
          tn: '951'
        }],
      cells: [
        {
          0: {
            num: '949',
            rf: true
          },
          1: {
            num: '950', 'validate': {'or': [{'length': {'min': '1', 'max': '30'}}, {'check': 'isEmpty'}]},
            rf: true
          },
          2: {
            num: '951', 'validate': {'or': [{'length': {'min': '1', 'max': '30'}}, {'check': 'isEmpty'}]},
            rf: true
          }
        }]
    },
    '801': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'text',
          header: 'Position',
          tn: '954'
        },
        {
          type: 'custom',
          format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
          header: 'Work Phone',
          tn: '956',
          tipGuidance: 'Format = 9999999999 If the telephone is not in Canada or USA enter per following examples:' +
          ' <br> <b>Example 1:</b> Sydney Australia Telephone no = 123456 City code = 2 Country code = 61' +
          ' Format = 6121234560 (A dummy zero is used to fill the tenth digit.)<br> ' +
          '<b>Example 2:</b> Cambridge U.K. Telephone no. = 123 4567 City code = 1223 Country code = 44 ' +
          'Format = 2231234567 (The last ten numbers of the complete series of numbers is used.)'
        },
        {
          type: 'text',
          header: 'Extension',
          colClass: 'half-col-width'
        },
        {
          type: 'custom',
          format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
          header: 'Mobile Phone',
          tipGuidance: 'Format = 9999999999 If the telephone is not in Canada or USA enter per following examples:' +
          ' <br> <b>Example 1:</b> Sydney Australia Telephone no = 123456 City code = 2 Country code = 61' +
          ' Format = 6121234560 (A dummy zero is used to fill the tenth digit.)<br> ' +
          '<b>Example 2:</b> Cambridge U.K. Telephone no. = 123 4567 City code = 1223 Country code = 44 ' +
          'Format = 2231234567 (The last ten numbers of the complete series of numbers is used.)'
        }
      ],
      cells: [
        {
          0: {
            num: '954', 'validate': {'or': [{'length': {'min': '1', 'max': '30'}}, {'check': 'isEmpty'}]},
            rf: true
          },
          1: {
            num: '956',
            rf: true,
            validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
          },
          2: {
            num: '637',
            rf: true
          },
          3: {
            num: '638',
            rf: true,
            validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
          }
        }]
    },
    '176': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'text',
          header: 'Fax',
          tipGuidance: 'Format = 9999999999 If the telephone is not in Canada or USA enter per following examples:' +
          ' <br> <b>Example 1:</b> Sydney Australia Telephone no = 123456 City code = 2 Country code = 61' +
          ' Format = 6121234560 (A dummy zero is used to fill the tenth digit.)<br> ' +
          '<b>Example 2:</b> Cambridge U.K. Telephone no. = 123 4567 City code = 1223 Country code = 44 ' +
          'Format = 2231234567 (The last ten numbers of the complete series of numbers is used.)',
          colClass: 'std-address-width',
          format: ['({N3}){N3}-{N4}', '{N10}']
        },
        {
          type: 'text',
          header: 'Email Address'
        }
      ],
      cells: [
        {
          0: {
            num: '639',
            rf: true
          },
          1: {
            num: '640',
            rf: true
          }
        }]
    },
    'Contact_1': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          header: 'Title',
          width: '15%',
          type: 'dropdown',
          options: titlesList
        },
        {
          header: 'Last Name First Name',
          tn: '958',
          type: 'text'
        }],
      cells: [
        {
          0: {
            num: '611',
            rf: true,
            disabled: true
          },
          1: {
            type: 'text',
            num: '958', 'validate': {'or': [{'length': {'min': '1', 'max': '60'}}, {'check': 'isEmpty'}]},
            rf: true
          }
        }]
    },
    'Contact_2': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'text',
          header: 'Position'
        },
        {
          type: 'custom',
          format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
          header: 'Work Phone',
          tn: '959'
        },
        {
          type: 'text',
          header: 'Extension',
          colClass: 'half-col-width'
        },
        {
          type: 'custom',
          format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
          header: 'Mobile Phone'
        }
        ],
      cells: [
        {
          0: {
            num: '615',
            rf: true
          },
          1: {num: '959'},
          2: {num: '617'},
          3: {num: '618'}
        }]
    },
    'Contact_4': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'custom',
          format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
          header: 'Fax',
          colClass: 'std-address-width'
        },
        {
          type: 'text',
          header: 'Email Address'
        }
      ],
      cells: [
        {
          0: {num: '619'},
          1: {num: '620'}
        }
      ]
    },
    'commonQuestionsTable': {
      fixedRows: true,
      infoTable: true,
      columns: checklistTableCol,
      cells: [
        {
          '0': {
            'label': 'Is this tax return for a Non-Profit Organization (NPO)?',
            cellClass: 'alignLeft'
          },
          '1': {
            'note': 'T1044',
            'formId': 'T1044',
            cellClass: 'alignLeft'
          },
          '2': {
            'num': 'to-0'
          }
        },
        {
          '0': {
            'label': 'Does the corporation make charitable donations?'
          },
          '1': {
            'note': 'S2',
            'formId': 'T2S2'
          },
          '2': {
            'num': 'to-1'
          }
        },
        {
          '0': {
            'label': 'Does the corporation have any fixed assets?'
          },
          '1': {
            'note': 'S8',
            'formId': 'T2S8'
          },
          '2': {
            'num': 'to-2'
          }
        },
        {
          '0': {
            label: 'Does the corporation have meals and entertainment expenses? '
          },
          '1': {
            'note': 'M&E',
            'formId': 'mealsEnt'
          },
          '2': {
            'num': 'to-3'
          }
        },
        {
          '0': {
            'label': 'Does the corporation have transactions with shareholders, officers or employees?'
          },
          '1': {
            'note': 'S11',
            'formId': 'T2S11'
          },
          '2': {
            'num': 'to-4'
          }
        },
        {
          '0': {
            'label': 'Does the corporation have rental income?'
          },
          '1': {
            'note': 'RENT',
            'formId': 'rentalsWcSummary'
          },
          '2': {
            'num': 'to-5'
          }
        },
        {
          '0': {
            'label': 'Does the corporation have automobile interest or leasing expenses?'
          },
          '1': {
            'note': 'AUTOSUM',
            'formId': 'automobileSummary'
          },
          '2': {
            'num': 'to-6'
          }
        },
        {
          '0': {
            'label': 'Did the corporation dispose any capital property?'
          },
          '1': {
            'note': 'S6',
            'formId': 'T2S6'
          },
          '2': {
            'num': 'to-7'
          }
        },
        {
          '0': {
            'label': 'Does the corporation claim any capital gains reserves or other reserves (e.g. reserves for doubtful debts, etc.)?'
          },
          '1': {
            'note': 'S13',
            'formId': 'T2S13'
          },
          '2': {
            'num': 'to-8'
          }
        },
        {
          '0': {
            'label': 'Does the corporation have non-resident shareholders?'
          },
          '1': {
            'note': 'S19',
            'formId': 'T2S19'
          },
          '2': {
            'num': 'to-9'
          }
        },
        {
          '0': {
            'label': 'Does the corporation claim Canadian Film or Video Production Tax Credits?'
          },
          '1': {
            'note': 'S48',
            'formId': 'T2S48'
          },
          '2': {
            'num': 'to-10'
          }
        },
        {
          '0': {
            'label': 'Does the corporation have foreign advertising expenses?',
            'formId': 'advertisingWorkchartS'
          },
          '1': {
            'note': 'FAE',
            'formId': 'advertisingWorkchartS'
          },
          '2': {
            'num': 'to-11'
          }
        },
        {
          '0': {
            'label': 'Does the corporation have eligible capital property?'
          },
          '1': {
            'note': 'S10',
            'formId': 'T2S10'
          },
          '2': {
            'num': 'to-12'
          }
        },
        {
          '0': {
            'label': 'Does the corporation claim any Federal Foreign Income Tax Credits or Federal Logging Tax Credits?'
          },
          '1': {
            'note': 'S21',
            'formId': 'T2S21'
          },
          '2': {
            'num': 'to-13'
          }
        },
        {
          '0': {
            'label': 'Does Aggregate Investment Income & Active Business Income apply for this Corporation?'
          },
          '1': {
            'note': 'S7',
            'formId': 'T2S7'
          },
          '2': {
            'num': 'to-14'
          }
        },
        {
          '0': {
            'label': 'Does the Corporation pay dividends?'
          },
          '2': {
            'num': 'to-15'
          }
        },
        {
          showWhen: {fieldId: 'commonQuestionsTable', row: 15, col: 2},
          '0': {
            'label': 'S3 - Dividends Received, Taxable Dividends Paid and Part IV Tax Calculation',
            cellClass: 'indent-3'
          },
          '1': {
            'note': 'S3',
            'formId': 'T2S3'
          },
          '2': {
            type: 'none',
            linkNum: 'to-15'
          }
        },
        {
          showWhen: {fieldId: 'commonQuestionsTable', row: 15, col: 2},
          '0': {
            'label': 'S54 - Low Rate Income Pool (LRIP) Calculation',
            cellClass: 'indent-3'
          },
          '1': {
            'note': 'S54',
            'formId': 'T2S54'
          },
          '2': {
            type: 'none',
            linkNum: 'to-15'
          }
        },
        {
          showWhen: {fieldId: 'commonQuestionsTable', row: 15, col: 2},
          '0': {
            'label': 'S55 - Part III.1 Tax on Excessive Eligible Dividend Designation',
            cellClass: 'indent-3'
          },
          '1': {
            'note': 'S55',
            'formId': 'T2S55'
          },
          '2': {
            type: 'none',
            linkNum: 'to-15'
          }
        },
        {
          showWhen: {fieldId: 'commonQuestionsTable', row: 15, col: 2},
          '0': {
            'label': 'If the dividends paid by the corporation are inter-corporate dividends to related parties, Would you like to use CaseWare’s safe income workchart?'
          },
          '1': {
            'note': 'SI-Safe Income Workchart',
            'formId': 'safeIncomeWorkchart'
          },
          '2': {
            'num': 'to-16'
          }
        },
        {
          '0': {
            'label': 'Does the Corporation have any shareholder who holds 10% or more of the corporation’s common and/or preferred shares?'
          },
          '1': {
            'note': 'S50',
            'formId': 'T2S50'
          },
          '2': {
            'num': 'to-17'
          }
        },
        {
          '0': {
            'label': 'Does the corporation have a loss in the current year or loss carried forward from prior years?',
            'formId': 'T2S4'
          },
          '1': {
            'note': 'S4',
            'formId': 'T2S4'
          },
          '2': {
            'num': 'to-18'
          }
        },
        {
          '0': {
            'label': 'Was this the first year of filing after incorporation, amalgamation, or for the first time after winding-up a subsidiary corporation(s) under section 88 of the Income Tax Act during the taxation year?'
          },
          '1': {
            'note': 'S24',
            'formId': 'T2S24'
          },
          '2': {
            'num': 'to-19'
          }
        },
        {
          '0': {
            'label': 'Does the corporation require a Canadian Manufacturing and Processing Profits Deduction to be calculated?'
          },
          '1': {
            'note': 'S27',
            'formId': 'T2S27'
          },
          '2': {
            'num': 'to-20'
          }
        },
        {
          '0': {
            'label': 'Does the corporation and its related corporations have taxable capital employed in Canada that is over $10,000,000?'
          },
          '1': {
            'note': 'S33',
            'formId': 'T2S33'
          },
          '2': {
            'num': 'to-21'
          }
        },
        {
          '0': {
            'label': 'Did the corporation incur any non-arm’s length transactions during the taxation year?'
          },
          '1': {
            'note': 'S44',
            'formId': 'T2S44'
          },
          '2': {
            'num': 'to-22'
          }
        },
        {
          '0': {
            'label': 'Did the corporation incur any expenditures related to Canadian film or video production?'
          },
          '1': {
            'note': 'S47',
            'formId': 'T2S47'
          },
          '2': {
            'num': 'to-23'
          }
        },
        {
          '0': {
            'label': 'Would you like to request a verification from the CRA for the corporation’s Capital Dividend Account Balance?'
          },
          '1': {
            'note': 'S89',
            'formId': 'T2S89'
          },
          '2': {
            'num': 'to-24'
          }
        },
        {
          0: {
            label: 'Is this a non-profit corporation to which section 149 applies?'
          },
          1: {
            type: 'none'
          },
          2: {
            num: '2270',
            init: '2',
            rf: true,
            tipGuidance: 'Corporations which are exempt under Section 149, which include non-profit organizations, ' +
            'do not usually have to pay corporate income tax'
          }
        },
        {
          showWhen: {fieldId: '2270', compare: {is: '1'}},
          0: {
            label: 'Type of tax-exempt corporation under section 149'
          },
          1: {
            type: 'none'
          },
          2: {
            tn: '085',
            num: '085',
            rf: true,
            inputType: 'seDropdown',
            inputClass: 'std-input-col-width-2',
            options: [
              {
                option: '1. Exempt under Para. 149(1)(e) or (l)',
                value: '1'
              },
              {option: '2. Exempt under Para. 149(1)(j)', value: '2'},
              {option: '3. Exempt under Para. 149(1)(t)', value: '3'},
              {
                option: '4. Exempt under other Paragraphs of section 149',
                value: '4'
              }
            ]
          }
        },
        {
          showWhen: {fieldId: '2270', compare: {is: '1'}},
          0: {
            label: 'Is the tax year-end a deemed tax year-end according to subsection 149(3.1)?',
          },
          1: {
            type: 'none'
          },
          2: {
            inputType: 'seRadioButton',
            inputClass: 'std-input-col-width-2',
            num: '124',
            init: '2'
          }
        },
        {
          0: {
            label: 'Do you want to see Tax Planning &amp; Tax Data Analytics workcharts?',
          },
          1: {
            type: 'none'
          },
          2: {
            inputType: 'seRadioButton',
            num: '1528',
            rf: true
          }
        }
      ]
    },
    'foreignReportingTable': {
      fixedRows: true,
      infoTable: true,
      columns: checklistTableCol,
      cells: [
        {
          '0': {
            'label': 'Does the corporation have investments in foreign affiliates?'
          },
          '1': {
            'note': 'S25',
            'formId': 'T2S25'
          },
          '2': {
            'num': 'to-25'
          }
        },
        {
          '0': {
            'label': 'Did the corporation have any non-arm\'s length transactions with non-residents?'
          },
          '1': {
            'note': 'T106',
            'formId': 'T106'
          },
          '2': {
            'num': 'to-26'
          }
        },
        {
          '0': {
            'label': 'Does the corporation have any foreign affiliates and/or controlled foreign affiliates?'
          },
          '1': {
            'note': 'T1134',
            'formId': 'T1134'
          },
          '2': {
            'num': 'to-27'
          }
        },
        {
          '0': {
            'label': 'Does the corporation have specified foreign property whose cost totalled more than $100,000 (Canadian) during the taxation year?'
          },
          '1': {
            'note': 'T1135',
            'formId': 'T1135'
          },
          '2': {
            'num': 'to-28'
          }
        }
      ]
    },
    'sredTable': {
      fixedRows: true,
      infoTable: true,
      columns: checklistTableCol,
      cells: [
        {
          '0': {
            'label': 'Does the corporation claim any scientific research and experimental development (SR&ED) credits?'
          },
          '2': {
            'num': 'to-29'
          }
        },
        {
          showWhen: {fieldId: 'sredTable', row: 0, col: 2},
          '0': {
            'label': 'S30 - Third Party Payments for Scientific Research and Experimental Development (SR&ED)',
            cellClass: 'indent-3'
          },
          '1': {
            'note': 'S30',
            'formId': 'T2S1263'
          },
          '2': {
            type: 'none',
            linkNum: 'to-29'
          }
        },
        {
          showWhen: {fieldId: 'sredTable', row: 0, col: 2},
          '0': {
            'label': 'S31 - Investment Tax Credit - Corporations',
            cellClass: 'indent-3'
          },
          '1': {
            'note': 'S31',
            'formId': 'T2S31'
          },
          '2': {
            type: 'none',
            linkNum: 'to-29'
          }
        },
        {
          showWhen: {fieldId: 'sredTable', row: 0, col: 2},
          '0': {
            'label': 'S32 - Scientific Research and Experimental Development (SR&ED) Expenditures Claim',
            cellClass: 'indent-3'
          },
          '1': {
            'note': 'S32',
            'formId': 'T661'
          },
          '2': {
            type: 'none',
            linkNum: 'to-29'
          }
        },
        {
          '0': {
            'label': 'Does the corporation require an agreement to allocate assistance for SR&ED between persons not dealing at arm’s length?'
          },
          '1': {
            'note': 'S61',
            'formId': 'T1145'
          },
          '2': {
            'num': 'to-30'
          }
        },
        {
          '0': {
            'label': 'Does the corporation want an agreement to transfer qualified expenditures incurred with respect to SR&ED contracts between persons not dealing at arm’s length?'
          },
          '1': {
            'note': 'S62',
            'formId': 'T1146'
          },
          '2': {
            'num': 'to-31'
          }
        }
      ]
    },
    'lessCommmonQuestionsTable': {
      fixedRows: true,
      infoTable: true,
      columns: checklistTableCol,
      cells: [
        {
          '0': {
            'label': 'Did the corporation make payments or credit amounts to non-residents under subsections 202(1) and 105(1) of the Income Tax Regulations?'
          },
          '1': {
            'note': 'S29',
            'formId': 'T2S29'
          },
          '2': {
            'num': 'to-32'
          }
        },
        {
          '0': {
            label: 'Did the corporation make any miscellaneous payments to residents of Canada in the form of royalties that have not been filed in a T5 slip, any payments associated with research and development, management, technical, or any other, similar payments?'
          },
          '1': {
            'note': 'S14',
            'formId': 'T2S14'
          },
          '2': {
            'num': 'to-33'
          }
        },
        {
          '0': {
            'label': ' Does the corporation deduct payments from its income plans (RPP, RSUBP, etc.)?'
          },
          '1': {
            'note': 'S15',
            'formId': 'T2S15'
          },
          '2': {
            'num': 'to-34'
          }
        },
        {
          '0': {
            'label': 'Is the corporation filing an election for a Capital Dividend under subsection 83(2)?'
          },
          '1': {
            'note': 'T2054',
            'formId': 'T2054'
          },
          '2': {
            'num': 'to-35'
          }
        },
        {
          '0': {
            'label': 'Were there any organizations (corporation, a controlled foreign affiliate of the corporation or any other corporation or trust that did not deal at arm’s length with the corporation) that had a beneficial interest in a non-resident discretionary trust?'
          },
          '1': {
            'note': 'S22',
            'formId': 'T2S22'
          },
          '2': {
            'num': 'to-36'
          }
        },
        {
          '0': {
            'label': 'Does your corporation manufacture or process tobacco in Canada?'
          },
          '1': {
            'note': 'S46',
            'formId': 'T2S46'
          },
          '2': {
            'num': 'to-37'
          }
        },
        {
          '0': {
            'label': 'Does the corporation have any section 20(1)(e) deductions or related election on subsection 20(1)(e)(v)?'
          },
          '1': {
            'note': 'PAR20',
            'formId': 'sec_20_1_e'
          },
          '2': {
            'num': 'to-38'
          }
        },
        {
          '0': {
            'label': 'Does the corporation wish to elect itself to not become an Associated Corporation?'
          },
          '1': {
            'note': 'S28',
            'formId': 'T2S28'
          },
          '2': {
            'num': 'to-39'
          }
        }
      ]
    },
    'clientCommunicationTable': {
      fixedRows: true,
      infoTable: true,
      columns: checklistTableCol,
      cells: [
        {
          '0': {
            'label': 'RC59 - Business Consent'
          },
          '1': {
            'note': 'RC59',
            'formId': 'RC59 query'
          },
          '2': {
            'num': 'to-40'
          }
        },
        {
          '0': {
            'label': 'T183 - Information Return for Corporations Filing Electronically'
          },
          '1': {
            'note': 'T183',
            'formId': 'T183 query'
          },
          '2': {
            'num': 'to-41'
          }
        }
      ]
    },
    'taxDateTable': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'date',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          colClass: 'std-spacing-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'date',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          colClass: 'std-spacing-width'
        },
        {
          type: 'text',
          cellClass: 'alignCenter',
          colClass: 'std-input-col-width-2'
        }
      ],
      cells: [
        {
          1: {
            label: 'Tax Year Start',
            type: 'none'
          },
          4: {
            label: 'Tax Year End',
            type: 'none'
          },
          6: {
            label: 'Number of Days in the Taxation Year',
            type: 'none',
            tipGuidance: 'Must be a maximum of 371 days (or 378 days if the corporation has indicated an acquisition of control)'
          }
        },
        {
          0: {
            tn: '060'
          },
          1: {

            num: 'tax_start', init: {year: 2017, month: 1, day: 1},
            dateTimeShowWhen: {
              or: dateTimeConditionArr
            }
          },
          3: {
            tn: '061'
          },
          4: {
            num: 'tax_end', init: {year: 2017, month: 12, day: 31},
            dateTimeShowWhen: {
              or: dateTimeConditionArr
            }
          },
          6: {
            num: 'Days_Fiscal_Period',
            maxLength: 3,
            textAlign: 'center'
          }
        }
      ]
    },
    'amendedReturnTable': {
      fixedRows: true,
      infoTable: true,
      columns: checkListTableColwithTn,
      cells: [
        {
          '0': {
            label: 'Is the corporation filing an amended tax return?'
          },
          '1': {
            note: 'L996',
            formId: 'l996'
          },
          '2': {
            tn: '997'
          },
          '3': {
            num: '997'
          }
        }
      ]
    },
    'disclaimerTable': {
      fixedRows: true,
      infoTable: true,
      columns: checklistTableCol,
      cells: [
        {
          0: {
            label: 'Print on the last page of the T2 Return?'
          },
          1: {
            type: 'none'
          },
          2: {
            init: '2',
            num: '179'
          }
        },
        {
          0: {
            label: 'Print on the last page of each GIFI form?'
          },
          1: {
            type: 'none'
          },
          2: {
            init: '2',
            num: '184'
          }
        },
        {
          0: {
            label: 'Print on the last page of form T661 / Schedule 32?'
          },
          1: {
            type: 'none'
          },
          2: {
            init: '2',
            num: '181'
          }
        }
      ]
    }
  };
})();
