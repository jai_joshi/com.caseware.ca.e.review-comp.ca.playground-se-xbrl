(function () {
  function convertPhone(phoneNum) {
    phoneNum = phoneNum.replace(/[^0-9]/g, "");
    phoneNum = "(" + phoneNum.substring(0, 3) + ")" + phoneNum.substring(3, 6) + "-" + phoneNum.substring(6)
    return phoneNum

  }

  function convertCountryToCode(country) {
    var countryList = wpw.tax.codes.countryAddressCodes;
    var newCode = {};
    for (var key in countryList) {
      newCode[countryList[key]['value'].toUpperCase()] = key;
    }
    if (country.toUpperCase() in newCode) {
      return newCode[country.toUpperCase()];
    }
  }

  function convertProvStateToCode(ProvState, field) {
    var PSList;
    var Code = {};
    if (field('017').get() == 'CA') {
      PSList = wpw.tax.codes.canadaProvinces;
      for (var key in PSList) {
        Code[PSList[key]['EN'].toUpperCase()] = key;
        Code[PSList[key]['FR'].toUpperCase()] = key;
      }
      if (ProvState.toUpperCase() in Code) {
        return Code[ProvState.toUpperCase()];
      }
    }
    if (field('017').get() == 'US') {
      PSList = wpw.tax.codes.usaStates;
      for (var key in PSList) {
        Code[PSList[key]['EN'].toUpperCase()] = key;
        Code[PSList[key]['FR'].toUpperCase()] = key;
      }
      if (ProvState.toUpperCase() in Code) {
        return Code[ProvState.toUpperCase()];
      }
    }
    return ProvState;
  }

  function enableField(field, numArray) {
    for (var i = 0; i < numArray.length; i++) {
      field(numArray[i]).disabled(false);
    }
  }


  wpw.tax.create.calcBlocks('cp', function (calcUtils) {

    // calcUtils.onFormLoad('cp', function () {
    //   compareWithChecklist(procedures[i]);
    // });

    calcUtils.calc(function (calcUtils, field, form) {
      //download data from collaborate
      if (!form('CONFIG').field('isLoad').get()) {
        wpw.tax.global.engagementService.getEngagementProperties().then(function (eP) {
          var taxEndSEVal = eP['layout']['sequences'][0]['periods'][0]['end'];
          wpw.tax.field('tax_end').set(taxEndSEVal);
        });
        //download data from collaborate
        var clientObj = wpw.tax.global.engagementProperties.parentEntity;

        wpw.tax.field('002').set(clientObj.name || '');
        wpw.tax.field('bn').set(clientObj.businessNumber || '');
        wpw.tax.field('182').set(clientObj.website || '');
        if (clientObj.addresses && clientObj.addresses[0]) {
          wpw.tax.field('011').set(clientObj.addresses[0].address1 || '');
          wpw.tax.field('012').set(clientObj.addresses[0].address2 || '');
          wpw.tax.field('015').set(clientObj.addresses[0].city || '');
          wpw.tax.field('017').set(convertCountryToCode(clientObj.addresses[0].country) || '');
          wpw.tax.field('016').set(convertProvStateToCode(clientObj.addresses[0].province, field) || '');
          wpw.tax.field('018').set(clientObj.addresses[0].postalCode.replace(/\s/g, "") || '');
        }
        if (clientObj.phones && clientObj.phones[0]) {
          wpw.tax.field('956').set(convertPhone(clientObj.phones[0].number) || '');
        }
        wpw.tax.form('CONFIG').field('isLoad').set(true);
      }

      // field('070').config('cannotOverride', true);
    });

    calcUtils.calc(function (calcUtils, field) {
      //update NAICS code
      var naicsField = field('299').get();
      var naicsCodes = wpw.tax.codes.naicsCodes;
      for (var category in naicsCodes) {
        if (naicsCodes[category][naicsField]) {
          field('344').assign(naicsCodes[category][naicsField].replace(/([0-9]*)\s\-/, ""));
        }
      }
    });

    //Assigned value for when importing csv.
    calcUtils.calc(function (calcUtils, field) {
      //update only when importing:
      if (wpw.tax.isTaxprepImporting) {
        var tableDataArray = [];
        var tableName = ['commonQuestionsTable', 'foreignReportingTable', 'sredTable', 'lessCommmonQuestionsTable'];
        tableName.forEach(function (tableNum) {
          tableDataArray = tableDataArray.concat(field(tableNum).get().cells)
        });
        var flaggedForms = wpw.tax.utilities.flagger();
        tableDataArray.forEach(function (row) {
          var tFormID = row[1].formId;
          var tocField = (row[2].num != undefined) ? row[2].num : row[2].linkNum;
          if (tFormID in flaggedForms) {
            field(tocField).set('1');
          }
        });
      }
    });

    calcUtils.calc(function (calcUtils, field) {
      //Set software version
      field('099').assign('CW03');
      field('101').assign('2017-10');
    });

    calcUtils.calc(function (calcUtils, field) {
      //Bucky: do not remove this. This to set showWhen all field related to TOC on and off
      field('showTO').assign(false);
    });

    calcUtils.calc(function (calcUtils, field) {
      if ((field('tax_start').valueType() != field('060', 't2j').valueType()) || (field('tax_end').valueType() != field('061', 't2j').valueType())) {
        wpw.tax.field('060', 't2j').valueType(field('tax_start').valueType());
        wpw.tax.field('061', 't2j').valueType(field('tax_end').valueType());
      }
    });

    calcUtils.calc(function (calcUtils, field) {
      // update from rate table
      field('195').assign(field('ratesFed.2030').get());
      field('196').assign(field('ratesFed.2031').get());

      //auto-fill the tax end date when user enter start but user can change if its not 365 days fiscal year.
      var conditionLoaded = field('isConditionLoaded').get();
      var taxStart = field('tax_start').get();
      var taxEnd = field('tax_end').get();
      //todo: this condition is temporary and should be updated when import calcs are implemented
      if (((calcUtils.hasChanged('tax_start') && !calcUtils.hasChanged('tax_end')) ||
          (wpw.tax.isTaxprepImporting && (!taxEnd || wpw.tax.utilities.dateCompare.equal(taxEnd, wpw.tax.date(2017, 12, 31))))) ||
        angular.isDefined(conditionLoaded) && !conditionLoaded && taxStart) {
        // Set tax_end to be one day less than a full year from tax_start
        if (wpw.tax.utilities.dateCompare.lessThan(field('tax_start').get(), field('ratesFed.2030').get())) {
          field('tax_start').assign(wpw.tax.date(2015, 1, 1));
          field('tax_start').disabled(false);
          field('tax_end').set(calcUtils.addToDate(field('tax_start').get(), 1, 0, -1));
          field('tax_end').disabled(false);
          field('isConditionLoaded').assign(true);
        }
        else {
          field('tax_end').set(calcUtils.addToDate(field('tax_start').get(), 1, 0, -1));
          field('tax_end').disabled(false);
          field('isConditionLoaded').assign(true);
        }
        if (wpw.tax.utilities.dateCompare.greaterThan(field('tax_end').get(), field('ratesFed.2031').get())) {
          field('tax_end').assign(wpw.tax.date(2018, 5, 31));
          field('tax_end').disabled(false);
        }
      }

      field('Days_Fiscal_Period').assign(
        wpw.tax.actions.calculateDaysDifference(field('tax_start').get(), field('tax_end').get()));
      field('Days_Fiscal_Period').config('cannotOverride', true);
    });

    calcUtils.calc(function (calcUtils, field) {
      //Type of corporation
      var fieldCorpType = field('Corp_Type');
      if (fieldCorpType.get() != '5' && calcUtils.hasChanged(fieldCorpType)) {
        calcUtils.removeValue('117');
      }
    });

    calcUtils.calc(function (calcUtils, field) {
      // AMALGAMATION/DISSOLUTION/DEEMED YEAR END
      if (field('119').get() == '2') {
        calcUtils.equals('120', 'Corp_Type');
        field('121').assign(undefined);
      }
    });

    calcUtils.calc(function (calcUtils, field) {
      //tn180
      if (field('t2s88.100').get() > 0) {
        field('180').assign('1');
      }
      else {
        field('180').assign('2')
      }
      field('180').source(field('T2S88.100'))
    });

    calcUtils.calc(function (calcUtils, field) {
      calcUtils.getGlobalValue('920', 'tpp', '888');
      if (field('997').get() == '2') {
        calcUtils.removeValue(['996', '301'], true);
      }
      else {
        calcUtils.getGlobalValue('996', 'l996', '996');
        calcUtils.getGlobalValue('301', 'l996', '100');
      }
    });

    calcUtils.calc(function (calcUtils, field, form) {
      //Associated/Related Corporation
      if (form('T2S33').field('190').get() > form('ratesFed').field('234').get()) {
        field('234').assign('1')
      }
      else {
        field('234').assign('2')
      }
      if (form('T2S33').field('190').get() > form('ratesFed').field('233').get()) {
        field('233').assign('1')
      }
      else {
        field('233').assign('2')
      }
      //subsection 89(11)
      if (field('2664').get() == 2) {
        field('2665').assign(null);
      }
      else (field('2665').disabled(false));
      ///tn266
      if (field('2662').get() == 1) {
        field('266').assign(true)
      }
      else {
        field('266').assign(false)
      }
      //tn267
      if (field('2665').get() == 1) {
        field('267').assign(true)
      }
      else {
        field('267').assign(false)
      }
    });

    calcUtils.calc(function (calcUtils, field, form) {
      //subsection 89(11)
      ///tn266
      if (field('2662').get() == 1) {
        field('266').assign(true)
      }
      else {
        field('266').assign(false)
      }
      //tn267
      if (field('2665').get() == 1) {
        field('267').assign(true)
      }
      else {
        field('267').assign(false)
      }
    });

    calcUtils.calc(function (calcUtils, field, form) {
      //update provinces from s5
      var provinceCP = field('750').get();
      var provinceS5 = form('T2S5').field('provinces').get();
      var numProvs = 0;
      if (provinceS5) {
        numProvs = Object.keys(provinceS5).filter(function (prov) {
          return provinceS5[prov];
        }).length;
      }

      //If there is 1 province in CP and no data in S5
      if (provinceCP !== 'MJ' && numProvs == 0) {
        var province = {};
        province[field('750').get()] = true;
        field('prov_residence').assign(province)
      }
      else {
        field('prov_residence').assign(field('T2S5.provinces').get());
      }

    });

    calcUtils.calc(function (calcUtils, field, form) {
      var locationOfBooksArr = ['031', '032', '035', '036', '037', '038', '1165'];
      // UPDATES THE ADDRESS OF LOCATION OF BOOKS AND RECORDS
      // if (field('329').get() !== '3' && calcUtils.hasChanged('329')) {
      //   calcUtils.removeValue(locationOfBooksArr, false);
      // }
      if (field('329').get() == '1') {//same as head office
        if (calcUtils.hasChanged('329')) {
          calcUtils.removeValue(['1165']);
        }
        field('031').assign(field('011').get());
        field('032').assign(field('012').get());
        field('036').assign(field('016').get());
        field('035').assign(field('015').get());
        field('037').assign(field('017').get());
        field('038').assign(field('018').get());
      }
      else if (field('329').get() == '2') {
        if (calcUtils.hasChanged('329')) {
          calcUtils.removeValue(['1165']);
        }
        calcUtils.getGlobalValue('031', 'TPP', '1100');
        calcUtils.getGlobalValue('032', 'TPP', '1101');
        calcUtils.getGlobalValue('036', 'TPP', '1102');
        calcUtils.getGlobalValue('035', 'TPP', '1103');
        calcUtils.getGlobalValue('037', 'TPP', '1104');
        calcUtils.getGlobalValue('038', 'TPP', '1105');
        calcUtils.getGlobalValue('1165', 'TPP', '101');// c/o - tax preparer firm name
      }
      else {
        if (field('329').get() == '3') {
          enableField(field, locationOfBooksArr)
        }
      }
    });

    calcUtils.calc(function (calcUtils, field, form) {
      //UPDATE MAILING ADDRESS
      var mailingAddFieldArr = ['022', '023', '026', '025', '027', '028', '021'];
      // if (field('194').get() == '3' && calcUtils.hasChanged('194')) {
      //   calcUtils.removeValue(mailingAddFieldArr, false);
      // }
      if (field('194').get() == '1') {//same as head office
        field('022').assign(field('011').get());
        field('023').assign(field('012').get());
        field('026').assign(field('016').get());
        field('025').assign(field('015').get());
        field('027').assign(field('017').get());
        field('028').assign(field('018').get());
      }
      else if (field('194').get() == '2') {
        calcUtils.getGlobalValue('022', 'TPP', '1100');
        calcUtils.getGlobalValue('023', 'TPP', '1101');
        calcUtils.getGlobalValue('026', 'TPP', '1102');
        calcUtils.getGlobalValue('025', 'TPP', '1103');
        calcUtils.getGlobalValue('027', 'TPP', '1104');
        calcUtils.getGlobalValue('028', 'TPP', '1105');
        calcUtils.getGlobalValue('021', 'TPP', '101');
      }
      else {
        if (field('194').get() == '3') {
          enableField(field, mailingAddFieldArr)
        }
      }

      //contact info update//
      var contactPersonNumArr = ['611', '958', '614', '615', '959', '617', '618', '619', '620'];
      if (field('957').get() == '1') {
        calcUtils.removeValue(contactPersonNumArr);
        field('611').assign(field('949').get());
        field('615').assign(field('954').get());
        field('959').assign(field('956').get());
        field('617').assign(field('637').get());
        field('618').assign(field('638').get());
        field('619').assign(field('639').get());
        field('620').assign(field('640').get());
        field('958').assign(field('950').get() + ' ' + field('951').get());
      }
      else {
        if (field('300').get() == '1') {
          calcUtils.getGlobalValue('611', 'TPP', '121');
          field('958').assign(field('tpp.122').get() + ' ' + field('tpp.123').get());
          field('958').source(field('tpp.122'));

          calcUtils.getGlobalValue('614', 'TPP', '124');
          calcUtils.getGlobalValue('615', 'TPP', '125');
          calcUtils.getGlobalValue('959', 'TPP', '126');
          calcUtils.getGlobalValue('617', 'TPP', '127');
          calcUtils.getGlobalValue('618', 'TPP', '128');
          calcUtils.getGlobalValue('619', 'TPP', '129');
          calcUtils.getGlobalValue('620', 'TPP', '130');
        } else {
          if (!wpw.tax.isTaxprepImporting && calcUtils.hasChanged('300')) {
            calcUtils.removeValue(contactPersonNumArr);
          }
          contactPersonNumArr.forEach(function (fieldId) {
            field(fieldId).disabled(false)
          });
        }
      }
    });

    calcUtils.calc(function (calcUtils, field, form) {
      //assign current date to num 955 signing date but will allow user to change it
      if (field('signingDateLoaded').get() == false) {
        field('995').assign(wpw.tax.date.today());
        field('995').disabled(false);
        field('signingDateLoaded').assign(true);
      }
    });

    /**
     * @param {String|undefined} value
     */
    function setValue(value) {
      wpw.tax.field(this.linkedCellId).set(value);
    }

    /***** Add here procedures for synchronizing with the checklists *****/

    /*
     {
     linkedCellId: 'cp.750',
     'setValue': setValue,
     RESPONSE_ROW_ID: '56hQSO8LTxOabts6YuygoA',
     PROCEDURE_ID: 'YjWF0cx6SNmTTkN0oUtyTA',
     RESPONSE_SET_ID: 'q7JfqFbjRLK7uUZO_3WMVg',
     ANSWER_LIST:  {
     'YES': 'xxxxxxxxx',
     'NO': 'xxxxxxxxx'
     }
     }
     */

    wpw.tax.procedures = [
      {
        "text": "Is this tax return for an Non-Profit Organization (NPO) (T1044)?",
        "linkedCellId": "cp.to-0",
        "RESPONSE_SET_ID": "qoNAN-bwR8q-AtyuWv-dMA",
        "ANSWER_LIST": {
          "1": "8agYn2rMRr2qSMZeNUZbgQ",
          "2": "5sfkpZm0SVm6s-zdq0kDiQ"
        },
        "RESPONSE_ROW_ID": "WCf2SX-JSByhb7rAg11d8g",
        "PROCEDURE_ID": "h9uP1K4lRAeyd1-5qqX-AA"
      },
      {
        "text": "Does the corporation make charitable donations ?(S2)",
        "linkedCellId": "cp.to-1",
        "RESPONSE_SET_ID": "k8MtQiRoSAOE9ZyROziunA",
        "ANSWER_LIST": {
          "1": "Ly-PGyolRTWxuiO7wbemIQ",
          "2": "Z3ZEZuK0SySAW187j2J9pw"
        },
        "RESPONSE_ROW_ID": "2vrxP7EXQmuwt7-osLrGMQ",
        "PROCEDURE_ID": "irPEN4I_SKilNyXFngPSGg"
      },
      {
        "text": "Does the corporation have any fixed assets? (S8)",
        "linkedCellId": "cp.to-2",
        "RESPONSE_SET_ID": "diabh6iqQoW5zK9jD1ztRA",
        "ANSWER_LIST": {
          "1": "1a4jhcR-QhqOHhD1MQ5T8Q",
          "2": "fmg8zY0cQK27a8PR-_4xNg"
        },
        "RESPONSE_ROW_ID": "4750jzgRSY6iCEUJ5XTgog",
        "PROCEDURE_ID": "VQ0Q3ETrQ-SFzecEZwroOQ"
      },
      {
        "text": "Does the corporation have Meals and Entertainment Expenses?",
        "linkedCellId": "to-3",
        "RESPONSE_SET_ID": "HI7omST3TCSkKgOZoNSALw",
        "ANSWER_LIST": {
          "1": "FP1R8dG-TxCNFWQUs8HLhg",
          "2": "CfS8wjojQaC90-PMdxokpQ"
        },
        "RESPONSE_ROW_ID": "YxZe_RXtTqaBqbJOWCOLeg",
        "PROCEDURE_ID": "xNBPRZWDTx6LQfmY2tBX_g"
      },
      {
        "text": "Does the corporation have transactions with Shareholders, Officers or Employees? (S11)",
        "linkedCellId": "cp.to-4",
        "RESPONSE_SET_ID": "fyo8XZJiSsWNIogfcIIstQ",
        "ANSWER_LIST": {
          "1": "YHSxN2HcT82CRLcdm4wndQ",
          "2": "XAUskfv6TJWHqefznXI7xA"
        },
        "RESPONSE_ROW_ID": "Wg9FNusoSBi0sNHsMqvBKw",
        "PROCEDURE_ID": "HjktYPGLS_KPExrFJP680w"
      },
      {
        "text": "Does the corporation have rental income?",
        "linkedCellId": "cp.to-5",
        "RESPONSE_SET_ID": "_6wUMFeART-tQ6qUXFnaaQ",
        "ANSWER_LIST": {
          "1": "F7bv6sMJQoKvjn1lnMfjZw",
          "2": "Dx4owDj6QdaWrFrtJs6F4A"
        },
        "RESPONSE_ROW_ID": "SZ0_rnXRQO69ua6D-QNAng",
        "PROCEDURE_ID": "9I7UrIgrQkivl5DxqlfejQ"
      },
      {
        "text": "Does the corporation have Automobile Interest or Leasing Expenses?",
        "linkedCellId": "cp.to-6",
        "RESPONSE_SET_ID": "WNJSXMMbSQKAZ6nuLuv9Xw",
        "ANSWER_LIST": {
          "1": "cnHANhR8RTCm1siB0zhB5g",
          "2": "IwwyqLGFTUetjeWbj_fK8A"
        },
        "RESPONSE_ROW_ID": "E8iNvRtXR22rtAYPlo6blA",
        "PROCEDURE_ID": "1pR2OX7XRua0432pL1x6iQ"
      },
      {
        "text": "Did the corporation dispose any Capital Property? (S6)",
        "linkedCellId": "cp.to-7",
        "RESPONSE_SET_ID": "bzISu0DtQIW0c-X6gYkrXg",
        "ANSWER_LIST": {
          "1": "5V3l8BdnTO2npyylwyHusA",
          "2": "rsu72JmrSqKIGD1Fp8Db5Q"
        },
        "RESPONSE_ROW_ID": "auWtneRKT4W-G7bu0mVJXg",
        "PROCEDURE_ID": "YdE5fdmQQNeql9qA2sjSVA"
      },
      {
        "text": "Does the corporation claim any capital gains reserves or other reserves(eg. reserve for doubtful debts, etc...)? (S13)",
        "linkedCellId": "cp.to-8",
        "RESPONSE_SET_ID": "rs2qVC4YStOzaiIikKKphg",
        "ANSWER_LIST": {
          "1": "DPhucudHTjCBNU-ORkVM2Q",
          "2": "051vf2ltT3CF3jCAIAJwAw"
        },
        "RESPONSE_ROW_ID": "WOaPSGUmS6eLTPd7LIpltA",
        "PROCEDURE_ID": "DOJBiCz9Qkqt2xCX1KQ8Nw"
      },
      {
        "text": "Does the corporation have non-resident shareholders? (S19)",
        "linkedCellId": "cp.to-9",
        "RESPONSE_SET_ID": "xot6SLOFTE2y2liSrzQ5fg",
        "ANSWER_LIST": {
          "1": "eRbjMRlASFScF0PMt6gFBQ",
          "2": "KsSp8NUnTZ-PxZPy-UfOFQ"
        },
        "RESPONSE_ROW_ID": "9_Xu8crDTfuF8epeibf2tA",
        "PROCEDURE_ID": "bdzXo566Rj-O4U3JvdpUtg"
      },
      {
        "text": "Does the corporation claim Canadian Film or Video Production tax Credits? (S48)",
        "linkedCellId": "cp.to-10",
        "RESPONSE_SET_ID": "_7ZlYydVTjq7I8TG9ML_yA",
        "ANSWER_LIST": {
          "1": "Rw-nzAiATICIKMRELjCYkA",
          "2": "H8X0YSLfTvCBIqr7eAk9vQ"
        },
        "RESPONSE_ROW_ID": "k0dCjXaHQ0uM77QqdCH0Uw",
        "PROCEDURE_ID": "OFn181VzS7CNXTMOLqJmxw"
      },
      {
        "text": "Does the corporation have Foreign Advertising Expenses?",
        "linkedCellId": "cp.to-11",
        "RESPONSE_SET_ID": "S5uzqINTQj-Y08NuAYGIhA",
        "ANSWER_LIST": {
          "1": "7SBfdVrlSkCApX4y0XBBGA",
          "2": "oRf5qaMPTCCoq1qbd22VMg"
        },
        "RESPONSE_ROW_ID": "o3Dq7qq1TJWOQihwnS-MAg",
        "PROCEDURE_ID": "AXmji4XkQbqVgZbwB_ufzA"
      },
      {
        "text": "Does the corporation have eligible capital property? (S10)",
        "linkedCellId": "cp.to-12",
        "RESPONSE_SET_ID": "fgNtyqGJSMO049Judnj2cg",
        "ANSWER_LIST": {
          "1": "s2q7aL8iQ-i67Us84Mfi1Q",
          "2": "I-ejT5dMRvywimxc1-UpPQ"
        },
        "RESPONSE_ROW_ID": "4z824-GoQASh9iKTFSx06Q",
        "PROCEDURE_ID": "3gXnZifsRiuTobykXWMD7Q"
      },
      {
        "text": "Does the corporation claim Federal Foreign Income Tax Credits or Federal Logging Tax Credit? (S21)",
        "linkedCellId": "cp.to-13",
        "RESPONSE_SET_ID": "rbUwa_2xTGu-hDvQzEDtGA",
        "ANSWER_LIST": {
          "1": "Vm_LhaI9TMijah94_PVg0A",
          "2": "dMLTpE4PRjmDQ1Et4axLXg"
        },
        "RESPONSE_ROW_ID": "w8eS0dzbTXO5c3FGthPKMg",
        "PROCEDURE_ID": "bhSm--kQSzCy2wYNqMFhgw"
      },
      {
        "text": "Does Aggregate Investment Income &amp; Active Business Income apply for this Corporation? (S7)",
        "linkedCellId": "cp.to-14",
        "RESPONSE_SET_ID": "pDsTVl9rSYmxpVps7-8TUQ",
        "ANSWER_LIST": {
          "1": "nS1ow_bvTyCX91RRUfXy-A",
          "2": "V9UyyeFbTGKK-cb_BQ-MGg"
        },
        "RESPONSE_ROW_ID": "5RFPWis7RNmHcEeA10m_DA",
        "PROCEDURE_ID": "ak5XoDryQkCZNVnnWPxxZg"
      },
      {
        "text": "Does the Corporation pay dividends? (S3, S55 and S54)",
        "linkedCellId": "cp.to-15",
        "RESPONSE_SET_ID": "KRfqP-xJTvaIp_WDvjCpgA",
        "ANSWER_LIST": {
          "1": "bHXRqbXDSiWWpHr826edRQ",
          "2": "D8QwS3SPQ0ybccQCG3KfhQ"
        },
        "RESPONSE_ROW_ID": "ujP1taQATsOndzyZjAoTuA",
        "PROCEDURE_ID": "ifu9frpZQtqsS8Ow5sCKAw"
      },
      {
        "text": "If the dividends paid by the corporation are inter-corporate dividends to related parties, Would you like to use CaseWare’s safe income workchart?",
        "linkedCellId": "cp.to-16",
        "RESPONSE_SET_ID": "ISMgQ2VuRZqOBAvE0Dw3dw",
        "ANSWER_LIST": {
          "1": "q6t3TR7HQO2OVIPHO-_l1g",
          "2": "FsLOaJ9xS2efqc2KS4ptIQ"
        },
        "RESPONSE_ROW_ID": "HQU-LbvsQtCUWmmHpfchdA",
        "PROCEDURE_ID": "N47sAmEeTjOyYuZOkMejwQ"
      },
      {
        "text": "Does the Corporation have any shareholder who holds 10% or more of the corporation’s common and/or preferred shares? (S50)",
        "linkedCellId": "cp.to-17",
        "RESPONSE_SET_ID": "1NcFXJtYRDGJTRp0Vgv5YQ",
        "ANSWER_LIST": {
          "1": "97vVntYBSwKLjSFGgcvWlA",
          "2": "OR2KX0JqS7uKk2o5BXMuzg"
        },
        "RESPONSE_ROW_ID": "bIRNRJ9RQwGji-nrZdA6Tg",
        "PROCEDURE_ID": "51Sf-C9hTQO1-kZw810eNQ"
      },
      {
        "text": "&nbsp;Does the corporation have a loss in the current year or Loss Carry Carry Forwards from prior years? (S4)&nbsp;",
        "linkedCellId": "to-18",
        "RESPONSE_SET_ID": "r8pvjM3UTs-DyHGt7Yt9ig",
        "ANSWER_LIST": {
          "1": "XH-maojzRhKOtamdpeZCYQ",
          "2": "rA0BBgOYSEisfsBrZLHYMA"
        },
        "RESPONSE_ROW_ID": "GnbFxaRbTuqGj6u8mU74TA",
        "PROCEDURE_ID": "x5DaMJgKQsCJ_zCqXzB5jg"
      },
      {
        "text": "Was this the first year of filing after incorporation, amalgamation, or for the first time after winding-up a subsidiary corporation(s) under section 88 of the Income Tax Act during the taxation year? (S24)",
        "linkedCellId": "cp.to-19",
        "RESPONSE_SET_ID": "VDr64qnmRJmjokhBmh-bnA",
        "ANSWER_LIST": {
          "1": "4s9rSSvmTn2KhD49zD_nzQ",
          "2": "EvhZPEymRkaRMF3QWIKlXw"
        },
        "RESPONSE_ROW_ID": "CdTQr4EPRJKinNqo7cGt8A",
        "PROCEDURE_ID": "YgrQuykZSgWIs2v_g1ktJA"
      },
      {
        "text": "Does the corporation require calculation of Canadian Manufacturing and Processing Profits Deduction? (S27)",
        "linkedCellId": "to-20",
        "RESPONSE_SET_ID": "9U5hoLJBQCqdoKchcTTDHQ",
        "ANSWER_LIST": {
          "1": "3D42Ca49ScuPYoj4qh-MwQ",
          "2": "k3YIve7RS_uoAX0vLuEoMg"
        },
        "RESPONSE_ROW_ID": "BBTiTq6eTaSBT5HcSqRJuQ",
        "PROCEDURE_ID": "Iv0cASXbRKiZBdXuN4Hv4Q"
      },
      {
        "text": "Does the corporation and its related corporations have taxable capital employed in Canada that is over $10,000,000? (S33)",
        "linkedCellId": "cp.to-21",
        "RESPONSE_SET_ID": "zt9VEWcqSnGqs0qhfNVQhA",
        "ANSWER_LIST": {
          "1": "XcotS024Tw-Zjj40TeHQAg",
          "2": "Fgw1uIFPQ6KVlIzwSNxr0w"
        },
        "RESPONSE_ROW_ID": "_3N3uOG8TFeVkV6FdICu_A",
        "PROCEDURE_ID": "j9JEmB34RK-AWsSwo7rOng"
      },
      {
        "text": "Did the corporation incur any non-arm’s length transactions during the taxation year? (S44)",
        "linkedCellId": "cp.to-22",
        "RESPONSE_SET_ID": "8RHjxf5qRuWC1XT8GqVU_A",
        "ANSWER_LIST": {
          "1": "nBQ1ajTJQY-OiFKmjZJkgQ",
          "2": "3Ts3W2JiQs2oYT1ofX2TXw"
        },
        "RESPONSE_ROW_ID": "ERLva2vXR4i-Ql7EJA7qcw",
        "PROCEDURE_ID": "jEVA1xSTTdWKBe5VU0ADdA"
      },
      {
        "text": "Did the corporation incur any expenditures related to Canadian film or video production? (S47)",
        "linkedCellId": "cp.to-23",
        "RESPONSE_SET_ID": "REUxuWR8Rx-3OzhNnvN-Zg",
        "ANSWER_LIST": {
          "1": "z9n0vYj-Tmin6x57Y3k5kA",
          "2": "3Ef18i2gSWmdVP1tN7jgoA"
        },
        "RESPONSE_ROW_ID": "dwPMMTOfTn6vzeLkLI0F4g",
        "PROCEDURE_ID": "vQtrnuRuQH6uCSBCjk1nhQ"
      },
      {
        "text": "Would you like to Request from the CRA for Capital Dividend Account Balance verification of this corporation? (S89)",
        "linkedCellId": "cp.to-24",
        "RESPONSE_SET_ID": "IHRhMXaBTsC4aipUPvn2IQ",
        "ANSWER_LIST": {
          "1": "J04w0oe5StixlNqoqgYz4g",
          "2": "9MrTjCFBQJCQgM_ADWbqVA"
        },
        "RESPONSE_ROW_ID": "lEYKWZlVSUyQTJ_EtTpPyg",
        "PROCEDURE_ID": "4r3SVxRNQl2TY6jdq3fB7A"
      },
      {
        "text": "Does the corporation earn&nbsp;income from one or more webpages or websites? (S88)",
        "linkedCellId": "cp.088",
        "RESPONSE_SET_ID": "G1ul8h-hTk-10Uv69DjZnw",
        "ANSWER_LIST": {
          "1": "K1_lWR7rSVOu61E1-njnlA",
          "2": "mz-C5Mz-RU-8zFznjBhXeA"
        },
        "RESPONSE_ROW_ID": "oKR9bjMFRHqYeR2nvwFGpg",
        "PROCEDURE_ID": "DdcVmcdcRGuCcWiHqA9QTA"
      },
      {
        "text": "Does the corporation want to start/change direct deposit banking information?",
        "linkedCellId": "cp.1531",
        "RESPONSE_SET_ID": "nK_frBFpS1WtiaulYt4kTg",
        "ANSWER_LIST": {
          "1": "mCgZcQm_Q7GEXLkCHcefRw",
          "2": "R_McdQfoQOSoxkcVgKBzTA"
        },
        "RESPONSE_ROW_ID": "nCT4hYb_Rb-xt1jgdsPInQ",
        "PROCEDURE_ID": "2KPNEpE7R9S4XMXhld5bNw"
      },
      {
        "text": "Does the corporation want to sign up for Online mail for my T2 account?",
        "linkedCellId": "cp.088",
        "RESPONSE_SET_ID": "JF16AjQ1QFSUCmLOdQB43A",
        "ANSWER_LIST": {
          "1": "whdaq13NTiKV7uDH26zbCQ",
          "2": "Jz_iJy5sQL24Q_pwahajTw"
        },
        "RESPONSE_ROW_ID": "gIllONsvQJeLbQ7klTY7ag",
        "PROCEDURE_ID": "Dz73jiOhRAC9KF4WgwZkOw"
      },
      {
        "text": "Do you want to see Tax Planning &amp; Tax Data Analytics workcharts?",
        "linkedCellId": "cp.1528",
        "RESPONSE_SET_ID": "nWsBbFufQFSStHdiLJHCJQ",
        "ANSWER_LIST": {
          "1": "i6VOt0NORCiQdUjh4K-vcw",
          "2": "hF5VtHHARQiIjOZNJQCQQQ"
        },
        "RESPONSE_ROW_ID": "uNKbC5XwTUCY2S6jh-t8sQ",
        "PROCEDURE_ID": "oPFG6B9ZRCuWqSfSH5nQcw"
      },
      {
        "text": "RC59 - Business Consent",
        "linkedCellId": "cp.to-40",
        "RESPONSE_SET_ID": "vTKzyiqIQhiCF9SKxCbV_A",
        "ANSWER_LIST": {
          "1": "_ue-6PHYRYmyFiSrj_-FvA",
          "2": "L9_LO6KLQny-mnXfftvx2w"
        },
        "RESPONSE_ROW_ID": "oZoWNJ9iRcmnXWm8QHQX_g",
        "PROCEDURE_ID": "uQojMD9_SHyD_fSZ5mT_BQ"
      },
      {
        "text": "T183 -&nbsp;Information Return for Corporations Filing Electronically",
        "linkedCellId": "cp.to-41",
        "RESPONSE_SET_ID": "uosLPfpqQzyB95Rh6AZ12w",
        "ANSWER_LIST": {
          "1": "b_Z1uL0XSWmirwfcAjD4Ig",
          "2": "Kfimo6u7QkGdD8KCMG4H9Q"
        },
        "RESPONSE_ROW_ID": "yDspWAvfSzeMgE38WY6XCg",
        "PROCEDURE_ID": "dpqkdjiTQzqU-3L95XTRiQ"
      },
      {
        "text": "Is the corporation filing an amended tax return",
        "linkedCellId": "cp.997",
        "RESPONSE_SET_ID": "Joud6MCdRXa8X6uBMULUTQ",
        "ANSWER_LIST": {
          "1": "lqCBjyiERVKv-iGCblfjwA",
          "2": "pYRzlYQxReOxRxAByMgtsg"
        },
        "RESPONSE_ROW_ID": "NiWEXEfyTmu0nbYQHvJSTg",
        "PROCEDURE_ID": "8yH4vQuzQPuswf9xw5fs9g"
      },
      {
        "text": "Is this corporation part of Associated / Related Group ?",
        "linkedCellId": "cp.226",
        "RESPONSE_SET_ID": "HQr5tK84RH-qSX8NKRbtLA",
        "ANSWER_LIST": {
          "1": "lLJAVhf-REGQjzWFHdP_eA",
          "2": "_9q1eOeNRjODB7H47OzWLA"
        },
        "RESPONSE_ROW_ID": "EWIlX6RMR-ultCnpwsP5YA",
        "PROCEDURE_ID": "eYZLMlOYSIWfb3ITdq3NMw"
      },
      {
        "text": "Is the corporation a member of a Partnership?",
        "linkedCellId": "cp.2100",
        "RESPONSE_SET_ID": "vvfE1TdZQD6kFw0mJYSwJw",
        "ANSWER_LIST": {
          "1": "uQgcYTOJSSW2OfFLRMrCDA",
          "2": "mG48b9bJStaUingahdkFnw"
        },
        "RESPONSE_ROW_ID": "BIIVqjToQTq3aBwdkvgBMw",
        "PROCEDURE_ID": "em0AyQudTniFeERjtgTCLQ"
      },
      {
        "text": "Does Amalgamation / Dissolution / deemed year-end/ etc. apply?",
        "linkedCellId": "cp.2225",
        "RESPONSE_SET_ID": "f-nIXNlyT4mJY9k2iakCAA",
        "ANSWER_LIST": {
          "1": "qdgGiPtDRCWMYMTuoT5W_g",
          "2": "lJd47CEoR8e3hZ43RYkkKA"
        },
        "RESPONSE_ROW_ID": "Gf5jve0iRbW24p8gfq3g_A",
        "PROCEDURE_ID": "Wm_XqUqISGGicLXBzVoehA"
      },
      {
        "text": "Does the corporation have investments in Foreign Affiliates? (S25)",
        "linkedCellId": "cp.to-25",
        "RESPONSE_SET_ID": "3SZT_jToTY2PIgVgDIyblA",
        "ANSWER_LIST": {
          "1": "KfaDP_iRSdahq6YrwRSDAA",
          "2": "tZQl0f4RQCquKawvKrk-Og"
        },
        "RESPONSE_ROW_ID": "NrYJh2ZqTXucLQlLyR6Z4w",
        "PROCEDURE_ID": "bsMhtvxbQQelEAAWbBO9LA"
      },
      {
        "text": "Did the corporation have any non-arm's length transactions with non-residents?&nbsp; (T106)",
        "linkedCellId": "to-26",
        "RESPONSE_SET_ID": "CLuuZWJEROe254hFpcxqTw",
        "ANSWER_LIST": {
          "1": "4czCnvbLS1uuwiGpkYOuag",
          "2": "DnS7BlOJQfGga2wRxYl5ug"
        },
        "RESPONSE_ROW_ID": "D4DD4UQUTr6RsEBZOockWA",
        "PROCEDURE_ID": "dcHGwCS7TE2GvXjb1zPZ5g"
      },
      {
        "text": "Does the corporation have any foreign affiliate and/or controlled foreign affiliate? (T1134)",
        "linkedCellId": "cp.to-27",
        "RESPONSE_SET_ID": "cRI0KmHKRv2ptBGqN5kmaQ",
        "ANSWER_LIST": {
          "1": "OLlPMflFTOS7r7AQXaGFGQ",
          "2": "HSBZ8LYlQjSty5MAgnr_Lg"
        },
        "RESPONSE_ROW_ID": "iDUd1gmuTHie82CSWhV5CQ",
        "PROCEDURE_ID": "1klWDyS6RlGQRkqn9YfmUg"
      },
      {
        "text": "Does the corporation have specified foreign property whose cost totalled more than $100,000 (Canadian) during the taxation year? (T1135)&nbsp;",
        "linkedCellId": "to-28",
        "RESPONSE_SET_ID": "s_AMDrx2Raih_zxd9FzpcQ",
        "ANSWER_LIST": {
          "1": "w02OAjVaSbqJ2JDVb96PFQ",
          "2": "9-sVv2bZREGmR05yDnLNPg"
        },
        "RESPONSE_ROW_ID": "Dq2l2Cx-RGGUSeZqNdHWnQ",
        "PROCEDURE_ID": "ywb1puGoS1uMjHwEOB2tcQ"
      },
      {
        "text": "Is the corporation a resident of Canada? (S97)",
        "linkedCellId": "cp.080",
        "RESPONSE_SET_ID": "jEXNhUksTPaVWSQo61x6BQ",
        "ANSWER_LIST": {
          "1": "tU1a_TcGQiGd08IOjoFYKA",
          "2": "_xylog_1S3O0tx3kS4z1nw"
        },
        "RESPONSE_ROW_ID": "UVlHSDnMRiutQGLXv1GNgA",
        "PROCEDURE_ID": "KW54luQsRQ-ZCvwIhE3M0w"
      },
      {
        "text": "Province of jurisdiction of the corporation",
        "linkedCellId": "cp.750",
        "RESPONSE_SET_ID": "q7JfqFbjRLK7uUZO_3WMVg",
        "ANSWER_LIST": {
          "AB": "aDr_xylATlis8XofSVRX3A",
          "BC": "c0QaHD81SuGytdzgRkElhA",
          "MB": "i8qtxGP1Qo-ffUbuLwmWWg",
          "NB": "YhpGZz01QPiwjhAt3sYJuQ",
          "NL": "F2eu-aPESFmyR527j8I27w",
          "NS": "6e_nc9D-SM-cPi2F5uDKNQ",
          "NU": "xBNaK5QOR7md_Hg2hGso5A",
          "ON": "kp-vQ-URTPScuPl49qMdZA",
          "PE": "6dYazd35TvKz0bB-lyEIDQ",
          "SK": "7KbELUkZS8SmKeRi9Dk_ag",
          "YT": "6qmKEuSrRPCZrHhkzG8SUg",
          "MJ": "2Zp45qBsT7i67DUoWXgdCw",
          "QC": "xiWYvpq5TJizTOuS186fvQ",
          "OC": "zL1WkR9DT8a3ORfSxQ8Ftg"
        },
        "RESPONSE_ROW_ID": "56hQSO8LTxOabts6YuygoA",
        "PROCEDURE_ID": "YjWF0cx6SNmTTkN0oUtyTA"
      },
      {
        "text": "Did the corporation make payments or credit amounts to non-residents under subsections 202(1) and 105(1) of the Income Tax Regulations? (S29)",
        "linkedCellId": "cp.to-32",
        "RESPONSE_SET_ID": "KujXozq9QSGP8lfE7cWNuA",
        "ANSWER_LIST": {
          "1": "QTcEyleoTsO8xNwC5-5O5A",
          "2": "gR7inrF1TC6dD5HW4_iBWg"
        },
        "RESPONSE_ROW_ID": "u7F0RCL1TtC2S-yvESzhiw",
        "PROCEDURE_ID": "4amUdOnYQv6OpiVrVaUn9w"
      },
      {
        "text": "Did the corporation make any miscellaneous payments to residents of Canada in the form of royalties that has not been filed in a T5 slips, payments associated with research and development, management, technical and other similar payments? (S14)",
        "linkedCellId": "cp.to-33",
        "RESPONSE_SET_ID": "ffVNlAzLQkC8_ObBJt7zpg",
        "ANSWER_LIST": {
          "1": "uBzL0vvyQ3ez_ZdlIPQRfg",
          "2": "uI2V3B4VRIKvNSewLkWIBQ"
        },
        "RESPONSE_ROW_ID": "0Zct00KAQXqzZb0KIdJwCQ",
        "PROCEDURE_ID": "Vr6RTMnvRlm_G2vV0UgZqQ"
      },
      {
        "text": "Does the corporation deduct&nbsp;payments from its income plans (RPP, RSUBP, etc...)? (S15)",
        "linkedCellId": "cp.to-34",
        "RESPONSE_SET_ID": "_hh4qPpgQ72iiACz5ZYcgg",
        "ANSWER_LIST": {
          "1": "0JJNRTjASDurQoEBQsB1Vg",
          "2": "miOGkbZnQV6VoXbhcEyrjw"
        },
        "RESPONSE_ROW_ID": "DooWbb7dTdyl5tpB7MWKWQ",
        "PROCEDURE_ID": "cqQwDQRQQDmh2f6Vn1F81Q"
      },
      {
        "text": "Does the corporation file an election for a Capital Dividend Under Subsection 83(2)? (T2054)",
        "linkedCellId": "cp.to-35",
        "RESPONSE_SET_ID": "ZDABI9_MRoK87bEdVMWVCQ",
        "ANSWER_LIST": {
          "1": "LKav8dlUSjKukSf5bNvaoA",
          "2": "FmkRbNLfR1uRTkZlH838lA"
        },
        "RESPONSE_ROW_ID": "Z_BtoelxTR6g_JlWWSKbng",
        "PROCEDURE_ID": "uQFuU6FHSMKwyx8-NMZC3g"
      },
      {
        "text": "Were there any organizations (corporation, a controlled foreign affiliate of the corporation or any other corporation or trust that did not deal at arm’s length with the corporation) that had a beneficial interest in a non-resident discretionary trust? (S22)",
        "linkedCellId": "cp.to-36",
        "RESPONSE_SET_ID": "X90jG6n4QUac0HNgY-Lfww",
        "ANSWER_LIST": {
          "1": "hvphMmlUQM-2C001yczlTw",
          "2": "5s-L4ykFREq7ynby9mdkpg"
        },
        "RESPONSE_ROW_ID": "XiUAdCMUS4STNyGccDa7sQ",
        "PROCEDURE_ID": "98Vb6JTcTzeA-M0rjjfLGg"
      },
      {
        "text": "Does your corporation manufacture or process Tobacco in Canada? (S46)",
        "linkedCellId": "cp.to-37",
        "RESPONSE_SET_ID": "MkaPJnTVSYC54kv5A3KHCw",
        "ANSWER_LIST": {
          "1": "GMrs5nKPTkqGJ6nYwEO0Ew",
          "2": "AvWMiginRKm4msl4FKjdOQ"
        },
        "RESPONSE_ROW_ID": "d1eGyhR6TYGAFkLIYf01CA",
        "PROCEDURE_ID": "6XicnoAtQRCUtOs5YarZzw"
      },
      {
        "text": "Does the corporation have any&nbsp;section 20(1)(e) deduction and related election on subsection 20(1)(e)(v)?",
        "linkedCellId": "to-38",
        "RESPONSE_SET_ID": "m_duznLLSN66ggrIJODBjA",
        "ANSWER_LIST": {
          "1": "6cQw_8RATcWfRX-90F8KvA",
          "2": "7RT4gtZRSwOdHRapNmZZFg"
        },
        "RESPONSE_ROW_ID": "BUgToaDQQjmoJTWXmrNx4w",
        "PROCEDURE_ID": "Nkyh5RGrSuWLOMuaYy3rcg"
      },
      {
        "text": "Does the corporation wish to elect itself to not become an Associated Corporation? (S28)",
        "linkedCellId": "cp.to-39",
        "RESPONSE_SET_ID": "6mVvW7ltTFimDx0irLWPUg",
        "ANSWER_LIST": {
          "1": "D6Q9aRpdQ5Sk6nxx0GHZYg",
          "2": "bv8t5iQGS3ufYXPdHjHLGw"
        },
        "RESPONSE_ROW_ID": "3s57pHdARLGhQwWoV9HPbQ",
        "PROCEDURE_ID": "v-hYtos_SCumtIjYKeFScw"
      },
      {
        "text": "Does the corporation claim any scientific research and experimental development (SR&amp;ED) credits? (S30-31-32)",
        "linkedCellId": "cp.to-29",
        "RESPONSE_SET_ID": "vndysH23Rf60nn2rgtnOow",
        "ANSWER_LIST": {
          "1": "l8fVZrqJQm-3MVZ6Hfiu0w",
          "2": "jjX0prwER_SR3zSB3_wH0A"
        },
        "RESPONSE_ROW_ID": "vYh7CT_GShm8Fi2d-2975g",
        "PROCEDURE_ID": "6OeXX3i-TJSLH3gA1sAdcg"
      },
      {
        "text": "Does the corporation require an agreement to allocate assistance for SR&amp;ED between persons not dealing at arm’s length? (S61)",
        "linkedCellId": "to-30",
        "RESPONSE_SET_ID": "8-EXVzE8RpWeALB-5vn0SQ",
        "ANSWER_LIST": {
          "1": "B-gGpjhvRmCMSzEIHAvmaw",
          "2": "-Qr2CFC_T4u8oI9K6vjdmA"
        },
        "RESPONSE_ROW_ID": "hFdIYy8OS5aQCK9-eBtMaw",
        "PROCEDURE_ID": "Hsq8yjhxReSPupLFcqwB8w"
      },
      {
        "text": "Does the corporation want an agreement to transfer qualified expenditures incurred in respect of SR&amp;ED contracts between persons not dealing at arm’s length? (S62)&nbsp;",
        "linkedCellId": "to-31",
        "RESPONSE_SET_ID": "OsQ8KEoxS6ikZ9M2aB0P7A",
        "ANSWER_LIST": {
          "1": "CwdnGE0pTs6pgnfjgJPgqg",
          "2": "NVu37tCiSBKq-IkmBwx1Tg"
        },
        "RESPONSE_ROW_ID": "X6Cpa303Q4OJqy29SUoZVA",
        "PROCEDURE_ID": "e90pBizbTPW0eEUhdYtkxQ"
      },
      {
        "text": "RC59",
        "linkedCellId": "1529",
        "PROCEDURE_ID": "uQojMD9_SHyD_fSZ5mT_BQ",
        "RESPONSE_SET_ID": "vTKzyiqIQhiCF9SKxCbV_A",
        "RESPONSE_ROW_ID": "oZoWNJ9iRcmnXWm8QHQX_g",
        "ANSWER_LIST": {
          "1": "_ue-6PHYRYmyFiSrj_-FvA",
          "2": "L9_LO6KLQny-mnXfftvx2w"
        }//the answer is up side down because of the question on TOC, No means show query
      },
      {
        "text": "T183",
        "linkedCellId": "1530",
        "PROCEDURE_ID": "dpqkdjiTQzqU-3L95XTRiQ",
        "RESPONSE_SET_ID": "uosLPfpqQzyB95Rh6AZ12w",
        "RESPONSE_ROW_ID": "yDspWAvfSzeMgE38WY6XCg",
        "ANSWER_LIST": {
          "1": "b_Z1uL0XSWmirwfcAjD4Ig",
          "2": "Kfimo6u7QkGdD8KCMG4H9Q"
        }
      }

      // {
      //   "linkedCellId": "cp.070",
      //   "PROCEDURE_ID": "xJfqE1_JSaaN8nNjHTzH2w",
      //   "RESPONSE_SET_ID": "OV8uH06_TUunfDRv_pCPSw",
      //   "RESPONSE_ROW_ID": "Tv2TMnHfQPujYtGI4bjmNw",
      //   "ANSWER_LIST": {
      //     "1": "Tota_mL8TKmONOU5JJ3MLg",
      //     "2": "6kXIJ59DSOajrQgKOIqztw"
      //   },
      //   "setValue": setValue
      // }
    ]; //Add here all the procedures that need to be synchronized

    //Example of pulling value from the checklist
    // angular.element(document).ready(function () {
    //   var /** @type Object */ procedure1 = wpw.utilities.findObjectBy('linkedCellId', wpw.tax.procedures, 'cp.showQuery');
    //   wpw.tax.field(procedure1.linkedCellId).set(wpw.tax.getChecklistAnswer(wpw.tax.procedures, procedure1));
    //
    //   var /** @type Object */ procedure2 = wpw.utilities.findObjectBy('linkedCellId', wpw.tax.procedures, 'cp.070');
    //   wpw.tax.field(procedure2.linkedCellId).set(wpw.tax.getChecklistAnswer(wpw.tax.procedures, procedure2));
    // });


    calcUtils.calc(function (calcUtils) {
      angular.forEach(wpw.tax.procedures, function (procedure) {
        if (procedure.linkedCellId === 'cp.showQuery') {
          return;
        }
        //The answer shouldn't be undefined in order to be saved
        var /** @type String|undefined */ answerId;
        if (calcUtils.field(procedure.linkedCellId).get() == "") {
          //it check if we clear the answer on our TOC it will set the visibility to NO
          answerId = procedure.ANSWER_LIST['2']
        } else {
          answerId = procedure.ANSWER_LIST[calcUtils.field(procedure.linkedCellId).get()];
        }
        wpw.tax.saveChecklistAnswer(wpw.tax.procedures, procedure, answerId);
      });
    });


    /*function changeCell(procedure) {
    if (changes.id === procedure.PROCEDURE_ID &&
    changes.change && changes.change.responseRows &&
    changes.change.responseRows[procedure.RESPONSE_ROW_ID]){
    if(changes.change.responseRows[procedure.RESPONSE_ROW_ID].responseAnswers[procedure.RESPONSE_SET_ID] && changes.change.responseRows[procedure.RESPONSE_ROW_ID].responseAnswers[procedure.RESPONSE_SET_ID].responses['0']) {
    wpw.tax.field(procedure.linkedCellId).set(procedure.setValue(procedure, changes.change.responseRows[procedure.RESPONSE_ROW_ID].responseAnswers[procedure.RESPONSE_SET_ID].responses['0']));
    }else if(changes.change.responseRows[procedure.RESPONSE_ROW_ID].responseAnswers[procedure.RESPONSE_SET_ID] === null) {
    wpw.tax.field(procedure.linkedCellId).set(undefined);
    }
    }
    }

    for (var /!** @type Number *!/ i = 0; i < procedures.length; i++) {
    changeCell(procedures[i]);
    }
    });

    function compareWithChecklist(procedure) {
    if (procedure) {
    wpw.tax.getChecklistAnswer(procedure).then(function (id) {
    /!** Set jurisdiction with the value from the checklist  *!/
    if (procedure.ANSWER_LIST[wpw.tax.field(procedure.linkedCellId).get()] !== id) {
    wpw.tax.field(procedure.linkedCellId).set(procedure.setValue(procedure, id));
    }
    });
    }
    }
*/

    /*for (var /!** @type Number *!/ i = 0; i < procedures.length; i++) {
     var procedure = procedures[i];
     procedure.RESPONSE_ANSWER_ID = '';
     compareWithChecklist(procedure);
     }*/

  });
})();
