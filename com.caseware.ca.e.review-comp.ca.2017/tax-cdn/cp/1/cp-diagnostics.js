(function () {

  wpw.tax.create.diagnostics('cp', function (paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    function mark(fields, params) {
      fields = wpw.tax.utilities.ensureArray(fields);

      return function (tools) {
        tools.checkMethod(tools.list(fields), function () {
          this.mark(params);
        });
        return true;
      }
    }

    diagUtils.diagnostic('2000001', common.check('002', 'isFilled'));

    diagUtils.diagnostic('2000002', function (tools) {
      return tools.field('bn').require('isFilled');
    });

    diagUtils.diagnostic('2000006', common.check('010', 'isFilled'));

    diagUtils.diagnostic('2000007', common.prereq(
      common.check('010', 'isYes'),
      common.check(['011', '015', '016', '017', '018'], 'isFilled', true)
    ));

    diagUtils.diagnostic('2000008', common.check('010', 'isFilled'));

    diagUtils.diagnostic('2000009', common.check('020', 'isFilled'));

    // NOTE: 2000010 is the same as 2000011
    diagUtils.diagnostic('2000011', common.prereq(
      common.check('020', 'isYes'),
      common.check(['021', '022', '025', '026', '027', '028'], 'isFilled', true)
    ));

    diagUtils.diagnostic('2000012', common.check('030', 'isFilled'));

    /*        2000013 is a duplicate of 2000014
     diagUtils.diagnostic('2000013', common.prereq(common.check('030', 'isYes'),
     common.check(['031', '035', '036', '037', '038'], 'isFilled', true)))
     .diagnostic(mark(['t2j.031', 't2j.035', 't2j.036', 't2j.037', 't2j.038']));*/

    diagUtils.diagnostic('2000014', common.prereq(
      common.check('030', 'isYes'),
      common.check(['031', '035', '036', '037', '038'], 'isFilled', true)
    ));

    diagUtils.diagnostic('2000016',
      common.check(['tax_start', 'tax_end'], 'isNonZero', true)
     );

    diagUtils.diagnostic('2000017', common.check('070', 'isFilled'));
    diagUtils.diagnostic('2000018', common.check('071', 'isFilled'));
    diagUtils.diagnostic('2000024', common.check('078', 'isFilled'));
    diagUtils.diagnostic('2000025', common.check('063', 'isFilled'));

    diagUtils.diagnostic('2000026', common.prereq(
      common.check('063', 'isYes'),
      function (tools) {
        return tools.field('065').require('isFilled');
      }));

    diagUtils.diagnostic('2000027', common.check('067', 'isFilled'));

    diagUtils.diagnostic('2000028', common.prereq(common.check('957', 'isNo'),
      function (tools) {
        return tools.requireAll(tools.list(['958', '959']), 'isFilled');
      }));

    diagUtils.diagnostic('2000029', common.check('990', 'isFilled'));
    diagUtils.diagnostic('2000030', common.check('280', 'isFilled'));

    diagUtils.diagnostic('2000031', common.prereq(function (tools) {
      var fields = tools.list(['910', '914', '918']);
      return tools.checkMethod(fields, 'isFilled');
    }, function (tools) {
      var fields = tools.list(['910', '914', '918']);
      return tools.requireAll(fields, 'isFilled');
    }));

    diagUtils.diagnostic('2000032',
      common.check(['950', '951', '954', '955', '956', '957'], 'isFilled', true));

    diagUtils.diagnostic('2000033', common.check('080', 'isFilled'));

    //2000035 is not applicable because we have dropdown that forces only one entry

    diagUtils.diagnostic('2000036', common.check('072', 'isFilled'));

    //2000038 is not applicable because we have a date picker box that forces proper date format

    diagUtils.diagnostic('2000039', common.prereq(common.check('tax_end', 'isFilled'),
      function (tools) {
        var taxEnd = tools.field('tax_end');
        return taxEnd.require(function () {
          return this.get().year >= 2015;
        });
      }));

    //todo: 2000040: "Corporations cannot submit more than one return with the same tax year end" Needs EFILE functionality

    diagUtils.diagnostic('2000041', common.check('076', 'isFilled'));

    diagUtils.diagnostic('2000045', common.prereq(common.check('080', 'isNo'), function (tools) {
      return tools.field('cp.081').mark().isFilled();
    }));

    diagUtils.diagnostic('2000046', common.prereq(function (tools) {
      return tools.field('cp.900').get() == 1;
    }, function (tools) {
      return tools.requireAll(tools.list(['910', '914', '918']), 'isFilled');
    }));

    diagUtils.diagnostic('2000047', common.prereq(common.check('tax_end', 'isFilled'), function (tools) {
      var taxYearEnd = tools.field('tax_end').mark();
      return wpw.tax.utilities.dateCompare.lessThanOrEqual(taxYearEnd.get(), wpw.tax.date.today());
    }));

    //2000050 is not applicable because we have dropdown that forces proper province codes

    diagUtils.diagnostic('2000051', common.prereq(common.check(['070', '071'], 'isYes'), function (tools) {
      return tools.requireAll(tools.list(['031', '035', '036', '037', '038']), 'isFilled')
    }));

    diagUtils.diagnostic('2000088', common.prereq(common.check('997', 'isNo'), common.check('088', 'isFilled')));

    diagUtils.diagnostic('2000100', common.check('Corp_Type', 'isFilled'));

    //2000150 is implemented in T2J

    diagUtils.diagnostic('2000270', common.check('270', 'isFilled'));

    diagUtils.diagnostic('2000299', common.prereq(common.check('280', 'isNo'), function (tools) {
      return tools.requireOne(tools.field('299'), 'isFilled');
    }));

    diagUtils.diagnostic('2000300', common.check('750', 'isFilled'));

    //2000801 is implemented in T2J

    diagUtils.diagnostic('2000997', common.prereq(common.check('997', 'isYes'),
      function (tools) {
        return tools.requireOne(tools.field('996'), 'isNonZero');
      }));

    diagUtils.diagnostic('2001000',
      {label: 'The corporation has answered No (2) at line 200063 and the tax year provided at lines 200060 and 200061 is greater than 371 days.'},
      common.prereq(common.check('063', 'isNo'), function (tools) {
        return tools.field('cp.Days_Fiscal_Period').require(function () {
          return this.get() < 372;
        })
      }));

    diagUtils.diagnostic('2001000',
      {label: 'The corporation has indicated an acquisition of control and the tax year is greater than 378 days.'},
      common.prereq(common.check('063', 'isYes'), function (tools) {
        return tools.field('cp.Days_Fiscal_Period').require(function () {
          return this.get() < 378;
        })
      }));

    //todo: 2001001: The entry provided at this line must start immediately after the tax year end date of the immediately preceding tax year.
    //Possibly needs collaborate integration?

    //todo 2001002: The entry provided at this line must be within 7 days of the day and the month of the preceding tax year end date
    //Possibly needs collaborate integration?

    //todo 2001003: The entry provided at this line cannot precede the date of incorporation or amalgamation in our records.
    //Possibly needs collaborate integration?

    diagUtils.diagnostic('2001007', common.prereq(common.check('067', 'isYes'), function (tools) {
      if (tools.field('tax_end').get()) {
        return tools.field('tax_end').require(function () {
          return this.get().month == 12 && this.get().day == 31;
        })
      }
    }));

    //TODO: Add 2001010 and 2001011 when we can check if prior year returns exist (error msg below for reference)
    //2001010: If the answer at line 200070 or 200071 is Yes (1), there cannot be a return filed or assessed for any prior tax year.
    //2001011: If the answer at line 200070 or 200071 is No (2), there must be a return filed or assessed for a prior tax year.

    //diagUtils.diagnostic('2001010', common.check(['070', '071'], 'isNo'));

    //diagUtils.diagnostic('2001011', common.check(['070', '071'], 'isYes'));

    diagUtils.diagnostic('2001015', common.prereq(common.check('076', 'isYes'),
      function (tools) {
        return tools.field('109').require(function () {
          return wpw.tax.utilities.dateCompare.equal(tools.field('tax_end').get(), tools.field('109').get());
        })
      }));

    //2001018: We have a custom BN Mod 10 check

    diagUtils.diagnostic('2001019', function (tools) {
      return tools.field('Days_Fiscal_Period').require(function () {
        return this.get() < 1095;
      })
    });

    diagUtils.diagnostic('2001020', common.check(['078'], 'isNo'));

    //todo: 2001102: If the entry at this line is not the same as in the previous tax year, there must be an entry at line 200043
    //Possibly needs collaborate integration?

    diagUtils.diagnostic('2001104', function (tools) {
      var dateField = tools.field('043');
      var dateStart = tools.field('tax_start');
      var dateEnd = tools.field('tax_end');
      // Only check diagnostic if all the dates have been filled out
      if (tools.checkMethod([dateField, dateStart, dateEnd], 'isFilled') == 3) {
        return tools.field('043').require(function () {
          return (wpw.tax.utilities.dateCompare.lessThanOrEqual(dateField.get(), dateEnd.get()) &&
            wpw.tax.utilities.dateCompare.greaterThanOrEqual(dateField.get(), dateStart.get()))
        });
      }
      return true;
    });

    diagUtils.diagnostic('2001268', common.prereq(common.check('266', 'isChecked'),
      function (tools) {
        return tools.field('Corp_Type').require(function () {
          return this.get() == '1';
        });
      }));

    // CUSTOM DIAGNOSTICS

    diagUtils.diagnostic('BN', common.bnCheck('bn'));

    diagUtils.diagnostic('CP.088', common.prereq(common.check('997', 'isYes'),
      function (tools) {
        return tools.field('cp.088').require('isEmpty');
      }
    ));

    diagUtils.diagnostic('CP.termsOfUse', common.prereq(common.and(
      common.check('088', 'isYes'),
      common.check('onlineMail.101', 'isNo')),
      function (tools) {
        return (tools.field('089').require('isEmpty') && tools.field('088').require('isNo')) || tools.field('onlineMail.101').require('isYes');
      }
    ));

    diagUtils.diagnostic('CP.011', common.prereq(common.check('010', 'isYes'), common.check(['011', '015', '016', '017', '018'], 'isFilled', true)));

    diagUtils.diagnostic('default', function (tools) {
      if (tools.isIgnored())
        return null;
      return tools.requireAll(tools.list(['tax_start', 'tax_end']), 'isNotDefault');
    });

    diagUtils.diagnostic('CP.prov_residence', function (tools) {
      var jurisdictionDropdown = tools.field('750').mark();
      var provs = tools.field('prov_residence').get();

      var numProvs = Object.keys(provs).filter(function (prov) {
        return provs[prov];
      }).length;

      return ((jurisdictionDropdown.get() == 'MJ' && numProvs > 1) ||
        (jurisdictionDropdown.get() !== 'MJ' && numProvs <= 1))
    });

    diagUtils.diagnostic('CP.2662', common.prereq(
      common.check('2661', 'isYes'),
      common.check(['2662', '2665'], 'isNo')
    ));

    /*diagUtils.diagnostic('CP.validNAICS', function(tools) {
      tools.field('299').mark();
      // Allow empty field
      if (!tools.field('299').isFilled()) {
        return true;
      }

      var code = tools.field('299').get();

      // the code should always be 6 digits
      if (('' + code).length !== 6) {
        return false;
      }

      // Query collaborate to see if this NAICS code really exists.
      var payload = {
        'UniqueId': 'NAICS Canada2012',
        'F': {
          'BatchSize': 10,
          'FilterBy': {
            'CompareOpType': 'eq',
            'PredicateType': 'Compare',
            // 'StringValue': code,
            'LeftExpression': {'FieldName': 'Code', 'Type': 'BaseModel', 'ExpressionType': 'FilterField'},
            'RightExpression': {ExpressionType: 'FilterValue', StringValue: code.toString()}
          }
        }
      };

      return tools.request.collaborateUuid().then(function(uuid) {
        payload.Uuid = uuid;
        return tools._getCollabData('EntityService', 'GetIndustryCodeList', payload).then(function(data) {
          return data.result.length > 0;
        }, function(err) {
          console.log('NAIC error', err);
          return false;
        });
      });

      // return tools.requireOne(tools.field('299'), function() {
      //   return Object.keys(wpw.tax.codes.naicsCode).indexOf(tools.field('299').get()) > -1;
      // });
    });*/

    diagUtils.diagnostic('CP.supportedDateCheck', common.prereq(common.check('tax_end', 'isFilled'), function (tools) {
      var taxYearEnd = tools.field('tax_end').mark();
      var supportedEndDate = tools.field('196');
      return wpw.tax.utilities.dateCompare.lessThanOrEqual(taxYearEnd.get(), supportedEndDate.get());
    }));

    diagUtils.diagnostic('CP.totalCheck', common.prereq(common.and(
      common.check('tax_end', 'isFilled'),
      common.check('280', 'isNo')),
      function (tools) {
        var total = parseFloat(tools.field('285').get() + tools.field('287').get() + tools.field('289').get());
        return tools.requireAll(tools.list(['285', '287', '289']), function () {
          return total > 0 && total <= 100;
        })
      }));

    // diagUtils.diagnostic('CP.105', common.check('105', 'isNonZero'));

    diagUtils.diagnostic('CP.107', common.prereq(common.check('071', 'isYes'),
      function (tools) {
        return tools.field('107').require(function () {
          return wpw.tax.utilities.dateCompare.equal(tools.field('tax_start').get(), tools.field('107').get());
        })
      }));

    diagUtils.diagnostic('CP.111', common.prereq(common.check('078', 'isYes'), common.check('111', 'isNonZero')));

    diagUtils.diagnostic('CP.270', common.prereq(
      common.check('270', 'isYes'),
      function (tools) {
        return tools.field('270').require('isNo') || tools.field('t2s125.9998').require(function () {
          return tools.field('t2s125.9998').getKey(0) > 0;
        })
      }));

    diagUtils.diagnostic('CP.147', common.prereq(common.or(
      common.check('750', function () {
        return this.get() == 'ON';
      }),
      common.check('prov_residence', function () {
        return this.getKey('ON');
      })),
      function (tools) {
        return tools.field('147').require('isNonZero');
      }));

    diagUtils.diagnostic('CP.shortYear', function (tools) {
      return tools.field('Days_Fiscal_Period').require(function () {
        return this.get() >= 365
      });
    });

    diagUtils.diagnostic('CP.ontarioCorpNumber', common.prereq(function (tools) {
      return tools.field('750').get() == 'ON' || tools.field('prov_residence').getKey('ON');
    }, common.check('146', 'isFilled')));


    diagUtils.diagnostic('CP.albertaCorpNumber', common.prereq(function (tools) {
      return tools.field('750').get() == 'AB' || tools.field('prov_residence').getKey('AB');
    }, common.check('135', 'isFilled')));

    diagUtils.diagnostic('CP.071', common.prereq(common.check(['070', '071'], 'isYes'), common.check('tax_end', 'isZero')));

    diagUtils.diagnostic('CP.146', common.prereq(function (tools) {
      return tools.field('750').get() != 'ON' && !tools.field('prov_residence').getKey('ON');
    }, common.check('146', 'isEmpty')));

    diagUtils.diagnostic('CP.167', common.prereq(common.and(
      function (tools) {
        return tools.field('167').isFalse() || wpw.tax.utilities.isNullUndefined(tools.field('167').get());
      },
      function (tools) {
        return tools.field('t2s4.700').cell(0, 0).isFilled() || tools.field('t2s7.400').isNonZero() || tools.field('t2s73.1000').size().rows > 0;
      }
    ), common.check('167', 'isChecked')));

    diagUtils.diagnostic('CP.IncomeSprinkling', common.or(
        common.check('1990', 'isNo'),
      common.check('1990', 'isEmpty')));

    diagUtils.diagnostic('1134.eligibility', common.prereq(function (tools) {
      return tools.field('T1134.102').isFilled();
    }, function (tools) {
      return tools.field('to-27').require(function () {
        return this.get() == '1'
      })
    }));
    diagUtils.diagnostic('1135.eligibility', common.prereq(common.check(['T1135.2000', 'T1135.2001'], 'isChecked'),
      function (tools) {
      return tools.field('to-28').require(function () {
        return this.get() == '1'
      })
    }));
    diagUtils.diagnostic('T2J.addressChange', common.prereq(
      common.check(['010', '020'], 'isYes'),
      common.check(['010', '020'], 'isNo', true)
    ));
    //to flag if the country name does not match one of the drop down countries
    diagUtils.diagnostic('017.countryCheck', {
      label: 'Entity country name does not match any country code provided by CRA. Please check entity country.',
      category: 'CUSTOM',
      severity: 'warning',
      ignorable: true
    }, function(tools) {
        if(wpw.tax.global.engagementProperties && wpw.tax.global.engagementProperties.parentEntity.addresses){
          var countryName = wpw.tax.global.engagementProperties.parentEntity.addresses[0].country;//get country name from SE
          for (var i in wpw.tax.codes.countryAddressCodes){
            if(wpw.tax.codes.countryAddressCodes[i].value.toUpperCase()==countryName.toUpperCase()){
              return true;
            }
          }
          tools.field('017').mark();
          return false;
        }
        return true;
      });

  });
})();
