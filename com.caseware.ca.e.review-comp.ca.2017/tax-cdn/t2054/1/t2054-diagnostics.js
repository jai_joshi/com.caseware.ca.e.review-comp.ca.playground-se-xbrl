(function() {
  wpw.tax.create.diagnostics('t2054', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2054'), forEach.row('2000', forEach.bnCheckCol(1, true))));


  });
})();
