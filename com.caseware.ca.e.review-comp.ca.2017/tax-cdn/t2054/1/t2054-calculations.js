(function () {

  wpw.tax.create.calcBlocks('t2054', function (calcUtils) {
    calcUtils.calc(function (calcUtils, field, form) {
      //corp info update
      calcUtils.getGlobalValue('002', 'CP', '002');
      calcUtils.getGlobalValue('001', 'CP', 'bn');
      field('010').assign(
          field('CP.011').get() + ' ' +
          field('CP.012').get() + ' ' +
          field('CP.015').get() + ' ' +
          field('CP.016').get() + ' ' +
          field('CP.017').get()
      );
      field('010').source(field('CP.011'));
      calcUtils.getGlobalValue('018', 'CP', '018');
      calcUtils.getGlobalValue('019', 'CP', '958');
      calcUtils.getGlobalValue('022', 'CP', '959');
    });
    calcUtils.calc(function (calcUtils, field, form) {
      //required information
      field('102').assign(field('100').get() - field('110').get());
    });

    calcUtils.calc(function (calcUtils, field, form) {
      //late-filling penalty
      field('141').assign(field('CP.955').get());
      var dueDate = field('142').get();
      var filingDate = field('141').get();
      if (dueDate && filingDate) {
        field('310').assign(filingDate.month - dueDate.month);
      }
      else {
        field('310').assign(0)
      }

      field('301').assign(field('300').get() * 1 / 100);
      field('311').assign(field('310').get() / 12);
      field('312').assign(field('301').get() * field('311').get());
      field('313').assign(field('310').get());
      field('314').assign(500 * field('313').get() / 12);
      field('320').assign(Math.min(field('312').get(), field('314').get()));
    });

    calcUtils.calc(function (calcUtils, field, form) {
      //part III tax
      field('123').assign(field('102').get());
      field('126').assign(field('123').get());
      field('127').assign(field('126').get() * 0.6);
    });

    calcUtils.calc(function (calcUtils, field, form) {
      //total payment accompanying this election
      field('131').assign(field('320').get());
      field('133').assign(field('127').get());
      field('134').assign(field('131').get() + field('133').get());
    });

    calcUtils.calc(function (calcUtils, field, form) {
      //election certification
      calcUtils.getGlobalValue('950', 'cp', '958');//name
      calcUtils.getGlobalValue('954', 'cp', '954');//position
    })
  })
})();
