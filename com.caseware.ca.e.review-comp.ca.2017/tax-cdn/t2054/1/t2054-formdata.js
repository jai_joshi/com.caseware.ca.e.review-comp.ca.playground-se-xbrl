(function() {

  wpw.tax.create.formData('t2054', {
    'formInfo': {
      'abbreviation': 'T2054',
      isRepeatForm: true,
      'title': 'Election for a Capital Dividend Under Subsection 83(2)',
      printNum: 'T2054',
      headerImage: 'canada-federal',
      formFooterNum: 'T2054 E (12-2017)',
      agencyUseOnlyBox: ['01'],
      'description': [
        {text: ''},
        {
          'type': 'list',
          'items': [
            {
              label: 'Private corporations can use this form to elect to have the provisions of subsection 83(2)' +
              ' apply to a dividend. For more Do not use this area information, see Income Tax Folio S3-F2-C1, ' +
              '<i>Capital Dividends</i> and Interpretation Bulletin IT-149, <i>Winding-up Dividend</i>.'
            },
            {
              label: 'All legislative references are to the <i>Income Tax Act</i> and <i>Income Tax Regulations</i>.'
            },
            {
              label: 'Mail one completed copy of this election, separately from any other return, ' +
              'to your tax service office on or before the earlier of:',
              sublist: [
                'the day the dividend becomes payable; and',
                'the first day on which any part of the dividend was paid',
                'Find your tax service office\'s address by going to ' +
                'cra.gc.ca/tso'.link('http://www.cra-arc.gc.ca/tso/')

              ]
            },
            {
              label: 'A capital dividend paid to a non-resident is subject to Part XIII withholding tax reported' +
              ' on an NR4, <i>Statement of Amounts Paid or Credited to Non-residents of Canada</i>.'
            },
            {
              label: 'Where subsection 83(2.1) applies to treat a capital dividend as a taxable dividend received' +
              ' by a shareholder, that dividend will be considered to be a capital dividend in determining any' +
              ' liability of the corporation for Part III tax and in calculating the corporation\'s capital' +
              ' dividend account.'
            },
            {
              label: 'If the amount of the dividend that is elected under subsection 83(2) exceeds the balance' +
              ' of the capital dividend account (CDA), the corporation may be required to pay Part III tax ' +
              'on the excess portion of the dividend. See page 2 for more information.'
            },
            {
              label: 'A capital dividend paid to a non-resident is subject to Part XIII withholding tax and ' +
              'must be reported on an NR4, <i>Statement of Amounts Paid or Credited to Non-residents of Canada</i>.'
            },
            {
              label: 'File T2SCH89, <i>Request for Capital Dividend Account Balance Verification</i> with your election.'
            },
            {
              label: 'Generally, amounts are added to your CDA as they are realized. For more information, ' +
              'see Income Tax Folio S3-F2-C1 <i>Capital Dividends</i>.'
            },
            {
              label: 'For CDA purposes, a corporation’s capital gain or loss from the disposition of a property' +
              ' is computed without reference to subclause 52(3)(a)(ii)(A)(II) and subparagraph 53(1)(b)(ii).'
            },
            {
              label: 'Where subsection 83(2.1) applies to treat a capital dividend as a taxable dividend received' +
              ' by a shareholder, that dividend will still be considered to be a capital dividend in determining' +
              ' any liability of the corporation for Part III tax and in calculating the corporation\'s CDA.'
            }
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    'sections': [
      {
        header: 'Part 1- Identification',
        rows: [
          {
            type: 'table',
            num: '1000'
          },
          {
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'Part 2 - Required information',
        rows: [
          {
            'label': 'The full amount of the dividend for which this election is made*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '100'
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Capital dividend account immediately before this dividend becomes payable** .',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '110'
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': '<b>Excess amount, if any, subject to Part III tax</b> (amount A<b> minus</b> amount B)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '102'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': '(enter at amount I in Part 5)',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Date the dividend becomes payable',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '120',
                  'type': 'date'
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              }
            ]
          },
          {
            'label': 'First day on which any part of the dividend was paid ' +
            '(enter only if earlier than the date the dividend becomes payable)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'type': 'date'
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              }
            ]
          },
          {
            'label': 'Date of immediately previous election (if applicable)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '140',
                  'type': 'date'
                },
                'padding': {
                  'type': 'tn',
                  'data': '140'
                }
              }
            ]
          },
          {
            label: '* Attach a certified copy of the resolution or authorization as required per Regulation 2101.',
            labelClass: 'fullLength'
          },
          {
            label: '** Complete form T2SCH89, <i>Request for Capital Dividend Account Balance Verification</i> ' +
            'to support the calculation of the CDA for this election.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'Part 3 - Dividends received from other corporations',
        rows: [
          {
            label: 'Since your last election, or since the beginning of your CDA calculation if you have never' +
            ' made an election, does your CDA include a capital dividend received from another corporation ' +
            'that would have made an election under subsection 83(2)?',
            tn: '200',
            num: '200',
            type: 'infoField',
            inputType: 'radio'
          },
          {
            label: 'If <b>yes</b>, provide the following information for the other corporation.',
            labelClass: 'fullLength'
          },
          {
            type: 'table',
            num: '2000'
          }
        ]
      },
      {
        header: 'Part 4 - Late-filing penalty',
        rows: [
          {
            'label': 'According to subsection 83(4), a late-filing penalty will apply to this election if ' +
            'filed after its due date.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Due date',
            'labelClass': 'tabbed',
            'type': 'infoField',
            'inputType': 'date',
            'labelWidth': '75%',
            'num': '142'
          },
          {
            'label': 'Filing date',
            'labelClass': 'tabbed',
            'type': 'infoField',
            'inputType': 'date',
            'labelWidth': '75%',
            'num': '141'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Calculation of late-filing penalty:',
            'labelClass': 'fullLength bold'
          },
          {
            'type': 'table',
            'num': '113'
          },
          {
            'label': 'Subtotal (amount D <b>multiplied</b> by amount E)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '312'
                }
              }
            ],
            indicator: 'F'
          },
          {
            'type': 'table',
            'num': '118'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Late-filing penalty</b> (amount F or G, whichever is less)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                underline: 'double',
                'input': {
                  'num': '320'
                },
                padding: {
                  type: 'tn',
                  data: '320'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '*** N represents the number of months or parts of a month in the period from the required filing date to the actual filing date.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        header: 'Part 5 - Part III tax',
        rows: [
          {
            label: 'According to subsection 184(2), Part III tax will apply on the excess amount.',
            labelClass: 'fullLength'
          },
          {
            'label': 'Excess amount of dividend declared subject to Part III tax (amount C in Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '123'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'type': 'table',
            'num': '124'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Note:</b> The corporation may be able to avoid paying Part III tax by filing an election ' +
            'under subsection 184(3), to treat the excess as a separate taxable dividend per Regulation 2106. ',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        header: 'Part 6 - Total payment accompanying this election',
        rows: [
          {
            'label': 'Late-filing penalty (amount H in Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '131'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': 'Part III tax (amount J in Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '133'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': 'Amount owing(amount K <b>plus</b> amount L)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                underline: 'double',
                'input': {
                  'num': '134'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'label': 'Total payment accompanying this election',
            labelClass: 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                underline: 'double',
                'input': {
                  'num': '400'
                },
                padding: {
                  type: 'tn',
                  data: '400'
                }
              }
            ], indicator: 'O'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Make your cheque or money order payable to the Receiver General. ' +
            'On the back, write T2054, your corporation\'s name, business number, and  tax year-end.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Note</b>: Daily compound interest, at prescribed rates, applies to unpaid amounts, ' +
            'including late-filing penalties.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        header: 'Part 7 - Election certification',
        rows: [
          {
            label: 'The corporation hereby elects to have the provisions of subsection 83(2) apply for the ' +
            'full amount of the dividend indicated herein. I certify that the information given ' +
            'in this election, and in all documents attached, is true, correct, and complete in every respect.',
            labelClass: 'fullLength'
          },
          {
            'type': 'table',
            'num': '135'
          },
          {
            label: 'You must file this form when you pay Non-Taxable Capital Dividends:',
            labelClass: 'bold fullLength'
          },
          {
            type: 'infoField',
            inputType: 'link',
            note: 'Please Click here to go to Capital Dividend Account(CDA) Workchart',
            formId: 't2s89'
          }
        ]
      },
      {
        header: 'Privacy statement',
        rows: [
          {
            label: 'Personal information is collected under the <i>Income Tax Act</i> to administer tax, benefits, ' +
            'and related programs. It may also be used for any purpose related to the administration ' +
            'or enforcement of the Act such as audit, compliance and the payment of debts owed to ' +
            'the Crown. It may be shared or verified with other federal, provincial/territorial ' +
            'government institutions to the extent authorized by law. Failure to provide this ' +
            'information may result in interest payable, penalties or other actions. Under ' +
            'the <i>Privacy Act</i>, individuals have the right to access their personal information ' +
            'and request correction if there are errors or omissions. Refer to Info Source ' +
            'http://www.cra-arc.gc.ca/gncy/tp/nfsrc/nfsrc-eng.html'.link('http://www.cra-arc.gc.ca/gncy/tp/nfsrc/nfsrc-eng.html') +
            ' personal information ' +
            'bank CRA PPU 047.',
            labelClass: 'fullLength'
          }
        ]
      }
    ]
  })
})();
