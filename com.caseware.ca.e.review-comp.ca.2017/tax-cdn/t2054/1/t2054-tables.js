(function() {

  var part4Col = [
    {
      colClass: 'std-input-col-width',
      'type': 'none',
      cellClass: 'alignLeft'
    },
    {
      colClass: 'std-spacing-width',
      'type': 'none'
    },
    {
      colClass: 'std-padding-width',
      'type': 'none'
    },
    {
      colClass: 'std-spacing-width',
      'type': 'none'
    },
    {
      colClass: 'std-input-width',
      cellClass: 'alignRight'
    },
    {
      colClass: 'std-spacing-width',
      'type': 'none'
    },
    {
      colClass: 'small-input-width',
      cellClass: 'alignRight',
      'type': 'none'
    },
    {
      'type': 'none'
    },
    {
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-padding-width',
      'type': 'none'
    }
  ];

  wpw.tax.create.tables('t2054', {
    '113': {
      'fixedRows': true,
      'infoTable': true,
      'columns': part4Col,
      'cells': [
        {
          '0': {
            'label': 'Amount of the dividend'
          },
          '2': {
            'tn': '300'
          },
          '4': {
            'num': '300'
          },
          '6': {
            'label': 'x 1%'
          },
          '8': {
            'num': '301'
          },
          '9': {
            'label': 'D'
          }
        },
        {
          '0': {
            'label': 'Amount N***'
          },
          '2': {
            'tn': '310'
          },
          '4': {
            'num': '310'
          },
          '6': {
            'label': '÷ 12'
          },
          '8': {
            'num': '311'
          },
          '9': {
            'label': 'E'
          }
        }
      ]
    },
    '118': {
      'fixedRows': true,
      'infoTable': true,
      'columns': part4Col,
      'cells': [
        {
          '0': {
            'label': '$500 x amount N***'
          },
          '4': {
            'num': '313'
          },
          '6': {
            'label': '÷ 12'
          },
          '8': {
            'num': '314',
            cellClass: 'doubleUnderline'
          },
          '9': {
            'label': 'G'
          }
        }
      ]
    },
    '124': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          'type': 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignRight'
        },
        {
          colClass: 'small-input-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': '<b>Part III tax</b> (enter "0" if not applicable)',
            'labelClass': 'fullLength'
          },
          '2': {
            'label': 'amount I'
          },
          '3': {
            'num': '126'
          },
          '4': {
            'label': 'x 60%='
          },
          '5': {
            'num': '127',
            cellClass: 'doubleUnderline'
          },
          '6': {
            'label': 'J'
          }
        }
      ]
    },
    '135': {
      'infoTable': 'true',
      'fixedRows': 'true',
      'columns': [
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          0: {
            tn: '950'
          },
          '1': {
            'num': '950',
            'type': 'text'
          },
          3: {
            tn: '954'
          },
          '4': {
            'num': '954',
            'type': 'text'
          }
        },
        {
          '1': {
            type: 'none',
            label: 'Name of authorized officer'
          },
          '4': {
            type: 'none',
            label: 'Position or office'
          }
        },
        {
          '1': {
            'type': 'text'
          },
          3: {
            tn: '955'
          },
          '4': {
            num: '955',
            'type': 'date'
          }
        },
        {
          '1': {
            type: 'none',
            label: 'Signature of authorized officer'
          },
          '4': {
            type: 'none',
            label: 'Date'
          }
        }
      ]
    },
    '1000': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1],
      'columns': [
        {
          'width': '60%',
          cellClass: 'alignLeft',
          type: 'text'
        },
        {
          cellClass: 'alignLeft',
          type: 'text'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Corporation\'s name',
            'num': '002', "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
            'tn': '002',
            cannotOverride: true
          },
          1: {
            'label': 'Business number',
            'num': '001',
            'tn': '001',
            cannotOverride: true
          }
        },
        {
          '0': {
            label: 'Address',
            num: '010',
            tn: '010',
            cannotOverride: true
          },
          '1': {
            label: 'Postal code',
            num: '018',
            tn: '018',
            cannotOverride: true
          }
        },
        {
          '0': {
            label: 'Name of contact person',
            tn: '019',
            num: '019',
            cannotOverride: true
          },
          '1': {
            label: 'Tax services office',
            tn: '020',
            num: '020'
          }
        },
        {
          '0': {
            label: 'Mailing address (complete only if different from address above)',
            tn: '021',
            num: '021'
          },
          1: {
            label: 'Telephone number',
            tn: '022',
            num: '022',
            cannotOverride: true
          }
        }
      ]
    },
    '2000': {
      columns: [
        {
          header: 'Corporation\'s name',
          tn: '210',
          type: 'text'
        },
        {
          header: 'Business number',
          tn: '220',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          header: 'Date dividend became payable by the other corporation (provide the same information separately for ' +
          'each dividend received)',
          tn: '230',
          type: 'date',
          colClass: 'std-input-width'
        }
      ]
    }
  })

})();
