(function() {

  function runSumByKeyCalc(calcUtils, tableNum, valColIndex, dateColIndex, conditionNum, dateNum) {
    var data = [];
    var field = calcUtils.field;
    field(tableNum).getRows().forEach(function(row) {
      var date = calcUtils.dateCompare.greaterThanOrEqual(row[dateColIndex].get(), wpw.tax.date(2017, 1, 1));
      var amount = row[valColIndex].get();
      data.push({'date': date, 'amount': amount})
    });
    field(conditionNum).assign(calcUtils.sumByKey(data, 'date', true, 'amount'));
    field(dateNum).assign(wpw.tax.date(2017, 1, 1))
  }

  function assetCalcs(calcUtils, suffix) {
    var num205 = '205' + suffix;
    var num206 = '206' + suffix;
    var num207 = '207' + suffix;
    var num300 = '300' + suffix;
    var num301 = '301' + suffix;
    var num302 = '302' + suffix;
    var num303 = '303' + suffix;
    var num304 = '304' + suffix;
    var num305 = '305' + suffix;
    var num306 = '306' + suffix;
    var num402 = '402' + suffix;
    var num403 = '403' + suffix;
    var num404 = '404' + suffix;
    var num405 = '405' + suffix;
    var num406 = '406' + suffix;
    var num407 = '407' + suffix;
    var num500 = '500' + suffix;
    var num501 = '501' + suffix;

    var field = calcUtils.field;
    var form = calcUtils.form;

    //additional
    calcUtils.sumBucketValues(num207, [num205, num206]);
    calcUtils.equals(num300, num207);
    calcUtils.field(num304).assign(calcUtils.field(num300).get() - calcUtils.field(num301).get() -
        calcUtils.field(num302).get() + calcUtils.field(num303).get());
    calcUtils.subtract(num306, num304, num305);
    //disposal
    calcUtils.subtract(num404, num402, num403);
    //only allow enter acb is its partial
    if (calcUtils.field(num405).get() == 1) {
      calcUtils.field(num406).disabled(false);
      calcUtils.field(num407).assign(Math.min(calcUtils.field(num404).get(), calcUtils.field(num306).get(), calcUtils.field(num406).get()));
    }
    else {
      calcUtils.removeValue([num406], true);
      calcUtils.field(num407).assign(Math.min(calcUtils.field(num404).get(), calcUtils.field(num306).get()));
    }
    //class 14.1
    if (field('101').get() == 14.1 && field('T2S10.101').get() == 1) {
      //to update business name dropdown
      var businessNameOptions = [];
      var ecpFormIds = calcUtils.allRepeatForms('T2S10W');
      ecpFormIds.forEach(function(form) {
        businessNameOptions.push({
          value: form.sequence,
          option: 'T2S10-' + form.field('1001').get()
        });
      });
      field('501').config('options', businessNameOptions);

      //get history of ECP from sch10w
      var linkedS10W = calcUtils.allRepeatForms('T2S10W')[field('501').get() - 1];

      linkedS10W.forEach(function(form) {
        var linkedTableS10W = form.field('1002');
        var table1002 = field('1002');
        var numRows = table1002.size().rows;
        var numCols = table1002.size().cols;
        for (var i = 0; i < numRows; i++) {
          calcUtils.removeTableRow(calcUtils.form().valueFormId, '1002', 0);
        }
        linkedTableS10W.getRows().forEach(function(row, rIndex) {
          calcUtils.addTableRow(calcUtils.form().valueFormId, '1002', rIndex);
          var talble1002Row = table1002[rIndex];
          for(var i = 0; i < numCols; i++) {
            talble1002Row[i].assign(row[i].get());
          }
        })
      });
      //any ECP purchased or disposed after jan 1, 2017 need to be recognized on 203 or 207
      if (field('306').get() == !0) {
        field('201').assign();
        field('401').assign()
      }
      else {
        runSumByKeyCalc(calcUtils, '1002', 2, 1, '205', '201');
        runSumByKeyCalc(calcUtils, '1002', 5, 4, '402', '401')
      }
    }
  }

  function updateFromSourceForm(calcUtils, sourceRepeatId) {
    var sourceFullFormID = 'T2S8W-' + sourceRepeatId;
    calcUtils.getGlobalValue('101', sourceFullFormID, '101');
    calcUtils.getGlobalValue('102', sourceFullFormID, '102');
    calcUtils.getGlobalValue('103', sourceFullFormID, '103');
  }

  var isConditionLoaded = false;

  wpw.tax.create.calcBlocks('t2s8add', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('provinces', 'cp', 'prov_residence');
      var sourceDocId = calcUtils.field('sourceRepeatId').get();
      if (sourceDocId)
        updateFromSourceForm(calcUtils, sourceDocId);

      //autofill date available-to-use but allow overriding
      var field201 = field('201');
      if (calcUtils.hasChanged(field201) || !isConditionLoaded && !angular.isUndefined(field201.get())) {
        calcUtils.equals('202', '201');
        calcUtils.field('202').disabled(false);
        isConditionLoaded = true;
      }

      field('203').assign(wpw.tax.actions.isWithinFiscalYear(field201.get()) ? 1 : 2);

      assetCalcs(calcUtils, '');
      assetCalcs(calcUtils, '-Q');
      assetCalcs(calcUtils, '-A');
    });
  });

})();
