(function() {
  var expandableColumns = [
    {type: 'none'},
    {colClass: 'std-padding-width', type: 'none'},
    {colClass: 'std-input-width', header: 'Federal', labelClass: 'noBorder', filters: 'number', 'formField': true},
    {
      colClass: 'std-padding-width',
      type: 'none'
    },
    {
      colClass: 'std-input-width',
      header: 'Quebec',
      labelClass: 'noBorder',
      showWhen: {fieldId: 'provinces', has: {QC: true}},
      'formField': true,
      filters: 'number'
    },
    {
      colClass: 'std-padding-width',
      type: 'none',
      showWhen: {fieldId: 'provinces', has: {QC: true}}
    },
    {
      colClass: 'std-input-width',
      header: 'Alberta',
      labelClass: 'noBorder',
      showWhen: {fieldId: 'provinces', has: {AB: true}},
      'formField': true,
      filters: 'number'
    },
    {
      colClass: 'std-padding-width',
      type: 'none',
      showWhen: {fieldId: 'provinces', has: {AB: true}}
    }
  ];

  wpw.tax.global.tableCalculations.t2s8add = {
    "1000": {
      "infoTable": false,
      "fixedRows": true,
      "columns": [
        {
          'header': 'CCA Class',
          cellClass: 'alignCenter',
          type: 'selector',
          "width": "115px",
          "disabled": true,
          selectorOptions: {
            title: 'CCA Classes',
            items: wpw.tax.codes.ccaClasses,
            hideKeys: true
          }
        },
        {
          "header": "Class description",
          "disabled": true,
          type: 'text'
        }],
      "cells": [{
        "0": {
          "num": "101",
          disabled: true
        },
        "1": {
          "num": "102",
          type: 'text'
        }
      }],
      tableCalculations: function(paramsObj) {
        var summaryRow = paramsObj.summaryRow;
        var tableCalcs = paramsObj.tableCalcUtils;
        var numChanged = paramsObj.numChanged;
        var bucket = paramsObj.bucket;
        var rowIndex = paramsObj.rowIndex;
        var colIndex = paramsObj.colIndex;

      }
    },
    "1010": {
      type: 'table', num: '1010', infoTable: true, fixedRows: true,
      columns: expandableColumns,
      cells: [
        {
          0: {'label': 'Net cost', labelClass: 'bold'},
          2: {num: '300'},
          4: {num: '300-Q'},
          6: {num: '300-A'}
        },
        {
          0: {'label': 'Adjustments not recorded above', labelClass: 'bold'},
          2: {type: 'none'},
          4: {type: 'none'},
          6: {type: 'none'}
        },
        {
          0: {'label': 'Less: Government Assistance', labelClass: 'tabbed'},
          2: {num: '301'},
          4: {num: '301-Q'},
          6: {num: '301-A'}
        },
        {
          0: {'label': 'Less: I.T.C', labelClass: 'tabbed'},
          2: {num: '302'},
          4: {num: '302-Q'},
          6: {num: '302-A'}
        },
        {
          0: {'label': 'Add: Other adjustments', labelClass: 'tabbed'},
          2: {num: '303'},
          4: {num: '303-Q'},
          6: {num: '303-A'}
        },
        {
          0: {'label': 'Adjusted Cost Base before partial disposition', labelClass: 'bold'},
          2: {num: '304'},
          4: {num: '304-Q'},
          6: {num: '304-A'}
        },
        {
          0: {
            'label': 'Less: Adjusted Cost Base before partial disposition (if applicable) in prior years',
            labelClass: 'tabbed'
          },
          2: {num: '305'},
          4: {num: '305-Q'},
          6: {num: '305-A'}
        },
        {
          0: {'label': 'Adjusted Cost Base', labelClass: 'bold'},
          2: {num: '306'},
          4: {num: '306-Q'},
          6: {num: '306-A'}
        }
      ]
    },
    "1020": {
      type: 'table', num: '1020', infoTable: true, fixedRows: true,
      columns: expandableColumns,
      cells: [
        {
          0: {'label': 'Proceeds of disposal (from accounting records)', labelClass: 'bold'},
          2: {num: '402'},
          4: {num: '402-Q'},
          6: {num: '402-A'}
        },
        {
          0: {'label': 'Less: Expenses with respect to the disposition', labelClass: 'tabbed'},
          2: {num: '403'},
          4: {num: '403-Q'},
          6: {num: '403-A'}
        },
        {
          0: {'label': 'Net proceeds of disposition', labelClass: 'bold'},
          2: {num: '404'},
          4: {num: '404-Q'},
          6: {num: '404-A'}
        },
        {
          0: {'label': 'Is this a partial disposition'},
          2: {num: '405', type: 'radio', init: '2'},
          4: {num: '405-Q', type: 'radio', init: '2'},
          6: {num: '405-A', type: 'radio', init: '2'}
        },
        {
          0: {'label': 'If it is a partial disposition - enter amount of ACB disposed', labelClass: 'tabbed'},
          2: {num: '406'},
          4: {num: '406-Q'},
          6: {num: '406-A'}
        },
        {
          0: {'label': 'Lesser of adjusted cost base or net proceeds', labelClass: 'bold'},
          2: {num: '407'},
          4: {num: '407-Q'},
          6: {num: '407-A'}
        }
      ]
    },
    '1002': {
      'type': 'table',
      'infoTable': false,
      fixedRows: true,
      initRows: 0,
      'num': '1002',
      'maxLoop': 100000,
      'columns': [
        {
          'header': 'Description',
          'disabled': true,
          type: 'text'
        },
        {
          'header': 'Acquisition date',
          'type': 'date',
          'disabled': true,
          colClass: 'std-input-width'

        },
        {
          'header': 'Cost of acquisition',
          'disabled': true,
          colClass: 'std-input-width'
        },
        {
          'header': 'Other adjustments',
          'disabled': true,
          colClass: 'std-input-width'
        },
        {
          'header': 'Disposal date',
          'type': 'date',
          'disabled': true,
          colClass: 'std-input-width'
        },
        {
          'header': 'Proceeds of disposition',
          'disabled': true,
          colClass: 'std-input-width'
        },
        {
          'header': 'Other adjustments',
          'disabled': true,
          colClass: 'std-input-width'
        }]
    }
  }
})();
