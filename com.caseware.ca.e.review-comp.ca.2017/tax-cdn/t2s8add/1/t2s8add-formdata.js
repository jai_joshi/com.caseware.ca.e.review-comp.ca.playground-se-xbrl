(function() {
  'use strict';

  wpw.tax.global.formData.t2s8add = {
    formInfo: {
      abbreviation: 'T2S8ADD',
      isRepeatForm: true,
      repeatFormData: {
        titleNum: '103',
        linkedRepeatTable: 't2s8w.600',
        nestedLayer: true
      },
      title: 'Fixed Assets Listing (for CCA)',
      subTitle: 'For all CCA Classes except 10.1, 13 & 14',
      schedule: 'SCHEDULE 8 ADDITIONAL',
      showCorpInfo: true,
      category: 'Workcharts'
    },
    sections: [
      {
        header: 'CCA Class Information',
        rows: [
          {
            'type': 'table', 'num': '1000'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            label: 'Other CCA Class Description',
            'indicatorColumn': true,
            inputClass: 'std-input-col-width-3',
            num: '103'
          },
          {labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {label: 'The Above is Updated From S8 - CCA Workchart', labelClass: 'fullLength bold center'}
        ]
      },
      {
        header: 'Class 14.1 Only',
        showWhen: {fieldId: '101', compare: {is: '14.1'}},
        rows: [
          {
            label: 'Business name:',
            'indicatorColumn': true,
            type: 'infoField',
            inputType: 'dropdown',
            num: '501'
          }
        ]
      },
      {
        header: 'Depreciable Asset Information',
        rows: [
          {labelClass: 'fullLength'},
          {type: 'infoField','indicatorColumn': true, label: 'GL Account # where this asset is recorded', num: '104'},
          {type: 'infoField','indicatorColumn': true, label: 'GL Account name', num: '105'},
          {type: 'infoField','indicatorColumn': true, label: 'Asset Description', num: '106', inputClass: 'std-input-col-width-3'},
          {type: 'infoField','indicatorColumn': true, label: 'Asset Serial #/ID #', num: '107'},
          {type: 'infoField','indicatorColumn': true, label: 'Asset Location', num: '108', inputClass: 'std-input-col-width-3'},
          {type: 'infoField','indicatorColumn': true, label: 'PO #/Ref #', num: '109'},
          {type: 'infoField','indicatorColumn': true, label: 'Warranty #(if applicable)', num: '110'},
          {type: 'infoField','indicatorColumn': true, label: 'Warranty expiration date', num: '111', inputType: 'date'},
          {label: 'Other Notes'},
          {type: 'infoField','indicatorColumn': true, num: '112', inputType: 'textArea'}
        ]
      },
      {
        header: 'Historical/ New Addition Information',
        rows: [
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            'indicatorColumn': true,
            label: 'Date of Purchase',
            num: '201',
            inputType: 'date',
            labelWidth: '788px'
          },
          {
            type: 'infoField',
            'indicatorColumn': true,
            label: 'Date Asset was "Available for use"',
            num: '202',
            inputType: 'date',
            labelWidth: '788px'
          },
          {
            type: 'infoField',
            'indicatorColumn': true,
            inputType: 'radio',
            label: '(If date is within current fiscal period) Confirm asset was purchased during the year',
            num: '203',
            init: '1',
            labelWidth: '823px'
          },
          {
            type: 'infoField',
            'indicatorColumn': true,
            inputType: 'radio',
            label: 'Does the half-year rule apply?',
            num: '204',
            init: '1',
            labelWidth: '823px'
          },
          {
            'label': 'Original cost',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '205'
                }
              }
            ]
          },
          {
            'label': 'Other adjustment (from Accounting Records)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '206'
                }
              }
            ]
          },
          {
            'label': 'Net cost',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '207'
                }
              }
            ]
          },
          {
            type: 'table', num: '1010'
          }
        ]
      },
      {
        header: 'Disposition Information',
        rows: [
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            'indicatorColumn': true,
            label: 'Disposal date',
            num: '401',
            inputType: 'date',
            labelWidth: '788px'
          },
          {
            type: 'table', num: '1020'
          }
        ]
      },
      {
        header: 'Eligible Capital Property (Schedule 10) Addition/Deduction Information',
        showWhen: {
          and: [
            {fieldId: '101', compare: {is: 14.1}},
            {fieldId: 'T2S10.1000'}
          ]
        },
        rows: [
          {
            type: 'infoField',
            inputType: 'link',
            note: 'Please click here to go to Schedule 10 Workchart to add update additions and disposition data',
            formId: 'T2S10W'
          },
          {
            type: 'table',
            num: '1002'
          }
        ]
      }
    ]
  };
})();
