(function() {
  wpw.tax.create.diagnostics('t2s384', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var limitCheckFields = [
      {errorCode: '384.155Limit', tableNum: '150', limit: '20000'}, {
        errorCode: '384.260Limit',
        tableNum: '300',
        limit: '10000'
      },
      {errorCode: '384.360Limit', tableNum: '400', limit: '50000'}, {
        errorCode: '384.460Limit',
        tableNum: '500',
        limit: '33333'
      },
      {errorCode: '384.560Limit', tableNum: '600', limit: '25000'}, {
        errorCode: '384.760Limit',
        tableNum: '700',
        limit: '20000'
      },
      {errorCode: '384.660Limit', tableNum: '800', limit: '33333'}
    ];
    var requiredCols = [
      {errorCode: '3840005', tableNum: '100'}, {errorCode: '3840009', tableNum: '300'},
      {errorCode: '3840010', tableNum: '400'}, {errorCode: '3840011', tableNum: '500'},
      {errorCode: '3840012', tableNum: '600'}, {errorCode: '3840013', tableNum: '800'},
      {errorCode: '3840014', tableNum: '150'}, {errorCode: '3840015', tableNum: '700'}
    ];

    var limitCheck = function(diagObject) {
      diagUtils.diagnostic(diagObject.errorCode, common.prereq(common.requireFiled('T2S384'),
          function(tools) {
            var table = tools.field(diagObject.tableNum);
            return tools.checkAll(table.getRows(), function(row) {
              if (row[1].isNonZero()) {
                return row[1].require(function() {
                  if (diagObject.tableNum == '300' && tools.field('cp.tax_end').get()['year'] > 2015) {
                    return this.get() <= '33333';
                  } else {
                    return this.get() <= diagObject.limit;
                  }
                });
              } else return true;
            });
          }));
    };

    var requiredColCheck = function(diagObject) {
      diagUtils.diagnostic(diagObject.errorCode, common.prereq(common.requireFiled('T2S384'),
          function(tools) {
            var table = tools.field(diagObject.tableNum);
            return tools.checkAll(table.getRows(), function(row) {
              if (row[1].isNonZero())
                return row[0].require('isFilled');
              else return true;
            });
          }));
    };

    for (var i = 0; i < limitCheckFields.length; i++) {
      limitCheck(limitCheckFields[i]);
    }

    for (i = 0; i < requiredCols.length; i++) {
      requiredColCheck(requiredCols[i]);
    }

    diagUtils.diagnostic('3840001', common.prereq(common.check(['t2s5.603', 't2s5.622'], 'isNonZero'), common.requireFiled('T2S384')));

    diagUtils.diagnostic('3840004', common.prereq(common.requireFiled('T2S384'),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[0].isFilled())
              return row[1].require('isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('3840007', common.prereq(common.and(
        common.requireFiled('T2S384'),
        common.check('125', 'isNonZero')),
        function(tools) {
          var table = tools.field('100');
          return tools.checkAll(table.getRows(), function(row) {
            return row[1].require('isNonZero');
          });
        }));

  });
})();
