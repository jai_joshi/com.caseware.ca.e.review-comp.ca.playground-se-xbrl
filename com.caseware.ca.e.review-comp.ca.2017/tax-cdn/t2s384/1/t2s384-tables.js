(function() {
  var inputValueWidth = '107px';

  wpw.tax.create.tables('t2s384', {
    '3000': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {type: 'none'},
        {type: 'none', width: '600px'},
        {type: 'singleCheckbox', width: '60px'},
        {type: 'none'}
      ],
      cells: [
        {
          '1': {label: 'Part 2 - Youth work experience hiring incentive'},
          '2': {num: '3001'}
        },
        {
          '1': {label: 'Part 3 - Co-op student hiring incentive'},
          '2': {num: '3002'}
        },
        {
          '1': {label: 'Part 4 - Co-op graduate hiring incentive'},
          '2': {num: '3003'}
        },
        {
          '1': {label: 'Part 5 - Apprentice hiring incentive'},
          '2': {num: '3004'}
        },
        {
          '1': {label: 'Part 6 - Apprentice hiring incentive for rural or northern early level apprentice'},
          '2': {num: '3005'}
        },
        {
          '1': {label: 'Part 7 - Apprentice hiring incentive for high school apprentice'},
          '2': {num: '3006'}
        },
        {
          '1': {label: 'Part 8 - Journeyperson hiring incentive'},
          '2': {num: '3007'}
        }
      ]
    },
    '100': {
      hasTotals: true,
      'showNumbering': true,
      'columns': [
        {
          header: 'Serial number shown on the proof-of-credit certificate',
          tn: '100',
          type: 'custom',
          format: ['{N7}MB'],
          validate: {check: 'mod10'}
        },
        {
          header: 'Refundable credit *',
          width: '130px',
          total: true,
          totalMessage: ' ',
          tn: '203',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          totalNum: '203'
        }
      ]
    },
    '150': {
      hasTotals: true,
      'showNumbering': true,
      'columns': [
        {
          header: 'A1 <br> Name of qualifying youth ',
          tn: '150',"validate": {"or":[{"length":{"min":"1","max":"60"}},{"check":"isEmpty"}]},
          type: 'text'
        },
        {
          header: 'B1 <br> Salary and wages paid * ',
          tn: '155',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          width: inputValueWidth
        },
        {
          header: 'C1 <br> B1 x 25%',
          width: inputValueWidth
        },
        {
          header: '<br> Amount of the incentive you claimed for a previous period',
          width: inputValueWidth
        },
        {
          header: 'D1 <br>Lesser of C1 and maximum amount **',
          tn: '170',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          total: true,
          totalNum: '171',
          totalMessage: ' ',
          width: inputValueWidth
        }
      ]
    },
    '300': {
      hasTotals: true,
      'showNumbering': true,
      'columns': [
        {
          header: 'A2 <br> Name of co-op student <br> (maximum 5 placements per co-op student)',
          tn: '250',"validate": {"or":[{"length":{"min":"1","max":"60"}},{"check":"isEmpty"}]},
          type: 'text'
        },
        {
          header: 'B2 <br> Salary and wages paid * ',
          tn: '260',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          width: inputValueWidth
        },
        {
          header: 'C2 <br> B2 x 15%',
          width: inputValueWidth
        },
        {
          header: '<br> Amount of the incentive you claimed for a previous period',
          width: inputValueWidth
        },
        {
          header: 'D2 <br>Lesser of C2 and maximum amount **',
          tn: '270',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          total: true,
          totalNum: '271',
          totalMessage: ' ',
          width: inputValueWidth
        }
      ]
    },
    '400': {
      hasTotals: true,
      'showNumbering': true,
      'columns': [
        {
          header: 'A3 <br> Name of qualifying graduate <br> (first two full years of full-time employment only)',
          tn: '350',"validate": {"or":[{"length":{"min":"1","max":"60"}},{"check":"isEmpty"}]},
          type: 'text'
        },
        {
          header: 'B3 <br> Salary and wages paid * ',
          tn: '360',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          width: inputValueWidth
        },
        {
          header: 'C3 <br> B3 x 15%',
          width: inputValueWidth
        },
        {
          header: '<br> Number of days in the qualifying period of employment',
          width: inputValueWidth
        },
        {
          header: 'D3 <br>Lesser of C3 and maximum amount **',
          tn: '370',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          total: true,
          totalNum: '371',
          totalMessage: ' ',
          width: inputValueWidth
        }
      ]
    },
    '500': {
      hasTotals: true,
      'showNumbering': true,
      'columns': [
        {
          header: 'A4 <br> Name of qualifying apprentice ',
          tn: '450',"validate": {"or":[{"length":{"min":"1","max":"60"}},{"check":"isEmpty"}]},
          type: 'text'
        },
        {
          header: 'B4 <br> Salary and wages paid * ',
          tn: '460',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          width: inputValueWidth
        },
        {
          header: 'C4 <br> B4 x 15%',
          width: inputValueWidth
        },
        {
          header: '<br> Number of days in the qualifying period of employment',
          width: inputValueWidth
        },
        {
          header: 'D4 <br>Lesser of C4 and maximum amount **',
          tn: '470',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          total: true,
          totalNum: '471',
          totalMessage: ' ',
          width: inputValueWidth
        }
      ]
    },
    '600': {
      hasTotals: true,
      'showNumbering': true,
      'columns': [
        {
          header: 'A5 <br> Name of qualifying apprentice ',
          tn: '550',"validate": {"or":[{"length":{"min":"1","max":"60"}},{"check":"isEmpty"}]},
          type: 'text'
        },
        {
          header: 'B5 <br> Salary and wages paid * ',
          tn: '560',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          width: inputValueWidth
        },
        {
          header: 'C5 <br> B5 x 20%',
          width: inputValueWidth
        },
        {
          header: '<br> Number of days in the qualifying period of employment',
          width: inputValueWidth
        },
        {
          header: 'D5 <br>Lesser of C5 and maximum amount **',
          tn: '570',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          total: true,
          totalNum: '571',
          totalMessage: ' ',
          width: inputValueWidth
        }
      ]
    },
    '700': {
      hasTotals: true,
      'showNumbering': true,
      'columns': [
        {
          header: 'A6 <br> Name of qualifying apprentice ',
          tn: '750',"validate": {"or":[{"length":{"min":"1","max":"60"}},{"check":"isEmpty"}]},
          type: 'text'
        },
        {
          header: 'B6 <br> Salary and wages paid * ',
          tn: '760',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          width: inputValueWidth
        },
        {
          header: 'C6 <br> B6 x 25%',
          width: inputValueWidth
        },
        {
          header: '<br> Number of days in the qualifying period of employment',
          width: inputValueWidth
        },
        {
          header: 'D6 <br>Lesser of C6 and maximum amount **',
          tn: '770',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          total: true,
          totalNum: '771',
          totalMessage: ' ',
          width: inputValueWidth
        }
      ]
    },
    '800': {
      hasTotals: true,
      'showNumbering': true,
      'columns': [
        {
          header: 'A7 <br> Name of qualifying journeyperson <br> (first two full years of full-time employment only) ',
          tn: '650',"validate": {"or":[{"length":{"min":"1","max":"60"}},{"check":"isEmpty"}]},
          type: 'text'
        },
        {
          header: 'B7 <br> Salary and wages paid * ',
          tn: '660',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          width: inputValueWidth
        },
        {
          header: 'C7 <br> B7 x 15%',
          width: inputValueWidth
        },
        {
          header: '<br> Number of days in the qualifying period of employment',
          width: inputValueWidth
        },
        {
          header: 'D7 <br>Lesser of C7 and maximum amount **',
          tn: '670',"validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          total: true,
          totalNum: '671',
          totalMessage: ' ',
          width: inputValueWidth
        }
      ]
    },
    '1100': {
      'fixedRows': true,
      hasTotals: true,
      infoTable: true,
      'columns': [
        {
          colClass: 'std-address-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true,
          header: '<b>Year of origin</b>'
        },
        {
          'type': 'none'
        },
        {
          'width': '160px',
          header: '<b>Credit available for carryforward</b>',
          'total': true,
          'totalNum': '401',
          'totalMessage': 'Total (equal to line 200 in Part 10)'
        },
        {type: 'none'}
      ],
      'cells': [
        {
          '0': {
            'label': '10th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '9th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '8th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '7th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '6th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '5th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '4th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '3rd previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '2nd previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '1st previous tax year ending on'
          }
        }
      ]
    },
    '1200': {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {colClass: 'std-padding-width', type: 'none'},
        {
          colClass: 'std-input-width',
          type: 'date',
          disabled: true,
          header: '<b>Year of origin</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '1201',
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '1203',
          totalMessage: ' ',
          header: '<b>Transfer Amount</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', totalNum: '1204',
          header: '<b>Amount available to apply</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '1205',
          header: '<b>Applied<b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '1206',
          header: '<b>Balance to carry forward</b>'
        },
        {colClass: 'std-padding-width', type: 'none'}
      ],
      cells: [
        {
          '4': {label: '*'},
          '5': {type: 'none'},
          '7': {type: 'none'},
          '9': {type: 'none'},
          '11': {type: 'none', label: 'N/A'}
        },
        {
          '12': {label: '**'}
        },
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {}
      ]
    }
  })
})();
