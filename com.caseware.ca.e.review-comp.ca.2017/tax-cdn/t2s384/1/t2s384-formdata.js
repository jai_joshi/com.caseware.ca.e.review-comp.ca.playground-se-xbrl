(function() {
  'use strict';
  wpw.tax.create.formData('t2s384', {
    formInfo: {
      abbreviation: 't2s384',
      title: 'Manitoba Paid Work Experience Tax Credit ',
      //subTitle: '(2015 and later tax years)',
      schedule: 'Schedule 384',
      code: 'Code 1502',
      formFooterNum: 'T2 SCH 384 E (15)',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'You can claim a Manitoba paid work experience tax credit under section 10.1 of the Manitoba ' +
              '<i>Income Tax Act</i> if, at any time in the tax year, you paid salary and wages to:',
              sublist: [
                'a <b>qualifying youth</b> who has completed an <b>approved youth work experience training program</b>;',
                'a co-op student under a <b>qualifying work placement</b>; or ',
                'a <b>qualifying graduate</b>, a <b>qualifying apprentice</b>, or a <b>qualifying journeyperson</b>' +
                ', for a <b>qualifying period of employment</b>'
              ]
            },
            {
              label: 'This tax credit is made up of the following:',
              sublist: [
                '<b>youth work experience hiring incentive;</b>',
                '<b>co-op student hiring incentive;</b>',
                '<b>co-op graduate hiring incentive;</b>',
                '<b>apprentice hiring incentive;</b>',
                '<b>journeyperson hiring incentive</b>'
              ]
            },
            {
              label: 'The terms identified are defined in subsection 10.1 of the Manitoba <i>Income Tax Act</i>. ' +
              'For more information, go to the Manitoba Tax Assistance Office page on the Manitoba Finance website ' +
              'at' + ' www.gov.mb.ca/finance.'.link('http://www.gov.mb.ca/finance/')
            },
            {
              label: 'For the 2015 tax year, you can claim a <b>transitional 2015 hiring incentive</b> if you ' +
              'have a Manitoba Government proof-of-credit certificate issued for a qualifying work placement or a ' +
              'qualifying period of employment that ended in the 2015 tax year for work performed in the 2014 tax year'
            },
            {
              label: 'This credit is refundable and cannot be carried back or carried forward. ' +
              'If you have an unused non-refundable credit for a work placement that ended before ' +
              'March 7, 2006, it can be carried forward 10 years.'
            },
            {
              label: 'Use this schedule to:',
              sublist: [
                'claim a refund of the current-year credit;', '' +
                'claim the credit to reduce Manitoba income tax otherwise payable in the current tax year;', '' +
                'calculate the non-refundable credits you have available to carry forward; or',
                'transfer a credit after an amalgamation or the wind-up of a subsidiary, as described in subsections ' +
                '87(1) and 88(1) of the federal <i>Income Tax Act</i> respectively.'
              ]
            },
            {
              label: 'To claim this credit, file a completed copy of this schedule with your <i>T2 Corporation ' +
              'Income Tax Return</i>, along with a copy of the proof-of-credit certificate if you are claiming the ' +
              'transitional 2015 hiring incentive. If you file electronically, keep the certificate ' +
              'in case we ask for it later.'
            },
            {
              label: 'The salary and wages of an employee are net of any other government assistance ' +
              'you received or are entitled to receive'
            },
            {
              label: 'If you are an employer that is exempt under section 149 of the federal <i>Income Tax Act</i>, ' +
              'you can use this schedule; however, to claim the credit, you will also have to complete Schedule 5, ' +
              '<i>Tax Calculation Supplementary – Corporations</i>, and file a <i>T2 Corporation Income Tax Return</i>.'
            },
            {
              label: 'If you need more space, attach additional schedules.'
            }
          ]
        }
      ],
      category: 'Manitoba Forms'
    },
    sections: [
      {
        'header': 'Claimable incentives based on tax-year-end',
        'rows': [
          {
            'label': 'Only applicable incentives are displayed. Use the following to identify which incentives can be claimed:'
          },
          {
            'type': 'table',
            'num': '3000'
          }
        ]
      },
      {
        'header': 'Part 1 - Transitional 2015 hiring incentive',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total transitional 2015 hiring incentive',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '125',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '125'
                }
              }
            ]
          },
          {
            'label': '* If you are part of a partnership, prorate the credit according to the share of salary and wages you paid. '
          }
        ]
      },
      {
        'header': 'Part 2 - Youth work experience hiring incentive',
        'showWhen': {
          'fieldId': '3001'
        },
        'rows': [
          {
            'label': 'Effective September 1, 2015'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '150'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit allocated for qualifying youths whose salaries and wages were paid by a partnership.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '175',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '175'
                }
              }
            ]
          },
          {
            'label': '<b>Total youth work experience hiring incentive claimed</b>: total of column D1 <b>plus</b> line 175',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '180',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '180'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Less the amount of any other government assistance received or receivable. Maximum amount is $20,000 per qualified youth.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** Lifetime maximum amount is $5,000 per qualifying youth for all qualifying periods of employment, less the amount of the incentive you claimed for a previous period of employment of that youth.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Co-op student hiring incentive',
        'showWhen': {
          'fieldId': '3002'
        },
        'rows': [
          {
            'type': 'table',
            'num': '300'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit allocated for co-op students whose salaries and wages were paid by a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '275',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '275'
                }
              }
            ]
          },
          {
            'label': '<b>Total co-op student hiring incentive claimed</b>: total of column D2 <b>plus</b> line 275',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '280',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '280'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Less the amount of any other government assistance received or receivable. Maximum amount is $10,000 per co-op student.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** Maximum amount is $1,000 per placement, less the amount of the incentive you calculated for a previous tax year for this work placement. If the student is shared by more than one employer, calculate D2 as the lesser of C2 and the maximum amount multiplied by A/B. ',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'tabbed fullLength',
            'label': 'A = total salary and wages you paid to the student for the work placement of that student. <br>B = total salary and wages paid to the student for the work placement of that student.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Note</b>: For tax years ending after December 31, 2015, the co-op student hiring incentive is equal to 15% of qualifying salary and wages paid to the student less any government assistance received or receivable for the period and less the amount of incentive you claimed for salary and wages paid to the student in a previous tax year or claimed by any employer for a previous work placement of that student up to a maximum of $5,000 per co-op student under a qualifying work placement.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 4 - Co-op graduate hiring incentive',
        'showWhen': {
          'fieldId': '3003'
        },
        'rows': [
          {
            'type': 'table',
            'num': '400'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit allocated for qualifying graduates whose salaries and wages were paid by a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '375',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '375'
                }
              }
            ]
          },
          {
            'label': '<b>Total co-op graduate hiring incentive claimed</b>: total of column D3 <b>plus</b> line 375',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '380',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '380'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Less the amount of any other government assistance received or receivable. Maximum amount is $50,000 per graduate.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** Maximum amount is $2,500 multiplied by the number of days in the qualifying period of employment divided by 365. If the graduate is shared by more than one employer, calculate column D3 as the lesser of C3 and the maximum amount multiplied by A/B.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'tabbed fullLength',
            'label': 'A = total salary and wages you paid to the graduate for a qualifying period of employment.<br> B = total salary and wages paid to the graduate for the qualifying period of employment.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Note</b>: For tax years ending after December 31, 2015, the co-op graduate hiring incentive is equal to 15% of qualifying salary and wages paid to the graduate less any government assistance received or receivable for the period up to a maximum of $2,500 per qualifying graduate for a qualifying period of employment.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 5 - Apprentice hiring incentive',
        'showWhen': {
          'fieldId': '3004'
        },
        'rows': [
          {
            'label': 'Calculation of the apprentice hiring incentive for a qualifying period of employment in the tax year of a qualifying apprentice. If the apprenticeship placement qualifies as rural or northern early level, claim the credit in Part 6. For a high school student at an early level of apprenticeship, claim the credit in Part 7. ',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '500'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit allocated for qualifying apprentices whose salaries and wages were paid by a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '475',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '475'
                }
              }
            ]
          },
          {
            'label': 'Amount received from the Federal apprenticeship job creation tax credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '477',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '477'
                }
              }
            ]
          },
          {
            'label': '<b>Total apprentice hiring incentive claimed</b>: total of column D4 <b>plus</b> line 475 <b>minus</b> line 477',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '480',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '480'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Less the amount of any other government assistance received or receivable. Maximum amount is $33,333 per apprentice.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** Maximum amount is $5,000 multiplied by the number of days in the qualifying period of employment divided by 365. If the apprentice is shared by more than one employer, calculate column D4 as the lesser of C4 and the maximum amount multiplied by A/B.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'tabbed fullLength',
            'label': 'A = total salary and wages you paid to the apprentice for a qualifying period of employment. <br>B = total salary and wages paid to the apprentice for the qualifying period of employment.'
          }
        ]
      },
      {
        'header': 'Part 6 - Apprentice hiring incentive for rural or northern early level apprentice',
        'showWhen': {
          'fieldId': '3005'
        },
        'rows': [
          {
            'type': 'table',
            'num': '600'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit allocated for rural or northern early level apprentices whose salaries and wages were paid by a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '575',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '575'
                }
              }
            ]
          },
          {
            'label': 'Amount received from the Federal apprenticeship job creation tax credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '577',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '577'
                }
              }
            ]
          },
          {
            'label': '<b>Total apprentice hiring incentive claimed for rural or northern early level apprentice</b>: total of column D5 <b>plus</b> line 575 <b>minus</b> line 577',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '580',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '580'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Less the amount of any other government assistance received or receivable. Maximum amount is $25,000 per apprentice.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** Maximum amount is $5,000 multiplied by the number of days in the qualifying period of employment divided by 365. If the apprentice is shared by more than one employer, calculate column D5 as the lesser of C5 and the maximum amount multiplied by A/B.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'tabbed fullLength',
            'label': 'A = total salary and wages you paid to the apprentice for a qualifying period of employment. <br>B = total salary and wages paid to the apprentice for the qualifying period of employment.'
          }
        ]
      },
      {
        'header': 'Part 7 - Apprentice hiring incentive for high school apprentice',
        'showWhen': {
          'fieldId': '3006'
        },
        'rows': [
          {
            'type': 'table',
            'num': '700'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit allocated for high school apprentices whose salaries and wages were paid by a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '775',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '775'
                }
              }
            ]
          },
          {
            'label': 'Amount received from the Federal apprenticeship job creation tax credit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '777',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '777'
                }
              }
            ]
          },
          {
            'label': '<b>Total apprentice hiring incentive claimed for high school apprentice</b>: total of column D6 <b>plus</b> line 775 <b>minus</b> line 777',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '780',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '780'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Less the amount of any other government assistance received or receivable. Maximum amount is $20,000 per apprentice.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** Maximum amount is $5,000 multiplied by the number of days in the qualifying period of employment divided by 365. If the apprentice is shared by more than one employer, calculate column D6 as the lesser of C6 and the maximum amount multiplied by A/B.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'tabbed fullLength',
            'label': 'A = total salary and wages you paid to the apprentice for a qualifying period of employment. <br>B = total salary and wages paid to the apprentice for the qualifying period of employment.'
          }
        ]
      },
      {
        'header': 'Part 8 - Journeyperson hiring incentive',
        'showWhen': {
          'fieldId': '3007'
        },
        'rows': [
          {
            'type': 'table',
            'num': '800'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit allocated for qualifying journeypersons whose salaries and wages were paid by a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '675',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '675'
                }
              }
            ]
          },
          {
            'label': '<b>Total journeyperson hiring incentive claimed</b>: total of column D7 <b>plus</b> line 675',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '680',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '680'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Less the amount of any other government assistance received or receivable. Maximum amount is $33,333 per journeyperson.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** Maximum amount is $5,000 multiplied by the number of days in the qualifying period of employment divided by 365. If the journeyperson is shared by more than one employer, calculate column D7 as the lesser of C7 and the maximum amount multiplied by A/B.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'tabbed fullLength',
            'label': 'A = total salary and wages you paid to the journeyperson for a qualifying period of employment. <br>B = total salary and wages paid to the journeyperson for the qualifying period of employment.'
          }
        ]
      },
      {
        'header': 'Part 9 - Manitoba paid work experience tax credit',
        'rows': [
          {
            'label': 'Transitional 2015 hiring incentive (amount from line 125)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '900'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Youth work experience hiring incentive (amount from line 180)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '901'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Co-op student hiring incentive (amount from line 280)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '902'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Co-op graduate hiring incentive (amount from line 380)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '903'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Apprentice hiring incentive (amount from line 480)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '904'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Apprentice hiring incentive for rural or northern early level apprentice (amount from line 580)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '905'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Apprentice hiring incentive for high school apprentice (amount from line 780)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '906'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Journeyperson hiring incentive (amount from line 680)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '907'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': '<b>Manitoba paid work experience tax credit</b> (total of amounts A to H)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '908'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Enter amount I on line 622 of Schedule 5.'
          }
        ]
      },
      {
        'header': 'Part 10 - Non-refundable credit available for carryforward',
        'rows': [
          {
            'label': 'Unused credit at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '103'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Deduct</b>: credit expired after 10 tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit at the beginning of the tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '106',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '<b>Add</b>: unused credit transferred on an amalgamation or the wind-up of a subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              }
            ]
          },
          {
            'label': 'Total credit available',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '111'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': '<b>Deduct</b>: credit claimed in the current year* (enter it on line 603 in Part 2 of Schedule 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              }
            ]
          },
          {
            'label': '<b>Closing balance</b> - total credit available for carryforward',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* The credit claimed in the current year should not be more than the Manitoba income tax otherwise payable or amount J, whichever is less.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 11 - Non-refundable credit available for carryforward by year of origin',
        'rows': [
          {
            'label': 'You can complete this part to show all the non-refundable credits from previous tax years available for carryforward, by year of origin. This will help you determine the amount of credit that could expire in later years.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The amount available from the 10th previous tax year will expire after the current tax year. When you file your return for the next year, you will enter the expired amount on line 104 of Schedule 384 for that year.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Historical Data and Calculation for Manitoba Paid Work Experience Tax Credit',
        'rows': [
          {
            'type': 'table',
            'num': '1200'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            label: 'See the privacy notice on your return.',
            labelClass: 'absoluteAlignRight'
          }
        ]
      }
    ]
  });
})();
