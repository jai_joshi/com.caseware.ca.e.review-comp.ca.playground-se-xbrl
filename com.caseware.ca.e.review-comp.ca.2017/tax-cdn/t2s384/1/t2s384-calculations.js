(function() {
  function calcPart4ToPart8(calcUtils, tableNum, rate) {
    tableNum.forEach(function(tableNum) {
      var table = calcUtils.field(tableNum);
      table.getRows().forEach(function(row) {
        row[2].assign(row[1].get() * rate);
        row[4].assign(Math.round(Math.min(row[2].get(), 5000 * (row[3].get() / 365))));
      })
    })
  }

  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  wpw.tax.create.calcBlocks('t2s384', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {

      //claimable incentives
      var dateCompare = calcUtils.dateCompare;
      var taxEndDate = field('CP.tax_end').get();
      var taxStartDate = field('CP.tax_start').get();
      var part2Eligible = dateCompare.greaterThanOrEqual(taxStartDate, wpw.tax.date(2015, 9, 1));
      var part7Eligible = dateCompare.greaterThan(taxEndDate, wpw.tax.date(2015, 12, 31));
      var part3to8Eligible = dateCompare.greaterThan(taxEndDate, wpw.tax.date(2014, 12, 31));

      field('3001').assign(part2Eligible);
      field('3002').assign(part3to8Eligible);
      field('3003').assign(part3to8Eligible);
      field('3004').assign(part3to8Eligible);
      field('3005').assign(part3to8Eligible);
      field('3006').assign(part7Eligible);
      field('3007').assign(part3to8Eligible);

      var endingAfter2015 = calcUtils.dateCompare.greaterThan(taxEndDate, wpw.tax.date(2015, 12, 31));
      //part1
      field('125').assign(field('203').get());
      //part2
      field('180').assign(field('171').get() + field('175').get());
      field('150').getRows().forEach(function(row) {
        row[2].assign(row[1].get() * 0.25);
        row[4].assign(Math.min(row[2].get(), 5000 - row[3].get()));
      });
      //part3
      field('280').assign(field('271').get() + field('275').get());
      field('300').getRows().forEach(function(row) {
        row[2].assign(row[1].get() * 0.15);
        row[4].assign(endingAfter2015 ? Math.min(row[2].get(), 5000 - row[3].get()) : Math.min(row[2].get(), 1000 - row[3].get()));
      });
      //part4
      field('380').assign(field('371').get() + field('375').get());
      field('400').getRows().forEach(function(row) {
        row[2].assign(row[1].get() * 0.15);
        row[4].assign(Math.round(Math.min(row[2].get(), 2500 * (row[3].get() / 365))));
      });
    });
    calcUtils.calc(function(calcUtils, field, form) {

      //part5
      calcPart4ToPart8(calcUtils, ['500'], 0.15);
      field('480').assign(Math.max(field('471').get() + field('475').get() - field('477').get(), 0));
      //part6
      calcPart4ToPart8(calcUtils, ['600'], 0.2);
      field('580').assign(Math.max(field('571').get() + field('575').get() - field('577').get(), 0));
      //part7
      calcPart4ToPart8(calcUtils, ['700'], 0.25);
      field('780').assign(Math.max(field('771').get() + field('775').get() - field('777').get(), 0));
      //part8
      calcPart4ToPart8(calcUtils, ['800'], 0.15);
      field('680').assign(field('671').get() + field('675').get());
    });
    calcUtils.calc(function(calcUtils, field) {

      //part9
      field('900').assign(field('125').get());
      field('901').assign(field('180').get());
      field('902').assign(field('280').get());
      field('903').assign(field('380').get());
      field('904').assign(field('480').get());
      field('905').assign(field('580').get());
      field('906').assign(field('780').get());
      field('907').assign(field('680').get());
      calcUtils.sumBucketValues('908', ['900', '901', '902', '903', '904', '905', '906', '907']);
      //part10
      field('103').assign(field('1201').get());
      field('104').assign(field('1200').cell(0, 3).get());
      calcUtils.subtract('105', '103', '104');
      field('106').assign(field('105').get());
      field('110').assign(field('1203').get());
      field('111').assign(field('1206').get());
      field('108').assign(field('106').get() + field('110').get());
      field('160').assign(Math.min(field('111').get(), field('T2S383.301').get()));
      calcUtils.subtract('200', '108', '160');
    });
    calcUtils.calc(function(calcUtils, field) {

      //update year of origin based on tyh col6
      var tableArray = [1100, 1200];
      var taxationYearTable = field('tyh.200');
      tableArray.forEach(function(num) {
        field(num).getRows().forEach(function(row, rowIndex) {
          row[1].assign(taxationYearTable.getRow(rowIndex + 10)[6].get())
        });
        var summaryTable = field('1200');
        field('1100').getRows().forEach(function(row, rowIndex) {
          row[3].assign(summaryTable.getRow(rowIndex)[11].get());
        });
        // historical data chart
        var limitOnCredit = field('160').get();
        summaryTable.getRows().forEach(function(row, rowIndex) {
          if (rowIndex == 0) {
            // to avoid the first row being calculated to total
            row[7].assign(0);
            row[9].assign(0);
            row[11].assign(0);
          }
          else {
            row[7].assign(Math.max(row[3].get() + row[5].get(), 0));
            row[9].assign(returnAmountApplied(limitOnCredit, row[7].get()));
            row[11].assign(Math.max(row[7].get() - row[9].get(), 0));
            limitOnCredit -= row[9].get();
          }
        });
      })
    });

  })
})();

