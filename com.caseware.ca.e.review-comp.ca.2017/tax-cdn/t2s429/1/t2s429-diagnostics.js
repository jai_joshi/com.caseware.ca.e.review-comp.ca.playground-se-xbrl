(function() {
  wpw.tax.create.diagnostics('t2s429', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('4290001', common.prereq(common.check(['t2s5.680'], 'isNonZero'), common.requireFiled('T2S429')));

    diagUtils.diagnostic('4290002', common.prereq(common.and(
        common.requireFiled('T2S429'),
        function(tools) {
          return tools.checkMethod(tools.list(['t2s5.680', '400']), 'isNonZero') || tools.field('210').isYes();
        }),
        common.check('215', 'isNonZero')));

    diagUtils.diagnostic('4290003', common.prereq(common.and(
        common.requireFiled('T2S429'),
        common.check(['t2s5.680'], 'isNonZero')),
        common.check(['310', '315', '320', '325', '330', '335', '340', '345', '350', '355'], 'isNonZero', true)));

  });
})();