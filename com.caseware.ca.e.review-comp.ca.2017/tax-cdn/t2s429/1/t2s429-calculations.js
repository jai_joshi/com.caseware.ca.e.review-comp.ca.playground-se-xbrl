(function() {

  wpw.tax.create.calcBlocks('t2s429', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('502', 'ratesBc', '2141');
      //contact info //
      field('151').assign(field('T2J.951').get() + ' ' + field('T2J.950').get());
      field('153').assign(field('T2J.956').get());

      //Part 2 eligibility for BCIDMTC
      if (field('210').get() == 2) {
        calcUtils.removeValue([215, 400, 450, 490, 500, 501], true);
      }
      else {
        field('215').disabled(false);
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('502', 'ratesBc', '2141');
      //Part 2 and 3 eligibility for BCIDMTC
      if (field('310').get() == 1 ||
          field('315').get() == 1 ||
          field('320').get() == 1 ||
          field('325').get() == 1 ||
          field('330').get() == 1 ||
          field('335').get() == 1 ||
          field('340').get() == 1 ||
          field('345').get() == 1 ||
          field('350').get() == 1 ||
          field('355').get() == 1) {
        calcUtils.removeValue([400, 450, 490, 500, 501], true);
      }
      else {
        field('400').disabled(false);
        field('450').disabled(false);
        calcUtils.subtract('490', '400', '450');
        calcUtils.equals('500', '490');
        calcUtils.multiply('501', ['500', '502'], 1 / 100);
      }
    });


  });
})();
