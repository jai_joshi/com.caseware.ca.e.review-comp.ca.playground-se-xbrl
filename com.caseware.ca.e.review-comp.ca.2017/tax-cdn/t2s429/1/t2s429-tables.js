(function() {

  function getTableRate(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '1': {num: labelsObj.num[0]},
          '2': {label: ' x ', labelClass: 'center'},
          '3': {num: labelsObj.num[1], decimals: labelsObj.decimals},
          '4': {label: ' %= '},
          '6': {num: labelsObj.num[2], cellClass: 'doubleUnderline'},
          '7': {label: labelsObj.indicator}
        }]
    }
  }

  wpw.tax.global.tableCalculations.t2s429 = {
    '1000': getTableRate({
      label: ['<b>British Columbia interactive digital media tax credit</b>: amount C from Part 4 '],
      indicator: 'D',
      num: ['500', '502', '501'],
      decimals: '1'
    }),
    '100': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1],
      'columns': [
        {
          'width': '60%',
          cellClass: 'alignLeft'
        },
        {
          cellClass: 'alignLeft'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Name of person to contact for more information',
            'num': '151', "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
            'tn': '151',
            type: 'text'
          },
          '1': {
            'label': 'Telephone number including area code',
            'num': '153',
            'tn': '153',
            type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
            validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
          }
        }
      ]
    }
  }
})();
