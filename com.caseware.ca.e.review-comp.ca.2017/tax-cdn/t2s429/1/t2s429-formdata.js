(function() {
  'use strict';

  wpw.tax.global.formData.t2s429 = {
    formInfo: {
      abbreviation: 'T2S429',
      title: 'British Columbia Interactive Digital Media Tax Credit',
      schedule: 'Schedule 429',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      code: 'Code1701',
      formFooterNum: 'T2 SCH 429 E (17)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule to claim a British Columbia interactive digital media tax credit (BCIDMTC) ' +
              'under Part 10 of the <i>Income Tax Act</i> (British Columbia)'
            },
            {
              label: 'The BCIDMTC program provides a refundable 17.5% tax credit on net eligible salary and ' +
              'wages incurred after August 31, 2010, and before September 1, 2018, by eligible corporations to ' +
              'develop interactive digital media products in British Columbia'
            },
            {
              label: 'A corporation cannot claim the BCIDMTC if it has claimed a British Columbia scientific ' +
              'research and experimental development tax credit for the tax year.'
            },
            {
              label: 'A corporation is eligible for the BCIDMTC if it meets all of the following criteria;',
              sublist: [//TODo: sublist of sub\'slist
                'it is registered with the British Columbia Ministry of Finance for the tax year;',
                'it has a permanent establishment in British Columbia at any time during the tax year',
                'it is a taxable Canadian corporation throughout the tax year;',
                'either of the following applies:',
                '(i) the corporation\'s principal business in the tax year is the development of interactive ' +
                'digital media products; or',
                '(ii) all or substantially all of the corporation\'s business in the tax year consists of ' +
                'one or both of the following:',
                '(A) the development of interactive digital media products; or',
                '(B) the provision of eligible activities to a corporation that has a permanent ' +
                'establishment in British Columbia and that has as its principal business the development of ' +
                'interactive digital media products; and',
                'the amount equal to the eligible salary and wages, as determined under subsection 134(2) of the' +
                '<i> Income Tax Act</i> (British Columbia) for the corporation for the tax year, is greater than ' +
                '$100,000. If the tax year is less than 365 days, the $100,000 threshold is prorated based on' +
                ' the number of days in the tax year divided by 365.',
                'effective February 22, 2017, corporations with eligible salary and wages over $2' +
                ' million dollars are eligible for the tax credit without having to meet the eligibility criteria ' +
                'listed above. Corporations with eligible salary and wages over $100,000 but less than ' +
                '$2 million will continue to meet the existing eligibility criteria. Corporations with salary ' +
                'and wages under $100,000 are still not eligible for the credit.' +
                ' The proration will continue to apply for shortened tax years.',
                'effective February 22, 2017, interactive digital media corporations registered under Part 2' +
                ' of the British Columbia <i>Small Business Venture Capital Act</i>, will be eligible to ' +
                'claim the IDMTC. This will also be the case for any corporation that they control' +
                ' directly or indirectly.'
              ]
            },
            {
              label: 'To claim the BCIDMTC, file a completed copy of this schedule within 18 months of the end of the tax year in which the eligible salary and wages were '+
              'incurred. Late-filed tax credit forms will not be processed. The Canada Revenue Agency will not process tax credit forms that are filed late.'
            },
            {
              label: 'Include a completed copy of this schedule with the <i>T2 Corporation Income Tax Return</i> for the tax year'
            }
          ]
        }
      ],
      category: 'British Columbia Forms'
    },
    sections: [
      {
        'header': 'Freedom of Information and Protection of <i>Privacy Act</i> (FOIPPA)',
        'rows': [
          {
            'label': 'The personal information on this form is collected for the purpose of administering the <i>Income Tax Act</i> (British Columbia) under the authority of paragraph 26(a) of the FOIPPA. Questions about the collection or use of this information can be directed to the Manager, Intergovernmental Relations, PO Box 9444 Stn Prov Govt, Victoria BC  V8W 9W8. (Telephone: Victoria at <b>250-387-3332</b> or toll-free at <b>1-877-387-3332</b> and ask to be re-directed). <br>Email: ITBTaxQuestions@gov.bc.ca',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': '<b>Part 1 - Contact information</b>',
        'rows': [
          {
            type: 'table',
            num: '100'
          }
        ]
      },
      {
        'header': 'Part 2 - Registration',
        'rows': [
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': 'Has the corporation applied to the British Columbia Ministry of Finance for a registered number so it can ' +
            'claim a British Columbia interactive digital media tax credit?',
            'num': '210',
            'tn': '210',
            'init': '2'
          },
          {
            'type': 'infoField',
            'label': 'If you answered <b>yes</b> to the question at line 210, please give the registration number',
            'tn': '215',
            'num': '215',
            'validate': {
              'or': [
                {
                  'length': {
                    'min': '1',
                    'max': '60'
                  }
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          {
            'label': 'If you do not register the corporation, <b>you are not eligible</b> for the BCIDMTC.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Eligibility',
        'rows': [
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': '1. Has the corporation claimed a British Columbia scientific research and experimental development tax credit for the tax year?',
            'num': '310',
            'tn': '310',
            'init': '2'
          },
          {
            'label': 'Was the corporation at any time in the tax year:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': '2. exempt from tax under section 27 of the <i>Income Tax Act</i> (British Columbia)?',
            'num': '315',
            'tn': '315',
            'init': '2'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': '3. exempt from tax under Part I of the <i>Income Tax Act</i> (federal Act)?',
            'num': '320',
            'tn': '320',
            'init': '2'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': '4. a prescribed labour-sponsored venture capital corporation for the purpose of section 127.4 of the federal Act?',
            'num': '325',
            'tn': '325',
            'init': '2'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': '5. a corporation that has an employee share ownership plan registered under section 2 of the <i>Employee Investment Act</i>?',
            'num': '330',
            'tn': '330',
            'init': '2'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': '6. registered as an employee venture capital corporation under section 8 of the <i>Employee Investment Act</i>?',
            'num': '335',
            'tn': '335',
            'init': '2'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': '7. <b>a small business venture capital corporation</b> registered under section 3 of the <i>Small Business Venture Capital Act</i>?',
            'num': '340',
            'tn': '340',
            'init': '2'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': '8. an <b>eligible business corporation</b> registered under Part 2 of the ' +
            '<i>Small Business Venture Capital Act</i> before February 22, 2017?*',
            'num': '345',
            'tn': '345',
            'init': '2'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': '9. controlled directly or indirectly in any way by one or more corporations described in questions 2 to 8?',
            'num': '350',
            'tn': '350',
            'init': '2'
          },
          {
            'type': 'infoField',
            'inputType': 'radio',
            'label': '10. a personal services business as defined in subsection 125(7) of the federal Act?',
            'num': '355',
            'tn': '355',
            'init': '2'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If you answered <b>yes</b> to any of the above questions, <b>you are not eligible</b> for the BCIDMTC.'
          },
          {
            'label': '* If you are a corporation registered under Part 2 of the <i>Small Business Venture Capital Act</i>, ' +
            'you will now be eligible for the BCIDMTC starting February 22, 2017. ' +
            'The amounts before February 22, 2017, are not eligible. ',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 4 - Eligible salary and wages',
        'rows': [
          {
            'label': 'Eligible salary and wages*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '400',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '400'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Designated assistance**',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '450',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '450'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': '<b>Net eligible salary and wages</b> (amount A <b>minus</b> amount B) (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '490',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '490'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>* Salary and wages</b> has the same meaning as "salary or wages" in section 248 of the federal Act. Eligible salary and wages includes amounts that:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'a) are directly attributable to eligible activities;',
            'labelClass': 'fullLength'
          },
          {
            'label': 'b) are incurred:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- by the corporation in the tax year; and',
            'labelClass': 'fulllength tabbed'
          },
          {
            'label': '- after August 31, 2010, and before September 1, 2018;',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'c) are paid to an individual who was resident in British Columbia at the end of December 31 of the year before the end of the tax year for which a tax credit is claimed; and',
            'labelClass': 'fullLength'
          },
          {
            'label': 'd) are either of the following:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- included in the cost of a property to the corporation in the tax year or, in the case of depreciable property, the capital cost to the corporation in the tax year; or',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '- deductible as an outlay or expense in calculating the income of the corporation for the tax year;',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'Eligible salary and wages do not include amounts listed under subsection 134(1) of the <i>Income Tax Act</i> (British Columbia).',
            'labelClass': 'fullLength',
            'forceBreakAfter': true
          },
          {
            'label': '** Designated assistance is the total of all amounts that would be included under paragraph ' +
            '12(1)(x) of the federal Act in calculating the income of the corporation for the tax year, if ' +
            'that paragraph were read without reference to subparagraphs (v) to (vii) of that paragraph, and that ' +
            'can reasonably be considered to be for eligible salary and wages. Designated assistance does not include:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'a) a prescribed amount. No amount has been designated as prescribed;',
            'labelClass': 'fullLength'
          },
          {
            'label': 'b) an amount considered to have been paid under section 135 of the <i>Income Tax Act</i> (British Columbia); or',
            'labelClass': 'fullLength'
          },
          {
            'label': 'c) an amount deducted under subsection 127(5) or 127(6) of the federal Act.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 5 - British Columbia interactive digital media tax credit calculation',
        'rows': [
          {type: 'table', num: '1000'},
          {
            'label': 'Enter amount D on line 680 of Schedule 5, <i>Tax Calculation Supplementary – Corporations.</i>'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            label: 'See the privacy notice on your return.',
            labelClass: 'absoluteAlignRight'
          }
        ]
      }
    ]
  };
})();
