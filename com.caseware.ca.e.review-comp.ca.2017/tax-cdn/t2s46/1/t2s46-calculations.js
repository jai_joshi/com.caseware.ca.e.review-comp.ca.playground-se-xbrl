(function() {

  wpw.tax.create.calcBlocks('t2s46', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //step 1
      calcUtils.getGlobalValue('100', 'T2S27', '030');
      field('115').assign(field('T2J.040').get() == 1 ? field('T2J.427').get() : 0);
      field('115').source(field('T2J.427'));
      field('110').assign(field('T2S27.150').get() + field('T2S27.170').get());
      field('116').assign(21);
      field('117').assign(Math.max(((field('100').get() * (field('105').get() / field('110').get())) - field('115').get()) * field('116').get() / 100, 0));
      //step 2
      var mar23 = wpw.tax.date(2017, 3, 23);
      var dateCompare = calcUtils.compareDateAndFiscalPeriod(mar23);
      field('251').assign(field('117').get() * 0.5);
      field('252').assign(dateCompare.daysBeforeDate);
      field('253').assign(field('CP.Days_Fiscal_Period').get());
      field('120').assign(field('253').get() == 0 ? 0 : field('251').get() * (field('252').get() / field('253').get()));
    });
  });
})();
