(function() {
  wpw.tax.create.formData('t2s46', {
    formInfo: {
      abbreviation: 't2s46',
      title: 'Part II – Tobacco Manufacturers\' Surtax',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      //subTitle: '(2007 and later tax years)',
      showCorpInfo: true,
      schedule: 'Schedule 46',
      code: '0701',
      formFooterNum: 'T2 SCH 46 E (07)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule to report the tobacco manufacturers\' surtax'
            },
            {
              label: 'Tobacco manufacturing is any activity, other than an exempt activity as defined in subsection 182(2)' +
              ' of the <i>Income Tax Act</i>, relating to the manufacture or processing in Canada of tobacco' +
              ' or tobacco products in or into any form that is, or would after any further activity become, ' +
              'suitable for smoking'
            },
            {
              label: 'File the completed Schedule 46 with the <i>T2 Corporation Income Tax Return</i>' +
              ' within six months from the end of the tax year.'
            },
            {
              label: 'Do not file Schedule 46 if the corporation is a Canadian-controlled private corporation ' +
              'and the total of its income from active businesses(before deducting its losses from active businesses)' +
              ' is less than its business limit'
            },
            {
              label: 'Calculating the tobacco manufacturers\' surtax is done in two steps'
            }
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {
            label: 'STEP 1',
            labelClass: 'fullLength titleFont center'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num:'150'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Where:'
          },
          {
            type: 'table',
            num: '200'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {
            label: 'STEP 2',
            labelClass: 'fullLength titleFont center'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Part II – tobacco manufacturers\' surtax payable (amount from line A multiplied by 50%) ' +
            '<br> Enter this amount at line 708 on page 8 of your T2 return',
            input2: true,
            tn: '120',
            num: '120', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          }
        ]
      }
    ]
  });
})();
