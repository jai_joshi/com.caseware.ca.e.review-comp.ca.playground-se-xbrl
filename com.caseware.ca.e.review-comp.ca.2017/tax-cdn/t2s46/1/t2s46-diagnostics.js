(function() {
  wpw.tax.create.diagnostics('t2s46', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0460001', common.prereq(common.check(['cp.249'], 'isChecked'), common.requireFiled('T2S46')));

    diagUtils.diagnostic('0460002', common.prereq(common.and(
        common.requireFiled('T2S46'),
        common.check(['t2j.708'], 'isNonZero')),
        function(tools) {
          return tools.requireAll(tools.list(['100', '105', '110']), 'isNonZero')
        }));

  });
})();
