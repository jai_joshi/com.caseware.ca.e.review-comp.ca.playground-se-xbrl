(function() {
  var inputWidth = '107px';
  var tnWidth = '25px';
  var textWidth = '50px';

  wpw.tax.create.tables('t2s46', {
    '150': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          width: textWidth
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none'
        }
      ],
      cells: [
        {
          '1': {
            label: '(['
          },
          '2': {
            tn: '100'
          },
          '3':{
            num: '100', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          },
          '4': {
            label: ' x '
          },
          '5': {
            tn: '105'
          },
          '6':{
            num: '105', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            cellClass: 'singleUnderline'
          },
          '7': {
            label: '] -'
          },
          '8': {
            tn: '115'
          },
          '9':{
            num: '115', "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          },
          '10': {
            label: ') x'
          },
          '11': {
            num: '116'
          },
          '12': {
            label: '%='
          },
          '13': {
            num: '117'
          },
          '14': {
            label: 'A'
          }
        },
        {
          '3': {
            type: 'none'
          },
          '5': {
            tn: '110'
          },
          '6':{
            num: '110',
            "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          },
          '9': {
            type: 'none'
          },
          '11': {
            type: 'none'
          },
          '13': {
            type: 'none'
          }
        }
      ]
    },
    '200': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          width: '30px'
        },
        {
          type: 'none',
          width: '15px'
        },
        {
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {
            label: '<b>100</b>'
          },
          '2': {
            label: 'is the corporation\'s Canadian manufacturing and processing profits for the year as' +
            ' calculated in Part 2 of Schedule 27, except when the corporation has loss(es) from active' +
            ' business(es) other than tobacco manufacturing. If a corporation\'s loss(es) from ' +
            'active business(es) other than tobacco manufacturing exceeds its income from active business(es) ' +
            'other than tobacco manufacturing, the excess must be added back to determine the adjusted business income.'
          }
        },
        {},
        {
          '0': {
            label: '<b>105</b>'
          },
          '2': {
            label: 'is the total of the corporation\'s "tobacco manufacturing capital" and "tobacco labour cost" for' +
            ' the year. These two amounts can be calculated on Schedule 27. The amounts are calculated' +
            ' in the same manner as "manufacturing and processing capital" (Part 5) and "manufacturing and processing ' +
            'labour" (Part 7), but replace the term <b>manufacturing or processing</b> with <b>tobacco manufacturing</b>' +
            ' in the definition of "qualified activities."'
          },
        },
        {},
        {
          '0': {
            label: '<b>110</b>'
          },
          '2': {
            label: 'is the total of the corporation\'s cost of manufacturing and processing capital (MC) for the year' +
            ' and its cost of manufacturing and processing labour (ML) for the year. These two amounts are calculated ' +
            'on Schedule 27.'
          }
        },
        {},
        {
          '0': {
            label: '<b>115</b>'
          },
          '2': {
            label: 'is the corporation\'s business limit for the year. If the corporation is not a ' +
            'Canadian-controlled private corporation, this amount is nil.'
          }
        }
      ]
    },
    '250': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [{
        'type': 'none',
        cellClass: 'alignCenter'
      },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '330px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          'width': '5px',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          'width': '20px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        }
      ],
      'cells': [{
        '0': {
          'label': 'Part II – tobacco manufacturers\' surtax payable (amount from line A <b>multiplied</b> by 50%)'
        },
        '1': {
          'num': '251'
        },
        '2': {
          'label': ' x ',
          cellClass: 'alignCenter'
        },
        '3': {
          'label': 'Number of days in the tax year before March 23, 2017 ',
          cellClass: 'alignCenter singleUnderline'
        },
        '5': {num: '252'},
        '6': {label: '='},
        '7':{tn: '120'},
        '8': {
          num: '120',
          'formField': true,
          "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        }
      },
        {
          '1': {
            'type': 'none'
          },
          '3': {
            'label': 'Number of days in the tax year',
            cellClass: 'alignCenter'
          },
          '5': {num: '253'},
          '8': {type: 'none'}
        }]
    }
  });
})();
