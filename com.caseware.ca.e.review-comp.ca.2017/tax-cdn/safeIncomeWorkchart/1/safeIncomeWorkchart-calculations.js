(function() {

  function getTableTotal(calcUtils, tableNum, cIndex, ignoreFirstRow) {
    var totalValue = 0;
    calcUtils.field(tableNum).getCol(cIndex).forEach(function(cell, cIndex) {
      if (ignoreFirstRow) {
        if (cIndex == 0) {
          return;
        }
        else {
          totalValue += cell.get();
        }
      }
      else {
        totalValue += cell.get();
      }

    });
    return totalValue;
  }

  function calculateTotal(calcUtils, tableTotalNum, tableNumArray, cIndex, ignoreFirstRow) {
    calcUtils.field(tableTotalNum).cell(0, cIndex).assign(
        getTableTotal(calcUtils, tableNumArray[0], cIndex, ignoreFirstRow) +
        getTableTotal(calcUtils, tableNumArray[1], cIndex));
  }

  /**
   * this function calculate total of cells within the table based on checkbox value and assign it to assigned cell
   * @param calcUtils
   * @param storedInfo: {num, rowIndex}
   * @param tableNums
   * @param colInfo
   * @param cIndex
   * @param checkBoxValueToCheck
   */
  function calculateCheckedValue(calcUtils, storedInfo, tableNums, colInfo, cIndex, checkBoxValueToCheck) {
    var value = 0;
    var storedTableNum = storedInfo.num;
    var storedTableRowIndex = storedInfo.rowIndex;
    tableNums.forEach(function(tableNum) {
      calcUtils.field(tableNum).getRows().forEach(function(row) {
        if (row[colInfo.checkBoxCIndex].get() == checkBoxValueToCheck) {
          value += row[colInfo[cIndex]].get();
        }
      });
    });
    calcUtils.field(storedTableNum).cell(storedTableRowIndex, colInfo[cIndex]).assign(value);
  }

  /**
   * this function calculate different in $ or %
   * @param calcUtils
   * @param tableNumArr
   * @param colInfo
   */
  function calcsDif(calcUtils, tableNumArr, colInfo) {
    var diffOption = calcUtils.field('1045').get();
    tableNumArr.forEach(function(tableNum) {
      var table = calcUtils.field(tableNum);
      table.getRows().forEach(function(row) {
        var amountDiffCol;
        var percentDiffCol;
        var priorColAmount;
        var currentYearAmount;
        var frozenScenarioAmount;

        amountDiffCol = row[colInfo.amountDiffCIndex];
        percentDiffCol = row[colInfo.percentDiffCIndex];

        priorColAmount = row[colInfo.priorYearCIndex].get();
        currentYearAmount = row[colInfo.currentYearCIndex].get();
        frozenScenarioAmount = row[colInfo.frozenScenarioCIndex].get();

        if (!angular.isUndefined(amountDiffCol || percentDiffCol)) {
          if (diffOption == 1) {
            amountDiffCol.assign(priorColAmount - currentYearAmount);
            percentDiffCol.assign(currentYearAmount == 0 ? 0 : priorColAmount / currentYearAmount * 100);
          }
          else if (diffOption == 2) {
            amountDiffCol.assign(priorColAmount - frozenScenarioAmount);
            percentDiffCol.assign(frozenScenarioAmount == 0 ? 0 : priorColAmount / frozenScenarioAmount * 100);
          }
          else {
            amountDiffCol.assign(currentYearAmount - frozenScenarioAmount);
            percentDiffCol.assign(frozenScenarioAmount == 0 ? 0 : currentYearAmount / frozenScenarioAmount * 100);
          }
        }
      });
    });
  }

  function calcsDifShareholder(calcUtils, tableNum, colInfo, isShareHolder) {
    var table = calcUtils.field(tableNum);
    table.getRows().forEach(function(row) {
      var amountDiffCol;
      var percentDiffCol;
      var currentYearAmount;
      var frozenAmount;

      if (isShareHolder) {
        amountDiffCol = row[colInfo.shAmountDiffCIndex];
        percentDiffCol = row[colInfo.shPercentDiffCIndex];
        currentYearAmount = row[colInfo.shCurrentYearCIndex].get();
        frozenAmount = row[colInfo.shScenarioCIndex].get();
      } else {
        amountDiffCol = row[colInfo.amountDiffCIndex];
        percentDiffCol = row[colInfo.percentDiffCIndex];
        currentYearAmount = row[colInfo.currentYearCIndex].get();
        frozenAmount = row[colInfo.frozenScenarioCIndex].get();
      }

      if (!angular.isUndefined(amountDiffCol || percentDiffCol)) {
        amountDiffCol.assign(currentYearAmount - frozenAmount);
        percentDiffCol.assign(frozenAmount == 0 ? 0 : currentYearAmount / frozenAmount * 100);
      }
    });
  }

  function populateGlobalValue(calcUtils, tableNums, colInfo) {
    tableNums.forEach(function(tableNum) {
      var table = calcUtils.field(tableNum);
      table.getRows().forEach(function(row, rIndex) {
        var rowInfo = table.getRowInfo(rIndex);
        var formId = rowInfo.formId;
        var fieldId = rowInfo.fieldId;
        if (formId && fieldId) {
          var priorYearSourceField = calcUtils.form(formId).field(fieldId + '-P');
          var currentYearSourceField = calcUtils.form(formId).field(fieldId);
          var priorYearValue = priorYearSourceField.get();
          var currentYearValue = currentYearSourceField.get();

          //populate prior year value
          row[colInfo.priorYearCIndex].assign(priorYearValue);
          row[colInfo.priorYearCIndex].source(priorYearSourceField);

          //populate current year value
          row[colInfo.currentYearCIndex].assign(currentYearValue);
          row[colInfo.currentYearCIndex].source(currentYearSourceField);
        }
      });
    });
  }

  function runFrozeValue(calcUtils, colInfo, tableNumArr, isSharePart) {
    tableNumArr.forEach(function(tableNum) {
      var table = calcUtils.field(tableNum);
      table.getRows().forEach(function(row) {
        if (isSharePart) {
          row[colInfo.shScenarioCIndex].assign(row[colInfo.shCurrentYearCIndex].get());
        } else {
          row[colInfo.frozenScenarioCIndex].assign(row[colInfo.currentYearCIndex].get());
        }
      });
    });
  }

  function runColCalcs(calcUtils, colInfo, cIndex) {
    //Part 1
    //calculate Net income (loss) for income tax purposes
    calculateTotal(calcUtils, 997, [992, 996], colInfo[cIndex], true);
    //part 1 calcs END

    //Part 2
    // Net income (loss) for income tax purposes
    calcUtils.field('993').cell(0, colInfo[cIndex]).assign(calcUtils.field('997').cell(0, colInfo[cIndex]).get());
    // (Net income items - marked by checkbox)
    calculateCheckedValue(calcUtils, {num: '993', rowIndex: '1'}, [992, 996], colInfo, cIndex, true);
    // Safe income on hand - before dividends
    calculateTotal(calcUtils, 995, [993, 994], colInfo[cIndex]);
    //part 2 calcs END

    //Part 3
    //Safe income on hand row
    calcUtils.field('998').cell(1, colInfo[cIndex]).assign(calcUtils.field('995').cell(0, colInfo[cIndex]).get());
    // (Net income items - not marked by checkbox)
    calculateCheckedValue(calcUtils, {num: '998', rowIndex: '2'}, [992, 996], colInfo, cIndex, false);
    // (Retained earnings items - marked by checkbox)
    calculateCheckedValue(calcUtils, {num: '998', rowIndex: '3'}, [993, 994], colInfo, cIndex, true);
    //Ending retained earnings
    calculateTotal(calcUtils, 1001, [998, 999], colInfo[cIndex]);
    //part 3 calcs END
  }

  var tableArray = [992, 996, 997, 993, 994, 995, 998, 999, 1001];
  var tableGlobalArr = [992, 993, 998];//tables that has cells which value comes from other forms

  var colInfo = {
    checkBoxCIndex: '2',
    priorYearCIndex: '6',
    currentYearCIndex: '8',
    frozenScenarioCIndex: '10',
    amountDiffCIndex: '12',
    percentDiffCIndex: '14',
    percentOfShareCIndex: '2',//for shareholder distribution part
    numberOfShareCIndex: '4',//for shareholder distribution part
    shCurrentYearCIndex: '6',//for shareholder distribution part
    shScenarioCIndex: '8',//for shareholder distribution part
    shAmountDiffCIndex: '10',//for shareholder distribution part
    shPercentDiffCIndex: '12'//for shareholder distribution part
  };

  wpw.tax.create.calcBlocks('safeIncomeWorkchart', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field, form) {
      var currentFormId = calcUtils.form().valueFormId;
      var fiscalChangeOption = field('1011').get();
      var tableHistoricalNum = '981';
      var beginningFiscalDate = field('001').get();
      var i;
      var rowsNumDiff;
      var numberOfRows = field(tableHistoricalNum).size().rows;
      var numberOfFiscalYears;

      if (fiscalChangeOption == '1') {//is there is change in fiscal year
        numberOfFiscalYears = field('1012').get();
      }
      else {
        if (beginningFiscalDate) {
          var beginningFiscalYear = beginningFiscalDate.year;
          var currentFiscalYear = field('t2j.061').getKey('year');
          numberOfFiscalYears = currentFiscalYear - beginningFiscalYear - 1;
        }
      }

      field('showHistoricalTable').assign(numberOfFiscalYears > 0);

      rowsNumDiff = numberOfRows - numberOfFiscalYears;
      //based on rowsNumDiff, remove or create more table row
      if (rowsNumDiff > 0) {
        for (i = numberOfRows; i > numberOfFiscalYears; i--) {
          calcUtils.removeTableRow(currentFormId, tableHistoricalNum, i - 1);
        }
      } else if (rowsNumDiff < 0) {
        for (i = numberOfRows; i < numberOfFiscalYears; i++) {
          calcUtils.addTableRow(currentFormId, tableHistoricalNum);
        }
      } else {
        //do nothing
      }

      field(tableHistoricalNum).getCol(0).forEach(function(cell, cIndex) {
        if (fiscalChangeOption == '1') {
          if (cell.valueObj.data.disabled) {
            cell.assign(undefined);
            cell.disabled(false)
          }
        } else {
          if (beginningFiscalDate) {
            var newDate = calcUtils.addToDate(beginningFiscalDate, cIndex, 0, 0);
            cell.assign(newDate)
          }
        }
      });
    });

    //Part 1 - Input of net income for income tax purposes
    calcUtils.calc(function(calcUtils, field, form) {
      //date cell
      var taxEndDate = field('CP.tax_end').get();
      var priorYearDate = wpw.tax.utilities.addToDate(taxEndDate, -1, 0, 0);
      field('992').cell(0, colInfo.priorYearCIndex).assign(priorYearDate);
      field('992').cell(0, colInfo.currentYearCIndex).assign(taxEndDate);
      field('992').cell(0, colInfo.frozenScenarioCIndex).assign(taxEndDate);
    });

    //populate global value for prior year and current year cols
    calcUtils.calc(function(calcUtils, field, form) {
      populateGlobalValue(calcUtils, tableGlobalArr, colInfo);
    });

    //calcs for part 1,2,3
    calcUtils.calc(function(calcUtils, field, form) {
      //prior year column
      runColCalcs(calcUtils, colInfo, 'priorYearCIndex');
      //Opening retained earnings of current year equal ending of prior year
      field('998').cell(0, colInfo.currentYearCIndex).assign(field('1001').cell(0, colInfo.priorYearCIndex).get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //current year column
      runColCalcs(calcUtils, colInfo, 'currentYearCIndex');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //return total adj made to part 1
      field('totalAdjPart1').assign(field('997').cell(0, 8).get() - field('992').cell(1, 8).get());
      //return total adj made to part 2
      field('totalAdjPart2').assign(field('995').cell(0, 8).get() - field('993').cell(0, 8).get());
      //return total adj made to part 3
      field('totalAdjPart3').assign(field('1001').cell(0, 8).get() - field('998').cell(0, 8).get());
    });

    //calcs for part 1,2,3 END

    //calcs for part 4 -Distribution of Dividend to Shareholders
    calcUtils.calc(function(calcUtils, field, form) {
      //Dividend paid in current fiscal period
      field('1002').cell(0, colInfo.currentYearCIndex).assign(field('998').cell(4, colInfo.currentYearCIndex).get());
      var numberOfShareOs = field('1021').get();
      field('1002').cell(1, colInfo.currentYearCIndex).assign(numberOfShareOs);
      //Dividend per share
      field('1002').cell(2, colInfo.currentYearCIndex).assign(numberOfShareOs == 0 ? 0 : field('1002').cell(0, colInfo.currentYearCIndex).get() / numberOfShareOs);
    });
    //calcs for part 4 - END

    //calcs for share holder distribution
    calcUtils.calc(function(calcUtils, field, form) {
      //dividend for shareholder
      var numberShareOs = field('1002').cell(1, colInfo.currentYearCIndex).get();
      var dividendPerShare = field('1002').cell(2, colInfo.currentYearCIndex).get();
      var linkedTable = field('t2s50.050');
      if (linkedTable.size().rows > 0) {
        field('1003').getRows().forEach(function(row, rIndex) {
          //get value from s50
          row[0].assign(linkedTable.cell(rIndex, 0).get());//name of shareholder
          row[0].source(linkedTable.cell(rIndex, 0));
          var percentOfShares = linkedTable.cell(rIndex, 4).get();//percent of common share owned
          row[colInfo.percentOfShareCIndex].assign(percentOfShares);
          row[colInfo.percentOfShareCIndex].source(linkedTable.cell(rIndex, 4));
          //cals number of share owned
          var numberOfShare = percentOfShares * numberShareOs / 100;
          row[colInfo.numberOfShareCIndex].assign(numberOfShare);
          //calcs dividend payOut for current year
          row[colInfo.shCurrentYearCIndex].assign(numberOfShare * dividendPerShare);
        });
      }
    });

    //freeze current scenario and update to summary form
    calcUtils.calc(function(calcUtils, field, form) {
      if (field('freeze').get() === 1) {
        calcUtils.freezeFormToTable('safeIncomeWorkchartS', '981');
        runFrozeValue(calcUtils, colInfo, tableArray);
        runFrozeValue(calcUtils, colInfo, ['1002']);
        runFrozeValue(calcUtils, colInfo, ['1003'], true);
        field('freeze').assign(0);
        //calcs freeze scenario option dropdown
        var scenarioOptionArr = [];
        field('safeIncomeWorkchartS.981').getCol(0).forEach(function(cell, cIndex) {
          if (!wpw.tax.utilities.isEmptyOrNull(cell.get())) {
            scenarioOptionArr.push({
              value: cIndex,
              option: cell.get()
            });
          }
        });
        field('1014').config('options', scenarioOptionArr);
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //calculate diff for part 1,2,3
      calcsDif(calcUtils, tableArray, colInfo);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //calcs diff for part 4
      calcsDifShareholder(calcUtils, '1002', colInfo);
      calcsDifShareholder(calcUtils, '1003', colInfo, true);
    });

  });
})();
