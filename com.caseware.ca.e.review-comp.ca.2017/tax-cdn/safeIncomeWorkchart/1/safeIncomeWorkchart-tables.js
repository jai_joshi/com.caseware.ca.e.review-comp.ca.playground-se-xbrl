(function() {

  var columnWidth = 'std-input-width';
  var columnTnWidth = 'std-padding-width';
  var descriptionWidth = 'std-input-col-width';
  var percentWidth = 'half-col-width';
  var otherAdjWidth = 'std-input-col-width';
  var percentShareWidth = 'third-col-width';

  var part1RowsArr = [
    {
      type: 'date',
      noCheckbox: true,
      hiddenCellCols: [12, 14]
    },
    {
      label: '<b>Net income (loss) from financial statements</b>',
      noCheckbox: true,
      formId: 'T2S1',
      fieldId: '100'
    },
    {
      label: 'Provision for income taxes - current',
      tn: '101',
      formId: 'T2S1',
      fieldId: '101'
    },
    {
      label: 'Provision for income taxes - deferred',
      tn: '102',
      formId: 'T2S1',
      fieldId: '102'
    },
    {
      label: 'Interest and penalties on taxes',
      tn: '103',
      init: true,
      formId: 'T2S1',
      fieldId: '103'
    },
    {
      label: ' Amortization of tangible assets',
      tn: '104',
      formId: 'T2S1',
      fieldId: '104'
    },
    {
      label: ' Amortization of natural resource assets',
      tn: '105',
      formId: 'T2S1',
      fieldId: '105'
    },
    {
      label: ' Amortization of intangible assets',
      tn: '106',
      formId: 'T2S1',
      fieldId: '106'
    },
    {
      label: ' Recapture of capital cost allowance from Schedule 8',
      tn: '107',
      formId: 'T2S1',
      fieldId: '107'
    },
    {
      label: ' Gain on sale of eligible capital property from Schedule 10',
      tn: '108',
      formId: 'T2S1',
      fieldId: '108'
    },
    {
      label: 'Loss in equity of subsidiaries and affiliates',
      tn: '110',
      formId: 'T2S1',
      fieldId: '110'
    },
    {
      label: 'Loss on disposal of Class 10.1 vehicles',
      tn: '111',
      init: true,
      formId: 'T2S1',
      fieldId: '111'
    },
    {
      label: 'Charitable donations and gifts from Schedule 2',
      tn: '112',
      init: true,
      formId: 'T2S1',
      fieldId: '112'
    },
    {
      label: 'Taxable capital gains from Schedule 6',
      tn: '113',
      formId: 'T2S1',
      fieldId: '113'
    },
    {
      label: 'Political donations',
      tn: '114',
      init: true,
      formId: 'T2S1',
      fieldId: '114'
    },
    {
      label: 'Gross allowable SR&ED deducted per F/S'
    },
    {
      label: '(SR&ED ITC journalized against gross SR&ED)'
    },
    {
      label: 'Non-deductible club dues and fees',
      tn: '120',
      init: true
    },
    {
      label: 'Non-deductible meals and entertainment',
      tn: '121',
      init: true,
      formId: 'T2S1',
      fieldId: '120'
    },
    {
      label: 'Non-deductible automobile expenses',
      tn: '122',
      init: true,
      formId: 'T2S1',
      fieldId: '122'
    },
    {
      label: 'Non-deductible life insurance premiums ',
      tn: '123',
      init: true,
      formId: 'T2S1',
      fieldId: '123'
    },
    {
      label: 'Other reserves on lines 270-275 from Sch 13',
      tn: '125',
      init: true,
      formId: 'T2S1',
      fieldId: '125'
    },
    {
      label: 'Warranty reserve per F/S',
      init: true
    },
    {
      label: 'Unfunded pension per F/S',
      init: true
    },
    {
      label: 'Other reserves per F/S'
    },
    {
      label: 'Financing fees deducted',
      init: true
    },
    {
      label: 'Non-deductible advertising',
      init: true
    },
    {
      label: 'Non-deductible interest',
      init: true
    },
    {
      label: 'Non-deductible legal and accounting',
      init: true
    },
    {
      label: 'Recapture of SR&ED expenditures',
      init: 'Provincial ITCs from prior year'
    },
    {
      label: '(Gain on disposal of assets per F/S)'
    },
    {
      label: '(Non-taxable dividend under section 83)'
    },
    {
      label: '(Capital cost allowance)'
    },
    {
      label: '(Terminal loss)'
    },
    {
      label: '(Cumulative eligible capital deduction)'
    },
    {
      label: '(Allowable business investment loss)'
    },
    {
      label: '(Foreign non-business tax deduction)'
    },
    {
      label: '(SR&ED expenditures)'
    },
    {
      label: '(Other reserves of line 280 from Sch 13)'
    },
    {
      label: '(Warranty reserve per F/S)',
      init: true
    },
    {
      label: '(Unfunded pension per F/S)',
      init: true
    },
    {
      label: '(Other reserves per F/S)'
    },
    {
      label: '(Deduction under 20(1)(e))',
      init: true
    }
  ];

  var part2RowsArr = [
    {
      label: '<b>Net income (loss) for income tax purposes</b>',
      noCheckbox: true
    },
    {
      label: '(Net income items - marked by checkbox)',
      noCheckbox: true
    },
    {
      label: '(Federal Part I tax)',
      tn: '700',
      init: true,
      formId: 'T2J',
      fieldId: '700'
    },
    {
      label: '(Other federal taxes)',
      init: true
    },
    {
      label: '(Provincial corporate tax)',
      tn: '760',
      init: true,
      formId: 'T2J',
      fieldId: '760'
    },
    {
      label: '(Other provincial tax)',
      init: true
    },
    {
      label: '(Other income taxes)',
      init: true
    },
    {
      label: '(Financing fees capitalized per F/S)',
      init: true
    },
    {
      label: '(Net capital losses of previous years)',
      init: true
    },
    {
      label: 'Dividend refund',
      tn: '784',
      init: true,
      formId: 'T2J',
      fieldId: '784'
    },
    {
      label: 'Investment tax credit refund from Sch 31',
      tn: '780',
      init: true,
      formId: 'T2J',
      fieldId: '780'
    },
    {
      label: 'Provincial refundable credits from Sch 5',
      tn: '812',
      init: true,
      formId: 'T2J',
      fieldId: '812'
    },
    {
      label: 'Recovery of taxes paid in prior years',
      init: true
    },
    {
      label: 'Debt forgiveness',
      init: true
    }
  ];

  var part3RowsArr = [
    {
      label: '<b>Opening retained earnings</b>',
      noCheckbox: true
    },
    {
      label: 'Safe income on hand',
      noCheckbox: true
    },
    {
      label: '(Net income items - not marked in Part 1)',
      noCheckbox: true
    },
    {
      label: '(Retained earnings items - marked in Part 2)',
      noCheckbox: true
    },
    {
      label: ' (Dividends paid)',
      noCheckbox: true,
      formId: 'T2S3',
      fieldId: '552'
    }
  ];

  var part4RowsArr = [
    {
      label: 'Dividend paid in current fiscal period',
      noCheckbox: true,
      hiddenCellCols: [6]
    },
    {
      label: 'Total share number outstanding',
      noCheckbox: true,
      hiddenCellCols: [6]
    },
    {
      label: 'Dividend per share',
      noCheckbox: true,
      hiddenCellCols: [6]
    }
  ];

  function getTableColumns(colsObjArr) {
    var colsArr = [];
    if (!angular.isArray(colsObjArr))
      colsObjArr = [colsObjArr];
    colsObjArr.forEach(function(colObj) {
          var colClass = colObj.colClass || columnWidth;
          var colType = colObj.type;
          var colHeader = colObj.header || '';
          colsArr.push(
              {
                //amount
                header: '<b>' + colHeader + '</b>',
                type: colType,
                colClass: colClass,
                disabled: colObj.disabled
              },
              {
                //space between 2 blocks
                type: 'none',
                colClass: 'std-spacing-width'
              }
          )
        }
    );
    // colsArr.push(
    //     {
    //       //end spacing
    //       type: 'none'
    //     }
    // );
    return colsArr;
  }

  function getTableRows(labelArray) {
    var tableRows = [];

    if (!angular.isArray(labelArray))
      labelArray = [labelArray];

    labelArray.forEach(function(rowDataObj) {
      var hiddenCellCols = rowDataObj.hiddenCellCols || [];
      var cellType = rowDataObj.type;
      var inputRow = {};
      inputRow[0] = {label: rowDataObj.label};
      inputRow[2] = {
        type: !rowDataObj.noCheckbox ? 'singleCheckbox' : 'none',
        init: rowDataObj.init
      };
      inputRow[4] = {tn: rowDataObj.tn};

      inputRow[6] = {type: cellType};
      inputRow[8] = {type: cellType};
      inputRow[10] = {type: cellType};

      hiddenCellCols.forEach(function(colIndex) {
        inputRow[colIndex] = {type: 'none'};
      });

      inputRow.formId = rowDataObj.formId;
      inputRow.fieldId = rowDataObj.fieldId;
      tableRows.push(inputRow);
    });

    return tableRows
  }

  function getTableAdj() {
    return {
      maxLoop: 100000,
      showNumbering: true,
      infoTable: true,
      scrollx: true,
      dividers: [11],
      columns: getTableColumns(
          [
            {header: 'Other adjustments', type: 'text', colClass: otherAdjWidth},
            {type: 'singleCheckbox', colClass: columnTnWidth},
            {type: 'none', colClass: columnTnWidth},
            {},
            {},
            {},
            {},
            {colClass: percentWidth}
          ]
      )
    }
  }

  function getMainTable(rowsArr) {
    return {
      fixedRows: true,
      infoTable: true,
      superHeaders: [
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {
          header: 'Differences',
          divider: true,
          colspan: '4'
        }
      ],
      scrollx: true,
      dividers: [11],
      columns: getTableColumns(
          [
            {type: 'none', colClass: descriptionWidth},
            {type: 'singleCheckbox', colClass: columnTnWidth},
            {type: 'none', colClass: columnTnWidth},
            {header: 'Prior year'},
            {header: 'Current year'},
            {header: 'Last frozen scenario', disabled: true},
            {header: '$'},
            {header: '%', colClass: percentWidth}
          ]
      ),
      cells: getTableRows(rowsArr)
    }
  }

  function getTotalTable(desLabel) {
    return {
      fixedRows: true,
      infoTable: true,
      scrollx: true,
      superHeaders: [
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false}
      ],
      dividers: [11],
      columns: getTableColumns(
          [
            {type: 'none', colClass: descriptionWidth},
            {type: 'singleCheckbox', colClass: columnTnWidth},
            {type: 'none', colClass: columnTnWidth},
            {disabled: true},
            {disabled: true},
            {disabled: true},
            {disabled: true},
            {colClass: percentWidth, disabled: true}
          ]
      ),
      cells: getTableRows([{label: '<b>' + desLabel + '</b>', noCheckbox: true}])
    }
  }

  wpw.tax.create.tables('safeIncomeWorkchart', {
    '981': {
      showWhen: 'showHistoricalTable',
      fixedRows: true,
      infoTable: true,
      scrollx: true,
      columns: getTableColumns(
          [
            {header: 'Tax year-end', type: 'date'},
            {header: 'Net income (loss) for income tax purposes'},
            {header: 'Adjustments'},
            {header: 'SIOH - before dividends'},
            {header: 'Opening retained earnings'},
            {header: 'Dividends paid'},
            {header: 'Ending retained earnings'}
          ]
      )
    },
    '992': getMainTable(part1RowsArr),
    '996': getTableAdj(),
    '997': getTotalTable('Net income (loss) for income tax purposes'),
    '993': getMainTable(part2RowsArr),
    '994': getTableAdj(),
    '995': getTotalTable('Safe income on hand - before dividends'),
    '998': getMainTable(part3RowsArr),
    '999': getTableAdj(),
    '1001': getTotalTable('Ending retained earnings'),
    '1002': {
      fixedRows: true,
      infoTable: true,
      superHeaders: [
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {
          header: 'Differences',
          divider: true,
          colspan: '4'
        }
      ],
      scrollx: true,
      dividers: [11],
      columns: getTableColumns(
          [
            {type: 'none', colClass: descriptionWidth},
            {type: 'singleCheckbox', colClass: columnTnWidth},
            {type: 'none', colClass: columnTnWidth},
            {},
            {header: 'Current year'},
            {header: 'Last frozen scenario', disabled: true},
            {header: '$'},
            {header: '%', colClass: percentWidth}
          ]
      ),
      cells: getTableRows(part4RowsArr)
    },
    '1003': {
      fixedRows: true,
      showNumbering: true,
      infoTable: true,
      scrollx: true,
      superHeaders: [
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {divider: false},
        {
          header: 'Dividend payout amount',
          divider: true,
          colspan: '4'
        }
      ],
      dividers: [10],
      columns: getTableColumns([
        {header: 'Shareholder Name', type: 'text', colClass: otherAdjWidth},
        {header: '% common shares owned', colClass: percentShareWidth},
        {header: 'Number of owned shares'},
        {header: 'Current year'},
        {header: 'Last frozen scenario', disabled: true},
        {header: '$'},
        {header: '%', colClass: percentWidth}
      ])
    }
  })
})
();
