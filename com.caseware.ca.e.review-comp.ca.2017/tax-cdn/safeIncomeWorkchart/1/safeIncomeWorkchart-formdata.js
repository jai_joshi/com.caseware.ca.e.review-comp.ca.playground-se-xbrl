(function() {

  var freezeFunc = function() {
    wpw.tax.field('freeze').set(1);
  };

  wpw.tax.create.formData('safeIncomeWorkchart', {
    formInfo: {
      abbreviation: 'safeIncomeworkchart',
      title: 'Safe Income Workchart',
      showCorpInfo: true,
      category: 'Workcharts',
      description:[
        {
          type: 'list',
          items: [
              'The purpose of the Safe Income Worksheet is to calculate the safe income on hand for a class of shares, ' +
              'and allocate this balance to a shareholder.',
              'Any row with label inside a bracket, the user should enter amount as a negative value.',
              'Use these link to access the Safe Income webinars content for detailed:'
          ]
        }
      ]
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            type: 'infoField', inputType: 'link',
            link: 'http://wcc.on24.com/event/13/29/76/9/rt/1/documents/slidepdf/final__safe_income__cpa_canada.pdf'
          },
          {
            labelWidth: '100%',
            type: 'infoField', inputType: 'link',
            link: 'http://wcc.on24.com/event/13/24/03/5/rt/1/documents/slidepdf/final__subsection_55__cpa_canada.pdf'
          }
        ]
      },
      {
        header: 'Historical Data',
        rows: [
          {labelClass: 'fullLength'},
          {
            label: 'Is there any fiscal year period change during all years ?',
            labelClass: 'bold',
            type: 'infoField',
            inputType: 'radio',
            num: '1011',
            init: '2'
          },
          {
            label: 'Note: Any change to this option will remove inputted date values on historical data ' +
            '(A pop-up warning diagnostic will appear later)',
            labelClass: 'bold'
          },
          {type: 'horizontalLine'},
          //if there is fiscal period change
          {
            label: 'How many fiscal year periods excluding prior and current fiscal year period you want to track SIOH?',
            labelClass: 'bold fullLength',
            type: 'infoField',
            colClass: 'std-input-width',
            filters: 'number',
            init: '0',
            num: '1012',
            showWhen: {fieldId: '1011', compare: {is: '1'}}
          },
          {labelClass: 'fullLength'},
          {
            label: '(Please enter all fiscal ending period excluding prior and current fiscal year period ' +
            'you want to track SIOH on table below, ' +
            'all data will be saved and roll forward)',
            labelClass: 'bold',
            showWhen: {
              and: [
                {fieldId: '1011', compare: {is: '1'}},
                {fieldId: '1012', compare: {isNot: '0'}}
              ]
            }
          },
          //if no fiscal period change
          {
            type: 'infoField',
            label: 'Enter the FIRST ENDING fiscal year period you want to start tracking SIOH',
            labelClass: 'bold',
            inputType: 'date',
            num: '001',
            showWhen: {fieldId: '1011', compare: {is: '2'}}
          },
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '981'
          }
        ]
      },
      {
        header: 'Scenario settings',
        rows: [
          {
            type: 'infoField',
            label: 'Scenario description',
            labelClass: 'bold',
            width: '25%',
            num: '1013'
          },
          {
            type: 'infoField',
            inputType: 'functionButton',
            label: 'Freeze current year scenario',
            labelClass: 'bold',
            btnLabel: 'Freeze',
            paramFn: freezeFunc
          },
          {
            type: 'infoField',
            inputType: 'link',
            note: 'Please click here to go to SIOH summary workchart to see all freeze scenarios',
            formId: 'safeIncomeWorkchartS'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            label: 'Frozen scenario options',
            labelClass: 'bold',
            num: '1014',
            inputType: 'dropdown',
            width: '58%'
          },
          {
            type: 'infoField',
            inputType: 'functionButton',
            label: 'Load chosen scenario to current year data (WIP)',
            labelClass: 'bold',
            btnLabel: 'Load scenario'
            // paramFn: freezeFunc
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            label: 'Show difference between :',
            labelClass: 'bold',
            width: '58%',
            num: '1045',
            inputType: 'dropdown',
            init: '1',
            options: [
              {option: 'Prior Year vs Current fiscal year', value: '1'},
              {option: 'Prior Year vs Frozen scenario', value: '2'},
              {option: 'Current fiscal year vs Frozen scenario', value: '3'}
            ]
          },
          {labelClass: 'fullLength'},
          {
            label: 'Note for "Purpose test" subjected to Subsection 55',
            labelClass: 'bold'
          },
          {
            type: 'infoField',
            inputType: 'textArea',
            num: '1015'
          }
        ]
      },
      {
        header: 'Part 1 - Input of net income for income tax purposes',
        rows: [
          {labelClass: 'fullLength'},
          {
            label: 'Note: Mark the checkbox if this row adjusts safe income balance <br>' +
            'Where a Schedule 1 item has been marked, ' +
            'it is assumed to impact the safe income balance in the year the expense is incurred. <br>' +
            'Professional judgment is required for each possible adjustment.',
            labelClass: 'fullLength bold'
          },
          {labelClass: 'fullLength'},
          {type: 'table', num: '992'},
          {type: 'table', num: '996'},
          {labelClass: 'fullLength'},
          {type: 'table', num: '997'}
        ]
      },
      {
        header: 'Part 2 - Calculation of safe income on hand',
        rows: [
          {labelClass: 'fullLength'},
          {
            label: 'Note: Mark the checkbox if this row adjusts retained earnings <br>' +
            'Where an adjustment to the safe income calculation has been marked, ' +
            'the amounts will be reversed to reconcile the ending retained earnings. ',
            labelClass: 'fullLength bold'
          },
          {labelClass: 'fullLength'},
          {type: 'table', num: '993'},
          {type: 'table', num: '994'},
          {labelClass: 'fullLength'},
          {type: 'table', num: '995'}
        ]
      },
      {
        header: 'Part 3 - Reconciliation to retained earnings',
        rows: [
          {type: 'table', num: '998'},
          {type: 'table', num: '999'},
          {labelClass: 'fullLength'},
          {type: 'table', num: '1001'}
        ]
      },
      {
        header: 'Part 4 - Distribution of Dividend to Shareholders',
        rows: [
          {
            label: 'Please enter total share number outstanding in current fiscal year',
            labelClass: 'bold fullLength',
            type: 'infoField',
            colClass: 'std-input-width',
            filters: 'number',
            num: '1021'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Note: Differences shown in Part 4 are between current year and last frozen scenario.',
            labelClass: 'bold'
          },
          {type: 'horizontalLine'},
          {type: 'table', num: '1002'},
          {type: 'horizontalLine'},
          {
            label: 'Note: To change shareholders\' information please click Shareholder name column to jump to the schedule 50.',
            labelClass: 'bold'
          },
          {labelClass: 'fullLength'},
          {type: 'table', num: '1003'}
        ]
      }
    ]
  });
})();
