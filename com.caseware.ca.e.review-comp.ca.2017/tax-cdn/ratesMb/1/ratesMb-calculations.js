(function() {

  wpw.tax.create.calcBlocks('ratesMb', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      // var field = calcUtils.field;
      field('100').assign(0.2);
      field('101').assign(2.2);
      field('102').assign(3.0);
      field('103').assign(4.0);
      field('104').assign(5.0);
      field('105').assign(0.5);
      field('106').assign(11000000.0);
      field('107').assign(10000000.0);
      field('108').assign(10000000.0);
      field('109').assign(4000000000.0);
      field('110').assign(100 / 6);
      field('111').assign(25.0);
      field('112').assign(100 / 8);
      field('115').assign(2400.0);
      field('116').assign(5000.0);
      field('120').assign(1.0);
      field('129').assign(0);
      field('130').assign(12);
      field('122').assign(10);
      field('123').assign(135000);
      field('124').assign(202500);
      field('125').assign(67500);
      field('126').assign(45000);
      field('127').assign(425000);
      field('1271').assign(450000);
      field('128').assign(500000);
      field('140').assign(4);
      field('141').assign(4.5);
      field('142').assign(7);
      field('143').assign(8);
      field('144').assign(8);
      //sch380
      field('145').assign(20);
      field('1451').assign(15);
      field('146').assign(20);
      field('147').assign(10);
      field('148').assign(15);
      field('1481').assign(7.5);
      //sch381
      field('149').assign(10);
      field('150').assign(70);
      field('151').assign(80);
      field('152').assign(25);
      field('153').assign(15);
      field('154').assign(15);
      field('155').assign(15);
      field('156').assign(20);
      field('157').assign(25);
      field('158').assign(15);
      field('159').assign(10);

      field('1009').cell(0, 0).assign(wpw.tax.date(2014, 1, 1));
      field('1009').cell(1, 0).assign(wpw.tax.date(2016, 1, 1));
      field('1009').cell(0, 2).assign(425000);
      field('1009').cell(1, 2).assign(450000);
      field('1009').cell(0, 4).assign(12);
      field('1009').cell(1, 4).assign(12);
      field('1009').cell(0, 6).assign(0);
      field('1009').cell(1, 6).assign(0);
    });
  });
})();
