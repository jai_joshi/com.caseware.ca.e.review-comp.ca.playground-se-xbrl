(function() {
  wpw.tax.global.tableCalculations.ratesMb = {
    1009: {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          header: 'Effective Date',
          colClass: 'std-input-col-width',
          type: 'date',
          cellClass: 'alignCenter'
        },
        {
          type: 'none'
        },
        {
          header: 'Amount for D1',
          colClass: 'std-input-width'

        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Higher Rate',
          colClass: 'std-input-width'

        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Lower Rate',
          colClass: 'std-input-width'

        }],
      cells: [
        {
          2: {
            num: '901'
          },
          4: {
            num: '902',
            decimals: 1
          },
          6: {
            num: '903',
            decimals: 1
          }
        },
        {
          2: {
            num: '904'
          },
          4: {
            num: '905',
            decimals: 1
          },
          6: {
            num: '906',
            decimals: 1
          }
        }
      ]
    }
  };
})();
