(function() {
  wpw.tax.create.formData('ratesMb', {
    formInfo: {
      abbreviation: 'ratesMb',
      title: 'Manitoba Table of Rates and Values',
      showCorpInfo: true,
      category: 'Rates Tables'
    },
    sections: [
      {
        'header': 'MCT 1 - Corporation capital tax return',
        'rows': [
          {
            'label': '<b>Capital tax payable</b>'
          },
          {
            'label': 'General rate for corporations with a taxable capital that exceeds $11,000,000 and a fiscal year that begins after January 1, 2010',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '100',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Additional taxable capital rate for a corporation with a fiscal year that begins after January 1, 2010, and a taxable capital that exceeds $10,000,000, but that does not exceed $11,000,000',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '101',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Bank, loan or trust corporations rate - For fiscal years ending before April 18, 2012',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '102',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Bank, loan or trust corporations rate - For fiscal years ending after April 17, 2012 and before April 17, 2013 ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '103',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Bank, loan or trust corporations rate - For fiscal years ending after April 16, 2013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '104',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Crown corporations rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '105',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '<br><b>Taxable paid up capital</b>'
          },
          {
            'label': 'Bracket 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '106'
                }
              }
            ]
          },
          {
            'label': '<br><b>Taxable capital</b>'
          },
          {
            'label': 'Capital deduction for corporations with a fiscal year that begins after January 1, 2007',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '107'
                }
              }
            ]
          },
          {
            'label': 'Minimum taxable capital - large corporations',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '108'
                }
              }
            ]
          },
          {
            'label': '<br><b>Exemption for small banks, trusts and loans corporations</b>'
          },
          {
            'label': 'Capital tax exemption for banks, trusts and loans corporations with taxable paid-up capital under $4 bilion with a taxation year-ending after April 12, 2011',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '109'
                }
              }
            ]
          },
          {
            'label': '<br><b>Goodwill allowance</b>'
          },
          {
            'label': 'Capitalized value',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Goodwill allowance rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '111'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '<br><b>Taxable capital employed in Canada</b>'
          },
          {
            'label': 'Capitalized value',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '112',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': '<br><b>Manitoba tax instalments</b>'
          },
          {
            'label': 'Taxation year',
            'input2Header': 'Quarterly instalments threshold amount'
          },
          {
            'label': '2005-01-01',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '115'
                }
              }
            ]
          },
          {
            'label': '2008-01-02',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '116'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 393 - Manitoba Nutrient Management Tax Credit ',
        'rows': [
          {
            'label': 'Applicable rate (%)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '122'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 383 - Manitoba Corporation Tax Calculation',
        'rows': [
          {
            'type': 'table',
            'num': '1009'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Denominator amount for D1',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '128'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 387 - Manitoba Small Business Venture Capital Tax Credit',
        'rows': [
          {
            'label': 'Maximum ending credit - before June 12, 2014',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '123'
                }
              }
            ]
          },
          {
            'label': 'Maximum ending credit - after June 11, 2014',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '124'
                }
              }
            ]
          },
          {
            'label': 'Credit claimed in the current year - after June 12, 2014',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '125'
                }
              }
            ]
          },
          {
            'label': 'Credit claimed in the current year - before June 12, 2014',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '126'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Schedule 392 - Manitoba Data Processing Investment Tax Credit ',
        'rows': [
          {
            'label': '<b>Data Processing buildings rate (%)</b>'
          },
          {
            'label': 'before July 1, 2013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '140'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'after June 30, 2013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '141',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '<b>Data Processing centre property rate (%)</b>'
          },
          {
            'label': 'before July 1, 2013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '142'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'after June 30, 2013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '143'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '<b>Data Processing property investment rate (%)</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '144'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 380 - Manitoba Research and Development Tax Credit',
        'rows': [
          {
            label: 'Repayments for assistance received',
            labelClass: 'bold'
          },
          {
            'label': 'Before April 12, 2017',
            'layout': 'alignInput',
            labelCellClass: 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '145'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'After April 11, 2017',
            'layout': 'alignInput',
            labelCellClass: 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '1451'
                }
              }
            ],
            'indicator': '%'
          },
          {
            label: '<b>Refundable R&D tax credit</b>'
          },
          {
            'label': 'Line 108 in Part 1',
            'layout': 'alignInput',
            labelCellClass: 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '146'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Line 116 in Part 1',
            'layout': 'alignInput',
            labelCellClass: 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '147'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Line 109 in Part 1',
            'layout': 'alignInput',
            labelCellClass: 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '148'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Line 117 in Part 1',
            'layout': 'alignInput',
            labelCellClass: 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'decimals': 1,
                  'num': '1481'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 381 - Manitoba Manufacturing Investment Tax Credit',
        'rows': [
          {
            'label': 'Amount A from Part 1',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '149'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Credit included in amount a that is earned before July 1, 2013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '150'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Credit included in amount a that is earned after June 30, 2013',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '151'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 384 - Manitoba Paid Work Experience Tax Credit',
        'rows': [
          {
            'label': 'Part 2 - Youth work experience hiring incentive',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '152'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Part 3 - Co-op student hiring incentive',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '153'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Part 4 - Co-op graduate hiring incentive',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '154'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Part 5 - Apprentice hiring incentive',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '155'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Part 6 - Apprentice hiring incentive for rural or northern early level apprentice',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '156'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Part 7 - Apprentice hiring incentive for high school apprentice',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '157'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Part 8 - Journeyperson hiring incentive',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '158'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 385 - Manitoba Odour-Control Tax Credit',
        'rows': [
          {
            'label': 'Current-year credit earned',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '159'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      }
    ]
  });
})();
