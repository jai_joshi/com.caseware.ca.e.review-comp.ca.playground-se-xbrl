(function() {
  'use strict';

  wpw.tax.global.formData.t2s13w = {
      formInfo: {
        abbreviation: 't2s13w',
        isRepeatForm: true,
        repeatFormData: {
          titleNum: '102'
        },
        schedule: 'SCHEDULE 13 WORKCHART',
        showCorpInfo: true,
        category: 'Workcharts'
      },
      sections: [
        {
        header: 'Reserve Account Information',
          rows: [
            // {
            //   label: 'Update this form from:', num: '100',
            //   type: 'infoField', inputType: 'dropdown', init: '1',
            //   showValues: 'before',
            //   options: [
            //     {value: '1', option: 'No update, enter Data Directly'},
            //     {
            //       value: '2',
            //       option: 'Update from GL in Simple Engagement (the Default if there is SE with a GL)'
            //     },
            //     {value: '3', option: 'From GL in linked CW WP file through TaxSync'},
            //     {
            //       value: '4',
            //       option: 'From a CSV file (from Excel; Google Sheets; QuickBooks; Sage (Simply Accounting) '
            //     }
            //   ]
            // },
            // {labelClass: 'fullLength'},
            {type: 'infoField', label: 'GL A/C # (if applicable)', inputClass: 'std-input-col-width-3', num: '101'},
            {type: 'infoField', label: 'Description of GL A/C',
              inputClass: 'std-input-col-width-3', num: '102'},
            {labelClass: 'fullLength'},
            {
              type: 'infoField',
              label: 'Type of Reserve Account',
              init: '1',
              num: '103',
              inputClass: 'std-input-col-width-3',
              inputType: 'dropdown',
              options: [
                {value: '1', option: 'Reserve for doubtful debts'},
                {value: '2', option: 'Reserve for undelivered goods and services not rendered'},
                {value: '3', option: 'Reserve for prepaid rent'},
                {value: '4', option: 'Reserve for returnable containers'},
                {value: '5', option: 'Reserve for unpaid amounts'},
                {value: '6', option: 'Reserve for corporate insurance policy'},
                {value: '7', option: 'Reserve for banking'},
                {value: '8', option: 'Other tax reserves'}
              ]
            },
            {labelClass: 'fullLength'},
            {
              showWhen: {fieldId: '103', compare: {is: '1'}},
              label: '<b>Note:</b> <br>If the accounting reserve for doubtful debts is calculated by percentage ' +
              'method, it is advised that the reserve for tax purpose will possibly be different. In order to ' +
              'have an estimate, please reference with the reserve amount as if derived from aging method.',
              labelClass: 'fullLength'
            },
            {
              showWhen: {fieldId: '103', compare: {is: 2}},
              label: '<b>Note:</b> <br>For Reserve for undelivered goods and services not rendered: ' +
              'Please note new rules re. ' +
              'Reclamation Projects in the T2 Guide',
              labelClass: 'fullLength'
            },
            {type: 'horizontalLine'},
            {labelClass: 'fullLength'},
            {
              type: 'infoField',
              label: 'Has this amount been reported on the corporation\'s financial statements ?',
              labelClass: 'bold',
              labelWidth: '537px',
              inputType: 'radio',
              num: '131',
              init: '2'
            },
            {
              'label': 'Balance at the beginning of the year',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '110'
                  }
                }
              ]
            },
            {
              'label': 'Transfer on an amalgamation or the wind-up of a subsidiary ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '111'
                  }
                }
              ]
            },
            {
              'label': 'Adjustments during the year',
              'labelClass': 'bold'
            },
            {
              'type': 'table',
              'num': '120'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b>Net Adjustments</b>',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '125'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '126'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ]
            },
            {
              'label': 'Balance at the end of the year ',
              'labelClass': 'bold',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': false,
                'showDots': false,
                'paddingRight': false
              },
              'columns': [
                {
                  'input': {
                    'num': '130'
                  }
                }
              ]
            },
            {labelClass: 'fullLength'}
          ]
        },
        {
        header: 'Total summary of all reserves claimed which are allowed for tax purposes - ' +
          'Value will be transferring to Schedule 13 then Schedule 1 line 125 and line 414',
          rows: [
            {
              type: 'table', num: '201'
            }
          ]
        },
        {
        header: 'Total summary of all reserve accounts from Financial Statement which are not deductible for tax purposes - ' +
          'Total value will be transferring to Schedule 1 line 126 and line 413',
          rows: [
            {
              type: 'table', num: '998'
            }
          ]
        }
      ]
    };
})();
