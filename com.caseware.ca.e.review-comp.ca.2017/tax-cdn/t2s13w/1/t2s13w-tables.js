(function() {

  function getTableSummaryColumns() {
    return [
      {
        'header': 'Description',
        'width': '395px',
        'disabled': true,
        type: 'none'
      },
      {
        'header': 'Balance at the beginning of the year <br> $',
        'disabled': true,
        total: true
      },
      {
        'header': 'Transfer on an amalgamation or the wind-up of a subsidiary <br> $',
        'disabled': true,
        total: true
      },
      {
        'header': 'Net Adjustments made during the year',
        'disabled': true,
        total: true
      },
      {
        'header': 'Balance at the end of the year <br> $',
        'disabled': true,
        total: true
      }
    ]
  }

  function getTableSummaryRows() {
    return [
      {'0': {label: 'Reserve for doubtful debts'}},
      {'0': {label: 'Reserve for undelivered goods and services not rendered'}},
      {'0': {label: 'Reserve for prepaid rent'}},
      {'0': {label: 'Reserve for returnable containers'}},
      {'0': {label: 'Reserve for unpaid amounts'}},
      {'0': {label: 'Reserve for corporate insurance policy'}},
      {'0': {label: 'Reserve for banking'}},
      {'0': {label: 'Other tax reserves'}}
    ]
  }

  wpw.tax.global.tableCalculations.t2s13w = {
    '120': {
      hasTotals: true,
      'width': 'calc((100% - 105px))',
      'columns': [
        {
          'header': 'Transaction #',
          colClass: 'std-input-width',
          'canSort': true,
          type: 'text'
        },
        {
          'header': 'Date of Transaction',
          'width': '150px',
          'type': 'date',
          'canSort': true
        },
        {
          'header': 'Transaction Description',
          'canSort': true,
          type: 'text'
        },
        {
          'header': 'Type of Transaction',
          'type': 'dropdown',
          colClass: 'std-input-width',
          'showValues': 'before',
          'init': '1',
          'canSort': true,
          'options': [
            {
              'value': '1',
              'option': 'Addition'
            },
            {
              'value': '2',
              'option': 'Deduction'
            }
          ]
        },
        {
          'header': 'Transaction Amount',
          filters: 'prepend $',
          'canSort': true,
          colClass: 'std-input-width',
          'init': '0'
        }
      ]
    },
    '201': {
      'fixedRows': true,
      hasTotals: true,
      'columns': getTableSummaryColumns(),
      'cells': getTableSummaryRows()
    },
    '998': {
      'fixedRows': true,
      hasTotals: true,
      'columns': getTableSummaryColumns(),
      'cells': getTableSummaryRows()
    }
  }
})();
