(function() {

  wpw.tax.create.calcBlocks('t2s13w', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //to make sure there is a reserve type
      if (angular.isUndefined(field('103').get())) {
        field('103').assign('1');
        field('103').disabled = false;
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //calculate net adj during the year
      var netAdjustments = 0;
      field('120').getRows().forEach(function(row) {
        var typeOfTransaction = row[3].get();
        var amountOfTransaction = row[4].get();
        if (typeOfTransaction == 1) {
          netAdjustments += amountOfTransaction;
        }
        else {
          netAdjustments = netAdjustments - amountOfTransaction;
        }
      });
      field('125').assign(netAdjustments);
      calcUtils.equals('126', '125');
      calcUtils.sumBucketValues('130', ['110', '111', '126']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //populate table using value from s13
      var table201LinkedS13 = form('T2S13').field('201');
      var table998LinkedS13 = form('T2S13').field('998');

      field('201').getRows().forEach(function(row, rIndex) {
        row.forEach(function(cell, cIndex) {
          cell.assign(table201LinkedS13.cell(rIndex, cIndex).get())
        });
      });

      field('998').getRows().forEach(function(row, rIndex) {
        row.forEach(function(cell, cIndex) {
          cell.assign(table998LinkedS13.cell(rIndex, cIndex).get())
        });
      });

    });
  });
})();
