(function() {

  wpw.tax.global.formData.wacOnline = {
      formInfo: {
        abbreviation: 'wac-online',
        title: 'Web Access Code (WAC) - Online Request',
        headerImage: 'cw',
        showCorpInfo: true,
        category: 'EFILE',
        description: [
          {
            text: 'This workchart is for making online request to Web Access Code (WAC) for efiling.'
          }
        ]
      },
      sections: [
        {
        forceBreakAfter: true,
        header: 'Step 1 - Required information for getting Web Access Code (WAC)',
          rows: [
            {
              type: 'infoField',
              inputType: 'custom',
              format: ['{N9}RC{N4}', 'NR'],
              label: 'Business Number',
              num: '100',
              inputClass: 'std-input-width',
              disabled: true
            },
            {
              type: 'infoField',
              inputType: 'date',
              label: 'Tax year end date',
              inputClass: 'std-input-width',
              num: '101',
              disabled: true
            },
            {
              type: 'infoField',
              inputType: 'date',
              label: 'Date of incorporation',
              inputClass: 'std-input-width',
              num: '102',
              disabled: true
            },
            {
              type: 'infoField',
              label: 'Postal Code/Zip Code',
              inputClass: 'std-input-width',
              num: '103'
            },
            {
              label: 'Last name of authorized signing officer',
              type: 'infoField',
              inputClass: 'std-input-width',
              num: '104'
            },
            {
              label: 'First name of authorized signing officer',
              type: 'infoField',
              inputClass: 'std-input-width',
              num: '105'
            },
            {
              label: 'Position of authorized signing officer',
              type: 'infoField',
              inputClass: 'std-input-width',
              num: '106'
            }
          ]
        },
        {
        forceBreakAfter: true,
        header: 'Step 2 - Get the Web Access Code (WAC) from CRA',
          rows: [
            {
              label: 'After all the required information in the above section is inputted, please begin with ' +
              'the following steps to acquire a Web Access Code(WAC) from CRA.',
              labelClass: 'fullLength'
            },
            {
              type: 'infoField',
              inputType: 'functionButton',
              btnLabel: 'Request WAC',
              inputClass: 'std-input-width',
              paramFn: wpw.tax.actions.requestWACClick
            }
          ]
        },
        {
        forceBreakAfter: true,
        header: 'Step 3 - Web Access Code(WAC) input',
          rows: [
            {labelClass: 'fullLength'},
            {
              label: 'Web Access Code (WAC) input here:',
              input2: true,
              num2: '108',
              inputClass: 'std-input-width'
            }
          ]
        }
      ]
    };
})();
