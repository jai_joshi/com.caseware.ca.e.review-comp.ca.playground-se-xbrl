/* global wpw */

(function() {
  'use strict';

  /**
   * Adds leading zeroes to a string
   * @param {String} num
   * @param {number} size
   * @returns {string}
   */
  function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
  }

  /**
   * Formats date internal object {year: 2017, month: 6, day: 22, hour: 0, min: 0} into YYYY-MM-DD
   * @param date
   * @returns {String}
   */
  function formatDate(date) {
    var /** @type String */ result = "";
    if(date){
      result = date.year + "-" + pad(date.month, 2) + "-" +  pad(date.day, 2);
    }
    return result ;
  }

  /**
   * Formats date internal object {year: 2017, month: 6, day: 22, hour: 0, min: 0} into time hh:mm
   * @param date
   * @returns {String}
   */
  function formatTime(date){
    var /** @type String */ result = "";
    if(date){
      result =  pad(date.hour, 2) + ":" +  pad(date.min, 2);
    }
    return result === "00:00" ? "" : result;
  }

  if (!wpw.tax.actions)
    wpw.tax.actions = {};

  var /** {Object} */ parameters = {}, wait;

  wpw.tax.actions.requestWACClick = function() {
    checkValues().then(function(xml) {
      tryToObtainWAC(xml);
    });
  };

  function checkValues() {

    function showMessage(msg, id){
      wpw.tax.global.dialogs.notify(title, msg);
      var /** @type Object */ field = document.getElementById(id);
      if(field) field.focus();
      deferred.reject();
    }

    var deferred = wpw.tax.global.q.defer();
    var /** @type String */ title = "Compilation + Tax";

    parameters["businessNumber"] = wpw.tax.field('100', 'wacOnline').get() || "";
    parameters["taxationYearEndDate"] = formatDate(wpw.tax.field('101','wacOnline').get());
    parameters["taxationYearEndTime"] = formatTime(wpw.tax.field('101','wacOnline').get());
    parameters["incorporationDate"] = formatDate(wpw.tax.field('102','wacOnline').get());
    parameters["postalZipCode"] = wpw.tax.field('103','wacOnline').get() || "";
    parameters["lastName"]= wpw.tax.field('104','wacOnline').get() || "";
    parameters["firstName"]= wpw.tax.field('105','wacOnline').get() || "";
    parameters["title"]= wpw.tax.field('106','wacOnline').get() || "";

    //TODO: Add parameters evaluation, for now just resolve the promise
    if (!parameters["businessNumber"]) {
      showMessage("Please enter Business number", "id100");
    } else if (!parameters["taxationYearEndDate"]) {
      showMessage("Please enter Tax year end date", "id101");
    } else if (!parameters["incorporationDate"]) {
      showMessage( "Please enter Date of incorporation", "id102");
    } else if (!parameters["postalZipCode"]) {
      showMessage("Please enter Postal Code/Zip Code", "id103");
    } else if (!parameters["lastName"]) {
      showMessage("Please enter Last name of authorized signing officer", "id104");
    } else if (!parameters["firstName"]) {
      showMessage("Please enter First name of authorized signing officer", "id105");
    } else if (!parameters["title"]) {
      showMessage("Please enter Position of authorized signing officer", "id106");
    } else {
      deferred.resolve();
    }
    return deferred.promise;
  }

  /**
   * @param {Object} result
   */
  function parseWACRepsonse(result) {
    'use strict';

    /**
     * Tries to find the first node in given document using namespace and local name
     * @param doc a document to search
     * @param namespace namespace
     * @param localName local tag/node name
     * @return {String} found node value or empty string
     */
    function findNode(doc, namespace, localName){
      var /** @type HTMLCollection */ list = doc.getElementsByTagNameNS(namespace, localName);
      var /** @type String */ result = "";
      if(list.length > 0){
        result =list[0].textContent;
      }
      return result;
    }

    var /** @type String */ message = "", title = "", ns = "http://www.cra-arc.gc.ca/xmlns/tgif/response/1-0-0";

    try {
      var xmlDoc = $($.parseXML(result["xmlResponse"]))[0];
      message = "Business number: " + findNode(xmlDoc, ns, "BusinessNumber") + "<br/>" +
          "Taxation year end date: " + findNode(xmlDoc, ns, "TaxationYearEndDate") + "<br/>" +
          "Taxation year end time: " + findNode(xmlDoc, ns, "TaxationYearEndTime") + "<br/><br/>";
      if (xmlDoc.getElementsByTagNameNS(ns,"ErrorCode").length > 0) {
        title = "There was a problem during obtaining WAC";
        for (var i = 0; i < xmlDoc.getElementsByTagNameNS(ns,"ErrorCode").length; i++) {
          message = message + "Error code: " + xmlDoc.getElementsByTagNameNS(ns, "ErrorCode")[i].textContent + "<br/>" +
              "Error message: " + xmlDoc.getElementsByTagNameNS(ns, "EnglishMessageText")[i].textContent + "<br/><br/>";
        }
      } else {
        var /** @type String */ wac = findNode(xmlDoc, ns, "WACode");
        if (wac) {
          title = "WAC successfully obtained";
          message = message + "WAC: " + wac + "<br/><br/>Timestamp: " + findNode(xmlDoc, ns, "TimestampDateTime");
          wpw.tax.field('108', 'wacOnline').set(wac);
        }
      }
      wait.close();
      wpw.tax.global.dialogs.notify(title, message);
    } catch (e) {
      wait.close();
      wpw.tax.global.dialogs.error(e.name, e.message);
    }
  }

  /**
   * @param {Object} errors
   */
  function parseErrors(errors) {
    var message = "WAC number hasn't been received. Please contact your software developer.";
    if (errors && errors.status && errors.statusText) {
      message = errors.status + ' ' + errors.statusText;
    }
    wait.close();
    wpw.tax.global.dialogs.error('There was a problem during obtaining WAC', message);
  }

  function tryToObtainWAC() {
    wait = wpw.tax.global.dialogs.wait('', 'Connecting CRA...', 0);
    wpw.tax.global.pluginService.getOnlineWac(parameters).then(function(result) {
      parseWACRepsonse(result);
    }, function(errors) {
      parseErrors(errors);
    });
  }

})();