(function() {
  wpw.tax.create.diagnostics('wacOnline', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('BN', common.bnCheck('100'));

    diagUtils.diagnostic('WAC.400', common.prereq(
        common.check('cp.154', 'isNo'),
        common.check(['wacOnline.108', 'efile.wac'], 'isFilled', true)));

  });
})();
