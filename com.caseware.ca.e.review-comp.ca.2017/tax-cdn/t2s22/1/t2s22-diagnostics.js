(function() {
  wpw.tax.create.diagnostics('t2s22', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0220001', common.prereq(common.check(['t2j.168'], 'isChecked'), common.requireFiled('T2S22')));

    diagUtils.diagnostic('0220002', common.prereq(common.requireFiled('T2S22'),
        function(tools) {
          var table = tools.field('050');
          return tools.checkAll(table.getRows(), function(row) {
            var columns = [row[0], row[1], row[2]];
            if (tools.checkMethod(columns, 'isFilled'))
              return tools.requireAll(columns, 'isNonZero');
            else return true;
          });
        }));
  });
})();

