(function() {
  'use strict';
  //
  //$scope.id = $routeParams.id;

  wpw.tax.global.formData.t2s22 = {
    formInfo: {
      abbreviation: 'T2S22',
      title: 'Non-Resident Discretionary Trust',
      formFooterNum: 'T2 SCH 22 E (11)',
      //subTitle: '(2011 and later tax years)',
      schedule: 'Schedule 22',
      code: 'Code 1101',
      showCorpInfo: true,
      description: [
        {text: ''},
        {
          text: 'Complete this schedule if, at any time during the tax year, one of the following ' +
          'organizations had a beneficial interest in a non-resident discretionary trust:',
          labelClass: 'tabbed2'
        },
        {
          type: 'list',
          items: [
            'the corporation;',
            'a controlled foreign affiliate of the corporation; or',
            'any other corporation or trust that did not deal at arm\'s length with the corporation.'
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            type: 'table', num: '050'
          },
          {
            label: '<i>Privacy Act</i>, Personal Information Bank number CRA PPU 047',
            labelClass: 'absoluteAlignRight'
          }
        ]
      }
    ]
  };
})();
