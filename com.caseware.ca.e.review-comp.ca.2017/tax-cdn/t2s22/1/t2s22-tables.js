(function() {
  wpw.tax.global.tableCalculations.t2s22 = {
    "050": {
      "showNumbering": true,
      "maxLoop": 100000,
      "columns": [{
        "header": "Name of non-resident discretionary trust",
        "num": "100","validate": {"or":[{"length":{"min":"1","max":"175"}},{"check":"isEmpty"}]},
        "width": "30%",
        "tn": "100",
        type: 'text'
      },
        {
          "header": "Mailing address",
          "num": "200",
          "width": "50%",
          "tn": "200",
          "type": "address"
        },
        {
          "header": "Name of Trustee",
          "num": "300",
          "width": "20%",
          "tn": "300",
          "maxLength": "175",
          type: 'text'
        }]
    }
  }
})();
