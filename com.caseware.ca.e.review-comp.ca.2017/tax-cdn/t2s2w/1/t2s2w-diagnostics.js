(function() {
  wpw.tax.create.diagnostics('t2s2w', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('002W.DonType', common.prereq(function(tools) {
      return tools.field('103').get() != '4';
    }, function(tools) {
      var table = tools.field('200');
      return tools.checkAll(table.getRows(), function(row) {
        if (row[4].valueObj.data.overridden)
          return row[4].require(function() {
            return false;
          })
      })
    }));
  });
})();
