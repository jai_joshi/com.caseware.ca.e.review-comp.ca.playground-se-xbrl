(function() {
  function disableRow(row, condition) {
    if (condition == true) {
      row[5].assign(0);
      row[5].disabled(true);
      row[0].disabled(true);
      row[2].disabled(true);
      row[3].disabled(true);
    }
    else {
      row[5].disabled(false);
      row[0].disabled(false);
      row[2].disabled(false);
      row[3].disabled(false);
    }

  }
  wpw.tax.create.calcBlocks('t2s2w', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      var selectedType = field('103').get();
      var multiType = selectedType == '4';

      var data = [];
      var table = field('200');
      // use getCol instead of getRows to reduce number of cells that need to be converted into Field objects
      table.getCol(4).forEach(function(cell, rowIndex) {
        var donationType;
        if (multiType) {
          donationType = cell.get();
          cell.disabled(false);
        } else {
          donationType = selectedType;
          cell.assign(donationType);
        }
        var donationAmount = table.cell(rowIndex, 5).get();
        data.push({'type': donationType, 'amount': donationAmount});
      });
      field('300').assign(calcUtils.sumByKey(data, 'type', '1', 'amount'));
      field('302').assign(calcUtils.sumByKey(data, 'type', '2', 'amount'));
      field('303').assign(calcUtils.sumByKey(data, 'type', '3a', 'amount'));
      field('304').assign(calcUtils.sumByKey(data, 'type', '3b', 'amount'));
    });
    calcUtils.calc(function(calcUtils, field) {
      field('200').getRows().forEach(function(row) {
        if (row[4].get() == '3b' && calcUtils.dateCompare.lessThan(row[1].get(), wpw.tax.date(2014, 2, 10))) {
          disableRow(row, true)
        }
        else if (row[4].get() == '3a' && calcUtils.dateCompare.greaterThan(row[1].get(), wpw.tax.date(2014, 2, 11))) {
          disableRow(row, true)
        }
        else {
          disableRow(row, false)
        }
      })
    })
  });
})();
