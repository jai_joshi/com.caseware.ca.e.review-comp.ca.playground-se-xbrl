(function() {
  wpw.tax.global.tableCalculations.t2s2w = {
    "200": {
      hasTotals: true,
      "columns": [
        {
          "header": "Transaction #",
          colClass: 'std-input-col-width',
          "canSort": true,
          type: 'text'
        },
        {
          "header": "Date of Transaction",
          colClass: 'std-input-col-width',
          "type": "date",
          "canSort": true
        },
        {
          "header": "Name of the Donnee",
          "canSort": true,
          type: 'text'
        },
        {
          "header": "Registered Charity # (if applicable)",
          colClass: 'std-input-col-width',
          "canSort": true,
          type: 'text'
        },
        {
          "header": "Type of Donation (choose from Dropdown)",
          "type": "dropdown",
          colClass: 'std-input-col-width',
          "showValues": "before",
          "init": "1",
          "canSort": true,
          "num": "201",
          "options": [{
            "value": "1",
            "option": "Charitable Donations (Part 1 of Sch. 2)"
          },
            {
              "value": "2",
              "option": "Gifts of certified cultural property (Part 3 of Sch. 2)"
            },
            {
              "value": "3a",
              "option": "Gifts of certified ecologically sensitive land made before February 11, 2014(Part 4 of Sch. 2)"
            },
            {
              "value": "3b",
              "option": "Gifts of certified ecologically sensitive land made after February 10, 2014(Part 4 of Sch. 2)"
            }
          ]
        },
        {
          "header": "Amount of Donation",
          filters: 'prepend $',
          "total": true,
          "canSort": true,
          colClass: 'std-input-col-width',
          "init": "0"
        }]
    },
    '500': {
      hasTotals: true,
      'fixedRows': true,
      'columns': [
        {
          'header': 'Description of GL A/C',
          'disabled': true,
          type: 'text'
        },
        {
          'header': 'Account balance transferred',
          colClass: 'std-input-col-width',
          'disabled': true
        }
      ],
      'cells': [{
        '0': {},
        '1': {}
      }]
    }
    // '600':{
    //   fixedRows: true,
    //   infoTable: true,
    //   columns: [
    //     {
    //       type: 'none',
    //       colClass: 'std-input-col-padding-width'
    //     },
    //     {
    //       type: 'text',
    //       formField: true,
    //       textAlign: 'left'
    //     }
    //   ],
    //   cells: [
    //     {
    //       0:{
    //         label: 'Description of GL A/C'
    //       },
    //       1:{
    //         num: '101'
    //       }
    //     }
    //   ]
    // }
  }
})();
