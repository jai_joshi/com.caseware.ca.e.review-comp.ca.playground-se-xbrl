(function() {
  wpw.tax.create.diagnostics('t2s502', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('5020005', common.prereq(common.and(
        common.requireFiled('T2S502'),
        common.check(['t2s5.406'], 'isNonZero')),
        common.check(['025', '050', '075', '100', '110', '120', '130', '140',
          '150', '175', '200', '210', '220', '230', '240', '250'], 'isNonZero')));

    diagUtils.diagnostic('5020010', common.prereq(common.and(
        common.requireFiled('T2S502'),
        common.check(['t2s5.406', '150'], 'isNonZero', true)),
        function(tools) {
          return tools.requireOne(tools.list(['100']), 'isNonZero') &&
              (tools.requireAll(tools.list(['110', '120']), 'isNonZero') ||
              tools.requireAll(tools.list(['130', '140']), 'isNonZero'));
        }));

    diagUtils.diagnostic('5020015', common.prereq(common.and(
        common.requireFiled('T2S502'),
        common.check(['t2s5.406', '250'], 'isNonZero', true)),
        function(tools) {
          return tools.requireOne(tools.list(['200']), 'isNonZero') &&
              (tools.requireAll(tools.list(['210', '220']), 'isNonZero') ||
              tools.requireAll(tools.list(['230', '240']), 'isNonZero'));
        }));
  });
})();
