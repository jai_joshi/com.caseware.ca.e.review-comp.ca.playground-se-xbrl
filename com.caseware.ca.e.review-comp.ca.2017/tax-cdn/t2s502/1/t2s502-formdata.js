(function() {
  'use strict';

  wpw.tax.global.formData.t2s502 = {
      formInfo: {
        abbreviation: 'T2S502',
        title: 'Ontario Tax Credit for Manufacturing and Processing',
        //subTitle: '(2012 and later tax years)',
        schedule: 'Schedule 502',
        code: 'Code 1202',
        formFooterNum: 'T2 SCH 502 E (12)',
        showCorpInfo: true,
        headerImage: 'canada-federal',
        description: [
          {
            type: 'list',
            items: [
              'Complete this schedule if the corporation had Ontario taxable income during' +
              ' the tax year and had eligible Canadian profits (ECP) from any of the following' +
              ' activities: manufacturing and processing, farming, fishing, logging, mining,' +
              ' generating electrical energy for sale, or producing steam for sale.',
              'You do not need to file Schedule 27, ' + '<i>' + ' Calculation of Canadian' +
              ' Manufacturing and Processing Profits Deduction ' + '</i>' + ', if you are' +
              ' only eligible for the Ontario tax credit for manufacturing and processing.',
              'File this schedule with the ' + '<i>' + ' T2 Corporation Income Tax Return. ' + '</i>'
            ]
          }
        ],
        category: 'Ontario Forms'

      },
      sections: [
        {
          'header': 'Part 1 – Calculation of adjusted business income',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Active business income <b> minus </b> active business losses of the corporation for the year *' +
              '<br> (amount from line 100 or line 120, whichever applies, of Schedule 27, if filed)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '025',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '025'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Add: </b>'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Adjusted Crown royalties for the year (amount from line 100 of Schedule 504, <br><br> <i> Ontario Resource Tax Credit and Ontario Additional Tax re Crown Royalties </i>)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '030'
                  }
                }
              ]
            },
            {
              'label': 'Subtotal',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '035'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Deduct: </b>'
            },
            {
              'label': 'Notional resource allowance for the year (amount from line 105 of Schedule 504)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '040'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Adjusted business income </b> (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '050',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '050'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* This includes the corporation\'s share of active business income and active business loss for the fiscal period of each partnership of which the corporation was a member at any time in its tax year'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelCLass': 'fullLength'
            },
            {
              'label': '<b> Small manufacturing corporations </b>:'
            },
            {
              'label': 'To qualify as a small manufacturer, the corporation has to meet the following requirements:',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '- The adjusted business income of the corporation for the year is $250,000 or less; and ',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '- Its active business income from non-manufacturing and processing sources, including generating electrical energy for sale and producing steam for sale, cannot be greater than 20% of the corporation\'s adjusted business income. Active business income from manufacturing and processing sources includes income from farming, fishing, logging, or mining in Canada.',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If the corporation meets both requirements, its ECP is equal to its adjusted business income for the year. Enter the amount from line 050 on line 075 in Part 2 of this schedule. '
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'The corporation has met all the requirements to qualify as a small manufacturer',
              'type': 'infoField',
              'inputType': 'radio',
              'labelClass': 'tabbed',
              'labelWidth': '85%',
              'num': '051'
            }
          ]
        },
        {
          'header': 'Part 2 - Calculation of the Ontario tax credit for manufacturing and processing',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Eligible Canadian profits: </b>'
            },
            {
              'label': '<b> Note: </b> If the corporation is a small manufacturing corporation, enter the amount from line 050 from Part 1 of this schedule on line 075.',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Otherwise, complete the following calculation: ',
              'labelClass': 'tabbed2'
            },
            {
              'label': 'Canadian manufacturing and processing profits (amount from line 200 of Schedule 27)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '055'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'A1'
                  }
                }
              ]
            },
            {
              'label': 'Canadian profits from generating electrical  energy or producing steam for sale (amount from line 210 of Schedule 27 <b> minus </b> amount A1) ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '060'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'A2'
                  }
                }
              ]
            },
            {
              'label': 'Total Canadian farming, fishing, or logging income (amount from line 150 in Part 3)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '065'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'A3'
                  }
                }
              ]
            },
            {
              'label': 'Total Canadian mining income (amount from Line I in Part 4)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '070'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'A4'
                  }
                }
              ]
            },
            {
              'label': 'Total ECP (total of amounts A1, A2, A3, and A4)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '075',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '075'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'A'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Deduct: </b>'
            },
            {
              'label': 'Ontario adjusted small business income (amount from line I in Part 4 of Schedule 500, <i> Ontario Corporation Tax Calculation </i>)* ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '080'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'B'
                  }
                }
              ]
            },
            {
              'label': 'Excess (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '085'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '090'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'C'
            },
            {
              'label': 'Adjusted taxable income**',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '095'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'D'
                  }
                }
              ]
            },
            {
              'label': ' <b> Deduct: </b>'
            },
            {
              'label': '(For corporations resident in Canada, complete lines 1 to 3 as applicable. Otherwise, enter \'0\' on line E.)',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Ontario adjusted small business income (amount from line B)*',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '301'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '1'
                  }
                },
                null
              ]
            },
            {
              'type': 'table',
              'num': '600'
            },
            {
              'label': 'Aggregate investment income (amount from line 440 of the T2 Return)*',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '304'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '3'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal (total of lines 1, 2,  and 3)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '305'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '306'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'E'
                  }
                }
              ]
            },
            {
              'label': 'Excess (line D <b> minus </b> line E) (if negative, enter \'0\')',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '310'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '315'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'F'
            },
            {
              'label': 'Income eligible for the Ontario tax credit for manufacturing and processing:',
              'labelClass': 'bold'
            },
            {
              'label': '(least of amount from line 050 in Part 1, amount C, and amount F)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '320'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'G'
                  }
                }
              ]
            },
            {
              'labelClass': 'fulLLength'
            },
            {
              'label': '<b> Ontario tax credit for manufacturing and processing: </b>'
            },
            {
              'type': 'table',
              'num': '601'
            },
            {
              'type': 'table',
              'num': '602'
            },
            {
              'type': 'table',
              'num': '603'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Ontario tax credit for manufacturing and processing </b> (amount c <b> plus</b> amount d)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '500'
                  }
                },
                {
                  'input': {
                    'num': '505'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'H'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Enter amount H on line 406 of Schedule 5, <i> Tax Calculation Supplementary - Corporations. </i>',
              'labelClass': 'tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '*   Applies only to corporations that were Canadian-controlled private corporations throughout the tax year',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': '** Adjusted taxable income is equal to the corporation\'s taxable income for the year <b> plus </b> the amount of the corporation\'s adjusted Crown royalties for the year (amount from line 100 of Schedule 504) <b> minus </b> the amount of the corporation\'s notional resource allowance for the year (amount from line 105 of Schedule 504).',
              'labelClass': 'fullLength'
            },
            {
              'label': '*** Includes the offshore jurisdictions for Nova Scotia and Newfoundland and Labrador.',
              'labelClass': 'fullLength'
            },
            {
              'label': '**** If negative, enter "0"'
            },
            {
              'label': '***** If the corporation has a permanent establishment only in Ontario, enter the amount from line 360 from page 3 of the T2 return. Otherwise, enter the taxable income allocated to Ontario from column F in part 1 of Schedule 5.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Note: </b>'
            },
            {
              'label': 'If you are not a small manufacturing corporation as described in Part 1, complete parts 3 and 4 below.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 3 - Corporations that have income from farming, fishing, or logging in Canada',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'See notes below for instructions on how to complete this part.'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total Canadian farming, fishing, or logging income:'
            },
            {
              'type': 'table',
              'num': '300'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Enter the amount from line 150 on line A3 in Part 2.'
            }
          ]
        },
        {
          'header': 'Part 4 - Corporations that have income from mining in Canada',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'See notes below for instructions on how to complete this part.'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Mining profits under subparagraph 2i of subsection 33(7) of the <i> Taxation Act, 2007 </i> (Ontario)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '175',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '175'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Add: </b>'
            },
            {
              'label': 'complete the following calculation for industrial mining operations in Canada:*'
            },
            {
              'type': 'table',
              'num': '400'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total Canadian mining income (line 175 <b> plus </b> line 250). Enter this amount on line A4 in Part 2. ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '925'
                  }
                }
              ],
              'indicator': 'I'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* Do not include income reported on line 175. '
            }
          ]
        },
        {
          'hideFieldset': true,
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Notes: </b>'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'ADJUBI - Adjusted business income: enter the amount from line 130 in part 3 of Schedule 27, if filed. If Schedule 27 has not been filed, see Part 3 of that schedule to calculate adjusted business income.'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'CC – Cost of capital: See Part 4 of Schedule 27 to calculate the cost of capital for farming,' +
              ' fishing, logging, or industrial mining operations. For a farming operation, the cost of capital ' +
              'includes the cost of land, and/or the annual rental cost of the land, used by the corporation ' +
              'for its farming business.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'MCC- Cost of manufacturing and processing capital: See Part 5 of Schedule 27 to calculate the cost of manufacturing and processing capital for farming, fishing, logging, or industrial mining operations.'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'LC- Cost of labour: See Part 6 of Schedule 27 to calculate the cost of labour for farming, fishing, logging, or industrial mining operations.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'MLC - Cost of manufacturing and processing labour: See Part 7 of Schedule 27 to calculate the cost of manufacturing and processing labour for farming, fishing, logging, or industrial mining operations.',
              'labelClass': 'fullLength'
            }
          ]
        }
      ]
    };
})();
