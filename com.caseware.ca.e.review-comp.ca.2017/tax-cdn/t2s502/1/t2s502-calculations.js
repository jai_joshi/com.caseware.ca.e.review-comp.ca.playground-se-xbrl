(function() {

  wpw.tax.create.calcBlocks('t2s502', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      // Part 1 calculations
      calcUtils.getGlobalValue('line100S27', 'T2S27', '100');
      calcUtils.getGlobalValue('line120S27', 'T2S27', '120');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      if (field('line100S27').get()) {
        if (field('line120S27').get()) {
          calcUtils.equals('025', 'line120S27')
        }
        else {
          calcUtils.equals('025', 'line100S27')
        }
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('030', 'T2S504', '100');
      calcUtils.sumBucketValues('035', ['025', '030']);
      calcUtils.getGlobalValue('040', 'T2S504', '105');
      calcUtils.subtract('050', '035', '040');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      if (field('t2s27.090').get() == '1' && field('t2s27.091').get() != '2') {
        field('051').assign(1);
      }
      else {
        field('051').assign(2);
      }

      if (field('051').get() == '1') {
        calcUtils.removeValue(['055', '060', '065', '070'], true);
        calcUtils.equals('075', '050');
      }
      else {
        calcUtils.removeValue(['075'], true);
        calcUtils.getGlobalValue('055', 'T2S27', '200');
        calcUtils.getGlobalValue('A2', 'T2S27', '210');
        calcUtils.subtract('060', 'A2', '055');
        calcUtils.equals('065', '150');
        calcUtils.equals('070', '925');
        calcUtils.sumBucketValues('075', ['055', '060', '065', '070']);
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 2 calculations //
      calcUtils.getGlobalValue('080', 't2s500', '450');
      calcUtils.subtract('085', '075', '080');
      calcUtils.equals('090', '085');
      field('095').assign(
          field('T2J.360').get() +
          field('T2S504.100').get() -
          field('T2S504.105').get()
      );
      calcUtils.equals('301', '080');
      calcUtils.equals('801', '095');

      var taxableIncomeAllocation = calcUtils.getTaxableIncomeAllocation('ON');
      field('900').assign(taxableIncomeAllocation.provincialTI);
      field('802').assign(taxableIncomeAllocation.allProvincesTI);
      field('900').source(taxableIncomeAllocation.provincialTISourceField);
      field('802').source(taxableIncomeAllocation.allProvincesTISourceField);
      calcUtils.getGlobalValue('803', 'T2J', '371');
      field('901').assign(field('802').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();
      field('302').assign(field('801').get() * (1 - (field('802').get() / field('803').get())));
      calcUtils.getGlobalValue('304', 'T2J', '440');
      calcUtils.sumBucketValues('305', ['301', '302', '304']);
      calcUtils.equals('306', '305');
      calcUtils.subtract('310', '095', '306');
      calcUtils.equals('315', '310');
      calcUtils.min('320', ['050', '090', '315']);
      field('902').assign(field('901').get() == 0 ? 0 : field('900').get() / field('901').get());
      calcUtils.equals('915', '320');
      calcUtils.equals('916', '902');
      calcUtils.multiply('903', ['915', '916']);
      calcUtils.equals('904', '903');
      field('905').assign(0);
      field('907').assign(daysFiscalPeriod);
      field('906').assign(field('904').get() * field('905').get() / field('907').get() * 2 / 100);
      calcUtils.equals('908', '903');
      field('911').assign(daysFiscalPeriod);
      calcUtils.equals('909', '911');//cuz out version handle tax year after 01012012
      field('910').assign(field('911').get() == 0 ? 0 : field('908').get() * field('909').get() / field('911').get() * 15 / 1000);
      calcUtils.sumBucketValues('500', ['906', '910']);
      calcUtils.equals('505', '500');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // Part 3 calculations //
      if (field('120').get() + field('120').get() + field('140').get() + field('110').get() + field('130').get() > 0) {
        calcUtils.getGlobalValue('100', 'T2S27', '130');
      }
      else {
        field('100').assign(0);
      }
      if (field('110').get() + field('130').get() > 0) {
        field('150').assign(field('100').get() * (field('120').get() + field('140').get()) / (field('110').get() + field('130').get()));
      }
      else {
        field('150').assign(0);
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 4 calculations //
      if (field('175').get() + field('220').get() + field('220').get() + field('240').get() + field('210').get() + field('230').get() > 0) {
        calcUtils.getGlobalValue('200', 'T2S27', '130');
      }
      else {
        field('200').assign(0);
      }
      if (field('210').get() + field('230').get() > 0) {
        field('250').assign(field('200').get() * (field('220').get() + field('240').get()) / (field('210').get() + field('230').get()));
      }
      else {
        field('250').assign(0);
      }

      calcUtils.sumBucketValues('925', ['175', '250']);
    });

  });
})();
