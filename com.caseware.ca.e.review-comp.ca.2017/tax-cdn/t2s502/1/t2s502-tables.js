(function() {

  var adjubiCols = [
    {
      colClass: 'small-input-width',
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'std-padding-width',
      "type": "none"
    },
    {},
    {
      colClass: 'std-padding-width',
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'std-padding-width',
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'small-input-width',
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'std-padding-width',
      "type": "none"
    },
    {},
    {
      colClass: 'std-padding-width',
      cellClass: 'alignCenter',
      "type": "none"
    },
    {
      colClass: 'small-input-width',
      "type": "none"
    },
    {
      colClass: 'std-padding-width',
      "type": "none"
    },
    {},
    {
      colClass: 'std-padding-width',
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'small-input-width',
      "type": "none",
      cellClass: 'alignCenter'
    },
    {
      colClass: 'std-padding-width',
      "type": "none",
      cellClass: 'alignCenter'
    },
    {},
    {
      colClass: 'std-padding-width',
      "type": "none"
    }
  ];
  wpw.tax.global.tableCalculations.t2s502 = {
    "300": {
      "fixedRows": true,
      "infoTable": true,
      "columns": adjubiCols,
      "cells": [
        {
          "0": {
            "label": "ADJUBI"
          },
          "1": {
            "tn": "100"
          },
          "2": {
            "num": "100"
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          "3": {
            "label": "x"
          },
          "4": {
            "label": "[",
            "cellClass": "singleUnderline"
          },
          "5": {
            "label": "MCC",
            "cellClass": "singleUnderline"
          },
          "6": {
            "tn": "120",
            "cellClass": "singleUnderline"
          },
          "7": {
            "num": "120", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            "cellClass": "singleUnderline"
          },
          "8": {
            "label": " + ",
            "cellClass": "singleUnderline"
          },
          "9": {
            "label": "MLC",
            "cellClass": "singleUnderline"
          },
          "10": {
            "tn": "140",
            "cellClass": "singleUnderline"
          },
          "11": {
            "num": "140", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            "cellClass": "singleUnderline"
          },
          "12": {
            "label": "]",
            "cellClass": "singleUnderline"
          },
          "13": {
            "label": "="
          },
          "14": {
            "tn": "150"
          },
          "15": {
            "num": "150"
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          "2": {
            "type": "none"
          },
          "4": {
            "label": "["
          },
          "5": {
            "label": "CC"
          },
          "6": {
            "tn": "110"
          },
          "7": {
            "num": "110"
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          "8": {
            "label": "+"
          },
          "9": {
            "label": "LC"
          },
          "10": {
            "tn": "130"
          },
          "11": {
            "num": "130"
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          "12": {
            "label": "]"
          },
          "15": {
            "type": "none"
          }
        }]
    },
    "400": {
      "fixedRows": true,
      "infoTable": true,
      "columns": adjubiCols,
      "cells": [
        {
          "0": {
            "label": "ADJUBI"
          },
          "1": {
            "tn": "200"
          },
          "2": {
            "num": "200"
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          "3": {
            "label": "x"
          },
          "4": {
            "label": "[",
            "cellClass": "singleUnderline"
          },
          "5": {
            "label": "MCC",
            "cellClass": "singleUnderline"
          },
          "6": {
            "tn": "220",
            "cellClass": "singleUnderline"
          },
          "7": {
            "num": "220", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            "cellClass": "singleUnderline"
          },
          "8": {
            "label": "+",
            "cellClass": "singleUnderline"
          },
          "9": {
            "label": "MLC",
            "cellClass": "singleUnderline"
          },
          "10": {
            "tn": "240",
            "cellClass": "singleUnderline"
          },
          "11": {
            "num": "240", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            "cellClass": "singleUnderline"
          },
          "12": {
            "label": "]",
            "cellClass": "singleUnderline"
          },
          "13": {
            "label": " = "
          },
          "14": {
            "tn": "250"
          },
          "15": {
            "num": "250"
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          "2": {
            "type": "none"
          },
          "4": {
            "label": "["
          },
          "5": {
            "label": "CC "
          },
          "6": {
            "tn": "210"
          },
          "7": {
            "num": "210"
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          "8": {
            "label": "+"
          },
          "9": {
            "label": "LC"
          },
          "10": {
            "tn": "230"
          },
          "11": {
            "num": "230"
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          "12": {
            "label": "]"
          },
          "15": {
            "type": "none"
          }
        }]
    },
    "600": {
      "infoTable": true,
      "fixedRows": true,
      "columns": [
        {
          colClass: 'third-col-width',
          cellClass: 'alignLeft',
          "type": "none"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'third-col-width',
          cellClass: 'alignCenter',
          "type": "none"
        },
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      "cells": [{
        "0": {
          "label": "line D"
        },
        "1": {
          "num": "801",
          formField: true
        },
        "2": {
          "label": "x ( 1 - "
        },
        "3": {
          "label": "Taxable income earned in all provinces and territories ***",
          cellClass: 'singleUnderline alignCenter'
        },
        "4": {
          "num": "802",
          cellClass: 'singleUnderline',
          formField: true
        },
        "5": {
          "label": ") = "
        },
        "6": {
          "num": "302"
        },
        "7": {
          "label": "2****"
        }
      },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "Total taxable income ",
            "type": "none",
            "labelClass": "center"
          },
          "4": {
            "num": "803",
            formField: true
          },
          "6": {
            "type": "none"
          }
        }]
    },
    "601": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "width": "150px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "type": "none",
          "alignText": "centre"
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          "alignText": "right"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'small-input-width',
          "type": "none",
          "alignText": "right"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      "cells": [{
        "0": {
          "label": "Ontario domestic factor:",
          cellClass: 'alignCenter'
        },
        "1": {
          "label": "Ontario taxable income *****",
          cellClass: 'singleUnderline',
          "type": "none"
        },
        "3": {
          "num": "900",
          decimals: 2,
          formField: true,
          "type": "none"
        },
        "4": {
          "label": "="
        },
        "5": {
          "num": "902",
          decimals: 5,
          cellClass: 'doubleUnderline'
        },
        "6": {
          "label": "a"
        }
      },
        {
          "1": {
            "label": "Taxable income earned in all provinces and territories ***",
            "labelClass": "center"
          },
          "3": {
            "num": "901",
            formField: true
          },
          "5": {
            "type": "none"
          }
        }]
    },
    "602": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          colClass: 'std-input-width',
          "type": "none",
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Amount G"
          },
          "1": {
            "num": "915"
          },
          "2": {
            "label": "x Amount a"
          },
          "3": {
            "num": "916", decimals: 5
          },
          "4": {
            "label": "="
          },
          "5": {
            "num": "903",
            cellClass: 'doubleUnderline'
          },
          "6": {
            "label": "b"
          }
        }
      ]
    },
    "603": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          colClass: 'small-input-width',
          "type": "none",
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      "cells": [{
        "0": {
          "label": "Amount b"
        },
        "1": {
          "num": "904"
        },
        "2": {
          "label": "x"
        },
        "3": {
          "label": "Number of days in the tax year before July 1, 2011",
          cellClass: 'singleUnderline'
        },
        "4": {
          "num": "905",
          cellClass: 'singleUnderline'
        },
        "6": {
          "label": "x 2.0%= "
        },
        "7": {
          "num": "906",
          cellClass: 'singleUnderline'
        },
        "8": {
          "label": "c"
        }
      },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "4": {
            "num": "907"
          },
          "7": {
            "type": "none"
          }
        },
        {
          "0": {
            "label": "Amount b"
          },
          "1": {
            "num": "908"
          },
          "2": {
            "label": "x"
          },
          "3": {
            "label": "Number of days in the tax year after June 30, 2011",
            cellClass: 'singleUnderline'
          },
          "4": {
            "num": "909",
            cellClass: 'singleUnderline'
          },
          "6": {
            "label": "x 1.5%= "
          },
          "7": {
            "num": "910",
            cellClass: 'singleUnderline'
          },
          "8": {
            "label": "d"
          }
        },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "Number of days in the tax year",
            "labelClass": "center"
          },
          "4": {
            "num": "911"
          },
          "7": {
            "type": "none"
          }
        }]
    }
  }
})();
