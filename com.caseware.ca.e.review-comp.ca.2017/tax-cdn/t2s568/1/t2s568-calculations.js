(function() {

  wpw.tax.create.calcBlocks('t2s568', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      //Part 2 calcs
      if (field('200').get() == 1) {
        field('205').assign(field('315').get());
      }
      else if (field('200').get() == 2) {
        calcUtils.getGlobalValue('205', 'ratesOn', '806');
      }
      field('2071').assign(365);
      if ((field('CP.Days_Fiscal_Period').get() / 7) >= 51) {
        field('210').assign(field('205').get());
      }
      else {
        calcUtils.equals('206', '205');
        calcUtils.getGlobalValue('207', 'CP', 'Days_Fiscal_Period');
        field('209').assign(field('206').get() * (field('207').get() / field('2071').get()));
        calcUtils.equals('210', '209');
      }
    });
    // comment out for CRA; TODO: total shouldn't be in COR when it's only 1 row
    // calcUtils.calc(function(calcUtils, field, form) {
    //   var table = field('1100');
    //   var linkedTable = form('entityProfileSummary').field('568');
    //   table.getRows().forEach(function(row, rIndex) {
    //     row.forEach(function(cell, cIndex) {
    //       cell.assign(linkedTable.cell(rIndex, cIndex).get());
    //       cell.source(linkedTable.cell(rIndex, cIndex));
    //     })
    //   });
    // });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('500', 'ratesOn', '805');
      //part 4 calcs
      field('400').assign(calcUtils.numRepeatForms('T2S569'));
      calcUtils.setRepeatSummaryValue('405', 'T2S569', '310', 'add');
      calcUtils.equals('406', '210');
      calcUtils.min('410', ['405', '406']);
      calcUtils.multiply('411', ['410', '500'], 1 / 100);
    });
  });
})();
