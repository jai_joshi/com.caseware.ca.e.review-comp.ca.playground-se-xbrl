(function() {
  var columnWidth = '157px';

  wpw.tax.global.tableCalculations.t2s568 = {
    "1000": {
      "fixedRows": true,
      "infoTable": true,
      columns: [
        {
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          formField: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          width: '200px',
          type: 'none'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          formField: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          formField: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Amount A",
            cellClass: 'alignRight'
          },
          "1": {
            "num": "206"
          },
          '2':{label: 'x'},
          "3": {
            "label": "Days in the tax year ",
            cellClass: 'singleUnderline',
            "labelClass": "center"
          },
          "5": {
            "num": "207",
            cellClass: 'singleUnderline'
          },
          "6": {
            "label": " = ",
            "labelClass": "center"
          },
          "7": {
            type: 'none'
          },
          '8':{num: '209'},
          "9": {
            "label": "B"
          }
        },
        {
          '1':{type: 'none'},
          "2": {
            "type": "none"
          },
          "3": {
            "label": " 365 ",
            "labelClass": "center"
          },
          "5": {
            num: '2071'
          },
          '8':{type: 'none'},
          "9": {
            "type": "none"
          }
        }]
    },
    "1100": {
      fixedRows: true,
      "showNumbering": true,
      hasTotals: true,
      "maxLoop": 100000,
      "columns": [
        {
          "header": "Name of all associated corporations, including the filing corporation <br> (include the associated corporations that have a tax year that ends in the calendar year)",
          "num": "300",
          "tn": "300",
          "maxLength": 80,
          type: 'text'
        },
        {
          "header": "Business Number<br> (enter \"NR\" if corporation is not registered)",
          "num": "305",
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR'],
          "tn": "305",
          "maxLength": 80
        },
        {
          header: 'Expenditure limit',
          colClass: 'std-input-width'
        },
        {
          "header": "Expenditure limit allocated",
          "width": columnWidth,
          "num": "310",
          "tn": "310",
          "maxLength": 80,
          "total": true,
          "totalNum": "315",
          totalTn: '315',
          totalIndicator: 'D',
          totalMessage: 'Total expenditure limit (cannot exceed $20 million) '
        }
      ]
    },
    "1200": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "width": "370px",
          "type": "none",
          cellClass: 'alignLeft'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "type": "none"
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        }],
      "cells": [{
        "0": {
          "label": "Ontario business-research institute tax credit (line 410 x "
        },
        "1": {
          "num": "500"
        },
        "2": {
          "label": "%=",
          "labelClass": "center"
        },
        "4": {
          "num": "411",
          cellClass: 'doubleUnderline'
        },
        "5": {
          "label": " G ",
          "labelClass": "center"
        }
      }]
    }
  }
})();
