(function() {
  'use strict';

  wpw.tax.global.formData.t2s568 = {
    'formInfo': {
      'abbreviation': 'T2S568',
      'title': 'Ontario Business-Research Institute Tax Credit',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      //'subTitle': '(2009 and later tax years)',
      'schedule': 'Schedule 568',
      'showCorpInfo': true,
      code: 'Code 0902',
      formFooterNum: 'T2 SCH 568 E (10)',
      headerImage: 'canada-federal',
      'description': [
        {
          'type': 'list',
          'items': [
            {
              label: ' Use this schedule to claim the Ontario business-research ' +
              'institute tax credit (OBRITC) under section 97 of the<i> Taxation Act, 2007</i> (Ontario).'
            },
            {
              label: 'The OBRITC is a 20% refundable tax credit based on qualified expenditures ' +
              'incurred in Ontario under an eligible contract with an eligible research institute (ERI).'
            },
            {
              label: 'A list of eligible research institutes and the applicable ERI codes for ' +
              'eligible contracts can be found on our web site. Go to<b> www.cra.gc.ca/ctao</b> and ' +
              'select "business-research institute tax credit".'
            },
            {
              label: 'The criteria for a corporation to be eligible for the OBRITC ' +
              'include the eligibility requirements in Part 1 of this schedule.'
            },
            {
              label: 'The annual qualified expenditure limit is $20 million. If a corporation ' +
              'is associated with other corporations at any time in the calendar year, the $20 ' +
              'million limit must be allocated among the associated corporations.'
            },
            {
              label: ' Qualifying corporations are defined in subsection 97(3) of the ' +
              '<i>Taxation Act,</i> 2007 (Ontario).'
            },
            {
              label: 'For each eligible contract, you must complete a separate ' +
              'Schedule 569,<i> Ontario Business-Research Institute Tax Credit Contract Information</i>'
            },
            {
              label: 'Keep the eligible contract to support your claim. Do not submit the ' +
              'contract with the<i> T2 Corporation Income Tax Return</i>'
            },
            {
              label: ' To claim the OBRITC, include the following with the<i> T2 Corporation Income Tax Return:</i>',
              sublist: [
                'a completed copy of this schedule; and',
                'a completed copy of Schedule 569 for each eligible contract.'
              ]
            }
          ]
        }
      ],
      category: 'Ontario Forms'
    },
    'sections': [
      {
        header: 'Part 1 - Eligibility',
        rows: [
          {labelClass: 'fullLength'},
          {
            label: '1. Did the corporation, for the tax year, carry on business in Ontario ' +
            'through a permanent establishment in Ontario?',
            type: 'infoField',
            inputType: 'radio',
            num: '100',
            tn: '100',
            init: '2',
            labelClass: 'fullLength',
            labelWidth: '80%'
          },
          {
            label: '2. Was the corporation exempt from tax for the tax year under Part III ' +
            'of the<i> Taxation Act, 2007</i> (Ontario)?',
            type: 'infoField',
            inputType: 'radio',
            num: '105',
            tn: '105',
            init: '2',
            labelClass: 'fullLength',
            labelWidth: '80%'
          },
          {labelClass: 'fullLength'},
          {
            label: 'If you answered<b> no </b>to question 1 or <b>yes</b> to question 2, the corporation ' +
            'is <b>not eligible</b> for the OBRITC',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'Part 2 -  Qualified expenditure limit for the tax year',
        rows: [
          {
            'label': 'Was the corporation associated at any time in the tax year with another corporation?',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '200',
            'tn': '200',
            'init': '2',
            'labelClass': 'fullLength',
            'labelWidth': '80%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the corporation answered <b>no</b> at line 200, enter $20,000,000 on line 205. If the corporation answered <b>yes </b>at line 200, complete Part 3 and enter on line 205 the expenditure limit allocated to the corporation in column 310 in Part 3.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Qualified expenditure limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '205',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '205'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'A'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the tax year is 51 weeks or more, enter amount A on line 210.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the tax year of the filing corporation is less than 51 weeks, complete the following proration calculation:'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'label': '<b>Qualified expenditure limit for the tax year </b>(amount A or amount B, whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '210',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '210'
                }
              }
            ],
            'indicator': 'C'
          }
        ]
      },
      {
        header: 'Part 3 - Allocation of the $20 million expenditure limit between associated corporations',
        rows: [
          {
            label: 'Use this part to allocate the $20 million expenditure limit to the ' +
            'filing corporation and all its associated corporations for each of their tax years ' +
            'ending in the calendar year. See subsection 38(4) of Ontario Regulation 37/09 for ' +
            'expenditure limit allocation rules for associated corporations. Attach additional ' +
            'schedules if you need more space.',
            labelClass: 'fullLength'
          },
          {
            type: 'table', num: '1100'
          },
          {
            label: 'Enter the expenditure limit allocated to the corporation on line 205 in Part 2.'
          }
        ]
      },
      {
        header: 'Part 4 - Calculation of the Ontario business-research institute tax credit',
        rows: [
          {
            'label': 'Total number of eligible contracts used to determine the OBRITC for this tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '400',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,3}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '400'
                }
              }
            ]
          },
          {
            'label': 'Total qualified expenditures for all eligible contracts identified on line 400 for this tax year ' +
            '<br>(total of amounts on line 310 in Part 3 of each<b> Schedule 569</b>)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '405',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '405'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'label': 'Qualified expenditure limit for the tax year (amount C in Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '406',
                  'disabled': true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F'
                }
              }
            ]
          },
          {
            'label': 'Qualified expenditures for the OBRITC for the tax year (amount E or F, whichever is less)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '410',
                  'disabled': true,
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '410'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '1200'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter amount G on line 470 of Schedule 5, <i>Tax Calculation Supplementary - Corporations.</i>'
          }
        ]
      }
    ]
  };
})();
