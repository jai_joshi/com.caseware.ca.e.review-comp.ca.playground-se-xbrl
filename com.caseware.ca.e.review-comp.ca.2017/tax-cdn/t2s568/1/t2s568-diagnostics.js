(function() {
  wpw.tax.create.diagnostics('t2s568', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S568'), forEach.row('1100', forEach.bnCheckCol(1, true))));

    diagUtils.diagnostic('5680005', common.prereq(common.check('t2s5.470', 'isNonZero'), common.requireFiled('T2S568')));

    diagUtils.diagnostic('5680010', common.prereq(common.and(
        common.requireFiled('T2S568'),
        common.check('410', 'isNonZero')),
        common.check('100', 'isNonZero')));

    diagUtils.diagnostic('5680012', common.prereq(common.and(
        common.requireFiled('T2S568'),
        common.check(['410'], 'isNonZero')),
        common.check('105', 'isNonZero')));

    diagUtils.diagnostic('5680015', common.prereq(common.and(
        common.requireFiled('T2S568'),
        common.check('t2s5.470', 'isNonZero')),
        common.check('200', 'isNonZero')));

    diagUtils.diagnostic('5680025', common.prereq(common.and(
        common.requireFiled('T2S568'),
        common.and(
            common.check('t2s5.470', 'isNonZero'),
            common.check('200', 'isYes'))),
        function(tools) {
          var table = tools.field('1100');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireAll([row[0], row[1], row[3]], 'isNonZero');
          });
        }));

    //5680030 is an exact duplicate of 5680025

    diagUtils.diagnostic('568.315', common.prereq(common.requireFiled('T2S568'),
        function(tools) {
          return tools.field('315').require(function() {
            return this.get() <= 20000000;
          });
        }));
  });
})();
