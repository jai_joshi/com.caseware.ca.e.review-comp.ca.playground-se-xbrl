(function () {
  wpw.tax.create.formData('t2s366', {
    formInfo: {
      abbreviation: 't2s366',
      title: 'New Brunswick Corporation Tax Calculation',
      schedule: 'Schedule 366',
      headerImage: 'canada-federal',
      showCorpInfo: true,
      category: 'New Brunswick Forms',
      formFooterNum: 'T2 SCH 366 E (17)',
      description: [
        {
          'text': '• Use this schedule if your corporation had a permanent establishment (as defined in section ' +
          '400 of the federal <i>Income Tax Regulations</i>) in New Brunswick, and had taxable income earned in the ' +
          'year in New Brunswick.'
        },
        {
          'text': '• This schedule is a worksheet only and does not have to be filed with your <i>T2 Corporation ' +
          'Income Tax Return</i>'
        }
      ]
    },
    sections: [
      {
        'header': 'Part 1 - Income subject to New Brunswick lower and higher tax rates',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable income for New Brunswick *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Income eligible for the New Brunswick lower tax rate:</b>',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount from line 400 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 405 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '110'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 427 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '115'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount B, C, or D, whichever is the least',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '120'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '125'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Income subject to New Brunswick higher tax rate</b> (amount A <b>minus</b> amount F)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Enter amount F and/or amount G on the applicable line(s) in Part 2',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* If the corporation has a permanent establishment only in New Brunswick, enter the taxable income from line 360 of the T2 return. Otherwise, enter the taxable income allocated to New Brunswick from column F in Part 1 of Schedule 5, <i>Tax Calculation Supplementary – Corporations</i>.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** Includes the territories and the offshore jurisdictions for Nova Scotia and Newfoundland and Labrador.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - New Brunswick tax before credits',
        'rows': [
          {
            'label': '<b>New Brunswick tax at the lower rate: </b>',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total New Brunswick tax at the lower rate (amount H <b>plus</b> amount I <b>plus</b> I1)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '233'
                }
              },
              {
                'input': {
                  'num': '236'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>New Brunswick tax at the higher rate: </b>',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          // {
          //   'type': 'table',
          //   'num': '2390'
          // },
          {
            type: 'table',
            num: '239'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total New Brunswick tax at the higher rate (amount K <b>plus</b> amount L)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '270'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '272'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>New Brunswick tax before credits</b> (line J <b>plus</b> line K) *.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '275'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* If the corporation has a permanent establishment in more than one jurisdiction or is claiming a New Brunswick tax credit, enter amount L on line 225 of Schedule 5. Otherwise, enter it on line 760 of the T2 return.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  })
})();
