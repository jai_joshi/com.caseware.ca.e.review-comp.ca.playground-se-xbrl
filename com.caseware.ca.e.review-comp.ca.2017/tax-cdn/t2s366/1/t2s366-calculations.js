(function () {

  function runTableRateCalcs(table, rIndex) {
    table.cell(rIndex, 10).assign(
      table.cell(rIndex, 2).get() *
      table.cell(rIndex, 6).get() /
      table.cell(rIndex + 1, 6).get() *
      table.cell(rIndex, 8).get() / 100);
  }

  wpw.tax.create.calcBlocks('t2s366', function (calcUtils) {

    calcUtils.calc(function (calcUtils, field) {
      var table125 = field('125');
      var table200 = field('2000');
      var table239 = field('239');

      table200.cell(0, 8).assign(field('ratesNb.1009').cell(11, 6).get());
      table200.cell(0, 8).source(field('ratesNb.1009').cell(11, 6));
      table200.cell(2, 8).assign(field('ratesNb.1009').cell(12, 6).get());
      table200.cell(2, 8).source(field('ratesNb.1009').cell(12, 6));
      table200.cell(4, 8).assign(field('ratesNb.1009').cell(13, 6).get());
      table200.cell(4, 8).source(field('ratesNb.1009').cell(13, 6));
      table239.cell(0, 8).assign(field('ratesNb.1009').cell(11, 4).get());
      table239.cell(0, 8).source(field('ratesNb.1009').cell(11, 4));
      table239.cell(2, 8).assign(field('ratesNb.1009').cell(12, 4).get());
      table239.cell(2, 8).source(field('ratesNb.1009').cell(12, 4));

      field('291').assign(field('ratesNb.1009').cell(12, 4).get());
      field('291').source(field('ratesNb.1009').cell(12, 4));

      var taxableIncomeAllocation = calcUtils.getTaxableIncomeAllocation('NB');
      field('100').assign(taxableIncomeAllocation.provincialTI);
      field('100').source(taxableIncomeAllocation.provincialTISourceField);
      table125.cell(1, 6).assign(taxableIncomeAllocation.allProvincesTI);
      table125.cell(0, 6).assign(field('100').get());

      calcUtils.getGlobalValue('105', 'T2J', '400');
      calcUtils.getGlobalValue('110', 'T2J', '405');
      calcUtils.getGlobalValue('115', 'T2J', '427');

      field('120').assign(Math.min(
        field('105').get(),
        field('110').get(),
        field('115').get()));

      table125.cell(0, 2).assign(field('120').get());
      table125.cell(0, 8).assign(table125.cell(0, 2).get() * table125.cell(0, 6).get() / table125.cell(1, 6).get());

      field('150').assign(field('100').get() - table125.cell(0, 8).get());

      var date2016 = wpw.tax.date(2016, 4, 1);
      var date2017 = wpw.tax.date(2017, 4, 1);
      var date2016Comparisons = calcUtils.compareDateAndFiscalPeriod(date2016);
      var date2017Comparisons = calcUtils.compareDateAndFiscalPeriod(date2017);
      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();
      //from rate table

      //days fiscal period
      table200.cell(1, 6).assign(daysFiscalPeriod);
      table200.cell(3, 6).assign(daysFiscalPeriod);
      table200.cell(5, 6).assign(daysFiscalPeriod);
      table239.cell(1, 6).assign(daysFiscalPeriod);
      table239.cell(3, 6).assign(daysFiscalPeriod);
      //days in tax year
      table200.cell(0, 6).assign(date2016Comparisons.daysBeforeDate);
      table200.cell(2, 6).assign(date2016Comparisons.daysAfterDate - date2017Comparisons.daysAfterDate);
      table200.cell(4, 6).assign(date2017Comparisons.daysAfterDate);
      table239.cell(0, 6).assign(date2016Comparisons.daysBeforeDate);
      table239.cell(2, 6).assign(date2016Comparisons.daysAfterDate);
      //amount F
      var amountF = table125.cell(0, 8).get();
      table200.cell(0, 2).assign(amountF);
      table200.cell(2, 2).assign(amountF);
      table200.cell(4, 2).assign(amountF);
      //amount G
      var amountG = field('150').get();
      table239.cell(0, 2).assign(amountG);
      table239.cell(2, 2).assign(amountG);
      //rate amount calcs
      runTableRateCalcs(table200, 0);
      runTableRateCalcs(table200, 2);
      runTableRateCalcs(table200, 4);
      field('233').assign(table200.cell(0, 10).get() + table200.cell(2, 10).get() + table200.cell(4, 10).get());
      field('236').assign(field('233').get());

      runTableRateCalcs(table239, 0);
      runTableRateCalcs(table239, 2);

      calcUtils.equals('290', '150');

      field('292').assign(field('290').get() * field('291').get() / 100);

      field('270').assign(field('239').cell(0, 10).get() + field('239').cell(2, 10).get());
      field('272').assign(field('270').get());
      field('275').assign(field('236').get() + field('272').get());
    });
  });
})();
