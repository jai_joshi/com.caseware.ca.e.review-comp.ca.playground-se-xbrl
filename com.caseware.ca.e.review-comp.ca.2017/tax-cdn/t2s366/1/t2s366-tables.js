(function() {

  function getTableTaxInc(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {type: 'none', colClass: 'std-padding-width'},
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {

          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '2': {num: labelsObj.num[0]},
          '3': {label: 'x'},
          '4': {
            label: labelsObj.label[1],
            labelClass: 'center',
            cellClass: 'singleUnderline'
          },
          '6': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '7': {label: '='},
          '8': {num: labelsObj.num[2]},
          '9': {label: labelsObj.indicator || ''}
        },
        {
          '2': {type: 'none'},
          '4': {
            label: labelsObj.label[2],
            labelClass: 'center fullLength'
          },
          '6': {num: labelsObj.num[3]},
          '8': {type: 'none'}
        }
      ]
    }
  }

  function getTableRateColumns() {
    return [
      {
        colClass: 'std-input-width',
        type: 'none',
        cellClass: 'alignCenter'
      },
      {
        colClass: 'std-spacing-width',
        type: 'none'
      },
      {
        colClass: 'std-input-width'
      },
      {
        colClass: 'std-padding-width',
        type: 'none',
        cellClass: 'alignCenter'
      },
      {
        type: 'none',
        cellClass: 'alignCenter'
      },
      {
        colClass: 'std-spacing-width',
        type: 'none'
      },
      {
        colClass: 'small-input-width'
      },
      {
        colClass: 'std-padding-width',
        type: 'none',
        cellClass: 'alignCenter'
      },
      {
        colClass: 'small-input-width'
      },
      {
        colClass: 'std-padding-width',
        type: 'none'
      },
      {
        colClass: 'std-input-width'
      },
      {
        type: 'none',
        colClass: 'std-padding-width'
      },
      {
        colClass: 'std-input-width',
        'type': 'none'
      },
      {
        colClass: 'std-padding-width',
        'type': 'none'
      }
    ]
  }

  function getTableRateRows(tableRowObjArr) {
    var tableRows = [];

    tableRowObjArr.forEach(function(tableRowObj) {
      tableRows.push(
          {
            '0': {
              label: tableRowObj.label[0]
            },
            '3': {
              label: 'x',
              labelClass: 'center'
            },
            '4': {
              label: tableRowObj.label[1],
              labelClass: 'center',
              cellClass: 'singleUnderline'
            },
            '6': {
              cellClass: 'singleUnderline'
            },
            '7': {
              label: tableRowObj.isNoRate ? '' : 'x',
              labelClass: 'center'
            },
            '8': {
              type: tableRowObj.isNoRate ? 'none' : '',
              decimals: 1
            },
            '9': {
              label: tableRowObj.isNoRate ? '=' : '%='
            },
            '11': {
              label: tableRowObj.label[2]
            }
          },
          {
            '2': {
              type: 'none'
            },
            '4': {
              label: tableRowObj.label[3],
              labelClass: 'center'
            },
            '8': {
              type: 'none'
            },
            '10': {
              type: 'none'
            }
          }
      )
    });
    return tableRows
  }

  wpw.tax.global.tableCalculations.t2s366 = {
    '125': getTableTaxInc({
      label: ['Amount E', 'Taxable income for New Brunswick *', 'Taxable income for all provinces **'],
      indicator: 'F'
    }),
    '2000': {
      fixedRows: true,
      infoTable: true,
      columns: getTableRateColumns(),
      cells: getTableRateRows([
        {label: ['Amount F', 'Number of days in the tax year before April 1, 2016', 'H', 'Number of days in the tax year']},
        {label: ['Amount F', 'Number of days in the tax year before April 1, 2017', 'I', 'Number of days in the tax year']},
        {label: ['Amount F', 'Number of days in the tax year after March 31, 2017', 'I1', 'Number of days in the tax year']}
      ])
    },
    '2390': {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "colClass": "std-input-width",
          "type": "none",
          "cellClass": "alignCenter"
        },
        {
          "colClass": "std-input-width"
        },
        {
          "colClass": "std-padding-width",
          "type": "none",
          "cellClass": "alignCenter"
        },
        {
          "colClass": "small-input-width"
        },
        {
          "colClass": "small-input-width",
          "type": "none",
          "cellClass": "alignCenter"
        },
        {
          "type": "none"
        },
        {
          "colClass": "std-input-width"
        },
        {
          "type": "none",
          "colClass": "std-padding-width"
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Amount G"
          },
          "1": {num: '290'},
          "2": {
            "label": " x ",
            "labelClass": "center"
          },
          "3": {num: '291'},
          "4": {
            "label": " %= "
          },
          "6": {num: '292'},
          "7": {
            "label": "K"
          }
        }
      ]
    },
    '239': {
      fixedRows: true,
      infoTable: true,
      columns: getTableRateColumns(),
      cells: getTableRateRows([
        {label: ['Amount G', 'Number of days in the tax year before April 1, 2016', 'K', 'Number of days in the tax year']},
        {label: ['Amount G', 'Number of days in the tax year after March 31, 2016', 'L', 'Number of days in the tax year']}
      ])
    }
  }
})();
