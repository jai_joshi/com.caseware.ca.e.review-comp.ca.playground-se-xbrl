(function() {
  wpw.tax.create.diagnostics('t2s141', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    //No prereq required as this field is always mandatory
    diagUtils.diagnostic('1410002', common.check('101', 'isNonZero'));

    diagUtils.diagnostic('1410003', common.prereq(common.and(
        common.requireFiled('T2S141'),
        common.check('101', 'isYes')),
        function(tools) {
          return tools.requireAll(tools.list(['104', '105', '106', '107']), 'isNonZero');
        }));

    //No prereq required as this field is always mandatory
    diagUtils.diagnostic('1410004', common.check('108', 'isNonZero'));

    diagUtils.diagnostic('1410006', common.prereq(common.and(
        common.requireFiled('T2S141'),
        common.and(
            common.check('095', 'isYes'),
            common.check('097', 'isNo'))),
        common.check('198', 'isNonZero')));

    diagUtils.diagnostic('1410007', common.prereq(common.and(
        common.requireFiled('T2S141'),
        function(tools) {
          return tools.field('198').get() == '1' || tools.field('198').get() == '2';
        }), common.check('099', 'isNonZero')));

  });
})();
