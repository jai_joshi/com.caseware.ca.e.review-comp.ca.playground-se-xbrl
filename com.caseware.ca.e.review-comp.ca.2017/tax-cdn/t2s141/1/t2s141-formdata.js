(function() {
  'use strict';

  wpw.tax.global.formData.t2s141 = {
    formInfo: {
      abbreviation: 't2s141',
      title: 'Notes Checklist',

      //subTitle: '(2010 and later tax years)',
      schedule: 'Schedule 141',
      category: 'GIFI',
      code: 'Code 1002',
      formFooterNum: 'T2 SCH 141 E (14)',
      description: [
        {
          'type': 'list',
          'items': [
            'Parts 1, 2, and 3 of this schedule must be completed from the perspective of the person ' +
            '(referred to in these parts as the <b>accountant</b>) who prepared or reported on the financial statements. ' +
            'If the person preparing the tax return is not the accountant referred to above, ' +
            'they must still complete Parts 1, 2, 3, and 4, as applicable.',
            'For more information, see Guide RC4088, <i>General Index of Financial Information (GIFI)</i> and T4012, <i>T2 Corporation – Income Tax Guide.</i>',
            'Complete this schedule and include it with your T2 return along with the other GIFI schedules.'
          ]
        }
      ]
    },
    sections: [
      {
        header: 'Part 1 - Information on the accountant who prepared or reported on the financial statements',
        rows: [
          {
            type: 'infoField',
            inputType: 'radio',
            num: '095',
            tn: '095',
            label: 'Does the accountant have a professional designation',
            labelWidth: '70%'
          },
          {
            type: 'infoField',
            inputType: 'radio',
            num: '097',
            tn: '097',
            label: 'Is the accountant connected * with the corporation?',
            labelWidth: '70%'
          },
          {label: 'Note', labelClass: 'bold'},
          {
            label: 'If the accountant does not have a professional designation or is connected to the ' +
            'corporation, you do not have to complete Parts 2 and 3 of this schedule. ' +
            'However, you do have to complete Part 4, as applicable. ',
            labelClass: 'fullLength'
          },
          {
            label: '* A person connected with a corporation can be: (i) a shareholder of the corporation who ' +
            'owns more than 10% of the common shares; (ii) a director, an officer, or an employee ' +
            'of the corporation; or (iii) a person not dealing at arm\'s length with the corporation',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'Part 2 - Type of involvement with the financial statements',
        rows: [
          {
            type: 'splitInputs', inputType: 'radio', num: '198', tn: '198', divisions: '1',
            label: 'Choose the option that represents the highest level of involvement of the accountant:',
            items: [
              {label: 'Completed an auditor\'s report', value: '1'},
              {label: 'Completed a review engagement report', value: '2'},
              {label: 'Conducted a compilation engagement', value: '3'}
            ]
          }
        ]
      },
      {
        header: 'Part 3 - Reservations',
        rows: [
          {
            label: 'If you selected option 1 or 2 under <b>Type of involvement with the financial statements</b> above,' +
            ' answer the following question:',
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            inputType: 'radio',
            num: '099',
            tn: '099',
            label: 'Has the accountant expressed a reservation?',
            labelWidth: '70%'
          }
        ]
      },
      {
        header: 'Part 4 - Other information',
        rows: [
          {
            type: 'splitInputs', inputType: 'radio', num: '110', tn: '110', divisions: '1',
            label: 'If you have a professional designation and are not the accountant associated with the financial' +
            ' statements in Part 1 above, choose one of the following options:',
            items: [
              {label: 'Prepared the tax return (financial statements prepared by client)', value: '1'},
              {
                label: 'Prepared the tax return and the financial information contained therein (financial' +
                ' statements have not been prepared)',
                value: '2'
              }
            ]
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            num: '101',
            tn: '101',
            label: 'Were notes to the financial statements prepared?',
            labelWidth: '70%'
          },
          {label: 'If <b>yes</b>, complete lines 104 to 107 below:', labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            num: '104',
            tn: '104',
            label: 'Are subsequent events mentioned in the notes?',
            labelWidth: '70%',
            labelClass: 'tabbed',
            canClear: true
          },
          {
            type: 'infoField',
            inputType: 'radio',
            num: '105',
            tn: '105',
            label: 'Is re-evaluation of asset information mentioned in the notes?',
            labelWidth: '70%',
            labelClass: 'tabbed',
            canClear: true
          },
          {
            type: 'infoField',
            inputType: 'radio',
            num: '106',
            tn: '106',
            label: 'Is contingent liability information mentioned in the notes?',
            labelWidth: '70%',
            labelClass: 'tabbed',
            canClear: true
          },
          {
            type: 'infoField',
            inputType: 'radio',
            num: '107',
            tn: '107',
            label: 'Is information regarding commitments mentioned in the notes?',
            labelWidth: '70%',
            labelClass: 'tabbed',
            canClear: true
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            inputType: 'radio',
            num: '108',
            tn: '108',
            label: 'Does the corporation have investments in joint venture(s) or partnership(s)?',
            labelWidth: '70%'
          },

          {labelClass: 'fullLength'},
          {label: 'Impairment and fair value changes', labelClass: 'fullLength bold'},
          {
            type: 'infoField', inputType: 'radio', num: '200', tn: '200', labelWidth: '70%',
            label: 'In any of the following assets, was an amount recognized in net income or other' +
            ' comprehensive income (OCI) as a result of an impairment loss in the tax year, a reversal' +
            ' of an impairment loss recognized in a previous tax year, or a change in fair value during the tax year?'
          },
          {label: 'If <b>yes</b>, enter the amount recognized: ', labelClass: 'fullLength'},
          {
            type: 'table', num: '205'
          },
          {labelClass: 'fullLength'},
          {label: 'Financial instruments', labelClass: 'fullLength bold'},
          {
            type: 'infoField',
            inputType: 'radio',
            num: '250',
            tn: '250',
            label: 'Did the corporation derecognize any financial instrument(s) during the tax year' +
            ' (other than trade receivables)?',
            labelWidth: '70%'
          },
          {
            type: 'infoField',
            inputType: 'radio',
            num: '255',
            tn: '255',
            label: 'Did the corporation apply hedge accounting during the tax year?',
            labelWidth: '70%'
          },
          {
            type: 'infoField',
            inputType: 'radio',
            num: '260',
            tn: '260',
            label: 'Did the corporation discontinue hedge accounting during the tax year?',
            labelWidth: '70%'
          },

          {labelClass: 'fullLength'},
          {label: 'Adjustments to opening equity', labelClass: 'fullLength bold'},
          {
            type: 'infoField', inputType: 'radio', num: '265', tn: '265', labelWidth: '70%',
            label: 'Was an amount included in the opening balance of retained earnings or equity, in order' +
            ' to correct an error, to recognize a change in accounting policy, or to adopt a new accounting' +
            ' standard in the current tax year?'
          },
          {label: 'If <b>yes</b>, you have to maintain a separate reconciliation.', labelClass: 'fullLength tabbed'}
        ]
      }
    ]
  };
})();
