(function() {
  wpw.tax.global.tableCalculations.t2s141 = {
    "205": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [{
        "header": "",
        "type": "none",
        "width": "30%"
      },
        {
          "header": "",
          "type": "none"
        },
        {
          width: '25%',
          "header": "<b>In net income</b> <br> increase(decrease)"
        },
        {
          "header": "",
          "type": "none",
            colClass: 'std-padding-width'
        },
        {
          "header": "",
          "type": "none"
        },
        {
          width: '25%',
          "header": "<b>In OCI</b> <br> Increase(decrease)"
        },
        {
          "header": "",
          "type": "none",
          "width": "25%"
        }],
      "cells": [{
        "0": {
          "label": "Property, plant, equipment"
        },
        "1": {

        },
        "2": {
          "tn": "210",
          "saveNum": true,
          "num": "210", "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        },
        "4": {
        },
        "5": {
          "tn": "211",
          "saveNum": true,
          "num": "211", "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
        }
      },
        {
          "0": {
            "label": "Intangible assets"
          },
          "1": {
          },
          "2": {
            "tn": "215",
            "saveNum": true,
            "num": "215", "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          },
          "4": {
          },
          "5": {
            "tn": "216",
            "saveNum": true,
            "num": "216", "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          }
        },
        {
          "0": {
            "label": "Investment property"
          },
          "1": {
          },
          "2": {
            "tn": "220",
            "saveNum": true,
            "num": "220", "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          },
          "4": {},
          "5": {
            "type": "none"
          }
        },
        {
          "0": {
            "label": "Biological assets"
          },
          "1": {
          },
          "2": {
            "tn": "225",
            "saveNum": true,
            "num": "225", "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          },
          "4": {},
          "5": {
            "type": "none"
          }
        },
        {
          "0": {
            "label": "Financial instruments"
          },
          "1": {
          },
          "2": {
            "tn": "230",
            "saveNum": true,
            "num": "230", "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          },
          "4": {
          },
          "5": {
            "tn": "231",
            "saveNum": true,
            "num": "231", "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          }
        },
        {
          "0": {
            "label": "Other"
          },
          "1": {
          },
          "2": {
            "tn": "235",
            "saveNum": true,
            "num": "235", "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          },
          "4": {
          },
          "5": {
            "tn": "236",
            "saveNum": true,
            "num": "236", "validate": {"or":[{"matches":"^-?[.\\d]{1,13}$"},{"check":"isEmpty"}]}
          }
        }]
    }
  }
})();
