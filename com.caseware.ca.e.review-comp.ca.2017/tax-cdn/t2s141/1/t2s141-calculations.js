(function() {
  function enableNum(field, numArray) {
    for (var i = 0; i < numArray.length; i++) {
      field(numArray[i]).disabled(false);
    }
  }

  wpw.tax.create.calcBlocks('t2s141', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {

      if (field('095').get() == 1 && field('097').get() == 2) {
        enableNum(field, ['198', '099']);
      }
      else {
        field('198').disabled(true);
        field('099').disabled(true);
        if (calcUtils.hasChanged('095') || calcUtils.hasChanged('097')) {
          calcUtils.removeValue(['198', '099'], true);
        }
      }

      var line104to107 = ['104', '105', '106', '107'];
      if (field('101').get() == 2 && calcUtils.hasChanged('101')) {
        calcUtils.removeValue(line104to107, true);
      }
      else {
        enableNum(field, line104to107);
      }
    });
  });
})();
