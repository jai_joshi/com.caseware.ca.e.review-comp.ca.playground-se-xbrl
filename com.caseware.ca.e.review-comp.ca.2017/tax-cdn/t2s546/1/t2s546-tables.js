(function() {
  var countryAddressCodes = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.countryAddressCodes, 'value');

  wpw.tax.global.tableCalculations.t2s546 = {

    "105": {
      "fixedRows": true,
      "infoTable": true,
      "dividers": [1,
        2],
      "columns": [
        {
          "type": "none",
          "width": "30%"
        },
        {
          "type": "date",
          cellClass: 'alignCenter'
        },
        {
          "width": "20%",
          cellClass: 'alignCenter'
        }],
      "cells": [
        {
          "0": {
            "label": "Jurisdiction incorporated, continued, or amalgamated, whichever is the most recent <br><br><b>Ontario</b>",
            "labelClass": "center"
          },
          "1": {
            "num": "110",
            "label": "Date of incorporation or amalgamation, whichever is the most recent",
            "tn": "110"
          },
          "2": {
            "num": "120",
            "validate": {"or":[{"matches": "^-?[.\\d]{2,9}$"},{"check":"isEmpty"}]},
            "label": "Ontario Corporation No.",
            "tn": "120",
            "maxLength": "9",
            "validator": "required",
            type: 'text'
          }
        }]
    },
    "195": {
      "infoTable": true,
      "fixedRows": true,
      "columns": [{type: "text"}],
      "cells": [{
        "0": {
          "num": "200", "validate": {"or":[{"length":{"min":"1","max":"42"}},{"check":"isEmpty"}]},
          "rf": true,
          "label": "Care of (if applicable)",
          "tn": "200",
          "maxLength": "42"
        }
      }]
    },
    "205": {
      "infoTable": true,
      "fixedRows": true,
      "dividers": [1, 2],
      "columns": [
        {
          cellClass: 'alignCenter',
          type: 'text'
        },
        {
          "width": "75%",
          cellClass: 'alignCenter',
          type: 'text'
        },
        {
          cellClass: 'alignCenter',
          type: 'text'

        }
      ],
      "cells": [
        {
          "0": {
            "num": "210", "validate": {"or":[{"length":{"min":"1","max":"8"}},{"check":"isEmpty"}]},
            "rf": true,
            "label": "Street number",
            "tn": "210",
            "maxLength": "8"
          },
          "1": {
            "num": "220", "validate": {"or":[{"length":{"min":"1","max":"29"}},{"check":"isEmpty"}]},
            "rf": true,
            "label": "Street name/Rural route/Lot and Concession number",
            "tn": "220",
            "maxLength": "29"
          },
          "2": {
            "num": "230", "validate": {"or":[{"length":{"min":"1","max":"10"}},{"check":"isEmpty"}]},
            "rf": true,
            "label": "Suite number",
            "tn": "230",
            "maxLength": "10"
          }
        }]
    },
    "235": {
      "infoTable": true,
      "fixedRows": true,
      "columns": [
        {
          cellClass: 'alignCenter',
          type: 'text'
        }
      ],
      "cells": [
        {
          "0": {
            "num": "240", "validate": {"or":[{"length":{"min":"1","max":"42"}},{"check":"isEmpty"}]},
            "rf": true,
            "label": "Additional address information if applicable (line 220 must be completed first)",
            "tn": "240",
            "maxLength": "42"
          }
        }]
    },
    "245": {
      "infoTable": true,
      "fixedRows": true,
      "dividers": [1, 2, 3],
      "columns": [
        {
          cellClass: 'alignCenter',
          type: 'text'
        },
        {
          cellClass: 'alignCenter',
          type: 'text',
          "isProvinceField": {
            "countryNum": "270"
          }
        },
        {
          cellClass: 'alignCenter',
          "type": "dropdown",
          "options": countryAddressCodes,
          "init": "CA",
          "isCountry": {
            "provinceNum": "260",
            "postalNum": "280"
          }
        },
        {
          "width": "12%",
          cellClass: 'alignCenter',
          type: "text",
          "isPostalField": {
            "countryNum": "270"
          }
        }
      ],
      "cells": [
        {
          "0": {
            "num": "250", "validate": {"or":[{"length":{"min":"1","max":"30"}},{"check":"isEmpty"}]},
            "rf": true,
            "label": "Municipality (e.g. city, town)",
            "tn": "250",
            "maxLength": "30"
          },
          "1": {
            "num": "260", "validate": {"or":[{"length":{"min":"2","max":"2"}},{"check":"isEmpty"}]},
            "rf": true,
            "label": "Province",
            "tn": "260"
          },
          "2": {
            "num": "270", "validate": {"or":[{"length":{"min":"2","max":"2"}},{"check":"isEmpty"}]},
            "rf": true,
            "label": "Country",
            "tn": "270"
          },
          "3": {
            "num": "280", "validate": {"or":[{"length":{"min":"1","max":"10"}},{"check":"isEmpty"}]},
            "rf": true,
            "label": "Postal code",
            "tn": "280"
          }
        }]
    },
    //todo: confirmed to be pending on as Sandra's bug
    "445": {
      "fixedRows": true,
      "infoTable": true,
      dividers: [1, 2, 3],
      "columns": [
        {
          type: 'text',
          header: 'Last name'
        },
        {
          type: 'text',
          header: 'First name'
        },
        {
          type: 'text',
          header: 'Middle name(s)'
        }
      ],
      "cells": [
        {
          "0": {
            "num": "450", "validate": {"or":[{"length":{"min":"1","max":"35"}},{"check":"isEmpty"}]},
            "tn": "450",
            "maxLength": "35"
          },
          "1": {
            "num": "451", "validate": {"or":[{"length":{"min":"1","max":"20"}},{"check":"isEmpty"}]},
            "tn": "451",
            "maxLength": "20"
          },
          "2": {
            "num": "454", "validate": {"or":[{"length":{"min":"1","max":"20"}},{"check":"isEmpty"}]},
            "tn": "454",
            "maxLength": "20"
          }
        }]
    },
    "505": {
      "infoTable": true,
      "fixedRows": true,
      "columns": [{
        cellClass: 'alignCenter',
        type: "text"
      }],
      "cells": [{
        "0": {
          "label": "Care of (if applicable)",
          "num": "510", "validate": {"or":[{"length":{"min":"1","max":"42"}},{"check":"isEmpty"}]},
          "tn": "510",
          "maxLength": "2"
        }
      }]
    },
    "515": {
      "infoTable": true,
      "fixedRows": true,
      "dividers": [1,
        2],
      "columns": [{
        cellClass: 'alignCenter'
      },
        {
          "width": "75%",
          cellClass: 'alignCenter'
        },
        {
          cellClass: 'alignCenter'
        }],
      "cells": [{
        "0": {
          "num": "520", "validate": {"or":[{"length":{"min":"1","max":"8"}},{"check":"isEmpty"}]},
          "label": "Street number",
          "tn": "520",
          "maxLength": "8",
          type: 'text'
        },
        "1": {
          "num": "530", "validate": {"or":[{"length":{"min":"1","max":"29"}},{"check":"isEmpty"}]},
          "label": "Street name/Rural route/Lot and Concession number",
          "tn": "530",
          "maxLength": "29",
          "validator": "required",
          type: 'text'
        },
        "2": {
          "num": "540", "validate": {"or":[{"length":{"min":"1","max":"10"}},{"check":"isEmpty"}]},
          "label": "Suite number",
          "tn": "540",
          "maxLength": "10",
          type: 'text'
        }
      }]
    },
    "545": {
      "infoTable": true,
      "fixedRows": true,
      "columns": [{
        cellClass: 'alignCenter'
      }],
      "cells": [{
        "0": {
          "label": "Additional address information if applicable (line 530 must be completed first",
          "num": "550", "validate": {"or":[{"length":{"min":"1","max":"42"}},{"check":"isEmpty"}]},
          "tn": "550",
          "maxLength": "42",
          type: 'text'
        }
      }]
    },
    "555": {
      "infoTable": true,
      "fixedRows": true,
      "dividers": [1,
        2,
        3,
        4,
        5],
      "columns": [{
        cellClass: 'alignCenter',
        type: "text"
      },
        {
          cellClass: 'alignCenter',
          type: "text",
          "isProvinceField": {
            "countryNum": "580"
          }
        },
        {
          cellClass: 'alignCenter',
          "type": "dropdown",
          "options": countryAddressCodes,
          // "init": "CA",
          "isCountry": {
            "provinceNum": "570",
            "postalNum": "590"
          }
        },
        {
          "width": "12%",
          cellClass: 'alignCenter',
          type: "text",
          "isPostalField": {
            "countryNum": "580"
          }
        }],
      "cells": [{
        "0": {
          "num": "560", "validate": {"or":[{"length":{"min":"1","max":"30"}},{"check":"isEmpty"}]},
          "label": "Municipality (e.g. city, town)",
          "tn": "560",
          "maxLength": "30",
          "validator": "required",
          "saveNum": true,
          type: 'text'
        },
        "1": {
          "num": "570", "validate": {"or":[{"length":{"min":"2","max":"2"}},{"check":"isEmpty"}]},
          "label": "Province",
          "tn": "570",
          type: 'text'
        },
        "2": {
          "num": "580", "validate": {"or":[{"length":{"min":"2","max":"2"}},{"check":"isEmpty"}]},
          "label": "Country",
          "tn": "580",
          "saveNum": true,
          "validator": "required"
        },
        "3": {
          "num": "590", "validate": {"or":[{"length":{"min":"1","max":"10"}},{"check":"isEmpty"}]},
          "label": "Postal/zip code",
          "tn": "590",
          "maxLength": "10",
          type: 'text'
        }
      }]
    }
  }
})();
