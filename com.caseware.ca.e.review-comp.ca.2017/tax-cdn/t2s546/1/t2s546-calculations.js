(function() {

  wpw.tax.create.calcBlocks('t2s546', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      //Part1: corporation name and number
      calcUtils.getGlobalValue('100', 'cp', '002');
      var date105 = field('cp.105').get(), date107 = field('cp.107').get();
      if (!date105) {
        calcUtils.getGlobalValue('110', 'cp', '107');
      }
      else if (!date107) {
        calcUtils.getGlobalValue('110', 'cp', '105');
      }
      else if (wpw.tax.utilities.dateCompare.greaterThan(date105, date107)) {
        calcUtils.getGlobalValue('110', 'cp', '105');
      }
      else {
        calcUtils.getGlobalValue('110', 'cp', '107');
      }
      field('120').assign(field('CP.146').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      if (field('CP.750').get()== 'ON' || (field('CP.750').get() == 'MJ' && field('T2S5.013').get())){
        //Part2: Getting Address from head office address in cp
        var address = field('CP.011').get();
        var streetNum = address.substring(0, address.indexOf(' '));
        if(!calcUtils.hasChanged('210')||!calcUtils.hasChanged('220')||!calcUtils.hasChanged('240')){
          field('210').assign(streetNum);
          field('220').assign(address.substring(streetNum.length + 1));
          calcUtils.getGlobalValue('240', 'CP', '012');
        }
        var enableFields = [210, 220, 230, 240];
        enableFields.forEach(function(fieldId) {
          field(fieldId).disabled(false);
        });
        calcUtils.getGlobalValue('250', 'cp', '015');
        calcUtils.getGlobalValue('260', 'cp', '016');
        calcUtils.getGlobalValue('270', 'cp', '017');
        calcUtils.getGlobalValue('280', 'cp', '018');
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 4 calculations//
      calcUtils.getGlobalValue('450', 'cp', '950');
      calcUtils.getGlobalValue('451', 'cp', '951');
      if (field('500').get() == '2') {
        calcUtils.equals('510', '200');
        calcUtils.equals('520', '210');
        calcUtils.equals('530', '220');
        calcUtils.equals('540', '230');
        calcUtils.equals('550', '240');
        calcUtils.equals('560', '250');
        calcUtils.equals('590', '280');
        calcUtils.equals('570', '260');
        calcUtils.equals('580', '270');
      }
      else {
        for (var fieldId = 510; fieldId <= 590; fieldId = fieldId + 10) {
          field(fieldId).disabled(false);
        }
      }
      if (field('300').get() ==1) {
        field('600').assign(' ')
      }
    });
  });
})();
