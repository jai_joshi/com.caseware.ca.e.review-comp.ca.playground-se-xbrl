(function() {
  wpw.tax.create.diagnostics('t2s546', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var part5List = ['500', '520', '530', '560', '580'];

    diagUtils.diagnostic('5460001', common.prereq(common.requireFiled('T2S546'), common.requireNotFiled('T2S548')));

    diagUtils.diagnostic('5460001', common.prereq(common.requireFiled('T2S548'), common.requireNotFiled('T2S546')));

    diagUtils.diagnostic('5460002', common.prereq(common.requireFiled('T2S546'), common.check(['100', '110', '120'], 'isFilled', true)));

    diagUtils.diagnostic('5460003', common.prereq(common.requireFiled('T2S546'),
        function(tools) {
          if (tools.field('270').get() == 'CA' || tools.field('270').get() == 'US')
            return tools.requireAll(tools.list(['220', '250', '260', '270', '280']), 'isFilled');
          else
            return tools.requireAll(tools.list(['220', '250', '270']), 'isFilled')
        }));

    diagUtils.diagnostic('5460004', common.prereq(common.and(
        common.requireFiled('T2S546'),
        function(tools) {
          return tools.field('300').get() == 2;
        }), common.or(
        common.check(part5List, 'isNonZero', true),
        common.check('600', 'isNonZero'))));

    diagUtils.diagnostic('5460005', common.prereq(common.and(
        common.requireFiled('T2S546'),
        function(tools) {
          return tools.field('500').get() == 1 || tools.field('500').get() == 2;
        }), common.check(['510', '520', '530', '540', '550', '560', '570', '580', '590'], 'isEmpty', true)));

    diagUtils.diagnostic('5460006', common.prereq(common.and(
        common.requireFiled('T2S546'),
        function(tools) {
          return tools.field('500').get() == 3;
        }), function(tools) {
      if (tools.field('580').get() == 'CA' || tools.field('580').get() == 'US')
        return tools.requireAll(tools.list(['520', '530', '560', '580', '570', '590']), 'isNonZero');
      else
        return tools.requireAll(tools.list(['520', '530', '560', '580']), 'isNonZero');
    }));

    diagUtils.diagnostic('5460007', common.prereq(common.requireFiled('T2S546'),
        common.check(['300', '450', '451', '460'], 'isFilled', true)));

    diagUtils.diagnostic('5460008', common.prereq(common.and(
        common.requireFiled('T2S546'),
        function(tools) {
          return tools.field('300').get() == 1;
        }), common.and(
        common.check(['510', '520', '530', '540', '550', '560', '570', '580', '590', '600'], 'isEmpty', true),
        common.check('500', 'isZero'))));

    diagUtils.diagnostic('part2.addressCheck',  common.prereq(common.check('210', 'isFilled'), function (tools) {
      return tools.requireAll(tools.list(['220', '230', '240']), 'isFilled')
    }))
  });
})();
