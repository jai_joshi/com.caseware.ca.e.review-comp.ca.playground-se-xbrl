(function() {
  'use strict';

  wpw.tax.global.formData.t2s546 = {
    'formInfo': {
      abbreviation: 'T2S546',
      'title': 'Corporations Information Act Annual Return for Ontario Corporations',
      //'subTitle': '(2009 and later tax years)',
      'schedule': 'Schedule 546',
      'code': 'Code 0902',
      formFooterNum: 'T2 SCH 546 E (10)',
      'headerImage': 'canada-federal',
      'showCorpInfo': true,
      'description': [
        {
          'type': 'list',
          'items': [
            'This schedule should be completed by a corporation that is incorporated, continued, or amalgamated ' +
            'in Ontario and subject to the Ontario <i>Business Corporations Act</i> (BCA) or Ontario ' +
            '<i>Corporations Act</i> (CA), except for registered charities under the federal <i>Income Tax Act</i>.' +
            ' This completed schedule serves  as a <i>Corporations Information Act</i> Annual Return under the ' +
            'Ontario <i>Corporations Information Act</i>.',
            'Complete parts 1 to 4. Complete parts 5 to 7 only to report change(s) in the information recorded' +
            ' on the Ontario Ministry of Government Services (MGS) public record.',
            'This schedule must set out the required information for the corporation as of the date of' +
            ' delivery of this schedule.',
            'A completed Ontario ' + '<i> ' + ' Corporations Information Act ' + '</i> ' + ' Annual Return must' +
            ' be delivered within six months after the end of the corporation\'s tax year-end. The MGS considers' +
            ' this return to be delivered on the date that it is filed with the Canada Revenue Agency (CRA)' +
            ' together with the corporation\'s income tax return.',
            'It is the corporation\'s responsibility to ensure that the information shown on the MGS public record' +
            ' is accurate and up-to-date. To review the information shown for the corporation on the public record' +
            ' maintained by the MGS, obtain a Corporation Profile Report. Visit ' +
            '<b>' + ' www.ServiceOntario.ca ' + '</b>' + ' for more information.',
            'This schedule contains non-tax information collected under the authority of the Ontario ' + '<i> ' +
            ' Corporations Information Act ' + '</i> ' + '. This information will be sent to the MGS for the ' +
            'purposes of recording the information on the public record maintained by the MGS.'
          ]
        }
      ],
      category: 'Ontario Forms'
    },
    'sections': [
      {
        'header': 'Part 1 - Identification',
        'rows': [
          {
            'label': 'Corporation\'s name (exactly as shown on the MGS public record)',
            labelClass: 'fullLength'
          },
          {
            'type': 'infoField',
            'num': '100', "validate": {"or": [{"length": {"min": "1", "max": "168"}}, {"check": "isEmpty"}]},
            'tn': '100',
            maxLength: '168'
          },
          {'type': 'horizontalLine'},
          {
            'type': 'table', 'num': '105'
          }
        ]
      },
      {
        'header': 'Part 2 – Head or registered office address (P.O. box not acceptable as stand-alone address)',
        'rows': [
          {
            'type': 'table', 'num': '195'
          },
          {
            'type': 'table', 'num': '205'
          },
          {
            'type': 'table', 'num': '235'
          },
          {
            'type': 'table', 'num': '245'
          }
        ]
      },
      {
        'header': 'Part 3 - Change identifier',
        'rows': [
          {
            'label': 'Have there been any changes in any of the information most recently filed' +
            ' for the public record maintained by the MGS for the corporation with respect to names,' +
            ' addresses for service, and the date elected/appointed and, if applicable, the date the ' +
            'election/appointment ceased of the directors and five most senior officers, or with respect ' +
            'to the corporation\'s mailing address or language of preference? To review the information shown ' +
            'for the corporation on the public record maintained by the MGS, obtain a Corporation Profile Report. ' +
            'For more information, visit ' + 'www.ServiceOntario.ca'.link('https://www.ontario.ca/welcome-serviceontario'),
            'labelClass': 'fullLength'
          },
          {'labelClass': 'fullLength'},
          {
            'type': 'infoField',
            'labelEnd': true,
            'label': 'If there have been no changes, enter <b>1</b> in this box and then go to "Part 4 – ' +
            'Certification." <br> If there are changes, enter <b>2</b> in this box and complete the applicable ' +
            'parts on the next page, and then go to "Part 4 – Certification."',
            'labelWidth': '90%',
            'num': '300',
            'inputType': 'dropdown',
            'options': [
              {'option': ' '},
              {'value': '1', 'option': '1'},
              {'value': '2', 'option': '2'}
            ],
            'tn': '300',
            validator: 'required'
          }
        ]
      },
      {
        'header': 'Part 4 - Certification',
        'rows': [
          {
            'label': 'I certify that all information given in this <i>Corporations Information Act</i> ' +
            'Annual Return is true, correct, and complete.',
            'labelClass': 'fullLength'
          },
          {'labelClass': 'fullLength'},
          {
            'type': 'table', 'num': '445'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            labelEnd: true,
            label: 'Please enter one of the following numbers in this box for the above-named person:' +
            ' <b> 1 </b> for director, <b> 2</b> for officer, or <b> 3</b> for other individual having knowledge of the' +
            ' affairs of the corporation. If you are a director and officer, enter <b> 1</b> or <b>2</b>.',
            labelWidth: '90%',
            num: '460',
            "validate": {"or": [{"length": {"min": "1", "max": "1"}}, {"check": "isEmpty"}]},
            rf: true,
            tn: '460',
            inputType: 'dropdown',
            options: [
              {'option': ' '},
              {value: '1', option: '1'},
              {value: '2', option: '2'},
              {value: '3', option: '3'}
            ]
          },
          {labelClass: 'fullLength'},
          {
            label: 'Note: Sections 13 and 14 of the Ontario <i> Corporations Information Act </i> provide penalties ' +
            'for making false or misleading statements or omissions.', labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'}
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            labelClass: 'fullLength'
          },
          {
            label: 'Complete the applicable parts to report changes in the information recorded ' +
            'on the MGS public record.',
            labelClass: 'fullLength bold center'
          }
        ]
      },
      {
        'header': 'Part 5 - Mailing address',
        'rows': [
          {labelClass: 'fullLength'},
          {label: 'Please select one of the following in the box:'},
          {
            'type': 'infoField',
            'labelEnd': true,
            'label': 'Please enter one of the following numbers in this box: <br> ' + '<b>' + '1' + '</b>' +
            ' - Show no mailing address on the MGS public record. ' + '<br>' + '<b>' + '2' + '</b>' + ' - The ' +
            'corporation\'s mailing address is the same as the head or registered office address in Part 2 of this ' +
            'schedule. <br>' + '<b>' + '3' + '</b>' + ' - The corporation\'s complete mailing address is as follows:',
            'labelWidth': '90%',
            'num': '500',
            'inputType': 'dropdown', init: '0',
            'options': [
              {value: '0', option: ''},
              {'value': '1', 'option': '1 - Show no mailing address on the MGS public record'},
              {
                'value': '2', 'option': '2 - The corporation\'s mailing address is the same as the head or ' +
              'registered office address in Part 2 of this schedule.'
              },
              {'value': '3', 'option': '3 - The corporation\'s complete mailing address is as follows:'}
            ],
            'tn': '500', maxLength: '1'
          },
          {
            'type': 'table', 'num': '505'
          },
          {
            'type': 'table', 'num': '515'
          },
          {
            'type': 'table', 'num': '545'
          },
          {
            'type': 'table', 'num': '555'
          }
        ]
      },
      {
        'header': 'Part 6 - Language of preference',
        'rows': [
          {
            'type': 'infoField',
            'labelEnd': true,
            'label': 'Indicate your language of preference by entering 1 for English or 2 for French. This is the ' +
            'language of preference recorded on the MGS public record for communications with the corporation. ' +
            'It may be different from line 990 on the T2 return',
            'num': '600',
            init: '1',
            'inputType': 'dropdown',
            'options': [
              {value: ' ', option: ' '},
              {'value': '1', 'option': '1'},
              {'value': '2', 'option': '2'}
            ],
            'tn': '600'
          }
        ]
      }

      // part 7
      // type: 'table', num: '101'//

      //{
      //  'hideFieldset': true,
      //  'rows': [
      //    {'labelClass': 'fullLength'},
      //    {
      //      'label': 'CRA internal form identifier 547, Code 0902',
      //      'labelClass': 'bold text-right fullLength noBackground'
      //    }
      //  ]
      //}
    ]
  };
})();
