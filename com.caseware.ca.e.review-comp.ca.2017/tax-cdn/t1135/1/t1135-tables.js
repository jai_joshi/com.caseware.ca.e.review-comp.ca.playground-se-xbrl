(function() {
  var countryCodes = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.countryCodes, 'value', 'value');
  var addCanada = wpw.utilities.clone(wpw.tax.codes.countryCodes);
  var addOther = wpw.utilities.clone(wpw.tax.codes.countryCodes);
  addOther['OTH'] = {value: 'OTH Other'};
  addCanada['CAN'] = {value: 'CAN Canada'};
  var countryCodesAddCanada = new wpw.tax.actions.codeToDropdownOptions(addCanada, 'value', 'value');
  var countryCodesAddOther = new wpw.tax.actions.codeToDropdownOptions(addOther, 'value', 'value');

  function createTable(categoryType) {
    var descriptionCol;
    var countryCol;
    var valueCols = [];

    switch (categoryType) {
      case '1':
        descriptionCol = {header: 'Name of bank/other entity holding the funds', num: '3000'};
        countryCol = {header:'Country code', num: '3001'};
        valueCols = [{header: 'Maximum funds held during the year', num: '3002'}, {header:'Funds held at year end', num: '3003', totalNum: '3005'}, {header:'Income', num: '3004', totalNum: '3006'}];
        break;
      case '2':
        descriptionCol = {header: 'Name of corporation', num: '3007'};
        countryCol = {header: 'Country code', num: '3008'};
        valueCols = [{header: 'Maximum cost amount during the year', num: '3009'}, {header:'Cost amount at year end', num: '3010', totalNum: '3013'},{header:'Income', num: '3011', totalNum: '3014'},{header:'Gain (loss) on disposition', num: '3012', totalNum: '3015'}];
        break;
      case '3':
        descriptionCol = {header: 'Description of indebtedness', num: '3016'};
        countryCol = {header: 'Country code', num: '3017'};
        valueCols = [{header: 'Maximum cost amount during the year', num: '3018'}, {header:'Cost amount at year end', num: '3019', totalNum: '3022'},{header:'Income', num: '3020', totalNum: '3023'},{header:'Gain (loss) on disposition', num: '3021', totalNum: '3024'}];
        break;
      case '4':
        descriptionCol = {header: 'Name of trust', num: '3025'};
        countryCol = {header: 'Country code', num: '3026'};
        valueCols = [{header: 'Maximum cost amount during the year', num: '3027'}, {header:'Cost amount at year end', num: '3028', totalNum: '3032'},{header:'Income received', num: '3029', totalNum: '3033'},{header:'Capital received', num: '3030', totalNum: '3034'},{header:'Gain (loss) on disposition', num: '3031', totalNum: '3035'}];
        break;
      case '5':
        descriptionCol = {header: 'Description of property', num: '3036'};
        countryCol = {header: 'Country code', num: '3037'};
        valueCols = [{header: 'Maximum cost amount during the year', num: '3038'}, {header:'Cost amount at year end', num: '3039', totalNum: '3042'},{header:'Income', num: '3040', totalNum: '3043'},{header:'Gain (loss) on disposition', num: '3041', totalNum: '3044'}];
        break;
      case '6':
        descriptionCol = {header: 'Description of property', num: '3045'};
        countryCol = {header: 'Country code', num: '3046'};
        valueCols = [{header: 'Maximum cost amount during the year', num: '3047'}, {header:'Cost amount at year end', num: '3048', totalNum: '3051'},{header:'Income', num: '3049', totalNum: '3052'},{header:'Gain (loss) on disposition', num: '3050', totalNum: '3053'}];
        break;
      case '7':
        descriptionCol = {header: 'Name of registered security dealer/Canadian trust company', num: '3054'};
        countryCol = {header: 'Country code', num: '3055'};
        valueCols = [{header: 'Maximum fair market value during the year', num: '3056'}, {header:'Fair market value at year-end', num: '3057', totalNum: '3060'},{header:'Income', num: '3058', totalNum: '3061'},{header:'Gain (loss) on disposition', num: '3059', totalNum: '3062'}];
        break;
    }

    var tableCols = [];

    tableCols.push(
        {
          header: descriptionCol.header,
          type: 'text',
          num: descriptionCol.num,
          colClass: 'std-input-col-padding-width'
        },
        {
          header: countryCol.header,
          type: 'dropdown',
          num: countryCol.num,
          colClass: 'std-input-width',
          options: categoryType === '1' ? countryCodes : countryCodesAddOther
        }
    );

    valueCols.forEach(function(col) {
      var colObj = {};
      if(col.totalNum){
        colObj = {
          header: col.header,
          num: col.num,
          total: true,
          totalNum: col.totalNum
        }
      }else {
        colObj = {
          header: col.header,
          num: col.num,
          colClass: 'std-input-width'
        }
      }
      tableCols.push(colObj);
    });

    return {
      showNumbering: true,
      maxLoop: 100,
      hasTotals: true,
      initRows: 0,
      // scrollx: true,
      columns: tableCols
    }
  }

  wpw.tax.create.tables('t1135', {
    '1080': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          disabled: true,
          cannotOverride: true,
          width: '80px'
        },
        {
          disabled: true,
          cannotOverride: true

        },
        {
          disabled: true,
          cannotOverride: true

        }
        ,
        {
          disabled: true,
          cannotOverride: true,
          width: '100px'
        }
        ,
        {
          disabled: true,
          cannotOverride: true

        }
        ,
        {
          disabled: true,
          cannotOverride: true

        }
      ],
      cells: [
        {
          '0': {
            type: 'singleCheckbox',
            label: 'Individual'
          },
          '1': {
            label: 'First name',
            type: 'text'
          },
          '2': {
            label: 'Last name',
            type: 'text'
          },
          '3': {
            label: 'Initial',
            type: 'text'
          },
          '4': {
            label: 'Social insurance number',
            validate: {check: 'mod10'},
            type: 'text'
          },
          '5': {
            type: 'dropdown',
            options: [
              {value: '1', option: '1 Self Employeed'},
              {value: '2', option: '2 Non-self employeed'}
            ],
            label: 'Individual code'
          }
        }
      ]
    },
    '1090': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '80px'
        },
        {
          disabled: true
        },
        {
          disabled: true
        }
      ],
      cells: [
        {
          '0': {
            type: 'singleCheckbox',
            label: 'Corporation',
            init: true
          },
          '1': {
            label: 'Corporation\'s name',
            type: 'text',
            num: '3001',
            cellClass: 'alignLeft'
          },
          '2': {
            label: 'Business number (BN)',
            num: '1002',
            type: 'custom',
            format: ['{N9}RC{N4}', 'NR']
          }
        }
      ]
    },
    '1091': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          disabled: true,
          cannotOverride: true,
          width: '80px'
        },
        {
          cannotOverride: true,
          disabled: true
        },
        {
          cannotOverride: true,
          disabled: true
        }
      ],
      cells: [
        {
          '0': {
            type: 'singleCheckbox',
            label: 'Trust'
          },
          '1': {
            label: 'Trust\'s name',
            type: 'text'
          },
          '2': {
            label: 'Account number',
            type: 'text'
          }
        }
      ]
    },
    '1200': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          disabled: true,
          cannotOverride: true,
          width: '80px'
        },
        {
          disabled: true,
          cannotOverride: true
        },
        {
          disabled: true,
          cannotOverride: true,
          width: '210px'
        },
        {
          disabled: true,
          cannotOverride: true
        }
      ],
      cells: [
        {
          '0': {
            type: 'singleCheckbox',
            label: 'Partnership'
          },
          '1': {
            label: 'Partnership\'s name',
            type: 'text'
          },
          '2': {
            inputType: 'dropdown',
            label: 'Partnership code',
            width: '202px',
            options: [
              {value: '1', option: '1 If end partners are individuals or trusts'},
              {value: '2', option: '2 If end partners are corporations'},
              {value: '3', option: '3 If end partners are a combination of 1 and 2 mentioned above'}]
          },
          '3': {
            label: 'Partnership\'s account number',
            type: 'custom',
            format: ['{N9}RZ{N4}', 'NR']
          }
        }
      ]
    },
    '1010': createTable('1'),
    '1020': createTable('2'),
    '1030': createTable('3'),
    '1040': createTable('4'),
    '1050': createTable('5'),
    '1060': createTable('6'),
    '1070': createTable('7'),
/*    '1337': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          inputType: 'singleCheckbox',
          label: 'If the total cost of all specified foreign property held at any time during the year exceeds ' +
          '$100,000 but was less than $250,000, you are required to complete either Part A or Part B; ',
          disabled: false
        },
        {
          inputType: 'singleCheckbox',
          label: 'If the total cost of all specified foreign property held at any time during the year was ' +
          '$250,000 or more, you are required to complete Part B',
          disabled: false
        }
      ],
      'cells': [
        {
          '0': {
            'num': '2000'
          },
          '1': {
            'num': '2001'
          }
        }
      ]
    },*/
    '1210': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          'header': 'Country code',
          'type': 'dropdown',
          'options': countryCodesAddCanada
        },
        {
          'header': 'Country code',
          'type': 'dropdown',
          'options': countryCodesAddCanada
        },
        {
          header: 'Country code',
          type: 'dropdown',
          options: countryCodesAddCanada
        }
      ],
      "cells": [
        {
          '0': {
            num: '3067'
          },
          '1': {
            num: '3068'
          },
          '2': {
            num: '3069'
          }
        }
      ]
    }
  })
})();
