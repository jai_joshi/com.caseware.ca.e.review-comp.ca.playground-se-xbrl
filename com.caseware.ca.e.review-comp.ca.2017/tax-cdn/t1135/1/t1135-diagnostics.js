(function() {
  wpw.tax.create.diagnostics('t1135', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var disallowedAddressChars = ['-', "'", '/', '&', '(', ')', '#'];
    var partAFields = ['2002', '2003', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '3067', '3068', '3069'];
    var partBTables = ['1010', '1020', '1030', '1040', '1050', '1060', '1070'];
    var addressFields = ['104', '907', '908'];
    var forEach = diagUtils.forEach;

    //Uncomment once we support partnerships
    // diagUtils.diagnostic('BN', forEach.row('1200', forEach.bnCheckCol(3, true)));
    diagUtils.diagnostic('BN', common.bnCheck('3002'));

    function addressCheck(fields) {
      angular.forEach(fields, function(field) {
        diagUtils.diagnostic('1135.addressCheck', common.prereq(function(tools) {
          return tools.field(field).isFilled();
        }, function(tools) {
          return tools.field(field).require(function() {
            var addressField = this.get().toString();
            var firstPos = addressField.charAt(0);
            var lastPos = addressField.charAt(addressField.length - 1);
            return !(disallowedAddressChars.indexOf(firstPos) > -1 ||
            disallowedAddressChars.indexOf(lastPos) > -1 ||
            firstPos == '.');
          })
        }));
      });
    }

    function partBTableCheck(tables) {
      angular.forEach(tables, function(tableNum) {
        diagUtils.diagnostic('1135.partBTableCheck', common.prereq(function(tools) {
          var table = tools.field(tableNum);
          return tools.checkAll(table.getRows(), function(row) {
            return tools.checkMethod(row, 'isFilled');
          });
        }, function(tools) {
          var table = tools.field(tableNum);
          var rows = [];
          return tools.checkAll(table.getRows(), function(row) {
            angular.forEach(row, function(col) {
              rows.push(col);
            });
            return tools.requireAll(rows, 'isFilled');
          });
        }));
      })
    }

    function resetFields(tools) {
      //Reset Part A
      angular.forEach(partAFields, function(num) {
        tools.field(num).set('');
      });
      //Reset Part B
      angular.forEach(partBTables, function(tableNum) {
        var table = tools.field(tableNum);
        var initRowCount = table.size().rows;
        table.track();
        for (var i = initRowCount-1; i >= 0; i--) {
          wpw.tax.utilities.removeTableRow('t1135', tableNum, i);
        }
        wpw.tax.utilities.addTableRow('t1135', tableNum);
      });
    }

    partBTableCheck(partBTables);

    addressCheck(addressFields);
    diagUtils.diagnostic('1135.requiredToFile', common.prereq(common.check('CP.to-28', 'isYes'),common.check(['2000', '2001'], 'isChecked')));

    diagUtils.diagnostic('1135.headOfficeAddressCheck', common.prereq(function(tools) {
      return tools.checkMethod(tools.list(['2000', '2001']), 'isChecked');
    }, function(tools) {
      return tools.requireAll(tools.list(['104', '106', '107', '108', '109']), 'isFilled');
    }));

    diagUtils.diagnostic('1135.partAorB', common.prereq(function(tools) {
      return tools.checkMethod([tools.field('2000'), tools.field('2001')], 'isChecked') > 1;
    }, function(tools) {
      return tools.requireOne(tools.list(['2000', '2001']), 'isEmpty');
    }));

    diagUtils.diagnostic('1135.propertyType', common.prereq(common.check('2000', 'isChecked'),
        function(tools) {
          var requiredFields = ['2002', '2003', '2004', '2005', '2006', '2007', '2010'];
          return tools.requireOne(tools.list(requiredFields), 'isChecked') ||
              !tools.checkAll(partBTables, function(tableNum) {
                  return tools.field(tableNum).size().rows == 0;
              });
        }));

    diagUtils.diagnostic('1135.countryCode', common.prereq(function(tools) {
      return tools.checkMethod(tools.list(['2002', '2003', '2004', '2005', '2006', '2007', '2010']), 'isChecked');
    }, function(tools) {
      return tools.requireOne(tools.list(['3067', '3068', '3069']), 'isFilled');
    }));

    diagUtils.diagnostic('1135.foreignIncome', common.prereq(function(tools) {
      return tools.checkMethod(tools.list(['2002', '2003', '2004', '2005', '2006', '2007', '2010']), 'isChecked');
    }, function(tools) {
      return tools.field('2008').require('isFilled');
    }));

    diagUtils.diagnostic('1135.gainLoss', common.prereq(function(tools) {
      return tools.checkMethod(tools.list(['2002', '2003', '2004', '2005', '2006', '2007', '2010']), 'isChecked');
    }, function(tools) {
      return tools.field('2009').require('isFilled');
    }));

    diagUtils.diagnostic('1135.nameCheck', common.prereq(function(tools) {
      return tools.checkMethod(tools.list(['2000', '2001']), 'isChecked');
    }, function(tools) {
      return tools.requireAll(tools.list(['914', '915']), 'isFilled');
    }));

    diagUtils.diagnostic('1135.startDate', common.prereq(function(tools) {
      return tools.checkMethod(tools.list(['2000', '2001']), 'isChecked');
    }, function(tools) {
      return tools.requireAll(tools.list(['110', '111']), 'isFilled');
    }));

    diagUtils.diagnostic('1135.clearFields', common.prereq(function(tools) {
      return (tools.originalField('2000', 't1135').get() === true && tools.field('2000').get() === false) ||
          (tools.originalField('2001', 't1135').get() === true && tools.field('2001').get() === false)
    }, function(tools) {
      var errorMsg = 'All data below will be lost.';
      return tools.popup(errorMsg, function(response) {
        if (response === true) {
          resetFields(tools);
        }
      })
    }));

    // diagUtils.diagnostic('1135.maxRows', function(tools) {
    //   return tools.checkAll(partBTables, function(tableNum) {
    //     var table = tools.field(tableNum);
    //     return table.getRow(0)[0].require(function() {
    //       return table.size().rows <= 100;
    //     })
    //   });
    // });

  });
})();
