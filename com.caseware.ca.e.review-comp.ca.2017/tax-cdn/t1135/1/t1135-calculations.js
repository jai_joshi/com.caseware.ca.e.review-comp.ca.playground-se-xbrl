(function() {

  wpw.tax.create.calcBlocks('t1135', function(calcUtils) {
    //populate table based on selection from S6
    calcUtils.calc(function(calcUtils, field, form) {
      var t1135tableNumArray = ['1010', '1020', '1030', '1040', '1050', '1060'];
      var t2s6TableObj = {
        '101': {nameCIndex: 1, transferCIndex: 8},
        '201': {nameCIndex: 0, transferCIndex: 6},
        '301': {nameCIndex: 2, transferCIndex: 8}
      };
      //todo: figure out why only 3 table from s6 get transfer to 1135 as per Taxprep

      t1135tableNumArray.forEach(function(t1135TableNum, tableIndex) {
        var t1135Table = field(t1135TableNum);
        // If row in s6 table is deleted, then we need to remove that row in t1135 table
        // do that by filtering out any rows in sumTable whose link no longer exist.
        calcUtils.filterTableRow(t1135Table, function(table, rowIndex) {
          // We return false to delete this row.
          return !calcUtils.checkLinkDeleted(table, rowIndex, table.getRowLinks(rowIndex).filter(function(link) {
            return link.type === 'src';
          }));
        });

        var groupedRows = {};
        // Add rows to t1135 whenever new rows have been added to the s6 table
        // Note that the first table passed into fillAndLinkTable is the one that will be populated with data from the
        // second table.
        for (var t2s6tableNum in t2s6TableObj) {
          var t2s6table = form('t2s6').field(t2s6tableNum);
          var linkedRepeatForms = {};
          var group = groupedRows[t2s6tableNum] = {};
          calcUtils.fillAndLinkTable(t1135Table, t2s6table, function(t2s6table, rowIndex) {
            // First check if we should transfer this row
            var transferCIndex = t2s6TableObj[t2s6tableNum].transferCIndex;
            var categoryCIndex = transferCIndex + 1;
            var shouldTranser = (t2s6table.cell(rowIndex, categoryCIndex).get() == tableIndex + 1 &&
            t2s6table.cell(rowIndex, transferCIndex).get());

            if (!shouldTranser) {
              return false;
            }

            // Check if this t2s6table has a source link to ACBSUM
            var srcLink = t2s6table.getRowLinks(rowIndex).filter(function(link) {
              return link.type === 'src' && wpw.tax.utilities.capitalizeFormId(link.formId) === 'ACBSUM'
            })[0];

            // If there is a link follow it all the way back to ACBTRACKER and check if we already added a row for this
            // repeat form. If we did then return false. If we didn't then add it to the linkedRepeatForms set and
            // add the row.
            if (srcLink) {
              var acbSumLink = calcUtils.followLink(srcLink);
              var acbTrackerLink = acbSumLink.table.getRowLinks(acbSumLink.rowIndex).filter(function(link) {
                return link.type === 'src' && wpw.tax.utilities.capitalizeFormId(link.formId).startsWith('ACBTRACKER')
              })[0];

              if (acbTrackerLink) {
                var t2s6RowId = t2s6table.getRowId(rowIndex);
                // If we already created a row for this repeat form then don't add another one
                if (linkedRepeatForms[acbTrackerLink.formId]) {
                  group[linkedRepeatForms[acbTrackerLink.formId]].push(t2s6RowId);
                  return false;
                }
                // Set to true to indicate we'll create one
                linkedRepeatForms[acbTrackerLink.formId] = t2s6RowId;
                group[t2s6RowId] = [t2s6RowId];
              }
            }
            return true;
          });
        }

        // We perform some calc with the linked rows to show it is actually linked
        calcUtils.forEachLinkedRow(t1135Table, function(mainTable, mainRowInd, linkedTable, linkedRowInd) {
          var mainRow = mainTable.getRow(mainRowInd);
          var linkRow = linkedTable.getRow(linkedRowInd);
          var mainTableColLength = mainTable.size().cols;
          var linkedTableNum = linkedTable.fieldObj.fieldId;
          var nameCIndex = t2s6TableObj[linkedTableNum].nameCIndex;
          var countryCIndex = t2s6TableObj[linkedTableNum].transferCIndex + 2;
          var gainLossCIndex = t2s6TableObj[linkedTableNum].transferCIndex - 1;
          var nameVal = linkRow[nameCIndex].get();
          if (linkedTableNum == '201') {
            nameVal = nameVal.address1 + ' ' + nameVal.address2;
          }
          mainRow[0].assign(nameVal);//name
          mainRow[1].assign(linkRow[countryCIndex].get());//country code
          mainRow[1].source(linkRow[countryCIndex]);

          var group = groupedRows[linkedTable.fieldObj.fieldId][linkedTable.getRowId(linkedRowInd)];
          var sum = 0;
          if (group && group.length > 0) {
            group.forEach(function(rowId) {
              sum += linkedTable.cell(linkedTable.rowIdToIndex(rowId), gainLossCIndex).get();
            });
            mainRow[mainTableColLength - 1].assign(sum);//gain loss
          } else {
            mainRow[mainTableColLength - 1].assign(linkRow[gainLossCIndex].get());//gain loss
            mainRow[mainTableColLength - 1].source(linkRow[gainLossCIndex]);
          }
        });
        //todo: rearrange row order
      });
    });

    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('3001', 'CP', '002');
      calcUtils.getGlobalValue('1002', 'CP', 'bn');
      calcUtils.getGlobalValue('914', 'CP', '951');
      calcUtils.getGlobalValue('915', 'CP', '950');
      calcUtils.getGlobalValue('903', 'CP', '954');
      calcUtils.getGlobalValue('904', 'CP', '956');
      calcUtils.getGlobalValue('905', 'CP', '955');
      calcUtils.getGlobalValue('904-ext', 'CP', '637');
      calcUtils.getGlobalValue('104', 'CP', '011');
      calcUtils.getGlobalValue('105', 'CP', '012');
      calcUtils.getGlobalValue('106', 'CP', '016');
      calcUtils.getGlobalValue('107', 'CP', '015');
      calcUtils.getGlobalValue('108', 'CP', '017');
      calcUtils.getGlobalValue('109', 'CP', '018');
      calcUtils.getGlobalValue('110', 'CP', 'tax_start');
      calcUtils.getGlobalValue('111', 'CP', 'tax_end');

/*      field('1337').getRows().forEach(function(row) {
        calcUtils.field('2000').set(row[0].get());
        calcUtils.field('2001').set(row[1].get());
      });*/
    });

  });
})
();
