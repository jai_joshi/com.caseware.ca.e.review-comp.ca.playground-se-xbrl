(function() {
  'use strict';
  var currencyCodes = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.currencyCodes, 'value');

  wpw.tax.create.formData('t1135', {
    formInfo: {
      abbreviation: 'T1135',
      title: 'Foreign Income Verification Statement',
      // printNum: 'T1135',
      headerImage: 'canada-federal',
      dynamicFormWidth: true,
      hideHeaderBox: true,
      formFooterNum: 'T1135 E (17)',
      category: 'Federal Tax Forms',
      agencyUseOnlyBox: {
        header: 'For departmental use.',
        width: '200px',
        height: '140px',
        textAbove: ' '
      },
      description: [
        {
          text: ' • This form must be used for the 2015 and later taxation years.'
        },
        {
          text: ' • Complete and file this form if at any time in the year the total cost amount to the reporting taxpayer ' +
          'of all specified foreign property was more than $100,000 (Canadian)'
        },
        {
          text: ' • See attached instructions for more information about completing this form.'
        }
      ]
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            labelClass: 'fullLength',
            hidePrint: true
          },
          {
            'type': 'infoField',
            'inputType': 'dropdown',
            'options': currencyCodes,
            'labelWidth': '80%',
            'num': '100',
            'label': '• If an election has been made to use a functional currency (see attached instructions), state the elected functional currency code'
          },
          {
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'labelWidth': '80%',
            'num': 'amendedIndicator',
            'init': false,
            'label': '• If this is an amended return check this box. '
          }
        ]
      },
      {
        header: 'Identification',
        rows: [
/*          {
            'type': 'table',
            'num': '1080'
          },*/
          {
            'type': 'table',
            'num': '1090'
          },
/*          {
            'type': 'table',
            'num': '1091'
          },*/
/*          {
            'type': 'table',
            'num': '1200'
          },*/
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'inputType': 'address',
            'add1Num': '104',
            'add2Num': '105',
            'provNum': '106',
            'cityNum': '107',
            'countryNum': '108',
            'postalCodeNum': '109'
          },
          {
            'label': '<b>For what taxation year are you filing this form?</b>',
            'labelClass': 'fullLength text-center'
          },
          {
            type: 'infoField',
            inputType: 'dateRange',
            label1: 'Tax year-start',
            label2: 'Tax year-end',
            source1: 'cp-tax_start',
            source2: 'cp-tax_end',
            num: '110',
            num2: '111',
            disabled: true
          }
        ]
      },
      {
        rows: [
          {
            type: 'splitTable', fieldAlignRight: true,
            side1: [
              {
                label: 'If the total cost of all specified foreign property held at any time during the year exceeds ' +
                '$100,000 but was less than $250,000, you are required to complete either Part A or Part B',
              },
              {
                type: 'infoField',
                inputType: 'singleCheckbox',
                disabled: false,
                num: '2000'
              }
            ],
            side2: [
              {
                label: 'If the total cost of all specified foreign property held at any time during the year was ' +
                '$250,000 or more, you are required to complete Part B'
              },
              {
                type: 'infoField',
                inputType: 'singleCheckbox',
                disabled: false,
                num: '2001'
              }]
          }
          /*          {
                      type: 'table',
                      num: '1337'
                    }*/
        ]
      },
      {
        forceBreakAfter: true,
        header: 'Part A: Simplified reporting method',
        showWhen: {
          fieldId: '2000',
          compare: {is: true}
        },
        rows: [
          {
            'label': 'For each type of property that applies to you, check the appropriate box.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Type of property:',
            'labelClass': 'bold'
          },
          {
            'label': 'Funds held outside Canada',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'num': '2002'
          },
          {
            'label': 'Shares of non-resident corporations (other than foreign affiliates)',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'num': '2003'
          },
          {
            'label': 'Indebtedness owed by non-resident',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'num': '2004'
          },
          {
            'label': 'Interests in non-resident trusts',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'num': '2005'
          },
          {
            'label': 'Real property outside Canada (other than personal use and real estate used in an active business)',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'num': '2006'
          },
          {
            'label': 'Other property outside Canada',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'num': '2010'
          },
          {
            'label': 'Property held in an account with a Canadian registered securities dealer or a Canadian trust company',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'num': '2007'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Country code:',
            'labelClass': 'bold'
          },
          {
            'label': 'Select the top three countries based on the maximum cost amount of specified foreign property held during the year. Enter the country codes in the boxes below:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1210'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Income from all specified foreign property',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '2008'
                }
              },
              null
            ]
          },
          {
            'label': 'Gain(loss) from the disposition from all specified foreign property',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '2009'
                }
              },
              null
            ]
          }
        ]
      },
      {
        forceBreakAfter: true,
        header: 'Part B: Detailed reporting method',
        showWhen: {
          or: [{
            fieldId: '2000',
            compare: {is: true}
          }, {
            fieldId: '2001',
            compare: {is: true}
          }]
        },
        rows: [

          {
            'label': 'Part B: Detailed reporting method',
            'labelClass': 'bold'
          },
          {
            'label': 'Categories of specified foreign property',
            'labelClass': 'bold'
          },
          {
            'label': 'In each of the tables below, provide the required details of each specified foreign property held at any time during the particular tax year. If you need additional space, please attach a separate sheet of paper using the same format as the tables. ',
            'labelClass': 'fullLength'
          },
          {
            'label': 'A taxpayer who held specified foreign property with a Canadian registered securities dealer or a Canadian trust company is permitted to report the aggregate amount, on a country by country basis, of all such property in Category 7, Property held in an account with a Canadian registered securities dealer or a Canadian trust company. See attached instructions for Category 7 for details as to how to report under this method. ',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '1. Funds held outside Canada',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'num': '1010'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '2. Shares of non-resident corporations (other than foreign affiliates)',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'num': '1020'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '3. Indebtedness owed by non-resident',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'num': '1030'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '4. Interests in non-resident trusts',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'num': '1040'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '5. Real property outside Canada (other than personal use and real estate used in an active business)',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'num': '1050'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '6. Other property outside Canada',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'num': '1060'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '7. Property held in an account with a Canadian registered securities dealer or a Canadian trust company',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'num': '1070'
          }
        ]
      },
      {
        'header': 'Certification',
        'rows': [
          {
            type: 'splitTable', fieldAlignRight: true,
            side1: [
              {
                'label': 'I certify that the information given on this form is, to my knowledge, correct and complete, and fully discloses the reporting taxpayer\'s foreign property and related information.',
                'labelClass': 'fullLength bold'
              },
              {
                'label': 'Given name',
                'num': '914',
                'type': 'infoField'
              },
              {
                'label': 'Family name',
                'num': '915',
                'type': 'infoField'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Sign here (It is a serious offence to file a false statement.)',
                'labelClass': 'fullLength bold'
              },
              {
                'type': 'infoField',
                'inputType': 'textArea',
                'num': '916'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'Position/title',
                'num': '903'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'Telephone number',
                'num': '904',
                'telephoneNumber': true,
                format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
                validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'Date (MM/DD/YYYY)',
                'num': '905',
                'inputType': 'date'
              }
            ],
            side2: [
              {
                'label': 'If someone other than the taxpayer or the partnership prepared this form, provide their:',
                'labelClass': 'fullLength bold'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'label': 'Print name',
                'num': '906'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'inputType': 'address',
                'add1Num': '907',
                'add2Num': '908',
                'provNum': '909',
                'cityNum': '910',
                'countryNum': '911',
                'postalCodeNum': '912'
              },
              {
                'label': 'Telephone number',
                type: 'infoField',
                inputType: 'custom',
                format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
                'num': '913',
                'telephoneNumber': true,
                validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
              }
            ]
          }
        ]
      }
    ]
  });
})();
