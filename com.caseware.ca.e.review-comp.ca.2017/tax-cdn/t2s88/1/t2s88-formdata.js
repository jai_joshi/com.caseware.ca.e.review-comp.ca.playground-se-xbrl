(function() {
  'use strict';

  wpw.tax.global.formData.t2s88 = {
    formInfo: {
      abbreviation: 'T2S88',
      title: 'Internet Business Activities',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      //subTitle: '(2013 and later tax years)',
      showCorpInfo: true,
      schedule: 'Schedule 88',
      formFooterNum: 'T2 SCH 88 E',
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'File this schedule if your corporation earns income from one or more webpages or websites.'
            },
            {
              label: 'You may earn income from your webpages or websites if:',
              sublist: [
                'you sell goods and/or services on your own pages or websites. You may have a shopping' +
                ' cart and process payment transactions yourself or through a third-party service;',
                'your site doesn’t support transactions but your customers call, complete, and submit a form,' +
                ' or email you to make a purchase, order, booking, and others;',
                'you sell goods and/or services on auction, marketplace, or similar websites operated by' +
                ' others; or'
              ]
            },
            {
              label: 'you earn income from advertising, income programs, or traffic your site generates.' +
              ' For example:',
              sublist: [
                'static advertisements you place on your site for other businesses',
                'affiliate programs',
                'advertising programs such as Google AdSense or Microsoft adCentre',
                'other types of traffic programs.'
              ]
            },
            {
              label: 'Also file this schedule if you don’t have a website but you have created a profile or other page' +
              ' describing your business on blogs, auction, market place, or any other portal or directory ' +
              'websites from which you earn income.'
            },
            {
              label: 'File this schedule with your <i>T2 – Corporation Income Tax Return</i>'
            }
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        rows: [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'How many Internet webpages or websites does your corporation earn income from?',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100',
                  'require': {
                    'compare': {
                      'between': [
                        0,
                        999999
                      ]
                    }
                  },
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,6}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Provide the Internet webpage or website addresses (also known as URL addresses)*:',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '105'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'What is the percentage of the corporation\'s gross revenue generated from the Internet in comparison to the corporation\'s total gross revenue?',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '120',
                  'decimals': 3,
                  'validate': {
                    'and': [
                      'percent',
                      {
                        'or': [
                          {
                            'length': {
                              'min': '1',
                              'max': '6'
                            }
                          },
                          {
                            'check': 'isEmpty'
                          }
                        ]
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: '* If you have more than five websites, enter the addresses of those that generate the most ' +
            'Internet income. If you don’t have a website but you have created a profile or other page describing ' +
            'your business on blogs, auction, market place, or any other portal or directory websites, ' +
            'enter the addresses of the pages if they generate income.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  };
})();