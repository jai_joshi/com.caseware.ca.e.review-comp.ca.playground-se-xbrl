(function() {

  wpw.tax.create.calcBlocks('t2s88', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      var rowsLen = field('105').size().rows;

      for (var rIndex = 0; rIndex < rowsLen; rIndex++) {
        var site = field('105').getRow(rIndex)[1];
        if (angular.isDefined(site.get()) && site.get().indexOf('http://') == -1 && site.get() != '') {
          site.assign('http://' + site.get());
          site.disabled(false);
        }
      }
    });
  });
})();
