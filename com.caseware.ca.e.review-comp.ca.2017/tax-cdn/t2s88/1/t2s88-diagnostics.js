(function() {
  wpw.tax.create.diagnostics('t2s88', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0880050', common.prereq(function(tools) {
      var yearEnd = tools.field('cp.tax_end').get() || 0;
      return tools.field('t2j.180').isYes() &&
          wpw.tax.utilities.dateCompare.greaterThan(yearEnd, {day: 30, month: 6, year: 2014})
    }, common.requireFiled('T2S88')));

    diagUtils.diagnostic('088.120', common.prereq(common.requireFiled('T2S88'),
        function(tools) {
          return tools.field('120').require(function() {
            return this.get() <= 100;
          })
        }));

    diagUtils.diagnostic('088.100', common.prereq(common.requireFiled('T2S88'),
        function(tools) {
          var websiteCount = 0;
          return tools.field('100').require(function() {
            var table = tools.field('105');
            tools.checkAll(table.getRows(), function(row) {
              if (row[1].isFilled())
                websiteCount++;
            });
            return (this.get() == websiteCount || (this.get() > 5 && websiteCount == 5));
          })
        }));
  });
})();
