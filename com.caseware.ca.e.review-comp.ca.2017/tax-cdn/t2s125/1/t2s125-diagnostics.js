(function() {
  wpw.tax.create.diagnostics('t2s125', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var tableNums = ['100', '200', '150', '300', '400', '500', '600'];
    var negativeOnlyCodes = ['8500', '8501', '8502', '8503'];
    var filledGIFI = [];

    diagUtils.diagnostic('125.farmingCheck', common.prereq(common.and(
        common.requireFiled('T2S125'),
        function(tools) {
          var totalFields = [tools.field('9659').get(), tools.field('9898').get(), tools.field('9899').get()];
          for (var a in totalFields) {
            if (checkTotals(totalFields[a]))
              return true;
          }
          return false;
        }), function(tools) {
      return tools.field('type').require(function() {
        return this.get() == 'both' || this.get() == 'F';
      });
    }));

    diagUtils.diagnostic('125.overMillion', common.prereq(common.and(
        common.requireFiled('T2S125'),
        function(tools) {
          return tools.field('cp.tax_end').getKey('year') > 2009 &&
              (tools.field('cp.080').isYes() ||
                  tools.field('cp.122').get() == '13' ||
                  (tools.field('cp.079').get() == ' ' || angular.isUndefined(tools.field('cp.079').get())) ||
                  angular.isUndefined(tools.field('cp.085').get()));
        }), function(tools) {
      return tools.checkAll(tools.field('8299').get(), function(val8299, key) {
        return tools.field('8299').require(function() {
          if (val8299 && tools.field('9659').getKey(key))
            return (parseFloat(val8299) + parseFloat(tools.field('9659').getKey(key))) < 1000000;
          else return true;
        })
      })
    }));

    diagUtils.diagnostic('125.8299', common.prereq(common.and(
        common.requireFiled('T2S125'),
        function(tools) {
          return tools.field('9659').getKey('0') == 0 || angular.isUndefined(tools.field('9659').getKey(0));
        }), function(tools) {
      return tools.field('8299').require('isFilled');
    }));

    diagUtils.diagnostic('125.9368', common.prereq(common.and(
        common.requireFiled('T2S125'),
        function(tools) {
          return tools.field('9898').getKey('0') == 0 || angular.isUndefined(tools.field('9898').getKey(0));
        }), common.check(['9368'], 'isFilled')));

    diagUtils.diagnostic('125.9898Or9368', common.prereq(common.requireFiled('T2S125'), common.check(['9368', '9898'], 'isFilled')));

    diagUtils.diagnostic('1250001', common.prereq(common.requireFiled('T2S125'),
        function(tools) {
          return tools.requireAll(tools.list(['8299', '9368']), 'isFilled') ||
              tools.requireAll(tools.list(['9659', '9898']), 'isFilled')
        }));

    //Error code 1250002 is implemented in s140

    //Error code 1250003: if more than one s125 is reported, 9970-9999 codes should be empty (in s125)
    //Only 9998 needs to be checked in s125, other values checked in s140
    diagUtils.diagnostic('1250003', common.prereq(common.and(
        common.requireFiled('T2S125'),
        function() {
          return wpw.tax.form.allRepeatForms('t2s125').length > 1
        }), function(tools) {
      return tools.requireOne(tools.field('9998'), function() {
        return this.getKey(0) == 0 || angular.isUndefined(this.getKey(0));
      })
    }));

    diagUtils.diagnostic('1250004', common.prereq(common.and(
        common.requireFiled('T2S125'),
        function() {
          return wpw.tax.form.allRepeatForms('t2s125').length > 1;
        }), function(tools) {
      return tools.field('003').require('isNonZero');
    }));

    diagUtils.diagnostic('125.requiredNegative', common.prereq(common.requireFiled('T2S125'),
        function(tools) {
          var diagCheck = true;
          return tools.checkAll(negativeOnlyCodes, function(value) {
            if (tools.field(value).get()) {
              for (var i = 0; i < Object.keys(tools.field(value).get()).length; i++) {
                var formId = tools.field(value).fieldObj.formId;
                var gifiCodeRows = wpw.tax.gifiService.getCurrentCodes(formId, '200', tools.field(value).fieldPath);
                //If the key exists and there are no duplicate gifi codes
                if (tools.field(value).getKey(i) && gifiCodeRows && gifiCodeRows.length < 2) {
                  for (var k = 0; k < gifiCodeRows.length; k++) {
                    if (tools.field('200').cell(gifiCodeRows, i + 2).isPositive()) {
                      diagCheck = false;
                      tools.field('200').cell(gifiCodeRows, i + 2).require('isNegative');
                    }
                  }
                }
              }
              return diagCheck;
            } else return true;
          })
        }));

    diagUtils.diagnostic('100.foreignCheck', common.prereq(function(tools) {
      return tools.checkMethod(tools.list(['t2s125.8091', 't2s125.8097']), function() {
        if(!wpw.tax.utilities.isEmptyOrNull(this.getKey(0))) {
          filledGIFI.push(this);
          return true;
        }
        return false;
      })
    }, function(tools) {
      return tools.checkAll(filledGIFI, function(field) {
        var gifiCodeRows = wpw.tax.gifiService.getCurrentCodes('t2s125', '150', field.fieldObj.fieldId);
        return tools.field('150').cell(gifiCodeRows, 2).require('isZero') || tools.requireOne(tools.list(['t1135.2000', 't1135.2001']), 'isChecked');
      })
    }));

    function checkTotals(field) {
      if (field && Object.keys(field).length > 0) {
        for (var a in field) {
          if (field[a] != 0) {
            return true;
          }
        }
      }
      return false;
    }

    function convertToSet(arr) {
      var set = {};
      for (var i = 0; i < arr.length; i++) {
        var item = arr[i];
        set[item] = set[item] + 1 || 1;
      }
      return set;
    }

    function checkDuplicates(tableNum) {
      diagUtils.diagnostic('100.duplicate', function(tools) {
        var table = tools.field(tableNum);
        var codeSet = convertToSet(wpw.tax.gifiService.getCurrentCodes(tools.form().valueFormId, tableNum));
        return tools.checkAll(table.getRows(), function(row) {
          var code = row[0] ? row[0].valueObj.label : null;
          if (!wpw.tax.utilities.isEmptyOrNull(code) && codeSet[code] > 1) {
            row[1].mark();
            return false;
          }
          return true;
        });
      });
    }

    function checkHasCode(tableNum) {
      diagUtils.diagnostic('gifi.noCode', function(tools) {
        var table = tools.field(tableNum);
        return tools.checkAll(table.getRows(), function(row) {
          if (tools.checkMethod(row.slice(2), 'isNonZero') && !row[0].valueObj.label) {
            row[1].mark();
            return false;
          }
          return true;
        });
      });
    }

    for (var table in tableNums) {
      checkDuplicates(tableNums[table]);
      checkHasCode(tableNums[table]);
    }

  });
})();