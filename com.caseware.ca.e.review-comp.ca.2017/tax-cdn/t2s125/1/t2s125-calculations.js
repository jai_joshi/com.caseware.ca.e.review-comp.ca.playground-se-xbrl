(function() {
  wpw.tax.create.calcBlocks('t2s125', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {

      function setMin0(formId, tableNum, gifiCode, yearIndex) {
        var value = Math.max(calcUtils.gifiValue(formId, tableNum, gifiCode, yearIndex), 0);
        calcUtils.field(gifiCode).setKey(yearIndex, value);
        var cell = calcUtils.form(formId).field(tableNum).cellByCode(gifiCode, yearIndex);
        if (cell) {
          cell.assign(value);
          cell.disabled(false);
        }
      }

      for (var yearIndex = 0; yearIndex < 5; yearIndex++) {
        setMin0('T2S125', '200', '8300', yearIndex);
        setMin0('T2S125', '200', '8301', yearIndex);
        setMin0('T2S125', '200', '8302', yearIndex);
        setMin0('T2S125', '200', '8303', yearIndex);
        var gifi9970 = (field('9369').getKey(yearIndex) || 0) + (field('9899').getKey(yearIndex) || 0);
        field('550').cell(0, yearIndex + 2).assign(gifi9970);
        field('550').cell(0, yearIndex + 2).config('cannotOverride', true);
        field('9970').setKey(yearIndex, gifi9970);
      }

    });
  });
})();
