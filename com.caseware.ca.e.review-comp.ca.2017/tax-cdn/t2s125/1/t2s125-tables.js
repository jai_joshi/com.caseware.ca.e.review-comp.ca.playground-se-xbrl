(function () {
  wpw.tax.global.tableCalculations.t2s125 = {
    '100': {
      'type': 'gifiWorkchart',
      'gifiCategory': 'IncomeStatement',
      'gifiCodes': 'salesGS',
      hasTotals: true,
      'header': 'Sales of Goods and Services',
      'fixedGifiRows': [
        {
          'code': '8000',
          'description': 'Trade sales of goods and services'
        }
      ],
      'range': [8000, 8089],
      'totalCode': '8089',
      'totalDescription': 'Total Sales of Goods and Services',
      'numberOfYearsToShow': 5
    },
    '150': {
      'type': 'gifiWorkchart',
      'gifiCategory': 'IncomeStatement',
      'gifiCodes': 'revenue',
      hasTotals: true,
      'header': 'Other Revenue',
      'fixedGifiRows': [
        {
          'code': '8210',
          'description': 'Realized gains/losses on disposal of assets'
        },
        {
          'code': '8211',
          'description': 'Realized gains/losses on sale of investments'
        },
        {
          'code': '8212',
          'description': 'Realized gains/losses on sale of resource properties'
        },
        {
          'code': '8232',
          'description': 'Income/losses of subsidiaries/affiliates'
        },
        {
          'code': '8235',
          'description': 'Income/loss of partnerships'
        }],
      'range': [8090, 8299],
      'totalCode': '8298-1',
      'totalDescription': 'Total Other Revenue',
      'hideTotalCode': true,
      'postTotalRows': [{
        'code': '8299',
        'description': 'Total Non-farming Revenue',
        'dependantCodes': [8089, '8298-1'],
        'operation': 'addition'
      }],
      'numberOfYearsToShow': 5
    },
    '200': {
      'type': 'gifiWorkchart',
      'gifiCategory': 'IncomeStatement',
      'gifiCodes': 'costSale',
      hasTotals: true,
      'header': 'Cost of Sales',
      'fixedGifiRows': [{
        'code': '8300',
        'description': 'Opening Inventory'
      }],
      // 'endFixedGifiRows': [
      //   {code: '8500', description: 'Closing Inventory', isCredit: true},
      //   {code: '8501', description: 'Closing inventory - Finished goods', isCredit: true},
      //   {code: '8502', description: 'Closing inventory - Raw materials', isCredit: true},
      //   {code: '8503', description: 'Closing inventory - Goods in process', isCredit: true}
      // ],
      'range': [8300, 8519],
      'totalCode': '8518',
      'totalDescription': 'Total Cost of Sales',
      'postTotalRows': [{
        'code': '8519',
        'description': 'Gross Profit / Loss',
        'dependantCodes': [8089, 8518],
        'operation': 'subtraction'
      }],
      'numberOfYearsToShow': 5
    },
    '300': {
      'type': 'gifiWorkchart',
      'gifiCategory': 'IncomeStatement',
      'gifiCodes': 'opExpenses',
      hasTotals: true,
      'header': 'Operating Expenses',
      'fixedGifiRows': [
        {
          'code': '8570',
          'description': 'Amortization of intangible assets'
        },
        {
          'code': '8650',
          'description': 'Amortization of natural resource assets'
        },
        {
          'code': '8670',
          'description': 'Amortization of tangible assets'
        },
        {
          'code': '9282',
          'description': 'Research and development'
        }],
      'range': [8520, 9369],
      'totalCode': '9367',
      'totalDescription': 'Total Operating Expenses',
      'postTotalRows': [
        {
          'code': '9368',
          'description': 'Total Non-farming Expenses',
          'dependantCodes': [8518, 9367],
          'operation': 'addition'
        },
        {
          'code': '9369',
          'description': 'Net non-farming income',
          'dependantCodes': [8299, 9368],
          'operation': 'subtraction'
        }],
      'numberOfYearsToShow': 5
    },
    '400': {
      'type': 'gifiWorkchart',
      'gifiCategory': 'IncomeStatement',
      'gifiCodes': 'farmingRevenue',
      hasTotals: true,
      'header': 'Farming Income',
      'range': [9370, 9659],
      'totalCode': '9659',
      'totalDescription': 'Total Farming Revenue',
      'numberOfYearsToShow': 5
    },
    '500': {
      'type': 'gifiWorkchart',
      'gifiCategory': 'IncomeStatement',
      'gifiCodes': 'farmingExpenses',
      hasTotals: true,
      'header': 'Farming Expenses',
      'range': [9660, 9899],
      'totalCode': '9898',
      'totalDescription': 'Total Farming Expenses',
      'postTotalRows': [{
        'code': '9899',
        'description': 'Net Farming Income',
        'dependantCodes': [9659, 9898],
        'operation': 'subtraction'
      }],
      'numberOfYearsToShow': 5
    },
    '550': {
      'type': 'gifiWorkchart',
      'numberOfYearsToShow': 5,
      'fixedRows': true,
      'range': [9970, 9970],
      'fixedGifiRows': [{
        'code': '9970',
        'description': 'Net income/loss before taxes and extraordinary items'
      }],
      "keepButtonsSpace": true
    },
    '600': {
      'type': 'gifiWorkchart',
      'gifiCodes': 'otherComprehensiveIncome',
      hasTotals: true,
      'fixedRows': true,
      'keepButtonsSpace': true,
      'range': [7000, 7020],
      'totalDescription': 'Other Comprehensive Income',
      'totalCode': '9998',
      'fixedGifiRows': [
        {
          'code': '7000',
          'description': 'Revaluation surplus'
        },
        {
          'code': '7002',
          'description': 'Defined benefit gains/losses'
        },
        {
          'code': '7004',
          'description': 'Foreign operation translation gains/losses'
        },
        {
          'code': '7006',
          'description': 'Equity instruments gains/losses'
        },
        {
          'code': '7008',
          'description': 'Cash flow hedge effective portion gains/losses'
        },
        {
          'code': '7010',
          'description': 'Income Tax Relating to Components of Other Comprehensive Income'
        },
        {
          'code': '7020',
          'description': 'Miscellaneous other comprehensive income'
        }],
      'numberOfYearsToShow': 5
    },
    '050': {
      'fixedRows': true,
      'infoTable': false,
      hasTotals: true,
      'columns': [
        {
          'header': 'Operating Name <br> if different from the corporations\' legal name',
          'type': 'text',
          'rf': true,
          cellClass: 'alignLeft',
          'tn': '0001',
          'extendedTN': true
        },
        {
          'header': 'Description of operation <br> if filing multiple Schedule 125\'s',
          'type': 'text',
          'rf': true,
          cellClass: 'alignLeft',
          'tn': '0002',
          'extendedTN': true
        },
        {
          'header': 'Sequence Number <br> if filing multiple Schedule 125\'s',
          'width': '190px',
          'type': 'text',
          'rf': true,
          cellClass: 'alignLeft',
          'tn': '0003',
          "disabled": true,
          'extendedTN': true
        }
      ],
      "cells": [
        {
          "0": {
            'num': '0001'
          },
          "1": {
            'num': '0002'
          },
          "2": {
            'num': '0003'
          }
        }
      ]
    }
  }
})();
