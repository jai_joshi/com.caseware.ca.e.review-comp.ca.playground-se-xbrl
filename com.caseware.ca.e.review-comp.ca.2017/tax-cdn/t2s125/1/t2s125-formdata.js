(function() {

  wpw.tax.global.formData.t2s125 = {
    formInfo: {
      abbreviation: 'T2S125',
      isRepeatForm: true,
      repeatFormData: {
        titleNum: '0001'
      },
      title: 'Income Statement Information',
      schedule: 'Schedule 125',
      code: 'Code 1001',
      headerImage: 'federal',
      category: 'GIFI',
      disclaimerShowWhen: {fieldId: 'cp.184', check: 'isYes'},
      disclaimerTextField: {formId: 'cp', fieldId: '183'},
      formFooterNum: 'T2 SCH 125 E (14)',
      description: [
        {
          'type': 'list',
          'items': [
            'Use this schedule to report the corporation\'s income statement information.',
            'For more information, see Guide RC4088, <i>General Index of Financial Information (GIFI)</i> and T4012,<i>T2 Corporation – Income Tax Guide.</i>',
            'If you need more space, attach additional schedules.'
          ]
        }
      ]
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {
            type: 'table', num: '050'
          },
          // {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {
            type: 'infoField', inputType: 'dropdown', num: 'type',
            label: 'What kind of information is provided in this Income Statement?',
            init: 'NF',
            options: [
              {option: 'Non-farming', value: 'NF'},
              {option: 'Farming', value: 'F'},
              {option: 'Farming and Non-farming', value: 'both'}
            ]
          }
        ]
      },
      {
        forceBreakAfter:true,
        header: 'Non-farming information',
        showWhen: {fieldId: 'type', compare: {isNot: 'F'}},
        rows: [
          // {labelClass: 'fullLength'},
          {
            type: 'gifiWorkchart', gifiCategory: 'IncomeStatement', gifiCodes: 'salesGS',
            num: '100', header: 'Sales of Goods and Services',
            fixedGifiRows: [
              {code: '8000', description: 'Trade sales of goods and services'}
            ],
            range: [8000, 8089],
            totalCode: '8089', totalDescription: 'Total Sales of Goods and Services'
          },

          // {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},

          {
            type: 'gifiWorkchart', gifiCategory: 'IncomeStatement', gifiCodes: 'costSale',
            num: '200', header: 'Cost of Sales'
          },

          // {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},

          {
            type: 'gifiWorkchart', gifiCategory: 'IncomeStatement', gifiCodes: 'revenue',
            num: '150', header: 'Other Revenue'
          },

          {labelClass: 'fullLength'},
          // {labelClass: 'fullLength'},

          {
            type: 'gifiWorkchart', gifiCategory: 'IncomeStatement', gifiCodes: 'opExpenses',
            num: '300', header: 'Operating Expenses'
          }
        ]
      },
      {
        header: 'Farming Information',
        showWhen: {fieldId: 'type', compare: {isNot: 'NF'}},
        rows: [
          {labelClass: 'fullLength'},

          {
            type: 'gifiWorkchart', gifiCategory: 'IncomeStatement', gifiCodes: 'farmingRevenue',
            num: '400', header: 'Farming Income',
            range: [9370, 9659],
            totalCode: '9659', totalDescription: 'Total Farming Revenue'
          },

          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},

          {
            type: 'gifiWorkchart', gifiCategory: 'IncomeStatement', gifiCodes: 'farmingExpenses',
            num: '500', header: 'Farming Expenses'
          }
        ]
      },
      {
        rows: [
          {
            type: 'gifiWorkchart',
            num: '550'
          }
        ]
      },
      {
        header: 'Other Comprehensive Income',
        rows: [
          {type: 'gifiWorkchart', gifiCodes: 'otherComprehensiveIncome', num: '600'}
        ]
      }
    ]
  }
  ;
})();
