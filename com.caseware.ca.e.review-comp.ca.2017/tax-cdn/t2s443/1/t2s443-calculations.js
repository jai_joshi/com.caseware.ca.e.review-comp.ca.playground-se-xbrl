(function() {

  wpw.tax.create.calcBlocks('t2s443', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //Part 1 Cals
      var taxableIncomeAllocation = calcUtils.getTaxableIncomeAllocation('YT');
      field('500').assign(taxableIncomeAllocation.provincialTI);
      field('112').assign(taxableIncomeAllocation.allProvincesTI);

      field('500').source(taxableIncomeAllocation.provincialTISourceField);
      field('112').source(taxableIncomeAllocation.allProvincesTISourceField);

      field('111').assign(field('500').get());

      calcUtils.getGlobalValue('501', 'T2J', '400');
      calcUtils.getGlobalValue('502', 'T2J', '405');
      calcUtils.getGlobalValue('503', 'T2J', '427');
      calcUtils.min('504', ['501', '502', '503']);
      field('110').assign(field('504').get());


      calcUtils.divide('113', '111', '112', '110');
      calcUtils.subtract('510', '500', '113');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //rates
      calcUtils.getGlobalValue('953', 'ratesYt', '111');
      calcUtils.getGlobalValue('1003', 'ratesYt', '112');
      calcUtils.getGlobalValue('706', 'ratesYt', '113');
      calcUtils.getGlobalValue('711', 'ratesYt', '114');
      //Part 2 Calcs
      //amount f
      calcUtils.equals('704', '510');
      calcUtils.equals('709', '510');
      //fiscal period
      var date = wpw.tax.date(2017, 7, 1);
      var dateComparisons = calcUtils.compareDateAndFiscalPeriod(date);
      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();
      field('955').assign(daysFiscalPeriod);
      field('1005').assign(daysFiscalPeriod);
      field('708').assign(daysFiscalPeriod);
      field('713').assign(daysFiscalPeriod);
      field('952').assign(dateComparisons.daysBeforeDate);
      field('705').assign(dateComparisons.daysBeforeDate);
      field('1002').assign(dateComparisons.daysAfterDate);
      field('710').assign(dateComparisons.daysAfterDate);
      //amount g
      calcUtils.equals('951', '113');
      calcUtils.equals('1001', '113');
      //rates calculation
      field('954').assign(field('951').get() * field('952').get() / field('955').get() * field('953').get() / 100);
      field('1004').assign(field('1001').get() * field('1002').get() / field('1005').get() * field('1003').get() / 100);
      field('810').assign(field('954').get() + field('1004').get());
      field('707').assign(field('704').get() * field('705').get() / field('708').get() * field('706').get() / 100);
      field('712').assign(field('709').get() * field('710').get() / field('713').get() * field('711').get() / 100);
      field('811').assign(field('707').get() + field('712').get());
      field('812').assign(field('810').get() + field('811').get());
    });

  });
})();
