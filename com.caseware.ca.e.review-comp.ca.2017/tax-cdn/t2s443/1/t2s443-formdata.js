(function () {

  wpw.tax.create.formData('t2s443', {
    formInfo: {
      abbreviation: 't2s443',
      title: 'Yukon Corporation Tax Calculation',
      schedule: 'Schedule 443',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 443 E (17)',
      description: [
        {
          type: 'list',
          items: [
            'Use this schedule if your corporation had a permanent establishment (as defined under section 400 of the federal <i>Income Tax Regulations</i>) in Yukon and had taxable income earned in the year in Yukon.',
            'This schedule is a worksheet only and is not required to be filed with your <i>T2 Corporation Income Tax Return</i>.'
          ]
        }
      ],
      category: 'Yukon Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Calculation of income subject to Yukon lower and higher tax rates',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable income for Yukon *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '500'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Income eligible for Yukon lower tax rate:',
            'labelClass': 'bold'
          },
          {
            'label': 'Amount from line 400 of the T2 return ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '501'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 405 of the T2 return ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '502'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 427 of the T2 return ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '503'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'label': 'Amount B, C, or D, whichever is the least ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '504'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'labelclass': 'fulllength'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '100'
          },
          {
            'labelclass': 'fulllength'
          },
          {
            'label': ' <b> Income subject to Yukon higher tax rate </b>(amount A <b>minus </b>amount F)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline':'double',
                'input': {
                  'num': '510'
                }
              }
            ],
            'indicator': 'G'
          },
          // {
          //   'label': 'Enter amount F and/or amount G on the applicable line(s) in Part 2'
          // },
          // {
          //   'labelclass': 'fulllength'
          // },
          {
            'label': '* * If the corporation has a permanent establishment only in Yukon, enter the taxable income from line 360 of the T2 return. Otherwise, enter the taxable income allocated to Yukon from column F in Part 1 of Schedule 5, <i>Tax Calculation Supplementary – Corporations</i>.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '** Includes the territories and the offshore jurisdictions for Nova Scotia and Newfoundland and Labrador.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of Yukon tax before credits',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'labelClass'
          },
          {
            'label': 'Yukon tax at the lower rate:',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '950'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          // {
          //   'type': 'table',
          //   'num': '1006'
          // },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Yukon tax at the lower rate</b> (amount 1 <b>plus</b> amount 2)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '810'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': 'Yukon tax at the higher rate:',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '600'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '700'
          },
          {
            'label': '<b>Yukon tax at the higher rate</b> (amount 3 <b>plus</b> amount 4)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '811'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Yukon tax before credits </b> (amount H <b>plus</b> amount I)*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '812'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '*  If the corporation has a permanent establishment in more than one jurisdiction or is claiming a Yukon tax credit, enter amount J on line 245 of Schedule 5. Otherwise, enter it on line 760 of the T2 return.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  });
})();
