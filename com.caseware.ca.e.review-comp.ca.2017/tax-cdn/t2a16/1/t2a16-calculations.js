(function () {

  wpw.tax.create.calcBlocks('t2a16', function (calcUtils) {
    calcUtils.calc(function (calcUtils, field) {
      calcUtils.getGlobalValue('002', 'T661', '400');
      field('004').assign(field('T661.429').get() + field('T661.431').get() + field('T661.432').get());
      calcUtils.getGlobalValue('006', 'T661', '435');
      calcUtils.getGlobalValue('008', 'T661', '440');
      calcUtils.getGlobalValue('010', 'T661', '445');
      calcUtils.getGlobalValue('012', 'T661', '450');
      calcUtils.getGlobalValue('014', 'T661', '452');
      calcUtils.getGlobalValue('015', 'T661', '453');
      field('016').assign(
          field('002').get() -
          (field('004').get() + field('006').get() + field('008').get()) +
          field('010').get() +
          field('012').get() +
          field('014').get() +
          field('015').get()
      );

      if (field('016').get() >= 0) {
        field('018').assign(field('016').get());
        field('020').assign(field('018').get());
        field('022').assign(field('018').get() - field('020').get());
      } else {
        field('018').assign(0);
        field('020').assign(0);
        field('022').assign(0);
      }

    })
  });
})();
