(function () {
  wpw.tax.create.formData('t2a16', {
    formInfo: {
      abbreviation: 't2a16',
      title: 'Alberta Scientific Research Expenditures',
      schedule: 'Schedule 16',
      formFooterNum: 'AT238 (Jul-12)',
      // headerImage: 'canada-alberta',
      showCorpInfo: true,
      category: 'Alberta Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: 'This schedule is required if the opening balance or the claim for Alberta purposes differs from that for federal purposes.',
            labelClass: 'bold'
          },
          {
            label: 'Report all monetary amounts in dollars; DO NOT include cents. Show negative amounts in brackets ( ).'
          }
        ]
      },
      {
        rows: [
          {
            "label": "Allowable SR&ED expenditures (federal schedule 32 (T661) line 400)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "002"
                },
                "padding": {
                  "type": "tn",
                  "data": "002"
                }
              }
            ]
          },
          {
            label: 'Deduct:'
          },
          {
            "label": "Government and non-government assistance for expenditures included in above line (use federal schedule 32 (T661) line 430 from 2007 and prior versions; use sum of lines 429, 431 and 432 from 2008 and later versions)",
            labelClass: 'indent-2',
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "004"
                },
                "padding": {
                  "type": "tn",
                  "data": "004"
                }
              }
            ]
          },
          {
            "label": "Previous year's investment tax credit (ITC) claimed for SR&ED <br>(federal schedule 32 (T661) line 435)",
            labelClass: 'indent-2',
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "006"
                },
                "padding": {
                  "type": "tn",
                  "data": "006"
                }
              }
            ]
          },
          {
            "label": "Sale of SR&ED capital assets and other deductions <br>(federal schedule 32 (T661) line 440)",
            labelClass: 'indent-2',
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "008"
                },
                "padding": {
                  "type": "tn",
                  "data": "008"
                }
              }
            ]
          },
          {label: 'Add:'},
          {
            "label": "Repayments of government and non-government assistance for SR&ED <br>(federal schedule 32 (T661) line 445)",
            labelClass: 'indent-2',
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "010"
                },
                "padding": {
                  "type": "tn",
                  "data": "010"
                }
              }
            ]
          },
          {
            "label": "Unclaimed SR&ED expenditure pool balance from the previous year",
            labelClass: 'indent-2',
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "012"
                },
                "padding": {
                  "type": "tn",
                  "data": "012"
                }
              }
            ]
          },
          {
            "label": "SR&ED expenditure pool transfer from amalgamation or wind-up of a wholly-owned subsidiary",
            labelClass: 'indent-2',
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "014"
                },
                "padding": {
                  "type": "tn",
                  "data": "014"
                }
              }
            ]
          },
          {
            "label": "Amount of ITC recaptured in the previous tax year <br>(federal schedule 32 (T661) line 453)",
            labelClass: 'indent-2',
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "015"
                },
                "padding": {
                  "type": "tn",
                  "data": "015"
                }
              }
            ]
          },
          {
            "label": "Subtotal: Line 002 - (004 + 006 + 008) + 010+ 012 + 014 + 015",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "016"
                },
                "padding": {
                  "type": "tn",
                  "data": "016"
                }
              }
            ]
          },
          {
            label: 'If this amount is positive, enter that amount on line 018.',
            labelClass: 'indent-2'
          },
          {
            label: 'If this amount is negative, <b>carry forward the negative amount to Schedule 12, line 034 </b>and enter "0" on lines 018, 020 and 022. ',
            labelClass: 'indent-2'
          },
          {
            "label": "SR&ED expenditure pool deduction available",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "018"
                },
                "padding": {
                  "type": "tn",
                  "data": "018"
                }
              }
            ]
          },
          {
            "label": "Deduct: SR&ED expenditure pool deduction claimed in the current year <br>(see Guide for more details)<br> " +
            "If other than \"0\", <b>carry this amount forward to Schedule 12, line 034</b>",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "020"
                },
                "padding": {
                  "type": "tn",
                  "data": "020"
                }
              }
            ]
          },
          {
            "label": "Unclaimed SR&ED expenditure pool deduction balance (line 018 - 020) <br>" +
            "(Use this amount as the carry forward amount for next year, line 012)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "022"
                },
                "padding": {
                  "type": "tn",
                  "data": "022"
                }
              }
            ]
          }
        ]
      }
    ]
  });
})();
