(function() {

  wpw.tax.create.formData('ratesPei', {
    formInfo: {
      abbreviation: 'ratesPei',
      title: 'Prince Edward Island Table of Rates and Values',
      showCorpInfo: true,
      description: [
        {type: 'heading', text: 'Prince Edward Island'}
      ],
      category: 'Rates Tables'
    },
    sections: [
      {
        'header': 'Schedule 321 - Prince Edward Island Corporate Investment Tax Credit',
        'rows': [
          {
            'label': 'Current year credit earned rate',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '100'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 322 - Prince Edward Island Corporation Tax Calculation ',
        'rows': [
          {
            'label': 'Lower rate from 2014 to present',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '703',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'Higher rate from 2014 to present',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '705'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      }
    ]
  });
})();
