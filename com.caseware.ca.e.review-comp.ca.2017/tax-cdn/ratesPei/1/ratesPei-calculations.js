(function() {

  wpw.tax.create.calcBlocks('ratesPei', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      field('100').assign(10);
      field('703').assign(4.5);
      field('705').assign(16);
    });
  });
})();
