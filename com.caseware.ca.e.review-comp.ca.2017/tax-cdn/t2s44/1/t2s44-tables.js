(function() {
  wpw.tax.global.tableCalculations.t2s44 = {
    '500': {
      'showNumbering': true,
      'columns': [
        {
          'header': 'Transferor corporation\'s name',
          'tn': '100',
          'width': '50%',
          'maxLength': '175',
          type: 'text'
        },
        {
          'header': 'Transferor corporation\'s<br>Business Number',
          'tn': '200',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          'header': 'Date of Transfer<br>(YYYY/MM/DD)',
          'type': 'date',
          'width':'15%',
          'tn': '300',
          'maxLength': '8'
        }
      ]
    }
  }
})();
