(function() {
  wpw.tax.create.diagnostics('t2s44', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S44'), forEach.row('500', forEach.bnCheckCol(1, true))));

    diagUtils.diagnostic('0440002', common.prereq(common.check(['t2j.163'], 'isChecked'), common.requireFiled('T2S44')));

    diagUtils.diagnostic('0440004', common.prereq(common.requireFiled('T2S44'),
        function(tools) {
          var table = tools.field('500');
          return tools.checkAll(table.getRows(), function(row) {
            var columns = [row[0], row[1], row[2]];
            if (tools.checkMethod(columns, 'isNonZero'))
              return tools.requireAll(columns, 'isNonZero');
            else return true;
          });
        }));

  });
})();
