(function() {
  'use strict';

  wpw.tax.global.formData.t2s44 = {
    formInfo: {
      abbreviation: 'T2S44',
      title: 'Non-Arm\'s Length Transactions',
      //subTitle: '(2006 and later tax years)',
      schedule: 'Schedule 44',
      code: 'Code 0601',
      formFooterNum: 'T2 SCH 44 E (07)',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          text: 'Where all or almost all (90% or more) of the assets of a non-arm\'s length corporation have been ' +
          'received in the tax year, and subsection 85(1) or (2) or 142.7(3) of the federal <i> Income ' +
          'Tax Act </i> applied for the disposition of any of the property, report the following details: '
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            type: 'table',
            num: '500'
          }
        ]
      }
    ]
  };
})();
