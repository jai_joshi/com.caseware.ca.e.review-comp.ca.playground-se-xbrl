(function() {
  'use strict';
  wpw.tax.create.formData('t2s385', {
    formInfo: {
      abbreviation: 't2s385',
      title: 'Manitoba Neighbourhoods Alive! Tax Credit ',
      //subTitle: '(2013 and later tax years)',
      schedule: 'Schedule 385',
      code: 'Code 1303',
      formFooterNum: 'T2 SCH 385 E (17) ',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'You can claim a Manitoba odour-control tax credit under section 10.2 of <i>Income Tax Act</i>' +
              ' (Manitoba) if the corporation:',
              sublist: [
                'has a permanent establishment in Manitoba; and',
                'had eligible expenditures in the current tax year or unused credit from a prior tax year'
              ]
            },
            {
              label: 'Eligible expenditures refer to the capital cost of a depreciable capital property ' +
              'acquired under the following conditions:',
              sublist: [
                'it was acquired by the corporation after April 19, 2004, and before April 12, 2017 to prevent,' +
                ' eliminate, or significantly reduce odour that arises from organic waste used or created ' +
                'in the course of the corporation\'s business in Manitoba;',
                'the property became available for use by the corporation in the tax year and before April 12, 2017;',
                'the property was not used or acquired for any use by anyone before it was acquired by the corporation;',
                'it is an item of eligible equipment (straw cannons, sewage lagoon covers and seals, ' +
                'biofiltering equipment, a storage tank or container for organic waste, spraying equipment' +
                ' for aerobic or anaerobic treatment of organic waste, a soil injector for a manure spreader),' +
                'or is declared by the minister to be a qualifying property; and',
                'it is property that is used in a process that involves aerobic or anaerobic treatment, ' +
                'composting, drying or dehydration, or fermentation of organic waste'
              ]
            },
            {
              label: '<b>Note</b>: The elimination of this credit does not impact the carry forward ' +
              'of your unused credits for eligible expenditures made before April 12, 2017.'
            },
            {
              label: 'Use this schedule to:',
              sublist: [
                'claim the credit to reduce Manitoba income tax otherwise payable in the current tax year;',
                'claim the refundable credit, if the corporation is carrying on the business of farming;',
                'calculate the credit you have available to carry forward;',
                'request a carryback of the credit;',
                'transfer a credit after an amalgamation or the wind-up of a subsidiary, as described ' +
                'in subsections 87(1) and 88(1) of the federal <b><i>Income Tax Act</i></b>;',
                'calculate the current-year credit earned from eligible expenditures allocated ' +
                'from a trust or a partnership; or',
                'renounce the current-year credit in whole or in part'
              ]
            },
            {
              label: 'An eligible expenditure must be identified on this schedule and filed no later than ' +
              '12 months after the <i>T2 Corporation Income Tax Return</i>' +
              ' is due for the tax year in which the expenditure was incurred.'
            },
            {
              label: 'For non-agricultural corporations, an unused credit earned in the current tax year is ' +
              'not refundable. The unused credit can be carried forward for 10 tax years and carried back 3 tax years.'
            },
            {
              label: 'Agricultural corporations are eligible for a refundable part of the odour-control tax credit. ' +
              'The refundable credit is the amount of credit available for the current year that is greater than ' +
              'the Manitoba tax otherwise payable for the current year.'
            },
            {
              label: 'File this schedule with your T2 return'
            }
          ]
        }
      ],
      category: 'Manitoba Forms'
    },
    sections: [
      {
        'header': 'Part 1 -  Eligible expenditures made in the current tax year',
        'rows': [
          {
            'type': 'table',
            'num': '130'
          },
          {
            'label': '<b>Total eligible expenditures made in the current tax year</b> (total of amounts A to C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '103'
                }
              }
            ],
            'indicator': 'D'
          }
        ]
      },
      {
        'header': 'Part 2 - Credit available for the year and credit available for carryforward',
        'rows': [
          {
            'label': 'Credit at the end of the preceding tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1031'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'Credit expired after 10 tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': 'Credit at the beginning of the tax year (amount a <b>minus</b> amount b)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '106',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Credit transferred on an amalgamation or the wind-up of a subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'type': 'table',
            'num': '250'
          },
          {
            'label': 'Credit renounced',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '150'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount c <b>minus</b> amount d)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '151'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '152'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Total credit available for the current tax year (total of amounts E, F, and G) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '153'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': 'Non-refundable tax credit claimed in the current year* (enter on line 607 of Schedule 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': 'Refundable tax credit for agricultural corporations (complete Part 3)<br> (enter on line 623 of Schedule 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '161',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '161'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'label': 'Credit carried back to previous tax years (complete Part 4)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '162'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'g'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts e to g)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '163'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '164'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': '<b>Closing balance - credit available for carryforward</b> (amount H <b>minus</b> amount I) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* The credit claimed in the current tax year cannot exceed the Manitoba income tax otherwise payable or the amount on line H, whichever is less.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Refundable tax credit for agricultural corporations',
        'rows': [
          {
            'label': 'Use this part only if you are a corporation carrying on the business of farming.',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Is the corporation carrying on the business of farming?',
            'type': 'infoField',
            'inputType': 'radio',
            'labelWidth': '80%',
            'num': '290',
            'init': '2'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total credit available for the current tax year (amount H)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '300'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': '<b>Deduct:</b> Manitoba income tax otherwise payable ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '301'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': '<b>Refundable tax credit</b> (amount K <b>minus</b> amount L)<br>  (enter result on line 161. Enter "0" if negative)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '302'
                }
              }
            ],
            'indicator': 'M'
          }
        ]
      },
      {
        'header': 'Part 4 - Request for carryback of credit',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Use this part only if you are a non-agricultural corporation',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Complete this part to ask for a carryback of a current-year credit earned. The amount carried back cannot exceed the Manitoba tax otherwise payable in that prior year. ',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1000'
          }
        ]
      },
      {
        'header': 'Part 5 - Analysis of credit available for carryforward by year of origin',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'You can complete this part to show all the credits from previous tax years available for carryforward, by year of origin. This will help you determine the amount of credit that could expire in following years.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The amount available from the 10th previous tax year will expire after this tax year. When you file your return for the next year, you will enter the expired amount on line 104 of Schedule 385 for that year.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for Manitoba odour-control tax credit',
        'rows': [
          {
            'type': 'table',
            'num': '3000'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  });
})();
