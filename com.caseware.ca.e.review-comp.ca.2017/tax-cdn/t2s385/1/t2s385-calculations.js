(function() {
  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  wpw.tax.create.calcBlocks('t2s385', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      field('252').assign(field('ratesMb.159').get());
      field('252').source(field('ratesMb.159'));

      //part1
      calcUtils.sumBucketValues('103', ['100', '101', '102']);

      //part2
      field('1031').assign(field('501').get());
      field('104').assign(field('3000').cell(0, 3).get());
      field('105').assign(field('1031').get() - field('104').get());
      field('106').assign(field('105').get());
      field('110').assign(field('503').get());
      field('251').assign(field('103').get());

      field('120').assign(field('251').get() * field('252').get() / 100);
      field('151').assign(Math.max(field('120').get() - field('150').get(), 0));
      field('152').assign(field('151').get());
      calcUtils.sumBucketValues('153', ['106', '110', '152']);
      field('160').assign(Math.min(field('153').get() - field('904').get(), field('T2S383.301').get()));
      field('161').assign(field('302').get());
      field('162').assign(field('904').get());
      calcUtils.sumBucketValues('163', ['160', '161', '162']);
      field('164').assign(field('163').get());
      field('200').assign(Math.max(field('153').get() - field('164').get(), 0));
    });
    calcUtils.calc(function(calcUtils, field) {

      //part 3; run calc only when 290==1
      if (field('290').get() == 1) {
        field('300').assign(field('153').get() - field('162').get());//even though the form indicates it's the amount H, both TaxCycle and TaxPrep subtracts line g
        field('301').assign(field('T2S383.301').get());
        field('302').assign(Math.max(field('300').get() - field('301').get(), 0));
      }
      else {
        calcUtils.removeValue(['300', '301', '302'], true)
      }
    });
    calcUtils.calc(function(calcUtils, field) {

      var tableArray = [1000, 2000, 3000];
      var taxationYearTable = field('tyh.200');
      tableArray.forEach(function(num) {
        field(num).getRows().forEach(function(row, rowIndex) {
          if (num == 1000) {
            var dateCol = Math.abs(rowIndex - 19);
            row[1].assign(taxationYearTable.cell(dateCol, 6).get());
          }
          else {
            row[1].assign(taxationYearTable.getRow(rowIndex + 10)[6].get())
          }
        })
      });
    });
    calcUtils.calc(function(calcUtils, field) {

      //part 4
      var summaryTable = field('3000');
      field('2000').getRows().forEach(function(row, rowIndex) {
        row[3].assign(summaryTable.getRow(rowIndex)[13].get());
      });
      // historical data chart
      var limitOnCredit = field('151').get();
      summaryTable.getRows().forEach(function(row, rowIndex) {
        if (rowIndex == 0) {
          // to avoid the first row being calculated to total
          row[7].assign(0);
          row[9].assign(0);
          row[11].assign(0);
          row[13].assign(0);
        } else {
          row[9].assign(Math.max(row[3].get() + row[5].get() + row[7].get(), 0));
          row[11].assign(returnAmountApplied(limitOnCredit, row[9].get()));
          row[13].assign(Math.max(row[9].get() - row[11].get(), 0));
          limitOnCredit -= row[11].get();
        }
      });
      summaryTable.cell(10, 5).assign(field('152').get())
    });
  })
})();

