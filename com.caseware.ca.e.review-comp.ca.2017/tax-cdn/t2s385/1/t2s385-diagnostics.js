(function() {
  wpw.tax.create.diagnostics('t2s385', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3850001', common.prereq(common.check('t2s5.607', 'isNonZero'), common.requireFiled('T2S385')));

    diagUtils.diagnostic('3850002', common.prereq(common.and(
        common.check('120', 'isNonZero'),
        common.requireFiled('T2S385')),
        common.check(['100', '101', '102'], 'isNonZero')));

    diagUtils.diagnostic('3850003', common.prereq(common.and(
        common.check(['901', '902', '903'], 'isNonZero'),
        common.requireFiled('T2S385')),
        common.check('120', 'isNonZero')));

    diagUtils.diagnostic('3850007', common.prereq(common.check('t2s5.623', 'isNonZero'), common.requireFiled('T2S385')));
  });
})();
