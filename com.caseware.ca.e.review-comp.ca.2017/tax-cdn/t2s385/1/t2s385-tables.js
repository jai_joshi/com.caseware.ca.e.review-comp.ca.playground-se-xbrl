(function() {
  wpw.tax.create.tables('t2s385', {
    '130': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none'
        },
        {
          type: 'none',
          width: '30px'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {
            label: 'Corporation\'s share of eligible expenditures allocated from a partnership'
          },
          '1': {
            tn: '100'
          },
          '2': {
            num: '100',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          '3': {
            label: 'A'
          }
        },
        {
          '0': {
            label: 'Corporation\'s share of eligible expenditures allocated from a trust'
          },
          '1': {
            tn: '101'
          },
          '2': {
            num: '101',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          '3': {
            label: 'B'
          }
        },
        {
          '0': {
            label: 'Corporation\'s eligible expenditures'
          },
          '1': {
            tn: '102'
          },
          '2': {
            num: '102'
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          '3': {
            label: 'C'
          }
        }
      ]
    },
    '250': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          type: 'none'
        },
        {
          type: 'none',
          colClass: 'std-input-col-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'small-input-width',
          cellClass: 'alignRight'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          0: {
            label: 'Current-year credit earned'
          },
          1: {
            label: '(amount D from Part 1)'
          },
          2: {
            num: '251'
          },
          3: {
            label: 'x'
          },
          4: {
            num: '252'
          },
          5: {
            label: '%='
          },
          6: {
            tn: '120'
          },
          7: {
            num: '120',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          },
          8: {
            label: 'c'
          }
        }
      ]
    },
    '1000': {
      'fixedRows': true,
      'infoTable': true,
      hasTotals: true,
      'columns': [
        {
          'width': '190px',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'formField': true,
          'total': true,
          'totalNum': '904',
          'totalMessage': '<b>Total</b> (enter amount at line g)'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        }],
      'cells': [
        {
          '0': {
            'label': '1st preceding taxation year'
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '901'
          },
          '5': {
            'num': '901',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '2nd preceding taxation year'
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '902'
          },
          '5': {
            'num': '902',
            "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '3rd preceding taxation year'
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '903'
          },
          '5': {
            'num': '903', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }]
    },
    '2000': {
      'hasTotals': true,
      fixedRows: true,
      infoTable: true,
      'columns': [

        {
          'type': 'none',
          cellClass: 'alignRight'
        },
        {
          'width': '160px',
          'type': 'date',
          'disabled': true,
          header: '<b>Year of origin</b>'
        },
        {
          'type': 'none'
        },
        {
          'header': '<b>Credit available for carryforward</b>',
          'total': true,
          'totalNum': '2001',
          'totalMessage': 'Total (equals line 200)',
          colClass: 'std-input-width'
        },
        {type: 'none'}
      ],
      'cells': [
        {
          '0': {
            'label': '10th previous tax year ending on '
          }
        },
        {
          '0': {
            'label': '9th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '8th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '7th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '6th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '5th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '4th previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '3rd previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '2nd previous tax year ending on'
          }
        },
        {
          '0': {
            'label': '1st previous tax year ending on'
          }
        },
        {
          '0':{
            label: 'Current tax year ending on'
          }
        }]
    },
    '3000': {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {colClass: 'std-padding-width', type: 'none'},
        {
          colClass: 'std-input-width',
          type: 'date',
          disabled: true,
          header: '<b>Year of origin</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '501',
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '502',
          totalMessage: ' ',
          header: '<b>Current year credit</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true,
          totalNum: '503',
          totalMessage: ' ',
          header: '<b>Transfer</b>',
          formField: true
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', totalNum: '504',
          header: '<b>Amount available to apply</b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '505',
          header: '<b>Applied<b>'
        },
        {colClass: 'std-spacing-width', type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '506',
          header: '<b>Balance to carry forward</b>'
        },
        {colClass: 'std-padding-width', type: 'none'}
      ],
      cells: [
        {
          '4': {label: '*'},
          '5': {type: 'none'},
          '7': {type: 'none'},
          '9': {type: 'none'},
          '11': {type: 'none'},
          '13': {type: 'none', label: 'N/A'}
        },
        {
          '5': {type: 'none'},
          '14': {label: '**'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '3': {type: 'none'},
          '7': {type: 'none'}
        }
      ]
    }
  })
})();
