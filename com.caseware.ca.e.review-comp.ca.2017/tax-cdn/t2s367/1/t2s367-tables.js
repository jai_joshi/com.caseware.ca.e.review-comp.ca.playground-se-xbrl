(function() {
  wpw.tax.global.tableCalculations.t2s367 = {
    200: {
      hasTotals: true,
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none',
        },
        {
          type: 'none',
          width: '250px'
        },
        {
          header: 'Tax year in which to apply the credit',
          type: 'date',
          width: '160px'
        },
        {
          type: 'none',
          width: '34%'
        },
        {
          header: 'Amount to be applied',
          width: '137px',
          total: true,
          totalNum: 201,
          totalMessage: '<b>Total amount to be applied</b> (enter on line d in Part 1)'
        },
        {
          type: 'none',
          width: 'calc((33% - 249px))'
        }
      ],
      cells: [
        {
          1: {
            label: '1st previous tax year ending on'
          },
          4: {
            num: 901, "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            tn: 901
          }
        },
        {
          1: {
            label: '2nd previous tax year ending on'
          },
          4: {
            num: 902, "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            tn: 902
          }
        },
        {
          1: {
            label: '3rd previous tax year ending on'
          },
          4: {
            num: 903, "validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
            tn: 903
          }
        }
      ]
    },
    300: {
      hasTotals: true,
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none',
        },
        {
          type: 'none',
          width: '250px'
        },
        {
          header: 'Year of origin',
          type: 'date',
          width: '160px'
        },
        {
          type: 'none',
          width: '34%'
        },
        {
          header: 'Credit available for carryforward',

          width: '137px',
          total: true,
          totalNum: 301,
          totalMessage: '<b>Total credit available for carryforward</b>'
        },
        {
          type: 'none',
          width: 'calc((33% - 249px))'
        }
      ],
      cells: [
        {
          1: {
            label: '7th previous tax year ending on'
          }
        },
        {
          1: {
            label: '6th previous tax year ending on'
          }
        },
        {
          1: {
            label: '5th previous tax year ending on'
          }
        },
        {
          1: {
            label: '4th previous tax year ending on'
          }
        },
        {
          1: {
            label: '3rd previous tax year ending on'
          }
        },
        {
          1: {
            label: '2nd previous tax year ending on'
          }
        },
        {
          1: {
            label: '1st previous tax year ending on'
          }
        },
        {
          1: {
            label: 'Current tax year ending on'
          }
        }
      ]
    },
    1001: {
      fixedRows: true,
      infoTable: true,
      hasTotals: true,
      columns: [
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Year of origin',
          colClass: 'std-input-width',
          type: 'date',
          disabled: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Opening balance',
          total: true,
          totalNum: '1002',
          totalMessage: 'Totals : '
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Current Year Credit',
          total: true,
          totalNum: '1003',
          totalMessage: ' '
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Applied',
          disabled: true,
          total: true,
          totalNum: '1004',
          totalMessage: ' '
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          header: 'Balance to carry forward',
          disabled: true,
          total: true,
          totalNum: '1005',
          totalMessage: ' '
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }],
      cells: [
        {
          4: {
            label: '*'
          },
          5: {
            type: 'none'
          },
          7: {
            type: 'none'
          },
          9: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          3: {
            type: 'none'
          },
          5: {
            cellClass: 'singleUnderline'
          },
          7: {
            cellClass: 'singleUnderline'
          },
          9: {
            cellClass: 'singleUnderline'
          }
        }
      ]
    }
  };
})();
