(function() {
  wpw.tax.create.diagnostics('t2s367', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3670001', common.prereq(common.check('t2s5.578', 'isNonZero'), common.requireFiled('T2S367')));

    diagUtils.diagnostic('367.100-1', common.prereq(common.and(
        common.requireFiled('T2S367'),
        common.check('100', 'isNonZero')),
        function(tools) {
          return tools.field('100').require(function() {
            return this.get() <= 75000;
          })
        }));

    //Display notice that certificate must be filed
    diagUtils.diagnostic('367.100-2', common.prereq(common.requireFiled('T2S367'), common.check('100', 'isZero')));

  });
})();
