(function() {

  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  function runHistoricalTableCalc(calcUtils, tableObj, caryBackAmount, limitOnCreditAmount) {
    var tableNum = tableObj.tableNum;
    var openingBalanceCindex = tableObj.openingBalanceCindex;
    var currentYearCindex = tableObj.currentYearCindex;
    var appliedCindex = tableObj.appliedCindex;
    var endingBalanceCindex = tableObj.endingBalanceCindex;

    var summaryTable = calcUtils.field(tableNum);
    var numRows = summaryTable.size().rows;

    summaryTable.getRows().forEach(function(row, rowIndex) {
      if (rowIndex == numRows - 1) {
        if (caryBackAmount > 0) {
          summaryTable.cell(8, 7).assign(caryBackAmount);
          row[endingBalanceCindex].assign(Math.max(row[currentYearCindex].get() - row[appliedCindex].get(), 0))
        }
        else {
          summaryTable.cell(8, 7).assign(0)
        }
      }
      else {
        if (rowIndex == 0) {
          // to avoid the first row being calculated to total
          row[currentYearCindex].assign(0);
          row[appliedCindex].assign(0);
          row[endingBalanceCindex].assign(0);
        } else {
          row[appliedCindex].assign(returnAmountApplied(limitOnCreditAmount, row[openingBalanceCindex].get()));
          row[endingBalanceCindex].assign(Math.max(row[openingBalanceCindex].get() - row[appliedCindex].get(), 0));
          limitOnCreditAmount -= row[appliedCindex].get();
        }
      }
    });
  }

  wpw.tax.create.calcBlocks('t2s367', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      //to get year for table summary
      var taxationYearTable = field('tyh.200');
      var tableArray = [200, 300, 1001];
      tableArray.forEach(function(num) {
        field(num).getRows().forEach(function(row, rowIndex) {
          if (num == 200) {
            var dateCol = Math.abs(rowIndex - 19);
            row[2].assign(taxationYearTable.cell(dateCol, 6).get());
          }
          else if (num == 300) {
            row[2].assign(taxationYearTable.getRow(rowIndex + 13)[6].get())
          }
          else {
            row[1].assign(taxationYearTable.getRow(rowIndex + 12)[6].get())
          }
        })
      });

    });

    calcUtils.calc(function(calcUtils, field) {
      field('110').assign(field('1002').get());
      field('130').assign(field('1001').cell(0, 3).get());
      field('135').assign(field('110').get() - field('130').get());
      field('136').assign(field('135').get());
      field('140').assign(field('100').get() + field('136').get());
      field('150').assign(Math.min(75000,
          field('140').get(),
          field('T2S5.574').get()));
      field('160').assign(field('201').get());
      field('170').assign(field('150').get() + field('160').get());
      field('171').assign(field('170').get());
      field('190').assign(field('140').get() - field('171').get());
    });

    calcUtils.calc(function(calcUtils, field) {
      //part3
      var summaryTable = field('1001');
      field('300').getRows().forEach(function(row, rowIndex) {
        row[4].assign(summaryTable.getRow(rowIndex + 1)[9].get())
      });
      // historical data chart
      var caryBackAmount = field('200').get().totals[4];
      var limitOnCreditAmount = field('171').get() - caryBackAmount;

      runHistoricalTableCalc(
          calcUtils,
          {
            tableNum: '1001',
            currentYearCindex: 5,
            openingBalanceCindex: 3,
            appliedCindex: 7,
            endingBalanceCindex: 9
          },
          caryBackAmount,
          limitOnCreditAmount
      );
      summaryTable.cell(8, 5).assign(field('100').get())

    });

  });
})();
