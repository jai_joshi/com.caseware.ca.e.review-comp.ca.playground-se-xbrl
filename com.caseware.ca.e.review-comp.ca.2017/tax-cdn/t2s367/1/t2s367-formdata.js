(function() {
  wpw.tax.create.formData ('t2s367', {
    formInfo: {
      abbreviation: 't2s367',
      title: 'New Brunswick Corporation Tax Calculation',
      schedule: 'Schedule 367',
      headerImage: 'canada-federal',
      code: 'Code 1401',
      category: 'New Brunswick Forms',
      formFooterNum: 'T2 SCH 367 E ',
      description: [
        {
          'text': '• Use this schedule to claim a New Brunswick Small Business Investor Tax Credit (NBSBITC) under ' +
          'section 61.1 of the <i>New Brunswick <i>Income Tax Act</i></i>'
        },
        {
          'text': '• The NBSBITC is a non-refundable credit for eligible investors equal to 15% of all amounts not ' +
          'exceeding $500,000 paid during the tax year to a corporation registered under the <i>New Brunswick Small ' +
          'Business Investor Tax Credit Act</i> for eligible shares issued by the corporation as part of ' +
          'a specified issue.'
        },
        {
          'text': '• You are an eligible investor if you are a corporation that maintained a permanent establishment ' +
          'in New Brunswick at any time during the tax year, and you received a New Brunswick Small Business Investor ' +
          'Tax Credit Certificate for the tax year.'
        },
        {
          'text': '• File a completed copy of this schedule and a copy of the relevant NB-SBITC-1 certificates with ' +
          'your <i>T2 Corporation Income Tax Return</i>'
        }
      ]
    },
    sections: [
      {
        'header': 'Part 1 - Credit available for the year and credit available for carryforward',
        'rows': [
          {
            'label': 'Total credit earned in the current tax year <br>(total of all credit amounts from NB-SBITC-1 certificates received for the tax year) (cannot exceed $75,000)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': '<b>Add:</b>',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Unused credit at the end of the previous tax year ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '110'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': '<b>Deduct:</b> Credit expired after 7 tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '130',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': 'Unused credit at the beginning of this tax year (amount a <b>minus</b> amount b)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '135',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '135'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '136',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Total credit available for the current tax year (amount A <b>plus</b> amount B)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '140'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': '<b>Deduct:</b>',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit claimed in the current tax year<br>(the least of amount C, $75,000 and New Brunswick income tax otherwise payable)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '150',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '150'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'Enter amount c on line 578 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'Credit carried back to the previous tax years (complete Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount c <b>plus</b> amount d)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '170'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '171'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': '<b>Closing balance - credit available for carryforward</b> (amount C <b>minus</b> amount D)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '190',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              }
            ],
            'indicator': 'E'
          }
        ]
      },
      {
        'header': 'Part 2 - Request for carryback of credit',
        'rows': [
          {
            'label': 'Complete this part to request a carryback of a current-year credit earned to a tax year that ends after <b>December 31, 2013</b>. The credit that can be carried back to the three previous tax years is the current year credit earned less the amount by which the current year tax otherwise payable exceeds any unused credits applied in the tax year.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'The maximum amount of credit that can be applied to a prior tax year is the lesser of:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- the income tax payable in the prior tax year; and',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '- $75,000 less the amount of any credit previously deducted in that prior tax year.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '200'
          }
        ]
      },
      {
        'header': 'Part 3 -  Analysis of credit available for carryforward by year of origin',
        'rows': [
          {
            'label': 'If you are an eligible investor that is a corporation, you can complete this part to show all the credits from previous tax years available for carryforward, by year of origin. This will help you determine the amount of credit that could expire in future years.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '300'
          },
          {
            'label': 'The amount available from the 7th previous year will expire after this tax year. When you file your return for the next year, you will enter the expired amount on line 130 of Schedule 367 for that year.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        hideFieldset: true,
        rows:[
          {
            label: '<i>Privacy Act</i>, personal information bank number CRA PPU 047',
            labelClass: 'absoluteAlignRight'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for NBSBITC',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1001'
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  })
})();
