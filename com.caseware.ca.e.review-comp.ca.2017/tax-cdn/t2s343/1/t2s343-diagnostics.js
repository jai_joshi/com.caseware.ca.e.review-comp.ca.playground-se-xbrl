(function() {
  wpw.tax.create.diagnostics('t2s343', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S343'), forEach.row('900', forEach.bnCheckCol(1, true))));

  });
})();
