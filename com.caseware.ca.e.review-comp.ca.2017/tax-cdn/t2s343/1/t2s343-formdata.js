(function() {
  'use strict';

  wpw.tax.global.formData.t2s343 = {
    formInfo: {
      abbreviation: 'T2S343',
      title: 'Nova Scotia Tax on Large Corporations - Agreement Among Related Corporations',
      //subTitle: '(2008 and later tax years)',
      schedule: 'Schedule 343',
      code: 'Code 0801',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 343 E (08)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'For use by members of a related group of corporations to allocate the capital deduction ' +
              'of $5,000,000 among the members of the related group. The $5,000,000 capital deduction is ' +
              'only available if the combined taxable capital for the year for all members of the related ' +
              'group, using line 500 on Schedule 33, 34, or 35, is less than $10,000,000. If the combined ' +
              'taxable capital for the year for related corporations is $10,000,000, or more, none of the ' +
              'corporations are entitled to a deduction.'
            },
            {
              label: 'A related corporation that has more than one tax year ending in a calendar year, ' +
              'is required to file an agreement for each tax year ending in that calendar year.'
            },
            {
              label: 'The terms "related", "related group", and "associated" are defined in sections 251 ' +
              'and 256 and, for the Nova Scotia tax on large corporations, have the meaning assigned to ' +
              'those sections.'
            },
            {
              label: 'If no agreement is filed, the Minister may ask that one be filed. If the ' +
              'corporation does not file such an agreement within 30 days after receiving the ' +
              'request, the Minister may allocate an amount among the members of the related group.'
            },
            {
              label: 'Subsections 181.5(4) to (7) apply to this allocation.'
            },
            {
              label: 'According to subsection 181.5(5), where a corporation has more than one tax year ending in the' +
              ' same calendar year and is related in two or more of those tax years to another corporation that has' +
              ' a tax year ending in that calendar year, the capital deduction of the first corporation for each' +
              ' such tax year at the end of which it is related to the other corporation is an amount equal' +
              ' to its capital deduction for the first such tax year.'
            },
            {
              label: 'According to subsection 181.5(7), a Canadian-controlled private corporation is not ' +
              'considered to be related to another corporation for the capital deduction unless it is ' +
              'also associated with that corporation.'
            },
            {
              label: 'Except as otherwise stated, sections and subsections referred to on this schedule ' +
              'are those from the federal <i>Income Tax Act</i>.'
            },
            {
              label: 'Provide details below. If you need more space, continue on a separate schedule.'
            }
          ]
        }
      ],
      category: 'Nova Scotia Forms'
    },
    sections: [
      {
        'header': 'Allocation of capital deduction for related corporations',
        'rows': [
          {
            'label': 'Date filed (do not use this area)',
            'labelWidth': '82%',
            'type': 'infoField',
            'inputType': 'date',
            'num': '010',
            'tn': '010',
            'disabled': true
          },
          {
            'label': 'Is this an amended agreement?',
            'labelWidth': '82%',
            'type': 'infoField',
            'inputType': 'radio',
            'num': '020',
            'tn': '020'
          },
          {
            'label': 'Calendar year to which the agreement applies',
            'labelWidth': '82%',
            'num': '030',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{4,4}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'tn': '030',
            'type': 'infoField',
            'inputType': 'fixedSizeBox',
            'boxes': 4
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '900'
          },
          {
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  };
})();
