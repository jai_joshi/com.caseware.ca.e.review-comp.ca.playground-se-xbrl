(function() {

  wpw.tax.global.tableCalculations.t2s343 = {
    '900': {
      fixedRows: true,
      hasTotals: true,
      showNumbering: true,
      columns: [
        {
          header: 'Name of each corporation that is a member of the related group',
          num: '200',
          validate: {'or': [{'length': {'min': '1', 'max': '175'}}, {'check': 'isEmpty'}]},
          tn: '200',
          type: 'text'
        },
        {
          header: 'Business Number<br>(if a corporation is not registered, enter \'NR\')',
          num: '300',
          tn: '300',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          header: 'Expenditure limit',
          colClass: 'std-input-width'
        },
        {
          header: 'Allocation of capital deduction for the year<br>$',
          num: '400',
          tn: '400',
          colClass: 'std-input-width',
          total: true,
          totalNum: '501',
          totalLimit: '5000000',
          totalMessage: 'Total (not to exceed $5,000,000)'
        }
      ]
    }
  }
})();
