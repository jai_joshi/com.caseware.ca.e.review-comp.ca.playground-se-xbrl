(function() {

  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  wpw.tax.create.calcBlocks('t2s387', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      var dateCompare = calcUtils.dateCompare;
      var taxEnd = field('CP.tax_end').get();
      //part 2
      field('103').assign(field('501').get());
      field('104').assign(field('500').cell(0, 3).get());
      field('105').assign(field('103').get() + field('104').get());
      field('106').assign(field('105').get());
      var date = wpw.tax.date(2014, 6, 12);
      if (angular.isUndefined(field('102').get())) {
        field('120').assign(0);
      }
      else {
        if (dateCompare.lessThan(taxEnd, date)) {
          field('120').assign(Math.min(field('102').get(), field('ratesMb.123').get()))
        }
        else {
          field('120').assign(Math.min(field('102').get(), field('ratesMb.124').get()))
        }
      }
      field('121').assign(field('106').get() + field('120').get());
      if (dateCompare.lessThan(taxEnd, date)) {
        field('123').assign(Math.min(field('T2S383.301').get(), field('121').get() - field('124').get(), field('ratesMb.125').get()))
      }
      else {
        field('123').assign(Math.min(field('T2S383.301').get(), field('121').get() - field('124').get(), field('ratesMb.126').get()))
      }
      field('124').assign(field('904').get());
      field('125').assign(field('123').get() + field('124').get());
      field('126').assign(field('125').get());
      field('200').assign(field('121').get() - field('126').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part3
      var tableArray = [300, 400, 500];
      var taxationYearTable = field('tyh.200');
      tableArray.forEach(function(num) {
        field(num).getRows().forEach(function(row, rowIndex) {
          if (num == 300) {
            var dateCol = Math.abs(rowIndex - 19);
            row[1].assign(taxationYearTable.cell(dateCol, 6).get());
          }
          else {
            row[1].assign(taxationYearTable.getRow(rowIndex + 10)[6].get())
          }
        })
      });
      field('904').assign(field('901').get() + field('902').get() + field('903').get());
    });

    calcUtils.calc(function(calcUtils, field) {
      //part 4
      var summaryTable = field('500');
      field('400').getRows().forEach(function(row, rowIndex) {
        row[3].assign(summaryTable.getRow(rowIndex)[13].get());
      });
      // historical data chart
      var limitOnCredit = field('125').get();
      summaryTable.getRows().forEach(function(row, rowIndex) {
        if (rowIndex == 0) {
          // to avoid the first row being calculated to total
          row[7].assign(0);
          row[9].assign(0);
          row[11].assign(0);
          row[13].assign(0);
        } else {
          row[9].assign(Math.min(row[3].get() + row[5].get() - row[7].get()), 0);
          row[11].assign(returnAmountApplied(limitOnCredit, row[9].get()));
          row[13].assign(Math.max(row[9].get() - row[11].get(), 0));
        }
      });
      summaryTable.cell(10, 5).assign(field('120').get())

    })
  });
})();

