(function() {
  wpw.tax.create.tables('t2s387', {
    '100': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'type': 'none',
          cellClass: 'alignLeft'
        },
        {
          type: 'none',
          cellClass: 'alignRight',
          width: '30px'
        },
        {
          colClass: 'std-input-width',
          'formField': true
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        }],
      'cells': [{
        '0': {
          'label': 'Corporation\'s credit amount from SBVC tax credit receipts '
        },
        '1': {
          'tn': '102'
        },
        '2': {
          'num': '102'
          , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
        },
        '3': {
          'label': 'A',
          'labelClass': 'left'
        }
      }
      ]
    },
    '300': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          colClass: 'std-input-col-width-2',
          'type': 'none',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true
        },
        {
          'type': 'none'
        },
        {
          'type': 'none',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        }
      ],
      'cells': [
        {
          '0': {
            'label': '1st preceding taxation year'
          },
          '3': {
            'label': ' Credit to be applied '
          },
          '4': {
            'tn': '901'
          },
          '5': {
            'num': '901'
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '2nd preceding taxation year'
          },
          '3': {
            'label': ' Credit to be applied '
          },
          '4': {
            'tn': '902'
          },
          '5': {
            'num': '902'
            , "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '3rd preceding taxation year'
          },
          '3': {
            'label': ' Credit to be applied '
          },
          '4': {
            'tn': '903'
          },
          '5': {
            'num': '903', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }]
    },
    '400': {
      'hasTotals': true,
      fixedRows: true,
      infoTable: true,
      'columns': [
        {
          colClass: 'std-input-col-width-2',
          'type': 'none',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true,
          header: '<b>Year of origin</b>'
        },
        {
          'type': 'none'
        },
        {
          'header': '<b>Credit available for carryforward</b>',
          'total': true,
          'totalNum': '2001',
          'totalMessage': 'Total (equals line 200 in Part 2)'
        },
        {type: 'none'}
      ],
      'cells': [
        {
          '0': {
            'label': '10th preceding taxation year'
          }
        },
        {
          '0': {
            'label': '9th preceding taxation year'
          }
        },
        {
          '0': {
            'label': '8th preceding taxation year'
          }
        },
        {
          '0': {
            'label': '7th preceding taxation year'
          }
        },
        {
          '0': {
            'label': '6th preceding taxation year'
          }
        },
        {
          '0': {
            'label': '5th preceding taxation year'
          }
        },
        {
          '0': {
            'label': '4th preceding taxation year'
          }
        },
        {
          '0': {
            'label': '3rd preceding taxation year'
          }
        },
        {
          '0': {
            'label': '2nd preceding taxation year'
          }
        },
        {
          '0': {
            'label': '1st preceding taxation year'
          }
        },
        {
          '0': {
            label: 'Current tax year-ending on '
          }
        }
      ]
    },
    '500': {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {colClass: 'std-padding-width', type: 'none'},
        {
          colClass: 'std-input-width',
          type: 'date',
          disabled: true,
          header: '<b>Year of origin</b>'
        },
        {colClass: 'std-spacing-width',type: 'none'},
        {
          total: true,
          totalNum: '501',
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>',
          formField: true
        },
        {colClass: 'std-spacing-width',type: 'none'},
        {
          total: true,
          totalNum: '502',
          totalMessage: ' ',
          header: '<b>Current year contribution</b>'
        },
        {colClass: 'std-spacing-width',type: 'none'},
        {
          total: true,
          totalNum: '503',
          totalMessage: ' ',
          header: '<b>Carryback Amount</b>',
          formField: true
        },
        {colClass: 'std-spacing-width',type: 'none'},
        {
          total: true, totalMessage: ' ', totalNum: '504',
          header: '<b>Amount available to apply</b>'
        },
        {colClass: 'std-spacing-width',type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '505',
          header: '<b>Applied<b>'
        },
        {colClass: 'std-spacing-width',type: 'none'},
        {
          total: true, totalMessage: ' ', disabled: true, totalNum: '506',
          header: '<b>Balance to carry forward</b>'
        },
        {colClass: 'std-padding-width',type: 'none'}
      ],
      cells: [
        {
          '4': {label: '*'},
          '5': {type: 'none'},
          '7': {type: 'none'},
          '9': {type: 'none'},
          '11': {type: 'none'},
          '13': {type: 'none', label: 'N/A'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'},
          '14': {label: '**'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '5': {type: 'none'},
          '7': {type: 'none'}
        },
        {
          '3': {type: 'none'}
        }
      ]
    }
  })
})();
