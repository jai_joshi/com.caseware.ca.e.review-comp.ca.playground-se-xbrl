(function () {
  'use strict';
  wpw.tax.create.formData('t2s387', {
    formInfo: {
      abbreviation: 't2s387',
      title: 'Manitoba Small Business Venture Capital Tax Credit',
      schedule: 'Schedule 387',
      code: 'Code 1601',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 387 E (16)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'You can claim a Manitoba small business venture capital tax credit under section 11.13 of the ' +
              '<i>Income Tax Act</i> (Manitoba) if the corporation:',
              sublist: [
                'is an eligible investor and has a small business venture capital (SBVC)' +
                ' tax credit receipt for the current tax year; or',
                'has unused tax credits from a prior tax year (a SBVC tax credit from a tax year ending after 2009, or a community enterprise investment tax credit from a tax year ending in 2008 or 2009).'
              ]
            },
            {
              label: 'An eligible investor, in relation to an investment, means a corporation that is not a prescribed ' +
              'venture capital corporation or prescribed labour-sponsored venture capital corporation under Part LXVII' +
              ' of the federal Income Tax Regulations or acting in its capacity as a ' +
              'dealer under <i>The Securities Act</i>.'
            },
            {
              label: 'The credit applies to an eligible investment acquired on or after January 1, 2008, and ' +
              'before January 1, 2020. The credit you earned in the year is used to reduce your Manitoba tax payable ' +
              'for that year. Any unused credit can be carried forward for 10 tax years ' +
              'or carried back to three previous tax years.'
            },
            {
              label: 'For a tax year ending before June 12, 2014, the maximum credit you can earn for an eligible investment made in the year is: the lesser of $135,000 and 30% of the cost of the investment.'
            },
            {
              label: '• For a tax year ending after June 11, 2014, the maximum credit you can earn is:',
              sublist: [
                'the lesser of $135,000 and 30% of the cost of the investment, for an eligible' +
                ' investment made in the year and before June 12, 2014; and',
                'the lesser of $202,500 and 45% of the cost of the investment, for an eligible investment made in ' +
                'the year and after June 11, 2014'
              ]
            },
            {
              label: 'The maximum credit you can claim is the least of:',
              sublist: [
                '$67,500, for a tax year ending after June 11, 2014, and $45,000 for a tax year ending before June 12, 2014; and',
                'the total of current tax year credit earned and any amounts carried back or carried forward; and',
                'Manitoba income tax otherwise payable.'
              ]
            },
            {
              label: 'Attach a copy of the SBVC tax credit receipt to this schedule, and include it' +
              ' with your T2 Corporation Income Tax Return. If you are filing electronically, ' +
              'keep a copy of the receipt for your records in case we need to see it.'
            }
          ]
        }
      ],
      category: 'Manitoba Forms'
    },
    sections: [
      {
        'header': 'Part 1 -  Credit earned in the current tax year',
        'rows': [
          {
            'type': 'table',
            'num': '100'
          },
          {
            'label': '<b>Note</b>: If the eligible investor irrevocably subscribed and paid for an eligible investment before acquiring it, the acquisition of it is deemed to have occurred when it was irrevocably subscribed and paid for.'
          }
        ]
      },
      {
        'header': 'Part 2 - Credit available for the year and credit available for carryforward',
        'rows': [
          {
            'label': 'Unused credit at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '103'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'a'
                }
              }
            ]
          },
          {
            'label': 'Credit expired after 10 tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'b'
                }
              }
            ]
          },
          {
            'label': 'Unused credit at the beginning of this tax year (amount a <b>minus</b> amount b)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '106',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit earned in the current tax year (amount A)*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '120',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': '<b>Total credit available for the current tax year</b> (amount B <b>plus</b> amount C) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '121'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Credit claimed in the current year** (enter on line 608 of Schedule 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '123'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': 'Credit carried back to previous tax years (complete Part 3) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '124'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount c <b>plus</b> amount d)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '125'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '126'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': '<b>Closing balance - credit available for carryforward</b> (amount D <b>minus</b> amount E)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '200'
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': '* The maximum is $135,000 for a tax year ending before June 12, 2014 and $202,500 for a tax year ending after June 11, 2014.',
            'labelClass': 'fullLength'
          },
          {
            'label': '**  The credit claimed in the current year <b>is equal to </b> the Manitoba income tax otherwise payable, amount D, or $67,500 (or $45,000 for a tax year ending before June 12, 2014), whichever is the least. ',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Request for carryback of credit',
        'rows': [
          {
            'label': 'Complete this part to request a carryback of a credit earned in the current tax year to the three previous tax years. The credit that can be carried back is the current year credit earned, less the amount by which the Manitoba tax otherwise payable in the current tax year exceeds any unused credits applied in the current tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The maximum amount of credit that can be applied to a prior tax year is the lesser of:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- $67,500 (or $45,000 for a tax year ending before June 12, 2014), less the amount of any credit previously deducted in that prior tax year',
            'labelClass': 'fullLength tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '300'
          },
          {
            'label': '<b>Total</b> (enter on line d in Part 2)',
            'labelCellClass': 'alignRight tabbed',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '904',
                  'disabled': true
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 4 - Analysis of credit available for carryforward by year of origin',
        'rows': [
          {
            'label': 'You can complete this part to show all the credits from previous tax years available for carryforward, by year of origin. This will help you determine the amount of credit that could expire in following years.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '400'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The amount available from the 10th previous tax year will expire after this tax year. When you file your return for the next year, you will enter the expired amount on line 104 of Schedule 387.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Historical Data and Calculation for Manitoba Small Business Venture Capital Tax Credit',
        'rows': [
          {
            'type': 'table',
            'num': '500'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  });
})();
