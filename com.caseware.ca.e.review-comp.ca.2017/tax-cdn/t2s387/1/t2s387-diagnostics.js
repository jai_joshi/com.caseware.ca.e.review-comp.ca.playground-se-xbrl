(function() {
  wpw.tax.create.diagnostics('t2s387', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3870005', common.prereq(common.check('t2s5.608', 'isNonZero'), common.requireFiled('T2S387')));

    diagUtils.diagnostic('3870010', common.prereq(common.and(
        common.requireFiled('T2S387'),
        common.check('120', 'isNonZero')),
        common.check('102', 'isNonZero')));

  });
})();
