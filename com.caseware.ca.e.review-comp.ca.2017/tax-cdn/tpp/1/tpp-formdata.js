(function() {
  'use strict';

  wpw.tax.global.formData.tpp = {
      formInfo: {
        abbreviation: 'TPP',
        title: 'Tax Preparer Profile',
        headerImage: 'cw',
        highlightFieldsets: true,
        css: 'style-cdn.css',
        category: 'Start Here',
        showDots: false
        // description: [
        //   {text: 'This data will automatically be updated from Collaborate.'}
        // ]
      },
      sections: [
        {
        header: 'Tax Preparer\'s Address',
          rows: [
            {
              'label': 'Tax Preparer Firm Name'
            },
            {
              'type': 'infoField',
              'inputType': 'textArea',
              'num': '101'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Firm or company name to print in all client communication'
            },
            {
              'type': 'infoField',
              'inputType': 'textArea',
              'labelWidth': '90%',
              'width': '50%',
              'num': '102'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'infoField',
              'inputType': 'address',
              'num': '117',
              'add1Num': '1100',
              'add2Num': '1101',
              'provNum': '1102',
              'cityNum': '1103',
              'countryNum': '1104',
              'postalCodeNum': '1105',
              'provinceNum': '1106'
            },
            {
              'label': 'Contact Information:',
              'labelClass': 'fullLength bold'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': 'Tax_Prepare_7'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': 'Tax_Prepare_8'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': 'Tax_Prepare_9'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': 'Tax_Prepare_10'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'infoField',
              'label': 'EFile number',
              inputClass: 'std-input-col-width-2',
              'num': '888'
            }
            // {
            //   'type': 'infoField',
            //   'label': 'CW customer number',
            //   inputClass: 'std-input-col-width-2',
            //   'num': '131'
            // }
          ]
        }
      ]
    };
})();
