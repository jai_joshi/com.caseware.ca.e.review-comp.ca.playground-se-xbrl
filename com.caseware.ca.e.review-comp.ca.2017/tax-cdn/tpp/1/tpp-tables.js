(function() {

  var usaStates = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.usaStates, 'EN');
  var canadaProvinces = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.canadaProvinces, 'EN');
  var countries = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.countryAddressCodes, 'value');
  var titlesList = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.titleList, 'value');

  wpw.tax.create.tables('tpp', {
    'Tax_Prepare_7': {
      type: 'table', fixedRows: true, infoTable: true, num: 'Tax_Prepare_7',
      columns: [
        {header: 'Title', width: '15%', type: 'dropdown', options: titlesList},
        {header: 'First name'},
        {header: 'Last name'}
      ],
      cells: [
        {
          0: {num: '121', type: 'text'},
          1: {num: '122', type: 'text'},
          2: {num: '123', type: 'text'}
        }
      ]
    },
    'Tax_Prepare_8': {
      type: 'table', fixedRows: true, infoTable: true, num: 'Tax_Prepare_8',
      columns: [
        {header: 'Nickname', type: 'text'},
        {header: 'Position', type: 'text'}
      ],
      cells: [
        {
          0: {num: '124'},
          1: {num: '125'}
        }
      ]
    },
    'Tax_Prepare_9': {
      type: 'table', fixedRows: true, infoTable: true, num: 'Tax_Prepare_9',
      columns: [
        {
          header: 'Work phone', type: 'custom',
          format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
          validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
        },
        {header: 'Extension', width: '20%'},
        {
          header: 'Mobile phone', type: 'custom',
          format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
          validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
        }
      ],
      cells: [
        {
          0: {num: '126'},
          1: {num: '127', type: 'text'},
          2: {num: '128'}
        }
      ]
    },
    'Tax_Prepare_10': {
      type: 'table', fixedRows: true, infoTable: true, num: 'Tax_Prepare_10',
      columns: [
        {header: 'Fax', width: '40%', type: 'text'},
        {header: 'Email address', type: 'text', validate: 'email'}
      ],
      cells: [
        {
          0: {num: '129'},
          1: {num: '130'}
        }
      ]
    }
  });
})();
