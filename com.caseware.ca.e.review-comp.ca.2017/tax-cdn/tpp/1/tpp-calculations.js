(function () {

  var isConditionLoaded = false;

  wpw.tax.create.calcBlocks('tpp', function (calcUtils) {
    calcUtils.calc(function (calcUtils, field, form) {
      //download data from collaborate
      if (!field('isLoad').get()) {
        var firmEntityObj = wpw.tax.global.engagementProperties.firmEntity;
        field('101').set(firmEntityObj.name || '');
        if (firmEntityObj.addresses && firmEntityObj.addresses[0]) {
          field('1100').set(firmEntityObj.addresses[0].address1 || '');
          field('1101').set(firmEntityObj.addresses[0].address2 || '');
          field('1103').set(firmEntityObj.addresses[0].city || '');
          field('1102').set(firmEntityObj.addresses[0].province || '');
          field('1105').set(firmEntityObj.addresses[0].postalCode || '');
        }
        if (firmEntityObj.phones && firmEntityObj.phones[0]) {
          field('126').set(firmEntityObj.phones[0].number || '');
        }
        wpw.tax.field('isLoad').set(true)
      }

    });

    calcUtils.calc(function (calcUtils, field, form) {
      //   if (!isConditionLoaded && !angular.isUndefined(calcUtils.field('101').get())) {
      //     field('102').assign(field('101').get());
      //     field('102').disabled(false);
      //     isConditionLoaded = true;
      //   }
    });
  });

})();
