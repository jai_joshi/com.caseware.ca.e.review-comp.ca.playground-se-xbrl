(function() {

  wpw.tax.create.formData('t2s322', {
    formInfo: {
      abbreviation: 't2s322',
      title: 'Prince Edward Island Corporation Tax Calculation',
      schedule: 'Schedule 322',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 322 E (16) ',
      description: [
        {
          type: 'list',
          items: [
            'Use this schedule if your corporation had a permanent establishment (as defined in section 400 of ' +
            'the federal <i>Income Tax Regulations</i>) in Prince Edward Island, and had taxable income ' +
            'earned in the year in Prince Edward Island.',
            'This schedule is a worksheet only and is not required to be filed with your <i>T2 ' +
            'Corporation Income Tax Return.</i>'
          ]
        }
      ],
      category: 'Prince Edward Island Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Calculation of income subject to Prince Edward Island lower and higher tax rates',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable income for Prince Edward Island *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '500'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Income eligible for the Prince Edward Island lower tax rate:',
            'labelClass': 'bold'
          },
          {
            'label': 'Amount from line 400 of the T2 return ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '501'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 405 of the T2 return ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '502'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 427 of the T2 return ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '503'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'label': 'Amount B, C, or D, whichever is the least ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '504'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For credit unions only: ',
            'labelClass': 'bold'
          },
          {
            'label': 'Amount from line F of Schedule 17, Credit Union Deductions',
            'labelClass': '2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '505'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F'
                }
              }
            ]
          },
          {
            'label': 'Total (amount E <b>plus</b> amount F)',
            'labelClass': '2 text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '507'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': ' <b> Income subject to  Prince Edward Island higher tax rate </b>(amount A <b>minus </b>amount H)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '510'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': 'Enter amount H and/or amount I on the applicable line(s) in Part 2.'
          },
          {
            'label': '* If the corporation has a permanent establishment only in Prince Edward Island, enter the taxable income from line 360 of the T2 return. Otherwise, enter the taxable income allocated to Prince Edward Island from column F in Part 1 of Schedule 5, Tax Calculation Supplementary - Corporations',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '**  Includes the territories and the offshore jurisdictions for Nova Scotia and Newfoundland and Labrador.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of Prince Edward Island tax before credits',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'labelClass'
          },
          {
            'label': 'Prince Edward Island tax at the lower rate:',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '700'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Prince Edward Island tax at the higher rate:',
            'labelClass': 'bold'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '600'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Prince Edward Island tax before credits </b> (amount J <b>plus</b> amount K)*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'double',
                'input': {
                  'num': '811'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '*  If the corporation has a permanent establishment in more than one jurisdiction, or is claiming a Prince Edward Island tax credit, enter amount L on line 210 of Schedule 5. Otherwise, enter it on line 760 of the T2 return.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  });
})();
