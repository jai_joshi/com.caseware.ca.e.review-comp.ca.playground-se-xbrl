(function() {
  wpw.tax.global.tableCalculations.automobileLeasing = {
    "400": {
      "fixedRows": true,
      "infoTable": false,
      "columns": [
          {
        "header": "Description",
        cellClass: 'alignLeft',
        type: 'text'
      },
        {
          "header": "Date financing began",
          "type": "date",
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          "header": "Date financing terminated",
          "type": "date",
          cellClass: 'alignCenter',
          colClass: 'std-input-width'
        },
        {
          "header": "GST/PST combined rate or HST at the time of the lease",
          "indicator": "a",
          colClass: 'std-input-width'
        }],
      "cells": [{
        "0": {
          "num": "405"
        },
        "1": {
          "num": "410"
        },
        "2": {
          "num": "415"
        },
        "3": {
          "num": "420"
        }
      }]
    }
  }
})();
