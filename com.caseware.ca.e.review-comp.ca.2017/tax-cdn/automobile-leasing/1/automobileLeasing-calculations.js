(function() {

  wpw.tax.create.calcBlocks('automobileLeasing', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('225', 'ratesFed', '835');
      calcUtils.getGlobalValue('237', 'ratesFed', '836');
      calcUtils.getGlobalValue('240', 'ratesFed', '837');
      calcUtils.getGlobalValue('261', 'ratesFed', '833');
    });

    calcUtils.calc(function(calcUtils, field) {
      //automobile leasing
      field('215').assign(Math.min(wpw.tax.actions.calculateDaysDifference(field('410').get(), field('415').get()),
          wpw.tax.actions.calculateDaysDifference(field('410').get(), field('CP.tax_end').get())));
      calcUtils.multiply('230', ['225', '420'], (1 / 100));
      calcUtils.sumBucketValues('235', ['225', '230']);
      field('238').assign(field('237').get() == 0 ? 0 : field('235').get() / (field('237').get() / 100));
      if (field('220').get() > field('238').get()) {
        field('239').assign(field('220').get() * (field('240').get() / 100));
      }
      else {
        field('239').assign(field('238').get() * (field('240').get() / 100));
      }
      field('262').assign((parseInt(field('261').get()) + (parseInt(field('261').get()) * field('420').get() / 100)) *
          (field('215').get() / 30) - field('210').get()
          - field('245').get() - field('255').get());
      field('263').assign(field('239').get() == 0 ? 0 : (parseInt(field('225').get()) + parseInt(field('230').get())) *
          (field('205').get() / field('239').get()) - field('250').get() - field('260').get());
      field('264').assign(field('205').get() - (Math.min(field('262').get(), field('263').get())));
      calcUtils.sumBucketValues('270', ['264', '265']);
    });
  });
})();