(function() {
  'use strict';

  wpw.tax.global.formData.automobileLeasing = {
    formInfo: {
      abbreviation: 'automobileLeasing',
      title: 'Non-Deductible Automobile Leasing and Other Expenses',
      headerImage: 'cw',
      isRepeatForm: true,
      repeatFormData: {
        linkedRepeatTable: 'automobileSummary.200',
        titleNum: '405'
      },
      category: 'Workcharts'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            'type': 'table',
            'num': '400'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter lease payments made or payable for the current fiscal period (including taxes)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '205'
                }
              }
            ],
            'indicator': 'b'
          },
          {
            'label': 'Leasing costs already deducted ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '210'
                }
              }
            ],
            'indicator': 'c'
          },
          {
            'label': '# of days from the date the lease commenced to the end of the current fiscal period or the date lease terminated, if the lease terminated during the current fiscal period',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '215',
                  'filters': ''
                }
              }
            ],
            'indicator': 'd'
          },
          {
            'label': 'Manufacturer\'s suggested price for the motor vehicle',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '220',
                  'init': 0
                }
              }
            ],
            'indicator': 'e'
          },
          {
            'label': 'Cost limit',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '225'
                }
              },
              null
            ]
          },
          {
            'label': 'Taxes',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '230'
                }
              },
              null
            ]
          },
          {
            'label': 'Eligible Cost',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '235'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal = (Eligible Cost / ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '237',
                  'filters': 'decimals 2'
                }
              },
              {
                'input': {
                  'num': '238',
                  'filters': 'decimals 2'
                },
                'padding': {
                  'type': 'text',
                  'data': '%)='
                }
              }
            ],
            'indicator': 'f'
          },
          {
            'label': 'Greater of (line e and f) x ',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '240'
                }
              },
              {
                'input': {
                  'num': '239'
                },
                'padding': {
                  'type': 'text',
                  'data': '%='
                }
              }
            ],
            'indicator': 'g'
          },
          {
            'label': 'Refundable Deposits - Imputed Interest',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total imputed interest on refundable deposits in excess of $1K for the current and all preceeding fiscal periods in the lease period',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '245'
                }
              }
            ],
            'indicator': 'h'
          },
          {
            'label': 'Imputed interest on refundable deposits in excess of $1K for the current fiscal period ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '250'
                }
              }
            ],
            'indicator': 'i'
          },
          {
            'label': 'Reimbursements receivable ',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total for the current + all prior fiscal periods',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '255'
                }
              }
            ],
            'indicator': 'j'
          },
          {
            'label': 'For the current year',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '260'
                }
              }
            ],
            'indicator': 'k'
          },
          {
            'label': 'Amount for monthly limit',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '261'
                }
              }
            ],
            'indicator': 'l'
          },
          {
            'label': 'Monthly limit: (amount l + tax) * number of month - amount c - amount h - amount j',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '262'
                }
              }
            ]
          },
          {
            'label': 'Eligible cost limit: (eligible cost + tax) * (amount b / g) - amount i - amount k',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '263'
                }
              }
            ]
          },
          {
            'label': 'Deductible leasing costs (amount b minus the lesser of monthly limit and eligible cost limit)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '264'
                }
              }
            ],
            'indicator': 'm'
          },
          {
            'label': 'Other non-deductible expenses for this leased vehicle',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '265'
                }
              }
            ],
            'indicator': 'n'
          },
          {
            'label': 'Total non-deductible leasing expenses and other non-deductible expenses for this leased vehicle (total of amount m and n)',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '270'
                }
              }
            ]
          }
        ]
      }
    ]
  };
})();