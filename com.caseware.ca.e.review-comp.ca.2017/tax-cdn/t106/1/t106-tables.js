(function() {

  wpw.tax.global.tableCalculations.t106 = {
    '1000': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {},
        {
          width: '50px',
          type: 'none'
        },
        {}
      ],
      cells: [
        {
          '0': {
            label: 'Corporation name',
            num: '1001',
            type: 'text',
            disabled: true
          },
          '2': {
            type: 'custom',
            format: ['{N9}RC{N4}', 'NR'],
            label: 'Business Number (BN)',
            num: '1002',
            disabled: true
          },
          showWhen: {
            fieldId: '103',
            compare: {is: '1'}
          }
        }
      ]
    },
    '1040': {
      num: '1040',
      fixedRows: true,
      infoTable: true,
      columns: [
        {},
        {width: '250px'},
        {}
      ],
      cells: [
        {
          '0': {
            label: 'Partnership name',
            num: '1005',
            type: 'text'
          },
          '1': {
            label: 'Partnership code',
            type: 'dropdown',
            num: '1006',
            options: [
              {value: '1', option: '1 If end partners are individuals or trusts'},
              {value: '2', option: '2 If end partners are corporations'},
              {value: '3', option: '3 If end partners are a combination of 1 and 2 mentioned above'}]
          },
          '2': {
            label: 'Partnership identification number',
            type: 'custom',
            format: ['{N9}RZ{N4}', 'NR'],
            num: '1012'
          },
          showWhen: {
            fieldId: '103',
            compare: {is: '2'}
          }
        }
      ]
    },
    '1020': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {},
        {}
      ],
      cells: [
        {
          '0': {
            label: 'Trust name',
            num: '1008',
            type: 'text'
          },
          '1': {
            label: 'Trust account number',
            num: '1010'
          },
          showWhen: {
            fieldId: '103',
            compare: {is: '3'}
          }
        }
      ]
    },
    '1030': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {},
        {},
        {
          width: '50px'
        },
        {
          width: '100px'
        },
        {}
      ],
      cells: [{
        '0': {
          label: 'First Name',
          type: 'text'
        },
        '1': {
          label: 'Last Name',
          type: 'text'
        },
        '2': {
          label: 'Initial',
          type: 'text'
        },
        '3': {
          label: 'Individual Code',
          type: 'dropdown',
          options: [
            {value: '1', option: '1 self-employed'},
            {value: '2', option: '2 non self-employed'}
          ]
        },
        '4': {
          label: 'Social Insurance Number',
          num: '1011'
        },
        showWhen: {
          fieldId: '103',
          compare: {is: '4'}
        }
      }
      ]
    },
    '1130': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'header': '',
          'type': 'none',
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          "header": "NAICS code",
          "type": "selector",
          selectorOptions: {
            title: 'NAICS Codes',
            items: wpw.tax.codes.naicsCodes
          }
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          "header": "NAICS code",
          "type": "selector",
          selectorOptions: {
            title: 'NAICS Codes',
            items: wpw.tax.codes.naicsCodes
          }
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          "header": "NAICS code",
          "type": "selector",
          selectorOptions: {
            title: 'NAICS Codes',
            items: wpw.tax.codes.naicsCodes
          }
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          "header": "NAICS code",
          "type": "selector",
          selectorOptions: {
            title: 'NAICS Codes',
            items: wpw.tax.codes.naicsCodes
          }
        }
      ],
      'cells': [
        {
          '0': {
            label: 'NAICS Code(s):'
          },
          '2': {
            num: '1131',
            cellClass: 'alignLeft'
          },
          '4': {
            num: '1132',
            cellClass: 'alignLeft'
          },
          '6': {
            num: '1133',
            cellClass: 'alignLeft'
          },
          '8': {
            num: '1134',
            cellClass: 'alignLeft'
          }
        }
      ]
    },
    '1150': {
      infoTable: true,
      fixedRows: true,
      showWhen: {fieldId: '119', compare: {is: '1'}},
      columns: [
        {
          'type': 'none',
          colClass: 'std-input-col-width'
        },
        {
          label: '1',
          type: 'text',
          colClass: 'std-input-col-width-2'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          label: '2',
          type: 'text',
          colClass: 'std-input-col-width-2'
        }
      ],
      cells: [
        {
          '0': {
            label: 'If <b>yes</b>, state the primary account numbers:'
          },
          '1': {
            num: '1151'
          },
          '3': {
            num: '1152'
          }
        }
      ]
    },
    '1160': {
      fixedRows: true,
      'columns': [{
        'header': 'First Name',
        type: 'text'
      },
        {
          'header': 'Last Name',
          type: 'text'
        },
        {
          'header': 'Telephone Number',
          'telephoneNumber': true,
          type: 'text'
        },
        {
          'header': 'Extension',
          'telephoneNumber': true,
          type: 'text',
          colClass: 'small-input-width'
        }],
      'cells': [
        {
          '0': {
            num: '1161'
          },
          '1':{
            num: '1162'
          },
          '2': {
            num: '1163'
          },
          '3': {
            num: '1163-ext'
          }
        }
      ]
    },
    '1170': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'type': 'none',
          width: '20px'
        },
        {
          colClass: 'std-input-col-width',
          type: 'text'
        },
        {
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            label: 'I,'
          },
        '1':{
            num: '1171'
        },
          '2': {
            label: ' , certify that the information given on these T106 Summary and Slips is, to the best of my ' +
            'knowledge, correct and complete.'
          }
        }
      ]

    },
    '1180': {
      infoTable: true,
      fixedRows: true,
      'columns': [
        {
          'header': 'Date',
          'type': 'date'
        },
        {
          'header': 'Authorized signing officer\'s, or representative\'s signature',
          type: 'text'
        },
        {
          'header': 'Position, title, officer\'s rank',
          type: 'text'
        }
      ],
      'cells': [
        {
          '0': {
            num: '1181'
          },
          '1':{
            num: '1182'
          },
          '2': {
            num: '1183'
          }
        }
      ]
    }
  }
})();
