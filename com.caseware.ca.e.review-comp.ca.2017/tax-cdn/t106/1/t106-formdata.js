(function() {
  'use strict';
  var currencyCodes = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.currencyCodes, 'value');

  wpw.tax.global.formData.t106 = {
    formInfo: {
      abbreviation: 'T106',
      title: 'Information Return of Non-Arm\'s Length Transactions with Non-Residents',
      subtitle: 'T106 Summary Form',
      headerImage: 'canada-federal',
      printTitle: 'Information Return of Non-arm\'s Length Transactions with Non-residents - T106 Summary Form',
      formFooterNum: 'T106 E (10/2017)',
      hideHeaderBox: true,
      neededRepeatForms: ['t106s'],
      category: 'Federal Tax Forms',
      agencyUseOnlyBox: {
        width: '200px',
        height: '140px',
        boxes: 10,
        textAbove: ' '
      },
      description: [
        {
          'text': ' • Refer to the instruction sheet before you complete the T106 Summary and Slips.'
        },
        {
          'text': ' • Complete a separate T106 Slip for each non-resident.'
        },
        {
          'text': ' • Refer to the instruction sheet for information on the penalties applicable to each T106 Slip.'
        }
      ]
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            labelClass: 'fullLength',
            hidePrint: true
          },
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            labelClass: 'fullLength',
            width: '20px',
            num: 'amendedIndicator',
            init: false,
            label: '• If this is an amended return, check this box. '
          },
          {
            type: 'infoField',
            inputType: 'dropdown',
            options: currencyCodes,
            num: '100',
            label: ' • If an election has been made to use a functional currency (see attached instructions),' +
            ' state the elected functional currency code',
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            inputType: 'dropdown',
            num: '102',
            options: [
              {option: '', value: ''},
              {option: 'New', value: '1'},
              {option: 'Unmodified', value: '2'},
              {option: 'Amended', value: '3'}
            ],
            label: 'What type of submission is this?'
          }
        ]
      },
      {
        header: 'Section 1 – Reporting person/partnership identification',
        rows: [
          {
            label: 'Indicate who you are reporting for, and complete the areas that apply',
            type: 'infoField',
            num: '103',
            init: '1',
            disabled: true,
            inputType: 'dropdown',
            options: [
              {value: '1', option: 'Corporation'},
              {value: '2', option: 'Partnership'},
              {value: '3', option: 'Trust'},
              {value: '4', option: 'Individual'}
            ]
          },
          {
            type: 'table',
            num: '1000'
          },
          {
            type: 'table',
            num: '1040'
          },
          {
            type: 'table',
            num: '1020'
          },
          {
            type: 'table',
            num: '1030'
          },
          {
            label: 'Reporting person/partnership address:'
          },
          {
            type: 'infoField', inputType: 'address',
            add1Num: '104', add2Num: '105', provNum: '106', cityNum: '107', countryNum: '108', postalCodeNum: '109'
          }
        ]
      },
      {
        header: 'Section 2 – Summary information',
        rows: [
          {
            type: 'splitTable', fieldAlignRight: true,
            side1: [
              {
                type: 'infoField',
                label: '1. For what taxation year are you filing these T106 form?',
                inputType: 'none'
              }, {
                type: 'infoField',
                inputType: 'dateRange',
                label1: 'Tax year-start', num: '110', disabled: true,
                source1: 'cp-tax_start',
                label2: 'Tax year-end', num2: '111', source2: 'cp-tax_end'
              }
            ],
            side2: [
              {
                type: 'infoField',
                inputType: 'radio',
                canClear: true,
                label: '2. Is this the first time you have filed T106 forms?',
                num: '112'
              },
              {
                labelClass: 'fullLength',
                showWhen: {fieldId: '112', compare: {is: '2'}}
              },
              {
                showWhen: {
                  fieldId: '112',
                  compare: {is: '2'}
                },
                type: 'infoField',
                inputType: 'date',
                label: 'If <b>no</b>, indicate the last tax year/fiscal period end for which the reporting' +
                ' person/partnership filed T106 forms',
                num: '1121'
              }
            ]
          },
          {type: 'horizontalLine'},
          {
            type: 'infoField',
            label: '3. Enter the total number of T106 Slips attached (automatically calculated):',
            num: '113',
            disabled: true,
            width: '10%'
          },
          {type: 'horizontalLine'},
          {
            type: 'splitTable', fieldAlignRight: true,
            side1: [
              {
                type: 'infoField',
                label: ' 4. Enter the total of all box "I" amounts from the T106 Slips attached' +
                ' (automatically calculated)',
                valueType: 'number',
                textAlign: 'right',
                filters: 'prepend $',
                disabled: true,
                num: '114'
              }
            ],
            side2: [
              {
                type: 'infoField',
                valueType: 'number',
                label: '5. Enter the gross revenue of the reporting person/partnership' +
                ' (automatically calculated)',
                textAlign: 'right',
                filters: 'prepend $',
                disabled: true,
                num: '115'
              }
            ]
          },
          {type: 'horizontalLine'},
          {
            label: '6. State the main business activities of the reporting person/partnership' +
            ' by entering the appropriate NAICS code(s) – see Instructions for NAICS codes.',
            labelClass: 'fullLength'
          },
          {type: 'table', num: '1130'},
          {type: 'horizontalLine'},
          {
            type: 'splitTable', fieldAlignRight: true,
            side1: [
              {
                type: 'infoField',
                inputType: 'radio',
                canClear: true,
                label: '7. Are any of the amounts (e.g., income, deductions, foreign tax credits)' +
                ' claimed by the reporting person/partnership in the current tax year/fiscal period affected' +
                ' by any completed, outstanding or anticipated requests for competent authority assistance?',
                num: '116'
              }
            ],
            side2: [
              {
                type: 'infoField',
                inputType: 'radio',
                canClear: true,
                label: '8. Are any of the amounts (e.g., income, deductions, foreign tax credits)' +
                ' claimed by the reporting person/partnership in the current tax year/fiscal period adjusted' +
                ' to reflect an assessment or a proposed assessment by a foreign tax administration?',
                num: '117'
              }
            ]
          },
          {type: 'horizontalLine'},
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: '9. Are any of the transfer pricing methodologies (TPM)' +
            ' used by the reporting person/partnership predicated on an advanced pricing arrangement (APA)' +
            ' or similar arrangement between any non-resident and a foreign tax administration?',
            num: '118'
          },
          {type: 'horizontalLine'},
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: '10. Does the reporting person/partnership have to file a NR4,' +
            ' T4, T4A or T4A-NR return(s) for the transactions reported in Part III of the T106 Slips?',
            num: '119'
          },
          {
            type: 'table',
            num: '1150'
          }
        ]
      },
      {
        header: 'Section 3 – Non-monetary or nil consideration',
        rows: [
          {
            type: 'infoField',
            inputType: 'radio',
            label: '1. Has the reporting person/partnership received from or provided to any non-resident' +
            ' any non-monetary consideration for any service,transfer of tangible or intangible property,' +
            ' or anything whatever, under an exchange, swap, barter, bonus, discount or other such arrangement?',
            num: '120'
          },
          {
            type: 'infoField',
            inputType: 'radio',
            canClear: true,
            label: '2. Has the reporting person/partnership provided to any non-resident any service,' +
            ' transfer of tangible or intangible property, or anything whatever, for which there was nil consideration?',
            num: '121'
          }
        ]
      },
      {
        header: 'Certification',
        rows: [
          {
            label: 'Person to contact for more information (please print)',
            labelClass: 'fullLength'
          },
          {
            type: 'table',
            num: '1160'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '1170'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '1180'
          }
        ]
      }
    ]
  }
})
();