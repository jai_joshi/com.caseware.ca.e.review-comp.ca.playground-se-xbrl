(function() {

  wpw.tax.create.calcBlocks('t106', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      if (field('amendedIndicator').get())
        field('102').assign('3');

      calcUtils.getGlobalValue('1001', 'cp', '002');
      calcUtils.getGlobalValue('1002', 'cp', 'bn');

      calcUtils.getGlobalValue('104', 'cp', '011');
      calcUtils.getGlobalValue('105', 'cp', '012');
      calcUtils.getGlobalValue('106', 'cp', '016');
      calcUtils.getGlobalValue('107', 'cp', '015');
      calcUtils.getGlobalValue('108', 'cp', '017');
      calcUtils.getGlobalValue('109', 'cp', '018');

      if (field('CP.957').get() == 1) {
        calcUtils.getGlobalValue('1161', 'cp', '951');
        calcUtils.getGlobalValue('1162', 'cp', '950');
      }
      else {
        field('1161').disabled(false);
        field('1162').disabled(false)
      }
      field('1171').assign(field('CP.950').get() + ' ' + field('CP.951').get());
      field('1171').source(field('CP.950'));
      calcUtils.getGlobalValue('1183', 'cp', '954');
      calcUtils.getGlobalValue('1181', 'cp', '955');
      calcUtils.getGlobalValue('1163', 'cp', '959');
      calcUtils.getGlobalValue('1163-ext', 'cp', '617');
      calcUtils.getGlobalValue('110', 'cp', 'tax_start');
      calcUtils.getGlobalValue('111', 'cp', 'tax_end');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.setRepeatSummaryValue('114', 't106s', '1961', 'add', {fieldId: '100', isValue: '4'});
      field('113').assign(calcUtils.numRepeatForms('t106s'));
      calcUtils.field('115').assign(
          calcUtils.sumRepeatGifiValue('t2s125', 150, 8299) +
          calcUtils.sumRepeatGifiValue('t2s125', 400, 9659));
      calcUtils.field('115').source(field('t2s125.8299'));
    });
  });
})();