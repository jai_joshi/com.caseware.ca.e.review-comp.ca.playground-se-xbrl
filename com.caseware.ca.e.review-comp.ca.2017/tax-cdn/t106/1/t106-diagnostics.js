(function() {
  wpw.tax.create.diagnostics('t106', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var mandatoryCheck = ['112', '114', '116', '117', '118', '119', '120', '121', '1131'];
    var mandatoryFields = [
      {'num': '1001', errorCode: '106.1001Check'}, {'num': '1002', errorCode: '106.1002Check'}, {'num': '102', errorCode: '106.102Check'},
      {'num': ['104', '105'], errorCode: '106.104Check'}, {'num': '106', errorCode: '106.106Check'}, {'num': '107', errorCode: '106.107Check'},
      {'num': '108', errorCode: '106.108Check'}, {'num': '109', errorCode: '106.109Check'}, {'num': '110', errorCode: '106.110Check'},
      {'num': '111', errorCode: '106.111Check'}, {'num': '112', errorCode: '106.112Check'}, {'num': '116', errorCode: '106.116Check'},
      {'num': '117', errorCode: '106.117Check'}, {'num': '118', errorCode: '106.118Check'}, {'num': '119', errorCode: '106.119Check'},
      {'num': '120', errorCode: '106.120Check'}, {'num': '121', errorCode: '106.121Check'}, {'num': '1131', errorCode: '106.1131Check'},
      {'num': '1171', errorCode: '106.1171Check'}, {'num': '1181', errorCode: '106.1181Check'}, {'num': '1183', errorCode: '106.1183Check'}];

    diagUtils.diagnostic('106.lastReturnFilingDate', common.prereq(common.check('112', 'isNo'), common.check('1121', 'isNonZero')));

    diagUtils.diagnostic('106.primaryAccountNumber', common.prereq(common.check('119', 'isYes'), common.check('1151', 'isNonZero')));

    diagUtils.diagnostic('106.requiredToFile', common.prereq(common.check('CP.to-26', 'isYes'), common.check('102', 'isNonZero')));

    for (var field in mandatoryFields) {
      requiredFieldCheck(mandatoryFields[field]);
    }
    diagUtils.diagnostic('106.eligibility', common.prereq(
      function (tools) {
      return tools.checkMethod(tools.list(mandatoryCheck), 'isNonZero')
    }, common.check('cp.to-26', 'isYes')));

    function requiredFieldCheck(field) {
      diagUtils.diagnostic(field.errorCode, common.prereq(common.check('102', 'isFilled'),
        function(tools) {
        return tools.checkMethod(tools.list(mandatoryCheck), 'isNonZero')
      }, common.check(field.num, 'isNonZero')));
    }

  });
})();
