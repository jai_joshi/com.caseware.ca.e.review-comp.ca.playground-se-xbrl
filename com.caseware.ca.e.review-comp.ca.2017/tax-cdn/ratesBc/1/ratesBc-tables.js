(function() {
  wpw.tax.global.tableCalculations.ratesBc = {
    "1008": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          colClass: 'std-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "10px",
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        }
      ],
      "cells": [
        {
          "0": {
            "label": "Effective Date"
          },
          "2": {
            "label": "Amount",
            "labelClass": "center fullLength"
          },
          "4": {
            "label": "Higher rate",
            "labelClass": "center fullLength"
          },
          "6": {
            "label": "Lower rate",
            "labelClass": "center fullLength"
          }
        }
      ]
    },
    "1009": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          colClass: 'std-input-col-width',
          "type": "date",
          cellClass: 'alignCenter'
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true
        }
      ],
      "cells": [
        {
          "2": {
            "num": "992"
          },
          "4": {
            "num": "993",
            decimals: 1
          },
          "6": {
            "num": "994",
            decimals: 1
          }
        },
        {
          "2": {
            "num": "995"
          },
          "4": {
            "num": "996",
            decimals: 1
          },
          "6": {
            "num": "997",
            decimals: 1
          }
        },
        {
          "2": {
            "num": "998"
          },
          "4": {
            "num": "999"
          },
          "6": {
            "num": "1100",
            decimals: 1
          }
        },
        {
          "2": {
            "num": "1101"
          },
          "4": {
            "num": "1102"
          },
          "6": {
            "num": "1103",
            decimals: 1
          }
        },
        {
          "2": {
            "num": "1104"
          },
          "4": {
            "num": "1105"
          },
          "6": {
            "num": "1106",
            decimals: 1
          }
        },
        {
          "2": {
            "num": "1107"
          },
          "4": {
            "num": "1108",
            decimals: 1
          },
          "6": {
            "num": "1109",
            decimals: 1
          }
        },
        {
          "2": {
            "num": "1110"
          },
          "4": {
            "num": "1111",
            decimals: 1
          },
          "6": {
            "num": "1112",
            decimals: 1
          }
        },
        {
          "2": {
            "num": "1113"
          },
          "4": {
            "num": "1114"
          },
          "6": {
            "num": "1115",
            decimals: 1
          }
        },
        {
          "2": {
            "num": "1116"
          },
          "4": {
            "num": "1117"
          },
          "6": {
            "num": "1118",
            decimals: 1
          }
        },
        {
          "2": {
            "num": "1119"
          },
          "4": {
            "num": "1120"
          },
          "6": {
            "num": "1121",
            decimals: 1
          }
        }
      ]
    }
  }
})();
