(function() {

  wpw.tax.global.formData.ratesBc = {
      formInfo: {
        abbreviation: 'ratesBc',
        title: 'British Columbia Table of Rates and Values',
        showCorpInfo: true,
        description: [
          {type: 'heading', text: 'British Columbia'}
        ],
        category: 'Rates Tables'
      },
      sections: [
        {
          'header': 'Schedule 421 - British Columbia mining exploration tax credit',
          'rows': [
            {
              'label': 'Tax credit rate ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '2030'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Additional tax credit rate for the qualified mining exploration expenses incurred after February 20, 2007,in the prescribed area ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '2031'
                  }
                }
              ],
              'indicator': '%'
            }
          ]
        },
        {
          'header': 'Schedule 422 (T1196) - British Columbia Film and Television Tax Credit',
          'rows': [
            {
              'label': 'Eligibility',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Total copyright',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '4220'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Production Cost Limit',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Rate for Amount G',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '4221'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Basic Tax Credit',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Rate for Amount AA',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '4222'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Regional Tax Credit',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Rate for Amount JJ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '4223'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Rate for Amount KK',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '4224'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'DAVE Tax Credit',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Rate for Amount 1',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '4225',
                    'decimals': 1
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Rate for Amount 2',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '4226'
                  }
                }
              ],
              'indicator': '%'
            }
          ]
        },
        {
          'header': 'Schedule 423 (T1197) - British Columbia production services tax credit',
          'rows': [
            {
              'label': 'Production services tax credit',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Basic tax credit applicable rate',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '2040'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Additional tax credit applicable rate for expenses incurred after December 31, 2007',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '2041'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Additional tax credit applicable rate when principal photography begins after February 28, 2010',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '2042'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Regional production services tax credit',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Applicable rate',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '2043'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Distant location production sevices tax credit',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Applicable rate',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '2044'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Digital animation or visual effects production services tax credit',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Basic tax credit applicable rate',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '2045'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Additional tax credit applicable rate',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '2046',
                    'decimals': 1
                  }
                }
              ],
              'indicator': '%'
            }
          ]
        },
        {
          'header': 'Schedule 425 (T666) British Columbia Manufacturing and Processing Tax Credit',
          'rows': [
            {
              'label': 'Applicable rate(Part 4 & 5)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '488'
                  }
                }
              ],
              'indicator': '%'
            }
          ]
        },
        {
          'header': 'Schedule 426 - British Columbia Manufacturing And Processing Tax Credit ',
          'rows': [
            {
              'label': 'Current year credit earned calculation rate',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '322'
                  }
                }
              ],
              'indicator': '%'
            }
          ]
        },
        {
          'header': 'Schedule 427 - British Columbia Corporation Tax Calculation ',
          'rows': [
            {
              'type': 'table',
              'num': '1008'
            },
            {
              'type': 'table',
              'num': '1009'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b>Amount 3</b>',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Rate in the tax year in 2016',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '407'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Rate in the tax year in 2017',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '413'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Rate in the tax year in 2018',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '418'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Rate in the tax year in 2019',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '423'
                  }
                }
              ],
              'indicator': '%'
            }
          ]
        },
        {
          'header': 'Schedule 428 - British Columbia training tax credit',
          'rows': [
            {
              'label': 'Basic credit',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Taxation year',
              'input1Header': 'Rate',
              'input2Header': 'Maximum amount that can be claimed'
            },
            {
              'label': '2007-01-01',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '2100'
                  }
                },
                {
                  'input': {
                    'num': '2101'
                  },
                  'padding': {
                    'type': 'text',
                    'data': '%'
                  }
                }
              ]
            },
            {
              'label': '2009-07-01',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '2102'
                  }
                },
                {
                  'input': {
                    'num': '2103'
                  },
                  'padding': {
                    'type': 'text',
                    'data': '%'
                  }
                }
              ]
            },
            {
              'label': 'Completion credit',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Employee that completes level 3',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Rate',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '2110'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Maximum amount that can be claimed',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '2111'
                  }
                }
              ]
            },
            {
              'label': 'Employee that completes level 4 or higher',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Rate',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '2112'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Maximum amount that can be claimed',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '2113'
                  }
                }
              ]
            },
            {
              'label': 'Enhanced credit',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'Employee in the first two years of any Red Seal program',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Rate for the salary and wages payable before June 3, 2010 ',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '2114'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Rate for the salary and wages payable after June 2, 2010',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '2115',
                    'decimals': 1
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Maximum amount that can be claimed',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '2116'
                  }
                }
              ]
            },
            {
              'label': 'Employee in the first two years of a non-Red Seal program',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Taxation year',
              'labelClass': 'tabbed',
              'input1Header': 'Rate',
              'input2Header': 'Maximum amount that can be claimed'
            },
            {
              'label': '2007-01-01',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '2117'
                  }
                },
                {
                  'input': {
                    'num': '2118'
                  },
                  'padding': {
                    'type': 'text',
                    'data': '%'
                  }
                }
              ]
            },
            {
              'label': '2007-01-01',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '2119'
                  }
                },
                {
                  'input': {
                    'num': '2120'
                  },
                  'padding': {
                    'type': 'text',
                    'data': '%'
                  }
                }
              ]
            },
            {
              'label': 'Employee that completes level 3',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Rate',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '2121',
                    'decimals': 1
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Maximum amount that can be claimed',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '2122'
                  }
                }
              ]
            },
            {
              'label': 'Employee that completes level 4 or higher',
              'labelClass': 'fullLength'
            },
            {
              'label': 'Rate',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '2123',
                    'decimals': 1
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Maximum amount that can be claimed',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'labelCellClass': 'tabbed',
              'columns': [
                {
                  'input': {
                    'num': '2124'
                  }
                }
              ]
            }
          ]
        },
        {
          'header': 'Schedule 429 - British Columbia interactive digital media tax credit',
          'rows': [
            {
              'label': 'Eligible salary and wages threshold to qualify for the BCIDMTC',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '2140'
                  }
                }
              ]
            },
            {
              'label': 'Tax credit rate',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '2141',
                    'decimals': 1
                  }
                }
              ],
              'indicator': '%'
            }
          ]
        },
        {
          'header': 'Schedule 430 - British Columbia shipbuilding and ship repair industry tax credit',
          'rows': [
            {
              'label': 'Part 1 - Basic tax credit (Red Seal and non-Red Seal apprenticeship programs)',
              'labelClass': 'fullLenght bold'
            },
            {
              'label': 'D1 - Column C1 x 20% (the same for D2, D3)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'decimals': 1,
                    'num': '510'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'F1 - $5,250 minus column E1*** (the same for E2, E3)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '515'
                  }
                }
              ]
            },
            {
              'label': 'Part 3 - Enhanced tax credit (Red Seal and non-Red Seal apprenticeship programs)',
              'labelClass': 'fullLenght bold'
            },
            {
              'label': 'D4 - Column C4 x 30% (the same for D5, D6)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '520'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'F4 - $7,875 minus column E4*** (the same for E5, E6)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '525'
                  }
                }
              ]
            }
          ]
        },
        {
          'header': 'FIN 542 - Logging tax return of income',
          'rows': [
            {
              'label': '<b>Rates for the calculation of Logging Tax Payable</b>'
            },
            {
              'label': 'Taxpayer&#039;s income derived from logging operations in British Columbia',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '100'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Credit that would have been allowable under section 127(1) of the <i>Income Tax Act</i> (Canada)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '101'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': '<br><b>Rates for the calculation of Federal Logging Tax Credits</b>'
            },
            {
              'label': 'Taxpayer&#039;s income for the year from logging operations in the province',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '105',
                    'decimals': 2
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Provincial credit allowable',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '106'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': '<br><b>Rates for the Processing Allowance</b>'
            },
            {
              'label': 'Original cost of assets owned and used in changing logs into forest products',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '110'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Processing income',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '111'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'label': 'Processing income',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '112'
                  }
                }
              ],
              'indicator': '%'
            }
          ]
        }
      ]
    };
})();
