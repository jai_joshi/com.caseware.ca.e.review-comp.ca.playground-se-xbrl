(function() {

  wpw.tax.create.diagnostics('t2s410', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('4100002', common.prereq(common.requireFiled('T2S410'),
        function(tools) {
          var table = tools.field('1000');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[0], row[1]], 'isNonZero'))
              return tools.requireAll([row[0], row[1]], 'isNonZero');
            else return true;
          });
        }));

  });
})();
