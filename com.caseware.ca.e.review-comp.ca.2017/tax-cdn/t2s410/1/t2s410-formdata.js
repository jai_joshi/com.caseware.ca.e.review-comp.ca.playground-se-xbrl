(function() {
  'use strict';

  wpw.tax.create.formData('t2s410', {
    formInfo: {
      abbreviation: 'T2S410',
      title: 'S410 - Additional Certificate Numbers for the Saskatchewan Film Employment Tax Credit',
      schedule: 'Schedule 410',
      highlightFieldsets: true,
      headerImage: 'cw',
      category: 'Saskatchewan Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {label: 'For use by a corporation that has more than one certificate number for the Saskatchewan film employment tax credit.'},
          {
            type: 'table',
            num: '1000'
          }
        ]
      }
    ]
  });
})();
