(function() {

  wpw.tax.global.tableCalculations.t2s410 = {
    "1000": {
      hasTotals: true,
      "showNumbering": true,
      "columns": [
        {
          "header": "Certificate number ",
          "tn": "100"
        },
        {
          "header": "Amount of the Saskatchewan film employment tax credit<br>$",
          "tn": "200",
          "total": true,
          "totalNum": '300',
          "totalTn": '300',
          "totalMessage": 'Total amount of the Saskatchewan film employment tax credit<br>Enter this amount on line 643 in Part 2 of Schedule 5. '
        }
      ]
    }
  };
})();