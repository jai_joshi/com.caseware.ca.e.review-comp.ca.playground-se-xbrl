(function() {

  wpw.tax.create.calculations('importpage', function(paramsObj) {

    var num = paramsObj.numChanged;
    var calcUtils = paramsObj.calcUtils;
    var field = calcUtils.field;

    // TODO: manual adjustment import
    if (field('000').get() == true) {
      field('MAstatus').assign('Starting');

      //Parsing the table
      field('2000').getRows().forEach(function(row, ri) {

        var formid = row[0].get();
        var fieldid = row[1].get();
        var value = row[2].get().toString();
        var rowIndex = row[3].get();
        var colIndex = row[4].get();
        var seq = row[5].get();
        var type;
        var repeatformid = wpw.tax.form(formid, seq);

        if (formid == '') {
          wpw.tax.form('importpage').field('2000').getCol(7)[ri].set('Not imported');
          return;
        }
        else if (fieldid == '') {
          wpw.tax.form('importpage').field('2000').getCol(7)[ri].set('Not imported');
          return;
        }
        else if (fieldid.indexOf('/' != -1)) {
          value = {"year": value.split('/')[2], "month": value.split('/')[1], "day": value.split('/')[0]};
        }

        //Set values through accessor
        else if (wpw.tax.create.retrieve('formData', formid).formInfo.isRepeatForm != undefined) {
          seq = (Number(seq) != 0 && !isNaN(Number(seq))) ? seq : 1;
          if (!isNaN(Number(rowIndex)) && !isNaN(Number(colIndex)) && wpw.tax.global.tableCalculations[formid][fieldid]) {
            wpw.tax.utilities.addTableRow(formid, fieldid, rowIndex);
            wpw.tax.utilities.addTableRow(formid, fieldid, rowIndex + 1);
            wpw.tax.form('importpage').field('2000').getCol(7)[ri].set(wpw.tax.form(formid, seq).field(fieldid).getCol(colIndex)[rowIndex].set(value));
          }
        }
        else {
          if (!isNaN(Number(rowIndex)) && !isNaN(Number(colIndex)) && wpw.tax.global.tableCalculations[formid][fieldid]) {
            type = wpw.tax.form(formid).field(fieldid).getCol(colIndex)[rowIndex].localData.type;
            wpw.tax.utilities.addTableRow(formid, fieldid, rowIndex);
            wpw.tax.utilities.addTableRow(formid, fieldid, rowIndex + 1);
            wpw.tax.form('importpage').field('2000').getCol(7)[ri].set(wpw.tax.form(formid).field(fieldid).getCol(colIndex)[rowIndex].set(value));
          }
          else {
            type = wpw.tax.form(formid).field(fieldid).config.type;
            wpw.tax.form('importpage').field('2000').getCol(7)[ri].set(wpw.tax.form(formid).field(fieldid).set(value));
          }
        }
        // Provide type information
        type = type == undefined ? 'Number or text' : type;
        wpw.tax.form('importpage').field('2000').getCol(6)[ri].set(type);
      });

      //Call calculation from import services

      //Reset import button
      field('000').assign(false);
      field('000').disabled(false);
      field('MAstatus').assign('Finished importing, please refresh this page for re-calculations');
    }
    else {
      field('000').disabled(false);
      field('MAstatus').assign('Can input data.');
    }

    //TODO: check repeat form sequence number
    // if (field('006').get()) {
    //   //Checking the inputted values
    //
    //   //Verify whether the form existing
    //
    //   //Pass in the result to the result as a string under all situation
    //
    //   //Reset check button
    //   field('006').assign(false);
    //   field('MAcheck').assign('Finished checking');
    //
    // }

  });

})();
