(function() {
  // function cellrows() {
  //   if (wpw.tax.utilities.logs && wpw.tax.utilities.logs != {}) {
  //     var store =  wpw.tax.utilities.logs;
  //     return store;
  //   }
  //   else {
  //     return {
  //       0: {label: 'Schedule'},
  //       1: {label: 'Input field id'},
  //       2: {label: 'Table num'},
  //       3: {label: 'Row index'},
  //       4: {label: 'Column index'},
  //       5: {label: 'Value from the uploaded json'},
  //       6: {label: 'Value from the imported global'},
  //       7: {label: 'Truth or Dare'}
  //     };
  //   }
  // }

  wpw.tax.global.tableCalculations.importpage = {
    "2000": {
      "infoTable": true,
      columns: [
        {
          header: 'Form id',
          type: 'text'
        },
        {
          header: 'Field id',
          type: 'text'
        },
        {
          header: 'Value to be imported',
          type: 'text'
        },
        {
          header: 'Row Index'
        },
        {
          header: 'Column Index'
        },
        {
          header: 'Sequence' + 'Note 1'.sup()
        },
        {
          header: 'Type',
          type: 'text',
          disabled: true
        },
        {
          header: 'Import?',
          type: 'text',
          disabled: true
        }
      ]
      // cells: [cellrows()]
    }
  }
})();
