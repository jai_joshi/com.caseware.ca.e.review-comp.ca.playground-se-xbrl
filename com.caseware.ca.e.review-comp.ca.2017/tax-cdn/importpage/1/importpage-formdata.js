(function() {
  'use strict';
  wpw.tax.global.formData.importpage = {
    formInfo: {
      abbreviation: 'importpage',
      title: 'Ah-hoc Import Page',
      highlightFieldsets: true,
      category: 'Workcharts'
    },
    sections: [
      {
        'header': 'Step 1 - Import the Taxprep csv in the second button below the Tax Window',
        'rows': [
          {
            'label': 'and then wait until the calculations finish running'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Status: '
          },
          {
            'disabled': true,
            'type': 'infoField',
            'inputType': 'textArea',
            'num': '1000'
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'asterisk',
                'items': [
                  {
                    'label': 'Importing - File uploaded and waiting for calculation process'
                  },
                  {
                    'label': 'Import calculations started'
                  },
                  {
                    'label': 'Import calculations finished, please wait for 20 seconds to settle down.'
                  },
                  {
                    'label': 'Refreshed - this page has been refreshed or emptied cached, please import again with a new engagement,'
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        'header': 'Step 2 - Click the <b>Download Global</b> button for the global json converted into csv',
        'rows': [
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'asterisk',
                'items': [
                  {
                    'label': 'If the global csv for downloading cannot obtain the name from the test file, a simple time stamp will be the name of the file'
                  },
                  {
                    'label': 'If you clear the cached or refresh the page, the test file name will be lost and the time stamp will be used'
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        'header': 'Step 3 - Import manual adjustments',
        'rows': [
          {
            'label': 'Please enter the import value in the following after the import',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Test scenario name',
            'type': 'infoField',
            'num': 'testname',
            'disabled': true
          },
          {
            'label': 'Manual adjustment import status',
            'type': 'infoField',
            'num': 'MAstatus',
            'disabled': true
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Note 1: Sequence number can be zero or blank ir not applicable - zero also defines a form of not being a repeat form',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Finished data table',
            'labelClass': 'bold',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'labelWidth': '70%',
            'num': '000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Not Available Yet</b>  Check repeat form sequence number by reviewing value - Not Available Yet',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter the value you would like to check for a particular repeat form, in order to ensure the right repeat form'
          },
          {
            'label': 'Form id',
            'labelWidth': '70%',
            'type': 'infoField',
            'num': '001'
          },
          {
            'label': 'Field id',
            'labelWidth': '70%',
            'type': 'infoField',
            'num': '002'
          },
          {
            'label': 'Sequence to be checked',
            'labelWidth': '70%',
            'type': 'infoField',
            'inputType': 'fixedSizeBox',
            'boxes': 2,
            'num': '003'
          },
          {
            'label': 'Row Index',
            'labelWidth': '70%',
            'num': '004',
            'type': 'infoField',
            'inputType': 'fixedSizeBox',
            'boxes': 2
          },
          {
            'label': 'Column Index',
            'labelWidth': '70%',
            'num': '005',
            'type': 'infoField',
            'inputType': 'fixedSizeBox',
            'boxes': 2
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Check',
            'labelClass': 'bold',
            'type': 'infoField',
            'inputType': 'singleCheckbox',
            'labelWidth': '70%',
            'num': '006'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Result - values',
            'labelWidth': '70%',
            'num': '007',
            'type': 'infoField',
            'disabled': true
          },
          {
            'label': 'Manual adjustment - repeat form check status',
            'labelWidth': '70%',
            'type': 'infoField',
            'num': 'MAcheck',
            'disabled': true
          }
        ]
      }
    ]
  }
})();



