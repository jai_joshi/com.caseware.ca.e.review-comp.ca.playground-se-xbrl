(function() {
  'use strict';

  wpw.tax.global.formData.t2s25 = {
      formInfo: {
        abbreviation: 'T2S25',
        title: 'Investment in Foreign Affiliates',
        //TODO: DO NOT DELETE THIS LINE: subtitle
        //subTitle: '(1998 and later taxation years)',
        schedule: 'Schedule 25',
        formFooterNum: 'T2 SCH 25 (99)',
        showCorpInfo: true,
        description: [
          {text: ''},
          {
            type: 'list',
            items: [
              'If the corporation is resident, or is deemed to be resident, in Canada and owns shares in one or ' +
              'more foreign affiliates, as defined in subsection 95(1) of the <i>Income Tax Act</i>, please complete the' +
              ' information below.',
              'This schedule does not have to be filed by a "non-resident-owned investment corporation" as ' +
              'defined in subsection 133(8) of the <i>Income Tax Act</i>'
            ]
          }
        ],
        category: 'Federal Tax Forms'
      },
      sections: [
        {
          hideFieldset: true,
          rows: [
            {
              type: 'table', num: '050'
            }
          ]
        }
      ]
    };
})();
