(function() {
  wpw.tax.create.diagnostics('t2s25', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0250001', common.prereq(common.check(['t2j.169'], 'isChecked'), common.requireFiled('T2S25')));

    diagUtils.diagnostic('0250002', common.prereq(common.requireFiled('T2S25'),
        function(tools) {
          var table = tools.field('050');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[0], row[1], row[2]], 'isNonZero'))
              return tools.requireAll([row[0], row[1], row[2]], 'isNonZero');
            else return true;
          });
        }));
  });
})();

