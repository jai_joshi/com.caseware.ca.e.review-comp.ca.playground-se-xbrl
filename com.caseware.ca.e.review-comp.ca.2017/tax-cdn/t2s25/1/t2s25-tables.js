(function() {
  wpw.tax.global.tableCalculations.t2s25 = {
    "050": {
      "showNumbering": true,
      "columns": [{
        "header": "Name of foreign affiliate",
        "tn": "100","validate": {"or":[{"length":{"min":"1","max":"175"}},{"check":"isEmpty"}]},
        "width": "70%",
        type: 'text'
      },
        {
          "header": "Equity % held",
          "tn": "200",
          "width": "15%",
          filters: 'decimals 2',
          validate: 'percent',
          decimals: 3
        },
        {
          "header": "Is foreign affiliate: 1- Controlled, 2- Other",
          "tn": "300",
          "width": "15%",
          "type": "dropdown",
          "options": [{
            "value": "1",
            "option": "1- Controlled"
          },
            {
              "value": "2",
              "option": "2- Other"
            }]
        }]
    }
  }
})();
