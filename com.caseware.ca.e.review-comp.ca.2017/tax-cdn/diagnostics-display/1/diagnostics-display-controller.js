wpw.main.controller('diagnostics-display-controller', ['$scope', 'saveService',
  function($scope, saveService) {
    'use strict';

    $scope.filter = {};
    var key;

    saveService.executeOnLoad(function() {
      wpw.tax.global.values = saveService.values;
      key = wpw.tax.global.engagementProperties.id + wpw.tax.global.engagementProperties.user + wpw.tax.global.currentForm + '-category';
      $scope.type = wpw.utilities.getLocalSetting(key) || 'ALL';
      $scope.filter = {category: $scope.type};
    });

    /**
     * Call this function to update the filter.
     * @type {?function}
     */
    $scope.updateFilter = null;
    $scope.setUpdateFilter = function(updateFn) {
      $scope.updateFilter = updateFn;
    };

    function updateFilter() {
      if (!$scope.updateFilter) return;
      var type = $scope.type;
      if ($scope.type == 'ALL') {
        type = undefined;
      }
      $scope.updateFilter({
        category: type
      });
    }

    $scope.$on('end.calcSession', function() {
      updateFilter();
    });

    $scope.$watch('type', function() {
      updateFilter();
    });
  }
]);