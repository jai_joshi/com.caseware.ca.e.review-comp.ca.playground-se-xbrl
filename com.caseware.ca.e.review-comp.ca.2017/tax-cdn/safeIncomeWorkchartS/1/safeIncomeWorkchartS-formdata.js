(function() {
  'use strict';

  wpw.tax.create.formData('safeIncomeWorkchartS', {
    formInfo: {
      abbreviation: 'safeIncomeWorkchartS',
      title: 'Safe Income Scenario Summary',
      showCorpInfo: true,
      dynamicFormWidth: true,
      category: 'Workcharts'
    },
    sections: [
      {
        rows: [
          {type: 'table', num: '981'}
        ]
      }
    ]
  });
})();
