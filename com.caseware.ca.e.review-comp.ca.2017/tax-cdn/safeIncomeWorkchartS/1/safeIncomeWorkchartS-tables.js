(function() {

  var columnWidth = '107px';
  var columnSpaceWidth = '3px';
  var descriptionWidth = '246px';

  function getTableColumns(colsObjArr) {
    var colsArr = [];

    if (!angular.isArray(colsObjArr))
      colsObjArr = [colsObjArr];

    colsObjArr.forEach(function(colObj) {
          var colWidth = colObj.width || columnWidth;
          var colType = colObj.type;
          var colHeader = colObj.header || '';

          colsArr.push(
              {
                //amount
                header: '<b>' + colHeader + '</b>',
                type: colType,
                width: colWidth,
                linkedFieldId: colObj.linkedFieldId,
                linkedRowIndex: colObj.linkedRowIndex,
                linkedColIndex: colObj.linkedColIndex
              },
              {
                //space between 2 blocks
                type: 'none',
                colClass: 'std-spacing-width'
              }
          )
        }
    );

    colsArr.push(
        {
          //end
          type: 'none',
          width: '100%'
        }
    );

    return colsArr;
  }

  wpw.tax.create.tables('safeIncomeWorkchartS', {
    '981': {
      fixedRows: true,
      infoTable: true,
      scrollx: true,
      initRows: 0,
      dividers: [11, 17],
      columns: getTableColumns(
          [
            {header: 'Scenario description', type: 'text', width: descriptionWidth, linkedFieldId: '1013'},
            {
              header: 'Net income (loss) from financial statements',
              linkedFieldId: '992',
              linkedRowIndex: '1',
              linkedColIndex: '8'
            },
            {header: 'Total adjustments', linkedFieldId: 'totalAdjPart1'},
            {
              header: 'Net income (loss) for income tax purposes',
              linkedFieldId: '997',
              linkedRowIndex: '0',
              linkedColIndex: '8'
            },
            {header: 'Total adjustments', linkedFieldId: 'totalAdjPart2'},
            {header: 'SIOH - before dividends', linkedFieldId: '995', linkedRowIndex: '0', linkedColIndex: '8'},
            {header: 'Opening retained earnings', linkedFieldId: '998', linkedRowIndex: '0', linkedColIndex: '8'},
            {header: 'Total adjustments', linkedFieldId: 'totalAdjPart3'},
            {header: 'Ending retained earnings', linkedFieldId: '1001', linkedRowIndex: '0', linkedColIndex: '8'},
            {
              header: 'Dividend paid in current fiscal period ',
              linkedFieldId: '1002',
              linkedRowIndex: '0',
              linkedColIndex: '8'
            },
            {header: 'Total share number outstanding', linkedFieldId: '1002', linkedRowIndex: '1', linkedColIndex: '8'},
            {header: 'Dividend per share', linkedFieldId: '1002', linkedRowIndex: '2', linkedColIndex: '8'}
          ]
      )
    }
  })
})
();
