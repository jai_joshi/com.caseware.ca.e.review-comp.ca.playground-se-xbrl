(function() {
  wpw.tax.create.formData('t2s404', {
    formInfo: {
      abbreviation: 'T2S404',
      title: 'S404 - Saskatchewan Manufacturing and Processing Profits Tax Reduction',
      schedule: 'Schedule 404',
      formFooterNum: 'T2 SCH 404 E (16)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if the corporation had:',
              sublist: [
                'a <b>permanent establishment</b> (as defined under subsection 400(2) of the Regulations) in Saskatchewan at any time in the year;',
                'taxable income earned in the year in Saskatchewan; and',
                '<b>Canadian manufacturing and processing profits</b>, as defined under subsection 125.1(3) of the Act, earned in Saskatchewan.'
              ]
            },
            {label: 'All legislative references are to the federal <i>Income Tax Act</i> and the federal <i>Income Tax Regulations</i>.'},
            {label: 'Canadian manufacturing and processing profits earned in Saskatchewan include profits from generating or producing electrical energy or steam for sale in Saskatchewan.'},
            {label: 'This schedule is a worksheet only and does not have to be filed with your T2 return.'}
          ]
        }
      ],
      category: 'Saskatchewan Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Income eligible for the Saskatchewan manufacturing and processing profits tax reduction',
        'spacing': 'R_mult_tn2',
        'rows': [
          {
            'label': 'Canadian manufacturing and processing profits (amount from line 200 of Schedule 27)*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Amount E from Schedule 411**',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '101'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Excess (amount A <b>minus</b> amount B, if negative enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '103'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Taxable income (amount from line 360 of the T2 return) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '104'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Amount E from Schedule 411**',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '105'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'label': 'Aggregate investment income (amount from line 440 of the T2 return)**',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '106'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': 'di_table_1'
          },
          {
            'label': 'Subtotal (<b>add</b> amounts E to G)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '110'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '111'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': 'Excess (amount D minus amount H, if negative, enter "0")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '112'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': '<b>Income eligible for the Saskatchewan manufacturing and processing profits tax reduction</b><br>(amount C or I, whichever is less)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '113'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'asterisk',
                'items': [
                  {
                    'label': 'If the corporation only generated or produced electrical energy or steam for sale in Saskatchewan, use the amount from line 210 of Schedule27'
                  },
                  {
                    'label': 'Applies only to a corporation that was a Canadian-controlled private corporation throughout the tax year. '
                  },
                  {
                    'label': 'Calculate the amount of federal foreign business income tax credit deductible at line 636 of the T2 return without reference to the corporate tax reductions under section 123.4 of the Act.'
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 2 - Saskatchewan manufacturing and processing profits tax reduction',
        'rows': [
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Saskatchewan manufacturing and processing profits tax reduction (amount K multiplied by amount L) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'label': 'Enter amount M on line 626 of Schedule 5, Tax Calculation Supplementary - Corporations.',
            'labelClass': 'fullLength'
          },
          {
            'label': '* Includes the territories and the offshore jurisdictions for Nova Scotia and Newfoundland and Labrador.',
            'labelClass': 'tabbed fullLength'
          }
        ]
      }
    ]
  });
})();
