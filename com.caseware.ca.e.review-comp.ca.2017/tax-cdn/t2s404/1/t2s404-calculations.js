(function() {

  wpw.tax.create.calcBlocks('t2s404', function(calcUtils) {
    calcUtils.calc(function(calcUtils) {
      var num = '107';
      var num2 = '108';
      var midnum = '109';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get();
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('109', 'ratesSk', '700');

      if (field('T2S27.210').get() && field('CP.750').get() == "SK") {
        field('100').assign(field('T2S27.210').get());
        field('100').source(field('T2S27.210'))
      }
      else {
        field('100').assign(field('T2S27.200').get());
        field('100').source(field('T2S27.200'))
      }
      calcUtils.getGlobalValue('101', 'T2S411', '104');
      calcUtils.subtract('103', '100', '101');
      calcUtils.getGlobalValue('104', 'T2J', '360');
      calcUtils.getGlobalValue('105', 'T2S411', '104');
      calcUtils.getGlobalValue('106', 'T2J', '440');
      calcUtils.getGlobalValue('107', 'T2J', '636');
      calcUtils.sumBucketValues('110', ['105', '106', '108']);
      calcUtils.equals('111', '110');
      calcUtils.subtract('112', '104', '111');
      field('113').assign(Math.min(field('112').get(), field('103').get()));
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('523', 'ratesSk', '701');
      calcUtils.equals('420', '113');

      var taxableIncomeAllocation = calcUtils.getTaxableIncomeAllocation('SK');
      field('421').assign(taxableIncomeAllocation.provincialTI);
      field('422').assign(taxableIncomeAllocation.allProvincesTI);

      field('421').source(taxableIncomeAllocation.provincialTISourceField);
      field('422').source(taxableIncomeAllocation.allProvincesTISourceField);

      field('521').assign(field('421').get());
      field('522').assign(field('422').get());

      field('423').assign(field('422').get() == 0 ? 0 : field('420').get() * field('421').get() / field('422').get());

      field('524').assign(field('522').get() == 0 ? 0 : field('523').get() * field('521').get() / field('522').get());
      field('200').assign(field('423').get() * field('524').get() / 100);
    });
  })
}());