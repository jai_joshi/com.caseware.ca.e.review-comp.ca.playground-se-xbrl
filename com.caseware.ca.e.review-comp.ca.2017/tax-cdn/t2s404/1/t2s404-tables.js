(function() {

  wpw.tax.global.tableCalculations.t2s404 = {
    'di_table_1': {
      'num': 'di_table_1',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Federal foreign business income tax credit<br>(amount from line 636*** of the T2 return)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '107'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '109',
            'init': undefined
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '108'
          },
          '8': {
            'label': 'G'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    "1000": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "width": '40px',
          "type": "none"
        },
        {
          "width": '100px',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          "width": "30px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          // "width": "200px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "10px",
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          "type": "none",
          cellClass: 'alignCenter',
          colClass: 'std-padding-width'
        },
        {
          "width": "10px",
          "formField": true
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        }
      ],
      "cells": [
        {
          "1": {"label": "Amount J"},
          "2": {"num": "420", "disabled": true},
          "3": {"label": " x ", "labelClass": "center"},
          "4": {"label": "Taxable income earned in Saskatchewan", cellClass: 'singleUnderline'},
          "6": {"num": "421", cellClass: 'singleUnderline', "disabled": true},
          "8": {"type": "none"},
          "9": {"label": " = "},
          "10": {"num": "423"},
          "11": {"label": 'K'}
        },
        {
          "2": {"type": "none"},
          "4": {"label": "Taxable income earned in all provinces*", "labelClass": "center"},
          "6": {"num": "422", "disabled": true},
          "8": {"type": "none"},
          "10": {"type": "none"}
        }
      ]
    },
    "2000": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          "type": "none",
          cellClass: 'alignCenter',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-padding-width',
          "formField": true
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          // "width": "200px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "10px",
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          "width": "30px",
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          "width": "100px",
          "formField": true
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          "formField": true
        },
        {
          "type": "none",
          colClass: 'std-padding-width'
        }
      ],
      "cells": [
        {
          // "0": {
          //   "label": "Amount A, B, C, or H, whichever is the least"
          // },
          "1": {
            "type": "none"
          },
          // "2": {
          //   "label": "",
          //   "labelClass": "center"
          // },
          "3": {
            "label": "Taxable income earned in Saskatchewan",
            labelClass: 'center',
            cellClass: 'singleUnderline'
          },
          "5": {
            "num": "521",
            cellClass: 'singleUnderline',
            "disabled": true
          },
          "6": {
            "label": " x ",
            "labelClass": "center"
          },
          "7": {
            "num": "523"
          },
          "8": {
            "label": " %= "
          },
          "9": {
            "num": "524",
            decimals: '6'
          },
          "10": {
            "label": "%L"
          }
        },
        {
          "1": {
            "type": "none"
          },
          "3": {
            "label": "Taxable income earned in all provinces*",
            "labelClass": "center"
          },
          "5": {
            "num": "522",
            "disabled": true
          },
          "7": {
            "type": "none"
          },
          "9": {
            "type": "none"
          }
        }
      ]
    }
  };
})();