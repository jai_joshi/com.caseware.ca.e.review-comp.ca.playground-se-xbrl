(function() {

  function runTableRateCalcs(table, rIndex, isNoRate) {
    var result = 0;
    if (isNoRate) {
      result = table.cell(rIndex, 1).get() * table.cell(rIndex, 5).get() / table.cell(rIndex + 1, 5).get()
    } else {
      result = table.cell(rIndex, 1).get() * table.cell(rIndex, 5).get() / table.cell(rIndex + 1, 5).get() * table.cell(rIndex, 7).get() / 100
    }
    table.cell(rIndex, 9).assign(result);
  }

  wpw.tax.create.calcBlocks('t2s427', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.getGlobalValue('501', 'T2J', '400');
      calcUtils.getGlobalValue('502', 'T2J', '405');
      calcUtils.getGlobalValue('503', 'T2J', '427');
      calcUtils.min('504-0', ['501', '502', '503']);
      calcUtils.equals('504', '504-0');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //check if corporation is credit union
      var isCreditUnion = (field('CP.1011').get() == 1);
      if (isCreditUnion) {
        // TODO: S17 is not avaiable
      }
      else {
        field('505').assign(0)
      }
      calcUtils.equals('506', '504');
      field('507').assign(Math.max(0, field('505').get() - field('506').get()));
      calcUtils.equals('507-1', '507');
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var table800 = field('800');
      var table810 = field('810');

      table810.cell(0, 7).assign(field('ratesBc.407').get());
      table810.cell(0, 7).source(field('ratesBc.407'));

      var janDate2016 = wpw.tax.date(2016, 1, 1);
      var janDate2017 = wpw.tax.date(2017, 1, 1);

      var dateComparisons2016 = calcUtils.compareDateAndFiscalPeriod(janDate2016);
      var dateComparisons2017 = calcUtils.compareDateAndFiscalPeriod(janDate2017);

      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();

      //Part 1 Amount 3 Calcs
      var amount3 = field('507-1').get();

      table800.cell(0, 1).assign(amount3);
      table810.cell(0, 1).assign(amount3);

      table800.cell(1, 5).assign(daysFiscalPeriod);
      table810.cell(1, 5).assign(daysFiscalPeriod);

      table800.cell(0, 5).assign(dateComparisons2016.daysBeforeDate);
      table810.cell(0, 5).assign(dateComparisons2016.daysAfterDate);

      runTableRateCalcs(table800, 0, true);
      runTableRateCalcs(table810, 0);

      field('430').assign(
          table800.cell(0, 9).get() +
          table810.cell(0, 9).get()
      );

      calcUtils.equals('508', '430');
      calcUtils.sumBucketValues('509', ['504', '508']);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      var table700 = field('700');
      var table710 = field('710');
      var table910 = field('910');
      var table920 = field('920');

      table700.cell(0, 7).assign(field('ratesBc.1115').get());
      table710.cell(0, 7).assign(field('ratesBc.1118').get());
      table910.cell(0, 7).assign(field('ratesBc.1117').get());
      table920.cell(0, 7).assign(field('ratesBc.1120').get());

      var taxableIncomeAllocation = calcUtils.getTaxableIncomeAllocation('BC');
      var table100 = field('100');
      field('500').assign(taxableIncomeAllocation.provincialTI);
      field('500').source(taxableIncomeAllocation.provincialTISourceField);
      table100.cell(0, 2).assign(field('509').get());
      table100.cell(0, 6).assign(taxableIncomeAllocation.provincialTI);
      table100.cell(1, 6).assign(taxableIncomeAllocation.allProvincesTI);
      table100.cell(0, 8).assign(table100.cell(0, 2).get() * table100.cell(0, 6).get() / table100.cell(1, 6).get());
      field('510').assign(field('500').get() - table100.cell(0, 8).get());

      var amountH = table100.cell(0, 8).get();
      table700.cell(0, 1).assign(amountH);
      table710.cell(0, 1).assign(amountH);

      var aprilDate = wpw.tax.date(2017, 4, 1);
      var dateComparisons = calcUtils.compareDateAndFiscalPeriod(aprilDate);
      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();

      table700.cell(1, 5).assign(daysFiscalPeriod);
      table710.cell(1, 5).assign(daysFiscalPeriod);

      table700.cell(0, 5).assign(dateComparisons.daysBeforeDate);
      table710.cell(0, 5).assign(dateComparisons.daysAfterDate);

      runTableRateCalcs(table700, 0);
      runTableRateCalcs(table710, 0);

      field('900').assign(table700.cell(0, 9).get() + table710.cell(0, 9).get());
      field('901').assign(field('900').get());

      //higher rate calc
      var amountI = field('510').get();
      table910.cell(0, 1).assign(amountI);
      table920.cell(0, 1).assign(amountI);

      var jan2018Date = wpw.tax.date(2018, 1, 1);
      var dateCompare2018 = calcUtils.compareDateAndFiscalPeriod(jan2018Date);

      table910.cell(1, 5).assign(daysFiscalPeriod);
      table920.cell(1, 5).assign(daysFiscalPeriod);

      table910.cell(0, 5).assign(dateCompare2018.daysBeforeDate);
      table920.cell(0, 5).assign(dateCompare2018.daysAfterDate);

      runTableRateCalcs(table910, 0);
      runTableRateCalcs(table920, 0);

      field('930').assign(table910.cell(0, 9).get() + table920.cell(0, 9).get());
      field('931').assign(field('930').get());

      field('811').assign(field('931').get() + field('901').get())
    });

  });
})();
