(function() {

  function getTableTaxInc(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {type: 'none', colClass: 'std-padding-width'},
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {

          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '2': {num: labelsObj.num[0]},
          '3': {label: 'x'},
          '4': {
            label: labelsObj.label[1],
            labelClass: 'center',
            cellClass: 'singleUnderline'
          },
          '6': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '7': {label: '='},
          '8': {num: labelsObj.num[2]},
          '9': {label: labelsObj.indicator || ''}
        },
        {
          '2': {type: 'none'},
          '4': {
            label: labelsObj.label[2],
            labelClass: 'center fullLength'
          },
          '6': {num: labelsObj.num[3]},
          '8': {type: 'none'}
        }
      ]
    }
  }

  function getTableRate(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'small-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '1': {num: labelsObj.num[0]},
          '2': {label: ' x ', labelClass: 'center'},
          '3': {num: labelsObj.num[1], decimals: labelsObj.decimals},
          '4': {label: ' %= '},
          '6': {num: labelsObj.num[2]},
          '7': {label: labelsObj.indicator}
        }]
    }
  }

  function getTableProRate(labelsObj) {
    labelsObj.num = labelsObj.num || [];

    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '1': {num: labelsObj.num[0]},
          '2': {label: 'x', labelClass: 'center'},
          '3': {
            label: labelsObj.label[1],
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          '5': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '6': {label: 'x'},
          '7': {num: labelsObj.num[2], decimals: labelsObj.decimals},
          '8': {label: '%='},
          '9': {num: labelsObj.num[3]},
          '10': {label: labelsObj.indicator}
        },
        {
          '1': {type: 'none'},
          '3': {
            label: labelsObj.label[2],
            labelClass: 'center'
          },
          '5': {num: labelsObj.num[4]},
          '7': {type: 'none'},
          '9': {type: 'none'}
        }
      ]
    }
  }

  function getTableNoProRate(labelsObj) {
    labelsObj.num = labelsObj.num || [];

    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '1': {num: labelsObj.num[0]},
          '2': {label: 'x', labelClass: 'center'},
          '3': {
            label: labelsObj.label[1],
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          '5': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '7': {num: labelsObj.num[2], decimals: labelsObj.decimals},
          '9': {num: labelsObj.num[3]},
          '10': {label: labelsObj.indicator}
        },
        {
          '1': {type: 'none'},
          '3': {
            label: labelsObj.label[2],
            labelClass: 'center'
          },
          '5': {num: labelsObj.num[4]},
          '7': {type: 'none'},
          '9': {type: 'none'}
        }
      ]
    }
  }

  wpw.tax.global.tableCalculations.t2s427 = {
    '100': getTableTaxInc({
      label: ['Amount G', 'Taxable income for British Columbia *', 'Taxable income for all provinces **'],
      indicator: 'H'
    }),

    '800': getTableNoProRate({
      label: ['Amount 3', 'Number of days in the tax year before January 1, 2016', 'Number of days in the tax year'],
      indicator: '4',
      decimals: 1
    }),

    '810': getTableProRate({
      label: ['Amount 3', 'Number of days in the tax year in 2016', 'Number of days in the tax year'],
      indicator: '5',
      decimals: 1
    }),

    '820': getTableProRate({
      label: ['Amount 3', 'Number of days in the tax year in 2017', 'Number of days in the tax year'],
      indicator: '5',
      decimals: 1
    }),

    '700': getTableProRate({
      label: ['Amount H', 'Number of days in the tax year before April 1, 2017', 'Number of days in the tax year'],
      indicator: 'J',
      decimals: 1
    }),

    '710': getTableProRate({
      label: ['Amount H', 'Number of days in the tax year after March 31,2017', 'Number of days in the tax year'],
      indicator: 'K',
      decimals: 1
    }),

    '910': getTableProRate({
      label: ['Amount I', 'Number of days in the tax year before January 1, 2018', 'Number of days in the tax year'],
      indicator: 'M',
      decimals: 1
    }),

    '920': getTableProRate({
      label: ['Amount I', 'Number of days in the tax year after December 31, 2017', 'Number of days in the tax year'],
      indicator: 'N',
      decimals: 1
    })

  }
})();
