(function () {
  'use strict';

  wpw.tax.global.formData.t2s427 = {
    formInfo: {
      abbreviation: 'T2S427',
      title: 'British Columbia Corporation Tax Calculation',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      //subTitle: '(2013 and later tax years)',
      schedule: 'Schedule 427',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 427 E (18)',
      description: [
        {
          type: 'list',
          items: [
            'Use this schedule if your corporation had a permanent establishment (as defined under section 400 of the federal <i>Income Tax Regulations</i>) in British Columbia and had taxable income earned in the year in British Columbia.',
            'This schedule is a worksheet only and is not required to be filed with your ' +
            '<i>T2 Corporation Income Tax Return</i>.'
          ]
        }
      ],
      category: 'British Columbia Forms'

    },
    sections: [
      {
        'header': 'Part 1 - Income subject to British Columbia lower and higher tax rates',
        'rows': [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Taxable income for British Columbia * ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '500'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Income eligible for British Columbia lower tax rate: </b>'
          },
          {
            'label': 'Amount from line 400 of the T2 return ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '501'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 405 of the T2 return ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '502'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 427 of the T2 return ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '503'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'label': 'Amount B, C, or D, whichever is the least ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '504-0'
                }
              },
              {
                'input': {
                  'num': '504'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For credit unions only: ',
            'labelClass': 'tabbed bold'
          },
          {
            'label': 'Amount D from Schedule 17, <i>Credit Union Deductions</i>',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '505'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '1'
                }
              }, null
            ]
          },
          {
            'label': 'Amount E above',
            'labelClass': ' 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '506'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '2'
                }
              }, null
            ]
          },
          {
            'label': 'Subtotal (amount 1 <b>minus</b> amount 2, if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '507'
                }
              },
              {
                'input': {
                  'num': '507-1'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '3'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '800'
          },
          {
            'type': 'table',
            'num': '810'
          },
          {
            'labelclass': 'fulllength'
          },
          {
            'label': 'Subtotal (amount 4 <b>plus</b> amount 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '430'
                }
              },
              {
                'input': {
                  'num': '508'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'F'
          },
          // {
          //   'label': '<b>Note</b> <br> For days in the tax year after December 31, 2019, the additional deduction is eliminated.',
          //   'labelClass': 'fullLength'
          // },
          {
            'label': 'Total (amount E <b>plus</b> amount F)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '509'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'type': 'table',
            'fixedRows': true,
            'num': '100'
          },
          {
            'labelclass': 'fulllength'
          },
          {
            'label': ' <b> Income subject to British Columbia higher tax rate </b>(amount A <b>minus </b>amount H)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '510'
                }
              }
            ],
            'indicator': 'I'
          },
          { 'labelClass': 'fullLength' },
          // {
          //   'label': 'Enter amount H and/or amount I on the applicable line(s) in Part 2.'
          // },
          {
            'label': '* If the corporation has a permanent establishment only in British Columbia, enter the taxable income from line 360 of the T2 return. Otherwise, enter the taxable income allocated to British Columbia from column F in Part 1 of Schedule 5, <i>Tax Calculation Supplementary – Corporations</i>.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '** Includes the territories and the offshore jurisdictions for Nova Scotia and Newfoundland and Labrador.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        header: 'Part 2 – British Columbia tax before credits',
        rows: [
          {
            labelClass: 'fullLength'
          },
          {
            'label': '<b> British Columbia tax at the lower rate: </b>'
          },
          {
            'type': 'table',
            'num': '700'
          },
          {
            'type': 'table',
            'num': '710'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total British Columbia tax at the lower rate (amount J <b>plus</b> amount K)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '900'
                }
              },
              {
                'input': {
                  'num': '901'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': '<b> British Columbia tax at the higher rate: </b>'
          },
          {
            type: 'table',
            num: '910'
          },
          {
            type: 'table',
            num: '920'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total British Columbia tax at the higher rate (amount M <b>plus</b> amount N)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '930'
                }
              },
              {
                'input': {
                  'num': '931'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'label': '<b>British Columbia tax before credits</b>*** (amount L <b>plus</b> amount O)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '811'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '*** If the corporation has a permanent establishment in more than one jurisdiction or is claiming a British Columbia tax credit, enter amount P on line 240 of Schedule 5. Otherwise, enter it on line 760 of the T2 return.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  };
})();
