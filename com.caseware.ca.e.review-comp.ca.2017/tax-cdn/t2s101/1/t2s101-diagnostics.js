(function() {
  wpw.tax.create.diagnostics('t2s101', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var tableNums = ['100', '101', '102', '103', '104', '200', '201', '202', '300', '400'];
    var gifiCodes = ['1061', '1063', '1065', '1070', '1072', '1602', '1621', '1623', '1625', '1627', '1629', '1631',
      '1633', '1681', '1683', '1741', '1743', '1745', '1747', '1749', '1751', '1753', '1755', '1757', '1759', '1761',
      '1763', '1765', '1767', '1769', '1771', '1773', '1775', '1777', '1779', '1781', '1784', '1786', '1788', '1901',
      '1903', '1905', '1907', '1909', '1911', '1913', '1915', '1917', '1919', '1922', '2011', '2013', '2015', '2017',
      '2019', '2021', '2023', '2025', '2027', '2071', '2073', '2075', '2077'];

    function convertToSet(arr) {
      var set = {};
      for (var i = 0; i < arr.length; i++) {
        var item = arr[i];
        set[item] = set[item] + 1 || 1;
      }
      return set;
    }
    function checkDuplicates(tableNum) {
      diagUtils.diagnostic('100.duplicate', function(tools) {
        var table = tools.field(tableNum);
        var codeSet = convertToSet(wpw.tax.gifiService.getCurrentCodes(tools.form().valueFormId, tableNum));
        return tools.checkAll(table.getRows(), function(row) {
          var code = row[0] ? row[0].valueObj.label : null;
          if (!wpw.tax.utilities.isEmptyOrNull(code) && codeSet[code] > 1) {
            row[1].mark();
            return false;
          }
          return true;
        });
      });
    }

    function checkHasCode(tableNum) {
      diagUtils.diagnostic('gifi.noCode', function(tools) {
        var table = tools.field(tableNum);
        return tools.checkAll(table.getRows(), function(row) {
          if (tools.checkMethod(row.slice(2), 'isNonZero') && !row[0].valueObj.label) {
            row[1].mark();
            return false;
          }
          return true;
        });
      });
    }

    diagUtils.diagnostic('1010002', common.prereq(common.and(
            common.requireFiled('T2S101'),
            common.check(['2599', '3499', '3620'], 'isFilled')),
            function(tools) {
          return (tools.field('2599').getKey(0) || 0) ==
              (parseFloat(tools.field('3499').getKey(0) || 0) +
              parseFloat(tools.field('3620').getKey(0) || 0))
        }));//No highlight required

    diagUtils.diagnostic('gifi.requiredPair',  common.prereq(common.requireFiled('T2S101'),
        function(tools) {
      return tools.checkAll(gifiCodes, function(code) {
        if (tools.field(code).getKey(0) && tools.field(code).getKey(0) != 0) {
          if (code >= 1000 && code <= 1599) {
            var gifiCodeRows = wpw.tax.gifiService.getCurrentCodes('t2s101', '100', code);
            var relatedGifiCodeRows = wpw.tax.gifiService.getCurrentCodes('t2s101', '100', code - 1);
            if (gifiCodeRows) {
              return tools.field('100').cell(gifiCodeRows[0], 2).require(function() {
                return relatedGifiCodeRows ?
                    (this.isZero() || this.isNegative()) &&
                    this.get() <= tools.field('100').cell(relatedGifiCodeRows[0], 2).get() :
                    this.isZero();
              });
            } else return true;
          }
          else if (code >= 1600 && code <= 2009) {
            gifiCodeRows = wpw.tax.gifiService.getCurrentCodes('t2s101', '101', code);
            relatedGifiCodeRows = wpw.tax.gifiService.getCurrentCodes('t2s101', '101', code - 1);
            if (gifiCodeRows) {
              return tools.field('101').cell(gifiCodeRows[0], 2).require(function() {
                return relatedGifiCodeRows ?
                    (this.isZero() || this.isNegative()) &&
                    this.get() <= tools.field('101').cell(relatedGifiCodeRows[0], 2).get() :
                    this.isZero();
              });
            } else return true;
          }
          else if (code >= 2010 && code <= 2179) {
            gifiCodeRows = wpw.tax.gifiService.getCurrentCodes('t2s101', '102', code);
            relatedGifiCodeRows = wpw.tax.gifiService.getCurrentCodes('t2s101', '102', code - 1);
            if (gifiCodeRows) {
              return tools.field('102').cell(gifiCodeRows[0], 2).require(function() {
                return relatedGifiCodeRows ?
                    (this.isZero() || this.isNegative()) &&
                    this.get() <= tools.field('102').cell(relatedGifiCodeRows[0], 2).get() :
                    this.isZero();
              });
            } else return true;
          }
        }
        else return true;
      });
    }));

    for (var table in tableNums) {
      checkDuplicates(tableNums[table]);
      checkHasCode(tableNums[table]);
    }

  });
})();
