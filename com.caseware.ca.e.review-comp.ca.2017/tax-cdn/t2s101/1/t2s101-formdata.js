(function() {
  'use strict';

  var gifiCodes = new wpw.tax.codes.BalanceSheet();

  wpw.tax.global.formData.t2s101 = {
      formInfo: {
        abbreviation: 't2s101',
        title: 'Opening Balance Sheet',
        schedule: 'Schedule 101',
        category: 'GIFI',
        disclaimerShowWhen: {fieldId: 'cp.184', check: 'isYes'},
        disclaimerTextField: {formId: 'cp', fieldId: '183'}
      },
      sections: [
        {
        header: 'Assets',
          rows: [
            { labelClass: 'fullLength'},
            {
              type: 'gifiWorkchart', gifiCategory: 'BalanceSheet', gifiCodes: 'currentAsset', num: '100',
              header: 'Current Assets',
              fixedGifiRows: [
                {code: '1000', description: 'Cash and deposits'}
              ],
              range: [1000, 1599],
              numberOfYearsToShow: 1,
              totalCode: '1599',
              totalDescription: 'Total current assets'
            },
            { labelClass: 'fullLength'},
            { labelClass: 'fullLength'},
            {
              type: 'gifiWorkchart', gifiCategory: 'BalanceSheet', gifiCodes: 'tangibleAsset', num: '101',
              header: 'Tangible Capital Assets<br>(Note: Accumulated amortization must be entered as a negative value)',
              fixedGifiRows: [
                {code: '1600', description: 'Land'},
                {code: '1684', description: 'Buildings under construction'},
                {code: '1782', description: 'Machinery and equipment under construction'},
                {code: '1920', description: 'Other capital assets under construction'}
              ],
              range: [1600, 2009],
              numberOfYearsToShow: 1,
              hasAmortizations: true,
              postTotalRows: [
                {
                  code: '2008', description: 'Total tangible capital assets',
                  operation: 'addNonAmortizations'
                },
                {
                  code: '2009', description: 'Total accumulated amortization of tangible capital assets',
                  operation: 'addAmortizations'
                }
              ]
            },
            { labelClass: 'fullLength'},
            { labelClass: 'fullLength'},
            {
              type: 'gifiWorkchart', gifiCategory: 'BalanceSheet', gifiCodes: 'intangibleAsset', num: '102',
              header: 'Intangible Capital Assets<br>(Note: Accumulated amortization must be entered as a negative value)',
              range: [2010, 2179],
              numberOfYearsToShow: 1,
              hasAmortizations: true,
              postTotalRows: [
                {
                  code: '2178', description: 'Total intangible capital assets',
                  operation: 'addNonAmortizations'
                },
                {
                  code: '2179', description: 'Total accumulated amortization of intangible capital assets',
                  operation: 'addAmortizations'
                }
              ]
            },
            { labelClass: 'fullLength'},
            { labelClass: 'fullLength'},
            {
              type: 'gifiWorkchart', gifiCategory: 'BalanceSheet', gifiCodes: 'longtermAsset', num: '103',
              header: 'Long-term Assets',
              range: [2180, 2589],
              numberOfYearsToShow: 1,
              totalCode: '2589',
              totalDescription: 'Total long-term assets'
            },
            { labelClass: 'fullLength'},
            { labelClass: 'fullLength'},
            {
              type: 'gifiWorkchart', gifiCategory: 'BalanceSheet', gifiCodes: 'otherAsset', num: '104',
              header: 'Other Assets',
              range: [2590, 2599],
              numberOfYearsToShow: 1,
              totalCode: '2598',
              hideTotalCode: true,
              totalDescription: 'Total other assets',
              postTotalRows: [
                {
                  code: '2599', description: 'Total assets',
                  dependantCodes: [1599, 2008, 2009, 2178, 2179, 2589, 2598],
                  operation: 'addition'
                }
              ]
            }
          ]
        },
        {
        header: 'Liabilities',
          rows: [
            { labelClass: 'fullLength'},
            {
              type: 'gifiWorkchart', gifiCategory: 'BalanceSheet', gifiCodes: 'currentLiability', num: '200',
              header: 'Current Liabilities',
              range: [2600, 3139],
              numberOfYearsToShow: 1,
              totalCode: '3139',
              totalDescription: 'Total current liabilities'
            },
            { labelClass: 'fullLength'},
            { labelClass: 'fullLength'},
            {
              type: 'gifiWorkchart', gifiCategory: 'BalanceSheet', gifiCodes: 'longtermLiability', num: '201',
              header: 'Long-term Liabilities',
              range: [3140, 3450],
              numberOfYearsToShow: 1,
              totalCode: '3450',
              totalDescription: 'Total long-term liabilities'
            },
            { labelClass: 'fullLength'},
            { labelClass: 'fullLength'},
            {
              type: 'gifiWorkchart', gifiCategory: 'BalanceSheet', gifiCodes: 'otherLiability', num: '202',
              header: 'Other Liabilities',
              range: [3460, 3499],
              numberOfYearsToShow: 1,
              totalCode: '3498',
              hideTotalCode: true,
              totalDescription: 'Total other liabilities',
              postTotalRows: [
                {
                  code: '3499', description: 'Total Liabilities',
                  dependantCodes: [3139, 3450, 3498],
                  operation: 'addition'
                }
              ]
            }
          ]
        },
        {
        header: 'Equity',
          rows: [
            { labelClass: 'fullLength'},
            {
              type: 'gifiWorkchart', gifiCategory: 'BalanceSheet', gifiCodes: 'equity',
              num: '300',
              fixedGifiRows: [
                {code: '3500', description: 'Common Shares'}
              ],
              range: [3520, 3620],
              numberOfYearsToShow: 1,
              endFixedGifiRows: [
                {code: '3600', description: 'Retained Earnings/Deficit'}
              ],
              totalCode: '3620',
              totalDescription: 'Total Equity',
              postTotalRows: [
                {
                  code: '3640',
                  description: 'Total Liabilities and Equity',
                  dependantCodes: [3499, 3620],
                  operation: 'addition'
                }
              ]
            },
            { labelClass: 'fullLength'},
            { labelClass: 'fullLength'},
            {
              type: 'gifiWorkchart', gifiCategory: 'BalanceSheet', gifiCodes: 're',
              num: '400',
              fixedGifiRows: [
                {code: '3660', description: 'Retained Earnings / Deficit-Start'},
                {code: '3680', description: 'Net income / Loss*'}
              ],
              //TODO: 3960 & 3750 is only for partnerships
              range: [3660, 3849],
              totalCode: 3849,
              numberOfYearsToShow: 1,
              totalDescription: 'Total Retained Earnings'

            },
            { labelClass: 'fullLength'},
            {
              label: '* The amount on line 3680 must equal the amount on line 9999' +
              ' of S125 or S140 without considering line 9998',
              labelClass: 'fullLength'
            }
          ]
        }
      ]
    };
})();
