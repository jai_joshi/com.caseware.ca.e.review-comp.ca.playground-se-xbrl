(function () {
  wpw.tax.global.tableCalculations.t2s101 = {
    "100": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "currentAsset",
      hasTotals: true,
      "header": "Current Assets",
      "fixedGifiRows": [{
        "code": "1000",
        "description": "Cash and deposits"
      }],
      "range": [1000, 1599],
      "numberOfYearsToShow": 1,
      "totalCode": "1599",
      "totalDescription": "Total current assets"
    },
    "101": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "tangibleAsset",
      "header": "Tangible Capital Assets<br>(Note: Accumulated amortization must be entered as a negative value)",
      "fixedGifiRows": [
        {
          "code": "1600",
          "description": "Land"
        },
        {
          "code": "1684",
          "description": "Buildings under construction"
        },
        {
          "code": "1782",
          "description": "Machinery and equipment under construction"
        },
        {
          "code": "1920",
          "description": "Other capital assets under construction"
        }],
      "range": [1600, 2009],
      "numberOfYearsToShow": 1,
      "totalCode": "2008",
      "totalDescription": "Total Tangible Capital Assets",
      "amortizationCode": "2009",
      "amortizationDescription": "Total Accumulated Amortization of Tangible Capital Assets"
    },
    "102": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "intangibleAsset",
      "header": "Intangible Capital Assets<br>(Note: Accumulated amortization must be entered as a negative value)",
      "range": [2010, 2179],
      "numberOfYearsToShow": 1,
      "totalCode": "2178",
      "totalDescription": "Total Intangible Capital Assets",
      "amortizationCode": "2179",
      "amortizationDescription": "Total Accumulated Amortization of Intangible Capital Assets"
    },
    "103": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "longtermAsset",
      hasTotals: true,
      "header": "Long-term Assets",
      "range": [2180, 2589],
      "numberOfYearsToShow": 1,
      "totalCode": "2589",
      "totalDescription": "Total long-term assets"
    },
    "104": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "otherAsset",
      hasTotals: true,
      "header": "Other Assets",
      "range": [2590, 2599],
      "numberOfYearsToShow": 1,
      "totalCode": "2598",
      "hideTotalCode": true,
      "totalDescription": "Total other assets",
      "postTotalRows": [{
        "code": "2599",
        "description": "Total assets",
        "dependantCodes": [1599,
          2008,
          2009,
          2178,
          2179,
          2589,
          2598],
        "operation": "addition"
      }]
    },
    "200": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "currentLiability",
      hasTotals: true,
      "header": "Current Liabilities",
      "range": [2600, 3139],
      "numberOfYearsToShow": 1,
      "totalCode": "3139",
      "totalDescription": "Total current liabilities"
    },
    "201": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "longtermLiability",
      hasTotals: true,
      "header": "Long-term Liabilities",
      "range": [3140, 3450],
      "numberOfYearsToShow": 1,
      "totalCode": "3450",
      "totalDescription": "Total long-term liabilities"
    },
    "202": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "otherLiability",
      hasTotals: true,
      "header": "Other Liabilities",
      "range": [3460, 3499],
      "numberOfYearsToShow": 1,
      "totalCode": "3498",
      "hideTotalCode": true,
      "totalDescription": "Total other liabilities",
      "postTotalRows": [{
        "code": "3499",
        "description": "Total Liabilities",
        "dependantCodes": [3139,
          3450,
          3498],
        "operation": "addition"
      }]
    },
    "300": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "equity",
      "header": "Shareholder's Equity",
      hasTotals: true,
      "fixedGifiRows": [{
        "code": "3500",
        "description": "Common Shares"
      }],
      "range": [3500, 3620],
      "numberOfYearsToShow": 1,
      "endFixedGifiRows": [{
        "code": "3600",
        "description": "Retained Earnings/Deficit",
        "dependantCodes": [3849],
        "operation": "addition"
      }],
      "totalCode": "3620",
      "totalDescription": "Total Equity",
      "postTotalRows": [{
        "code": "3640",
        "description": "Total Liabilities and Equity",
        "dependantCodes": [3499,
          3620],
        "operation": "addition"
      }]
    },
    "400": {
      "type": "gifiWorkchart",
      "gifiCategory": "BalanceSheet",
      "gifiCodes": "re",
      "header": "Retained Earnings",
      hasTotals: true,
      "fixedGifiRows": [
        {
          "code": "3660",
          "description": "Retained Earnings / Deficit-Start"
        },
        {
          "code": "3680",
          "description": "Net income / Loss*"
        }],
      "range": [3660, 3849],
      "totalCode": 3849,
      "numberOfYearsToShow": 1,
      "totalDescription": "Total Retained Earnings"
    }
  }
})();
