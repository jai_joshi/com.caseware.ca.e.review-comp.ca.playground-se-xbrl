(function() {
  'use strict';

  wpw.tax.global.formData.t2s348 = {
    formInfo: {
      abbreviation: 'T2S348',
      title: 'Additional Certificate Numbers for the Nova Scotia Digital Animation Tax Credit',
      //TODO: DO NOT DELETE THIS LINE: subtitle
      //subTitle: '(2015 and later tax years)',
      schedule: 'Schedule 348',
      showCorpInfo: true,
      formFooterNum: 'T2 SCH 348 E (17)',
      code: 'Code 1701',
      headerImage: 'canada-federal',
      category: 'Nova Scotia Forms',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if you have more than one certificate number for the Nova Scotia digital ' +
              'animation tax credit. '
            },
            {
              label: 'To claim the Nova Scotia Digital Animation Tax Credit, file this schedule,' +
              ' Schedule 5 – <i>Tax Calculation Supplementary – Corporations</i>' +
              ' and each certificate of eligibility with your <i>T2 Corporation Income Tax Return</i>.'
            }
          ]
        }

      ]
    },
    'sections': [
      {
        hideFieldset: true,
        'rows': [
          {
            'type': 'table',
            'num': '900'
          },
          {
            label: 'Enter this amount on line 569 in Part 2 of Schedule 5.',
            labelClass: 'fullLength tabbed'
          }
        ]
      }
    ]
  };
})();
