(function() {
  wpw.tax.global.tableCalculations.t2s348 = {
    "900": {
      "showNumbering": true,
      "columns": [{
        "header": "Certificate Number",
        "num": "100",
        "tn": "100","validate": {"or":[{"length":{"min":"9","max":"9"}},{"check":"isEmpty"}]},
        type: 'text',
        formFooterNum: 'T2 SCH 348 E',
        code: 'Code 1501'
      },
        {
          "header": "Amount of Nova Scotia digital animation tax credit",
          "num": "200",
          "tn": "200","validate": {"or":[{"matches":"^[.\\d]{1,13}$"},{"check":"isEmpty"}]},
          "total": true,
          "totalNum": "300",
          "totalTn": "300",
          "totalMessage": "Total Nova Scotia Digital Animation Tax Credit"
        }],
      "hasTotals": true
    }
  }
})();
