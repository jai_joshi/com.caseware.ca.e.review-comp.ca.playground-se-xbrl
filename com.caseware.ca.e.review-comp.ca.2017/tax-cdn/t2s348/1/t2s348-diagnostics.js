(function() {
  wpw.tax.create.diagnostics('t2s348', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3480001', common.prereq(common.and(
        common.requireFiled('T2S348'),
        common.check(['t2s5.569'], 'isNonZero')),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne([row[0], tools.field('t2s5.839')], 'isNonZero');
          });
        }));

    diagUtils.diagnostic('3480005', common.prereq(common.requireFiled('T2S348'),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([tools.field('t2s5.839'), row[0]], 'isNonZero') > 1)
              return tools.requireOne([row[0], tools.field('t2s5.839')], 'isEmpty');
            else return true;
          });
        }));

    diagUtils.diagnostic('3480010', common.prereq(common.requireFiled('T2S348'),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[0].isNonZero())
              return row[1].require('isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('3480015', common.prereq(common.requireFiled('T2S348'),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[1].isNonZero())
              return row[0].require('isNonZero');
            else return true;
          });
        }));

  });
})();
