(function() {

  wpw.tax.create.formData('t2s411', {
    formInfo: {
      abbreviation: 'T2S411',
      title: 'Saskatchewan Corporation Tax Calculation',
      category: 'Saskatchewan Forms',
      formFooterNum: 'T2 SCH 411 E (17)',
      schedule: 'Schedule 411',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if your corporation had a permanent establishment (as defined in section 400 of the federal <i>Income Tax Regulations</i>) in Saskatchewan and had taxable income earned in the year in Saskatchewan.'
            },
            {
              label: 'This schedule is a worksheet only and does not have to be filed with your ' +
              '<i>T2 Corporation Income Tax Return</i>.'
            }
          ]
        }
      ]
    },
    sections: [
      {
        'forceBreakAfter': true,
        'header': 'Part 1 - Calculation of income subject to Saskatchewan lower and higher tax rates',
        'rows': [
          {
            label: 'Period before January 1, 2018',
            labelClass: 'bold center'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Taxable income for Saskatchewan *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100'
                }
              }
            ],
            'indicator': 'A1'
          },
          {
            'label': 'Income eligible for Saskatchewan lower tax rate:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Amount from line 400 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '101'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B1'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 405 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '102'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C1'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 427 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '103'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D1'
                }
              }
            ]
          },
          {
            'label': 'Amount B1, C1, or D1, whichever is the least ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '104'
                }
              }, {
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                },
                'input': {
                  'num': '1104'
                }
              }
            ],
            'indicator': 'E1'
          },
          {
            'label': 'For credit unions only: ',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Amount from line D of Schedule 17, <i>Credit Union Deductions</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '105'
                }
              },
              null,
              null
            ]
          },
          {
            'label': '<b>Deduct:</b> amount E above',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '106'
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (if negative, enter "0")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '107'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '108'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F1'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {type: 'table', num: '5000'},
          {type: 'table', num: '5001'},
          {type: 'table', num: '5002'},
          {type: 'table', num: '5003'},
          {
            'label': '<b>Subtotal</b> (total of amounts G1 to J1)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1107'
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '1108'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'K1'
          },
          {
            'label': '<b>Note</b><br>For days in the tax year after December 31, 2019, the additional deduction is eliminated.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total of amounts E and K',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '109'
                }
              }
            ],
            'indicator': 'L1'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '3100'
          },
          {
            'label': '<b>Income subject to Saskatchewan higher tax rate</b> (amount A <b>minus</b> amount M)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110'
                }
              }
            ],
            'indicator': 'N1'
          },
          // {
          //   'label': 'Enter amount H and/or amount I on the applicable line(s) in Part 2.',
          //   'labelClasS': 'fullLength'
          // },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'asterisk',
                'items': [
                  {
                    'label': 'If the corporation has a permanent establishment only in Saskatchewan, enter the taxable income from line 360 of the T2 return. Otherwise, enter the taxable income allocated to Saskatchewan from column F in Part 1 of Schedule 5, <i>Tax Calculation Supplementary – Corporations</i>.'
                  },
                  {
                    'label': 'Includes the territories and the offshore jurisdictions for Nova Scotia and Newfoundland and Labrador.'
                  }
                ]
              }
            ]
          },
          {
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'Part 1 - Calculation of income subject to Saskatchewan lower and higher tax rates(continued)',
        rows: [
          {
            label: 'Period after December 31, 2017',
            labelClass: 'bold center'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Taxable income for Saskatchewan *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  disabled: true
                }
              }
            ],
            'indicator': 'A2'
          },
          {
            'label': 'Income eligible for Saskatchewan lower tax rate:',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Amount from line 400 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '201',
                  disabled: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B2'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 405 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '202',
                  disabled: true
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C2'
                }
              }
            ]
          },
          {type: 'table', num: '8000'},
          {
            'label': 'Amount B2, C2, or D2, whichever is the least ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '204'
                }
              }, {
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                },
                'input': {
                  'num': '2041',
                  disabled: true
                }
              }
            ],
            'indicator': 'E2'
          },
          {
            'label': 'For credit unions only: ',
            'labelClass': 'bold fullLength'
          },
          {
            'label': 'Amount from line D of Schedule 17, <i>Credit Union Deductions</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'input': {
                  'num': '205',
                  disabled: true
                }
              },
              null,
              null
            ]
          },
          {
            'label': '<b>Deduct:</b> amount E above',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'indent',
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '206',
                  disabled: true
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Subtotal (if negative, enter "0")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '2071',
                  disabled: true
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '2081',
                  disabled: true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F2'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {type: 'table', num: '6000'},
          {type: 'table', num: '6001'},
          {type: 'table', num: '6002'},
          {type: 'table', num: '6003'},
          {
            'label': '<b>Subtotal</b> (total of amounts G2 to J2)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '207',
                  disabled: true
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '208',
                  disabled: true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'K2'
          },
          {
            'label': '<b>Note</b><br>For days in the tax year after December 31, 2019, the additional deduction is eliminated.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total of amounts E2 and K2',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '209',
                  disabled: true
                }
              }
            ],
            'indicator': 'L2'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '7100'
          },
          {
            'label': '<b>Income subject to Saskatchewan higher tax rate</b> (amount A2 <b>minus</b> amount M2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '210',
                  disabled: true
                }
              }
            ],
            'indicator': 'N2'
          },
          {
            'type': 'description',
            'lines': [
              {
                'type': 'list',
                'listType': 'asterisk',
                'items': [
                  {
                    'label': 'If the corporation has a permanent establishment only in Saskatchewan, enter the taxable income from line 360 of the T2 return. Otherwise, enter the taxable income allocated to Saskatchewan from column F in Part 1 of Schedule 5, <i>Tax Calculation Supplementary – Corporations</i>.'
                  },
                  {
                    'label': 'Includes the territories and the offshore jurisdictions for Nova Scotia and Newfoundland and Labrador.'
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of Saskatchewan tax before credits',
        'rows': [
          {
            'label': 'Saskatchewan tax at the lower rate:',
            'labelClass': 'bold fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1010'
          },
          {
            'type': 'table',
            'num': '1020'
          },
          {
            'label': 'Total Saskatchewan tax at the lower rate (amount O1 <b>plus</b> amount O2)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1021',
                  disabled: true
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '1022',
                  disabled: true
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Saskatchewan tax at the higher rate:',
            'labelClass': 'bold fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2100'
          },
          {
            'type': 'table',
            'num': '2110'
          },
          {
            'type': 'table',
            'num': '2120'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total Saskatchewan tax at the higher rate (amount P to amount R)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '996'
                }
              }
            ],
            'indicator': 'S'
          },
          {
            'label': '<b>Saskatchewan tax before credits</b> (amount O <b>plus</b> amount S) *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '306'
                }
              }
            ],
            'indicator': 'T'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* If the corporation has a permanent establishment in more than one jurisdiction or is claiming a Saskatchewan tax credit, enter amount T on line 235 of Schedule 5. Otherwise, enter it on line 760 of the T2 return.',
            'labelClass': 'tabbed fullLength'
          }
        ]
      }
    ]
  });
})();
