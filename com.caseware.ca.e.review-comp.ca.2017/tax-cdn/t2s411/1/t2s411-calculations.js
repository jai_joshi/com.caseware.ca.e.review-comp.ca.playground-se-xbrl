(function() {

  function runTableSimpleRateCalcs(table, rIndex) {
    table.cell(rIndex, 6).assign(
        table.cell(rIndex, 1).get() *
        table.cell(rIndex, 3).get() / 100);
  }

  function runTableRateCalcs(table, rIndex) {
    if (table.cell(rIndex + 1, 5).get() == 0) {
      table.cell(rIndex, 9).assign(0);
    } else {
      table.cell(rIndex, 9).assign(
          table.cell(rIndex, 1).get() *
          table.cell(rIndex, 5).get() /
          table.cell(rIndex + 1, 5).get() *
          table.cell(rIndex, 7).get() / 100);
    }
  }

  wpw.tax.create.calcBlocks('t2s411', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      //get rates
      field('5001').cell(0, 7).assign(field('ratesSk.810').get());
      field('5001').cell(0, 7).source(field('ratesSk.810'));
      field('5002').cell(0, 7).assign(field('ratesSk.811').get());
      field('5002').cell(0, 7).source(field('ratesSk.811'));
      field('5003').cell(0, 7).assign(field('ratesSk.812').get());
      field('5003').cell(0, 7).source(field('ratesSk.812'));

      field('1010').cell(0, 7).assign(field('ratesSk.800').get());
      field('1010').cell(0, 7).source(field('ratesSk.800'));
      field('1020').cell(0, 7).assign(field('ratesSk.800').get());
      field('1020').cell(0, 7).source(field('ratesSk.800'));

      field('2100').cell(0, 7).assign(field('ratesSk.801').get());
      field('2100').cell(0, 7).source(field('ratesSk.801'));
      field('2110').cell(0, 7).assign(field('ratesSk.802').get());
      field('2110').cell(0, 7).source(field('ratesSk.802'));
      field('2120').cell(0, 7).assign(field('ratesSk.803').get());
      field('2120').cell(0, 7).source(field('ratesSk.803'));

      field('6001').cell(0, 7).assign(field('ratesSk.810').get());
      field('6001').cell(0, 7).source(field('ratesSk.810'));
      field('6002').cell(0, 7).assign(field('ratesSk.811').get());
      field('6002').cell(0, 7).source(field('ratesSk.811'));
      field('6003').cell(0, 7).assign(field('ratesSk.812').get());
      field('6003').cell(0, 7).source(field('ratesSk.812'));

      //part 1 period before Jan 1, 2018
      var taxableIncomeAllocation = calcUtils.getTaxableIncomeAllocation('SK');
      field('100').assign(taxableIncomeAllocation.provincialTI);
      field('100').source(taxableIncomeAllocation.provincialTISourceField);
      field('3002').assign(taxableIncomeAllocation.provincialTI);
      field('3004').assign(taxableIncomeAllocation.allProvincesTI);

      calcUtils.getGlobalValue('101', 'T2J', '400');
      calcUtils.getGlobalValue('102', 'T2J', '405');
      calcUtils.getGlobalValue('103', 'T2J', '427');
      field('104').assign(Math.min(field('101').get(), field('102').get(), field('103').get()));
      calcUtils.equals('1104', '104');
      //todo: num105 from t2s17, which is not avaliable
      calcUtils.equals('106', '104');
      field('107').assign(Math.max(field('105').get() - field('106').get(), 0));
      calcUtils.equals('108', '107');

      var date = wpw.tax.date(2017, 7, 1);
      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();

      calcUtils.equals('5101', '108');
      calcUtils.equals('5111', '108');
      calcUtils.equals('5121', '108');
      calcUtils.equals('5131', '108');

      field('5104').assign(daysFiscalPeriod);
      field('5001').cell(1, 5).assign(daysFiscalPeriod);
      field('5002').cell(1, 5).assign(daysFiscalPeriod);
      field('5003').cell(1, 5).assign(daysFiscalPeriod);

      field('5102').assign(calcUtils.compareDateAndFiscalPeriod(wpw.tax.date(2017, 1, 1)).daysBeforeDate);
      field('5112').assign(calcUtils.compareDateAndFiscalPeriod(wpw.tax.date(2018, 1, 1)).daysBeforeDate - field('5102').get());
      field('5122').assign(calcUtils.compareDateAndFiscalPeriod(wpw.tax.date(2019, 1, 1)).daysBeforeDate - field('5102').get() - field('5112').get());
      field('5132').assign(calcUtils.compareDateAndFiscalPeriod(wpw.tax.date(2019, 1, 1)).daysAfterDate);

      field('5103').assign(field('5101').get() * field('5102').get() / field('5104').get());
      runTableRateCalcs(field('5001'), 0);
      runTableRateCalcs(field('5002'), 0);
      runTableRateCalcs(field('5003'), 0);

      calcUtils.sumBucketValues('1107', ['5103', '5114', '5124', '5134']);
      calcUtils.equals('1108', '1107');
      calcUtils.sumBucketValues('109', ['1108', '1104']);

      field('3001').assign(field('109').get());
      field('3003').assign(field('3004').get() == 0 ? 0 : field('3001').get() * field('3002').get() / field('3004').get());
      field('110').assign(field('100').get() - field('3003').get());

      //part 1 period after Dec 31, 2017
      var jan2018 = wpw.tax.date(2018, 1, 1);
      var taxStart = field('CP.tax_start').get();
      var taxEnd = field('CP.tax_end').get();
      var isAfter2018 = calcUtils.dateCompare.betweenInclusive(jan2018, taxStart, taxEnd) || calcUtils.dateCompare.greaterThanOrEqual(taxStart, jan2018);
      if (isAfter2018) {
        var taxableIncomeAllocation = calcUtils.getTaxableIncomeAllocation('SK');
        field('200').assign(taxableIncomeAllocation.provincialTI);
        field('200').source(taxableIncomeAllocation.provincialTISourceField);
        field('7002').assign(taxableIncomeAllocation.provincialTI);
        field('7004').assign(taxableIncomeAllocation.allProvincesTI);
        calcUtils.getGlobalValue('201', 'T2J', '400');
        calcUtils.getGlobalValue('202', 'T2J', '405');
        field('8000').cell(0, 2).assign(field('T2J.427').get());
        field('8000').cell(0, 2).source(field('T2J.427'));
        field('8000').cell(0, 5).assign(field('ratesSk.804').get());
        field('8000').cell(1, 5).assign(field('T2J.427').get());
        field('8000').cell(0, 7).assign(
            field('8000').cell(0, 2).get() * field('8000').cell(0, 5).get() / field('8000').cell(1, 5).get()
        );
        field('204').assign(Math.min(field('201').get(), field('202').get(), field('8000').cell(0, 7).get()));
        field('2041').assign(field('204').get());

        var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();

        calcUtils.equals('6101', '2081');
        calcUtils.equals('6111', '2081');
        calcUtils.equals('6121', '2081');
        calcUtils.equals('6131', '2081');

        field('6104').assign(daysFiscalPeriod);
        field('6001').cell(1, 5).assign(daysFiscalPeriod);
        field('6002').cell(1, 5).assign(daysFiscalPeriod);
        field('6003').cell(1, 5).assign(daysFiscalPeriod);

        field('6102').assign(calcUtils.compareDateAndFiscalPeriod(wpw.tax.date(2017, 1, 1)).daysBeforeDate);
        field('6112').assign(calcUtils.compareDateAndFiscalPeriod(wpw.tax.date(2018, 1, 1)).daysBeforeDate - field('6102').get());
        field('6122').assign(calcUtils.compareDateAndFiscalPeriod(wpw.tax.date(2019, 1, 1)).daysBeforeDate - field('6102').get() - field('56112').get());
        field('6132').assign(calcUtils.compareDateAndFiscalPeriod(wpw.tax.date(2019, 1, 1)).daysAfterDate);

        field('6103').assign(field('6101').get() * field('6102').get() / field('6104').get());
        runTableRateCalcs(field('6001'), 0);
        runTableRateCalcs(field('6002'), 0);
        runTableRateCalcs(field('6003'), 0);
        field('207').assign(field('6114').get() +  field('6103').get() + field('6124').get() + field('6134').get());
        field('208').assign(field('207').get());
        field('209').assign(field('208').get() + field('2041').get());
        field('7001').assign(field('209').get());
        field('7003').assign(field('7004').get() == 0 ? 0 : field('7001').get() * field('7002').get() / field('7004').get());
        field('210').assign(field('200').get() - field('7003').get());
      }
      else {
        field('200').assign(0)
      }
    });
    calcUtils.calc(function(calcUtils, field) {
      var date = wpw.tax.date(2017, 7, 1);
      var dateComparisons = calcUtils.compareDateAndFiscalPeriod(date);
      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();
      //part 2
      field('1010').cell(0, 1).assign(field('3003').get());
      field('1020').cell(0, 1).assign(field('7003').get());
      field('1010').cell(0, 5).assign(calcUtils.compareDateAndFiscalPeriod(wpw.tax.date(2018, 1, 1)).daysBeforeDate);
      field('1020').cell(0, 5).assign(calcUtils.compareDateAndFiscalPeriod(wpw.tax.date(2018, 1, 1)).daysAfterDate);
      field('1010').cell(1, 5).assign(daysFiscalPeriod);
      field('1020').cell(1, 5).assign(daysFiscalPeriod);
      runTableRateCalcs(field('1010'), 0);
      runTableRateCalcs(field('1020'), 0);
      field('1021').assign(field('1010').cell(0, 9).get() + field('1020').cell(0, 9).get());
      field('1022').assign(field('1021').get());

      field('2100').cell(0, 1).assign(field('110').get());
      field('2110').cell(0, 1).assign(field('110').get());
      field('2120').cell(0, 1).assign(field('210').get());

      field('2100').cell(0, 5).assign(dateComparisons.daysBeforeDate);
      field('2110').cell(0, 5).assign(Math.max(calcUtils.compareDateAndFiscalPeriod(wpw.tax.date(2018, 1, 1)).daysBeforeDate - dateComparisons.daysBeforeDate, 0));
      field('2120').cell(0, 5).assign(calcUtils.compareDateAndFiscalPeriod(wpw.tax.date(2018, 1, 1)).daysAfterDate);

      field('2100').cell(1, 5).assign(daysFiscalPeriod);
      field('2110').cell(1, 5).assign(daysFiscalPeriod);
      field('2120').cell(1, 5).assign(daysFiscalPeriod);

      runTableRateCalcs(field('2100'), 0);
      runTableRateCalcs(field('2110'), 0);
      runTableRateCalcs(field('2120'), 0);

      field('996').assign(field('2100').cell(0, 9).get() + field('2110').cell(0, 9).get() + field('2120').cell(0, 9).get());
      field('306').assign(field('1022').get() + field('996').get());
    })
  })
}());