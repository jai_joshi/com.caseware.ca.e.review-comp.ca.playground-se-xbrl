(function() {

  function getTableTaxInc2(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'half-col-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          disabled: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width',
          disabled: true
        },
        {
          colClass: 'third-col-width',
          type: 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width',
          disabled: true
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter',
          disabled: true
        },
        {type: 'none', colClass: 'std-padding-width'}
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '1': {num: labelsObj.num[0]},
          '2': {label: 'x'},
          '3': {
            label: labelsObj.label[1],
            labelClass: 'center',
            cellClass: 'singleUnderline'
          },
          '5': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '6': {label: '='},
          '7': {num: labelsObj.num[2]},
          '8': {label: labelsObj.indicator || ''}
        },
        {
          '1': {type: 'none'},
          '3': {
            label: labelsObj.label[2],
            labelClass: 'center fullLength'
          },
          '5': {num: labelsObj.num[3]},
          '7': {type: 'none'}
        }
      ]
    }
  }

  function getTableTaxInc(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'half-col-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {type: 'none', colClass: 'std-padding-width'},
        {
          colClass: 'std-input-width',
          disabled: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {

          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          disabled: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          disabled: true
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '2': {num: labelsObj.num[0]},
          '3': {label: 'x'},
          '4': {
            label: labelsObj.label[1],
            labelClass: 'center',
            cellClass: 'singleUnderline'
          },
          '6': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '7': {label: '='},
          '8': {num: labelsObj.num[2]},
          '9': {label: labelsObj.indicator || ''}
        },
        {
          '2': {type: 'none'},
          '4': {
            label: labelsObj.label[2],
            labelClass: 'center fullLength'
          },
          '6': {num: labelsObj.num[3]},
          '8': {type: 'none'}
        }
      ]
    }
  }

  function getTableRate(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter',
          disabled: true
        },
        {
          colClass: 'std-input-width',
          disabled: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width',
          disabled: true
        },
        {
          colClass: 'small-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          disabled: true
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '1': {num: labelsObj.num[0]},
          '2': {label: ' x ', labelClass: 'center'},
          '3': {num: labelsObj.num[1], decimals: labelsObj.decimals},
          '4': {label: ' %= '},
          '6': {num: labelsObj.num[2]},
          '7': {label: labelsObj.indicator}
        }]
    }
  }

  function getTableProRate(labelsObj) {
    labelsObj.num = labelsObj.num || [];

    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'half-col-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          disabled: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width',
          disabled: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width',
          disabled: true
        },
        {
          colClass: 'small-input-width',
          type: 'none',
          cellClass: 'alignCenter',
          disabled: true
        },
        {
          colClass: 'std-input-width',
          disabled: true
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter',
          disabled: true
        },
        {type: 'none', colClass: 'std-padding-width'}
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '1': {num: labelsObj.num[0]},
          '2': {label: 'x', labelClass: 'center'},
          '3': {
            label: labelsObj.label[1],
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          '5': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '6': {label: 'x'},
          '7': {num: labelsObj.num[2], decimals: labelsObj.decimals},
          '8': {label: '%='},
          '9': {num: labelsObj.num[3]},
          '10': {label: labelsObj.indicator}
        },
        {
          '1': {type: 'none'},
          '3': {
            label: labelsObj.label[2],
            labelClass: 'center'
          },
          '5': {num: labelsObj.num[4]},
          '7': {type: 'none'},
          '9': {type: 'none'}
        }
      ]
    }
  }

  function getTable8000(labelsObj) {
    // labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      'columns':[
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          formField: true,
          disabled: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          formField: true,
          disabled: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          formField: true,
          disabled: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {type: 'none', colClass: 'std-padding-width'}
      ],
      'cells': [
        {
          '1':{label: labelsObj.label[0]},
          '3':{label: labelsObj.label[1], labelClass: 'center'},
          '5':{cellClass: 'singleUnderline'},
          '7':{cellClass: 'doubleUnderline'},
          '8':{label: labelsObj.indicator}
        },
        {
          '2':{type: 'none'},
          '7':{type: 'none'}
        }
      ]
  }}

  wpw.tax.create.tables('t2s411', {
    '2100': getTableProRate({
      label: ['Amount N1', 'Number of days in the tax year before July 1, 2017', 'Number of days in the tax year'],
      indicator: 'P',
      decimals: 1
    }),
    '2110': getTableProRate({
      label: ['Amount N1', 'Number of days in the tax year after June 30, 2017, and before January 1, 2018', 'Number of days in the tax year'],
      indicator: 'Q',
      decimals: 1
    }),
    '2120': getTableProRate({
      label: ['Amount N2', 'Number of days in the tax year after December 31, 2017', 'Number of days in the tax year'],
      indicator: 'R',
      decimals: 1
    }),
    '3100': getTableTaxInc({
      label: ['Amount L', 'Taxable income for Saskatchewan *', 'Taxable income for all provinces **'],
      indicator: 'M1',
      num: ['3001', '3002', '3003', '3004']
    }),
    '5000': getTableTaxInc2({
      label: ['Amount F1', 'Number of days in the tax year before January 1, 2017', 'Number of days in the tax year'],
      indicator: 'G1',
      num: ['5101', '5102', '5103', '5104']
    }),
    '5001': getTableProRate({
      label: ['Amount F1', 'Number of days in the tax year in 2017', 'Number of days in the tax year'],
      indicator: 'H1',
      decimals: 1,
      num: ['5111', '5112', '5113', '5114']
    }),
    '5002': getTableProRate({
      label: ['Amount F1', 'Number of days in the tax year in 2018', 'Number of days in the tax year'],
      indicator: 'I1',
      decimals: 1,
      num: ['5121', '5122', '5123', '5124']
    }),
    '5003': getTableProRate({
      label: ['Amount F1', 'Number of days in the tax year in 2019', 'Number of days in the tax year'],
      indicator: 'J1',
      decimals: 1,
      num: ['5131', '5132', '5133', '5134']
    }),
    '6000': getTableTaxInc2({
      label: ['Amount F2', 'Number of days in the tax year before January 1, 2017', 'Number of days in the tax year'],
      indicator: 'G2',
      num: ['6101', '6102', '6103', '6104']
    }),
    '6001': getTableProRate({
      label: ['Amount F2', 'Number of days in the tax year in 2017', 'Number of days in the tax year'],
      indicator: 'H2',
      decimals: 1,
      num: ['6111', '6112', '6113', '6114']
    }),
    '6002': getTableProRate({
      label: ['Amount F2', 'Number of days in the tax year in 2018', 'Number of days in the tax year'],
      indicator: 'I2',
      decimals: 1,
      num: ['6121', '6122', '6123', '6124']
    }),
    '6003': getTableProRate({
      label: ['Amount F2', 'Number of days in the tax year in 2019', 'Number of days in the tax year'],
      indicator: 'J2',
      decimals: 1,
      num: ['6131', '6132', '6133', '6134']
    }),
    '7100': getTableTaxInc({
      label: ['Amount L2', 'Taxable income for Saskatchewan *', 'Taxable income for all provinces **'],
      indicator: 'M2',
      num: ['7001', '7002', '7003', '7004']
    }),
    '8000':getTable8000({
      label: ['Amount from line 427 of the T2 return', 'x'],
      indicator: 'D2'
    }),
    '1010': getTableProRate({
      label: ['Amount M1', 'Number of days in the tax year before January 1, 2018', 'Number of days in the tax year'],
      indicator: 'O1',
      decimals: 1
    }),
    '1020': getTableProRate({
      label: ['Amount M2', 'Number of days in the tax year after December 31, 2017', 'Number of days in the tax year'],
      indicator: 'O2',
      decimals: 1
    })
  });

})();