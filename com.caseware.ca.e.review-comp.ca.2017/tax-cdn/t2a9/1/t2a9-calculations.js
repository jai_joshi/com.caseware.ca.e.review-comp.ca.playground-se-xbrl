(function () {

  function isLeapYear(year) {

    if ((year % 4 == 0) && year % 100 != 0) {
      return true;
    }
    else if ((year % 4 == 0) && (year % 100 == 0) && (year % 400 == 0)) {
      return true;
    }
    else {
      return false;
    }
  }

  wpw.tax.create.calcBlocks('t2a9', function (calcUtils) {

    calcUtils.calc(function (calcUtils, field, form) {
      calcUtils.getGlobalValue('003', 'T661', '559');
      field('005').assign(Math.min(field('T2A4960.135').get(),field('003').get()));
      field('005').source(field('T2A4960.135'));
      field('007').assign(field('T2A4960.998').total(5).get());
      field('007').source(field('T2A4960.998').total(5));
      field('009').assign(field('T2A4960.998').total(6).get());
      field('009').source(field('T2A4960.998').total(6));
      field('011').assign(field('110').get());
      field('031').assign(
          field('005').get() -
          field('007').get() +
          field('009').get() +
          field('011').get() +
          field('025').get()
      )
    });

    calcUtils.calc(function (calcUtils, field, form) {
      //if tn100 is yes
      if (field('100').get() == 1) {
        field('102').assign(field('250').get());
        field('104').assign(0);
        field('104').disabled(true);
        field('108').assign(field('102').get());
      } else {
        field('102').assign(0);
        var taxStart = field('cp.tax_start').get();
        var taxEnd = field('cp.tax_end').get();
        var numberOfDays = 0;
        if (taxStart && taxEnd) {
          if(taxStart.month>2){
            numberOfDays = isLeapYear(taxEnd.year) ? 366:365;
          }else {
            numberOfDays = isLeapYear(taxStart.year) ? 366:365;
          }
        }
        field('990').cell(0, 4).assign(field('cp.Days_Fiscal_Period').get());
        field('990').cell(1, 4).assign(numberOfDays);
        field('104').assign(
            4000000 *
            field('990').cell(0, 4).get() /
            field('990').cell(1, 4).get()
        );
        field('108').assign(field('104').get());
      }
    });

    calcUtils.calc(function (calcUtils, field, form) {
      field('110').assign(Math.min(field('031').get(), field('108').get()) * 10 / 100);
      field('114').assign(field('110').get() - field('112').get());
      field('120').assign(field('114').get() - field('116').get());
      field('1005').assign(field('cp.955').get());
    });

    calcUtils.calc(function (calcUtils, field, form) {
      field('206').assign(wpw.tax.actions.calculateDaysDifference(field('202').get(), field('204').get()));
      field('992').cell(0, 2).assign(field('206').get());

      var numberOfDays = 0;
      if (field('202').get() && field('204').get()) {
        if (isLeapYear(field('202').get().year) || isLeapYear(field('204').get().year)) {
          var feb29Date = wpw.tax.date(field('204').get().year, 2, 29);
          numberOfDays = calcUtils.dateCompare.between(feb29Date, field('202').get(), field('204').get()) ? 366 : 365
        }
        else {
          numberOfDays = 365;
        }
      } else {
        numberOfDays = 365;
      }
      field('992').cell(0, 4).assign(numberOfDays);
      field('208').assign(
          4000000 *
          field('992').cell(0, 2).get() /
          field('992').cell(0, 4).get()
      )
    });

  });
})();
