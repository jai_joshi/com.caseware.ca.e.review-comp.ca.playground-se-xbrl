(function() {

  wpw.tax.global.tableCalculations.t2a9 = {
    '990': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-col-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          formField: true,
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      cells: [
        {
          '0': {label: '$4,000,000'},
          '1': {label: 'x', labelClass: 'center'},
          '2': {
            label: 'days in tax year *',
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          '4': {
            cellClass: 'singleUnderline'
          },
          '5': {
            label: '='
          },
          '7': {tn: '104'},
          8: {num: '104'}
        },
        {
          '2': {
            label: '365 **',
            labelClass: 'center'
          },
          '8': {type: 'none'}
        }
      ]
    },
    '992': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'small-input-width',
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          formField: true,
          colClass: 'std-input-width-2'
        },
        {
          'type': 'none'
        }
      ],
      cells: [
        {
          '0': {label: '$4,000,000'},
          '1': {label: 'x(', labelClass: 'center'},
          '2': {labelClass: 'center', cellClass: 'singleUnderline'},
          '3': {label: '/'},
          '5': {label: ')='},
          '6': {tn: '208'},
          '7': {num: '208'}
        },
        {
          '2': {label: 'Line 206',
            labelClass: 'center',
            type: 'none'
          },
          '4': {type: 'none'},
          '7': {type: 'none', label: 'SR&ED Maximum Expenditure Limit', labelClass: 'bold'}
        }
      ]
    },
    '993': {
      'showNumbering': true,
      'maxLoop': 100000,
      hasTotals: true,
      columns: [
        {header: 'Name of Corporation', tn: '220', type: 'text'},
        {
          header: 'Alberta Corporate<br>Account Number (CAN)<br><i>(Enter the 9 or 10 digit number</i>',
          tn: '230',
          'inputType': 'custom',
          'format': [
            '{N10}'
          ]
        },
        {
          header: 'Allocated Amount<br>Enter the amount allocated to the corporation on line 102 on page 2',
          tn: '240',
          total: true,
          totalTn: '250',
          totalNum: '250',
          totalMessage: 'Total Allocated Maximum Expenditure Limit: ***'
        }
      ]
    }

  }
})();
