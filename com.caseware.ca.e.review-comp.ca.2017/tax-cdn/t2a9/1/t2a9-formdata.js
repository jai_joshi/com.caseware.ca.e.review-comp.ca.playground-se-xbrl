(function () {
  'use strict';

  wpw.tax.global.formData.t2a9 = {
    formInfo: {
      abbreviation: 't2a9',
      title: 'Alberta Scientific Research & Experimental Development (SR&ED) Tax Credit',
      schedule: 'AT1 Schedule 9',
      headerImage: 'canada-Alberta',
      showCorpInfo: true,
      formFooterNum: 'AT190 (Oct-12)',
      category: 'Alberta Tax Forms',
      agencyUseOnlyBox: {
        tn: '001',
        headerClass: 'alignRight',
        textAbove: 'For Department Use'
      },
      description: [
        {
          text: 'For use by a corporation for a taxation year in which the corporation is claiming the Alberta ' +
          'Scientific Research & Experimental Development (SR&ED) tax credit. The tax credit will be paid for ' +
          'eligible expenditures carried out in Alberta after December 31, 2008 that are also eligible for the ' +
          'federal SR&ED investment tax credit. <b>Schedule 9 must be received by Alberta Tax and Revenue ' +
          'Administration within 21 months of the taxation year end in which the Alberta SR&ED eligible ' +
          'expenditures were incurred</b>. For more information on completing Schedule 9, see the Guide to ' +
          'Claiming The Alberta SR&ED Tax Credit (the Guide).'
        },
        {text: 'Report all monetary values in dollars; DO NOT include cents.'}
      ]
    },
    sections: [
      {
        header: 'Eligible Expenditures for Alberta Purposes',
        rows: [
          {
            "label": "Federal amount of total qualified SR&ED expenditures at line 559 of federal T661",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "003"
                },
                "padding": {
                  "type": "tn",
                  "data": "003"
                }
              }
            ]
          },
          {
            "label": "Portion of line 559 of federal T661 carried out in Alberta",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "005"
                },
                "padding": {
                  "type": "tn",
                  "data": "005"
                }
              }
            ]
          },
          {
            "label": "Deduct: Federal prescribed proxy amount (if any) included in the Alberta portion of line 559 (total of line 111 on Listing of SR&ED Projects Claimed in Alberta)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "007"
                },
                "padding": {
                  "type": "tn",
                  "data": "007"
                }
              }
            ]
          },
          {
            "label": "Add: Alberta proxy amount<br>(total of line 113 on Listing of SR&ED Projects Claimed in Alberta)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "009"
                },
                "padding": {
                  "type": "tn",
                  "data": "009"
                }
              }
            ]
          },
          {
            "label": "Add: Alberta SR&ED tax credit that reduced the federal expense in line 559 of federal T661 in the taxation year (enter amount from line 110 on page 2)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "011"
                },
                "padding": {
                  "type": "tn",
                  "data": "011"
                }
              }
            ]
          },
          {
            "label": "Add: Alberta portion of any repayment of assistance (other than an Alberta SR&ED tax credit) and contract payments made in the year that relates to amounts included in line 005 above made in the year or any prior year (portion of line 560 of federal T661 that relates to Alberta other than an Alberta SR&ED tax credit)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "025"
                },
                "padding": {
                  "type": "tn",
                  "data": "025"
                }
              }
            ]
          },
          {
            "label": "<b>Total: Eligible Expenditures for Alberta Purposes</b><br>(lines 005 - 007 + 009 + 011 + 025)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "031"
                },
                "padding": {
                  "type": "tn",
                  "data": "031"
                }
              }
            ]
          }
        ]
      },
      {
        rows: [
          {
            'type': 'infoField',
            "indicatorColumn": true,
            'inputType': 'dropdown',
            label: 'Select the primary field of science or technology the corporation is involved in:',
            'num': '040',
            'tn': '040',
            'options': [
              {
                'option': ' ',
                'value': '0'
              },
              {
                'option': '1. Natural and formal sciences ',
                'value': '1'
              },
              {
                'option': '2. Engineering and technology',
                'value': '2'
              },
              {
                'option': '3. Medical and health sciences',
                'value': '3'
              },
              {
                'option': '4. Agricultural sciences',
                'value': '4'
              }
            ]
          }
        ]
      },
      {
        header: 'Maximum Expenditure Limit',
        rows: [
          {
            'type': 'infoField',
            'label': 'Is the corporation associated with one or more corporations for SR&ED purposes?',
            'inputType': 'radio',
            'num': '100',
            'tn': '100'
          },
          {
            label: 'If "Yes", complete page 3',
            labelClass: 'bold'
          },
          {
            "label": "If the corporation is associated (line 100 = Yes), enter the allocated amount from applicable line 240 on page 3",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "102"
                },
                "padding": {
                  "type": "tn",
                  "data": "102"
                }
              }
            ]
          },
          {
            label: 'If the corporation is not associated (line 100 = No), calculate the following and enter the amount on line 104',
            labelClass: 'bold'
          },
          {type: 'table', num: '990'},
          {
            "label": "Maximum expenditure limit for the year (line 102 or line 104 as applicable)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "108"
                },
                "padding": {
                  "type": "tn",
                  "data": "108"
                }
              }
            ]
          },
          {
            label: '* Maximum of 365 or 366 days if the taxation year includes February 29 ',
            labelClass: 'indent'
          },
          {
            label: '** 366 days if the taxation year includes February 29',
            labelClass: 'indent'
          }
        ]
      },
      {
        header: 'Alberta SR&ED Tax Credit ',
        rows: [
          {
            "label": "Calculate: (lesser of line 031 and line 108) X 10%<br>(also enter on page 1, line 011)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "110"
                },
                "padding": {
                  "type": "tn",
                  "data": "110"
                }
              }
            ]
          },
          {
            "label": "If the corporation disposed of any Alberta SR&ED property or is deemed to have disposed of property during the taxation year, enter the amount of recapture",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "112"
                },
                "padding": {
                  "type": "tn",
                  "data": "112"
                }
              }
            ]
          },
          {
            "label": "<b>Alberta SR&ED Tax Credit</b> (line 110 - line 112)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "114"
                },
                "padding": {
                  "type": "tn",
                  "data": "114"
                }
              }
            ]
          },
          {
            label: 'For taxation years ending on or before March 31, 2012, complete AT1 Schedule 9 Supplemental to\n' +
            'determine the amount to be included at line 116. If the corporation, in the immediately preceding\n' +
            'taxation year, applied federal investment tax credits for SR&ED that originated in more than one\n' +
            'taxation year, then a separate AT1 Schedule 9 Supplemental must be prepared for each year in which\n' +
            'the federal investment tax credits originated. At line 116, enter the combined total of line 428 of each\n' +
            'AT1 Schedule 9 Supplemental (see the Guide). For taxation years ending after March 31, 2012, enter\n' +
            'zero at line 116.',
            labelClass: 'fullLength bold'
          },
          {
            "label": "Less: Alberta portion of prior year federal investment tax credit<br>(line 428 of Schedule 9 Supplemental)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "116"
                },
                "padding": {
                  "type": "tn",
                  "data": "116"
                }
              }
            ]
          },
          {
            "label": "<b>Net Alberta SR&ED Tax Credit (Repayment)</b> (line 114 - line 116)",
            "layout": "alignInput",
            "layoutOptions": {
              "indicatorColumn": true,
              "showDots": true
            },
            "columns": [
              {
                "input": {
                  "num": "120"
                },
                "padding": {
                  "type": "tn",
                  "data": "120"
                }
              }
            ]
          },
          {
            label: 'Enter the amount from line 120 on AT1 page 2, line 081 ',
            labelClass: 'bold'
          }
        ]
      },
      {
        header: 'Name of person or firm who prepared the SR&ED Schedule',
        rows: [
          {
            'type': 'multiColumn',
            'dividers': [
              false
            ],
            'columns': [
              [
                {
                  'type': 'infoField',
                  'label': 'Name (please print): ',
                  'num': '1001'
                }
              ],
              [
                {
                  'type': 'infoField',
                  'label': 'Title',
                  'num': '1002'
                }
              ]
            ]
          },
          {
            'type': 'infoField',
            'label': 'Firm Name, if applicable (please print):',
            'num': '1003'
          },
          {
            'type': 'multiColumn',
            'dividers': [
              false
            ],
            'columns': [
              [
                {
                  'type': 'infoField',
                  'label': 'Telephone Number',
                  'num': '1004'
                }
              ],
              [
                {
                  'type': 'infoField',
                  'label': 'Date',
                  inputType: 'date',
                  'num': '1005'
                }
              ]
            ]
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            label: 'ALBERTA SR&RD TAX CREDIT AGREEMENT AMONG ASSOCIATED CORPORATIONS',
            labelClass: 'bold titleFont'
          },
          {
            label: 'To be completed when two or more associated corporations have each incurred SR&ED eligible expenditures in their taxation year\n' +
            'ending in the same calendar year. One copy of this completed agreement is to be filed by each corporation of the group with its AT1\n' +
            'return for the taxation year. A new agreement must be filed in respect of each corporate taxation year. Use additional pages if space is\n' +
            'insufficient.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'Allocation of Maximum Expenditure Limit ',
        rows: [
          {
            'type': 'infoField',
            'inputType': 'custom',
            'format': [
              '{N10}'
            ],
            'label': 'The Alberta Corporate Account Number of the associated\n' +
            'corporation with the longest taxation year: ',
            'num': '200',
            'tn': '200'
          },
          {labelClass: 'fullLength'},
          {
            'type': 'multiColumn',
            'dividers': [
              false
            ],
            'columns': [
              [
                {
                  'type': 'infoField',
                  inputType: 'date',
                  'label': 'Taxation Year Beginning',
                  'num': '202',
                  tn: '202'
                }
              ],
              [
                {
                  'type': 'infoField',
                  inputType: 'date',
                  'label': 'Taxation Year Ending',
                  'num': '204',
                  tn: '204'
                }
              ],
              [
                {
                  'type': 'infoField',
                  'label': 'Number of days in the longest year',
                  'num': '206',
                  tn: '206'
                },
                {
                  label: '(max 365 days**)'
                }
              ]
            ]
          },
          {labelClass: 'fullLength'},
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '992'
          },
          {
            label: '** 366 days if the longest taxation year includes February 29 ',
            labelClass: 'indent'
          }
        ]
      },
      {
        header: 'Allocation of the Maximum Expenditure Limit',
        rows: [
          {
            type: 'table',
            num: '993'
          },
          {
            label: '*** Total must not exceed line 208',
            lebelClass: 'indent'
          }
        ]
      }
    ]
  };
})();
