(function() {
  wpw.tax.create.diagnostics('t2s552', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('5520010', common.prereq(common.check(['t2s5.454'], 'isNonZero'), common.requireFiled('T2S552')));

    diagUtils.diagnostic('5520020', common.prereq(common.requireFiled('T2S552'),
        function(tools) {
          var table = tools.field('1000');
          if ((table.getRow(0)[0].isFilled() || table.getRow(0)[1].isFilled()) && tools.field('cp.070').get() == 2)
            return tools.requireOne(tools.list(['300']), 'isNonZero');
          else return true;
        }));

    diagUtils.diagnostic('5520030', common.prereq(common.requireFiled('T2S552'),
        function(tools) {
          var table = tools.mergeTables(tools.field('700'), tools.field('800'));
          table = tools.mergeTables(table, tools.field('1000'));
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[7], row[8]], 'isPositive'))
              return tools.requireAll([row[0], row[1], row[2], row[3], row[4], row[5], row[6]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('5520040', common.prereq(common.requireFiled('T2S552'),
        function(tools) {
          var table = tools.mergeTables(tools.field('700'), tools.field('800'));
          table = tools.mergeTables(table, tools.field('1100'));
          return tools.checkAll(table.getRows(), function(row) {
            if (row[8].isPositive())
              return tools.requireAll([row[0], row[1], row[2], row[3], row[4], row[5], row[6]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('5520045', common.prereq(common.requireFiled('T2S552'),
        function(tools) {
          var table = tools.mergeTables(tools.field('700'), tools.field('800'));
          table = tools.mergeTables(table, tools.field('900'));
          table = tools.mergeTables(table, tools.field('1000'));
          table = tools.mergeTables(table, tools.field('1100'));
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9]], 'isNonZero'))
              return tools.requireOne([row[10], row[11], row[14]], 'isNonZero');
          });
        }));

    diagUtils.diagnostic('5520050', common.prereq(common.and(
        common.requireFiled('T2S552'),
        common.check('150', 'isYes')),
        common.check('160', 'isNonZero')));

    diagUtils.diagnostic('5520060', common.prereq(common.and(
        common.requireFiled('T2S552'),
        common.check('150', 'isYes')),
        common.check('170', 'isNonZero')));

    diagUtils.diagnostic('5520065', common.prereq(common.and(
        common.requireFiled('T2S552'),
        common.check(['150'], 'isNo')),
        common.check(['160', '170'], 'isZero', true)));

    diagUtils.diagnostic('5520070', common.prereq(common.and(
        common.requireFiled('T2S552'),
        common.check(['500'], 'isNonZero')),
        common.check(['200', '210'], 'isNonZero', true)));

    diagUtils.diagnostic('5520184', common.prereq(common.requireFiled('T2S552'),
        function(tools) {
          var table = tools.mergeTables(tools.field('800'), tools.field('900'));
          return tools.checkAll(table.getRows(), function(row) {
            if (row[4].isNonZero()) {
              var totalDays = wpw.tax.actions.calculateDaysDifference(row[2].get(), row[3].get());
              return row[4].require(function() {
                return this.get() <= totalDays;
              })
            } else return true;
          });
        }));

    diagUtils.diagnostic('5520185', common.prereq(common.requireFiled('T2S552'),
        function(tools) {
          var table = tools.mergeTables(tools.field('800'), tools.field('900'));
          return tools.checkAll(table.getRows(), function(row) {
            if (row[5].isNonZero()) {
              var totalDays = wpw.tax.actions.calculateDaysDifference(row[2].get(), row[3].get());
              return row[5].require(function() {
                return this.get() <= totalDays;
              })
            } else return true;
          });
        }));
  });
})();
