(function() {

  function isLeapYear(year) {

    if ((year % 4 == 0) && year % 100 != 0) {
      return true;
    }
    else if ((year % 4 == 0) && (year % 100 == 0) && (year % 400 == 0)) {
      return true;
    }
    else {
      return false;
    }
  }

  function disabledAllFields(calcUtils, numArray) {
    numArray.forEach(function(num) {
      calcUtils.field(num).disabled(true);
    });
  }

  wpw.tax.create.calcBlocks('t2s552', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field) {
      //update from rate table
      calcUtils.getGlobalValue('302', 'ratesOn', '901');
      calcUtils.getGlobalValue('303', 'ratesOn', '903');
      calcUtils.getGlobalValue('307', 'ratesOn', '906');
      calcUtils.getGlobalValue('321', 'ratesOn', '908');
      calcUtils.getGlobalValue('322', 'ratesOn', '910');
      calcUtils.getGlobalValue('326', 'ratesOn', '913');

      var dateCompare = calcUtils.dateCompare;
      var taxStart = field('CP.tax_start').get();
      var taxEnd = field('CP.tax_end').get();

      //Part1
      var contactPerson;
      var contactPhone;
      var phoneExtension;

      if (field('T2J.957').get() == 1) {
        contactPerson = field('T2J.951').get() + ' ' + field('T2J.950').get();
        contactPhone = field('T2J.956').get();
        phoneExtension = field('T2J.637').get();
      }
      else {
        contactPerson = field('T2J.958').get();
        contactPhone = field('T2J.959').get();
        phoneExtension = field('T2J.617').get();
      }

      field('110').assign(contactPerson);
      field('120').assign(contactPhone);
      field('1200').assign(phoneExtension);

      //Part2
      //tn200
      var provinces = field('CP.prov_residence').get() || {};
      if (provinces['ON']) {
        field('200').assign(1);
      }
      else {
        field('200').assign(2);
      }
      var tn200 = field('200').get();
      var tn210 = field('210').get();

      var isEligible = (tn200 == '1' && tn210 == '2');

      //Part 3
      if (isEligible) {
        var lowerLimit312 = field('ratesOn.904').get();
        var higherLimit312 = field('ratesOn.905').get();

        var val300 = field('300').get();

        if (val300 <= lowerLimit312) {
          field('304').assign(0);
          field('312').assign(field('ratesOn.901').get());
        }
        else if (val300 >= higherLimit312) {
          field('304').assign(0);
          field('312').assign(field('ratesOn.902').get());
        }
        else {
          calcUtils.equals('304', '300');
          field('312').assign(
              (field('302').get() -
              (field('303').get() * (field('304').get() - lowerLimit312) / field('307').get())));
        }

        var lowerLimit314 = field('ratesOn.911').get();
        var higherLimit314 = field('ratesOn.912').get();

        if (val300 <= lowerLimit314) {
          field('323').assign(0);
          field('314').assign(field('ratesOn.908').get());
        }
        else if (val300 >= higherLimit314) {
          field('323').assign(0);
          field('314').assign(field('ratesOn.909').get());
        }
        else {
          calcUtils.equals('323', '300');
          field('314').assign(
              (field('321').get() -
              (field('322').get() * (field(323).get() - lowerLimit314) / field(326).get())))
        }

        //table700 calcs
        var tradeCode = wpw.tax.codes.tradeCodes;
        var codeConcat = {};
        Object.keys(tradeCode).forEach(function(category) {
          Object.keys(tradeCode[category]).forEach(function(code) {
            codeConcat[code] = tradeCode[category][code];
          });
        });
        field('700').getRows().forEach(function(row, rowIndex) {
          var code = row[0].valueObj.value;
          if (angular.isDefined(codeConcat[code])) {
            row[1].assign(codeConcat[code]);
          }
          else {
            row[1].assign(null);
          }
        });

        //table900 calcs
        field('900').getRows().forEach(function(row, rowIndex) {
          if (taxStart && taxEnd) {
            if (isLeapYear(taxStart.year) || isLeapYear(taxEnd.year)) {
              var feb29Date = wpw.tax.date(taxEnd.year, 2, 29);
              field('numberOfDays').assign(dateCompare.between(feb29Date, taxStart, taxEnd) ? 366 : 365);
            }
            else {
              field('numberOfDays').assign(365);
            }
          }
          //check if start date of the employee is before or after April24,2015
          var employeeStartDate = field('800').cell(rowIndex, 1).get();
          var april24Date = wpw.tax.date(2015, 4, 24);
          var isBeforeApril242015 = dateCompare.greaterThanOrEqual(april24Date, employeeStartDate);

          if (isBeforeApril242015) {
            row[2].assign(10000 * row[0].get() / field('numberOfDays').get());
          } else {
            row[2].assign(5000 * row[1].get() / field('numberOfDays').get());
          }
        });

        field('1000').getRows().forEach(function(row, rowIndex) {
          //check if start date of the employee is before or after April24,2015
          var employeeStartDate = field('800').cell(rowIndex, 1).get();
          var april24Date = wpw.tax.date(2015, 4, 24);
          var isBeforeApril242015 = dateCompare.greaterThanOrEqual(april24Date, employeeStartDate);

          if (isBeforeApril242015) {
            row[2].assign(row[0].get() * field('312').get() / 100);
          } else {
            row[2].assign(row[1].get() * field('314').get() / 100);
          }
        });

        field('1100').getRows().forEach(function(row, rowIndex) {
          var repeatedTableRow1 = field('900').getRow(rowIndex);
          var repeatedTableRow2 = field('1000').getRow(rowIndex);

          if (!repeatedTableRow1 || !repeatedTableRow2)
            return;

          var min = Math.min(repeatedTableRow1[2].get(), repeatedTableRow2[2].get());
          row[0].assign(min);

          var colL = row[0].get();
          var colM = row[1].get();
          if (!colL && !colM)
            row[2].assign(0);
          else if (colM)
            row[2].assign(colM);
          else
            row[2].assign(colL);
        });

        field('500').assign(field('1100').total(2).get());

        if (field('150').get() == '1') {
          calcUtils.equals('610', '500');
          calcUtils.equals('620', '170');
        } else {
          field('610').assign(0);
          field('620').assign(0);
        }

        calcUtils.multiply('630', ['610', '620'], 1 / 100);
      } else {
        disabledAllFields(calcUtils, ['312', '314']);
        field('500').assign(0);
      }
    });
  });
})();