(function() {

  var percentageCols = [
    {
      colClass: 'std-input-col-width',
      "type": "none"
    },
    {
      colClass: 'small-input-width'
    },
    {
      colClass: 'small-input-width',
      "type": "none"
    },
    {
      colClass: 'small-input-width'
    },
    {
      colClass: 'std-padding-width',
      "type": "none"
    },
    {
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-input-width',
      "type": "none"
    },
    {
      "type": "none"
    }
  ];

  wpw.tax.global.tableCalculations.t2s552 = {
    "100": {
      "infoTable": "true",
      "fixedRows": "true",
      "dividers": [1],
      "columns": [
        {
          cellClass: 'alignLeft'
        },
        {
          cellClass: 'alignLeft'
        },
        {
          cellClass: 'alignLeft',
          width: '10%'
        }
      ],
      "cells": [{
        "0": {
          "num": "110", "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
          "tn": "110",
          "maxLength": 60,
          "minLength": 1,
          "label": "Name of person to contact for more information",
          "type": "text"
        },
        "1": {
          "num": "120",
          type: 'custom',
          format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
          "tn": "120",
          "fixedLength": 10,
          "label": "Telephone number including area code",
          'telephoneNumber': true,
          validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
        },
        '2': {
          num: '1200',
          "label": "Extension",
          "type": "text"
        }
      }]
    },
    "155": {
      "infoTable": "true",
      "fixedRows": "true",
      "columns": [{
        "width": "50%",
        "type": "none"
      },
        {}],
      "cells": [{
        "0": {
          "label": "If you answered <b>yes</b> to the question at line 150, what is the name of the partnership?",
          "labelWidth": "85%"
        },
        "1": {
          "num": "160", "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]},
          "rf": true,
          "tn": "160",
          type: 'text',
          "maxLength": 175,
          cellClass: 'alignLeft'
        }
      }]
    },
    "301": {
      "fixedRows": true,
      "infoTable": true,
      "columns": percentageCols,
      "cells": [
        {
          "0": {
            "label": "Specified percentage"
          },
          "1": {
            "num": 302,
            "filters": "decimals 2",
            "disabled": true
          },
          "2": {
            "label": "% + ["
          },
          "3": {
            "num": 303,
            "filters": "decimals 2",
            "disabled": true
          },
          "4": {
            "label": " x ( "
          },
          "5": {
            "num": 304,
            cellClass: 'singleUnderline',
            "disabled": true
          },
          "6": {
            "label": " - $400,000 )",
            cellClass: 'singleUnderline'
          },
          "7": {
            "label": "]",
            cellClass: 'alignLeft'
          }
        },
        {
          "1": {
            "num": 305,
            "type": "none",
            "inputClass": "noBorder"
          },
          "3": {
            "num": 306,
            "type": "none",
            "inputClass": "noBorder"
          },
          "5": {
            "num": 307,
            "disabled": true
          }
        }]
    },
    "320": {
      "fixedRows": true,
      "infoTable": true,
      "columns": percentageCols,
      "cells": [
        {
          "0": {
            "label": "Specified percentage = "
          },
          "1": {
            "num": 321,
            "filters": "decimals 2",
            "disabled": true
          },
          "2": {
            "label": "% + ["
          },
          "3": {
            "num": 322,
            "filters": "decimals 2",
            "disabled": true
          },
          "4": {
            "label": " x ("
          },
          "5": {
            "num": 323,
            cellClass: 'singleUnderline',
            "disabled": true
          },
          "6": {
            "label": " - $400,000 )",
            cellClass: 'singleUnderline'
          },
          "7": {
            "label": "]",
            cellClass: 'alignLeft'
          }
        },
        {
          "1": {
            "num": 324,
            "type": "none",
            "inputClass": "noBorder"
          },
          "3": {
            "num": 325,
            "type": "none",
            "inputClass": "noBorder"
          },
          "5": {
            "num": 326,
            "filters": "currency",
            "disabled": true
          }
        }]
    },
    "600": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [
        {
          colClass: 'std-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'small-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        }
      ],
      "cells": [{
        "0": {
          "label": "Amount O"
        },
        "1": {
          "num": "610"
        },
        "2": {
          "label": "x percentage on line 170 in Part 1"
        },
        "3": {
          "num": "620",
          decimals: 3,
          "filters": "decimals 2"
        },
        "4": {
          "label": "%=",
          "labelClass": "bold"
        },
        "6": {
          "num": "630",
          "filters": "currency"
        },
        "7": {
          "label": "P",
          "labelClass": "center"
        }
      }]
    },
    "700": {
      "infoTable": false,
      "showNumbering": true,
      "repeats": [800, 900, 1000, 1100],
      "maxLoop": 100000,
      "columns": [
        {
          "header": "Trade code",
          "type": "selector",
          selectorOptions: {
            title: 'Trade Codes',
            items: wpw.tax.codes.tradeCodes,
            notes: wpw.tax.codes.tradeCodeNotes
          },
          "tradeCode": true,
          "width": "15%",
          "num": "400",
          "tn": "400",
          "indicator": "A",
          "maxLength": 10
        },
        {
          "header": "Apprenticeship program/ <br>trade name",
          "width": "30%",
          "num": "405",
          "tn": "405",
          "maxLength": 60,
          "indicator": "B",
          type: 'text'
        },
        {
          "header": "Name of apprentice",
          "num": "410",
          "tn": "410",
          "maxLength": 60,
          "indicator": "C",
          type: 'text'
        }]
    },
    "800": {
      "infoTable": false,
      "showNumbering": true,
      "maxLoop": 100000,
      "columns": [{
        "header": "Original contract or training <br> agreement number",
        "num": "420",
        "tn": "420",
        "maxLength": 12,
        "indicator": "D",
        type: 'text'
      },
        {
          "header": "Original registration date of <br> apprenticeship contract or <br> training agreement <br> (see note 1 )",
          "width": "23%",
          "num": "425",
          "tn": "425",
          "type": "date",
          "indicator": "E",
          "maxLength": 8
        },
        {
          "header": "Start date of employment as <br> an apprentice in the tax year <br> (see note 2)",
          "width": "23%",
          "num": "430",
          "tn": "430",
          "indicator": "F",
          "type": "date",
          "maxLength": 8
        },
        {
          "header": " End date of employment as <br> an apprentice in the tax year <br> (see note 3)",
          "width": "23%",
          "num": "435",
          "tn": "435",
          "type": "date",
          "indicator": "G",
          "maxLength": 8
        }],
      "startTable": "700"
    },
    "900": {
      "infoTable": false,
      "showNumbering": true,
      //"repeats": [1000, 1100],
      "executeAtEnd": true,
      "columns": [{
        "header": "Number of days in the tax year employed as an apprentice in a qualifying apprenticeship program that began before April 24, 2015 (see note 1)",
        "num": "442",
        "tn": "442",
        "indicator": "H1",
        "maxLength": 3
      },
        {
          "header": "Number of days in the tax year employed as an apprentice in a qualifying apprenticeship program that began after April 23, 2015 (see note 1)",
          "num": "443",
          "tn": "443",
          "indicator": "H2",
          "maxLength": 3
        },
        {
          "header": "Maximum credit amount for the tax year (see note 2)",
          "indicator": "I",
          "num": "445",
          "tn": "445",
          filters: 'prepend $',
          "disabled": true,
          "maxLength": 13
        }
      ],
      "startTable": "700"
    },
    "1000": {
      "infoTable": false,
      "showNumbering": true,
      "columns": [
        {
          "header": "Eligible expenditures incurred after March 26, 2009 for a qualifying apprenticeship program that began before April 24, 2015 (see note 3)",
          "num": "452",
          "tn": "452",
          "indicator": "J1",
          "maxLength": 13,
          filters: 'prepend $',
        },
        {
          "header": "Eligible expenditures incurred for a qualifying apprenticeship program that began after April 23, 2015 (see note 3)",
          "num": "453",
          "tn": "453",
          "indicator": "J2",
          "maxLength": 13,
          filters: 'prepend $',
        },
        {
          "header": "Eligible expenditures multiplied by specified percentage (see note 4)",
          "indicator": "K",
          "num": "460",
          "tn": "460",
          filters: 'prepend $',
          "disabled": true,
          "maxLength": 13
        }
      ],
      "startTable": "700"
    },
    "1100": {
      hasTotals: true,
      "infoTable": false,
      "showNumbering": true,
      "executeAtEnd": true,
      "columns": [
        {
          "header": "ATTC on eligible expenditures <br> (lesser of columns I and K)",
          "num": "470",
          "tn": "470",
          "indicator": "L",
          filters: 'prepend $',
          "disabled": true,
          "maxLength": 13
        },
        {
          "header": "ATTC on repayment of <br> government assistance <br> (see note 5)",
          "indicator": "M",
          "num": "480",
          "tn": "480",
          filters: 'prepend $',
          "maxLength": 13
        },
        {
          "header": "ATTC for each apprentice <br> (column L or M, whichever applies)",
          "indicator": "N",
          "tn": "490",
          "num": "490",
          filters: 'prepend $',
          "total": true,
          "totalNum": "491",
          "maxLength": 13
        }
      ],
      "startTable": "700"
    }
  }
})();
