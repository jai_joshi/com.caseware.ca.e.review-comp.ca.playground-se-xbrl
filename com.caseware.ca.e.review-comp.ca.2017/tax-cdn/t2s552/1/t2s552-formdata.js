(function() {
  wpw.tax.global.formData.t2s552 = {
      'formInfo': {
        'abbreviation': 'T2S552',
        'isRepeatForm': true,
        'title': 'Ontario Apprenticeship Training Tax Credit',
        //TODO: DO NOT DELETE THIS LINE: subtitle
        //'subTitle': '(2009 and later tax years)',
        'schedule': 'Schedule 552',
        'showCorpInfo': true,
        code: 'Code 1501',
        formFooterNum: 'T2 SCH 552 E (15)',
        headerImage: 'canada-federal',
        repeatFormData:{
          titleNum: '160'
        },
        'description': [
          {text: ''},
          {
            'type': 'list',
            'items': [
              {
                label: ' Use this schedule to claim an Ontario apprenticeship training tax credit (ATTC) under' +
                ' section 89 of the <i>Taxation Act, 2007 (Ontario)</i>'
              },
              {
                label: 'The ATTC is a refundable tax credit that is equal to a specified percentage (25% to 45%) ' +
                'of the eligible expenditures incurred by a corporation for a qualifying apprenticeship. ' +
                'For eligible expenditures incurred after March 26, 2009 for an apprenticeship program that ' +
                'began before April 24, 2015, the maximum credit for each qualifying apprenticeship ' +
                'is $10,000 per year to a maximum credit of $40,000 over the first 48-month period of the ' +
                'qualifying apprenticeship. For an apprenticeship program that began after April 23, 2015, ' +
                'the maximum credit for each qualifying apprenticeship is $5,000 per year to a maximum credit of ' +
                '$15,000 over the first 36-month period of the qualifying apprenticeship.'
              },
              //{
              //  label: 'The ATTC is a refundable tax credit that is equal to a specified percentage (25% to 45%) of' +
              //  ' the eligible expenditures incurred by a corporation for a qualifying apprenticeship.' +
              //  ' Before March 27, 2009, the maximum credit for each apprentice is $5,000 per year to a maximum' +
              //  ' credit of $15,000 over the first 36-month period of the qualifying apprenticeship. After March 26,' +
              //  ' 2009, the maximum credit for each apprentice is $10,000 per year to a maximum credit of $40,000' +
              //  ' over the first 48-month period of the qualifying apprenticeship. The maximum credit amount is' +
              //  ' prorated for an employment period of an apprentice that straddles March 26, 2009.'
              //},
              {
                label: 'Eligible expenditures are salaries and wages (including taxable benefits) paid to an' +
                ' apprentice in a qualifying apprenticeship or fees paid to an employment agency for the provision' +
                ' of services performed by the apprentice in a qualifying apprenticeship. These expenditures must be:',
                sublist: [
                  'paid on account of employment or services, as applicable, at a permanent establishment of the' +
                  ' corporation in Ontario;',
                  'for services provided by the apprentice during the first 48 months of the apprenticeship program, ' +
                  'if an apprenticeship program began before April 24, 2015; and',
                  'for services provided by the apprentice during the first 36 months of the apprenticeship program, ' +
                  'if an apprenticeship program began after April 23, 2015.'
                  //'for services provided by the apprentice during the first 36 months of the apprenticeship program,' +
                  //' if incurred before March 27, 2009; and',
                  //'for services provided by the apprentice during the first 48 months of the apprenticeship program,' +
                  //' if incurred after March 26, 2009.'
                ]
              },
              {
                label: 'An expenditure is not eligible for an ATTC if:',
                sublist: [
                  'the same expenditure was used, or will be used, to claim a co-operative education tax credit; or',
                  'it is more than an amount that would be paid to an arm\'s length apprentice.'
                ]
              },
              {
                label: 'An apprenticeship must meet the following conditions to be a qualifying apprenticeship:',
                sublist: [
                  //'the apprenticeship is in a qualifying skilled trade approved by the Ministry of Training, Colleges' +
                  //' and Universities (Ontario); and',
                  'the apprenticeship is in a qualifying skilled trade approved by the Ministry of Training, ' +
                  'Colleges and Universities (Ontario) or a person designated by him or her; and',
                  'the corporation and the apprentice must be participating in an apprenticeship program in which' +
                  ' the training agreement has been registered under the <i>Ontario College of Trades and' +
                  ' Apprenticeship Act, 2009 or the Apprenticeship and Certification Act, 1998</i>, or in which the' +
                  ' contract of apprenticeship has been registered under <i>the Trades Qualification and' +
                  ' Apprenticeship Act</i>.'
                ]
              },
              {
                label: 'Do not submit the training agreement or contract of apprenticeship with your T2 Corporation ' +
                'Income Tax Return. Keep a copy of the training agreement or contract of apprenticeship to ' +
                'support your claim.'
              },
              //{
              //  label: 'Make sure you keep a copy of the training agreement or contract of apprenticeship to support' +
              //  ' your claim. Do not submit the training agreement or contract of apprenticeship with your' +
              //  '<i>T2 Corporation Income Tax Return</i>.'
              //},
              {label: 'File this schedule with your <i>T2 Corporation Income Tax Return</i>'}
            ]
          }
        ],
        category: 'Ontario Forms'
      },
      'sections': [
        {
          'header': 'Part 1 - Corporate information',
          'rows': [
            {
              'type': 'table',
              'num': '100'
            },
            {
              'labelClass': 'fullLength tabbed'
            },
            {
              'type': 'infoField',
              'label': 'Is the claim filed for an ATTC earned through a partnership? *',
              'labelClass': 'fullLength',
              'labelWidth': '85%',
              'inputType': 'radio',
              'num': '150',
              'rf': true,
              'tn': '150',
              'maxLength': 1
            },
            {
              'type': 'table',
              'num': '155'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Enter the percentage of the partnership\'s ATTC allocated to the corporation',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': 170,
                    'decimals': 3,
                    'validate': {
                      'or': [
                        {
                          'length': {
                            'min': '1',
                            'max': '6'
                          }
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'filters': 'decimals 2'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': 170
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* When a corporate member of a partnership is claiming an amount for eligible expenditures incurred by a partnership, complete a Schedule 552 for the partnership as if the partnership were a corporation. Each corporate partner, other than a limited partner, should file a separate Schedule 552 to claim the partner\'s share of the partnership\'s ATTC. The total of the partners\' allocated amounts can never exceed the amount of the partnership\'s ATTC.',
              'labelClass': 'fullLength tabbed2'
            }
          ]
        },
        {
          'header': 'Part 2 - Eligibility',
          'rows': [
            {
              'type': 'infoField',
              'label': ' 1. Did the corporation have a permanent establishment in Ontario in the tax year? ',
              'labelClass': 'fullLength',
              'labelWidth': '85%',
              'inputType': 'radio',
              'num': '200',
              'tn': '200',
              'init': '2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'infoField',
              'label': ' 2. Was the corporation exempt from tax under Part III of the Taxation Act, 2007 (Ontario)? ',
              'labelClass': 'fullLength',
              'labelWidth': '85%',
              'inputType': 'radio',
              'num': '210',
              'tn': '210',
              'init': '2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If you answered <b>no</b> to question 1 or <b>yes</b> to question 2, then you are <b>not eligible</b> for the ATTC.',
              'labelClass': 'fullLength tabbed'
            }
          ]
        },
        {
          'header': 'Part 3 - Specified percentage',
          'rows': [
            {
              'label': 'Corporation\'s salaries and wages paid in the previous tax year *',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '300',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '300'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'For eligible expenditures incurred after March 26, 2009 for an apprenticeship program that began before April 24, 2015:',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '- If line 300 is $400,000 or less, enter 45% on line 312',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': '- If line 300 is $600,000 or more, enter 35% on line 312. ',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': '- If line 300 is more than $400,000 and less than $600,000, enter the percentage on line 312 using the following formula:',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '301'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Specified percentage',
              'labelClass': 'bold',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '312',
                    'decimals': 3,
                    'validate': {
                      'or': [
                        {
                          'length': {
                            'min': '1',
                            'max': '6'
                          }
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'filters': 'decimals 2'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '312'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'For eligible expenditures incurred for an apprenticeship program that began after April 23, 2015:',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '- If line 300 is $400,000 or less, enter 30% on line 314',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': '- If line 300 is $600,000 or more, enter 25% on line 314',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': '- If line 300 is more than $400,000 and less than $600,000, enter the percentage on line 314 using the following formula:',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '320'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Specified percentage',
              'labelClass': 'bold',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': 314,
                    'decimals': 3,
                    'validate': {
                      'or': [
                        {
                          'length': {
                            'min': '1',
                            'max': '6'
                          }
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'filters': 'decimals 2'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': 314
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If this is the first tax year of an amalgamated corporation and subsection 89(6) of the <i>Taxation Act, 2007</i> (Ontario) applies, enter salaries and wages paid in the previous tax year by the predecessor corporations.',
              'labelClass': 'fullLength tabbed'
            }
          ]
        },
        {
          'header': 'Part 4 - Ontario apprenticeship training tax credit',
          'rows': [
            {
              label: 'Complete a <b>separate entry</b> for each apprentice for each qualifying apprenticeship with the ' +
              'corporation. When claiming an ATTC for repayment of government assistance, complete a ' +
              '<b>separate entry</b> for each repayment, and complete columns A to G and M and N with the ' +
              'details for the employment period in the previous tax year in which the government assistance' +
              ' was received. ',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength tabbed'
            },
            {
              'type': 'table',
              'num': '700'
            },
            {
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength tabbed'
            },
            {
              'type': 'table',
              'num': '800'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 1: Enter the original registration date of the apprenticeship contract or training agreement in all cases, even when multiple employers employed the apprentice.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 2: When there are multiple employment periods as an apprentice in the tax year with the corporation, enter the date that is the first day of employment as an apprentice in the tax year with the corporation. When claiming an ATTC for repayment of government assistance, enter the start date of employment as an apprentice for the tax year in which the government assistance was received.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 3: When there are multiple employment periods as an apprentice in the tax year with the corporation, enter the date that is the last day of employment as an apprentice in the tax year with the corporation. When claiming an ATTC for repayment of government assistance, enter the end date of employment as an apprentice for the tax year in which the government assistance was received.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '900'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '1000'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '1100'
            },
            {
              'label': 'Ontario apprenticeship training tax credit (total of amounts in column N)',
              'labelClass': 'bold text-right',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '500',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '500'
                  }
                }
              ],
              'indicator': 'O'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b>Or</b>, if the corporation answered <b>yes</b> at line 150 in Part 1, determine the partner\'s share of amount O:'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '600'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Enter amount O or P, whichever applies, on line 454 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>. If you are filing more than one Schedule 552, <b>add</b> the amounts from line O or P, whichever applies, on all the schedules, and enter the total amount on line 454 of Schedule 5.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 1: When there are multiple employment periods as an apprentice in the tax year with thecorporation, do not include days in which the individual was not employed as an apprentice.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': 'For H1: The days employed as an apprentice must be within 48 months of the registration date provided in column E on page 2.',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': 'For H2: The days employed as an apprentice must be within 36 months of the registration date provided in column E on page 2.',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 2: Maximum credit = ($10,000 × H1/ 365*) or ($5,000 × H2/ 365*), whichever applies.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': '* 366 days, if the tax year includes February 29',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 3: Reduce eligible expenditures by all government assistance, as defined under subsection 89(19) of the <i>Taxation Act, 2007</i> (Ontario), that the corporation has received, is entitled to receive, or may reasonably expect to receive, in respect of the eligible expenditures, on or before the filing due date of the <i>T2 Corporation Income Tax Return</i> for the tax year.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': 'For J1: Eligible expenditures must be for services provided by the apprentice to the taxpayer during the first 48 months of the apprenticeship program, and not relating to services performed before the apprenticeship program began or after it ended.',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': 'For J2: Eligible expenditures must be for services provided by the apprentice to the taxpayer during the first 36 months of the apprenticeshipprogram, and not relating to services performed before the apprenticeship began or after it ended.',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 4: Calculate the amount in column K as follows:',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': 'Column K = (J1 × line 312) or (J2 × line 314), whichever applies.',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 5: Include the amount of government assistance repaid in the tax year multiplied by the specified percentage for the tax year in which the government assistance was received, to the extent that the government assistance reduced the ATTC in that tax year. Complete a <b>separate entry</b> for each repayment of government assistance.',
              'labelClass': 'fullLength tabbed'
            }
          ]
        }
      ]
    };
})();
