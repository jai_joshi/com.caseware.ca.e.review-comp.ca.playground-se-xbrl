(function() {
  wpw.tax.global.tableCalculations.t2s19 = {
    "050": {
      "tn": "050",
      "showNumbering": true,
      "columns": [
        {
          "header": "Class of shares",
          "tn": "100",
          "width": "80%",
           "validate": {"or":[{"length":{"min":"1","max":"60"}},{"check":"isEmpty"}]}, 
          type: 'text'
        },
        {
          "header": "Percentage owned by non-residents",
          "tn": "200",
          "width": "20%",
          "validate": {
            "and": [
              'percent',
              {
                "or": [
                  {"length": {"min": "1", "max": "6"}},
                  {"check": "isEmpty"}
                ]
              }
            ]
          },
          "filters": "decimals 2",
          // validate: 'percent',
          decimals: 3
        }
      ]
    }
  }
})();
