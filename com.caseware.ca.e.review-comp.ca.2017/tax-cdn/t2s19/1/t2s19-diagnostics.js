(function() {
  wpw.tax.create.diagnostics('t2s19', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0190001', common.prereq(common.check(['t2j.151'], 'isChecked'), common.requireFiled('T2S19')));

    diagUtils.diagnostic('0190002', common.prereq(common.requireFiled('T2S19'),
        function(tools) {
          var table = tools.field('050');
          return tools.checkAll(table.getRows(), function(row) {
            var cols = [row[0], row[1]];
            if (tools.checkMethod(cols, 'isNonZero'))
              return tools.requireAll(cols, 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('0190010', common.prereq(common.and(
        common.requireFiled('T2S19'),
        function(tools) {
          var dateField = tools.field('t2j.061');
          return tools.dateCompare.greaterThan(dateField.get(), wpw.tax.date(2009, 12, 31)) &&
              !!wpw.tax.utilities.flagger()['T2S19'];
        }), function(tools) {
      return tools.field('300').require('isNonZero');
    }));
  });
})();

