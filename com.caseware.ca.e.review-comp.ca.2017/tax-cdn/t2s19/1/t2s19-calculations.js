(function() {
  wpw.tax.create.calcBlocks('t2s19', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //A CCPC cannot have more than 50% of the overall voting shares owned by non-residents.
      var isCCPC = (calcUtils.form('CP').field('Corp_Type').get() == '1');
      field('050').getRows().forEach(function(row) {
        if (isCCPC && row[1].get() > 50) {
          row[1].assign(50);
        }
        if (row[1].get() > 100) {
          row[1].assign(100);
        }
      });

      if (field('300').get() > 100) {
        field('300').assign(100);
        field('300').disabled(false);
      }
    });
  });
})();
