(function() {
  'use strict';

  wpw.tax.global.formData.t2s19 = {
    formInfo: {
      abbreviation: 'T2S19',
      title: 'Non-Resident Shareholder Information',
      formFooterNum: 'T2 SCH 19 E (09)',
      //subTitle: '(2009 and later tax years)',
      schedule: 'Schedule 19',
      code: 'Code 0901',
      showCorpInfo: true,
      description: [
        {text: ''},
        {
          text: 'If a non-resident shareholder owned a share of any class of the corporation\'s capital stock at' +
          ' any time during the tax year, indicate the class and the percentage of voting shares that ' +
          'non-resident shareholders owned. If the percentage varied throughout the year, indicate the highest' +
          ' percentage non-residents owned at any time during that year. Enter the overall percentage of voting' +
          ' shares owned by non-residents at line 300.'
        },
        {text: ''}
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        rows: [
          {
            type: 'table', num: '050'
          },
          {labelclass: 'fullLength'},
          {
            'label': 'Overall percentage of voting shares owned by non-residents (can not exceed 100)',
            'labelClass': 'alignTextRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '300',
                  'decimals': 3,
                  'validate': {
                    'or': [
                      {
                        'length': {
                          'min': '1',
                          'max': '6'
                        }
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'filters': 'decimals 2'
                },
                'padding': {
                  'type': 'tn',
                  'data': '300'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      }
    ]
  };
})();
