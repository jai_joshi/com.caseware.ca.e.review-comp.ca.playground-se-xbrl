(function() {
  'use strict';

  wpw.tax.global.formData.t2s550 = {
      'formInfo': {
        'abbreviation': 'T2S550',
        'title': 'Ontario Co-operative Education Tax Credit',
        //TODO: DO NOT DELETE THIS LINE: subtitle
        'schedule': 'Schedule 550',
        'showCorpInfo': true,
        code: 'Code 0902',
        formFooterNum: 'T2 SCH 550 E (09)',
        isRepeatForm: true,
        repeatFormData:{
          titleNum: '160'
        },
        headerImage: 'canada-federal',
        'description': [
          {text: ''},
          {
            'type': 'list',
            'items': [
              {
                label: 'Use this schedule to claim an Ontario co-operative education tax credit (CETC)' +
                ' under section 88 of the ' + '<i>' + 'Taxation Act, 2007' + '</i>' + ' (Ontario).'
              },
              {
                label: 'The CETC is a refundable tax credit that is equal to an eligible percentage (10% to 30%)' +
                ' of the eligible expenditures incurred by a corporation for a qualifying work placement. The maximum' +
                ' credit amount is $1,000 for each qualifying work placement ending before March 27, 2009, and ' +
                '$3,000 for each qualifying work placement beginning after March 26, 2009. For a qualifying work ' +
                'placement that straddles March 26, 2009, the maximum credit amount is prorated.'
              },
              {
                label: 'Eligible expenditures are salaries and wages (including taxable benefits) paid or payable to ' +
                'a student in a qualifying work placement, or fees paid or payable to an employment agency for ' +
                'services performed by the student in a qualifying work placement. These expenditures must be paid ' +
                'on account of employment or services, as applicable, at a permanent establishment of the ' +
                'corporation in Ontario. Expenditures for a work placement (WP) are not eligible expenditures ' +
                'if they are greater than the amounts that would be paid to an arm\'s length employee.'
              },
              {
                label: 'A WP must meet all of the following conditions to be a qualifying work placement: ',
                sublist: ['the student performs employment duties for a corporation under a qualifying ' +
                'co-operative education program (QCEP);',
                  'the WP has been developed or approved by an eligible educational institution as a ' +
                  'suitable learning situation;',
                  'the terms of the WP require the student to engage in productive work;',
                  'the WP is for a period of at least 10 consecutive weeks or, in the case of an ' +
                  'internship program, not less than 8 consecutive months and not more than 16 consecutive months;',
                  'the student is paid for the work performed in the WP;',
                  'the corporation is required to supervise and evaluate the job performance of the student in the WP;',
                  'the institution monitors the student\'s performance in the WP; and',
                  'the institution has certified the WP as a qualifying work placement.'
                ]
              },
              {
                label: 'Make sure you keep a copy of the letter of certification from the Ontario eligible ' +
                'educational institution containing the name of the student, the employer, the institution, the term ' +
                'of the WP, and the name/discipline of the QCEP to support the claim. Do not submit the letter of ' +
                'certification with the ' + '<i>' + 'T2 Corporation Income Tax Return' + '</i>' + '. '
              },
              {label: 'File this schedule with the ' + '<i>' + 'T2 Corporation Income Tax Return' + '</i>' + '. '}
            ]
          }
        ],
        category: 'Ontario Forms'
      },
      'sections': [
        {
          'header': 'Part 1 - Corporate information (please print)',
          'rows': [
            {
              'type': 'table',
              'num': '100'
            },
            {
              'labelClass': 'fullLength tabbed'
            },
            {
              'type': 'infoField',
              'label': ' Is the claim filed for a CETC earned through a partnership?*',
              'labelClass': 'fullLength',
              'labelWidth': '85%',
              'inputType': 'radio',
              'num': '150',
              'tn': '150',
              'maxLength': 1
            },
            {
              'type': 'table',
              'num': '155'
            },
            {
              'type': 'table',
              'num': '165'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': ' When a corporate member of a partnership is claiming an amount for eligible expenditures incurred by a partnership, complete a Schedule 550 for the partnership as if the partnership were a corporation. Each corporate partner, other than a limited partner, should file a separate Schedule 550 to claim the partner\'s share of the partnership\'s CETC. The allocated amounts can not exceed the amount of the partnership\'s CETC.',
              'labelClass': 'fullLength tabbed2'
            }
          ]
        },
        {
          'header': 'Part 2 - Eligibility',
          'rows': [
            {
              'type': 'infoField',
              'label': ' 1. Did the corporation have a permanent establishment in Ontario in the tax year? ',
              'labelClass': 'fullLength',
              'labelWidth': '85%',
              'inputType': 'radio',
              'num': '200',
              'tn': '200',
              'init': 'false'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'infoField',
              'label': ' 2. Was the corporation exempt from tax under Part III of the Taxation Act, 2007 (Ontario)? ',
              'labelClass': 'fullLength',
              'labelWidth': '85%',
              'inputType': 'radio',
              'num': '210',
              'tn': '210',
              'init': 'false'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If you answered<b> no</b> to question 1 or<b> yes</b> to question 2, then the corporation is<b> not eligible</b> for the CETC. ',
              'labelClass': 'fullLength tabbed'
            }
          ]
        },
        {
          'header': 'Part 3 - Eligible percentage for determining the eligible amount',
          'rows': [
            {
              'label': 'Corporation\'s salaries and wages paid in the previous tax year *',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '300',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '300'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'For eligible expenditures incurred before March 27, 2009:',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '- If line 300 is $400,000 or less, enter 15% on line 310',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': '- If line 300 is $600,000 or more, enter 10% on line 310. ',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': '- If line 300 is more than $400,000 and less than $600,000, enter the percentage on line 310 using the following formula:',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '301'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Eligible percentage for determining the eligible amount',
              'labelClass': 'bold',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '310',
                    'decimals': 3,
                    'validate': {
                      'or': [
                        {
                          'length': {
                            'min': '1',
                            'max': '6'
                          }
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'filters': 'decimals 3'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '310'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'For eligible expenditures incurred after March 26, 2009:',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '- If line 300 is $400,000 or less, enter 30% on line 312',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': '- If line 300 is $600,000 or more, enter 25% on line 312',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': '- If line 300 is more than $400,000 and less than $600,000, enter the percentage on line 312 using the following formula:',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '320'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Eligible percentage for determining the eligible amount',
              'labelClass': 'bold',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '312',
                    'decimals': 3,
                    'validate': {
                      'or': [
                        {
                          'length': {
                            'min': '1',
                            'max': '6'
                          }
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'filters': 'decimals 3'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '312'
                  }
                }
              ],
              'indicator': '%'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* If this is the first tax year of an amalgamated corporation and subsection 88(9) of the <i>Taxation Act, 2007 </i>(Ontario) applies, enter the salaries and wages paid in the previous tax year by the predecessor corporations. ',
              'labelClass': 'fullLength tabbed'
            }
          ]
        },
        {
          'header': 'Part 4 - Calculation of the Ontario co-operative education tax credit',
          'rows': [
            {
              'label': 'Complete a separate entry for each student for each qualifying work placement that ended in the corporation\'s tax year. If a qualifying work placement would otherwise exceed four consecutive months, divide the WP into periods of four consecutive months and enter each full period of four consecutive months as a separate WP. If the WP does not divide equally into four-month periods and if the period that is less than 4 months is 10 or more consecutive weeks, then enter that period as a separate WP. If that period is less than 10 consecutive weeks, then include it with the WP for the last period of 4 consecutive months. Consecutive WPs with two or more associated corporations are deemed to be with only one corporation, as designated by the corporations. ',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength tabbed'
            },
            {
              'type': 'table',
              'num': '700'
            },
            {
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength tabbed'
            },
            {
              'type': 'table',
              'num': '800'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 1: When the WP has been divided into separate periods because it exceeds four consecutive months, enter the start date for the separate WP.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 2: When the WP has been divided into separate periods because it exceeds four consecutive months, enter the end date for the separate WP.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '900'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '1000'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If you need more space, attach additional schedule(s).',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'or, if the corporation answered <b>yes</b> at line 150 in Part 1, determine the partner\'s share of amount L: '
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '600'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Enter amount L or M, whichever applies, on line 452 of Schedule 5, Tax Calculation Supplementary - Corporations. If you are filing more than one Schedule 550, add the amounts from line L or M, whichever applies, on all the schedules and enter the total amount on line 452 of Schedule 5.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 1: Reduce eligible expenditures by all government assistance, as defined under subsection 88(21) of the Taxation Act, 2007 (Ontario), that the corporation has received, is entitled to receive, or may reasonably expect to receive, for the eligible expenditures, on or before the filing due date of the T2 Corporation Income Tax Return for the tax year.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 2: Calculate the eligible amount (Column G) using the following formula:',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Column G = (column F1 x percentage on line 310) + (column F2 x percentage on line 312)',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 3: If the WP ends before March 27, 2009, the maximum credit amount for the WP is $1,000.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': 'If the WP begins after March 26, 2009, the maximum credit amount for the WP is $3,000.',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': 'If the WP begins before March 27, 2009, and ends after March 26, 2009, calculate the maximum credit amount using the following formula:',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '($1,000 x X/Y) + [$3,000 x (Y - X)/Y]',
              'labelClass': 'fullLength center'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'where "X" is the number of consecutive weeks of the WP completed by the student before March 27, 2009, and "Y" is the total number of consecutive weeks of the student\'s WP.',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note 4: When claiming a CETC for repayment of government assistance, complete a separate entry for each repayment and complete columns A to E and J and K with the details for the previous year WP in which the government assistance was received. Include the amount of government assistance repaid in the tax year multiplied by the eligible percentage for the tax year in which the government assistance was received, to the extent that the government assistance reduced the CETC in that tax year.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            }
          ]
        }
      ]
    };
})();
