(function() {

  function table900Calcs(calcUtils, rowIndex) {
    var field = calcUtils.field;
    var table800Row = field('800').getRow(rowIndex);
    var table900Row = field('900').getRow(rowIndex);
    var marchLimit = wpw.tax.date(2009, 3, 27);
    table900Row[1].assign((wpw.tax.actions.calculateDaysDifference(table800Row[1].get(), marchLimit)) / 7);
    table900Row[3].assign((wpw.tax.actions.calculateDaysDifference(table800Row[1].get(), table800Row[2].get())) / 7);
    table900Row[4].assign((table900Row[0].get() * 0.15) + (table900Row[2].get() * 0.3));
    table900Row[5].assign((1000 * (table900Row[1].get() / table900Row[3].get())) +
        (3000 * ((table900Row[3].get() - table900Row[1].get()) / table900Row[3].get())));
    table1000Calcs(calcUtils, rowIndex)
  }

  function table1000Calcs(calcUtils, rowIndex) {
    var field = calcUtils.field;
    var table900Row = field('900').getRow(rowIndex);
    var table1000Row = field('1000').getRow(rowIndex);

    table1000Row[0].assign(Math.min(table900Row[4].get(), table900Row[5].get()));
    if (table1000Row[1].get() > 0) {
      table1000Row[2].assign(table1000Row[1].get());
    }
    else {
      table1000Row[2].assign(table1000Row[0].get())
    }
  }

  wpw.tax.create.calcBlocks('t2s550', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      ////update part 3 from rate table
      calcUtils.getGlobalValue('302', 'ratesOn', '670');
      calcUtils.getGlobalValue('303', 'ratesOn', '672');
      calcUtils.getGlobalValue('307', 'ratesOn', '675');
      calcUtils.getGlobalValue('321', 'ratesOn', '677');
      calcUtils.getGlobalValue('322', 'ratesOn', '679');
      calcUtils.getGlobalValue('326', 'ratesOn', '682');

      //Part1
      field('110').assign(field('cp.951').get() + ' ' + field('cp.950').get());
      field('110').source(field('cp.950'));
      field('120').assign(field('cp.959').get());
      field('120').source(field('cp.959'));
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part2
      //tn200
      if (field('CP.750').get() == 'ON' || (field('CP.750').get() == 'MJ' && field('t2s5.013').get() == true)) {
        field('200').assign(1);
      }
      else {
        field('200').assign(2);
      }
      //tn210
      var tn085 = field('CP.085').get();
      var isTaxExempt = (tn085 == '1' || tn085 == '2' || tn085 == '4');
      if (isTaxExempt) {
        field('210').assign(1);
      }
      else {
        field('210').assign(2);
      }

      var tn200 = field('200').get();
      var tn210 = field('210').get();

      //var isEligible = (tn200 == '1' && tn210 == '2');
      //TODO: add custom diagnostic for when not eligible for CTEC instead of stopping the calc.
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part3
      var val300 = field('300').get();
      if (!val300)
        val300 = 0;

      //lower limit
      if (val300 <= field('ratesON.673').get()) {
        field('304').assign(0);
        field('310').assign(field('ratesON.670').get());
      }
      //higher limit
      else if (val300 >= field('ratesON.674').get()) {
        field('304').assign(0);
        field('310').assign(field('ratesON.671').get());
      }
      else if (field('307').get() == '0' || !angular.isDefined(field('307').get())) {
        field('310').assign(0);
      }
      else {
        calcUtils.equals('304', '300');
        field('310').assign((field('302').get() - (field('303').get() *
            (field('304').get() - field('ratesON.673').get()) / field('307').get())));
      }

      //lower limit
      if (val300 <= field('ratesON.680').get()) {
        field('323').assign(0);
        field('312').assign(field('ratesON.677').get());
      }
      //higher limit
      else if (val300 >= field('ratesON.681').get()) {
        field('323').assign(0);
        field('312').assign(field('ratesON.678').get());
      }
      else if (field('326').get() == '0' || !angular.isDefined(field('326').get())) {
        field('312').assign(0);
      }
      else {
        calcUtils.equals('323', '300');
        field('312').assign((field('321').get() - (field('322').get() *
            (field('323').get() - field('ratesON.680').get()) / field('326').get())))
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 4
      //table900 calcs
      field('900').getRows().forEach(function(row, rIndex) {
        table900Calcs(calcUtils, rIndex)
      });
      //table1000 calcs
      field('1000').getRows().forEach(function(row, rIndex) {
        table1000Calcs(calcUtils, rIndex)
      });

      if (field('200').get() == '2' || field('210').get() == '1') {
        field('500').assign(undefined);
      }

      if (field('150').get() == '1') {
        calcUtils.equals('620', '170');
        calcUtils.equals('610', '500');
        calcUtils.multiply('630', ['610', '620'], 1 / 100);
      }
      else {
        calcUtils.removeValue(['610', '620', '630'],true);
      }
    });

  })
})
();
