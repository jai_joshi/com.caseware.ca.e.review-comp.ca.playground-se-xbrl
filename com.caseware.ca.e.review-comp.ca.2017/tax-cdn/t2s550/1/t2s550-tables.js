(function() {

  var percentageCols = [
    {
      colClass: 'std-input-col-width',
      "type": "none"
    },
    {
      colClass: 'small-input-width'
    },
    {
      colClass: 'small-input-width',
      "type": "none"
    },
    {
      colClass: 'small-input-width'
    },
    {
      colClass: 'qtr-col-width',
      "type": "none"
    },
    {
      colClass: 'std-input-width'
    },
    {
      colClass: 'std-input-width',
      "type": "none"
    },
    {
      "type": "none"
    }
  ];

  wpw.tax.global.tableCalculations.t2s550 = {
    '100': {
      'infoTable': 'true',
      'fixedRows': 'true',
      'dividers': [1],
      'columns': [{
        'width': '60%',
        cellClass: 'alignLeft'
      },
        {
          cellClass: 'alignLeft'
        }],
      'cells': [{
        '0': {
          'num': '110', "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
          'tn': '110',
          'maxLength': 60,
          'minLength': 1,
          'label': 'Name of person to contact for more information',
          type: 'text'
        },
        '1': {
          'num': '120',
          'tn': '120',
          type: 'custom',
          format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
          'fixedLength': 10,
          'label': 'Telephone number including area code',
          'telephoneNumber': true,
          validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
        }
      }]
    },
    '155': {
      'infoTable': 'true',
      'fixedRows': 'true',
      'columns': [{
        'width': '40%',
        'type': 'none'
      },
        {}],
      'cells': [{
        '0': {
          'label': 'If you answered<b> yes</b> to the question at line 150, what is the name of the partnership?',
          'labelWidth': '85%'
        },
        '1': {
          'num': '160', "validate": {"or": [{"length": {"min": "1", "max": "175"}}, {"check": "isEmpty"}]},
          'tn': '160',
          'maxLength': 175,
          cellClass: 'alignLeft',
          type: 'text'
        }
      }]
    },
    '165': {
      'infoTable': 'true',
      'fixedRows': 'true',
      'columns': [{
        'width': '75%',
        'type': 'none'
      },
        {}],
      'cells': [{
        '0': {
          'label': 'Enter the percentage of the partnership\'s CETC allocated to the corporation',
          'labelClass': 'fullLength'
        },
        '1': {
          'num': '170', "validate": {"or": [{"length": {"min": "1", "max": "6"}}, {"check": "isEmpty"}]},
          'tn': '170',
          'maxLength': 6,
          'filters': 'decimals 2',
          cellClass: 'alignLeft',
          type: 'text',
          format: {
            suffix: '%'
          }
        }
      }]
    },
    '301': {
      'fixedRows': true,
      'infoTable': true,
      'columns': percentageCols,
      'cells': [
        {
          '0': {
            'label': 'Eligible percentage = '
          },
          '1': {
            'num': 302,
            'filters': 'decimals 2',
            'disabled': true
          },
          '2': {
            'label': '%  - ['
          },
          '3': {
            'num': 303,
            'filters': 'decimals 2',
            'disabled': true
          },
          '4': {
            'label': '% x ( '
          },
          '5': {
            'num': 304,
            cellClass: 'singleUnderline',
            'disabled': true
          },
          '6': {
            'label': ' - $400,000 )',
            cellClass: 'singleUnderline'
          },
          '7': {
            'label': ']',
            cellClass: 'alignLeft'
          }
        },
        {
          '1': {
            'num': 305,
            'type': 'none',
            'inputClass': 'noBorder'
          },
          '3': {
            'num': 306,
            'type': 'none',
            'inputClass': 'noBorder'
          },
          '5': {
            'num': 307,
            'disabled': true
          }
        }]
    },
    '320': {
      'fixedRows': true,
      'infoTable': true,
      'columns': percentageCols,
      'cells': [{
        '0': {
          'label': 'Eligible percentage = '
        },
        '1': {
          'num': 321,
          'filters': 'decimals 2',
          'disabled': true
        },
        '2': {
          'label': '%  - ['
        },
        '3': {
          'num': 322,
          'filters': 'decimals 2',
          'disabled': true
        },
        '4': {
          'label': '%  x ('
        },
        '5': {
          'num': 323,
          cellClass: 'singleUnderline',
          'disabled': true
        },
        '6': {
          'label': ' - $400,000 )',
          cellClass: 'singleUnderline'
        },
        '7': {
          'label': ']',
          cellClass: 'alignLeft'
        }
      },
        {
          '1': {
            'num': 324,
            'type': 'none',
            'inputClass': 'noBorder'
          },
          '3': {
            'num': 325,
            'type': 'none',
            'inputClass': 'noBorder'
          },
          '5': {
            'num': 326,
            'disabled': true
          }
        }]
    },
    '600': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          colClass: 'std-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'small-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-input-width',
          "type": "none",
          cellClass: 'alignCenter'
        }
      ],
      'cells': [{
        '0': {
          'label': 'Amount L'
        },
        '1': {
          'num': '610'
        },
        '2': {
          'label': 'x percentage on line 170 in Part 1'
        },
        '3': {
          'num': '620',
          'filters': 'decimals 2'
        },
        '4': {
          'label': '=',
          'labelClass': 'bold'
        },
        '5': {
          'num': '660',
          'disabled': 'true'
        },
        '6': {
          'num': '630'
        },
        '7': {
          'label': 'M',
          'labelClass': 'bold doubleUnderline center'
        }
      }]
    },
    '700': {
      'infoTable': false,
      'showNumbering': true,
      'repeats': [800, 900, 1000],
      'maxLoop': 100000,
      'columns': [{
        'header': 'Name of university, college, <br>or other eligible educational institution',
        'width': '55%',
        'num': '400',
        'tn': '400',
        'maxLength': 175,
        'indicator': 'A',
        type: 'text'
      },
        {
          'header': 'Name of qualifying <br>co-operative education program',
          'num': '405',
          'tn': '405',
          'maxLength': 175,
          'indicator': 'B',
          type: 'text'
        }]
    },
    '800': {
      'infoTable': false,
      'showNumbering': true,
      'maxLoop': 100000,
      'columns': [{
        'header': 'C <br><br> Name of student<br><br><br>',
        'width': '55%',
        'num': '410',
        'tn': '410',
        'maxLength': 60,
        type: 'text'
      },
        {
          'header': 'D <br><br> Start date of WP <br>(see note 1 below)<br><br>',
          'num': '430',
          'tn': '430',
          'type': 'date'
        },
        {
          'header': 'E <br><br> End date of WP <br>(see note 2 below)<br><br>',
          'num': '435',
          'tn': '435',
          'type': 'date'
        }
      ],
      'startTable': '700'
    },
    '900': {
      'infoTable': false,
      'showNumbering': true,
      'maxLoop': 100000,
      'columns': [{
        'header': 'F1 <br><br> Eligible expenditures before <br> March 27, 2009 <br> (see note 1 below) <br><br><br>',
        'num': '450',
        'tn': '450',
        'maxLength': 13
      },
        {
          header: 'X <br><br> Number of consecutive weeks of the WP completed by the student before March 27, 2009',
          disabled: true
        },
        {
          'header': 'F2 <br><br> Eligible expenditures after <br> March 26, 2009 <br> (see note 1 below)<br><br><br>',
          'num': '452',
          'tn': '452',
          'maxLength': 13
        },
        {
          header: 'Y <br><br> Total number of consecutive weeks of the student\'s WP'
        },
        {
          'header': 'G <br><br> Eligible amount <br> (eligible expenditures <b>' +
          ' multiplied </b><br> by eligible percentage)<br>  (see note 2 below) <br><br>',
          'num': '460',
          'tn': '460',
          'maxLength': 13,
          disabled: true
        },
        {
          'header': 'H <br><br> Maximum CETC <br> per WP <br> (see note 3 below)<br><br>',
          'num': '462',
          'tn': '462',
          'maxLength': 13,
          disabled: true
        }],
      'startTable': '700'
    },
    '1000': {
      'infoTable': false,
      'showNumbering': true,
      hasTotals: true,
      'maxLoop': 100000,
      'columns': [{
        'header': 'I <br><br> CETC on eligible expenditures <br> (column G or H, whichever is less) <br><br><br>',
        'num': '470',
        'tn': '470',
        'maxLength': 13,
        disabled: true
      },
        {
          'header': 'J <br><br> CETC on repayment of government assistance (see note 4 below)',
          'num': '480',
          'tn': '480',
          'maxLength': 13
        },
        {
          'header': 'K <br><br> CETC for each WP <br>(column I or column J)<br><br><br>',
          'tn': '490',
          'maxLength': 13,
          totalNum: '500',
          'total': true,
          'totalIndicator': 'L',
          'totalMessage': 'Ontario co-operative education tax credit<br> (total of amounts in column K)',
          'totalTn': '500',
          disabled: true
        }
      ],
      'startTable': '700'
    }
  }
})();
