(function() {
  wpw.tax.create.diagnostics('t2s550', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('5500010', common.prereq(common.check(['t2s5.452'], 'isNonZero'), common.requireFiled('T2S550')));

    diagUtils.diagnostic('5500020', common.prereq(common.requireFiled('T2S550'),
        function(tools) {
          var table = tools.field('900');
          if ((table.getRow(0)[0].isFilled() || table.getRow(0)[1].isFilled()) && tools.field('cp.070').get() == 2)
            return tools.requireOne(tools.list(['300']), 'isNonZero');
          else return true;
        }));

    diagUtils.diagnostic('5500030', common.prereq(common.requireFiled('T2S550'),
        function(tools) {
          var table = tools.mergeTables(tools.field('700'), tools.field('800'));
          table = tools.mergeTables(table, tools.field('900'));
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[5], row[7]], 'isNonZero'))
              return tools.requireAll([row[0], row[1], row[2], row[3], row[4]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('5500040', common.prereq(common.requireFiled('T2S550'),
        function(tools) {
          var table = tools.mergeTables(tools.field('700'), tools.field('800'));
          table = tools.mergeTables(table, tools.field('1000'));
          return tools.checkAll(table.getRows(), function(row) {
            if (row[6].isNonZero())
              return tools.requireAll([row[0], row[1], row[2], row[3], row[4]], 'isNonZero');
          });
        }));

    diagUtils.diagnostic('5500045', common.prereq(common.requireFiled('T2S550'),
        function(tools) {
          var table = tools.mergeTables(tools.field('700'), tools.field('800'));
          table = tools.mergeTables(table, tools.field('900'));
          table = tools.mergeTables(table, tools.field('1000'));
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[0], row[1], row[2], row[3], row[4]], 'isNonZero'))
              return tools.requireOne([row[5], row[7], row[12]], 'isNonZero');
            else return true;
          });
        }));

    diagUtils.diagnostic('5500050', common.prereq(common.and(
        common.requireFiled('T2S550'),
        common.check(['150'], 'isYes')),
        common.check('160', 'isNonZero')));

    diagUtils.diagnostic('5500060', common.prereq(common.and(
        common.requireFiled('T2S550'),
        common.check(['150'], 'isYes')),
        common.check('170', 'isNonZero')));

    diagUtils.diagnostic('5500065', common.prereq(common.and(
        common.requireFiled('T2S550'),
        common.check(['150'], 'isNo')),
        common.check(['160', '170'], 'isEmpty', true)));

    diagUtils.diagnostic('5500070', common.prereq(common.and(
        common.requireFiled('T2S550'),
        common.check(['500'], 'isNonZero')),
        common.check(['200', '210'], 'isNonZero', true)));

  });
})();
