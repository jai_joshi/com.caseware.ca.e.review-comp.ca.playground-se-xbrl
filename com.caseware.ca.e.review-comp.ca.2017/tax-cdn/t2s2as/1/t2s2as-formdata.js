(function() {
  'use strict';
  wpw.tax.global.formData.t2s2as = {
      formInfo: {
        abbreviation: 'T2S2AS',
        title: 'Donations Through Amalgamation/Wind-up Workchart Summary',
        neededRepeatForms: ['T2S2AW'],
        headerImage: 'cw',
        category: 'Workcharts'
      },
      sections: [
        {
          hideFieldset: true,
          rows: [
            {labelClass: 'fullLength'},
            {labelClass: 'fullLength'},
            {
              type: 'table',
              num: '100'
            },
            {labelClass: 'fullLength'},
            {
              'label': 'Total of all TB GL Accounts:',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '999',
                    'disabled': true,
                    'cannotOverride': true
                  }
                }
              ]
            }
          ]
        }
      ]
    };
})();
