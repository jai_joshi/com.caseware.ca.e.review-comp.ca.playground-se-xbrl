(function() {
  wpw.tax.create.diagnostics('t1145', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0610001', common.prereq(common.and(
        common.requireFiled('T1145'),
        common.check('010', 'isNonZero')),
        common.check(['015', '020'], 'isNonZero')));

    diagUtils.diagnostic('0610003', common.prereq(common.and(
        common.requireFiled('T1145'),
        common.check(['015', '020'], 'isNonZero')),
        common.check('010', 'isNonZero')));

    diagUtils.diagnostic('0610005', common.prereq(common.requireFiled('T1145'),
        common.and(
            common.check('010', 'isNonZero'),
            common.check(['015', '020'], 'isNonZero'))));

    diagUtils.diagnostic('0610009', common.prereq(common.and(
        common.requireFiled('T1145'),
        common.check('030', 'isYes')),
        common.check('035', 'isFilled')));

    diagUtils.diagnostic('0610011', common.prereq(common.requireFiled('T1145'),
        function(tools) {
          return tools.requireAll(tools.list(['040', '050', '055', '060', '070', '080', '085', '090']), 'isNonZero') &&
              tools.requireOne(tools.list(['1011', '1012', '1013']), 'isFilled') &&
              tools.requireOne(tools.list(['1031', '1032', '1033']), 'isFilled');
        }));

    diagUtils.diagnostic('061.010', common.prereq(common.requireFiled('T1145'),
        function(tools) {
          return tools.field('010').require(function() {
            return this.get() == parseFloat(tools.field('015').get()) + parseFloat(tools.field('020').get());
          })
        }));

  });
})();
