(function() {

  wpw.tax.create.calcBlocks('t1145', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field, form) {
      field('010').assign(field('015').get() + field('020').get());
    });
  });
})();
