(function() {

  wpw.tax.create.formData('t1145', {
    formInfo: {
      isRepeatForm: true,
      abbreviation: 'T1145',
      title: 'Agreement to Allocate Assistance for SR&ED Between Persons Not Dealing at Arm\'s Length',
      printNum: 'T1145',
      showCorpInfo: true,
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        rows: [
          {
            'label': 'Agreement',
            'labelClass': 'fullLength titleFont center'
          },
          {
            'label': 'The transferor and the transferee identified below hereby agree to allocate this amount of assistance for SR&ED to the transferee.',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '010',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'format': {
                    'prefix': '$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '010'
                }
              }
            ]
          },
          {
            'label': 'The breakdown of the allocated assistance is:'
          },
          {
            'label': 'Current expenditures',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '015',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'format': {
                    'prefix': '$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '015'
                }
              }
            ]
          },
          {
            'label': 'Capital expenditures (Ensure that you only include amounts related to expenditures of a capital nature made before 2014)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '020',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'format': {
                    'prefix': '$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '020'
                }
              }
            ]
          },
          {
            'label': 'The transferee has to report the allocated amounts (from lines 015 and 020 above) on lines 538 and 540 of Form T661.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'label': 'Is this an amended agreement?',
            'inputType': 'radio',
            'num': '025',
            'tn': '025'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'label': 'Authorization of Transfer',
            'labelClass': 'fullLength titleFont center'
          },
          {
            'label': 'The transferor and the transferee must file with Form T1145: ',
            'labelClass': 'fullLength'
          },
          {
            'label': '- certified copies of the resolutions of the Directors authorizing the agreement; or ',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '- a Directors\' resolution delegating authority to an authorized officer of each corporation signing this form.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': 'The Directors\' resolution will be in effect for all subsequent years until it is rescinded.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'If two corporations are owned exclusively by one shareholder, a T1145 signed by authorized officers of each corporation will be accepted if a signed confirmation by the shareholder is filed with the form stating that he is the only shareholder of both corporations, and that he has authorized the transfer of the assistance from one corporation to the other. A Directors\' resolution will not be required.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'label': 'Were copies of the resolutions/confirmation authorizing the transfer submitted in a previous year?',
            'inputType': 'radio',
            'num': '030',
            'tn': '030'
          },
          {
            'type': 'infoField',
            'label': 'If you answered yes to line 030, in what year was it submitted?',
            'inputType': 'fixedSizeBox',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{4,4}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'boxes': 4,
            'num': '035',
            'tn': '035'
          },
          {
            'label': 'If you answered no to line 030:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- If you are filing a paper return, attach the required documents to Form T1145.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '- If you are filing electronically, refer to the "Paper Documentation" section of RC4018, Electronic Filers Manual, for instructions on how to file paper documents in support of electronically filed forms.',
            'labelClass': 'fullLength tabbed'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'splitTable',
            'side1': [
              {
                'label': 'Name of transferor (print)'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '040',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '175'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '040'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Address (head office if corporation)',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'inputType': 'address',
                'add1Num': '1001',
                'add2Num': '1002',
                'provNum': '1003',
                'cityNum': '1004',
                'countryNum': '1005',
                'postalCodeNum': '1006'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Name of the individual, authorized signing officer of the corporation or authorized partner',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '055',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '2',
                        'max': '20'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '055'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Signature of individual, authorized signing officer of the corporation or authorized partner',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '1008'
              }
            ],
            'side2': [
              {
                'type': 'infoField',
                'inputType': 'custom',
                'format': [
                  '{N9}RC{N4}',
                  '{N9}RZ{N4}',
                  'NR',
                  '{N9}',
                  'NA'
                ],
                'label': 'Business number, Social Insurance Number or Partnership Identification Number',
                'labelWidth': '20%',
                'tn': '045',
                'num': '045',
                'validate': {
                  'check': 'mod10'
                }
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'inputType': 'date',
                'label': 'Tax year-end',
                'num': '050',
                'tn': '050'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Title',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '060',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '60'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '060'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Date',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'inputType': 'date',
                'labelWidth': '0%',
                'width': '90%',
                'num': '065',
                'tn': '065'
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'type': 'splitTable',
            'side1': [
              {
                'label': 'Name of transferee (print)'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '070',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '175'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '070'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Address (head office if corporation)',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'inputType': 'address',
                'add1Num': '1021',
                'add2Num': '1022',
                'provNum': '1023',
                'cityNum': '1024',
                'countryNum': '1025',
                'postalCodeNum': '1026'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Name of the individual, authorized signing officer of the corporation or authorized partner',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '085',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '60'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '085'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Signature of individual, authorized signing officer of the corporation or authorized partner',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '1028'
              }
            ],
            'side2': [
              {
                'type': 'infoField',
                'inputType': 'custom',
                'format': [
                  '{N9}RC{N4}',
                  '{N9}RZ{N4}',
                  'NR',
                  '{N9}',
                  'NA'
                ],
                'label': 'Business number, Social Insurance Number or Partnership Identification Number',
                'labelWidth': '20%',
                'tn': '075',
                'num': '075',
                'validate': {
                  'check': 'mod10'
                }
              },
              {
                'type': 'horizontalLine'
              },
              {
                'type': 'infoField',
                'inputType': 'date',
                'label': 'Tax year-end',
                'num': '080',
                'tn': '080'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Title',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'labelWidth': '0%',
                'width': '90%',
                'num': '090',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '60'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '090'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Date',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'inputType': 'date',
                'labelWidth': '0%',
                'width': '90%',
                'num': '095',
                'tn': '095'
              }
            ]
          },
          {
            'label': 'Personal information is collected pursuant to the <i>Income Tax Act</i> and the Income Tax Regulations and is used for verification of compliance, administration and enforcement of the Scientific Research and Experimental Development (SR&ED) program requirements. Information may also be used for the administration and enforcement of other provisions of the Act, including assessment, audit, enforcement, collections, and appeals, and may be disclosed under information-sharing agreements in accordance with the Act. Incomplete or inaccurate information may result in delays in processing SR&ED claims. The social insurance number is collected pursuant to section 237 of the Act and is used for identification purposes. Information is described in Scientific Research and Experimental Development CRA PPU 441 and is protected under the <i>Privacy Act</i>. Individuals have a right of protection, access to, and correction or notation of their personal information. Please be advised that you are entitled to complain to the Privacy Commissioner of Canada regarding our handling of your information.',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  });
})();
