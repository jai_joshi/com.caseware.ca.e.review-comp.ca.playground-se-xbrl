(function() {
  wpw.tax.create.calcBlocks('t2s305', function(calcUtils) {

    //Part 1
    calcUtils.calc(function(calcUtils, field) {
      // TODO: tn 110 from S38 is not available for now
      var totalCapitalOfAllRelatedCorp = field('entityProfileSummary.010').total(8).get();
      var amount120 = 0;
      if (totalCapitalOfAllRelatedCorp <= 10000000) {
        amount120 = field('t2s306.100').cell(0, 2).get()
      } else {
        amount120 = 0;
      }
      field('120').assign(amount120);
      field('121').assign(Math.abs(Math.max(field('110').get() - field('120').get(), 0)));
      field('101').assign(field('t2s5.143').get() + field('t2s5.144').get());
      field('103').assign(field('t2s5.169').get());
      field('103').source(field('t2s5.169'));
      field('102').assign(field('101').get() / field('103').get() * 100);

      field('201').assign(field('121').get());
      field('202').assign(field('102').get());
      field('203').assign(field('201').get() * field('202').get() / 100);

      field('301').assign(field('t2s5.103').get() + field('t2s5.104').get());
      field('303').assign(field('t2s5.129').get());
      field('303').source(field('t2s5.129'));
      field('302').assign(field('301').get() / field('303').get() * 100);

      field('304').assign(field('t2s5.143').get() + field('t2s5.144').get());
      field('306').assign(field('t2s5.169').get());
      field('306').source(field('t2s5.169'));
      field('305').assign(field('304').get() / field('306').get() * 100);

      field('401').assign(field('302').get() + field('305').get());

      var amount402 = 0;
      var amount404 = 0;

      if (field('t2s5.129').get() == 0 && field('T2S5.169').get() > 0) {
        amount402 = field('ratesNl.501').get();
        amount404 = field('ratesNl.502').get();
      }
      else if (field('t2s5.169').get() == 0 && field('T2S5.129').get() > 0) {
        amount402 = field('ratesNl.505').get();
        amount404 = field('ratesNl.506').get();
      }
      else {
        amount402 = field('ratesNl.503').get();
        amount404 = field('ratesNl.504').get();
      }
      field('402').assign(amount402);
      field('404').assign(amount404);

      field('403').assign(field('401').get() * field('402').get() / field('404').get());
      field('501').assign(field('121').get());
      field('502').assign(field('403').get());
      field('503').assign(field('501').get() * field('502').get() / 100);
      calcUtils.getApplicableValue('124', ['121', '203', '503']);
      calcUtils.subtract('130', '124', '125');
    });

    //Part 2
    calcUtils.calc(function(calcUtils, field) {

      calcUtils.equals('601', '130');
      calcUtils.equals('606', '130');
      calcUtils.equals('611', '130');

      field('603').assign(field('ratesNl.300').get());
      field('608').assign(field('ratesNl.400').get());
      field('613').assign(field('ratesNl.500').get());

      field('604').assign(field('601').get() *
          field('602').get() /
          field('605').get() *
          field('603').get() / 100);
      field('609').assign(field('606').get() *
          field('607').get() /
          field('610').get() *
          field('608').get() / 100);
      field('614').assign(field('611').get() *
          field('612').get() /
          field('615').get() *
          field('613').get() / 100);

      calcUtils.sumBucketValues('650', ['604', '609', '614']);

      var aprilDate = wpw.tax.date(2015, 4, 1);
      var janDate = wpw.tax.date(2016, 1, 1);
      var aprilDateComparisons = calcUtils.compareDateAndFiscalPeriod(aprilDate);
      var janDateComparisons = calcUtils.compareDateAndFiscalPeriod(janDate);
      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();

      field('602').assign(aprilDateComparisons.daysBeforeDate);
      field('605').assign(daysFiscalPeriod);
      field('607').assign(aprilDateComparisons.daysAfterDate - janDateComparisons.daysAfterDate);
      field('610').assign(daysFiscalPeriod);
      field('612').assign(janDateComparisons.daysAfterDate);
      field('615').assign(daysFiscalPeriod);

      field('702').assign(daysFiscalPeriod);

      field('701').assign(field('650').get());
      field('703').assign(365);

      field('704').assign((daysFiscalPeriod < 357) ? (field('701').get() * field('702').get() / field('703').get()) : 0);

      calcUtils.getApplicableValue('150', ['650', '704']);

    });
  });
})();
