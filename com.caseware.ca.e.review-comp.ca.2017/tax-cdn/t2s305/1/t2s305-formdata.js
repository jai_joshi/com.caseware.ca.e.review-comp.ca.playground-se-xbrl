(function() {

  wpw.tax.create.formData('t2s305', {
    formInfo: {
      abbreviation: 'T2S305',
      title: 'Newfoundland and Labrador Capital Tax On Financial Institutions<br> (2016 and later tax years)',
      schedule: 'Schedule 305',
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'For use by corporations that are financial institutions with a permanent ' +
              'establishment (as defined in the federal <i>Income Tax Regulations</i>) at any time ' +
              'in the tax year in the province of Newfoundland and Labrador, including off-shore areas, ' +
              'and that are liable to pay tax on their capital for tax years starting after October 31, 2008.'
            },
            {
              label: 'Subsection 66.1(1) of the Newfoundland and Labrador <i>Income Tax Act</i> ' +
              'defines the term <b>financial institution</b>.'
            },
            {
              label: 'You have to complete Schedule 38, <i>Part VI Tax on Capital of Financial Institutions</i>, ' +
              'in order to complete this schedule. File these two completed schedules with the ' +
              '<i>T2 Corporation Income Tax Return</i> within six months of the end of the tax year.'
            }
          ]
        }
      ],
      category: 'Newfoundland and Labrador Forms'
    },
    sections: [
      {
        'header': 'Part 1 – Newfoundland and Labrador taxable capital in excess of capital deduction for the year',
        'rows': [
          {
            'label': 'Capital for the year (amount from line 190 or 290 of Schedule 38)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Deduct: ',
            'labelClass': 'bold'
          },
          {
            'label': 'Capital deduction claimed for the year*',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '120',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Excess amount (line 110 <b>minus</b> line 120, if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '121'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'If the corporation has a permanent establishment only in Newfoundland and Labrador, including off-shore areas, enter amount C on line J. Otherwise, complete one of the following calculations (whichever applies):',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '  <b>If the corporation is a loan corporation, a trust corporation, or a trust and loan corporation:</b>'
          },
          {
            'type': 'table',
            'num': '100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '200'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter amount E on line J.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '  <b>If the corporation is a bank:</b>'
          },
          {
            'type': 'table',
            'num': '300'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '400'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '500'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Enter amount I on line J.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Newfoundland and Labrador taxable capital employed in the year (amount C, E, or I, whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '124'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': 'Deduct: ',
            'labelClass': 'bold'
          },
          {
            'label': 'Investments in related financial institutions as determined under section 66.4 of the Newfoundland and Labrador <i>Income Tax Act</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '125',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '125'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': '<b>Newfoundland and Labrador taxable capital in excess of capital deduction for the year</b><br>(amount J <b>minus</b> amount K)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Enter $5,000,000 or, for related corporations, the amount allocated on Schedule 306, <i>Newfoundland and Labrador Capital Tax on Financial Institutions – Agreement Among Related Corporations,</i> if the capital of the corporation (amount A) or the combined capital of the corporation and its related financial institutions is $10,000,000 or less. Otherwise, enter zero.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** If line 129 of Schedule 5, <i>Tax Calculation Supplementary – Corporations</i>, is blank or zero, multiply by 1/2 instead of 1/3. If line 169 of Schedule 5 is blank or zero, multiply by 1 instead of 1/3.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 – Newfoundland and Labrador capital tax on financial institutions',
        'rows': [
          {
            'type': 'table',
            'num': '600'
          },
          {
            'label': 'Subtotal (total of amounts M to O)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '650'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'label': '<b>For a tax year less than 51 weeks:</b>'
          },
          {
            'type': 'table',
            'num': '700'
          },
          {
            'label': '<b>Newfoundland and Labrador capital tax on financial institutions</b> (amount P or amount Q, whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '150'
                }
              }
            ],
            'indicator': 'R'
          },
          {
            'label': 'Enter amount R on line 518 of Schedule 5, <i>Tax Calculation Supplementary – Corporations</i>. Amount R may be deducted in calculating net income for federal income tax purposes.'
          }
        ]
      }
    ]
  });
})();
