(function() {
  wpw.tax.create.diagnostics('t2s305', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3050001', common.prereq(common.or(
        common.requireFiled('T2S305'),
        common.check('t2s5.518', 'isFilled')),
        function(tools) {
          return tools.field('cp.tax_start').require(function() {
            return wpw.tax.utilities.dateCompare.greaterThan(this.get(), {year: 2008, month: 10, day: 31});
          })
        }));

    diagUtils.diagnostic('3050002', common.prereq(common.and(
        common.requireFiled('T2S305'),
        function(tools) {
          return tools.checkMethod(tools.list(['110', '120', '125', '130', '150']), 'isNonZero') &&
              tools.field('t2s5.518').isPositive();
        }), function(tools) {
      return tools.requireOne(tools.list(['t2s5.003', 't2s5.103', 't2s5.143', 't2s5.004', 't2s5.104', 't2s5.144']), 'isNonZero') &&
          tools.field('CP.750').require(function() {
            return this.get() == 'NL' || this.get() == 'MJ';
          })
    }));

    diagUtils.diagnostic('3050003', common.prereq(function(tools) {
      return tools.checkMethod(tools.list(['t2s5.003', 't2s5.103', 't2s5.143', 't2s5.004', 't2s5.104', 't2s5.144']), 'isNonZero') &&
          tools.field('CP.750').require(function() {
            return this.get() == 'NL' || this.get() == 'MJ' || this.get() == 'XO';
          }) &&
          (tools.field('t2s24.100').require(function() {
            return this.get() == '7';
          }) ||
          tools.field('t2s5.100').require(function() {
            return this.get() == '405';
          }))
    }, common.requireFiled('T2S305')));

    diagUtils.diagnostic('3050004', common.prereq(common.check(['t2s5.518'], 'isPositive'),
        common.requireFiled('T2S305')));

    diagUtils.diagnostic('3050005', common.prereq(function(tools) {
      return tools.field('120').isPositive && tools.field('cp.226').isYes();
    }, common.requireFiled('T2S306')));

  });
})();

