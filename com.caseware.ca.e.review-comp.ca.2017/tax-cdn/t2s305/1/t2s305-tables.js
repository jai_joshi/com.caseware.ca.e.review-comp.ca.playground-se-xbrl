(function() {

  wpw.tax.create.tables('t2s305', {
    //Part 1
    100: {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '50px',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          type: 'none'
        },
        {
          width: '50px',

          cellClass: 'alignRight'
        },
        {
          width: '10px',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          width: '50px',

          cellClass: 'alignRight'
        },
        {
          width: '20px',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          width: '100px',
          type: 'none'
        }
      ],
      cells: [
        {
          1: {
            label: 'Line 143 <b>plus</b> line 144 of Schedule 5',
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          2: {
            num: '101'
          },
          3: {
            label: '='
          },
          4: {
            num: '102'
          },
          5: {
            label: '% D'
          }
        },
        {
          1: {
            label: 'Line 169 of Schedule 5',
            labelClass: 'center'
          },
          2: {
            num: '103'
          },
          4: {
            type: 'none'
          }
        }
      ]
    },
    200: {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '50px',
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          width: '80px',

        },
        {
          width: '10px',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          width: '100px',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          width: '80px',

        },
        {
          width: '20px',
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          width: '220px',
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          width: '75px',

        },
        {
          width: '15px',
          type: 'none'
        }
      ],
      cells: [
        {
          0: {
            label: 'Amount C'
          },
          1: {
            num: '201'
          },
          2: {
            label: 'x'
          },
          3: {
            label: 'Percentage on line D',
            labelClass: 'right'
          },
          4: {
            num: '202'
          },
          5: {
            label: '%='
          },
          7: {
            num: '203'
          },
          8: {
            label: 'E'
          }
        }
      ]
    },
    300: {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '50px',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          type: 'none'
        },
        {
          width: '50px',

          cellClass: 'alignRight'
        },
        {
          width: '10px',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          width: '50px',

          cellClass: 'alignRight'
        },
        {
          width: '20px',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          width: '100px',
          type: 'none'
        }
      ],
      cells: [
        {
          1: {
            label: 'Line 103 <b>plus</b> line 104 of Schedule 5',
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          2: {
            num: '301'
          },
          3: {
            label: '='
          },
          4: {
            num: '302'
          },
          5: {
            label: '% F'
          }
        },
        {
          1: {
            label: 'Line 129 of Schedule 5',
            labelClass: 'center'
          },
          2: {
            num: '303'
          },
          4: {
            type: 'none'
          }
        },
        {
          1: {
            label: '2 x (line 143 <b>plus</b> line 144 of Schedule 5)',
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          2: {
            num: '304'
          },
          3: {
            label: '='
          },
          4: {
            num: '305'
          },
          5: {
            label: '% G'
          }
        },
        {
          1: {
            label: 'Line 169 of Schedule 5',
            labelClass: 'center'
          },
          2: {
            num: '306'
          },
          4: {
            type: 'none'
          }
        }
      ]
    },
    400: {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '230px',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          type: 'none'
        },
        {
          width: '80px',

          cellClass: 'alignRight'
        },
        {
          width: '20px',
          type: 'none'
        },
        {
          width: '30px',

          cellClass: 'alignRight'
        },
        {
          width: '10px',
          type: 'none'
        },
        {
          width: '30px',

          cellClass: 'alignRight'
        },
        {
          width: '30px',
          type: 'none'
        },
        {
          width: '70px',

          cellClass: 'alignCenter'
        },
        {
          width: '18px',
          type: 'none',
          cellClass: 'alignRight'
        }
      ],
      cells: [
        {
          1: {
            label: '<b>Add</b> percentages F and G'
          },
          2: {
            num: '401'
          },
          3: {
            label: '% x'
          },
          4: {
            num: '402'
          },
          5: {
            label: '/'
          },
          6: {
            num: '404'
          },
          7: {
            label: '** ='
          },
          8: {
            num: '403'
          },
          9: {
            label: '% H'
          }
        }
      ]
    },
    500: {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '50px',
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          width: '80px',

        },
        {
          width: '10px',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          width: '100px',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          width: '80px',

        },
        {
          width: '20px',
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          width: '220px',
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          width: '75px',

        },
        {
          width: '15px',
          type: 'none'
        }
      ],
      cells: [
        {
          0: {
            label: 'Amount C'
          },
          1: {
            num: '501'
          },
          2: {
            label: 'x'
          },
          3: {
            label: 'Percentage on line H',
            labelClass: 'right'
          },
          4: {
            num: '502'
          },
          5: {
            label: '%='
          },
          7: {
            num: '503'
          },
          8: {
            label: 'I'
          }
        }
      ]
    },
    //Part 2
    600: {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '50px',
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          width: '80px',

        },
        {
          width: '10px',
          type: 'none'
        },
        {
          width: '130px',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          width: '80px',

          cellClass: 'alignRight'
        },
        {
          width: '10px',
          type: 'none'
        },
        {
          width: '30px',

        },
        {
          width: '20px',
          type: 'none'
        },
        {
          width: '130px',
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          width: '70px',

        },
        {
          width: '15px',
          type: 'none'
        }
      ],
      cells: [
        {
          0: {
            label: 'Amount L'
          },
          1: {
            num: '601'
          },
          2: {
            label: 'x'
          },
          3: {
            label: 'Number of days in the tax year before April 1, 2015',
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          4: {
            num: '602'
          },
          5: {
            label: 'x'
          },
          6: {
            num: '603'
          },
          7: {
            label: '%='
          },
          9: {
            num: '604'
          },
          10: {
            label: 'M'
          }
        },
        {
          1: {
            type: 'none'
          },
          3: {
            label: 'Number of days in the tax year',
            labelClass: 'center'
          },
          4: {
            num: '605'
          },
          6: {
            type: 'none'
          },
          9: {
            type: 'none'
          }
        },
        {
          0: {
            label: 'Amount L'
          },
          1: {
            num: '606'
          },
          2: {
            label: 'x'
          },
          3: {
            label: 'Number of days in the tax year after March 31, 2015 and before January 1, 2016',
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          4: {
            num: '607'
          },
          5: {
            label: 'x'
          },
          6: {
            num: '608'
          },
          7: {
            label: '%='
          },
          9: {
            num: '609'
          },
          10: {
            label: 'N'
          }
        },
        {
          1: {
            type: 'none'
          },
          3: {
            label: 'Number of days in the tax year',
            labelClass: 'center'
          },
          4: {
            num: '610'
          },
          6: {
            type: 'none'
          },
          9: {
            type: 'none'
          }
        },
        {
          0: {
            label: 'Amount L'
          },
          1: {
            num: '611'
          },
          2: {
            label: 'x'
          },
          3: {
            label: 'Number of days in the tax year after December 31, 2015',
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          4: {
            num: '612'
          },
          5: {
            label: 'x'
          },
          6: {
            num: '613'
          },
          7: {
            label: '%='
          },
          9: {
            num: '614'
          },
          10: {
            label: 'O'
          }
        },
        {
          1: {
            type: 'none'
          },
          3: {
            label: 'Number of days in the tax year',
            labelClass: 'center'
          },
          4: {
            num: '615'
          },
          6: {
            type: 'none'
          },
          9: {
            type: 'none'
          }
        }
      ]
    },
    700: {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          width: '50px',
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          width: '70px',

        },
        {
          width: '10px',
          type: 'none'
        },
        {
          width: '130px',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          width: '70px',

        },
        {
          width: '10px',
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          width: '130px',
          type: 'none'
        },
        {
          width: '60px',

          cellClass: 'alignRight'
        },
        {
          width: '15px',
          type: 'none'
        }
      ],
      cells: [
        {
          0: {
            label: 'Amount P'
          },
          1:{
            num: '701'
          },
          2: {
            label: 'x'
          },
          3: {
            label: 'Number of days in the tax year',
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          4: {
            num: '702'
          },
          5: {
            label: '='
          },
          7: {
            num: '704'
          },
          8: {
            label: 'Q'
          }
        },
        {
          1: {
            type: 'none'
          },
          3: {
            label: '365',
            labelClass: 'center'
          },
          4: {
            num: '703'
          },
          7: {
            type: 'none'
          }
        }
      ]
    }
  })
})();
