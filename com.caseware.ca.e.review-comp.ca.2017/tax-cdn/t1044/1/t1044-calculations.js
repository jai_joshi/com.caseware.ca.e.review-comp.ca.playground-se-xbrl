(function() {

  function assignFromRepeat(calcUtils, form, field, assignField, allForms, gifiArray) {
    if (allForms.length == 1 && gifiArray.length == 1) {
      assignField.fieldAssign(form(allForms[0]).field(gifiArray[0]), form(allForms[0]).field(gifiArray[0]).get() || 0);
    }
    else {
      var formTotal = 0;
      allForms.forEach(function(form) {
        formTotal += getGifiValueSum(calcUtils, form, gifiArray);
      });
      assignField.assign(formTotal);
      assignField.source(null);
    }
  }

  function getGifiValueSum(calcUtils, form, gifiArray) {
    var total = 0;

    gifiArray.forEach(function(gifiNum) {
      total += calcUtils.gifiValue(form, null, gifiNum);
    });
    return total;
  }

  function getGifiValueAdd(calcUtils, gifiArray) {
    var s100 = calcUtils.form('T2S100');
    return calcUtils.gifiValue(s100, null, gifiArray[0]) + calcUtils.gifiValue(s100, null, gifiArray[1]);
  }

  wpw.tax.create.calcBlocks('t1044', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //Part 1 identification//
      calcUtils.getGlobalValue('201', 'cp', 'tax_start');//fiscal period
      calcUtils.getGlobalValue('900', 'cp', 'tax_end');
      calcUtils.getGlobalValue('202', 'cp', 'bn');//business number
      calcUtils.getGlobalValue('001', 'cp', '002');//name of organization
      calcUtils.getGlobalValue('150', 'cp', '194');
      calcUtils.getGlobalValue('220', 'cp', '022');//add1
      calcUtils.getGlobalValue('221', 'cp', '023');//add2
      calcUtils.getGlobalValue('222', 'cp', '026');//prov
      calcUtils.getGlobalValue('223', 'cp', '025');//city
      calcUtils.getGlobalValue('224', 'cp', '027');//country
      calcUtils.getGlobalValue('225', 'cp', '028');//postal
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part1 contact person name
      calcUtils.getGlobalValue('1000', 'CP', '951');//first name
      calcUtils.getGlobalValue('1002', 'CP', '950');//last name
      calcUtils.getGlobalValue('004', 'CP', '954');//title
      calcUtils.getGlobalValue('1001', 'CP', '956');//phone
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.calcOnFormChange('T2S125');
      var t2s100 = calcUtils.form('t2s100');

      var interest = [8090, 8091, 8092, 8093, 8094, 8100, 8101, 8102, 8103, 8095, 8096, 8097, 8247, 8140, 8141, 8237, 8050];
      var gifiS125Remaining = [8221, 8222, 8242, 8210, 8224, 8223];
      var cashShortTermInvestment = [1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1180, 1181, 1182, 1183, 1184, 1185, 1186, 1187];
      var inventory = [1120, 1121, 1122, 1123, 1124, 1127];
      var longTermInvestment = [2300, 2301, 2302, 2303, 2304, 2305, 2306, 2307, 2308, 2309, 2310, 2311];
      var gifi8620To8622 = [8620, 8621, 8622];
      var gifi9060To9066 = [9060, 9061, 9063, 9064, 9065, 9066];
      var gifiS100Remaining = [1073, 1060, 1484, 2630, 2620, 1680, 1681, 1740, 1741, 1774, 1775, 1785, 1786, 1787, 1788,
        1910, 1911, 1912, 1913, 1742, 1743, 1914, 1915];
      //Part 2 GIFI transfer//
      if (field('200').get() == 1) {
        var allForms = wpw.tax.actions.getFilteredRepeatIds('T2S125');
        // var gifi125Array = [];
        // gifi125Array.concat(interest, gifiS125Remaining);
        // assignGifiToNumber(calcUtils, formId, gifi125Array);
        assignFromRepeat(calcUtils, form, field, field('100'), allForms, [8221, 8222]);
        assignFromRepeat(calcUtils, form, field, field('101'), allForms, [8242]);
        assignFromRepeat(calcUtils, form, field, field('102'), allForms, interest);
        assignFromRepeat(calcUtils, form, field, field('103'), allForms, [8210]);
        assignFromRepeat(calcUtils, form, field, field('104'), allForms, [8224]);
        assignFromRepeat(calcUtils, form, field, field('105'), allForms, [8223]);
        field('106').fieldAssign(field('250'));
        //Part 3 GIFI transfer//
        var gifi100Array = [];
        gifi100Array.concat(cashShortTermInvestment, inventory, longTermInvestment,
            gifi8620To8622, gifi9060To9066, gifiS100Remaining);
        // assignGifiToNumber(calcUtils, 'T2S100', gifi100Array, true);
        field('108').assign(getGifiValueSum(calcUtils, t2s100, cashShortTermInvestment));
        field('109').fieldAssign(form('t2s100').field('1073'), form('t2s100').field('1073').get() || 0);
        field('110').fieldAssign(form('t2s100').field('1060'), form('t2s100').field('1060').get() || 0);
        field('111').fieldAssign(form('t2s100').field('1484'), form('t2s100').field('1484').get() || 0);
        field('112').assign(getGifiValueSum(calcUtils, t2s100, inventory));
        field('113').assign(getGifiValueSum(calcUtils, t2s100, longTermInvestment));

        //Part 3 fixed Assets GIFI//
        var landAndLandImprovements =
            calcUtils.gifiValue(t2s100, '101', 1600) +
            calcUtils.gifiValue(t2s100, '101', 1601) -
            calcUtils.gifiValue(t2s100, '101', 1602);

        var buildings = getGifiValueAdd(calcUtils, [1680, 1681]);
        var machinery = getGifiValueAdd(calcUtils, [1740, 1741]);
        var computerEquipments = getGifiValueAdd(calcUtils, [1774, 1775]);
        var otherMachinery = getGifiValueAdd(calcUtils, [1785, 1786]);
        var furniture = getGifiValueAdd(calcUtils, [1787, 1788]);
        var capitalLeasesBuilding = getGifiValueAdd(calcUtils, [1910, 1911]);
        var capitalLeasesEquipment = getGifiValueAdd(calcUtils, [1912, 1913]);

        field('114').assign(
            landAndLandImprovements + buildings + machinery + computerEquipments + otherMachinery +
            furniture + capitalLeasesBuilding + capitalLeasesEquipment);
        field('114').source(field('t2s100.1600'));

        //Part 3 other assets
        var motorVehicles = getGifiValueAdd(calcUtils, [1742, 1743]);
        var capitalLeasesVehicles = getGifiValueAdd(calcUtils, [1914, 1915]);
        field('215').cell(0, 3).assign(motorVehicles);
        field('215').cell(1, 3).assign(capitalLeasesVehicles);
        field('115').assign(
            field('241').get() +
            field('240').get()
        );

        //Part 3 liabilities
        field('117').fieldAssign(form('T2S100').field(2630));
        field('118').fieldAssign(field('251'));

        //Part 4
        var employeeBenefits = getGifiValueSum(calcUtils, t2s100, gifi8620To8622);
        var salaries = getGifiValueSum(calcUtils, t2s100, gifi9060To9066);
        field('120').assign(employeeBenefits + salaries);
      }
      else if (calcUtils.hasChanged('200')) {
        calcUtils.removeValue([100, 101, 102, 103, 104, 105, 106, 108, 109, 110, 111, 112, 113, 114, 115,
          117, 118, 120, 121, 122, 241])
      }
      field('106').fieldAssign(field('250'));
      field('115').fieldAssign(field('240'));
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 2 calculation
      calcUtils.sumBucketValues(107, [100, 101, 102, 103, 104, 105, 106]);
      field('123').fieldAssign(field('107'));
      calcUtils.sumBucketValues(116, [108, 109, 110, 111, 112, 113, 114, 115]);
      field('124').fieldAssign(field('116'));
      calcUtils.sumBucketValues(119, [117, 118]);
      field('125').fieldAssign(field('119'));
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 5
      if (field('520').get() == 2) {
        field('530').disabled(true);
      }
      else {
        field('530').disabled(false);
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part7
      calcUtils.getGlobalValue('701', 'CP', '951');//first name
      calcUtils.getGlobalValue('702', 'CP', '950');//last name
      calcUtils.getGlobalValue('703', 'CP', '954');//title
      calcUtils.getGlobalValue('720', 'CP', '955');
      calcUtils.getGlobalValue('800', 'CP', '990');

    });
  });
})();
