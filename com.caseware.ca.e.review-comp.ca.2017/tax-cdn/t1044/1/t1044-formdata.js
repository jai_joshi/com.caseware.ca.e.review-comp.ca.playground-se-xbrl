(function() {
  'use strict';

  wpw.tax.global.formData.t1044 = {
    'formInfo': {
      abbreviation: 'T1044',
      'title': 'T1044 - Non-Profit Organization (NPO) Information Return',
      printNum: 'T1044',
      headerImage: 'canada-federal',
      agencyUseOnlyBox: true,
      'description': [
        {text: ''},
        {
          'type': 'list',
          'items': [
            {
              label: 'This return is for:',
              sublist: [
                'non-profit organizations (NPOs) described in paragraph 149(1)(l) of the <i>Income Tax Act</i>; and',
                'organizations described in paragraph 149(1)(e) of the Act ' +
                '(agricultural organizations, boards of trade or chambers of commerce).'
              ]
            },
            {
              label: 'An organization has to file this return if:',
              sublist: [
                'it received or is entitled to receive taxable dividends, interest, rentals or royalties ' +
                'totalling more than $10,000 in the fiscal period;',
                'it owned assets valued at more than $200,000 at the end of the ' +
                'immediately preceding fiscal period; or',
                'it had to file an NPO information return for a previous fiscal period.'
              ]
            },
            {
              label: 'To determine if the organization you represent has to complete this return, see Guide T4117, ' +
              'Income Tax Guide to the Non-Profit Organization (NPO) Information Return'
            },
            {label: 'Mail your completed return to: Ottawa Technology Centre, 875 Heron Road, Ottawa ON K1A 1A2'}
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    'sections': [
      {
        header: 'Part 1 - Identification',
        rows: [
          {
            type: 'splitTable',
            side1: [
              {
                type: 'table', num: '199'
              }
            ],
            side2: [
              {labelClass: 'fullLength'},
              {
                type: 'infoField',
                inputType: 'custom',
                format: ['{N9}RC{N4}', 'NR'],
                label: 'Business Number',
                num: '202',
                tn: '202',
                labelWidth: '25%'
              }
            ]
          },
          {type: 'horizontalLine'},
          {
            type: 'splitTable',
            side1: [
              {
                label: 'Name of Organization'
              },
              {
                type: 'infoField',
                inputType: 'textArea',
                num: '001',
                disabled: true
              }
            ],
            side2: [
              { labelClass: 'fullLength'},
              {
                type: 'infoField',
                label: 'Trust (T3) number, if any',
                labelWidth: '68%',
                num: '212',
                textAlign: 'left'
              }
            ]
          },
          {type: 'horizontalLine'},
          {
            type: 'splitTable',
            side1: [
              {
                label: 'Mailing address', labelClass: 'fullLength'
              },
              {
                type: 'infoField',
                label: 'Should the Mailing Address be:',
                labelWidth: '40%',
                width: '30%',
                inputType: 'dropdown',
                num: '150',
                textAlign: 'right',
                options: [
                  {option: 'Same as Head Office', value: '1'},
                  {option: 'Tax Preparers address', value: '2'},
                  {option: 'Other', value: '3'}
                ],
                init: '1',
                disabled: true
              },
              {
                label: 'Care of',
                labelClass: 'fullLength',
                showWhen: {
                  fieldId: '150',
                  compare: {is: 2}
                }
              },
              {
                type: 'infoField',
                labelWidth: '0%',
                width: '90%',
                num: '151',
                tn: '151',
                showWhen: {
                  fieldId: '150',
                  compare: {is: '2'}
                },
                'disabled': true
              },
              {
                type: 'infoField',
                inputType: 'address',
                add1Num: '220',
                add2Num: '221',
                provNum: '222',
                cityNum: '223',
                countryNum: '224',
                postalCodeNum: '225'
              }
            ],
            side2: [
              { labelClass: 'fullLength'},
              { labelClass: 'fullLength'},
              {
                type: 'infoField',
                label: 'Is this the final return to be filed by this organization? If yes, attach an explanation',
                num: '907',
                labelWidth: '73%',
                inputType: 'radio',
                disabled: true,
                init: 2
              },
              { labelClass: 'fullLength'},
              {
                label: 'Explanation why this is the final return to be filed by this organization.',
                labelClass: 'fullLength'
              },
              {
                type: 'infoField',
                inputType: 'textArea',
                num: '003',
                showWhen: {
                  fieldId: '907',
                  compare: {is: 'true'}
                }
              },
              { labelClass: 'fullLength'},
              {type: 'horizontalLine'},
              { labelClass: 'fullLength'},
              { labelClass: 'fullLength'},
              {
                type: 'infoField',
                inputType: 'dropdown',
                label: 'Type of organization (see Guide T4117)',
                labelWidth: '90%',
                width: '100%',
                num: '002',
                init: '1',
                options: [
                  {option: '01. Recreational or social organization ', value: '1'},
                  {option: '02. Professional association', value: '2'},
                  {option: '03. Board of trade or chamber of commerce ', value: '3'},
                  {option: '04. Organization operated for civic improvement ', value: '4'},
                  {option: '05. Agricultural organization ', value: '5'},
                  {option: '06. Educational organization ', value: '6'},
                  {option: '07. Multicultural organization ', value: '7'},
                  {option: '08. Arts or cultural organization ', value: '8'},
                  {option: '09. Low-cost housing organization ', value: '9'},
                  {option: '10. Residential condominium corporation ', value: '10'},
                  {option: '30. Other', value: '30'}
                ]
              }
            ]
          },
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {
            label: 'Name of person to contact'
          },
          {
            type: 'table', num: '909'
          }
        ]
      },
      {
        ////TODO: This section shouldn't be displayed when the form is printed///

        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {
            label: '<b>Complete T1044 from GIFI?</b> <br> (As you answer "Yes", T1044 will be calculated from GIFI)',
            type: 'infoField',
            inputType: 'radio',
            init: '1',
            num: '200'
          },
          {
            label: 'If you want to modify completed T1044 from GIFI, please <i>override</i> the fields',
            labelClass: 'tabbed2'
          }
        ]
      },
      {
        header: 'Part 2 - Amounts received during the fiscal period',
        rows: [
          {
            'label': 'Membership dues, fees, and assessments',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '100'
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              },
              null
            ]
          },
          {
            'label': 'Federal, provincial, and/or municipal grants and payments',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '101'
                },
                'padding': {
                  'type': 'tn',
                  'data': '101'
                }
              },
              null
            ]
          },
          {
            'label': 'Interest, taxable dividends, rentals, and royalties',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '102'
                },
                'padding': {
                  'type': 'tn',
                  'data': '102'
                }
              },
              null
            ]
          },
          {
            'label': 'Proceeds of disposition of capital property',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '103'
                },
                'padding': {
                  'type': 'tn',
                  'data': '103'
                }
              },
              null
            ]
          },
          {
            'label': 'Gross sales and revenues from organizational activities',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '104'
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              },
              null
            ]
          },
          {
            'label': 'Gifts',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '105'
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              null
            ]
          },
          {
            'label': 'Other receipts (specify):'
          },
          {
            'type': 'table',
            'num': '016'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total other receipts',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '106'
                },
                'padding': {
                  'type': 'tn',
                  'data': '106'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Total receipts </b> 107 (add lines 100 to 106)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '107'
                },
                'padding': {
                  'type': 'tn',
                  'data': '107'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '123'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          }
        ]
      },
      {
        header: 'Part 3 - Statement of assets and liabilities at the end of the fiscal period',
        rows: [
          {
            'label': 'Assets',
            'labelClass': 'bold'
          },
          {
            'label': 'Method used to record assets',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true
            },
            'columns': [
              {
                'input': {
                  'num': 813
                }
              },
              null,
              null
            ]
          },
          {
            'label': 'Cash and short-term investments',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '108'
                },
                'padding': {
                  'type': 'tn',
                  'data': '108'
                }
              },
              null
            ]
          },
          {
            'label': 'Amounts receivable from members',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '109'
                },
                'padding': {
                  'type': 'tn',
                  'data': '109'
                }
              },
              null
            ]
          },
          {
            'label': 'Amounts receivable from all others (not included on line 109) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '110'
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              },
              null
            ]
          },
          {
            'label': 'Prepaid expenses',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '111'
                },
                'padding': {
                  'type': 'tn',
                  'data': '111'
                }
              },
              null
            ]
          },
          {
            'label': 'Inventory',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '112'
                },
                'padding': {
                  'type': 'tn',
                  'data': '112'
                }
              },
              null
            ]
          },
          {
            'label': 'Long-term investments',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '113'
                },
                'padding': {
                  'type': 'tn',
                  'data': '113'
                }
              },
              null
            ]
          },
          {
            'label': 'Fixed assets',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '114'
                },
                'padding': {
                  'type': 'tn',
                  'data': '114'
                }
              },
              null
            ]
          },
          {
            'label': 'Other assets (specify):'
          },
          {
            'type': 'table',
            'num': '215'
          },
          {
            'type': 'table',
            'num': '015'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total other assets',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '115'
                },
                'padding': {
                  'type': 'tn',
                  'data': '115'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Total assets</b>(add lines 108 to 115)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '116'
                },
                'padding': {
                  'type': 'tn',
                  'data': '116'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '124'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Liabilities',
            'labelClass': 'bold'
          },
          {
            'label': 'Amounts owing to members',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '117'
                },
                'padding': {
                  'type': 'tn',
                  'data': '117'
                }
              },
              null
            ]
          },
          {
            'label': 'Amounts owing to all others (specify):'
          },
          {
            'type': 'table',
            'num': '026'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amounts owing to all others',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '118'
                },
                'padding': {
                  'type': 'tn',
                  'data': '118'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Total liabilities </b> 108 (add lines 108 to 115)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '119'
                },
                'padding': {
                  'type': 'tn',
                  'data': '119'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '125'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          }
        ]
      },
      {
        header: 'Part 4 - Remuneration',
        rows: [
          {
            'label': 'Total remuneration and benefits paid to all employees and officers',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '120'
                },
                'padding': {
                  'type': 'tn',
                  'data': '120'
                }
              }
            ]
          },
          {
            'label': 'Total remuneration and benefits paid to employees and officers who are members',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '121'
                },
                'padding': {
                  'type': 'tn',
                  'data': '121'
                }
              }
            ]
          },
          {
            'label': 'Other payments to members (specify)',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true
            },
            'columns': [
              {
                'input': {
                  'num': 814
                }
              },
              null,
              {
                'input': {
                  'num': '122'
                },
                'padding': {
                  'type': 'tn',
                  'data': '122'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Number of members in the organization',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '126'
                }
              }
            ]
          },
          {
            'label': 'Number of members who received remuneration or other amounts',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': false,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '127'
                }
              }
            ]
          }
        ]
      },
      {
        header: 'Part 5 - The organization\'s activities',
        rows: [
          {
            label: 'Briefly describe the activities of the organization. If this is the organization\'s ' +
            'first year filing this return, attach a copy of the organization\'s Mission Statement.'
          },
          {
            num: '510',
            type: 'infoField',
            inputType: 'textArea'
          },
          {
            num: '520',
            'label': ' Are any of the organization\'s activities carried on outside of Canada?',
            labelWidth: '80%',
            type: 'infoField',
            inputType: 'radio',
            init: 2
          },
          {
            label: 'If yes, indicate where:'
          },
          {
            num: '530',
            type: 'infoField',
            inputType: 'textArea'
          }
        ]
      },
      {
        header: 'Part 6 - Location of books and records',
        rows: [
          {
            label: 'Leave this area blank if the information is the same as in Part 1.'
          },
          {
            label: 'Name of person to contact',
            type: 'infoField',
            num: '610'
          },
          {type: 'horizontalLine'},
          {
            label: 'Mailing address',
            labelClass: 'fullLength bold'
          },
          {
            type: 'infoField',
            inputType: 'address',
            add1Num: '600',
            add2Num: '601',
            provNum: '602',
            cityNum: '603',
            countryNum: '604',
            postalCodeNum: '605'
          }
        ]
      },
      {
        header: 'Part 7 - Certification',
        rows: [
          {
            label: 'I certify that the information given on this return and ' +
            'in any attached documents is correct and complete',
            labelClass: 'fullLength'
          },
          {

            labelClass: 'fullLength'
          },
          {
            label: 'Authorized officer:',
            labelClass: 'fullLength tabbed2 bold'
          },
          {
            type: 'table', num: '700'
          },
          {

            labelClass: 'fullLength'
          },
          {

            labelClass: 'fullLength'
          },
          {
            type: 'table', num: '1200'
          },
          {

            labelClass: 'fullLength'
          },
          {

            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            label: 'Language of correspondence/Langue de correspondance',
            labelClass: 'bold',
            inputType: 'dropdown',
            num: '800',
            options: [
              {option: 'English', value: "1"},
              {option: 'French', value: "2"}
            ]
          },
          {
            label: 'Indicate the language of your choice/Indiquer la langue de votre choix',
            labelClass: 'fullLength'
          }
        ]
      }
    ]
  };
})();
