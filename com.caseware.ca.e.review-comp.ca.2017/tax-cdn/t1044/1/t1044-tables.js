(function() {
  wpw.tax.global.tableCalculations.t1044 = {
    '199': {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          header: 'Fiscal period:<br> from',
          type: 'date',
          cellClass: 'alignCenter'
        },
        {
          type: 'none',
          width: '20px'
        },
        {
          header: 'To',
          type: 'date',
          cellClass: 'alignCenter'
        }
      ],
      cells: [
        {
          0: {num: '201'},
          2: {num: '900'}
        }
      ]
    },
    '700': {
      'infoTable': 'true',
      'fixedRows': 'true',
      'columns': [
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          cellClass: 'alignCenter',
          'width': '25%',
          type: 'text'
        },
        {
          cellClass: 'alignCenter',
          'width': '25%',
          type: 'text'
        },
        {
          'width': '10px',
          'type': 'none'
        },
        {
          cellClass: 'alignCenter',
          type: 'text'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '1': {
            'num': '701',
            type: 'text'
          },
          '2': {
            'num': '702',
            type: 'text'
          },
          '4': {
            'num': '703',
            type: 'text'
          }
        },
        {
          '1': {
            'type': 'none',
            'label': 'First Name',
            cellClass: 'alignCenter'
          },
          '2': {
            'type': 'none',
            'label': 'Last Name',
            cellClass: 'alignCenter'
          },
          '4': {
            'type': 'none',
            'label': 'Position',
            cellClass: 'alignCenter'
          }
        }]
    },
    '909': {
      'fixedRows': true,
      'infoTable': true,
      'num': 909,
      'columns': [
        {cellClass: 'alignLeft', type: 'text'},
        {cellClass: 'alignLeft', type: 'text'},
        {cellClass: 'alignLeft', type: 'text'},
        {cellClass: 'alignLeft', type: 'text'}
      ],
      'cells': [
        {
          '0': {
            'num': '1000',
            'label': 'First Name'
          },
          '1': {
            'num': '1002',
            'label': 'Last Name'
          },
          '2': {
            'num': '004',
            'label': 'Title'
          },
          '3': {
            'num': '1001',
            'label': 'Telephone number',
            'telephoneNumber': true, type: 'text'
          }
        }
      ]
    },
    '1200': {
      'infoTable': 'true',
      'fixedRows': 'true',
      'columns': [
        {colClass: 'std-padding-width', 'type': 'none'},
        {cellClass: 'alignCenter'},
        {'width': '10px', 'type': 'none'},
        {cellClass: 'alignCenter', type: 'date'},
        {colClass: 'std-padding-width', 'type': 'none'}
      ],
      'cells': [
        {
          '1': {'type': 'none'},
          '3': {'num': '720'}
        },
        {
          '1': {
            'type': 'none',
            'label': 'Authorized officer\'s signature',
            'labelClass': 'fullLength center lineUp'
          },
          '3': {
            'type': 'none',
            'label': 'Date',
            cellClass: 'alignCenter'
          }
        }
      ]
    },
    '015': {
      'infoTable': true,
      'maxLoop': 100000,
      'showNumbering': true,
      hasTotals: true,
      'columns': [
        {
          'type': 'none',
          'width': '2.5%'
        },
        {
          'width': '350px',
          type: 'text'
        },
        {
          'type': 'none',
          'width': '10%'
        },
        {
          'total': true,
          totalNum: '240',
          'width': '12%',
          filters: 'prepend $',
        },
        {
          'type': 'none'
        }
      ]
    },
    '215': {
      'infoTable': true,
      'fixedRows': true,
      hasTotals: true,
      'maxLoop': 100000,
      'columns': [
        {
          'type': 'none',
          'width': '4%'
        },
        {
          'header': 'Description',
          'width': '350px',
          type: 'none'
        },
        {
          'type': 'none',
          'width': '10%'
        },
        {
          'header': 'Amount',
          'total': true,
          'totalNum': '241',
          'totalMessage': 'Total from GIFI:',
          'width': '12%',
          filters: 'prepend $',
        },
        {
          'type': 'none'
        }
      ],
      cells: [
        {
          '1': {
            label: 'Motor vehicles'
          }
        },
        {
          '1': {
            label: 'Capital leases - Vehicles'
          }
        }
      ]
    },
    '016': {
      'infoTable': true,
      'maxLoop': 100000,
      hasTotals: true,
      'showNumbering': true,
      'columns': [
        {
          'type': 'none',
          'width': '2.5%'
        },
        {
          'width': '350px',
          type: 'text'
        },
        {
          'type': 'none',
          'width': '10%'
        },
        {
          'total': true,
          totalNum: '250',
          'width': '12%',
          filters: 'prepend $',
        },
        {
          'type': 'none'
        }
      ]
    },
    '026': {
      'infoTable': true,
      'maxLoop': 100000,
      hasTotals: true,
      'showNumbering': true,
      'columns': [
        {
          'type': 'none',
          'width': '2.5%'
        },
        {
          'width': '350px',
          type: 'text'
        },
        {
          'type': 'none',
          'width': '10%'
        },
        {
          'total': true,
          totalNum: '251',
          'width': '12%',
          filters: 'prepend $',
        },
        {
          'type': 'none'
        }
      ]
    }
  }
})();
