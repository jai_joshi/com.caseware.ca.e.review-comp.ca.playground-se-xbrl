(function() {
  wpw.tax.create.diagnostics('t1044', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T1044'), common.bnCheck('202')));

  });
})();
