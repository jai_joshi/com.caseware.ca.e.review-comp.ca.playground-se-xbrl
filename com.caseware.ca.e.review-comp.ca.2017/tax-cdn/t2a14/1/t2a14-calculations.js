(function() {
  function disableNums(field, numArray) {
    for (var i = 0; i < numArray.length; i++) {
      field(numArray[i]).disabled(true);
    }
  }

  wpw.tax.create.calcBlocks('t2a14', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('002', 'T2S10', '200');
      calcUtils.getGlobalValue('004', 'T2S10', '222');
      calcUtils.getGlobalValue('008', 'T2S10', '226');
      field('010').assign((field('004').get() + field('008').get())* field('ratesAb.014').get());
      calcUtils.getGlobalValue('0101', 'T2S10', '228');
      field('011').assign(field('0101').get() * field('ratesAb.016').get());
      field('013').assign(Math.max(field('010').get() - field('011').get(), 0));
      calcUtils.getGlobalValue('006', 'T2S10', '224');
      field('012').assign(field('002').get() + field('013').get() + field('006').get());
      calcUtils.getGlobalValue('014', 'T2S10', '242');
      calcUtils.getGlobalValue('016', 'T2S10', '244');
      calcUtils.getGlobalValue('018', 'T2S10', '246');
      field('020').assign((field('014').get() + field('016').get() + field('018').get())* field('ratesAb.014').get());
      field('022').assign(field('012').get() - field('020').get());
      field('024').assign(field('022').get() < 0 ? 0 :
        Math.max(field('022').get() - field('023').get(), 0) * field('ratesAb.015').get() /100
      );
      field('026').assign(field('022').get() < 0 ? 0 :
        field('022').get() - field('023').get() - field('024').get());
    });
    calcUtils.calc(function (calcUtils, field) {
      if (field('022').get() < 0) {
        field('030').assign(field('022').get() * -1);
        calcUtils.getGlobalValue('042', 'T2S10', '1680');
        field('046').assign(Math.max(field('042').get() + field('044').get() + field('032').get() - field('034').get(), 0));
        calcUtils.getGlobalValue('048', 'T2A14', '056');
        field('050').assign(Math.max(field('046').get() - field('047').get() - field('048').get(), 0));
        field('052').assign(Math.max(field('030').get() - field('050').get(), 0));
        field('036').assign(Math.max((field('032').get() - field('034').get()) * field('ratesAb.016').get(), 0));
        field('054').assign(Math.max((field('052').get() - field('036').get()) * field('ratesAb.017').get(), 0));
      }
      else {
        disableNums(field, ['030', '042', '044', '032', '034', '046', '047', '048', '050', '052', '036', '054', '056', '040'])
      }
      //For a taxation year that ends before January 1, 2017, complete line 056 and line 040
      var endDate2016 = wpw.tax.date(2016, 12, 31);
      var isStartAfter2017 = calcUtils.dateCompare.lessThan(endDate2016, field('cp.tax_start').get());
      if (isStartAfter2017) {
        field('056').assign(Math.max(Math.min(field('030').get(), field('050').get()),0));
        field('040').assign(Math.max(field('054').get() + field('056').get(),0));
      }
      else {
        field('056').assign(0);
      }

    })
  })
}());