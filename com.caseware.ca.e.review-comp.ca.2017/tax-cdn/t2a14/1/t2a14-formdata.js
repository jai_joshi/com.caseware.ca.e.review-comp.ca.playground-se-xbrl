(function () {

  wpw.tax.create.formData('t2a14', {
    formInfo: {
      abbreviation: 'T2a14',
      title: 'AT14 - Alberta Cumulative Eligible Capital Deduction',
      schedule: 'Schedule 14',
      category: 'Alberta Forms',
      formFooterNum: 'AT14 (Mar-18)',
      hideHeaderBox: true,
      hideProtectedB: true,
      description: [
        {
          type: 'list',
          items: [
            {
              label: '<b>This schedule is <u>required</u> if the opening balance or the claim for Alberta ' +
              'purposes differs from that for federal purposes</b>'
            },
            {
              label: 'For a tax year that ends on or after January 1, 2017, and includes December 31, 2016,' +
              'complete up to line 022 and either of the following:',
              sublist: [
                'If line 022 Cumulative Eligible Capital balance is positive, complete the AT1 ' +
                'Schedule 14 Supplemental Worksheet.',
                'If line 022 Cumulative Eligible Capital balance is negative, complete Schedule 14, Area B on Page 2.'
              ]
            },
            {
              label: 'Report all monetary amounts in dollars; DO NOT include cents. Show negative amounts in brackets ( ).'
            }
          ]
        }
      ]
    },
    sections: [
      {
        header: 'AREA A - Current Year Deduction and Carry-Forward',
        rows: [
          {
            'label': 'Cumulative eligible capital - balance at end of preceding taxation year<br>' +
            '<i>(if negative, enter "0" )</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '002',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '002'
                }
              }
            ]
          },
          {
            'label': 'Cost of eligible capital property acquired during the taxation year',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '004',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '004'
                }
              },
              null
            ]
          },
          {
            'label': 'Other adjustments',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '008',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '008'
                }
              },
              null
            ]
          },
          {
            'label': 'Calculate: (lines 004 + 008) X 3/4',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '010',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '010'
                }
              }
            ]
          },
          {
            label: 'Non-taxable portion of a non-arm\'s length transferor\'s gain realized on the transfer' +
            ' of an eligible capital property to the corporation after',
            'labelClass': 'tabbed'
          },
          {
            type: 'table',
            num: '100'
          },
          {
            'label': 'Subtotal: line 010 - line 011 <i>(if negative, enter "0")</i>',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '013',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '013'
                }
              }
            ]
          },
          {
            'label': 'Amount transferred on amalgamation or wind-up of subsidiary',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '006',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '006'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 002 + line 013 + line 006)',
            'layout': 'alignInput',
            'labelCellClass': 'alignRight',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '012',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '012'
                }
              }
            ]
          },
          {
            'label': 'Proceeds of sale (less outlays and expenses not otherwise deductible), ' +
            'from the disposition of all eligible capital property during the taxation year',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '014',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '014'
                }
              },
              null
            ]
          },
          {
            'label': 'The gross amount of a reduction in respect of a forgiven debt obligation as ' +
            'provided for in ITA subsection 80(7)',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '016',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '016'
                }
              },
              null
            ]
          },
          {
            'label': 'Other adjustments',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '018',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '018'
                }
              },
              null
            ]
          },
          {
            'label': 'Calculate: (line 014 + line 016 + line 018) X 3/4',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '020',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '020'
                }
              }
            ]
          },
          {
            'label': '<b>Cumulative eligible capital balance</b> (line 012 - line 020)<br>' +
            '<i>(If negative, enter "0" at lines 024 and 026 and proceed to AREA B on page 2)</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '022',
                  'validate': {
                    'or': [
                      {
                        'matches': '^-?[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '022'
                }
              }
            ]
          },
          {
            'label': 'Cumulative eligible capital for a property no longer owned after ceasing to carry on that business',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '023',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '023'
                }
              }
            ]
          },
          {
            'label': '<b>Current year deduction</b> (Line 022 - line 023) X 7% *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '024',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '024'
                }
              }
            ]
          },
          {
            'label': '<b><i>Carry forward the sum of line 023 + line 024 to Schedule 12, line 010</i></b>'
          },
          {
            'label': '<b>Cumulative eligible capital - closing balance</b><br>(line 022 - line 023 - line 024)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '026',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '026'
                }
              }
            ]
          },
          {
            labelClass: 'fullLength'
          },
          {
            type: 'description',
            lines: [
              {
                type: 'list', listType: 'asterisk',
                items: [
                  {
                    label: 'You can claim any amount up to the maximum deduction of 7%. <br>' +
                    'The deduction may not exceed the maximum amount prorated by the number of days in the ' +
                    'taxation year divided by 365. If your taxation year ends after December 31, 2016, and you ' +
                    'are using Schedule 14 and the Schedule 14 Supplemental Worksheet to calculate your ' +
                    'CEC balance at the beginning of January 1, 2017, you are not entitled to a current year deduction.'
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        'header': 'AREA B - Amount to be Included in Income Arising from Disposition',
        rows: [
          {
            'label': '(Complete this section only if the amount at line 022 in AREA A is negative)'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount from line 022 above <b>(show as a positive amount)</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '030',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '030'
                }
              }
            ]
          },
          {
            'label': 'Total of cumulative eligible capital deductions claimed for taxation' +
            ' years commencing after June 30, 1988',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '042',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '042'
                }
              },
              null
            ]
          },
          {
            'label': 'Total of all amounts which reduced cumulative eligible capital in' +
            ' the current or prior years under ITA subsection 80(7)',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '044',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '044'
                }
              },
              null
            ]
          },
          {
            'label': 'Total of cumulative eligible capital deductions claimed for taxation years commencing before July 1, 1988',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '032',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '032'
                }
              },
              null
            ]
          },
          {
            'label': 'Cumulative eligible capital account balances that were required' +
            ' to be included in income for taxation years commencing before July 1, 1988 (show as a positive amount)',
            'layout': 'alignInput',
            'labelCellClass': 'indent',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '034',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '034'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal: line 042 + line 044 + line 032 - line 034 <i>(if negative, enter "0" )</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '046',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '046'
                }
              }
            ]
          },
          {
            'label': 'Amounts included in income under ITA paragraph 14(1)(b),' +
            ' as that paragraph applied to a taxation year ending after June 30, 1988 and before February 28, 2000,' +
            ' to the extent that it is for an amount described at line 042',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '047',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '047'
                }
              }
            ]
          },
          {
            'label': 'For a taxation ending after February 27, 2000:<br>' +
            'Line 056 from Schedule 14 of previous taxation years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '048',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '048'
                }
              }
            ]
          },
          {
            'label': 'line 046 - (line 047 + line 048) <i>(if negative, enter "0")</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '050',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '050'
                }
              }
            ]
          },
          {
            'label': 'Line 030 - line 050 <i>(if negative, enter "0")</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '052',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '052'
                }
              }
            ]
          },
          {
            'label': '(Line 032 - line 034) X 1/2<i>(if negative, enter "0")</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '036',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '036'
                }
              }
            ]
          },
          {
            'label': '(Line 052 - line 036) X 2/3<i>(if negative, enter "0")</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '054',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '054'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: 'For a taxation year that ends on or after January 1, 2017, and includes December 31, 2016, ' +
            'proceed to Page 2 of the Schedule 14 Supplemental Worksheet.<br>' +
            'For a taxation year that ends before January 1, 2017, complete line 056 and line 040',
            labelClass: 'bold'
          },
          {
            'label': 'The lesser of line 030 or line 050 <i>(if negative, enter "0")</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '056',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '056'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount to be included in income (line 054 + line 056) <i>(If negative, enter "0")</i>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '040',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '040'
                }
              }
            ]
          },
          {
            'label': '<b><i>Carry this amount to Schedule 12, line 012</b></i>'
          }
        ]
      }
    ]
  });
})();
