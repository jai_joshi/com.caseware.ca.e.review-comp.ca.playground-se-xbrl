(function() {
  wpw.tax.create.tables('t2a14', {
    '100':{
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          type: 'none',
          colClass: 'std-input-col-width'
        },
        {
          colClass: 'std-input-width ',
          formField: true
        },
        {
          type: 'none',
          colClass: 'half-col-width'
        },
        {
          type: 'none'
        },
        {
          colClass: 'std-input-col-width',
          formField: true
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      cells: [
        {
          '1':{
            'label': 'December 20, 2002:'
          },
          '2':{
            'num': '0101'
          },
          '3':{
            'label': 'X 1/2'
          },
          '5':{
            'tn': '011',
            'num': '011'
          }
        }
      ]
    }
  });
})();