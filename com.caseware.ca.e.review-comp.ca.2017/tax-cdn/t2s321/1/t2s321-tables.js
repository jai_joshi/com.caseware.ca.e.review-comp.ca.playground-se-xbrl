(function() {

  var descriptionWidth = '190px';

  wpw.tax.create.tables('t2s321', {
    '099': {
      showNumbering: true,
      hasTotals: true,
      maxLoop: 100000,
      columns: [
        {
          'header': 'CCA Class',
          colClass: 'std-input-width',
          type: 'selector',
          tn: '101',
          selectorOptions: {
            title: 'CCA Classes',
            items: wpw.tax.codes.ccaClasses,
            hideKeys: true
          }
        },
        {
          header: 'Description of qualified property',
          type: 'text'
        },
        {
          header: 'Acquisition date',
          type: 'date',
          colClass: 'std-input-width',
          tn: '102'
        },
        {
          header: 'Capital cost',
          colClass: 'std-input-width',
          tn: '103', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          total: true,
          totalNum: '100',
          totalMessage: 'Total capital cost',
          totalIndicator: 'A'
        }
      ]
    },
    '1220': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'small-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      cells: [
        {
          '0': {label: 'Current year credit earned:........Amount A from above'},
          '1': {num: '1004'},
          '2': {label: 'x'},
          '3': {num: '1003'},
          '4': {label: '%='},
          '5': {tn: '120'},
          '6': {num: '120', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}}
        }
      ]
    },
    '1000': {
      fixedRows: true,
      infoTable: true,
      hasTotals: true,
      columns: [
        {
          width: descriptionWidth,
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          type: 'date'
        },
        {
          type: 'none'
        },
        {
          width: descriptionWidth,
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',

          total: true,
          totalNum: '904',
          totalMessage: 'Total (enter on line C in Part 2)'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {
            label: '1st preceding taxation year'
          },
          '3': {
            label: ' Credit to be applied ',
            labelClass: 'center'
          },
          '4': {
            tn: '901'
          },
          '5': {
            num: '901', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            label: '2nd preceding taxation year'
          },
          '3': {
            label: ' Credit to be applied ',
            labelClass: 'center'
          },
          '4': {
            tn: '902'
          },
          '5': {
            num: '902', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            label: '3rd preceding taxation year'
          },
          '3': {
            label: ' Credit to be applied ',
            labelClass: 'center'
          },
          '4': {
            tn: '903'
          },
          '5': {
            num: '903', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }]
    },
    '2000': {
      hasTotals: true,
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          width: descriptionWidth,
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          colClass: 'std-input-width',
          type: 'date',
          header: 'Year of origin (earliest year first)'
        },
        {
          type: 'none'
        },
        {
          header: 'Credit available',
          colClass: 'std-input-width',
          total: true,
          totalNum: '905',
          totalMessage: 'Total (equals line 200 in Part 2)'
        },
        {type: 'none', colClass: 'std-padding-width'}
      ],
      cells: [
        {
          '0': {
            label: '7th preceding taxation year'
          }
        },
        {
          '0': {
            label: '6th preceding taxation year'
          }
        },
        {
          '0': {
            label: '5th preceding taxation year'
          }
        },
        {
          '0': {
            label: '4th preceding taxation year'
          }
        },
        {
          '0': {
            label: '3rd preceding taxation year'
          }
        },
        {
          '0': {
            label: '2nd preceding taxation year'
          }
        },
        {
          '0': {
            label: '1st preceding taxation year'
          }
        },
        {
          '0': {
            label: 'Current tax year-ending on '
          }
        }
      ]
    },
    '3000': {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          type: 'date',
          header: '<b>Year of origin</b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          totalNum: '501',
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          totalNum: '502',
          totalMessage: ' ',
          header: '<b>Current year contribution</b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          totalNum: '503',
          totalMessage: ' ',
          header: '<b>Transfer</b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          totalMessage: ' ',
          totalNum: '504',
          header: '<b>Amount available to apply</b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          totalMessage: ' ',
          disabled: true,
          totalNum: '505',
          header: '<b>Applied<b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          totalMessage: ' ',
          disabled: true,
          totalNum: '506',
          header: '<b>Balance to carry forward</b>'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      cells: [
        {
          '4': {label: '*'},
          '5': {type: 'none'},
          '7': {type: 'none'},
          '9': {type: 'none'},
          '11': {type: 'none'},
          '13': {type: 'none', label: 'N/A'}
        },
        {
          '5': {type: 'none'},
          '14': {label: '**'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '5': {type: 'none'}
        },
        {
          '3': {type: 'none'},
          '7': {type: 'none'}
        }
      ]
    }

  })
})();
