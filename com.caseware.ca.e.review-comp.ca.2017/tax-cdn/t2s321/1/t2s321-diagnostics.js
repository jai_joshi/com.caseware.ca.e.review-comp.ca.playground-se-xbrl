(function() {
  wpw.tax.create.diagnostics('t2s321', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3210001', common.prereq(common.and(
        common.requireFiled('T2S321'),
        common.check(['t2s5.530'], 'isNonZero')),
        function(tools) {
          var table = tools.field('099');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireAll([row[0], row[2], row[3]], 'isNonZero')
                || tools.requireOne(tools.list(['105', '110', '130', '140']), 'isNonZero');
          })
        }));

    diagUtils.diagnostic('3210002', common.prereq(common.requireFiled('T2S321'),
        function(tools) {
          var table = tools.field('099');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[0], row[2], row[3]], 'isNonZero'))
              return tools.requireAll([row[0], row[2], row[3]], 'isNonZero');
            else return true;
          })
        }));

  });
})();

