(function() {
  wpw.tax.create.formData('t2s321', {
    formInfo: {
      abbreviation: 'T2S321',
      title: 'Prince Edward Island Corporation Investment Tax Credit',
      schedule: 'Schedule 321',
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 321 E (99)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'For use by corporations who have acquired qualified property after December 31, 1992, ' +
              'and wish to reduce Prince Edward Island tax payable. Qualified property is machinery and ' +
              'equipment prescribed for the purposes of paragraph (b) of the definition "qualified property" ' +
              'in subsection 127(9) of the federal <i>Income Tax Act</i>. The capital cost of qualified property is ' +
              'determined without reference to subsection 13(7.1) of the federal <i>Income Tax Act</i>.'
            },
            {
              label: 'The qualified property has to be used by the corporation in Prince Edward Island' +
              'primarily for the purpose of manufacturing or processing of goods for sale or lease. ' +
              'Property leased by the corporation to a lessee for this purpose (other than a person ' +
              'exempt from tax under section 149 of the federal <i>Income Tax Act</i>) may also qualify for ' +
              'the credit. Manufacturing or processing is defined in subsection 125.1(3) of the ' +
              'federal <i>Income Tax Act</i> and includes qualified activities as defined by Regulation 5202 ' +
              'of the federal Income Tax Regulations.'
            },
            {
              label: 'The credit may be renounced but must include all current year credits; partial renouncements ' +
              'are not permitted. The renouncement must be filed on or before the filing date of the ' +
              'federal <i>T2 Corporation Income Tax Return</i>'
            },
            {label: 'The credit is eligible for a seven year carry-forward and a three year carry-back.'},
            {
              label: 'Use this schedule to show a credit transfer following an amalgamation or wind-up ' +
              'of a subsidiary as described under subsections 87(1) and 88(1) of the federal <i>Income Tax Act</i>. ' +
              'This schedule can also be used to show the credit allocated from a trust or a partnership.'
            },
            {label: 'File one completed copy of this schedule with your T2 Corporation Income Tax Return.'}
          ]
        }
      ],
      category: 'Prince Edward Island Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Qualified property (acquired in current taxation year) eligible for the credit',
        'rows': [
          {
            'type': 'table',
            'num': '099'
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of total credit available and credit available for carry-forward',
        'rows': [
          {
            'label': 'Credit at end of preceding taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1001'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Deduct:</b> Credit expired after seven taxation years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '104',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '104'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit at beginning of taxation year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '105',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '105'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1002',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Add',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit transferred on amalgamation or wind-up of subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '110'
                }
              },
              null
            ]
          },
          {
            'type': 'table',
            'num': '1220'
          },
          {
            'label': 'Credit allocated from a partnership',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit allocated from a trust',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '140',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '140'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1005'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1006'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Total credit available',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1007'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Deduct: ',
            'labelClass': 'bold'
          },
          {
            'label': 'Credit renounced',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '150'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit claimed in the current year (enter on line 530 in Part 2 of Schedule 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '160',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '160'
                }
              },
              null
            ]
          },
          {
            'label': 'Credit carried back to preceding taxation year(s) (complete Part 3) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1008'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Subtotal',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '1009'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '1010'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': 'Closing balance',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ]
          }
        ]
      },
      {
        'header': 'Part 3 – Request for carry-back of credit',
        'rows': [
          {
            'type': 'table',
            'num': '1000'
          }
        ]
      },
      {
        'header': 'Part 4 – Analysis of credit available for carry-forward by year of origin',
        'rows': [
          {
            'type': 'table',
            'num': '2000'
          }
        ]
      },
      {
        'header': 'Historical Data and Calculation for PEI Corporate Investment Tax Credit',
        'rows': [
          {
            'type': 'table',
            'num': '3000'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  });
})();
