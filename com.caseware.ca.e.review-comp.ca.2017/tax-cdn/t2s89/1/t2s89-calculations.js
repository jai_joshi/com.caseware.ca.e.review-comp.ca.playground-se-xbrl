(function() {

  function getSortedData(sortedDataArr) {
    return sortedDataArr.filter(function(a) {
      return angular.isDefined(a);
    }).sort(function(a, b) {
      return wpw.tax.utilities.milliseconds(b) - wpw.tax.utilities.milliseconds(a);
    });
  }

  wpw.tax.create.calcBlocks('t2s89', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field, form) {
      //part 1 calcs
      var inputTable = field('090');
      var calcTable = field('091');
      var tableRow = calcTable.getRow(0);
      var taxYearEnd = field('cp.tax_end').get();
      tableRow[0].assign(taxYearEnd);
      //col 1
      var amounttn890S6 = field('t2s6.890');
      var amountABILS6 = field('t2s6.710');
      var col1Val = (amounttn890S6.get() + amountABILS6.get()) / 2;
      tableRow[1].assign(Math.max(0, col1Val - inputTable.total(1).get()));
      tableRow[1].source(amounttn890S6);
      //col 2
      var amountES3 = field('t2s3.542').total(1);//total of amount E on part 1 S3
      tableRow[2].assign(Math.max(0, amountES3.get() - inputTable.total(2).get()));
      tableRow[2].source(amountES3);
      //col6
      var amount510S3 = field('t2s3.510');//amount 510 on S3
      tableRow[6].assign(Math.max(0, amount510S3.get() - inputTable.total(6).get()));
      tableRow[2].source(amount510S3);
      //total of 091 = total of 090 + total of 091
      for (var i = 1; i < 7; i++) {
        var total = 0;
        total += inputTable.total(i).get();
        total += calcTable.cell(0, i).get();
        calcTable.track();
        calcTable['valueObj']['value']['totals'][i] = total;
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      // tn001 calcs
      var taxYearEnd = field('cp.tax_end').get();
      var dataArr = [];
      field('090').getCol(0).forEach(function(cell) {
        dataArr.push(cell.get())
      });
      dataArr.push(taxYearEnd);
      dataArr = getSortedData((dataArr));
      var latestDateVal = dataArr[0];
      field('001').assign(latestDateVal);
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 2 section B
      var inputTable = field('249');
      var calcTable = field('248');
      var tableRow = calcTable.getRow(0);
      var taxYearEnd = field('cp.tax_end').get();

      inputTable.getRows().forEach(function(row) {
        row[3].assign(row[1].get() - row[2].get());
      });

      tableRow[0].assign(taxYearEnd);
      //col 1
      tableRow[1].assign(field('t2s10.214').get());
      tableRow[1].source(field('t2s10.214'));
      tableRow[3].assign(tableRow[1].get() - tableRow[2].get());
      //total of 248 = total of 249 + total of 248
      var total = 0;
      total += inputTable.total(3).get();
      total += calcTable.cell(0, 3).get();
      calcTable.track();
      calcTable['valueObj']['value']['totals'][3] = total;
    });

    calcUtils.calc(function(calcUtils, field, form) {
      calcUtils.calcOnFieldChange(field('091'));
      calcUtils.calcOnFieldChange(field('248'));
      field('390').assign(Math.max(field('091').total(1).get(), 0));
      field('395').assign(field('091').total(2).get());
      field('400').assign(Math.max(field('248').total(3).get(), 0));
      field('405').assign(Math.max(field('091').total(3).get(), 0));
      field('415').assign(field('091').total(4).get());
      field('416').assign(field('091').total(5).get());
      calcUtils.sumBucketValues('421', ['390', '395', '400', '405', '410', '415', '416', '420']);
      field('422').assign(field('091').total(6).get());
      calcUtils.subtract('430', '421', '422');
      field('431').assign(field('248').cell(0, 3).get());
      field('432').assign(Math.max(field('430').get() - field('431').get(), 0));
    });
  });
})();
