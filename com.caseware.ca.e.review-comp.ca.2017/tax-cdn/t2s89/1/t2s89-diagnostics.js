(function() {
  wpw.tax.create.diagnostics('t2s89', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var forEach = diagUtils.forEach;

    diagUtils.diagnostic('BN', common.prereq(common.requireFiled('T2S89'), forEach.row('299', forEach.bnCheckCol(1, true))));

  });
})();
