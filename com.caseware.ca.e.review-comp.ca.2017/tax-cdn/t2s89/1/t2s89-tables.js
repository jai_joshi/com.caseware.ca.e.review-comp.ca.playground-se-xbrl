(function() {

  var columnWidth = '107px';

  var part1ColsArr = [
    {
      header: '1<br>Tax year-end or<br> relevant date<br>(Note 3)',
      type: 'date',
      colClass: 'std-input-width',
      tn: '100'
    },
    {
      header: '2<br>Non-taxable portion of capital gains and non-deductible capital losses per paragraph 89(1)(a)' +
      '<br>(Note 4)',
      tn: '110',
      total: true
    },
    {
      header: '3<br>Capital dividends received per paragraph 89(1)(b)<br>(Note 5)',
      tn: '120',
      total: true
    },
    {
      header: '4<br>Net proceeds of a life insurance policy per paragraph 89(1)(d)',
      tn: '130',
      total: true
    },
    {
      header: '5<br>Non-taxable portion of capital gains from a trust per paragraph 89(1)(f)<br>(Note 6)',
      tn: '140',
      total: true
    },
    {
      header: '6<br>Capital dividends from a trust per paragraph 89(1)(g)',
      tn: '150',
      total: true
    },
    {
      header: '7<br>Capital dividends payable per subsection 83(2)',
      tn: '160',
      total: true
    }
  ];

  var part2ColsArr = [
    {
      header: '1<br>Tax year-end',
      tn: '250',
      type: 'date',
      colClass: 'std-input-width'
    },
    {
      header: '2<br>Amount from line S of Schedule 10',
      tn: '260'
    },
    {
      header: '3<br>Appropriate portion of the amount deducted as a bad debt per subsection 20(4.2) or as an allowable capital loss per subsection 20(4.3)',
      tn: '270'
    },
    {
      header: '4<br>Non-taxable portion of ECP sales<br>(column 2 minus column 3)',
      tn: '280',
      total: true
    }
  ];

  function getTableColumns(colsObjArr, isCalculatedTable) {
    var colsArr = [];

    if (!angular.isArray(colsObjArr))
      colsObjArr = [colsObjArr];

    colsObjArr.forEach(function(colObj) {
          var colType = colObj.type;
          var colHeader = colObj.header || '';

          colsArr.push(
              {
                header: isCalculatedTable ? '' : '<b>' + colHeader + '</b>',
                type: colType,
                disabled: colObj.disabled,
                tn: isCalculatedTable ? '' : colObj.tn,
                total: colObj.total,
                hiddenTotal: !isCalculatedTable
              }
          )
        }
    );

    return colsArr;
  }

  wpw.tax.create.tables('t2s89', {
    '199': {
      hasTotals: true,
      maxLoop: 100000,
      showNumbering: true,
      columns: [
        {
          header: '<b>1</b><br>Tax year-end',
          tn: '200',
          colClass: 'std-input-width',
          type: 'date'
        },
        {

          header: '2<br>Cost of eligible capital property acquired',
          tn: '210'
        },
        {
          header: '<b>3</b><br>Proceeds of sale (less outlays and expenses not otherwise deductible) from ' +
          'the disposition of all eligible capital property ',
          tn: '220'
        }
      ]
    },
    '249': {
      hasTotals: true,
      maxLoop: 100000,
      columns: getTableColumns(part2ColsArr)
    },
    '248': {
      hasTotals: true,
      fixedRows: true,
      keepButtonsSpace: true,
      columns: getTableColumns(part2ColsArr, true)
    },
    '299': {
      hasTotals: true,
      showNumbering: true,
      maxLoop: 100000,
      columns: [
        {
          header: '1<br>Corporation\'s name',
          tn: '300'
        },
        {
          header: '2<br>Business number',
          tn: '310',
          type: 'custom',
          format: ['{N9}RC{N4}', 'NR']
        },
        {
          header: '3<br>Date the dividend became payable',
          tn: '320',
          type: 'date',
          colClass: 'std-input-width'
        }
      ]
    },
    '090': {
      hasTotals: true,
      maxLoop: 100000,
      columns: getTableColumns(part1ColsArr)
    },
    '091': {
      hasTotals: true,
      fixedRows: true,
      keepButtonsSpace: true,
      columns: getTableColumns(part1ColsArr, true)
    }
  })
})();
