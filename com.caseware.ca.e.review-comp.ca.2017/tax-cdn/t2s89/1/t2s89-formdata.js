(function() {
  'use strict';

  wpw.tax.create.formData('t2s89', {
    'formInfo': {
      'abbreviation': 't2s89',
      'schedule': 'Schedule 89',
      'title': 'Request for Capital Dividend Account Balance Verification',
      'headerImage': 'canada-federal',
      formFooterNum: 'T2 SCH 89 E ',
      'showCorpInfo': true,
      description: [
        {
          'type': 'list',
          'items': [
            'If you are a private corporation, use this schedule to summarize the components making up ' +
            'your capital dividend account (CDA) balance as at the date entered on line 001 below.',
            'Mail one completed copy of this schedule, separately from any other return, to your tax services office.' +
            'Find your tax service office\'s address by going to <b>cra.gc.ca/tso</b>',
            'For specific details about calculating the CDA balance, see the applicable legislation in ' +
            'the federal <i>Income Tax Act</i>.',
            'All legislative references are to the current version of the Act. But since the CDA balance ' +
            'components can span several years, these references may not apply to older components of ' +
            'your CDA balance. In these cases, see the version of the Act that applies to the appropriate year.',
            'All references to paragraphs in subsection 89(1) of the Act are under the definition of ' +
            '"capital dividend account".',
            'If you are paying out a capital dividend from your CDA, you must file Form T2054, <i>Election for a ' +
            'Capital Dividend Under Subsection 83(2)</i>.Attach a copy of this completed form. ' +
            'Note that if a capital dividend paid out under this election exceeds the balance ' +
            'of the CDA at the time the dividend becomes payable, you may have to pay Part lll tax on ' +
            'the excessive dividends (see section 184 of the Act).'
          ]
        }
      ],
      category: 'Federal Tax Forms'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            type: 'infoField',
            label: 'Capital dividend account balance as at',
            inputType: 'date',
            tn: '001',
            num: '001',
            labelWidth: '280px'
          },
          {
            label: '<b>Please check one of the following:</b>'
          },
          {
            label: 'Is this a balance verification request?',
            type: 'infoField',
            inputType: 'radio',
            num: '002',
            tn: '002',
            labelWidth: '280px'
          },
          {
            label: 'Is this request related to the requirements of section 89(1) for form T2054?',
            type: 'infoField',
            inputType: 'radio',
            num: '003',
            tn: '003',
            labelWidth: '280px'
          }
        ]
      },
      {
        header: 'Part 1 - CDA components (except for eligible capital property) (Note 1 and Note 2)',
        rows: [
          {
            type: 'table',
            num: '090'
          },
          {
            type: 'table',
            num: '091'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Note 1. For eligible capital property, see parts 2 and 4.',
            labelClass: 'fullLength'
          },
          {
            label: 'Note 2.  If you were a private corporation under non-resident control that became ' +
            'Canadian controlled as per subsection 89(1.1), and/or were a tax-exempt corporation that became' +
            ' taxable as per subsection 89(1.2), the CDA balance may be reduced to nil immediately ' +
            'before the dates referred to in those provisions.',
            labelClass: 'fullLength'
          },
          {
            label: 'Note 3. Include as many tax years as required. Start your list with the tax year that ' +
            'began after the corporation last became a private corporation and that ended after 1971. ' +
            'End your list on the CDA balance date shown on line 001. If you are completing this schedule ' +
            'before your tax year-end, enter the relevant date of the activity.',
            labelClass: 'fullLength'
          },
          {
            label: 'Note 4. Along with applicable losses, include the non-deductible portion' +
            ' of a business investment loss here. Show losses as a negative.',
            labelClass: 'fullLength'
          },
          {
            label: 'Note 5. May be adjusted by an excessive dividend election under subsection 184(3). ' +
            'Exclude a dividend that subsection 83(2.1) applies to.',
            labelClass: 'fullLength'
          },
          {
            label: 'Note 6. The amounts that can be added to the CDA of the corporation in a particular taxation year, ' +
            'in respect of amounts received by the corporation, from a trust and that are attributable to capital gains ' +
            'realized by the trust or to dividends received and distributed by a trust, can only be determined after' +
            ' the end of the taxation year of the trust in which the capital gains were realized or the dividends ' +
            'were received and distributed by it.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'Part 2 - CDA components - Eligible capital property (ECP)',
        rows: [
          {
            label: 'Record in these tables the most common amounts included in the eligible capital ' +
            'property (ECP) component of the CDA. This information is not meant to replace the ' +
            'calculation at line C in Part 4.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Section A: CDA components - List of eligible capital property acquisitions and dispositions ' +
            '<br>(for tax years ending before February 28, 2000)',
            labelClass: 'bold'
          },
          {
            type: 'table',
            num: '199'
          },
          {labelClass: 'fullLength'},
          {
            label: 'Section B: CDA components - List of ECP dispositions ' +
            '(for tax years ending after February 27, 2000)',
            labelClass: 'bold'
          },
          {
            type: 'table',
            num: '249'
          },
          {
            type: 'table',
            num: '248'
          }
        ]
      },
      {
        header: 'Part 3 - Additional information',
        rows: [
          {
            label: 'For each capital dividend received, as recorded in column 3 in Part 1, give the name and ' +
            'business number of the corporation that paid the capital dividend and the date the ' +
            'dividend became payable.',
            labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            type: 'table',
            num: '299'
          }
        ]
      },
      {
        header: 'Part 4 - CDA balance',
        rows: [
          {
            'label': 'Non-taxable portion of capital gains and non-deductible portion of capital losses' +
            ' (total of column 2 in Part 1; if negative enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '390'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Capital dividends received (total of column 3 in Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '395'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': 'Eligible capital property (as calculated per paragraphs 89(1)(c), (c.1) and (c.2); if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '400'
                },
                'padding': {
                  'type': 'tn',
                  'data': '400'
                }
              }
            ],
            'indicator': 'C'
          },
          {
            'label': 'Life insurance proceeds (total of column 4 in Part 1; if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '405'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Life insurance CDA (Note 7)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '410'
                },
                'padding': {
                  'type': 'tn',
                  'data': '410'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Non-taxable portion of capital gains from a trust (total of column 5 in Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '415'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Capital dividends from a trust (total of column 6 in Part 1).',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '416'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Amounts from predecessor and subsidiary corporations (Note 8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '420'
                },
                'padding': {
                  'type': 'tn',
                  'data': '420'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': 'Subtotal (total of amounts A to H)',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'input': {
                  'num': '421'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': '<b>Deduct:</b> capital dividends that previously became payable (total of column 7 in Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '422'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': '<b>CDA balance </b>(amount I <b>minus</b> amount J)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '430'
                },
                'padding': {
                  'type': 'tn',
                  'data': '430'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': '<b>Deduct: </b>Non-taxable portion of eligible property sales for the current year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '431'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': 'Balance of capital dividend account that can be paid',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '432'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 7. Include the balance of the corporation\'s life insurance CDA immediately before ' +
            'May 24, 1985, in accordance with paragraph 89(1)<i>(e)</i>. <br>For more information, see paragraph 15' +
            ' of Interpretation Bulletin IT66R6 Archived, <i>Capital Dividends</i>.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Note 8.',
            'labelClass': 'fullLength'
          },
          {
            'label': '- For amalgamations and wind-ups <b>before</b> July 14, 1990, calculate the CDA balance of each predecessor or subsidiary corporation separately. Then add these CDA balances to the CDA of the successor or parent corporation. Do not carry forward negative amounts, since these are considered to be nil.',
            'labelClass': 'tabbed'
          },
          {
            'label': '- For amalgamations and wind-ups <b>after</b> July 13, 1990, carry over the amounts of all the CDA components of each predecessor or subsidiary corporation into the calculation of the CDA components of the new corporation. As a result, a negative balance in a component of a CDA of a predecessor or subsidiary corporation has to show in the CDA of the successor or parent corporation. Include a separate CDA calculation on a separate schedule for each predecessor or subsidiary corporation.',
            'labelClass': 'tabbed'
          },
          {
            'label': '- For amalgamations, see paragraph 87(2)(z.1). For wind-ups, see paragraph 88(1)(e.2).',
            'labelClass': 'tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'You must file a form T2054 when you pay Non-Taxable Capital Dividends:',
            'labelClass': 'bold fullLength',
            'labelWidth': '55%',
            'type': 'infoField',
            'inputType': 'link',
            'note': 'Please Click here to go to T2054',
            'formId': 't2054',
            'width': '80%'
          }
        ]
      }
    ]
  });
})();
