(function() {
  var tableMonthlyCells = [
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {}
  ];

  var tableQuarterlyCells = [
    {},
    {},
    {},
    {}
  ];

  function getTableColumns(totalNum1, totalNum2, totalNum3, totalNum4) {
    return [
      {
        colClass: 'std-input-width',
        type: 'date',
        header: '<b>Date</b>'
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        total: true,
        totalNum: totalNum1,
        totalMessage: 'Total : ',
        header: '<b>Tax instalments</b>',
        disabled: true
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        total: true,
        totalNum: totalNum2,
        totalMessage: ' ',
        header: '<b>Instalments paid</b>'
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        disabled: true,
        totalNum: totalNum3,
        header: '<b>Cumulative difference</b>'
      },
      {colClass: 'std-spacing-width', type: 'none'},
      {
        total: true,
        totalNum: totalNum4,
        totalMessage: ' ',
        header: '<b>Instalments payable</b>',
        disabled: true
      },
      {colClass: 'std-padding-width', type: 'none'}
    ]
  }

  wpw.tax.global.tableCalculations.instalmentsWorkchart = {
    'di_table_1': {
      'num': 'di_table_1',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Monthly instalments required',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '402'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '406',
            'init': '1',
            'colClass': 'singleUnderline'
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '403'
          }
        },
        {
          '2': {
            'type': 'none'
          },
          '4': {
            'num': '406_D',
            'init': '12'
          },
          '7': {
            'type': 'none'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_2': {
      'num': 'di_table_2',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Quarterly instalments required',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '404'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '407',
            'init': '1',
            'colClass': 'singleUnderline'
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '405'
          }
        },
        {
          '2': {
            'type': 'none'
          },
          '4': {
            'num': '407_D',
            'init': '4'
          },
          '7': {
            'type': 'none'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_3': {
      'num': 'di_table_3',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Monthly instalments required',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '502'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '506',
            'init': '1',
            'colClass': 'singleUnderline'
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '503'
          }
        },
        {
          '2': {
            'type': 'none'
          },
          '4': {
            'num': '506_D',
            'init': '12'
          },
          '7': {
            'type': 'none'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_4': {
      'num': 'di_table_4',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Quarterly instalments required',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '504'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '507',
            'init': '1',
            'colClass': 'singleUnderline'
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '505'
          }
        },
        {
          '2': {
            'type': 'none'
          },
          '4': {
            'num': '507_D',
            'init': '4'
          },
          '7': {
            'type': 'none'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_5': {
      'num': 'di_table_5',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Each of the first 2 payments under option 3',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '611'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '617',
            'init': '1',
            'colClass': 'singleUnderline'
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '612',
            'cellClass': ' doubleUnderline'
          }
        },
        {
          '2': {
            'type': 'none'
          },
          '4': {
            'num': '617_D',
            'init': '12'
          },
          '7': {
            'type': 'none'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_6': {
      'num': 'di_table_6',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Each of the remaining 10 payments under option 3',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '615'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '618',
            'init': '1',
            'colClass': 'singleUnderline'
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '616',
            'cellClass': ' doubleUnderline'
          }
        },
        {
          '2': {
            'type': 'none'
          },
          '4': {
            'num': '618_D',
            'init': '10'
          },
          '7': {
            'type': 'none'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_7': {
      'num': 'di_table_7',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'First payments under option 3',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '631'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '637',
            'init': '1',
            'colClass': 'singleUnderline'
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '632',
            'cellClass': ' doubleUnderline'
          }
        },
        {
          '2': {
            'type': 'none'
          },
          '4': {
            'num': '637_D',
            'init': '4'
          },
          '7': {
            'type': 'none'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_8': {
      'num': 'di_table_8',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Each of the remaining 3 payments under option 3',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '635'

          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '638',
            'init': '1',
            'colClass': 'singleUnderline'
          },
          '5': {
            'label': '='
          },
          '6': {
            'tn': undefined
          },
          '7': {
            'num': '636',
            'cellClass': ' doubleUnderline'
          }
        },
        {
          '2': {
            'type': 'none'
          },
          '4': {
            'num': '638_D',
            'init': '3'
          },
          '7': {
            'type': 'none'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    '101': {
      fixedRows: true,
      infoTable: true,
      hasTotals: true,
      columns: getTableColumns('102', '103', '104', '105'),
      cells: tableMonthlyCells
    },
    '201': {
      fixedRows: true,
      infoTable: true,
      hasTotals: true,
      columns: getTableColumns('202', '203', '204', '205'),
      cells: tableQuarterlyCells
    },
    '999': {
      infoTable: true,
      fixedRows: true,
      columns: [
        {type: 'none'},
        {colClass: 'std-padding-width', type: 'none'},
        {
          colClass: 'std-input-width',
          type: 'none',
          header: '<b>Option 1 base<br>(Estimated)</b>'
        },
        {colClass: 'std-padding-width', type: 'none'},
        {
          colClass: 'std-input-width',
          header: '<b>Option 2 base</b>',
          type: 'none'
        },
        {colClass: 'std-padding-width', type: 'none'}
      ],
      cells: [
        {}
      ]
    }
  }
})();
