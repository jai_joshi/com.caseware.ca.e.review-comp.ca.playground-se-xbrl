wpw.tax.create.importCalcs('INSTALMENTSWORKCHART', function(importTools) {
  function getImportObj(taxPrepIdsObj) {
    //get and sort by key of taxPrepIdsObj
    //if not in csv, key is not returned
    var output = {};
    Object.keys(taxPrepIdsObj).forEach(function(key) {
      importTools.intercept(taxPrepIdsObj[key], function(importObj) {
        output[key] = importObj;
      });
    });
    return output;
  }

  var insObj = getImportObj({
    method: 'FDINF.Ttwinf66',
    compliance: 'FDINF.Ttwinf173',
    monthly: 'FDINF.Ttwinf174'
  });

  /*importTools.after(function() {
    var ins = importTools.form('INSTALMENTSWORKCHART');

    function getPropIfExists(obj, properties) {
      //recursively return either the object if some multilayer property exists, or an empty string
      //i.e. if foo[1][2][3], return it; if not, return ''
      properties = wpw.tax.utilities.ensureArray(properties, false);
      return properties.length < 1 ? (obj || '') : getPropIfExists((obj || '')[properties[0]], properties.splice(1));
    }

    //to select the correct instalment base method
    if (getPropIfExists(insObj, ['method', 'value'])) {
      var method = Number(insObj['method']['value']);
      ins.setValue(309, (method % 3 + 1).toString());
    }

    //to set both compliance answers according to taxprep's single question
    if (getPropIfExists(insObj, ['compliance', 'value'])) {
      var compliance = insObj['compliance']['value'] == 'Y' ? 1 : 0;
      ins.setValue(302, compliance);
      ins.setValue(303, compliance);
    }

    if (getPropIfExists(insObj, ['monthly', 'value'])) {
      var monthly = insObj['monthly']['value'] == 'Y' ? 1 : 0;
      ins.setValue(308, monthly);
    }
  });*/
});