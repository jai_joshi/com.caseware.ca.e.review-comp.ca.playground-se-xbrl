(function() {

  function fillArray(array, start, end, suffix) {
    for (var i = start; i <= end; i++) {
      if (suffix)
        array.push(i + suffix);
      else
        array.push(i)
    }
  }

  function getDateMonthly(taxEndDate, numberOfMonth) {
    return wpw.tax.utilities.addToDate(taxEndDate, 0, numberOfMonth, -1);
  }

  function instalmentBasecalcUtils(calcUtils, suffix) {
    var totalNum719 = [];
    var totalNum739 = [];
    var totalNum749 = [];
    var totalNum755 = [];
    var totalNum769 = [];
    var totalNum776 = [];
    var totalNum849 = [];

    fillArray(totalNum719, 711, 713, suffix);
    fillArray(totalNum739, 721, 732, suffix);
    fillArray(totalNum749, 741, 744, suffix);
    fillArray(totalNum769, 761, 763, suffix);
    fillArray(totalNum849, 781, 834, suffix);

    //threshold
    if (calcUtils.field('300').get() == 1) {
      calcUtils.removeValue(['755', '776', '777', '862', '865', '755-B', '776-B', '777-B', '862-B', '865-B'], true)
    }
    else {
      fillArray(totalNum755, 751, 752, suffix);
      fillArray(totalNum776, 773, 774, suffix);
    }

    var num700 = '700' + suffix;
    var num711 = '711' + suffix;
    var num719 = '719' + suffix;
    var num739 = '739' + suffix;
    var num741 = '741' + suffix;
    var num749 = '749' + suffix;
    var num750 = '750' + suffix;
    var num751 = '751' + suffix;
    var num755 = '755' + suffix;
    var num756 = '756' + suffix;
    var num769 = '769' + suffix;
    var num771 = '771' + suffix;
    var num772 = '772' + suffix;
    var num773 = '773' + suffix;
    var num776 = '776' + suffix;
    var num777 = '777' + suffix;
    var num849 = '849' + suffix;
    var num862 = '862' + suffix;
    var num863 = '863' + suffix;
    var num865 = '865' + suffix;

    calcUtils.multiply(num711, [num700], (38 / 100), true);
    calcUtils.sumBucketValues(num719, totalNum719, true);
    calcUtils.sumBucketValues(num739, totalNum739, true);
    calcUtils.subtract(num741, num719, num739, true);
    calcUtils.sumBucketValues(num749, totalNum749, true);
    var fiscalPeriod = calcUtils.field('CP.Days_Fiscal_Period').get();
    if (fiscalPeriod > 365) {
      calcUtils.assignValue(num750, 365)
    }
    else {
      calcUtils.assignValue(num750, fiscalPeriod)
    }
    calcUtils.equals(num772, num750, true);
    calcUtils.divide(num751, num749, num750, false, 365, true);
    calcUtils.sumBucketValues(num755, totalNum755, true);
    calcUtils.sumBucketValues(num769, totalNum769, true);
    calcUtils.sumBucketValues(num771, [num756, num769], true);
    calcUtils.divide(num773, num771, num772, false, 365, true);
    calcUtils.sumBucketValues(num776, totalNum776, true);
    calcUtils.sumBucketValues(num777, [num755, num776], true);
    calcUtils.sumBucketValues(num849, totalNum849, true);
    calcUtils.equals(num862, num777, true);
    calcUtils.equals(num863, num849, true);
    calcUtils.subtract(num865, num862, num863, false, true);
  }

  function instalmentCalcs(calcUtils, tableNum, isDelayed, monthDelayed) {
    var cumulativeAmount = 0;
    var tableRows = calcUtils.field(tableNum).getRows();
    tableRows.forEach(function(row, rowIndex) {
      var instalmentAmount = row[2].get();
      var instalmentPaid = row[4].get();
      if (instalmentPaid > 0) {
        row[6].assign(cumulativeAmount + instalmentAmount - instalmentPaid);
        row[8].assign(0);
        cumulativeAmount = row[6].get();
      }
      else {
        if (isDelayed) {
          if (rowIndex < monthDelayed) {
            row[6].assign(cumulativeAmount + instalmentAmount - instalmentPaid);
            row[8].assign(0);
            cumulativeAmount = row[6].get();
          }
          else {
            row[6].assign(0);
            row[8].assign(cumulativeAmount + instalmentAmount);
            cumulativeAmount = 0;
          }
        }
        else {
          row[6].assign(0);
          row[8].assign(cumulativeAmount + instalmentAmount);
          cumulativeAmount = 0;
        }
      }
    });
  }

  wpw.tax.create.calcBlocks('instalmentsWorkchart', function(calcUtils) {

    calcUtils.calc(function(calcUtils, field) {

      if (field('cp.Corp_Type').get() == 1) {
        field('301').assign(1);
      }
      else {
        field('301').assign(2);
      }
      if (angular.isDefined(field('t2j.430').get()) && field('t2j.430').get() > 0) {
        field('304').assign(1);
      }
      else {
        //TODO: Calculation for prior year SBD claim
        field('304').disabled(false);
      }
      //Instalment payments option
      if (field('301').get() == 1 && field('302').get() == 1 && field('303').get() == 1 &&
          field('304').get() == 1 && field('305').get() == 1 && field('306').get() == 1 && field('308').get() == 2) {
        field('307').assign(2)
      }
      else {
        field('307').assign(1)
      }
    });

    calcUtils.calc(function(calcUtils, field) {
      //Option2 get value from t2j & s5
      calcUtils.getGlobalValue('700-B', 'T2J', '371');
      //calcUtils.getGlobalValue('711-B', 'T2J', '550'); already calculated
      calcUtils.getGlobalValue('712-B', 'T2J', '602');
      calcUtils.getGlobalValue('713-B', 'T2J', '604');
      calcUtils.getGlobalValue('721-B', 'T2J', '430');
      calcUtils.getGlobalValue('722-B', 'T2J', '608');
      calcUtils.getGlobalValue('723-B', 'T2J', '616');
      calcUtils.getGlobalValue('724-B', 'T2J', '620');
      calcUtils.getGlobalValue('725-B', 'T2J', '628');
      calcUtils.getGlobalValue('726-B', 'T2J', '632');
      calcUtils.getGlobalValue('727-B', 'T2J', '636');
      calcUtils.getGlobalValue('728-B', 'T2J', '638');
      calcUtils.getGlobalValue('729-B', 'T2J', '639');
      calcUtils.getGlobalValue('730-B', 'T2J', '640');
      calcUtils.getGlobalValue('731-B', 'T2J', '641');
      calcUtils.getGlobalValue('732-B', 'T2J', '648');
      calcUtils.getGlobalValue('733-B', 'T2J', '652');
      calcUtils.getGlobalValue('742-B', 'T2J', '720');
      calcUtils.getGlobalValue('743-B', 'T2J', '724');
      calcUtils.getGlobalValue('744-B', 'T2J', '727');
      //TODO: add Alberta & Quebec when applicable
      var provincialTaxNotON = calcUtils.form('T2S5').field('255').get() - calcUtils.form('T2S5').field('290').get();
      calcUtils.field('756-B').assign(provincialTaxNotON);
      calcUtils.getGlobalValue('761-B', 'T2S5', '496');
      calcUtils.getGlobalValue('762-B', 'T2S5', '278');
      calcUtils.getGlobalValue('763-B', 'T2S5', '280');
      calcUtils.getGlobalValue('789-B', 'T2J', '812');
      calcUtils.getGlobalValue('790-B', 'T2S5', '491');
    });

    calcUtils.calc(function(calcUtils, field) {
      //Calculating instalment base
      instalmentBasecalcUtils(calcUtils, '');
      instalmentBasecalcUtils(calcUtils, '-B');
    });

    calcUtils.calc(function(calcUtils, field) {
      //option 1 calcUtils
      calcUtils.equals('402', '865', true);
      field('403').assign(field('406_D').get() == 0 ? 0 :
          Math.ceil(field('402').get() * field('406').get() / field('406_D').get()));
      if (field('406_D').get())
        calcUtils.equals('404', '865', true);
      field('405').assign(
          field('407_D').get() == 0 ? 0 :
              Math.ceil(field('404').get() * field('407').get() / field('407_D').get()));
    });

    calcUtils.calc(function(calcUtils, field) {
      //option 2 calcUtils
      calcUtils.equals('502', '865-B', true);
      field('503').assign(
          field('506_D').get() == 0 ? 0 :
              Math.ceil(field('502').get() * field('506').get() / field('506_D').get()));
      calcUtils.equals('504', '865-B', true);
      field('505').assign(
          field('507_D').get() == 0 ? 0 :
              Math.ceil(field('504').get() * field('507').get() / field('507_D').get()));
    });

    calcUtils.calc(function(calcUtils, field) {
      //option 3 calcUtils
      calcUtils.sumBucketValues('606', ['602', '603', '604', '605']);
      calcUtils.sumBucketValues('905', ['606', '901', '902', '903', '904']);
      calcUtils.equals('906', '849', true);
      calcUtils.subtract('610', '905', '906');
      //monthly calcUtils
      calcUtils.equals('611', '610');
      field('612').assign(
          field('617_D').get() == 0 ? 0 :
              Math.ceil(field('611').get() * field('617').get() / field('617_D').get()));
      calcUtils.equals('614', '865-B');
      calcUtils.multiply('1615', ['612'], 2);
      calcUtils.subtract('1616', '614', '1615');
      calcUtils.equals('615', '1616');
      field('616').assign(
          field('618_D').get() == 0 ? 0 :
              Math.ceil(field('615').get() * field('618').get() / field('618_D').get()));
      //quarterly calcUtils
      calcUtils.equals('631', '610');
      field('632').assign(
          field('637_D').get() == 0 ? 0 :
              Math.ceil(field('631').get() * field('637').get() / field('637_D').get()));
      calcUtils.equals('634', '865-B');
      calcUtils.equals('1635', '634');
      calcUtils.subtract('1636', '634', '1635');
      calcUtils.equals('635', '1636');
      field('636').assign(
          field('638_D').get() == 0 ? 0 :
              Math.ceil(field('635').get() * field('638').get() / field('638_D').get()));
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //get date for workchart table
      var taxEndDate = calcUtils.form('CP').field('tax_end').get();

      //to get instalment amount monthly
      field('101').getRows().forEach(function(row, rIndex) {
        row[0].assign(getDateMonthly(taxEndDate, rIndex + 1));
        //option 1
        var installmentOption = field('309').get();
        if (installmentOption == 1) {
          rIndex == 11 ? row[2].assign(field('402').get() - field('403').get() * 11) : row[2].assign(field('403').get());
        }
        //option 2
        else if (installmentOption == 2) {
          rIndex == 11 ? row[2].assign(field('502').get() - field('503').get() * 11) : row[2].assign(field('503').get());
        }
        //option 3 : 2 first payment is different from the rest
        else {
          if (rIndex == 0 || rIndex == 1) {
            row[2].assign(field('612').get());
          }
          else {
            rIndex == 11 ? row[2].assign(field('615').get() - field('616').get() * 9) : row[2].assign(field('616').get());
          }
        }
        //to get instalment payable
        var startPaymentDate = field('101').getRow(0)[0].get();
        if (field('isConditionLoad').get()) {
          calcUtils.assignValue('310', startPaymentDate ? startPaymentDate.month.toString() : undefined);
          field('310').disabled(false);
          field('isConditionLoad').assign(true)
        }

        if (startPaymentDate && startPaymentDate.month == field('310').get()) {
          //if no delayed payment
          //calcs the installment payable
          instalmentCalcs(calcUtils, '101');
        }
        else {
          //if there is delayed payment
          if (startPaymentDate) {
            var startPaymentMonth = startPaymentDate.month;
            //check how many months delayed
            var monthsDelayed = field('310').get() - startPaymentMonth;
            if (monthsDelayed < 0) {
              monthsDelayed = 12 - startPaymentMonth + field('310').get()
            }
            instalmentCalcs(calcUtils, '101', true, monthsDelayed);
          }
        }
      });

      //to get instalment amount quarterly
      field('201').getRows().forEach(function(row, rIndex) {
        row[0].assign(getDateMonthly(taxEndDate, rIndex + 4));
        //option 1
        var installmentOption = field('309').get();
        if (installmentOption == 1) {
          rIndex == 3 ? row[2].assign(field('404').get() - field('405').get() * 3) : row[2].assign(field('405').get());
        }
        //option 2
        else if (installmentOption == 2) {
          rIndex == 3 ? row[2].assign(field('504').get() - field('505').get() * 3) : row[2].assign(field('505').get());
        }
        else {
          //option 3 : first payment is different from the rest
          if (rIndex == 0) {
            row[2].assign(field('632').get());
          }
          else {
            rIndex == 3 ? row[2].assign(field('635').get() - field('636').get() * 2) : row[2].assign(field('636').get());
          }
        }
        //calcs the installment payable amount
        instalmentCalcs(calcUtils, '201');
      });

    });
  });
})();
