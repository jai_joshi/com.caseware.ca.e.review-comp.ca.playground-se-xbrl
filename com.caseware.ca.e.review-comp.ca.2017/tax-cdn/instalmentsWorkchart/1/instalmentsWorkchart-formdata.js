(function() {

  function createBaseCalcsColumnHeading() {
    return [
      {
        type: 'table', num: '999'
      }
    ]
  }

  function createBaseCalcsFormdataRows(heading, labelArray, startNum, hasTotal, totalInfo) {
    var formDataRows = [];

    if (!angular.isArray(labelArray))
      labelArray = [labelArray];

    var headerObj = {
      label: heading,
      labelClass: 'bold fullLength'
    };

    formDataRows.push(headerObj);

    for (var i = 0; i < labelArray.length; i++) {
      var rowDataObj = labelArray[i];

      if (!angular.isObject(rowDataObj)) {
        rowDataObj = {label: rowDataObj} || {};
      }

      var defaultInputRow =
          {
            layout: 'alignInput',
            label: rowDataObj.label,
            layoutOptions: {
              'showDots': true,
              'indicatorColumn': true
            },
            'columns': [
              {
                'input': {
                  'num': startNum.toString()
                }
              },
              {
                'input': {
                  'num': startNum.toString() + '-B'
                }
              }
            ]
          };

      if (i == (labelArray.length - 1) && hasTotal) {
        defaultInputRow.columns[0].underline = 'single';
        defaultInputRow.columns[1].underline = 'single';
      }

      formDataRows.push(defaultInputRow);
      startNum++;
    }

    if (hasTotal) {

      var totalObj = {
        layout: 'alignInput',
        label: totalInfo.inputInfo,
        layoutOptions: {
          'showDots': true,
          'indicatorColumn': true
        },
        indicator: totalInfo.indicator,
        'columns': [
          {
            'input': {
              'num': totalInfo.num.toString()
            }
          },
          {
            'input': {
              'num': totalInfo.num.toString() + '-B'
            }
          }
        ]
      };

      formDataRows.push(totalObj);
    }

    return formDataRows;
  }

  wpw.tax.global.formData.instalmentsWorkchart = {
    formInfo: {
      abbreviation: 'INS',
      title: 'Instalments Workchart',
      showCorpInfo: true,
      description: [
        {
          type: 'list',
          items: [
            'Use this worksheet to calculate your monthly or quarterly instalments.',
            'You can use the <b>option</b> that results in the least amount payable by instalments. ' +
            'Any remaining unpaid tax is payable on or before the balance-due day.'
          ]
        },
        {
          'type': 'list',
          'items': [
            'Option 1 - Based on the estimated tax payable for the current tax year',
            'Option 2 - Based on  the tax payable from the previous tax year',
            'Option 3 - Based on the previous tax year and the year before the previous tax year'
          ]
        },
        {
          type: 'list',
          items: [
            'You will be charged <b>interest</b> according to the prescribed interest rate if you ' +
            'make <b>late or insufficient</b> instalment payments.',
            'This schedule is a worksheet only and <b>does not</b> have to be filed with your ' +
            '<i>T2 Corporation Income Tax Return</i>.'
          ]
        }
      ],
      'descriptionHighlighted': true,
      'highlightFieldsets': true,
      category: 'Workcharts'
    },
    sections: [
      {
        forceBreakAfter: true,
        header: 'Instalments method',
        rows: [
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            label: 'Is your taxes payable for either the current or previous year <b>$3,000</b> or less?',
            num: '300',
            inputType: 'radio',
            init: '2'
          },
          {
            label: 'If you answered <b>yes</b>, you <b>do not</b> have to make instalment payments on your ' +
            'federal and provincial or territorial taxes.', labelClass: 'fullLength'
          },
          {labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          // 6 questions are from T7B Guide from CRA
          {labelClass: 'fullLength'},
          {
            label: 'The corporation is eligible to make quarterly instalments, ' +
            'if at the time the payment is due, all the following requirements are satisfied.',
            labelClass: 'fullLength bold'
          },
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            label: 'Is the corporation a Canadian-controlled private corporation (CCPC) ?',
            num: '301',
            inputType: 'radio',
            init: '2'
          },
          {
            type: 'infoField',
            label: 'Has the corporation claimed a small business deduction for the current or previous tax year?',
            num: '304',
            inputType: 'radio',
            init: '2'
          },
          {
            label: 'Has the corporation qualified for a <b>perfect compliance history</b> subject to the following conditions,<br> during the last 12 months up till the previous instalment due date:',
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            label: 'Has the corporation remitted on time all the amounts required for GST/HST, withholding under subsection 153(1), Canada Pension Plan contributions and employment insurance premiums?',
            'labelCellClass': 'indent',
            num: '302',
            inputType: 'radio',
            canClear: true
          },
          {
            type: 'infoField',
            label: 'Has the corporation filed on time all returns required under the <i>Income Tax Act</i> or under Part IX of the <i>Excise Tax Act</i> (GST/HST)?',
            'labelCellClass': 'indent',
            num: '303',
            inputType: 'radio',
            canClear: true
          },
          {
            label: 'Has the corporation together with any <b>associated corporations</b>, for the ' +
            'current or previous tax year: ',
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            label: 'Has taxable income of $500,000 or less?',
            'labelCellClass': 'indent',
            num: '305',
            inputType: 'radio',
            canClear: true
          },
          {
            type: 'infoField',
            label: 'Has taxable capital employed in Canada for the tax year of $10 million or less?',
            'labelCellClass': 'indent',
            num: '306',
            inputType: 'radio',
            canClear: true
          },
          {labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            label: 'Based on the above answer, instalments are calculated: ',
            labelClass: 'bold',
            inputType: 'dropdown',
            num: '307',
            init: '1',
            options: [
              {option: 'Monthly', value: '1'},
              {option: 'Quarterly', value: '2'}
            ]
          },
          {
            label: 'If you answered <b>no</b> to any question, you are <b>not eligible</b> for quarterly instalment method.'
          },
          {
            type: 'infoField',
            label: 'If the corporation is eligible to make <b>quarterly</b> instalment payments, ' +
            'do you want to use the <b>monthly</b> instalments option instead?',
            num: '308',
            inputType: 'radio',
            init: '2'
          },
          {labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          {labelClass: 'fullLength'},
          {
            type: 'infoField',
            label: 'Please choose the instalment calculation option: ',
            labelClass: 'bold',
            inputType: 'dropdown',
            num: '309',
            init: '2',
            options: [
              {option: 'Option 1', value: '1'},
              {option: 'Option 2', value: '2'},
              {option: 'Option 3', value: '3'}
            ]
          },
          {
            label: 'Option 1 - Based on the estimated tax payable for the current tax year<br>' +
            'Option 2 - Based on  the tax payable from the previous tax year<br>' +
            'Option 3 - Based on the previous tax year and the year before the previous tax year',
            'labelCellClass': 'indent'
          },
          {
            type: 'infoField',
            label: 'If the monthly instalments is delayed, choose which month ' +
            'you want the instalments to begin with',
            labelClass: 'bold',
            inputType: 'dropdown',
            num: '310',
            options: [
              {option: 'January', value: '1'},
              {option: 'February', value: '2'},
              {option: 'March', value: '3'},
              {option: 'April', value: '4'},
              {option: 'May', value: '5'},
              {option: 'June', value: '6'},
              {option: 'July', value: '7'},
              {option: 'August', value: '8'},
              {option: 'September', value: '9'},
              {option: 'October', value: '10'},
              {option: 'November', value: '11'},
              {option: 'December', value: '12'}
            ]
          },
          {labelClass: 'fullLength'}
        ]
      },
      {
        forceBreakAfter: true,
        header: 'Monthly instalments workchart ',
        rows: [
          {
            type: 'table', num: '101'
          }
        ]
      },
      {
        forceBreakAfter: true,
        header: 'Quarterly instalments workchart ',
        rows: [
          {
            type: 'table', num: '201'
          }
        ]
      },
      {
        forceBreakAfter: true,
        header: 'Option 1 - Based on the estimated tax payable for the current tax year', spacing: 'R_mult',
        rows: [
          {type: 'table', num: 'di_table_1'},
          {type: 'table', num: 'di_table_2'}
        ]
      },
      {
        forceBreakAfter: true,
        header: 'Option 2 - Based on the tax payable from the previous tax year', spacing: 'R_mult',
        rows: [
          {type: 'table', num: 'di_table_3'},
          {type: 'table', num: 'di_table_4'}
        ]
      },
      {
        forceBreakAfter: true,
        header: 'Option 3 - Based on the previous tax year and the year before the previous tax year ',
        rows: [
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Tax Payable from the year before the previous tax year',
            'labelClass': 'bold'
          },
          {
            'label': 'Part I tax payable',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '602'
                }
              }
            ]
          },
          {
            'label': 'Part VI tax payable',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '603'
                }
              }
            ]
          },
          {
            'label': 'Part VI.1 tax payable',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '604'
                }
              }
            ]
          },
          {
            'label': 'Part XIII.1 tax payable',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '605'
                }
              }
            ]
          },
          {
            'label': 'Total of Parts I, VI, VI.1, and XIII.1 tax :',
            'labelClass': 'bold text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'input': {
                  'num': '606'
                }
              }
            ]
          },
          {
            'label': '<b>Add:</b> Federal adjustment',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '901'
                }
              }
            ]
          },
          {
            'label': 'Provincial or territorial tax (other than Alberta, Quebec and Ontario) before refundable credits',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '902'
                }
              }
            ]
          },
          {
            'label': 'Ontario tax (if applicable)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '903'
                }
              }
            ]
          },
          {
            'label': 'Provincial adjustment',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '904'
                }
              }
            ]
          },
          {
            'label': 'Total tax before refundable credits :',
            'labelClass': 'bold text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'input': {
                  'num': '905'
                }
              }
            ]
          },
          {
            'label': '<b>Subtract:</b> Total current year estimated refundable credits',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '906'
                }
              }
            ]
          },
          {
            'label': 'Instalment base amount :',
            'labelClass': 'bold text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'input': {
                  'num': '610'
                }
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Monthly instalments required :',
            'labelClass': 'bold'
          },
          {type: 'table', num: 'di_table_5'},
          {
            'label': 'Option 2 instalment base amount',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '614'
                }
              }
            ]
          },
          {
            'label': '<b>Subtract:</b> Total of the first 2 payments',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1615',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'The difference for the remaining 10 payments :',
            'labelClass': 'text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'input': {
                  'num': '1616'
                }
              }
            ]
          },
          {type: 'table', num: 'di_table_6'},
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<i>Amount a and b will show in monthly instalments workchart accordingly</i>'
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Quarterly instalments required :',
            'labelClass': 'bold'
          },
          {type: 'table', num: 'di_table_7'},
          {
            'label': 'Option 2 instalment base amount',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '634'
                }
              }
            ]
          },
          {
            'label': '<b>Subtract:</b> First payment',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1635'
                }
              }
            ]
          },
          {
            'label': 'The difference for the remaining 3 payments :',
            'labelClass': 'text-right',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'labelCellClass': 'alignRight',
            'columns': [
              {
                'input': {
                  'num': '1636'
                }
              }
            ]
          },
          {type: 'table', num: 'di_table_8'},
          {
            'label': '<i>Amount c and d will show in quarterly instalments workchart accordingly</i>'
          }
        ]
      },
      {
        forceBreakAfter: true,
        header: 'Calculating instalment base',
        rows: createBaseCalcsColumnHeading().concat(
            createBaseCalcsFormdataRows('',
                [
                  '<b>Taxable income</b>'
                ], 700),
            createBaseCalcsFormdataRows('Calculation of tax payable',
                [
                  'Federal Part I tax',
                  'Recapture of investment tax credit',
                  'Refundable tax on CCPC\'s investment income'
                ], 711, true, {num: '719', indicator: 'A', inputInfo: 'Total tax payable :'}),
            createBaseCalcsFormdataRows('Calculation of deduction',
                [
                  'Small business deduction',
                  'Federal tax abatement',
                  'Manufacturing and processing profits deduction',
                  'Investment corporation deduction',
                  'Additional deduction - credit unions',
                  'Federal foreign non-business income tax credit',
                  'Federal foreign business income tax credit',
                  'General tax reduction for CCPCs',
                  'General tax reduction',
                  'Federal logging tax credit',
                  'Eligible Canadian bank deduction',
                  'Federal qualifying environmental trust tax credit',
                  'Investment tax credit'
                ], 721, true, {num: '739', indicator: 'B', inputInfo: 'Total deduction :'}),
            createBaseCalcsFormdataRows('Federal tax summary',
                [
                  'Total Part I tax payable (amount A minus amount B)',
                  'Total Part VI tax payable',
                  'Total Part VI.1 tax payable',
                  'Total Part XIII.1 tax payable'
                ], 741, true, {num: '749', inputInfo: 'Total federal tax :'}),
            createBaseCalcsFormdataRows('Federal tax adjustment',
                [
                  'Number of days in the tax year',
                  'Adjustment for short taxation year (Total federal tax × 365 / number of days in the tax year)',
                  'Adjustment for amalgamation, winding-up or transfer'
                ], 750, true, {num: '755', inputInfo: 'Total federal tax after adjustment :'}),
            createBaseCalcsFormdataRows('Provincial tax',
                [
                  'Provincial or territorial tax (other than Alberta, Quebec and Ontario) ' +
                  'before refundable credits'
                ], 756, false),
            createBaseCalcsFormdataRows('Ontario tax summary (if applicable)',
                [
                  'Income tax',
                  'Corporation minimum tax paid (credited)',
                  'Special additional tax on life insurance corporation'
                ], 761, true, {num: '769', inputInfo: 'Total Ontario tax :'}),
            createBaseCalcsFormdataRows('',
                [
                  '<b>Provincial or territorial tax (other than Alberta and Quebec) ' +
                  'before refundable credits</b>'
                ], 771, false),
            createBaseCalcsFormdataRows('Provincial tax adjustment',
                [
                  'Number of days in the tax year',
                  'Adjustment for short taxation year (Total federal tax × 365 / number of days in the tax year)',
                  'Adjustment for amalgamation, winding-up or transfer'
                ], 772, true, {num: '776', inputInfo: 'Total provincial tax after adjustment :'}),
            createBaseCalcsFormdataRows('',
                [], '', true, {num: '777', indicator: 'C', inputInfo: 'Total tax before refundable credits :'}),
            createBaseCalcsFormdataRows('Calculation of estimated refundable tax credits',
                [
                  'Investment tax credit refund',
                  'Dividend refund',
                  'Federal capital gains refund',
                  'Federal qualifying environmental trust tax credit refund',
                  'Canadian film or video production tax credit refund',
                  'Film or video production services tax credit refund',
                  'Tax withheld at source',
                  'Provincial or territorial capital gains refund',
                  'Provincial or territorial refundable tax credits other than Alberta, Quebec and Ontario',
                  'Ontario refundable tax credit'
                ], 781, true, {num: '849', indicator: 'D', inputInfo: 'Total estimated refundable tax credits :'}),
            createBaseCalcsFormdataRows('',
                [
                  'Total tax before refundable credits :',
                  'Total refundable tax credit :'
                ], 862, true, {num: '865', inputInfo: 'Instalment base amount (C - D) :'})
        )
      }
    ]
  };
})();