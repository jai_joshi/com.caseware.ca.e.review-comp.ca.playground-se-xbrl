(function() {
  wpw.tax.create.diagnostics('instalmentsWorkchart', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('INS.NoPay', common.prereq(function(tools) {
      return tools.field('cp.Days_Fiscal_Period').get() < 31 ||
          (tools.field('cp.Corp_Type').get() == '1' &&
          tools.field('t2j.425').isPositive() &&
          tools.field('cp.Days_Fiscal_Period').get() < 91) ||
          tools.field('cp.070').isYes() ||
          (tools.field('t2j.700').get() +
          tools.field('t2j.712').get() +
          tools.field('t2j.716').get() +
          tools.field('t2j.727').get() < 3000);
    }, function(tools) {
      return tools.requireAll(tools.list(['865', '865-B']), 'isZero');
    }));

    diagUtils.diagnostic('INS.quarterlyPayment', common.prereq(common.check('301', 'isYes')), common.check('304', 'isNo'));

    diagUtils.diagnostic('INS.amalgamation', common.prereq(common.check('cp.071', 'isYes'), common.check('752', 'isNotDefault')));

  });
})();
