(function() {
  wpw.tax.global.tableCalculations.t2s365 = {

    900: {
      showNumbering: true,
      hasTotals: true,
      columns: [
        {
          header: 'Certificate number<br><br>',
          tn: '100', "validate": {"or": [{"length": {"min": "9", "max": "9"}}, {"check": "isEmpty"}]},
          type: 'text'
        },
        {
          header: 'Amount of the New Brunswick film tax credit<br>$<br>',
          colClass: 'std-input-width',
          tn: '200', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          total: true,
          totalMessage: 'Total amount of the New Brunswick film tax credit',
          totalNum: '300',
          totalTn: '300'
        }
      ]
    }
  }
})();

