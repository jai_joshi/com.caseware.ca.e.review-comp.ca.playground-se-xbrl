(function() {
  wpw.tax.create.diagnostics('t2s365', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0050530', common.prereq(common.and(
        common.requireFiled('T2S365'),
        common.check(['t2s5.595'], 'isNonZero')),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne([row[0], tools.field('t2s5.850')], 'isNonZero');
          });
        }));

    diagUtils.diagnostic('0050532', common.prereq(common.requireFiled('T2S365'),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([tools.field('t2s5.850'), row[0]], 'isNonZero') > 1)
              return tools.requireOne([row[0], tools.field('t2s5.850')], 'isEmpty');
            else return true;
          });
        }));

    diagUtils.diagnostic('3650540', common.prereq(common.requireFiled('T2S365'),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[0], row[1]], 'isNonZero') == 1)
              return tools.requireAll([row[0], row[1]], 'isNonZero');
            else return true;
          });
        }));

  });
})();

