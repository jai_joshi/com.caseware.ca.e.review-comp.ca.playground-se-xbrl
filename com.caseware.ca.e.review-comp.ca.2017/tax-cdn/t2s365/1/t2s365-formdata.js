(function() {
  'use strict';

  wpw.tax.global.formData.t2s365 = {
    formInfo: {
      abbreviation: 'T2S365',
      title: 'Additional Certificate Numbers for the New Brunswick Film Tax Credit',
      schedule: 'Schedule 365',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      category: 'New Brunswick Forms',
      formFooterNum: 'T2 SCH 365 E (99)'
    },
    'sections': [
      {
        hideFieldset: true,
        'rows': [
          {
            label: 'For use by a corporation that has more than one certificate number for the ' +
            'New Brunswick film tax credit. ',
            labelClass: 'fullLength tabbed2'
          },
          {labelClass: 'fullLength'},
          {'type': 'table', 'num': '900'},
          {
            label: 'Enter this amount on line 595 in Part 2 of Schedule 5.',
            labelClass: 'fullLength tabbed'
          }
        ]
      }
    ]
  };
})();

