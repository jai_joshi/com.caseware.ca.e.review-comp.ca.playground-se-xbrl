(function() {

  wpw.tax.create.formData('onlineMail', {
    formInfo: {
      abbreviation: 'onlineMail',
      title: 'Online Mail Terms of Use ',
      highlightFieldsets: true,
      showCorpInfo: true,
      category: 'Client Communication'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: 'Reference only. Do not submit this form to the CRA',
            labelClass: 'bold center'
          }
        ]
      },
      {
        header: 'Terms of use',
        rows: [
          {
            labelClass: 'fullLength'
          },
          {
            label: 'I authorize the Canada Revenue Agency (CRA) to send emails to the e-mail address(es), that ' +
            'I have supplied to the CRA, notifying me that there is a notice or other correspondence available' +
            ' in My Business Account requiring my immediate attention.' +
            ' All notices and correspondence available in My Business Account will be presumed to have been received' +
            ' on the date that the email is sent. I understand and agree that all notices and other correspondence ' +
            'eligible for electronic delivery will no longer be paper printed and mailed.',
            labelClass: 'fullLength tabbed'
          },
          {
            labelClass: 'fullLength'
          },
          {
            label: 'Email address',
            labelClass: 'tabbed fullLength'
          },
          {
            type: 'infoField',
            num: '100'
          },
          {
            labelClass: 'fullLength'
          },
          {
            label: 'Do you agree with terms of use?',
            labelClass: 'tabbed',
            labelWidth: '25%',
            type: 'infoField',
            inputType: 'radio',
            num: '101',
            init: '2'
          }
        ]
      },
      {
        header: 'Manage online mail – Privacy Notice',
        rows: [
          {
            labelClass: 'fullLength'
          },
          {
            label: 'Personal information is collected under the authority of subsection 220(1) of the <i>T2 Corporation Income Tax Return</i>' +
            ' and subsection 275(1) of the Excise Tax Act and is used for the purpose of sending notices electronically. ' +
            'Information is described in personal information banks CRA PPU 047 Business Returns and Payment Processing ' +
            'and CRA PPU 005 Individual Returns and Payment Processing in the Canada Revenue Agency (CRA) ' +
            'chapter of Info Source. Personal information is protected under the ' +
            '<i>Privacy Act</i> '.link('http://laws-lois.justice.gc.ca/eng/acts/P-21/') + 'and individuals ' +
            'have a right to access, correct, or notate their personal information and to have their personal ' +
            'information protected. More details about requests for personal information ' +
            'at the CRA and the CRA’s Info Source chapter can be found at ' + 'www.cra.gc.ca/atip.'.link('http://www.cra-arc.gc.ca/atip/'),
            labelClass: 'fullLength tabbed'
          }
        ]
      }
    ]
  });
})();
