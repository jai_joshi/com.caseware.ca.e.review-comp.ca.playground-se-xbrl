(function() {
  'use strict';

  wpw.tax.global.formData.t2s10 = {
    formInfo: {
      abbreviation: 'T2S10',
      title: 'Cumulative Eligible Capital Deduction',
      schedule: 'Schedule 10',
      code: 'Code 1602',
      formFooterNum: 'T2 SCH 10 E (18)',
      headerImage: 'canada-federal',
      neededRepeatForms: ['t2s10w'],
      showCorpInfo: true,
      category: 'Federal Tax Forms',
      description: [
        {
          'type': 'list',
          'items': [
            'Use this schedule if you have or had eligible capital property in the tax year.',
            '<b>Do not</b> complete this schedule for tax years that start after December 31, 2016. Refer to Schedule 8, Capital Cost Allowance (CCA).',
            'Effective January 1, 2017, the rules governing eligible capital property (ECP) are replaced by the new capital cost allowance (CCA) class 14.1. ' +
            'Property that would be eligible capital property prior to January 1, 2017 will be depreciable property in new class 14.1 after December 31, 2016. ',
            'Prior to 2017, a separate cumulative eligible capital (CEC) account must be kept for each business. Effective January 1, 2017, subsection 1101(1) of ' +
            'the <i>Income Tax Regulations</i> provides for a separate Class 14.1 in respect of each business of the taxpayer.',
            'All legislative references in this form are to the federal Income Tax Act and Income Tax Regulations.',
            'Do not complete this schedule for tax years that start after December 31, 2016.'
          ]
        }
      ]
    },
    sections: [
      {
        'header': 'Part 1 – Calculation of CEC balance and current year deduction (if applicable)',
        'rows': [
          {
            label: '• For tax years that end on or before December 31, 2016 complete up to amount K and either of the following:',
            'labelClass': 'fullLength'
          },
          {
            label: '– If amount K is positive, calculate the CEC deduction for the tax year and the closing balance up to amount M.',
            'labelClass': 'fullLength tabbed'
          },
          {
            label: '– If amount K is negative, complete Part 2 to calculate the amount to be included in income.',
            'labelClass': 'fullLength tabbed'
          },
          {
            label: '• For tax years that end on or after January 1, 2017 and includes December 31, 2016, complete up to amount K and either of the following:',
            'labelClass': 'fullLength'
          },
          {
            label: '– If amount K is positive, complete Part 3 to determine the undepreciated capital cost for the new Class 14.1.',
            'labelClass': 'fullLength tabbed'
          },
          {
            label: '– If amount K is negative, complete Part 2 up to amount S and then complete parts 3 and 4.',
            'labelClass': 'fullLength tabbed'
          },
          {
            label: '• For tax years that end on January 1, 2017, complete Part 3 to determine the undepreciated capital cost for the new CCA class 14.1.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Cumulative eligible capital - Balance at the end of the preceding taxation year </b> (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '200',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '200'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Cost of eligible capital property acquired during the taxation year before January 1, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '222',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '222'
                }
              },
              null,
              null,
              null
            ]
          },
          {
            'label': 'Other adjustment before January 1, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'underline': 'single',
                'input': {
                  'num': '226',

                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '226'
                }
              },
              null,
              null,
              null
            ]
          },
          {
            'type': 'table',
            'num': 'di_table_1'
          },
          {
            'type': 'table',
            'num': 'di_table_2'
          },
          {
            'label': 'Subtotal(amount B <b>minus</b> amount C (if negative, enter "0")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '104'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '105'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Amount transferred on amalgamation or wind-up of subsidiary prior to January 1, 2017',
            'labelClass': '4',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'labelCellClass': 'tabbed',
            'columns': [
              {
                'input': {
                  'num': '224',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '224'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Subtotal (<b>add</b> amounts A, D, and E)&nbsp',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '230',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '230'
                }
              }
            ],
            'indicator': 'F'
          },
          {
            'label': 'Proceeds of sale (less outlays and expenses not otherwise deductible) from ' +
            'the disposition of all eligible capital property during the tax year before January 1, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '242',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '242'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G'
                }
              }, null
            ]
          },
          {
            'label': 'The gross amount of a reduction before January 1, 2017 in respect of a forgiven debt\n' +
            'obligation as provided for in subsection 80(7)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '244',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '244'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'H'
                }
              }, null
            ]
          },
          {
            'label': 'Other adjustments before January 1, 2017',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '246',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '246'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'I'
                }
              }, null
            ]
          },
          {
            'type': 'table',
            'num': 'di_table_3'
          },
          {
            'label': '<b> Cumulative eligible capital balance </b> (amount F <b>minus</b> amount J)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '107'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': '(if amount K is negative, enter "0" at line M and proceed to Part 2)'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Cumulative eligible capital for a property no longer owned after ceasing to carry on that business before January 1, 2017 ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '249',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '249'
                }
              },
              null
            ]
          },
          {
            'label': 'Amount K',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '108'
                }
              },
              null,
              null, null
            ]
          },
          {
            'label': 'amount from line 249',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '109'
                }
              },
              null,
              null,
              null
            ]
          },
          {
            'type': 'table',
            'num': 'di_table_4'
          },
          {
            'label': '<b>Subtotal</b> (line 249<b> plus</b> line 250)<br>' +
            '(enter at line 405 of the 2016 version of Schedule 1, Net Income (Loss) for Income Tax Purposes)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '112'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '113'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b> Cumulative eligible capital - Closing balance </b> (amount K <b>minus</b> amount L)(if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '300',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '300'
                }
              }
            ],
            'indicator': 'M'
          },
          {
            'label': '* You can claim any amount up to the maximum deduction of 7%. The deduction may not be more than the maximum amount prorated by the number\n' +
            'of days in the taxation year divided by 365. If your tax year ends after December 31, 2016, and you are using this schedule to calculate your CEC\n' +
            'balance as at the beginning of January 1, 2017, you are not entitled to a current year deduction. The deduction may not be more than the maximum\n' +
            'amount prorated by the number of days in the taxation year divided by 365.',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 – Deemed capital gain or amount to be included in income arising from disposition',
        'rows': [
          {
            'label': 'Complete this part only if amount K in Part 1 is negative',
            'labelClass': 'bold'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'For all dispositions of eligible capital property before January 1, 2017.',
            'labelClass': 'bold'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount from line K (show as positive amount)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '199'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total of cumulative eligible capital (CEC) deductions from income for taxation years beginning after June 30, 1988',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '400',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '400'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '1'
                }
              }
            ]
          },
          {
            'label': 'Total of all amounts which reduced CEC in the current or previous years under subsection 80(7) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '401',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '401'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '2'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total of CEC deductions claimed for taxation years beginning before July 1, 1988',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '402',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '402'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '3'
                }
              },
              null
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Negative balances in the CEC account that were included in income for tax years beginning before July 1, 1988',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '408',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '408'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '4'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Subtotal</b> (Amount 3 <b>minus</b> amount 4) (if negative, enter "0")',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '201'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '202'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '5'
                }
              }
            ]
          },
          {
            'label': 'Total of lines 1, 2, and 5',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '203'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '6'
                }
              }
            ]
          },
          {
            'label': 'Amounts included in income under paragraph 14(1)(b), as that paragraph applied to taxation years ending after June 30, 1988 and before February 28, 2000, to the extent that it is for an amount described at line 400',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '204'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '7'
                }
              },
              null
            ]
          },
          {
            forceBreakAfter:true,
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amounts at line T from Schedule 10 of previous taxation years ending after February 27, 2000',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '205'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '8'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Subtotal</b> (line 7 <b>plus</b> line 8)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '409',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '409'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '206',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '9'
                }
              }
            ]
          },
          {
            'label': 'Amount 6 <b>minus</b> amount 9 (if negative, enter "0") ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '207'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '208'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'label': 'Amount N <b>minus</b> amount O (if negative, enter "0") ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '209'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'type': 'table',
            'num': 'di_table_5'
          },
          {
            'label': 'Amount P <b>minus</b> amount Q (if negative, enter "0")',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '212'
                }
              }
            ],
            'indicator': 'R'
          },
          {
            'type': 'table',
            'num': '500'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: 'For a tax year that ends on or after January 1, 2017, and includes December 31, 2016, proceed to parts 3 and 4.',
            labelClass: 'bold fullLength'
          },
          {
            label: 'For tax years that end before January 1, 2017, complete amounts T and U.',
            labelClass: 'bold fullLength'
          },
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Amount N or amount O, whichever is less',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '215'
                }
              }
            ],
            'indicator': 'T'
          },
          {
            'label': '<b> Amount to be included in income </b> (amount S <b>plus</b> amount T)(enter this amount on line 108 of Schedule 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '410',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '410'
                }
              }
            ],
            'indicator': 'U'
          }
        ]
      },
      {
        'header': 'Table of CEC deductions from income of Previous years',
        'rows': [
          {
            'type': 'table',
            'num': '099'
          },
          {
            'type': 'table',
            'num': '100'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* The total CEC deductions from income is reported on line 400 in Part 2 of this schedule when the amount on line K in Part 1 is negative.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** The total of deductions with respect to incorporeal capital property is reported on line 41 in Part 3 of form CO-130.B when the amount on line 30 in Part 2 of this form is negative.',
            'labelClass': 'fullLength'
          },
          {
            'label': '*** The total CEC deductions from income is reported on line 42 in "Area B- Amount to be included in Income Arising from Disposition" of the ATI Schedule 14 when the amount on line 22 in "Area A - Current Year Deduction and Carry-Forward" of this schedule is negative',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'forceBreakAfter': true,
        'header': 'Part 3 – Undepreciated capital cost of Class 14.1 at the beginning of January 1, 2017',
        'rows': [
          {
            'label': 'The cumulative eligible capital (CEC) regime will stop after the 2016 tax year. ' +
            'Starting January 1, 2017, the CEC will be replaced with the capital cost allowance rules.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Transitional rules',
            'labelClass': 'bold'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'CEC pool balances will be calculated and transferred to the new CCA class as of January 1, ' +
            '2017; the opening balance of the new CCA class would be equal to the balance as of December 31, ' +
            '2016, in the existing CEC pool.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'CEC balance at the beginning of January 1, 2017',
            labelCellClass: 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '420',
                  'validate': {
                    'matches': '^[.\\d]{1,13}$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '420'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'V'
                }
              }
            ]
          },
          {
            label: '(Enter amount K if positive. If amount K is negative, enter "0")',
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Total CEC deductions applied in prior years that have not been recaptured</b>' +
            '<br>(amount O from Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '425',
                  'validate': {
                    'matches': '^[.\\d]{1,13}$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '425'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'W'
                }
              }
            ]
          },
          {
            'label': '<b>Subtotal</b> (Amount V <b>plus</b> amount W)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '4251'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '4252'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            indicator: 'X'
          },
          {'labelClass': 'fullLength'},
          {
            'label': '<b>Negative CEC balance</b> (amount N from Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4253',
                  'validate': {
                    'matches': '^[.\\d]{1,13}$'
                  }
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'Y'
                }
              }
            ]
          },
          {
            type: 'table', num: '600'
          },
          {
            'label': 'Amount Q (from part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '431',
                  'validate': {
                    'matches': '^[.\\d]{1,13}$'
                  }
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '11'
                }
              },
              null
            ]
          },
          {
            'label': '<b>Subtotal</b> (Amount 10 <b>plus</b> amount 11)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '4304'
                }
              },
              {
                'underline': 'single',
                'input': {
                  'num': '4305'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'Z'
                }
              },
            ],
          },
          {
            'label': '<b>Subtotal</b> (Amount Y <b>minus</b> amount Z)(if negative, enter ”0”)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '4301'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '4302'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            indicator: 'AA'
          },
          {
            'label': '<b>Subtotal</b> (Amount X <b>minus</b> amount AA)(if negative, enter ”0”)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '4303'
                }
              }
            ],
            indicator: 'BB'
          },
          {
            'label': '<b>Deemed capital cost of former ECP </b>(amount BB multiplied by 4/3)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '435'
                },
                'padding': {
                  'type': 'tn',
                  'data': '435'
                }
              }
            ],
            indicator: 'CC'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Total deemed capital cost of former ECP (amount CC)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4351',
                  'validate': {
                    'matches': '^[.\\d]{1,13}$'
                  }
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'DD'
                }
              }
            ]
          },
          {
            'label': 'Amount AA',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'single',
                'input': {
                  'num': '4352',
                  'validate': {
                    'matches': '^[.\\d]{1,13}$'
                  }
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'EE'
                }
              }
            ]
          },
          {
            'label': '<b>Subtotal</b> (Amount DD <b>plus</b> amount EE)',
            labelCellClass: 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '4353'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '4354'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            indicator: 'FF'
          },
          {
            'label': 'Amount V',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'single',
                'input': {
                  'num': '4355'
                }
              }
            ],
            indicator: 'GG'
          },
          {
            'label': 'Amount deemed to have been allowed under paragraph 20(1)(a) for class 14.1',
            labelCellClass: 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '440'
                },
                'padding': {
                  'type': 'tn',
                  'data': '440'
                }
              }
            ],
            indicator: 'HH'
          },
          {
            label: '(amount FF <b>minus</b> amount GG) (if negative, enter ”0”)',
            labelClass: 'fullLength'
          },
          {
            'label': '<b>Undepreciated capital cost to Schedule 8</b> (amount DD <b>minus</b> amount HH)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'double',
                'input': {
                  'num': '445'
                },
                'padding': {
                  'type': 'tn',
                  'data': '445'
                }
              }
            ],
            indicator: 'II'
          },
          {
            label: 'If amount II is positive, enter the amount on line 201 of Schedule 8 in respect of Class 14.1.',
            labelClass: 'fullLength'
          },
          {
            label: ' If amount II is negative, enter the amount on line 205 of Schedule 8 in respect of Class 14.1.',
            labelClass: 'fullLength'
          },
          // {'labelClass': 'fullLength'},
          // {
          //   'type': 'infoField',
          //   'inputType': 'link',
          //   'label': '<b>ECP should be updated using an asset class 14.1 as of January 1, 2017</b><br><br> Go to Schedule 8 and update Class 14.1',
          //   'labelClass': 'fullLength ',
          //   'labelWidth': '60%',
          //   'note': 'Please Click here to go to Schedule 8',
          //   'formId': 't2s8',
          //   'showWhen': {
          //     'fieldId': '101'
          //   }
          // },
          {'labelClass': 'fullLength'},
          {
            label: 'Note',
            labelClass: 'tabbed bold'
          },
          {
            label: 'For disposition of properties of the new class 14.1 acquired before January 1, 2017 and ' +
            'disposed after December 31, 2016, refer to subsection 13(38)(b) to determine the cost of each intangible ' +
            'property and establish the resulting cost of the goodwill. ',
            labelClass: 'tabbed fullLength'
          }
        ]
      },
      {
        'header': 'Part 4 – Transitional rules under paragraph 13(38)(d)',
        'rows': [
          {
            'label': 'Only complete the following if your tax year ends on or after January 1, 2017 and ' +
            'includes December 31, 2016 ',
            'labelClass': 'fullLength bold'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'There are 2 elections available on this page for the amount calculated at Amount S in Part 2 of this schedule :',
            'labelClass': 'fullLength'
          },
          {
            'label': '• Subparagraph 13(38)(d)(iv) election, to defer the deemed capital gain or income ' +
            'inclusion, and have the amounts reported on schedule 8 at the end of the tax year, (see line 101), and',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• Subparagraph 13(38)(d)(iii) to report an income inclusion instead of a capital gain, ' +
            'and report the income on schedule 1, (see line 102).',
            'labelClass': 'fullLength tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Election under 13(38)(d)(iv)',
            'labelClass': 'bold'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Is the corporation electing under subparagraph 13(38)(d)(iv) to defer the deemed capital gain or income inclusion?',
            'type': 'infoField',
            'inputType': 'radio',
            tn: '101',
            'num': '101',
            'canClear': true
          },
          {
            'label': 'You can only elect during the tax year and after December 31, 2016, if you acquired ' +
            'property included in class 14.1 or are deemed by subsection 13(35) to acquire goodwill in respect ' +
            'of the business. '
          },
          {
            label: 'If you answer Yes at line 101, complete amounts JJ to LL'
          },
          {
            label: 'If you answer No at line 101, complete amounts 10 thru 12 and proceed to line 102.'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Capital cost of goodwill or Class 14.1 property acquired during the tax year and after December 31, 2016',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '450',
                  'validate': {
                    'matches': '^[.\\d]{1,13}$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '450'
                }
              }
            ],
            indicator: 'JJ'
          },
          {
            label: '(Including goodwill)'
          },
          {
            'label': '1/2 of amount JJ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '455'
                },
                padding: {
                  type: 'tn',
                  data: '455'
                }
              },
              {
                padding: {
                  type: 'text',
                  data: '12'
                }
              },
              null
            ]
          },
          {
            'label': 'Amount S (from part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'single',
                'input': {
                  'num': '4551'
                }
              },
              {
                padding: {
                  type: 'text',
                  data: '13'
                }
              },
              null
            ]
          },
          {type: 'table', num: '700'},
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Reduced capital cost of property, goodwill or Class 14.1 property acquired under clause 13(38)(d)(iv)(B)',
            labelCellClass: 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'double',
                'input': {
                  'num': '470',
                  'validate': {
                    'matches': '^[.\\d]{1,13}$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '470'
                }
              }
            ],
            indicator: 'LL'
          },
          {
            label: '(Amount JJ <b>minus</b> amount KK) (enter amount LL on line 203 of Schedule 8 and proceed to line 102). ',
            labelClass: 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: 'Election under 13(38)(d)(iii)',
            labelClass: 'fullLength bold'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Is the corporation electing subparagraph 13(38)(d)(iii) to report an income inclusion ' +
            'instead of a capital gain?',
            'type': 'infoField',
            'inputType': 'radio',
            tn: '102',
            'num': '102',
            'canClear': true
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: 'If you answer <b>yes</b> at line 102, complete amounts MM to OO.',
            labelClass: 'fullLength'
          },
          {
            label: 'If you answer <b>no</b> at line 102, then a capital gain will need to be reported on Schedule 6,' +
            '<i> Summary of Dispositions of Capital Property</i>, complete amounts 15 to PP.',
            labelClass: 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount S (from part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4701',
                  'validate': {
                    'matches': '^[.\\d]{1,13}$'
                  }
                }
              }
            ],
            indicator: 'MM'
          },
          {
            'label': 'Amount 14 (if applicable)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'single',
                'input': {
                  'num': '4702',
                  'validate': {
                    'matches': '^[.\\d]{1,13}$'
                  }
                }
              }
            ],
            indicator: 'NN'
          },
          {
            'label': '<b>Income inclusion under subparagraph 13(38)(d)(iii)</b> (amount MM <b>minus</b> amount NN)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'double',
                'input': {
                  'num': '475',
                  'validate': {
                    'matches': '^[.\\d]{1,13}$'
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '475'
                }
              }
            ],
            indicator: 'OO'
          },
          {
            label: '(enter amount OO on line 108 of Schedule 1)(if negative, enter ”0”).',
            labelClass: 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            label: 'Proceeds of disposition',
            labelClass: 'bold'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Amount S (from part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '4751'
                }
              },
              {
                padding: {
                  type: 'text',
                  data: '15'
                }
              },
              null
            ]
          },
          {
            'label': 'Amount 14 (if applicable)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                underline: 'single',
                'input': {
                  'num': '4752'
                }
              },
              {
                padding: {
                  type: 'text',
                  data: '16'
                }
              },
              null
            ]
          },
          {type: 'table', num: '800'},
          {
            label: '(amount 15 <b>minus</b> amount 16)(if negative, enter ”0”).',
            labelClass: 'fullLength'
          },
          {'labelClass': 'fullLength'},
          {
            label: 'Enter amount PP on line 420 in Part 4 of Schedule 6',
            'labelClass': 'fullLength'
          }
        ]
      }
    ]
  };
})();
