(function () {

  function part2CalcsUntilS(calcUtils) {
    // Part 2 calculations until S //
    calcUtils.multiply('199', ['107'], -1);
    calcUtils.field('400').assign(calcUtils.field('100').total(2).get());
    calcUtils.equals('400', '1580');
    calcUtils.subtract('201', '402', '408');
    calcUtils.equals('202', '201');
    calcUtils.sumBucketValues('203', ['400', '401', '202']);
    calcUtils.sumBucketValues('409', ['204', '205']);
    calcUtils.equals('206', '409');
    calcUtils.subtract('207', '203', '206');
    calcUtils.equals('208', '207');
    calcUtils.subtract('209', '199', '208');
    calcUtils.equals('210', '202');
    calcUtils.subtract('212', '209', '211');
    calcUtils.equals('213', '212');
    calcUtils.field('214').assign(calcUtils.field('213').get() * calcUtils.field('213Mult-0').get() / calcUtils.field('213Mult-1').get());
  }

  function part3Calcs(calcUtils) {
    var field = calcUtils.field;
    field('420').assign(Math.max(field('300').get(), field('107').get()));
    field('425').assign(field('208').get());
    field('4251').assign(field('420').get() + field('425').get());
    field('4252').assign(field('4251').get());
    field('4253').assign(field('199').get());
    field('4254').assign(field('214').get());

    field('430').assign(field('4254').get() * field('430Mult-0').get() / field('430Mult-1').get());
    field('431').assign(field('211').get());
    field('4304').assign(field('430').get() + field('431').get());
    field('4305').assign(field('4304').get());
    field('4301').assign(Math.max(field('4253').get() - field('4305').get(), 0));
    field('4302').assign(field('4301').get());
    field('4303').assign(Math.max(field('4252').get() - field('4302').get(), 0));
    field('435').assign(field('4303').get() * 4 / 3);
    field('4351').assign(field('435').get());
    field('4352').assign(field('4302').get());
    field('4353').assign(field('4351').get() + field('4352').get());
    field('4354').assign(field('4353').get());
    field('4355').assign(field('420').get());
    field('440').assign(Math.max(field('4354').get() - field('4355').get(), 0));
    field('445').assign(field('4351').get() - field('440').get());
  }

  wpw.tax.create.calcBlocks('t2s10', function (calcUtils) {
    calcUtils.calc(function (calcUtils) {
      var num = '1010';
      var num2 = '1020';
      var midnum = '101Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function (calcUtils) {
      var num = '228';
      var num2 = '103';
      var midnum = '228Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function (calcUtils) {
      var num = '106';
      var num2 = '248';
      var midnum = '106Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function (calcUtils) {
      var num = '110';
      var num2 = '250';
      var midnum = '110Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });
    calcUtils.calc(function (calcUtils) {
      var num = '210';
      var num2 = '211';
      var midnum = '210Mult';
      var product = calcUtils.field(num).get() * calcUtils.field(midnum).get() / 100;
      calcUtils.field(num2).assign(product);
    });

    calcUtils.calc(function (calcUtils, field, form) {
      calcUtils.getGlobalValue('provinces', 'cp', 'prov_residence');
      // update from rate table
      calcUtils.getGlobalValue('101Mult', 'ratesFed', '245');
      calcUtils.getGlobalValue('228Mult', 'ratesFed', '246');
      calcUtils.getGlobalValue('106Mult', 'ratesFed', '247');
      calcUtils.getGlobalValue('210Mult', 'ratesFed', '248');
      calcUtils.getGlobalValue('213Mult-0', 'ratesFed', '249-0');
      calcUtils.getGlobalValue('213Mult-1', 'ratesFed', '249-1');
      calcUtils.getGlobalValue('110Mult', 'ratesFed', '241');
      field('430Mult-0').assign(3);
      field('430Mult-1').assign(2);
      field('460Mult').assign(2);
      field('480Mult').assign(2);
      //to update table of CEC deductions for AB & QC
      var jurisdiction = (field('CP.750').get());
      if (jurisdiction == 'AB' || (jurisdiction == 'MJ' && field('T2S5.019').get())){
        field('100').getRows().forEach(function(row) {
          row[6].assign(row[2].get());
        });
      }
      if (jurisdiction == 'QC' || (jurisdiction == 'MJ' && field('T2S5.011').get())){
        field('100').getRows().forEach(function(row) {
          row[8].assign(row[2].get());
        });
      }
      var valueArray = [200, 222, 226, 228, 224, 242, 244, 246, 249];
      var sum = new Array(valueArray.length);
      calcUtils.calcOnFormChange('t2s10w');
      wpw.tax.actions.getFilteredRepeatIds('t2s10w').forEach(function (formId) {
        valueArray.forEach(function (fieldId, index) {
          sum[index] = (sum[index] || 0) + form(formId).field(fieldId).get();
        });
      });

      valueArray.forEach(function (fieldId, index) {
        field(fieldId).assign(sum[index]);
        field(fieldId).source(form('T2S10W').field(fieldId));
      });

      var endDate2016 = wpw.tax.date(2016, 12, 31);
      var begDate2017 = wpw.tax.date(2017, 1, 1);
      var isStartAfter2017 = calcUtils.dateCompare.lessThan(endDate2016, field('cp.tax_start').get());
      var isEndBefore2017 = calcUtils.dateCompare.lessThan(field('cp.tax_end').get(), begDate2017);

      //to avoid part 4 calculation being provoked upon import since field 101 and 102 get imported
      if (wpw.tax.isTaxprepImporting && isEndBefore2017) {
        field('101').assign();
        field('102').assign()
      }
      if (isStartAfter2017) {
        //For tax years that starts after December 31, 2016, do not complete schedule S10. Refer to schedule 8
        //todo : disabled S10 or diagnostic
      } else {
        //calculate up to amount K
        // Part 1 calculations //
        calcUtils.sumBucketValues('1010', ['222', '226']);
        field('104').assign(Math.max(field('1020').get() - field('103').get(), 0));
        calcUtils.equals('105', '104');
        calcUtils.sumBucketValues('230', ['200', '105', '224']);
        calcUtils.sumBucketValues('106', ['242', '244', '246']);
        field('107').assign(field('230').get() - field('248').get());
        var isKPositive = calcUtils.field('107').get() >= 0;

        var part1 = [108, 110, 250, 112, 113, 300];
        var part2 = [400, 199, 201, 202, 203, 409, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 410,
          401, 402, 408, 204, 205];
        var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();
        var num250BeforeProrated;
        if (isEndBefore2017) {
          // For tax years that end on or before December 31, 2016 complete up to amount K and either of the following:
          if (isKPositive) {
            //– If amount K is positive, calculate the CEC deduction for the tax year and the closing balance.
            //part 1 calcs`
            calcUtils.equals('108', '107');
            calcUtils.equals('109', '249');
            calcUtils.subtract('110', '108', '109');
            num250BeforeProrated = (field('110').get() * field('110Mult').get() / 100);
            field('250').assign(num250BeforeProrated * Math.min(daysFiscalPeriod / 365, 1));
            calcUtils.sumBucketValues('112', ['249', '250']);
            calcUtils.equals('113', '112');
            calcUtils.subtract('300', '107', '113');
            calcUtils.removeValue(part2, true);
          } else {
            //– If amount K is negative, complete Part 2 to calculate the amount to be included in income.
            calcUtils.removeValue(part1, true);
            //calculation until S in part 2
            part2CalcsUntilS(calcUtils);
            calcUtils.min('215', ['199', '208']);
            calcUtils.sumBucketValues('410', ['214', '215']);
          }
        } else {
          //• For tax years that end on or after January 1, 2017 and includes December 31, 2016, complete up to amount K and either of the following:
          if (isKPositive) {
            //– If amount K is positive, complete Part 3 to determine the undepreciated Capital Cost for the new CCA class 14.1.
            //part 3 calculation
            part3Calcs(calcUtils)
          } else {
            //– If amount K is negative complete Part 2 up to amount S, and then complete parts 3 and 4.
            part2CalcsUntilS(calcUtils);
            part3Calcs(calcUtils);
            //part 4 calculation
            var is101Selected = (field('101').get() == '1');
            var is102Selected = (field('102').get() == '1');
            if (is101Selected) {
              //If you answer Yes at line 101, complete amounts JJ to LL
              field('450').disabled(false);
              field('4551').assign(field('214').get());
              field('455').assign(field('450').get() / 2);
              field('460').assign(Math.min(field('455').get(), field('4551').get()));
              field('465').assign(field('460').get() * field('460Mult').get());
              field('470').assign(Math.max(field('450').get() - field('465').get(), 0));
            } else {
              //If you answer No at line 101, complete amounts 10 thru 12 and proceed to line 102.
              calcUtils.removeValue(['450', '455', '465', '470'], true);
              field('460').assign(Math.min(field('455').get(), field('4551').get()));
            }

            if (is102Selected) {
              //If you answer yes at line 102, complete amounts MM to OO
              //remove 13 to 15
              calcUtils.removeValue(['4751', '4752']);
              field('4701').assign(field('214').get());
              field('4702').assign(field('460').get());
            } else {
              //If you answer no at line 102, complete amounts 13 to PP
              //remove MM to OO
              calcUtils.removeValue(['4701', '4702']);
              field('4751').assign(field('214').get());
              field('4752').assign(field('460').get());
            }
            field('475').assign(Math.max(field('4701').get() - field('4702').get(), 0));
            field('4753').assign(Math.max(field('4751').get() - field('4752').get(), 0));
            field('480').assign(field('4753').get() * field('480Mult').get());
          }
        }
      }
    });

  })
})();
