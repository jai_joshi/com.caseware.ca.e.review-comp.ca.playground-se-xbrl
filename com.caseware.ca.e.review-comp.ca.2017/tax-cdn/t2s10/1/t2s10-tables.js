(function() {
  wpw.tax.global.tableCalculations.t2s10 = {
    'di_table_1': {
      'num': 'di_table_1',
      'type': 'table',
      'columns': [
        {
          'type': 'none',
          cellClass: 'alignRight'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width',
          'disabled': true
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width',
          'type': 'none'
        },
        {
          'width': '26.6px',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': '<b>Subtotal</b> (line 222 <b>plus</b> line 226)'
          },
          '2': {
            'num': '1010'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '101Mult'
          },
          '5': {
            'label': '%='
          },
          '6': {
            'num': '1020'
          },
          '7': {
            'label': 'B'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_2': {
      'num': 'di_table_2',
      'type': 'table',
      'columns': [
        {
          'type': 'none',
          cellClass: 'alignLeft'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width',
          'disabled': true
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width',
          'type': 'none'
        },
        {
          'width': '26.6px',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Non-taxable portion of a non-arm\'s length transferor\'s gain realized on the transfer' +
            ' of an eligible capital property to the corporation after December 20, 2002 and before January 1, 2017',
            'trailingDots': true
          },
          '1': {
            'tn': '228'
          },
          '2': {
            'num': '228',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '228Mult'
          },
          '5': {
            'label': '%='
          },
          '6': {
            'num': '103',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' singleUnderline'
          },
          '7': {
            'label': 'C'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_3': {
      'num': 'di_table_3',
      'type': 'table',
      'columns': [
        {
          'type': 'none',
          cellClass: 'alightRight'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-spacing-width',
          'type': 'none'
        },
        {
            'colClass': 'std-spacing-width',
            'type': 'none'
        },
        {
          'colClass': 'std-spacing-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': '<b>Subtotal (add </b>amounts G, H, and I)'
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'formField': true,
            'num': '106',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' doubleUnderline'
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '106Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '248'
          },
          '7': {
            'num': '248',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' singleUnderline'
          },
          '8': {
            'label': 'J'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_4': {
      'num': 'di_table_4',
      'type': 'table',
      'columns': [
        {
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width',
            'type': 'none'
        },
        {
          'width': '26px',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': '<b>Current year deduction</b> (amount K <b>minus</b> line 249)',
            'trailingDots': true
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '110',
            formField: 'true',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '3': {
            'label': 'x'
          },
          '4': {
            'num': '110Mult',
            'init': undefined
          },
          '5': {
            'label': '%='
          },
          '6': {
            'tn': '250'
          },
          '7': {
            'num': '250',
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '8': {
            'label': '*'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    'di_table_5': {
      'num': 'di_table_5',
      'type': 'table',
      'columns': [
        {
          'type': 'none',
          cellClass: 'alignRight'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'textAlign': 'center',
          'type': 'none'
        },
          {
              'colClass': 'std-spacing-width',
              'type': 'none'
          },
        {
          'colClass': 'half-col-width',
          'textAlign': 'center',
          'disabled': true
        },
        {
          'colClass': 'qtr-col-width',
            'textAlign': 'center',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-spacing-width',
          'type': 'none'
        },
        {
          'colClass': 'std-spacing-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Amount 5'
          },
          '1': {
            'tn': undefined
          },
          '2': {
            'num': '210'
          },
          '3': {
            'label': 'x'
          },
          '5': {
            'num': '210Mult',
            'init': undefined
          },
          '6': {
            'label': '%=&nbsp'
          },
          '7': {
            'num': '211',
            'cellClass': ' singleUnderline'
          },
          '8': {
            'label': 'Q'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    "100": {
      hasTotals: true,
      "infoTable": true,
      "showNumbering": true,
      "maxLoop": 100000,
      "columns": [{
        "width": "140px",
        "type": "date"
      },
        {
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true,
          "total": true,
          "totalNum": "1580",
          "totalMessage": "Total: ",
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "formField": true,
          "disabled": true,
          "total": true,
          "totalNum": "1620",
          "totalMessage": ' ',
          showWhen: {
            fieldId: 'provinces',
            has: {QC: true}
          },
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          showWhen: {
            fieldId: 'provinces',
            has: {QC: true}
          }
        },
        {
          colClass: 'std-input-width',
          "formField": true,
          "disabled": true,
          "total": true,
          "totalNum": "1680",
          "totalMessage": ' ',
          showWhen: {
            fieldId: 'provinces',
            has: {AB: true}
          },
        },
        {
          "type": "none",
          colClass: 'std-padding-width',
          showWhen: {
            fieldId: 'provinces',
            has: {AB: true}
          }
        }]
    },
    500: {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none',
          cellClass: 'alignRight'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'qtr-col-width'
        },
        {
          width: '15px',
          type: 'none'
        },
        {
          colClass: 'qtr-col-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width',

          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        }
      ],
      cells: [
        {
          0: {
            label: 'Amount R'
          },
          2: {
            num: '213'
          },
          3: {
            label: 'x'
          },
          4: {
            num: '213Mult-0'
          },
          5: {label: '/'},
          6: {
            num: '213Mult-1'
          },
          7: {
            label: '='
          },
          8: {
            num: '214'
          },
          9: {
            label: 'S'
          }
        }]
    },
    "099": {
      "fixedRows": true,
      "infoTable": true,
      "columns": [{
        "width": "140px",
        "type": "none"
      },
        {
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "type": "none"
        },
        {
          colClass: 'std-padding-width',
          "type": "none"
        },
        {
          colClass: 'std-input-width',
          "type": "none",
          showWhen: {
            fieldId: 'provinces',
            has: {QC: true}
          }
        },
        {
          colClass: 'std-padding-width',
          "type": "none",
          showWhen: {
            fieldId: 'provinces',
            has: {QC: true}
          }
        },
        {
          colClass: 'std-input-width',
          "type": "none",
          showWhen: {
            fieldId: 'provinces',
            has: {AB: true}
          }
        },
        {
          "type": "none",
          colClass: 'std-padding-width',
          showWhen: {
            fieldId: 'provinces',
            has: {AB: true}
          }
        },
        {
          colClass: 'third-col-width',
          "type": "none"
        }],
      "cells": [{
        "0": {
          "label": "Year of origin",
          "labelClass": "bold center"
        },
        "2": {
          "label": "CEC deduction from income (line 250) *",
          "labelClass": "bold center"
        },
        "4": {
          "label": "Deduction with respect to incorporeal capital property (line 36 of Form CO-130.B) **",
          "labelClass": "bold center"
        },
        "6": {
          "label": "CEC deduction from income (line 24 of the AT1 Schedule 14) ***",
          "labelClass": "bold center"
        }
      }]
    },
    600: {
      infoTable: true,
      fixedRows: true,
      columns: [
        {
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'qtr-col-width'
        },
        {
          colClass: 'std-spacing-width',
          cellClass: 'alignCenter',
          type: 'none'
        },
        {
          colClass: 'qtr-col-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'qtr-col-width',
          type: 'none'
        },
        {
          width: '316px',
          type: 'none'
        }
      ],
      cells: [
        {
          0: {
            label: 'Amount S (from part 2)'
          },
          1: {
            num: '4254',
            formField: true
          },
          2: {
            label: 'x'
          },
          3: {
            num: '430Mult-0'
          },
          4: {label: '/'},
          5: {
            num: '430Mult-1'
          },
          6: {
            label: '='
          },
          7: {
            tn: '430'
          },
          8: {
            num: '430',
            formField: true
          },
          9: {
            label: '10'
          }
        }
      ]
    },
    '700': {
      'columns': [
        {
          'type': 'none',
          cellClass: 'alignRight'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'third-col-width',
          cellClass: 'alignRight',
          'type': 'none'
        },
        {
          'colClass': 'std-spacing-width',
          'type': 'none'
        },
        {
          'colClass': 'qtr-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-spacing-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Lesser of amount 12 and amount 13'
          },
          '1': {
            'tn': '460'
          },
          '2': {
            'num': '460',
            formField: true,
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' doubleUnderline'
          },
          '3': {
            'label': '14'
          },
          '4': {
            'label': 'x'
          },
          '6': {
            'num': '460Mult'
          },
          '7': {
            'label': '='
          },
          '8': {
            'tn': '465'
          },
          '9': {
            'num': '465',
            formField: true,
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            }
          },
          '10': {
            'label': 'KK'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    },
    '800': {
      'columns': [
        {
          'type': 'none',
          cellClass: 'alignLeft'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'third-col-width',
          cellClass: 'alignRight',
          'type': 'none'
        },
        {
          'colClass': 'std-spacing-width',
          'type': 'none'
        },
        {
            colClass: 'qtr-col-width',
          'textAlign': 'center'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-padding-width',
          'type': 'none'
        },
        {
          'colClass': 'std-input-width'
        },
        {
          'colClass': 'std-spacing-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': '<b>Proceeds of disposition under subparagraph 13(38)(d)(ii)</b>'
          },
          '2': {
            'num': '4753',
            formField: true,
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' doubleUnderline'
          },
          '3': {
            'label': '17'
          },
          '4': {
            'label': 'x'
          },
          '6': {
            'num': '480Mult'
          },
          '7': {
            'label': '='
          },
          '8': {
            'tn': '480'
          },
          '9': {
            'num': '480',
            formField: true,
            'validate': {
              'or': [
                {
                  'matches': '^[.\\d]{1,13}$'
                },
                {
                  'check': 'isEmpty'
                }
              ]
            },
            'cellClass': ' doubleUnderline'
          },
          '10': {
            'label': 'PP'
          }
        }
      ],
      'infoTable': true,
      'fixedRows': true
    }
  }
})();
