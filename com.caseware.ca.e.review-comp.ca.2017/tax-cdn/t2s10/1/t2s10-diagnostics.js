(function() {
  wpw.tax.create.diagnostics('t2s10', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('0100001', common.prereq(common.check(['t2j.210'], 'isChecked'), common.requireFiled('T2S10')));

    diagUtils.diagnostic('0100002', common.prereq(common.and(
        common.requireFiled('T2S10'),
        common.check(['t2s1.405'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['200', '222', '224', '226', '228']), 'isNonZero');
        }));

    diagUtils.diagnostic('0100003', common.prereq(common.and(
        common.requireFiled('T2S10'),
        common.check(['t2s1.108'], 'isNonZero')),
        function(tools) {
          return tools.requireOne(tools.list(['242', '244', '246']), 'isNonZero');
        }));

    diagUtils.diagnostic('010.14-1', common.prereq(common.and(
        common.requireFiled('T2S10'),
        function(tools) {
      return wpw.tax.utilities.dateCompare.between({day: 1, month: 1, year: 2017}, tools.field('cp.tax_start').get(), tools.field('cp.tax_end').get());
    }), function(tools) {
      return tools.requireAll(tools.list(['300', '410']), function() {
        return this.isZero() ||
            !wpw.tax.utilities.dateCompare.between({day: 1, month: 1, year: 2017}, tools.field('cp.tax_start').get(), tools.field('cp.tax_end').get());
          })
    }));

    diagUtils.diagnostic('010.100', common.prereq(
        common.requireFiled('T2S10'),
        function(tools) {
          return tools.requireAll([tools.field('100').cell(0,0), tools.field('100').cell(0,2)], 'isNonZero');
        }));
  });
})();
