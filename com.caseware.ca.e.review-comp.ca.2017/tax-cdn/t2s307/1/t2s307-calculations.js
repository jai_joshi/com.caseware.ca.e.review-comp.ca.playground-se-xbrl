(function() {

  wpw.tax.create.calcBlocks('t2s307', function(calcUtils) {
    //Part 1
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.getGlobalValue('302', 'ratesNl', '600');
      calcUtils.getGlobalValue('403', 'ratesNl', '700');
      calcUtils.getGlobalValue('408', 'ratesNl', '800');
      field('101').assign(field('T2J.400').get());
      field('101').source(field('T2J.400'));
      field('102').assign(field('T2J.405').get());
      field('102').source(field('T2J.405'));
      field('103').assign(field('T2J.425').get());
      field('103').source(field('T2J.425'));
      field('104').assign(Math.min(field('101').get(), field('102').get(), field('103').get()));


      // If is multiple jurisdictions

      var taxableIncomeAllocationNL = calcUtils.getTaxableIncomeAllocation('NL');
      var taxableIncomeAllocationXO = calcUtils.getTaxableIncomeAllocation('XO');

      var totalProvincialTI = taxableIncomeAllocationNL.provincialTI + taxableIncomeAllocationXO.provincialTI;
      field('100').assign(totalProvincialTI);
      field('204').assign(taxableIncomeAllocationNL.allProvincesTI);

      field('100').source(taxableIncomeAllocationNL.provincialTISourceField);
      field('204').source(taxableIncomeAllocationNL.allProvincesTISourceField);

      field('202').assign(field('100').get());

      field('502').assign(taxableIncomeAllocationXO.provincialTI);
      field('502').source(taxableIncomeAllocationXO.provincialTISourceField);
      field('504').assign(totalProvincialTI);

      calcUtils.equals('201', '104');
      if (field('204').get() != 0) {
        field('203').assign(field('201').get() *
            field('202').get() /
            field('204').get())
      }
      else {
        field('203').assign(0);
      }
      field('105').assign(Math.max(0, field('100').get() - field('203').get()));
    });

    //Part 2
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.equals('301', '203');
      field('303').assign(field('301').get() * field('302').get() / 100);
      calcUtils.equals('401', '105');
      calcUtils.equals('406', '105');
      field('404').assign(field('405').get() == 0 ? 0 :
          field('401').get() *
          field('402').get() /
          field('405').get() *
          field('403').get() / 100);
      field('409').assign(field('410').get() == 0 ? 0 :
          field('406').get() *
          field('407').get() /
          field('410').get() *
          field('408').get() / 100);

      var janDate = wpw.tax.date(2016, 1, 1);
      var dateComparisons = calcUtils.compareDateAndFiscalPeriod(janDate);
      var daysFiscalPeriod = field('CP.Days_Fiscal_Period').get();

      field('402').assign(dateComparisons.daysBeforeDate);
      field('405').assign(daysFiscalPeriod);
      field('407').assign(dateComparisons.daysAfterDate);
      field('410').assign(daysFiscalPeriod);

      field('220').assign(field('404').get() + field('409').get());
      calcUtils.equals('221', '220');
      field('222').assign(field('303').get() + field('221').get());

      //calcs should run only when jurisdictions are in both NS & XO
      if (field('T2S5.003').get() && field('T2S5.004').get()) {
        calcUtils.equals('501', '222');
        field('503').assign(field('502').get() == 0 ? 0 :
            field('501').get() *
            field('502').get() /
            field('504').get());
      }
      else {
        field('501').assign(0)
      }
      calcUtils.subtract('240', '222', '503');
    });
  });
})();
