(function() {

  function getTableTaxInc(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {type: 'none', colClass: 'std-padding-width'},
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {

          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '2': {num: labelsObj.num[0]},
          '3': {label: 'x'},
          '4': {
            label: labelsObj.label[1],
            labelClass: 'center',
            cellClass: 'singleUnderline'
          },
          '6': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '7': {label: '='},
          '8': {num: labelsObj.num[2]},
          '9': {label: labelsObj.indicator || ''}
        },
        {
          '2': {type: 'none'},
          '4': {
            label: labelsObj.label[2],
            labelClass: 'center fullLength'
          },
          '6': {num: labelsObj.num[3]},
          '8': {type: 'none'}
        }
      ]
    }
  }

  function getTableRate(labelsObj) {
    labelsObj.num = labelsObj.num || [];
    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none',
          cellClass: 'alignLeft'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'small-input-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '1': {num: labelsObj.num[0]},
          '2': {label: ' x ', labelClass: 'center'},
          '3': {num: labelsObj.num[1], decimals: labelsObj.decimals},
          '4': {label: ' %= '},
          '6': {num: labelsObj.num[2]},
          '7': {label: labelsObj.indicator}
        }]
    }
  }

  function getTableProRate(labelsObj) {
    labelsObj.num = labelsObj.num || [];

    return {
      fixedRows: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-input-width',
          type: 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          type: 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-input-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          '0': {label: labelsObj.label[0]},
          '1': {num: labelsObj.num[0]},
          '2': {label: 'x', labelClass: 'center'},
          '3': {
            label: labelsObj.label[1],
            labelClass: 'center', cellClass: 'singleUnderline'
          },
          '5': {
            num: labelsObj.num[1],
            cellClass: 'singleUnderline'
          },
          '6': {label: 'x'},
          '7': {num: labelsObj.num[2], decimals: labelsObj.decimals},
          '8': {label: '%='},
          '9': {num: labelsObj.num[3]},
          '10': {label: labelsObj.indicator}
        },
        {
          '1': {type: 'none'},
          '3': {
            label: labelsObj.label[2],
            labelClass: 'center'
          },
          '5': {num: labelsObj.num[4]},
          '7': {type: 'none'},
          '9': {type: 'none'}
        }
      ]
    }
  }

  wpw.tax.create.tables('t2s307', {

    '900': getTableTaxInc({
      label: ['Amount E', 'Taxable income for Newfoundland and Labrador *', 'Taxable income for all provinces **'],
      indicator: 'F',
      num: ['201', '202', '203', '204']
    }),

    '300': getTableRate({
      label: ['Amount F'],
      indicator: 'H',
      num: ['301', '302', '303']
    }),
    '400': getTableProRate({
      label: ['Amount G', 'Number of days in the tax year before January 1, 2016', 'Number of days in the tax year'],
      indicator: '1',
      decimals: 1,
      num: ['401', '402', '403', '404', '405']
    }),

    '1400': getTableProRate({
      label: ['Amount G', 'Number of days in the tax year after December 31, 2015', 'Number of days in the tax year'],
      indicator: '2',
      decimals: 1,
      num: ['406', '407', '408', '409', '410']
    }),

    '500': getTableTaxInc({
      label: ['Amount J', 'Taxable income for the Newfoundland and Labrador offshore area',
        'Taxable income for Newfoundland and Labrador <b>plus</b>' +
        ' taxable income for the Newfoundland and Labrador offshore area'],
      indicator: 'K',
      num: ['501', '502', '503', '504']
    })

  })
})();
