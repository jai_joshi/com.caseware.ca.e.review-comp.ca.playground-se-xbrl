(function() {

  wpw.tax.create.formData('t2s307', {
    formInfo: {
      abbreviation: 'T2S307',
      title: 'Newfoundland and Labrador Corporation Tax Calculation<br> (2016 and later tax years)',
      schedule: 'Schedule 307',
      headerImage: 'canada-federal',
      formFooterNum: 'T2 SCH 307 E (16) ',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this schedule if your corporation had a permanent establishment ' +
              '(as defined in section 400 of the federal <i>Income Tax Regulations</i>) in ' +
              'Newfoundland and Labrador, and had taxable income earned in the year in Newfoundland ' +
              'and Labrador and its offshore area.'
            },
            {
              label: 'This schedule is a worksheet only. You do not have to file it with your' +
              ' <i>T2 Corporation Income Tax Return</i>.'
            }
          ]
        }
      ],
      category: 'Newfoundland and Labrador Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Calculation of income subject to the lower and higher tax rates for Newfoundland and Labrador and its offshore area',
        'rows': [
          {
            'label': 'Taxable income for Newfoundland and Labrador *',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': '<b>Income eligible for the lower tax rate for Newfoundland and Labrador and its offshore area:</b>'
          },
          {
            'label': 'Amount from line 400 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '101'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 405 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '102'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Amount from line 427 of the T2 return',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '103'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'D'
                }
              }
            ]
          },
          {
            'label': 'Amount B, C, or D, whichever is the least',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '104'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '900'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Income subject to the higher tax rate for Newfoundland and Labrador and its offshore area</b> (amount A <b>minus</b> amount F)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '105'
                }
              }
            ],
            'indicator': 'G'
          },
          {
            'label': 'Enter amount F and/or amount G on the applicable line(s) in Part 2.'
          },
          {
            'label': ' * If the corporation has a permanent establishment only in Newfoundland and Labrador, or in the offshore area of Newfoundland and Labrador, enter the taxable income from line 360 of the T2 return. Otherwise, enter the total of the taxable incomes allocated to both jurisdictions in Newfoundland and Labrador (the province itself and the offshore area) from column F in Part 1 of Schedule 5, Tax Calculation Supplementary - Corporations.',
            'labelClass': 'fullLength'
          },
          {
            'label': ' ** Includes the territories and the offshore jurisdictions for Nova Scotia and Newfoundland and Labrador.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - Calculation of Newfoundland and Labrador tax before credits and of Newfoundland and Labrador offshore tax',
        'rows': [
          {
            'label': '<b>Tax at the lower rate for Newfoundland and Labrador and its offshore area:</b>'
          },
          {
            'type': 'table',
            'num': '300'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Tax at the higher rate for Newfoundland and Labrador and its offshore area:</b>'
          },
          {
            'type': 'table',
            'num': '400'
          },
          {
            'type': 'table',
            'num': '1400'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Tax at the higher rate for Newfoundland and Labrador and its offshore area</b> (amount 1 <b>plus</b> amount 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '220'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '221'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': '<b>Tax for Newfoundland and Labrador and its offshore area</b> (amount H <b>plus</b> amount I)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '222'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Only one jurisdiction</b>'
          },
          {
            'label': 'If the taxable income is allocated only to Newfoundland and Labrador, or to the Newfoundland ' +
            'and Labrador offshore area, and the corporation is not claiming a Newfoundland and Labrador tax credit, ' +
            'enter amount J on line 760 of the T2 return. If the corporation is claiming a credit, enter amount J on ' +
            'line 200 or 205 of Schedule 5, whichever applies. ',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Jurisdictions in both Newfoundland and Labrador and the Newfoundland and Labrador offshore area</b>'
          },
          {
            'label': 'If the corporation has taxable income allocated to both Newfoundland and Labrador and its offshore area, calculate the following:',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '500'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '<b>Newfoundland and Labrador offshore tax</b> - enter amount K on line 205 of Schedule 5.'
          },
          {
            'label': '<b>Newfoundland and Labrador tax before credits</b> (amount J <b>minus</b> amount K)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '240'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': 'Enter amount L on line 200 of Schedule 5.'
          }
        ]
      }
    ]
  });
})();
