(function() {
  wpw.tax.create.diagnostics('t2s430', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var tableObjects = [
      {errorCode: '4300002', tableNum: '1000'}, {errorCode: '4300003', tableNum: '2000'},
      {errorCode: '4300004', tableNum: '3000'}, {errorCode: '4300005', tableNum: '4000'},
      {errorCode: '4300006', tableNum: '5000'}, {errorCode: '4300007', tableNum: '6000'}
    ];

    diagUtils.diagnostic('4300001', common.prereq(common.check(['t2s5.681'], 'isNonZero'), common.requireFiled('T2S430')));

    function checkRowFilled(diagObject) {
      diagUtils.diagnostic(diagObject.errorCode, common.prereq(common.requireFiled('T2S430'),
          function(tools) {
            var table = tools.field(diagObject.tableNum);
            return tools.checkAll(table.getRows(), function(row) {
              if (row[2].isPositive())
                return tools.requireAll([row[0], row[1]], 'isNonZero');
              else return true;
            });
          }));
    }

    for (var i = 0; i < tableObjects.length; i++) {
      checkRowFilled(tableObjects[i]);
    }

    diagUtils.diagnostic('4300008', common.prereq(common.requireFiled('T2S430'),
        function(tools) {
          var table = tools.mergeTables(tools.field('1000'), tools.field('4000'));
          return tools.checkAll(table.getRows(), function(row) {
            if (tools.checkMethod([row[4], row[11]], 'isNonZero'))
              return !!wpw.tax.utilities.flagger()['T2S428'];
            else return true;
          })
        }));

  });
})();
