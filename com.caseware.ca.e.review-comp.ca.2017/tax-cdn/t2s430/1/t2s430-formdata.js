(function() {
  'use strict';

  wpw.tax.global.formData.t2s430 = {
    formInfo: {
      abbreviation: 'T2S430',
      title: 'British Columbia Shipbuilding and Ship Repair Industry Tax Credit',
      schedule: 'Schedule 430',
      showCorpInfo: true,
      code: 'Code 1201',
      formFooterNum: 'T2 SCH 430 E',
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Complete this schedule if your principal business is the construction, repair, ' +
              'or conversion of ships and if, at any time in the tax year after September 30, 2012, and ' +
              'before January 1, 2020:',
              sublist: [
                'you paid salary or wages to an employee enrolled in an eligible apprenticeship program ' +
                'administered through the British Columbia Industry Training Authority (ITA); and',
                'your employee performed services related to the apprenticeship program at a permanent ' +
                'establishment in British Columbia.'
              ]
            },
            {
              label: 'If your tax year includes October 1, 2012, you may also be eligible to claim the ' +
              'British Columbia training tax credit. <br>Complete Schedule 428, British Columbia Training Tax ' +
              'Credit, if during the tax year:',
              sublist: [
                'you paid salary or wages to an employee who completed level 3 or higher of an eligible ' +
                'apprenticeship program before October 1, 2012; or',
                'you paid salary or wages before October 1, 2012, to an employee registered in the first ' +
                '24 months of an eligible non-Red Seal program.'
              ]
            },
            {
              label: 'Complete Schedule 428 before you complete this schedule. The total amount of the basic tax ' +
              'credit claimed under the training tax credit and the shipbuilding and ship repair industry tax ' +
              'credit cannot be more than $5,250 per employee ($7,875 for the basic and enhanced tax credits), ' +
              'per completed level for the 2012 tax year. For more information, see Guide T4012, T2 ' +
              'Corporation Income Tax Guide.'
            },
            {
              label: 'File a completed copy of this schedule with your T2 Corporation Income Tax Return no ' +
              'later than 36 months after the end of the tax year in which you paid the eligible salary and wages.'
            },
            {
              label: 'There are three elements to the shipbuilding and ship repair industry tax credit program:',
              sublist: [
                '1. basic credit for an eligible apprenticeship program (Red Seal and non-Red Seal) (see Part 1);',
                '2. completion credits for an eligible apprenticeship program (Red Seal and non-Red Seal) ' +
                '(see Part 2); and',
                '3. enhanced credits for First Nations individuals and persons with disabilities for an ' +
                'eligible apprenticeship program (Red Seal and non-Red Seal—see Part 3).'
              ]
            },
            {
              label: 'Enter the identification number provided by the ITA. If there is no identification number, ' +
              'enter the social insurance number (SIN) or the name of the employee. Also enter the name of the ' +
              'Red Seal or non-Red Seal program and the salary and wages payable in the period. If you need more ' +
              'space, attach additional schedules.'
            },
            {
              label: '<b>Do not complete Part 1 or Part 2 for an employee enrolled in an eligible apprenticeship ' +
              'program if you are claiming the enhanced tax credit in Part 3 for that employee.</b>'
            },
            {
              label: '<b>Eligible apprenticeship programs and completion requirements</b> are described in ' +
              'the <i>Income Tax Act</i> (British Columbia) and by the <i>Training Tax Credits Regulation</i> ' +
              'and the <i>Industry Training Regulation</i>. <b>Ship</b> is defined in section 126.1 of the ' +
              '<i>Income Tax Act</i> (British Columbia).'
            }
          ]
        }
      ],
      category: 'British Columbia Forms'
    },
    sections: [
      {
        'rows': [
          {
            'label': '<i>Freedom of Information and Protection of Privacy Act</i> (FOIPPA): The personal information on this form is collected for the purpose of administering the <i>Income Tax Act</i> (British Columbia) under the authority of both this Act and section 26 of the FOIPPA. Questions about the collection or use of this information can be directed to the Manager, Intergovernmental Relations, P.O. Box 9444 Stn Prov Govt, Victoria BC V8W 9W8. (Telephone: Victoria at 250-387-3332 or toll free at 1-877-387-3332 and ask to be re-directed). Email: <b>ITBTaxQuestions@gov.bc</b>',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 1 - Basic tax credit (Red Seal and non-Red Seal apprenticeship programs)',
        'rows': [
          {
            'label': 'The basic tax credit is available during an employee\'s first 24 months of an eligible apprenticeship program. An employee does not have to complete level 1 or 2 of an eligible apprenticeship program for an employer to make a claim. To claim this credit, enter on line 110 the salary and wages* payable during the tax year when the employee was still within the first 24 months of the program.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Net of any other government and non-government assistance received, to be received, or that you are entitled to receive, other than the training tax credit, the shipbuilding and ship repair industry tax credit, and the federal investment tax credit on apprenticeship job creation.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** Use Schedule 428 to calculate the basic tax credit for non-Red Seal apprenticeship programs only and for the period before October 1, 2012 only.',
            'labelClass': 'fullLength'
          },
          {
            'label': '*** The total amount of the basic tax credit claimed under the training tax credit and the shipbuilding and ship repair industry tax credit cannot be more than $5,250 per employee for the 2012 tax year.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 2 - Completion tax credits (Red Seal and non-Red Seal apprenticeship programs)',
        'rows': [
          {
            'label': 'Calculation for an employee who has completed level 3 of an eligible apprenticeship program after September 30, 2012',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'To claim this credit, enter on line 210 the salary and wages* payable in the 12 months just before the completion of the level. If the employee\'s completion date is before October 1, 2012, claim the completion tax credit under the British Columbia training tax credit on Schedule 428.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Calculation for an employee who has completed level 4 or higher of an eligible apprenticeship program after September 30, 2012',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'To claim this credit, enter on line 310 the salary and wages* payable in the 12 months just before the completion of the level. If the employee\'s completion date is before October 1, 2012, claim the completion tax credit under the British Columbia training tax credit on Schedule 428.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '3000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Net of any other government and non-government assistance received, to be received, or that you are entitled to receive, other than the training tax credit, the shipbuilding and ship repair industry tax credit, and the federal investment tax credit on apprenticeship job creation.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Enhanced tax credit (Red Seal and non-Red Seal apprenticeship programs)',
        'rows': [
          {
            'label': 'The enhanced training tax credit applies only to the following employees: <br>- eligible First Nations individuals (defined as persons registered as Indians under the <i>Indian Act</i>); or <br>- persons with disabilities (defined as persons eligible, for themselves, for the federal disability amount on their income tax and benefit return).',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Calculation for an employee\'s first 24 months of an eligible apprenticeship program',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'This calculation includes the basic and enhanced tax credits. To claim this credit, enter on line 410 the salary and wages* payable during the tax year when the employee was still within the first 24 months of an eligible apprenticeship program. An employee does not have to complete level 1 or 2 of an eligible apprenticeship program for an employer to make a claim.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '4000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Net of any other government and non-government assistance received, to be received, or that you are entitled to receive, other than the training tax credit, the shipbuilding and ship repair industry tax credit, and the federal investment tax credit on apprenticeship job creation.',
            'labelClass': 'fullLength'
          },
          {
            'label': '** Use Schedule 428 to calculate the basic and enhanced tax credits for the period before October 1, 2012, only.',
            'labelClass': 'fullLength'
          },
          {
            'label': '*** The total amount of the basic and enhanced tax credits claimed under the British Columbia training tax credit and the British Columbia shipbuilding and ship repair industry tax credit cannot be more than $7,875 per employee for the 2012 tax year.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Calculation for an employee who has completed level 3 of an eligible apprenticeship program after September 30, 2012',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'This calculation includes the completion and enhanced tax credits. To claim this credit for level 3, enter on line 510 the salary and wages* payable to the employee in the 12 months just before the completion of the level. If the employee\'s completion date is before October 1, 2012, claim the enhanced tax credit under the British Columbia training tax credit on Schedule 428.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '5000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Calculation for an employee who has completed level 4 or higher of an eligible apprenticeship program after September 30, 2012',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'This calculation includes the completion and enhanced tax credits. To claim this credit for level 4 or higher, enter on line 610 the salary and wages* payable in the 12 months just before the completion of the level. If the employee\'s completion date is before October 1, 2012, claim the enhanced tax credit under the British Columbia training tax credit on Schedule 428.',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '6000'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': '* Net of any other government and non-government assistance received, to be received, or that you are entitled to receive, other than the training tax credit, the shipbuilding and ship repair industry tax credit, and the federal investment tax credit on apprenticeship job creation.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 4 - British Columbia shipbuilding and ship repair industry tax credit',
        'rows': [
          {
            'label': '<b>Basic tax credit</b> (total G1 from Part 1)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '800'
                },
                'padding': {
                  'type': 'tn',
                  'data': '800'
                }
              }
            ],
            'indicator': 'A'
          },
          {
            'label': 'Completion tax credit',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Total E2 from Part 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '801'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'B'
                }
              }
            ]
          },
          {
            'label': 'Total E3 from Part 2',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '802'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': '<b>Completion tax credit</b> (line B <b>plus</b> line C)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '803',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '805',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '805'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Enhanced tax credit',
            'labelClass': 'fullLength bold'
          },
          {
            'label': 'Total G4 from Part 3',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '806'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'E'
                }
              }
            ]
          },
          {
            'label': 'Total E5 from Part 3',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '807'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'F'
                }
              }
            ]
          },
          {
            'label': 'Total E6 from Part 3',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '808'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'G'
                }
              }
            ]
          },
          {
            'label': '<b>Enhanced tax credit</b> (total of amounts E to G)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '809',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '810',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '810'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': 'Credit allocated from a partnership*',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  },
                  'num': '815'
                },
                'padding': {
                  'type': 'tn',
                  'data': '815'
                }
              }
            ],
            'indicator': 'I'
          },
          {
            'label': '<b>British Columbia shipbuilding and ship repair industry tax credit</b> (total of amounts A, D, H, and I)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '830'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': 'Enter the amount from line J on line 681 in Part 2 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>.',
            'labelClass': 'fullLength'
          },
          {
            'label': '* A corporation that is a member of a partnership, other than a specified member as' +
            ' defined in subsection 248(1) of the federal Income Tax Act, can claim its appropriate portion ' +
            'of the British Columbia shipbuilding and ship repair industry tax credit for the partnership for its' +
            ' tax year ending  in the tax year of the corporation. That portion would be reasonably considered ' +
            'to be in proportion to any amount the partners have agreed to share as income or loss. ' +
            'A partnership is subject to the same rules as a corporation regarding salary and wages ' +
            'paid in a fiscal period that includes October 1, 2012.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {
            label: '<i>Privacy</i> Act, Personal Information Bank number CRA PPU 047',
            'labelClass': 'absoluteAlignRight'
          }

        ]
      }
    ]
  };
})();
