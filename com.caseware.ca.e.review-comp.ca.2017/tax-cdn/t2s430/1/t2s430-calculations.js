(function() {

  wpw.tax.create.calcBlocks('t2s430', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      //part 1
      field('1000').getRows().forEach(function(row) {
        // var externalTableSummaryRow = form('T2S428').field('099').getRow(rIndex);
        // row[0].assign(externalTableSummaryRow[0].get());
        // row[1].assign(externalTableSummaryRow[1].get());
        // row[4].assign(externalTableSummaryRow[4].get()); comment out as there's no relationship btw s428 and s430
        row[3].assign(row[2].get() * field('ratesBC.510').get());
        if (row[4].get() !== 0) {
          row[5].assign(Math.max(field('ratesBC.515').get() - row[4].get(), 0));
        }
        row[6].assign(Math.min(row[3].get(), row[5].get()));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {

      //part 2
      field('2000').getRows().forEach(function(row) {
        row[3].assign(row[2].get() * 0.2);
        row[4].assign(Math.min(row[3].get(), 5250));
      });

      field('3000').getRows().forEach(function(row) {
        row[3].assign(row[2].get() * 0.2);
        row[4].assign(Math.min(row[3].get(), 5250));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {

      //part 3
      field('4000').getRows().forEach(function(row) {
        row[3].assign(row[2].get() * 0.3);
        row[5].assign(Math.max(7875 - row[4].get(), 0));
        row[6].assign(Math.min(row[3].get(), row[5].get()));
      });

      field('5000').getRows().forEach(function(row) {
        row[3].assign(row[2].get() * 0.3);
        row[4].assign(Math.min(row[3].get(), 7875));
      });

      field('6000').getRows().forEach(function(row) {
        row[3].assign(row[2].get() * 0.3);
        row[4].assign(Math.min(row[3].get(), 7875));
      });
    });

    calcUtils.calc(function(calcUtils, field, form) {

      //Part 4
      calcUtils.equals('800', '130');
      calcUtils.equals('801', '230');
      calcUtils.equals('802', '330');
      calcUtils.sumBucketValues('803', ['801', '802']);
      calcUtils.equals('805', '803');
      calcUtils.equals('806', '430');
      calcUtils.equals('807', '530');
      calcUtils.equals('808', '630');

      calcUtils.sumBucketValues('809', ['806', '807', '808']);
      calcUtils.equals('810', '809');
      calcUtils.sumBucketValues('830', ['800', '805', '810', '815']);
    });

  });
})();
