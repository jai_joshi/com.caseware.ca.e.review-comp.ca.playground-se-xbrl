(function() {
  // function tableGetGlobalValue(tableCalcs, bucket, tableNum, colIndex, sourceId) {
  //
  //   if (!angular.isArray(sourceId))
  //     sourceId = [sourceId];
  //
  //   for (var rowIndex = 0; rowIndex < sourceId.length; rowIndex++) {
  //     if (rowIndex >= sourceId.length) {
  //       //do nothing
  //     }
  //     else {
  //       tableCalcs.setCellValue(bucket[tableNum].cells[rowIndex], colIndex, sourceId[rowIndex]);
  //     }
  //   }
  // }

  wpw.tax.global.tableCalculations.t2s430 = {
    "1000": {
      "showNumbering": true,
      "columns": [
        {
          "header": "ITA identification number (or SIN or name of employee)",
          "indicator": "A1",
          "tn": "100",
          "num": "100",
          "totalNum": "100-t",
          "width": "12%",
          type: 'text'
        },
        {
          "header": "Name of program",
          "indicator": "B1",
          "tn": "105",
          "num": "105",
          "totalNum": "105-t",
          "width": "12%",
          type: 'text'
        },
        {
          "header": "Salary and wages payable after September 30, 2012*",
          "indicator": "C1",
          "tn": "110",
          "num": "110",
          "totalNum": "110-t",
          "width": "12%"
        },
        {
          "header": "Column C1 x 20%",
          "indicator": "D1",
          "tn": "115",
          "num": "115",
          "totalNum": "115-t",
          "width": "12%",
          "disabled": true
        },
        {
          "header": "British Columbia training tax credit** claimed in the tax year for the employee (from column E1 in Part 1 of Schedule 428)",
          "indicator": "E1",
          "tn": "120",
          "num": "120",
          "totalNum": "120-t",
          "width": "12%",
          disabled: true
        },
        {
          "header": "$5,250 <b>minus</b> column E1***",
          "indicator": "F1",
          "num": "121",
          "totalNum": "121-t",
          "width": "12%",
          "disabled": true
        },
        {
          "header": "Lesser of column D1 and column F1",
          "indicator": "G1",
          "tn": "125",
          "num": "125",
          "total": true,
          "totalNum": "130",
          "totalMessage": "Total G1",
          "width": "12%",
          "disabled": true
        }],
      "hasTotals": true
    },
    "2000": {
      "showNumbering": true,
      "columns": [
        {
        "header": "ITA identification number (or SIN or name of employee)",
        "indicator": "A2",
        "tn": "200",
        "num": "200",
        "totalNum": "200-t",
        "width": "12%",
        type: 'text'
      },
        {
          "header": "Name of program",
          "indicator": "B2",
          "tn": "205",
          "num": "205",
          "totalNum": "205-t",
          "width": "12%",
          type: 'text'
        },
        {
          "header": "Salary and wages*",
          "indicator": "C2",
          "tn": "210",
          "num": "210",
          "totalNum": "210-t",
          "width": "12%"
        },
        {
          "header": "Column C2 x 20%",
          "indicator": "D2",
          "tn": "215",
          "num": "215",
          "totalNum": "215-t",
          "width": "12%",
          "disabled": true
        },
        {
          "header": "Lesser of column D2 or $5,250",
          "indicator": "E2",
          "tn": "220",
          "num": "220",
          "total": true,
          "totalNum": "230",
          "totalMessage": "Total E2",
          "width": "12%",
          "disabled": true
        }],
      "hasTotals": true
    },
    "3000": {
      "showNumbering": true,
      "columns": [{
        "header": "ITA identification number (or SIN or name of employee)",
        "indicator": "A3",
        "tn": "300",
        "num": "300",
        "totalNum": "300-t",
        "width": "12%",
        type: 'text'
      },
        {
          "header": "Name of program",
          "indicator": "B3",
          "tn": "305",
          "num": "305",
          "totalNum": "305-t",
          "width": "12%",
          type: 'text'
        },
        {
          "header": "Salary and wages*",
          "indicator": "C3",
          "tn": "310",
          "num": "310",
          "totalNum": "310-t",
          "width": "12%"
        },
        {
          "header": "Column C3 x 20%",
          "indicator": "D3",
          "tn": "315",
          "num": "315",
          "totalNum": "315-t",
          "width": "12%",
          "disabled": true
        },
        {
          "header": "Lesser of column D3 or $5,250",
          "indicator": "E3",
          "tn": "320",
          "num": "320",
          "total": true,
          "totalNum": "330",
          "totalMessage": "Total E3",
          "width": "12%",
          "disabled": true
        }],
      "hasTotals": true
    },
    "4000": {
      "showNumbering": true,
      "columns": [
        {
        "header": "ITA identification number (or SIN or name of employee)",
        "indicator": "A4",
        "tn": "400",
        "num": "400",
        "totalNum": "400-t",
        "width": "12%",
        type: 'text'
      },
        {
          "header": "Name of program",
          "indicator": "B4",
          "tn": "405",
          "num": "405",
          "totalNum": "405-t",
          "width": "12%",
          type: 'text'
        },
        {
          "header": "Salary and wages payable after September 30, 2012*",
          "indicator": "C4",
          "tn": "410",
          "num": "410",
          "totalNum": "410-t",
          "width": "12%"
        },
        {
          "header": "Column C4 x 30%",
          "indicator": "D4",
          "tn": "415",
          "num": "415",
          "totalNum": "415-t",
          "width": "12%",
          "disabled": true
        },
        {
          "header": "British Columbia enhanced training tax credit** claimed in the tax year for the employee (from column E4 or E5 in Part 3 of Schedule 428)",
          "indicator": "E4",
          "tn": "420",
          "num": "420",
          "totalNum": "420-t",
          "width": "12%"
        },
        {
          "header": "$7,875 <b>minus</b> column E4***",
          "indicator": "F4",
          "num": "421",
          "totalNum": "421-t",
          "width": "12%",
          "disabled": true
        },
        {
          "header": "Lesser of column D4 and column F4",
          "indicator": "G4",
          "tn": "425",
          "num": "425",
          "total": true,
          "totalNum": "430",
          "totalMessage": "Total G4",
          "width": "12%",
          "disabled": true
        }],
      "hasTotals": true
    },
    "5000": {
      "showNumbering": true,
      "columns": [
        {
        "header": "ITA identification number (or SIN or name of employee)",
        "indicator": "A5",
        "tn": "500",
        "num": "500",
        "totalNum": "500-t",
        "width": "12%",
        type: 'text'
      },
        {
          "header": "Name of program",
          "indicator": "B5",
          "tn": "505",
          "num": "505",
          "totalNum": "505-t",
          "width": "12%",
          type: 'text'
        },
        {
          "header": "Salary and wages*",
          "indicator": "C5",
          "tn": "510",
          "num": "510",
          "totalNum": "510-t",
          "width": "12%"
        },
        {
          "header": "Column C5 x 30%",
          "indicator": "D5",
          "tn": "515",
          "num": "515",
          "totalNum": "515-t",
          "width": "12%",
          "disabled": true
        },
        {
          "header": "Lesser of column D5 or $7,875",
          "indicator": "E5",
          "tn": "520",
          "num": "520",
          "total": true,
          "totalNum": "530",
          "width": "12%",
          "disabled": true
        }],
      "hasTotals": true
    },
    "6000": {
      "showNumbering": true,
      "columns": [
        {
        "header": "ITA identification number (or SIN or name of employee)",
        "indicator": "A6",
        "tn": "600",
        "num": "600",
        "totalNum": "600-t",
        "width": "12%",
        type: 'text'
      },
        {
          "header": "Name of program",
          "indicator": "B6",
          "tn": "605",
          "num": "605",
          "totalNum": "605-t",
          "width": "12%",
          type: 'text'
        },
        {
          "header": "Salary and wages*",
          "indicator": "C6",
          "tn": "610",
          "num": "610",
          "totalNum": "610-t",
          "width": "12%"
        },
        {
          "header": "Column C5 x 30%",
          "indicator": "D6",
          "tn": "615",
          "num": "615",
          "totalNum": "615-t",
          "width": "12%",
          "disabled": true
        },
        {
          "header": "Lesser of column D6 or $7,875",
          "indicator": "E6",
          "tn": "620",
          "num": "620",
          "total": true,
          "totalNum": "630",
          "width": "12%",
          "disabled": true
        }],
      "hasTotals": true
    }
  }
})();
