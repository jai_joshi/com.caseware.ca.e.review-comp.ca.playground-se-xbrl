(function() {
  wpw.tax.create.diagnostics('efile', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('EFILE.type', common.prereq(common.check('cp.154', 'isYes'),
        function(tools) {
      return tools.requireAll(tools.list(['login', 'pass']), 'isFilled') || tools.requireOne(tools.list(['type', 'cp.154']), 'isNo');
    }));

  });
})();
