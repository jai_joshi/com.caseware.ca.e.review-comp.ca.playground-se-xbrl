/* global wpw */

(function() {
  'use strict';

  //Global module variables
  var /** @type Object*/ cred, wait; //Credentials and progress bar dialog objects

  wpw.tax.actions.submitClick = function() {
    betaVersionWarning().then(function() {
      checkDiagnostics().then(function() {
        checkWorkflow().then(function() {
          getCredentials().then(function() {
            showTermsDialog().then(function() {
              tryToSubmitT2Return();
            });
          });
        });
      });
    });
  };

  wpw.tax.actions.submit1135 = function() {
    betaVersionWarning().then(function() {
      checkForeignReportingDiagnostics('T1135').then(function() {
        checkWorkflow().then(function() {
          getCredentials().then(function() {
            showTermsDialog().then(function() {
              tryToSubmitForeignReport('T1135');
            });
          });
        });
      });
    });
  };

  wpw.tax.actions.submit1134 = function() {
    betaVersionWarning().then(function() {
      checkForeignReportingDiagnostics('T1134').then(function() {
        checkWorkflow().then(function() {
          getCredentials().then(function() {
            showTermsDialog().then(function() {
              tryToSubmitForeignReport('T1134');
            });
          });
        });
      });
    });
  };

  wpw.tax.actions.submit106 = function() {
    betaVersionWarning().then(function() {
      checkForeignReportingDiagnostics('T106').then(function() {
        checkWorkflow().then(function() {
          getCredentials().then(function() {
            showTermsDialog().then(function() {
              tryToSubmitForeignReport('T106');
            });
          });
        });
      });
    });
  };

  function checkDiagnostics() {
    return wpw.tax.global.efile.checkBeforeSubmitting();
  }

  function checkForeignReportingDiagnostics(form) {
    return wpw.tax.global.foreignReportingService.checkForeignReportingBeforeSubmitting(form);
  }

  function checkWorkflow() {
    var deferred = wpw.tax.global.q.defer();
    if (wpw.tax.global.engagementProperties.workflowStatus.id !== 'REVIEWED') {
      wpw.tax.global.dialogs.confirm('Workflow warning',
          'The return you are trying to send isn\'t marked as ready to efile.' +
          ' Are you sure you want to continue with the Efile?').result.then(function() {
        deferred.resolve();
      }, function() {
        deferred.reject();
      });
    } else {
      deferred.resolve();
    }
    return deferred.promise;
  }

  function betaVersionWarning() {
    return wpw.tax.global.dialogs.confirm('Efile Transmission', 'NOTE - although this software has been certified for Efile by the ' +
        'CRA, it is still a Beta Version of CorpTax. Would you still like to proceed with the Transmission?').result;
  }

  function showTermsDialog() {
    return wpw.tax.global.dialogs.confirm('CRA Security Requirements (Terms & Conditions)',
        'By clicking "Yes" you agree to the following "Terms & Conditions" ' +
        'before Efiling the Return:<br/><br/>' +
        '<li>I may file a corporation income tax return using Corporation Internet Filing only for the corporation' +
        'to which a Web Access Code has been assigned or for my client if I am a registered tax professional ' +
        'with a valid EFILE On-Line number and password.</li><br />' +
        '<li>I may only file an initial or amended, eligible, 2002 or subsequent year corporation income tax ' +
        'return through Corporation Internet Filing.</li><br />' +
        '<li>I understand that I will not be able to use the Corporation Internet Filing service to file an ' +
        'initial or amended return for year prior to the 2002 tax year.</li><br />' +
        '<li>I understand and agree that I cannot change the corporation\'s name, and head office' +
        ' or mailing addresses and head office or mailing addresses' +
        'with a tax return filed through Corporation Internet Filing.</li><br />' +
        '<li>I understand that I cannot add or change direct deposit information with a tax return filed ' +
        'through Corporation Internet Filing.</li><br />' +
        '<li>I will not attempt to disrupt the Corporation Internet Filing service by uploading files other than the' +
        ' eligible corporation income tax return. If I do, the Canada Revenue Agency (CRA) will deny me' +
        ' access to electronic services.</li><br />' +
        '<li>I certify that the attached electronic tax return is complete and accurate, and that it reports the ' +
        'corporation\'s income from all sources. Or, if filing on behalf of a client, my client has signed ' +
        'Form T183CORP and certified that the attached electronic return is correct, complete, and reports ' +
        'the client\'s income from all sources.').result;
  }

  function getCredentials() {
    var USING_LOGIN = '1', USING_WAC = '2', ERR_DIALOG_HEADER = "Error submitting return";
    var deferred = wpw.tax.global.q.defer();
    cred = {
      type: wpw.tax.field('type', 'efile').get() || '1',
      password: ""
    };
    if (String(cred.type) === USING_LOGIN) {
      cred.loginOrWac = wpw.tax.field('login', 'efile').get() || "";
      cred.password = wpw.tax.field('pass', 'efile').get() || "";
    } else {
      cred.loginOrWac = wpw.tax.field('wac', 'efile').get() || "";
    }
    if ((String(cred.type) === USING_LOGIN && !(cred.loginOrWac || cred.password)) || (String(cred.type) === USING_WAC && !cred.wac)) {
      wpw.tax.global.dialogs.error(ERR_DIALOG_HEADER, "Please enter correct login information");
      deferred.reject();
    } else {
      deferred.resolve();
    }
    return deferred.promise;
  }

  function updateProgress(prog, msg) {
    var arg = {progress: prog};
    if (msg) {
      arg.msg = msg;
    }
    wpw.tax.global.broadcast('dialogs.wait.progress', arg);
  }

  function parseResponse(result) {
    if (result && result["xmlResponse"]) {
      var xml = $($.parseXML(result["xmlResponse"]))[0];
      var message = "", title = "", status = "Submitted", confirmation = "", errors = "", collaborateMessage = '';

      var timeStamp = xml.getElementsByTagName("tgif:TimestampDateTime")[0].innerHTML;
      if (xml.getElementsByTagName("tgif:ErrorCode").length > 0) {

        title = "There was an error with sending your return";

        message = "Business number: " + xml.getElementsByTagName("tgif:BusinessNumber")[0].innerHTML + "<br/>" +
            "Taxation year end date: " + xml.getElementsByTagName("tgif:TaxationYearEndDate")[0].innerHTML + "<br/>" +
            "Time stamp date time: " + timeStamp + "<br/><br/>";

        for (var i = 0; i < xml.getElementsByTagName("tgif:ErrorCode").length; i++) {
          errors = errors + 'Error ' + xml.getElementsByTagName("tgif:ErrorCode")[i].innerHTML + ': ' +
              xml.getElementsByTagName("tgif:EnglishMessageText")[i].innerHTML;

          message = message + "Error code: " + xml.getElementsByTagName("tgif:ErrorCode")[i].innerHTML + "<br/>" +
              "Error message: " + xml.getElementsByTagName("tgif:EnglishMessageText")[i].innerHTML + "<br/><br/>";

          collaborateMessage = collaborateMessage + "Error " + xml.getElementsByTagName("tgif:ErrorCode")[i].innerHTML +
              ": " + xml.getElementsByTagName("tgif:EnglishMessageText")[i].innerHTML + " ";
        }

        status = "Failed";

      } else if (xml.getElementsByTagName("tgif:ConfirmationNumber").length > 0) {

        title = "Successfully transmitted return";

        confirmation = xml.getElementsByTagName("tgif:ConfirmationNumber")[0].innerHTML;

        var businessNum = "";
        var taxYearEnd = "";
        var businessName = "";

        if (wpw.tax.global.values.CP.bn.value) {
          businessNum = wpw.tax.global.values.CP.bn.value;
        }

        if (wpw.tax.global.values.CP["tax_end"].value) {
          var taxYearEndDate = new Date(wpw.tax.utilities.toJsDate(wpw.tax.global.values.CP["tax_end"].value));
          taxYearEnd = taxYearEndDate.toLocaleDateString();

        }

        if (wpw.tax.global.values.CP["002"].value) {
          businessName = wpw.tax.global.values.CP["002"].value;
        }

        message = "Please keep this confirmation number for your records" + "<br/><br/>" +
            "Confirmation number: " + confirmation + "<br/>" +
            "Business number: " + businessNum + "<br/>" +
            "Name: " + businessName + "<br/>" +
            "Taxation year end: " + taxYearEnd + "<br/>" +
            "Confirmation of receipt: " + timeStamp + "<br/><br/>" +
            "Your return will be verified using the same criteria that we apply to other " +
            "filing methods. You are required to keep all information for six years.";

        var state = wpw.tax.global.engagementService.getEngagementProperties();
        if (state.$$state.value) {
          var sourceEngagement = wpw.utilities.clone(state.$$state.value);
          state.$$state.value.status = 'completed';
          wpw.tax.global.engagementService.saveEngagementProperties(state.$$state.value, sourceEngagement).then(function(result) {
          }, function(err) {
            dialogs.error('Error', err);
          });
        }

        //lastSubmissionStatus = "Failed";

      }

      var date = new Date(timeStamp); // some mock date
      var newRow = wpw.tax.global.efile.formGenerator.generate['row']('EFILE', '333');
      var table = wpw.tax.global.efile.fieldAccessor('EFILE.333');
      table.track();
      angular.merge(newRow, {
        0: {
          value: {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate(),
            hour: date.getHours(),
            min: date.getMinutes()
          }
        },
        1: {value: status || 'Failed'},
        2: {value: confirmation || 'N/A'},
        3: {value: errors}
      });

      var cells = table.getKey('cells');
      if (cells && cells[0] && cells[0][1] && !cells[0][1].value) {
        cells.splice(0, 1);
      }

      cells.push(newRow);

      wait.close();
      wpw.tax.global.dialogs.notify(title, message);
    } else {
      wait.close();
      wpw.tax.global.dialogs.error('Error', 'There was a problem during submitting this return');
    }
  }

  function parseErrors(errors) {
    var message = 'The return has not been filed. Please contact your software developer.';
    if (errors && errors.status && errors.statusText) {
      message = errors.status + ' ' + errors.statusText;
    }
    wait.close();
    wpw.tax.global.dialogs.error('There was a problem during submitting this return', message);
  }

  function tryToSubmitT2Return() {
    //Show progress bar
    wait = wpw.tax.global.dialogs.wait('', 'Preparing and submitting data to CRA', 0);
    //Gather data from the forms
    wpw.tax.global.efile.createCORObject1(wpw.tax.global.efile.nMode.TEXT_BLOB);
    updateProgress(10);
    //Create a text variable from the data
    var sBlob = wpw.tax.global.efile.assemble(wpw.tax.global.efile.taxForms, wpw.tax.global.efile.nMode.TEXT_BLOB);
    updateProgress(20);
    var /** @type Object */ params = {"corFile": sBlob, "login": cred.loginOrWac, "password": cred.password};
    wpw.tax.global.pluginService.t2FileReturn(params).then(function(result) {
      parseResponse(result);
    }, function(errors) {
      parseErrors(errors);
    });
  }

  /**
   * Parses xml response from the server
   * @param {Object} result
   * @param formSubmitted
   */
  function parseForeignReportingResponse(result, formSubmitted) {
    //TODO: Deal with en/fr translations (CRA gives responses in en and fr but we need our string to be translated as well)
    if (result && result["xmlResponse"]) {
      var xml = $.parseXML(result["xmlResponse"]);
      var title = "", status = "Submitted", confirmation = "", message = "";
      var timeStamp = new Date();
      var resultCode = xml.getElementsByTagName("t2ersp:T2ResultCode")[0].textContent;

      if (xml.getElementsByTagName("t1rsp:Rejection").length == 0 && xml.getElementsByTagName("t1rsp:SystemError").length == 0) {
        wpw.tax.form(formSubmitted).field('efileSubmitted').set(true);
        if(resultCode == '481')
          wpw.tax.form(formSubmitted).field('efileAmendedSubmitted').set(true);

        title = "Successfully transmitted return";
        confirmation = xml.getElementsByTagName("t2ersp:T2TransmissionNumber")[0].textContent.toString();
        var businessNum = "";
        var taxYearEnd = "";
        var businessName = "";

        if (wpw.tax.global.values.CP.bn.value === xml.getElementsByTagName("t2ersp:BusinessNumber")[0].textContent) {
          businessNum = wpw.tax.global.values.CP.bn.value;
        }

        if (wpw.tax.global.values.CP["tax_end"].value.year == xml.getElementsByTagName("t2ersp:TaxYear")[0].textContent.toString()) {
          var taxYearEndDate = new Date(wpw.tax.utilities.toJsDate(wpw.tax.global.values.CP["tax_end"].value));
          taxYearEnd = taxYearEndDate.toLocaleDateString();
        }

        if (wpw.tax.global.values.CP["002"].value === xml.getElementsByTagName("t2ersp:CorporationName")[0].textContent) {
          businessName = wpw.tax.global.values.CP["002"].value;
        }

        message = "Please keep this confirmation number for your records" + "<br/><br/>" +
            "Confirmation number: " + confirmation + "<br/>" +
            "Business number: " + businessNum + "<br/>" +
            "Name: " + businessName + "<br/>" +
            "Taxation year end: " + taxYearEnd + "<br/>" +
            "Confirmation of receipt: " + timeStamp + "<br/><br/>" +
            "Your return will be verified using the same criteria that we apply to other " +
            "filing methods. You are required to keep all information for six years.";

        var state = wpw.tax.global.engagementService.getEngagementProperties();
        if (state.$$state.value) {
          var sourceEngagement = wpw.utilities.clone(state.$$state.value);
          state.$$state.value.status = 'completed';
          wpw.tax.global.engagementService.saveEngagementProperties(state.$$state.value, sourceEngagement).then(function(result) {
          }, function(err) {
            dialogs.error('Error', err);
          });
        }
      } else {
        var bn = wpw.tax.form(formSubmitted).field('1002').get();
        title = "There was an error with sending your return";
        taxYearEnd = wpw.tax.utilities.toFormattedDateString(wpw.tax.form(formSubmitted).field('111').get());

        message = "Business number: " + (bn || "Not Found") + "<br/>" +
            "Taxation year end date: " + (taxYearEnd || "Not Found") + "<br/>" +
            "Time stamp date time: " + timeStamp + "<br/><br/>";

        var fullEnglishMessage = '';
        var fullFrenchMessage = '';
        for (var i = 0; i < xml.getElementsByTagName("t1rsp:MessageText").length; i++) {

          xml.getElementsByTagName("t1rsp:MessageText")[i].getAttribute('xml:lang') == 'en' ?
              fullEnglishMessage += xml.getElementsByTagName("t1rsp:MessageText")[i].textContent + '<br/>' :
              fullFrenchMessage += xml.getElementsByTagName("t1rsp:MessageText")[i].textContent + '<br/>';
        }

        message += "Error code: " + resultCode + "<br/><br/>" + "English message: " + fullEnglishMessage + "<br/><br/>" +
            "French message: " + fullFrenchMessage + "<br/><br/>";
        status = "Failed";
      }

      var date = timeStamp;
      var newRow = wpw.tax.global.efile.formGenerator.generate['row']('EFILE', '400');
      var table = wpw.tax.global.efile.fieldAccessor('EFILE.400');
      var rowMessage = fullEnglishMessage ?
          'Error Code: ' + resultCode + ': ' + fullEnglishMessage.replace(/<br\/>/, '') :
          xml.getElementsByTagName("t1rsp:EnglishMessage")[0].textContent;
      table.track();
      angular.merge(newRow, {
        0: {value: formSubmitted, config: {type: 'text'}},
        1: {
          value: {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate(),
            hour: date.getHours(),
            min: date.getMinutes()
          }
        },
        2: {value: status || 'Failed'},
        3: {value: confirmation || 'N/A'},
        4: {value: rowMessage}
      });

      var cells = table.getKey('cells');
      if (cells && cells[0] && cells[0][1] && !cells[0][1].value) {
        cells.splice(0, 1);
      }

      cells.push(newRow);
      wait.close();
      wpw.tax.global.dialogs.notify(title, message);
    }
  }

  function tryToSubmitForeignReport(form) {
    //Show progress bar
    wait = wpw.tax.global.dialogs.wait('', 'Preparing and submitting data to CRA', 0);
    wpw.tax.actions.generateXML(form, function(data) {
      wait.close();
      validateXML(data).then(function() {
        sendData(data, form);
      }, function(errors) {
        parseErrors(errors);
      });
    });
  }

  /**
   * Temporary XML validation until server side code can be written
   * @param {String} xml
   * @return {Object} - returns a promise
   */
  function validateXML(xml) {
    var q = wpw.tax.global.q.defer();
    try {
      $.parseXML(xml);
      q.resolve();
    } catch (e) {
      q.reject(console.error('Invalid XML'));
    }
    return q.promise;
  }

  /**
   * The function tries to send data to CRA
   * @param xml
   * @param form
   */
  function sendData(xml, form) {
    updateProgress(20);
    var /** @type Object */ params = {
      "corFile": xml,
      "login": cred.loginOrWac,
      "password": cred.password
    };
    wpw.tax.global.pluginService.fileFormService(params).then(function(result) {
      parseForeignReportingResponse(result, form);
    }, function(errors) {
      parseErrors(errors);
    });
  }

})();
