(function() {
  'use strict';

  wpw.tax.global.formData.efile = {
    formInfo: {
      abbreviation: 'efile',
      title: 'Efile Information',
      showCorpInfo: true,
      headerImage: 'cw',
      category: 'EFILE'
    },
    sections: [
      {
        header: 'EFILE submitting',
        rows: [{
          type: 'splitTable',
          side1: [{
            labelClass: 'tabbed',
            type: 'infoField',
            label: 'Choose authentication mode:',
            num: 'type',
            inputType: 'dropdown',
            options: [{
              option: 'Login Information',
              value: '1'
            }, {
              option: 'WACode',
              value: '2'
            }],
            init: '1'

          },
            {
              labelClass: 'fullLength'
            }, {
              labelClass: 'tabbed bold',
              type: 'infoField',
              label: 'Login:',
              labelWidth: '10%',
              inputType: 'text',
              width: '50%',
              num: 'login',
              showWhen: 'type'
            },
            {
              labelClass: 'tabbed bold',
              type: 'infoField',
              label: 'Password:',
              labelWidth: '10%',
              inputType: 'text',
              width: '50%',
              num: 'pass',
              showWhen: 'type'
            },
            {
              labelClass: 'tabbed bold',
              type: 'infoField',
              label: 'WAC:',
              labelWidth: '100%',
              inputType: 'text',
              width: '50%',
              num: 'wac',
              showWhen: {
                fieldId: 'type',
                compare: {is: '2'}
              }
            },
            {
              labelClass: 'fullLength'
            },
            {
              type: 'infoField',
              inputType: 'functionButton',
              btnLabel: 'Submit Return',
              inputClass: 'std-input-width',
              paramFn: wpw.tax.actions.submitClick
            }
          ],
          side2: [{
            label: 'Foreign Reporting Submission',
            labelClass: 'fullLength bold center'
          },
            {
              labelClass: 'fullLength'
            },
            {
              type: 'infoField',
              inputType: 'functionButton',
              btnLabel: 'Submit T1134',
              label: 'Submit T1134',
              width: '110px',
              paramFn: wpw.tax.actions.submit1134
            },
            {
              type: 'infoField',
              inputType: 'functionButton',
              btnLabel: 'Submit T1135',
              label: 'Submit T1135',
              width: '110px',
              paramFn: wpw.tax.actions.submit1135
            },
            {
              type: 'infoField',
              inputType: 'functionButton',
              btnLabel: 'Submit T106',
              label: 'Submit T106',
              width: '110px',
              paramFn: wpw.tax.actions.submit106
            },
            {
              type: 'infoField',
              inputType: 'singleCheckbox',
              label: 'Print Xml',
              num: '1337'
            }]
        }]
      },
      {
        header: 'History',
        rows: [
          {
            type: 'table',
            num: '333'
          },
          {labelClass: 'fullLength'},
          {
            label: '*Warnings',
            labelClass: 'bold fullLength'
          },
          {
            label: '- Please keep the confirmation number(s) for your records',
            labelClass: 'tabbed fullLength'
          },
          {
            label: '- Your return will be verified by the CRA using the same '
            + 'criteria applied to other filling methods',
            labelClass: 'tabbed fullLength'
          },
          {
            label: 'According to the CRA, you are required to keep all information for six years',
            labelClass: 'tabbed fullLength'
          }]
      },
      {
        hideFieldset: true,
        rows: [
          {

            labelClass: 'fullLength'
          },
          {
            label: 'Error Codes (EFILE)',
            labelClass: 'subTitleFont fullLength'
          },
          {
            label: 'The program will display the EFILE error code(s) '
            + 'and description(s) in this section, where applicable.',
            labelClass: 'fullLength'
          }]
      },
      {
        header: 'Alberta Net File',
        rows: [
          {
            type: 'table',
            num: '200'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {

            labelClass: 'fullLength'
          },
          {
            label: 'Error Codes (Alberta Net File)',
            labelClass: 'subTitleFont fullLength'
          },
          {
            label: 'The program will display the Alberta Net File error code(s)'
            + ' and description(s) in this section, where applicable.',
            labelClass: 'fullLength'
          }]
      },
      // TODO: create accent on 'Quebec'
      {
        header: 'Netfile Quebéc or Clic Revenue',
        rows: [
          {
            type: 'table',
            num: '300'
          }
        ]
      },
      {
        hideFieldset: true,
        rows: [
          {

            labelClass: 'fullLength'
          },
          {
            label: 'Error Codes (NetFile Quebéc or Clic Revenu)',
            labelClass: 'subTitleFont fullLength'
          },
          {
            label: 'The program will display the Netfile Quebec or '
            + 'Clic Revenue error code(s) and description(s) in this section, where applicable.',
            labelClass: 'fullLength'
          }]
      },
      {
        header: 'T2 Corporation Foreign Reporting',
        rows: [
          {
            type: 'table',
            num: '400'
          }
        ]
      }]
  };
})();
