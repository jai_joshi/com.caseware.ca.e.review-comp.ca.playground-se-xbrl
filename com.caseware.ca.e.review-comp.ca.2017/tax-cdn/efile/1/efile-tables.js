(function() {
  wpw.tax.global.tableCalculations.efile = {
    "200": {
      "infoTable": true,
      "fixedRows": true,
      "columns": [{
        "header": "Status",
        "type": "none",
        "width": "40%",
        "num": "205"
      },
        {
          "header": "Date",
          "num": "210",
          "type": 'datetime'
        },
        {
          "header": "Confirmation number*",
          "num": "215",
          "type": 'text'
        },
        {
          "header": "",
          "type": "none",
          "width": "25%",
          "num": "220"
        }]
    },
    "300": {
      "infoTable": true,
      "fixedRows": true,
      "columns": [{
        "header": "Status",
        "type": "none",
        "width": "40%",
        "num": "305"
      },
        {
          "header": "Date",
          "type": "date",
          "num": "310"
        },
        {
          "header": "Confirmation number*",
          "num": "315",
          "type": 'text'
        },
        {
          "header": "Transmission service used",
          "width": "25%",
          "num": "320",
          "type": 'text'
        }]
    },
    "333": {
      "infoTable": true,
      "fixedRows": true,
      "showNumbering": true,
      "columns": [{
          "width": "20%",
          "header": "Date",
          "type": "datetime",
          "num": "110",
          "disabled": true,
          "cannotOverride": true
        },
        {
          "header": "Status",
          "width": "10%",
          "num": "105",
          "disabled": true,
          "cannotOverride": true,
          "type": 'text',
          "textAlign": 'center'
        },
        {
          "header": "Confirmation number*",
          "width": "13%",
          "num": "115",
          "cannotOverride": true,
          "type": 'text',
          "textAlign": 'center',
          "disabled": true
        },
        {
          "header": "Errors",
          "num": "120",
          "cannotOverride": true,
          "type": 'text',
          "disabled": true
        }]
    },
    "400": {
      "type": "table",
      "infoTable": true,
      "fixedRows": true,
      "showNumbering": true,
      "num": "400",
      "columns": [{
        "width": "10%",
        "header": "Form",
        "type": "text",
        "num": "425",
        "disabled": true,
        "cannotOverride": true
      },{
        "width": "20%",
        "header": "Date",
        "type": "datetime",
        "num": "410",
        "disabled": true,
        "cannotOverride": true
      },
        {
          "header": "Status",
          "width": "10%",
          "num": "405",
          "disabled": true,
          "cannotOverride": true,
          "type": 'text',
          "textAlign": 'center'
        },
        {
          "header": "Confirmation number*",
          "width": "13%",
          "num": "415",
          "cannotOverride": true,
          "type": 'text',
          "textAlign": 'center',
          "disabled": true
        },
        {
          "header": "Information",
          "num": "420",
          "cannotOverride": true,
          "type": 'text',
          "disabled": true
        }]
    }
  }
})();
