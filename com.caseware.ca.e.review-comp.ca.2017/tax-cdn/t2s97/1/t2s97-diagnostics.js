(function() {
  wpw.tax.create.diagnostics('t2s97', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;
    var checkBoxFields = ['001', '002', '003', '004', '006', '007', '008', '009', '010', '011', '012'];

    diagUtils.diagnostic('0971000', common.prereq(common.and(
        common.check('t2j.070', 'isYes'),
        common.check('t2j.080', 'isNo')),
        common.requireFiled('T2S97')));

    diagUtils.diagnostic('0971001', common.prereq(common.requireFiled('T2S97'),
        function(tools) {
          return tools.requireOne(tools.list(checkBoxFields), 'isChecked')
        }));

    diagUtils.diagnostic('097.300', common.prereq(common.and(
        common.requireFiled('T2S97'),
        function(tools) {
          return tools.checkMethod(tools.list(checkBoxFields), 'isChecked') > 1
        }), function(tools) {
      return tools.checkAll(checkBoxFields, function(num) {
        return tools.field(num).require('isZero');
      })
    }));
  });
})();