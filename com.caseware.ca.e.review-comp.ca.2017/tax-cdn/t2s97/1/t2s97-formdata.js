(function() {
  'use strict';

  var countryAddressCodes = new wpw.tax.actions.codeToDropdownOptions(wpw.tax.codes.countryAddressCodes, 'value');

  wpw.tax.global.formData.t2s97 = {
    formInfo: {
      abbreviation: 'T2S97',
      title: 'Additional Information On Non-Resident Corporations In Canada',
      //subTitle: '(2011 and later years)',
      schedule: 'Schedule 97',
      code: 'Code 1101',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      category: 'Federal Tax Forms',
      formFooterNum: 'T2 SCH 97 E (12)'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {
            label: '• Non-resident corporations must complete and file this schedule with their' +
            '<i>T2 Corporation Income Tax Return</i>.',
            labelClass: 'fullLength'
          },
          {
            label: '• A non-resident corporation includes an emigrant corporation and' +
            'a deemed non-resident corporation according to subsection 250(5) of the <i>Income Tax Act</i>.',
            labelClass: 'fullLength'
          },
          {
            label: '• All legislative references are to the federal <i>Income Tax Act and Income Tax Regulations</i>',
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'Part 1 – Incorporation information',
        rows: [
          {
            type: 'infoField',
            inputType: 'dropdown',
            options: countryAddressCodes,
            label: 'Name the country in which the corporation was incorporated' +
            '(or deemed to have been incorporated according to subsection 250(5.1))',
            labelWidth: '60%',
            tn: '200',
            num: '200', "validate": {"or":[{"length":{"min":"2","max":"2"}},{"check":"isEmpty"}]}
          },
          {
            labelClass: 'fullLength'
          },
          {
            type: 'infoField',
            inputType: 'radio',
            label: 'If the corporation was incorporated in Canada, was a certificate of discontinuance issued?',
            labelWidth: '80%',
            init: '2',
            tn: '210',
            num: '210'
          },
          {
            labelClass: 'fullLength'
          }
        ]
      },
      {
        header: 'Part 2 – Canadian income of a non-resident corporation',
        rows: [
          {
            type: 'infoField',
            inputType: 'none',
            labelAfterNum: true,
            label: 'Tick <b>only one</b> of the following that most closely applies to the non-resident corporation:',
            tn: '300',
            num: '300', "validate": {"or":[{"matches":"^-?[.\\d]{2,2}$"},{"check":"isEmpty"}]}
          },
          {type: 'horizontalLine'},
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            width: '30px',
            labelEnd: true,
            label: '01  <b>Treaty-based exempt corporation</b>:' +
            'A corporation that carried on a treaty-protected business in Canada, had a taxable capital gain ' +
            'subject to tax in Canada, or disposed of a taxable Canadian property ' +
            'that was a treaty-protected property. Complete and attach Schedule 91, Information Concerning Claims' +
            ' for Treaty-Based Exemptions. If you are claiming a refund of the withholding tax for ' +
            'services rendered in Canada, provide the original T4A-NR slip along with a copy of your contract.' +
            '<br><br><b>Note</b><br> Box 01 also includes a non-resident corporation' +
            '(or deemed non-resident according to subsection 250(6)) with income earned in Canada' +
            ' from the operation of a ship or aircraft in international traffic,' +
            'which is exempt from tax under paragraph 81(1)(c).',
            num: '01'
          },
          {type: 'horizontalLine'},
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            labelEnd: true,
            width: '30px',
            label: '02 <b>Disposition of taxable Canadian property</b>: A corporation reporting ' +
            'a disposition of taxable Canadian property. Attach Form T2064,' +
            '<i>Certificate – Proposed Disposition of Property by a Non-resident of Canada</i>,' +
            'or Form T2068, <i>Certificate – The Disposition of Property by a Non-Resident of Canada</i>.' +
            'See section 116 and Information Circular IC72-17, <i>Procedures concerning the disposition of' +
            ' taxable Canadian property by non-residents of Canada – Section 116</i>.' +
            '<br><br><b>Note</b><br> If the corporation disposed of a taxable Canadian property' +
            '(other than real property) that was a treaty-protected property or had a taxable capital gain in respect' +
            ' of a property that was a treaty-protected property, tick box 01 rather than box 02. If the corporation ' +
            'carried on business in Canada through a permanent establishment and disposed of a taxable' +
            ' Canadian property, tick box 07 rather than box 02.',
            labelWidth: '90%',
            num: '02'
          },
          {type: 'horizontalLine'},
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            labelEnd: true,
            width: '30px',
            label: '03 <b>Section 216</b>: A corporation that is electing to file a Canadian income tax return' +
            ' under section 216. If a corporation files its T2 return under subsection 216(1),' +
            ' it will be subject to Part I tax on the net rental income and must file its income tax return ' +
            'within two years from the end of the tax year unless an election under subsection 216(4) has been filed.' +
            'Where an election under subsection 216(4) has been filed, the income tax return must be filed ' +
            'within six months from the end of the tax year.' +
            'See Interpretation Bulletin IT-393, <i>Election Re: Tax on Rents and Timber Royalties Non-Residents</i>.' +
            '<br><br><b>Note</b><br>If both rental income and disposition of a taxable Canadian property' +
            ' have to be reported during the tax year, file a separate tax return under section 116 for capital gains.' +
            'Tick box 02 for the separate return.',
            labelWidth: '90%',
            num: '03'
          },
          {type: 'horizontalLine'},
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            labelEnd: true,
            width: '30px',
            label: '04 <b>Travelling corporation</b>: A corporation that operated in Canada' +
            ' for a limited period of time in a tax year and through which services were provided by an entertainer' +
            ' such as an actor (other than film and video services income, see box 12), a musician, or an athlete' +
            ' to a third party. For more information, see the article on artists and athletes in the appropriate' +
            ' tax convention. Complete and attach Schedule 20,' +
            '<i>Part XIV – Additional Tax on Non-Resident Corporations</i>.+' +
            '<br><br><b>Note</b><br> If the travelling corporation is claiming a treaty-based exemption' +
            ' for business carried on in Canada, tick box 01 rather than box 04.',
            labelWidth: '90%',
            num: '04'
          },
          {type: 'horizontalLine'},
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            labelEnd: true,
            width: '30px',
            label: '06 <b>Emigrant corporation</b>: A corporation that ceased to be resident in Canada' +
            ' and is subject to Part I and Part XIV taxes. See subsection 219.1(1), subsection 219.1(2),' +
            'subsection 250(5), and Interpretation Bulletin IT-451,Deemed Disposition and ' +
            'Acquisition on Ceasing to be or Becoming Resident in Canada',
            labelWidth: '90%',
            num: '06'
          },
          {type: 'horizontalLine'},
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            labelEnd: true,
            width: '30px',
            label: '07 <b>Canadian branch</b>: A corporation that earned income from a business carried on in Canada' +
            ' through a branch office. Complete and attach Schedule 20, <i>Part XIV – Additional Tax on Non-Resident' +
            ' Corporations</i>. See sections 115 and 219 and Interpretation Bulletin IT-137, ' +
            '<i>Additional Tax on Certain Corporations Carrying on Business in Canada</i>.' +
            '<br><br><b>Note</b><br> If the corporation is a non-resident insurance company, refer to box 10 or box 11.',
            labelWidth: '90%',
            num: '07'
          },
          {type: 'horizontalLine'},
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            labelEnd: true,
            width: '30px',
            label: '08 <b>Limited liability company (LLC)</b>: A corporation registered under the laws of a state of the ' +
            'United States that is generally recognized as a fiscally transparent entity that is not subject' +
            ' to US taxes, unless it has checked the box on the US tax return to be treated as a corporation' +
            ' for US tax purposes. If the LLC has checked the box on the US tax return, it must provide documentation ' +
            'from the IRS substantiating that it has elected to be taxed as a corporation. Where the LLC has not ' +
            'checked the box, Article IV(6) of the Canada-US Tax Convention (Convention) establishes the parameters' +
            ' under which a fiscally transparent LLC may claim the benefits of the Convention. Convention benefits' +
            ' claimed by a fiscally transparent LLC with respect to an amount of income, profit or gain will be' +
            ' permitted only if the amount is considered to be derived, pursuant to Article IV(6) of the Convention,' +
            ' by a person who is a resident of the United States and that person is a "qualifying person"' +
            ' under Article XXIX-A of the Convention or is entitled, with respect to the amount, to the benefits' +
            '  of the Convention pursuant to paragraph 3, 4, or 6 of Article XXIX-A of the Convention.' +
            ' Where the LLC is eligible to receive benefits under the Convention, the LLC must file Form NR 303,' +
            '<i>Declaration of Eligibility for Benefits under a Tax Treaty for a Hybrid Entity</i>. ' +
            'Where the LLC is not eligible for benefits under the Convention as described above, ' +
            'the LLC is not considered to be a resident of the US for the purposes of Article IV of the Convention. ' +
            'It is subject to Part I and Part XIV taxes and it does not qualify for the reduced tax rates ' +
            'on Canadian-source income. Complete and attach Schedule 20, ' +
            '<i>Part XIV – Additional Tax on Non-Resident Corporations</i>.',
            labelClass: 'fullLength',
            labelWidth: '90%',
            num: '08'
          },
          {type: 'horizontalLine'},
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            labelEnd: true,
            width: '30px',
            label: '09 <b>Authorized foreign bank</b>: A foreign bank that carried on business in Canada through ' +
            'branch offices and is subject to Part XIII.1 tax as per section 218.2. ' +
            'Provide and identify calculations as Schedule 92, ' +
            '<i>Part XIII.1 Tax – Additional Tax on Authorized Foreign Banks</i>.',
            labelClass: 'fullLength',
            labelWidth: '90%',
            num: '09'
          },
          {type: 'horizontalLine'},
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            labelEnd: true,
            width: '30px',
            label: '10 <b>Life insurance company</b>: A corporation that carried on a life insurance business in Canada' +
            ' at any time in the year. See sections 115, 138, and 219, and Interpretation Bulletin IT-137, ' +
            '<i>Additional Tax on Certain Corporations Carrying on Business in Canada.</i>',
            labelWidth: '90%',
            num: '10'
          },
          {type: 'horizontalLine'},
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            labelEnd: true,
            width: '30px',
            label: '11 <b> Other insurance company</b>: A corporation that carried on an insurance business other than ' +
            'life insurance in Canada at any time in the year. See sections 115, 138, and 219, and ' +
            'Interpretation Bulletin IT-137, <i>Additional Tax on Certain Corporations Carrying on Business in Canada</i>.,'
            + '<br><br><b>Note</b><br>If the insurance company carried on a life insurance business and another type of ' +
            'insurance business in Canada, tick box 10.',
            labelWidth: '90%',
            num: '11'
          },
          {type: 'horizontalLine'},
          {
            type: 'infoField',
            inputType: 'singleCheckbox',
            labelEnd: true,
            width: '30px',
            label: '12 <b>Actor corporation (section 216.1)</b>: A corporation electing to file a Canadian tax return ' +
            'under section 216.1 will be subject to the applicable taxes on the net Canadian-source acting income ' +
            'from film and video services rendered in Canada.',
            labelWidth: '90%',
            num: '12'
          }
        ]
      }
    ]
  };
})();