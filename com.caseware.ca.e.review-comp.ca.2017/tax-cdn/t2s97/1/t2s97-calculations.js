(function() {
  wpw.tax.create.calcBlocks('t2s97', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      var fieldIdsArr = ['01', '02', '03', '04', '06', '07', '08', '09', '10', '11', '12'];
      fieldIdsArr.forEach(function(fieldId) {
        if (!calcUtils.hasChanged(fieldId)) {
          field(fieldId).assign(false);
          wpw.tax.form('t2s97').field(fieldId).set(false);
          field(fieldId).disabled(false);
        }

        if (calcUtils.hasChanged(fieldId) && field(fieldId).get()) {
          field('300').assign(fieldId)
          fieldIdsArr.forEach(function(fieldId2) {
            if(fieldId!=fieldId2){
              field(fieldId2).assign(false);
              field(fieldId2).disabled(false);
            }
          });
        }
      });
    })
  });
})();
