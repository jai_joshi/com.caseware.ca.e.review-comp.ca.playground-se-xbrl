(function() {
  wpw.tax.global.tableCalculations.automobileSummary = {
    "100": {
      hasTotals: true,
      "maxLoop": 100000,
      "fixedRows": true,
      initRows: 0,
      // "linkedRepeatForm": "automobileInterest",
      "zeroCellRowsMessage": "No Interest and Other Non-Deductible Expenses incurred in the current fiscal year period",
      "columns": [
        {
          "header": "Description",
          cellClass: 'alignCenter',
          'type': 'text',
          "linkedFieldId": "105",
          "num": "120"
        },
        {
          "header": "Non-deductible interest and other expenses",
          colClass: 'std-input-width',
          "totalNum": "130",
          "total": true,
          "disabled": true,
          "linkedFieldId": "340"
        }]
    },
    "200": {
      hasTotals: true,
      "maxLoop": 100000,
      "fixedRows": true,
      initRows: 0,
      // "linkedRepeatForm": "automobileLeasing",
      "zeroCellRowsMessage": "No Leasing Costs and Other Non-Deductible Expenses incurred in the current fiscal year period",
      "columns": [{
        "header": "Description",
        cellClass: 'alignCenter',
        'type': 'text',
        "num": "220",
        "linkedFieldId": "405"
      },
        {
          "header": "Non-deductible leasing costs and other expenses",
          colClass: 'std-input-width',
          "totalNum": "230",
          "total": true,
          "disabled": true,
          "linkedFieldId": "270"
        }]
    }
  }
})();
