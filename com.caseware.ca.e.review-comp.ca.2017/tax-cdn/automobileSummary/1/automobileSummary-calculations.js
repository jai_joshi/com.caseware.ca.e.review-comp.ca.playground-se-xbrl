(function() {

  wpw.tax.create.calcBlocks('automobileSummary', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      calcUtils.filterLinkedTable('automobileInterest', '100', ['340'], function(f340) {return f340 > 0});
      calcUtils.filterLinkedTable('automobileLeasing', '200', ['270'], function(f270) {return f270 > 0});
      calcUtils.sumBucketValues('300', ['130', '230'], true);
    });
  })
})();
