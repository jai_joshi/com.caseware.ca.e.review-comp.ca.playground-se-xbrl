(function () {
  'use strict';

  wpw.tax.global.formData.automobileSummary = {
    formInfo: {
      abbreviation: 'automobileSummary',
      title: 'Non-Deductible Automobile Expenses - Summary',
      headerImage: 'cw',
      category: 'Workcharts'
    },
    sections: [
      {
        hideFieldset: true,
        rows: [
          {labelClass: 'fullLength'},
          {label: 'Summary of Interest and Other Non-Deductible Expenses', labelClass: 'bold center fullLength'},
          {
            type: 'table',
            num: '100'
          },
          {labelClass: 'fullLength'},
          {label: 'Summary of Leasing Costs and Other Non-Deductible Expenses', labelClass: 'bold center fullLength'},
          {
            type: 'table',
            num: '200'
          },
          {labelClass: 'fullLength'},
          {type: 'horizontalLine'},
          {
            'label': 'Non-deductible automobile expenses',
            'labelClass': 'bold',
            'layout': 'alignInput',
            'layoutOptions': {
              'showDots': true
            },
            'columns': [
              {
                'input': {
                  'num': '300'
                }
              }
            ]
          },
          {
            label:'(This amount is posted to line 122 of Schedule 1)'
          }
        ]
      }
    ]
  };
})();
