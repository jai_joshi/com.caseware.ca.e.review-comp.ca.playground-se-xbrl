(function() {
  'use strict';

  wpw.tax.global.formData.t2s345 = {
      'formInfo': {
        abbreviation: 'T2S345',
        'title': 'Additional Certificate Numbers for the Nova Scotia Film Industry Tax Credit',

        //'subTitle': '(1998 and later tax years)',
        'schedule': 'Schedule 345',
        //'code': 'Code 0901',
        'headerImage': 'canada-federal',
        'showCorpInfo': true,
        category: 'Nova Scotia Forms'
      },
      'sections': [
        {
          hideFieldset: true,
          'rows': [
            {
              label: 'For use by a corporation that has more than one certificate number for the Nova ' +
              'Scotia film industry tax credit.',
              labelClass: 'fullLength tabbed2'
            },
            {labelClass: 'fullLength'},
            {
              'type': 'table',
              'num': '900'
            },
            {
              label: 'Enter this amount on line 565 in Part 2 of Schedule 5.',
              labelClass: 'fullLength tabbed'
            }
          ]
        }
      ]
    };
})();
