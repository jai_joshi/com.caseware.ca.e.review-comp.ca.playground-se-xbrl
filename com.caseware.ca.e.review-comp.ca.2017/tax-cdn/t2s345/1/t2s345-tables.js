(function() {
  wpw.tax.global.tableCalculations.t2s345 = {
    "900": {
      "showNumbering": true,
      hasTotals: true,
      "columns": [
        {
          "header": "Certificate number",
          "num": "100",
          "tn": "100", "validate": {"or": [{"length": {"min": "9", "max": "9"}}, {"check": "isEmpty"}]},
          type: 'text'
        },
        {
          "header": "Amount of Nova Scotia film industry tax credit<br>$",
          "num": "200",
          "tn": "200", "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          "total": true,
          "totalNum": "300",
          "totalTn": "300",
          "totalMessage": "Total amount of the Nova Scotia film industry tax credit.",
          colClass: 'std-input-width'
        }
      ]
    }
  }
})();
