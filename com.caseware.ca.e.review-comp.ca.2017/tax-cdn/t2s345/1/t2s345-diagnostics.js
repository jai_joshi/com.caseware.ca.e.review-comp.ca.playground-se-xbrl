(function() {
  wpw.tax.create.diagnostics('t2s345', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    //Duplicate of t2s5 diagnostic 0050501
/*    diagUtils.diagnostic('0050501', common.prereq(common.check(['t2s5.565'], 'isNonZero'),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireOne([row[0]], 'isNonZero') &&
                tools.requireOne(tools.list(['t2s5.836']), 'isNonZero');
          });
        }));*/

    diagUtils.diagnostic('0050502', common.prereq(common.requireFiled('T2S345'),
        function(tools) {
          var table = tools.field('900');
          return tools.checkAll(table.getRows(), function(row) {
            var columns = [row[0], row[1]];
            if (tools.checkMethod(columns, 'isNonZero'))
              return tools.requireAll(columns, 'isNonZero');
            else return true;
          });
        }));

    //0050503 is implemented in t2s5-diagnostics

  });
})();
