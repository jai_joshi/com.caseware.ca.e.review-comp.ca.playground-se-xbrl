(function () {

  wpw.tax.create.formData('ratesAb', {
    formInfo: {
      abbreviation: 'ratesAb',
      title: 'Alberta Table of Rates and Values',
      showCorpInfo: true,
      description: [
        {type: 'heading', text: 'Alberta'}
      ],
      category: 'Rates Tables'
    },
    sections: [
      {
        'header': 'Schedule 20 - Alberta Charitable Donations and Gifts',
        'rows': [
          {
            label: 'Maximum deduction allowable',
            labelClass: 'bold'
          },
          {
            'label': 'Net income limitation',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '001'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'line 046',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '002'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        header: 'Schedule 18 - Alberta Dispositions of Capital Property',
        rows: [
          {
            'label': 'Inclusion Rate',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '003'
                }
              }
            ],
            'indicator': '%'
          }
        ]
      },
      {
        'header': 'Schedule 1 - Alberta Small Business Deduction',
        'rows': [
          {
            label: 'Small Business Deduction (SBD)',
            labelClass: 'bold'
          },
          {
            'label': 'April 1, 2001 to March 31, 2002',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '004',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'April 1, 2002 to March 31, 2003',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '005',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'April 1, 2003 to March 31, 2004',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '006',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'April 1, 2004 to March 31, 2006',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '007',
                  'decimals': 1
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'April 1, 2006 to March 31, 2007',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '008'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'April 1, 2007 to March 31, 2008',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '009'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'April 1, 2008 to March 31, 2009',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '010'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'April 1, 2009 to June 30, 2015',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '011'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'July 1, 2015 to December 31, 2016',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '012'
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'January 1, 2017 to current',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '013'
                }
              }
            ],
            'indicator': '%'
          },
        ]
      },
      {
        'header': 'Schedule 14 -  Alberta Cumulative Eligible Capital Deduction',
        rows: [
          {
            'label': 'Eligible CEC rate',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '014',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'Rate',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '015',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': '<b>Adjustment</b>'
          },
          {
            'label': 'before July 1, 1988',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '016',
                  'decimals': 2
                }
              }
            ]
          },
          {
            'label': 'after December 20, 2002',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '017',
                  'decimals': 5
                }
              }
            ]

          }
        ]
      },
      {
        'header': 'AT1 - Alberta Corporate Income Tax Return',
        'rows': [
          {
            label: 'General Corporate Tax Rate',
            labelClass: 'bold'
          },
          {
            'label': 'April 1, 2001 to March 31, 2002',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '018',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'April 1, 2002 to March 31, 2003',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '019',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'April 1, 2003 to March 31, 2004',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '020',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'April 1, 2004 to March 31, 2006',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '021',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'April 1, 2006 to March 31, 2007',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '022',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'April 1, 2007 to March 31, 2008',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '023',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'April 1, 2008 to March 31, 2009',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '024',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'April 1, 2009 to June 30, 2015',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '025',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'July 1, 2015 to December 31, 2016',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '026',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
          {
            'label': 'January 1, 2017 to current',
            'labelCellClass': 'indent',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'rightPadding': false
            },
            'columns': [
              {
                'input': {
                  'num': '027',
                  'decimals': 2
                }
              }
            ],
            'indicator': '%'
          },
        ]
      },
    ]
  });
})();
