(function() {
  wpw.tax.create.calcBlocks('ratesAb', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field) {
      field('001').assign(75);
      field('002').assign(25);
      field('003').assign(50);
      field('004').assign(8.5);
      field('005').assign(8.5);
      field('006').assign(8.5);
      field('007').assign(8.5);
      field('008').assign(7);
      field('009').assign(7);
      field('010').assign(7);
      field('011').assign(7);
      field('012').assign(9);
      field('013').assign(10);
      field('014').assign(3/4);
      field('015').assign(7);
      field('016').assign(1/2);
      field('017').assign(2/3);
      field('018').assign(13.5);
      field('019').assign(13);
      field('020').assign(12.5);
      field('021').assign(11.5);
      field('022').assign(10);
      field('023').assign(10);
      field('024').assign(10);
      field('025').assign(10);
      field('026').assign(12);
      field('027').assign(12);

    });
  });
})();
