(function() {
  wpw.tax.create.diagnostics('t2s308', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('3080001', common.prereq(common.check(['t2s5.504'], 'isNonZero'),
        common.requireFiled('T2S308')));

    diagUtils.diagnostic('303.160', common.prereq(common.requireFiled('T2S308'),
        function(tools) {
      return tools.field('145').require(function() {
        return this.get() < 75000;
      })
    }));

    diagUtils.diagnostic('308.200', common.prereq(common.requireFiled('T2S308'),
        function(tools) {
          var table = tools.field('200');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[5].isNonZero())
              return row[5].require(function() {
                return row[1].getKey('year') >= 2014;
              });
            else return true;
          });
        }));

    diagUtils.diagnostic('308.1000', common.prereq(common.requireFiled('T2S308'),
        function(tools) {
          var table = tools.field('1000');
          return tools.checkAll(table.getRows(), function(row) {
            if (row[3].isNonZero())
              return row[3].require(function() {
                return row[1].getKey('year') >= 2014;
              });
            else return true;
          });
        }));

  });
})();

