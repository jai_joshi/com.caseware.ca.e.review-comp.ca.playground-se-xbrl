(function() {

  wpw.tax.create.formData('t2s308', {
    formInfo: {
      abbreviation: 'T2S308',
      title: 'Newfoundland and Labrador Venture Capital Tax Credit',
      schedule: 'Schedule 308',
      code: 'Code 1402',
      formFooterNum: 'T2 SCH 308 E (17)',
      headerImage: 'canada-federal',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'You can claim a 30% non-refundable Newfoundland and Labrador venture capital' +
              ' tax credit under section 46.2 of the <i><i>Income Tax Act</i>, 2000</i> (Newfoundland and Labrador)' +
              ' based on the Venture Capital Tax Credit Regulations if your corporation is a qualifying investor and:',
              sublist: ['has received a venture capital tax credit certificate for the year; or',
                'has unused tax credits from any of the three previous years ending after 2013.']
            },
            {
              label: 'The credit applies to a qualifying investment acquired on or after ' +
              'January 1, 2014. The credit you earned in the year is used to reduce your ' +
              'Newfoundland and Labrador tax payable for that year. Any unused credit can be ' +
              'carried forward for seven years or carried back for three previous tax years ending after 2013.'
            },
            {
              label: 'The maximum lifetime credit that a qualifying investor can claim is $75,000' +
              ' based on one or more qualifying investments totalling $250,000.'
            },
            {
              label: 'Attach this schedule with a copy of your certificate to your T2 <i>Corporation Income Tax Return</i>.'
            }
          ]
        }
      ],
      category: 'Newfoundland and Labrador Forms'
    },
    sections: [
      {
        'header': 'Part 1 - Credit available for the year and credit available for carryforward',
        'rows': [
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Corporation\'s credit earned in the current year<br>' +
            '(total of all credit amounts from venture capital tax credit certificates received for the tax year)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '100',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '100'
                }
              }
            ]
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Unused credit at the end of the previous tax year',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '110'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'A'
                }
              }
            ]
          },
          {
            'label': 'Credit expired after seven tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '130',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '130'
                }
              },
              null
            ]
          },
          {
            'label': 'Unused credit at the beginning of this tax year (amount A <b>minus</b> line 130)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '135',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '135'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '140',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ]
          },
          {
            'label': '<b>Total credit available for the current tax year</b> (line 100 <b>plus</b> line 135)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '145'
                }
              }
            ],
            'indicator': 'B'
          },
          {
            'label': ' Credit claimed in the current year* (enter on line 504 of Schedule 5, <i>Tax Calculation Supplementary - Corporations</i>)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '150',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '150'
                }
              },
              null
            ]
          },
          {
            'label': ' Credit carried back to previous tax years (complete Part 2)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '160'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'C'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (line 150 <b>plus</b> amount C)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '170'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '180'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': '<b>Closing balance - credit available for carryforward</b> (amount B <b>minus</b> amount D)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '190',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '190'
                }
              }
            ]
          },
          {
            'label': '* This amount is equal to the least of: amount B and the Newfoundland and Labrador income tax otherwise payable for the year.',
            'labelClass': 'fullLength'
          },
          {
            labelClass: 'fullLength'
          }
        ]
      },
      {
        forceBreakAfter: true,
        'header': 'Part 2 - Request for carryback of credit',
        'rows': [
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'Complete this part to carry back a credit to any of the three previous tax years.' +
            ' The total amount available to carry back to the three previous tax years is amount B minus line 150 in Part 1.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The maximum amount of credit that can be applied to a prior tax year is the lesser of:'
          },
          {
            'label': '- the income tax payable in the prior tax year; and',
            'labelClass': 'tabbed'
          },
          {
            'label': '- $75,000 less the amount of any credit previously deducted in any tax year.',
            'labelClass': 'tabbed'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '200'
          },
          {
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 3 - Analysis of credit available for carryforward by year of origin',
        'rows': [
          {
            labelClass: 'fullLength'
          },
          {
            'label': 'You can complete this part to show all the credits from previous tax years available ' +
            'for carryforward, by year of origin. This will help you determine the amount ' +
            'of credit that could expire in future years.',
            'labelClass': 'fullLength'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '300'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'The amount available from the seventh previous tax year expires at the end of the current' +
            ' tax year. When you file your return for the next tax year, enter the ' +
            'expired amount on line 130 of Schedule 308 for that year.',
            'labelClass': 'fullLength'
          },
          {
            labelClass: 'fullLength'
          }
        ]
      },
      {
        'header': 'Historical Data and calculation for Newfoundland and Labrador Venture Capital Tax Credit',
        'rows': [
          {
            'type': 'table',
            'num': '1000'
          },
          {
            'label': '* Expired'
          },
          {
            'label': '** Expiring if not use this year'
          }
        ]
      }
    ]
  });
})();
