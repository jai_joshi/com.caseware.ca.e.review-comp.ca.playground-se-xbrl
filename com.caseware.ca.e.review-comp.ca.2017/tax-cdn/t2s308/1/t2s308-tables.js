(function() {

  var descriptionWidth = '220px';

  wpw.tax.create.tables('t2s308', {
    //Part 2
    200: {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {
          'width': descriptionWidth,
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'type': 'date',
          'disabled': true,
          header: 'Year End Date'
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-input-col-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          cellClass: 'alignCenter'
        },
        {
          colClass: 'std-input-width',
          'total': true,
          'totalNum': '904',
          'totalMessage': 'Total (Add lines 901, 902, and 903 and then enter amount on amount C of Part 1)'
        },
        {
          'type': 'none',
          colClass: 'std-padding-width'
        }],
      'cells': [
        {
          '0': {
            'label': '1st previous tax year'
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '901'
          },
          '5': {
            'num': '901', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '2nd previous tax year'
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '902'
          },
          '5': {
            'num': '902', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '0': {
            'label': '3rd previous tax year'
          },
          '3': {
            'label': ' Credit to be applied ',
            cellClass: 'alignCenter'
          },
          '4': {
            'tn': '903'
          },
          '5': {
            'num': '903', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline'
          }
        }
      ]
    },
    //Part 3
    300: {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      'columns': [
        {
          width: descriptionWidth,
          type: 'none'
        },
        {
          header: 'Year End Date',
          colClass: 'std-input-width',
          type: 'date',
          disabled: true
        },
        {
          type: 'none'
        },
        {
          header: 'Credit available for carryforward',
          colClass: 'std-input-width',
          total: true,
          totalNum: '350',
          totalMessage: 'Total (equals line 190 in Part 1)'
        },
        {
          colClass: 'std-padding-width',
          type: 'none'
        }
      ],
      'cells': [
        {0: {label: '7th previous tax year'}},
        {0: {label: '6th previous tax year'}},
        {0: {label: '5th previous tax year'}},
        {0: {label: '4th previous tax year'}},
        {0: {label: '3rd previous tax year'}},
        {0: {label: '2nd previous tax year'}},
        {0: {label: '1st previous tax year'}},
        {0: {label: 'Current tax year'}}
      ]
    },
    //History
    1000: {
      fixedRows: true,
      hasTotals: true,
      infoTable: true,
      columns: [
        {
          colClass: 'std-padding-width',
          type: 'none'
        },
        {
          type: 'date',
          header: '<b>Year of origin</b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          totalNum: '501',
          totalMessage: 'Total :',
          header: '<b>Opening balance</b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          totalNum: '502',
          totalMessage: ' ',
          header: '<b>Current year contribution</b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          disabled: true,
          totalNum: '505',
          totalMessage: ' ',
          header: '<b>Applied<b>'
        },
        {
          colClass: 'std-spacing-width',
          type: 'none'
        },
        {
          total: true,
          disabled: true,
          totalNum: '506',
          totalMessage: ' ',
          header: '<b>Balance to carry forward</b>'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        }
      ],
      cells: [
        {
          4: {
            label: '*'
          },
          5: {
            type: 'none'
          },
          7: {
            type: 'none'
          },
          9: {
            type: 'none',
            label: 'N/A'
          }
        },
        {
          5: {
            type: 'none'
          },
          10: {
            label: '**'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          5: {
            type: 'none'
          }
        },
        {
          3: {
            type: 'none'
          }
        }
      ]
    }
  })
})();
