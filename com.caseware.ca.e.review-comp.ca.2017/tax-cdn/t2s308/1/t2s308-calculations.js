(function() {

  function returnAmountApplied(limit, available) {
    var applied = 0;
    if (available == 0) {
      applied = available;
    }
    else {
      if (limit > available) {
        applied = available;
      }
      else {
        applied = limit;
      }
    }
    return applied;
  }

  wpw.tax.create.calcBlocks('t2s308', function(calcUtils) {

    //calcs years for all table
    calcUtils.calc(function(calcUtils, field, form) {
      var tableArrays = [200, 300, 1000];
      var summaryTable = field('tyh.200');
      tableArrays.forEach(function(tableNum) {
        field(tableNum).getRows().forEach(function(row, rowIndex) {
          if (tableNum == 200) {
            var year = Math.abs(rowIndex - 19);
            row[1].assign(summaryTable.cell(year, 6).get());
          }
          else if (tableNum == 300) {
            row[1].assign(summaryTable.getRow(rowIndex + 13)[6].get())
          }
          else {
            row[1].assign(summaryTable.getRow(rowIndex + 12)[6].get())
          }
        });
      });
    });

    calcUtils.calc(function(calcUtils, field) {
      //Part 3
      var summaryTable = field('1000');
      field('300').getRows().forEach(function(row, rowIndex) {
        row[3].assign(summaryTable.getRow(rowIndex)[9].get());
      });

      // historical data chart
      var limitOnCredit = field('145').get();
      summaryTable.getRows().forEach(function(row, rowIndex) {
        if (rowIndex == 0) {
          // to avoid the first row being calculated to total
          row[7].assign(0);
          row[9].assign(0);
        } else {
          row[7].assign(returnAmountApplied(limitOnCredit, Math.max(row[3].get() + row[5].get(), 0)));
          row[9].assign(Math.max(row[3].get() + row[5].get() - row[7].get(), 0));
          limitOnCredit -= row[7].get()
        }
      });
      summaryTable.cell(8, 5).assign(field('100').get());
    });

    //Part 1
    calcUtils.calc(function(calcUtils, field) {
      field('110').assign(field('1000').total(3).get());
      field('130').assign(field('1000').cell(0, 3).get());
      calcUtils.subtract('135', '110', '130');
      calcUtils.equals('140', '135');
      field('145').assign(field('100').get() + field('140').get());
      field('150').assign(Math.min(field('145').get() - field('904').get(), field('t2s307.240').get()));
      calcUtils.equals('160', '904');
      field('170').assign(field('150').get() + field('160').get());
      calcUtils.equals('180', '170');
      calcUtils.subtract('190', '145', '180');
    });
  });
})();
