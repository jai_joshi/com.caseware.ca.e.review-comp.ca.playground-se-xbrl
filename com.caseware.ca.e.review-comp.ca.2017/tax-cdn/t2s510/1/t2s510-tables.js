(function() {

  function getTableSummaryCols(totalNumArr) {
    return [
      {
        colClass: 'std-input-width',
        'type': 'date',
        'header': 'Year of origin',
        'disabled': true
      },
      {
        colClass: 'std-spacing-width',
        'type': 'none'
      },
      {
        'header': 'Opening balance',
        'total': true,
        'totalNum': totalNumArr[0],
        'totalMessage': 'Totals : '
      },
      {
        colClass: 'std-spacing-width',
        'type': 'none'
      },
      {
        'header': 'Current year credit',
        'total': true,
        'totalNum': totalNumArr[1],
        'totalMessage': ' '
      },
      {
        'type': 'none', colClass: 'std-spacing-width'
      },
      {
        'header': 'Transfers',
        'total': true,
        'totalNum': totalNumArr[2],
        'totalMessage': ' '
      },
      {
        colClass: 'std-spacing-width',
        'type': 'none'
      },
      {
        'header': 'Applied',
        'disabled': true,
        'total': true,
        'totalNum': totalNumArr[3],
        'totalMessage': ' '
      },
      {
        colClass: 'std-spacing-width',
        'type': 'none'
      },
      {
        'header': 'Balance to carry forward',
        'disabled': true,
        'total': true,
        'totalNum': totalNumArr[4],
        'totalMessage': ' '
      },
      {
        'type': 'none',
        colClass: 'std-padding-width'
      }
    ];
  }

  wpw.tax.global.tableCalculations.t2s510 = {
    '154': {
      'fixedRows': true,
      hasTotals: true,
      'width': '80%',
      'columns': [{
        'header': 'Year of origin',
        'type': 'none',
        'width': '20%'
      },
        {
          'header': 'Balance earned in a tax year ending before March 23, 2007 *',
          'total': true,
          'totalMessage': 'Total***'
        },
        {
          'header': 'Balance earned in a tax year ending after March 22, 2007 **',
          'total': true
        }],
      'cells': [{
        '0': {
          'label': '10th previous tax year'
        },
        '1': {
          'num': '810', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          'tn': '810'
        },
        '2': {
          'num': '820', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          'tn': '820'
        }
      },
        {
          '0': {
            'label': '9th previous tax year'
          },
          '1': {
            'num': '811', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '811'
          },
          '2': {
            'num': '821', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '821'
          }
        },
        {
          '0': {
            'label': '8th previous tax year'
          },
          '1': {
            'num': '812', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '812'
          },
          '2': {
            'num': '822', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '822'
          }
        },
        {
          '0': {
            'label': '7th previous tax year'
          },
          '1': {
            'num': '813', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '813'
          },
          '2': {
            'num': '823', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '823'
          }
        },
        {
          '0': {
            'label': '6th previous tax year'
          },
          '1': {
            'num': '814', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '814'
          },
          '2': {
            'num': '824', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '824'
          }
        },
        {
          '0': {
            'label': '5th previous tax year'
          },
          '1': {
            'num': '815', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '815'
          },
          '2': {
            'num': '825', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '825'
          }
        },
        {
          '0': {
            'label': '4th previous tax year'
          },
          '1': {
            'num': '816', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '816'
          },
          '2': {
            'num': '826', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '826'
          }
        },
        {
          '0': {
            'label': '3rd previous tax year'
          },
          '1': {
            'num': '817', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '817'
          },
          '2': {
            'num': '827', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '827'
          }
        },
        {
          '0': {
            'label': '2nd previous tax year'
          },
          '1': {
            'num': '818', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '818'
          },
          '2': {
            'num': '828', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '828'
          }
        },
        {
          '0': {
            'label': '1st previous tax year',
            'disabled': true
          },
          '1': {
            'type': 'none'
          },
          '2': {
            'num': '829', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '829'
          }
        }]
    },
    '200': {
      'fixedRows': true,
      'width': '50%',
      hasTotals: true,
      'alignment': 'center',
      'columns': [{
        'header': 'Year of origin',
        'width': '40%',
        'type': 'none'
      },
        {
          'header': 'CMT credit balance*',
          'total': true,
          'totalNum': '690',
          'totalMessage': 'Total **'
        }],
      'cells': [{
        '0': {
          'label': '10th previous tax year'
        },
        '1': {
          'num': '680', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
          'tn': '680'
        }
      },
        {
          '0': {
            'label': '9th previous tax year'
          },
          '1': {
            'num': '681', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '681'
          }
        },
        {
          '0': {
            'label': '8th previous tax year'
          },
          '1': {
            'num': '682', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '682'
          }
        },
        {
          '0': {
            'label': '7th previous tax year'
          },
          '1': {
            'num': '683', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '683'
          }
        },
        {
          '0': {
            'label': '6th previous tax year'
          },
          '1': {
            'num': '684', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '684'
          }
        },
        {
          '0': {
            'label': '5th previous tax year'
          },
          '1': {
            'num': '685', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '685'
          }
        },
        {
          '0': {
            'label': '4th previous tax year'
          },
          '1': {
            'num': '686', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '686'
          }
        },
        {
          '0': {
            'label': '3rd previous tax year'
          },
          '1': {
            'num': '687', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '687'
          }
        },
        {
          '0': {
            'label': '2nd previous tax year'
          },
          '1': {
            'num': '688', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '688'
          }
        },
        {
          '0': {
            'label': '1st previous tax year'
          },
          '1': {
            'num': '689', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'tn': '689'
          }
        }]
    },
    '300': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {
          colClass: 'std-input-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'textAlign': 'center',
          'type': 'none'
        },
        {
          'type': 'none',
          'textAlign': 'center'
        },
        {
          colClass: 'small-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          'textAlign': 'center'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          'textAlign': 'center'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none',
          'textAlign': 'center'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Amount from line 520'
          },
          '1': {
            'num': '301'
          },
          '2': {
            'label': 'x',
            'inputType': 'none'
          },
          '3': {
            'label': 'Number of days in the tax year before July 1, 2010',
            'labelClass': 'fullLength center',
            cellClass: 'singleUnderline'
          },
          '4': {
            'num': '302',
            cellClass: 'singleUnderline'
          },
          '5': {
            'label': 'x'
          },
          '6': {
            'label': '4%'
          },
          '7': {
            'label': '='
          },
          '8': {
            'num': '304'
          },
          '9': {
            'label': '1'
          }
        },
        {
          '1': {
            'inputType': 'none'
          },
          '3': {
            'label': 'Number of days in the tax year',

            'inputType': 'none',
            'labelClass': 'fullLength center'
          },
          '4': {
            'num': '303'
          },
          '8': {
            'inputType': 'none'
          }
        },
        {
          '0': {
            'label': 'Amount from line 520'
          },
          '1': {
            'num': '305'
          },
          '2': {
            'label': 'x',
            'inputType': 'none'
          },
          '3': {
            'label': 'Number of days in the tax year after June 30, 2010',
            'labelClass': 'fullLength center',
            cellClass: 'singleUnderline'
          },
          '4': {
            'num': '306',
            cellClass: 'singleUnderline'
          },
          '5': {
            'label': 'x'
          },
          '6': {
            'label': '2.7%'
          },
          '7': {
            'label': '='
          },
          '8': {
            'num': '308'
          },
          '9': {
            'label': '2'
          }
        },
        {
          '1': {
            'inputType': 'none'
          },
          '3': {
            'label': 'Number of days in the tax year',

            'inputType': 'none',
            'labelClass': 'fullLength center'
          },
          '4': {
            'num': '307'
          },
          '8': {
            'inputType': 'none'
          }
        }]
    },
    '310': {
      'infoTable': true,
      'fixedRows': true,
      'columns': [
        {
          'type': 'none',
          'textAlign': 'center'
        },
        {
          'width': '45px',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          'width': '70px',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          'width': '300px',
          'type': 'none'
        }],
      'cells': [{
        '0': {
          'label': 'Ontario taxable income ****',
          'labelClass': 'fullLength center',
          cellClass: 'singleUnderline'
        },
        '2': {
          'num': '311',
          decimals: 2,
          cellClass: 'singleUnderline'
        },
        '3': {
          'label': '=',
          'labelClass': 'center'
        },
        '4': {
          'num': '312',
          decimals: 5,
          'labelClass': 'doubleUnderline'
        }
      },
        {
          '0': {
            'label': 'Taxable income *****',

            'labelClass': 'fullLength center'
          },
          '2': {
            'num': '313'
          },
          '4': {
            'inputType': 'none'
          }
        }]
    },
    '1998': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-col-width',
          'textAlign': 'left'
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [
        {
          '1': {
            'tn': '381'
          },
          '2': {
            'num': '381', "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
            type: 'text'
          },
          '4': {
            'tn': '382'
          },
          '5': {
            'num': '382', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '1': {
            'tn': '383'
          },
          '2': {
            'num': '383', "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
            type: 'text'
          },
          '4': {
            'tn': '384'
          },
          '5': {
            'num': '384', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '1': {
            'tn': '385'
          },
          '2': {
            'num': '385', "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
            type: 'text'
          },
          '4': {
            'tn': '386'
          },
          '5': {
            'num': '386', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '1': {
            'tn': '387'
          },
          '2': {
            'num': '387', "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
            type: 'text'
          },
          '4': {
            'tn': '388'
          },
          '5': {
            'num': '388', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
          }
        },
        {
          '1': {
            'tn': '389'
          },
          '2': {
            'num': '389', "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
            type: 'text'
          },
          '4': {
            'tn': '390'
          },
          '5': {
            'num': '390', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'labelClass': 'underline'
          }
        }]
    },
    '1999': {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-col-width',
          'textAlign': 'left'
        },
        {
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-width'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        },
        {
          colClass: 'std-input-col-width',
          'type': 'none'
        }
      ],
      'cells': [{
        '1': {
          'tn': '281'
        },
        '2': {
          'num': '281', "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
          type: 'text'
        },
        '4': {
          'tn': '282'
        },
        '5': {
          'num': '282', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]}
        }
      },
        {
          '1': {
            'tn': '283'
          },
          '2': {
            'num': '283', "validate": {"or": [{"length": {"min": "1", "max": "60"}}, {"check": "isEmpty"}]},
            type: 'text'
          },
          '4': {
            'tn': '284'
          },
          '5': {
            'num': '284', "validate": {"or": [{"matches": "^[.\\d]{1,13}$"}, {"check": "isEmpty"}]},
            'labelClass': 'underline'
          }
        }]
    },
    '2001': {
      'fixedRows': true,
      hasTotals: true,
      'infoTable': true,
      columns: getTableSummaryCols(['1198', '1199', '1249', '1299', '1350']),
      'cells': [
        {
          '3': {
            'label': '*'
          },
          '4': {
            'inputType': 'none'
          },
          '6': {
            'inputType': 'none'
          },
          '8': {
            'inputType': 'none'
          },
          '10': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          },
          '11': {
            'label': '**'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '2': {
            'inputType': 'none',
            'labelClass': 'underline'
          },
          '4': {
            'labelClass': 'underline'
          },
          '6': {
            'inputType': 'none',
            'labelClass': 'underline'
          },
          '8': {
            'inputType': 'none',
            'labelClass': 'underline'
          },
          '10': {
            'labelClass': 'underline'
          }
        },
      ]
    },
    '2004': {
      'fixedRows': true,
      hasTotals: true,
      'infoTable': true,
      columns: getTableSummaryCols(['2198', '2199', '2249', '2299', '2350']),
      'cells': [{
        '3': {
          'label': '*'
        },
        '4': {
          'inputType': 'none'
        },
        '6': {
          'inputType': 'none'
        },
        '8': {
          'inputType': 'none'
        },
        '10': {
          'inputType': 'none'
        }
      },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '2': {
            'inputType': 'none',
            'labelClass': 'underline'
          },
          '4': {
            'num': '2197',
            'labelClass': 'underline'
          },
          '6': {
            'inputType': 'none',
            'labelClass': 'underline'
          },
          '8': {
            'inputType': 'none',
            'labelClass': 'underline'
          },
          '10': {
            'num': '2310',
            'labelClass': 'underline'
          }
        }
      ]
    },
    '2006': {
      'fixedRows': true,
      hasTotals: true,
      'infoTable': true,
      columns: getTableSummaryCols(['3198', '3199', '3249', '3299', '3350']),
      'cells': [{
        '3': {
          'label': '*'
        },
        '4': {
          'inputType': 'none'
        },
        '6': {
          'inputType': 'none'
        },
        '8': {
          'inputType': 'none'
        },
        '10': {
          'inputType': 'none'
        }
      },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '2': {
            'inputType': 'none',
            'labelClass': 'underline'
          },
          '4': {
            'num': '3197',
            'labelClass': 'underline'
          },
          '6': {
            'inputType': 'none',
            'labelClass': 'underline'
          },
          '8': {
            'inputType': 'none',
            'labelClass': 'underline'
          },
          '10': {
            'num': '3320',
            'labelClass': 'underline'
          }
        }
      ]
    },
    '2008': {
      'fixedRows': true,
      hasTotals: true,
      'infoTable': true,
      columns: getTableSummaryCols(['4198', '4199', '4249', '4299', '4350']),
      'cells': [{
        '3': {
          'label': '*'
        },
        '4': {
          'inputType': 'none'
        }
      },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '4': {
            'inputType': 'none'
          }
        },
        {
          '2': {
            'inputType': 'none',
            'labelClass': 'underline'
          },
          '4': {
            'num': '4197',
            'labelClass': 'underline'
          },
          '6': {
            'inputType': 'none',
            'labelClass': 'underline'
          },
          '8': {
            'inputType': 'none',
            'labelClass': 'underline'
          },
          '10': {
            'num': '4310',
            'labelClass': 'underline'
          }
        }
      ]
    }
  }
})();