(function() {
  'use strict';

  wpw.tax.global.formData.t2s510 = {
      formInfo: {
        abbreviation: 'T2S510',
        title: 'Ontario Corporate Minimum Tax',
        //subTitle: '(2009 and later tax years)',
        headerImage: 'canada-federal',
        schedule: 'Schedule 510',
        code: 'Code 0904',
        formFooterNum: 'T2 SCH 510 E (14)',
        showCorpInfo: true,
        description: [
          {
            type: 'list',
            items: [
              'File this schedule if the corporation is subject to Ontario corporate minimum tax (CMT).' +
              ' CMT is levied under section 55 of the Taxation Act, 2007 (Ontario), referred to as the "Ontario Act".',
              'Complete Part 1 to determine if the corporation is subject to CMT for the tax year',
              'A corporation not subject to CMT in the tax year is still required to file this schedule ' +
              'if it is deducting a CMT credit, has a CMT credit carryforward, or has a CMT loss carryforward ' +
              'or a current year CMT loss.',
              'A corporation that has Ontario special additional tax on life insurance corporations (SAT) ' +
              'payable in the tax year must complete Part 4 of this schedule even if it is not subject to CMT ' +
              'for the tax year.',
              {
                label: 'A corporation is exempt from CMT if, throughout the tax year, it was one of the following:',
                sublist: [
                  '1) a corporation exempt from income tax under section 149 of the federal <i>Income Tax Act</i>;',
                  '2) a mortgage investment corporation under subsection 130.1(6) of the federal Act;',
                  '3) a deposit insurance corporation under subsection 137.1(5) of the federal Act;',
                  '4) a congregation or business agency to which section 143 of the federal Act applies;',
                  '5) an investment corporation as referred to in subsection 130(3) of the federal Act; or',
                  '6) a mutual fund corporation under subsection 131(8) of the federal Act.'
                ]
              },
              'File this schedule with the <i>T2 Corporation Income Tax Return</i>.'
            ]
          }
        ],
        category: 'Ontario Forms'
      },
      sections: [
        {
          'header': 'Part 1 - Determination of CMT applicability',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total assets of the corporation at the end of the tax year*',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '112'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '112'
                  }
                }
              ]
            },
            {
              'label': 'Share of total assets from partnership(s) and joint venture(s)*',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '114'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '114'
                  }
                }
              ]
            },
            {
              'label': 'Total assets of associated corporations (amount from line 450 on Schedule 511)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '116'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '116'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total assets (total of lines 112 to 116)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '100'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total revenue of the corporation for the tax year**',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '142'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '142'
                  }
                }
              ]
            },
            {
              'label': 'Share of total revenue from partnership(s) and joint venture(s)**',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '144'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '144'
                  }
                }
              ]
            },
            {
              'label': 'Total revenue of associated corporations (amount from line 550 on Schedule 511)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '146'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '146'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Total revenue (total of lines 142 to 146)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '101'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'The corporation is subject to CMT if:'
            },
            {
              'label': '– for tax years ending before July 1, 2010, the total assets at the end of the year' +
              ' of the corporation or the associated group of corporations are more than $5,000,000, or the total ' +
              'revenue for the year of the corporation or the associated group of corporations is ' +
              'more than $10,000,000.',
              'labelClass': 'fullLength'
            },
            {
              'label': '– for tax years ending after June 30, 2010, the total assets at the end of the year of the ' +
              'corporation or the associated group of corporations are equal to or more than $50,000,000, ' +
              'and the total revenue for the year of the corporation or the associated group of corporations is ' +
              'equal to or more than $100,000,000.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If the corporation is not subject to CMT, do not complete the remaining parts unless the' +
              ' corporation is deducting a CMT credit, or has a CMT credit carryforward, a CMT loss carryforward,' +
              ' a current year CMT loss, or SAT payable in the year.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* Rules for total assets',
              'labelClass': 'bold'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '- Report total assets according to generally accepted accounting principles, adjusted so that consolidation and equity methods are not used',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': '- Do not include unrealized gains and losses on assets and foreign currency gains and losses on assets that are included in net income for accounting purposes but not in income for corporate income tax purposes.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': '- The amount on line 114 is determined at the end of the last fiscal period of the partnership or joint venture that ends in the tax year of the corporation. Add the proportionate share of the assets of the partnership(s) and joint venture(s), and deduct the recorded asset(s) for the investment in partnerships and joint ventures.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': '- A corporation\'s share in a partnership or joint venture is determined under paragraph 54(5)(b) of the Ontario Act, and if the partnership or joint venture had no income or loss, is calculated as if the partnership\'s or joint venture\'s incomewere $1 million. For a corporation with an indirect interest in a partnership or joint venture, determine the corporation\'s share according to paragraph 54(5)(c) of the Ontario Act.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': ' ** Rules for total revenue',
              'labelClass': 'bold'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '- Report total revenue in accordance with generally accepted accounting principles, adjusted so that consolidation and equity methods are not used',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': '- If the tax year is less than 51 weeks, <b> multiply </b> the total revenue of the corporation or the partnership, whichever applies, by 365 and <b> divide </b> by the number of days in the tax year',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': '– The amount on line 144 is determined for the partnership or joint venture fiscal period' +
              ' that ends in the tax year of the corporation. If the partnership or joint venture has 2 or more' +
              ' fiscal periods ending in the filing corporation\'s tax year, <b>multiply</b> the sum of the total revenue ' +
              'for each of the fiscal periods by 365 and <b>divide</b> by the total number of days in all the fiscal periods.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': ' - A corporation\'s share in a partnership or joint venture is determined under paragraph 54(5)(b) of the Ontario Act and, if the partnership or joint venture had no income or loss, is calculated as if the partnership\'s or joint venture\'s income were $1 million. For a corporation with an indirect interest in a partnership or joint venture, determine the corporation\'s share according to paragraph 54(5)(c) of the Ontario Act.',
              'labelClass': 'fullLength tabbed'
            }
          ]
        },
        {
          'header': 'Part 2 – Adjusted net income/loss for CMT purposes',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Net income/loss per financial statements *',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '210',
                    'validate': {
                      'or': [
                        {
                          'matches': '^-?[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '210'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Add </b> (to the extent reflected in income/loss):'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Provision for current income taxes/cost of current income taxes',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '220',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '220'
                  }
                },
                null
              ]
            },
            {
              'label': 'Provision for deferred income taxes (debits)/cost of future income taxes',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '222',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '222'
                  }
                },
                null
              ]
            },
            {
              'label': 'Equity losses from corporations',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '224',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '224'
                  }
                },
                null
              ]
            },
            {
              'label': 'Financial statement loss from partnerships and joint ventures',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '226',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '226'
                  }
                },
                null
              ]
            },
            {
              'label': 'Dividends deducted on financial statements (subsection 57(2) of the Ontario Act), excluding dividends paid by credit unions under subsection 137(4.1) of the federal Act',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '230',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '230'
                  }
                },
                null
              ]
            },
            {
              'label': '<b> Other additions </b> (see note below):'
            },
            {
              'label': 'Share of adjusted net income of partnerships and joint ventures **',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '228',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '228'
                  }
                },
                null
              ]
            },
            {
              'label': 'Total patronage dividends received, not already included in net income/loss',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '232',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '232'
                  }
                },
                null
              ]
            },
            {
              'type': 'table',
              'num': '1999'
            },
            {
              'label': 'Subtotal',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '102'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '103'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'A'
            },
            {
              'label': '<b> Deduct </b> (to the extent reflected in income/loss):'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Provision for recovery of current income taxes/benefit of current income taxes',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '320',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '320'
                  }
                },
                null
              ]
            },
            {
              'label': 'Provision for deferred income taxes (credits)/benefit of future income taxes',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '322',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '322'
                  }
                },
                null
              ]
            },
            {
              'label': 'Equity income from corporations',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '324',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '324'
                  }
                },
                null
              ]
            },
            {
              'label': 'Financial statement income from partnerships and joint ventures',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '326',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '326'
                  }
                },
                null
              ]
            },
            {
              'label': 'Dividends deductible under section 112, section 113, or subsection 138(6) of the Federal Act',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '330',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '330'
                  }
                },
                null
              ]
            },
            {
              'label': 'Dividends not taxable under section 83 of the federal Act (from Schedule 3)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '332',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '332'
                  }
                },
                null
              ]
            },
            {
              'label': 'Gain on donation of listed security or ecological gift',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '340',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '340'
                  }
                },
                null
              ]
            },
            {
              'label': 'Accounting gain on transfer of property to a corporation under section 85 or 85.1 of the federal Act ***',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '342',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '342'
                  }
                },
                null
              ]
            },
            {
              'label': 'Accounting gain on transfer of property to/from a partnership under section 85 or 97 of the federal Act ****',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '344',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '344'
                  }
                },
                null
              ]
            },
            {
              'label': 'Accounting gain on disposition of property under subsection 13(4), subsection 14(6), or section 44 of the federal Act *****',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '346',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '346'
                  }
                },
                null
              ]
            },
            {
              'label': 'Accounting gain on a windup under subsection 88(1) of the federal Act or an amalgamation under section 87 of the federal Act',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '348',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '348'
                  }
                },
                null
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Other deductions </b> (see note below):'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Share of adjusted net loss of partnerships and joint ventures **',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '328',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '328'
                  }
                },
                null
              ]
            },
            {
              'label': 'Tax payable on dividends under subsection 191.1(1) of the federal Act <b> multiplied </b> by 3',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '334',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '334'
                  }
                },
                null
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Interest deducted/deductible under paragraph 20(1)(c) or (d) of the federal Act, not already included in net income/loss',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '336',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '336'
                  }
                },
                null
              ]
            },
            {
              'label': 'Patronage dividends paid (fromSchedule 16) not already included in net income/loss',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '338',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '338'
                  }
                },
                null
              ]
            },
            {
              'type': 'table',
              'num': '1998'
            },
            {
              'label': 'Subtotal',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '104'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '105'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'B'
            },
            {
              'label': 'Adjusted net income/loss for CMT purposes (line 210 <b> plus </b> amount A <b> minus </b> amount B)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^-?[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '490'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '490'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If the amount on line 490 is positive and the corporation is subject to CMT  as determined in Part 1, enter the amount on line 515 in Part 3.',
              'labelClass': 'fullLength'
            },
            {
              'label': 'If the amount on line 490 is negative, enter the amount on line 760 in Part 7 (enter as a positive amount).'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Note',
              'labelClass': 'bold'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'In accordance with <i> Ontario Regulation 37/09</i>, when calculating net income for CMT purposes, accounting income should be adjusted to:',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '- exclude unrealized gains and losses due to mark-to-market changes or foreign currency changes on specified mark-to-market property (assets only);',
              'labelClass': 'fullLength'
            },
            {
              'label': '– include realized gains and losses on the disposition of specified mark-to-market property' +
              ' not already included in the accounting income, if the property is not a capital property or is a ' +
              'capital property disposed in the year or in a previous tax year ended after March 22, 2007. ',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '"Specified mark-to-market property" is defined in subsection 54(1) of the Ontario Act.'
            },
            {
              'label': 'These rules also apply to partnerships. A corporate partner\'s share of a partnership\'s adjusted income flows through on a proportionate basis to the corporate partner.',
              'labelClass': 'fullLength'
            },
            {
              'labelCLass': 'fullLength'
            },
            {
              'label': '* Rules for net income/loss',
              'labelClass': 'bold'
            },
            {
              'label': '– Banks must report net income/loss as per the report accepted by the Superintendent of Financial Institutions under the federal <i> Bank Act</i>, adjusted so consolidation and equity methods are not used.',
              'labelClass': 'fullLength'
            },
            {
              'label': '– Life insurance corporations must report net income/loss as per the report accepted by the' +
              ' federal Superintendent of Financial Institutions or equivalent provincial insurance regulator, ' +
              'before SAT and adjusted so consolidation and equity methods are not used. If the life insurance ' +
              'corporation is resident  in Canada and carries on business in and outside of Canada,' +
              '<b> multiply</b> the net income/loss by the ratio of the Canadian reserve liabilities <b>divided</b> by ' +
              'the total reserve liability. The reserve liabilities are calculated in accordance with' +
              ' Regulation 2405(3) of the federal Act.',
              'labelClass': 'fullLength'
            },
            {
              'label': '– Other corporations must report net income/loss in accordance with generally accepted accounting principles, except that consolidation and equity methods must not be used. When the equity method has been used for accounting purposes, equity losses and equity income are removed from book income/loss on lines 224 and 324 respectively.',
              'labelClass': 'fullLength'
            },
            {
              'label': '– Corporations, other than insurance corporations, should report net income from line 9999 of the GIFI (Schedule 125) on line 210.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': ' ** The share of the adjusted net income of a partnership or joint venture is calculated as if the partnership or joint venture were a corporation and the tax year of the partnership orjoint venture were its fiscal period. For a corporation with an indirect interest in a partnership through one or more partnerships, determine the corporation\'s share according to clause 54(5)(c) of the Ontario Act',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': ' *** A joint election will be considered made under subsection 60(1) of the Ontario Act if ' +
              'there is an entry on line 342, and an election has been made for transfer of property to a ' +
              'corporation under subsection 85(1) of the federal Act.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': ' **** A joint election will be considered made under subsection 60(2)  of the Ontario Act if there is an entry on line 344, and an election has been made under subsection 85(2) or 97(2)of the Federal Act',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': ' ***** A joint election will be considered made under subsection 61(1) of the Ontario Act if there is an entry on line 346, and an election has been made under subsection 13(4) or 14(6) and/or section 44 of the federal Act.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': 'For more information on how to complete this part, see the <i> T2 Corporation - Income Tax Guide </i>'
            }
          ]
        },
        {
          'header': 'Part 3 - CMT payable',
          'rows': [
            {
              'label': 'Adjusted net income for CMT purposes (line 490 in Part 2, if positive)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '515',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '515'
                  }
                },
                null
              ]
            },
            {
              'label': 'Deduct:',
              'labelClass': 'bold'
            },
            {
              'label': 'CMT loss available (amount R from Part 7)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '106'
                  }
                },
                null,
                null
              ]
            },
            {
              'label': '<b> Minus</b>: Adjustment for an acquisition of control*',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '518',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '518'
                  }
                },
                null,
                null
              ]
            },
            {
              'label': 'Adjusted CMT loss available',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '107'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '108'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'C'
                  }
                }
              ]
            },
            {
              'label': 'Net income subject to CMT calculation (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '520',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '520'
                  }
                },
                null
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '300'
            },
            {
              'label': 'Subtotal (amount 1 <b> plus </b> amount 2)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '309'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '3'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Gross CMT: amount on line 3 above X OAF **',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '540'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '540'
                  }
                }
              ]
            },
            {
              'label': 'Deduct:',
              'labelClass': 'bold'
            },
            {
              'label': 'Foreign tax credit for CMT purposes ***',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '550',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '550'
                  }
                }
              ]
            },
            {
              'label': 'CMT after foreign tax credit deduction (line 540 <b>minus</b> line 550) (if negative, enter "0") ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '118'
                  }
                }
              ],
              'indicator': 'D'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Deduct:',
              'labelClass': 'bold'
            },
            {
              'label': 'Ontario corporate income tax payable before CMT credit (amount F6 from Schedule 5)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '119'
                  }
                }
              ]
            },
            {
              'label': 'Net CMT payable (if negative, enter "0")',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '120'
                  }
                }
              ],
              'indicator': 'E'
            },
            {
              'label': 'Enter amount E on line 278 of Schedule 5, <i> Tax Calculation Supplementary - Corporations </i> and complete Part 4.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* Enter the portion of CMT loss available that exceeds the adjusted net income  for the tax year from carrying on a business before the acquisition of control. See subsection 58(3) of the Ontario Act.',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'labelClass': 'fulLLength'
            },
            {
              'label': '*** Enter "0" on line 550 for life insurance corporations as they are not eligible for this deduction. For all other corporations, enter the cumulative total of amount J for the province of Ontario from Part 9 of Schedule 21 on line 550.',
              'labelClass': 'fullLength tabbed2'
            }
          ]
        },
        {
          'header': ' ** Calculation of the Ontario allocation factor (OAF)',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If the provincial or territorial jurisdiction entered on line 750 of the T2 return is "Ontario," enter "1" on line F.',
              'labelClass': 'fullLength'
            },
            {
              'label': 'If the provincial or territorial jurisdiction entered on line 750 of the T2 return is "multiple," complete the following calculation, and enter the result on line F:',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '310'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Ontario allocation factor',
              'labelClass': 'bold',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'decimals': 5,
                    'num': '124'
                  }
                }
              ],
              'indicator': 'F'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '**** Enter the amount allocated to Ontario from column F in Part 1 of Schedule 5. If the taxable income is nil, calculate the amount in column F as if the taxable income were $1,000.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': ' ***** Enter the taxable income amount from line 360 or amount Z of the T2 return, whichever applies. If the taxable income is nil, enter "1,000".',
              'labelClass': 'fullLength tabbed'
            }
          ]
        },
        {
          'header': 'CMT credit available for carryforward (credit that can be carried forward over 20 years) Summary',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '2001'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* Note that this credit expires only at the beginning of fiscal period. Therefore, this amount is updated to line 600 of Schedule 510.',
              'labelClass': 'fullLength'
            },
            {
              'label': '** Note that this credit expires only at the subsequent fiscal period. Consequently, this amount will be updated to line 600 of Schedule 510 for the subsequent fiscal year.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'CMT credit available for carryforward (credit that can be carried forward over 10 years) Summary',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '2004'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* Note that this credit expires only at the beginning of fiscal period. Therefore, this amount is updated to line 600 of Schedule 510.',
              'labelClass': 'fullLength'
            },
            {
              'label': '** Note that this credit expires only at the subsequent fiscal period. Consequently, this amount will be updated to line 600 of Schedule 510 for the subsequent fiscal year.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 4 - Calculation of CMT credit carryforward',
          'spacing': 'R_tn2',
          'rows': [
            {
              'label': 'CMT credit carryforward at the end of the previous tax year *',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '125'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'G'
                  }
                }
              ]
            },
            {
              'labelCLass': 'fullLength'
            },
            {
              'label': 'Deduct:',
              'labelClass': 'bold'
            },
            {
              'label': 'CMT credit expired *',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '600',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '600'
                  }
                },
                null
              ]
            },
            {
              'label': 'CMT credit carryforward at the beginning of the current tax year * (see note below)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '126',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '620',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '620'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Add:',
              'labelClass': 'bold'
            },
            {
              'label': 'CMT credit carryforward balances transferred on an amalgamation or the windup of a subsidiary (see note below)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '650'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '650'
                  }
                }
              ]
            },
            {
              'label': 'CMT credit available for the tax year (amount on line 620 plus amount on line 650)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '127'
                  }
                }
              ],
              'indicator': 'H'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Deduct:',
              'labelClass': 'bold'
            },
            {
              'label': 'CMT credit deducted in the current tax year (amount P from Part 5)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '128'
                  }
                }
              ],
              'indicator': 'I'
            },
            {
              'label': 'Subtotal (amount H <b> minus </b> amount I)',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '129'
                  }
                }
              ],
              'indicator': 'J'
            },
            {
              'label': 'Add:',
              'labelClass': 'bold'
            },
            {
              'label': 'Net CMT payable (amount E from Part 3)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '130'
                  }
                },
                null
              ]
            },
            {
              'label': 'SAT payable (amount O from Part 6 of Schedule 512)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '131'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '132'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '133'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'K'
            },
            {
              'label': 'CMT credit carryforward at the end of the tax year (amount J <b> plus </b> amount K)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '670'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '670'
                  }
                }
              ],
              'indicator': 'L'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* For the first harmonized T2 return filed with a tax year that includes days in 2009:',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': '- do not enter an amount on line G or line 600',
              'labelClass': 'tabbed2'
            },
            {
              'label': '- for line 620, enter the amount from line 2336 of Ontario CT23 Schedule 101, <i>Corporate Minimum Tax (CMT)</i> , for the last tax year that ended in 2008. ',
              'labelClass': 'fullLength tabbed2'
            },
            {
              'label': 'For other tax years, enter on line G the amount from line 670 of Schedule 510 from the previous tax year.',
              'labelClass': 'tabbed fullLength'
            },
            {
              'label': '<b> Note: </b> If you entered an amount on line 620 or line 650, complete Part 6.'
            }
          ]
        },
        {
          'header': 'Part 5 - Calculation of CMT credit deducted from Ontario corporate income tax payable',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'CMT credit available for the tax year (amount H from Part 4)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '134'
                  }
                }
              ],
              'indicator': 'M'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Ontario corporate income tax payable before CMT credit (amount F6 from Schedule 5)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '135'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '1'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'For a corporation that is not a life insurance corporation:'
            },
            {
              'label': 'CMT after foreign tax credit deduction (amount D from Part 3)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '136'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '2'
                  }
                },
                null
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'For a life insurance corporation:'
            },
            {
              'label': 'Gross CMT (line 540 from Part 3)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '137'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '3'
                  }
                },
                null
              ]
            },
            {
              'label': 'Gross SAT (line 460 from Part 6 of Schedule 512)',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '138'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '4'
                  }
                },
                null
              ]
            },
            {
              'label': 'The <b> greater </b> of amounts 3 and 4',
              'labelClass': '',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '139'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '5'
                  }
                },
                null
              ]
            },
            {
              'label': '<b> Deduct: </b> line 2 or line 5, whichever applies',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '140'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': '6'
                  }
                }
              ]
            },
            {
              'label': 'Subtotal (if negative, enter "0")',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '141'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '158'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'N'
            },
            {
              'label': 'Ontario corporate income tax payable before CMT credit (amount F6 from Schedule 5)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '143'
                  }
                },
                null
              ]
            },
            {
              'label': 'Deduct:',
              'labelClass': 'bold'
            },
            {
              'label': 'Total refundable tax credits excluding Ontario qualifying environmental trust tax credit'
            },
            {
              'label': ' (amount J6 <b>minus</b> line 450 from Schedule 5) ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '159'
                  }
                },
                null
              ]
            },
            {
              'label': 'Subtotal (if negative, enter "0")',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '145'
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '148'
                  },
                  'padding': {
                    'type': 'symbol',
                    'data': 'glyphicon-triangle-right'
                  }
                }
              ],
              'indicator': 'O'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'CMT credit deducted in the current tax year (least of amounts M, N, and O)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '147'
                  }
                }
              ],
              'indicator': 'P'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Enter amount P on line 418 of Schedule 5 and on line I in Part 4 of this schedule.'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Is the corporation claiming a CMT credit earned before an acquisition of control?',
              'tn': '675',
              'num': '675',
              'type': 'infoField',
              'inputType': 'radio',
              'labelLength': '80%'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'If you answered <b>yes</b> to the question at line 675, the CMT credit deducted in the current' +
              ' tax year may be restricted. For information on how the deduction may be restricted, ' +
              'see subsections 53(6) and (7) of the Ontario Act.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 6 - Analysis of CMT credit available for carryforward by year of origin',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Complete this part if:'
            },
            {
              'label': '- the tax year includes January 1, 2009; or',
              'labelCellClass': 'indent'
            },
            {
              'label': ' - the previous tax year-end is deemed to be December 31, 2008, under subsection 249(3) of the federal Act.',
              'labelCellClass': 'indent'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '200'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* CMT credit that was earned (by the corporation, predecessors of the corporation, and subsidiaries wound up into the corporation) in each of the previous 10 tax years and has not been deducted.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '** Must equal the total of the amounts entered on lines 620 and 650 in Part 4.',
              'labelClass': 'fullLength tabbed'
            }
          ]
        },
        {
          'header': 'CMT loss available for carryforward (loss that can be carried forward over 20 years) Summary',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '2006'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* Note that this credit expires only at the beginning of fiscal period. Therefore, this amount is updated to line 700 of Schedule 510.',
              'labelClass': 'fullLength'
            },
            {
              'label': '** Note that this credit expires only at the subsequent fiscal period. Consequently, this amount will be updated to line 700 of Schedule 510 for the subsequent fiscal year.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'CMT loss available for carryforward (loss that can be carried forward over 10 years) Summary',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '2008'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* Note that this credit expires only at the beginning of fiscal period. Therefore, this amount is updated to line 700 of Schedule 510.',
              'labelClass': 'fullLength'
            },
            {
              'label': '** Note that this credit expires only at the subsequent fiscal period. Consequently, this amount will be updated to line 700 of Schedule 510 for the subsequent fiscal year.',
              'labelClass': 'fullLength'
            }
          ]
        },
        {
          'header': 'Part 7 - Calculation of CMT loss carryforward',
          'spacing': 'R_tn2',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'CMT loss carryforward at the end of the previous tax year *',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '149'
                  }
                },
                {
                  'padding': {
                    'type': 'text',
                    'data': 'Q'
                  }
                }
              ]
            },
            {
              'label': 'Deduct:',
              'labelClass': 'fullLength bold'
            },
            {
              'label': 'CMT loss expired *',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '700',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '700'
                  }
                },
                null
              ]
            },
            {
              'label': 'CMT loss carryforward at the beginning of the tax year * (see note below)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'underline': 'double',
                  'input': {
                    'num': '150',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  }
                },
                {
                  'underline': 'double',
                  'input': {
                    'num': '720',
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    }
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '720'
                  }
                }
              ]
            },
            {
              'label': 'Add:',
              'labelClass': 'bold'
            },
            {
              'label': 'CMT loss transferred on an amalgamation under section 87 of the federal Act ** (see note below)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '750'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '750'
                  }
                }
              ]
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'CMT loss available (line 720 <b> plus </b> line 750)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '151'
                  }
                }
              ],
              'indicator': 'R'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Deduct:',
              'labelClass': 'bold'
            },
            {
              'label': 'CMT loss deducted against adjusted net income for the tax year (lesser of line 490 (if positive) and line C in Part 3) ',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '152'
                  }
                }
              ]
            },
            {
              'label': 'Subtotal (if negative, enter "0")',
              'labelCellClass': 'alignRight',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': false,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'num': '153'
                  }
                }
              ],
              'indicator': 'S'
            },
            {
              'label': 'Add:',
              'labelClass': 'bold'
            },
            {
              'label': 'Adjusted net loss for CMT purposes (amount from line 490 in Part 2, if <b> negative) </b> (enter as a positive amount)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '760'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '760'
                  }
                }
              ]
            },
            {
              'label': 'CMT loss carryforward balance at the end of the tax year (amount S <b> plus </b> line 760)',
              'layout': 'alignInput',
              'layoutOptions': {
                'indicatorColumn': true,
                'showDots': true,
                'rightPadding': false
              },
              'columns': [
                {
                  'input': {
                    'validate': {
                      'or': [
                        {
                          'matches': '^[.\\d]{1,13}$'
                        },
                        {
                          'check': 'isEmpty'
                        }
                      ]
                    },
                    'num': '770'
                  },
                  'padding': {
                    'type': 'tn',
                    'data': '770'
                  }
                }
              ],
              'indicator': 'T'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* For the first harmonized T2 return filed with a tax year that includes days in 2009:'
            },
            {
              'label': ' - do not enter an amount on line Q or line 700;',
              'labelClass': 'tabbed fullLength'
            },
            {
              'label': ' - for line 720, enter the amount from line 2214 of Ontario CT23 Schedule 101, <i> Corporate Minimum Tax (CMT)</i> , for the last tax year that ended in 2008. ',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': 'For other tax years, enter on line Q the amount from line 770 of Schedule 510 from the previous tax year.'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '** Do not include an amount from a predecessor corporation if it was controlled at any time before the amalgamation by any of the other predecessor corporations.',
              'labelClass': 'fullLength'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '<b> Note: </b> If you entered an amount on line 720 or line 750, complete Part 8.'
            }
          ]
        },
        {
          'header': 'Part 8 - Analysis of CMT loss available for carryforward by year of origin',
          'rows': [
            {
              'labelClass': 'fullLength'
            },
            {
              'label': 'Complete this part if:'
            },
            {
              'label': ' - the tax year includes January 1, 2009; or',
              'labelCellClass': 'indent'
            },
            {
              'label': ' - the previous tax year-end is deemed to be December 31, 2008, under subsection 249(3) of the federal Act',
              'labelClass': 'fullLength tabbed'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'type': 'table',
              'num': '154'
            },
            {
              'labelClass': 'fullLength'
            },
            {
              'label': '* Adjusted net loss for CMT purposes that was earned (by the corporation, by subsidiaries wound up into or amalgamated with the corporation before March 22, 2007, and by other predecessors of the corporation) in each of the previous 10 tax years that ended before March 23, 2007, and has not been deducted.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': '** Adjusted net loss for CMT purposes that was earned (by the corporation and its predecessors, but not by a subsidiary predecessor) in each of the previous 20 tax years that ended after March 22, 2007, and has not been deducted.',
              'labelClass': 'fullLength tabbed'
            },
            {
              'label': '*** The total of these two columns must equal the total of the amounts entered on lines 720 and 750.',
              'labelClass': 'fullLength tabbed'
            }
          ]
        }
      ]
    };
})();
