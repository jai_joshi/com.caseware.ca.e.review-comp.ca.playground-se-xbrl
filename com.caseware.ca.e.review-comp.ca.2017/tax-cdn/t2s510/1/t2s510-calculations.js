(function() {
  wpw.tax.create.calcBlocks('t2s510', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //to get year for table summary
      var summaryTable = field('tyh.200');
      var tableArray = [2001, 2004, 2006, 2008];
      tableArray.forEach(function(tableNum) {
        field(tableNum).getRows().forEach(function(row, rowIndex) {
          if (tableNum == 2004 || tableNum == 2008) {
            row[0].assign(summaryTable.getRow(rowIndex + 10)[6].get())
          }
          else {
            row[0].assign(summaryTable.getRow(rowIndex)[6].get())
          }
        });
      });

      // Part 1 calculations //
      field('112').assign(
          calcUtils.gifiValue('t2s100', '104', '2599') -
          calcUtils.gifiValue('t2s100', '103', '2245') -
          calcUtils.gifiValue('t2s100', '103', '2250')
      );
      field('112').source(field('T2S100.2599'));
      field('116').assign(field('t2s511.450').get() || 0);
      field('116').source(field('t2s511.450'));

      calcUtils.sumBucketValues('100', ['112', '114', '116']);

      var sumFor142 = 0;
      calcUtils.allRepeatForms('t2s125').forEach(function(t2s125) {
        sumFor142 +=
            calcUtils.sumRepeatGifiValue('t2s125', 150, 8299) +
            calcUtils.sumRepeatGifiValue('t2s125', 400, 9659)
      });

      var fiscalPeriod = calcUtils.field('CP.Days_Fiscal_Period').get();
      if (fiscalPeriod < 358) {
        sumFor142 = Math.round((sumFor142 * 365) / fiscalPeriod);
      }
      field('142').assign(sumFor142);
      field('142').source(field('t2s125.8299'));

      field('146').assign(field('t2s511.550').get() || 0);
      calcUtils.sumBucketValues('101', ['142', '144', '146']);

      //todo: calcUtils for rules for total assets and total revenue as stated in the form

      // Part 2 Calculations //
      calcUtils.getGlobalValue('210', 'T2S1', '100');
      var currentIncomeTax = field('T2S1.101');
      if (currentIncomeTax.get() < 0) {
        field('320').assign(currentIncomeTax.get() * -1);
        field('320').source(currentIncomeTax)
      }
      else {
        field('220').assign(currentIncomeTax.get());
        field('220').source(currentIncomeTax);
      }
      var deferredIncomeTax = field('T2S1.102');
      if (deferredIncomeTax.get() < 0) {
        field('322').assign(deferredIncomeTax.get() * -1);
        field('322').source(deferredIncomeTax)
      }
      else {
        field('222').assign(deferredIncomeTax.get());
        field('222').source(deferredIncomeTax);
      }
      calcUtils.getGlobalValue('224', 'T2S1', '110');
      field('226').assign(field('T2S1.248').get() + field('T2S1.249').get());
      field('226').source(field('T2S1.248'));
      var sum102 = [220, 222, 224, 226, 230, 228, 232, 282, 284];
      calcUtils.sumBucketValues('102', sum102);
      calcUtils.equals('103', '102');
      calcUtils.getGlobalValue('324', 'T2S1', '306');
      field('326').assign(field('T2S1.348').get() + field('T2S1.349').get());
      calcUtils.getGlobalValue('330', 'T2S3', '240');//TODO: ask Alnoor about other deductible dividends
      calcUtils.getGlobalValue('332', 'T2S1', '402');
      field('340').assign(field('T2S6.895').get() + field('T2S6.896').get());
      // calcUtils.getGlobalValue('334', '', '');//TODO: from S43 not available
      var sum104 = [320, 322, 324, 326, 330, 332, 340, 342, 344, 346, 348, 328, 334, 336, 338, 382, 384, 386, 388, 390];
      calcUtils.sumBucketValues('104', sum104);
      calcUtils.equals('105', '104');
      field('490').assign(field('210').get() + field('103').get() - field('105').get());
      if (((calcUtils.dateCompare.greaterThan(field('t2j.061').get(), wpw.tax.date(2010, 6, 30))) &&
          (field('100').get() > field('ratesOn.624').get()) && (field('101').get() > field('ratesOn.625').get())) ||
          ((calcUtils.dateCompare.lessThan(field('t2j.061').get(), wpw.tax.date(2010, 7, 1))) &&
          (field('100').get() > field('ratesOn.621').get()) && (field('101').get() > field('ratesOn.622').get()))) {
        if (field('490').get() >= 0) {
          field('515').assign(field('490').get());
        }
        else {
          field('515').assign(0);
        }
      }
      else {
        field('515').assign(0);
      }
      field('760').assign(field('490').get() * -1);
      //Part 3 calculations //
      calcUtils.equals('106', '151');
      calcUtils.subtract('107', '106', '518');
      calcUtils.equals('108', '107');
      calcUtils.subtract('520', '515', '108');
      calcUtils.equals('301', '520');
      field('302').assign(0);
      calcUtils.getGlobalValue('303', 'CP', 'Days_Fiscal_Period');
      calcUtils.equals('305', '520');
      calcUtils.getGlobalValue('306', 'CP', 'Days_Fiscal_Period');
      calcUtils.getGlobalValue('307', 'CP', 'Days_Fiscal_Period');
      field('304').assign(field('301').get() * field('302').get() / field('303').get() * 4 / 100);
      field('308').assign(field('305').get() * field('306').get() / field('307').get() * 27 / 1000);
      calcUtils.sumBucketValues('309', ['304', '308']);
      //OAF calculations //
      //Field 311
      if (field('cp.750').get() == 'MJ') {
        var s5totalB = field('T2S5.129').get();
        var s5totalD = field('T2S5.169').get();
        var s5colC = field('T2S5.998').cell(7, 2).get();
        var s5colE = field('T2S5.998').cell(7, 4).get();
        var taxableIncome = field('T2J.371').get();
        if (taxableIncome == 0) {
          if (s5totalB != 0) {
            s5colC = (s5colC * 1000) / s5totalB;
          }
          if (s5totalD != 0) {
            s5colE = (s5colE * 1000) / s5totalD;
          }
        }
        else {
          if (s5totalB != 0) {
            s5colC = (s5colC * taxableIncome) / s5totalB;
          }
          if (s5totalD != 0) {
            s5colE = (s5colE * taxableIncome) / s5totalD;
          }
        }
        if (s5totalD != 0 && s5totalB != 0) {
          field('311').assign((s5colC + s5colE) / 2);
        }
        else {
          field('311').assign((s5colC + s5colE));
        }
      }
      else {
        field('311').assign(0);
      }
      //taxable income field 313 calculation//
      if (field('cp.750').get() == 'MJ') {
        if (field('T2J.371').get() == 0)
          field('313').assign(1000);
        else
          calcUtils.getGlobalValue('313', 'T2J', '371')
      }
      else {
        field('313').assign(0);
      }
      //allocation factor field 124 calc//
      if (field('313').get() > 0) {
        field('312').assign(field('311').get() / field('313').get())
      }
      else {
        field('312').assign(0)
      }
      if (field('cp.750').get() == 'ON') {
        field('124').assign(1)
      }
      else {
        field('124').assign(field('312').get())
      }
      calcUtils.multiply('540', ['309', '124']);
      //from s21 :For Ontario, if the corporation is not a life insurance corporation, also enter amount J on
      //line 550 of Schedule 510, Ontario Corporate Minimum Tax
      var isLifeInsCorp = (form('CP').field('147').get() == '11');

      if (!isLifeInsCorp) {
        field('550').assign(field('t2s21w.240ON').get());
      }
      calcUtils.subtract('118', '540', '550');
      calcUtils.getGlobalValue('119', 'T2S5', '496');
      calcUtils.subtract('120', '118', '119');

      //CMT credit available for carryforward (credit that can be carried forward over 20 years) Summary
      field('2001').getRows().forEach(function(row) {
        row[10].assign(
            row[2].get() +
            row[4].get() +
            row[6].get() -
            row[8].get()
        );
      });
      //CMT credit available for carryforward (credit that can be carried forward over 10 years) Summary
      field('2004').getRows().forEach(function(row) {
        row[10].assign(
            row[2].get() +
            row[4].get() +
            row[6].get() -
            row[8].get()
        );
      });

      // Part 4 Calculations //
      calcUtils.sumBucketValues('125', ['1198', '2198']);
      field('600').assign(field('2001').cell(0, 2).get() + field('2004').cell(0, 2).get());
      calcUtils.subtract('126', '125', '600');
      calcUtils.equals('620', '126');
      calcUtils.sumBucketValues('650', ['1249', '2249']);
      calcUtils.sumBucketValues('127', ['620', '650']);
      field('128').assign(field('147').get());
      calcUtils.subtract('129', '127', '128');
      calcUtils.equals('130', '120');
      // calcUtils.getGlobalValue('131', '', '');//TODO: from S512 not available yet
      calcUtils.sumBucketValues('132', ['130', '131']);
      calcUtils.equals('133', '132');
      calcUtils.sumBucketValues('670', ['129', '133']);

      // Part 5 Calculations //
      calcUtils.equals('134', '127');
      calcUtils.getGlobalValue('135', 'T2S5', '496');
      calcUtils.equals('136', '118');

      //TODO: Life insurance corporation efile not supported - add switch in CP for calc when adding support
      //if (field('cp.lifeInsuranceCorp').get() == 1) {
      //calcUtils.equals('137', '540');
      //calcUtils.getGlobalValue('138', '', '');//TODO: from S512 not avaiable yet
      //calcUtils.max('139', ['137', '138']);
      //field('140').assign(field('139').get());
      //}
      //else {
      field('137').assign(0);
      //calcUtils.getGlobalValue('138', '', '');//TODO: from S512 not avaiable yet
      calcUtils.max('139', ['137', '138']);
      field('140').assign(field('136').get());
      //}

      calcUtils.subtract('141', '135', '140');
      calcUtils.equals('158', '141');
      calcUtils.getGlobalValue('143', 'T2S5', '496');
      calcUtils.getGlobalValue('lineJ6', 'T2S5', '490');
      calcUtils.getGlobalValue('line450', 'T2S5', '450');
      calcUtils.subtract('159', 'lineJ6', 'line450');
      calcUtils.subtract('145', '143', '159');
      calcUtils.equals('148', '145');
      calcUtils.min('147', ['134', '158', '148']);

      //CMT loss available for carryforward- 20 years
      field('2006').getRows().forEach(function(row) {
        row[10].assign(
            row[2].get() +
            row[4].get() +
            row[6].get() -
            row[8].get()
        );
      });

      //CMT loss available for carryforward- 10 years
      field('2008').getRows().forEach(function(row) {
        row[10].assign(
            row[2].get() +
            row[4].get() +
            row[6].get() -
            row[8].get()
        );
      });

      // Part 7 calculations //
      calcUtils.sumBucketValues('149', ['3198', '4198']);
      field('700').assign(field('2006').cell(0, 2).get() + field('2008').cell(0, 2).get());
      calcUtils.subtract('150', '149', '700');
      calcUtils.equals('720', '150');
      calcUtils.sumBucketValues('750', ['3249', '4249']);
      calcUtils.sumBucketValues('151', ['720', '750']);

      if (field('490').get() >= 0) {
        calcUtils.min('152', ['490', '108']);
      }

      calcUtils.subtract('153', '151', '152');

      field('760').assign(Math.abs(Math.min(field('490').get(), 0)));

      calcUtils.sumBucketValues('770', ['153', '760']);

    });

  });
})();
