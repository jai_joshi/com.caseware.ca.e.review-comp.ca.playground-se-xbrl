(function() {
  wpw.tax.create.diagnostics('t2s510', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('5100001', common.prereq(common.or(
        common.check('t2s5.278', 'isNonZero'),
        common.check('t2s5.418', 'isNonZero')),
        common.requireFiled('T2S510')));

    diagUtils.diagnostic('5100002', common.prereq(common.and(
        common.requireFiled('T2S510'),
        common.check('t2s5.278', 'isNonZero')),
        common.or(
            common.check([
              '210', '220', '222', '224', '226', '230', '228', '232', '282', '284',
              '320', '322', '324', '326', '330', '332', '340', '342', '344', '346',
              '348', '328', '334', '336', '338', '382', '384', '386', '388', '390'
            ], 'isNonZero'),
            common.or(
                common.requireFiled('T2S1'),
                common.requireFiled('T2S125'))
        )));

    diagUtils.diagnostic('5100003', common.prereq(function(tools) {
      return wpw.tax.utilities.dateCompare.lessThan(tools.field('cp.tax_end').get(), {day: 1, month: 7, year: 2010}) &&
          tools.field('cp.prov_residence').get()['ON'] == true ||
          (tools.field('cp.prov_residence').get()['MJ'] && tools.field('t2s5.013').get() == true) &&
          tools.field('cp.2270').get() == '2' &&
          tools.field('cp.122').get() != '1' &&
          tools.field('cp.122').get() != '3' &&
          tools.field('cp.122').get() != '9' &&
          tools.field('cp.122').get() != '10' &&
          tools.field('cp.122').get() != '11' &&
          ((((tools.field('t2s125.8299').getKey('0') || 0) + (tools.field('t2s125.9659').getKey('0') || 0) - (tools.field('t2s125.8232').getKey('0') || 0)) > 10000000) ||
              ((tools.field('t2s100.2599').getKey('0') || 0) - (tools.field('t2s100.2245').getKey('0') || 0) - (tools.field('t2s100.2250').getKey('0') || 0)) > 5000000);
    }, common.requireFiled('T2S510')));

    diagUtils.diagnostic('5100004', common.prereq(common.requireFiled('T2S510'),
        common.and(
            common.check(['112', '114', '116'], 'isNonZero'),
            common.check(['142', '144', '146'], 'isNonZero'))));

    diagUtils.diagnostic('5100005', common.prereq(function(tools) {
      return wpw.tax.utilities.dateCompare.greaterThan(tools.field('cp.tax_end').get(), {
            day: 30,
            month: 6,
            year: 2010
          }) &&
          tools.field('cp.prov_residence').get()['ON'] == true &&
          tools.field('cp.2270').get() == '2' &&
          tools.field('cp.122').get() != '1' &&
          tools.field('cp.122').get() != '3' &&
          tools.field('cp.122').get() != '9' &&
          tools.field('cp.122').get() != '10' &&
          tools.field('cp.122').get() != '11' &&
          ((((tools.field('t2s125.8299').getKey('0') || 0) + (tools.field('t2s125.9659').getKey('0') || 0) - (tools.field('t2s125.8232').getKey('0') || 0)) > 100000000) ||
              ((tools.field('t2s100.2599').getKey('0') || 0) - (tools.field('t2s100.2245').getKey('0') || 0) - (tools.field('t2s100.2250').getKey('0') || 0)) > 50000000);
    }, common.requireFiled('T2S510')));
    //TODO: 5100005: Check for aggregation behaviour re. repeat forms on s125 and how we pull from the repeat forms in diagnostics
  });
})();
