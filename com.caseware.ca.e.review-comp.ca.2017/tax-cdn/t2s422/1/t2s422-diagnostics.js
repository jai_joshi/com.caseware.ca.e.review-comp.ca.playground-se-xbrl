(function() {
  wpw.tax.create.diagnostics('t2s422', function(paramObj) {
    var diagUtils = paramObj.diagUtils;
    var common = diagUtils.common;

    diagUtils.diagnostic('4220001', common.prereq(common.check(['t2s5.671'], 'isNonZero'), common.requireFiled('T2S422')));

    diagUtils.diagnostic('4220002', common.prereq(common.requireFiled('T2S422'),
        function(tools) {
          var table = tools.field('099');
          return tools.checkAll(table.getRows(), function(row) {
            return tools.requireAll([row[0], row[1]], 'isNonZero');
          })
        }));

    diagUtils.diagnostic('4220003', common.prereq(common.requireFiled('T2S422'),
        common.check(['220', '222', '230', '235', '240', '245', '250'], 'isNonZero', true)));

    diagUtils.diagnostic('4220004', common.prereq(common.requireFiled('T2S422'),
        common.check(['301', '302', '303', '310', '311', '312', '313'], 'isNonZero', true)));

    diagUtils.diagnostic('4220005', common.prereq(common.and(
        common.requireFiled('T2S422'),
        common.check('800', 'isNonZero')),
        common.or(
            common.check('405', 'isNonZero'),
            common.check(['505', '515', '516', '520', '521', '525', '530'], 'isNonZero'))));

    diagUtils.diagnostic('4220010', common.check(['t2s5.671', '800'], 'isZero'));

    diagUtils.diagnostic('422.535', common.prereq(common.and(
        common.requireFiled('T2S422'),
        common.check('530', 'isNonZero')),
        common.check('535', 'isNonZero')));

    diagUtils.diagnostic('422.590', common.prereq(common.and(
        common.requireFiled('T2S422'),
        common.check(['480', '505', '515', '516', '520', '525'], 'isNonZero')),
        common.check('590', 'isNonZero')));

    diagUtils.diagnostic('422.610', common.prereq(common.and(
        common.requireFiled('T2S422'),
        common.check(['590', '620'], 'isPositive')),
        common.check('610', 'isNonZero')));
  });
})();
