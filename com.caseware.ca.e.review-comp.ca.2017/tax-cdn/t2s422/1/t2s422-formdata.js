(function() {

  wpw.tax.create.formData('t2s422', {
    formInfo: {
      abbreviation: 'T2S422',
      isRepeatForm: true,
      repeatFormData: {
        titleNum: '301'
      },
      title: 'British Columbia Film and Television Tax Credit',
      schedule: 'Schedule 422',
      code: 'Code 1601',
      showCorpInfo: true,
      headerImage: 'canada-federal',
      agencyUseOnlyBox: {
        text: 'Code number ',
        textAbove: '',
        tn: '422',
        agencyBoxClass: 'cra-box-small',
        headerClass: 'header-margin',
        textClass: 'text-margin',
        tnPosition: 'none'
      },
      formFooterNum: 'T1196 E (16)',
      description: [
        {
          type: 'list',
          items: [
            {
              label: 'Use this form to claim the following credits under the <i>Income Tax Act</i> (British Columbia):',
              sublist: [
                'basic tax credit (section 80), complete Parts 1, 2, 3, 4, 5, 7 and 12;',
                'regional tax credit (section 81.1), complete Part 8;',
                'distant location regional tax credit (section 81.11), complete Part 9;',
                'digital animation, visual effects and post-production tax credit (DAVE) (section 81.2), ' +
                'complete Parts 6 and 11; and',
                'film training tax credit (section 82), complete Part 10.'
              ]
            },
            {
              label: 'To claim any of the above credits, include the following with the ' +
              '<i>T2 Corporation Income Tax Return</i>:',
              sublist: [
                'eligibility certificate (or a copy);',
                'if the production was completed in the tax year, include a copy of the completion certificate ' +
                'and a copy of the audited statement of production costs and notes provided to Creative BC; and',
                'a completed copy of this form for each eligible production. If a film or video production is ' +
                'intended for television broadcast as a series, all the episodes constituting one cycle ' +
                'of the series are to be considered a single production.'
              ]
            }
          ]
        }
      ],
      category: 'British Columbia Forms'
    },
    sections: [
      {
        hideFieldset: true,
        'rows': [
          {labelClass: 'fullLength'},
          {
            label: '<i>Freedom of Information and Protection of Privacy Act (FOIPPA)</i>',
            labelClass: 'fullLength bold'
          },
          {
            'label': 'The personal information on this form is collected for the purpose of administering the ' +
            '<i>Income Tax Act</i> (British Columbia) under the authority of paragraph 26(a) of the FOIPPA. Questions' +
            ' about the collection or use of this information can be directed to the Manager, Intergovernmental' +
            ' Relations, PO Box 9444 Stn Prov Govt, Victoria BC V8W 9W8. (Telephone: Victoria at <b>250-387-3332</b>' +
            ' or toll-free at <b>1-877-387-3332</b> and ask to be re-directed). <br>Email: ITBTaxQuestions@gov.bc.ca',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'}
        ]
      },
      {
        'header': 'Part 1 - Contact Information',
        'rows': [
          {
            'type': 'table',
            'num': '099'
          }
        ]
      },
      {
        forceBreakAfter: true,
        'header': 'Part 2 - Identifying the film or video production',
        'rows': [
          {
            'type': 'splitTable',
            'fieldAlignRight': true,
            'side1': [
              {
                'label': 'Title of production'
              },
              {
                'type': 'infoField',
                'num': '301',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '175'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '301'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Title of production (from eligibility certificate if different than line 301)',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'num': '304',
                'validate': {
                  'or': [
                    {
                      'length': {
                        'min': '1',
                        'max': '175'
                      }
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '304'
              }
            ],
            'side2': [
              {
                'label': 'Date principal photography began',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'inputType': 'date',
                'num': '302',
                'tn': '302',
                'labelWidth': '65%'
              },
              {
                'type': 'horizontalLine'
              },
              {
                'label': 'Eligibility certificate number',
                'labelClass': 'fullLength'
              },
              {
                'type': 'infoField',
                'inputType': 'custom',
                'num': '303',
                'validate': {
                  'or': [
                    {
                      'matches': '^-?[.\\d]{3,5}$'
                    },
                    {
                      'check': 'isEmpty'
                    }
                  ]
                },
                'tn': '303',
                'format': [
                  '{N3}',
                  '{N4}',
                  '{N5}'
                ]
              }
            ]
          },
          {
            'type': 'horizontalLine'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'type': 'infoField',
            'label': 'Was the production completed in the tax year?',
            'inputType': 'radio',
            'canClear': true,
            'num': '310',
            'tn': '310'
          },
          {
            'type': 'infoField',
            'label': 'Has a completion certificate been obtained?',
            'inputType': 'radio',
            'canClear': true,
            'num': '311',
            'tn': '311'
          },
          {
            'type': 'infoField',
            'label': 'Is the production an interprovincial co-production?',
            'inputType': 'radio',
            'canClear': true,
            'num': '312',
            'tn': '312'
          },
          {
            'type': 'infoField',
            'label': 'Is the production a treaty co-production?',
            'inputType': 'radio',
            'canClear': true,
            'num': '313',
            'tn': '313'
          }
        ]
      },
      {
        'header': 'Part 3 - Eligibility',
        'rows': [
          {
            'label': 'If you answer <b>yes</b> to any of the questions in this part, you <b>are not eligible</b> ' +
            'for a British Columbia (BC) film and television tax credit.',
            'labelClass': 'fullLength'
          },
          {'labelClass': 'fullLength'},
          {
            'type': 'infoField',
            'label': 'Was the corporation at any time in the tax year controlled directly or indirectly in any manner ' +
            'whatever by one or more persons, all or part of whose taxable income was exempt from tax under section 27 ' +
            'of the <i>Income Tax Act</i> (British Columbia) or Part I of the federal <i>Income Tax Act</i>?',
            'inputType': 'radio',
            'init': '1',
            'num': '220',
            'tn': '220'
          },
          {
            'type': 'infoField',
            'label': 'Was all or part of the corporation\'s taxable income at any time in the tax year exempt from tax ' +
            'under section 27 of the <i>Income Tax Act</i> (British Columbia) or Part I of the federal Act?',
            'inputType': 'radio',
            'init': '2',
            'num': '222',
            'tn': '222'
          },
          {'labelClass': 'fullLength'},
          {
            'label': 'Was the corporation at any time in the tax year:'
          },
          {
            'type': 'infoField',
            'label': 'a) a prescribed labour-sponsored venture capital corporation for the purposes of section 127.4 of the federal Act?',
            'inputType': 'radio',
            'init': '2',
            'num': '230',
            'tn': '230'
          },
          {
            'type': 'infoField',
            'label': 'b) a small business venture capital corporation registered under section 3 of the ' +
            '<i>Small Business Venture Capital Act</i>?',
            'inputType': 'radio',
            'init': '2',
            'num': '235',
            'tn': '235'
          },
          {
            'type': 'infoField',
            'label': 'c) a corporation that has an employee share ownership plan registered under section 2 of ' +
            'the <i>Employee Investment Act</i>?',
            'inputType': 'radio',
            'init': '2',
            'num': '240',
            'tn': '240'
          },
          {
            'type': 'infoField',
            'label': 'd) registered as an employee venture capital corporation under section 8 of the ' +
            '<i>Employee Investment Act</i>?',
            'inputType': 'radio',
            'init': '2',
            'num': '245',
            'tn': '245'
          },
          {
            'type': 'infoField',
            'label': 'Has the corporation claimed a BC production services tax credit for this production?',
            'inputType': 'radio',
            'init': '2',
            'num': '250',
            'tn': '250'
          },
          {'labelClass': 'fullLength'},
          {
            'type': 'infoField',
            'label': 'In the case of a production that is an interprovincial co-production, is 50% or less* of the ' +
            'copyright in the production owned by the corporation or owned by the corporation and <b>one or both</b>' +
            ' of an eligible production corporation related to the corporation, and the Film Development Society ' +
            'of British Columbia? (If the answer is no and principal photography started before January 1, 2012,' +
            ' complete the calculation below.)',
            'inputType': 'radio',
            'init': '2',
            'num': '255',
            'tn': '255'
          },
          {'labelClass': 'fullLength'},
          {
            'type': 'infoField',
            'label': 'Is the corporation eligible for the British film and television tax credit',
            'inputType': 'radio',
            'num': '256'
          },
          {
            'labelClass': 'fullLength'
          },
          {
            'label': 'Percentage of copyright owned by the corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1001',
                  'validate': {
                    'compare': {
                      'between': [
                        0,
                        100
                      ]
                    }
                  }
                }
              }
            ],
            'indicator': '%A'
          },
          {
            'label': 'Total copyright',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1002',
                  'init': 100,
                  'validate': {
                    'compare': {
                      'between': [
                        0,
                        100
                      ]
                    }
                  }
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '%a'
                }
              }
            ]
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Percentage of copyright owned by federal and provincial agencies with a mandate to finance film or video productions in Canada',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1003',
                  'validate': {
                    'compare': {
                      'between': [
                        0,
                        100
                      ]
                    }
                  }
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '%1'
                }
              },
              null
            ]
          },
          {
            'label': 'Percentage of copyright owned by non-profit organizations that have a fund used to finance film or video productions ',
            'labelClass': '',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1004',
                  'validate': {
                    'compare': {
                      'between': [
                        0,
                        100
                      ]
                    }
                  }
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '%2'
                }
              },
              null
            ]
          },
          {
            'label': 'Subtotal (amount 1 <b>plus</b> amount 2)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1005',
                  'validate': {
                    'compare': {
                      'between': [
                        0,
                        200
                      ]
                    }
                  }
                }
              },
              {
                'input': {
                  'num': '1006',
                  'validate': {
                    'compare': {
                      'between': [
                        0,
                        200
                      ]
                    }
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': '%b'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount a <b>minus</b> amount b)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1007',
                  'validate': {
                    'compare': {
                      'between': [
                        0,
                        100
                      ]
                    }
                  }
                }
              },
              {
                'input': {
                  'num': '1008',
                  'validate': {
                    'compare': {
                      'between': [
                        0,
                        100
                      ]
                    }
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': '%B'
          },
          {
            'label': 'Percentage of copyright owned for the purpose of the basic tax credit calculation (amount A ' +
            '<b>divided</b> by amount B)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1009',
                  'decimals': 3,
                  'validate': {
                    'compare': {
                      'greaterThanOrEqual': 0
                    }
                  }
                }
              }
            ],
            'indicator': '%C'
          },
          {
            'label': '(Enter amount C on line 610 at Part 7)',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            'label': '* In the case of a production that is an interprovincial co-production that began principal photography before January 1, 2012, the percentage is 20% or less of the copyright owned by the corporation or by an eligible production corporation that is related to the corporation.',
            'labelClass': 'fullLength tabbed'
          }
        ]
      },
      {
        'header': 'Part 4 - Production cost limit',
        'rows': [
          {
            'label': 'Cumulative production cost as at the end of the tax year (include current and prior year production costs)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '405',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '405'
                }
              }
            ],
            'indicator': 'D'
          },
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Government or non-government assistance that the corporation has not repaid',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '410',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '410'
                }
              }
            ],
            'indicator': 'E'
          },
          {
            'label': 'Subtotal (amount D <b>minus</b> amount E)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '1010'
                }
              }
            ],
            'indicator': 'F'
          },
          {type: 'table', num: '1030'},
          {
            'label': 'Deduct:',
            'labelClass': 'bold'
          },
          {
            'label': 'Qualified BC labour expenditure claimed in all previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '420',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '420'
                }
              }
            ],
            'indicator': 'H'
          },
          {
            'label': '<b>Production cost limit</b> (amount G <b>minus</b> amount H) ',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '480',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '480'
                }
              }
            ],
            'indicator': 'I'
          },
          {labelClass: 'fullLength'},
          {
            'label': '75% of the cost of producing the BC portion of the production must be payable for <b>goods or ' +
            'services</b> provided in BC by BC-based individuals or BC-based corporations. For documentaries, 75% of ' +
            'the cost of producing the BC portion of the production must be payable for <b>goods or services</b> ' +
            'provided by BC-based individuals or BC-based corporations.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 5 - Qualified BC labour expenditure',
        'rows': [
          {
            'label': 'BC labour expenditure for the tax year includes amounts:',
            'labelClass': 'fullLength'
          },
          {
            'label': '• incurred from the final script stage to the end of the post-production stage;',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• incurred in the tax year or previous tax year and that did not form part of the claimant\'s BC labour expenditure for the previous tax year;',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• paid during the tax year or within 60 days after the end of the tax year;',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• that are directly attributable to the production; and',
            'labelClass': 'fullLength tabbed'
          },
          {
            'label': '• that are for services provided by BC-based individuals.',
            'labelClass': 'fullLength tabbed'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'A BC-based individual is defined as an individual who was resident in BC on December 31 of the year preceding the end of the tax year for which the corporation claims this credit.',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'BC labour expenditure does not include amounts paid that are included in a BC interactive digital media tax credit claim.',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            'label': '<b>BC labour expenditure for the tax year </b>is the total of:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Salary or wages paid that are directly attributable to the production',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '505',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '505'
                }
              }
            ],
            'indicator': 'J'
          },
          {
            'label': 'Remuneration directly attributable to the production paid to:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- BC-based individuals',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '515',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '515'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'c'
                }
              }
            ]
          },
          {
            'label': '- taxable Canadian corporations (solely owned by a BC-based individual)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '516',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '516'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'd'
                }
              }
            ]
          },
          {
            'label': '- other taxable Canadian corporations (for their BC-based employees)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '520',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '520'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'e'
                }
              }
            ]
          },
          {
            'label': '- partnerships carrying on business in Canada (for their BC-based members or employees)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '521',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '521'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'f'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts c to f)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '522'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '523'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'K'
          },
          {
            'label': '<b>Add:</b>',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Labour expenditure that would have qualified as a BC labour expenditure transferred under a reimbursement agreement by the corporation, a wholly owned subsidiary, to the parent corporation that is a taxable Canadian corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '525',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '525'
                }
              }
            ],
            'indicator': 'L'
          },
          {
            'label': '<b>BC labour expenditure for the tax year</b> (total of amounts J to L)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '526'
                }
              }
            ],
            'indicator': 'M'
          },
          {labelClass: 'fullLength'},
          {
            'label': '<b>Unclaimed BC labour expenditure from the previous tax year:</b>',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Total BC labour expenditure for the previous tax year (amount Q from previous tax year)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '530',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '530'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'g'
                }
              }
            ]
          },
          {
            'label': 'Qualified BC labour expenditure claimed in the previous tax year<br> (line 590 from the previous tax year)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '535',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '535'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'h'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (amount g <b>minus</b> amount h) (if negative, enter "0")',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '536'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '537'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'N'
          },
          {
            'label': 'Subtotal (amount M <b>plus</b> amount N)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '538'
                }
              }
            ],
            'indicator': 'O'
          },
          {
            'label': 'BC labour expenditure transferred under a reimbursement agreement by the parent corporation, that is a taxable Canadian corporation, to the corporation, a wholly owned subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '550',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '550'
                }
              }
            ],
            'indicator': 'P'
          },
          {
            'label': '<b>Total BC labour expenditure for the tax year</b> (amount O <b>minus</b> amount P).',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '551'
                }
              }
            ],
            'indicator': 'Q'
          },
          {
            'label': '<b>Qualified BC labour expenditure for the tax year</b> (the lesser of amount I in Part 4 and amount Q)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '590',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '590'
                }
              }
            ],
            'indicator': 'R'
          }
        ]
      },
      {
        'header': 'Part 6 - BC labour expenditure directly attributable to digital animation, visual effects and post-production (DAVE) activities',
        'rows': [
          {
            'label': 'DAVE activities include prescribed digital animation or visual effects activities. If principal photography began after February 28, 2015, DAVE activities are expanded to include prescribed digital post-production activities.',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            'label': '<b>BC labour expenditure directly attributable to DAVE activities (DAVE BC labour expenditure) for the tax year</b> is the total of:',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Salary or wages paid that are directly attributable to the production\'s DAVE activities',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '710',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '710'
                }
              }
            ],
            'indicator': 'S'
          },
          {'labelClass': 'fullLength'},
          {
            'label': 'Remuneration directly attributable to the production\'s DAVE activities paid to:',
            'labelClass': 'fullLength'
          },
          {
            'label': '- BC-based individuals',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '715',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '715'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'i'
                }
              }
            ]
          },
          {
            'label': '- taxable Canadian corporations (solely owned by a BC-based individual)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '720',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '720'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'j'
                }
              }
            ]
          },
          {
            'label': '- other taxable Canadian corporations (for their BC-based employees)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '725',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '725'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'k'
                }
              }
            ]
          },
          {
            'label': '- partnerships carrying on business in Canada (for their BC-based members or employees)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '726',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '726'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'l'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts i to l)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '728'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '729'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'T'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Labour expenditure that would have qualified as a DAVE BC labour expenditure transferred under a reimbursement agreement by the corporation, a wholly owned subsidiary, to the parent corporation that is a taxable Canadian corporation',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '730',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '730'
                }
              }
            ],
            'indicator': 'U'
          },
          {
            'label': 'DAVE BC labour expenditure for the current tax year (total of amounts S to U)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '731'
                }
              }
            ],
            'indicator': 'V'
          },
          {
            'label': 'DAVE BC labour expenditure for the previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '733',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '733'
                }
              }
            ],
            'indicator': 'W'
          },
          {
            'label': 'DAVE BC labour expenditure for the current tax year and previous tax years (amount V <b>plus</b> amount W)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '735',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '735'
                }
              }
            ],
            'indicator': 'X'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'All government or non-government assistance that the corporation has not repaid and can be reasonably considered to be attributable to DAVE BC labour expenditure',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '740',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '740'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'm'
                }
              }
            ]
          },
          {
            'label': 'All DAVE BC labour expenditure claimed in the previous tax years',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '745',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '745'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'n'
                }
              }
            ]
          },
          {
            'label': 'DAVE BC labour expenditure transferred under a reimbursement agreement by the parent corporation, that is a taxable Canadian corporation, to the corporation, a wholly owned subsidiary',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '750',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '750'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'o'
                }
              }
            ]
          },
          {
            'label': 'Subtotal (total of amounts m to o)',
            'labelCellClass': 'alignRight',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': false,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '751'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '752'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'Y'
          },
          {
            'label': '<b>DAVE BC labour expenditure for the tax year</b> (amount X <b>minus</b> amount Y)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '755',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '755'
                }
              }
            ],
            'indicator': 'Z'
          }
        ]
      },
      {
        'header': 'Part 7 - Basic tax credit',
        'rows': [
          {type: 'table', num: '1000'},
          {
            'label': 'For interprovincial co-productions where the principal photography began before January 1, 2012, <br>enter the amount calculated on line C in Part 3, <b>otherwise enter "100"</b>',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '610',
                  'validate': {
                    'or': [
                      {
                        'and': [
                          {
                            'length': {
                              'min': '1',
                              'max': '6'
                            }
                          },
                          {
                            'check': 'isPositive'
                          }
                        ]
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '610'
                }
              }
            ],
            'indicator': '%BB'
          },
          {
            'label': '<b>Basic tax credit</b> (amount AA <b>multiplied by</b> amount BB)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '620',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '620'
                }
              }
            ],
            'indicator': 'CC'
          }
        ]
      },
      {
        'header': 'Part 8 - Regional tax credit',
        'rows': [
          {
            'label': 'To be eligible for a regional tax credit, principal photography of the production, or at least ' +
            'three of the qualifying episodes of a series (see note) must be done  in BC outside of the designated ' +
            'Vancouver area for a minimum of five days and must be more than 50% of the total number of principal ' +
            'photography days in BC. For animated productions that start key animation after June 26, 2015, there is no' +
            ' minimum number or percentage of principal photography days required. Claim the regional tax credit on the' +
            ' qualified BC labour expenditure (QBCLE) for the tax year prorated by the BC labour expenditure (BCLE)' +
            ' incurred  in BC outside the designated Vancouver area over the BC labour expenditure for the tax year.',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Qualified BC labour expenditure for the tax year (amount R from Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '689'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'p'
                }
              }
            ]
          },
          {
            'label': 'Qualified BC labour expenditure related to episodes that do not qualify for the regional tax credit (if this is a series intended for television broadcast) (see note)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '690',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '690'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'q'
                }
              }
            ]
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Qualified BC labour expenditure for the purpose of the regional tax credit calculation <br>' +
            '(amount p <b>minus</b> amount q)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '691'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '692'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'DD'
          },
          {labelClass: 'fullLength'},
          {
            'label': '<b>For animated productions that start key animation before June 27, 2015, and for live action productions:</b>',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2000'
          },
          {
            'label': '<b>For animated productions that start key animation after June 26, 2015:</b>',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2005'
          },
          {
            'label': 'Prorated qualified BC labour expenditure <br>(amount DD <b>multiplied</b> by amount r or s, whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '704L',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'input': {
                  'num': '704R',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'EE'
          },
          {
            'label': '<b>Regional tax credit</b> (amount EE <b>multiplied by</b> 12.5%)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '705',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '705'
                }
              }
            ],
            'indicator': 'FF'
          },
          {labelClass: 'fullLength'},
          {
            'label': '<b>Note</b>'
          },
          {
            'label': 'Qualifying episode means an episode of a film or video production that is intended for television' +
            ' broadcast as a series and that comprises a cycle of at least three episodes.',
            'labelClass': 'fullLength tabbed'
          },
          {labelClass: 'fullLength'},
          {
            'label': '* Principal photography days of the eligible production or qualifying episodes in British Columbia.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 9 - Distant location regional tax credit',
        'rows': [
          {
            'label': 'To be eligible for a distant location regional tax credit, principal photography of the production,' +
            ' or at least three of the qualifying episodes of a series (see note) must be done in BC in a distant ' +
            'location for a minimum of one day and the production must qualify for the regional tax credit in Part 8.' +
            ' For animated productions that start key animation after June 26, 2015, there is no minimum number or ' +
            'percentage of principal photography days required. Claim the distant location regional tax credit on the' +
            ' QBCLE for the tax year prorated by the BC labour expenditure (BCLE) incurred in a distant location over' +
            ' the BC labour expenditure for the tax year.',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Qualified BC labour expenditure for the tax year (amount R from Part 5)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '650'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 't'
                }
              }
            ]
          },
          {
            'label': 'Qualified BC labour expenditure related to episodes that do not qualify for the distant location regional tax credit (if this is a series intended for television broadcast) (see note)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '655',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '655'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'u'
                }
              }
            ]
          },
          {
            'label': 'Qualified BC labour expenditure for the purpose of the distant location regional tax credit calculation (amount t <b>minus</b> amount u)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '656'
                }
              },
              {
                'underline': 'double',
                'input': {
                  'num': '657'
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'GG'
          },
          {
            'label': '<b>For animated productions that start key animation before June 27, 2015, and for live action productions:</b>',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2010'
          },
          {
            'label': '<b>For animated productions that start key animation after June 26, 2015:</b>',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2015'
          },
          {
            'label': 'Prorated qualified BC labour expenditure (amount GG <b>multiplied by</b> amount v or w, whichever applies)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '669L',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                }
              },
              {
                'input': {
                  'num': '669R',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'symbol',
                  'data': 'glyphicon-triangle-right'
                }
              }
            ],
            'indicator': 'HH'
          },
          {
            'label': '<b>Distant location regional tax credit</b> (amount HH <b>multiplied by</b> 6%)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '670',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '670'
                }
              }
            ],
            'indicator': 'II'
          },
          {labelClass: 'fullLength'},
          {
            label: '<b>Note</b>'
          },
          {
            'label': 'The qualifying episodes for the distant location regional tax credit are the same as the qualifying episodes for the regional tax credit',
            'labelClass': 'fullLength tabbed'
          },
          {labelClass: 'fullLength'},
          {
            'label': '* Principal photography days of the eligible production or qualifying episodes in British Columbia.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        'header': 'Part 10 - Film training tax credit',
        'rows': [
          {
            'label': 'BC labour expenditure paid to BC-based individuals in an approved training program',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '674',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '674'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'x'
                }
              }
            ]
          },
          {
            'label': '<b>Deduct:</b>',
            'labelClass': 'fullLength'
          },
          {
            'label': 'Any assistance for the training program or the BC-based individuals',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '678',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '678'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'y'
                }
              }
            ]
          },
          {
            'label': 'Net BC labour expenditure paid to BC-based individuals in an approved training program <br>(amount x <b>minus</b> amount y)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'underline': 'double',
                'input': {
                  'num': '679'
                }
              },
              {
                'padding': {
                  'type': 'text',
                  'data': 'z'
                }
              }
            ]
          },
          {
            'type': 'table',
            'num': '2020'
          },
          {
            'label': '<b>Film training tax credit</b> (enter the lesser of amount JJ and amount KK)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '685',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '685'
                }
              }
            ],
            'indicator': 'LL '
          }
        ]
      },
      {
        'header': 'Part 11 - Digital animation, visual effects and post-production tax credit',
        'rows': [
          {
            'label': 'The DAVE production services tax credit rate is 16% for productions that start principal' +
            ' photography after September 30, 2016. For episodic productions that start principal photography of the' +
            ' first eligible episode before October 1, 2016, the DAVE production services tax credit rate will remain ' +
            'at 17.5% for all other eligible episodes in that cycle.',
            'labelClass': 'fullLength'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Use line 1, if principal photography for the production started before October 1, 2016<br>Use line 2, if principal photography for the production started after September 30, 2016',
            'labelClass': 'fullLength'
          },
          {
            'type': 'table',
            'num': '2030'
          },
          {
            'label': '<b>Digital animation, visual effects and post-production tax credit</b> (Use either amount 1 or amount 2 depending on the production\'s principal photography start date, but not both)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '760',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '760'
                }
              }
            ],
            'indicator': 'MM'
          }
        ]
      },
      {
        'header': 'Part 12 - British Columbia film and television tax credit',
        'rows': [
          {
            'label': 'Basic tax credit (amount CC from Part 7)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '790'
                }
              }
            ],
            'indicator': 'NN'
          },
          {
            'label': 'Regional tax credit (amount FF from Part 8)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '792'
                }
              }
            ],
            'indicator': 'OO'
          },
          {
            'label': 'Distant location regional tax credit (amount II from Part 9)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '794'
                }
              }
            ],
            'indicator': 'PP'
          },
          {
            'label': 'Film training tax credit (amount LL from Part 10)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '796'
                }
              }
            ],
            'indicator': 'QQ'
          },
          {
            'label' : 'Digital animation, visual effects and post-production tax credit (amount MM from Part 11)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '798'
                }
              }
            ],
            'indicator': 'RR'
          },
          {
            'label': '<b>British Columbia film and television tax credit</b> (total of amounts NN to RR)',
            'layout': 'alignInput',
            'layoutOptions': {
              'indicatorColumn': true,
              'showDots': true,
              'paddingRight': false
            },
            'columns': [
              {
                'input': {
                  'num': '800',
                  'validate': {
                    'or': [
                      {
                        'matches': '^[.\\d]{1,13}$'
                      },
                      {
                        'check': 'isEmpty'
                      }
                    ]
                  }
                },
                'padding': {
                  'type': 'tn',
                  'data': '800'
                }
              }
            ],
            'indicator': 'SS'
          },
          {labelClass: 'fullLength'},
          {
            'label': 'Enter amount SS on line 671 of Schedule 5, <i>Tax Calculation Supplementary - Corporations.</i> If you are filing more than one of these forms, add all SS amounts from all of the forms and enter the total on line 671 of Schedule 5.',
            'labelClass': 'fullLength'
          }
        ]
      },
      {
        hideFieldset: true,
        rows:[
          {
            label: '<i>See the privacy notice on your return</i>.',
            labelClass: 'absoluteAlignRight'
          }
        ]
      }
    ]
  });
})();
