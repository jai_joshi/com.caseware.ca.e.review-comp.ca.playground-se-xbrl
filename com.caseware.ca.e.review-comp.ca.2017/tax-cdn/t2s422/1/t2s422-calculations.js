(function() {
  function getApplicableValue(calcUtils, num1, num2) {
    var field = calcUtils.field;
    var value = 0;
    var num1Val = field(num1).get();
    var num2Val = field(num2).get();
    if (num1Val > 0 && num2Val == 0) {
      value = num1Val
    } else if (num1Val == 0 && num2Val > 0) {
      value = num2Val
    } else {
      value = num1Val
    }
    return value;
  }

  function calcRates(field, storeNum, numeratorNum, denominatorNum) {
    var result = 0;
    if (field(denominatorNum).get() !== 0) {
      result = field(numeratorNum).get() / field(denominatorNum).get()
    }
    field(storeNum).assign(result)
  }

  wpw.tax.create.calcBlocks('t2s422', function(calcUtils) {
    calcUtils.calc(function(calcUtils, field, form) {
      //part 1 calcs
      field('099').cell(0, 0).assign(field('T2J.951').get() + ' ' + field('T2J.950').get());
      field('099').cell(0, 1).assign(field('T2J.956').get());
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('1002').assign(form('ratesBc').field('4220').get());
      field('1002').source(form('ratesBc').field('4220'));

      //part 3 calcs
      field('1005').assign(Math.max(0, field('1003').get()) + Math.max(0, field('1004').get()));
      field('1006').assign(field('1005').get());
      field('1007').assign(Math.max(0, field('1002').get() - field('1006').get()));
      field('1008').assign(field('1007').get());
      if (field('1008').get() != 0) {
        field('1009').assign(Math.max(0, (field('1001').get() * 100)) / Math.max(0, field('1008').get()));
      }
      else {
        field('1009').assign(0);
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('411Mult').assign(form('ratesBc').field('4221').get());
      field('411Mult').source(form('ratesBc').field('4221'));
      //part 4 calcs
      field('1010').assign(Math.max(0, field('405').get() - field('410').get()));
      field('411').assign(field('1010').get());
      field('412').assign(field('411').get() * field('411Mult').get() /100);
      field('480').assign(Math.max(0, field('412').get() - field('420').get()));
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 5 calcs
      field('522').assign(
          field('515').get() +
          field('516').get() +
          field('520').get() +
          field('521').get()
      );
      field('523').assign(field('522').get());
      field('526').assign(
          field('505').get() +
          field('523').get() +
          field('525').get()
      );
      field('536').assign(Math.max(0,
          field('530').get() -
          field('535').get()
      ));
      field('537').assign(field('536').get());
      field('538').assign(
          field('526').get() +
          field('537').get()
      );
      field('551').assign(Math.max(0,
          field('538').get() -
          field('550').get()
      ));
      if (field('480').get() < field('551').get()) {
        field('590').assign(field('480').get());
      }
      else {
        field('590').assign(field('551').get());
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //part 6 calcs
      field('728').assign(
          field('715').get() +
          field('720').get() +
          field('725').get() +
          field('726').get()
      );
      field('729').assign(field('728').get());
      field('731').assign(
          field('710').get() +
          field('729').get() +
          field('730').get()
      );
      field('751').assign(Math.max(0,
          field('740').get() +
          field('745').get() +
          field('750').get()
      ));
      field('752').assign(field('751').get());
      field('735').assign(
          field('731').get() +
          field('733').get()
      );
      if (field('302').get() != null) {
        field('755').assign(Math.max(0,
            field('735').get() -
            field('752').get()
        ));
      }
      else {
        field('755').assign(0);
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('601Mult').assign(form('ratesBc').field('4222').get());
      field('601Mult').source(form('ratesBc').field('4222'));
      //Part 7 calcs
      var beforeJan1 = wpw.tax.utilities.dateCompare.lessThan(field('302').get(), wpw.tax.date(2012, 1, 1));
      var interProv = !!field('313').get() == '1';
      field('600').assign(field('590').get());
      field('601').assign(field('600').get() * field('601Mult').get() / 100);

      if (calcUtils.hasChanged('1009') && beforeJan1 && interProv) {
        field('610').assign(field('1009').get());
      }
      else {
        field('610').assign(100);
      }
      field('620').assign(
          field('601').get() *
          field('610').get() *
          (1 / 100)
      );
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 8 calcs
      var beforeJun27 = wpw.tax.utilities.dateCompare.lessThan(field('302').get(), wpw.tax.date(2015, 6, 27));

      field('689').assign(field('590').get());
      field('691').assign(Math.max(0,
          field('689').get() -
          field('690').get()
      ));
      field('692').assign(field('691').get());

      calcRates(field, '696', '695', '700');
      calcRates(field, '703', '702', '704');

      var value = getApplicableValue(calcUtils, '696', '703');
      field('704L').assign(value * field('692').get());
      field('704R').assign(field('704L').get());
      field('705').assign(
          field('704L').get() *
          (125 / 1000)
      );
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 9 calcs
      var dateDefined = !wpw.tax.utilities.isNullUndefined(field('302').get());

      field('650').assign(dateDefined ? field('590').get() : 0);
      field('656').assign(Math.max(0,
          field('650').get() -
          field('655').get()
      ));
      field('657').assign(field('656').get());

      calcRates(field, '661', '660', '665');
      calcRates(field, '668', '667', '669');

      var value = getApplicableValue(calcUtils, '661', '668');
      field('669L').assign(value * field('657').get());
      field('669R').assign(field('669L').get());
      field('670').assign(
          field('669L').get() *
          (6 / 100)
      );
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('680Mult').assign(form('ratesBc').field('4223').get());
      field('680Mult').source(form('ratesBc').field('4223'));
      field('682Mult').assign(form('ratesBc').field('4224').get());
      field('682Mult').source(form('ratesBc').field('4224'));

      //Part 10 calcs
      field('679').assign(Math.max(0, field('674').get() - field('678').get()));
      field('680').assign(field('679').get());
      field('682').assign(field('590').get());
      field('681').assign(field('680').get() * (field('680Mult').get() * (1 / 100)));
      field('683').assign(field('682').get() * (field('682Mult').get() * (1 / 100)));
      field('685').assign(Math.min(
          Math.max(0, field('681').get()),
          Math.max(0, field('683').get())
      ));
    });

    calcUtils.calc(function(calcUtils, field, form) {
      field('756Mult').assign(form('ratesBc').field('4225').get());
      field('756Mult').source(form('ratesBc').field('4225'));
      field('758Mult').assign(form('ratesBc').field('4226').get());
      field('758Mult').source(form('ratesBc').field('4226'));
      //Part 11 calcs
      var beforeOct1 = wpw.tax.utilities.dateCompare.lessThan(field('302').get(), wpw.tax.date(2016, 10, 1));
      var dateDefined = !wpw.tax.utilities.isNullUndefined(field('302').get());
      if (dateDefined) {
        if (beforeOct1) {
          field('756').assign(field('755').get());
          field('757').assign(field('756').get() * (field('756Mult').get() * (1 / 100)));
          field('760').assign(field('757').get());
          field('758').assign(0);
          field('759').assign(0);
        }
        else {
          field('758').assign(field('755').get());
          field('759').assign(field('758').get() * (field('758Mult').get() * (1 / 100)));
          field('760').assign(field('759').get());
          field('756').assign(0);
          field('757').assign(0);
        }
      }
      else {
        field('756').assign(0);
        field('757').assign(0);
        field('760').assign(0);
        field('758').assign(0);
        field('759').assign(0);
      }
    });

    calcUtils.calc(function(calcUtils, field, form) {
      //Part 12 calcs
      //eligibility
      if (field('220').get() == 2 &&
          field('222').get() == 2 &&
          field('230').get() == 2 &&
          field('235').get() == 2 &&
          field('240').get() == 2 &&
          field('245').get() == 2 &&
          field('250').get() == 2 &&
          field('255').get() == 2) {
        field('256').assign(1)
      }
      else {
        field('256').assign(2)
      }

      var isEligible = false;

      if (field('256').get() == 1) {
        isEligible = true
      }

      var beforeOct1 = wpw.tax.utilities.dateCompare.lessThan(field('302').get(), wpw.tax.date(2016, 10, 1));
      field('790').assign(isEligible ? field('620').get() : 0);
      field('792').assign(isEligible ? field('705').get() : 0);
      field('794').assign(isEligible ? field('670').get() : 0);
      field('796').assign(isEligible ? field('685').get() : 0);
      field('798').assign(isEligible ? (beforeOct1 ? field('757').get() : field('759').get()) : 0);
      field('800').assign(
          field('790').get() +
          field('792').get() +
          field('794').get() +
          field('796').get() +
          field('798').get()
      );
    });

  });
})();
