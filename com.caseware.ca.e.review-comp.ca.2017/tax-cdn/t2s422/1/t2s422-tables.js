(function() {

  var rateCols = [
    {
      type: 'none',
      cellClass: 'alignLeft'
    },
    {
      colClass: 'std-input-width'
    },
    {
      type: 'none',
      colClass: 'std-padding-width'
    },
    {
      colClass: 'small-input-width'
    },
    {
      type: 'none',
      colClass: 'std-padding-width'
    },
    {
      colClass: 'std-input-width'
    },
    {
      type: 'none',
      colClass: 'std-padding-width'
    }
  ];

  function getPart7Table(tableObj) {
    return {
      'fixedRows': true,
      'infoTable': true,
      'columns': [
        {type: 'none'},
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {colClass: 'std-input-width'},
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {colClass: 'std-input-width'},
        {
          type: 'none',
          colClass: 'std-padding-width'
        },
        {
          colClass: 'std-input-width',
          'type': 'none'
        },
        {
          colClass: 'std-padding-width',
          'type': 'none'
        }
      ],
      cells: [
        {
          '0': {
            label: tableObj.label[0],
            cellClass: 'alignCenter singleUnderline'
          },
          '2': {
            tn: tableObj.tn[0]
          },
          '3': {
            num: tableObj.num[0],
            // "validate": {"or": [{"matches": "^-?[.\\d]{1,3}$"}, {"check": "isEmpty"}]},
            cellClass: 'singleUnderline',
            formField: true
          },
          '4': {
            label: '='
          },
          '5': {
            num: tableObj.num[1],
            decimals: 5
          },
          '6': {
            label: tableObj.indicator
          }
        },
        {
          '0': {
            label: tableObj.label[1],
            labelClass: 'center'
          },
          '2': {
            tn: tableObj.tn[1]
          },
          '3': {
            num: tableObj.num[2],
            // "validate": {"or": [{"matches": "^-?[.\\d]{1,3}$"}, {"check": "isEmpty"}]}
            formField: true
          },
          '5': {
            type: 'none'
          }
        }
      ]
    }
  }

  wpw.tax.create.tables('t2s422', {
    '099': {
      'fixedRows': true,
      'infoTable': true,
      'dividers': [1],
      'columns': [
        {
          'width': '60%',
          cellClass: 'alignLeft'
        },
        {
          cellClass: 'alignLeft'
        }
      ],
      'cells': [
        {
          '0': {
            'label': 'Name of person to contact',
            'num': '151', "validate": {"or":[{"length":{"min":"1","max":"60"}},{"check":"isEmpty"}]},
            'tn': '151',
            type: 'text'
          },
          '1': {
            'label': 'Telephone number',
            'num': '153',
            'tn': '153',
            type: 'custom',
            format: ['({N3}){N3}-{N4}', '{N10}','{N3}-{N3}-{N4}'],
            validate: {"or": [{"matches": "^[\\d]{3}-[\\d]{3}-[\\d]{4}$"},{"matches": "^[(]{1}[\\d]{3}[)]{1}[\\d]{3}-[\\d]{4}$"},{"matches": "^[\\d]{10}$"}]}
          }
        }
      ]
    },

    '2000': getPart7Table({
      label: ['Number of principal photography days* outside the designated Vancouver area', 'Total number of days*'],
      num: ['695', '696', '700'],
      tn: ['695', '700'],
      indicator: 'r'
    }),

    '2005': getPart7Table({
      label: ['BCLE for the tax year outside the designated Vancouver', 'BCLE for tax year'],
      num: ['702', '703', '704'],
      tn: ['702', '704'],
      indicator: 's'
    }),

    '2010': getPart7Table({
      label: ['Number of principal photography days* in a distant location', 'Total number of days*'],
      num: ['660', '661', '665'],
      tn: ['660', '665'],
      indicator: 'v'
    }),

    '2015': getPart7Table({
      label: ['BCLE for the tax year in a distant location', 'BCLE for tax year'],
      num: ['667', '668', '669'],
      tn: ['667', '669'],
      indicator: 'w'
    }),
    '1000': {
      fixedRows: true,
      infoTable: true,
      columns: rateCols,
      cells: [
        {
          0: {
            label: 'Qualified BC labour expenditure for the tax year (amount R from Part 5)'
          },
          1: {num: '600'},
          2: {label: 'x'},
          3: {num: '601Mult'},
          4: {label: '%='},
          5: {num: '601'},
          6: {label: 'AA'}
        }
      ]
    },
    '1030': {
      fixedRows: true,
      infoTable: true,
      columns: rateCols,
      cells: [
        {
          0: {label: 'Amount F'},
          1: {num: '411'},
          2: {label: 'x'},
          3: {num: '411Mult'},
          4: {label: '%='},
          5: {num: '412'},
          6: {label: 'G'}
        }
      ]
    },
    '2020': {
      fixedRows: true,
      infoTable: true,
      columns: rateCols,
      cells: [
        {
          0: {
            label: 'Amount z'
          },
          1: {num: '680'},
          2: {label: 'x'},
          3: {num: '680Mult'},
          4: {label: '%='},
          5: {num: '681'},
          6: {label: 'JJ'}
        },
        {
          0: {
            label: 'Qualified BC labour expenditure for the tax year (amount R from Part 5)'
          },
          1: {num: '682'},
          2: {label: 'x'},
          3: {num: '682Mult'},
          4: {label: '%='},
          5: {num: '683'},
          6: {label: 'KK'}
        }
      ]
    },
    '2030': {
      fixedRows: true,
      infoTable: true,
      columns: rateCols,
      cells: [
        {
          0: {
            label: '<b>DAVE tax credit:</b> amount Z from Part 6 <br>(for productions that started principal photography ' +
            'before October 1, 2016)'
          },
          1: {num: '756'},
          2: {label: 'x'},
          3: {num: '756Mult', decimals: 1},
          4: {label: '%='},
          5: {num: '757'},
          6: {label: '1'}
        },
        {
          0: {
            label: '<b>DAVE tax credit:</b> amount Z from Part 6 <br>(for productions that started principal photography ' +
            'after September 30, 2016)'
          },
          1: {num: '758'},
          2: {label: 'x'},
          3: {num: '758Mult'},
          4: {label: '%='},
          5: {num: '759'},
          6: {label: '2'}
        }
      ]
    }
  })
})();
