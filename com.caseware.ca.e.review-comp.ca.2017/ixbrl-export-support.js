(function(wpw){
  'use strict';

  /**
   * @type {IXBRLExportSupport} Setup the support for XBRL import and exporting.
   */
  window.IXBRLExportSupport = new IXBRLExportSupport();

  /**
   * @constructor The base class for XBRL support what is tracked in here is up to implementor.
   */
  function IXBRLExportSupport() {
    /**
     * @type {string}
     */
    this.currentNameSpace = null;

    /**
     * @type {wpw.xbrl.models.EngagementInfo} A copy of the engagement info (Provided by onStart).
     */
    this.engagmentInfo = null;
  }

  /**
   * @return {string} A version implemented is required this is the CaseWare version to be checked against
   *                  and will refer to the version of the XBRL micro service.
   */
  IXBRLExportSupport.prototype.implementedForVersion = function() {
    return '1';
  };

  /**
   * @return {Array.<wpw.xbrl.models.TaxonomyDefinition>} A representation of various taxonomies supported.
   */
  IXBRLExportSupport.prototype.taxonomiesSupported = function() {
    /**
     * @type {wpw.xbrl.models.TaxonomyDefinition} Define your taxonomy definition by defining a namespace.
     */
    var tax1 = new wpw.xbrl.models.TaxonomyDefinition();
    tax1.namespace = new wpw.xbrl.models.LabeledUri();
    /**
     * @type {string} Define your URL for the XBRL taxonomy. (Replace this with your actual taxonomy url).
     */
    tax1.namespace.url = 'http://myhostedurl.com/HelloWorld';
    /**
     * @type {string} Define the various language codes this will be displayed on screen.
     */
    tax1.namespace.label = {'en': 'Hello World XBRL Test'};

    return [tax1];
  };

  /**
   * @param {wpw.xbrl.models.LabeledUri} namespace
   * @param {Array.<wpw.xbrl.models.LabeledUri>} entryPoints (This might not be required)
   */
  IXBRLExportSupport.prototype.onInit = function(namespace) {
    this.currentNameSpace = namespace.uri;
  };

  /**
   * Get source documents is responsible for filtering the container info so you only have a list of documents you
   * intend on supporting.
   *
   * @param {Array.<wpw.xbrl.models.XbrlContainerInfo>} supportedDocuments Container info objects generated from the active engagement.
   * @return {Array.<wpw.xbrl.models.XbrlContainerInfo>} A filtered list of supported documents.
   */
  IXBRLExportSupport.prototype.getSourceDocuments = function(supportedDocuments) {
    /**
     * Apply any filtering required here before returning the full list of documents back to SE.
     */
    return supportedDocuments;
  };

  /**
   * SE will call this function when it is about to begin processing XBRL you should use this opportunity
   * to log any information you will need during the execution of your XBRL export.
   *
   * @param {wpw.xbrl.models.EngagementInfo} engagement A copy of the current engagement info.
   * @return {wpw.xbrl.models.XbrlExportInfo} Any changes to XBRLExportInfo should also be included here.
   */
  IXBRLExportSupport.prototype.onStart = function(engagement) {
    var xbrlExportInfo = new wpw.xbrl.models.XbrlExportInfo();
    this.engagmentInfo = engagement;
    xbrlExportInfo.namespace = this.currentNameSpace;
    return xbrlExportInfo;
  };

  /**
   * If XBRL facts from financials or letters sections that aren't Text Areas or Dynamic Tables are required
   * this method should be implemented to generate XBRL facts.
   *
   * @param {wpw.xbrl.models.XbrlContainerInfo} document
   * @param {wpw.xbrl.models.XbrlSectionInfo} section
   * @param {wpw.xbrl.models.XbrlSourceData} data
   * @returns {null|wpw.xbrl.models.XbrlExportInfo}
   */
  IXBRLExportSupport.prototype.getXbrlFactsFromSection = function(document, section, data) {
    return null;
  };

  /**
   * If XBRL facts from financials or letters text areas are required
   * this method should be implemented to generate XBRL facts.
   *
   * @param {wpw.xbrl.models.XbrlContainerInfo} document
   * @param {wpw.xbrl.models.XbrlSectionInfo} section
   * @param {wpw.xbrl.models.XbrlSourceData} content
   * @param {Array.<wpw.xbrl.models.XbrlSourceData>=} subSectionData
   * @returns {null|wpw.xbrl.models.XbrlExportInfo}
   */
  IXBRLExportSupport.prototype.getXbrlFactsFromTextArea = function(document, section, content, subSectionData) {
    return null;
  };

  /**
   * If XBRL facts from financials dynamic tables are required
   * this method should be implemented to generate XBRL facts.
   *
   * @param {wpw.xbrl.models.XbrlContainerInfo} document
   * @param {wpw.xbrl.models.XbrlSectionInfo} section
   * @param {wpw.xbrl.models.XbrlRowSpecification} row
   * @param {Array.<wpw.xbrl.models.XbrlSourceData>} data
   * @returns {null|wpw.xbrl.models.XbrlExportInfo}
   */
  IXBRLExportSupport.prototype.getXbrlFactsFromRow = function(document, section, row, data) {
    return null;
  };

  /**
   * If XBRL facts from checklist procedures are required
   * this method should be implemented to generate XBRL facts.
   *
   * @param {wpw.xbrl.models.XbrlContainerInfo} document
   * @param {wpw.xbrl.models.XbrlProcedureInfo} procedure
   * @param {Array.<wpw.xbrl.models.XbrlProcedureRowData>} rowData
   * @returns {null|wpw.xbrl.models.XbrlExportInfo}
   */
  IXBRLExportSupport.prototype.getXbrlFactsFromProcedure = function(document, procedure, rowData) {
    return null;
  };

  /**
   * Once all the facts are available you have a chance to pre process the document content.
   *
   * @param {string} xbrlDocument XBRL Document content.
   * @return {string} XBRL Document content that the user will download.
   */
  IXBRLExportSupport.prototype.onPrepared = function(xbrlDocument) {
    return xbrlDocument;
  };

  /**
   * After onPrepared this is the final chance to do any processing before the user downloads the document.
   *
   * @param {string} xbrlDocument XBRL Document content.
   * @return {string} XBRL Document content that the user will download.
   */
  IXBRLExportSupport.prototype.onComplete = function(xbrlDocument) {
    return xbrlDocument;
  };
})(wpw);