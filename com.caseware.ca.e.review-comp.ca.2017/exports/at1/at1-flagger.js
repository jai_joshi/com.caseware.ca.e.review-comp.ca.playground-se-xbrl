(function(wpw) {
  'use strict';

  if (!wpw.tax.utilities)
    wpw.tax.utilities = {};

  /**
   * A class with basic functions needed for flagging. An instance of this class will have it's own flaggedItems
   * array(which is an object). Identifiers for each item in the flaggedItems array are of the String type and can be
   * form or cell ids, or any other unique string. Integrated methods help to apply certain conditions to the
   * flaggedItems for changing their state. Mostly this class is used for flagging froms (determine whether they apply
   * or not to a certain situation such as tax return for example). Also, can be used to calculate business rules for a
   * certain cell such as conditional mandatory flags for Alberta at1. Important: the class and its methods should be
   * primarily optimized for fast execution. Important: methods do not support extracting values from repeated forms.
   * @param {Object} [accessor] - an object providing `field()` and `form()` methods for accessing field and forms
   * @returns {wpw.tax.Flagger}
   * @constructor
   */
  wpw.tax.utilities.Flagger = function(accessor) {
    /** @type {Object} */
    this.accessor = accessor || wpw.tax;
    /** @type {Object} */
    this.calcBlocks = {};
    /** @type {Object} */
    this.flaggedItems = {};
    /** @type {Object} */
    this.data = {};
  };

  /**
   * @callback calcBlockCallback
   * @param  {wpw.tax.utilities.Flagger} flagger
   */

  /**
   * Adds a flagging calculation block
   * @param {String} id
   * @param {calcBlockCallback} foo
   */
  wpw.tax.utilities.Flagger.prototype.addCalcBlock = function(id, foo) {
    this.calcBlocks[id] = foo;
  };

  /**
   * Runs given calculation block or array of blocks
   * @param {String|String[]} ids May be Array if calc blocks ids or just one calc block id
   * @return {Object}
   */
  wpw.tax.utilities.Flagger.prototype.calculateBlocksById = function(ids) {
    if (typeof ids === 'string') {
      this.calcBlocks[ids](this);
    } else if (ids instanceof Array) {
      var _this = this;
      ids.forEach(function(id) {
        _this.calcBlocks[id](_this);
      });
    }
    return this.flaggedItems;
  };

  /**
   * Runs all flagging calculation blocks
   * @return {Object}
   */
  wpw.tax.utilities.Flagger.prototype.calculateAllBlocks = function() {
    var _this = this;
    Object.keys(this.calcBlocks).forEach(function(key) {
      _this.calcBlocks[key](_this);
    });
    return this.flaggedItems;
  };

  /**
   * Runs all flagging calculation blocks, then removes all unflagged items from the data object and returns the data
   * object
   * @param {Object} [data]
   * @return {Object}
   */
  wpw.tax.utilities.Flagger.prototype.calculateAllBlocksAndApplyToData = function(data) {
    data = data || this.data;
    this.calculateAllBlocks();
    return this.removeUnflaggedItemsFromData(data);
  };

  /**
   * Removes unflagged items from the data object
   * @param {Object} [data] object to remove unflagged items from;
   * if the object is undefined the Flagger will try to use it's internal data object
   * @return {Object}
   */
  wpw.tax.utilities.Flagger.prototype.removeUnflaggedItemsFromData = function(data) {
    data = data || this.data;
    var flagged = this.flaggedItems;
    Object.keys(data).forEach(function(key) {
      if (!flagged[key]) {
        delete data[key];
      }
    });
    return data;
  };

  /**
   * Method applies condition to a flag (an array of flags). For example, if condition is true, flagIds will be flagged.
   * Important: if condition is false, then all flagIds will be removed from the flaggedItems list even if they've been
   * added there earlier.
   * @param {String[]|String} flagIds one or an array of ids to apply the condition
   * @param {boolean} condition
   * @return {boolean} returns the condition value
   */
  wpw.tax.utilities.Flagger.prototype.flagWithCondition = function(flagIds, condition) {
    if (flagIds instanceof Array && flagIds.length) {
      var flagged = this.flaggedItems;
      if (condition) {
        flagIds.forEach(function(flagId) {
          flagged[flagId] = true;
        });
      } else {
        flagIds.forEach(function(flagId) {
          if (flagged[flagId]) delete flagged[flagId];
        });
      }
    } else { //If flagIds is a String
      if (condition) {
        this.flaggedItems[flagIds] = true;
      } else if (this.flaggedItems[flagIds]) {
        delete this.flaggedItems[flagIds];
      }
    }
    return condition;
  };

  /**
   * Returns true if any of the given cells has value, otherwise returns false. If flagId is passed, the given Items
   * will be flagged or removed respectively.
   * @param {String} flagId flag id
   * @param {String[]|String} cellIds array of cell ids (['t2s1.100', 't2s5.200']) or just one cell id ('cp.205')
   * @param {String} [formId] used to indicate a form id if all the cells in previous parameter are from the same form
   * @return
   */
  wpw.tax.utilities.Flagger.prototype.flagIfAnyCell = function(flagId, cellIds, formId) {
    cellIds = cellIds instanceof Array && cellIds.length ? cellIds : [cellIds];
    var cellIndex = cellIds.length;
    while (cellIndex--) {
      var /** @type String */ num = cellIds[cellIndex];
      if (formId) {
        num = formId + '.' + num;
      }
      if (this.accessor.field(num).get()) {
        if (flagId) {
          this.flagItems(flagId);
        }
        return true;
      }
    }
    if (flagId) {
      this.unflagItems(flagId);
    }
    return false;
  };

  /**
   * Returns true if at least one cell in the given column(or number of columns) has value. If flagId is passed to this
   * method, the given Items will be flagged or removed respectively.
   * @param {String} flagId array of flag ids or just one flag id
   * @param {String} tableNum num of a table to check in the following format: "t2s8.090"
   * @param {Number[]|Number} columns array of column indexes or just one column index
   * @return {boolean} true if at least one cell for given columns has value
   */
  wpw.tax.utilities.Flagger.prototype.flagIfAnyColumn = function(flagId, tableNum, columns) {
    columns = columns instanceof Array && columns.length ? columns : [columns];
    var field = this.accessor.field(tableNum);
    var columnIndex = columns.length;
    while (columnIndex--) {
      var rows = field.getCol(columns[columnIndex]);
      var rowIndex = columns.length;
      while (rowIndex--) {
        if (rows[rowIndex].get()) {
          if (flagId) {
            this.flagItems(flagId);
          }
          return true;
        }
      }
    }
    if (flagId) {
      this.unflagItems(flagId);
    }
    return false;
  };

  /**
   * Adds flag(s) for the given ids
   * @param {String[]|String} flagIds array of flag ids or just one flag id
   */
  wpw.tax.utilities.Flagger.prototype.flagItems = function(flagIds) {
    if (flagIds instanceof Array && flagIds.length) {
      var flagged = this.flaggedItems;
      flagIds.forEach(function(flagId) {
        flagged[flagId] = true;
      });
    } else {
      this.flaggedItems[flagIds] = true;
    }
  };

  /**
   * Removes flag(s) with the given ids
   * @param {String[]|String} flagIds array of flag ids or just one flag id
   */
  wpw.tax.utilities.Flagger.prototype.unflagItems = function(flagIds) {
    if (flagIds instanceof Array && flagIds.length) {
      var flagged = this.flaggedItems;
      flagIds.forEach(function(flagId) {
        if (flagged[flagId]) {
          delete flagged[flagId];
        }
      });
    } else if (this.flaggedItems[flagIds]) {
      delete this.flaggedItems[flagIds];
    }
  };

})(wpw);

(function (wpw) {
  'use strict';

  /**
   * Module for declaring calculations for flagging at1 forms
   */
  if (!wpw.tax.flaggers) wpw.tax.flaggers = {};
  if (!wpw.tax.flaggers.at1) wpw.tax.flaggers.at1 = {};

  /** @type wpw.tax.utilities.Flagger */  wpw.tax.flaggers.at1 = new wpw.tax.utilities.Flagger();

  wpw.tax.flaggers.at1.addCalcBlock('mandatoryForms', function (flagger) {
    flagger.flagItems(['t2a']);
  });

  wpw.tax.flaggers.at1.addCalcBlock('t2a1', function (flagger) {
    var /** @type {function} */ form = flagger.accessor.form;
    var t2a = form('t2a');
    flagger.flagWithCondition('t2a1',
        t2a.field('030').get() !== '5'
    );
  });

  wpw.tax.flaggers.at1.addCalcBlock('t2a2', function (flagger) {
    var /** @type {function} */ form = flagger.accessor.form;
    var t2a = form('t2a');
    var t2s5 = form('t2s5');
    flagger.flagWithCondition('t2a2',
        t2a.field('062').get() > 0 &&
        ((t2s5.field('119').get() < t2s5.field('129').get()) || (t2s5.field('159').get() < t2s5.field('169').get()))
    );
  });

  wpw.tax.flaggers.at1.addCalcBlock('t2a3', function (flagger) {
    flagger.flagIfAnyCell('t2a3', ['100', '102', '200', '202']);
  });

  wpw.tax.flaggers.at1.addCalcBlock('t2a4', function (flagger) {
    flagger.flagIfAnyCell('t2a4', '014');
  });

  wpw.tax.flaggers.at1.addCalcBlock('t2a5', function (flagger) {
    flagger.flagIfAnyCell('t2a5', '021');
  });

  wpw.tax.flaggers.at1.addCalcBlock('t2a8', function (flagger) {
    flagger.flagIfAnyCell('t2a8', ['015', '030']);
  });

  wpw.tax.flaggers.at1.addCalcBlock('t2a9', function (flagger) {
    flagger.flagIfAnyCell('t2a9', '428');
  });

  wpw.tax.flaggers.at1.addCalcBlock('t2a10', function (flagger) {
    flagger.flagIfAnyCell('t2a10', ['800', '801', '802', '803', '300', '500', '700']);
  });

  wpw.tax.flaggers.at1.addCalcBlock('t2a12', function (flagger) {
    var /** @type {function} */ form = flagger.accessor.form;
    var t2a = form('t2a');
    flagger.flagWithCondition('t2a12',
        t2a.field('060').get() === '1' ||
        t2a.field('061').get() === '1'
    );
  });

  wpw.tax.flaggers.at1.addCalcBlock('t2a14', function (flagger) {
    var /** @type {function} */ form = flagger.accessor.form;
    var t2a = form('t2a');
    var t2a14 = form('t2a14');
    var t2s10 = form('t2s10');
    var cell061Value = t2a.field('061').get();
    flagger.flagWithCondition('t2a14',
        !(t2a.field('060').get() === '2' && cell061Value === '2') &&
        (cell061Value === '1' &&
            !((t2s10.field('250').get() === t2a14.field('024').get()) || (t2s10.field('410').get() === t2a14.field('040').get())))
    );
  });

  // wpw.tax.flaggers.at1.addCalcBlock('t2a15', function (flagger) {
  //   var /** @type {function} */ form = flagger.accessor.form;
  //   var t2a = form('t2a');
  //   flagger.flagWithCondition('t2a15',
  //       t2a.field('061').get() == '1'
  //   );
  // });

  wpw.tax.flaggers.at1.addCalcBlock('t2a16', function (flagger) {
    var /** @type {function} */ form = flagger.accessor.form;
    var t2a = form('t2a');
    var t2a16 = form('t2a16');
    var t661 = form('t661');
    flagger.flagWithCondition('t2a16',
        t2a.field('061').get() === '1' && (t2a16.field('002').get() !== t661.field('400').get())
    );
  });

  wpw.tax.flaggers.at1.addCalcBlock('t2a17', function (flagger) {
    var /** @type {function} */ form = flagger.accessor.form;
    var passCondition;
    var field999 = form('t2a17').field('999');
    var field201 = form('t2s13').field('201');
    var rowCount = field999.size().rows;
    for (var i = 0; i < rowCount; i++) {
      if (field999.cell(i, 3).get() !== field201.cell(i, 4).get()) {
        passCondition = true;
        break;
      }
    }
    flagger.flagWithCondition('t2a17',
        form('t2a').field('061').get() === '1' && passCondition
    );
  });

  wpw.tax.flaggers.at1.addCalcBlock('t2a18', function (flagger) {
    var /** @type {function} */ form = flagger.accessor.form;
    var t2a = form('t2a');
    var t2a18 = form('t2a18');
    var t2s13 = form('t2s13');
    flagger.flagWithCondition('t2a18',
        !(t2a.field('060').get() === '2' && t2a.field('061').get() === '2') &&
        !((t2a18.field('066').get() === t2s13.field('008').get()) ||
            (t2a18.field('068').get() === t2s13.field('010').get()))
    );
  });

  wpw.tax.flaggers.at1.addCalcBlock('t2a20', function (flagger) {
    var /** @type {function} */ form = flagger.accessor.form;
    var t2a = form('t2a');
    var t2s2 = form('t2s2');
    var passCondition;
    var fieldArr = ['238', '210', '280', '438', '480', '538', '580', '638', '680'];
    var length = fieldArr.length;
    for (var i = 0; i < length; i++) {
      if (t2s2.field(fieldArr[i]).get() !== t2s2.field(fieldArr[i] + '-A').get()) {
        passCondition = true;
        break;
      }
    }
    flagger.flagWithCondition('t2a20',
        !(t2a.field('060').get() === '2' && t2a.field('061').get() === '2') && passCondition
    );
  });

  wpw.tax.flaggers.at1.addCalcBlock('t2a21', function (flagger) {
    var /** @type {function} */ form = flagger.accessor.form;
    var t2a = form('t2a');
    var t2a21 = form('t2a21');
    var t2s4 = form('t2s4');
    var passCondition;
    var federalFieldArr = ['102', '110', '180', '200', '210', '280'];
    var albertaFieldArr = ['031', '037', '049', '051', '057', '069'];
    var length = federalFieldArr.length;
    for (var i = 0; i < length; i++) {
      if (t2s4.field(federalFieldArr[i]).get() !== t2a21.field(albertaFieldArr[i]).get()) {
        passCondition = true;
        break;
      }
    }
    flagger.flagWithCondition('t2a21',
        !(t2a.field('060').get() === '2' && t2a.field('061').get() === '2') && passCondition
    );
  });

})(wpw);



