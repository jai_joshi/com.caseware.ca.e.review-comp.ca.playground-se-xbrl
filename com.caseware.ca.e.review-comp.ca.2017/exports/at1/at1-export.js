(function(wpw) {

  var FOLDER = '/exports/at1/', FILE_NAME = "at1-export.json";
  var ALLOWABLE_CHARACTERS = "-'/&*#!@+=?$%()_;:\",.<>~`^{}[]|\\ «»ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789ÂÀÇÉÊËÈÎÏÔÖÛÜÙâàçéêëèîïôöûüù";
  var CHAR_TO_REPLACE_UNSUPPORTED = "?";
  var DOWNLOAD_XML_FILENAME  = "alberta.xml";

  if(!wpw.tax.exports) wpw.tax.exports = {};
  if(!wpw.tax.exports.at1) wpw.tax.exports.at1 = {};

  /**
   * Pads a number with leading zeros
   * @typedef {function({(Number|String)}, Number)} wpw.utilities.zeroPad
   */

  /**
   * Transforms data received from the export service to the following format:
   *
   *  {
   *    "000005001" : "123456789"
   *    "000010001" : "123456  Alberta  Corporation"
   *    "000011001" : "Alberta  Corporation:  A  Division  of  the  Canadian  Group  of  Companies..."
   *    "000012001" : "Suite  1090  Edmonton  Place"
   *  }
   *
   * For the format definitions read Chapter 3 of the Treasury
   * Board and Finance guide for Corporate Income Tax Return
   *
   * @param {Object} data
   * @return {Object}
   */
  function formatDataForRSI(data){

    /**
     * Adds given field to transformedData object
     * @param {String} formId
     * @param {Object} field
     * @param {String|number} occurrence
     */
    function addField(formId, field, occurrence){
      var value = applyMox(formatValue(field, formId), field);
      if(!wpw.tax.utilities.isEmptyOrNull(value)) {
        formId = wpw.utilities.zeroPad(formId, 3);
        var lineItemId = wpw.utilities.zeroPad(field.num, 3) +
            wpw.utilities.zeroPad(occurrence, 3);
        if(!transformedData[formId]) transformedData[formId] = {};
        transformedData[formId][lineItemId] = value;
      }
    }

    var transformedData = {};
    Object.keys(data).forEach(function(key){
      var form = data[key];
      if(form.data){
        for(var i = 0; i < form.data.length; i++){
          var field = form.data[i];
          if(field.cells){
            for(var rowIndex = 0; rowIndex < field.cells.length; rowIndex++){
              var row = field.cells[rowIndex];
              for(var colIndex = 0; colIndex < row.length; colIndex++){
                addField(form.number, row[colIndex], rowIndex + 1);
              }
            }
          } else {
            addField(form.number, field, "001");
          }
        }
      }
    });
    return transformedData;
  }

  /**
   * Apply Mandatory/Optional/Conditional rules
   * @param {undefined|String} value
   * @param field
   */
  function applyMox(value, field){
    switch(field.type){
      case "N" :
      case "$" :
        if(value == "0" && field.mox === "O"){
          value = undefined;
        } else if((value === undefined || value === "") && field.mox === "M"){
          value = "0";
        }
        break;
      case "%" :
        if((value === undefined || value === "") && field.mox === "M"){
          value = "0";
        }
        break;
      default:
    }
    return value;
  }

  /**
   * Formats value in the field in regards with its type and length
   * (the "+/-" column should be taken care by the diagnostics)
   * @param {Object} field
   * @param {String} formId
   * @return {undefined|String}
   */
  function formatValue(field, formId){

    /**
     * Replaces characters that aren't listed in the allowable string with provided replacer
     * @param {String} str
     * @param {String} replacer
     * @param {String} allowable
     * @return {String}
     */
    function repUnsupportedChars(str, replacer, allowable){
      var /** @type String[] */ aStr = str.split("");
      for(var i = 0; i < aStr.length; i++){
        if(allowable.indexOf(aStr[i]) === -1){
          aStr[i] = replacer;
        }
      }
      return aStr.join("");
    }

    /**
     * Gets max number of characters from sLength of the following format "MIN/MAX"
     * Example: "2/30" -> returns 30; "2" -> returns 2
     * @param {String} sLength
     * @return {number}
     */
    function getMaxLength(sLength) {
      if (sLength.indexOf('/') !== -1) {
        var arLength = sLength.split('/');
        return parseInt(arLength[1], 10);
      } else {
        return parseInt(sLength);
      }
    }

    var /** @type {*} */ value = field.value;
    if(value === undefined || value === null){
      return undefined;
    } else if(typeof value === 'boolean'){
      value = value ? 1 : 0;
    }
    var /** @type String */ formattedValue;
    var maxLength = getMaxLength(field.length);
    switch(field.type){
      case "$" :
        if(isNaN(value)){
          //TODO: delete this console message
          console.log("Currency field form:" + formId + " num:" + field.num + " should be numeric, but it is " + value);
        } else {
          if(value.length > maxLength){
            //TODO: delete this console message
            console.log("A currency field form:" + formId + " num:" + field.num + " should be maximum " + maxLength + " characters length, but it is " + value.length);
          } else {
            formattedValue = value;
          }
        }
        break;
      case "%" :
        var percentage = parseFloat(value);
        if(typeof percentage === 'number') {
          formattedValue = String(percentage);
          if (percentage < 1) {
            //if the value is less than 1 remove leading zero, for example: "0.09879" -> ".09879"
            formattedValue = formattedValue.substring(1);
          }
          if(formattedValue.length > maxLength){
            formattedValue = formattedValue.substring(0, maxLength);
          }
        } else {
          //TODO: delete this console message
          console.log("A percentage field form:" + formId + " num:" + field.num + " can't be parsed as a number. The value is " + value);
        }
        break;
      case "N"  :
        //replace all non-digit symbols
        formattedValue = String(value).replace(/\D/g, "");
        break;
      case "A"  :
      case "AN" :
        if(value !== ""){
          formattedValue = repUnsupportedChars(value, CHAR_TO_REPLACE_UNSUPPORTED, ALLOWABLE_CHARACTERS);
          formattedValue = String(value).substring(0, maxLength);
        }
        break;
      case "D" :
        formattedValue = value.year + wpw.utilities.zeroPad(value.month, 2) + wpw.utilities.zeroPad(value.day, 2);
        break;
      default:
        formattedValue = repUnsupportedChars(String(value), CHAR_TO_REPLACE_UNSUPPORTED, ALLOWABLE_CHARACTERS);
    }
    return formattedValue;
  }

  /**
   * Creates XML string containing cell values; the format is described in TBF RSI documentation
   * @param {Object} data object containing data to use for XML
   * @param {boolean} formatted flag that enables XML formatting
   * @return {string} generated XML
   */
  function createInnerXML(data, formatted){
    var lineBreak = "", tab = "";
    if(formatted){
      lineBreak = "\n";
      tab = "\t"
    }
    var xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + lineBreak +
        "<ReturnSubmission xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" + lineBreak +
        tab + "<Return>" + lineBreak +
        tab + tab + "<ProgramCode>01</ProgramCode>" + lineBreak;
    Object.keys(data).forEach(function(formId) {
      xml = xml + tab + tab + "<Schedule Number=\"" + formId + "\">" + lineBreak;
      Object.keys(data[formId]).forEach(function(lineItemId) {
        xml = xml + tab + tab + tab + "<Value LineItemID=\"" + formId + lineItemId + "\">" + data[formId][lineItemId] +
            "</Value>" + lineBreak;
      });
      xml = xml + tab + tab + "</Schedule>" + lineBreak;
    });
    xml = xml + tab + tab + "</Return>" + lineBreak +
        "</ReturnSubmission>";
    return xml;
  }

  /**
   * Generates RSI specific XML described in TBF RSI documentation
   * @param {Object} data object with the data for XML
   * @return {string} generated XML string
   */
  wpw.tax.exports.at1.generateXML = function(data){
    return "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>" +
        "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:cit=\"http://cit.tra.fin.goa/\">" +
        "<soap:Header/>" +
        "<soap:Body>" +
        "<cit:fileReturn>" +
        "<arg0><![CDATA[" + createInnerXML(data, false) + "]]></arg0></cit:fileReturn>" +
        "</soap:Body>" +
        "</soap:Envelope>";
  };

  var /** @type {boolean} */ _innerOnly;

  /**
   * Generates and downloads Alberta XML
   * @param {boolean} innerOnly flag to generate only inner xml
   */
  wpw.tax.exports.at1.downloadXML = function(innerOnly){
    _innerOnly = innerOnly;
    wpw.tax.exports.at1.getData(function(data){
      var xml;
      if(_innerOnly){
        xml = createInnerXML(data, true);
      } else {
        xml = wpw.tax.exports.at1.generateXML(data);
      }
      var a = document.createElement("a");
      document.body.appendChild(a);
      a.style.display = "none";
      var blob = new Blob([xml], {type: "octet/stream"});
      var url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = DOWNLOAD_XML_FILENAME;
      a.click();
      window.URL.revokeObjectURL(url);
    },function(err){
      alert("Problem downloading xml:" + err);
    });
  };

  /**
   * Method for extracting values for AT1
   * @async
   * @function getData
   * @param {function(Object)} success
   * @param {function(Object)} error
   */
  wpw.tax.exports.at1.getData = function(success, error){
    var exportService = wpw.tax.services.exportService;
    if(!exportService) {
      console.error("Error: exportService isn't defined");
      return;
    }
    var url = wpw.global.staticRoot + '../e/prod/' + wpw.tax.global.engagementProperties.source + FOLDER + FILE_NAME;
    exportService.loadJSONFile(url, function(json) {
      //extract values declared in the json
      var data = exportService.getData(json);
      //remove forms that aren't flagged
      data = wpw.tax.flaggers.at1.calculateAllBlocksAndApplyToData(data);
      //format values according with their type, length
      data = formatDataForRSI(data);
      success(data);
    }, function(err) {
      error(err);
    });
  };

})(wpw);