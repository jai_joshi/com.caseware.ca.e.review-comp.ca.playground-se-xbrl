(function() {
  'use strict';

  /** Return an array with objects indicating whether each form is applied.
   * @param {Object} [accessor] - an object providing `field()` and `form()` methods for accessing field and forms
   * @returns {Object}
   */
  wpw.tax.utilities.flagger = function(accessor) {
    accessor = accessor || wpw.tax;

    var flaggedResult = {"T2J": true};

    /**
     * the function returns an object of the following format:
     {
       "T2S100": true,
       "T2S101": true,
       "T2S125": true,
       "T2S140": true,
       "T2S141": true,
       "T2S141S": true,
       "T2S50": true,
       "T2S1": true,
       "T2S8": true,
       "T2S21": true
      }
     */

    /**
     * This is the flagging method by check field id with various conditions such as non-numerical fields
     * @param conds
     * @param form
     */
    function flagging(conds, form) {
      if (conds) {
        flaggedResult[form] = true;
        return true;
      }
      else {
        //Note: we don't need the forms that are not applied
        //flaggedResult[form] = false;
        if (flaggedResult[form]) {
          delete flaggedResult[form];
        }
      }
    }

    /**
     * alternative approach for flagging
     * @param arr
     * @param form
     */
    function altflagging(arr, form) {
      arr.forEach(function(num) {
        var value;
        if (num.indexOf('.') > -1) {
          value = accessor.field(num.toLowerCase()).get()
        } else {
          value = accessor.field(form.toLowerCase() + '.' + num).get();
        }
        if (Number(value) == 'NaN' && value.toString().trim()) {
          flaggedResult[form] = true;
          return;
        }
        else if (Number(value) > 0) {
          flaggedResult[form] = true;
          return;
        }
      });
    }

    /**
     * checking conditions throughout tables inside the flagging function
     * @param num
     * @param col
     * @param form
     * @return {boolean}
     */
    function checkingTables(num, col, form) {
      if (col instanceof Array) {
        col.forEach(function(colindex) {
          accessor.field(num).getCol(colindex).forEach(function(cell) {
            if (cell.get()) {
              return true;
            }
          });
        })
      }
      else {
        accessor.field(num).getCol(col).forEach(function(cell) {
          if (cell.get()) {
            return true;
          }
        });
      }
      return false;

    }

    /**
     * fixed condition for flagging
     * @param form
     */
    function gifiFlagging(form) {
      flaggedResult[form] = true;
    }

    /**
     * fixed conditions for version control
     * especially for forms implemented but not required to flag
     * @param form
     */
    function versionControl(form) {
      //flaggedResult[form] = false;
      if (flaggedResult[form]) {
        delete flaggedResult[form];
      }
    }

    /**
     * further calling flagging function, return nothing
     * @param {string} prov lower-cased characters representing provincial codes
     * @param {string} sch lower-cased schedule number without letters
     * @param {array} des an array of the destination going to t2s5 exclusively
     */
    function provflagging(prov, sch, des) {

      var precheck = false;

      des.forEach(function(num) {
        if (num.toString().indexOf('!') > -1) {
          num = num.indexOf('!') > -1 ? num.replace('!', '') : num;
          if (!accessor.field('t2s5.' + num).get()) {
            precheck = true;
            return;
          }
        }
        else if (accessor.field('t2s5.' + num).get() ? accessor.field('t2s5.' + num).get() : 0 > 0) {
          precheck = true;
          return;
        }
      });

      switch (prov.toLowerCase()) {
        case 'nl':
          flagging(((accessor.field('CP.750').get() == "NL" ||
              accessor.field('CP.750').get() == "XO" ||
              (accessor.field('t2s5.003').get() &&
                  accessor.field('CP.750').get() == "MJ") ||
              (accessor.field('t2s5.004').get() &&
                  accessor.field('CP.750').get() == "MJ")) && precheck), 'T2S' + sch);
          return;
        case 'pei':
          flagging(((accessor.field('CP.750').get() == "PE" ||
              (accessor.field('t2s5.005').get() &&
                  accessor.field('CP.750').get() == "MJ")) && precheck), 'T2S' + sch);
          return;
        case 'ns':
          flagging(((accessor.field('CP.750').get() == "NS" ||
              accessor.field('CP.750').get() == "NO" ||
              (accessor.field('t2s5.007').get() &&
                  accessor.field('CP.750').get() == "MJ") ||
              (accessor.field('t2s5.008').get() &&
                  accessor.field('CP.750').get() == "MJ")) && precheck), 'T2S' + sch);
          return;
        case 'nb':
          flagging(((accessor.field('CP.750').get() == "NB" ||
              (accessor.field('t2s5.009').get() &&
                  accessor.field('CP.750').get() == "MJ")) &&
              precheck), 'T2S' + sch);
          return;
        case 'mb':
          flagging(((accessor.field('CP.750').get() == "MB" ||
              (accessor.field('t2s5.015').get() &&
                  accessor.field('CP.750').get() == "MJ")) &&
              precheck), 'T2S' + sch);
          return;
        case 'sk':
          flagging(((accessor.field('CP.750').get() == "SK" ||
              (accessor.field('t2s5.017').get() &&
                  accessor.field('CP.750').get() == "MJ")) &&
              precheck), 'T2S' + sch);
          return;
        case 'bc':
          flagging(((accessor.field('CP.750').get() == "BC" ||
              (accessor.field('t2s5.021').get() &&
                  accessor.field('CP.750').get() == "MJ")) &&
              precheck), 'T2S' + sch);
          return;
        case 'yt':
          flagging(((accessor.field('CP.750').get() == "YT" ||
              (accessor.field('t2s5.023').get() &&
                  accessor.field('CP.750').get() == "MJ")) &&
              precheck), 'T2S' + sch);
          return;
        case 'nt':
          flagging(((accessor.field('CP.750').get() == "NT" ||
              (accessor.field('t2s5.025').get() &&
                  accessor.field('CP.750').get() == "MJ")) &&
              precheck), 'T2S' + sch);
          return;
        case 'nu':
          flagging(((accessor.field('CP.750').get() == "NU" ||
              (accessor.field('t2s5.026').get() &&
                  accessor.field('CP.750').get() == "MJ")) &&
              precheck), 'T2S' + sch);
          return;
        case 'on':
          flagging(((accessor.field('CP.750').get() == "ON" ||
              (accessor.field('t2s5.013').get() &&
                  accessor.field('CP.750').get() == "MJ")) &&
              precheck), 'T2S' + sch);
          return;
        case 'no':
          flagging(((accessor.field('cp.750').get() == "NO" ||
              (accessor.field('t2s5.008').get() &&
                  accessor.field('cp.750').get() == "MJ")) &&
              precheck), 'T2S' + sch);
          return;
      }
    }

    // gifiFlagging('T2S100');
    // gifiFlagging('T2S101');
    // gifiFlagging('T2S125');
    // gifiFlagging('T2S140');
    // gifiFlagging('T2S141');
    gifiFlagging('T2S141S');

    // -----------------------------------  GIFI -------------------------------------------------------------------------------
    flagging((accessor.form('t2s100').field('2599').getKey(0) != 0 ||
        accessor.form('t2s100').field('3499').getKey(0) != 0 ||
        accessor.form('t2s100').field('3620').getKey(0) != 0), 'T2S100');
    flagging(((accessor.form('t2s101').field('2599').getKey(0) != 0 ||
        accessor.form('t2s101').field('3499').getKey(0) != 0 ||
        accessor.form('t2s101').field('3620').getKey(0) != 0)), 'T2S101');

    wpw.tax.form.allRepeatForms('t2s125').forEach(function(form) {
      if ((form.field('8299').getKey(0) != 0 ||
          form.field('9368').getKey(0) != 0) ||
          (form.field('9659').getKey(0) != 0 ||
              form.field('9898').getKey(0) != 0)) {
        flaggedResult['T2S125'] = true;
      }
    });

    flagging((accessor.form('t2s140').field('9970').getKey(0) != 0), 'T2S140');
    flagging((accessor.form('t2s141').field('101').get() && accessor.form('t2s141').field('108').get()), 'T2S141');

    // -----------------------------------  attachment section -----------------------------------------------------------------
    function temps9selfcheckfunc() {
      var temps9selfcheck = false;
      accessor.field('t2s9.050').forEachRow(function(row) {
        // entries with filing corp does not flag
        if (row[2].get() != 'NR' &&
            // the assocaited rule from t2s23 data rule
            ((row[2].get() && (row[3].get() == '1' || row[3].get() == '2' || row[3].get() == '3' && row[4])) ||
                // if efile diagnostics are triggered
                row[4].get() || row[5].get() || row[6].get() || row[7].get())) {
          temps9selfcheck = true;
        }
      });
      return temps9selfcheck;
    }

    // t2s9 are filled in with more than one entries
    if (accessor.field('t2s9.050') && Number(accessor.field('t2s9.050').size().rows) > 1) {
      flagging(temps9selfcheckfunc, 'T2S9');
    }

    //todo: Ignored master agreement requirement for one associated group. Should modify when linked returns can be achieved
    // currently ignored the master agreement requirement from the last part of the data rule
    flagging(accessor.field('t2j.040').get() == '1' &&
        ((Number(accessor.field('t2s31.400').get()) > 0 ||
            Number(accessor.field('t2s31.410').get()) > 0) &&
            (Number(accessor.field('t2s31.350').get()) > 0 ||
                Number(accessor.field('t2s31.360').get()) > 0)) &&
        (wpw.tax.utilities.isNullUndefined(accessor.field('t2s31.385').get()) ||
            accessor.field('t2s31.385').get() == '1'), 'T2S49');

    // an alternative when function foreachrow always return true
    function relatedChecks(schedule) {
      var temps9 = false;
      var temps23 = false;
      var temps28 = false;
      var temps49 = false;

      switch (schedule.toLowerCase()) {
        case 't2s9':
          accessor.field('t2s9.050').forEachRow(function(row) {
            if (row[2].get() != 'NR' &&
                (row[3].get() == '1' || row[3].get() == '2' || row[3].get() == '3') && Number(row[4].get()) > 0) {
              temps9 = true;
            }
          });
          return temps9;
        case 't2s23':
          accessor.field('t2s23.085').forEachRow(function(row) {
            if (!(row[1].get() == accessor.field('cp.bn').get()) && row[1].get() != 'NR' && row[2].get()) {
              temps23 = true;
            }
          });
          return temps23;
        case 't2s28':
          accessor.field('t2s23.085').forEachRow(function(row) {
            if (!(row[1].get() == accessor.field('cp.bn').get()) && row[1].get() != 'NR' && row[2].get() == '2') {
              temps28 = true;
            }
          });
          return temps28;
        case 't2s49':
          accessor.field('t2s49.085').forEachRow(function(row) {
            if (!(row[1].get() == accessor.field('cp.bn').get()) && row[1].get() != 'NR' && row[2].get()) {
              temps49 = true;
            }
          });
          return temps49;
      }

      return false;
    }

    flagging(Boolean(accessor.field('t2j.040').get() == '1' &&
        Boolean(Number(accessor.field('t2j.430').get()) > 0 || accessor.field('t2j.896').get() == '1' || accessor.field('cp.119').get() == '1'|| accessor.field('t2s23.085').size().rows > '1') &&
        Boolean(relatedChecks('t2s9') || relatedChecks('t2s23') || relatedChecks('t2s49') || accessor.field('t2s31.400').get())), 'T2S23');

    flagging((accessor.field('t2j.040').get() && relatedChecks('t2s28')), 'T2S28');

    if (accessor.field('t2s19.050')) {
      flagging((accessor.field('t2s19.050').cell(0, 0).get()), 'T2S19');
    }
    flagging((accessor.field('t2s11.050').cell(0, 0).get()), 'T2S11');
    flagging((accessor.field('t2s44.500').cell(0, 0).get() ||
        accessor.field('t2s44.500').cell(0, 1).get() ||
        accessor.field('t2s44.500').cell(0, 2).get()), 'T2S44');
    flagging((accessor.field('t2s14.001').cell(0, 2).get() ||
        accessor.field('t2s14.001').cell(0, 3).get() ||
        accessor.field('t2s14.001').cell(0, 4).get() ||
        accessor.field('t2s14.001').cell(0, 5).get() ||
        accessor.field('t2s14.001').cell(0, 6).get()), 'T2S14');
    flagging((accessor.field('t2s15.101').cell(0, 1).get()), 'T2S15');
    //todo: no t5004
    //todo: no t5013
    flagging(((accessor.field('t2s22.050').cell(0, 1).get() &&
        accessor.field('t2s22.050').cell(0, 1).get().province &&
        accessor.field('t2s22.050').cell(0, 1).get().province != "") ||
        accessor.field('t2s22.050').cell(0, 0).get() ||
        accessor.field('t2s22.050').cell(0, 2).get()), 'T2S22');
    flagging((accessor.field('t2s25.050').cell(0, 2).get() ||
        accessor.field('t2s25.050').cell(0, 0).get() ||
        accessor.field('t2s25.050').cell(0, 1).get()), 'T2S25');
    flagging((accessor.field('t2s29.050').cell(0, 3).get()), 'T2S29');
    // altflagging(['701', '702', '704', '706', '711', '712', '714', '716', '718', '721', '731', '732', '734', '736', '738', '740'], 'T1263');

    wpw.tax.form.allRepeatForms('t2s1263').forEach(function(form) {
      if (form.field('701').get() ||
          form.field('702').get() ||
          form.field('704').get() ||
          form.field('706').cell(0, 0).get() ||
          form.field('711').get() ||
          form.field('712').get() ||
          form.field('714').get() ||
          form.field('716').get() ||
          form.field('718').get() ||
          form.field('721').get() ||
          form.field('731').get() ||
          form.field('732').get() ||
          form.field('734').get() ||
          form.field('736').get() ||
          form.field('738').get() ||
          form.field('740').get()) {
        flaggedResult['T2S1263'] = true;
      }
    });

    if(accessor.field('t2s50.050').size().rows > 0) {
      flagging((accessor.field('t2s50.050').cell(0, 0).get()), 'T2S50');
    }
    altflagging(['101', '102', '103', '104', '105', '106', '107', '108', '110', '111', '112', '113', '114', '115', '116', '117', '118', '119', '120', '121', '122', '123', '124', '125', '126', '127', '128', '129', '130', '131', '132', '199', '201', '202', '203', '204', '206', '208', '209', '210', '211', '212', '213', '214', '215', '216', '217', '218', '219', '220', '221', '222', '224', '226', '227', '228', '229', '230', '231', '232', '233', '234', '235', '236', '237', '238', '239', '248', '249', '300', '301', '302', '303', '304', '306', '307', '309', '310', '311', '312', '313', '314', '315', '316', '340', '341', '342', '344', '345', '347', '348', '349', '395', '396', '401', '402', '403', '404', '405', '406', '407', '408', '409', '410', '411', '413', '414', '416', '417', '499', '500', '510', '705', '295', '296', '605'], 'T2S1');
    flagging((accessor.field('t2s2.210').get() ||
        accessor.field('t2s2.240').get() ||
        accessor.field('t2s2.250').get() ||
        accessor.field('t2s2.310').get() ||
        accessor.field('t2s2.340').get() ||
        accessor.field('t2s2.350').get() ||
        accessor.field('t2s2.410').get() ||
        accessor.field('t2s2.440').get() ||
        accessor.field('t2s2.450').get() ||
        accessor.field('t2s2.510').get() ||
        accessor.field('t2s2.520').get() ||
        accessor.field('t2s2.540').get() ||
        accessor.field('t2s2.550').get() ||
        accessor.field('t2s2.610').get() ||
        accessor.field('t2s2.640').get() ||
        accessor.field('t2s2.650').get()), 'T2S2');
    flagging((accessor.field('t2s3.360').get() ||
        accessor.field('t2s3.600').get() ||
        //TODO: confirming with efile regarding tn assignment issue for table columns
        accessor.field('t2s3.240').get() ||
        //TODO: confirming with efile regarding tn assignment issue for table columns
        accessor.field('t2s3.430').get() ||
        accessor.field('t2s3.450').get() ||
        accessor.field('t2s3.510').get() ||
        accessor.field('t2s3.520').get() ||
        accessor.field('t2s3.530').get() ||
        accessor.field('t2s3.540').get()), 'T2S3');
    if ((
        accessor.field('t2s4.100').get() ||
        accessor.field('t2s4.102').get() ||
        accessor.field('t2s4.895').get() ||
        accessor.field('t2s4.150').get() ||
        accessor.field('t2s4.140').get() ||
        accessor.field('t2s4.889').get() ||
        accessor.field('t2s4.887').get() ||
        accessor.field('t2s4.885').get() ||
        accessor.field('t2s4.225').get() ||
        accessor.field('t2s4.878').get() ||
        accessor.field('t2s4.280').get() ||
        accessor.field('t2s4.300').get() ||
        accessor.field('t2s4.874').get() ||
        accessor.field('t2s4.871').get() ||
        accessor.field('t2s4.868').get() ||
        accessor.field('t2s4.485').get() ||
        accessor.field('t2s4.400').get() ||
        accessor.field('t2s4.857').get() ||
        accessor.field('t2s4.440').get() ||
        accessor.field('t2s4.851').get() ||
        accessor.field('t2s4.500').get() ||
        accessor.field('t2s4.846').get() ||
        accessor.field('t2s4.843').get() ||
        accessor.field('t2s4.190').get() ||
        checkingTables('t2s4.700', [0, 1, 2, 3, 4, 5], 'T2S4') ||
        checkingTables('t2s4.710', [2, 3, 4, 6], 'T2S4') ||
        checkingTables('t2s4.720', [2, 4], 'T2S4'))) {
      gifiFlagging('T2S4');
    }
    flagging(Boolean(Boolean(accessor.field('t2s5.1033').get() && accessor.field('CP.750').get() == "MJ") && accessor.field('t2s5.255').get()) ||
        // Part 2 - NL
        Boolean(accessor.field('CP.750').get() == "NL" && accessor.field('t2s5.200').get() != 0 && Boolean(accessor.field('t2s5.200').get() != accessor.field('t2s5.209').get())) ||
        // Part 2 - PE
        Boolean(accessor.field('CP.750').get() == "PE" && accessor.field('t2s5.210').get() != 0 && Boolean(accessor.field('t2s5.210').get() != accessor.field('t2s5.214').get())) ||
        // Part 2 - NS
        Boolean(accessor.field('CP.750').get() == "NS" && accessor.field('t2s5.215').get() != 0 && Boolean(accessor.field('t2s5.215').get() != accessor.field('t2s5.224').get())) ||
        // Part 2 - NB
        Boolean(accessor.field('CP.750').get() == "NB" && accessor.field('t2s5.225').get() != 0 && Boolean(accessor.field('t2s5.225').get() != accessor.field('t2s5.229').get())) ||
        // Part 2 - ON
        Boolean(accessor.field('CP.750').get() == "ON" && accessor.field('t2s5.270').get() != 0 && Boolean(accessor.field('t2s5.270').get() != accessor.field('t2s5.290').get())) ||
        // Part 2 - MB
        Boolean(accessor.field('CP.750').get() == "MB" && accessor.field('t2s5.230').get() != 0 && Boolean(accessor.field('t2s5.230').get() != accessor.field('t2s5.234').get())) ||
        // Part 2 - SK
        Boolean(accessor.field('CP.750').get() == "SK" && accessor.field('t2s5.235').get() != 0 && Boolean(accessor.field('t2s5.235').get() != accessor.field('t2s5.239').get())) ||
        // Part 2 - BC
        Boolean(accessor.field('CP.750').get() == "BC" && accessor.field('t2s5.240').get() != 0 && Boolean(accessor.field('t2s5.240').get() != accessor.field('t2s5.244').get())) ||
        // Part 2 - YT
        Boolean(accessor.field('CP.750').get() == "YT" && accessor.field('t2s5.245').get() != 0 && Boolean(accessor.field('t2s5.245').get() != accessor.field('t2s5.249').get())) ||
        // Part 2 - NT
        Boolean(accessor.field('CP.750').get() == "NT" && accessor.field('t2s5.250').get() != 0 && Boolean(accessor.field('t2s5.250').get() != accessor.field('t2s5.254').get())) ||
        // Part 2 - NU
        Boolean(accessor.field('CP.750').get() == "NU" && accessor.field('t2s5.260').get() != 0 && Boolean(accessor.field('t2s5.260').get() != accessor.field('t2s5.264').get())) ||
        // Part 2 - XO
        Boolean(accessor.field('cp.750').get() == "XO" && (accessor.field('t2s5.200').get() != 0 || accessor.field('t2s5.209').get() != 0) &&
            (Boolean(accessor.field('t2s5.205').get() > 0 ? accessor.field('t2s5.205').get() != accessor.field('t2s5.209').get(): Boolean(accessor.field('t2s5.200').get() != accessor.field('t2s5.209').get())))) ||
        // Part 2 - NO
        Boolean(accessor.field('cp.750').get() == "NO" && (accessor.field('t2s5.215').get() != 0 || accessor.field('t2s5.224').get() != 0) &&
            (Boolean(accessor.field('t2s5.220').get() > 0 ? accessor.field('t2s5.220').get() != accessor.field('t2s5.224').get(): Boolean(accessor.field('t2s5.215').get() != accessor.field('t2s5.224').get())))), 'T2S5');


    flagging((accessor.field('t2s6.101').total(4).get() ||
        accessor.field('t2s6.101').total(5).get() ||
        accessor.field('t2s6.101').total(6).get() ||
        accessor.field('t2s6.101').total(7).get() ||
        accessor.field('t2s6.160').get() ||

        accessor.field('t2s6.201').total(2).get() ||
        accessor.field('t2s6.201').total(3).get() ||
        accessor.field('t2s6.201').total(4).get() ||
        accessor.field('t2s6.201').total(5).get() ||

        accessor.field('t2s6.301').total(4).get() ||
        accessor.field('t2s6.301').total(5).get() ||
        accessor.field('t2s6.301').total(6).get() ||
        accessor.field('t2s6.301').total(7).get() ||

        accessor.field('t2s6.401').total(2).get() ||
        accessor.field('t2s6.401').total(3).get() ||
        accessor.field('t2s6.401').total(4).get() ||
        accessor.field('t2s6.401').total(5).get() ||

        accessor.field('t2s6.501').total(2).get() ||
        accessor.field('t2s6.501').total(3).get() ||
        accessor.field('t2s6.501').total(4).get() ||
        accessor.field('t2s6.501').total(5).get() ||

        accessor.field('t2s6.601').total(2).get() ||
        accessor.field('t2s6.601').total(3).get() ||
        accessor.field('t2s6.601').total(4).get() ||
        accessor.field('t2s6.601').total(5).get() ||
        accessor.field('t2s6.655').get() ||

        accessor.field('t2s6.700').total(3).get() ||
        accessor.field('t2s6.700').total(4).get() ||
        accessor.field('t2s6.700').total(5).get() ||
        accessor.field('t2s6.700').total(6).get() ||

        accessor.field('t2s6.875').get() ||
        accessor.field('t2s6.880').get() ||
        accessor.field('t2s6.885').get() ||
        accessor.field('t2s6.890').get() ||
        accessor.field('t2s6.999').get() ||
        accessor.field('t2s6.899').get() ||
        accessor.field('t2s6.901').get()), 'T2S6');
    flagging((
        accessor.field('t2j.040').get() == '1' && (
            Boolean(accessor.field('t2j.440').get() && Boolean(accessor.field('t2s7.002').get() != accessor.field('t2s7.092').get())) ||
            Boolean(accessor.field('t2j.400').get() && Boolean(accessor.field('t2s7.501').get() != accessor.field('t2s7.545').get())) ||
            Boolean(accessor.field('t2s7.385').get()) ||
            Boolean(accessor.field('t2s7.360').get()) ||
            Boolean(accessor.field('t2j.440').get()) ||
            Boolean(accessor.field('t2j.445').get())
        )), 'T2S7');
    flagging((accessor.field('t2s8.100').total(4).get() ||
        accessor.field('t2s8.100').total(5).get() ||
        accessor.field('t2s8.100').total(6).get()), 'T2S8');
    flagging((accessor.field('t2s10.300').get() ||
        accessor.field('t2s10.410').get() ||
        accessor.field('t2s10.445').get() ||
        accessor.field('t2s10.470').get() ||
        accessor.field('t2s10.480').get()
    ), 'T2S10');

    flagging((accessor.field('t2s13.010').get() ||
        accessor.field('t2s13.280').get()), 'T2S13');
    flagging((accessor.field('t2s1.416').get()), 'T2S16');

    flagging((accessor.field('t2s21.120').get() ||
        accessor.field('t2s21.130').get() ||
        accessor.field('t2s21.180').get() ||
        accessor.field('t2s21.221').get() ||
        accessor.field('t2s21.280').get() ||
        checkingTables('t2s21.300', 4, 'T2S21') ||
        checkingTables('t2s21.401', [2, 3, 4], 'T2S21') ||
        accessor.field('t2s21.580').get()
        // ||    accessor.field('t2s21.610').get()  ||
        // accessor.field('t2s21.620').get()
    ), 'T2S21');
    flagging(
        //Part 1 - SBD qualification for M&P
        (accessor.field('t2s27.090').get() == '1' &&
            accessor.field('t2s27.091').get() == '1' &&
            accessor.field('t2s27.092').get() == '1' &&
            accessor.field('t2s27.093').get() == '1') ||
        // Part 2 - non-SBD M&P
        (Number(accessor.field('t2s27.030').get()) > 0) ||
        //Part 10 - eligible activities
        (Number(accessor.field('t2s27.372').get()) > 0) ||
        // Amount transferring to T2J line 616
        (accessor.field('t2s27.366').get() || accessor.field('t2s27.417').get()), 'T2S27');
    flagging((accessor.field('t2s31.1004').get() ||
        accessor.field('t2s31.1010').get()), 'T2S31');
    altflagging(['t2s1.231', 't2s1.411', 't2s31.350', 't2s31.360', 't2s31.370', 't2s31.420', 't2s31.430', 't2s31.440', 't2s31.450', 't2s31.460', 't2s31.480', 't2s31.490'], 'T661');
    altflagging(['t2s1.231', 't2s1.411', 't2s31.350', 't2s31.360', 't2s31.370', 't2s31.420', 't2s31.430', 't2s31.440', 't2s31.450', 't2s31.460', 't2s31.480', 't2s31.490'], 'T661S');

    // flagging((accessor.field('t2s37.420').get()), 'T2S37');

    flagging((accessor.field('t2j.708').get()), 'T2S46');
    flagging((accessor.field('t2j.796').get()), 'T2S47');
    flagging((accessor.field('t2j.797').get()), 'T2S48');
    flagging((accessor.field('t2s55.190').get() ||
        accessor.field('t2s55.200').get() ||
        accessor.field('t2s55.100').get() ||
        accessor.field('t2s55.290').get()), 'T2S55');

    wpw.tax.form.allRepeatForms('t2s53').forEach(function(form) {
      if (form.field('590').get() || form.field('4427').get())
        flaggedResult['T2S53'] = true;
    });

    wpw.tax.form.allRepeatForms('t2s54').forEach(function(form) {
      if (form.field('120').get() ||
          form.field('140').get() ||
          form.field('150').get() ||
          form.field('160').get() ||
          form.field('302').get() ||
          form.field('352').get() ||
          form.field('199').getCols([1, 2, 4, 5]).forEach(function(col) {
            col.forEach(function(row) {
              if (Number(row.get()) > 0) {
                return true;
              }
            })
          }) ||
          form.field('205').getCols([2, 3]).forEach(function(col) {
            col.forEach(function(row) {
              if (Number(row.get()) > 0) {
                return true;
              }
            })
          })) {
        flaggedResult['T2S54'] = true;
      }
    });

    altflagging(['010', '015', '020', '040', '045', '050', '055', '060', '070', '075', '080', '085', '090'], 'T2S61');
    altflagging(['010', '015', '020', '040', '045', '050', '055', '060', '070', '075', '080', '085', '090', '100', '102', '106', '110', '114'], 'T2S62');

    //todo: outstanding federal
    flagging((accessor.field('cp.070').get() == "1" || accessor.field('cp.071').get() == "1" ||
        accessor.field('cp.072').get() == "1"), 'T2S24');
    flagging((Number(accessor.field('t2s33.190').get()) > 10000000 &&
        (accessor.field('t2s33.610').get() || accessor.field('t2s33.701').get()) &&
        accessor.field('t2s33.113').get()
    ), 'T2S33');

    // t2s61, t2s63
    wpw.tax.form.allRepeatForms('t1145').forEach(function(form) {
      if (form.field('015').get() || form.field('020').get() || form.field('010').get())
        flaggedResult['T1145'] = true;
    });
    wpw.tax.form.allRepeatForms('t1146').forEach(function(form) {
      if (form.field('015').get() || form.field('020').get() || form.field('010').get())
        flaggedResult['T1146'] = true;
    });

    flagging((accessor.field('t2s1.130').get() || accessor.field('t2s1.131').get()), 'T2S73');
    flagging((accessor.field('t2j.180').get() == "1" && wpw.tax.utilities.dateCompare.greaterThan(accessor.field('cp.tax_end').get(), {
      day: 30,
      month: 6,
      year: 2014
    })), 'T2S88');

    //todo: for t2s33, t2s34, t2s35
    if ((flaggedResult['T2S33'] && flaggedResult['T2S34'])) {
      delete flaggedResult['T2S33'];
      delete flaggedResult['T2S34'];
    }
    else if ((flaggedResult['T2S33'] && flaggedResult['T2S35'])) {
      delete flaggedResult['T2S33'];
      delete flaggedResult['T2S35'];
    }
    else if ((flaggedResult['T2S34'] && flaggedResult['T2S35'])) {
      delete flaggedResult['T2S34'];
      delete flaggedResult['T2S35'];
    }

    flagging((accessor.field('t2j.082').get() == '2' &&
        accessor.field('t2s97.300').get() == '1') ||
        accessor.field('t2j.082').get() == '1', 'T2S91');
    flagging((accessor.field('t2j.070').get() == '1' &&
        accessor.field('t2sj.080').get() == '2') ||
        accessor.field('t2s97.300').get(), 'T2S97');

    //todo: ------------------------------- NL -------------------------------------------------------------------------
    provflagging('nl', 300, [503]);
    provflagging('nl', 301, [520]);
    provflagging('nl', 302, [521]);
    provflagging('nl', 303, [505]);
    provflagging('nl', 304, [507]);
    provflagging('nl', 307, [200, 205]);
    provflagging('nl', 308, [504]);
    provflagging('nl', 309, [522, !840]);

    //todo: --------------------------------- PEI ----------------------------------------------------------------------
    provflagging('pei', 321, [530]);
    provflagging('pei', 322, [210]);

    //todo: --------------------------------- NS ----------------------------------------------------------------------
    provflagging('ns', 340, [221, 566]);
    provflagging('ns', 341, [556]);
    flagging(((accessor.field('CP.750').get() == "NS" ||
        accessor.field('CP.750').get() == "NO" ||
        (accessor.field('t2s5.007').get() &&
            accessor.field('CP.750').get() == "MJ") ||
        (accessor.field('t2s5.008').get() &&
            accessor.field('CP.750').get() == "MJ")) &&
        (accessor.field('cp.226').get() == '1') &&
        (accessor.field('t2s33.500').get() < 10000000 ||
            accessor.field('t2s34.500').get() < 10000000 ||
            accessor.field('t2s35.500').get() < 10000000)), 'T2S343');
    provflagging('ns', 344, [561]);
    provflagging('ns', 345, [565]);
    provflagging('ns', 346, [215, 220]);
    provflagging('ns', 347, [567]);
    provflagging('ns', 348, [569]);

    //todo: ------------------------------------------- NB -------------------------------------------------------------
    provflagging('nb', 360, [573, 597]);
    provflagging('nb', 365, [595]);
    provflagging('nb', 366, [225]);
    provflagging('nb', 367, [578]);

    //todo: ------------------------------------------- MB -------------------------------------------------------------
    provflagging('mb', 380, [606, 613]);
    provflagging('mb', 381, [605, 621]);
    provflagging('mb', 383, [230]);
    provflagging('mb', 384, [603, 622]);
    provflagging('mb', 385, [607, 623]);
    provflagging('mb', 387, [608]);
    provflagging('mb', 388, [620]);
    provflagging('mb', 389, [615]);
    provflagging('mb', 390, [609, 612]);
    flagging((accessor.field('cp.750').get() == 'MB' ||
        (accessor.field('cp.750').get() == 'MJ' && accessor.field('t2s5.015').get()))
        && (accessor.field('t2s391.122').get() > 0 || accessor.field('t2s5.610').get() > 0), 'T2S391');
    provflagging('mb', 392, [324]);
    provflagging('mb', 393, [325]);
    provflagging('mb', 394, [602, 326]);

    //todo: ------------------------------------------ SK --------------------------------------------------------------
    provflagging('sk', 400, [632]);
    provflagging('sk', 402, [630, 644]);
    provflagging('sk', 403, [631, 645]);
    provflagging('sk', 404, [626]);
    provflagging('sk', 411, [235]);

    //todo: ------------------------------------------ BC --------------------------------------------------------------
    provflagging('bc', 421, [673]);
    provflagging('bc', 422, [671]);
    provflagging('bc', 423, [672]);
    provflagging('bc', 425, [674, 659]);
    provflagging('bc', 427, [240]);
    provflagging('bc', 428, [679]);
    provflagging('bc', 429, [680]);
    provflagging('bc', 430, [681]);

    //todo: --------------------------------- YT ----------------------------------------------------------------------
    provflagging('yt', 440, [677]);
    provflagging('yt', 441, [697]);
    provflagging('yt', 442, [698]);
    provflagging('yt', 443, [245]);

    //todo: --------------------------------- NT ----------------------------------------------------------------------
    provflagging('nt', 460, [705]);
    provflagging('nt', 461, [250]);

    //todo: --------------------------------- NU ----------------------------------------------------------------------
    provflagging('nu', 480, [735]);
    provflagging('nu', 481, [260]);

    //todo: ------------------------------------------ ON --------------------------------------------------------------
    provflagging('on', 500, [270, 402]);
    provflagging('on', 502, [406]);
    provflagging('on', 504, [274, 404]);
    provflagging('on', 508, [277, 416]);
    // provflagging('on', 525, [250]);
    provflagging('on', 550, [452]);
    provflagging('on', 552, [454]);
    provflagging('on', 554, [456]);
    provflagging('on', 556, [458]);
    provflagging('on', 558, [460]);
    provflagging('on', 560, [462]);
    provflagging('on', 562, [464]);
    provflagging('on', 564, [466]);
    provflagging('on', 566, [468]);
    provflagging('on', 568, [470]);

    // for t2s510 and t2s511
    flagging(Boolean(accessor.field('CP.750').get() == "ON" || accessor.field('t2s5.013').get() && accessor.field('CP.750').get() == "MJ") &&
        (
            (
                //todo: TC flags return for only determining the applicability
                //todo: diagnostic rule overlapping with flagging condition
                //data rule before Jul 1st, 2010
                (
                    Boolean((wpw.tax.utilities.dateCompare.lessThan(accessor.field('cp.tax_end').get(), wpw.tax.date(2010, 7, 1))) && accessor.field('cp.2270').get() == '2' && !(accessor.field('t2s24.100').get() == '1' || accessor.field('t2s24.100').get() == '3' || accessor.field('t2s24.100').get() == '9' || accessor.field('t2s24.100').get() == '10' || accessor.field('t2s24.100').get() == '11' || accessor.field('cp.147').get() == '3' || accessor.field('cp.147').get() == '14'/*todo: checking t2s18 in the next version*/) && Number(accessor.field('t2s510.100').get()) > 50000000 || Number(accessor.field('t2s510.101').get()) > 100000000) ||
                    //data rule after june 30, 2010
                    Boolean(!(wpw.tax.utilities.dateCompare.lessThan(accessor.field('cp.tax_end').get(), wpw.tax.date(2010, 6, 30))) && accessor.field('cp.2270').get() == '2' && !(accessor.field('t2s24.100').get() == '1' || accessor.field('t2s24.100').get() == '3' || accessor.field('t2s24.100').get() == '9' || accessor.field('t2s24.100').get() == '10' || accessor.field('t2s24.100').get() == '11' || accessor.field('cp.147').get() == '3' || accessor.field('cp.147').get() == '14'/*todo: checking t2s18 in the next version*/) && Number(accessor.field('t2s510.100').get()) >= 50000000 && Number(accessor.field('t2s510.101').get()) >= 100000000)
                )
                && (
                    Boolean(
                        // CY data rule
                        Number(accessor.field('t2s510.120').get()) ||
                        Number(accessor.field('t2s510.540').get()) ||
                        Number(accessor.field('t2s510.600').get()) ||
                        Number(accessor.field('t2s510.620').get()) ||
                        Number(accessor.field('t2s510.650').get()) ||
                        Number(accessor.field('t2s510.670').get()) ||
                        Number(accessor.field('t2s510.131').get()) ||
                        (Number(accessor.field('t2s510.147').get()) > 0 && Number(accessor.field('t2s510.147').get()) != 0) ||
                        Number(accessor.field('t2s510.700').get()) ||
                        Number(accessor.field('t2s510.720').get()) ||
                        Number(accessor.field('t2s510.750').get()) ||
                        Number(accessor.field('t2s510.770').get())
                    )
                )
            )
            ||
            (
                Boolean(
                    // Non-CY data rule
                    // Number(accessor.field('t2s510.120').get()) ||
                    // Number(accessor.field('t2s510.540').get()) ||
                    Number(accessor.field('t2s510.600').get()) ||
                    Number(accessor.field('t2s510.620').get()) ||
                    Number(accessor.field('t2s510.650').get()) ||
                    Number(accessor.field('t2s510.670').get()) ||
                    // Number(accessor.field('t2s510.131').get()) ||
                    // (Number(accessor.field('t2s510.147').get()) > 0 && Number(accessor.field('t2s510.147').get()) != 0) ||
                    Number(accessor.field('t2s510.700').get()) ||
                    Number(accessor.field('t2s510.720').get()) ||
                    Number(accessor.field('t2s510.750').get()) ||
                    Number(accessor.field('t2s510.770').get())
                )
            )
        )
        , 'T2S510');

    flagging(((accessor.field('CP.750').get() == "ON" ||
        (accessor.field('t2s5.013').get() &&
            accessor.field('CP.750').get() == "MJ")) &&
        accessor.field('t2s510.116').get() &&
        accessor.field('t2s510.146').get()), 'T2S511');

    //exclusion rule for t2s510 and t2s511
    //diagnostic 5110003: "If Schedule 510 is filed and the corporation is associated, Schedule 511 must be filed".
    if (flaggedResult['T2S510'] && accessor.field('T2J.160').get() == '1') {
      flaggedResult['T2S511'] = true
    }

    // for t2s524
    flagging((accessor.field('CP.750').get() == "ON" ||
        accessor.field('t2s5.013').get() &&
        accessor.field('CP.750').get() == "MJ") &&
        (flaggedResult['T2S24'] ||
            wpw.tax.utilities.dateCompare.betweenInclusive(wpw.tax.date(2009, 1, 1), accessor.field('cp.tax_start').get(), accessor.field('cp.tax_end').get())) ||
        accessor.field('t2s524.101').get() == true, 'T2S524');

    // for t2s525
    flagging((Boolean(accessor.field('CP.750').get() == "ON" ||
            Boolean(accessor.field('t2s5.013').get() &&
                accessor.field('CP.750').get() == "MJ")) &&
        accessor.field('cp.250').get()
    ), 'T2S525');

    //for t2s546, t2s547, t2s548
    flagging((Boolean(accessor.field('CP.750').get() == "ON" ||
            Boolean(accessor.field('t2s5.013').get() &&
                accessor.field('CP.750').get() == "MJ")) &&
        Boolean(accessor.field('cp.149').get() == '1')
    ), 'T2S546');
    flagging((flaggedResult['T2S546'] &&
        accessor.field('t2s546.300').get() == '2'), 'T2S547');
    flagging(((accessor.field('CP.750').get() == "ON" ||
            (accessor.field('t2s5.013').get() &&
                accessor.field('CP.750').get() == "MJ")) &&
        accessor.field('cp.152').get() == 1
    ), 'T2S548');

    // exclusion rule for t2s546, t2s547, t2s548
    if (flaggedResult['T2S546'] && flaggedResult['T2S548'] && accessor.field('CP.149').get() == '1') {
      delete flaggedResult['T2S548'];
    }
    else if (flaggedResult['T2S546'] && flaggedResult['T2S548'] && accessor.field('CP.152').get() == '1') {
      delete flaggedResult['T2S546'];
    }

    // for t2s569
    flagging(((accessor.field('CP.750').get() == "ON" ||
        (accessor.field('t2s5.013').get() &&
            accessor.field('CP.750').get() == "MJ")) &&
        accessor.field('t2s5.470').get() &&
        accessor.field('t2s568.405').get()), 'T2S569');

    // These forms will never appera in cor file
    versionControl('T2S12');
    versionControl('T2S17');
    versionControl('T2S18');
    versionControl('T2S20');
    versionControl('T2S34');
    versionControl('T2S35');
    versionControl('T2S38');
    versionControl('T2S39');
    versionControl('T2S42');
    versionControl('T2S43');
    versionControl('T2S45');
    versionControl('T2S506');
    versionControl('T2S513');

    return flaggedResult;

  };
})();