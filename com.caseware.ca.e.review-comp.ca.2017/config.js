(function(wpw) {
  //Canadian Locale configuration
  wpw.main.config(['taxConfigProvider', function(conf) {
    //Set Canadian stylesheet
    conf.setCssRoot('prod/com.caseware.ca.e.review-comp.ca.2017/');
    conf.setCssFilesLocal(['style-cdn.css']);
    conf.setCssFiles(['style-cdn-global.css']);

    //Set Canadian variables
    conf.setNumLimit(13);
    // conf.setMenuVisibility(2);

    conf.setPrintHeaderFooterValues({
      taxYearEnd: 'CP.tax_end',
      corpName: 'CP.002',
      businessNum: 'CP.bn',
      hideProtectedB: ['formInfo.hideProtectedB', 'false'],
      printLandscape: ['formInfo.printLandscape', 'false'],
      footerVersion: 'CP.099',
      footerRelease: 'CP.101',
      formFooterNum: 'formInfo.formFooterNum'
    });

     // conf.setPrintTemplates(['albertaRSI']);
  }]);
})(wpw);