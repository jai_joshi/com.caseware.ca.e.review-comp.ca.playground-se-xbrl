(function() {
  var /** @const */ custom = wpw.financials.custom;

  // Statement of Retained Earnings customizations
	var reAdditions = wpw.financials.StatementOfRetainedEarnings.customizations.additions;
	
  reAdditions.push(new custom.Addition(['Retained earnings, beginning of year', 'Deficit, beginning of year',
      'Retained earnings (deficit), beginning of year'], true, 'retained_earnings_beginning', wpw.models.Tag.SIGN.CREDIT,
      custom.PLACEMENT.INSERT, ['285.100'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, custom.TYPE.LINE));

  // Add net income
  reAdditions.push(new custom.Addition(['Net income', 'Net loss', 'Net income (loss)'], true, 'net_income',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['300','400','500','600','860','870'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1,
      custom.TYPE.LINE));

  // Add a total for the two dividends
  reAdditions.push(new custom.Addition('Dividends', true, 'dividends', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, ['285.300'], custom.ADD_SCHEDULE_LINE.TOP,
      {}, 1, custom.TYPE.LINE));
	
	  // Prior Period Adjustments (285.400)
  reAdditions.push(new custom.Addition('Prior period adjustments', true, 'prior_period_adjustments', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, ['285.400'], custom.ADD_SCHEDULE_LINE.TOP,
      {}, 1, custom.TYPE.LINE));
	
	  // Other items affecting retained earnings (285.500)
  reAdditions.push(new custom.Addition('Other items affecting retained earnings', true, 'other_items_affecting_retained_earnings', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, ['285.500'], custom.ADD_SCHEDULE_LINE.TOP,
      {}, 1, custom.TYPE.LINE));

  // Add Ending retained earnings to the bottom of the Statement of Equity
  reAdditions.push(new custom.Addition(['Retained earnings, end of year', 'Deficit, end of year',
      'Retained earnings (deficit), end of year'], false, 'retained_earnings_end', wpw.models.Tag.SIGN.CREDIT,
      custom.PLACEMENT.INSERT, ['285'], custom.ADD_SCHEDULE_LINE.BOTTOM, {}, 1, custom.TYPE.TOTAL));
	
})();
