(function() {
  var /** @const */ custom = wpw.financials.custom;

  /** @type {string} The name of the dimension category to print by */
  wpw.financials.StatementOfFunctionalExpenses.customizations.dimensionCategory = 'Services';

  var expensesCustomization = wpw.financials.StatementOfFunctionalExpenses.customizations;

  expensesCustomization.topLevelTags.push(new custom.TopLevelTagInfo('2.040'));
})();
