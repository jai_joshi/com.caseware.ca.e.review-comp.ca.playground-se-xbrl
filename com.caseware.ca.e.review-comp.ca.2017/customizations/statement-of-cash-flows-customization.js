(function() {
  var /** @const */ custom = wpw.financials.custom;

  //Statement of Cash Flows customizations
  var cfAdditions = wpw.financials.StatementOfCashFlows.customizations.additions;

  //Operating Activities
  cfAdditions.push(new custom.Addition('Operating activities', true, 'OP_TITLE', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, ['OP_SUB', '-OP_DIFF'], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 0,
      wpw.financials.custom.TYPE.TITLE));

  cfAdditions.push(new custom.Addition('Net income (loss)', true, 'NET_INC',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['-285.200'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true'}, 1, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Adjustments to reconcile net income (loss) to net cash provided by (used in) operations',
      true, 'OP_SUB', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['OP_0', 'OP_1', 'OP_2', 'OP_3', 'OP_4',
        'OP_5', 'OP_6', 'OP_7', 'OP_8', 'OP_9', 'OP_10'], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true', canAdd: 'true'},
      1, wpw.financials.custom.TYPE.TITLE));

  cfAdditions.push(new custom.Addition('Depreciation, depletion and amortization', true, 'OP_0',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['513.100', '521.100', '571.400.84', '571.400.02', '519'],
      custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Allowance for doubtful accounts', true, 'OP_1', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, ['515', '516.100'], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Inventory write-down', true, 'OP_2', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, ['428'], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Goodwill impairment loss', true, 'OP_3', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, ['514'], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Asset impairment charge', true, 'OP_4', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, ['513.200', '521.200'],
      custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Gains (losses) on disposition of property, plant and equipment', true, 'OP_5',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['605.200'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Gains (losses) on disposition of intangible assets', true, 'OP_6',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['605.300'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Realized gains (losses) on sale of investments', true,
      'OP_7', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['605.100'],
      custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Future income taxes', true, 'OP_8', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, ['860.200'], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Other adjustments', true, 'OP_9', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Unrealized gains (losses) on securities', true, 'OP_10', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, ['680.200'], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Change in working capital', true, 'OP_DIFF',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['OP_11', 'OP_12', 'OP_13', 'OP_14', 'OP_15', 'OP_16',
        'OP_17', 'OP_18', 'OP_19', 'OP_20', 'OP_21', 'OP_22', 'OP_23', 'OP_24', 'OP_25', 'OP_26', 'OP_27', 'OP_28', 'OP_29'], 
        custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true', canAdd: 'true'}, 1, wpw.financials.custom.TYPE.TITLE));

  cfAdditions.push(new custom.Addition('Receivables', true, 'OP_11', wpw.models.Tag.SIGN.CREDIT,
      custom.PLACEMENT.INSERT, ['115', '127'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Inventories', true, 'OP_12', wpw.models.Tag.SIGN.CREDIT,
      custom.PLACEMENT.INSERT, ['125'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Trading securities', true, 'OP_13',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['113'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Other current assets', true, 'OP_14',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['128'],
      custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2,
      wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Current income taxes', true, 'OP_15',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['129'],
      custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2,
      wpw.financials.custom.TYPE.LINE));    

  cfAdditions.push(new custom.Addition('Other assets', true, 'OP_16',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['116', '118', '121'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Related party receivables', true, 'OP_17',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['123.200', '123.300', '123.800', '123.100'],
      custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2,
      wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Investment in joint ventures and partnerships', true, 'OP_18',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['126'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Deferred items and charges', true, 'OP_19',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['181.400'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Accounts payable and accrued expenses', true, 'OP_20',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['215'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Income taxes payable', true, 'OP_21',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['217'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Other changes, net', true, 'OP_22',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['216'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Due to related parties', true, 'OP_23',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['223'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Short term debt', true, 'OP_24',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['213'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Due to shareholders and directors', true, 'OP_25',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['221', '222'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Other current liabilities', true, 'OP_26',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['228'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Other income taxes', true, 'OP_27',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['227'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Other liabilities', true, 'OP_28',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['250', '224', '225', '226'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Deferred revenue', true, 'OP_29',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['218'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', balanceType: 'differenceFromPrior'}, 2, wpw.financials.custom.TYPE.LINE));

  cfAdditions.push(new custom.Addition('Cash flows from operating activities', false, 'OP_TOTAL',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['NET_INC', 'OP_SUB', '-OP_DIFF'],
      custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 1, wpw.financials.custom.TYPE.TOTAL));

  //Investing Activities
  cfAdditions.push(new custom.Addition('Investing activities', true, 'IA_TITLE', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, ['IA_1', 'IA_2', 'IA_3', 'IA_4', 'IA_5', 'IA_6', 'IA_7', 'IA_8', 'IA_9', 'IA_10',
        'IA_11', 'IA_12', 'IA_13'], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true', canAdd: 'true',
        extraPadding: 'top'}, 1, wpw.financials.custom.TYPE.TITLE));

  cfAdditions.push(new custom.Addition('Purchase of property, plant, and equipment', true, 'IA_1',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Proceeds from the sale of property, plant and equipment', true, 'IA_2',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Payments to acquire intangible assets', true, 'IA_3',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Payments to acquire equipment on lease', true, 'IA_4',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Purchase of marketable securities', true, 'IA_5',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Purchase of investments', true, 'IA_6',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Acquisition of businesses, net of cash acquired', true, 'IA_7',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Divestiture of businesses', true, 'IA_8',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Proceeds from sale and maturity of marketable securities', true, 'IA_9',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Proceeds from sale of short-term investments', true, 'IA_10',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Purchase of short-term investments', true,
      'IA_11', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Repayments of notes receivable', true, 'IA_12',wpw.models.Tag.SIGN.DEBIT, 
      custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Other investing activities, net', true, 'IA_13',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));   

  cfAdditions.push(new custom.Addition('Cash flows from investing activities', false, 'IA_TOTAL',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT,
      ['IA_1', 'IA_2', 'IA_3', 'IA_4', 'IA_5', 'IA_6', 'IA_7', 'IA_8', 'IA_9', 'IA_10', 'IA_11', 'IA_12', 'IA_13', 'IA_14'],
      custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 1, wpw.financials.custom.TYPE.TOTAL));

  //Financing Activities
  cfAdditions.push(new custom.Addition('Financing activities', true, 'FA_TITLE', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, ['FA_1', 'FA_2', 'FA_3', 'FA_4', 'FA_5', 'FA_6', 'FA_7', 'FA_8', 'FA_9', 'FA_10',
        'FA_11', 'FA_12', 'FA_13', 'FA_14', 'FA_15', 'FA_16', 'FA_17', 'FA_18'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true', canAdd: 'true', extraPadding: 'top'}, 1, wpw.financials.custom.TYPE.TITLE));

  cfAdditions.push(new custom.Addition('Proceeds from long-term debt', true, 'FA_1',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Repayments of long-term debt', true, 'FA_2',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Proceeds from issuance of common shares', true, 'FA_3',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Proceeds from issuance of preferred shares', true, 'FA_4',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Payments of dividends on common shares', true, 'FA_5',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Payments of dividends on preferred shares', true, 'FA_6',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Redemption of common shares', true, 'FA_7',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Redemption of preferred shares', true, 'FA_8',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Net change in the balance of refundable dividend tax', true, 'FA_9',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Change in bank overdrafts (net)', true, 'FA_10',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Proceeds from short-term borrowings', true, 'FA_11',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Repayments of short-term borrowings', true,
      'FA_12', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Net change, long term debt and mandatorily redeemable securities', true, 'FA_13',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Payment for repurchases of equity', true, 'FA_14',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Repayments on capital lease obligations', true, 'FA_15',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Proceeds from notes payable', true, 'FA_16',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Repayments on notes payable', true, 'FA_17',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Cash flows from financing activities', false, 'FA_TOTAL',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT,
      ['FA_1', 'FA_2', 'FA_3', 'FA_4', 'FA_5', 'FA_6', 'FA_7', 'FA_8', 'FA_9', 'FA_10', 'FA_11', 'FA_12', 'FA_13',
        'FA_14', 'FA_15', 'FA_16', 'FA_17'], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 1,
      wpw.financials.custom.TYPE.TOTAL));

  //Other Activities
  cfAdditions.push(new custom.Addition('Other activities', true, 'OA_TITLE', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, ['OA_1'], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true', canAdd: 'true',
        extraPadding: 'top'}, 1, wpw.financials.custom.TYPE.TITLE));

  cfAdditions.push(new custom.Addition('Effect of exchange rate on cash and cash equivalents', true, 'OA_1',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 2,
      wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Cash flows from other activities', false, 'OA_TOTAL', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, ['OA_TITLE'], custom.ADD_SCHEDULE_LINE.TOP, {hideWhenZero: 'true'}, 1,
      wpw.financials.custom.TYPE.TOTAL));

  //Subtotals
  cfAdditions.push(new custom.Addition('Net increase (decrease) in cash and cash equivalents', false, 'NET',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['OP_TOTAL', 'IA_TOTAL', 'FA_TOTAL', 'OA_TOTAL'],
      custom.ADD_SCHEDULE_LINE.TOP, {underline: 'none'}, 1, wpw.financials.custom.TYPE.TOTAL));

  cfAdditions.push(new custom.Addition('Cash and cash equivalents, beginning of year', true, 'CASH_BEGIN',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.1.000'], custom.ADD_SCHEDULE_LINE.TOP,
      {balanceType: 'priorPeriod'}, 1, wpw.financials.custom.TYPE.INPUT));

  cfAdditions.push(new custom.Addition('Cash and cash equivalents, end of year', false, 'CASH_END',
      wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['NET', 'CASH_BEGIN'], custom.ADD_SCHEDULE_LINE.TOP, {}, 0,
      wpw.financials.custom.TYPE.TOTAL));
})();
