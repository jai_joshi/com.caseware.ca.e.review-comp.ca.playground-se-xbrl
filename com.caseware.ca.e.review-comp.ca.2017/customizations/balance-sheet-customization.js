(function() {
  var /** @const */ custom = wpw.financials.custom;

  // Balance sheet customizations
  var bsAdditions = wpw.financials.BalanceSheet.customizations.additions;

  bsAdditions.push(new custom.Addition('Assets', true, 'assets', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.TOP_LEVEL,
      ['105','130'], custom.ADD_SCHEDULE_LINE.TOP, {}, 0, wpw.financials.custom.TYPE.LINE));

  bsAdditions.push(new custom.Addition('Liabilities and equity', true, 'liabilities_and_equity', wpw.models.Tag.SIGN.CREDIT,
      custom.PLACEMENT.TOP_LEVEL, ['205', '270'], custom.ADD_SCHEDULE_LINE.TOP,
      {extraPadding: 'top', omitChildPadding: true}, 0, wpw.financials.custom.TYPE.LINE));
  wpw.financials.financialsSetup.roundingErrorGroupNumbers = ['100', '200'];
})();
