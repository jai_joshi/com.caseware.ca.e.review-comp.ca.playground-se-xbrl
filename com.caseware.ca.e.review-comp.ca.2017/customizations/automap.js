(function() {
  // convert the account number to dotted notation, "10123" -> "1.0.1.2.3"
  function toDottedNotation(accountNumber) {
	if (!accountNumber || accountNumber.indexOf('.') >= 0) {
	  return accountNumber;
	}
	return accountNumber.split('').join('.');
  }

  function findMatch(groupNumbers, accountNumber) {
	if (!accountNumber) {
	  return null;
	}
	var tag = groupNumbers.find(function(groupNumber) {
	  return groupNumber === accountNumber;
	});
	if (tag) {
	  return tag;
	}

	var parentNumber = '';
	var index = accountNumber.lastIndexOf('.');
	if (index !== -1) {
	  parentNumber = accountNumber.substring(0, index);
	}
	return findMatch(groupNumbers, parentNumber);
  }

  wpw.tb.importUtilities.setAutoMapFunction(function(groupNumbers, accountNumber) {
    return findMatch(groupNumbers, toDottedNotation(accountNumber));
  });	
})();
