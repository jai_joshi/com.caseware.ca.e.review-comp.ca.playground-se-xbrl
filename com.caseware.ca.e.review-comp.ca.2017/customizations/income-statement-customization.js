(function() {
  var /** @const */ custom = wpw.financials.custom;
  var is = wpw.financials.IncomeStatement.customizations;

  // Revenue
  is.topLevelTags.push(new custom.TopLevelTagInfo('300'));

  // Cost of sales
  is.topLevelTags.push(new custom.TopLevelTagInfo('400'));

  // Gross profit
  is.additions.push(new custom.Addition('Gross profit', false, 'gross_profit',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['300', '400'], custom.ADD_SCHEDULE_LINE.AFTER,
      {omitChildPadding: true, anchor: '400', underline: 'normal'}, 0, wpw.financials.custom.TYPE.TOTAL));

  // Operating expenses
  is.topLevelTags.push(new custom.TopLevelTagInfo('500', {extraPadding: 'top-small', underline: 'normal'}));

  // Income from operations [585]
  is.additions.push(new custom.Addition(
      ['Income from operations', 'Loss from operations', 'Income (loss) from operations'], false,
      'income_from_operations', wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['gross_profit', '500'],
      custom.ADD_SCHEDULE_LINE.AFTER, {omitChildPadding: true, anchor: '500', underline: 'normal'}, 0,
      wpw.financials.custom.TYPE.TOTAL));

  // Non-operating income (expense) [600]
  is.topLevelTags.push(new custom.TopLevelTagInfo('600', {extraPadding: 'top-small'}));

  // Income before income tax expense [855]
  is.additions.push(new custom.Addition(
      ['Income from continuing operations before income taxes', 'Loss from continuing operations before income taxes', 'Income (loss) from continuing operations before income taxes'],
      false, 'income_before_income_tax_expense', wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT,
      ['300', '400', '500', '600'], custom.ADD_SCHEDULE_LINE.AFTER,
      {omitChildPadding: true, anchor: '600', underline: 'normal'}, 0, wpw.financials.custom.TYPE.TOTAL));

  // Income taxes [860]
  is.topLevelTags.push(new custom.TopLevelTagInfo('860'));

  // Income from continuing operations [865]
  is.additions.push(new custom.Addition(
      ['Income from continuing operations', 'Loss from continuing operations', 'Income (loss) from continuing operations'],
      false, 'income_before_income_tax_expense', wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT,
      ['300', '400', '500', '800','860'], custom.ADD_SCHEDULE_LINE.AFTER,
      {omitChildPadding: true, anchor: '860', underline: 'normal'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
  // Other comprehensive income [870]
  is.topLevelTags.push(new custom.TopLevelTagInfo('870'));
	

  // Net income
  is.additions.push(new custom.Addition(['Net income', 'Net loss', 'Net income (loss)'], false, 'inserted_net_income',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['300','400','500','600','860','870'], custom.ADD_SCHEDULE_LINE.BOTTOM, {}, 0,
      wpw.financials.custom.TYPE.TOTAL));
})();
